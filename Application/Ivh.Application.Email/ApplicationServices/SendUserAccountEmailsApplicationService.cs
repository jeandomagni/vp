namespace Ivh.Application.Email.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using Base.Common.Interfaces;
    using Common.Dtos;
    using Common.Interfaces;
    using Core.Common.Dtos;
    using Domain.Email.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.User.Entities;
    using Domain.User.Interfaces;
    using Internal.Dtos;
    using Internal.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Attributes;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Communication;
    using Ivh.Common.VisitPay.Messages.Core;
    using Ivh.Common.VisitPay.Strings;
    using Ivh.Common.Web.Constants;
    using MassTransit;

    public class SendUserAccountEmailsApplicationService : CommunicationApplicationServiceBase, ISendUserAccountEmailsApplicationService
    {
        public SendUserAccountEmailsApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<ICommunicationSender> communicationSender,
            Lazy<ICommunicationTemplateLoader> communicationTemplateLoader,
            Lazy<ICommunicationService> communicationService,
            Lazy<IEmailService> emailService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IVisitPayUserRepository> visitPayUserRepository,
            Lazy<IVisitPayUserService> visitPayUserService)
            : base(
                applicationServiceCommonService,
                communicationSender,
                communicationTemplateLoader,
                communicationService,
                emailService,
                guarantorService,
                visitPayUserRepository,
                visitPayUserService)
        {
        }

        private void AccountClosedCompleteCancelation(int visitPayUserId)
        {
            this.SendCommunications(SendTo.User(visitPayUserId), CommunicationTypeEnum.AccountCancelationComplete, visitPayUser => new Dictionary<string, string>
            {
                {"[[PersonName]]", visitPayUser.FirstName}
            });
        }

        private void AccountCredentialChangePatientUser(int visitPayUserId)
        {
            this.SendCommunications(SendTo.User(visitPayUserId), CommunicationTypeEnum.AccountCredentialChangeConfirmationPatient, visitPayUser => new Dictionary<string, string>
            {
                {"[[FirstName]]", visitPayUser.FirstName}
            });
        }

        private void AccountReactivationComplete(int visitPayUserId)
        {
            this.SendCommunications(SendTo.User(visitPayUserId), CommunicationTypeEnum.AccountReactivationComplete, visitPayUser => new Dictionary<string, string>
            {
                {"[[PersonName]]", visitPayUser.FirstName},
                {"[[username]]", visitPayUser.UserName},
            });
        }

        private void ClientAccountLockout(int visitPayUserId)
        {
            this.SendCommunications(SendTo.User(visitPayUserId), CommunicationTypeEnum.ClientAccountLockout, visitPayUser => new Dictionary<string, string>
            {
                {"[[FirstName]]", visitPayUser.FirstName}
            });
        }

        private void ClientProfileChange(int visitPayUserId)
        {
            this.SendCommunications(SendTo.User(visitPayUserId), CommunicationTypeEnum.ClientProfileChange, visitPayUser => new Dictionary<string, string>
            {
                {"[[FirstName]]", visitPayUser.FirstName},
            });
        }

        private void CreditAgreement(int visitPayUserId, int financePlanOfferId, DateTime actionDate)
        {
            string creditAgreementUrl = string.Format(this.GetClient().AppCreditAgreementUrl, financePlanOfferId);

            this.SendCommunications(SendTo.User(visitPayUserId), CommunicationTypeEnum.VpccCreditAgreement, visitPayUser => new Dictionary<string, string>
            {
                {"[[Name]]", visitPayUser.FirstName},
                {"[[AppCreditAgreementUrl]]", creditAgreementUrl},
                {"[[FinancePlanExpirationDate]]", actionDate.Date.ToString(Format.DateFormat)},
            });
        }

        private void ForgotPasswordWithOutPinClientUser(int visitPayUserId, string tempPassword)
        {
            string tempPasswordExpiration = this.GetClient().ClientTempPasswordExpLimitInHours.ToString(CultureInfo.InvariantCulture);

            this.SendCommunications(SendTo.User(visitPayUserId), CommunicationTypeEnum.ForgotPasswordWithoutPinClient, visitPayUser => new Dictionary<string, string>
            {
                {"[[FirstName]]", string.IsNullOrWhiteSpace(visitPayUser.FirstName) ? string.Empty : visitPayUser.FirstName},
                {"[[ClientResetPasswordPath]]", UrlPath.ClientResetPassword},
                {"[[ClientForgotPasswordPath]]", UrlPath.ClientForgotPassword},
                {"[[tempPassword]]", tempPassword},
                {"[[TempPasswordExpLimit]]", tempPasswordExpiration},
            });
        }

        private void ForgotPasswordWithOutPinPatientUser(int visitPayUserId, string tempPassword)
        {
            string tempPasswordExpiration = this.GetClient().GuarantorTempPasswordExpLimitInHours.ToString(CultureInfo.InvariantCulture);

            this.SendCommunications(SendTo.User(visitPayUserId), CommunicationTypeEnum.ForgotPasswordWithoutPinPatient, visitPayUser => new Dictionary<string, string>
            {
                {"[[FirstName]]", visitPayUser.FirstName},
                {"[[GuarantorResetPasswordPath]]", UrlPath.GuarantorResetPassword},
                {"[[GuarantorForgotPasswordPath]]", UrlPath.GuarantorForgotPassword},
                {"[[tempPassword]]", tempPassword},
                {"[[TempPasswordExpLimit]]", tempPasswordExpiration},
            });
        }

        private void ForgotUsername(int visitPayUserId)
        {
            this.SendCommunications(SendTo.User(visitPayUserId), CommunicationTypeEnum.ForgotUsername, visitPayUser => new Dictionary<string, string>
            {
                {"[[FirstName]]", visitPayUser.FirstName},
                {"[[Username]]", visitPayUser.UserName},
            });
        }

        private void GuarantorAccountLockout(int visitPayUserId)
        {
            this.SendCommunications(SendTo.User(visitPayUserId), CommunicationTypeEnum.GuarantorAccountLockout, visitPayUser => new Dictionary<string, string>
            {
                {"[[FirstName]]", visitPayUser.FirstName}
            });
        }

        private void NewUserInvitation(string firstName, string toEmail, Guid token)
        {
            string registrationUrl = string.Format(this.GetClient().AppGuarantorRegistrationUrl, token);

            this.SendCommunications(toEmail, CommunicationTypeEnum.NewUserInvitation, () => new Dictionary<string, string>
            {
                {"[[FirstName]]", firstName},
                {"[[AppGuarantorRegistrationUrl]]", registrationUrl},
            });
        }

        private void NewUserWithOutPinClientUser(int visitPayUserId, string tempPassword)
        {
            string tempPasswordExpiration = this.GetClient().ClientTempPasswordExpLimitInHours.ToString(CultureInfo.InvariantCulture);

            this.SendCommunications(SendTo.User(visitPayUserId), CommunicationTypeEnum.NewUserWithoutPinClient, visitPayUser => new Dictionary<string, string>
            {
                {"[[FirstName]]", string.IsNullOrWhiteSpace(visitPayUser.FirstName) ? string.Empty : visitPayUser.FirstName},
                {"[[tempPassword]]", tempPassword},
                {"[[TempPasswordExpLimit]]", tempPasswordExpiration},
            });
        }

        private void PostRegistrationWelcome(int visitPayUserId)
        {
            ClientDto client = this.GetClient();
            string createFinancePlanUrl = $"{client.AppGuarantorUrlHost}{UrlPath.CreateFinancePlan}";
            string manageHouseholdUrl = $"{client.AppGuarantorUrlHost}{UrlPath.ManageHouseholdUrl}";
            string registrationPendingDays = client.RegistrationPendingDays.ToString(CultureInfo.InvariantCulture);

            this.SendCommunications(SendTo.User(visitPayUserId), CommunicationTypeEnum.RegistrationSuccess, visitPayUser => new Dictionary<string, string>
            {
                {"[[Name]]", visitPayUser.FirstName},
                {"[[CreateFinancePlanUrl]]", createFinancePlanUrl},
                {"[[ManageHouseholdUrl]]", manageHouseholdUrl},
                {"[[PendingDays]]", registrationPendingDays},
            });
        }

        private void ResetPasswordChangedClientUser(int visitPayUserId)
        {
            this.SendCommunications(SendTo.User(visitPayUserId), CommunicationTypeEnum.PasswordResetConfirmationClient, visitPayUser => new Dictionary<string, string>
            {
                {"[[FirstName]]", visitPayUser.FirstName}
            });
        }

        private void SmsActivateEmailAlert(int visitPayUserId)
        {
            this.SendCommunications(SendTo.User(visitPayUserId), CommunicationTypeEnum.SmsActivateEmailAlert, visitPayUser => new Dictionary<string, string>
            {
                {"[[FirstName]]", visitPayUser.FirstName},
            });
        }

        private void SmsDeactivateEmailAlert(int visitPayUserId)
        {
            this.SendCommunications(SendTo.User(visitPayUserId), CommunicationTypeEnum.SmsDeactivateEmailAlert, visitPayUser => new Dictionary<string, string>
            {
                {"[[FirstName]]", visitPayUser.FirstName},
            });
        }

        /// <summary>
        /// this is a one-off because the user hasn't accepted the SMS acknowledgement at this point, and we check for that before sending other SMS's
        /// </summary>
        private void SmsValidationMessage(int visitPayUserId, string phoneNumber, string token)
        {
            const CommunicationTypeEnum communicationType = CommunicationTypeEnum.SmsValidationMessage;
            CommunicationAttribute attribute = this.GetAttribute(communicationType);
            if (attribute.SendSms && this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.Sms))
            {
                VisitPayUser visitPayUser = this.VisitPayUserRepository.Value.FindById(visitPayUserId.ToString());
                Dictionary<string, string> replacementValues = new Dictionary<string, string>
                {
                    {"[[Token]]", token}
                };

                string toPhoneNumber = phoneNumber;
                SendTo sendToVisitPayUser = new SendTo { VisitPayUserId = visitPayUserId };
				CommunicationDto sms = this.CommunicationTemplateLoader.Value.CreateSmsFromTemplate(communicationType, this.GetClient(), sendToVisitPayUser, visitPayUser.Locale, toPhoneNumber, replacementValues, false, false);
                this.SendSms(sms, attribute.Frequency);
            }
        }

        private void UserProfileChange(int visitPayUserId, string email)
        {
            this.SendCommunications(SendTo.User(visitPayUserId), CommunicationTypeEnum.UserProfileChange, visitPayUser => new Dictionary<string, string>
            {
                {"[[Name]]", visitPayUser.FirstName}
            }, email); // overriding email address because this sends to the users previous e-mail address during an e-mail address change.
        }

        private void SendVpccLoginLinkSms(int vpGuarantorId, string tempPassword, string phoneNumber)
        {
            const CommunicationTypeEnum communicationType = CommunicationTypeEnum.VpccLoginLinkSms;
            CommunicationAttribute attribute = this.GetAttribute(communicationType);
            if (!attribute.SendSms || !this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.Sms))
            {
                return;
            }

            string tempPasswordExpiration = this.GetClient().ClientTempPasswordExpLimitInHours.ToString(CultureInfo.InvariantCulture);

            Guarantor guarantor = this.GuarantorService.Value.GetGuarantor(vpGuarantorId);
            SendTo sendToVisitPayUser = new SendTo
            {
	            VisitPayUserId = guarantor.User.VisitPayUserId,
	            VpGuarantorId = vpGuarantorId
            };
			Dictionary<string, string> replacementValues = new Dictionary<string, string>
            {
                {"[[GuarantorVpccTokenLogin]]", UrlPath.GuarantorVpccTokenLogin},
                {"[[Token]]", tempPassword},
                {"[[TempPasswordExpLimit]]", tempPasswordExpiration}
            };

            CommunicationDto sms = this.CommunicationTemplateLoader.Value.CreateSmsFromTemplate(communicationType, this.GetClient(), sendToVisitPayUser, guarantor.User.Locale, phoneNumber, replacementValues, false, false);
            this.SendSms(sms, attribute.Frequency);
        }

        private void SendVpccLoginLink(int vpGuarantorId, string tempPassword)
        {
            string tempPasswordExpiration = this.GetClient().ClientTempPasswordExpLimitInHours.ToString(CultureInfo.InvariantCulture);

            this.SendCommunications(SendTo.Guarantor(vpGuarantorId), CommunicationTypeEnum.VpccLoginLink, visitPayUser => new Dictionary<string, string>
            {
                {"[[FirstName]]", visitPayUser.FirstName},
                {"[[GuarantorVpccTokenLogin]]", UrlPath.GuarantorVpccTokenLogin},
                {"[[Token]]", tempPassword},
                {"[[TempPasswordExpLimit]]", tempPasswordExpiration}
            });
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(SendUserProfileChangeEmailMessage message, ConsumeContext<SendUserProfileChangeEmailMessage> consumeContext)
        {
            this.UserProfileChange(message.VisitPayUserId, message.Email);
        }

        public bool IsMessageValid(SendUserProfileChangeEmailMessage message, ConsumeContext<SendUserProfileChangeEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(SendSmsValidationMessage message, ConsumeContext<SendSmsValidationMessage> consumeContext)
        {
            this.SmsValidationMessage(message.VisitPayUserId, message.Phone, message.Token);
        }

        public bool IsMessageValid(SendSmsValidationMessage message, ConsumeContext<SendSmsValidationMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(SendSmsDeactivateAlertEmailMessage message, ConsumeContext<SendSmsDeactivateAlertEmailMessage> consumeContext)
        {
            this.SmsDeactivateEmailAlert(message.VisitPayUserId);
        }

        public bool IsMessageValid(SendSmsDeactivateAlertEmailMessage message, ConsumeContext<SendSmsDeactivateAlertEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(SendSmsActivateAlertEmailMessage message, ConsumeContext<SendSmsActivateAlertEmailMessage> consumeContext)
        {
            this.SmsActivateEmailAlert(message.VisitPayUserId);
        }

        public bool IsMessageValid(SendSmsActivateAlertEmailMessage message, ConsumeContext<SendSmsActivateAlertEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(SendPostRegistrationWelcomeMessage message, ConsumeContext<SendPostRegistrationWelcomeMessage> consumeContext)
        {
            this.PostRegistrationWelcome(message.VisitPayUserId);
        }

        public bool IsMessageValid(SendPostRegistrationWelcomeMessage message, ConsumeContext<SendPostRegistrationWelcomeMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(SendClientProfileChangeEmailMessage message, ConsumeContext<SendClientProfileChangeEmailMessage> consumeContext)
        {
            this.ClientProfileChange(message.VisitPayUserId);
        }

        public bool IsMessageValid(SendClientProfileChangeEmailMessage message, ConsumeContext<SendClientProfileChangeEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(SendAccountCredentialChangePatientEmailMessage message, ConsumeContext<SendAccountCredentialChangePatientEmailMessage> consumeContext)
        {
            this.AccountCredentialChangePatientUser(message.VisitPayUserId);
        }

        public bool IsMessageValid(SendAccountCredentialChangePatientEmailMessage message, ConsumeContext<SendAccountCredentialChangePatientEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority10)]
        public void ConsumeMessage(SendClientPasswordWithoutPinEmailMessage message, ConsumeContext<SendClientPasswordWithoutPinEmailMessage> consumeContext)
        {
            this.ForgotPasswordWithOutPinClientUser(message.VisitPayUserId, message.TempPassword);
        }

        public bool IsMessageValid(SendClientPasswordWithoutPinEmailMessage message, ConsumeContext<SendClientPasswordWithoutPinEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority10)]
        public void ConsumeMessage(SendClientResetPasswordEmailMessage message, ConsumeContext<SendClientResetPasswordEmailMessage> consumeContext)
        {
            this.ResetPasswordChangedClientUser(message.VisitPayUserId);
        }

        public bool IsMessageValid(SendClientResetPasswordEmailMessage message, ConsumeContext<SendClientResetPasswordEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority10)]
        public void ConsumeMessage(SendPaperCreditAgreementMailMessage message, ConsumeContext<SendPaperCreditAgreementMailMessage> consumeContext)
        {
            this.CreditAgreement(message.VisitPayUserId, message.FinancePlanOfferId, message.ActionDate);
        }

        public bool IsMessageValid(SendPaperCreditAgreementMailMessage message, ConsumeContext<SendPaperCreditAgreementMailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority10)]
        public void ConsumeMessage(SendNewUserClientPasswordWithoutPinEmailMessage message, ConsumeContext<SendNewUserClientPasswordWithoutPinEmailMessage> consumeContext)
        {
            this.NewUserWithOutPinClientUser(message.VisitPayUserId, message.TempPassword);
        }

        public bool IsMessageValid(SendNewUserClientPasswordWithoutPinEmailMessage message, ConsumeContext<SendNewUserClientPasswordWithoutPinEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority10)]
        public void ConsumeMessage(SendNewUserInvitationEmailMessage message, ConsumeContext<SendNewUserInvitationEmailMessage> consumeContext)
        {
            this.NewUserInvitation(message.FirstName, message.Email, message.Token);
        }

        public bool IsMessageValid(SendNewUserInvitationEmailMessage message, ConsumeContext<SendNewUserInvitationEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority10)]
        public void ConsumeMessage(SendPasswordWithoutPinEmailMessage message, ConsumeContext<SendPasswordWithoutPinEmailMessage> consumeContext)
        {
            this.ForgotPasswordWithOutPinPatientUser(message.VisitPayUserId, message.TempPassword);
        }

        public bool IsMessageValid(SendPasswordWithoutPinEmailMessage message, ConsumeContext<SendPasswordWithoutPinEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority10)]
        public void ConsumeMessage(SendUsernameReminderEmailMessage message, ConsumeContext<SendUsernameReminderEmailMessage> consumeContext)
        {
            this.ForgotUsername(message.VisitPayUserId);
        }

        public bool IsMessageValid(SendUsernameReminderEmailMessage message, ConsumeContext<SendUsernameReminderEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SendAccountClosedCompleteMessage message, ConsumeContext<SendAccountClosedCompleteMessage> consumeContext)
        {
            this.AccountClosedCompleteCancelation(message.VisitPayUserId);
        }

        public bool IsMessageValid(SendAccountClosedCompleteMessage message, ConsumeContext<SendAccountClosedCompleteMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SendAccountReactivationCompleteMessage message, ConsumeContext<SendAccountReactivationCompleteMessage> consumeContext)
        {
            this.AccountReactivationComplete(message.VisitPayUserId);
        }

        public bool IsMessageValid(SendAccountReactivationCompleteMessage message, ConsumeContext<SendAccountReactivationCompleteMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SendClientAccountLockoutEmailMessage message, ConsumeContext<SendClientAccountLockoutEmailMessage> consumeContext)
        {
            this.ClientAccountLockout(message.VisitPayUserId);
        }

        public bool IsMessageValid(SendClientAccountLockoutEmailMessage message, ConsumeContext<SendClientAccountLockoutEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SendGuarantorAccountLockoutEmailMessage message, ConsumeContext<SendGuarantorAccountLockoutEmailMessage> consumeContext)
        {
            this.GuarantorAccountLockout(message.VisitPayUserId);
        }

        public bool IsMessageValid(SendGuarantorAccountLockoutEmailMessage message, ConsumeContext<SendGuarantorAccountLockoutEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SendVpccLoginLinkEmailMessage message, ConsumeContext<SendVpccLoginLinkEmailMessage> consumeContext)
        {
            this.SendVpccLoginLink(message.VpGuarantorId, message.TempPassword);
        }

        public bool IsMessageValid(SendVpccLoginLinkEmailMessage message, ConsumeContext<SendVpccLoginLinkEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SendVpccLoginLinkSmsMessage message, ConsumeContext<SendVpccLoginLinkSmsMessage> consumeContext)
        {
            this.SendVpccLoginLinkSms(message.VpGuarantorId, message.TempPassword, message.PhoneNumber);
        }

        public bool IsMessageValid(SendVpccLoginLinkSmsMessage message, ConsumeContext<SendVpccLoginLinkSmsMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }


        #endregion
    }
}
namespace Ivh.Application.Email.ApplicationServices
{
    using System;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Core.Common.Dtos;
    using Domain.Email.Interfaces;
    using Internal.Interfaces;

    public abstract class GuestPayEmailApplicationServiceBase : ApplicationService
    {
        private readonly Lazy<IEmailService> _emailService;
        private readonly Lazy<ICommunicationSender> _communicationSender;

        protected GuestPayEmailApplicationServiceBase(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IEmailService> emailService,
            Lazy<ICommunicationSender> communicationSender) : base(applicationServiceCommonService)
        {
            this._emailService = emailService;
            this._communicationSender = communicationSender;
        }
        
        private ClientDto _clientDto;
        protected ClientDto GetClient()
        {
            return this._clientDto ?? (this._clientDto = Mapper.Map<ClientDto>(this.ClientService.Value.GetClient()));
        }

        protected virtual void SendEmail(CommunicationDto communicationDto)
        {
            if (communicationDto == null)
            {
                this.LoggingService.Value.Fatal(() => "GuestPayEmailApplicationServiceBase::Email is null - Stack Trace: {0}", Environment.StackTrace);
                return;
            }

            this._emailService.Value.Update(communicationDto);

            if (!this._communicationSender.Value.SendEmailAsync(communicationDto).Result)
            {
                this.LoggingService.Value.Warn(() =>  $"Unable to send email {communicationDto.Subject} to {communicationDto.ToAddress}");
            }
        }
    }
}
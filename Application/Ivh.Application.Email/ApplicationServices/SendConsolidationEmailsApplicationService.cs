﻿namespace Ivh.Application.Email.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using Base.Common.Interfaces;
    using Common.Interfaces;
    using Domain.Guarantor.Interfaces;
    using Domain.Email.Interfaces;
    using Domain.User.Interfaces;
    using Internal.Dtos;
    using Internal.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Consolidation;
    using MassTransit;

    public class SendConsolidationEmailsApplicationService : CommunicationApplicationServiceBase, ISendConsolidationEmailsApplicationService
    {
        public SendConsolidationEmailsApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<ICommunicationSender> communicationSender,
            Lazy<ICommunicationTemplateLoader> communicationTemplateLoader,
            Lazy<ICommunicationService> communicationService,
            Lazy<IEmailService> emailService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IVisitPayUserRepository> visitPayUserRepository,
            Lazy<IVisitPayUserService> visitPayUserService)
            : base(
                applicationServiceCommonService,
                communicationSender,
                communicationTemplateLoader,
                communicationService,
                emailService,
                guarantorService,
                visitPayUserRepository,
                visitPayUserService)
        {
        }
        
        public void ConsolidationHouseholdRequestToManaging(int managingVpGuarantorId, int managedVpGuarantorId, string consolidationRequestExpiryDay)
        {
            this.SendCommunications(SendTo.Guarantor(managingVpGuarantorId), CommunicationTypeEnum.ConsolidateHouseholdRequestToManaging, managingVisitPayUser => new Dictionary<string, string>
            {
                {"[[Consolidate Request Expiry]]", consolidationRequestExpiryDay},
                {"[[ConsolidateRequestExpiryDay]]", consolidationRequestExpiryDay},
                {"[[NameA]]", managingVisitPayUser.FirstName}
            });
        }

        public void ConsolidationHouseholdRequestToManaged(int managingVpGuarantorId, int managedVpGuarantorId, string consolidationRequestExpiryDay)
        {
            this.SendCommunications(SendTo.Guarantor(managedVpGuarantorId), CommunicationTypeEnum.ConsolidateHouseholdRequestToManaged, managedVisitPayUser => new Dictionary<string, string>
            {
                {"[[Consolidate Request Expiry]]", consolidationRequestExpiryDay},
                {"[[ConsolidateRequestExpiryDay]]", consolidationRequestExpiryDay},
                {"[[NameB]]", managedVisitPayUser.FirstName}
            });
        }

        public void ConsolidationHouseholdAcceptedToManaged(int managingVpGuarantorId, int managedVpGuarantorId)
        {
            this.SendCommunications(SendTo.Guarantor(managedVpGuarantorId), CommunicationTypeEnum.ConsolidateHouseholdRequestCompleteToManaged, managedVisitPayUser => new Dictionary<string, string>
            {
                {"[[NameB]]", managedVisitPayUser.FirstName}
            });
        }

        public void ConsolidationHouseholdRequestAcceptedByManagedToManaging(int managingVpGuarantorId, int managedVpGuarantorId)
        {
            this.SendCommunications(SendTo.Guarantor(managingVpGuarantorId), CommunicationTypeEnum.ConsolidateHouseholdRequestAcceptedByManagedToManaging, managingVisitPayUser => new Dictionary<string, string>
            {
                {"[[NameA]]", managingVisitPayUser.FirstName}
            });
        }

        public void ConsolidationHouseholdRequestAcceptedByManagedWithFpToManaging(int managingVpGuarantorId, int managedVpGuarantorId, string consolidationRequestExpiryDay2)
        {
            this.SendCommunications(SendTo.Guarantor(managingVpGuarantorId), CommunicationTypeEnum.ConsolidateHouseholdRequestAcceptedByManagedWithFpToManaging, managingVisitPayUser => new Dictionary<string, string>
            {
                {"[[ConsolidateRequestExpiryDay2]]", consolidationRequestExpiryDay2},
                {"[[NameA]]", managingVisitPayUser.FirstName}
            });
        }

        public void ConsolidationHouseholdRequestDeclinedToManaged(int managingVpGuarantorId, int managedVpGuarantorId)
        {
            this.SendCommunications(SendTo.Guarantor(managedVpGuarantorId), CommunicationTypeEnum.ConsolidateHouseholdRequestDeclinedToManaged, managedVisitPayUser => new Dictionary<string, string>
            {
                {"[[NameB]]", managedVisitPayUser.FirstName}
            });
        }

        public void ConsolidationHouseholdRequestDeclinedToManaging(int managingVpGuarantorId, int managedVpGuarantorId)
        {
            this.SendCommunications(SendTo.Guarantor(managingVpGuarantorId), CommunicationTypeEnum.ConsolidateHouseholdRequestDeclinedToManaging, managingVisitPayUser => new Dictionary<string, string>
            {
                {"[[NameA]]", managingVisitPayUser.FirstName}
            });
        }

        public void ConsolidationHouseholdRequestCancelledToManaging(int managingVpGuarantorId, int managedVpGuarantorId)
        {
            this.SendCommunications(SendTo.Guarantor(managingVpGuarantorId), CommunicationTypeEnum.ConsolidateHouseholdRequestCancelledByManagedToManaging, managingVisitPayUser => new Dictionary<string, string>
            {
                {"[[NameA]]", managingVisitPayUser.FirstName}
            });
        }

        public void ConsolidationHouseholdRequestCancelledByManagingToManaged(int managingVpGuarantorId, int managedVpGuarantorId)
        {
            this.SendCommunications(SendTo.Guarantor(managedVpGuarantorId), CommunicationTypeEnum.ConsolidateHouseholdRequestCancelledByManagingToManaged, managedVisitPayUser => new Dictionary<string, string>
            {
                {"[[NameB]]", managedVisitPayUser.FirstName}
            });
        }

        public void ConsolidationHouseholdRequestCancelledByManagedToManagingWithPendingFpTerms(int managingVpGuarantorId, int managedVpGuarantorId)
        {
            this.SendCommunications(SendTo.Guarantor(managingVpGuarantorId), CommunicationTypeEnum.ConsolidateHouseholdRequestCancelledByManagedToManagingWithPendingFpTerms, managingVisitPayUser => new Dictionary<string, string>
            {
                {"[[NameA]]", managingVisitPayUser.FirstName}
            });
        }

        public void ConsolidationLinkageCancelledByManagingToManaging(int managingVpGuarantorId, int managedVpGuarantorId)
        {
            this.SendCommunications(SendTo.Guarantor(managingVpGuarantorId), CommunicationTypeEnum.ConsolidateLinkageCancelledByManagingToManaging, managingVisitPayUser => new Dictionary<string, string>
            {
                {"[[NameA]]", managingVisitPayUser.FirstName}
            });
        }

        public void ConsolidationLinkageCancelledByManagingToManaged(int managingVpGuarantorId, int managedVpGuarantorId)
        {
            this.SendCommunications(SendTo.Guarantor(managedVpGuarantorId), CommunicationTypeEnum.ConsolidateLinkageCancelledByManagingToManaged, managedVisitPayUser => new Dictionary<string, string>
            {
                {"[[NameB]]", managedVisitPayUser.FirstName}
            });
        }

        public void ConsolidationLinkageCancelledByManagingToManagedWithFp(int managingVpGuarantorId, int managedVpGuarantorId)
        {
            this.SendCommunications(SendTo.Guarantor(managedVpGuarantorId), CommunicationTypeEnum.ConsolidateLinkageCancelledByManagingToManagedWithFp, managedVisitPayUser => new Dictionary<string, string>
            {
                {"[[NameB]]", managedVisitPayUser.FirstName}
            });
        }

        public void ConsolidationLinkageCancelledByManagedToManaging(int managingVpGuarantorId, int managedVpGuarantorId)
        {
            this.SendCommunications(SendTo.Guarantor(managingVpGuarantorId), CommunicationTypeEnum.ConsolidateLinkageCancelledByManagedToManaging, managingVisitPayUser => new Dictionary<string, string>
            {
                { "[[NameA]]", managingVisitPayUser.FirstName }
            });
        }

        public void ConsolidationLinkageCancelledByManagedToManaged(int managingVpGuarantorId, int managedVpGuarantorId)
        {
            this.SendCommunications(SendTo.Guarantor(managedVpGuarantorId), CommunicationTypeEnum.ConsolidateLinkageCancelledByManagedToManaged, managedVisitPayUser => new Dictionary<string, string>
            {
                {"[[NameB]]", managedVisitPayUser.FirstName}
            });
        }

        public void ConsolidationLinkageCancelledByManagedToManagedWithFp(int managingVpGuarantorId, int managedVpGuarantorId)
        {
            this.SendCommunications(SendTo.Guarantor(managedVpGuarantorId), CommunicationTypeEnum.ConsolidateLinkageCancelledByManagedToManagedWithFp, managedVisitPayUser => new Dictionary<string, string>
            {
                {"[[NameB]]", managedVisitPayUser.FirstName}
            });
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(ConsolidationHouseholdAcceptedToManagedMessage message, ConsumeContext<ConsolidationHouseholdAcceptedToManagedMessage> consumeContext)
        {
            this.ConsolidationHouseholdAcceptedToManaged(message.ManagingVpGuarantorId, message.ManagedVpGuarantorId);
        }

        public bool IsMessageValid(ConsolidationHouseholdAcceptedToManagedMessage message, ConsumeContext<ConsolidationHouseholdAcceptedToManagedMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(ConsolidationHouseholdRequestAcceptedByManagedToManagingMessage message, ConsumeContext<ConsolidationHouseholdRequestAcceptedByManagedToManagingMessage> consumeContext)
        {
            this.ConsolidationHouseholdRequestAcceptedByManagedToManaging(message.ManagingVpGuarantorId, message.ManagedVpGuarantorId);
        }

        public bool IsMessageValid(ConsolidationHouseholdRequestAcceptedByManagedToManagingMessage message, ConsumeContext<ConsolidationHouseholdRequestAcceptedByManagedToManagingMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(ConsolidationHouseholdRequestAcceptedByManagedWithFpToManagingMessage message, ConsumeContext<ConsolidationHouseholdRequestAcceptedByManagedWithFpToManagingMessage> consumeContext)
        {
            this.ConsolidationHouseholdRequestAcceptedByManagedWithFpToManaging(message.ManagingVpGuarantorId, message.ManagedVpGuarantorId, message.ConsolidationRequestExpiryDay);
        }

        public bool IsMessageValid(ConsolidationHouseholdRequestAcceptedByManagedWithFpToManagingMessage message, ConsumeContext<ConsolidationHouseholdRequestAcceptedByManagedWithFpToManagingMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(ConsolidationHouseholdRequestCancelledByManagedToManagingMessage message, ConsumeContext<ConsolidationHouseholdRequestCancelledByManagedToManagingMessage> consumeContext)
        {
            this.ConsolidationHouseholdRequestCancelledToManaging(message.ManagingVpGuarantorId, message.ManagedVpGuarantorId);
        }

        public bool IsMessageValid(ConsolidationHouseholdRequestCancelledByManagedToManagingMessage message, ConsumeContext<ConsolidationHouseholdRequestCancelledByManagedToManagingMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(ConsolidationHouseholdRequestCancelledByManagedToManagingWithPendingFpTermsMessage message, ConsumeContext<ConsolidationHouseholdRequestCancelledByManagedToManagingWithPendingFpTermsMessage> consumeContext)
        {
            this.ConsolidationHouseholdRequestCancelledByManagedToManagingWithPendingFpTerms(message.ManagingVpGuarantorId, message.ManagedVpGuarantorId);
        }

        public bool IsMessageValid(ConsolidationHouseholdRequestCancelledByManagedToManagingWithPendingFpTermsMessage message, ConsumeContext<ConsolidationHouseholdRequestCancelledByManagedToManagingWithPendingFpTermsMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(ConsolidationHouseholdRequestCancelledByManagingToManagedMessage message, ConsumeContext<ConsolidationHouseholdRequestCancelledByManagingToManagedMessage> consumeContext)
        {
            this.ConsolidationHouseholdRequestCancelledByManagingToManaged(message.ManagingVpGuarantorId, message.ManagedVpGuarantorId);
        }

        public bool IsMessageValid(ConsolidationHouseholdRequestCancelledByManagingToManagedMessage message, ConsumeContext<ConsolidationHouseholdRequestCancelledByManagingToManagedMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(ConsolidationHouseholdRequestDeclinedToManagedMessage message, ConsumeContext<ConsolidationHouseholdRequestDeclinedToManagedMessage> consumeContext)
        {
            this.ConsolidationHouseholdRequestDeclinedToManaged(message.ManagingVpGuarantorId, message.ManagedVpGuarantorId);
        }

        public bool IsMessageValid(ConsolidationHouseholdRequestDeclinedToManagedMessage message, ConsumeContext<ConsolidationHouseholdRequestDeclinedToManagedMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(ConsolidationHouseholdRequestDeclinedToManagingMessage message, ConsumeContext<ConsolidationHouseholdRequestDeclinedToManagingMessage> consumeContext)
        {
            this.ConsolidationHouseholdRequestDeclinedToManaging(message.ManagingVpGuarantorId, message.ManagedVpGuarantorId);
        }

        public bool IsMessageValid(ConsolidationHouseholdRequestDeclinedToManagingMessage message, ConsumeContext<ConsolidationHouseholdRequestDeclinedToManagingMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(ConsolidationHouseholdRequestToManagedMessage message, ConsumeContext<ConsolidationHouseholdRequestToManagedMessage> consumeContext)
        {
            this.ConsolidationHouseholdRequestToManaged(message.ManagingVpGuarantorId, message.ManagedVpGuarantorId, message.ConsolidationRequestExpiryDay);
        }

        public bool IsMessageValid(ConsolidationHouseholdRequestToManagedMessage message, ConsumeContext<ConsolidationHouseholdRequestToManagedMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(ConsolidationHouseholdRequestToManagingMessage message, ConsumeContext<ConsolidationHouseholdRequestToManagingMessage> consumeContext)
        {
            this.ConsolidationHouseholdRequestToManaging(message.ManagingVpGuarantorId, message.ManagedVpGuarantorId, message.ConsolidationRequestExpiryDay);
        }

        public bool IsMessageValid(ConsolidationHouseholdRequestToManagingMessage message, ConsumeContext<ConsolidationHouseholdRequestToManagingMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(ConsolidationLinkageCancelledByManagedToManagedMessage message, ConsumeContext<ConsolidationLinkageCancelledByManagedToManagedMessage> consumeContext)
        {
            this.ConsolidationLinkageCancelledByManagedToManaged(message.ManagingVpGuarantorId, message.ManagedVpGuarantorId);
        }

        public bool IsMessageValid(ConsolidationLinkageCancelledByManagedToManagedMessage message, ConsumeContext<ConsolidationLinkageCancelledByManagedToManagedMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(ConsolidationLinkageCancelledByManagedToManagedWithFpMessage message, ConsumeContext<ConsolidationLinkageCancelledByManagedToManagedWithFpMessage> consumeContext)
        {
            this.ConsolidationLinkageCancelledByManagedToManagedWithFp(message.ManagingVpGuarantorId, message.ManagedVpGuarantorId);
        }

        public bool IsMessageValid(ConsolidationLinkageCancelledByManagedToManagedWithFpMessage message, ConsumeContext<ConsolidationLinkageCancelledByManagedToManagedWithFpMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(ConsolidationLinkageCancelledByManagedToManagingMessage message, ConsumeContext<ConsolidationLinkageCancelledByManagedToManagingMessage> consumeContext)
        {
            this.ConsolidationLinkageCancelledByManagedToManaging(message.ManagingVpGuarantorId, message.ManagedVpGuarantorId);
        }

        public bool IsMessageValid(ConsolidationLinkageCancelledByManagedToManagingMessage message, ConsumeContext<ConsolidationLinkageCancelledByManagedToManagingMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(ConsolidationLinkageCancelledByManagingToManagedMessage message, ConsumeContext<ConsolidationLinkageCancelledByManagingToManagedMessage> consumeContext)
        {
            this.ConsolidationLinkageCancelledByManagingToManaged(message.ManagingVpGuarantorId, message.ManagedVpGuarantorId);
        }

        public bool IsMessageValid(ConsolidationLinkageCancelledByManagingToManagedMessage message, ConsumeContext<ConsolidationLinkageCancelledByManagingToManagedMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(ConsolidationLinkageCancelledByManagingToManagedWithFpMessage message, ConsumeContext<ConsolidationLinkageCancelledByManagingToManagedWithFpMessage> consumeContext)
        {
            this.ConsolidationLinkageCancelledByManagingToManagedWithFp(message.ManagingVpGuarantorId, message.ManagedVpGuarantorId);
        }

        public bool IsMessageValid(ConsolidationLinkageCancelledByManagingToManagedWithFpMessage message, ConsumeContext<ConsolidationLinkageCancelledByManagingToManagedWithFpMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(ConsolidationLinkageCancelledByManagingToManagingMessage message, ConsumeContext<ConsolidationLinkageCancelledByManagingToManagingMessage> consumeContext)
        {
            this.ConsolidationLinkageCancelledByManagingToManaging(message.ManagingVpGuarantorId, message.ManagedVpGuarantorId);
        }

        public bool IsMessageValid(ConsolidationLinkageCancelledByManagingToManagingMessage message, ConsumeContext<ConsolidationLinkageCancelledByManagingToManagingMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion
    }
}
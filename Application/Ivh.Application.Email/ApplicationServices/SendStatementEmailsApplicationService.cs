﻿namespace Ivh.Application.Email.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Base.Common.Interfaces;
    using Common.Interfaces;
    using Domain.Email.Interfaces;
    using Domain.FinanceManagement.Statement.Entities;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.User.Entities;
    using Domain.User.Interfaces;
    using Domain.User.Services;
    using Domain.Visit.Interfaces;
    using Internal.Dtos;
    using Internal.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using MassTransit;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Aging;
    using Ivh.Common.VisitPay.Messages.Communication;
    using Ivh.Common.VisitPay.Strings;
    using Ivh.Common.Web.Constants;
    using NHibernate;
    using AgingVisitDto = Ivh.Common.VisitPay.Messages.Aging.Dtos.VisitDto;

    public class SendStatementEmailsApplicationService : CommunicationApplicationServiceBase, ISendStatementEmailsApplicationService
    {
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IVpStatementService> _statementService;
        private readonly Lazy<VisitPayUserService> _userService;
        private readonly Lazy<IVisitEventTrackerService> _visitEventService;
        private readonly ISession _session;

        public SendStatementEmailsApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<ICommunicationSender> communicationSender,
            Lazy<ICommunicationTemplateLoader> communicationTemplateLoader,
            Lazy<ICommunicationService> communicationService,
            Lazy<IEmailService> emailService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IVisitPayUserRepository> visitPayUserRepository,
            Lazy<IVpStatementService> statementService,
            Lazy<VisitPayUserService> userService,
            Lazy<IVisitPayUserService> visitPayUserService,
            Lazy<IVisitEventTrackerService> visitEventService,
            ISessionContext<VisitPay> sessionContext)
            : base(applicationServiceCommonService,
                communicationSender,
                communicationTemplateLoader,
                communicationService,
                emailService,
                guarantorService,
                visitPayUserRepository,
                visitPayUserService)
        {
            this._guarantorService = guarantorService;
            this._statementService = statementService;
            this._userService = userService;
            this._visitEventService = visitEventService;
            this._session = sessionContext.Session;
        }

        private void FinancePlanStatementNotification(int vpGuarantorId, DateTime statementDate, DateTime statementDueDate, decimal statementBalance, StatementNotificationTierEnum statementNotificationTierEnum)
        {
            CommunicationTypeEnum communicationType = this.GetFinancePlanStatementCommunicationType(statementNotificationTierEnum);

            this.SendStatementNotification(
                vpGuarantorId,
                communicationType,
                statementDate,
                statementDueDate,
                statementBalance,
                null
            );
        }
        
        private void VpccFinancePlanStatementNotification(int vpGuarantorId, DateTime statementDate, DateTime statementDueDate, decimal statementBalance, StatementNotificationTierEnum statementNotificationTierEnum)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            if (!guarantor.IsOfflineGuarantor())
            {
                return;
            }
            
            bool guarantorUsingPaperCommunications = guarantor.User.Email.IsNullOrEmpty();
            bool isAutoPay = guarantor.UseAutoPay;

            if (!this.DoesVpccUserNeedCommunication(statementNotificationTierEnum, isAutoPay, guarantorUsingPaperCommunications))
            {
                return;
            }

            CommunicationTypeEnum communicationType = this.GetVpccFinancePlanStatementCommunicationType(statementNotificationTierEnum, isAutoPay);
            
            VisitPayUser visitPayUser = guarantor.User;
            int tempPasswordExpLimitInHours = this.GetClient().ClientTempPasswordExpLimitInHours;
            string tempPasswordExpiration = tempPasswordExpLimitInHours.ToString(CultureInfo.InvariantCulture);
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                string tempPassword = this._userService.Value.GenerateOfflineLoginToken(visitPayUser);
                unitOfWork.Commit();

                this._session.Transaction.RegisterPostCommitSuccessAction(() =>
                {
                    bool emailSent = this.SendCommunications(SendTo.Guarantor(vpGuarantorId), communicationType, visitPayUserDto => new Dictionary<string, string>
                    {
                        {"[[PersonName]]", visitPayUserDto.FirstName},
                        {"[[StatementDate]]", statementDate.ToString(Format.FullMonthYearFormat)},
                        {"[[PaymentDueDate]]", statementDueDate.ToString(Format.MonthDayYearFormat)},
                        {"[[StatementBalance]]", statementBalance.FormatCurrency()},
                        {"[[StatementMonth]]", statementDate.ToString(Format.MonthFormat)},
                        {"[[GuarantorVpccTokenLogin]]", UrlPath.GuarantorVpccTokenLogin},
                        {"[[Token]]", tempPassword},
                        {"[[TempPasswordExpLimit]]", tempPasswordExpiration}
                    });
                });
            }
            //currently, no need to send ager updates because these are financed visits. 
        }
        
        private void StatementNotification(int vpGuarantorId, DateTime statementDate, DateTime statementDueDate, decimal statementBalance, int vpStatementId, StatementNotificationTierEnum statementNotificationTierEnum)
        {
            CommunicationTypeEnum communicationType = CommunicationTypeEnum.StatementNotificationGoodStanding;

            switch (statementNotificationTierEnum)
            {
                case StatementNotificationTierEnum.Good:
                    communicationType = CommunicationTypeEnum.StatementNotificationGoodStanding;
                    break;
                case StatementNotificationTierEnum.PastDue:
                    communicationType = CommunicationTypeEnum.StatementNotificationPastDue;
                    break;
                case StatementNotificationTierEnum.FinalPastDue:
                    communicationType = CommunicationTypeEnum.StatementNotificationFinalPastDue;
                    break;
                case StatementNotificationTierEnum.Uncollectable:
                    communicationType = CommunicationTypeEnum.StatementNotificationUncollectable;
                    break;
            }

            this.SendStatementNotification(
                vpGuarantorId,
                communicationType,
                statementDate,
                statementDueDate,
                statementBalance,
                vpStatementId
            );
        }

        private void SendStatementNotification(int vpGuarantorId, CommunicationTypeEnum communicationTypeEnum, DateTime statementDate, DateTime statementDueDate, decimal statementBalance, int? vpStatementId)
        {
            CommunicationTypeEnum managingUserCommunicationTypeEnum = this.GetManagingUserCommunicationTypeEnum(communicationTypeEnum);

            bool emailSent = 
                this.SendCommunications(
                    SendTo.Guarantor(vpGuarantorId), 
                    communicationTypeEnum, 
                    managingUserCommunicationTypeEnum, 
                    visitPayUser => new Dictionary<string, string>
                    {
                        {"[[PersonName]]", visitPayUser.FirstName},
                        {"[[StatementDate]]", statementDate.ToString(Format.FullMonthYearFormat)},
                        {"[[PaymentDueDate]]", statementDueDate.ToString(Format.MonthDayYearFormat)},
                        {"[[StatementBalance]]", statementBalance.FormatCurrency()},
                        {"[[StatementMonth]]", statementDate.ToString(Format.MonthFormat)},
                    },
                    null);

            //only sending ager updates for nonfinanced visit statements
            if (emailSent && vpStatementId.HasValue && communicationTypeEnum.IsInCategory(CommunicationTypeEnumCategory.Aging))
            {
                VpStatement statement = this._statementService.Value.GetStatement(vpStatementId.Value);
                if (statement != null)
                {
                    IList<AgingVisitDto> agingVisitDtos = statement.ActiveVpStatementVisitsNotOnFinancePlan
                        .Select(x => new AgingVisitDto
                        {
                            VisitBillingSystemId = x.Visit.BillingSystemId ?? default(int),
                            VisitSourceSystemKey = x.Visit.SourceSystemKey
                        }).ToList();

                    if (agingVisitDtos.Any())
                    {
                        this.Bus.Value.PublishMessage(new VisitCommunicationMessage
                        {
                            CommunicationType = AgingCommunicationTypeEnum.StatementNotification,
                            Visits = agingVisitDtos,
                            CommunicationDateTime = DateTime.UtcNow
                        });
                    }
                }
            }
        }

        private CommunicationTypeEnum GetManagingUserCommunicationTypeEnum(CommunicationTypeEnum communicationTypeEnum)
        {
            switch (communicationTypeEnum)
            {
                case CommunicationTypeEnum.StatementNotificationGoodStanding:
                    return CommunicationTypeEnum.StatementNotificationGoodStandingToManaging;

                case CommunicationTypeEnum.StatementNotificationPastDue:
                    return CommunicationTypeEnum.StatementNotificationPastDueToManaging;

                case CommunicationTypeEnum.StatementNotificationFinalPastDue:
                    return CommunicationTypeEnum.StatementNotificationFinalPastDueToManaging;

                case CommunicationTypeEnum.StatementNotificationUncollectable:
                    return CommunicationTypeEnum.StatementNotificationUncollectableToManaging;

                case CommunicationTypeEnum.FinancePlanStatementNotificationGoodStanding:
                    return CommunicationTypeEnum.FinancePlanStatementNotificationGoodStandingToManaging;

                case CommunicationTypeEnum.FinancePlanStatementNotificationPastDue:
                    return CommunicationTypeEnum.FinancePlanStatementNotificationPastDueToManaging;

                case CommunicationTypeEnum.FinancePlanStatementNotificationFinalPastDue:
                    return CommunicationTypeEnum.FinancePlanStatementNotificationFinalPastDueToManaging;

                case CommunicationTypeEnum.FinancePlanStatementNotificationUncollectable:
                    return CommunicationTypeEnum.FinancePlanStatementNotificationUncollectableToManaging;

                default:
                    throw new ArgumentOutOfRangeException(nameof(communicationTypeEnum), "No matching managing user communication type");
            }
        }

        private void LogFinancePlanStatementEvent(FinancePlanStatementNotificationMessage message)
        {
            switch (message.StatementNotificationTierEnum)
            {
                case StatementNotificationTierEnum.Uncollectable:
                    this._visitEventService.Value.LogCommunicationMessage(message.VisitIds, CommunicationTypeEnum.FinancePlanStatementNotificationUncollectable, message.EntityToJsonSafe());
                    return;
                case StatementNotificationTierEnum.FinalPastDue:
                    this._visitEventService.Value.LogCommunicationMessage(message.VisitIds, CommunicationTypeEnum.FinancePlanStatementNotificationFinalPastDue, message.EntityToJsonSafe());
                    return;
                case StatementNotificationTierEnum.PastDue:
                    this._visitEventService.Value.LogCommunicationMessage(message.VisitIds, CommunicationTypeEnum.FinancePlanStatementNotificationPastDue, message.EntityToJsonSafe());
                    return;
            }
        }

        private CommunicationTypeEnum GetVpccFinancePlanStatementCommunicationType(StatementNotificationTierEnum statementNotificationTierEnum, bool autoPay)
        {
            switch (statementNotificationTierEnum)
            {
                case StatementNotificationTierEnum.Good:
                    return CommunicationTypeEnum.VpccFinancePlanStatementNotificationGoodStanding;
                case StatementNotificationTierEnum.PastDue:
                    return autoPay ? CommunicationTypeEnum.VpccFinancePlanAutoPayStatementNotificationPastDue : CommunicationTypeEnum.VpccFinancePlanNonAutoPayStatementNotificationPastDue;
                case StatementNotificationTierEnum.FinalPastDue:
                    return CommunicationTypeEnum.VpccFinancePlanNotificationFinalPastDue;
                default:
                    //According to Product, there is no expectation to add the Uncollectible notification here(it's not a legal requirement), opposed to non VPCC Finance Plan Statements.
                    return CommunicationTypeEnum.VpccFinancePlanStatementNotificationGoodStanding;
            }
        }

        private CommunicationTypeEnum GetFinancePlanStatementCommunicationType(StatementNotificationTierEnum statementNotificationTierEnum)
        {
            switch (statementNotificationTierEnum)
            {
                case StatementNotificationTierEnum.Good:
                    return CommunicationTypeEnum.FinancePlanStatementNotificationGoodStanding;
                case StatementNotificationTierEnum.PastDue:
                    return CommunicationTypeEnum.FinancePlanStatementNotificationPastDue;
                case StatementNotificationTierEnum.FinalPastDue:
                    return CommunicationTypeEnum.FinancePlanStatementNotificationFinalPastDue;
                case StatementNotificationTierEnum.Uncollectable:
                    return CommunicationTypeEnum.FinancePlanStatementNotificationUncollectable;
                default:
                    return CommunicationTypeEnum.FinancePlanStatementNotificationGoodStanding;
            }
        }

        private bool DoesVpccUserNeedCommunication(StatementNotificationTierEnum tier, bool isAutoPay, bool isPaperCommunicationUser)
        {
            //https://ivinci.atlassian.net/browse/VP-6536 - says users in this scenario do not need the communication - might differ for clients in future?
            if (isAutoPay && tier == StatementNotificationTierEnum.Good && isPaperCommunicationUser)
            {
                return false;
            }
            return true;
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(StatementNotificationMessage message, ConsumeContext<StatementNotificationMessage> consumeContext)
        {
            this.StatementNotification(
                message.VpGuarantorId,
                message.StatementDate,
                message.StatementDueDate,
                message.StatementBalance,
                message.VpStatementId,
                message.StatementNotificationTierEnum
            );
        }

        public bool IsMessageValid(StatementNotificationMessage message, ConsumeContext<StatementNotificationMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(FinancePlanStatementNotificationMessage message, ConsumeContext<FinancePlanStatementNotificationMessage> consumeContext)
        {
            if (message.IsOfflineGuarantor)
            {
                this.VpccFinancePlanStatementNotification(
                    message.VpGuarantorId,
                    message.StatementDate,
                    message.StatementDueDate,
                    message.StatementBalance,
                    message.StatementNotificationTierEnum
                );
                this.LogFinancePlanStatementEvent(message);
                return;
            }
            this.FinancePlanStatementNotification(
                message.VpGuarantorId,
                message.StatementDate,
                message.StatementDueDate,
                message.StatementBalance,
                message.StatementNotificationTierEnum
            );
            this.LogFinancePlanStatementEvent(message);
        }

        public bool IsMessageValid(FinancePlanStatementNotificationMessage message, ConsumeContext<FinancePlanStatementNotificationMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion
    }
}
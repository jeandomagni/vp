﻿namespace Ivh.Application.Email.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.Linq;
    using Base.Common.Interfaces;
    using Common.Interfaces;
    using Core.Common.Dtos;
    using Domain.Visit.Interfaces;
    using Domain.Email.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.User.Interfaces;
    using Domain.Visit.Entities;
    using Internal.Dtos;
    using Internal.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using MassTransit;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Aging;
    using Ivh.Common.VisitPay.Messages.Core;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;
    using NHibernate;

    public class SendVisitEmailApplicationService : CommunicationApplicationServiceBase, ISendVisitEmailApplicationService
    {
        private readonly Lazy<IFinancePlanService> _financePlanService;
        private readonly Lazy<IVisitService> _visitService;
        private readonly Lazy<IVisitEventTrackerService> _visitEventService;

        private readonly ISession _session;

        public SendVisitEmailApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<ICommunicationSender> communicationSender,
            Lazy<ICommunicationTemplateLoader> communicationTemplateLoader,
            Lazy<ICommunicationService> communicationService,
            Lazy<IEmailService> emailService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IVisitPayUserRepository> visitPayUserRepository,
            Lazy<IFinancePlanService> financePlanService,
            Lazy<IVisitService> visitService,
            Lazy<IVisitPayUserService> visitPayUserService,
            Lazy<IVisitEventTrackerService> visitEventService,
            ISessionContext<VisitPay> sessionContext)
            : base(
                applicationServiceCommonService, 
                communicationSender, 
                communicationTemplateLoader, 
                communicationService,
                emailService, 
                guarantorService, 
                visitPayUserRepository,
                visitPayUserService)
        {
            this._financePlanService = financePlanService;
            this._visitService = visitService;
            this._visitEventService = visitEventService;
            this._session = sessionContext.Session;
        }

        public bool VisitsPastDue(int vpGuarantorId)
        {
            int pastDueFinancePlans;
            int pastDueVisits;

            using (IUnitOfWork uow = new UnitOfWork(this._session, IsolationLevel.ReadCommitted))
            {
                Guarantor guarantor = this.GuarantorService.Value.GetGuarantor(vpGuarantorId);
                // VP-4543- GetAllActiveFinancePlansForGuarantor
                // We need send past due communication until the FP is accepted.
                // Excluding pending FPs when determining if the visit is attached to a FP
                IList<FinancePlan> financePlans = this._financePlanService.Value.GetAllOpenFinancePlansForGuarantor(guarantor) ?? new List<FinancePlan>();
                IList<int> financedVisitIds = financePlans.SelectMany(x => x.FinancePlanVisits).Select(x => x.VisitId).ToList();
                VisitResults pastDueVisitsResults = this._visitService.Value.GetVisits(vpGuarantorId, new VisitFilter { IsPastDue = true }, null, null);

                pastDueVisits = pastDueVisitsResults.Visits.Count(x => !financedVisitIds.Contains(x.VisitId));
                pastDueFinancePlans = financePlans.Count(x => x.IsPastDue());

                uow.Commit();
            }

            int total = pastDueVisits + pastDueFinancePlans;
            if (total < 1)
            {
                this.LoggingService.Value.Info(() => $"{nameof(SendVisitEmailApplicationService)}.{nameof(this.VisitsPastDue)}: not processed for Guarantor {vpGuarantorId}. No past due finance plans or visits found.");
                return false;
            }

            return this.SendCommunications(SendTo.Guarantor(vpGuarantorId), CommunicationTypeEnum.VisitsPastDue, visitPayUser => new Dictionary<string, string>
            {
                {"[[PersonName]]", visitPayUser.FirstName}
            });
        }

        public bool PendingUncollectable(int vpGuarantorId)
        {
            int pendingUncollectableFinancePlans;
            int pendingUncollectableVisits;

            using (IUnitOfWork uow = new UnitOfWork(this._session, IsolationLevel.ReadCommitted))
            {
                pendingUncollectableFinancePlans = this._financePlanService.Value.GetFinancePlanCount(new Guarantor { VpGuarantorId = vpGuarantorId }, new[] { FinancePlanStatusEnum.UncollectableActive });
                pendingUncollectableVisits = this._visitService.Value.GetVisitTotals(vpGuarantorId, new VisitFilter { AgingCountMin = (int)AgingTierEnum.MaxAge });

                uow.Commit();
            }

            int total = pendingUncollectableFinancePlans + pendingUncollectableVisits;
            if (total < 1)
            {
                this.LoggingService.Value.Info(() => $"{nameof(SendVisitEmailApplicationService)}.{nameof(this.PendingUncollectable)}: not processed for Guarantor {vpGuarantorId}. No pending uncollectable finance plans or visits found.");
                return false;
            }

            return this.SendCommunications(SendTo.Guarantor(vpGuarantorId), CommunicationTypeEnum.UncollectableBalanceNotification, visitPayUser => new Dictionary<string, string>
            {
                {"[[Name]]", visitPayUser.FirstName}
            });
        }

        public bool FinalPastDueNotification(int vpGuarantorId, IList<int> visitIds, bool isOnFinancePlan)
        {
            if (isOnFinancePlan)
            {
                return this.SendCommunications(SendTo.Guarantor(vpGuarantorId), CommunicationTypeEnum.FinalPastDueNotificationFinancePlan, visitPayUser => new Dictionary<string, string>
                {
                    {"[[NameA]]", visitPayUser.FirstName}
                });
            }
            else
            {
                bool emailSent = this.SendCommunications(SendTo.Guarantor(vpGuarantorId), CommunicationTypeEnum.FinalPastDueNotificationNonFinancePlan, visitPayUser => new Dictionary<string, string>
                {
                    {"[[NameA]]", visitPayUser.FirstName}
                });

                if (emailSent && CommunicationTypeEnum.FinalPastDueNotificationNonFinancePlan.IsInCategory(CommunicationTypeEnumCategory.Aging))
                {
                    if (visitIds != null)
                    {
                        IList<Visit> visits = this._visitService.Value.GetAllVisitsWithIntList(visitIds, true);
                        if (visits != null)
                        {
                            IList<Ivh.Common.VisitPay.Messages.Aging.Dtos.VisitDto> agingVisitDtos = visits
                                .Select(v => new Ivh.Common.VisitPay.Messages.Aging.Dtos.VisitDto
                                {
                                    VisitSourceSystemKey = v.SourceSystemKey,
                                    VisitBillingSystemId = v.BillingSystemId ?? default(int)
                                }).ToList();

                            this.Bus.Value.PublishMessage(new VisitCommunicationMessage
                            {
                                CommunicationType = AgingCommunicationTypeEnum.PastDueNotification,
                                Visits = agingVisitDtos,
                                CommunicationDateTime = DateTime.UtcNow
                            });
                        }

                    }
                }

                return emailSent;
            }
        }

        public void NewVisitLoaded(int vpGuarantorId)
        {
            this.SendCommunications(SendTo.Guarantor(vpGuarantorId), CommunicationTypeEnum.NewVisitLoaded, visitPayUser => new Dictionary<string, string>
            {
                {"[[Name]]", visitPayUser.FirstName}
            });
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SendFinalPastDueNotificationEmailMessage message, ConsumeContext<SendFinalPastDueNotificationEmailMessage> consumeContext)
        {
            bool didFinalPastDueCommunicationSendSuccessfully = this.FinalPastDueNotification(message.VpGuarantorId, message.VisitIds, message.IsOnFinancePlan);
            if(didFinalPastDueCommunicationSendSuccessfully)
            {
                this._visitEventService.Value.LogCommunicationMessage(message.VisitIds, CommunicationTypeEnum.FinalPastDueNotificationNonFinancePlan, message.EntityToJsonSafe());
            }
        }

        public bool IsMessageValid(SendFinalPastDueNotificationEmailMessage message, ConsumeContext<SendFinalPastDueNotificationEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SendPastDueVisitsEmailMessage message, ConsumeContext<SendPastDueVisitsEmailMessage> consumeContext)
        {
            bool didVisitPastDueCommunicationSendSuccessfully = this.VisitsPastDue(message.VpGuarantorId);
            if(didVisitPastDueCommunicationSendSuccessfully)
            {
                this._visitEventService.Value.LogCommunicationMessage(message.VisitIds, CommunicationTypeEnum.VisitsPastDue, message.EntityToJsonSafe());
            }
        }

        public bool IsMessageValid(SendPastDueVisitsEmailMessage message, ConsumeContext<SendPastDueVisitsEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SendPendingUncollectableEmailMessage message, ConsumeContext<SendPendingUncollectableEmailMessage> consumeContext)
        {
            bool didPendingUncollectableCommunicationSendSuccessfully = this.PendingUncollectable(message.VpGuarantorId);
            if(didPendingUncollectableCommunicationSendSuccessfully){
                this._visitEventService.Value.LogCommunicationMessage(message.VisitIds, CommunicationTypeEnum.UncollectableBalanceNotification, message.EntityToJsonSafe());
            }
        }

        public bool IsMessageValid(SendPendingUncollectableEmailMessage message, ConsumeContext<SendPendingUncollectableEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(NewVisitLoadedMessage message, ConsumeContext<NewVisitLoadedMessage> consumeContext)
        {
            this.NewVisitLoaded(message.VpGuarantorId);
        }

        public bool IsMessageValid(NewVisitLoadedMessage message, ConsumeContext<NewVisitLoadedMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SendTextToPayPaymentOptionMessage message, ConsumeContext<SendTextToPayPaymentOptionMessage> consumeContext)
        {
            this.SendCommunications(SendTo.Guarantor(message.VpGuarantorId), CommunicationTypeEnum.TextToPayPaymentOption, visitPayUser => new Dictionary<string, string>
            {
                {"[[Amount]]", message.Amount.ToString(CultureInfo.InvariantCulture)}
            });
        }

        public bool IsMessageValid(SendTextToPayPaymentOptionMessage message, ConsumeContext<SendTextToPayPaymentOptionMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion
    }
}
﻿namespace Ivh.Application.Email.ApplicationServices
{
    using System;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Core.Common.Interfaces;
    using Domain.Email.Interfaces;
    using Domain.Logging.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Communication;
    using Ivh.Common.VisitPay.Messages.Core;
    using MassTransit;
    using Common.Utilities;

    public class TwilioSmsEventApplicationService : ApplicationService, ITwilioSmsEventApplicationService
    {
        private readonly Lazy<IEmailService> _emailService;
        private readonly Lazy<ICommunicationService> _communicationService;
        private readonly Lazy<IVisitPayUserApplicationService> _visitPayUserApplicationService;
        private readonly Lazy<IMetricsProvider> _metricsProvider;

        public TwilioSmsEventApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IEmailService> emailService,
            Lazy<ICommunicationService> communicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<IMetricsProvider> metricsProvider) : base(applicationServiceCommonService)
        {
            this._emailService = emailService;
            this._communicationService = communicationService;
            this._visitPayUserApplicationService = visitPayUserApplicationService;
            this._metricsProvider = metricsProvider;
        }

        public void SmsEvent(TwilioSmsEventMessage message)
        {
            CommunicationEventDto communicationEvent = Mapper.Map<CommunicationEventDto>(message);
            communicationEvent.InsertedDate = DateTime.UtcNow;
            ConversionUtil.SetEventDateFromTimeStamp(communicationEvent);
            this._emailService.Value.Event(communicationEvent);
        }

        public SmsResponseResult SmsMessage(TwilioCallbackMessage message)
        {
            //Twilio has STOP as a keyword
            //https://support.twilio.com/hc/en-us/articles/223134027-Twilio-support-for-STOP-BLOCK-and-CANCEL-SMS-STOP-filtering-
            //Never forget.

            SmsResponseResult result = new SmsResponseResult() { ResponseActionEnum = SmsResponseActionEnum.Unknown };

            if (string.Equals(message.Body, "s", StringComparison.InvariantCultureIgnoreCase)
                || string.Equals(message.Body, "STOP", StringComparison.InvariantCultureIgnoreCase)//These cases will cause us to not be able to send anymore messages to them.
                || string.Equals(message.Body, "STOPALL", StringComparison.InvariantCultureIgnoreCase)
                || string.Equals(message.Body, "UNSUBSCRIBE", StringComparison.InvariantCultureIgnoreCase)
                || string.Equals(message.Body, "CANCEL", StringComparison.InvariantCultureIgnoreCase)
                || string.Equals(message.Body, "END", StringComparison.InvariantCultureIgnoreCase)
                || string.Equals(message.Body, "QUIT", StringComparison.InvariantCultureIgnoreCase)
                )
            {
                if (string.Equals(message.Body, "STOP", StringComparison.InvariantCultureIgnoreCase) //These cases will cause us to not be able to send anymore messages to them.
                    || string.Equals(message.Body, "STOPALL", StringComparison.InvariantCultureIgnoreCase)
                    || string.Equals(message.Body, "UNSUBSCRIBE", StringComparison.InvariantCultureIgnoreCase)
                    || string.Equals(message.Body, "CANCEL", StringComparison.InvariantCultureIgnoreCase)
                    || string.Equals(message.Body, "END", StringComparison.InvariantCultureIgnoreCase)
                    || string.Equals(message.Body, "QUIT", StringComparison.InvariantCultureIgnoreCase))
                {
                    this._communicationService.Value.AddBlockedNumber(message.From, message.To);
                }

                result.ResponseActionEnum = SmsResponseActionEnum.Stop;
                this._visitPayUserApplicationService.Value.RemoveSmsAcknowledgements(message.From, null);
                this._metricsProvider.Value.Increment(Metrics.Increment.Communication.Sms.StoppedViaSms);
                //Dont need to send via the TwiML interface as .RemoveSmsAcknowledgements already sends message.
                result.Body = string.Empty;
            }
            else if (string.Equals(message.Body, "START", StringComparison.InvariantCultureIgnoreCase))
            {
                this._communicationService.Value.RemoveBlockedNumber(message.From, message.To);
            }
            else if (string.Equals(message.Body, "e", StringComparison.InvariantCultureIgnoreCase))
            {
                result.Body = $"Echo: {message.Body}";
            }

            //Did the user reply to Text to Pay?
            else if (string.Equals(message.Body, "PAY", StringComparison.InvariantCultureIgnoreCase))
            {
                this.Bus.Value.PublishMessage(new ReceiveTextToPayPaymentOptionMessage
                {
                    PhoneNumber = message.From,
                    RawBodyText = message.Body
                }).Wait();
            }

            return result;
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(TwilioSmsEventMessage message, ConsumeContext<TwilioSmsEventMessage> consumeContext)
        {
            this.SmsEvent(message);
        }

        public bool IsMessageValid(TwilioSmsEventMessage message, ConsumeContext<TwilioSmsEventMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion
    }
}
﻿namespace Ivh.Application.Email.ApplicationServices
{
    using System;
    using Ivh.Application.Email.Common.Dtos;
    using Ivh.Application.Email.Common.Interfaces;
    using Ivh.Domain.Email.Interfaces;
    using System.Collections.Generic;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Ivh.Common.VisitPay.Enums;


    public class EmailApplicationService : ApplicationService, IEmailApplicationService
    {
        private readonly IEmailService _emailService;

        public EmailApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            IEmailService emailService) : base(applicationServiceCommonService)
        {
            this._emailService = emailService;
        }

        public IList<CommunicationDto> GetLastSentEmails(int limit)
        {
            return Mapper.Map<IList<CommunicationDto>>(this._emailService.GetLastSentEmails(limit));
        }

        public IList<CommunicationDto> GetUnsentCommunications(CommunicationMethodEnum communicationMethod)
        {
            return Mapper.Map<IList<CommunicationDto>>(this._emailService.GetUnsentCommunications(communicationMethod));
        }
    }
}

namespace Ivh.Application.Email.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Core.Common.Dtos;
    using Core.Common.Interfaces;
    using Domain.Content.Entities;
    using Domain.Content.Interfaces;
    using Domain.Email.Entities;
    using Domain.Email.Interfaces;
    using Internal.Interfaces;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.EventJournal;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Jobs;

    public class CommunicationExportApplicationService : ApplicationService, 
        ICommunicationExportApplicationService,
        IJobRunnerService<PaperMailExportBatchProcessJobRunner>
    {
        private readonly Lazy<ICommunicationSender> _communicationSender;
        private readonly Lazy<IEmailApplicationService> _emailApplicationService;

        public CommunicationExportApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<ICommunicationSender> communicationSender,
            Lazy<IEmailApplicationService> emailApplicationService
            ) : base(applicationServiceCommonService)
        {
            this._communicationSender = communicationSender;
            this._emailApplicationService = emailApplicationService;
        }

        public void ExportUnsentPaperCommunications()
        {
            IList<CommunicationDto> communicationDtos = this._emailApplicationService.Value.GetUnsentCommunications(CommunicationMethodEnum.Mail);
            foreach (CommunicationDto communicationDto in communicationDtos)
            {
                bool wasSuccessful = this._communicationSender.Value.SendMail(communicationDto);
            }
        }

        public void Execute(DateTime begin, DateTime end, string[] args)
        {
            this.ExportUnsentPaperCommunications();
        }
    }
}
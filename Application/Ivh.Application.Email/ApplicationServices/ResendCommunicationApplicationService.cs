﻿namespace Ivh.Application.Email.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Common.Interfaces;
    using Common.Dtos;
    using Common.Interfaces;
    using Core.Common.Dtos;
    using Domain.User.Interfaces;
    using Domain.Email.Interfaces;
    using Domain.Guarantor.Interfaces;
    using Internal.Interfaces;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.EventJournal;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Core;
    using MassTransit;
    using Ivh.Common.VisitPay.Jobs;
    using Ivh.Common.JobRunner.Common.Interfaces;

    public class ResendCommunicationApplicationService : CommunicationApplicationServiceBase, IResendCommunicationApplicationService
    {
        private readonly Lazy<IVisitPayUserJournalEventService> _userJournalEventService;

        public ResendCommunicationApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<ICommunicationSender> communicationSender,
            Lazy<ICommunicationTemplateLoader> communicationTemplateLoader,
            Lazy<ICommunicationService> communicationService,
            Lazy<IEmailService> emailService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IVisitPayUserRepository> visitPayUserRepository,
            Lazy<IVisitPayUserJournalEventService> userJournalEventService,
            Lazy<IVisitPayUserService> visitPayUserService)
            : base(applicationServiceCommonService,
                communicationSender,
                communicationTemplateLoader,
                communicationService,
                emailService,
                guarantorService,
                visitPayUserRepository,
                visitPayUserService)
        {
            this._userJournalEventService = userJournalEventService;
        }

        public void ResendCommunication(int communicationId, string toAddress = null, int? vpGuarantorId = null, int? sentByVisitPayUserId = null)
        {
            CommunicationDto communication = this.ResendCommunicationBase(communicationId, toAddress);
            if (communication != null && vpGuarantorId.HasValue && sentByVisitPayUserId.HasValue)
            {
                GuarantorDto vpGuarantorDto = this.GetGuarantor(vpGuarantorId.Value);
                JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(null, sentByVisitPayUserId.Value);
                this._userJournalEventService.Value.LogResendCommunication(
                    journalEventUserContext: journalEventUserContext,
                    vpGuarantorId: vpGuarantorId.Value,
                    originalCommunicationId: communicationId,
                    resentCommunicationId: communication.CommunicationId,
                    communicationType: communication.CommunicationType.CommunicationTypeName,
                    vpGuarantorUserName: vpGuarantorDto.User.UserName
                );
            }
        }

        public void QueueHeldEmailForResend(IList<CommunicationTypeEnum> emailTypes = null)
        {
            this.QueueHeldEmailForResendBase(emailTypes);
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority10)]
        public void ConsumeMessage(ResendEmailMessage message, ConsumeContext<ResendEmailMessage> consumeContext)
        {
            this.ResendCommunication(message.EmailId, message.ToAddress, message.VpGuarantorId, message.SentByVisitPayUserId);
        }

        public bool IsMessageValid(ResendEmailMessage message, ConsumeContext<ResendEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion

        #region Jobs

        void IJobRunnerService<QueueHeldEmailForResendJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            IList<CommunicationTypeEnum> emailTypes = null;
            if (args.Length > 1)
            {
                emailTypes = args[1].Split(",;:".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Select(x => (CommunicationTypeEnum)Enum.Parse(typeof(CommunicationTypeEnum), x)).ToList<CommunicationTypeEnum>();
            }
            this.QueueHeldEmailForResend(emailTypes);
        }

        #endregion
    }
}
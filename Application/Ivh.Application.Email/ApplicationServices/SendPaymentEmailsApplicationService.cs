namespace Ivh.Application.Email.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Common.Interfaces;
    using Common.Dtos;
    using Common.Interfaces;
    using Core.Common.Dtos;
    using Domain.Email.Interfaces;
    using Domain.Guarantor.Interfaces;
    using Domain.User.Interfaces;
    using Internal.Dtos;
    using Internal.Interfaces;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Attributes;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Communication;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;
    using Ivh.Common.VisitPay.Strings;
    using MassTransit;

    public class SendPaymentEmailsApplicationService : CommunicationApplicationServiceBase, ISendPaymentEmailsApplicationService
    {
        private readonly Lazy<ICommunicationTrackerService> _emailTrackerService;

        public SendPaymentEmailsApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<ICommunicationSender> communicationSender,
            Lazy<ICommunicationTemplateLoader> communicationTemplateLoader,
            Lazy<ICommunicationService> communicationService,
            Lazy<IEmailService> emailService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IVisitPayUserRepository> visitPayUserRepository,
            Lazy<ICommunicationTrackerService> emailTrackerService,
            Lazy<IVisitPayUserService> visitPayUserService)
            : base(
                applicationServiceCommonService,
                communicationSender,
                communicationTemplateLoader,
                communicationService,
                emailService,
                guarantorService,
                visitPayUserRepository,
                visitPayUserService)
        {
            this._emailTrackerService = emailTrackerService;
        }

        private void BalanceDueReminder(int vpGuarantorId, DateTime paymentDate)
        {
            this.SendCommunications(SendTo.Guarantor(vpGuarantorId), CommunicationTypeEnum.BalanceDueReminder, visitPayUser => new Dictionary<string, string>
            {
                {"[[NameA]]", visitPayUser.FirstName},
                {"[[PaymentDate]]", paymentDate.Date.ToString(Format.DateFormat)},
            });
        }

        private void CardExpired(int vpGuarantorId, string expiryDate)
        {
            this.SendCommunications(SendTo.Guarantor(vpGuarantorId), CommunicationTypeEnum.CardExpired, visitPayUser => new Dictionary<string, string>
            {
                {"[[NameA]]", visitPayUser.FirstName},
                {"[[expirydate]]", expiryDate},
            });
        }

        private void CardExpiring(int vpGuarantorId, string expiryDate)
        {
            this.SendCommunications(SendTo.Guarantor(vpGuarantorId), CommunicationTypeEnum.CardExpiring, visitPayUser => new Dictionary<string, string>
            {
                {"[[NameA]]", visitPayUser.FirstName},
                {"[[expirydate]]", expiryDate},
            });
        }

        private void PaymentDueDateChange(int vpGuarantorId, DateTime paymentDueDate)
        {
            GuarantorDto guarantorDto = this.GetGuarantor(vpGuarantorId);
            if (guarantorDto.IsManaged)
            {
                vpGuarantorId = guarantorDto.ManagingGuarantorId ?? 0;
            }

            this.SendCommunications(SendTo.Guarantor(vpGuarantorId), CommunicationTypeEnum.PaymentDueDateChange, visitPayUser => new Dictionary<string, string>
            {
                {"[[FirstName]]", visitPayUser.FirstName},
                {"[[NextPaymentDueDate]]", paymentDueDate.Date.ToString(Format.DateFormat)},
            });
        }

        private void PaymentFailure(int vpGuarantorId)
        {
            this.SendCommunications(SendTo.Guarantor(vpGuarantorId),
                CommunicationTypeEnum.PaymentFailure,
                CommunicationTypeEnum.PaymentFailureCcManaging,
                visitPayUser => new Dictionary<string, string> { {"[[PersonName]]", visitPayUser.FirstName} }, 
                null);
        }

        private void PaymentMethodChange(int vpGuarantorId)
        {
            this.SendCommunications(SendTo.Guarantor(vpGuarantorId), CommunicationTypeEnum.PaymentMethodChange, visitPayUser => new Dictionary<string, string>
            {
                {"[[FirstName]]", visitPayUser.FirstName},
            });
        }

        private void PaymentReversalNotification(int vpGuarantorId, string transactionNumber, PaymentTypeEnum paymentType)
        {
            GuarantorDto guarantorDto = this.GetGuarantor(vpGuarantorId);
            CommunicationTypeEnum reversalNotifcationType = guarantorDto.IsOfflineGuarantor 
                ? CommunicationTypeEnum.VpccPaymentReversalNotification 
                : CommunicationTypeEnum.PaymentReversalNotification;

            this.SendCommunications(SendTo.Guarantor(vpGuarantorId), reversalNotifcationType, visitPayUser =>
            {
                IDictionary<string, string> replacementValues = new Dictionary<string, string>
                {
                    {"[[FirstName]]", visitPayUser.FirstName},
                    {"[[TransactionNumber]]", transactionNumber},
                };

                switch (paymentType)
                {
                    case PaymentTypeEnum.PartialRefund:
                        replacementValues.Add("[[PaymentReversalType1]]", "partially refunded");
                        replacementValues.Add("[[PaymentReversalType2]]", "partial refund");
                        break;
                    case PaymentTypeEnum.Refund:
                        replacementValues.Add("[[PaymentReversalType1]]", "refunded");
                        replacementValues.Add("[[PaymentReversalType2]]", "refund");
                        break;
                    case PaymentTypeEnum.Void:
                        replacementValues.Add("[[PaymentReversalType1]]", "voided");
                        replacementValues.Add("[[PaymentReversalType2]]", "void");
                        break;
                    default:
                        throw new ArgumentException("This email is valid only for reversal payment types.", nameof(paymentType));
                }

                return replacementValues;
            });
        }

        // this method is different from all the other e-mails, and is a one off.
        private void PaymentSubmittedConfirmation(int vpGuarantorId, IList<int> paymentProcessorResponseIds, bool isPromptPayment, bool isTextToPayPayment = false)
        {
            ClientDto client = this.GetClient();
            GuarantorDto guarantorDto = this.GetGuarantor(vpGuarantorId);

            if (string.IsNullOrWhiteSpace(guarantorDto.User.Email))
            { 
                this.LoggingService.Value.Warn(() => $"{nameof(SendPaymentEmailsApplicationService)}::{nameof(this.PaymentSubmittedConfirmation)} - No email address found for GuarantorId: {vpGuarantorId}");
                return;
            }

            Dictionary<string, string> additionalValues = new Dictionary<string, string>
            {
                {"[[PersonName]]", guarantorDto.User.FirstName},
            };

            const CommunicationTypeEnum communicationTypeEnum = CommunicationTypeEnum.PaymentSubmittedConfirmation;
            CommunicationAttribute attribute = this.GetAttribute(communicationTypeEnum);

            if (!attribute.SendEmail)
            {
                return;
            }

            SendTo sendToVisitPayUser = new SendTo
            {
	            VisitPayUserId = guarantorDto.User.VisitPayUserId,
				VpGuarantorId = vpGuarantorId
            };

            string locale = guarantorDto.User.Locale;

            CommunicationDto email = this.CommunicationTemplateLoader.Value.CreateEmailFromTemplate(communicationTypeEnum, client, sendToVisitPayUser, locale, guarantorDto.User.Email, additionalValues, false, false, false);

            if (this._emailTrackerService.Value.IsCommunicationForPaymentSent(paymentProcessorResponseIds, EnumHelper<CommunicationTypeEnum>.GetValues(CommunicationTypeEnumCategory.PaymentConfirmation).ToList(), email.CommunicationType.CommunicationMethodId))
            {
                return;
            }

            this.SendEmail(email, attribute.Frequency);
            this._emailTrackerService.Value.TrackPaymentEmail(paymentProcessorResponseIds, email);

            if (!this.ShouldSendSmsMessageForPromptPay(isPromptPayment, isTextToPayPayment, attribute))
            {
                return;
            }

            CommunicationDto sms = this.CommunicationTemplateLoader.Value.CreateSmsFromTemplate(communicationTypeEnum, client, sendToVisitPayUser, locale, guarantorDto.User.SmsPhoneNumber, additionalValues, false, false);
            this.SendSms(sms, attribute.Frequency);
        }

        private bool ShouldSendSmsMessageForPromptPay(bool isPromptPayment, bool isTextToPayPayment, CommunicationAttribute attribute)
        {
            if (!this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.Sms))
            {
                return false;
            }
            if (!attribute.SendSms)
            {
                return false;
            }
            // VPNG-18368 - don't send SMS messages for prompt payments
            // but if it is a TextToPayPayment we want to send a response
            if (isPromptPayment && !isTextToPayPayment)
            {
                return false;
            }

            return true;

        }

        private void PendingPayment(int vpGuarantorId, DateTime paymentDate)
        {
            ClientDto clientDto = this.GetClient();

            this.SendCommunications(SendTo.Guarantor(vpGuarantorId), CommunicationTypeEnum.PendingPayment, visitPayUser => new Dictionary<string, string>
            {
                {"[[ExpiryTime]]", $"{clientDto.ProcessPaymentStartTime} {clientDto.ClientTimeZoneCode}"},
                {"[[NameA]]", visitPayUser.FirstName},
                {"[[PaymentDate]]", paymentDate.Date.ToString(Format.DateFormat)},
            });
        }

        private void PrimaryPaymentMethodChange(int vpGuarantorId)
        {
            this.SendCommunications(SendTo.Guarantor(vpGuarantorId), CommunicationTypeEnum.PrimaryPaymentMethodChange, visitPayUser => new Dictionary<string, string>
            {
                {"[[FirstName]]", visitPayUser.FirstName},
            });
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(SendPaymentReversalConfirmationEmailMessage message, ConsumeContext<SendPaymentReversalConfirmationEmailMessage> consumeContext)
        {
            this.PaymentReversalNotification(message.VpGuarantorId, message.TransactionNumber, message.PaymentType);
        }

        public bool IsMessageValid(SendPaymentReversalConfirmationEmailMessage message, ConsumeContext<SendPaymentReversalConfirmationEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(SendPaymentDueDateChangeEmailMessage message, ConsumeContext<SendPaymentDueDateChangeEmailMessage> consumeContext)
        {
            this.PaymentDueDateChange(message.VpGuarantorId, message.NextPaymentDueDate);
        }

        public bool IsMessageValid(SendPaymentDueDateChangeEmailMessage message, ConsumeContext<SendPaymentDueDateChangeEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(ScheduledSendPaymentConfirmationEmailMessage message, ConsumeContext<ScheduledSendPaymentConfirmationEmailMessage> consumeContext)
        {
            IList<int> paymentProcessorResponseIds = new List<int>();
            if (message.PaymentProcessorResponseIds != null)
            {
                paymentProcessorResponseIds = message.PaymentProcessorResponseIds;
            }

            this.PaymentSubmittedConfirmation(message.VpGuarantorId, paymentProcessorResponseIds, message.IsPromptPayment);
        }

        public bool IsMessageValid(ScheduledSendPaymentConfirmationEmailMessage message, ConsumeContext<ScheduledSendPaymentConfirmationEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SendCardExpiredEmailMessage message, ConsumeContext<SendCardExpiredEmailMessage> consumeContext)
        {
            this.CardExpired(message.VpGuarantorId, message.ExpiryDate);
        }

        public bool IsMessageValid(SendCardExpiredEmailMessage message, ConsumeContext<SendCardExpiredEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SendCardExpiringEmailMessage message, ConsumeContext<SendCardExpiringEmailMessage> consumeContext)
        {
            this.CardExpiring(message.VpGuarantorId, message.ExpiryDate);
        }

        public bool IsMessageValid(SendCardExpiringEmailMessage message, ConsumeContext<SendCardExpiringEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SendPaymentConfirmationEmailMessage message, ConsumeContext<SendPaymentConfirmationEmailMessage> consumeContext)
        {
            IList<int> paymentProcessorResponseIds = new List<int>();
            if (message.PaymentProcessorResponseIds != null)
            {
                paymentProcessorResponseIds = message.PaymentProcessorResponseIds;
            }

            this.PaymentSubmittedConfirmation(message.VpGuarantorId, paymentProcessorResponseIds, message.IsPromptPayment, message.IsTextToPayPayment);
        }

        public bool IsMessageValid(SendPaymentConfirmationEmailMessage message, ConsumeContext<SendPaymentConfirmationEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SendPaymentDueReminderEmailMessage message, ConsumeContext<SendPaymentDueReminderEmailMessage> consumeContext)
        {
            this.BalanceDueReminder(message.VpGuarantorId, message.PaymentDate);
        }

        public bool IsMessageValid(SendPaymentDueReminderEmailMessage message, ConsumeContext<SendPaymentDueReminderEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SendPaymentFailureEmailMessage message, ConsumeContext<SendPaymentFailureEmailMessage> consumeContext)
        {
            this.PaymentFailure(message.VpGuarantorId);
        }

        public bool IsMessageValid(SendPaymentFailureEmailMessage message, ConsumeContext<SendPaymentFailureEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SendPaymentMethodChangeEmailMessage message, ConsumeContext<SendPaymentMethodChangeEmailMessage> consumeContext)
        {
            this.PaymentMethodChange(message.VpGuarantorId);
        }

        public bool IsMessageValid(SendPaymentMethodChangeEmailMessage message, ConsumeContext<SendPaymentMethodChangeEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SendPendingPaymentEmailMessage message, ConsumeContext<SendPendingPaymentEmailMessage> consumeContext)
        {
            this.PendingPayment(message.VpGuarantorId, message.PaymentDate);
        }

        public bool IsMessageValid(SendPendingPaymentEmailMessage message, ConsumeContext<SendPendingPaymentEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SendPrimaryPaymentMethodChangeEmailMessage message, ConsumeContext<SendPrimaryPaymentMethodChangeEmailMessage> consumeContext)
        {
            this.PrimaryPaymentMethodChange(message.VpGuarantorId);
        }

        public bool IsMessageValid(SendPrimaryPaymentMethodChangeEmailMessage message, ConsumeContext<SendPrimaryPaymentMethodChangeEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion
    }
}
namespace Ivh.Application.Email.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Core.Common.Utilities.Builders;
    using Domain.Base.Extensions;
    using Domain.Content.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.Email.Interfaces;
    using Ivh.Common.VisitPay.Strings;
    using OfficeOpenXml;
    using OfficeOpenXml.Style;

    public class UserEmailApplicationService : ApplicationService, IUserEmailApplicationService
    {
        private readonly Lazy<IEmailService> _emailService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IImageService> _imageService;

        public UserEmailApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IEmailService> emailService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IImageService> imageService) : base(applicationServiceCommonService)
        {
            this._emailService = emailService;
            this._guarantorService = guarantorService;
            this._imageService = imageService;
        }

        public IList<CommunicationDto> GetByUserId(int userId)
        {
            throw new NotImplementedException();
        }

        public EmailActionResultDto ResendCommunication(int emailId, string toAddress, int vpGuarantorId, int sentByVisitPayUserId)
        {
            try
            {
                this._emailService.Value.Resend(emailId, toAddress, vpGuarantorId, sentByVisitPayUserId);
                return new EmailActionResultDto(true, "Sent");
            }
            catch (Exception ex)
            {
                this.LoggingService.Value.Warn(() => ex.Message);
                return new EmailActionResultDto(false, "Failed to resend");
            }
        }

        public async Task<byte[]> ExportCommunicationsAsync(CommunicationsFilterDto communicationsFilterDto, int visitPayUserId, string userName)
        {
            const int maxExportRecords = 10000;
            int guarantorId = this._guarantorService.Value.GetGuarantorId(visitPayUserId);
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(guarantorId);
            CommunicationsResultsDto emailResults = this.GetCommunications(communicationsFilterDto, 1, maxExportRecords, visitPayUserId);

            System.Drawing.Image logo = await this._imageService.Value.GetExportLogoAsync();

            using (ExcelPackage package = new ExcelPackage())
            {
                iVinciExcelExportBuilder builder = new iVinciExcelExportBuilder(package, $"{guarantor.User.FirstNameLastName} Email Communications")
                    .WithWorkSheetNameOf("Email Communications")
                    .WithLogo(logo)
                    .AddInfoBlockRow(new ColumnBuilder(DateTime.UtcNow.ToString(Format.MonthDayYearFormat), "File Date:").WithStyle(StyleTypeEnum.AlignLeft))
                    .AddInfoBlockRow(new ColumnBuilder(userName, "Exported By:").WithStyle(StyleTypeEnum.AlignLeft))
                    .AddDataBlock(emailResults.Communications, emailDto => new List<Column>
                    {
                        new ColumnBuilder(this.ConvertDate(emailDto.CreatedDate), "Date first sent").AlignCenter(),
                        new ColumnBuilder(emailDto.Subject, "Subject").AlignLeft(),
                        new ColumnBuilder(this.ConvertDate(emailDto.DeliveredDate), "First Delivered").AlignCenter(),
                        new ColumnBuilder(this.ConvertDate(emailDto.FirstOpenDate), "First Opened").AlignCenter()
                    })
                    .WithNoRecordsFoundMessageOf("There are no emails to display at this time.");

                builder.Build();
                return package.GetAsByteArray();
            }
        }

        public CommunicationsResultsDto GetCommunications(CommunicationsFilterDto communicationsFilterDto, int batch, int batchSize, int visitPayUserId)
        {
            return Mapper.Map<CommunicationsResultsDto>(this._emailService.Value.GetEmailsForUserId(visitPayUserId, Mapper.Map<CommunicationFilter>(communicationsFilterDto), batch, batchSize));
        }

        public int GetCommunicationsTotals(int guarantorVisitPayUserId, CommunicationsFilterDto communicationsFilterDto)
        {
            return this._emailService.Value.GetEmailsForUserIdTotals(guarantorVisitPayUserId, Mapper.Map<CommunicationFilter>(communicationsFilterDto));
        }

        public CommunicationEventResultsDto EmailEventsForEmailId(int emailId, int page, int rows, string sortField, string sortOrder)
        {
            return Mapper.Map<CommunicationEventResultsDto>(this._emailService.Value.GetEventsForEmailId(emailId, page, rows, sortField, sortOrder));
        }

        //TODO- This needs to be converted to the new api. 
        public byte[] ExportEmailEventsForEmailId(int emailId, string sortField, string sortOrder, string userName)
        {
            const int maxExportRecords = 10000;
            int visitPayUserId = this._emailService.Value.GetUserIdForEmailId(emailId);
            int guarantorId = this._guarantorService.Value.GetGuarantorId(visitPayUserId);
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(guarantorId);
            CommunicationEventResultsDto eventResults = this.EmailEventsForEmailId(emailId, 1, maxExportRecords, sortField, sortOrder);

            using (ExcelPackage package = new ExcelPackage())
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Email Events");

                // title
                worksheet.Cells["A1"].Value = $"{guarantor.User.FirstNameLastName} Email Events";
                worksheet.Cells["A1:E1"].Merge = true;
                using (ExcelRange range = worksheet.Cells["A1:E1"])
                {
                    range.Style.Font.Size = 13;
                    range.Style.Font.Bold = true;
                }

                // export info
                worksheet.Cells["A3"].Value = "File Date:";
                worksheet.Cells["A3:B3"].Merge = true;
                worksheet.Cells["C3"].Value = DateTime.UtcNow.ToClientDateTime().ToString(Format.MonthDayYearFormat);
                worksheet.Cells["C3:E3"].Merge = true;

                worksheet.Cells["A4"].Value = "Exported By:";
                worksheet.Cells["A4:B4"].Merge = true;
                worksheet.Cells["C4"].Value = userName;
                worksheet.Cells["C4:E4"].Merge = true;

                using (ExcelRange range = worksheet.Cells["A3:E4"])
                {
                    range.Style.Font.Size = 11;
                    range.Style.Font.Bold = true;
                }

                // data rows
                if (eventResults.TotalRecords > 0)
                {
                    // too many records
                    if (eventResults.TotalRecords > maxExportRecords)
                    {
                        worksheet.Cells["A6"].Value = string.Format("Incomplete download. {0} rows of {1} rows were downloaded. Only {0} rows can be downloaded.", maxExportRecords.ToString("N0"), eventResults.TotalRecords.ToString("N0"));
                        worksheet.Cells["A6:E6"].Merge = true;
                        using (ExcelRange range = worksheet.Cells["A6:E6"])
                        {
                            range.Style.Font.Italic = true;
                        }
                    }

                    int headerIndex = (eventResults.TotalRecords > maxExportRecords) ? 8 : 6;

                    // data headers
                    worksheet.Cells[string.Format("A{0}", headerIndex)].Value = "Sent To";
                    worksheet.Cells[string.Format("B{0}", headerIndex)].Value = "Event";
                    worksheet.Cells[string.Format("C{0}", headerIndex)].Value = "Date";
                    worksheet.Cells[string.Format("D{0}", headerIndex)].Value = "Reason";
                    worksheet.Cells[string.Format("E{0}", headerIndex)].Value = "Response";

                    using (ExcelRange range = worksheet.Cells[string.Format("A{0}:E{0}", headerIndex)])
                    {
                        range.Style.Font.Size = 11;
                        range.Style.Font.Bold = true;
                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        range.Style.Border.BorderAround(ExcelBorderStyle.Medium);
                        range.Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    }

                    int startRowIndex = headerIndex + 1;
                    int rowIndex = startRowIndex;

                    foreach (CommunicationEventDto eventDto in eventResults.Events)
                    {
                        worksheet.Cells[string.Format("A{0}", rowIndex)].Value = eventDto.ToAddress;
                        worksheet.Cells[string.Format("B{0}", rowIndex)].Value = eventDto.EventName;
                        worksheet.Cells[string.Format("C{0}", rowIndex)].Value = this.ConvertDate(eventDto.EventDateTime);
                        worksheet.Cells[string.Format("D{0}", rowIndex)].Value = eventDto.Reason;
                        worksheet.Cells[string.Format("E{0}", rowIndex)].Value = eventDto.Response;

                        worksheet.Cells[string.Format("B{0}", rowIndex)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        worksheet.Cells[string.Format("A{0}", rowIndex)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[string.Format("C{0}", rowIndex)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[string.Format("D{0}", rowIndex)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        worksheet.Cells[string.Format("E{0}", rowIndex)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        rowIndex++;
                    }

                    // border
                    using (ExcelRange range = worksheet.Cells[string.Format("A{0}:E{1}", startRowIndex, rowIndex - 1)])
                    {
                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }
                }
                else
                {
                    worksheet.Cells["A6"].Value = "There are no email events to display at this time.";
                    worksheet.Cells["A6:E6"].Merge = true;
                    using (ExcelRange range = worksheet.Cells["A6:E6"])
                    {
                        range.Style.Font.Size = 11;
                        range.Style.Font.Bold = true;
                    }
                }

                worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();

                return package.GetAsByteArray();
            }
        }

        private string ConvertDate(DateTime? date, string format = Format.DateTimeFormat, string altText = "No")
        {
            return date.HasValue ? date.Value.ToString(format) : "No";
        }
    }
}
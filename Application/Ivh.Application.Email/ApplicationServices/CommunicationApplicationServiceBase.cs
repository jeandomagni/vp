namespace Ivh.Application.Email.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Core.Common.Dtos;
    using Domain.Base.Extensions;
    using Domain.Email.Entities;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.Email.Interfaces;
    using Domain.User.Entities;
    using Domain.User.Interfaces;
    using Internal.Dtos;
    using Internal.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.VisitPay.Attributes;
    using Ivh.Common.VisitPay.Enums;
    using User.Common.Dtos;
    using Ivh.Common.VisitPay.Strings;
    using Ivh.Application.FinanceManagement.Common.Dtos;

    public abstract class CommunicationApplicationServiceBase : ApplicationService
    {
        protected readonly Lazy<ICommunicationSender> CommunicationSender;
        protected readonly Lazy<ICommunicationTemplateLoader> CommunicationTemplateLoader;
        protected readonly Lazy<ICommunicationService> CommunicationService;
        protected readonly Lazy<IEmailService> EmailService;
        protected readonly Lazy<IGuarantorService> GuarantorService;
        protected readonly Lazy<IVisitPayUserRepository> VisitPayUserRepository;
        protected readonly Lazy<IVisitPayUserService> VisitPayUserService;

        private ClientDto _clientDto;

        protected CommunicationApplicationServiceBase(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<ICommunicationSender> communicationSender,
            Lazy<ICommunicationTemplateLoader> communicationTemplateLoader,
            Lazy<ICommunicationService> communicationService,
            Lazy<IEmailService> emailService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IVisitPayUserRepository> visitPayUserRepository,
            Lazy<IVisitPayUserService> visitPayUserService) : base(applicationServiceCommonService)
        {
            this.CommunicationSender = communicationSender;
            this.CommunicationTemplateLoader = communicationTemplateLoader;
            this.CommunicationService = communicationService;
            this.EmailService = emailService;
            this.GuarantorService = guarantorService;
            this.VisitPayUserRepository = visitPayUserRepository;
            this.VisitPayUserService = visitPayUserService;
        }

        protected GuarantorDto GetGuarantor(int guarantorId)
        {
            return Mapper.Map<GuarantorDto>(this.GuarantorService.Value.GetGuarantor(guarantorId));
        }

        protected ClientDto GetClient()
        {
            return this._clientDto ?? (this._clientDto = Mapper.Map<ClientDto>(this.ClientService.Value.GetClient()));
        }

        protected bool IsSuppressedOnRegistration(CommunicationAttribute attribute, Guarantor guarantor)
        {
            return attribute != null && attribute.SuppressOnRegistrationDay && guarantor?.RegistrationDate.Date == DateTime.UtcNow.Date;
        }

        protected bool SendCommunications(
            SendTo sendTo,
            CommunicationTypeEnum communicationTypeEnum,
            Func<VisitPayUserDto, IDictionary<string, string>> replacementFactory,
            FinancePlanDto financePlanDto = null)
        {
            return this.SendCommunications(
                sendTo,
                communicationTypeEnum,
                communicationTypeEnum,
                replacementFactory,
                financePlanDto);
        }

        protected bool SendCommunications(
            SendTo sendTo,
            CommunicationTypeEnum communicationTypeEnum,
            Func<VisitPayUserDto, IDictionary<string, string>> replacementFactory,
            string overrideEmail)
        {
            return this.SendCommunications(
                sendTo,
                communicationTypeEnum,
                communicationTypeEnum,
                replacementFactory,
                null,
                overrideEmail);
        }

        protected bool SendCommunications(
            SendTo sendTo,
            CommunicationTypeEnum communicationTypeEnum,
            CommunicationTypeEnum managingUserCommunicationTypeEnum,
            Func<VisitPayUserDto, IDictionary<string, string>> replacementFactory,
            FinancePlanDto financePlanDto,
            string overrideEmail = null)
        {
            CommunicationAttribute attribute = this.GetAttribute(communicationTypeEnum);
            if (attribute == null || (!attribute.SendEmail && !attribute.SendSms && !attribute.SendMail))
            {
                this.LoggingService.Value.Fatal(() => $"{nameof(CommunicationApplicationServiceBase)}::{nameof(SendCommunications)} - SendEmail, SendSms, and SendMail are all false for type = {communicationTypeEnum.ToString()}");
                return false;
            }

            Guarantor guarantor;
            VisitPayUser visitPayUser;

            if (sendTo.VpGuarantorId.HasValue)
            {
                // find user and guarantor from VpGuarantorId
                guarantor = this.GuarantorService.Value.GetGuarantor(sendTo.VpGuarantorId.Value);
                visitPayUser = guarantor.User;
            }
            else if (sendTo.VisitPayUserId.HasValue)
            {
                // find guarantor and user by VisitPayUserId
                // might not have a guarantor if this is going to a client user
                visitPayUser = this.VisitPayUserRepository.Value.FindById(sendTo.VisitPayUserId.ToString());
                if (visitPayUser != null && !visitPayUser.IsInClientRole)
                {
                    guarantor = this.GuarantorService.Value.GetGuarantorEx(sendTo.VisitPayUserId.Value);
                }
                else
                {
                    guarantor = null;
                }
            }
            else
            {
                this.LoggingService.Value.Fatal(() => $"{nameof(CommunicationApplicationServiceBase)}::{nameof(SendCommunications)} - user and guarantor not provided for type = {communicationTypeEnum.ToString()}");
                return false;
            }

            if (visitPayUser == null)
            {
                string s = $"vpgid = {(sendTo.VpGuarantorId.HasValue ? sendTo.VpGuarantorId.Value.ToString() : "NULL")}, vpu = {(sendTo.VisitPayUserId.HasValue ? sendTo.VisitPayUserId.Value.ToString() : "NULL")}";
                this.LoggingService.Value.Fatal(() => $"{nameof(CommunicationApplicationServiceBase)}::{nameof(SendCommunications)} - user ({s}) not found for type = {communicationTypeEnum.ToString()}");
                return false;
            }

            bool isManaged = false;
            int? managingVpGuarantorId = null;
            if (guarantor != null)
            {
                isManaged = guarantor.IsManaged();
                managingVpGuarantorId = isManaged ? guarantor.ManagingGuarantorId : null;
            }

            ClientDto client = this.GetClient();
            VisitPayUserDto visitPayUserDto = Mapper.Map<VisitPayUserDto>(visitPayUser);
            string userLocale = visitPayUser.Locale;

            //
            bool emailSent = false;
            bool smsSent = false;
            bool mailSent = false;

            IDictionary<string, string> replacementValues = replacementFactory(visitPayUserDto);

            if (this.IsSuppressedOnRegistration(attribute, guarantor))
            {
                return false;
            }

            SendTo sendToVisitPayUser = new SendTo
            {
	            VisitPayUserId = visitPayUserDto.VisitPayUserId,
				VpGuarantorId = guarantor?.VpGuarantorId
            };

			// send to current user
			if (attribute.SendEmail)
            {
                string toEmail = string.IsNullOrWhiteSpace(overrideEmail) ? visitPayUserDto.Email : overrideEmail;

                if (string.IsNullOrWhiteSpace(toEmail))
                {
                    this.LoggingService.Value.Warn(() => $"{nameof(CommunicationApplicationServiceBase)}::{nameof(SendCommunications)} - No email address found for GuarantorId: {guarantor?.VpGuarantorId}");
                    emailSent = false;
                }
                else
                {
                    CommunicationDto email = this.CommunicationTemplateLoader.Value.CreateEmailFromTemplate(communicationTypeEnum, client, sendToVisitPayUser, userLocale, toEmail, replacementValues, isManaged, false, attribute.HideLogin);
                    emailSent = this.SendEmail(email, attribute.Frequency);
                }
            }

            if (attribute.SendSms && this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.Sms))
            {
                string toPhoneNumber = visitPayUserDto.SmsPhoneNumber;
                CommunicationDto sms = this.CommunicationTemplateLoader.Value.CreateSmsFromTemplate(communicationTypeEnum, client, sendToVisitPayUser, userLocale, toPhoneNumber, replacementValues, isManaged, false);
                smsSent = this.SendSms(sms, attribute.Frequency);
            }

            if (attribute.SendMail && this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureMailIsEnabled))
            {
                string toAddressee = visitPayUserDto.DisplayFirstNameLastName;

                if (replacementValues == null)
                {
                    replacementValues = new Dictionary<string, string>();
                }

                this.SetRecipientMailingAddressReplacementValues(visitPayUserDto, replacementValues);
                replacementValues["[[IssueDate]]"] = this.ClientNow.ToString(Format.MonthDayYearFormat);

                CommunicationDto mail = 
                    this.CommunicationTemplateLoader.Value
                        .CreateMailFromTemplate(
                            communicationTypeEnum,
                            client,
                            sendToVisitPayUser,
                            userLocale,
                            toAddressee,
                            replacementValues,
                            isManaged,
                            false,
                            financePlanDto);

                mailSent = this.SendMail(mail, attribute.Frequency);
            }

            // cc managing user
            if (attribute.SendManaging && managingVpGuarantorId.HasValue)
            {
				Guarantor managingGuarantor = guarantor.ManagingConsolidationGuarantors
                    .Where(x => x.ManagingGuarantor != null)
                    .Select(x => x.ManagingGuarantor)
                    .FirstOrDefault(x => x.VpGuarantorId == managingVpGuarantorId.Value);

                if (managingGuarantor != null)
                {
                    VisitPayUser managingUser = managingGuarantor.User;
                    string managingLocale = managingUser.Locale;
                    
                    sendToVisitPayUser = new SendTo
                    {
	                    VisitPayUserId = managingUser.VisitPayUserId,
	                    VpGuarantorId = managingVpGuarantorId.Value
                    };

					CommunicationAttribute managingAttribute = this.GetAttribute(managingUserCommunicationTypeEnum);

                    if (managingAttribute.SendEmail)
                    {
                        CommunicationDto managingEmail = this.CommunicationTemplateLoader.Value.CreateEmailFromTemplate(managingUserCommunicationTypeEnum, client, sendToVisitPayUser, managingLocale, managingUser.Email, replacementValues, true, true, managingAttribute.HideLogin);
                        this.SendEmail(managingEmail, managingAttribute.Frequency);
                    }

                    if (managingAttribute.SendSms && this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.Sms))
                    {
                        CommunicationDto managingSms = this.CommunicationTemplateLoader.Value.CreateSmsFromTemplate(managingUserCommunicationTypeEnum, client, sendToVisitPayUser, managingLocale, managingUser.SmsPhoneNumber, replacementValues, true, true);
                        this.SendSms(managingSms, managingAttribute.Frequency);
                    }
                }
            }

            return emailSent || smsSent || mailSent;
        }

        private void SetRecipientMailingAddressReplacementValues(
            VisitPayUserDto visitPayUserDto,
            IDictionary<string, string> replacementValues)
        {
            replacementValues["[[RecipientMailingAddressName1]]"] = visitPayUserDto.DisplayFirstNameLastName;

            replacementValues["[[RecipientMailingAddressAddress1]]"] = 
                visitPayUserDto.MailingAddressStreet1.IsNullOrEmpty() 
                    ? visitPayUserDto.AddressStreet1 
                    : visitPayUserDto.MailingAddressStreet1;

            replacementValues["[[RecipientMailingAddressAddress2]]"] = 
                visitPayUserDto.MailingAddressStreet2.IsNullOrEmpty()
                    ? visitPayUserDto.AddressStreet2
                    : visitPayUserDto.MailingAddressStreet2;

            replacementValues["[[RecipientMailingAddressCity]]"] = 
                visitPayUserDto.MailingCity.IsNullOrEmpty()
                    ? visitPayUserDto.City
                    : visitPayUserDto.MailingCity;

            replacementValues["[[RecipientMailingAddressState]]"] = 
                visitPayUserDto.MailingState.IsNullOrEmpty()
                    ? visitPayUserDto.State
                    : visitPayUserDto.MailingState;

            replacementValues["[[RecipientMailingAddressZip]]"] = 
                visitPayUserDto.MailingZip.IsNullOrEmpty()
                    ? visitPayUserDto.Zip
                    : visitPayUserDto.MailingZip;
        }

        protected bool SendCommunications(string toEmail, CommunicationTypeEnum communicationTypeEnum, Func<IDictionary<string, string>> replacementFactory)
        {
            if (string.IsNullOrWhiteSpace(toEmail))
            {
                this.LoggingService.Value.Fatal(() => $"{nameof(CommunicationApplicationServiceBase)}::{nameof(SendCommunications)} - {nameof(toEmail)} not provided for type = {communicationTypeEnum.ToString()}");
                return false;
            }

            CommunicationAttribute attribute = this.GetAttribute(communicationTypeEnum);
            ClientDto client = this.GetClient();
            IDictionary<string, string> replacementValues = replacementFactory();

            CommunicationDto email = this.CommunicationTemplateLoader.Value.CreateEmailFromTemplate(communicationTypeEnum, client, null, null, toEmail, replacementValues, false, false, attribute.HideLogin);
            bool emailSent = this.SendEmail(email, attribute.Frequency);

            return emailSent;
        }

        protected CommunicationDto ResendCommunicationBase(int communicationId, string toAddress)
        {
            CommunicationDto communicationDto = this.CommunicationSender.Value.ResendCommunicationAsync(communicationId, toAddress).Result;
            if (communicationDto != null)
            {
                return communicationDto;
            }

            this.LoggingService.Value.Warn(() => $"Unable to resend emailid {communicationId}");
            return null;
        }

        protected void QueueHeldEmailForResendBase(IList<CommunicationTypeEnum> emailTypes = null)
        {
            foreach (int emailId in this.EmailService.Value.GetHeldEmailIds(emailTypes))
            {
                this.EmailService.Value.Resend(emailId);
            }
        }

        private bool CanSendCommunication(CommunicationDto communicationDto)
        {
            if (communicationDto == null)
            {
                this.LoggingService.Value.Fatal(() => $"CommunicationApplicationServiceBase::Communication is null - Stack Trace: {Environment.StackTrace}");
                return false;
            }

            //we need to check to see if this is a guarantor and if so that the guarantor is active
            if (this.IsGuarantorDisabledForClosedAccount(communicationDto))
            {
                return false;
            }

            // allow email blast and guest pay and other communications not directed to a visit pay user specifically
            if (!this.IsVisitPayUserSpecific(communicationDto))
            {
                return true;
            }

            if (this.HasGuarantorDisabledCommunicationGroup(communicationDto))
            {
                return false;
            }

            if (!this.IsCommunicationTypeAllowedForGuarantor(communicationDto))
            {
                return false;
            }

            return true;
        }

        private bool IsVisitPayUserSpecific(CommunicationDto communicationDto)
        {
            // if false then this is a promotional email, guest pay or support request where the person receiving the email may not be a vp user or it's not relevant
            bool hasVisitPayUserId = communicationDto.SentToUserId.HasValue;
            return hasVisitPayUserId;
        }

        protected bool SendEmail(CommunicationDto communicationDto, NotificationFrequencyEnum frequencyEnum = NotificationFrequencyEnum.None, IList<CommunicationTypeEnum> additionalCheckTypes = null)
        {
            bool canSend = this.CanSendCommunication(communicationDto);
            if (!canSend)
            {
                return false;
            }

            if (frequencyEnum == NotificationFrequencyEnum.None || !this.HasBeenSent(communicationDto, frequencyEnum, CommunicationMethodEnum.Email, additionalCheckTypes))
            {
                this.EmailService.Value.Update(communicationDto);
                bool emailSent = this.CommunicationSender.Value.SendEmailAsync(communicationDto).Result;
                if (!emailSent)
                {
                    this.LoggingService.Value.Warn(() => $"Unable to send email {communicationDto.Subject} to {communicationDto.ToAddress}");
                }

                return emailSent;
            }
            else
            {
                this.LoggingService.Value.Warn(() => $"CommunicationApplicationServiceBase::SendEmail - (Seems like this email was already Sent) Choosing to not send email {communicationDto.Subject} to {communicationDto.ToAddress}");
            }
            return false;
        }

        protected bool SendSms(CommunicationDto communicationDto, NotificationFrequencyEnum frequencyEnum = NotificationFrequencyEnum.None, IList<CommunicationTypeEnum> additionalCheckTypes = null)
        {
            bool canSend = this.CanSendCommunication(communicationDto);
            if (!canSend)
            {
                return false;
            }

            if (frequencyEnum == NotificationFrequencyEnum.None || !this.HasBeenSent(communicationDto, frequencyEnum, CommunicationMethodEnum.Sms, additionalCheckTypes))
            {
                this.EmailService.Value.Update(communicationDto);

                if (!this.CommunicationSender.Value.SendSmsAsync(communicationDto).Result)
                {
                    this.LoggingService.Value.Warn(() => $"Unable to send SMS {communicationDto.Subject} to {communicationDto.ToAddress}");
                    return false;
                }

                return true;
            }

            this.LoggingService.Value.Warn(() => $"CommunicationApplicationServiceBase::SendSms - (Seems like this SMS was already sent) Choosing to not send SMS {communicationDto.Subject} to {communicationDto.ToAddress}");
            return false;
        }

        protected bool SendMail(CommunicationDto communicationDto, NotificationFrequencyEnum frequencyEnum = NotificationFrequencyEnum.None, IList<CommunicationTypeEnum> additionalCheckTypes = null)
        {
            bool canSend = this.CanSendCommunication(communicationDto);
            if (!canSend)
            {
                return false;
            }

            if (frequencyEnum == NotificationFrequencyEnum.None || !this.HasBeenSent(communicationDto, frequencyEnum, CommunicationMethodEnum.Mail, additionalCheckTypes))
            {
                this.EmailService.Value.Update(communicationDto);

                // We are not sending the mail immediately, so we just save the unsent communication record.
                // A job will ran later to export unsent mail communication records to files for the client's print/mail vendor to consume.

                return true;
            }

            this.LoggingService.Value.Warn(() => $"CommunicationApplicationServiceBase::SendMail - (Seems like this paper mail was already sent) Choosing to not send paper mail {communicationDto.Subject} to {communicationDto.ToAddress}");
            return false;
        }

        protected CommunicationAttribute GetAttribute(CommunicationTypeEnum communicationTypeEnum)
        {
            CommunicationAttribute attribute = communicationTypeEnum.GetAttribute<CommunicationAttribute>();
            if (attribute == null)
            {
                string Message() => $"{nameof(CommunicationApplicationServiceBase)}::{nameof(this.GetAttribute)} - {communicationTypeEnum.ToString()} does not have ${nameof(CommunicationAttribute)}";
                this.LoggingService.Value.Fatal(Message);

                throw new NotImplementedException($"{nameof(CommunicationApplicationServiceBase)}::{nameof(this.GetAttribute)} - {communicationTypeEnum.ToString()} does not have ${nameof(CommunicationAttribute)}");
            }

            return attribute;
        }

        private bool HasGuarantorDisabledCommunicationGroup(CommunicationDto communicationDto)
        {
            bool sentToUserIdIsSet = communicationDto.SentToUserId.HasValue;

            CommunicationMethodEnum communicationMethod = communicationDto.CommunicationType.CommunicationMethodId;
            CommunicationTypeEnum communicationType = communicationDto.CommunicationType.CommunicationTypeId;
            int? communicationGroup = communicationDto.CommunicationType.CommunicationGroupId;

            bool noCommunicationGroupFound = communicationGroup == null;
            bool alwaysSend = communicationGroup == 0;
            bool isCommunicationGroupExempt = noCommunicationGroupFound || alwaysSend;

            if (isCommunicationGroupExempt)
            {
                //if the user has opted out of the communication method we do not want to exempt the communication
                if (sentToUserIdIsSet)
                {
                    IList<VisitPayUserCommunicationPreference> communicationPreferences = this.CommunicationService.Value.CommunicationPreferences(communicationDto.SentToUserId.Value);
                    bool hasOptedIntoCommunicationMethod = communicationPreferences?.Any(x => x.CommunicationMethodId == communicationMethod)?? false;
                    isCommunicationGroupExempt = hasOptedIntoCommunicationMethod; 
                }
            }

            if (isCommunicationGroupExempt)
            {
                return false;
            }

            if (sentToUserIdIsSet)
            {
                Guarantor guarantor = this.GuarantorService.Value.GetGuarantorEx(communicationDto.SentToUserId.Value);
                bool hasCommunicationPreference = this.CommunicationService.Value.HasCommunicationPreference(guarantor.User.VisitPayUserId, communicationMethod, communicationType);
                return !hasCommunicationPreference;
            }

            this.LoggingService.Value.Warn(() => $"Attempting to send to guarantor with problematic communication preference. Guarantor Id: {(communicationDto.SentToUserId.HasValue ? communicationDto.SentToUserId.ToString() : "No SentToUserId")} Destination: {communicationDto.ToAddress} CommunicationMethod: {communicationMethod} CommunicationType: {communicationType} CommunicationGroup: {communicationGroup}");

            return true;
        }

        private bool IsGuarantorDisabledForClosedAccount(CommunicationDto communicationDto)
        {
            Guarantor guarantor = null;
            if (communicationDto.SentToUserId.HasValue)
            {
                guarantor = this.GuarantorService.Value.GetGuarantorEx(communicationDto.SentToUserId.Value);
            }

            if (guarantor == null || !guarantor.IsClosed || communicationDto.CommunicationType.CommunicationTypeId.IsInCategory(CommunicationTypeEnumCategory.Cancellation))
            {
                return false;
            }

            this.LoggingService.Value.Warn(() => $"Attempting to send to guarantor with closed account. Guarantor Id: {guarantor.VpGuarantorId} Email Address {communicationDto.ToAddress}");
            return true;
        }

        private bool IsCommunicationTypeAllowedForGuarantor(CommunicationDto communicationDto)
        {

            Guarantor guarantor = this.GuarantorService.Value.GetGuarantorEx(communicationDto.SentToUserId.Value);
            if (guarantor?.IsOfflineGuarantor() ?? false)
            {
                bool isAllowed = this.IsCommunicationTypeAllowedForOfflineGuarantor(guarantor, communicationDto.CommunicationType.CommunicationTypeId);
                if (isAllowed)
                {
                    return true;
                }

                this.LoggingService.Value.Info(() => $"This is an invalid communication type for an offline guarantor. Choosing not to send. Guarantor Id: {guarantor.VpGuarantorId.ToString()} ToAddress: {communicationDto.ToAddress} CommunicationType: {communicationDto.CommunicationType.CommunicationTypeId}");
                return false;
            }

            return true;
        }

        private bool IsCommunicationTypeAllowedForOfflineGuarantor(Guarantor guarantor, CommunicationTypeEnum communicationTypeEnum)
        {
            if (guarantor?.IsOfflineGuarantor() ?? false)
            {
                List<CommunicationTypeEnum> allowedCommunicationTypeEnums = Enum.GetValues(typeof(CommunicationTypeEnum))
                    .Cast<CommunicationTypeEnum>()
                    .Where(x => x.GetAttribute<CommunicationAttribute>() != null)
                    .Where(x => x.GetAttribute<CommunicationAttribute>().SendToVpccUser)
                    .ToList();

                return allowedCommunicationTypeEnums.Contains(communicationTypeEnum);
            }

            return true;
        }

        private bool HasBeenSent(CommunicationDto communicationDto, NotificationFrequencyEnum frequencyEnum, CommunicationMethodEnum communicationMethod, IList<CommunicationTypeEnum> additionalCheckTypes)
        {
            bool hasBeenSent = this.HasBeenSent(communicationDto.SentToUserId, communicationDto.CommunicationType, frequencyEnum);
            if (hasBeenSent)
            {
                return true;
            }

            if (additionalCheckTypes == null || !additionalCheckTypes.Any())
            {
                return false;
            }

            foreach (CommunicationTypeEnum additionalCheckType in additionalCheckTypes)
            {
                int? cmsVersionId = this.EmailService.Value.GetCmsVersionIdForEmailType(additionalCheckType, communicationMethod);
                hasBeenSent = cmsVersionId.HasValue && this.HasBeenSent(communicationDto.SentToUserId, communicationDto.CommunicationType, frequencyEnum);
                if (hasBeenSent)
                {
                    return true;
                }
            }

            return false;
        }

        private bool HasBeenSent(int? sentToUserId, CommunicationTypeDto communicationTypeDto, NotificationFrequencyEnum frequencyEnum)
        {
            if (!sentToUserId.HasValue || frequencyEnum == NotificationFrequencyEnum.None)
            {
                return false;
            }

            DateTime dateRangeFrom;
            switch (frequencyEnum)
            {
                case NotificationFrequencyEnum.OncePerHour:
                    dateRangeFrom = DateTime.UtcNow.AddHours(-1);
                    break;
                case NotificationFrequencyEnum.OncePerDay:
                    dateRangeFrom = DateTime.UtcNow.Date;
                    break;
                case NotificationFrequencyEnum.OncePerWeek:
                    dateRangeFrom = DateTime.UtcNow.Date.AddDays(-7);
                    break;
                case NotificationFrequencyEnum.OncePerMonth:
                    DateTime today = DateTime.UtcNow;
                    dateRangeFrom = new DateTime(today.Year, today.Month, 1, 0, 0, 0, 0);
                    break;
                default:
                    return false;
            }

            int total = this.EmailService.Value.GetEmailsForUserIdTotals(sentToUserId.Value, new CommunicationFilter
            {
                DateRangeFrom = dateRangeFrom,
                CommunicationTypeId = (int?)communicationTypeDto.CommunicationTypeId,
                CommunicationMethodId = (int?)communicationTypeDto.CommunicationMethodId
            });

            return total > 0;
        }
    }
}
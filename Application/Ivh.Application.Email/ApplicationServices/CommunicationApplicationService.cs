﻿namespace Ivh.Application.Email.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Core.Common.Dtos;
    using Core.Common.Interfaces;
    using Domain.Content.Entities;
    using Domain.Content.Interfaces;
    using Domain.Email.Entities;
    using Domain.Email.Interfaces;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.EventJournal;
    using Ivh.Common.VisitPay.Enums;

    public class CommunicationApplicationService : ApplicationService, ICommunicationApplicationService
    {
        private readonly ICommunicationService _communicationService;
        private readonly Lazy<IContentService> _contentService;
        private readonly Lazy<IVisitPayUserJournalEventApplicationService> _visitPayUserJournalEventApplicationService;

        public CommunicationApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            ICommunicationService communicationService,
            Lazy<IContentService> contentService,
            Lazy<IVisitPayUserJournalEventApplicationService> visitPayUserJournalEventApplicationService) : base(applicationServiceCommonService)
        {
            this._contentService = contentService;
            this._communicationService = communicationService;
            this._visitPayUserJournalEventApplicationService = visitPayUserJournalEventApplicationService;
        }

        public IList<VisitPayUserCommunicationPreferenceDto> CommunicationPreferences(int visitPayUserId)
        {
            return Mapper.Map<IList<VisitPayUserCommunicationPreference>, IList<VisitPayUserCommunicationPreferenceDto>>(this._communicationService.CommunicationPreferences(visitPayUserId));
        }

        public void AddPreference(CommunicationMethodEnum communicationMethodEnum, int targetVisitPayUserId, int communicationGroupId, int vpGuarantorId, int visitPayUserId, JournalEventHttpContextDto context)
        {
            this._communicationService.AddPreference(targetVisitPayUserId, communicationMethodEnum, communicationGroupId);
            string category = this.GetCommunicationGroupName(communicationGroupId);
            switch (communicationMethodEnum)
            {
                case CommunicationMethodEnum.Sms :
                    this._visitPayUserJournalEventApplicationService.Value.LogSmsCategoryChanged(targetVisitPayUserId, visitPayUserId, vpGuarantorId, category, "enabled", context);
                    break;
            }
            
        }

        public void RemovePreference(CommunicationMethodEnum communicationMethodEnum, int targetVisitPayUserId, int communicationGroupId, int vpGuarantorId, int visitPayUserId, JournalEventHttpContextDto context)
        {
            this._communicationService.RemovePreference(targetVisitPayUserId, communicationMethodEnum, communicationGroupId);
            string category = this.GetCommunicationGroupName(communicationGroupId);
            switch (communicationMethodEnum)
            {
                case CommunicationMethodEnum.Sms :
                    this._visitPayUserJournalEventApplicationService.Value.LogSmsCategoryChanged(targetVisitPayUserId, visitPayUserId, vpGuarantorId, category , "disabled", context);
                    break;
            }          
        }

        public async Task<IList<CommunicationGroupDto>> GetCommunicationGroupsAsync()
        {
            IList<CommunicationGroupDto> communicationGroupDtos = new List<CommunicationGroupDto>();

            IList<CommunicationGroup> communicationGroups = this._communicationService.CommunicationGroups;
            foreach (CommunicationGroup communicationGroup in communicationGroups)
            {
                CommunicationGroupDto communicationGroupDto = Mapper.Map<CommunicationGroupDto>(communicationGroup);
                communicationGroupDto.Description = string.Empty;

                if (communicationGroup.DescriptionCmsRegion.HasValue)
                {
                    CmsVersion descriptionCmsVersion = await this._contentService.Value.GetCurrentVersionAsync(communicationGroup.DescriptionCmsRegion.Value, true);
                    communicationGroupDto.Description = descriptionCmsVersion?.ContentBody ?? string.Empty;
                }

                communicationGroupDtos.Add(communicationGroupDto);
            }

            return communicationGroupDtos;
        }

        private string GetCommunicationGroupName(int communicationGroupId)
        {
            CommunicationGroup communicationGroup = this._communicationService.GetCommunicationGroup(communicationGroupId);
            string category = communicationGroup?.CommunicationGroupName ?? communicationGroupId.ToString();
            return category;
        }

        public IList<CommunicationSmsBlockedNumberDto> FindBlockedNumbers(string phoneNumber)
        {
            return Mapper.Map<IList<CommunicationSmsBlockedNumberDto>>(this._communicationService.FindBlockedNumbers(phoneNumber));
        }

        public void AddBlockedNumber(string phoneNumber, string smsNumber)
        {
            this._communicationService.AddBlockedNumber(phoneNumber, smsNumber);
        }

        public void RemoveBlockedNumber(string phoneNumber, string smsNumber)
        {
            this._communicationService.RemoveBlockedNumber(phoneNumber, smsNumber);
        }

    }
}
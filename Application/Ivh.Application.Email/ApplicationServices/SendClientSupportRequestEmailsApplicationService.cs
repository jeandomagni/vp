﻿namespace Ivh.Application.Email.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using Base.Common.Interfaces;
    using Common.Interfaces;
    using Core.Common.Dtos;
    using Domain.User.Interfaces;
    using Domain.Email.Interfaces;
    using Domain.Guarantor.Interfaces;
    using Internal.Dtos;
    using Internal.Interfaces;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Communication;
    using Ivh.Common.VisitPay.Strings;
    using MassTransit;

    public class SendClientSupportRequestEmailsApplicationService : CommunicationApplicationServiceBase, ISendClientSupportRequestEmailsApplicationService
    {
        public SendClientSupportRequestEmailsApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<ICommunicationSender> communicationSender,
            Lazy<ICommunicationTemplateLoader> communicationTemplateLoader,
            Lazy<ICommunicationService> communicationService,
            Lazy<IEmailService> emailService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IVisitPayUserRepository> visitPayUserRepository,
            Lazy<IVisitPayUserService> visitPayUserService)
            : base(applicationServiceCommonService,
                communicationSender,
                communicationTemplateLoader,
                communicationService,
                emailService,
                guarantorService,
                visitPayUserRepository,
                visitPayUserService)
        {
        }

        public void ClientSupportRequestConfirmation(int visitPayUserId, string clientSupportRequestDisplayId)
        {
            this.SendCommunications(SendTo.User(visitPayUserId), CommunicationTypeEnum.ClientSupportRequestConfirmation, visitPayUser => new Dictionary<string, string>
            {
                {"[[Name]]", visitPayUser.FirstName},
                {"[[ClientSupportRequestDisplayId]]", clientSupportRequestDisplayId}
            });
        }

        public void ClientSupportRequestAdminNotification(string clientSupportRequestDisplayId)
        {
            ClientDto clientDto = this.GetClient();

            this.SendCommunications(clientDto.AppSupportNotificationEmail, CommunicationTypeEnum.ClientSupportRequestAdminNotification, () => new Dictionary<string, string>
            {
                {"[[ClientSupportRequestDisplayId]]", clientSupportRequestDisplayId},
            });
        }

        public void ClientSupportRequestMessageAdminNotification(string clientSupportRequestDisplayId, DateTime insertDate)
        {
            ClientDto clientDto = this.GetClient();

            this.SendCommunications(clientDto.AppSupportNotificationEmail, CommunicationTypeEnum.ClientSupportRequestMessageNotification, () => new Dictionary<string, string>
            {
                {"[[Name]]", clientDto.AppSupportRequestNamePrefix},
                {"[[ClientSupportRequestDisplayId]]", clientSupportRequestDisplayId},
                {"[[InsertDate]]", insertDate.ToString(Format.DateFormat)}
            });
        }

        public void ClientSupportRequestMessageNotification(int visitPayUserId, string clientSupportRequestDisplayId, DateTime insertDate)
        {
            this.SendCommunications(SendTo.User(visitPayUserId), CommunicationTypeEnum.ClientSupportRequestMessageNotification, visitPayUser => new Dictionary<string, string>
            {
                {"[[Name]]", visitPayUser.FirstName},
                {"[[ClientSupportRequestDisplayId]]", clientSupportRequestDisplayId},
                {"[[InsertDate]]", insertDate.ToString(Format.DateFormat)}
            });
        }

        public void ClientSupportRequestStatusNotification(int visitPayUserId, string clientSupportRequestDisplayId, DateTime insertDate, ClientSupportRequestStatusEnum status)
        {
            this.SendCommunications(SendTo.User(visitPayUserId), CommunicationTypeEnum.ClientSupportRequestStatusNotification, visitPayUser => new Dictionary<string, string>
            {
                {"[[Name]]", visitPayUser.FirstName},
                {"[[ClientSupportRequestDisplayId]]", clientSupportRequestDisplayId},
                {"[[InsertDate]]", insertDate.ToString(Format.DateFormat)},
                {"[[ModificationStatus]]", status == ClientSupportRequestStatusEnum.Closed ? "closed" : "re-opened"}
            });
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(SendClientSupportRequestStatusNotificationEmailMessage message, ConsumeContext<SendClientSupportRequestStatusNotificationEmailMessage> consumeContext)
        {
            this.ClientSupportRequestStatusNotification(message.VisitPayUserId, message.ClientSupportRequestDisplayId, message.InsertDate, message.Status);
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(SendClientSupportRequestMessageAdminNotificationEmailMessage message, ConsumeContext<SendClientSupportRequestMessageAdminNotificationEmailMessage> consumeContext)
        {
            this.ClientSupportRequestMessageAdminNotification(message.ClientSupportRequestDisplayId, message.InsertDate);
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(SendClientSupportRequestMessageNotificationEmailMessage message, ConsumeContext<SendClientSupportRequestMessageNotificationEmailMessage> consumeContext)
        {
            this.ClientSupportRequestMessageNotification(message.VisitPayUserId, message.ClientSupportRequestDisplayId, message.InsertDate);
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(SendClientSupportRequestConfirmationEmailMessage message, ConsumeContext<SendClientSupportRequestConfirmationEmailMessage> consumeContext)
        {
            this.ClientSupportRequestConfirmation(message.VisitPayUserId, message.ClientSupportRequestDisplayId);
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(SendClientSupportRequestAdminNotificationEmailMessage message, ConsumeContext<SendClientSupportRequestAdminNotificationEmailMessage> consumeContext)
        {
            this.ClientSupportRequestAdminNotification(message.ClientSupportRequestDisplayId);
        }

        public bool IsMessageValid(SendClientSupportRequestStatusNotificationEmailMessage message, ConsumeContext<SendClientSupportRequestStatusNotificationEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        public bool IsMessageValid(SendClientSupportRequestMessageNotificationEmailMessage message, ConsumeContext<SendClientSupportRequestMessageNotificationEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        public bool IsMessageValid(SendClientSupportRequestMessageAdminNotificationEmailMessage message, ConsumeContext<SendClientSupportRequestMessageAdminNotificationEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        public bool IsMessageValid(SendClientSupportRequestConfirmationEmailMessage message, ConsumeContext<SendClientSupportRequestConfirmationEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        public bool IsMessageValid(SendClientSupportRequestAdminNotificationEmailMessage message, ConsumeContext<SendClientSupportRequestAdminNotificationEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion
    }
}
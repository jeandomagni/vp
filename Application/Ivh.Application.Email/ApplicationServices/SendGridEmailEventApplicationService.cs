﻿namespace Ivh.Application.Email.ApplicationServices
{
    using System;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.Email.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Messages.Communication;
    using MassTransit;
    using Common.Utilities;

    public class SendGridEmailEventApplicationService : ApplicationService, ISendGridEmailEventApplicationService
    {
        private readonly IEmailService _emailService;

        public SendGridEmailEventApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            IEmailService emailService) : base(applicationServiceCommonService)
        {
            this._emailService = emailService;
        }

        public void EmailEvent(SendGridEmailEventMessage message)
        {
            CommunicationEventDto communicationEvent = Mapper.Map<CommunicationEventDto>(message);
            communicationEvent.InsertedDate = DateTime.UtcNow;
            ConversionUtil.SetEventDateFromTimeStamp(communicationEvent);
            this._emailService.Event(communicationEvent);
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SendGridEmailEventMessage message, ConsumeContext<SendGridEmailEventMessage> consumeContext)
        {
            this.EmailEvent(message);
        }

        public bool IsMessageValid(SendGridEmailEventMessage message, ConsumeContext<SendGridEmailEventMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion
    }
}
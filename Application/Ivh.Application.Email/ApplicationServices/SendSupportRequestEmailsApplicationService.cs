namespace Ivh.Application.Email.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using Base.Common.Interfaces;
    using Common.Interfaces;
    using Core.Common.Dtos;
    using Domain.Email.Interfaces;
    using Domain.Guarantor.Interfaces;
    using Domain.User.Interfaces;
    using Internal.Dtos;
    using Internal.Interfaces;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Communication;
    using MassTransit;

    public class SendSupportRequestEmailsApplicationService : CommunicationApplicationServiceBase, ISendSupportRequestEmailsApplicationService
    {
        public SendSupportRequestEmailsApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<ICommunicationSender> communicationSender,
            Lazy<ICommunicationTemplateLoader> communicationTemplateLoader,
            Lazy<ICommunicationService> communicationService,
            Lazy<IEmailService> emailService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IVisitPayUserRepository> visitPayUserRepository,
            Lazy<IVisitPayUserService> visitPayUserService)
            : base(
                applicationServiceCommonService,
                communicationSender,
                communicationTemplateLoader,
                communicationService,
                emailService,
                guarantorService,
                visitPayUserRepository,
                visitPayUserService)
        {
        }

        public void SupportRequestConfirmation(int vpGuarantorId, string caseNumber)
        {
            ClientDto clientDto = this.GetClient();
            int supportRequestReviewDays = clientDto.SupportRequestReviewDays;
            string confirmationText = supportRequestReviewDays > 0 ? $" within {supportRequestReviewDays} business days" : string.Empty;

            this.SendCommunications(SendTo.Guarantor(vpGuarantorId), CommunicationTypeEnum.SupportRequestConfirmation, visitPayUser => new Dictionary<string, string>
            {
                {"[[Name]]", visitPayUser.FirstName},
                {"[[CaseNumber]]", caseNumber},
                {"[[BusinessDayText]]", $"A customer care request has been submitted and will be reviewed{confirmationText}."},
            });
        }

        public void SupportRequestUpdate(int vpGuarantorId, string caseNumber, string submittedDate, string modificationStatus)
        {
            this.SendCommunications(SendTo.Guarantor(vpGuarantorId), CommunicationTypeEnum.SupportRequestUpdate, visitPayUser => new Dictionary<string, string>
            {
                {"[[CaseNumber]]", caseNumber},
                {"[[ModificationStatus]]", modificationStatus},
                {"[[Name]]", visitPayUser.FirstName},
                {"[[SubmittedDate]]", submittedDate},
            });
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(SendSupportRequestUpdatedEmailMessage message, ConsumeContext<SendSupportRequestUpdatedEmailMessage> consumeContext)
        {
            this.SupportRequestUpdate(message.VpGuarantorId, message.SupportRequestDisplayId, message.InsertDate, message.ModificationStatus);
        }

        public bool IsMessageValid(SendSupportRequestUpdatedEmailMessage message, ConsumeContext<SendSupportRequestUpdatedEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(SendSupportRequestCreatedEmailMessage message, ConsumeContext<SendSupportRequestCreatedEmailMessage> consumeContext)
        {
            this.SupportRequestConfirmation(message.VpGuarantorId, message.SupportRequestDisplayId);
        }

        public bool IsMessageValid(SendSupportRequestCreatedEmailMessage message, ConsumeContext<SendSupportRequestCreatedEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion
    }
}
﻿namespace Ivh.Application.Email.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using Base.Common.Interfaces;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.Email.Interfaces;
    using Internal.Interfaces;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Communication;
    using MassTransit;

    public class SendGuestPayEmailsApplicationService : GuestPayEmailApplicationServiceBase, ISendGuestPayEmailsApplicationService
    {
        private readonly Lazy<IGuestPayEmailTemplateLoader> _guestPayEmailTemplateLoader;

        private const string GuarantorFirstNameReplacementToken = "[[GuarantorFirstName]]";
        private const string TransactionNumberReplacementToken = "[[TransactionNumber]]";

        public SendGuestPayEmailsApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IGuestPayEmailTemplateLoader> guestPayEmailTemplateLoader,
            Lazy<IEmailService> emailService,
            Lazy<ICommunicationSender> emailEmailSender)
            : base(applicationServiceCommonService, emailService, emailEmailSender)
        {
            this._guestPayEmailTemplateLoader = guestPayEmailTemplateLoader;
        }

        public void SendPaymentConfirmation(string toEmail, string locale, int sentToPaymentUnitId, string guarantorFirstName, string transactionId)
        {
            CommunicationDto communicationDto = this._guestPayEmailTemplateLoader.Value.CreateEmailFromTemplate(CommunicationTypeEnum.GuestPayPaymentConfirmation, this.GetClient(), sentToPaymentUnitId, toEmail, locale, new Dictionary<string, string>
            {
                {GuarantorFirstNameReplacementToken, guarantorFirstName},
                {TransactionNumberReplacementToken, transactionId},
            });

            this.SendEmail(communicationDto);
        }

        public void SendPaymentFailure(string toEmail, int sentToPaymentUnitId, string guarantorFirstName, string transactionId)
        {
            CommunicationDto communicationDto = this._guestPayEmailTemplateLoader.Value.CreateEmailFromTemplate(CommunicationTypeEnum.GuestPayPaymentFailure, this.GetClient(), sentToPaymentUnitId, toEmail, null, new Dictionary<string, string>
            {
                {GuarantorFirstNameReplacementToken, guarantorFirstName},
                {TransactionNumberReplacementToken, transactionId},
            });

            this.SendEmail(communicationDto);
        }

        public void SendPaymentReversalNotification(string toEmail, int sendToPaymentUnitId, string guarantorFirstName, string transactionId, PaymentTypeEnum paymentType)
        {
            IDictionary<string, string> replacementValues = new Dictionary<string, string>
            {
                {GuarantorFirstNameReplacementToken, guarantorFirstName},
                {TransactionNumberReplacementToken, transactionId},
            };

            switch (paymentType)
            {
                case PaymentTypeEnum.PartialRefund:
                    replacementValues.Add("[[PaymentReversalType1]]", "partially refunded");
                    replacementValues.Add("[[PaymentReversalType2]]", "partial refund");
                    break;
                case PaymentTypeEnum.Refund:
                    replacementValues.Add("[[PaymentReversalType1]]", "refunded");
                    replacementValues.Add("[[PaymentReversalType2]]", "refund");
                    break;
                case PaymentTypeEnum.Void:
                    replacementValues.Add("[[PaymentReversalType1]]", "voided");
                    replacementValues.Add("[[PaymentReversalType2]]", "void");
                    break;
                default:
                    throw new ArgumentException("This email is valid only for reversal payment types.", nameof(paymentType));
            }

            CommunicationDto communicationDto = this._guestPayEmailTemplateLoader.Value.CreateEmailFromTemplate(CommunicationTypeEnum.GuestPayPaymentReversalNotification, this.GetClient(), sendToPaymentUnitId, toEmail, null, replacementValues);

            this.SendEmail(communicationDto);
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(SendGuestPayPaymentReversalConfirmationEmailMessage message, ConsumeContext<SendGuestPayPaymentReversalConfirmationEmailMessage> consumeContext)
        {
            this.SendPaymentReversalNotification(
                message.EmailAddress,
                message.PaymentUnitId,
                message.GuarantorFirstName,
                message.TransactionId,
                message.PaymentType
            );
        }

        public bool IsMessageValid(SendGuestPayPaymentReversalConfirmationEmailMessage message, ConsumeContext<SendGuestPayPaymentReversalConfirmationEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SendGuestPayPaymentConfirmationEmailMessage message, ConsumeContext<SendGuestPayPaymentConfirmationEmailMessage> consumeContext)
        {
            this.SendPaymentConfirmation(
                message.EmailAddress,
                message.Locale,
                message.PaymentUnitId,
                message.GuarantorFirstName,
                message.TransactionId
            );
        }

        public bool IsMessageValid(SendGuestPayPaymentConfirmationEmailMessage message, ConsumeContext<SendGuestPayPaymentConfirmationEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SendGuestPayPaymentFailureEmailMessage message, ConsumeContext<SendGuestPayPaymentFailureEmailMessage> consumeContext)
        {
            this.SendPaymentFailure(
                message.EmailAddress,
                message.PaymentUnitId,
                message.GuarantorFirstName,
                message.TransactionId
            );
        }

        public bool IsMessageValid(SendGuestPayPaymentFailureEmailMessage message, ConsumeContext<SendGuestPayPaymentFailureEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion
    }
}
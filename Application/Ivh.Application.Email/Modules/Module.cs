﻿using Autofac;
using Ivh.Application.Email.ApplicationServices;
using Ivh.Application.Email.Common.Interfaces;
using Ivh.Application.Email.Internal.Implementations;
using Ivh.Application.Email.Internal.Interfaces;

namespace Ivh.Application.Email.Modules
{
    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            LoadExported(builder);
            LoadInternal(builder);
        }

        private static void LoadExported(ContainerBuilder builder)
        {
            builder.RegisterType<UserEmailApplicationService>().As<IUserEmailApplicationService>();
            builder.RegisterType<EmailApplicationService>().As<IEmailApplicationService>();
            builder.RegisterType<SendClientSupportRequestEmailsApplicationService>().As<ISendClientSupportRequestEmailsApplicationService>();
            builder.RegisterType<SendConsolidationEmailsApplicationService>().As<ISendConsolidationEmailsApplicationService>();
            builder.RegisterType<SendFinancePlanEmailsApplicationService>().As<ISendFinancePlanEmailsApplicationService>();
            builder.RegisterType<SendGuestPayEmailsApplicationService>().As<ISendGuestPayEmailsApplicationService>();
            builder.RegisterType<SendPaymentEmailsApplicationService>().As<ISendPaymentEmailsApplicationService>();
            builder.RegisterType<SendStatementEmailsApplicationService>().As<ISendStatementEmailsApplicationService>();
            builder.RegisterType<SendSupportRequestEmailsApplicationService>().As<ISendSupportRequestEmailsApplicationService>();
            builder.RegisterType<SendUserAccountEmailsApplicationService>().As<ISendUserAccountEmailsApplicationService>();
            builder.RegisterType<SendVisitEmailApplicationService>().As<ISendVisitEmailApplicationService>();
            builder.RegisterType<SendGridEmailEventApplicationService>().As<ISendGridEmailEventApplicationService>();
            builder.RegisterType<ResendCommunicationApplicationService>().As<IResendCommunicationApplicationService>();
            builder.RegisterType<CommunicationApplicationService>().As<ICommunicationApplicationService>();
            builder.RegisterType<TwilioSmsEventApplicationService>().As<ITwilioSmsEventApplicationService>();
            builder.RegisterType<CommunicationExportApplicationService>().As<ICommunicationExportApplicationService>();
        }

        private static void LoadInternal(ContainerBuilder builder)
        {
            builder.RegisterType<CommunicationSender>().As<ICommunicationSender>();
            builder.RegisterType<CommunicationTemplateLoader>().As<ICommunicationTemplateLoader>();
            builder.RegisterType<CommunicationTemplateLoader>().As<IGuestPayEmailTemplateLoader>();
        }
    }
}

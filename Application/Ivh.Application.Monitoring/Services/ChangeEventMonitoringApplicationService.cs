﻿namespace Ivh.Application.Monitoring.Services
{
    using System;
    using System.Threading.Tasks;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Ivh.Common.Base.Utilities.Helpers;

    public class ChangeEventMonitoringApplicationService : ApplicationService, IChangeEventMonitoringApplicationService
    {
        private readonly Lazy<IMonitoringApplicationService> _monitoringApplicationService;

        public ChangeEventMonitoringApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IMonitoringApplicationService> monitoringApplicationService) : base(applicationServiceCommonService)
        {
            this._monitoringApplicationService = monitoringApplicationService;
        }

        public async Task SendChangeEventMonitors()
        {
            await Task.Run(() =>
            {
                try
                {
                    this.LoggingService.Value.Info(() => "ChangeEventMonitoringApplicationService::SendChangeEventMonitors::Start");
                    //this._monitoringApplicationService.Value.GetMonitor(MonitorEnum.QueueChangeEventsBeforeMonitor, DateTime.UtcNow, DateTime.UtcNow, true);
                    //this._monitoringApplicationService.Value.GetMonitor(MonitorEnum.QueueChangeEventsAfterMonitor, DateTime.UtcNow, DateTime.UtcNow, true);
                    //this._monitoringApplicationService.Value.GetMonitor(MonitorEnum.VisitStageTruncInsertAfterMonitor, DateTime.UtcNow, DateTime.UtcNow, true);
                    //this._monitoringApplicationService.Value.GetMonitor(MonitorEnum.GuarantorStageTruncInsertAfterMonitor, DateTime.UtcNow, DateTime.UtcNow, true);
                    //this._monitoringApplicationService.Value.GetMonitor(MonitorEnum.VisitTransactionStageTruncInsertAfterMonitor, DateTime.UtcNow, DateTime.UtcNow, true);
                    //this._monitoringApplicationService.Value.GetMonitor(MonitorEnum.AccountSnapshotandHistMonitor, DateTime.UtcNow, DateTime.UtcNow, true);
                    //this._monitoringApplicationService.Value.GetMonitor(MonitorEnum.GuarantorSnapshotAndHistMonitor, DateTime.UtcNow, DateTime.UtcNow, true);
                    //this._monitoringApplicationService.Value.GetMonitor(MonitorEnum.TransactionSnapshotandHistMonitor, DateTime.UtcNow, DateTime.UtcNow, true);
                    //this._monitoringApplicationService.Value.GetMonitor(MonitorEnum.QueueVisitChangeSetMonitor, DateTime.UtcNow, DateTime.UtcNow, true);
                    //this._monitoringApplicationService.Value.GetMonitor(MonitorEnum.PlacementActualVsExpectedMonitor, DateTime.UtcNow, DateTime.UtcNow, true);

                    this.LoggingService.Value.Info(() => "ChangeEventMonitoringApplicationService::SendChangeEventMonitors::End");
                }
                catch (Exception e)
                {
                    this.LoggingService.Value.Fatal(() => $"ChangeEventMonitoringApplicationService::SendChangeEventMonitors - exception = {ExceptionHelper.AggregateExceptionToString(e)}");
                    throw;
                }
            });
        }
    }
}
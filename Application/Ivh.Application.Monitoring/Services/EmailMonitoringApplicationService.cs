﻿namespace Ivh.Application.Monitoring.Services
{
    using System;
    using System.Threading.Tasks;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.Logging.Interfaces;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.VisitPay.Enums;

    public class EmailMonitoringApplicationService : ApplicationService, IEmailMonitoringApplicationService
    {
        private readonly Lazy<IMonitoringApplicationService> _monitoringApplicationService;

        public EmailMonitoringApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IMonitoringApplicationService> monitoringApplicationService) : base(applicationServiceCommonService)
        {
            this._monitoringApplicationService = monitoringApplicationService;
        }

        public async Task SendEmailPastDueExpectedVsActual()
        {
            await Task.Run(() =>
            {
                try
                {
                    this.LoggingService.Value.Info(() => "EmailMonitoringApplicationService::SendEmailPastDueExpectedVsActual::Start");
                    this._monitoringApplicationService.Value.GetMonitor(MonitorEnum.EmailPastDueExpectedVsActualMonitor, DateTime.UtcNow, DateTime.UtcNow, true);
                    this.LoggingService.Value.Info(() => "EmailMonitoringApplicationService::SendEmailPastDueExpectedVsActual::End");
                }
                catch (Exception e)
                {
                    this.LoggingService.Value.Fatal(() => $"EmailMonitoringApplicationService::SendEmailPastDueExpectedVsActual - exception = {ExceptionHelper.AggregateExceptionToString(e)}");
                    throw;
                }
            });
        }

        public async Task SendEmailFinalPastDueExpectedVsActual()
        {
            await Task.Run(() =>
            {
                try
                {
                    this.LoggingService.Value.Info(() => "EmailMonitoringApplicationService::SendEmailFinalPastDueExpectedVsActual::Start");
                    this._monitoringApplicationService.Value.GetMonitor(MonitorEnum.EmailFinalPastDueExpectedVsActualMonitor, DateTime.UtcNow, DateTime.UtcNow, true);
                    this.LoggingService.Value.Info(() => "EmailMonitoringApplicationService::SendEmailFinalPastDueExpectedVsActual::End");
                }
                catch (Exception e)
                {
                    this.LoggingService.Value.Fatal(() => $"EmailMonitoringApplicationService::SendEmailPastDueExpectedVsActual - exception = {ExceptionHelper.AggregateExceptionToString(e)}");
                    throw;
                }
            });
        }

        public async Task SendEmailPaymentConfirmationExpectedVsActual()
        {
            await Task.Run(() =>
            {
                try
                {
                    this.LoggingService.Value.Info(() => "EmailMonitoringApplicationService::SendEmailPaymentConfirmationExpectedVsActual::Start");
                    this._monitoringApplicationService.Value.GetMonitor(MonitorEnum.EmailPaymentConfirmationExpectedVsActualMonitor, DateTime.UtcNow, DateTime.UtcNow, true);
                    this.LoggingService.Value.Info(() => "EmailMonitoringApplicationService::SendEmailPaymentConfirmationExpectedVsActual::End");
                }
                catch (Exception e)
                {
                    this.LoggingService.Value.Fatal(() => $"EmailMonitoringApplicationService::SendEmailPaymentConfirmationExpectedVsActual - exception = {ExceptionHelper.AggregateExceptionToString(e)}");
                    throw;
                }
            });
        }

        public async Task SendEmailPaymentDueDateChangeExpectedVsActual()
        {
            await Task.Run(() =>
            {
                try
                {
                    this.LoggingService.Value.Info(() => "EmailMonitoringApplicationService::SendEmailPaymentDueDateChangeExpectedVsActual::Start");
                    this._monitoringApplicationService.Value.GetMonitor(MonitorEnum.EmailPaymentDueDateChangeExpectedVsActualMonitor, DateTime.UtcNow, DateTime.UtcNow, true);
                    this.LoggingService.Value.Info(() => "EmailMonitoringApplicationService::SendEmailPaymentDueDateChangeExpectedVsActual::End");
                }
                catch (Exception e)
                {
                    this.LoggingService.Value.Fatal(() => $"EmailMonitoringApplicationService::SendEmailPaymentDueDateChangeExpectedVsActual - exception = {ExceptionHelper.AggregateExceptionToString(e)}");
                    throw;
                }
            });
        }

        public async Task SendEmailStatementNotificationExpectedVsActual()
        {
            await Task.Run(() =>
            {
                try
                {
                    this.LoggingService.Value.Info(() => "EmailMonitoringApplicationService::SendEmailStatementNotificationExpectedVsActual::Start");
                    this._monitoringApplicationService.Value.GetMonitor(MonitorEnum.EmailStatementNotificationExpectedVsActualMonitor, DateTime.UtcNow, DateTime.UtcNow, true);
                    this.LoggingService.Value.Info(() => "EmailMonitoringApplicationService::SendEmailStatementNotificationExpectedVsActual::End");
                }
                catch (Exception e)
                {
                    this.LoggingService.Value.Fatal(() => $"EmailMonitoringApplicationService::SendEmailStatementNotificationExpectedVsActual - exception = {ExceptionHelper.AggregateExceptionToString(e)}");
                    throw;
                }
            });
        }

        public async Task SendEmailUncollectableExpectedVsActual()
        {
            await Task.Run(() =>
            {
                try
                {
                    this.LoggingService.Value.Info(() => "EmailMonitoringApplicationService::SendEmailUncollectableExpectedVsActual::Start");
                    this._monitoringApplicationService.Value.GetMonitor(MonitorEnum.EmailUncollectableExpectedVsActualMonitor, DateTime.UtcNow, DateTime.UtcNow, true);
                    this.LoggingService.Value.Info(() => "EmailMonitoringApplicationService::SendEmailUncollectableExpectedVsActual::End");
                }
                catch (Exception e)
                {
                    this.LoggingService.Value.Fatal(() => $"EmailMonitoringApplicationService::SendEmailUncollectableExpectedVsActual - exception = {ExceptionHelper.AggregateExceptionToString(e)}");
                    throw;
                }
            });
        }

        public async Task SendEmailFailedPaymentsExpectedVsActual()
        {
            await Task.Run(() =>
            {
                try
                {
                    this.LoggingService.Value.Info(() => "EmailMonitoringApplicationService::SendEmailUncollectableExpectedVsActual::Start");
                    this._monitoringApplicationService.Value.GetMonitor(MonitorEnum.EmailPaymentFailuresExpectedVsActualMonitor, DateTime.UtcNow, DateTime.UtcNow, true);
                    this.LoggingService.Value.Info(() => "EmailMonitoringApplicationService::SendEmailUncollectableExpectedVsActual::End");
                }
                catch (Exception e)
                {
                    this.LoggingService.Value.Fatal(() => $"EmailMonitoringApplicationService::SendEmailUncollectableExpectedVsActual - exception = {ExceptionHelper.AggregateExceptionToString(e)}");
                    throw;
                }
            });
        }

        public async Task SendEmailAccountCancellation()
        {
            await Task.Run(() =>
            {
                try
                {
                    this.LoggingService.Value.Info(() => "EmailMonitoringApplicationService::EmailAccountCancellationMonitor::Start");
                    this._monitoringApplicationService.Value.GetMonitor(MonitorEnum.EmailAccountCancellationMonitor, DateTime.UtcNow, DateTime.UtcNow, true);
                    this.LoggingService.Value.Info(() => "EmailMonitoringApplicationService::EmailAccountCancellationMonitor::End");
                }
                catch (Exception e)
                {
                    this.LoggingService.Value.Fatal(() => $"EmailMonitoringApplicationService::EmailAccountCancellationMonitor - exception = {ExceptionHelper.AggregateExceptionToString(e)}");
                    throw;
                }
            });
        }


        public async Task SendEmailFinancePlanDurationIncrease()
        {
            await Task.Run(() =>
            {
                try
                {
                    this.LoggingService.Value.Info(() => "EmailMonitoringApplicationService::SendEmailFinancePlanDurationIncrease::Start");
                    this._monitoringApplicationService.Value.GetMonitor(MonitorEnum.EmailFinancePlanDurationIncreaseMonitor, DateTime.UtcNow, DateTime.UtcNow, true);
                    this.LoggingService.Value.Info(() => "EmailMonitoringApplicationService::SendEmailFinancePlanDurationIncrease::End");
                }
                catch (Exception e)
                {
                    this.LoggingService.Value.Fatal(() => $"EmailMonitoringApplicationService::SendEmailFinancePlanDurationIncrease - exception = {ExceptionHelper.AggregateExceptionToString(e)}");
                    throw;
                }
            });
        }
    }
}
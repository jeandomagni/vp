﻿namespace Ivh.Application.Monitoring.Services
{
    using System;
    using System.Threading.Tasks;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.Logging.Interfaces;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.VisitPay.Enums;

    public class ClientDataIntegrationMonitoringApplicationService : ApplicationService, IClientDataIntegrationMonitoringApplicationService
    {
        private readonly Lazy<IMonitoringApplicationService> _monitoringApplicationService;

        public ClientDataIntegrationMonitoringApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IMonitoringApplicationService> monitoringApplicationService) : base(applicationServiceCommonService)
        {
            this._monitoringApplicationService = monitoringApplicationService;
        }

       public async Task SendPlacementActualVsExpected()
       {
           await Task.Run(() =>
           {
               try
               {
                   this.LoggingService.Value.Info(() => "EmailMonitoringApplicationService::PlacementActualVsExpectedMonitor::Start");
                   this._monitoringApplicationService.Value.GetMonitor(MonitorEnum.PlacementActualVsExpectedMonitor, DateTime.UtcNow, DateTime.UtcNow, true);
                   this.LoggingService.Value.Info(() => "EmailMonitoringApplicationService::PlacementActualVsExpectedMonitor::End");
               }
               catch (Exception e)
               {
                   this.LoggingService.Value.Fatal(() => $"EmailMonitoringApplicationService::PlacementActualVsExpectedMonitor - exception = {ExceptionHelper.AggregateExceptionToString(e)}");
                   throw;
               }
           });
       }
    }
}
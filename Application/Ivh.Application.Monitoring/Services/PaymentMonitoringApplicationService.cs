﻿namespace Ivh.Application.Monitoring.Services
{
    using System;
    using System.Threading.Tasks;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.Logging.Interfaces;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.VisitPay.Enums;

    public class PaymentMonitoringApplicationService : ApplicationService, IPaymentMonitoringApplicationService
    {
        private readonly Lazy<IMonitoringApplicationService> _monitoringApplicationService;

        public PaymentMonitoringApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IMonitoringApplicationService> monitoringApplicationService) : base(applicationServiceCommonService)
        {
            this._monitoringApplicationService = monitoringApplicationService;
        }

        public async Task SendPaymentActualVsExpected()
        {
            await Task.Run(() =>
            {
                try
                {
                    this.LoggingService.Value.Info(() => "PaymentMonitoringApplicationService::SendPaymentActualVsExpected::Start");
                    this._monitoringApplicationService.Value.GetMonitor(MonitorEnum.PaymentActualVsExpectedMonitor, DateTime.UtcNow, DateTime.UtcNow, true);
                    this._monitoringApplicationService.Value.GetMonitor(MonitorEnum.QueueScheduledPaymentsByGuarantorJobRunnerActualVsExpectedMonitor, DateTime.UtcNow, DateTime.UtcNow, true);
                    this._monitoringApplicationService.Value.GetMonitor(MonitorEnum.PaymentAllocationMonitor, DateTime.UtcNow, DateTime.UtcNow, true);
                    this.LoggingService.Value.Info(() => "PaymentMonitoringApplicationService::SendPaymentActualVsExpected::End");
                }
                catch (Exception e)
                {
                    this.LoggingService.Value.Fatal(() => $"PaymentMonitoringApplicationService::SendPaymentActualVsExpected - exception = {ExceptionHelper.AggregateExceptionToString(e)}");
                    throw;
                }
            });
        }


        public async Task SendACHClosedFailedActualVsExpected()
        {
            await Task.Run(() =>
            {
                try
                {
                    this.LoggingService.Value.Info(() => "PaymentMonitoringApplicationService::SendACHClosedFailedActualVsExpected::Start");
                    this._monitoringApplicationService.Value.GetMonitor(MonitorEnum.ACHClosedFailedExpectedVsActualMonitor, DateTime.UtcNow, DateTime.UtcNow, true);
                    this.LoggingService.Value.Info(() => "PaymentMonitoringApplicationService::SendACHClosedFailedActualVsExpected::End");
                }
                catch (Exception e)
                {
                    this.LoggingService.Value.Fatal(() => $"PaymentMonitoringApplicationService::SendACHClosedFailedActualVsExpected - exception = {ExceptionHelper.AggregateExceptionToString(e)}");
                    throw;
                }
            });
        }

        public async Task SendACHClosedPaidActualVsExpected()
        {
            await Task.Run(() =>
            {
                try
                {
                    this.LoggingService.Value.Info(() => "PaymentMonitoringApplicationService::SendACHClosedPaidActualVsExpected::Start");
                    this._monitoringApplicationService.Value.GetMonitor(MonitorEnum.ACHClosedPaidExpectedVsActualMonitor, DateTime.UtcNow, DateTime.UtcNow, true);
                    this.LoggingService.Value.Info(() => "PaymentMonitoringApplicationService::SendACHClosedPaidActualVsExpected::End");
                }
                catch (Exception e)
                {
                    this.LoggingService.Value.Fatal(() => $"PaymentMonitoringApplicationService::SendACHClosedPaidActualVsExpected - exception = {ExceptionHelper.AggregateExceptionToString(e)}");
                    throw;
                }
            });
        }

        public async Task SendACHReturnReportCount()
        {
            await Task.Run(() =>
            {
                try
                {
                    this.LoggingService.Value.Info(() => "PaymentMonitoringApplicationService::SendACHReturnReportCount::Start");
                    this._monitoringApplicationService.Value.GetMonitor(MonitorEnum.ACHReturnReportCountMonitor, DateTime.UtcNow, DateTime.UtcNow, true);
                    this.LoggingService.Value.Info(() => "PaymentMonitoringApplicationService::SendACHReturnReportCount::End");
                }
                catch (Exception e)
                {
                    this.LoggingService.Value.Fatal(() => $"PaymentMonitoringApplicationService::SendACHReturnReportCount - exception = {ExceptionHelper.AggregateExceptionToString(e)}");
                    throw;
                }
            });
        }

        public async Task SendACHSettlementReportCount()
        {
            await Task.Run(() =>
            {
                try
                {
                    this.LoggingService.Value.Info(() => "PaymentMonitoringApplicationService::SendACHSettlementReportCount::Start");
                    this._monitoringApplicationService.Value.GetMonitor(MonitorEnum.ACHSettlementReportCountMonitor, DateTime.UtcNow, DateTime.UtcNow, true);
                    this.LoggingService.Value.Info(() => "PaymentMonitoringApplicationService::SendACHSettlementReportCount::End");
                }
                catch (Exception e)
                {
                    this.LoggingService.Value.Fatal(() => $"PaymentMonitoringApplicationService::SendACHSettlementReportCount - exception = {ExceptionHelper.AggregateExceptionToString(e)}");
                    throw;
                }
            });
        }
    }
}
﻿namespace Ivh.Application.Monitoring.Services
{
    using System;
    using System.Threading.Tasks;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.Logging.Interfaces;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.VisitPay.Enums;

    public class UncollectableMonitoringApplicationService : ApplicationService, IUncollectableMonitoringApplicationService
    {
        private readonly Lazy<ILoggingService> _logger;
        private readonly Lazy<IMonitoringApplicationService> _monitoringApplicationService;

        public UncollectableMonitoringApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<ILoggingService> logger, 
            Lazy<IMonitoringApplicationService> monitoringApplicationService) : base(applicationServiceCommonService)
        {
            this._logger = logger;
            this._monitoringApplicationService = monitoringApplicationService;
        }

        public async Task SendUncollectableActiveActualVsExpected()
        {
            await Task.Run(() =>
            {
                try
                {
                    this._logger.Value.Info(() => "UncollectableMonitoringApplicationService::SendUncollectableActiveActualVsExpected::Start");
                    this._monitoringApplicationService.Value.GetMonitor(MonitorEnum.UncollectableActiveActualVsExpectedMonitor, DateTime.UtcNow, DateTime.UtcNow, true);
                    this._logger.Value.Info(() => "UncollectableMonitoringApplicationService::SendUncollectableActiveActualVsExpected::End");
                }
                catch (Exception e)
                {
                    this._logger.Value.Fatal(() => $"UncollectableMonitoringApplicationService::SendUncollectableActiveActualVsExpected - exception = {ExceptionHelper.AggregateExceptionToString(e)}");
                    throw;
                }
            });
        }
        public async Task SendUncollectableClosedActualVsExpected()
        {
            await Task.Run(() =>
            {
                try
                {
                    this._logger.Value.Info(() => "UncollectableMonitoringApplicationService::SendUncollectableClosedActualVsExpected::Start");
                    this._monitoringApplicationService.Value.GetMonitor(MonitorEnum.UncollectableClosedActualVsExpectedMonitor, DateTime.UtcNow, DateTime.UtcNow, true);
                    this._logger.Value.Info(() => "UncollectableMonitoringApplicationService::SendUncollectableClosedActualVsExpected::End");
                }
                catch (Exception e)
                {
                    this._logger.Value.Fatal(() => $"UncollectableMonitoringApplicationService::SendUncollectableClosedActualVsExpected - exception = {ExceptionHelper.AggregateExceptionToString(e)}");
                    throw;
                }
            });
        }
        public async Task SendClosedUncollectableFinancePlans()
        {
            await Task.Run(() =>
            {
                try
                {
                    this._logger.Value.Info(() => "UncollectableMonitoringApplicationService::SendClosedUncollectableFinancePlans::Start");
                    this._monitoringApplicationService.Value.GetMonitor(MonitorEnum.ClosedUncollectableFinancePlansMonitor, DateTime.UtcNow, DateTime.UtcNow, true);
                    this._logger.Value.Info(() => "UncollectableMonitoringApplicationService::SendClosedUncollectableFinancePlans::End");
                }
                catch (Exception e)
                {
                    this._logger.Value.Fatal(() => $"UncollectableMonitoringApplicationService::SendClosedUncollectableFinancePlans - exception = {ExceptionHelper.AggregateExceptionToString(e)}");
                    throw;
                }
            });
        }
    }
}
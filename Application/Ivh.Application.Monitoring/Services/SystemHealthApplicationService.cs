﻿namespace Ivh.Application.Monitoring.Services
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Net.Http;
    using System.Threading.Tasks;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.Content.Interfaces;
    using Domain.Settings.Enums;
    using Domain.Settings.Interfaces;
    using Ivh.Common.Api.Client;
    using Ivh.Common.Api.Helpers;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Cache;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Monitoring.SystemHealth;
    using Ivh.Common.VisitPay.Messages.Monitoring.SystemHealth.ProcessorHealth;
    using Ivh.Common.Web.HtmlHelpers;
    using MassTransit;
    using Newtonsoft.Json;

    public class SystemHealthApplicationService : ApplicationService, ISystemHealthApplicationService
    {
        private const string ClientDataUrl = "/api/Monitoring";
        private readonly Lazy<IMemoryCache> _memoryCache;
        private readonly Lazy<IConnectionStringService> _connectionStringService;
        private readonly Lazy<ISettingsProvider> _settingsProvider;
        private readonly Lazy<IContentProvider> _contentProvider;

        public SystemHealthApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IMemoryCache> memoryCache,
            Lazy<IConnectionStringService> connectionStringService,
            Lazy<ISettingsProvider> settingsProvider,
            Lazy<IContentProvider> contentProvider) : base(applicationServiceCommonService)
        {
            this._memoryCache = memoryCache;
            this._connectionStringService = connectionStringService;
            this._settingsProvider = settingsProvider;
            this._contentProvider = contentProvider;
        }

        public SystemHealthDetailsDto GetSystemHealthDetails()
        {
            bool Safe(Func<bool> f)
            {
                try
                {
                    Task<bool> task = Task.Run<bool>(f);
                    if (Task.WhenAny(task, Task.Delay(15000)).Result == task)
                    {
                        return task.Result;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch
                {
                    return false;
                }
            }

            bool PingProcessorPart1(ApplicationEnum applicationEnum, Guid roundTripToken)
            {
                if (this.Cache.Value.GetLock(roundTripToken.ToString(), 3, 500))
                {
                    this.Bus.Value.PublishMessage(new ProcessorHealthMessage() { RoundTripToken = roundTripToken, Application = applicationEnum });
                }
                //false until part 2 returns true
                return false;
            }

            bool PingProcessorPart2(Guid roundTripToken)
            {
                if (this.Cache.Value.GetLock(roundTripToken.ToString(), 3, 5000))
                {
                    this.Cache.Value.ReleaseLock(roundTripToken.ToString());
                    return true;
                }
                return false;
            }

            SystemInfoDetailsDto systemInfoDetailsDto = this.GetSystemInfoDetails();
            SystemHealthDetailsDto systemHealthDetailsDto = Mapper.Map<SystemHealthDetailsDto>(systemInfoDetailsDto);
            // Get the processor pings off early to give them a head start
            Guid universalProcessorRoundTripToken = Guid.NewGuid();
            systemHealthDetailsDto.UniversalProcessor = Safe(() => PingProcessorPart1(ApplicationEnum.UniversalProcessor, universalProcessorRoundTripToken));
            Guid scoringProcessorRoundTripToken = Guid.NewGuid();
            systemHealthDetailsDto.ScoringProcessor = Safe(() => PingProcessorPart1(ApplicationEnum.ScoringProcessor, scoringProcessorRoundTripToken));

            bool PingDb(Lazy<string> connectionString)
            {
                using (SqlConnection connection = new SqlConnection(connectionString.Value))
                {
                    try
                    {
                        connection.Open();
                        using (SqlCommand command = new SqlCommand("exec sp_executesql N'select 1'", connection))
                        {
                            int result = (int)command.ExecuteScalar();
                            return result == 1;
                        }
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            systemHealthDetailsDto.ReleaseBuild = !this.GetType().Assembly.IsAssemblyDebugBuild();
            systemHealthDetailsDto.Queue = Safe(() =>
            {
                try
                {
                    this.Bus.Value.PublishMessage(new SystemHealthCheckTestMessage()).Wait(5000);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            });
            // this needs to be deprecated
            systemHealthDetailsDto.MemoryCache = Safe(() =>
            {
                try
                {
                    string tempKey = Guid.NewGuid().ToString();
                    this._memoryCache.Value.SetStringAsync(tempKey, tempKey).Wait(5000);
                    return this._memoryCache.Value.GetStringAsync(tempKey).Result == tempKey;
                }
                catch (Exception)
                {
                    return false;
                }
            });
            systemHealthDetailsDto.DistributedCache = Safe(() =>
            {
                try
                {
                    string tempKey = Guid.NewGuid().ToString();
                    this.Cache.Value.SetStringAsync(tempKey, tempKey).Wait(5000);
                    return this.Cache.Value.GetStringAsync(tempKey).Result == tempKey;
                }
                catch (Exception)
                {
                    return false;
                }
            });
            systemHealthDetailsDto.ApplicationConnect = Safe(() => PingDb(this._connectionStringService.Value.ApplicationConnection));
            systemHealthDetailsDto.GuestPayConnect = Safe(() => PingDb(this._connectionStringService.Value.GuestPayConnection));
            systemHealthDetailsDto.LogConnect = Safe(() => PingDb(this._connectionStringService.Value.LoggingConnection));
            systemHealthDetailsDto.StorageConnect = Safe(() => PingDb(this._connectionStringService.Value.StorageConnection));
            systemHealthDetailsDto.QuartzConnect = Safe(() => PingDb(this._connectionStringService.Value.QuartzConnection));
            systemHealthDetailsDto.CdiEtlConnect = Safe(() => PingDb(this._connectionStringService.Value.CdiEtlConnection));
            systemHealthDetailsDto.Settings = Safe(() => this._settingsProvider.Value.GetAllSettings().Any());
            systemHealthDetailsDto.Content = Safe(() => this._contentProvider.Value.GetVersionAsync(CmsRegionEnum.PasswordResetConfirmation).Result != null);
            systemHealthDetailsDto.ClientReportsServer = Safe(() =>
            {
                return false; // this does not work yet

                //HttpRequestMessage message = new HttpRequestMessage(
                //    new HttpMethod("HEAD"),
                //    this._applicationSettingsService.Value.SsrsReportServerUri.Value);

                //HttpResponseMessage httpResponseMessage = HttpClient.SendAsync(message).Result;
                //return httpResponseMessage.IsSuccessStatusCode
                //        || (httpResponseMessage.StatusCode == HttpStatusCode.Unauthorized)
                //        || (httpResponseMessage.StatusCode == HttpStatusCode.Forbidden);
            });
            systemHealthDetailsDto.UniversalProcessor = Safe(() => PingProcessorPart2(universalProcessorRoundTripToken));
            systemHealthDetailsDto.ScoringProcessor = Safe(() => PingProcessorPart2(scoringProcessorRoundTripToken));

            string guestPayUrl = this.ApplicationSettingsService.Value.GetUrl(UrlEnum.GuestPayUrl).AbsoluteUri;
            Guid authorizationKey = this.ApplicationSettingsService.Value.PingAuthorizationKey.Value;
            IDictionary<string, string> guestPayApiResponse = null;
            systemHealthDetailsDto.GuestPayApi = Safe(() => this.PingSwaggerApi(guestPayUrl, authorizationKey, "GuestPayApi", out guestPayApiResponse));
            systemHealthDetailsDto.GuestPayApiResponse = guestPayApiResponse;

            IDictionary<string, string> paymentApiResponse = null;
            string paymentApiUrl = this.ApplicationSettingsService.Value.GetUrl(UrlEnum.PaymentApiUrl).AbsoluteUri;
            systemHealthDetailsDto.PaymentApi = Safe(() => this.PingSwaggerApi(paymentApiUrl, authorizationKey, "PaymentApi", out paymentApiResponse));
            systemHealthDetailsDto.PaymentApiResponse = paymentApiResponse;

            IDictionary<string, string> securePanApiResponse = null;
            string securePanApiUrl = this.ApplicationSettingsService.Value.GetUrl(UrlEnum.SecurePanApiUrl).AbsoluteUri;
            systemHealthDetailsDto.SecurePanApi = Safe(() => this.PingSwaggerApi(securePanApiUrl, authorizationKey, "SecurePanApi", out securePanApiResponse));
            systemHealthDetailsDto.SecurePanApiResponse = securePanApiResponse;
            
            return systemHealthDetailsDto;
        }

        public SystemInfoDetailsDto GetSystemInfoDetails()
        {
            return new SystemInfoDetailsDto()
            {
                Client = this.ApplicationSettingsService.Value.Client.Value,
                Environment = this.ApplicationSettingsService.Value.Environment.Value,
                Application = this.ApplicationSettingsService.Value.Application.Value.ToString(),
                AppVersion = $"{this.GetType().Assembly.GetAssemblyVersionAndDate()}",
                ClientDataApiVersion = this.GetAssemblyVersion(
                    this.ApplicationSettingsService.Value.GetUrl(UrlEnum.ClientDataApi).AbsoluteUri,
                    ClientDataUrl,
                    this.ApplicationSettingsService.Value.ClientDataApiAppId.Value,
                    this.ApplicationSettingsService.Value.ClientDataApiAppKey.Value,
                    "ClientDataApi"),
                SecurePanApiVersion = this.GetSecurePanAssemblyVersion(),
                ClientSettingsHash = this._settingsProvider.Value.GetManagedSettingsHash(),
                SecurePanUrl = this.ApplicationSettingsService.Value.GetUrl(UrlEnum.SecurePan).AbsoluteUri
            };
        }

        private bool PingSwaggerApi(string url, Guid authorizationKey, string appName, out IDictionary<string, string> response)
        {
            response = null;
            IDictionary<string, string> responseValues = null;
            if (this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeaturePaymentApiIsEnabled))
            {
                try
                {
                    
                    HttpClientHelper.CallApiWith<object, object>(
                        url,
                        string.Concat("ping?detail=true", $"&authorizationKey={authorizationKey}"),
                        HttpMethod.Get,
                        null,
                        (message, dto) =>
                        {
                            //Success
                            message.EnsureSuccessStatusCode();
                            string body = message.Content.ReadAsStringAsync().Result;
                            if (!string.IsNullOrEmpty(body))
                            {
                                responseValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(message.Content.ReadAsStringAsync().Result);
                            }
                        },
                        (message, s) => { this.LoggingService.Value.Fatal(() => $"SystemHealthApplicationService::PingSwaggerApi - Something went wrong while pinging {appName}.  StatusCode - {message.StatusCode}, ApiName - ping?detail=true"); },
                        () => HttpClientFactory.Create(new ApiHandler(this.ApplicationSettingsService.Value.ClientDataApiAppId.Value,
                            this.ApplicationSettingsService.Value.ClientDataApiAppKey.Value, appName))
                    );
                }
                catch (Exception e)
                {
                    this.LoggingService.Value.Fatal(() => $"SystemHealthApplicationService::PingSwaggerApi - Something went wrong while calling the API with the following exception: {e}");
                    return false;
                }
            }

            response = responseValues;
            return true;
        }

        private string GetAssemblyVersion(string baseUrl, string apiUrl, string appId, string appKey, string appName)
        {
            return HttpClientHelper.CallApiWith<string, object>(
                baseUrl,
                string.Concat(apiUrl, "/GetAssemblyVersion"),
                HttpMethod.Get,
                null,
                (message, dto) =>
                {
                    //Success
                },
                (message, s) =>
                {
                    this.LoggingService.Value.Fatal(() => $"SystemHealthApplicationService::GetAssemblyVersion - Something went wrong while calling the API.  StatusCode - {message.StatusCode}, ApiName - {appName}");
                },
                () => HttpClientFactory.Create(new ApiHandler(
                    appId,
                    appKey,
                    appName
                    )),
                false
            );
        }

        private string GetSecurePanAssemblyVersion()
        {
            string baseUrl = this.ApplicationSettingsService.Value.GetUrl(UrlEnum.SecurePan).AbsoluteUri;
            string requestUrl = SecurePanAuthorization.GenerateUrl(this.ApplicationSettingsService.Value);
            requestUrl = requestUrl.Insert(requestUrl.IndexOf(baseUrl) + baseUrl.Length, "pan/GetAssemblyVersion/");
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    HttpResponseMessage response = client.GetAsync(requestUrl).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        return response.Content.ReadAsStringAsync().Result;
                    }
                    else
                    {
                        this.LoggingService.Value.Fatal(() => "SystemHealthApplicationService::GetAssemblyVersion - Something went wrong while calling the API.  StatusCode - {0}, ApiName - {1}", response.StatusCode, "SecurePan");
                        return string.Empty;
                    }
                }
                catch (Exception e)
                {
                    this.LoggingService.Value.Fatal(() => "SystemHealthApplicationService::GetAssemblyVersion - Something went wrong while calling the API.  {0}, ApiName - {1}", e, "SecurePan");
                    return string.Empty;
                }
            }
        }

        public bool SystemIsHealthy()
        {
            return this.GetSystemHealthDetails().IsHealthy();
        }


        [UniversalConsumer(UniversalQueuesEnum.SystemManagementProcessor, UniversalPriorityEnum.Priority00)]
        public void ConsumeMessage(ProcessorHealthMessage message, ConsumeContext<ProcessorHealthMessage> consumeContext)
        {
            if (message.Application == ApplicationEnum.UniversalProcessor)
            {
                this.Cache.Value.ReleaseLock(message.RoundTripToken.ToString());
            }
        }

        public bool IsMessageValid(ProcessorHealthMessage message, ConsumeContext<ProcessorHealthMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.SystemManagementProcessor, UniversalPriorityEnum.Priority00)]
        public void ConsumeMessage(SystemHealthCheckTestMessage message, ConsumeContext<SystemHealthCheckTestMessage> consumeContext)
        {
            //EMPTY CONSUMER: Intended   
        }

        public bool IsMessageValid(SystemHealthCheckTestMessage message, ConsumeContext<SystemHealthCheckTestMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }
    }
}
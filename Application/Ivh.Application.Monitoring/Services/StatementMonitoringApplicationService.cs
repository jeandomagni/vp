﻿namespace Ivh.Application.Monitoring.Services
{
    using System;
    using System.Threading.Tasks;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.VisitPay.Enums;

    public class StatementMonitoringApplicationService : ApplicationService, IStatementMonitoringApplicationService
    {
        private readonly Lazy<IMonitoringApplicationService> _monitoringApplicationService;

        public StatementMonitoringApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IMonitoringApplicationService> monitoringApplicationService) : base(applicationServiceCommonService)
        {
            this._monitoringApplicationService = monitoringApplicationService;
        }

        public async Task SendStatementActualVsExpected()
        {
            await Task.Run(() =>
            {
                try
                {
                    this.LoggingService.Value.Info(() => "StatementMonitoringApplicationService::SendStatementActualVsExpected::Start");
                    this._monitoringApplicationService.Value.GetMonitor(MonitorEnum.StatementActualVsExpectedMonitor, DateTime.UtcNow, DateTime.UtcNow, true);
                    this.LoggingService.Value.Info(() => "StatementMonitoringApplicationService::SendStatementActualVsExpected::End");
                }
                catch (Exception e)
                {
                    this.LoggingService.Value.Fatal(() => $"StatementMonitoringApplicationService::SendStatementActualVsExpected - exception = {ExceptionHelper.AggregateExceptionToString(e)}");
                    throw;
                }
            });
        }

        public async Task SendStatementLineMismatchCount()
        {
            await Task.Run(() =>
            {
                try
                {
                    this.LoggingService.Value.Info(() => "StatementMonitoringApplicationService::SendStatementLineMismatchCount::Start");
                    this._monitoringApplicationService.Value.GetMonitor(MonitorEnum.StatementLineMismatchCountMonitor, DateTime.UtcNow, DateTime.UtcNow, true);
                    this.LoggingService.Value.Info(() => "StatementMonitoringApplicationService::SendStatementLineMismatchCount::End");
                }
                catch (Exception e)
                {
                    this.LoggingService.Value.Fatal(() => $"StatementMonitoringApplicationService::SendStatementLineMismatchCount - exception = {ExceptionHelper.AggregateExceptionToString(e)}");
                    throw;
                }
            });
        }

        public async Task SendStatementFPLineVsFPBalanceMismatchCount()
        {
            await Task.Run(() =>
            {
                try
                {
                    this.LoggingService.Value.Info(() => "StatementMonitoringApplicationService::StatementFPLineVsFPBalanceMismatchCount::Start");
                    this._monitoringApplicationService.Value.GetMonitor(MonitorEnum.StatementFPLineVsFPBalanceMismatchCountMonitor, DateTime.UtcNow, DateTime.UtcNow, true);
                    this.LoggingService.Value.Info(() => "StatementMonitoringApplicationService::StatementFPLineVsFPBalanceMismatchCount::End");
                }
                catch (Exception e)
                {
                    this.LoggingService.Value.Fatal(() => $"StatementMonitoringApplicationService::StatementFPLineVsFPBalanceMismatchCount - exception = {ExceptionHelper.AggregateExceptionToString(e)}");
                    throw;
                }
            });
        }
    }
}
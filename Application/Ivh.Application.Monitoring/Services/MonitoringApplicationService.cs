﻿namespace Ivh.Application.Monitoring.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.Logging.Interfaces;
    using Domain.Monitoring.Entities;
    using Domain.Monitoring.Exceptions;
    using Domain.Monitoring.Interfaces;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Jobs;

    public class MonitoringApplicationService : ApplicationService, IMonitoringApplicationService
    {
        private readonly Lazy<IMonitoringService> _monitoringService;
        private readonly Lazy<IMetricsProvider> _metricsProvider;

        public MonitoringApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IMonitoringService> monitoringService,
            Lazy<IMetricsProvider> metricsProvider) : base(applicationServiceCommonService)
        {
            this._monitoringService = monitoringService;
            this._metricsProvider = metricsProvider;
        }

        public IList<IMonitor> GetMonitors(IEnumerable<MonitorEnum> monitorEnums, DateTime dateTimeBegin, DateTime dateTimeEnd, bool publish = false)
        {
            IList<IMonitor> monitors = monitorEnums.Select(x => this._monitoringService.Value.GetMonitor(x, dateTimeBegin, dateTimeEnd)).ToList();

            if (monitors.IsNotNullOrEmpty())
            {
                foreach (IMonitor monitor in monitors.Where(x => x.Data.IsNotNullOrEmpty()))
                {
                    foreach (KeyValuePair<string, decimal> datum in monitor.Data)
                    {
                        if (publish)
                        {
                            this._metricsProvider.Value.Gauge(datum.Key, datum.Value);
                        }
                        this.LoggingService.Value.Info(() => "Monitor: {0} Key: {1} Value: {2}", monitor.Name, datum.Key, datum.Value);
                    }
                }
            }
            return monitors;
        }

        public IMonitor GetMonitor(MonitorEnum monitorEnum, DateTime dateTimeBegin, DateTime dateTimeEnd, bool publish = false)
        {
            IMonitor monitor = this._monitoringService.Value.GetMonitor(monitorEnum, dateTimeBegin, dateTimeEnd);
            if (monitor != null && monitor.Data.IsNotNullOrEmpty())
            {
                foreach (KeyValuePair<string, decimal> datum in monitor.Data)
                {
                    if (publish)
                    {
                        this._metricsProvider.Value.Gauge(datum.Key, datum.Value);
                    }
                    this.LoggingService.Value.Info(() => "Monitor: {0} Key: {1} Value: {2}", monitor.Name, datum.Key, datum.Value);
                }
            }
            return monitor;
        }

        public T GetDatum<T>(DatumEnum datumEnum, DateTime dateTimeBegin, DateTime dateTimeEnd, bool publish = false)
        {
            T datum = this._monitoringService.Value.GetDatum<T>(datumEnum, dateTimeBegin, dateTimeEnd);
            if (publish)
            {
                this._metricsProvider.Value.Gauge(datumEnum.ToString(), datum);
            }
            return datum;
        }

        public T GetDatum<T>(DatumEnum datumEnum, Func<T> value, bool publish = false)
        {
            T datum = value();
            if (publish)
            {
                this._metricsProvider.Value.Gauge(datumEnum.ToString(), datum);
            }
            return datum;
        }

        public IEnumerable<T> GetData<T>(IDictionary<DatumEnum, Func<T>> data, bool publish = false)
        {
            foreach (KeyValuePair<DatumEnum, Func<T>> datum in data)
            {
                yield return this.GetDatum<T>(datum.Key, datum.Value, publish);
            }
        }

        public IEnumerable<T> GetData<T>(IEnumerable<DatumEnum> datumEnum, DateTime dateTimeBegin, DateTime dateTimeEnd, bool publish = false)
        {
            foreach (DatumEnum datum in datumEnum)
            {
                yield return this.GetDatum<T>(datum, dateTimeBegin, dateTimeEnd, publish);
            }
        }

        public void PublishData(IEnumerable<DatumEnum> datumEnum, DateTime dateTimeBegin, DateTime dateTimeEnd, bool continueOnError = true)
        {
            foreach (DatumEnum datum in datumEnum)
            {
                try
                {
                    this.GetDatum<decimal>(datum, dateTimeBegin, dateTimeEnd, true);
                }
                catch (Exception ex)
                {
                    if (continueOnError)
                    {
                        this.LoggingService.Value.Warn(() =>  "Data Metric {0}: failed to publish. Exception message: {1}", datum.ToString(), ExceptionHelper.AggregateExceptionToString(ex));
                    }
                    else
                    {
                        throw;
                    }
                }
            }
        }

        void IJobRunnerService<PublishMonitorDataJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            if (args != null && args.Length > 1)
            {
                for (int i = 1; i < args.Length; i++)
                {
                    MonitorEnum monitorEnum;
                    if (Enum.TryParse(args[i], true, out monitorEnum))
                    {
                        IMonitor monitor = this.GetMonitor(monitorEnum, DateTime.UtcNow, DateTime.UtcNow, true);
                        if (!monitor.Success)
                        {
                            throw new MonitorFailureException(monitor);
                        }
                        Console.WriteLine("Successfully ran {0}", args[i]);
                    }
                }
            }
        }
    }
}

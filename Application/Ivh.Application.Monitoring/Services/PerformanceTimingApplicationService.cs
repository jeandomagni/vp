﻿namespace Ivh.Application.Monitoring.Services
{
    using System;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.Monitoring.Interfaces;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Messages.Monitoring;
    using MassTransit;

    public class PerformanceTimingApplicationService : ApplicationService, IPerformanceTimingApplicationService
    {
        private readonly IPerformanceTimingService _performanceTimingService;

        public PerformanceTimingApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            IPerformanceTimingService performanceTimingService) : base(applicationServiceCommonService)
        {
            this._performanceTimingService = performanceTimingService;
        }

        public void LogControllerTiming(string url, string requestIdenfitier, DateTime timeActionExecuting, DateTime timeActionExecuted, DateTime timeResultExecuted)
        {
            this._performanceTimingService.LogControllerTiming(url, requestIdenfitier, timeActionExecuting, timeActionExecuted, timeResultExecuted);
        }

        #region Message Consumers


        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(ControllerTimingMessage message, ConsumeContext<ControllerTimingMessage> consumeContext)
        {
            string requestIdentifier = RandomStringGeneratorUtility.GetRandomString(10);
            this._performanceTimingService.LogControllerTiming(message.Name, requestIdentifier, message.TimeActionExecuting, message.TimeActionExecuted, message.TimeResultExecuted);
        }

        public bool IsMessageValid(ControllerTimingMessage message, ConsumeContext<ControllerTimingMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(ControllerTimingMessages message, ConsumeContext<ControllerTimingMessages> consumeContext)
        {
            string requestIdentifier = RandomStringGeneratorUtility.GetRandomString(10);

            foreach (ControllerTimingMessage message1 in message.Messages)
            {
                this._performanceTimingService.LogControllerTiming(message1.Name, requestIdentifier, message1.TimeActionExecuting, message1.TimeActionExecuted, message1.TimeResultExecuted);
            };
        }

        public bool IsMessageValid(ControllerTimingMessages message, ConsumeContext<ControllerTimingMessages> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion
    }
}

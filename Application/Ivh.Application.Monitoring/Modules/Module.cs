﻿namespace Ivh.Application.Monitoring.Modules
{
    using Autofac;
    using Common.Interfaces;
    using Services;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<MonitoringApplicationService>().As<IMonitoringApplicationService>();
            builder.RegisterType<StatementMonitoringApplicationService>().As<IStatementMonitoringApplicationService>();
            builder.RegisterType<UncollectableMonitoringApplicationService>().As<IUncollectableMonitoringApplicationService>();
            builder.RegisterType<PaymentMonitoringApplicationService>().As<IPaymentMonitoringApplicationService>();
            builder.RegisterType<EmailMonitoringApplicationService>().As<IEmailMonitoringApplicationService>();
            builder.RegisterType<ClientDataIntegrationMonitoringApplicationService>().As<IClientDataIntegrationMonitoringApplicationService>();
            builder.RegisterType<ChangeEventMonitoringApplicationService>().As<IChangeEventMonitoringApplicationService>();
            builder.RegisterType<PerformanceTimingApplicationService>().As<IPerformanceTimingApplicationService>();
        }
    }
}
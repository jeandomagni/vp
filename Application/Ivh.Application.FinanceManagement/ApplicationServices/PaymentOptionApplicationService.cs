﻿namespace Ivh.Application.FinanceManagement.ApplicationServices
{
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Content.Common.Dtos;
    using Content.Common.Interfaces;
    using Core.Common.Dtos;
    using Core.Common.Interfaces;
    using Domain.AppIntelligence.Entities;
    using Domain.AppIntelligence.Interfaces;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Payment.Interfaces;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.Visit.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.Base.Utilities.Lookup;
    using Ivh.Common.VisitPay.Enums;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using User.Common.Dtos;

    public class PaymentOptionApplicationService : ApplicationService, IPaymentOptionApplicationService
    {
        private readonly Lazy<IPaymentOptionService> _paymentOptionService;
        private readonly Lazy<IContentApplicationService> _contentApplicationService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IRandomizedTestService> _randomizedTestService;
        private readonly Lazy<IVpStatementService> _statementService;
        private readonly Lazy<IConsolidationGuarantorApplicationService> _consolidationGuarantorApplicationService;
        private readonly Lazy<IFinancialDataSummaryApplicationService> _financialDataSummaryApplicationService;
        private readonly Lazy<IFinancePlanApplicationService> _financePlanApplicationService;
        private readonly Lazy<IPaymentSubmissionApplicationService> _paymentSubmissionApplicationService;
        private readonly Lazy<IVisitApplicationService> _visitApplicationService;
        private readonly Lazy<IVisitService> _visitService;
        private readonly Lazy<IGuarantorScoreService> _scoringService;

        public PaymentOptionApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IPaymentOptionService> paymentOptionService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IRandomizedTestService> randomizedTestService,
            Lazy<IVpStatementService> statementService,
            Lazy<IConsolidationGuarantorApplicationService> consolidationGuarantorApplicationService,
            Lazy<IFinancialDataSummaryApplicationService> financialDataSummaryApplicationService,
            Lazy<IFinancePlanApplicationService> financePlanApplicationService,
            Lazy<IPaymentSubmissionApplicationService> paymentSubmissionApplicationService,
            Lazy<IVisitApplicationService> visitApplicationService,
            Lazy<IVisitService> visitService,
            Lazy<IGuarantorScoreService> scoringService) : base(applicationServiceCommonService)
        {
            this._paymentOptionService = paymentOptionService;
            this._contentApplicationService = contentApplicationService;
            this._guarantorService = guarantorService;
            this._randomizedTestService = randomizedTestService;
            this._statementService = statementService;
            this._consolidationGuarantorApplicationService = consolidationGuarantorApplicationService;
            this._financialDataSummaryApplicationService = financialDataSummaryApplicationService;
            this._financePlanApplicationService = financePlanApplicationService;
            this._paymentSubmissionApplicationService = paymentSubmissionApplicationService;
            this._visitApplicationService = visitApplicationService;
            this._visitService = visitService;
            this._scoringService = scoringService;
        }

        public async Task<UserPaymentOptionDto> GetPaymentOptionAsync(int vpGuarantorId, int currentVisitPayUserId, PaymentOptionEnum paymentOption)
        {
            List<UserPaymentOptionDto> availableOptions = await this.GetPaymentOptionsAsync(vpGuarantorId, currentVisitPayUserId, paymentOption.ToListOfOne());
            
            return availableOptions.FirstOrDefault();
        }

        public async Task<IList<UserPaymentOptionDto>> GetPaymentOptionsAsync(int vpGuarantorId, int currentVisitPayUserId)
        {
            List<UserPaymentOptionDto> availableOptions = await this.GetPaymentOptionsAsync(vpGuarantorId, currentVisitPayUserId, null);
            
            return availableOptions;
        }

        private async Task<List<UserPaymentOptionDto>> GetPaymentOptionsAsync(int vpGuarantorId, int currentVisitPayUserId, IList<PaymentOptionEnum> paymentOptionEnums)
        {
            IList<PaymentOption> paymentOptions = this._paymentOptionService.Value.GetPaymentOptions();
            if (paymentOptionEnums.IsNotNullOrEmpty())
            {
                paymentOptions = paymentOptions.Where(x => paymentOptionEnums.Contains(x.PaymentOptionId)).ToList();
            }

            IList<UserPaymentOptionDto> userPaymentOptions = new List<UserPaymentOptionDto>();
            
            //Add user payment options for all non-FP payment options
            foreach (PaymentOption paymentOption in paymentOptions.Where(p => p.PaymentOptionId != PaymentOptionEnum.FinancePlan))
            {
                UserPaymentOptionDto userPaymentOption = Mapper.Map<UserPaymentOptionDto>(paymentOption);
                userPaymentOption.CmsVersion = this.GetCmsVersion(paymentOption.CmsRegionId);

                userPaymentOptions.Add(userPaymentOption);
            }

            //Add user payment options for FP payment option
            bool guarantorHasVisitsInMultipleStates = false;
            PaymentOption financePlanPaymentOption = paymentOptions.FirstOrDefault(p => p.PaymentOptionId == PaymentOptionEnum.FinancePlan);
            if (financePlanPaymentOption != null)
            {
                //Check if guarantor has a pending FP in any state. If there is a pending FP, we will only allow that option.
                CalculateFinancePlanTermsResponseDto pendingFinancePlanTerms = await this._financePlanApplicationService.Value.GetTermsOnPendingFinancePlanAsync(currentVisitPayUserId, vpGuarantorId);

                //Check for multi-state visits. Only allow multi-state setup if we don't have a pending plan
                guarantorHasVisitsInMultipleStates = this._visitService.Value.GuarantorHasVisitsInMultipleStates(vpGuarantorId);
                if (guarantorHasVisitsInMultipleStates && pendingFinancePlanTerms == null)
                {
                    //Handle multiple states
                    IList<string> stateCodesForVisits = this._visitService.Value.GetStateCodesForGuarantorVisits(vpGuarantorId);
                    foreach (string stateCode in stateCodesForVisits)
                    {
                        /*
                         * This should not happen in the real world (visit in a multi-facility client without a facility assigned).
                         * If there is a visit without a state code in here, then we should continue and not add a payment option for it.
                         * We can't handle visits with and without facilities at the same time, so defaulting to visits that actually have
                         * facilities. Log a WARN message.
                         */
                        if (string.IsNullOrEmpty(stateCode))
                        {
                            this.LoggingService.Value.Warn(() =>  $"{nameof(PaymentOptionApplicationService)}::{nameof(this.GetPaymentOptionsAsync)} - Guarantor has visits both with and without a facility assigned. Visits without a facility will not be included in payment options.");
                            continue;
                        }

                        //Add a payment option per state
                        UserPaymentOptionDto userPaymentOption = Mapper.Map<UserPaymentOptionDto>(financePlanPaymentOption);
                        userPaymentOption.CmsVersion = this.GetCmsVersion(financePlanPaymentOption.CmsRegionId);
                        userPaymentOption.FinancePlanOptionStateCode = stateCode;

                        //Add state name
                        string stateName;
                        if (Geo.GetStates().TryGetValue(stateCode, out stateName))
                        {
                            //Found the state name
                            userPaymentOption.FinancePlanOptionStateName = stateName;
                        }

                        //Add the user option
                        userPaymentOptions.Add(userPaymentOption);
                    }
                }
                else
                {
                    //Guarantor only has visits in a single state or has a pending FP, add payment option as usual
                    UserPaymentOptionDto userPaymentOption = Mapper.Map<UserPaymentOptionDto>(financePlanPaymentOption);
                    userPaymentOption.CmsVersion = this.GetCmsVersion(financePlanPaymentOption.CmsRegionId);
                    userPaymentOptions.Add(userPaymentOption);
                }
            }

            //
            GuarantorDto guarantorDto = Mapper.Map<GuarantorDto>(this._guarantorService.Value.GetGuarantor(vpGuarantorId));
            FinancialDataSummaryDto financialDataSummaryDto = this._financialDataSummaryApplicationService.Value.GetFinancialDataSummaryByGuarantor(guarantorDto.VpGuarantorId);

            foreach (UserPaymentOptionDto userPaymentOption in userPaymentOptions)
            {
                await this.SetUserSpecifics(userPaymentOption, guarantorDto, financialDataSummaryDto, currentVisitPayUserId);
            }

            // account balance depends on specific visits + specific fp's being enabled
            // setup after to avoid reloading data
            this.SetAccountBalanceOption(userPaymentOptions, guarantorDto);

            //
            this.FilterOptions(userPaymentOptions, currentVisitPayUserId, guarantorHasVisitsInMultipleStates);
            if (userPaymentOptions.Any(x => x.IsEnabled && x.IsExclusive))
            {
                return userPaymentOptions.Where(x => x.IsEnabled && x.IsExclusive).ToList();
            }

            // if it the option is not enabled check to see if it is visible when it is not enabled.
            List<UserPaymentOptionDto> availableOptions = userPaymentOptions.Where(x => !x.IsExclusive && ((!x.IsEnabled && x.IsVisibleWhenNotEnabled) || x.IsEnabled)).ToList();

            //If there are FP options for more than one state available, indicate that on the options
            List<UserPaymentOptionDto> fpOptions = userPaymentOptions.Where(o => o.PaymentOptionId == PaymentOptionEnum.FinancePlan).ToList();
            if (fpOptions.Count > 1)
            {
                fpOptions.ForEach(o => o.OtherFinancePlanStatesAreAvailable = true);
            }

            return availableOptions;
        }
        
        public async Task<UserPaymentOptionDto> GetFinancePlanPaymentOptionAsync(int vpGuarantorId, int currentVisitPayUserId, int? financePlanOfferSetTypeId = null, string financePlanOptionStateCode = null, bool otherFinancePlanStatesAreAvailable = false)
        {
            //
            GuarantorDto guarantorDto = Mapper.Map<GuarantorDto>(this._guarantorService.Value.GetGuarantor(vpGuarantorId));
            FinancialDataSummaryDto financialDataSummaryDto = this._financialDataSummaryApplicationService.Value.GetFinancialDataSummaryByGuarantor(guarantorDto.VpGuarantorId);

            // 
            UserPaymentOptionDto option = new UserPaymentOptionDto
            {
                PaymentOptionId = PaymentOptionEnum.FinancePlan,
                FinancePlanOptionStateCode = financePlanOptionStateCode,
                OtherFinancePlanStatesAreAvailable = otherFinancePlanStatesAreAvailable
            };

            //If state code provided, get the state name
            if (!string.IsNullOrEmpty(financePlanOptionStateCode))
            {
                string stateName = null;
                if (Geo.GetStates().TryGetValue(financePlanOptionStateCode, out stateName))
                {
                    //Found the state name
                    option.FinancePlanOptionStateName = stateName;
                }
            }

            FinancePlanCalculationParametersDto checkParameters = FinancePlanCalculationParametersDto.CreateForTermsCheck(guarantorDto.VpGuarantorId, financePlanOfferSetTypeId, false, stateCodeForVisits: financePlanOptionStateCode);
            option.UserApplicableBalance = this._financePlanApplicationService.Value.GetEligibleBalanceForFinancePlan(checkParameters, currentVisitPayUserId);
            if (option.UserApplicableBalance <= 0m)
            {
                // least restrictive check to see if a combined finance plan available - terms for statemented + pending visits
                checkParameters = FinancePlanCalculationParametersDto.CreateForTermsCheck(guarantorDto.VpGuarantorId, null, true, stateCodeForVisits: financePlanOptionStateCode);
                option.UserApplicableBalance = this._financePlanApplicationService.Value.GetEligibleBalanceForFinancePlan(checkParameters, currentVisitPayUserId);
            }
            VisitFilterDto visitFilter = new VisitFilterDto
            {
                VisitStateIds = ((int)VisitStateEnum.Active).ToListOfOne(),
                IsOnActiveFinancePlan = false,
                BillingHold = false
            };
            
            IList<int> visitsInPendingFinancePlan = this._financePlanApplicationService.Value.GetVisitIdsOnPendingFinancePlans(vpGuarantorId);
            option.FinancePlanVisits = this._visitApplicationService.Value.GetVisits(guarantorDto.User.VisitPayUserId, visitFilter, 1, 100).Visits.ToList();
            if (visitsInPendingFinancePlan?.Count > 0)
            {
                option.FinancePlanVisits = option.FinancePlanVisits.Where(x => visitsInPendingFinancePlan.Contains(x.VisitId)).ToList();
                option.SelectedVisits = option.FinancePlanVisits.Where(x => visitsInPendingFinancePlan.Contains(x.VisitId)).ToList();
            }

            await this.SetupFinancePlanPaymentOption(option, guarantorDto, financialDataSummaryDto, financePlanOfferSetTypeId, currentVisitPayUserId);

            return option;
        }

        public PaymentMenuDto GetPaymentOptionMenu(IList<UserPaymentOptionDto> paymentOptions, PaymentOptionEnum? selectedPaymentOption)
        {
            IList<PaymentMenuItem> paymentMenu = this._paymentOptionService.Value.GetPaymentMenu(1);
            IList<PaymentMenuItemDto> paymentMenuItemDtos = this.MapMenuItems(paymentMenu, paymentOptions);

            PaymentMenuDto paymentMenuDto = new PaymentMenuDto
            {
                PaymentMenuItems = paymentMenuItemDtos
            };

            List<PaymentMenuItemDto> enabledItems = new List<PaymentMenuItemDto>();
            enabledItems.AddRange(paymentMenuItemDtos.Where(x => x.IsEnabled));
            enabledItems.AddRange(paymentMenuItemDtos.Where(x => x.IsEnabled).SelectMany(x => x.PaymentMenuItems.Where(z => z.IsEnabled)));

            if (!enabledItems.Any())
            {
                return paymentMenuDto;
            }

            PaymentMenuItemDto selectedPaymentMenuItem = null;

            if (selectedPaymentOption.HasValue)
            {
                selectedPaymentMenuItem = enabledItems.FirstOrDefault(x => x.UserPaymentOption != null && x.UserPaymentOption.PaymentOptionId == selectedPaymentOption.Value);
            }

            if (selectedPaymentMenuItem == null)
            {
                selectedPaymentMenuItem = enabledItems.First();
            }

            if (selectedPaymentMenuItem != null)
            {
                paymentMenuDto.SelectedPaymentMenuItemId = selectedPaymentMenuItem.PaymentMenuItemId;
            }

            return paymentMenuDto;
        }

        public PaymentTypeEnum ConvertPaymentOptionToPaymentType(PaymentOptionEnum paymentOption, bool payBucketZero = false)
        {
            switch (paymentOption)
            {
                case PaymentOptionEnum.AccountBalance:
                    return PaymentTypeEnum.ManualPromptCurrentBalanceInFull;
                case PaymentOptionEnum.CurrentNonFinancedBalance:
                    return PaymentTypeEnum.ManualPromptCurrentNonFinancedBalanceInFull;
                case PaymentOptionEnum.FinancePlan:
                    return PaymentTypeEnum.RecurringPaymentFinancePlan;
                case PaymentOptionEnum.Resubmit:
                    return PaymentTypeEnum.ResubmitPayments;
                case PaymentOptionEnum.SpecificAmount:
                    return PaymentTypeEnum.ManualPromptSpecificAmount;
                case PaymentOptionEnum.SpecificFinancePlans:
                    if (payBucketZero)
                    {
                        return PaymentTypeEnum.ManualPromptSpecificFinancePlansBucketZero;
                    }
                    return PaymentTypeEnum.ManualPromptSpecificFinancePlans;
                case PaymentOptionEnum.SpecificVisits:
                    return PaymentTypeEnum.ManualPromptSpecificVisits;
                case PaymentOptionEnum.HouseholdCurrentNonFinancedBalance:
                    return PaymentTypeEnum.HouseholdBalanceCurrentNonFinancedBalance;
            }
            
            this.LoggingService.Value.Fatal(() => $"PaymentOptionApplicationService::ConvertPaymentOptionToPaymentType - invalid paymentOption {paymentOption.ToString()}");
            throw new Exception("Invalid Payment Option");
        }

        private IList<PaymentMenuItemDto> MapMenuItems(IList<PaymentMenuItem> menuItems, IList<UserPaymentOptionDto> userPaymentOptionDtos)
        {
            IList<PaymentMenuItemDto> dtoList = new List<PaymentMenuItemDto>();
            bool containsExclusiveOption = userPaymentOptionDtos.Any(x => x.IsExclusive);

            foreach (PaymentMenuItem menuItem in menuItems.Where(x => x.ParentPaymentMenuItem == null).OrderBy(x => x.DisplayOrder))
            {
                if (menuItem.PaymentOption == null && containsExclusiveOption)
                {
                    continue;
                }

                PaymentMenuItemDto dto = this.MapMenuItem(menuItem, userPaymentOptionDtos);
                if (dto == null)
                {
                    continue;
                }

                if (menuItem.PaymentMenuItems != null)
                {
                    foreach (PaymentMenuItem childItem in menuItem.PaymentMenuItems.OrderBy(x => x.DisplayOrder))
                    {
                        PaymentMenuItemDto childDto = this.MapMenuItem(childItem, userPaymentOptionDtos);
                        if (childDto == null)
                        {
                            continue;
                        }

                        dto.PaymentMenuItems.Add(childDto);
                    }
                }

                if (dto.IsEnabled && 
                    dto.PaymentMenuItems != null && 
                    dto.PaymentMenuItems.Any() && 
                    dto.PaymentMenuItems.All(x => !x.IsEnabled))
                {
                    dto.IsEnabled = false;
                    dto.PaymentMenuItems = null;
                }

                dtoList.Add(dto);
            }

            foreach (PaymentMenuItemDto dto in dtoList)
            {
                if (dto.IsEnabled && dto.PaymentMenuItems.IsNullOrEmpty() && dto.UserPaymentOption == null)
                {
                    dto.IsEnabled = false;
                }
            }

            return dtoList;
        }

        private PaymentMenuItemDto MapMenuItem(PaymentMenuItem menuItem, IList<UserPaymentOptionDto> userPaymentOptionDtos)
        {
            if (menuItem.PaymentOption != null && userPaymentOptionDtos.All(x => x.PaymentOptionId != menuItem.PaymentOption.PaymentOptionId))
            {
                return null;
            }

            PaymentMenuItemDto dto = Mapper.Map<PaymentMenuItemDto>(menuItem);

            dto.CmsVersion = this.GetCmsVersion(menuItem.CmsRegionId);

            if (menuItem.PaymentOption != null) 
            {
                dto.UserPaymentOption = userPaymentOptionDtos.FirstOrDefault(x => x.PaymentOptionId == menuItem.PaymentOption.PaymentOptionId);
            }

            dto.IsEnabled = dto.UserPaymentOption == null || dto.UserPaymentOption.IsEnabled;

            return dto;
        }

        private CmsVersionDto GetCmsVersion(int? cmsRegionId)
        {
            if (!cmsRegionId.HasValue)
            {
                return new CmsVersionDto { ContentTitle = "", ContentBody = "" };
            }

            CmsRegionEnum cmsRegionEnum;
            if (cmsRegionId != 0 && Enum.TryParse(cmsRegionId.ToString(), out cmsRegionEnum))
            {
                return this.GetCmsVersion(cmsRegionEnum);
            }

            return null;
        }

        private CmsVersionDto GetCmsVersion(CmsRegionEnum? cmsRegionEnum)
        {
            if (!cmsRegionEnum.HasValue)
            {
                return new CmsVersionDto {ContentTitle = "", ContentBody = ""};
            }

            return this._contentApplicationService.Value.GetCurrentVersionAsync(cmsRegionEnum.Value).Result;
        }

        private void FilterOptions(IList<UserPaymentOptionDto> options, int currentVisitPayUserId, bool guarantorHasVisitsInMultipleStates)
        {
            // remove household if n/a
            IList<UserPaymentOptionDto> householdOptions = options.Where(x => x.PaymentOptionId.IsInCategory(PaymentOptionEnumCategory.Household)).ToList();
            if (householdOptions.IsNotNullOrEmpty())
            {
                Guarantor currentGuarantor = this._guarantorService.Value.GetGuarantorEx(currentVisitPayUserId);

                if (this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.HouseholdBalance) && currentGuarantor.ConsolidationStatus == GuarantorConsolidationStatusEnum.Managing)
                {
                    foreach (UserPaymentOptionDto x in householdOptions)
                    {
                        if (!x.IsEnabled)
                        {
                            // remove any disabled household options
                            options.Remove(x);
                        }
                    }
                }
                else
                {
                    // household balance is not enabled, or the current guarantor is not a managing guarantor
                    // remove the household options
                    foreach (UserPaymentOptionDto x in householdOptions)
                    {
                        options.Remove(x);
                    }
                }
            }

            if (options.Any(x => x.PaymentOptionId == PaymentOptionEnum.HouseholdCurrentNonFinancedBalance))
            {
                // HouseholdCurrentNonFinancedBalance is enabled, remove CurrentNonFinancedBalance
                // HouseholdCurrentNonFinancedBalance will replace CurrentNonFinancedBalance
                UserPaymentOptionDto currentNonFinancedBalance = options.FirstOrDefault(x => x.PaymentOptionId == PaymentOptionEnum.CurrentNonFinancedBalance);
                if (currentNonFinancedBalance != null)
                {
                    options.Remove(currentNonFinancedBalance);
                }
            }

            // handle managed guarantor when they are the logged in user
            Guarantor currentUserGuarantor = this._guarantorService.Value.GetGuarantorEx(currentVisitPayUserId);
            if (currentUserGuarantor.IsManaged())
            {
                // managed guarantor cannot create a finance plan
                UserPaymentOptionDto financePlan = options.FirstOrDefault(x => x.PaymentOptionId == PaymentOptionEnum.FinancePlan);
                if (financePlan != null)
                {
                    options.Remove(financePlan);
                }

                // prompt pay only for a managed user
                foreach (UserPaymentOptionDto option in options)
                {
                    option.DateBoundary = DateTime.UtcNow;
                    option.DateBoundaryType = PaymentDateTypeEnum.Today;
                }
            }

            /*
             * Remove any FP options that are disabled if we are dealing with multi-state visits. We don't want to the state
             * as an option if there aren't any applicable visits for that state.
             */
            if (guarantorHasVisitsInMultipleStates)
            {
                //Remove disabled FP options or options without associated visits
                List<UserPaymentOptionDto> disabledFinancePlanOptions = options.Where(o => o.PaymentOptionId == PaymentOptionEnum.FinancePlan && (!o.IsEnabled || o.FinancePlanVisits == null || o.FinancePlanVisits.Count == 0)).ToList();
                foreach (UserPaymentOptionDto option in disabledFinancePlanOptions)
                {
                    options.Remove(option);
                }
            }
        }
        
        private void SetDatesForPaymentOption(UserPaymentOptionDto option)
        {
            DateTime clientSettingDate = DateTime.UtcNow.AddDays(this.Client.Value.MaxFuturePaymentIntervalInDays);
            
            // set payment date boundary
            switch (option.DateBoundaryType)
            {
                case PaymentDateTypeEnum.ClientSetting:
                    option.DateBoundary = clientSettingDate;
                    break;
                case PaymentDateTypeEnum.Today:
                    option.DateBoundary = DateTime.UtcNow;
                    break;
            }

            // set default payment date
            switch (option.DefaultPaymentDateType)
            {
                case PaymentDateTypeEnum.ClientSetting:
                    option.DefaultPaymentDate = clientSettingDate;
                    break;
                case PaymentDateTypeEnum.Today:
                    option.DefaultPaymentDate = DateTime.UtcNow;
                    break;
            }
        }

        private async Task SetUserSpecifics(UserPaymentOptionDto option, GuarantorDto guarantorDto, FinancialDataSummaryDto financialDataSummaryDto, int currentVisitPayUserId)
        {
            //Set dates
            this.SetDatesForPaymentOption(option);
            
            // set balances + enabled
            switch (option.PaymentOptionId)
            {
                case PaymentOptionEnum.CurrentNonFinancedBalance:
                    option.UserApplicableBalance = financialDataSummaryDto.TotalBalance;
                    option.IsEnabled = option.UserApplicableBalance > 0m;
                    break;
                case PaymentOptionEnum.FinancePlan:
                    FinancePlanCalculationParametersDto checkParameters = FinancePlanCalculationParametersDto.CreateForTermsCheck(guarantorDto.VpGuarantorId, null, false, stateCodeForVisits: option.FinancePlanOptionStateCode);

                    // check to see if a stacked finance plan available
                    option.UserApplicableBalance = this._financePlanApplicationService.Value.GetEligibleBalanceForFinancePlan(checkParameters, currentVisitPayUserId); 
                    if (option.UserApplicableBalance <= 0m)
                    {
                        // check to see if a combined finance plan available
                        checkParameters = FinancePlanCalculationParametersDto.CreateForTermsCheck(guarantorDto.VpGuarantorId, null, true, stateCodeForVisits: option.FinancePlanOptionStateCode);              
                        option.UserApplicableBalance = this._financePlanApplicationService.Value.GetEligibleBalanceForFinancePlan(checkParameters, currentVisitPayUserId);
                    }

                    if (option.UserApplicableBalance > 0m)
                    {
                        option.IsEnabled = true;

                        await this.SetupFinancePlanPaymentOption(option, guarantorDto, financialDataSummaryDto, null, currentVisitPayUserId);

                        // any pending plan
                        CalculateFinancePlanTermsResponseDto pendingTermsResponse = await this._financePlanApplicationService.Value.GetTermsOnPendingFinancePlanAsync(guarantorDto.User.VisitPayUserId, guarantorDto.VpGuarantorId);
                        if (pendingTermsResponse != null)
                        {
                            option.UserApplicableBalance = this._financePlanApplicationService.Value.GetEligibleBalanceForFinancePlan(FinancePlanCalculationParametersDto.CreateForTermsCheck(guarantorDto.VpGuarantorId, null, pendingTermsResponse.FinancePlan.IsCombined), currentVisitPayUserId);
                        }
                    }

                    option.HasPendingResubmitPayments = this._financePlanApplicationService.Value.HasFinancePlansWhichShouldResubmit(guarantorDto.VpGuarantorId);
                    //option.FinancePlanVisits = financialDataSummaryDto.SelectedVisits.Count > 0 ? financialDataSummaryDto.SelectedVisits : financialDataSummaryDto.Visits;
                    
                    if (!string.IsNullOrEmpty(option.FinancePlanOptionStateCode))
                    {
                        //Only add visits for the applicable state for this option.
                        IList<VisitDto> visitsForState = financialDataSummaryDto.Visits.Where(v => v.Facility?.RicState?.StateCode == option.FinancePlanOptionStateCode).ToList();
                        IList<VisitDto> selectedVisitsForState = financialDataSummaryDto.SelectedVisits.Where(v => v.Facility?.RicState?.StateCode == option.FinancePlanOptionStateCode).ToList();
                        option.FinancePlanVisits = visitsForState;
                        option.SelectedVisits = selectedVisitsForState;
                    }
                    else
                    {
                        option.FinancePlanVisits = financialDataSummaryDto.Visits;
                        option.SelectedVisits = financialDataSummaryDto.SelectedVisits;
                    }

                    option.AmountToFinance = financialDataSummaryDto.TotalBalance;

                    break;
                case PaymentOptionEnum.HouseholdCurrentNonFinancedBalance:
                    IDictionary<int, decimal> balances = this._consolidationGuarantorApplicationService.Value.GetHouseholdBalances(currentVisitPayUserId);
                    if (balances.Any())
                    {
                        option.HouseholdBalances = balances.Select(x => new HouseholdBalanceDto
                        {
                            VpStatementId = x.Key,
                            VisitPayUser = Mapper.Map<VisitPayUserDto>(this._guarantorService.Value.GetGuarantor(this._statementService.Value.GetStatement(x.Key).VpGuarantorId).User),
                            TotalBalance = x.Value
                        }).ToList();
                    }
                    option.UserApplicableBalance = (option.HouseholdBalances ?? new List<HouseholdBalanceDto>()).Sum(x => x.TotalBalance);
                    option.IsEnabled = option.UserApplicableBalance > 0m;
                    break;
                case PaymentOptionEnum.Resubmit:
                    option.IsEnabled = this._financePlanApplicationService.Value.HasFinancePlansWhichShouldResubmit(guarantorDto.VpGuarantorId);
                    break;
                case PaymentOptionEnum.SpecificAmount:
                    option.IsEnabled = financialDataSummaryDto.BalanceInFull > 0m;
                    option.MaximumPaymentAmount = financialDataSummaryDto.BalanceInFull;
                    break;
                case PaymentOptionEnum.SpecificFinancePlans:
                    option.IsEnabled = financialDataSummaryDto.BalanceInFull > 0m &&
                                       this._financePlanApplicationService.Value.HasFinancePlans(guarantorDto.User.VisitPayUserId, EnumHelper<FinancePlanStatusEnum>.GetValues(FinancePlanStatusEnumCategory.Active).ToList(), guarantorDto.VpGuarantorId);
                    break;
                case PaymentOptionEnum.SpecificVisits:
                    option.IsEnabled = financialDataSummaryDto.TotalBalance > 0m;
                    break;
            }

            if (option.IsEnabled)
            {
                // specific
                switch (option.PaymentOptionId)
                {
                    case PaymentOptionEnum.Resubmit:
                        option.Payments = this._financePlanApplicationService.Value.GetPaymentResubmitDtosForFinancePlans(guarantorDto.VpGuarantorId) ?? new List<ResubmitPaymentDto>();
                        break;
                    case PaymentOptionEnum.SpecificVisits:
                        VisitFilterDto visitFilter = new VisitFilterDto
                        {
                            SortField = "DischargeDate",
                            SortOrder = "desc",
                            VisitStateIds = ((int)VisitStateEnum.Active).ToListOfOne(),
                            IsOnActiveFinancePlan = false,
                            BillingHold = false
                        };
                        option.Visits = this._visitApplicationService.Value.GetVisits(guarantorDto.User.VisitPayUserId, visitFilter, 1, 100).Visits.ToList();
                        break;
                    case PaymentOptionEnum.SpecificFinancePlans:
                        option.FinancePlans = this._financePlanApplicationService.Value.GetOpenFinancePlansForGuarantor(guarantorDto.VpGuarantorId, false)
                            .Where(x => x.CurrentFinancePlanBalance > 0m)
                            .OrderByDescending(x => x.OriginationDate)
                            .ToList();
                        //This is wrong because it makes a bucket 0 seem like it's there when it's not actually there...
                        //foreach (FinancePlanDto optionFinancePlan in option.FinancePlans)
                        //{
                        //    this._financePlanApplicationService.Value.SetAmountDueForDisplay(optionFinancePlan);
                        //}
                        break;
                }

                // discounts
                switch (option.PaymentOptionId)
                {
                    case PaymentOptionEnum.HouseholdCurrentNonFinancedBalance:
                        this.AddHouseholdDiscount(option, currentVisitPayUserId);
                        break;
                    case PaymentOptionEnum.CurrentNonFinancedBalance:
                        this.AddDiscount(option, guarantorDto);
                        break;
                }
            }
        }

        private void SetAccountBalanceOption(IList<UserPaymentOptionDto> userPaymentOptions, GuarantorDto guarantorDto)
        {
            UserPaymentOptionDto accountBalance = userPaymentOptions.FirstOrDefault(x => x.PaymentOptionId == PaymentOptionEnum.AccountBalance);
            if (accountBalance == null)
            {
                return;
            }

            UserPaymentOptionDto specificFinancePlans = userPaymentOptions.FirstOrDefault(x => x.PaymentOptionId == PaymentOptionEnum.SpecificFinancePlans);
            UserPaymentOptionDto specificVisits = userPaymentOptions.FirstOrDefault(x => x.PaymentOptionId == PaymentOptionEnum.SpecificVisits);
            if (specificFinancePlans == null || specificVisits == null || !specificFinancePlans.IsEnabled || !specificVisits.IsEnabled)
            {
                // disabled unless have visits and fps
                accountBalance.IsEnabled = false;
                return;
            }

            accountBalance.IsEnabled = true;
            accountBalance.UserApplicableBalance = specificVisits.Visits.Sum(x => x.UnclearedBalance) + specificFinancePlans.FinancePlans.Sum(x => x.CurrentFinancePlanBalance);
            accountBalance.FinancePlans = specificFinancePlans.FinancePlans;
            accountBalance.Visits = specificVisits.Visits;

            this.AddDiscount(accountBalance, guarantorDto);
        }

        private void AddDiscount(UserPaymentOptionDto userPaymentOptionDto, GuarantorDto guarantorDto)
        {
            DiscountOfferDto discountOfferDto = this._paymentSubmissionApplicationService.Value.GetDiscountPerVisitForStatementedBalanceInFull(guarantorDto.VpGuarantorId);
            if (discountOfferDto == null || discountOfferDto.DiscountTotalPercent <= 0)
            {
                return;
            }

            //
            userPaymentOptionDto.IsDiscountEligible = true;
            userPaymentOptionDto.IsDiscountPromptPayOnly = discountOfferDto.IsPromptPayOnly;

            //
            userPaymentOptionDto.DiscountOffer = discountOfferDto;
            userPaymentOptionDto.DiscountAmount = discountOfferDto.DiscountTotal;
        }

        private void AddHouseholdDiscount(UserPaymentOptionDto userPaymentOptionDto, int currentVisitPayUserId)
        {
            GuarantorDto currentGuarantorDto = Mapper.Map<GuarantorDto>(this._guarantorService.Value.GetGuarantorEx(currentVisitPayUserId));

            this.AddDiscount(userPaymentOptionDto, currentGuarantorDto);
            AddDiscountToHouseholdBalance(userPaymentOptionDto.HouseholdBalances, userPaymentOptionDto, currentGuarantorDto);

            if (currentGuarantorDto.ManagedGuarantorIds == null || !currentGuarantorDto.ManagedGuarantorIds.Any())
            {
                return;
            }

            foreach (int managedGuarantorId in currentGuarantorDto.ManagedGuarantorIds)
            {
                GuarantorDto managedGuarantorDto = Mapper.Map<GuarantorDto>(this._guarantorService.Value.GetGuarantor(managedGuarantorId));
                if (managedGuarantorDto == null)
                {
                    continue;
                }

                UserPaymentOptionDto temp = new UserPaymentOptionDto();
                this.AddDiscount(temp, managedGuarantorDto);
                AddDiscountToHouseholdBalance(userPaymentOptionDto.HouseholdBalances, temp, managedGuarantorDto);
                
                userPaymentOptionDto.IsDiscountEligible = userPaymentOptionDto.IsDiscountEligible || temp.IsDiscountEligible;
                userPaymentOptionDto.DiscountAmount = userPaymentOptionDto.DiscountAmount.GetValueOrDefault(0) + temp.DiscountAmount.GetValueOrDefault(0);
                
                if (temp.DiscountOffer == null)
                {
                    continue;
                }

                userPaymentOptionDto.ManagedOffers.Add(managedGuarantorDto.User, temp.DiscountOffer);
            }
        }
        
        private static void AddDiscountToHouseholdBalance(IList<HouseholdBalanceDto> householdBalancesDto, UserPaymentOptionDto userPaymentOptionDto, GuarantorDto guarantorDto)
        {
            HouseholdBalanceDto householdBalanceDto = householdBalancesDto?.FirstOrDefault(x => x.VisitPayUser.VisitPayUserId == guarantorDto.User.VisitPayUserId);
            if (householdBalanceDto == null)
            {
                return;
            }

            householdBalanceDto.DiscountAmount = userPaymentOptionDto.DiscountAmount.GetValueOrDefault(0);
            householdBalanceDto.DiscountOffer = userPaymentOptionDto.DiscountOffer;
            householdBalanceDto.IsDiscountEligible = userPaymentOptionDto.IsDiscountEligible;
            householdBalanceDto.IsManaged = guarantorDto.ConsolidationStatus == GuarantorConsolidationStatusEnum.Managed;
        }

        #region finance plan

        private async Task SetupFinancePlanPaymentOption(UserPaymentOptionDto option, GuarantorDto guarantorDto, FinancialDataSummaryDto financialDataSummaryDto, int? financePlanOfferSetTypeId, int currentVisitPayUserId)
        {
            CalculateFinancePlanTermsResponseDto pendingTermsResponse = await this._financePlanApplicationService.Value.GetTermsOnPendingFinancePlanAsync(guarantorDto.User.VisitPayUserId, guarantorDto.VpGuarantorId);
            IList<FinancePlanOfferSetTypeDto> financePlanOfferSetTypes = this._financePlanApplicationService.Value.GetFinancePlanOfferSetTypes();
            IList<FinancePlanOfferSetTypeDto> applicableFinancePlanOfferSetTypes = new List<FinancePlanOfferSetTypeDto>();

            if (financePlanOfferSetTypeId.HasValue)
            {
                applicableFinancePlanOfferSetTypes.Add(financePlanOfferSetTypes.FirstOrDefault(x => x.FinancePlanOfferSetTypeId == financePlanOfferSetTypeId.Value));
            }
            else if (pendingTermsResponse != null)
            {
                applicableFinancePlanOfferSetTypes.Add(pendingTermsResponse.Terms.FinancePlanOfferSetType);
            }

            if (applicableFinancePlanOfferSetTypes.Count <= 0)
            {
                applicableFinancePlanOfferSetTypes = financePlanOfferSetTypes;
            }

            foreach (FinancePlanOfferSetTypeDto applicableFinancePlanOfferSetType in applicableFinancePlanOfferSetTypes)
            {
                bool canSetPaymentDueDay = this._financePlanApplicationService.Value.MeetsCriteriaToChangePaymentDueDate(guarantorDto.VpGuarantorId);
                if (canSetPaymentDueDay && pendingTermsResponse == null)
                {
                    // no open finance plans, and no pending terms
                    // use suggested day and allow change
                    int suggestedDay = DateTime.UtcNow.AddDays(this.Client.Value.PaymentDueDateDefaultDaysInFuture).Day;
                    option.SuggestedPaymentDueDay = suggestedDay.ToValidPaymentDueDay();
                    option.CanSetPaymentDueDay = true;
                }
                else
                {
                    // use the guarantors payment due day
                    option.SuggestedPaymentDueDay = guarantorDto.PaymentDueDay;
                    option.CanSetPaymentDueDay = false;
                }

                // a pending plan
                // only one applicable configuration in this scenario
                if (pendingTermsResponse != null)
                {
                    bool isCombined = pendingTermsResponse.FinancePlan.IsCombined;
                    FinancePlanConfigurationDto config = await this.CreateFinancePlanConfigurationAsync(guarantorDto, financialDataSummaryDto, financePlanOfferSetTypes, applicableFinancePlanOfferSetType, pendingTermsResponse, isCombined, option.SuggestedPaymentDueDay, option.FinancePlanOptionStateCode);
                    if (config != null)
                    {
                        // guarantor should not be able to edit the parameters this plan
                        config.IsEditable = this.ApplicationSettingsService.Value.IsClientApplication.Value;

                        if (isCombined)
                        {
                            option.FinancePlanCombinedConfiguration = config;
                        }
                        else
                        {
                            option.FinancePlanDefaultConfiguration = config;
                        }
                    }
                }
                else
                {
                    // combined config
                    bool eligibleToCombineFinancePlan = false;
                    if (option.FinancePlanCombinedConfiguration == null &&
                        this._financePlanApplicationService.Value.IsGuarantorEligibleToCombineFinancePlans(currentVisitPayUserId, guarantorDto.VpGuarantorId, false, option.FinancePlanOptionStateCode))
                    {
                        eligibleToCombineFinancePlan = true;
                        FinancePlanConfigurationDto combinedConfig = await this.CreateFinancePlanConfigurationAsync(guarantorDto, financialDataSummaryDto, financePlanOfferSetTypes, applicableFinancePlanOfferSetType, pendingTermsResponse, true, option.SuggestedPaymentDueDay, option.FinancePlanOptionStateCode);
                        if (combinedConfig != null && combinedConfig.MinimumPaymentAmount > 0 && combinedConfig.InterestRates?.Count > 0)
                        {
                            option.FinancePlanCombinedConfiguration = combinedConfig;
                        }
                    }

                    // default config
                    if (option.FinancePlanDefaultConfiguration == null)
                    {
                        FinancePlanConfigurationDto defaultConfig = await this.CreateFinancePlanConfigurationAsync(guarantorDto, financialDataSummaryDto, financePlanOfferSetTypes, applicableFinancePlanOfferSetType, pendingTermsResponse, false, option.SuggestedPaymentDueDay, option.FinancePlanOptionStateCode);
                        if (!financePlanOfferSetTypeId.HasValue &&
                            !applicableFinancePlanOfferSetType.IsDefault
                            && (defaultConfig?.InterestRates == null || defaultConfig.InterestRates.Count <= 0))
                        {
                            continue;
                        }

                        if (defaultConfig.MinimumPaymentAmount > 0 && option.UserApplicableBalance >= defaultConfig.MinimumPaymentAmount &&
                            !this.IsOfflineComboOnly(guarantorDto.IsOfflineGuarantor, option.FinancePlanCombinedConfiguration != null))
                        {
                            option.FinancePlanDefaultConfiguration = defaultConfig;
                        }
                    }

                    if (eligibleToCombineFinancePlan && option.FinancePlanCombinedConfiguration == null)
                    {
                        // Evaluate the next offer type for combined config
                        continue;
                    }
                }

                option.IsEnabled = option.FinancePlanCombinedConfiguration != null || option.FinancePlanDefaultConfiguration != null;

                if (option.IsEnabled)
                {
                    if (option.FinancePlanDefaultConfiguration != null)
                    {
                        option.FinancePlanDefaultConfiguration.AmountToFinance = option.UserApplicableBalance.GetValueOrDefault(0);
                    }

                    if (option.FinancePlanCombinedConfiguration != null)
                    {
                        IList<FinancePlanDto> openFinancePlans = this._financePlanApplicationService.Value.GetOpenFinancePlans(guarantorDto.User.VisitPayUserId, guarantorDto.VpGuarantorId);
                        decimal currentFinancePlanBalance = openFinancePlans.Sum(x => x.CurrentFinancePlanBalanceIncludingAllAdjustments);
                        option.FinancePlanCombinedConfiguration.AmountToFinance = option.UserApplicableBalance.GetValueOrDefault(0) + currentFinancePlanBalance;
                    }

                    this.SetFinancePlanOptionOrder(option);
                    option.FinancePlanTotalMonthlyPaymentAmount = this._financePlanApplicationService.Value.GetTotalPaymentAmountForOpenFinancePlans(guarantorDto.VpGuarantorId);

                    // setup suggested amount A/B test
                    List<FinancePlanConfigurationDto> configurations = new[] {option.FinancePlanDefaultConfiguration, option.FinancePlanCombinedConfiguration}.Where(x => x != null).OrderBy(x => x.DisplayOrder).ToList();
                    foreach (FinancePlanConfigurationDto configuration in configurations)
                    {
                        await this.SetupAnchoringTest(guarantorDto, financialDataSummaryDto, configuration);
                    }
                }

                break;
            }
        }

        private bool IsOfflineComboOnly(bool isOfflineGuarantor, bool isCombinedConfig)
        {
            return (this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureOfflineComboOnly) && isOfflineGuarantor && isCombinedConfig);
        }

        private void AddInterestRatesToFinancePlanConfiguration(FinancePlanConfigurationDto financePlanConfigurationDto, FinancePlanCalculationParametersDto financePlanCalculationParametersDto)
        {
            bool isClientPortal = this.ApplicationSettingsService.Value.IsClientApplication.Value;
            bool isWebAppPatientOffer = !isClientPortal && financePlanConfigurationDto.IsPatientOffer;
           
            //always show interest rates for client, only show interest rates for patient if offer set type is patient
            if (isClientPortal || isWebAppPatientOffer)
            {
                List<InterestRateDto> sortedInterestRates = this._financePlanApplicationService.Value.GetInterestRates(financePlanCalculationParametersDto).OrderBy(x => x.DurationRangeStart).ToList();
                financePlanConfigurationDto.InterestRates = sortedInterestRates;
            }
        }
        
        private void AddCommonFinancePlanConfigurationParameters(FinancePlanConfigurationDto financePlanConfigurationDto, FinancePlanCalculationParametersDto financePlanCalculationParametersDto)
        {
            this.AddInterestRatesToFinancePlanConfiguration(financePlanConfigurationDto, financePlanCalculationParametersDto);
            FinancePlanBoundaryDto financePlanBoundaries = this._financePlanApplicationService.Value.GetMinimumPaymentAmount(financePlanCalculationParametersDto);

            financePlanConfigurationDto.MaximumMonthlyPayments = financePlanBoundaries.MaximumNumberOfPayments;
            financePlanConfigurationDto.MinimumPaymentAmount = financePlanBoundaries.MinimumPaymentAmount;
        }

        private async Task<FinancePlanConfigurationDto> CreateFinancePlanConfigurationAsync(
            GuarantorDto guarantorDto,
            FinancialDataSummaryDto financialDataSummaryDto,
            IList<FinancePlanOfferSetTypeDto> financePlanOfferSetTypes,
            FinancePlanOfferSetTypeDto financePlanOfferSetType,
            CalculateFinancePlanTermsResponseDto pendingTermsResponse,
            bool isCombined,
            int paymentDueDay,
            string financePlanStateCode
        )
        {
            IList<FinancePlanDto> activeFinancePlans = this._financePlanApplicationService.Value.GetOpenFinancePlans(guarantorDto.User.VisitPayUserId, guarantorDto.VpGuarantorId, financePlanStateCode: financePlanStateCode);

            //If a state is provided, we only want to look at active FPs for the state in question when combining plans
            if (isCombined && !string.IsNullOrEmpty(financePlanStateCode) && activeFinancePlans.Count == 0)
            {
                //No FPs for the state, can't create combined FP so return null configuration
                return null;
            }

            CmsRegionEnum optionCmsRegion = isCombined ? CmsRegionEnum.ArrangePaymentFinancePlanCombineLabel : CmsRegionEnum.ArrangePaymentFinancePlanDefaultLabel;
            CmsVersionDto optionCmsVersion = await this._contentApplicationService.Value.GetCurrentVersionAsync(optionCmsRegion);

            // todo: things from VP-5702 in 6.17
            IList<FinancePlanTypeDto> financePlanTypes =this.GetApplicableFinancePlanTypes(guarantorDto);
            CmsRegionEnum cmsRegionEnum = this._visitService.Value.GetRicCmsRegionEnum(guarantorDto.VpGuarantorId);

            FinancePlanConfigurationDto config = new FinancePlanConfigurationDto
            {
                ActiveFinancePlanBalance = activeFinancePlans.Sum(x => x.CurrentFinancePlanBalanceIncludingAllAdjustments),
                ActiveFinancePlanCount = activeFinancePlans.Count,
                FinancePlanOfferSetTypeId = financePlanOfferSetType.FinancePlanOfferSetTypeId,
                FinancePlanOfferSetTypes = financePlanOfferSetTypes,
                FinancePlanTypes = financePlanTypes,
                IsPatientOffer = financePlanOfferSetType.IsPatient,
                IsCombined = isCombined,
                OfferCalculationStrategy = OfferCalculationStrategyEnum.MonthlyPayment,
                StatementId = financialDataSummaryDto.CurrentStatementId.GetValueOrDefault(0),
                TermsCmsVersionId = (await this._contentApplicationService.Value.GetCurrentVersionAsync(cmsRegionEnum, false)).CmsVersionId,
                OptionText = optionCmsVersion.ContentBody,
                OptionTooltip = optionCmsVersion.ContentTitle
            };

            if (guarantorDto.IsOfflineGuarantor)
            {
                CmsVersionDto esignCmsVersion = (await this._contentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.EsignTerms, false));
                config.EsignCmsVersionId = esignCmsVersion?.CmsVersionId;
            }

            if (pendingTermsResponse != null)
            {
                config.FinancePlanId = pendingTermsResponse.FinancePlan.FinancePlanId;
                config.TermsCmsVersionId = pendingTermsResponse.Terms.TermsCmsVersionId;
            }

            if (pendingTermsResponse != null && pendingTermsResponse.FinancePlan.IsCombined == isCombined)
            {
                config.Terms = pendingTermsResponse.Terms;

                if (pendingTermsResponse.FinancePlan != null)
                {
                    config.OfferCalculationStrategy = pendingTermsResponse.FinancePlan.OfferCalculationStrategy;
                }
            }

            FinancePlanCalculationParametersDto financePlanCalculationParametersDto;
            if (isCombined)
            {
                financePlanCalculationParametersDto = FinancePlanCalculationParametersDto.CreateForTermsCheck(guarantorDto.VpGuarantorId, financePlanOfferSetType.FinancePlanOfferSetTypeId, true, paymentDueDay, stateCodeForVisits: financePlanStateCode);
            }
            else
            {
                financePlanCalculationParametersDto = FinancePlanCalculationParametersDto.CreateForTermsCheck(guarantorDto.VpGuarantorId, financePlanOfferSetType.FinancePlanOfferSetTypeId, false, paymentDueDay, stateCodeForVisits: financePlanStateCode);
            }

            this.AddCommonFinancePlanConfigurationParameters(config, financePlanCalculationParametersDto);

            // set the client offer text
            config.TextClientOfferSet = string.Empty;
            if (financePlanOfferSetType.IsClientOnly || pendingTermsResponse != null)
            {
                DateTime? expiryDate = this._financePlanApplicationService.Value.GetPendingFinancePlanCancellationDate(guarantorDto.VpGuarantorId);

                if (expiryDate.HasValue)
                {
                    IDictionary<string, string> replacementDictionary = new Dictionary<string, string>
                    {
                        ["[[ExpiryDate]]"] = expiryDate.ToDateText()
                    };

                    config.TextClientOfferSet = (await this._contentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.ClientOnlyOfferSetPatientPayBill, additionalValues: replacementDictionary)).ContentBody;
                }
                else
                {
                    this.LoggingService.Value.Warn(() =>  $"{nameof(PaymentOptionApplicationService)}::{nameof(this.CreateFinancePlanConfigurationAsync)} - no expiryDate found for {guarantorDto.VpGuarantorId}.");
                }
            }

            return config;
        }

        /// <summary>
        /// todo: things from VP-5702 in 6.17
        /// right now, this is just to get this (temporary) logic into c# instead of javascript
        /// </summary>
        /// <returns></returns>
        private IList<FinancePlanTypeDto> GetApplicableFinancePlanTypes(GuarantorDto guarantorDto)
        {
            if (!guarantorDto.IsOfflineGuarantor)
            {
                // not an offline guarantor - no options
                return new List<FinancePlanTypeDto>();
            }

            IList<FinancePlanTypeDto> types = this._financePlanApplicationService.Value.GetFinancePlanTypes();

            return types;
        }

        private void SetFinancePlanOptionOrder(UserPaymentOptionDto option)
        {
            if (option.PaymentOptionId != PaymentOptionEnum.FinancePlan)
            {
                return;
            }
            
            if (option.FinancePlanDefaultConfiguration != null)
            {
                option.FinancePlanDefaultConfiguration.DisplayOrder = 1;
            }

            if (option.FinancePlanCombinedConfiguration != null)
            {
                option.FinancePlanCombinedConfiguration.DisplayOrder = 0;
            }
        }

        private bool FinancePlanIsEligibleForAnchoringTest(FinancePlanConfigurationDto financePlanConfigurationDto)
        {
            if (financePlanConfigurationDto == null)
            {
                return false;
            }

            if (this.ApplicationSettingsService.Value.IsClientApplication.Value)
            {
                // ignore test if in client app
                return false;
            }

            if (!financePlanConfigurationDto.IsEditable)
            {
                // ignore test if it's a client offer set or pending plan
                return false;
            }

            if (financePlanConfigurationDto.FinancePlanId.HasValue)
            {
                // ignore test if it's a pending finance plan
                return false;
            }

            return true;
        }

        private async Task SetupSuggestedAmountTest(GuarantorDto guarantorDto, FinancialDataSummaryDto financialDataSummaryDto, FinancePlanConfigurationDto financePlanConfigurationDto, RandomizedTest randomizedTest, RandomizedTestGroupEnum randomizedTestGroupEnum)
        {
            // group A or B
            FinancePlanSuggestedAmountRandomizedTestGroupConfiguration testGroupConfiguration = new FinancePlanSuggestedAmountRandomizedTest(randomizedTest).GetTestGroupConfiguration(randomizedTestGroupEnum);

            if (testGroupConfiguration.MaxAmount != null && financialDataSummaryDto.TotalBalance > testGroupConfiguration.MaxAmount)
            {
                // Do not anchor balances greater than the threshold
                return;
            }

            // 
            decimal monthlyPaymentAmount = testGroupConfiguration.Amount;

            FinancePlanCalculationParametersDto parameters = FinancePlanCalculationParametersDto.CreateForMonthlyPayment(
                guarantorDto.VpGuarantorId,
                financialDataSummaryDto.CurrentStatementId.GetValueOrDefault(0),
                monthlyPaymentAmount,
                null,
                financePlanConfigurationDto.IsCombined);

            CalculateFinancePlanTermsResponseDto response = await this._financePlanApplicationService.Value.GetTermsByMonthlyPaymentAmountAsync(parameters);

            if (response?.Terms == null || response.Terms.MonthlyPaymentAmount <= 0 || response.Terms.NumberMonthlyPayments <= 0)
            {
                // the desired suggested terms were not available to this user
                return;
            }

            financePlanConfigurationDto.SuggestedMonthlyPaymentAmount = response.Terms.MonthlyPaymentAmount;
            financePlanConfigurationDto.SuggestedNumberMonthlyPayments = response.Terms.NumberMonthlyPayments;
        }

        private async Task SetupSuggestedDurationTest(GuarantorDto guarantorDto, FinancialDataSummaryDto financialDataSummaryDto, FinancePlanConfigurationDto financePlanConfigurationDto, RandomizedTest randomizedTest, RandomizedTestGroupEnum randomizedTestGroupEnum)
        {
            // group A or B
            FinancePlanSuggestedDurationRandomizedTestGroupConfiguration testGroupConfiguration = new FinancePlanSuggestedDurationRandomizedTest(randomizedTest).GetTestGroupConfiguration(randomizedTestGroupEnum);

            // get duration to check
            int? checkDuration = null;
            if (randomizedTestGroupEnum == RandomizedTestGroupEnum.FinancePlanSuggestedDurationA)
            {
                checkDuration = this._financePlanApplicationService.Value.GetFirstTierMaxDuration(FinancePlanCalculationParametersDto.CreateForTermsCheck(guarantorDto.VpGuarantorId, null, financePlanConfigurationDto.IsCombined));
            }
            else if (randomizedTestGroupEnum == RandomizedTestGroupEnum.FinancePlanSuggestedDurationB)
            {
                if (testGroupConfiguration?.Duration.HasValue ?? false)
                {
                    checkDuration = testGroupConfiguration.Duration;
                }
            }

            if (!checkDuration.HasValue || checkDuration.Value <= 0)
            {
                // the desired suggested duration was not available to this user
                return;
            }

            FinancePlanCalculationParametersDto parameters = FinancePlanCalculationParametersDto.CreateForNumberOfPayments(
                guarantorDto.VpGuarantorId,
                financialDataSummaryDto.CurrentStatementId.GetValueOrDefault(0),
                checkDuration.Value,
                null,
                financePlanConfigurationDto.IsCombined);

            CalculateFinancePlanTermsResponseDto response = await this._financePlanApplicationService.Value.GetTermsByNumberOfMonthlyPaymentsAsync(parameters);

            if (response?.Terms == null || response.Terms.MonthlyPaymentAmount <= 0 || response.Terms.NumberMonthlyPayments <= 0)
            {
                // the desired suggested terms were not available to this user
                return;
            }

            if (response.Terms.MonthlyPaymentAmount > testGroupConfiguration?.MaximumMonthlyPaymentAmount.GetValueOrDefault(0))
            {
                // the calculated monthly payment amount does not meet configured criteria
                return;
            }

            financePlanConfigurationDto.SuggestedMonthlyPaymentAmount = response.Terms.MonthlyPaymentAmount;
            financePlanConfigurationDto.SuggestedNumberMonthlyPayments = response.Terms.NumberMonthlyPayments;
        }

        private RandomizedTest GetAnchoringTests()
        {
            RandomizedTest randomizedTestDuration = this._randomizedTestService.Value.GetRandomizedTest(RandomizedTestEnum.FinancePlanSuggestedDuration);
            RandomizedTest randomizedTestAmount = this._randomizedTestService.Value.GetRandomizedTest(RandomizedTestEnum.FinancePlanSuggestedAmount);
            RandomizedTest scoreBasedTestAmount = this._randomizedTestService.Value.GetRandomizedTest(RandomizedTestEnum.FinancePlanScoreBasedSuggestedAmount);

            if (randomizedTestDuration != null && randomizedTestAmount != null ||
                randomizedTestDuration != null && scoreBasedTestAmount != null ||
                randomizedTestAmount != null && scoreBasedTestAmount != null)
            {
                // at least both tests are enabled. invalid config
                return null;
            }

            return randomizedTestDuration ?? (randomizedTestAmount ?? scoreBasedTestAmount);
        }

        private async Task SetupAnchoringTest(GuarantorDto guarantorDto, FinancialDataSummaryDto financialDataSummaryDto, FinancePlanConfigurationDto financePlanConfigurationDto)
        {
            if (!this.FinancePlanIsEligibleForAnchoringTest(financePlanConfigurationDto))
            {
                return;
            }

            RandomizedTest randomizedTest = this.GetAnchoringTests();
            if (randomizedTest == null)
            {
                // test not found, or not active
                return;
            }

            RandomizedTestGroup randomizedTestGroup = this._randomizedTestService.Value.GetRandomizedTestGroupMembership(randomizedTest, guarantorDto.VpGuarantorId);
            if (randomizedTestGroup == null)
            {
                // test not found, or not active
                return;
            }

            RandomizedTestGroupEnum randomizedTestGroupEnum = (RandomizedTestGroupEnum)randomizedTestGroup.RandomizedTestGroupId;
            if (randomizedTestGroupEnum.IsInCategory(RandomizedTestGroupEnumCategory.ControlGroup))
            {
                // control group, don't set anything
                return;
            }

            switch ((RandomizedTestEnum)randomizedTest.RandomizedTestId)
            {
                case RandomizedTestEnum.FinancePlanSuggestedAmount:
                    await this.SetupSuggestedAmountTest(guarantorDto, financialDataSummaryDto, financePlanConfigurationDto, randomizedTest, randomizedTestGroupEnum);
                    break;
                case RandomizedTestEnum.FinancePlanSuggestedDuration:
                    await this.SetupSuggestedDurationTest(guarantorDto, financialDataSummaryDto, financePlanConfigurationDto, randomizedTest, randomizedTestGroupEnum);
                    break;
                case RandomizedTestEnum.FinancePlanScoreBasedSuggestedAmount:
                    await this.SetupScoreBasedSuggestedAmount(guarantorDto, financialDataSummaryDto, financePlanConfigurationDto, randomizedTest, randomizedTestGroupEnum);
                    break;
                default:
                    return;
            }
        }

        private async Task SetupScoreBasedSuggestedAmount(GuarantorDto guarantorDto, FinancialDataSummaryDto financialDataSummaryDto, FinancePlanConfigurationDto financePlanConfigurationDto, RandomizedTest randomizedTest, RandomizedTestGroupEnum randomizedTestGroupEnum)
        {
            // group A (Score-based amount) or B (None)
            FinancePlanScoreBasedSuggestedAmountGroupConfiguration testGroupConfiguration = new FinancePlanScoreBasedSuggestedAmount(randomizedTest).GetTestGroupConfiguration(randomizedTestGroupEnum);
            if (testGroupConfiguration.FinancePlanScoreBasedPolicies == null || testGroupConfiguration.FinancePlanScoreBasedPolicies.Count <= 0)
            {
                // no score-based finance plan policy is defined
                return;
            }
            VpGuarantorScore vpGuarantorScore = this._scoringService.Value.GetScore(guarantorDto.VpGuarantorId, ScoreTypeEnum.PtpOriginal);
            if (vpGuarantorScore == null || financialDataSummaryDto == null)
            {
                return;
            }
            FinancePlanScoreBasedPolicy policy = null;
            foreach (FinancePlanScoreBasedPolicy x in testGroupConfiguration.FinancePlanScoreBasedPolicies)
            {
                decimal balance = financePlanConfigurationDto.IsCombined ? financialDataSummaryDto.BalanceInFull : financialDataSummaryDto.TotalBalance;
                if (vpGuarantorScore.Score >= x.MinPtp && 
                    vpGuarantorScore.Score <= x.MaxPtp &&
                    balance >= x.MinAmount &&
                    balance <= x.MaxAmount)
                {
                    policy = x;
                    break;
                }
            }

            if (policy == null)
            {
                // amount and ptp values are outside the policy bands
                return;
            }

            // 
            decimal monthlyPaymentAmount = policy.SuggestedAmount;

            FinancePlanCalculationParametersDto parameters = FinancePlanCalculationParametersDto.CreateForMonthlyPayment(
                guarantorDto.VpGuarantorId,
                financialDataSummaryDto.CurrentStatementId.GetValueOrDefault(0),
                monthlyPaymentAmount,
                null,
                financePlanConfigurationDto.IsCombined);

            CalculateFinancePlanTermsResponseDto response = await this._financePlanApplicationService.Value.GetTermsByMonthlyPaymentAmountAsync(parameters);

            if (response?.Terms == null || response.Terms.MonthlyPaymentAmount <= 0 || response.Terms.NumberMonthlyPayments <= 0)
            {
                // the desired suggested terms were not available to this user
                return;
            }

            financePlanConfigurationDto.SuggestedMonthlyPaymentAmount = response.Terms.MonthlyPaymentAmount;
            financePlanConfigurationDto.SuggestedNumberMonthlyPayments = response.Terms.NumberMonthlyPayments;
        }

        #endregion
    }
}
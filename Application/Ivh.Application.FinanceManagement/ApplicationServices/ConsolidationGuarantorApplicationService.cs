﻿namespace Ivh.Application.FinanceManagement.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Core.Common.Dtos;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.FinanceManagement.Consolidation.Interfaces;
    using Domain.FinanceManagement.Statement.Entities;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Jobs;
    using Ivh.Common.VisitPay.Messages.Consolidation;
    using Ivh.Common.VisitPay.Strings;
    using MassTransit;
    using NHibernate;
    
    public class ConsolidationGuarantorApplicationService : ApplicationService, IConsolidationGuarantorApplicationService
    {
        private readonly Lazy<IVpStatementService> _statementService;
        private readonly Lazy<IConsolidationGuarantorService> _consolidationGuarantorService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IVisitBalanceBaseApplicationService> _visitBalanceBaseApplicationService;
        private readonly ISession _session;
        
        public ConsolidationGuarantorApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IConsolidationGuarantorService> consolidationGuarantorService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IVpStatementService> statementService,
            Lazy<IVisitBalanceBaseApplicationService> visitBalanceBaseApplicationService,
            ISessionContext<VisitPay> sessionContext) : base(applicationServiceCommonService)
        {
            this._consolidationGuarantorService = consolidationGuarantorService;
            this._guarantorService = guarantorService;
            this._statementService = statementService;
            this._session = sessionContext.Session;
            this._visitBalanceBaseApplicationService = visitBalanceBaseApplicationService;
        }

        /// <summary>
        /// </summary>
        /// <param name="runDateTime"></param>
        /// <exception cref="System.NotImplementedException" />
        public void CheckForExpiringConsolidations(DateTime runDateTime)
        {
            IEnumerable<ConsolidationGuarantor> pendingConsolidations = this._consolidationGuarantorService.Value.GetPendingRequests();

            foreach (ConsolidationGuarantor pendingConsolidation in pendingConsolidations)
            {
                if (pendingConsolidation.MostRecentActionDate.Date.AddDays(this.Client.Value.ConsolidationExpirationDays) <= runDateTime.Date)
                {
                    this.Bus.Value.PublishMessage(new ConsolidationExpiredMessage
                    {
                        ConsolidationGuarantorId = pendingConsolidation.ConsolidationGuarantorId,
                        ExpirationDateTime = runDateTime
                    }).Wait();
                }
            }
        }

        public void SendConsolidationReminders(DateTime runDateTime)
        {
            IEnumerable<ConsolidationGuarantor> reminderConsolidations = this._consolidationGuarantorService.Value.GetPendingRequests();

            foreach (ConsolidationGuarantor reminderConsolidation in reminderConsolidations)
            {
                if (!reminderConsolidation.DateAcceptedByManagedGuarantor.HasValue
                    && reminderConsolidation.DateAcceptedByManagingGuarantor.HasValue
                    && reminderConsolidation.DateAcceptedByManagingGuarantor.Value.Date.AddDays(this.Client.Value.ConsolidationReminderDays).Date == runDateTime.Date)
                {
                    this._session.Transaction.RegisterPostCommitSuccessAction(x =>
                    {
                        this.Bus.Value.PublishMessage(new ConsolidationHouseholdRequestToManagedMessage
                        {
                            ManagedVpGuarantorId = x.ManagedGuarantor.VpGuarantorId,
                            ManagingVpGuarantorId = x.ManagingGuarantor.VpGuarantorId
                        }).Wait();
                    }, reminderConsolidation);
                }

                if (!reminderConsolidation.DateAcceptedByManagingGuarantor.HasValue
                    && reminderConsolidation.DateAcceptedByManagedGuarantor.HasValue
                    && reminderConsolidation.DateAcceptedByManagedGuarantor.Value.Date.AddDays(this.Client.Value.ConsolidationReminderDays).Date == runDateTime.Date)
                {
                    this._session.Transaction.RegisterPostCommitSuccessAction(x =>
                    {
                        this.Bus.Value.PublishMessage(new ConsolidationHouseholdRequestToManagingMessage
                        {
                            ManagedVpGuarantorId = x.ManagedGuarantor.VpGuarantorId,
                            ManagingVpGuarantorId = x.ManagingGuarantor.VpGuarantorId
                        }).Wait();
                    }, reminderConsolidation);
                }

                if (!reminderConsolidation.DateAcceptedFinancePlanTermsByManagingGuarantor.HasValue
                    && reminderConsolidation.ConsolidationGuarantorStatus == ConsolidationGuarantorStatusEnum.PendingFinancePlan
                    && reminderConsolidation.DateAcceptedByManagedGuarantor.HasValue
                    && reminderConsolidation.DateAcceptedByManagingGuarantor.HasValue
                    && reminderConsolidation.DateAcceptedByManagedGuarantor.Value.Date.AddDays(this.Client.Value.ConsolidationReminderDays).Date == runDateTime.Date)
                {
                    this._session.Transaction.RegisterPostCommitSuccessAction(x =>
                    {
                        this.Bus.Value.PublishMessage(new ConsolidationHouseholdRequestAcceptedByManagedWithFpToManagingMessage
                        {
                            ManagedVpGuarantorId = x.ManagedGuarantor.VpGuarantorId,
                            ManagingVpGuarantorId = x.ManagingGuarantor.VpGuarantorId,
                            ConsolidationRequestExpiryDay = x.MostRecentActionDate.Date.AddDays(this.Client.Value.ConsolidationExpirationDays).ToString(Format.DateFormat)
                        }).Wait();
                    }, reminderConsolidation);
                }
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="consolidationGuarantorId"></param>
        /// <param name="expirationDateTime"></param>
        public void ExpireConsolidation(int consolidationGuarantorId, DateTime expirationDateTime)
        {
            ConsolidationGuarantor consolidationGuarantor = this._consolidationGuarantorService.Value.ExpireConsolidationAsync(consolidationGuarantorId, expirationDateTime).Result;
        }

        public async Task<ConsolidationGuarantorDto> GetConsolidationGuarantorAsync(int consolidationGuarantorId, int visitPayUserId)
        {
            ConsolidationGuarantor consolidationGuarantor = await this._consolidationGuarantorService.Value.GetConsolidationGuarantorAsync(consolidationGuarantorId, visitPayUserId);
            return Mapper.Map<ConsolidationGuarantorDto>(consolidationGuarantor);
        }

        public IEnumerable<ConsolidationGuarantorDto> GetAllManagedGuarantors(int managingVisitPayUserId)
        {
            Guarantor managingGuarantor = this._guarantorService.Value.GetGuarantorEx(managingVisitPayUserId);

            return Mapper.Map<IEnumerable<ConsolidationGuarantorDto>>(this._consolidationGuarantorService.Value.GetAllManagedGuarantors(managingGuarantor));
        }

        public IEnumerable<ConsolidationGuarantorDto> GetAllManagingGuarantors(int managedVisitPayUserId)
        {
            Guarantor managedGuarantor = this._guarantorService.Value.GetGuarantorEx(managedVisitPayUserId);

            return Mapper.Map<IEnumerable<ConsolidationGuarantorDto>>(this._consolidationGuarantorService.Value.GetAllManagingGuarantors(managedGuarantor));
        }

        [Obsolete(ObsoleteDescription.VisitTransactions)]
        public IDictionary<int, decimal> GetHouseholdBalances(int managingVisitPayUserId)
        {
            // no managed guarantors
            List<ConsolidationGuarantorDto> managedGuarantors = this.GetAllManagedGuarantors(managingVisitPayUserId).Where(x => x.IsActive && !x.IsPending).ToList();
            if (!managedGuarantors.Any())
            {
                return new Dictionary<int, decimal>();
            }

            // managing guarantor not found
            Guarantor managingGuarantor = this._guarantorService.Value.GetGuarantorEx(managingVisitPayUserId);
            if (managingGuarantor == null)
            {
                return new Dictionary<int, decimal>();
            }

            List<GuarantorDto> guarantors = managedGuarantors.Select(x => x.ManagedGuarantor).ToList();
            guarantors.Insert(0, Mapper.Map<GuarantorDto>(managingGuarantor));

            IList<VpStatement> allStatements = new List<VpStatement>();
            foreach (GuarantorDto guarantorDto in guarantors)
            {
                Guarantor guarantor = this._guarantorService.Value.GetGuarantor(guarantorDto.VpGuarantorId);
                if (guarantor != null)
                {
                    VpStatement statement = this._statementService.Value.GetMostRecentStatement(guarantor);
                    if (statement != null)
                    {
                        allStatements.Add(statement);
                    }
                }
            }

            // no statements
            if (!allStatements.Any())
            {
                return new Dictionary<int, decimal>();
            }

            // payment due dates aren't synced
            if (allStatements.GroupBy(x => x.PaymentDueDate.Date).Count() > 1)
            {
                return new Dictionary<int, decimal>();
            }

            return allStatements.ToDictionary(x => x.VpStatementId, x => this._visitBalanceBaseApplicationService.Value.GetTotalBalance(x.VpGuarantorId));
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(ConsolidationExpiredMessage message, ConsumeContext<ConsolidationExpiredMessage> consumeContext)
        {
            this.ExpireConsolidation(message.ConsolidationGuarantorId, message.ExpirationDateTime);
        }

        public bool IsMessageValid(ConsolidationExpiredMessage message, ConsumeContext<ConsolidationExpiredMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion

        #region Jobs

        void IJobRunnerService<QueueConsolidationExpirationJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.CheckForExpiringConsolidations(DateTime.UtcNow.Date);
        }

        void IJobRunnerService<QueueConsolidationRemindersJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.SendConsolidationReminders(DateTime.UtcNow.Date);
        }

        #endregion
    }
}
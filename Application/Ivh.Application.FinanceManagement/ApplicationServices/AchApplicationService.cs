﻿namespace Ivh.Application.FinanceManagement.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.Core.SystemException.Interfaces;
    using Domain.FinanceManagement.Ach.Entities;
    using Domain.FinanceManagement.Ach.Interfaces;
    using Domain.FinanceManagement.Interfaces;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Statement.Entities;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Jobs;
    using Ivh.Common.VisitPay.Messages.Communication;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;
    using MassTransit;
    using NHibernate;
    using SystemException = Domain.Core.SystemException.Entities.SystemException;
    using GuestPayInterface = Domain.GuestPay.Interfaces;
    using GuestPayEntities = Domain.GuestPay.Entities;

    public class AchApplicationService : ApplicationService, IAchApplicationService
    {
        private readonly Lazy<IAchService> _achService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IVisitTransactionChangesApplicationService> _visitTransactionChangesApplicationService;
        private readonly Lazy<IPaymentReversalApplicationService> _paymentReversalApplicationService;
        private readonly Lazy<IPaymentService> _paymentService;
        private readonly Lazy<GuestPayInterface.IPaymentService> _gpPaymentService;
        private readonly ISession _session;
        private readonly Lazy<IVpStatementService> _vpStatementService;
        private readonly Lazy<ISystemExceptionService> _systemExceptionService;

        public AchApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IAchService> achService,
            Lazy<IPaymentService> paymentService,
            Lazy<GuestPayInterface.IPaymentService> gpPaymentService,
            Lazy<IVpStatementService> vpStatementService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<ISystemExceptionService> systemExceptionService,
            Lazy<IVisitTransactionChangesApplicationService> visitTransactionChangesApplicationService,
            Lazy<IPaymentReversalApplicationService> paymentReversalApplicationService,
            ISessionContext<VisitPay> sessionContext) : base(applicationServiceCommonService)
        {
            this._achService = achService;
            this._paymentService = paymentService;
            this._gpPaymentService = gpPaymentService;
            this._vpStatementService = vpStatementService;
            this._guarantorService = guarantorService;
            this._systemExceptionService = systemExceptionService;
            this._visitTransactionChangesApplicationService = visitTransactionChangesApplicationService;
            this._paymentReversalApplicationService = paymentReversalApplicationService;
            this._session = sessionContext.Session;
        }

        public void DownloadAchReturnsAndSettlements(DateTime startDate, DateTime endDate)
        {
            if (this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.PaymentsAch))
            {
                this._achService.Value.DownloadReturns(startDate, endDate);
                this._achService.Value.DownloadSettlements(startDate.AddDays(1), endDate.AddDays(1));
            }
            else
            {
                this.LoggingService.Value.Warn(() => "DownloadAchReturnsAndSettlements Job is running, but Ach is not enabled");
            }
        }

        public void PublishAchReturnsAndSettlements(DateTime startDate, DateTime endDate)
        {
            if (this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.PaymentsAch))
            {
                this.PublishReturns(startDate, endDate);
                this.PublishSettlements(startDate, endDate);
            }
            else
            {
                this.LoggingService.Value.Warn(() => "PublishAchReturnsAndSettlements Job is running, but Ach is not enabled");
            }
        }

        private void PublishReturns(DateTime startDate, DateTime endDate)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                // Group Returns By Guarantor
                IReadOnlyList<AchReturnDetail> returnDetails = this._achService.Value.GetReturnDetails(startDate, endDate, true);

                IList<Payment> allPayments = new List<Payment>();
                foreach (string individualId in returnDetails.Select(x => x.IndividualId.TrimNullSafe()).Distinct())
                {
                    allPayments.AddRange(this._paymentService.Value.GetPaymentsByTransactionId(individualId));
                }

                IList<GuestPayEntities.Payment> allGpPayments = new List<GuestPayEntities.Payment>();
                foreach (string individualId in returnDetails.Select(x => x.IndividualId.TrimNullSafe()).Distinct())
                {
                    allGpPayments.AddRange(this._gpPaymentService.Value.GetPaymentByTransactionId(individualId));
                }

                IDictionary<int, IList<AchReturnMessage>> guarantorReturnMessages = new Dictionary<int, IList<AchReturnMessage>>();
                foreach (AchReturnDetail returnDetail in returnDetails)
                {
                    int vpGuarantorId;
                    string individualId = returnDetail.IndividualId.TrimNullSafe();

                    Payment payment = allPayments.FirstOrDefault(x => x.TransactionId.TrimNullSafe() == individualId && x.PaymentMethod.IsAchType);
                    if (payment == null)
                    {
                        GuestPayEntities.Payment gpPayment = allGpPayments.FirstOrDefault(x => x.TransactionId.TrimNullSafe() == individualId && x.IsAchType);
                        if (gpPayment == null)
                        {
                            this.LoggingService.Value.Warn(() => "Payment must be of ACH PaymentMethodType: " + returnDetail.IndividualId);
                            this._achService.Value.MarkReturnDetailAsProcessed(returnDetail.AchReturnDetailId, DateTime.UtcNow);
                            continue;
                        }
                        else
                        {
                            vpGuarantorId = int.Parse(gpPayment.GuarantorNumber);
                        }
                    }
                    else
                    {
                        vpGuarantorId = payment.Guarantor.VpGuarantorId;
                    }

                    if (!guarantorReturnMessages.ContainsKey(vpGuarantorId))
                    {
                        guarantorReturnMessages[vpGuarantorId] = new List<AchReturnMessage>();
                    }

                    AchReturnMessage message = new AchReturnMessage
                    {
                        TransactionId = returnDetail.IndividualId.TrimNullSafe(),
                        AchReturnDetailId = returnDetail.AchReturnDetailId,
                        ReturnReasonCode = returnDetail.ReturnReasonCode
                    };

                    if (!guarantorReturnMessages[vpGuarantorId].Any(x =>
                        x.TransactionId == message.TransactionId &&
                        x.AchReturnDetailId == message.AchReturnDetailId &&
                        x.ReturnReasonCode == message.ReturnReasonCode))
                    {
                        guarantorReturnMessages[vpGuarantorId].Add(message);
                    }
                }

                this._session.Transaction.RegisterPostCommitSuccessAction((grm) =>
                {
                    // publish returns
                    foreach (KeyValuePair<int, IList<AchReturnMessage>> guarantorReturnMessage in grm)
                    {
                        this.Bus.Value.PublishMessage(new AchReturnMessageGuarantor
                        {
                            VpGuarantorId = guarantorReturnMessage.Key,
                            Messages = guarantorReturnMessage.Value
                        }).Wait();
                    }
                }, guarantorReturnMessages);

                unitOfWork.Commit();
            }
        }

        private void PublishSettlements(DateTime startDate, DateTime endDate)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                // Group settlements By Guarantor
                IReadOnlyList<AchSettlementDetail> settlementDetails = this._achService.Value.GetSettlementDetails(startDate, endDate, true);

                IList<Payment> allPayments = new List<Payment>();
                foreach (string individualId in settlementDetails.Select(x => x.IndividualId.TrimNullSafe()).Distinct())
                {
                    allPayments.AddRange(this._paymentService.Value.GetPaymentsByTransactionId(individualId));
                }

                IList<GuestPayEntities.Payment> allGpPayments = new List<GuestPayEntities.Payment>();
                foreach (string individualId in settlementDetails.Select(x => x.IndividualId.TrimNullSafe()).Distinct())
                {
                    allGpPayments.AddRange(this._gpPaymentService.Value.GetPaymentByTransactionId(individualId));
                }

                IDictionary<int, List<AchSettlementMessage>> guarantorSettlementMessages = new Dictionary<int, List<AchSettlementMessage>>();
                foreach (AchSettlementDetail settlementDetail in settlementDetails)
                {
                    int vpGuarantorId;
                    string individualId = settlementDetail.IndividualId.TrimNullSafe();

                    Payment payment = allPayments.FirstOrDefault(x => x.TransactionId.TrimNullSafe() == individualId && x.PaymentMethod.IsAchType);
                    if (payment == null)
                    {
                        GuestPayEntities.Payment gpPayment = allGpPayments.FirstOrDefault(x => x.TransactionId.TrimNullSafe() == individualId && x.IsAchType);
                        if (gpPayment == null)
                        {
                            this.LoggingService.Value.Warn(() => "Payment must be of ACH PaymentMethodType: " + settlementDetail.IndividualId);
                            this._achService.Value.MarkSettlementDetailAsProcessed(settlementDetail.AchSettlementDetailId, DateTime.UtcNow);
                            continue;
                        }
                        else
                        {
                            vpGuarantorId = int.Parse(gpPayment.GuarantorNumber);
                        }
                    }
                    else
                    {
                        vpGuarantorId = payment.Guarantor.VpGuarantorId;
                    }

                    if (!guarantorSettlementMessages.ContainsKey(vpGuarantorId))
                    {
                        guarantorSettlementMessages[vpGuarantorId] = new List<AchSettlementMessage>();
                    }

                    AchSettlementMessage message = new AchSettlementMessage
                    {
                        TransactionId = settlementDetail.IndividualId.TrimNullSafe(),
                        AchSettlementDetailId = settlementDetail.AchSettlementDetailId
                    };

                    if (!guarantorSettlementMessages[vpGuarantorId].Any(x =>
                        x.TransactionId == message.TransactionId &&
                        x.AchSettlementDetailId == message.AchSettlementDetailId))
                    {
                        guarantorSettlementMessages[vpGuarantorId].Add(message);
                    }
                }

                this._session.Transaction.RegisterPostCommitSuccessAction((gsm) =>
                {
                    // publish settlements
                    foreach (KeyValuePair<int, List<AchSettlementMessage>> guarantorSettlementMessage in gsm)
                    {
                        this.Bus.Value.PublishMessage(new AchSettlementMessageGuarantor
                        {
                            VpGuarantorId = guarantorSettlementMessage.Key,
                            Messages = guarantorSettlementMessage.Value
                        }).Wait();
                    }
                }, guarantorSettlementMessages);

                unitOfWork.Commit();
            }
        }

        public void ProcessAchReturn(AchReturnMessageGuarantor achReturnMessageGuarantor)
        {
            if (achReturnMessageGuarantor.Messages.IsNullOrEmpty())
            {
                return;
            }

            this.ProcessVpAchReturn(achReturnMessageGuarantor);
            this.ProcessGpAchReturn(achReturnMessageGuarantor);
        }

        public void ProcessGpAchReturn(AchReturnMessageGuarantor achReturnMessageGuarantor)
        {
            foreach (AchReturnMessage achReturnMessage in achReturnMessageGuarantor.Messages)
            {
                using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
                {
                    AchReturnDetail achReturnDetail = this._achService.Value.GetReturnDetail(achReturnMessage.AchReturnDetailId);
                    if (!this._achService.Value.IsNocReturn(achReturnDetail))
                    {
                        IList<GuestPayEntities.Payment> activePendingAchApprovalPayments = new List<GuestPayEntities.Payment>();
                        IList<GuestPayEntities.Payment> payments = this._gpPaymentService.Value.GetPaymentByTransactionId(achReturnMessage.TransactionId, new List<PaymentStatusEnum>
                        {
                            PaymentStatusEnum.ActivePendingACHApproval,
                            PaymentStatusEnum.ClosedPaid
                        });

                        decimal paymentsActualPaymentAmountSum = payments.Sum(x => x.PaymentAmount);

                        foreach (GuestPayEntities.Payment payment in payments)
                        {
                            if (!payment.IsAchType)
                            {
                                this.LoggingService.Value.Warn(() => "Payment must be of ACH PaymentMethodType: " + achReturnMessage.TransactionId);
                                continue;
                            }

                            switch (payment.PaymentStatus)
                            {
                                case PaymentStatusEnum.ClosedCancelled:
                                    break; // DO NOTHING. ALREADY CANCELLED

                                case PaymentStatusEnum.ActivePendingACHApproval:
                                    this.ReturnAchGuestPayPayment(payment, achReturnDetail, paymentsActualPaymentAmountSum, activePendingAchApprovalPayments);
                                    break;

                                case PaymentStatusEnum.ClosedPaid:

                                    bool hasOptimisticSettlementEnabled = this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureOptimisticAchSettlementIsEnabled);

                                    if (hasOptimisticSettlementEnabled)
                                    {
                                        IList<AchSettlementDetail> achSettlementDetails = this._achService.Value.GetAchSettlementDetailsByTransactionId(payment.TransactionId);
                                        bool hasFileSettlement = achSettlementDetails.Any(x => x.AchFile != null);

                                        if (hasFileSettlement)
                                        {
                                            this.CreateSystemException(payment, SystemExceptionTypeEnum.GpAchReturnedAfterSettlement);
                                        }
                                        else // Must be an optimistic ACH settlement, so it can be returned since no file based settlement was found
                                        {
                                            payment.PaymentStatus = PaymentStatusEnum.ActivePendingACHApproval;
                                            this.ReturnAchGuestPayPayment(payment, achReturnDetail, paymentsActualPaymentAmountSum, activePendingAchApprovalPayments);
                                        }
                                    }
                                    else
                                    {
                                        this.CreateSystemException(payment, SystemExceptionTypeEnum.GpAchReturnedAfterSettlement);
                                    }

                                    break;

                                case PaymentStatusEnum.ActivePending:
                                case PaymentStatusEnum.ActiveGatewayError:
                                case PaymentStatusEnum.ActivePendingLinkFailure:
                                case PaymentStatusEnum.ActivePendingGuarantorAction:
                                case PaymentStatusEnum.ClosedFailed:
                                default:
                                    throw new InvalidOperationException("Payment must be of status ClosedCancelled, ActivePendingACHApproval or ClosedPaid. Status was: " + payment.PaymentStatus);
                            }
                        }
                    }

                    this._achService.Value.MarkReturnDetailAsProcessed(achReturnMessage.AchReturnDetailId, DateTime.UtcNow);

                    unitOfWork.Commit();
                }
            }
        }

        private void CreateSystemException(GuestPayEntities.Payment payment, SystemExceptionTypeEnum systemExceptionType)
        {
            this._systemExceptionService.Value.LogSystemException(new SystemException
            {
                SystemExceptionType = systemExceptionType,
                SystemExceptionDescription = "Payment TransactionId: " + payment.TransactionId,
                TransactionId = payment.TransactionId,
                GuestPayPaymentId = payment.PaymentId,
                GpGuarantorId = payment.PaymentUnitAuthentication.PaymentUnitAuthenticationId
            });
        }

        private void ReturnAchGuestPayPayment(GuestPayEntities.Payment payment, AchReturnDetail achReturnDetail, decimal paymentsActualPaymentAmountSum, IList<GuestPayEntities.Payment> activePendingAchApprovalPayments)
        {
            if (achReturnDetail.AmountNumeric < paymentsActualPaymentAmountSum)
            {
                this._systemExceptionService.Value.LogSystemException(new SystemException
                {
                    SystemExceptionType = SystemExceptionTypeEnum.GpAchReturnLessThanOriginalPaymentAmount,
                    SystemExceptionDescription = "Payment TransactionId: " + payment.TransactionId,
                    TransactionId = payment.TransactionId,
                    GuestPayPaymentId = payment.PaymentId,
                    GpGuarantorId = payment.PaymentUnitAuthentication.PaymentUnitAuthenticationId
                });
            }
            activePendingAchApprovalPayments.Add(payment);

            payment.PaymentStatus = PaymentStatusEnum.ClosedCancelled;
            payment.PaymentAmount = achReturnDetail.AmountNumeric;
            this._gpPaymentService.Value.InsertOrUpdate(payment);

            bool hasOptimisticSettlementEnabled = this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureOptimisticAchSettlementIsEnabled);
            if (!hasOptimisticSettlementEnabled)
            {
                // Need to post a settled amount that the return payment below can offset to make the payment total = $0.00
                this._gpPaymentService.Value.PublishGuestPayPayment(payment); 
            }
            
            GuestPayEntities.Payment returnPayment = new GuestPayEntities.Payment
            {
                PaymentStatus = PaymentStatusEnum.ClosedCancelled,
                PaymentMethodType = payment.PaymentMethodType,
                PaymentAmount = decimal.Negate(achReturnDetail.AmountNumeric),
                PaymentDate = DateTime.UtcNow,
                EmailAddress = payment.EmailAddress,
                GuarantorNumber = payment.GuarantorNumber,
                GuarantorFirstName = payment.GuarantorFirstName,
                GuarantorLastName = payment.GuarantorLastName,
                PaymentUnitAuthentication = payment.PaymentUnitAuthentication,
                PaymentId = payment.PaymentId,
                MerchantAccount = payment.MerchantAccount,
                StatementIdentifierId = payment.StatementIdentifierId,
                PaymentSystemType = payment.PaymentSystemType
            };

            this._gpPaymentService.Value.PublishGuestPayPayment(returnPayment, AchSettlementTypeEnum.FileReturn);

            this._session.Transaction.RegisterPostCommitSuccessAction((p) =>
            {
                this.Bus.Value.PublishMessage(new SendGuestPayPaymentFailureEmailMessage
                {
                    EmailAddress = p.EmailAddress,
                    GuarantorFirstName = p.PaymentAccountHolderName,
                    PaymentUnitId = 2,
                    TransactionId = p.TransactionId
                }).Wait();
            }, payment);
        }

        public void ProcessVpAchReturn(AchReturnMessageGuarantor achReturnMessageGuarantor)
        {
            foreach (AchReturnMessage achReturnMessage in achReturnMessageGuarantor.Messages)
            {
                using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
                {
                    AchReturnDetail achReturnDetail = this._achService.Value.GetReturnDetail(achReturnMessage.AchReturnDetailId);
                    if (!this._achService.Value.IsNocReturn(achReturnDetail))
                    {
                        IList<Payment> returnedPayments = new List<Payment>();
                        IList<Payment> payments = this._paymentService.Value.GetPaymentsByTransactionId(achReturnMessage.TransactionId, new List<PaymentStatusEnum>
                        {
                            PaymentStatusEnum.ActivePendingACHApproval,
                            PaymentStatusEnum.ClosedPaid
                        });

                        decimal paymentsActualPaymentAmountSum = payments.Sum(x => x.ActualPaymentAmount);

                        foreach (Payment payment in payments)
                        {
                            if (!payment.PaymentMethod.IsAchType)
                            {
                                this.LoggingService.Value.Warn(() => "Payment must be of ACH PaymentMethodType: " + achReturnMessage.TransactionId);
                                continue;
                            }

                            switch (payment.PaymentStatus)
                            {
                                case PaymentStatusEnum.ClosedCancelled:
                                    break; // DO NOTHING. ALREADY CANCELLED

                                case PaymentStatusEnum.ActivePendingACHApproval:
                                    this.ReturnAchPayment(payment, achReturnMessage, achReturnDetail, paymentsActualPaymentAmountSum);
                                    returnedPayments.Add(payment);
                                    break;

                                case PaymentStatusEnum.ClosedPaid:

                                    bool hasOptimisticSettlementEnabled = this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureOptimisticAchSettlementIsEnabled);

                                    if (hasOptimisticSettlementEnabled)
                                    {
                                        IList<AchSettlementDetail> achSettlementDetails = this._achService.Value.GetAchSettlementDetailsByTransactionId(payment.TransactionId);
                                        bool hasFileSettlement = achSettlementDetails.Any(x => x.AchFile != null);

                                        if (hasFileSettlement)
                                        {
                                            this.CreateSystemException(payment, SystemExceptionTypeEnum.AchReturnedAfterSettlement);
                                        }
                                        else // Must be an optimistic ACH settlement, so it can be returned since no file based settlement was found
                                        {
                                            payment.PaymentStatus = PaymentStatusEnum.ActivePendingACHApproval;
                                            this.ReturnAchPayment(payment, achReturnMessage, achReturnDetail, paymentsActualPaymentAmountSum);
                                            returnedPayments.Add(payment);
                                        }
                                    }
                                    else
                                    {
                                        this.CreateSystemException(payment, SystemExceptionTypeEnum.AchReturnedAfterSettlement);
                                    }

                                    break;

                                case PaymentStatusEnum.ActivePending:
                                case PaymentStatusEnum.ActiveGatewayError:
                                case PaymentStatusEnum.ActivePendingLinkFailure:
                                case PaymentStatusEnum.ActivePendingGuarantorAction:
                                case PaymentStatusEnum.ClosedFailed:
                                default:
                                    throw new InvalidOperationException("Payment must be of status ClosedCancelled, ActivePendingACHApproval or ClosedPaid. Status was: " + payment.PaymentStatus);
                            }
                        }

                        if (returnedPayments.Any())
                        {
                            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(achReturnMessageGuarantor.VpGuarantorId);
                            VpStatement mostRecentStatement = this._vpStatementService.Value.GetMostRecentStatement(guarantor);
                            if (mostRecentStatement != null)
                            {
                                DateTime dateToProcess = returnedPayments.Select(s => s.ScheduledPaymentDate).Min();
                                List<Payment> scheduledPayments = returnedPayments.Where(w => !w.IsRecurringPayment()).ToList();
                                List<Payment> recurringPayments = returnedPayments.Where(w => w.IsRecurringPayment()).ToList();

                                string logMessage = $"AchApplicationService::ProcessVpAchReturn - Payments Returned: {string.Join(", ", returnedPayments.Select(x => x.PaymentId))}";
                                this._vpStatementService.Value.MarkStatementAsQueued(mostRecentStatement.VpStatementId, mostRecentStatement.VpGuarantorId, logMessage);
                                this._paymentService.Value.PostProcessPayment(
                                    guarantor,
                                    mostRecentStatement,
                                    dateToProcess,
                                    scheduledPayments,
                                    recurringPayments);
                            }
                        }
                    }

                    this._achService.Value.MarkReturnDetailAsProcessed(achReturnMessage.AchReturnDetailId, DateTime.UtcNow);

                    unitOfWork.Commit();
                }
            }
        }

        private void ReturnAchPayment(Payment payment, AchReturnMessage achReturnMessage, AchReturnDetail achReturnDetail, decimal paymentsActualPaymentAmountSum)
        {
            if (achReturnDetail.AmountNumeric == paymentsActualPaymentAmountSum)
            {
                this._paymentService.Value.ReturnAchPayment(payment, achReturnMessage.ReturnReasonCode, achReturnMessage.TransactionId);

                this.ReageFinancePlan(payment);

                // VP-5131 - leaving here for future reference
                // post process (called below) sends this email, it sends twice when called here
                /*if (returnsEmailed.Count(x => x.VpGuarantorId == payment.Guarantor.VpGuarantorId) == 0)
                {
                    this._paymentService.Value.PublishPaymentFailedEmail(payment.Guarantor.VpGuarantorId);
                    returnsEmailed.Add(payment.Guarantor);
                }*/
            }
            else
            {
                this.CreateSystemException(payment, SystemExceptionTypeEnum.AchReturnLessThanOriginalPaymentAmount);
            }
        }

        private void CreateSystemException(Payment payment, SystemExceptionTypeEnum systemExceptionType)
        {
            this._systemExceptionService.Value.LogSystemException(new SystemException
            {
                SystemExceptionType = systemExceptionType,
                SystemExceptionDescription = "Payment TransactionId: " + payment.TransactionId,
                TransactionId = payment.TransactionId,
                PaymentId = payment.PaymentId,
                VpGuarantorId = payment.Guarantor.VpGuarantorId
            });
        }

        public void ProcessAchSettlement(AchSettlementMessageGuarantor achSettlementMessageGuarantor)
        {
            if (achSettlementMessageGuarantor.Messages.IsNullOrEmpty())
            {
                return;
            }

            this.ProcessVpAchSettlement(achSettlementMessageGuarantor);
            this.ProcessGpAchSettlement(achSettlementMessageGuarantor);
        }

        private void ProcessGpAchSettlement(AchSettlementMessageGuarantor achSettlementMessageGuarantor)
        {
            bool hasOptimisticSettlementEnabled = this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureOptimisticAchSettlementIsEnabled);

            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                IList<GuestPayEntities.Payment> allPayments = new List<GuestPayEntities.Payment>();
                foreach (string transactionId in achSettlementMessageGuarantor.Messages.Select(x => x.TransactionId).Distinct())
                {
                    allPayments.AddRange(this._gpPaymentService.Value.GetPaymentByTransactionId(transactionId, new List<PaymentStatusEnum>
                    {
                        PaymentStatusEnum.ActivePendingACHApproval,
                        PaymentStatusEnum.ClosedFailed,
                        PaymentStatusEnum.ClosedPaid,
                        PaymentStatusEnum.ClosedCancelled
                    }));
                }

                foreach (GuestPayEntities.Payment payment in allPayments)
                {
                    if (!payment.IsAchType)
                    {
                        this.LoggingService.Value.Warn(() => "Payment must be of ACH PaymentMethodType: " + payment.TransactionId);
                        continue;
                    }

                    AchSettlementMessage achSettlementMessage = achSettlementMessageGuarantor.Messages.FirstOrDefault(x => x.TransactionId == payment.TransactionId);
                    AchSettlementDetail achSettlementDetail = null;
                    AchSettlementTypeEnum achSettlementType = AchSettlementTypeEnum.None;
                    if (achSettlementMessage != null)
                    {
                        achSettlementDetail = this._achService.Value.GetSettlementDetail(achSettlementMessage.AchSettlementDetailId);
                        if (achSettlementDetail != null)
                        {
                            achSettlementType = achSettlementDetail.AchFile == null ? AchSettlementTypeEnum.OptimisticSettlement : AchSettlementTypeEnum.FileSettlement;
                        }
                    }

                    switch (payment.PaymentStatus)
                    {
                        case PaymentStatusEnum.ClosedPaid:

                            if (hasOptimisticSettlementEnabled)
                            {
                                // Need to have file based ACH events sent to HS since we already optimistically settled
                                if (achSettlementType.IsInCategory(AchSettlementTypeEnumCategory.File))
                                {
                                    this._gpPaymentService.Value.PublishGuestPayPayment(payment, achSettlementType);
                                }
                            }

                            break;

                        case PaymentStatusEnum.ActivePendingACHApproval:

                            if (achSettlementMessage == null)
                            {
                                throw new InvalidOperationException("Could not resolve AchSettlementMessage by TransactionId: " + payment.TransactionId);
                            }

                            if (achSettlementDetail == null)
                            {
                                throw new InvalidOperationException("Could not find AchSettlementDetail by AchSettlementDetailId: " + achSettlementMessage.AchSettlementDetailId);
                            }

                            if (this._achService.Value.IsReturn(achSettlementDetail))
                            {
                                this.LoggingService.Value.Warn(() => "Ach Settled as return before return record processed. TransactionId: {0} AchSettlementDetailId: {1}", achSettlementMessage.TransactionId, achSettlementMessage.AchSettlementDetailId);
                            }
                            else
                            {
                                payment.PaymentStatus = PaymentStatusEnum.ClosedPaid;
                                this._gpPaymentService.Value.InsertOrUpdate(payment);
                                this._gpPaymentService.Value.PublishGuestPayPayment(payment, achSettlementType);
                            }

                            break;

                        case PaymentStatusEnum.ClosedFailed:
                        case PaymentStatusEnum.ClosedCancelled:
                            // VPNG-16076 The logic of this exception should exclude the settlements with status 'Return'
                            bool hasReturnSettlement = this.PaymentHasReturnSettlement(payment.TransactionId, achSettlementMessageGuarantor);
                            if (!hasReturnSettlement)
                            {
                                this._systemExceptionService.Value.LogSystemException(new SystemException
                                {
                                    SystemExceptionType = SystemExceptionTypeEnum.GpAchSettledAfterReturn,
                                    SystemExceptionDescription = "Payment TransactionId: " + payment.TransactionId,
                                    TransactionId = payment.TransactionId,
                                    GuestPayPaymentId = payment.PaymentId,
                                    GpGuarantorId = payment.PaymentUnitAuthentication.PaymentUnitAuthenticationId
                                });
                            }
                            break;

                        default:
                            throw new InvalidOperationException("Payment must be of status ActivePendingACHApproval or ClosedFailed. Status was: " + payment.PaymentStatus);
                    }
                }

                unitOfWork.Commit();
            }
        }

        private void ProcessVpAchSettlement(AchSettlementMessageGuarantor achSettlementMessageGuarantor)
        {
            bool hasOptimisticSettlementEnabled = this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureOptimisticAchSettlementIsEnabled);

            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                IList<Payment> allPayments = new List<Payment>();
                foreach (string transactionId in achSettlementMessageGuarantor.Messages.Select(x => x.TransactionId).Distinct())
                {
                    allPayments.AddRange(this._paymentService.Value.GetPaymentsByTransactionId(transactionId, new List<PaymentStatusEnum>
                    {
                        PaymentStatusEnum.ActivePendingACHApproval,
                        PaymentStatusEnum.ClosedFailed,
                        PaymentStatusEnum.ClosedPaid
                    }));
                }

                IList<Payment> settledPayments = new List<Payment>();

                foreach (Payment payment in allPayments)
                {
                    if (!payment.PaymentMethod.IsAchType)
                    {
                        this.LoggingService.Value.Warn(() => "Payment must be of ACH PaymentMethodType: " + payment.TransactionId);
                        continue;
                    }

                    AchSettlementMessage achSettlementMessage = achSettlementMessageGuarantor.Messages.FirstOrDefault(x => x.TransactionId == payment.TransactionId);
                    AchSettlementDetail achSettlementDetail = null;
                    AchSettlementTypeEnum achSettlementType = AchSettlementTypeEnum.None;
                    if (achSettlementMessage != null)
                    {
                        achSettlementDetail = this._achService.Value.GetSettlementDetail(achSettlementMessage.AchSettlementDetailId);
                        if (achSettlementDetail != null)
                        {
                            achSettlementType = achSettlementDetail.AchFile == null ? AchSettlementTypeEnum.OptimisticSettlement : AchSettlementTypeEnum.FileSettlement;
                        }
                    }

                    switch (payment.PaymentStatus)
                    {
                        case PaymentStatusEnum.ActivePendingACHApproval:

                            if (achSettlementMessage == null)
                            {
                                throw new InvalidOperationException("Could not resolve AchSettlementMessage by TransactionId: " + payment.TransactionId);
                            }

                            if (achSettlementDetail == null)
                            {
                                throw new InvalidOperationException("Could not find AchSettlementDetail by AchSettlementDetailId: " + achSettlementMessage.AchSettlementDetailId);
                            }

                            if (this._achService.Value.IsReturn(achSettlementDetail))
                            {
                                this.LoggingService.Value.Warn(() => "Ach Settled as return before return record processed. TransactionId: {0} AchSettlementDetailId: {1}", achSettlementMessage.TransactionId, achSettlementMessage.AchSettlementDetailId);
                            }
                            else
                            {
                                // settle the payment
                                this._paymentService.Value.SettleAchPayment(payment, achSettlementType);
                                settledPayments.Add(payment);
                            }

                            break;

                        case PaymentStatusEnum.ClosedFailed:

                            // VPNG-10782 A transaction should not be considered as settled if ACH.SettlementDetail.Status = Return
                            bool hasReturnSettlement = this.PaymentHasReturnSettlement(payment.TransactionId, achSettlementMessageGuarantor);
                            if (!hasReturnSettlement)
                            {
                                this._systemExceptionService.Value.LogSystemException(new SystemException
                                {
                                    SystemExceptionType = SystemExceptionTypeEnum.AchSettledAfterReturn,
                                    SystemExceptionDescription = "Payment TransactionId: " + payment.TransactionId,
                                    TransactionId = payment.TransactionId,
                                    PaymentId = payment.PaymentId,
                                    VpGuarantorId = payment.Guarantor.VpGuarantorId
                                });
                            }

                            this.LoggingService.Value.Warn(() => $"{nameof(AchApplicationService)} - Attempted to settle payment in ClosedFailed: {payment.TransactionId}");

                            break;

                        case PaymentStatusEnum.ClosedPaid:

                            if (hasOptimisticSettlementEnabled)
                            {
                                bool isOptimisticSettlement = achSettlementType == AchSettlementTypeEnum.OptimisticSettlement;
                                if (isOptimisticSettlement)
                                {
                                    this.LoggingService.Value.Info(() => $"{nameof(AchApplicationService)} - Optimistic settled payment exists already in ClosedPaid: {payment.TransactionId}");
                                }

                                // Need to have file based ACH events sent to HS since we already optimistically settled
                                if (achSettlementType.IsInCategory(AchSettlementTypeEnumCategory.File))
                                {
                                    this._paymentService.Value.SettleAchPayment(payment, achSettlementType);
                                }
                            }
                            else
                            {
                                this.LoggingService.Value.Warn(() => $"{nameof(AchApplicationService)} - Attempted to settle payment in ClosedPaid: {payment.TransactionId}");
                            }

                            break; // do nothing....already settled.

                        default:
                            throw new InvalidOperationException("Payment must be of status ActivePendingACHApproval or ClosedFailed. Status was: " + payment.PaymentStatus);
                    }
                }

                // publish that visit ach amounts changed.
                List<int> allVisitsthatWereSettled = allPayments.SelectMany(x => x.PaymentAllocations).Select(x => x.PaymentVisit.VisitId).Distinct().ToList();

                // todo: despite the name of this message, it appears it's only used for Raging visits?
                this._visitTransactionChangesApplicationService.Value.CreateVisitReconciliationBalanceChangeMessagesFromVisits(allVisitsthatWereSettled);

                // mark all settlements processed
                foreach (int achSettlementDetailId in achSettlementMessageGuarantor.Messages.Select(x => x.AchSettlementDetailId).Distinct())
                {
                    this._achService.Value.MarkSettlementDetailAsProcessed(achSettlementDetailId, DateTime.UtcNow);
                }

                // run post processing on payments that were settled in the foreach above
                if (settledPayments.Any())
                {
                    Guarantor guarantor = settledPayments.First().Guarantor;
                    VpStatement mostRecentStatement = this._vpStatementService.Value.GetMostRecentStatement(guarantor);
                    if (mostRecentStatement != null)
                    {
                        DateTime dateToProcess = settledPayments.Select(s => s.ScheduledPaymentDate).Min();
                        List<Payment> scheduledPayments = settledPayments.Where(w => !w.IsRecurringPayment()).ToList();
                        List<Payment> recurringPayments = settledPayments.Where(w => w.IsRecurringPayment()).ToList();

                        string logMessage = $"AchApplicationService::ProcessVpAchSettlement - Settled Payments: {string.Join(", ", settledPayments.Select(x => x.PaymentId))}";
                        this._vpStatementService.Value.MarkStatementAsQueued(mostRecentStatement.VpStatementId, mostRecentStatement.VpGuarantorId, logMessage);
                        this._paymentService.Value.PostProcessPayment(
                            guarantor,
                            mostRecentStatement,
                            dateToProcess,
                            scheduledPayments,
                            recurringPayments);
                    }
                }

                unitOfWork.Commit();
            }
        }

        private bool PaymentHasReturnSettlement(string transactionId, AchSettlementMessageGuarantor achSettlementMessageGuarantor)
        {
            AchSettlementMessage achSettlementMessageForPayment = achSettlementMessageGuarantor.Messages.FirstOrDefault(x => x.TransactionId == transactionId);
            if (achSettlementMessageForPayment != null)
            {
                AchSettlementDetail achSettlementDetail = this._achService.Value.GetSettlementDetail(achSettlementMessageForPayment.AchSettlementDetailId);
                if (achSettlementDetail != null)
                {
                    return this._achService.Value.IsReturn(achSettlementDetail);
                }
            }
            return false;
        }

        public void ProcessAchReturnAfterSettlement(string transactionId, int vpGuarantorId)
        {
            IList<Payment> returnedPayments = new List<Payment>();
            IList<Payment> payments = this._paymentService.Value.GetPaymentsByTransactionId(transactionId, new List<PaymentStatusEnum>
            {
                PaymentStatusEnum.ClosedPaid
            });

            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                foreach (Payment payment in payments)
                {
                    if (!payment.PaymentMethod.IsAchType)
                    {
                        this.LoggingService.Value.Warn(() => "Payment must be of ACH PaymentMethodType: " + transactionId);
                        continue;
                    }

                    switch (payment.PaymentStatus)
                    {
                        case PaymentStatusEnum.ClosedPaid:
                            this._paymentService.Value.ReturnAchPaymentAfterSettlement(payment, transactionId);
                            this.ReageFinancePlan(payment);
                            returnedPayments.Add(payment);
                            break;
                        default:
                            throw new InvalidOperationException("Payment must be of status ClosedPaid. Status was: " + payment.PaymentStatus);
                    }
                }

                if (!returnedPayments.Any())
                {
                    return;
                }

                //Publish that visit ach amounts changed.
                List<int> allSettledVisits = returnedPayments.SelectMany(x => x.PaymentAllocations).Select(x => x.PaymentVisit.VisitId).Distinct().ToList();
                this._visitTransactionChangesApplicationService.Value.CreateVisitReconciliationBalanceChangeMessagesFromVisits(allSettledVisits);

                Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
                VpStatement mostRecentStatement = this._vpStatementService.Value.GetMostRecentStatement(guarantor);
                if (mostRecentStatement == null)
                {
                    return;
                }

                DateTime dateToProcess = returnedPayments.Select(s => s.ScheduledPaymentDate).Min();
                List<Payment> scheduledPayments = returnedPayments.Where(w => !w.IsRecurringPayment()).ToList();
                List<Payment> recurringPayments = returnedPayments.Where(w => w.IsRecurringPayment()).ToList();

                string logMessage = $"AchApplicationService::ProcessAchReturnAfterSettlement - Payments Returned: {string.Join(", ", returnedPayments.Select(x => x.PaymentId))}";
                this._vpStatementService.Value.MarkStatementAsQueued(mostRecentStatement.VpStatementId, mostRecentStatement.VpGuarantorId, logMessage);

                this._paymentService.Value.PostProcessPayment(
                    guarantor,
                    mostRecentStatement,
                    dateToProcess,
                    scheduledPayments,
                    recurringPayments);

                unitOfWork.Commit();
            }
        }

        public void AddAchFile(AchFileDto achFileDto)
        {
            AchFile achFile = Mapper.Map<AchFile>(achFileDto);
            this._achService.Value.AddAchFile(achFile);
        }

        public AchReturnCodeEnum GetReturnCode(string code)
        {
            return this._achService.Value.GetReturnCode(code);
        }

        public string GetReturnCode(AchReturnCodeEnum code)
        {
            return this._achService.Value.GetReturnCode(code);
        }

        private void ReageFinancePlan(Payment payment)
        {
            List<int> financePlanIds = payment.PaymentAllocations.Where(x => x.FinancePlanId.HasValue)
                .Select(x => x.FinancePlanId.Value)
                .ToList();

            if (!financePlanIds.Any())
            {
                return;
            }

            int vpGuarantorId = payment.Guarantor.VpGuarantorId;
            IList<int> reversalPaymentIds = new List<int> { payment.PaymentId };
            foreach (int financePlanId in financePlanIds)
            {
                this._paymentReversalApplicationService.Value.ReageFinancePlanPaymentBuckets(reversalPaymentIds, financePlanId, vpGuarantorId);
            }
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(AchReturnMessageGuarantor message, ConsumeContext<AchReturnMessageGuarantor> consumeContext)
        {
            this.ProcessAchReturn(message);
        }

        public bool IsMessageValid(AchReturnMessageGuarantor message, ConsumeContext<AchReturnMessageGuarantor> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(AchSettlementMessageGuarantor message, ConsumeContext<AchSettlementMessageGuarantor> consumeContext)
        {
            this.ProcessAchSettlement(message);
        }

        public bool IsMessageValid(AchSettlementMessageGuarantor message, ConsumeContext<AchSettlementMessageGuarantor> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion

        #region Jobs

        void IJobRunnerService<QueueAchDownloadJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.DownloadAchReturnsAndSettlements(begin, end);
        }

        void IJobRunnerService<QueueAchPublishJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.PublishAchReturnsAndSettlements(begin, begin.AddDays(1));
        }

        #endregion
    }
}
﻿namespace Ivh.Application.FinanceManagement.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Content.Common.Dtos;
    using Content.Common.Interfaces;
    using Core.Common.Dtos;
    using Domain.User.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Interfaces;
    using Domain.FinanceManagement.Statement.Entities;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.Visit.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.EventJournal;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using MassTransit;
    using NHibernate;

    public class ReconfigureFinancePlanApplicationService : ApplicationService, IReconfigureFinancePlanApplicationService
    {
        private readonly Lazy<IAmortizationService> _amortizationService;
        private readonly Lazy<IFinancePlanService> _financePlanService;
        private readonly Lazy<IFinancePlanOfferService> _financePlanOfferService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IPaymentService> _paymentService;
        private readonly Lazy<IInterestService> _interestService;
        private readonly Lazy<IVisitPayUserJournalEventService> _userJournalEventService;
        private readonly Lazy<IContentApplicationService> _contentApplicationService;
        private readonly Lazy<ICreditAgreementApplicationService> _creditAgreementApplicationService;
        private readonly ISession _session;
        private readonly Lazy<IVpStatementService> _statementService;
        private readonly Lazy<IVisitService> _visitService;
        private readonly Lazy<IVisitPayUserService> _visitPayUserService;

        public ReconfigureFinancePlanApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IAmortizationService> amortizationService,
            Lazy<IFinancePlanService> financePlanService,
            Lazy<IFinancePlanOfferService> financePlanOfferService,
            Lazy<IVpStatementService> statementService,
            Lazy<IGuarantorService> guarantorService,
            ISessionContext<VisitPay> sessionContext,
            Lazy<IPaymentService> paymentService,
            Lazy<IInterestService> interestService,
            Lazy<IVisitPayUserJournalEventService> userJournalEventService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<ICreditAgreementApplicationService> creditAgreementApplicationService,
            Lazy<IVisitService> visitService,
            Lazy<IVisitPayUserService> visitPayUserService) : base(applicationServiceCommonService)
        {
            this._amortizationService = amortizationService;
            this._financePlanService = financePlanService;
            this._financePlanOfferService = financePlanOfferService;
            this._statementService = statementService;
            this._session = sessionContext.Session;
            this._guarantorService = guarantorService;
            this._paymentService = paymentService;
            this._interestService = interestService;
            this._userJournalEventService = userJournalEventService;
            this._contentApplicationService = contentApplicationService;
            this._creditAgreementApplicationService = creditAgreementApplicationService;
            this._visitService = visitService;
            this._visitPayUserService = visitPayUserService;
        }

        public bool HasPendingReconfiguration(int currentVisitPayUserId)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantorEx(currentVisitPayUserId);
            IList<FinancePlan> pendingAcceptanceFinancePlans = this._financePlanService.Value.GetAllPendingAcceptanceFinancePlansForGuarantor(guarantor, true, true);
            bool hasPendingReconfiguration = pendingAcceptanceFinancePlans?.Any() ?? false;

            return hasPendingReconfiguration;
        }

        public async Task<ReconfigureFinancePlanDto> GetOfflineReconfigurationInformation(int vpGuarantorId)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            if (!guarantor.IsOfflineGuarantor())
            {
                this.LoggingService.Value.Fatal(() => $"{nameof(this.GetOfflineReconfigurationInformation)}: guarantor ({vpGuarantorId}) is not an offline guarantor ({guarantor.VpGuarantorTypeEnum})");
                return null;
            }

            IList<FinancePlan> openFinancePlans = this._financePlanService.Value.GetAllOpenFinancePlansForGuarantor(guarantor);

            // vpcc is combo only - there should only be one open FP
            FinancePlan financePlanToReconfigure = openFinancePlans.FirstOrDefault();

            ReconfigureFinancePlanDto dto = await this.GetReconfigurationInformation(guarantor, financePlanToReconfigure, openFinancePlans, null, false);
            return dto;
        }

        public async Task<ReconfigureFinancePlanDto> GetReconfigurationInformation(int vpGuarantorId, int financePlanIdToReconfigure, int? financePlanOfferSetTypeId, bool isClientApplication)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            IList<FinancePlan> openFinancePlans = this._financePlanService.Value.GetAllOpenFinancePlansForGuarantor(guarantor);
            FinancePlan financePlanToReconfigure = openFinancePlans.FirstOrDefault(x => x.FinancePlanId == financePlanIdToReconfigure);
            
            ReconfigureFinancePlanDto dto = await this.GetReconfigurationInformation(guarantor, financePlanToReconfigure, openFinancePlans, financePlanOfferSetTypeId, isClientApplication);
            return dto;
        }

        public async Task<ReconfigurationActionResponseDto> ReconfigureFinancePlanAsync(int financePlanIdToReconfigure, FinancePlanCalculationParametersDto financePlanCalculationParametersDto, int loggedInVisitPayUserId, JournalEventHttpContextDto httpContext, bool sendEmail)
        {
            FinancePlan financePlanToReconfigure = this._financePlanService.Value.GetFinancePlanById(financePlanIdToReconfigure);
            if (financePlanToReconfigure == null)
            {
                this.LoggingService.Value.Fatal(() => $"{nameof(this.ReconfigureFinancePlanAsync)}: {nameof(financePlanToReconfigure)} ({financePlanIdToReconfigure}) is null");
                return ReconfigurationActionResponseDto.Error;
            }

            if (financePlanToReconfigure.VpGuarantor.VpGuarantorId != financePlanCalculationParametersDto.VpGuarantorId)
            {
                this.LoggingService.Value.Fatal(() => $"{nameof(this.ReconfigureFinancePlanAsync)}: vpGuarantorId mismatch: {financePlanToReconfigure.VpGuarantor.VpGuarantorId}, {financePlanCalculationParametersDto.VpGuarantorId}");
                return ReconfigurationActionResponseDto.Error;
            }

            if (!financePlanToReconfigure.IsEligibleForReconfiguration())
            {
                this.LoggingService.Value.Fatal(() => $"{nameof(this.ReconfigureFinancePlanAsync)}: finance plan {financePlanToReconfigure.FinancePlanId} is not eligible for reconfiguration");
                return ReconfigurationActionResponseDto.Error;
            }

            FinancePlan resultingFinancePlan;
            FinancePlan reconfiguredFinancePlan = financePlanToReconfigure.HasPendingReconfiguration() ? financePlanToReconfigure.ReconfiguredFinancePlan : null;

            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                FinancePlanCalculationParameters parameters;
                if (reconfiguredFinancePlan != null)
                {
                    parameters = this.MapFinancePlanCalculationParameters(financePlanCalculationParametersDto, reconfiguredFinancePlan);
                    resultingFinancePlan = this._financePlanService.Value.UpdateReconfiguredFinancePlan(reconfiguredFinancePlan, parameters, loggedInVisitPayUserId, httpContext);
                }
                else
                {
                    parameters = this.MapFinancePlanCalculationParameters(financePlanCalculationParametersDto, financePlanToReconfigure);
                    resultingFinancePlan = this._financePlanService.Value.CreateReconfiguredFinancePlan(parameters, financePlanToReconfigure, loggedInVisitPayUserId, httpContext);
                }

                if (resultingFinancePlan?.FinancePlanOffer != null)
                {
                    FinancePlanTermsDto financePlanTermsDto = this.CreateTerms(financePlanToReconfigure, resultingFinancePlan.FinancePlanOffer, parameters);
                    await this._creditAgreementApplicationService.Value.CacheCreditAgreementTermsAsync(financePlanTermsDto);
                }

                if (resultingFinancePlan != null && sendEmail)
                {
                    this._session.RegisterPostCommitSuccessAction(fp =>
                    {
                        this.SendFinancePlanModificationEmailMessage(resultingFinancePlan, parameters.FirstPaymentDueDate);
                    }, resultingFinancePlan);
                }

                uow.Commit();
            }

            return ReconfigurationActionResponseDto.Success;
        }

        public async Task<ReconfigurationActionResponseDto> AcceptReconfigurationAsync(int financePlanIdToReconfigure, int termsCmsVersionId, int vpGuarantorId, int loggedInVisitPayUserId, JournalEventHttpContextDto httpContext)
        {
            FinancePlan financePlanToReconfigure = this._financePlanService.Value.GetFinancePlanById(financePlanIdToReconfigure);
            FinancePlan reconfiguredFinancePlan = financePlanToReconfigure?.ReconfiguredFinancePlan;

            if (reconfiguredFinancePlan == null)
            {
                this.LoggingService.Value.Fatal(() => $"{nameof(this.AcceptReconfigurationAsync)}: {nameof(reconfiguredFinancePlan)} ({financePlanIdToReconfigure}) is null");
                return ReconfigurationActionResponseDto.Error;
            }

            if (reconfiguredFinancePlan.VpGuarantor.VpGuarantorId != vpGuarantorId)
            {
                this.LoggingService.Value.Fatal(() => $"{nameof(this.AcceptReconfigurationAsync)}: vpGuarantorId mismatch: {reconfiguredFinancePlan.VpGuarantor.VpGuarantorId}, {vpGuarantorId}");
                return ReconfigurationActionResponseDto.Error;
            }

            if (!reconfiguredFinancePlan.IsPendingReconfiguration())
            {
                this.LoggingService.Value.Fatal(() => $"{nameof(this.AcceptReconfigurationAsync)}: {nameof(reconfiguredFinancePlan)} is not a pending reconfiguration");
                return ReconfigurationActionResponseDto.Error;
            }

            if (termsCmsVersionId == default(int))
            {
                this.LoggingService.Value.Fatal(() => $"{nameof(this.AcceptReconfigurationAsync)}: {nameof(termsCmsVersionId)} {termsCmsVersionId} is not valid");
                return ReconfigurationActionResponseDto.Error;
            }

            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(httpContext, loggedInVisitPayUserId);

            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            VpStatement currentStatement = this._statementService.Value.GetMostRecentStatement(guarantor);
            FinancePlanCreditAgreementCollectionDto creditAgreementCollectionDto = await this._creditAgreementApplicationService.Value.GetCachedCreditAgreementAsync(reconfiguredFinancePlan.FinancePlanOffer.FinancePlanOfferId, termsCmsVersionId, false);

            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                this._financePlanService.Value.AcceptTermsForReconfiguration(reconfiguredFinancePlan, loggedInVisitPayUserId, creditAgreementCollectionDto, httpContext);
                this._paymentService.Value.RemovePendingPaymentAmountAssociatedWithFinancePlan(reconfiguredFinancePlan.OriginalFinancePlan);

                if (currentStatement.IsGracePeriod &&
                    currentStatement.ProcessingStatus == ChangeEventStatusEnum.Unprocessed)
                {
                    reconfiguredFinancePlan.SetTheAmountDueForFinancePlan(currentStatement);
                }

                uow.Commit();
            }

            this._userJournalEventService.Value.LogGuarantorAcceptReconfiguredFinancePlan(journalEventUserContext, vpGuarantorId, guarantor.User.UserName, reconfiguredFinancePlan.FinancePlanId, reconfiguredFinancePlan.FinancePlanOfferSetTypeId);

            return ReconfigurationActionResponseDto.Success;
        }

        public ReconfigurationActionResponseDto CancelReconfiguration(int financePlanIdToReconfigure, int vpGuarantorId, int actionVisitPayUserId, string action, JournalEventHttpContextDto context)
        {
            FinancePlan financePlanToReconfigure = this._financePlanService.Value.GetFinancePlanById(financePlanIdToReconfigure);
            FinancePlan reconfiguredFinancePlan = financePlanToReconfigure?.ReconfiguredFinancePlan;

            if (reconfiguredFinancePlan == null)
            {
                this.LoggingService.Value.Fatal(() => $"{nameof(this.CancelReconfiguration)}: {nameof(reconfiguredFinancePlan)} ({financePlanIdToReconfigure}) is null");
                return ReconfigurationActionResponseDto.Error;
            }

            if (reconfiguredFinancePlan.VpGuarantor.VpGuarantorId != vpGuarantorId)
            {
                this.LoggingService.Value.Fatal(() => $"{nameof(this.AcceptReconfigurationAsync)}: vpGuarantorId mismatch: {reconfiguredFinancePlan.VpGuarantor.VpGuarantorId}, {vpGuarantorId}");
                return ReconfigurationActionResponseDto.Error;
            }

            if (!reconfiguredFinancePlan.IsPendingReconfiguration())
            {
                this.LoggingService.Value.Fatal(() => $"{nameof(this.AcceptReconfigurationAsync)}: {nameof(reconfiguredFinancePlan)} is not a pending reconfiguration");
                return ReconfigurationActionResponseDto.Error;
            }

            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                this._financePlanService.Value.CancelReconfiguredFinancePlan(reconfiguredFinancePlan, action, actionVisitPayUserId, context);
                uow.Commit();
            }

            return ReconfigurationActionResponseDto.Success;
        }

        private async Task<ReconfigureFinancePlanDto> GetReconfigurationInformation(Guarantor guarantor, FinancePlan financePlanToReconfigure, IList<FinancePlan> openFinancePlans, int? financePlanOfferSetTypeId, bool isClientApplication)
        {
            if (financePlanToReconfigure == null)
            {
                this.LoggingService.Value.Fatal(() => $"{nameof(this.GetReconfigurationInformation)}: finance plan not found");
                return null;
            }

            const int defaultOfferTypeId = (int)FinancePlanOfferSetTypeEnum.GuarantorOffers;
            int checkOfferTypeId = financePlanOfferSetTypeId.GetValueOrDefault(defaultOfferTypeId);

            // get terms on any existing reconfiguration
            FinancePlanTermsDto reconfiguredTermsDto = null;
            CalculateFinancePlanTermsResponseDto response = await this.GetSelectedOfferTermsAsync(guarantor.VpGuarantorId, financePlanToReconfigure);
            if (response != null && !response.IsError)
            {
                int termsOfferTypeId = response.Terms.FinancePlanOfferSetType?.FinancePlanOfferSetTypeId ?? defaultOfferTypeId;
                if (financePlanOfferSetTypeId == null || termsOfferTypeId == financePlanOfferSetTypeId.Value)
                {
                    // we're requesting the same offer set as the pending reconfiguration, use the terms
                    reconfiguredTermsDto = response.Terms;
                    checkOfferTypeId = termsOfferTypeId;
                }
            }
            else
            {
                if (!isClientApplication)
                {
                    // if it's not the client app, reconfig is only allowed if the client has already begun the process
                    // or, the finance plan has a positive balance adjustment
                    if (!financePlanToReconfigure.HasPositiveVisitBalanceDifference())
                    {
                        this.LoggingService.Value.Fatal(() => $"{nameof(this.GetReconfigurationInformation)}: guarantor attempted reconfiguration without a positive adjustment or pending plan");
                        return null;
                    }
                }
            }

            // get minimum payment amount
            FinancePlanBoundaryDto boundary = this.GetMinimumPaymentAmount(financePlanToReconfigure.FinancePlanId, guarantor.VpGuarantorId, checkOfferTypeId);

            // interest rates
            bool hasFinancialAssistance = financePlanToReconfigure.HasFinancialAssistance();
            List<FinancePlan> financePlansForInterestRates = openFinancePlans.Where(x => x.FinancePlanId != financePlanToReconfigure.FinancePlanId).ToList();
            decimal financePlanBalance = financePlanToReconfigure.CurrentFinancePlanBalanceIncludingAllAdjustments;
            IList<InterestRate> interestRates = this._interestService.Value.GetInterestRates(financePlanBalance, financePlansForInterestRates, hasFinancialAssistance, guarantor.VpGuarantorId, checkOfferTypeId);

            // offer sets
            IList<FinancePlanOfferSetTypeDto> financePlanOfferSetTypes = Mapper.Map<IList<FinancePlanOfferSetTypeDto>>(this._financePlanOfferService.Value.GetFinancePlanOfferSetTypes());

            // dto
            ReconfigureFinancePlanDto informationDto = new ReconfigureFinancePlanDto
            {
                FinancePlanIdToReconfigure = financePlanToReconfigure.FinancePlanId,
                CurrentFinancePlanBalance = financePlanToReconfigure.CurrentFinancePlanBalance,
                CurrentFinancePlanInterestRate = financePlanToReconfigure.CurrentInterestRate,
                CurrentFinancePlanOriginatedBalance = financePlanToReconfigure.OriginatedFinancePlanBalance,
                CurrentFinancePlanNewCharges = financePlanToReconfigure.PositiveVisitBalanceDifferenceSum,
                CurrentFinancePlanPaymentAmount = financePlanToReconfigure.PaymentAmount,
                NewFinancePlanBalance = financePlanBalance,
                FinancePlanBoundary = boundary,
                Guarantor = Mapper.Map<GuarantorDto>(guarantor),
                ReconfiguredTerms = reconfiguredTermsDto,
                InterestRates = Mapper.Map<IList<InterestRateDto>>(interestRates),
                FinancePlanOfferSetTypes = financePlanOfferSetTypes
            };

            return informationDto;
        }

        #region terms

        public FinancePlanCalculationParameters MapFinancePlanCalculationParameters(FinancePlanCalculationParametersDto financePlanCalculationParametersDto, FinancePlan financePlan, Guarantor guarantor = null, VpStatement statement = null)
        {
            if (guarantor == null)
            {
                guarantor = this._guarantorService.Value.GetGuarantor(financePlanCalculationParametersDto.VpGuarantorId);
            }

            if (statement == null)
            {
                statement = this._statementService.Value.GetMostRecentStatement(guarantor);
            }

            if (statement == null)
            {
                throw new Exception("Current statement is null");
            }

            if (financePlanCalculationParametersDto.StatementId.HasValue && statement.VpStatementId != financePlanCalculationParametersDto.StatementId)
            {
                throw new Exception("Current statement was different than statementId passed in");
            }

            // each visit should have 1 month interest free
            const decimal overrideInterestRate = 0m;
            const int overridesRemaining = 1;

            // reconfig's don't allow changing payment due day - just use the next one
            DateTime firstPaymentDueDate = this._statementService.Value.GetNextPaymentDate(guarantor);

            FinancePlanCalculationParameters parameters = new FinancePlanCalculationParameters(
                guarantor,
                statement,
                financePlanCalculationParametersDto.MonthlyPaymentAmount.GetValueOrDefault(0),
                financePlanCalculationParametersDto.FinancePlanOfferSetTypeId,
                financePlanCalculationParametersDto.OfferCalculationStrategy,
                this.Client.Value.InterestCalculationMethod,
                firstPaymentDueDate)
            {
                CombineFinancePlans = false,
                Visits = new List<IFinancePlanVisitBalance>()
            };

            parameters.Visits.AddRange(financePlan.ActiveFinancePlanVisits.Select(fpv => new FinancePlanSetupVisit(
                fpv.VisitId,
                fpv.SourceSystemKey,
                fpv.BillingSystemId,
                fpv.CurrentVisitBalance, // this will include any adjustments (stump amount) applied to a financed visit, which is intended here.
                fpv.UnclearedPaymentsSum,
                fpv.InterestDue,
                fpv.CurrentVisitState,
                fpv.BillingApplication,
                fpv.InterestZero,
                overrideInterestRate,
                overridesRemaining)).ToList());

            return parameters;
        }

        public async Task<CalculateFinancePlanTermsResponseDto> GetTermsByMonthlyPaymentAmountAsync(int financePlanId, FinancePlanCalculationParametersDto financePlanCalculationParametersDto)
        {
            FinancePlan financePlan = this._financePlanService.Value.GetFinancePlan(financePlanCalculationParametersDto.VpGuarantorId, financePlanId);
            FinancePlanCalculationParameters financePlanCalculationParameters = this.MapFinancePlanCalculationParameters(financePlanCalculationParametersDto, financePlan);

            return await this.GetTermsAsync(financePlan, financePlanCalculationParameters);
        }

        public async Task<CalculateFinancePlanTermsResponseDto> GetTermsByNumberOfMonthlyPaymentsAsync(int financePlanId, FinancePlanCalculationParametersDto financePlanCalculationParametersDto)
        {
            FinancePlan financePlan = this._financePlanService.Value.GetFinancePlan(financePlanCalculationParametersDto.VpGuarantorId, financePlanId);
            FinancePlanCalculationParameters financePlanCalculationParameters = this.MapFinancePlanCalculationParameters(financePlanCalculationParametersDto, financePlan);
            decimal monthlyPaymentAmount = this._financePlanService.Value.GetPaymentAmountForNumberOfPayments(financePlanCalculationParameters, financePlanCalculationParametersDto.NumberOfMonthlyPayments.GetValueOrDefault(0));

            if (monthlyPaymentAmount > 0)
            {
                FinancePlanBoundaryDto boundary = this.GetMinimumPaymentAmount(financePlanId, financePlanCalculationParametersDto.VpGuarantorId, financePlanCalculationParametersDto.FinancePlanOfferSetTypeId);
                if (boundary.MinimumPaymentAmount > monthlyPaymentAmount &&
                    boundary.MaximumNumberOfPayments <= financePlanCalculationParametersDto.NumberOfMonthlyPayments)
                {
                    // example: 6 monthly payments for $295 results in $49.17, which is lower than the $50.00 minimum
                    // adjust up to the minimum
                    monthlyPaymentAmount = boundary.MinimumPaymentAmount;
                }
            }

            financePlanCalculationParameters.MonthlyPaymentAmount = monthlyPaymentAmount;
            
            return await this.GetTermsAsync(financePlan, financePlanCalculationParameters);
        }

        private FinancePlanBoundaryDto GetMinimumPaymentAmount(int financePlanId, int vpGuarantorId, int? financePlanOfferSetTypeId)
        {
            FinancePlanCalculationParametersDto newDto = FinancePlanCalculationParametersDto.CreateForTermsCheck(vpGuarantorId, financePlanOfferSetTypeId, false);

            FinancePlan financePlan = this._financePlanService.Value.GetFinancePlan(newDto.VpGuarantorId, financePlanId);
            FinancePlanCalculationParameters financePlanCalculationParameters = this.MapFinancePlanCalculationParameters(newDto, financePlan);

            FinancePlanOffer financePlanOffer = this._financePlanService.Value.GetOfferForReconfiguration(financePlanCalculationParameters, financePlan, false);
            if (financePlanOffer == null)
            {
                return new FinancePlanBoundaryDto(-1m, -1);
            }
            
            int amortizationDuration = this._amortizationService.Value.GetAmortizationDurationForAmounts(
                financePlanOffer.MinPayment,
                financePlanCalculationParameters.Visits,
                financePlanOffer.InterestRate,
                financePlanCalculationParameters.InterestFreePeriod,
                financePlanCalculationParameters.FirstPaymentDueDate,
                financePlanCalculationParameters.InterestCalculationMethod);

            return new FinancePlanBoundaryDto(financePlanOffer.MinPayment, amortizationDuration);
        }

        private async Task<CalculateFinancePlanTermsResponseDto> GetSelectedOfferTermsAsync(int vpGuarantorId, FinancePlan financePlanToReconfigure)
        {
            if (!financePlanToReconfigure.HasPendingReconfiguration())
            {
                // there is no pending reconfiguration for this finance plan
                return null;
            }

            // we have a pending reconfig
            FinancePlan reconfiguredFinancePlan = financePlanToReconfigure.ReconfiguredFinancePlan;

            FinancePlanCalculationParametersDto parametersDto = FinancePlanCalculationParametersDto.CreateForReconfigurationTermsCheck(vpGuarantorId, reconfiguredFinancePlan.PaymentAmount, null, reconfiguredFinancePlan.FinancePlanOfferSetTypeId);
            FinancePlanCalculationParameters parameters = this.MapFinancePlanCalculationParameters(parametersDto, financePlanToReconfigure);
            
            FinancePlanBoundaryDto boundary = this.GetMinimumPaymentAmount(financePlanToReconfigure.FinancePlanId, parameters.Guarantor.VpGuarantorId, parameters.FinancePlanOfferSetTypeId);
            FinancePlanTermsDto financePlanTermsDto = this.CreateTerms(financePlanToReconfigure, reconfiguredFinancePlan.FinancePlanOffer, parameters);

            await this._creditAgreementApplicationService.Value.CacheCreditAgreementTermsAsync(financePlanTermsDto);

            return new CalculateFinancePlanTermsResponseDto(financePlanTermsDto, boundary);
        }

        private async Task<CalculateFinancePlanTermsResponseDto> GetTermsAsync(FinancePlan financePlanToReconfigure, FinancePlanCalculationParameters parameters)
        {
            FinancePlanOffer financePlanOffer = this._financePlanService.Value.GetOfferForReconfiguration(parameters, financePlanToReconfigure, true);
            if (financePlanOffer == null)
            {
                CmsVersionDto cmsVersionDto = await this._contentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.ArrangePaymentFinancePlanMinimumAmountError);
                return new CalculateFinancePlanTermsResponseDto(cmsVersionDto.ContentBody, null);
            }

            FinancePlanBoundaryDto boundary = this.GetMinimumPaymentAmount(financePlanToReconfigure.FinancePlanId, parameters.Guarantor.VpGuarantorId, parameters.FinancePlanOfferSetTypeId);
            FinancePlanTermsDto terms = this.CreateTerms(financePlanToReconfigure, financePlanOffer, parameters);
            CalculateFinancePlanTermsResponseDto response = new CalculateFinancePlanTermsResponseDto(terms, boundary);

            if (response.IsError)
            {
                return response;
            }

            await this._creditAgreementApplicationService.Value.CacheCreditAgreementTermsAsync(response.Terms);

            return response;
        }

        private FinancePlanTermsDto CreateTerms(FinancePlan financePlanToReconfigure, FinancePlanOffer financePlanOffer, FinancePlanCalculationParameters parameters)
        {
            IList<AmortizationMonth> amortizationMonths = this._amortizationService.Value.GetAmortizationMonthsForAmounts(
                parameters.MonthlyPaymentAmount,
                parameters.Visits,
                financePlanOffer.InterestRate,
                parameters.InterestFreePeriod,
                parameters.FirstPaymentDueDate,
                parameters.InterestCalculationMethod);

            AmortizationMonth lastPayment = amortizationMonths.OrderByDescending(x => x.MonthDate).FirstOrDefault();

            decimal rolloverBalance = financePlanToReconfigure.CurrentFinancePlanBalance;
            decimal rolloverInterest = financePlanToReconfigure.InterestDue;
            decimal monthlyPaymentAmountPrevious = this._financePlanService.Value.GetTotalPaymentAmountForOpenFinancePlans(financePlanOffer.VpGuarantor.VpGuarantorId);
            decimal monthlyPaymentAmountOtherPlans = monthlyPaymentAmountPrevious - financePlanToReconfigure.PaymentAmount;
            int numberMonthlyPayments = amortizationMonths.Count;

            CmsRegionEnum termsCmsRegionEnum;

            bool requireRetailInstallmentContract = this._financePlanService.Value.RequireRetailInstallmentContract(financePlanOffer, numberMonthlyPayments);
            if (requireRetailInstallmentContract)
            {
                // find the appropriate RIC
                termsCmsRegionEnum = this._visitService.Value.GetRicCmsRegionEnum(financePlanOffer.VpGuarantor.VpGuarantorId);
                if (termsCmsRegionEnum == CmsRegionEnum.Unknown)
                {
                    termsCmsRegionEnum = CmsRegionEnum.VppCreditAgreement;
                }
            }
            else
            {
                // use the material terms
                termsCmsRegionEnum = CmsRegionEnum.FinancePlanSimpleTerms;
            }

            CmsVersionDto termsCmsVersion = this._contentApplicationService.Value.GetCurrentVersionAsync(termsCmsRegionEnum, false).Result;
            string agreementTitle = this._creditAgreementApplicationService.Value.GetAgreementTitle(requireRetailInstallmentContract);

            FinancePlanTermsDto terms = new FinancePlanTermsDto
            {
                IsCombined = false,
                EffectiveDate = this._financePlanService.Value.GetOriginationDate(parameters.Guarantor, parameters.Statement),
                FinalPaymentDate = lastPayment?.MonthDate ?? DateTime.MinValue,
                FinancePlanBalance = financePlanOffer.StatementedCurrentAccountBalance,
                FinancePlanOfferId = financePlanOffer.FinancePlanOfferId,
                FinancePlanOfferSetType = Mapper.Map<FinancePlanOfferSetTypeDto>(financePlanOffer.FinancePlanOfferSetType),
                InterestRate = financePlanOffer.InterestRate,
                MonthlyPaymentAmount = parameters.MonthlyPaymentAmount,
                MonthlyPaymentAmountLast = lastPayment?.Total ?? 0m,
                MonthlyPaymentAmountOtherPlans = monthlyPaymentAmountOtherPlans,
                MonthlyPaymentAmountPrevious = monthlyPaymentAmountPrevious,
                MonthlyPaymentAmountTotal = parameters.MonthlyPaymentAmount + monthlyPaymentAmountOtherPlans,
                NextPaymentDueDate = parameters.FirstPaymentDueDate,
                NumberMonthlyPayments = amortizationMonths.Count,
                PaymentDueDay = financePlanOffer.VpGuarantor.PaymentDueDay,
                RequireCreditAgreement = true,
                RolloverBalance = rolloverBalance,
                RolloverInterest = rolloverInterest,
                TotalInterest = Math.Max(0, amortizationMonths.Sum(x => x.InterestAmount) - rolloverInterest), // rollover interest is included in the balance
                VpGuarantorId = financePlanOffer.VpGuarantor.VpGuarantorId,
                DownpaymentAmount = 0m,
                TermsCmsRegionEnum = termsCmsRegionEnum,
                TermsCmsVersionId = termsCmsVersion.CmsVersionId,
                TermsAgreementTitle = agreementTitle,
                IsRetailInstallmentContract = requireRetailInstallmentContract
            };

            return terms;
        }

        #endregion

        public void SendFinancePlanModificationEmailMessage(FinancePlan financePlan, DateTime nextPaymentDueDate)
        {
            Guarantor guarantor = financePlan.VpGuarantor;
            if (guarantor.IsOfflineGuarantor())
            {
                if (financePlan.IsPendingAcceptance() &&
                    this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.OfflineVisitPay))
                {
                    using (IUnitOfWork uow = new UnitOfWork(this._session))
                    {
                        string tempPassword = this._visitPayUserService.Value.GenerateOfflineLoginToken(guarantor.User);

                        this._session.RegisterPostCommitSuccessAction(t =>
                        {
                            this.Bus.Value.PublishMessage(new SendVpccFinancePlanModificationEmailMessage
                            {
                                NextPaymentDueDate = nextPaymentDueDate,
                                VpGuarantorId = guarantor.VpGuarantorId,
                                TempPassword = t
                            }).Wait();
                        }, tempPassword);

                        uow.Commit();
                    }
                }
            }
            else
            {
                this.Bus.Value.PublishMessage(new SendFinancePlanModificationEmailMessage
                {
                    NextPaymentDueDate = nextPaymentDueDate,
                    VpGuarantorId = guarantor.VpGuarantorId
                }).Wait();
            }
        }

        private void ProcessInterestChangeInParentFinancePlan(UpdateReconfiguredFinancePlanInterestMessage message)
        {
            if (message.ParentFinancePlanId <= 0 || message.ReconfiguredFinancePlanId <= 0)
            {
                return;
            }

            FinancePlan reconfiguredFinancePlan = this._financePlanService.Value.GetFinancePlanById(message.ReconfiguredFinancePlanId);
            if (reconfiguredFinancePlan == null || !reconfiguredFinancePlan.IsNotOriginated())
            {
                return;
            }

            //Recalculate the interest and set new offer.
            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                this._financePlanService.Value.UpdateReconfiguredFinancePlan(reconfiguredFinancePlan, new FinancePlanCalculationParameters(reconfiguredFinancePlan), SystemUsers.SystemUserId, null);
                uow.Commit();
            }
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(UpdateReconfiguredFinancePlanInterestMessage message, ConsumeContext<UpdateReconfiguredFinancePlanInterestMessage> consumeContext)
        {
            this.ProcessInterestChangeInParentFinancePlan(message);
        }

        public bool IsMessageValid(UpdateReconfiguredFinancePlanInterestMessage message, ConsumeContext<UpdateReconfiguredFinancePlanInterestMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion
    }
}
﻿namespace Ivh.Application.FinanceManagement.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Core.Common.Dtos;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Interfaces;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using Ivh.Common.Base.Constants;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Jobs;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using MassTransit;
    using NHibernate;

    public class VisitTransactionChangesApplicationService : ApplicationService, IVisitTransactionChangesApplicationService
    {
        private readonly Lazy<IVisitService> _visitService;
        private readonly Lazy<IVisitTransactionService> _visitTransactionService;
        private readonly Lazy<IFinancePlanService> _financePlanService;
        private readonly Lazy<IPaymentService> _paymentService;
        private readonly ISession _session;

        public VisitTransactionChangesApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IVisitService> visitService,
            Lazy<IVisitTransactionService> visitTransactionService,
            Lazy<IFinancePlanService> financePlanService,
            Lazy<IPaymentService> paymentService,
            ISessionContext<VisitPay> sessionContext) : base(applicationServiceCommonService)
        {
            this._visitService = visitService;
            this._visitTransactionService = visitTransactionService;
            this._financePlanService = financePlanService;
            this._paymentService = paymentService;
            this._session = sessionContext.Session;
        }
        
        public void CreateVisitReconciliationBalanceChangeMessagesFromVisits(IList<int> visitIds)
        {
            IList<Visit> visits = this._visitService.Value.GetAllVisitsWithIntList(visitIds.Distinct().ToList(), false);
            List<VisitReconciliationBalanceChangeMessage> messages = new List<VisitReconciliationBalanceChangeMessage>();
            foreach (Visit visit in visits)
            {
                VisitReconciliationBalanceChangeMessage message = this.CreateVisitReconciliationBalanceChangeMessagesFromVisit(visit);
                if (message != null)
                {
                    messages.Add(message);
                }
            }

            this._session.Transaction.RegisterPostCommitSuccessAction(m =>
            {
                this.Bus.Value.PublishMessage(new VisitReconciliationBalanceChangeMessageList
                {
                    Messages = messages
                }).Wait();
            }, messages);
        }

        public void QueueAllGuarantorsWithAllVisits()
        {
            IList<VisitResult> listOfVisitsResults = this._visitService.Value.GetAllVisitsResults();
            this._session.Transaction.RegisterPostCommitSuccessAction(l =>
            {
                foreach (IGrouping<int, VisitResult> grouping in l.GroupBy(x => x.GuarantorId).ToList())
                {
                    this.Bus.Value.PublishMessage(new CreateVisitReconciliationBalanceChangesForGuarantorMessage
                    {
                        GuarantorId = grouping.Key,
                        VisitIds = grouping.Select(x => x.VisitId).ToList()
                    }).Wait();
                }
            }, listOfVisitsResults);
        }

        private VisitReconciliationBalanceChangeMessage CreateVisitReconciliationBalanceChangeMessagesFromVisit(Visit visit)
        {
            if (visit.HsGuarantorMap != null && visit.HsGuarantorMap.HsGuarantorMatchStatus == HsGuarantorMatchStatusEnum.VpccPending)
            {
                return null;
            }

            IList<FinancePlanVisit> financePlanVisits = this._financePlanService.Value.GetVisitOnAnyFinancePlan(visit.VisitId);
            FinancePlanVisit fpvOnActiveFinancePlan = financePlanVisits.Where(x => x.FinancePlan.IsActive()).OrderByDescending(x => x.InsertDate).FirstOrDefault();

            //only consider financed visits for VPCC guarantors
            if (visit.VPGuarantor.IsOfflineGuarantor() 
                && fpvOnActiveFinancePlan == null)
            {
                return null;
            }

            VisitTransaction lastTransaction = this._visitTransactionService.Value.GetVisitTransactions(visit.VpGuarantorId, visit.VisitId).OrderByDescending(x => x.InsertDate).FirstOrDefault();
            IReadOnlyList<Payment> paymentsForVisit = this._paymentService.Value.GetPaymentsForVisit(visit.VisitId);

            List<PaymentAllocation> paymentsThatArePendingAch = paymentsForVisit
                .Where(x => x.IsPendingAchApproval())
                .SelectMany(x => x.PaymentAllocations).Where(x => x.PaymentVisit != null && x.PaymentVisit.VisitId == visit.VisitId)
                .Distinct()
                .ToList();

            decimal achUnsettledInterest = paymentsThatArePendingAch
                .Where(x => x.PaymentAllocationType == PaymentAllocationTypeEnum.VisitInterest)
                .Sum(x => x.ActualAmount);

            decimal achUnsettledPrincipal = paymentsThatArePendingAch
                .Where(x => x.PaymentAllocationType == PaymentAllocationTypeEnum.VisitPrincipal)
                .Sum(x => x.ActualAmount);

            decimal interestAssessed = 0;
            decimal interestPaid = 0;
            decimal currentBalance = visit.CurrentBalance;

            if (financePlanVisits.Any())
            {
                interestAssessed = financePlanVisits.Sum(x => x.InterestAssessed);
                interestPaid = financePlanVisits.Sum(x => x.InterestPaid);

                if (fpvOnActiveFinancePlan != null)
                {
                    currentBalance = fpvOnActiveFinancePlan.CurrentBalance;
                }
            }

            VisitReconciliationBalanceChangeMessage message = new VisitReconciliationBalanceChangeMessage
            {
                VpVisitId = visit.VisitId,
                VpVisitBillingSystemId = visit.IsUnmatched ? visit.MatchedBillingSystem.BillingSystemId : visit.BillingSystem.BillingSystemId,
                VpVisitSourceSystemKey = visit.IsUnmatched ? visit.MatchedSourceSystemKey : visit.SourceSystemKey,
                LastTransactionDate = lastTransaction?.InsertDate ?? DateTime.MinValue,
                SumOfVppInterestAssessed = interestAssessed, //visit.InterestAssessed,
                SumOfVppInterestPaid = interestPaid, //visit.InterestPaidForDateRange(DateTime.MinValue, DateTime.MaxValue),
                SumOfAchUnsettledInterest = achUnsettledInterest,
                SumOfAchUnsettledPrincipal = achUnsettledPrincipal,
                VpVisitBalance = currentBalance,
                VisitState = visit.CurrentVisitState
            };

            return message;
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(CreateVisitReconciliationBalanceChangesForGuarantorMessage message, ConsumeContext<CreateVisitReconciliationBalanceChangesForGuarantorMessage> consumeContex)
        {
            this.CreateVisitReconciliationBalanceChangeMessagesFromVisits(message.VisitIds.Distinct().ToList());
        }

        public bool IsMessageValid(CreateVisitReconciliationBalanceChangesForGuarantorMessage message, ConsumeContext<CreateVisitReconciliationBalanceChangesForGuarantorMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion

        #region Jobs

        void IJobRunnerService<QueueVisitReconciliationJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.QueueAllGuarantorsWithAllVisits();
        }

        #endregion
    }
}
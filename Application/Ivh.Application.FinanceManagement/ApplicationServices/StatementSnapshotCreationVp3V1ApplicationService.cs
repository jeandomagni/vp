﻿namespace Ivh.Application.FinanceManagement.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Base.Common.Interfaces;
    using Base.Common.Interfaces.Entities.Visit;
    using Base.Services;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Interfaces;
    using Domain.FinanceManagement.Statement.Entities;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using Ivh.Common.VisitPay.Strings;
    using NHibernate;

    public class StatementSnapshotCreationVp3V1ApplicationService : ApplicationService, IStatementSnapshotCreationApplicationService
    {
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IStatementDateService> _statementDateService;
        private readonly Lazy<IVpStatementService> _statementService;
        private readonly Lazy<IVisitTransactionService> _visitTransactionService;
        private readonly Lazy<IVisitService> _visitService;
        private readonly Lazy<IFinancePlanStatementService> _financePlanStatementService;
        private readonly Lazy<IFinancePlanService> _financePlanService;
        private readonly ISession _session;

        private int? FindFinancePlanIdForVisitWithFinancePlanVisitResult(IVisit foundConnectedEntity, IList<FinancePlanVisit> visitResultsThatWereOnFpForThatDate)
        {
            FinancePlanVisit noDate = this.GetFinancePlanVisit(foundConnectedEntity, visitResultsThatWereOnFpForThatDate);

            return noDate?.FinancePlan.FinancePlanId;
        }

        private FinancePlanVisit GetFinancePlanVisit(IVisit foundConnectedEntity, IList<FinancePlanVisit> visitResultsThatWereOnFpForThatDate)
        {
            return visitResultsThatWereOnFpForThatDate?
                .Where(x => new FinancePlanStatus { FinancePlanStatusId = (int)x.FinancePlan.FinancePlanStatus.FinancePlanStatusEnum }.IsActiveOriginated())
                .OrderByDescending(x => x.FinancePlan.FinancePlanId)
                .FirstOrDefault(x => x.VisitId == foundConnectedEntity.VisitId);
        }

        public StatementSnapshotCreationVp3V1ApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IStatementDateService> statementDateService,
            Lazy<IVpStatementService> statementService,
            Lazy<IVisitTransactionService> visitTransactionService,
            Lazy<IVisitService> visitService,
            Lazy<IFinancePlanStatementService> financePlanStatementService,
            Lazy<IFinancePlanService> financePlanService,
            ISessionContext<VisitPay> sessionContext
        ) : base(applicationServiceCommonService)
        {
            this._guarantorService = guarantorService;
            this._statementDateService = statementDateService;
            this._statementService = statementService;
            this._visitTransactionService = visitTransactionService;
            this._visitService = visitService;
            this._financePlanStatementService = financePlanStatementService;
            this._financePlanService = financePlanService;
            this._session = sessionContext.Session;
        }
        
        public async Task<GeneratedStatementsResultDto> GenerateStatementForDateAsync(Guarantor guarantor, DateTime dateToProcess, bool isImmediateStatement)
        {
            return await Task.Run(() =>
            {
                VpStatement previousStatement = this._statementService.Value.GetMostRecentStatement(guarantor);
                bool isFirstStatement = previousStatement == null;

                FinancePlanStatement previousFinancePlanStatement = this._financePlanStatementService.Value.GetMostRecentStatement(guarantor);
                DateTime nextStatementDate = this._statementDateService.Value.GetNextStatementDate(previousStatement?.OriginalPaymentDueDate, guarantor, dateToProcess, false, isFirstStatement);

                if (guarantor.NextStatementDate == null)
                {
                    guarantor.NextStatementDate = nextStatementDate;
                    this._guarantorService.Value.InsertOrUpdateGuarantor(guarantor);
                }

                // Only honor the nextStatementDate if it's not a ImmediateStatement
                if (!isImmediateStatement && dateToProcess.Date < guarantor.NextStatementDate.Value.Date)
                {
                    // StatementShouldnt get generated yet as the NextStatementDate is in the future compared to the dateToProcess
                    return null;
                }

                // Check if statement has already been generated for this period and previous statement is not  Immediate Statement
                if (previousStatement != null && this._statementService.Value.HasStatementBeenCreatedAlready(guarantor, dateToProcess))
                {
                    // If statement has been created for this period, revalidate the next statement date
                    guarantor.NextStatementDate = nextStatementDate;
                    this._guarantorService.Value.InsertOrUpdateGuarantor(guarantor);
                    return null;
                }

                VpStatement newStatement = this._statementService.Value.CreateStatement(guarantor, dateToProcess, previousStatement, isImmediateStatement, isFirstStatement, VpStatementVersionEnum.Vp3V1);
                FinancePlanStatement newFpStatement = this._financePlanStatementService.Value.CreateFinancePlanStatement(guarantor, dateToProcess, previousFinancePlanStatement, isImmediateStatement, isFirstStatement, VpStatementVersionEnum.Vp3V1);

                // Make sure that the statementdate is the less than or equal the dateToProcess, dont want to do this for the future.
                // Only check this if it's not a ImmediateStatement.
                if (!isImmediateStatement && newStatement.StatementDate.Date > dateToProcess.Date)
                {
                    // Should we be pushing out the next statement date?
                    return null;
                }

                IList<Visit> visitsForNewStatement = this.LocateHistoricalVisitsForStatementPeriod(guarantor.VpGuarantorId, newStatement.PeriodEndDate);
                IList<FinancePlanVisit> visitsThatWereOnFpForThatDate = this._financePlanService.Value.AreVisitsOnFinancePlan_FinancePlanVisit(visitsForNewStatement.Select(x => x.VisitId).ToList(), newStatement.PeriodEndDate, EnumExt.GetAllInCategory<FinancePlanStatusEnum>(FinancePlanStatusEnumCategory.Active).ToList());

                this.RemoveVisitsThatWereNotActiveInTheLastStatementPeriod(visitsForNewStatement, newStatement);
                this.RemoveNonFinancedVisitsVpcc(visitsForNewStatement, visitsThatWereOnFpForThatDate);

                this.GenerateVpStatementVisitsForStatement(visitsForNewStatement, newStatement, visitsThatWereOnFpForThatDate);
                this.GenerateFinancePlanStatementVisitsForStatement(visitsForNewStatement, newFpStatement, previousFinancePlanStatement, visitsThatWereOnFpForThatDate);

                this.GenerateFinancePlanStatementFinancePlansForStatement(newFpStatement, previousFinancePlanStatement, visitsThatWereOnFpForThatDate);

                this.GenerateVpStatementSumsForStatement(newStatement, previousStatement);
                this.GenerateFinancePlanStatementSumsForStatement(newFpStatement, previousFinancePlanStatement);
                
                newStatement.CommitSnapshotObject();
                newFpStatement.CommitSnapshotObject();

                this._statementService.Value.InsertOrUpdate(newStatement);
                
                newFpStatement.VpStatementId = newStatement.VpStatementId;
                this._financePlanStatementService.Value.InsertOrUpdate(newFpStatement);

                guarantor.MostRecentVpStatementId = newStatement.VpStatementId;
                this._guarantorService.Value.InsertOrUpdateGuarantor(guarantor);

                this._statementService.Value.IncrementTheNextStatementDate(guarantor, dateToProcess, newStatement, previousStatement == null);

                return new GeneratedStatementsResultDto
                {
                    VpStatement = newStatement,
                    FinancePlanStatement = newFpStatement
                };
            });
        }

        #region VpStatement

        private void GenerateVpStatementSumsForStatement(VpStatement newStatement, VpStatement previousStatement)
        {
            if (newStatement != null)
            {
                VpStatementSnapshotVp3V1 snap = (VpStatementSnapshotVp3V1)newStatement.GetSnapshotObject();
                snap.StatementedSnapshotTotalBalance = newStatement.VpStatementVisits.Sum(x => ((VpStatementVisitSnapshotVp3V1)x.GetSnapshotObject()).StatementedSnapshotVisitBalance);
                snap.StatementedSnapshotVisitCreditBalanceSum = newStatement.VpStatementVisits.Sum(x => ((VpStatementVisitSnapshotVp3V1)x.GetSnapshotObject()).StatementedSnapshotCreditBalance);
                snap.StatementedSnapshotVisitBalanceSum = newStatement.ActiveVpStatementVisitsNotOnFinancePlan.Sum(x => ((VpStatementVisitSnapshotVp3V1)x.GetSnapshotObject()).StatementedSnapshotVisitBalance);

                newStatement.PriorStatementDate = previousStatement?.StatementDate;
            }
        }

        private void GenerateVpStatementVisitsForStatement(IList<Visit> visitsForNewStatement, VpStatement newStatement, IList<FinancePlanVisit> visitsThatWereOnFpForThatDate)
        {
            if (visitsForNewStatement != null && visitsForNewStatement.Count > 0)
            {
                List<OutboundVisitMessage> messages = new List<OutboundVisitMessage>();
                foreach (Visit foundConnectedEntity in visitsForNewStatement)
                {
                    if (foundConnectedEntity != null)
                    {
                        decimal endingBalanceForVisit = foundConnectedEntity.CurrentBalanceAsOf(newStatement.PeriodEndDate);

                        VpStatementVisit vpStatementVisit = new VpStatementVisit
                        {
                            VpStatement = newStatement,
                            Visit = foundConnectedEntity,
                            StatementedSnapshotFinancePlanId = this.FindFinancePlanIdForVisitWithFinancePlanVisitResult(foundConnectedEntity, visitsThatWereOnFpForThatDate),
                            StatementedSnapshotVisitState = foundConnectedEntity.VisitStateEnumForDate(newStatement.PeriodEndDate)
                        };
                        VpStatementVisitSnapshotVp3V1 snap = (VpStatementVisitSnapshotVp3V1)vpStatementVisit.GetSnapshotObject();
                        snap.StatementedSnapshotCreditBalance = 0m;
                        snap.StatementedSnapshotDescription = foundConnectedEntity.VisitDescription;
                        //Seems like there should be some type of visit.WasPastDueOn(newStatement.PeriodEndDate) such that I can set the display status or not
                        snap.StatementedSnapshotAgingCount = foundConnectedEntity.AgingTierAsOf(newStatement.PeriodEndDate);
                        snap.StatementedSnapshotPatientName = foundConnectedEntity.PatientName;
                        snap.StatementedSnapshotVisitBalance = endingBalanceForVisit;
                        snap.StatementedSnapshotVisitDate = foundConnectedEntity.VisitDisplayDateUnformatted; //calculated field, but typically discharge date.

                        if (endingBalanceForVisit < 0m)
                        {
                            snap.StatementedSnapshotCreditBalance = endingBalanceForVisit;
                        }
                        newStatement.VpStatementVisits.Add(vpStatementVisit);

                        messages.Add(new OutboundVisitMessage
                        {
                            ActionDate = DateTime.UtcNow,
                            BillingSystemId = Math.Abs(foundConnectedEntity.BillingSystem.BillingSystemId),
                            SourceSystemKey = foundConnectedEntity.SourceSystemKey ?? foundConnectedEntity.MatchedSourceSystemKey,
                            VpOutboundVisitMessageType = VpOutboundVisitMessageTypeEnum.VisitStatementedNotification,
                            VpVisitId = foundConnectedEntity.VisitId,
                            OutboundValue = DateTime.UtcNow.Date.ToString(Format.DateFormat2),
                            IsCallCenter = foundConnectedEntity.VPGuarantor.IsOfflineGuarantor(),
                            IsAutoPay = foundConnectedEntity.VPGuarantor.UseAutoPay
                        });
                    }
                }

                if (messages.Any())
                {
                    this._session.Transaction.RegisterPostCommitSuccessAction(() => this.Bus.Value.PublishMessage(new OutboundVisitMessageList
                    {
                        Messages = messages
                    }).Wait());
                }
            }
        }
        
        private void RemoveVisitsThatWereNotActiveInTheLastStatementPeriod(IList<Visit> visitsForNewStatement, VpStatement statement)
        {
            List<Visit> visitsToRemove = new List<Visit>();

            //look at all of the statusHistories and make sure it was open during the statement period.
            foreach (Visit visit in visitsForNewStatement)
            {
                if (!visit.WasActiveBetween(statement.PeriodStartDate, statement.PeriodEndDate))
                {
                    visitsToRemove.Add(visit);
                }
            }

            foreach (Visit removeMe in visitsToRemove)
            {
                visitsForNewStatement.Remove(removeMe);
            }
        }

        private void RemoveNonFinancedVisitsVpcc(IList<Visit> visitsForNewStatement, IList<FinancePlanVisit> visitsThatWereOnFpForThatDate)
        {
            List<Visit> visitsToRemove = new List<Visit>();

            foreach (Visit visit in visitsForNewStatement)
            {
                int? foundFinancePlanId = this.FindFinancePlanIdForVisitWithFinancePlanVisitResult(visit, visitsThatWereOnFpForThatDate);
                bool visitIsFinanced = foundFinancePlanId.HasValue;

                if (visit.VPGuarantor.IsOfflineGuarantor() && !visitIsFinanced)
                {
                    // VP-7891 - dont statement nonfinanced vpcc visits
                    visitsToRemove.Add(visit);
                }
            }

            foreach (Visit removeMe in visitsToRemove)
            {
                visitsForNewStatement.Remove(removeMe);
            }
        }

        private IList<Visit> LocateHistoricalVisitsForStatementPeriod(int guarantorId, DateTime endPeriod)
        {
            IReadOnlyList<Visit> allVisits = this._visitService.Value.GetVisits(guarantorId);
            List<Visit> visits = new List<Visit>();
            foreach (Visit visit in allVisits)
            {
                if (visit.InsertDate <= endPeriod)
                {
                    visits.Add(visit);
                }
            }

            return visits;
        }

        #endregion

        #region FinancePlanStatement

        private void GenerateFinancePlanStatementVisitsForStatement(IList<Visit> visitsForNewStatement, FinancePlanStatement newFpStatement, FinancePlanStatement previousFpStatement, IList<FinancePlanVisit> visitsThatWereOnFpForThatDate)
        {
            Dictionary<int, IDictionary<int, decimal>> financePlanStartingAmountsCache = new Dictionary<int, IDictionary<int, decimal>>();
            Dictionary<int, IDictionary<int, decimal>> financePlanEndingAmountsCache = new Dictionary<int, IDictionary<int, decimal>>();

            if (visitsForNewStatement != null && visitsForNewStatement.Count > 0)
            {
                foreach (Visit foundConnectedEntity in visitsForNewStatement)
                {
                    FinancePlanVisit foundFpVisit = this.GetFinancePlanVisit(foundConnectedEntity, visitsThatWereOnFpForThatDate);
                    if (foundConnectedEntity != null && foundFpVisit != null)
                    {
                        if (!financePlanStartingAmountsCache.ContainsKey(foundFpVisit.FinancePlan.FinancePlanId))
                        {
                            if (previousFpStatement != null)
                            {
                                financePlanStartingAmountsCache.Add(foundFpVisit.FinancePlan.FinancePlanId, this._financePlanService.Value.GetFinancePlanActiveVisitBalancesAsOf(foundFpVisit.FinancePlan, previousFpStatement.PeriodEndDate));
                            }
                            else
                            {
                                financePlanStartingAmountsCache.Add(foundFpVisit.FinancePlan.FinancePlanId, this._financePlanService.Value.GetFinancePlanActiveVisitBalancesAsOf(foundFpVisit.FinancePlan, newFpStatement.PeriodStartDate.AddMilliseconds(-10)));
                            }
                        }
                        if (!financePlanEndingAmountsCache.ContainsKey(foundFpVisit.FinancePlan.FinancePlanId))
                        {
                            financePlanEndingAmountsCache.Add(foundFpVisit.FinancePlan.FinancePlanId, this._financePlanService.Value.GetFinancePlanActiveVisitBalancesAsOf(foundFpVisit.FinancePlan, newFpStatement.PeriodEndDate));
                        }

                        //decimal endingBalanceForVisit = foundConnectedEntity.CurrentBalanceAsOf(newFpStatement.PeriodEndDate);
                        FinancePlanStatementVisit fpStatementVisit = new FinancePlanStatementVisit
                        {
                            FinancePlanStatement = newFpStatement,
                            VisitId = foundConnectedEntity.VisitId,
                            StatementedSnapshotFinancePlanId = foundFpVisit.FinancePlan.FinancePlanId,
                            //There must be more here.
                            //StatementedSnapshotVisitState = foundConnectedEntity.VisitStateEnumForDate(newStatement.PeriodEndDate),

                        };
                        FinancePlanStatementVisitSnapshotVp3V1 snap = (FinancePlanStatementVisitSnapshotVp3V1)fpStatementVisit.GetSnapshotObject();
                        snap.StatementedSnapshotInterestCharged = previousFpStatement != null ? foundFpVisit.InterestAssessedForDateRange(newFpStatement.PeriodStartDate, newFpStatement.PeriodEndDate) : foundFpVisit.InterestAssessedForDateRange(null, newFpStatement.PeriodEndDate);
                        snap.StatementedSnapshotInterestPaid = previousFpStatement != null ? foundFpVisit.InterestPaidForDateRange(newFpStatement.PeriodStartDate, newFpStatement.PeriodEndDate) : foundFpVisit.InterestPaidForDateRange(null, newFpStatement.PeriodEndDate);
                        snap.StatementedSnapshotOriginalBalance = foundFpVisit.OriginatedSnapshotVisitBalance;
                        snap.StatementedSnapshotPreviousBalance = financePlanStartingAmountsCache.ContainsKey(foundFpVisit.FinancePlan.FinancePlanId) && financePlanStartingAmountsCache[foundFpVisit.FinancePlan.FinancePlanId].ContainsKey(foundFpVisit.VisitId) ? financePlanStartingAmountsCache[foundFpVisit.FinancePlan.FinancePlanId][foundFpVisit.VisitId] : 0m;
                        snap.StatementedSnapshotPrincipalPaid = previousFpStatement != null ? foundFpVisit.PrincipalPaidForDateRange(newFpStatement.PeriodStartDate, newFpStatement.PeriodEndDate) : foundFpVisit.PrincipalPaidForDateRange(null, newFpStatement.PeriodEndDate);
                        snap.StatementedSnapshotTotalBalance = financePlanEndingAmountsCache.ContainsKey(foundFpVisit.FinancePlan.FinancePlanId) && financePlanEndingAmountsCache[foundFpVisit.FinancePlan.FinancePlanId].ContainsKey(foundFpVisit.VisitId) ? financePlanEndingAmountsCache[foundFpVisit.FinancePlan.FinancePlanId][foundFpVisit.VisitId] : 0m;
                        snap.StatementedSnapshotOtherBalanceCharges = snap.StatementedSnapshotTotalBalance
                            - (snap.StatementedSnapshotPreviousBalance + snap.StatementedSnapshotPrincipalPaid + snap.StatementedSnapshotInterestPaid + snap.StatementedSnapshotInterestCharged);
                        newFpStatement.FinancePlanStatementVisits.Add(fpStatementVisit);
                    }
                }
            }
        }
        
        private void GenerateFinancePlanStatementFinancePlansForStatement(FinancePlanStatement newFpStatement, FinancePlanStatement previousFpStatement, IList<FinancePlanVisit> visitsThatWereOnFpForThatDate)
        {
            Dictionary<int, IDictionary<int, decimal>> financePlanStartingAmountsCache = new Dictionary<int, IDictionary<int, decimal>>();
            Dictionary<int, IDictionary<int, decimal>> financePlanEndingAmountsCache = new Dictionary<int, IDictionary<int, decimal>>();

            if (newFpStatement != null)
            {
                List<FinancePlan> fps = visitsThatWereOnFpForThatDate.Select(x => x.FinancePlan).Distinct().ToList();
                foreach (FinancePlan financePlan in fps)
                {
                    if (!financePlanStartingAmountsCache.ContainsKey(financePlan.FinancePlanId))
                    {
                        if (previousFpStatement != null)
                        {
                            financePlanStartingAmountsCache.Add(financePlan.FinancePlanId, this._financePlanService.Value.GetFinancePlanActiveVisitBalancesAsOf(financePlan, previousFpStatement.PeriodEndDate));
                        }
                        else
                        {
                            financePlanStartingAmountsCache.Add(financePlan.FinancePlanId, this._financePlanService.Value.GetFinancePlanActiveVisitBalancesAsOf(financePlan, newFpStatement.PeriodStartDate.AddMilliseconds(-10)));
                        }
                    }
                    if (!financePlanEndingAmountsCache.ContainsKey(financePlan.FinancePlanId))
                    {
                        financePlanEndingAmountsCache.Add(financePlan.FinancePlanId, this._financePlanService.Value.GetFinancePlanActiveVisitBalancesAsOf(financePlan, newFpStatement.PeriodEndDate));
                    }

                    FinancePlanStatementFinancePlan fpStatementFp = new FinancePlanStatementFinancePlan()
                    {
                        FinancePlanStatement = newFpStatement,
                        FinancePlan = financePlan
                    };

                    bool isFirstStatementForFinancePlan = this._financePlanStatementService.Value.IsFirstStatementForFinancePlan(financePlan.VpGuarantor.VpGuarantorId, financePlan.FinancePlanId);
                    FinancePlanStatementFinancePlanSnapshotVp3V1 snap = (FinancePlanStatementFinancePlanSnapshotVp3V1)fpStatementFp.GetSnapshotObject();

                    snap.IsFirstStatement = isFirstStatementForFinancePlan;
                    snap.StatementedSnapshotFinancePlanBalance = financePlanEndingAmountsCache.ContainsKey(financePlan.FinancePlanId) ? financePlanEndingAmountsCache[financePlan.FinancePlanId].Values.Sum() : 0m;
                    snap.StatementedSnapshotPriorFinancePlanBalance = financePlanStartingAmountsCache.ContainsKey(financePlan.FinancePlanId) ? financePlanStartingAmountsCache[financePlan.FinancePlanId].Values.Sum() : 0m;
                    snap.StatementedSnapshotFinancePlanInterestCharged = previousFpStatement != null ? financePlan.InterestAssessedForDateRange(newFpStatement.PeriodStartDate, newFpStatement.PeriodEndDate) : financePlan.InterestAssessedForDateRange(null, newFpStatement.PeriodEndDate);
                    snap.StatementedSnapshotFinancePlanInterestPaid = previousFpStatement != null ? financePlan.InterestPaidForDateRange(newFpStatement.PeriodStartDate, newFpStatement.PeriodEndDate) : financePlan.InterestPaidForDate(newFpStatement.PeriodEndDate);
                    snap.StatementedSnapshotFinancePlanInterestRate = financePlan.InterestRateAsOf(newFpStatement.PeriodEndDate);
                    snap.StatementedSnapshotFinancePlanPrincipalPaid = previousFpStatement != null ? financePlan.PrincipalPaidForDateRange(newFpStatement.PeriodStartDate, newFpStatement.PeriodEndDate) : financePlan.PrincipalPaidForDate(newFpStatement.PeriodEndDate);
                    snap.StatementedSnapshotFinancePlanMonthlyPaymentAmount = financePlan.PaymentAmountAsOf(newFpStatement.PeriodEndDate);
                    snap.StatementedSnapshotFinancePlanEffectiveDate = financePlan.OriginationDate;
                    snap.StatementedSnapshotFinancePlanPastDueAmount = financePlan.PastDueAmountAsOf(newFpStatement.PeriodEndDate);
                    snap.StatementedSnapshotFinancePlanPaymentDue = financePlan.AmountDueAsOf(newFpStatement.PeriodEndDate) + financePlan.GetNextFinancePlanAmountDue(financePlan.PaymentAmount);
                    snap.StatementedSnapshotFinancePlanStatus = financePlan.FinancePlanStatusAsOf(newFpStatement.PeriodEndDate);
                    snap.StatementedSnapshotFinancePlanOtherBalanceCharges = snap.StatementedSnapshotFinancePlanBalance
                        - (snap.StatementedSnapshotPriorFinancePlanBalance + snap.StatementedSnapshotFinancePlanPrincipalPaid + snap.StatementedSnapshotFinancePlanInterestPaid + snap.StatementedSnapshotFinancePlanInterestCharged);
                    snap.StatementedSnapshotFinancePlanOriginalBalance = financePlan.OriginatedFinancePlanBalance;

                    newFpStatement.FinancePlanStatementFinancePlans.Add(fpStatementFp);
                }
            }
        }

        private void GenerateFinancePlanStatementSumsForStatement(FinancePlanStatement newFpStatement, FinancePlanStatement previousFinancePlanStatement)
        {
            if (newFpStatement != null)
            {
                //this._visitService.Value.

                FinancePlanStatementSnapshotVp3V1 snap = (FinancePlanStatementSnapshotVp3V1)newFpStatement.GetSnapshotObject();
                snap.StatementedSnapshotFinancePlanInterestChargedForStatementYearSum = this.InterestChargedForStatementYearSum(newFpStatement);
                snap.StatementedSnapshotFinancePlanFeesChargedForStatementYearSum = 0m;
                snap.StatementedSnapshotFinancePlanPaymentDueSum = newFpStatement.FinancePlanStatementFinancePlans.Sum(x => ((FinancePlanStatementFinancePlanSnapshotVp3V1)x.GetSnapshotObject()).StatementedSnapshotFinancePlanPaymentDue);
                snap.StatementedSnapshotFinancePlanInterestChargedSum = newFpStatement.FinancePlanStatementFinancePlans.Sum(x => ((FinancePlanStatementFinancePlanSnapshotVp3V1)x.GetSnapshotObject()).StatementedSnapshotFinancePlanInterestCharged);
                snap.StatementedSnapshotFinancePlanBalanceSum = newFpStatement.FinancePlanStatementFinancePlans.Sum(x => ((FinancePlanStatementFinancePlanSnapshotVp3V1)x.GetSnapshotObject()).StatementedSnapshotFinancePlanBalance);

                newFpStatement.PriorStatementDate = previousFinancePlanStatement?.StatementDate;
            }
        }

        private decimal InterestChargedForStatementYearSum(FinancePlanStatement newFpStatement)
        {
            decimal fromTrans = this._visitTransactionService.Value.GetSumOfAllInterestAssessedForDateRange(newFpStatement.VpGuarantorId, new DateTime(newFpStatement.PeriodEndDate.Year, 1, 1), newFpStatement.PeriodEndDate);
            IList<int> visitIdThatWereActive = this._visitService.Value.GetVisitsThatWereActiveForDateRange(newFpStatement.VpGuarantorId, new DateTime(newFpStatement.PeriodEndDate.Year, 1, 1), newFpStatement.PeriodEndDate);
            IList<FinancePlanVisitInterestDueResult> results = this._financePlanService.Value.GetInterestAssessedForVisits(visitIdThatWereActive);
            decimal snapStatementedSnapshotFinancePlanInterestChargedForStatementYearSum = results.Where(x => (x.OverrideInsertDate ?? x.InsertDate) >= new DateTime(newFpStatement.PeriodEndDate.Year, 1, 1) && (x.OverrideInsertDate ?? x.InsertDate) <= newFpStatement.PeriodEndDate).Sum(x => x.InterestDue);
            return snapStatementedSnapshotFinancePlanInterestChargedForStatementYearSum + fromTrans;
        }

        #endregion
    }
}
﻿
namespace Ivh.Application.FinanceManagement.ApplicationServices
{
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Core.Common.Utilities.Builders;
    using Domain.Content.Interfaces;
    using Domain.FinanceManagement.Interfaces;
    using Domain.FinanceManagement.Payment.Entities;
    using Ivh.Common.VisitPay.Enums;
    using OfficeOpenXml;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class PaymentQueueApplicationService : ApplicationService, IPaymentQueueApplicationService
    {
        private readonly Lazy<IPaymentService> _paymentService;
        private readonly Lazy<IImageService> _imageService;

        public PaymentQueueApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IPaymentService> paymentService,
            Lazy<IImageService> imageService) : base(applicationServiceCommonService)
        {
            this._paymentService = paymentService;
            this._imageService = imageService;
        }

        public IList<string> GetAllPaymentImportBatchNumbers()
        {
            return this._paymentService.Value.GetAllPaymentImportBatchNumbers();
        }


        public PaymentImportQueueDto LoadPaymentImportQueue(PaymentImportFilterDto paymentImportFilter)
        {
            PaymentImportFilter filter = Mapper.Map<PaymentImportFilter>(paymentImportFilter);
            IList<PaymentImport> paymentImports = this._paymentService.Value.GetPaymentImports(filter);

            IList<PaymentImportDto> paymentImportResults = paymentImports.Select(p => Mapper.Map<PaymentImportDto>(p)).ToList();

            //Get the totals
            decimal? allPaymentsTotal = paymentImportResults.Sum(p => p.InvoiceAmount);
            decimal? postedPaymentsTotal = paymentImportResults
                                                .Where(p => p.PaymentImportStateEnum == PaymentImportStateEnum.Success)
                                                .Sum(p => p.InvoiceAmount);
            decimal? unpostedPaymentsTotal = paymentImportResults
                                                .Where(p => p.PaymentImportStateEnum == PaymentImportStateEnum.Failed || p.PaymentImportStateEnum == PaymentImportStateEnum.Pending)
                                                .Sum(p => p.InvoiceAmount);
            decimal? nonVisitPayPaymentsTotal = paymentImportResults
                                                    .Where(p => p.PaymentImportStateEnum == PaymentImportStateEnum.Cancelled)
                                                    .Sum(p => p.InvoiceAmount);

            PaymentImportQueueDto queue = new PaymentImportQueueDto()
            {
                PaymentImports = paymentImportResults,
                AllPaymentsTotal = allPaymentsTotal,
                PostedPaymentsTotal = postedPaymentsTotal,
                UnpostedPaymentsTotal = unpostedPaymentsTotal,
                NonVisitPayPaymentsTotal = nonVisitPayPaymentsTotal
            };

            return queue;
        }

        public async Task<byte[]> ExportPaymentImportQueueAsync(PaymentImportFilterDto paymentImportFilter, string visitPayUserName)
        {
            PaymentImportQueueDto paymentImportQueueDto = this.LoadPaymentImportQueue(paymentImportFilter);
            System.Drawing.Image logo = await this._imageService.Value.GetExportLogoAsync();


            using (ExcelPackage package = new ExcelPackage())
            {
                iVinciExcelExportBuilder builder = new iVinciExcelExportBuilder(package, $"Payment Queue Export")
                    .WithWorkSheetNameOf("Payment Queue Export")
                    .WithLogo(logo)
                    .AddInfoBlockRow(new ColumnBuilder(paymentImportQueueDto.PostedPaymentsTotal, "Posted Payments").WithCurrencyFormat())
                    .AddInfoBlockRow(new ColumnBuilder(paymentImportQueueDto.UnpostedPaymentsTotal, "Unposted Payments").WithCurrencyFormat())
                    .AddInfoBlockRow(new ColumnBuilder(paymentImportQueueDto.NonVisitPayPaymentsTotal, "Non-VisitPay Payments").WithCurrencyFormat())
                    .AddInfoBlockRow(new ColumnBuilder(paymentImportQueueDto.AllPaymentsTotal, "Total").WithCurrencyFormat())
                    .AddInfoBlockRow(new ColumnBuilder("", ""))//spacer
                    .AddInfoBlockRow(new ColumnBuilder(visitPayUserName, "Exported By").WithStyle(StyleTypeEnum.AlignLeft))
                    .AddDataBlock(paymentImportQueueDto.PaymentImports, paymentImportDto => new List<Column>
                    {
                        new ColumnBuilder(paymentImportDto.BatchCreditDate, "Date").WithDateFormat(),
                        new ColumnBuilder(paymentImportDto.PaymentImportStateEnum.ToString(), "Status").AlignLeft(),
                        new ColumnBuilder(paymentImportDto.LockBoxNumber, "Lockbox #").AlignLeft(),
                        new ColumnBuilder(paymentImportDto.BatchNumber, "Batch #").AlignLeft(),
                        new ColumnBuilder(paymentImportDto.PaymentSequenceNumber, "Item #").AlignLeft(),
                        new ColumnBuilder(paymentImportDto.Guarantor?.VpGuarantorId, this.Client.Value.VpGuarantorPatientIdentifier).AlignLeft(),
                        new ColumnBuilder(paymentImportDto.GuarantorLastName, "Last Name").AlignLeft(),
                        new ColumnBuilder(paymentImportDto.GuarantorFirstName, "First Name").AlignLeft(),
                        new ColumnBuilder(paymentImportDto.GuarantorAccountNumber, this.Client.Value.HsGuarantorPatientIdentifier).AlignLeft(),
                        new ColumnBuilder(paymentImportDto.InvoiceAmount, "Amount Due").WithCurrencyFormat(),
                        new ColumnBuilder(paymentImportDto.PaymentImportStateReason, "Exceptions").AlignLeft(),

                    })
                    .WithNoRecordsFoundMessageOf("There are no payment queue records to display at this time.");

                builder.Build();
                return package.GetAsByteArray();
            }

        }
    }
}

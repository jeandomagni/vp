﻿namespace Ivh.Application.FinanceManagement.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Content.Common.Dtos;
    using Content.Common.Interfaces;
    using Core.Common.Dtos;
    using Core.Common.Interfaces;
    using Core.Common.Models.PaperStatement;
    using Domain.FileStorage.Entities;
    using Domain.FileStorage.Interfaces;
    using Domain.FinanceManagement.Statement.Entities;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.Settings.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Jobs;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using Ivh.Common.VisitPay.Strings;
    using Ivh.Domain.HospitalData.Outbound.Enums;
    using Newtonsoft.Json;
    using NHibernate;
    using static Ivh.Common.Base.Constants.SystemConstants;

    public class StatementExportApplicationService : ApplicationService, IStatementExportApplicationService
    {
        private readonly ISession _session;
        private readonly Lazy<IStatementExportService> _statementExportService;
        private readonly Lazy<IVpStatementService> _vpStatementService;
        private readonly Lazy<IApplicationSettingsService> _applicationSettingsService;
        private readonly Lazy<IContentApplicationService> _contentApplicationService;
        private readonly Lazy<IGuarantorApplicationService> _guarantorApplicationService;
        private readonly Lazy<IFinancePlanApplicationService> _financePlanApplicationService;
        private readonly Lazy<IClientApplicationService> _clientApplicationService;
        private readonly Lazy<IStatementApplicationService> _statementApplicationService;
        private readonly Lazy<IVisitTransactionApplicationService> _visitTransactionApplicationService;
        private readonly Lazy<IFinancePlanStatementService> _financePlanStatementService;
        private readonly Lazy<IVpStatementDocumentService> _statementDocumentService;

        public StatementExportApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IApplicationSettingsService> applicationSettingsService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            ISessionContext<VisitPay> sessionContext,
            Lazy<IStatementExportService> statementExportService,
            Lazy<IVpStatementService> vpStatementService,
            Lazy<IFinancePlanApplicationService> financePlanApplicationService,
            Lazy<IClientApplicationService> clientApplicationService,
            Lazy<IStatementApplicationService> statementApplicationService,
            Lazy<IVisitTransactionApplicationService> visitTransactionApplicationService,
            Lazy<IFinancePlanStatementService> financePlanStatementService,
            Lazy<IVpStatementDocumentService> statementDocumentService) : base(applicationServiceCommonService)
        {
            this._session = sessionContext.Session;
            this._statementExportService = statementExportService;
            this._vpStatementService = vpStatementService;
            this._applicationSettingsService = applicationSettingsService;
            this._contentApplicationService = contentApplicationService;
            this._guarantorApplicationService = guarantorApplicationService;
            this._financePlanApplicationService = financePlanApplicationService;
            this._clientApplicationService = clientApplicationService;
            this._statementApplicationService = statementApplicationService;
            this._visitTransactionApplicationService = visitTransactionApplicationService;
            this._financePlanStatementService = financePlanStatementService;
            this._statementDocumentService = statementDocumentService;
        }

        #region Jobs
        void IJobRunnerService<StatementExportBatchProcessJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            if (this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.OfflineVisitPay))
            {
                this.ExportPaperStatements(begin.Date);
            }
        }
        #endregion

        #region Export Statement workflow

        public void ExportPaperStatements(DateTime date)
        {
            // get the statements that need to be exported.
            IList<VpStatement> paperStatementsAll = this._vpStatementService.Value.GetStatementsForCreatingPaperStatements();

            //determine which ones are statements for finance plans
            List<int> paperVpStatementIdsAll = paperStatementsAll.Select(x => x.VpStatementId).ToList();
            IList<FinancePlanStatement> paperFinancePlanStatements = this._financePlanStatementService.Value.GetFinancePlanStatementByVpStatementIds(paperVpStatementIdsAll);

            //determine which ones are NOT finance plan statements
            List<int> paperFinancePlanVpStatementIds = paperFinancePlanStatements.Select(x => x.VpStatementId).ToList();
            IList<VpStatement> paperNonFinancedStatements = paperStatementsAll.Where(x => !paperFinancePlanVpStatementIds.Contains(x.VpStatementId)).ToList();

            this.LoggingService.Value.Info(() => 
            { 
                string vpStatementIds = string.Join(", ", paperVpStatementIdsAll);
                string financePlanVpStatementIds = string.Join(", ", paperFinancePlanVpStatementIds);
                return $"{nameof(this.ExportPaperStatements)} - VpStatementIds found: {vpStatementIds}, FinancePlanVpStatementIds found: {financePlanVpStatementIds}";
            });

            if (paperNonFinancedStatements.Any() || paperFinancePlanStatements.Any())
            {
                IList<VisitPayPaperBillingStatement> allNonFinancedPaperStatements = new List<VisitPayPaperBillingStatement>();
                foreach (VpStatement statement in paperNonFinancedStatements)
                {
                    VisitPayPaperBillingStatement visitPayPaperBillingStatement = this.GetVisitPayPaperBillingStatement(statement);
                    allNonFinancedPaperStatements.Add(visitPayPaperBillingStatement);
                }

                IList<VisitPayPaperFinancePlanStatement> allFinancedPaperStatements = new List<VisitPayPaperFinancePlanStatement>();

                foreach (FinancePlanStatement financePlanStatement in paperFinancePlanStatements)
                {
                    VisitPayPaperFinancePlanStatement visitPayPaperFinancePlanStatement = this.GetVisitPayPaperFinancePlanStatement(financePlanStatement);
                    allFinancedPaperStatements.Add(visitPayPaperFinancePlanStatement);
                }

                using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
                {
                    //We can serialize to a flatter delimited file format later if needed
                    //Export non-financed paper statements
                    string jsonNonFinanced = JsonConvert.SerializeObject(allNonFinancedPaperStatements, Formatting.Indented);
                    FileStored nonFinancedFileStored = this._statementExportService.Value.ExportOfflineStatements(jsonNonFinanced);

                    //Only update as processed if file was saved successfully
                    if (nonFinancedFileStored != null)
                    {
                        VpOutboundFileTypeEnum nonFinancePlanFileType = VpOutboundFileTypeEnum.NonFinancePlanStatements;

                        //Mark as processed
                        this.UpdateStatementsAsProcessed(paperNonFinancedStatements, nonFinancedFileStored, nonFinancePlanFileType);

                        //Send svc bus message
                        this.SendOutboundPaperFileMessage(nonFinancedFileStored, nonFinancePlanFileType);
                    }

                    //Export financed paper statements
                    string jsonFinanced = JsonConvert.SerializeObject(allFinancedPaperStatements, Formatting.Indented);
                    FileStored financedFileStored = this._statementExportService.Value.ExportOfflineStatements(jsonFinanced);

                    //Only update as processed if file was saved successfully
                    if (financedFileStored != null)
                    {
                        VpOutboundFileTypeEnum financePlanFileType = VpOutboundFileTypeEnum.FinancePlanStatements;

                        //Get the finance plan VpStatements
                        IList<int> financePlanStatementIds = paperFinancePlanStatements.Select(x => x.VpStatementId).ToList();
                        IList<VpStatement> financePlanStatements = paperStatementsAll.Where(x => financePlanStatementIds.Contains(x.VpStatementId)).ToList();

                        //Mark as processed
                        this.UpdateStatementsAsProcessed(financePlanStatements, financedFileStored, financePlanFileType);

                        //Send svc bus message
                        this.SendOutboundPaperFileMessage(financedFileStored, financePlanFileType);
                    }

                    unitOfWork.Commit();
                }
            }
        }

        private void SendOutboundPaperFileMessage(FileStored file, VpOutboundFileTypeEnum fileType)
        {
            OutboundPaperFileMessage message = new OutboundPaperFileMessage()
            {
                FileStorageExternalKey = file.ExternalKey,
                FileName = file.Filename,
                FileTypeId = (int)fileType
            };
            this.Bus.Value.PublishMessage(message).Wait();
        }

        private void UpdateStatementsAsProcessed(IList<VpStatement> offlineStatements, FileStored fileStored, VpOutboundFileTypeEnum fileType)
        {
            DateTime processedDate = DateTime.UtcNow;
            foreach (VpStatement statement in offlineStatements)
            {
                //Update paper statement sent date
                statement.PaperStatementSentDate = processedDate;
                this._vpStatementService.Value.InsertOrUpdate(statement);

                //Add VpStatementDocument record for the statement
                VpStatementDocument statementDocument = new VpStatementDocument()
                {
                    VpStatementId = statement.VpStatementId,
                    VpOutboundFileType = fileType,
                    FileStorageExternalKey = fileStored.ExternalKey,
                    FileName = fileStored.Filename,
                    MimeType = fileStored.FileMimeType,
                    InsertDate = processedDate
                };
                this._statementDocumentService.Value.InsertOrUpdate(statementDocument);
            }
        }

        #endregion

        #region Construct VisitPayPaperFinancePlanStatement

        //Localization dictionaries may require some translations for new entries
        private VisitPayPaperFinancePlanStatement GetVisitPayPaperFinancePlanStatement(FinancePlanStatement financePlanStatement)
        {
            int vpGuarantorId = financePlanStatement.VpGuarantorId;
            int financePlanStatementId = financePlanStatement.FinancePlanStatementId;

            GuarantorDto guarantorDto = this._guarantorApplicationService.Value.GetGuarantor(vpGuarantorId);
            string userLocale = guarantorDto.User.Locale;
            if (string.IsNullOrWhiteSpace(userLocale))
            {
                userLocale = LocalizationDefaultLocale;
            }

            int visitPayUserId = guarantorDto.User.VisitPayUserId;

            IDictionary<string, string> clientReplacementValues = this._contentApplicationService.Value.ClientReplacementValues();
            ClientDto clientDto = this._clientApplicationService.Value.GetClient();
            FinancePlanStatementDto financePlanStatementDto = this._statementApplicationService.Value.GetFinancePlanStatement(financePlanStatementId, visitPayUserId);

            string newLineJson = "\\n";
           
            string accountNumber = guarantorDto.StatementAccountNumber;
            string statementPeriod = $"{financePlanStatementDto.PeriodStartDate.ToDateText()} - {financePlanStatementDto.PeriodEndDate.ToDateText()}";
            string guarantorName = $"{guarantorDto.User.FirstName} {guarantorDto.User.LastName}";
            string paymentUrl = FormatHelper.DisplayUrl(clientDto.AppGuarantorUrlHost);
           
            CmsVersionDto tagLineCms = this._contentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.LimitedEnglishProficiencyTagLines, true).Result;
            string tagLines = tagLineCms.ContentBody;
            if (!string.IsNullOrWhiteSpace(tagLines))
            {
                //TODO: CMS is expected to be HTML, so non-HTML targets don't have CMS conversion strategies
                tagLines = tagLines.Replace("<br/>", newLineJson);
            }
            else
            {
                tagLines = string.Empty;
            }

            CultureInfo cultureInfo = new CultureInfo(userLocale);
            using (new CultureKeeper(cultureInfo))
            {
                AccountStatusInfo accountStatusInfo = this.GetPaperFinancePlanStatementAccountStatus(financePlanStatementDto);

                string doesNotDiscriminate = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.DoesNotDiscriminate);
                doesNotDiscriminate = ReplacementUtility.Replace(doesNotDiscriminate, clientReplacementValues, null, false);

                VisitPayPaperFinancePlanStatement visitPayPaperBillingStatement = new VisitPayPaperFinancePlanStatement()
                {
                    ClientLogo = $"{clientDto.ClientShortName.ToLower()}.png", //Where to get value? Might need new field
                    StatementName = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.FinancePlanStatement),
                    StatementPeriod = statementPeriod,
                    AccountHolderName = guarantorName,
                    AccountNumberName = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.AccountNumberShort),
                    AccountNumber = accountNumber,
                    NextPaymentText = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.NextPayment),
                    PaymentAmountDue = (financePlanStatementDto.StatementedSnapshotFinancePlanPaymentDueSum ?? 0).FormatCurrency(),
                    DueDateText = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.PaymentDueDateShort),
                    DueDate = financePlanStatementDto.PaymentDueDate.ToString(Format.DateFormat),
                    AccountStatusFillColor = accountStatusInfo.AccountStatusColor,
                    AccountStatusText = accountStatusInfo.AccountStatusText,
                    //NotificationsName = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.Notifications),
                    CustomerServiceName = $"{this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.CustomerService)}:",
                    //TODO: tag is a client setting with FormatHelper.DisplayUrl called on it, need to make this generic somehow
                    FinancePlanFooterText = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.FinancePlanDetailsMore).Replace("[[GuarantorUrlHost]]", paymentUrl),
                    //FinancialAssistanceHeaderText = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.FinanceAssistancePlainLanguage),
                    DetachCouponText = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.DetachCoupon).ToUpper(),
                    PayableToText = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.PayableTo),
                    //TODO: detect if autopay and blank fields if not on autopay
                    //AutoPayLogo = "", // "autopay.png", 
                    //AutoPayText = "", // this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.YouAreEnrolledInAutopay),
                    PayAmountText = $"{this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.PayThisAmount).ToUpper()}:",
                    AccountHolderText = $"{this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.GuarantorName).ToUpper()}:",
                    AmountEnclosedText = $"{this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.AmountEnclosed).ToUpper()}:",
                    ReturnPenaltyText = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.PaymentReturnPenalty).Replace("[[ReturnedPaymentPenaltyFee]]", clientDto.ReturnedPaymentPenaltyFeeAmount.FormatCurrency()),
                    PageText = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.Page),
                    OfText = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.Of).ToLower(),
                    FooterText = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.PoweredBy),
                    FooterLogo = "visitpay.png", //Where to get value? Might need new field
                    Language = userLocale,
                    StatementDisclaimerText = doesNotDiscriminate + newLineJson + tagLines,

                    ReturnAddressInfo = new AddressInfo
                    {
                        Name1 = clientDto.ReturnMailingAddress?.Name1 ?? "",
                        Name2 = clientDto.ReturnMailingAddress?.Name2 ?? "",
                        Address1 = clientDto.ReturnMailingAddress?.Address1 ?? "",
                        Address2 = clientDto.ReturnMailingAddress?.Address2 ?? "",
                        City = clientDto.ReturnMailingAddress?.City ?? "",
                        State = clientDto.ReturnMailingAddress?.State ?? "",
                        Zip = clientDto.ReturnMailingAddress?.Zip ?? ""
                    },

                    MailingAddressInfo = new AddressInfo
                    {
                        Name1 = guarantorName,
                        Name2 = "",
                        Address1 = guarantorDto.User.MailingAddressStreet1 ?? "",
                        Address2 = guarantorDto.User.MailingAddressStreet2 ?? "",
                        City = guarantorDto.User.MailingCity ?? "",
                        State = guarantorDto.User.MailingState ?? "",
                        Zip = guarantorDto.User.MailingZip ?? ""
                    },

                    RemitAddressInfo = new AddressInfo
                    {
                        Name1 = clientDto.RemitMailingAddress?.Name1 ?? "",
                        Name2 = clientDto.RemitMailingAddress?.Name2 ?? "",
                        Address1 = clientDto.RemitMailingAddress?.Address1 ?? "",
                        Address2 = clientDto.RemitMailingAddress?.Address2 ?? "",
                        City = clientDto.RemitMailingAddress?.City ?? "",
                        State = clientDto.RemitMailingAddress?.State ?? "",
                        Zip = clientDto.RemitMailingAddress?.Zip ?? ""
                    },

                    PaymentOptionList = this.GetFinancePlanStatementPaymentOptionList(clientDto),
                    //NotificationList = this.GetFinancePlanPaperStatementNotificationList(financePlanStatementDto),
                    CustomerServiceContactMethodList = this.GetFinancePlanCustomerServiceContactMethodList(clientDto),
                    //FinancialAssistanceList = this.GetFinancePlanFinancialAssistanceList(clientDto),

                    FinancePlansSummary = this.GetFinancePlansSummary(financePlanStatementDto),

                    FinancePlanInfoList = new List<FinancePlanInfo>(), // Will be built up below
                };

                visitPayPaperBillingStatement.OcrText = this.GetScanLineJpmc(
                    guarantorDto?.User?.LastName,
                    guarantorDto?.User?.FirstName,
                    accountNumber,
                    financePlanStatementDto.StatementedSnapshotFinancePlanPaymentDueSum
                );

                int financePlanIndex = 1;
                foreach (FinancePlanStatementFinancePlanDto financePlanStatementFinancePlanDto in financePlanStatementDto.FinancePlanStatementFinancePlans)
                {
                    FinancePlanDto financePlanDto = this._financePlanApplicationService.Value.GetFinancePlan(visitPayUserId, financePlanStatementFinancePlanDto.FinancePlanId, vpGuarantorId);
                    FinancePlanInfo financePlanInfo = this.GetFinancePlanInfo(financePlanStatementFinancePlanDto, financePlanStatementDto, financePlanDto, financePlanIndex);
                    visitPayPaperBillingStatement.FinancePlanInfoList.Add(financePlanInfo);
                    financePlanIndex++;
                }

                return visitPayPaperBillingStatement;
            }
        }

        public string GetScanLineJpmc(string lastName, string firstName, string accountNumber, decimal? balance)
        {
            //JP Morgan Chase OCR scan line specs

            //Guarantor Last Name=16 Alphanumeric Digits (Alpha conversion: A=10…Z=35)
            //Guarantor First Name=16 Alphanumeric digits (Alpha conversion: A=10…Z=35)
            //Guarantor Account Number=16 numeric digits
            //Amount Due=10 numeric digits
            //Check Digit=1 numeric digits (Mod 10, weights 2121, Sum of Values, Modulus-(Sum Mod Modulus))

            string ocrLastName = (lastName ?? "").ToUpper().RemoveNonAlphaNumericCharacters().PadLeft(16, '0').Truncate(16);
            string ocrFirstName = (firstName ?? "").ToUpper().RemoveNonAlphaNumericCharacters().PadLeft(16, '0').Truncate(16);
            string ocrAccountNumber = (accountNumber ?? "").ToUpper().RemoveNonAlphaNumericCharacters().PadLeft(16, '0').Truncate(16);
            int amountDueInCents = (int)((balance ?? 0m) * 100);
            string ocrAmountDue = amountDueInCents.ToString().PadLeft(10, '0').Truncate(10);

            string ocrScanLine = $"{ocrLastName}{ocrFirstName}{ocrAccountNumber}{ocrAmountDue}";
            int checkDigit = ocrScanLine.GetMod10CheckDigitJpmc();

            return $"{ocrScanLine}{checkDigit}";
        }

        AccountStatusInfo GetPaperFinancePlanStatementAccountStatus(FinancePlanStatementDto financePlanStatementDto)
        {
            bool isPastDue = financePlanStatementDto.FinancePlanStatementFinancePlans.Any(x => x.StatementedSnapshotFinancePlanStatus?.FinancePlanStatusEnum.IsInCategory(FinancePlanStatusEnumCategory.PastDue) ?? false);
            string yourAccountIs = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.YourAccountIs);
            string pastDue = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.PastDue);
            string yourFinancePlanIsInGoodStanding = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.YourFinancePlanIsInGoodStanding);

            AccountStatusInfo accountStatusInfo = new AccountStatusInfo()
            {
                AccountStatusText = $"{yourFinancePlanIsInGoodStanding}.",
                AccountStatusColor = "green" //TODO: make constant and possibly RGB value for target print vendor
            };
            if (isPastDue)
            {
                accountStatusInfo.AccountStatusColor = "red"; //TODO: make constant and possibly RGB value for target print vendor
                accountStatusInfo.AccountStatusText = $"{yourAccountIs} {pastDue}.";
                accountStatusInfo.AccountStatusLogo = "flag.png";
            }

            return accountStatusInfo;
        }

        List<PaymentOption> GetFinancePlanStatementPaymentOptionList(ClientDto clientDto)
        {
            string paymentUrl = FormatHelper.DisplayUrl(clientDto.AppGuarantorUrlHost);

            List<PaymentOption> paymentOptionList = new List<PaymentOption>()
            {
                new PaymentOption(){
                    PaymentOptionLogo = "computer.png",
                    PaymentOptionTitle = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.RegisterOnline),
                    PaymentOptionDescription = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.MakePaymentPhonePc),
                    PaymentOptionHighlight = paymentUrl
                },
                new PaymentOption(){
                    PaymentOptionLogo = "billEnvelope.png",
                    PaymentOptionTitle = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.PayByMail),
                    PaymentOptionDescription = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.DetachBottom),
                    PaymentOptionHighlight = ""
                },
                new PaymentOption(){
                    PaymentOptionLogo = "phone.png",
                    PaymentOptionTitle = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.PayByPhone),
                    PaymentOptionDescription = $"{this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.CallUsAt)} {clientDto.SupportPhone}",
                    PaymentOptionHighlight = ""
                }
            };
            return paymentOptionList;
        }

        List<Notification> GetFinancePlanPaperStatementNotificationList(FinancePlanStatementDto financePlanStatementDto)
        {
            List<Notification> notificationList = new List<Notification>();

            if (financePlanStatementDto.FinancePlanStatementFinancePlans.IsNullOrEmpty())
            {
                notificationList.Add(new Notification() { NotificationText = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.NoActiveFinancePlans) });
            }

            if (!notificationList.Any())
            {
                notificationList.Add(new Notification() { NotificationText = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.NoNotifications) });
            }

            return notificationList;
        }

        List<CustomerServiceContactMethod> GetFinancePlanCustomerServiceContactMethodList(ClientDto clientDto)
        {
            List<CustomerServiceContactMethod> customerServiceContactMethodList = new List<CustomerServiceContactMethod>()
            {
                new CustomerServiceContactMethod(){ContactText = clientDto.SupportPhone},
                //unable to get localized BusinessHours content since value comes from common.ClientSetting table
                new CustomerServiceContactMethod(){ContactText = clientDto.BusinessHours}, 
            };

            return customerServiceContactMethodList;
        }

        // Currently this is no longer needed by clients
        // Keeping around since client mentioned it is static from print vendor
        // Might not be localized by print vendor
        // Or we might need to make the web statement look like the printed statement in the future

        ////TODO: Finish this method once product has provided a concrete list of finance plan paper statement financial assistance text
        ////TODO: Localization needed for this content. It is similar to CmsRegionId -165, except it is not html and has many blocks of content
        //List<FinancialAssistance> GetFinancePlanFinancialAssistanceList(ClientDto clientDto)
        //{
        //    IEnumerable<string> languageList = clientDto.LanguageAppSupported.Select(x => x.Description.Split('(').First());
        //    string languages = string.Join(", ", languageList.ToArray());
        //    string federalPovertyUrl = "http://aspe.hhs.gov/poverty/index.cfm"; //TODO: needs to be constant
        //    string financialAssistanceUrl = FormatHelper.DisplayUrl(clientDto.FinancialAssistanceUrl);
        //    string clientLongAndShortName = $"{clientDto.ClientFullName} ({clientDto.ClientShortName})";
        //    string clientShortName = clientDto.ClientShortName;
        //    string financialAssistancePhone = clientDto.FinancialAssistanceSupportPhone;
        //    //TODO: need client FinancialAssistanceAddress (single line) field
        //    string financialAssistanceAddress = "123 Victory St Suite 200, Boise ID 83709";
        //    //TODO: need client CreditHistoryReportingDaysLimitDisplay field
        //    string creditHistoryReportingDaysLimitDisplay = "120"; //TODO: needs to be client setting

        //    List<FinancialAssistance> financialAssistanceList = new List<FinancialAssistance>()
        //    {
        //        new FinancialAssistance()
        //        {
        //            FinancialAssistanceTopic = "About Getting Assistance with Your Bill",
        //            FinancialAssistanceText = $"This is for anyone who receives hospital services from a {clientLongAndShortName} affiliated hospital. The Financial Assistance Policy and the Plain Language Summary for obtaining assistance with your bill are available in {languages} and other languages upon request. Financial assistance does not apply to bills from doctors, outside labs, or other providers."
        //        },
        //        new FinancialAssistance()
        //        {
        //            FinancialAssistanceTopic = "How Do I Qualify for Financial Assistance?",
        //            FinancialAssistanceText = $"You can ask for help with your bill at any time during your hospital stay or billing process. We will determine how much you owe by reviewing income, assets, or other resources. If your yearly income is less than or equal to 200% of the current Federal Poverty Guideline, you may receive some financial assistance. Federal Poverty Guidelines can be found at: {federalPovertyUrl}. You may qualify for assistance with all or part of your hospital bill. The help is based on a sliding scale that considers your yearly income and family size."
        //        },
        //        new FinancialAssistance()
        //        {
        //            FinancialAssistanceTopic = "How Can I Apply for Financial Assistance?",
        //            FinancialAssistanceText = $"To obtain a free copy of the Financial Assistance Application, Plain Language Summary, or Financial Assistance policy go to: {financialAssistanceUrl}. You can also pick up free paper copies, request free copies by mail, or receive help with the application in person at any {clientShortName} hospital in the admitting department, or at {financialAssistanceAddress}. You can ask for assistance with the Financial Assistance Policy or the application by calling the Customer Service Department at {financialAssistancePhone}. In some cases you may receive financial assistance from the hospital without applying."
        //        },
        //        new FinancialAssistance()
        //        {
        //            FinancialAssistanceTopic = "Paperwork",
        //            FinancialAssistanceText = "You are responsible for providing information timely about your health benefits, income, assets, and any other paperwork that will help to see if you qualify. Paperwork might be bank statements, income tax forms, check stubs, or other information."
        //        },
        //        new FinancialAssistance()
        //        {
        //            FinancialAssistanceTopic = "Emergency and Medically-Necessary Care",
        //            FinancialAssistanceText = "If you qualify for help with your bill, you will not be charged more for emergency or medically-necessary care than amounts generally billed to people who have insurance coverage for the same kind of care. To determine amounts generally billed we use a look-back method (we compare the amount paid by insured patients and their insurance companies in the prior year)."
        //        },
        //        new FinancialAssistance()
        //        {
        //            FinancialAssistanceTopic = "Collection Activities",
        //            FinancialAssistanceText = $"Bills that are not paid {creditHistoryReportingDaysLimitDisplay} days after the first billing date may be reported on your or your guarantor’s credit history. You or the guarantor can apply for help with your bill at any time during the collection process by contacting the Customer Service Department at {financialAssistancePhone}. We generally do not sue patients, take action against personal property, or garnish wages."
        //        }
        //    };

        //    return financialAssistanceList;
        //}

        FinancePlansSummary GetFinancePlansSummary(FinancePlanStatementDto financePlanStatementDto)
        {
            FinancePlansSummary financePlansSummary = new FinancePlansSummary
            {
                FinancePlansSummaryOverviewName = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.FinancePlanOverview),
                FinancePlansSummaryTotalInterestDueName = $"{this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.TotalInterestDue)}:",
                FinancePlansSummaryTotalPrincipalDueName = $"{this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.TotalPrincipalDue)}:",
                FinancePlansSummaryNextPaymentName = $"{this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.NextPayment)}:",
                FinancePlansSummaryTotalBalanceName = $"{this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.TotalBalance)}:",

                FinancePlansSummaryTotalInterestDue = (financePlanStatementDto.FinancePlanStatementFinancePlans.Sum(x => x.StatementedSnapshotFinancePlanInterestDue) ?? 0m).FormatCurrency(),
                FinancePlansSummaryTotalPrincipalDue = (financePlanStatementDto.FinancePlanStatementFinancePlans.Sum(x => x.StatementedSnapshotFinancePlanPrincipalDue) ?? 0m).FormatCurrency(),
                FinancePlansSummaryNextPayment = (financePlanStatementDto.StatementedSnapshotFinancePlanPaymentDueSum ?? 0).FormatCurrency(),
                FinancePlansSummaryTotalBalance = (financePlanStatementDto.StatementedSnapshotFinancePlanBalanceSum ?? 0).FormatCurrency(),
            };

            return financePlansSummary;
        }

        FinancePlanInfo GetFinancePlanInfo(FinancePlanStatementFinancePlanDto financePlanStatementFinancePlanDto, FinancePlanStatementDto financePlanStatementDto, FinancePlanDto financePlanDto, int financePlanIndex)
        {
            FinancePlanInfo financePlanInfo = new FinancePlanInfo()
            {
                FinancePlanName = $"{this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.FinancePlan)} {financePlanIndex}",
                EffectiveDateText = $"{this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.EffectiveDate)}:",
                EffectiveDate = financePlanStatementFinancePlanDto.StatementedSnapshotFinancePlanEffectiveDate.ToDateText(),
                OriginalBalanceText = $"{this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.OriginalBalance)}:",
                OriginalBalance = financePlanStatementFinancePlanDto.StatementedSnapshotFinancePlanOriginalBalance.FormatCurrency(),
                InterestRateText = $"{this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.InterestRate)}:",
                InterestRate = financePlanStatementFinancePlanDto.StatementedSnapshotFinancePlanInterestRate.FormatPercentage(),
                FinancePlanBalanceText = $"{this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.Balance)}:",
                FinancePlanBalanceAmount = financePlanStatementFinancePlanDto.StatementedSnapshotFinancePlanBalance.FormatCurrency(),
                FinancePlanMonthsRemainingText = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.MonthsRemaining),
                FinancePlanMonthsRemaining = financePlanDto.MonthsRemaining.ToString(),
                PaymentDetailsText = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.PaymentDetails),
                FinancePlanNextPaymentText = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.NextPayment),
                FinancePlanNextPaymentAmount = financePlanStatementFinancePlanDto.StatementedSnapshotFinancePlanPaymentDue.FormatCurrency(),
                //TODO: detect if autopay and blank fields if not on autopay
                //FinancePlanAutoPayLogo = "", //"autopay.png",
                //FinancePlanAutoPayText = "", //this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.YouAreEnrolledInAutopay),
                VisitDateHeaderText = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.VisitDate).ToUpper(),
                VisitDescriptionHeaderText = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.Description).ToUpper(),
                VisitBalanceHeaderText = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.AmountDue).ToUpper(),

                BalanceSummaryList = this.GetFinancePlanBalanceSummaryList(financePlanStatementFinancePlanDto),
                PaymentSummaryList = this.GetFinancePlanPaymentSummaryList(financePlanStatementFinancePlanDto),
                FinancePlanVisitDetailList = this.GetFinancePlanVisitDetailList(financePlanStatementFinancePlanDto, financePlanStatementDto),
            };

            return financePlanInfo;
        }

        List<FinancePlanVisitDetail> GetFinancePlanVisitDetailList(FinancePlanStatementFinancePlanDto financePlanStatementFinancePlanDto, FinancePlanStatementDto financePlanStatementDto)
        {
            List<FinancePlanVisitDetail> financePlanVisitDetailList = new List<FinancePlanVisitDetail>();

            IEnumerable<FinancePlanStatementVisitDto> financePlanStatementVisitDtoList = financePlanStatementDto.FinancePlanStatementVisits
                .Where(x => x.FinancePlanId == financePlanStatementFinancePlanDto.FinancePlanId);

            foreach (FinancePlanStatementVisitDto financePlanStatementVisitDto in financePlanStatementVisitDtoList)
            {
                financePlanVisitDetailList.Add(new FinancePlanVisitDetail()
                {
                    VisitDetailDate = financePlanStatementVisitDto.VisitDisplayDate,
                    VisitIdentifierText = $"{this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.Visit)}#:",
                    VisitIdentifier = financePlanStatementVisitDto.SourceSystemKey,
                    VisitDetailDescription = financePlanStatementVisitDto.VisitDescription,
                    VisitDetailPatient = financePlanStatementVisitDto.PatientName,
                    VisitDetailBalance = financePlanStatementVisitDto.StatementedSnapshotTotalBalance.FormatCurrency()
                });
            }

            return financePlanVisitDetailList;
        }

        List<PaymentSummary> GetFinancePlanPaymentSummaryList(FinancePlanStatementFinancePlanDto financePlanStatementFinancePlanDto)
        {
            List<PaymentSummary> paymentSummaryList = new List<PaymentSummary>()
            {
                new PaymentSummary()
                {
                    PaymentName = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.InterestDue),
                    PaymentAmount = financePlanStatementFinancePlanDto.StatementedSnapshotFinancePlanInterestDue.FormatCurrency()
                },
                new PaymentSummary()
                {
                    PaymentName = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.PrincipalDue),
                    PaymentAmount = financePlanStatementFinancePlanDto.StatementedSnapshotFinancePlanPrincipalDue.FormatCurrency()
                },
                new PaymentSummary()
                {
                    PaymentName = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.PastAmountDue),
                    PaymentAmount = financePlanStatementFinancePlanDto.StatementedSnapshotFinancePlanPastDueAmount.FormatCurrency()
                },
            };
            return paymentSummaryList;
        }

        List<BalanceSummary> GetFinancePlanBalanceSummaryList(FinancePlanStatementFinancePlanDto financePlanStatementFinancePlanDto)
        {
            List<BalanceSummary> balanceSummaryList = new List<BalanceSummary>()
            {
                new BalanceSummary()
                {
                    BalanceName = $"{this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.PreviousBalance)}:",
                    BalanceAmount = financePlanStatementFinancePlanDto.StatementedSnapshotPriorFinancePlanBalance.FormatCurrency()
                },
                new BalanceSummary()
                {
                    BalanceName = $"{this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.InterestCharged)}:",
                    BalanceAmount = financePlanStatementFinancePlanDto.StatementedSnapshotFinancePlanInterestCharged.FormatCurrency()
                },
                new BalanceSummary()
                {
                    BalanceName = $"{this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.InterestPaid)}:",
                    BalanceAmount = financePlanStatementFinancePlanDto.StatementedSnapshotFinancePlanInterestPaid.FormatCurrency()
                },
                new BalanceSummary()
                {
                    BalanceName = $"{this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.PrincipalPaid)}:",
                    BalanceAmount = financePlanStatementFinancePlanDto.StatementedSnapshotFinancePlanPrincipalPaid.FormatCurrency()
                },
                new BalanceSummary()
                {
                    BalanceName = $"{this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.Adjustments)}:",
                    BalanceAmount = financePlanStatementFinancePlanDto.StatementedSnapshotFinancePlanOtherBalanceCharges.FormatCurrency()
                },
            };

            return balanceSummaryList;
        }

        #endregion

        #region Construct VisitPayPaperBillingStatement

        //TODO: VP3 Statements are not capable of giving snapshot values for some of the amount fields. Need to add support to capture the values needed.
        //many values are mocked since paper statement template is not finalized and mailing address support does not exist yet (
        //constants NOT being used yet until template is finalized
        //localization support will need to be added which may require items in the localization dictionaries
        private VisitPayPaperBillingStatement GetVisitPayPaperBillingStatement(VpStatement statement)
        {
            int vpGuarantorId = statement.VpGuarantorId;
            int vpStatementId = statement.VpStatementId;

            GuarantorDto guarantorDto = this._guarantorApplicationService.Value.GetGuarantor(vpGuarantorId);
            int visitPayUserId = guarantorDto.User.VisitPayUserId;

            ClientDto clientDto = this._clientApplicationService.Value.GetClient();
            StatementDto statementDto = this._statementApplicationService.Value.GetStatement(vpStatementId, visitPayUserId);

            string accountNumber = guarantorDto.StatementAccountNumber;
            AccountStatusInfo accountStatusInfo = this.GetPaperStatementAccountStatus(statementDto);
            string paymentUrl = FormatHelper.DisplayUrl(clientDto.AppGuarantorUrlHost);
            string statementPeriod = $"{statement.PeriodStartDate.ToString(Format.DateFormat)} - {statement.PeriodEndDate.ToString(Format.DateFormat)}";
            string guarantorName = $"{guarantorDto.User.FirstName} {guarantorDto.User.LastName}";

            VisitPayPaperBillingStatement paperStatement = new VisitPayPaperBillingStatement()
            {
                ClientLogo = clientDto.ClientShortName, //Where to get value? Might need new field
                ReturnName = clientDto.ReturnMailingAddress?.Name1 ?? "",
                ReturnAddressStreet1 = clientDto.ReturnMailingAddress?.Address1 ?? "",
                ReturnAddressStreet2 = clientDto.ReturnMailingAddress?.Address2 ?? "",
                ReturnCity = clientDto.ReturnMailingAddress?.City ?? "",
                ReturnState = clientDto.ReturnMailingAddress?.State ?? "",
                ReturnZip = clientDto.ReturnMailingAddress?.Zip ?? "",
                StatementName = "Billing Statement",
                StatementPeriod = statementPeriod,
                AccountHolderName = guarantorName,
                AccountNumberName = "Guarantor ID",
                AccountNumber = accountNumber,
                PaymentUrl = paymentUrl,
                BalanceTotalName = "Total Balance",
                BalanceTotal = statementDto.StatementedSnapshotVisitBalanceSum.FormatCurrency(), //Is this the correct property to get this value?
                MailingName = guarantorName,
                MailingAddressStreet1 = $"{guarantorDto.User.MailingAddressStreet1}",
                MailingAddressStreet2 = $"{guarantorDto.User.MailingAddressStreet2}",
                MailingCity = $"{guarantorDto.User.MailingCity}",
                MailingState = $"{guarantorDto.User.MailingState}",
                MailingZip = $"{guarantorDto.User.MailingZip}",
                AccountStatusText = accountStatusInfo.AccountStatusText,
                AccountStatusFillColor = accountStatusInfo.AccountStatusColor,
                BalanceSummaryName = "Balance Summary",
                PayAtText = "Pay at",
                PayOnlineText = "Pay Online",
                PayOnlineLogo = "Computer", //Where to get value? Might need new field
                NotificationsName = "Notifications",
                CustomerServiceName = "Customer Service",
                PageText = "Page",
                OfText = "of",
                FooterText = "Powered by",
                FooterLogo = "VisitPay", //Where to get value? Might need new field

                VisitDateHeaderText = "Visit Date",
                VisitDescriptionHeaderText = "Description",
                VisitStatusHeaderText = "Status",
                VisitBalanceHeaderText = "Balance",
            };

            List<OnlinePaymentPromo> onlinePaymentPromoList = this.GetOnlinePaymentPromoList(statementDto);
            paperStatement.OnlinePaymentPromoList = onlinePaymentPromoList;

            List<Notification> notificationList = this.GetPaperStatementNotifications(statementDto);
            paperStatement.NotificationList = notificationList;

            List<BalanceSummary> balanceSummaryList = this.GetBalanceSummaries(statementDto);
            paperStatement.BalanceSummaryList = balanceSummaryList;

            List<CustomerServiceContactMethod> customerServiceContactMethodList = this.GetCustomerServiceContactMethodList(clientDto);
            paperStatement.CustomerServiceContactMethodList = customerServiceContactMethodList;

            List<Disclaimer> disclaimerList = this.GetDisclaimerList(clientDto, statementDto);
            paperStatement.DisclaimerList = disclaimerList;

            List<VisitDetail> visitDetailList = this.GetVisitDetailList(statementDto, visitPayUserId);
            paperStatement.VisitDetailList = visitDetailList;

            return paperStatement;
        }

        #endregion

        #region VisitPayPaperBillingStatement construction helpers (sub-reports)

        List<VisitDetail> GetVisitDetailList(StatementDto statementDto, int visitPayUserId)
        {
            List<VisitDetail> visitDetailList = new List<VisitDetail>();
            foreach (StatementVisitDto statementVisitDto in statementDto.ActiveVisitsNotOnFinancePlan)
            {
                VisitDetailStatusInfo visitDetailStatusInfo = this.GetVisitDetailStatus(statementVisitDto);
                visitDetailList.Add(new VisitDetail()
                {
                    VisitDetailStatusColor = visitDetailStatusInfo.VisitDetailStatusColor,
                    VisitDetailStatusLogo = visitDetailStatusInfo.VisitDetailStatusLogo,
                    VisitDetailStatus = visitDetailStatusInfo.VisitDetailStatusText,
                    VisitDetailBalance = statementVisitDto.StatementedSnapshotVisitBalance.FormatCurrency(),
                    VisitDetailDate = this.GetVisitDate(statementVisitDto.Visit.AdmitDate, statementVisitDto.Visit.DischargeDate),
                    VisitDetailPatient = statementVisitDto.Visit.PatientName,
                    VisitDetailDescription = statementVisitDto.Visit.VisitDescription,
                    VisitDetailSubtotalList = this.GetVisitDetailSubtotalList(statementDto, statementVisitDto, visitPayUserId)
                });
            }
            return visitDetailList;
        }

        //TODO: Finish this method once product has provided a concrete list of paper statement disclaimers
        List<Disclaimer> GetDisclaimerList(ClientDto clientDto, StatementDto statementDto)
        {
            List<Disclaimer> disclaimerList = new List<Disclaimer>()
            {
                new Disclaimer(){DisclaimerText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean mi odio, porttitor id vehicula ac, pharetra et orci. Aliquam semper mattis erat, eu mollis ipsum facilisis pellentesque. Sed lorem elit, consequat a consectetur ac, cursus in nisl. Morbi vel risus a urna rutrum malesuada et at dui. Nullam rutrum gravida nulla, et egestas nibh fringilla ac. Aliquam eu tempor felis. Curabitur eget mollis risus. Fusce at eros sollicitudin nulla porttitor imperdiet tempor ut dui. Aliquam commodo rutrum posuere. Aenean maximus dapibus lacus. Nullam quis mauris sed ipsum sagittis tempor in vel odio. Quisque quis posuere lorem. Aliquam vitae mauris quis turpis condimentum pharetra sit amet elementum dolor. Integer gravida laoreet risus eget tincidunt."},
                new Disclaimer(){DisclaimerText = "Pellentesque sollicitudin blandit quam luctus facilisis. Phasellus sodales leo nec sem gravida dignissim. Etiam vulputate ex tortor, venenatis cursus nulla sodales nec. Proin scelerisque mauris eget ante congue laoreet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Phasellus consectetur facilisis augue vel varius. In ultricies metus sit amet lacinia egestas. Maecenas consectetur magna non semper finibus. Morbi nunc augue, dapibus nec bibendum sed, efficitur a orci. Fusce justo erat, volutpat ac fringilla in, consequat vitae tortor. Etiam lacinia quis nibh sed laoreet. Donec id volutpat est. Vivamus placerat nulla ut felis elementum, at molestie libero vulputate. Proin fermentum pellentesque odio, vitae congue tortor aliquam mollis. Pellentesque rutrum turpis nec luctus lacinia."}
            };
            return disclaimerList;
        }

        List<CustomerServiceContactMethod> GetCustomerServiceContactMethodList(ClientDto clientDto)
        {
            List<CustomerServiceContactMethod> customerServiceContactMethodList = new List<CustomerServiceContactMethod>()
            {
                new CustomerServiceContactMethod(){ContactText = clientDto.SupportPhone},
                new CustomerServiceContactMethod(){ContactText = clientDto.SupportFromEmail},
            };

            return customerServiceContactMethodList;
        }

        List<OnlinePaymentPromo> GetOnlinePaymentPromoList(StatementDto statementDto)
        {
            List<OnlinePaymentPromo> onlinePaymentPromoList = new List<OnlinePaymentPromo>()
            {
                new OnlinePaymentPromo(){PromoText = "Set-up a finance plan in minutes"},
                new OnlinePaymentPromo(){PromoText = "Choose the payment that works for you"},
                new OnlinePaymentPromo(){PromoText = "View visit details and billing history"},
                new OnlinePaymentPromo(){PromoText = "Easy access from your phone or PC"},
            };
            return onlinePaymentPromoList;
        }

        //TODO: VP3 Statements are not capable of giving snapshot values for some of the amount fields. Need to add support to capture the values needed.
        List<BalanceSummary> GetBalanceSummaries(StatementDto statementDto)
        {
            List<BalanceSummary> balanceSummaryList = new List<BalanceSummary>
            {
                new BalanceSummary() { BalanceName = "Previous Balance", BalanceAmount = statementDto.StatementedSnapshotVisitBalance.FormatCurrency() }, //Is this the correct property to get this value?
                new BalanceSummary() { BalanceName = "New Balance", BalanceAmount = statementDto.StatementedSnapshotNewVisitsBalance.FormatCurrency() }, //Is this the correct property to get this value?
                new BalanceSummary() { BalanceName = "Payments Made", BalanceAmount = "$0.00" } //Need snapshot property to get this value
            };
            return balanceSummaryList;
        }

        //TODO: Finish this method once product has provided a concrete list of paper statement notifications
        List<Notification> GetPaperStatementNotifications(StatementDto statementDto)
        {
            List<Notification> notifications = new List<Notification>();
            decimal creditBalance = statementDto.StatementedSnapshotVisitCreditBalanceSum ?? 0m;
            if (creditBalance != 0m)
            {
                notifications.Add(new Notification() { NotificationText = $"You have a credit balance of {creditBalance.FormatCurrency()}. Contact customer support to resolve" });
            }
            return notifications;
        }

        //TODO: Finish this method once product has provided a concrete list of paper statement account statuses
        AccountStatusInfo GetPaperStatementAccountStatus(StatementDto statement)
        {
            bool isPastDue = statement.VisitsNotOnFinancePlan.Any(x => x.StatementedSnapshotAgingCount >= (int)AgingTierEnum.PastDue);
            bool isFinalPastDue = statement.VisitsNotOnFinancePlan.Any(x => x.StatementedSnapshotAgingCount >= (int)AgingTierEnum.FinalPastDue);
            bool isMaxAge = statement.VisitsNotOnFinancePlan.Any(x => x.StatementedSnapshotAgingCount >= (int)AgingTierEnum.MaxAge);

            AccountStatusInfo accountStatusInfo = new AccountStatusInfo()
            {
                AccountStatusText = "Your account is in good standing.",
                AccountStatusColor = "green"
            };
            if (isPastDue)
            {
                accountStatusInfo.AccountStatusColor = "red";
                if (isMaxAge)
                {
                    accountStatusInfo.AccountStatusText = "Your account is past due."; //TODO remove if product wants all messages to be the same
                }
                else if (isFinalPastDue)
                {
                    accountStatusInfo.AccountStatusText = "Your account is past due."; //TODO remove if product wants all messages to be the same
                }
                else
                {
                    accountStatusInfo.AccountStatusText = "Your account is past due."; //TODO remove if product wants all messages to be the same
                }
            }

            return accountStatusInfo;
        }

        VisitDetailStatusInfo GetVisitDetailStatus(StatementVisitDto statementVisitDto)
        {

            VisitDetailStatusInfo visitDetailStatusInfo = new VisitDetailStatusInfo()
            {
                VisitDetailStatusColor = "black",
                VisitDetailStatusLogo = "",
                VisitDetailStatusText = "Good Standing"
            };

            bool isPastDue = statementVisitDto.StatementedSnapshotAgingCount >= (int)AgingTierEnum.PastDue;
            bool isFinalPastDue = statementVisitDto.StatementedSnapshotAgingCount >= (int)AgingTierEnum.FinalPastDue;
            bool isMaxAge = statementVisitDto.StatementedSnapshotAgingCount >= (int)AgingTierEnum.MaxAge;

            if (isPastDue)
            {
                visitDetailStatusInfo.VisitDetailStatusLogo = "flag";
                visitDetailStatusInfo.VisitDetailStatusColor = "red";
            }
            if (isMaxAge)
            {
                visitDetailStatusInfo.VisitDetailStatusText = "Final Past Due";
            }
            else if (isFinalPastDue)
            {
                visitDetailStatusInfo.VisitDetailStatusText = "Final Past Due";
            }
            else if (isPastDue)
            {
                visitDetailStatusInfo.VisitDetailStatusText = "Past Due";
            }

            return visitDetailStatusInfo;
        }

        private List<VisitDetailSubtotal> GetVisitDetailSubtotalList(StatementDto statementDto, StatementVisitDto statementVisitDto, int visitPayUserId)
        {
            VisitTransactionSummaryDto visitTransactionSummaryDto = this._visitTransactionApplicationService.Value.GetTransactionSummary(
                visitPayUserId: visitPayUserId,
                visitId: statementVisitDto.VisitId,
                startDate: DateTime.MinValue, // we want all visit transactions written until the statement period ended
                endDate: statementDto.PeriodEndDate.ToEndOfDay()
            );
            decimal otherAmount = visitTransactionSummaryDto.Interest + visitTransactionSummaryDto.Other;
            List<VisitDetailSubtotal> visitDetailSubtotalList = new List<VisitDetailSubtotal>()
            {
                new VisitDetailSubtotal(){ SubtotalName = "Charges", SubtotalAmount = visitTransactionSummaryDto.Charges.FormatCurrency()},
                new VisitDetailSubtotal(){ SubtotalName = "Insurance", SubtotalAmount = visitTransactionSummaryDto.Insurance.FormatCurrency()},
                new VisitDetailSubtotal(){ SubtotalName = "Your Payments", SubtotalAmount = visitTransactionSummaryDto.AllPayments.FormatCurrency()},
                new VisitDetailSubtotal(){ SubtotalName = "Other", SubtotalAmount = otherAmount.FormatCurrency()},
            };
            return visitDetailSubtotalList;
        }

        #endregion

        #region VisitPayPaperBillingStatement formatting helpers

        // If both date are blank or NULL return N/A
        // If one of the dates is blank or NULL, show as a single date using the date that is populated(this is current implementation; discharge date has priority)
        // If the Admit and Discharge dates have the same value, show as a single date
        // If the Admit and Discharge dates have different values, show both dates as "[Admit Date] - [Discharge Date]", regardless of how they are provided to VP.

        private string GetVisitDate(DateTime? admitDate, DateTime? dischargeDate)
        {
            if (admitDate == null && dischargeDate == null)
                return "N/A";

            if (admitDate == null || dischargeDate == null)
                return admitDate?.ToString(Format.DateFormat) ?? dischargeDate.Value.ToString(Format.DateFormat);

            if (DateTime.Compare(admitDate.Value, dischargeDate.Value) == 0)
                return admitDate.Value.ToString(Format.DateFormat);

            return $"{admitDate.Value.ToString(Format.DateFormat)} - {dischargeDate.Value.ToString(Format.DateFormat)}";
        }

        #endregion

    }
}
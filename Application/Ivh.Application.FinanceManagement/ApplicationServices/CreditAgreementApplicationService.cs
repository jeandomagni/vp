﻿namespace Ivh.Application.FinanceManagement.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Content.Common.Dtos;
    using Content.Common.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.Visit.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.VisitPay.Enums;
    using User.Common.Dtos;

    public class CreditAgreementApplicationService : ApplicationService, ICreditAgreementApplicationService
    {
        private const string ReplacementTagOfferList = "[[FinancePlanOfferList]]";
        private const string ReplacementTagTilaBox = "[[TilaBox]]";
        private const string ReplacementTagContractDate = "[[ContractDate]]";
        private const string ReplacementTagContractNumber = "[[ContractNumber]]";
        private const int CreditAgreementCacheExpiresSeconds = 7200;
        
        private readonly Lazy<IContentApplicationService> _contentApplicationService;
        private readonly Lazy<IFinancePlanService> _financePlanService;
        private readonly Lazy<IFinancePlanOfferService> _financePlanOfferService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IVisitService> _visitService;

		public CreditAgreementApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IFinancePlanService> financePlanService,
            Lazy<IFinancePlanOfferService> financePlanOfferService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IVisitService> visitService
		) : base(applicationServiceCommonService)
        {
            this._contentApplicationService = contentApplicationService;
            this._financePlanService = financePlanService;
            this._financePlanOfferService = financePlanOfferService;
            this._guarantorService = guarantorService;
            this._visitService = visitService;
        }
        
        public async Task CacheCreditAgreementTermsAsync(FinancePlanTermsDto financePlanTermsDto)
        {
            if (financePlanTermsDto == null)
            {
                throw new Exception("finance plan terms are null");
            }

            financePlanTermsDto.ContractNumber = Guid.NewGuid();
            
            string cacheKey = this.GetCacheKey(financePlanTermsDto.FinancePlanOfferId);
            await this.Cache.Value.SetObjectAsync(cacheKey, financePlanTermsDto, CreditAgreementCacheExpiresSeconds);
        }

        public async Task<FinancePlanCreditAgreementCollectionDto> GetCachedCreditAgreementAsync(int financePlanOfferId, int? termsCmsVersionId, bool includeMaterialTerms)
        {
            FinancePlanOffer financePlanOffer = this._financePlanOfferService.Value.GetOfferById(financePlanOfferId);
            if (financePlanOffer == null)
            {
                throw new Exception("finance plan offer not found");
            }
			
            // load terms from cache
            FinancePlanTermsDto cachedTermsDto = await this.GetCachedTermsForCreditAgreementAsync(financePlanOfferId, financePlanOffer.VpGuarantor.VpGuarantorId);
            if (cachedTermsDto == null)
            {
                throw new Exception("cached terms are null");
            }

            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(cachedTermsDto.VpGuarantorId);
            FinancePlanCreditAgreementCollectionDto creditAgreements = new FinancePlanCreditAgreementCollectionDto();
            
            // generate in current locale
            FinancePlanCreditAgreementDto userCreditAgreement = await this.GetCreditAgreementAsync(cachedTermsDto, termsCmsVersionId);
            creditAgreements.UserLocale = userCreditAgreement;
            
            string userLocale = (guarantor.User.Locale ?? this.Client.Value.LanguageAppDefaultName).Trim();
            string clientLocale = (this.Client.Value.LanguageAppDefaultName ?? string.Empty).Trim();

            CmsRegionEnum termsCmsRegionEnum = cachedTermsDto.TermsCmsRegionEnum;

			if (!string.Equals(userLocale, clientLocale, StringComparison.OrdinalIgnoreCase))
            {
                using (new CultureKeeper(new CultureInfo(clientLocale)))
                {
                    IDictionary<int, int> versionNumbers = await this._contentApplicationService.Value.GetCmsVersionNumbersAsync(termsCmsRegionEnum);
                    List<int> versionIdsForNumber = versionNumbers.Where(x => x.Value == userCreditAgreement.TermsCmsVersionNum).Select(x => x.Key).ToList();
                    if (versionIdsForNumber.Any())
                    {
                        int clientLocaleTermsCmsVersionId = versionIdsForNumber.FirstOrDefault();
                        FinancePlanCreditAgreementDto clientLocaleCreditAgreement = await this.GetCreditAgreementAsync(cachedTermsDto, clientLocaleTermsCmsVersionId);
                        creditAgreements.ClientLocale = clientLocaleCreditAgreement;
                    }
                }
            }

            // generate material terms - client only
            if (includeMaterialTerms && guarantor.IsOfflineGuarantor() && this.ApplicationSettingsService.Value.IsClientApplication.Value)
            {
                FinancePlan financePlan = this._financePlanService.Value.GetFinancePlanByOfferId(financePlanOfferId);
                // only show material terms for pending terms
                if (financePlan == null)
                {
                    FinancePlanCreditAgreementDto materialTerms = await this.GetMaterialTermsAsync(cachedTermsDto);
                    creditAgreements.MaterialTerms = materialTerms;
                }
            }

            return creditAgreements;
        }

        public int? GetFinancePlanOfferIdFromContractNumber(string contractNumber, int? vpGuarantorId)
        {
	        if (!vpGuarantorId.HasValue)
	        {
		        return null;
	        }
	        FinancePlan financePlan = this._financePlanService.Value.GetFinancePlanByContractNumber(contractNumber);
	        return financePlan?.VpGuarantor.VpGuarantorId == vpGuarantorId ? financePlan.FinancePlanOffer.FinancePlanOfferId : (int?)null;
        }

		public async Task<ContentDto> GetCreditAgreementContentAsync(int financePlanOfferId, bool includeMaterialTerms = false)
        {
            FinancePlan financePlan = this._financePlanService.Value.GetFinancePlanByOfferId(financePlanOfferId);
            if (financePlan?.TermsCmsVersionId == null)
            {
                // it's an offer for a plan that hasn't been finalized or doesn't exist yet
                // this will respect whatever the current locale is, as it's built on the fly using cached terms
                ContentDto realtimeTermsCmsVersionDto;
                FinancePlanCreditAgreementCollectionDto cachedCreditAgreement = await this.GetCachedCreditAgreementAsync(financePlanOfferId, null, includeMaterialTerms);
                if (cachedCreditAgreement.MaterialTerms != null)
                {
                    realtimeTermsCmsVersionDto = new ContentDto
                    {
                        ContentBody = cachedCreditAgreement.MaterialTerms.AgreementText,
                        ContentTitle = this.GetAgreementTitle(cachedCreditAgreement.MaterialTerms.IsRetailInstallmentContract)
                    };
                }
                else
                {
                    realtimeTermsCmsVersionDto = new ContentDto
                    {
                        ContentBody = cachedCreditAgreement.UserLocale.AgreementText,
                        ContentTitle = this.GetAgreementTitle(cachedCreditAgreement.UserLocale.IsRetailInstallmentContract)
                    };
                }

                return realtimeTermsCmsVersionDto;
            }

            CmsRegionEnum cmsRegionEnum = CmsRegionEnum.VppCreditAgreement;
			if (financePlan?.VpGuarantor != null)
            {
                cmsRegionEnum = this._visitService.Value.GetRicCmsRegionEnum(financePlan.VpGuarantor.VpGuarantorId);
                if (cmsRegionEnum == CmsRegionEnum.Unknown) // fallback if something went wrong here...
                {
                    cmsRegionEnum = CmsRegionEnum.VppCreditAgreement;
                }
			}
            
            // plan exists and has accepted terms
            // plan doesn't have a saved credit agreement
            // this would be a vp2 migrated agreement
            // this will respect whatever the current locale is, as it's built on the fly
            if (financePlan.FinancePlanCreditAgreements.IsNullOrEmpty())
            {
                CmsVersionDto cmsVersionDto = await this.GetCreditAgreementCmsVersion(financePlan.TermsCmsVersionId.Value, Mapper.Map<VisitPayUserDto>(financePlan.VpGuarantor.User), cmsRegionEnum);
                cmsVersionDto.ContentTitle = this.GetAgreementTitle(true);
                if (cmsVersionDto.ContentBody.Contains(ReplacementTagOfferList))
                {
                    await this.ReplaceOfferTag(cmsVersionDto, financePlan.FinancePlanOffer.FinancePlanOfferId);
                }

                this.ReplaceUnreplacedTags(cmsVersionDto);

                return new ContentDto
                {
                    ContentTitle = cmsVersionDto.ContentTitle,
                    ContentBody = cmsVersionDto.ContentBody
                };
            }

            // plan has a saved credit agreement
            // look for a copy saved in current locale
            FinancePlanCreditAgreement creditAgreement = financePlan.FinancePlanCreditAgreements
                .Where(x => x.Locale == Thread.CurrentThread.GetLocale())
                .OrderByDescending(x => x.InsertDate)
                .ThenByDescending(x => x.FinancePlanCreditAgreementId)
                .FirstOrDefault();

            if (creditAgreement == null)
            {
                // no saved copy for current locale - revert to the locale the user agreed in
                creditAgreement = financePlan.FinancePlanCreditAgreements
                    .Where(x => x.IsUserLocale)
                    .OrderByDescending(x => x.InsertDate)
                    .ThenByDescending(x => x.FinancePlanCreditAgreementId)
                    .First();
            }

            ContentDto savedCreditAgreementCmsVersionDto = new ContentDto
            {
                ContentBody = creditAgreement.AgreementText,
                ContentTitle = this.GetAgreementTitle(creditAgreement.IsRetailInstallmentContract)
            };

            return savedCreditAgreementCmsVersionDto;
        }

        public async Task<FinancePlanTermsDto> GetCachedTermsForCreditAgreementAsync(int financePlanOfferId, int vpGuarantorId)
        {
            FinancePlanTermsDto financePlanTermsDto = await this.Cache.Value.GetObjectAsync<FinancePlanTermsDto>(this.GetCacheKey(financePlanOfferId));

            if (financePlanTermsDto == null)
            {
                this.LoggingService.Value.Fatal(() => $"{nameof(CreditAgreementApplicationService)}:{nameof(this.GetCachedTermsForCreditAgreementAsync)} - terms are null for offerid = {financePlanOfferId}");
                return null;
            }

            if (financePlanTermsDto.VpGuarantorId != vpGuarantorId)
            {
                this.LoggingService.Value.Fatal(() => $"{nameof(CreditAgreementApplicationService)}:{nameof(this.GetCachedTermsForCreditAgreementAsync)} - vpguarantorid mismatch for offerid = {financePlanOfferId} ({vpGuarantorId}, {financePlanTermsDto.VpGuarantorId}");
                return null;
            }

            if (financePlanTermsDto.TermsCmsRegionEnum == CmsRegionEnum.Unknown)
            {
                financePlanTermsDto.TermsCmsRegionEnum = CmsRegionEnum.VppCreditAgreement;
            }

            return financePlanTermsDto;
        }

        public string GetAgreementTitle(bool isRetailInstallmentContract)
        {
            string title = isRetailInstallmentContract ? this.Client.Value.CreditAgreementName : this.Client.Value.SimpleTermsAgreementName;
            string localizedTitle = this._contentApplicationService.Value.GetTextRegion(title);
            return localizedTitle;
        }

        private async Task<FinancePlanCreditAgreementDto> GetCreditAgreementAsync(FinancePlanTermsDto financePlanTermsDto, int? termsCmsVersionId)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(financePlanTermsDto.VpGuarantorId);
            VisitPayUserDto visitPayUserDto = Mapper.Map<VisitPayUserDto>(guarantor.User);

            CmsVersionDto cmsVersionDto = await this.GetCreditAgreementCmsVersion(termsCmsVersionId, visitPayUserDto, financePlanTermsDto.TermsCmsRegionEnum);
            
            // vp2 version
            if (cmsVersionDto.ContentBody.Contains(ReplacementTagOfferList))
            {
                await this.ReplaceOfferTag(cmsVersionDto, financePlanTermsDto.FinancePlanOfferId);
            }

            // vp3 version
            if (cmsVersionDto.ContentBody.Contains(ReplacementTagTilaBox))
            {
                await this.ReplaceTilaTag(cmsVersionDto, financePlanTermsDto);
            }

            // vp3 version
            if (cmsVersionDto.ContentBody.Contains(ReplacementTagContractDate))
            {
                cmsVersionDto.ContentBody = ReplacementUtility.Replace(cmsVersionDto.ContentBody, new Dictionary<string, string>
                {
                    {ReplacementTagContractDate, DateTime.UtcNow.Date.ToString("MM/dd/yyyy")}
                }, null, null);
            }

            // vp3 version
            if (cmsVersionDto.ContentBody.Contains(ReplacementTagContractNumber))
            {
                cmsVersionDto.ContentBody = ReplacementUtility.Replace(cmsVersionDto.ContentBody, new Dictionary<string, string>
                {
                    {ReplacementTagContractNumber, financePlanTermsDto.ContractNumber.ToString().ToUpper()}
                }, null, null);
            }
            
            this.ReplaceSimpleTermsTags(cmsVersionDto, financePlanTermsDto);
            this.ReplaceUnreplacedTags(cmsVersionDto);

            return new FinancePlanCreditAgreementDto
            {
                AgreementText = cmsVersionDto.ContentBody,
                ContractNumber = financePlanTermsDto.ContractNumber,
                IsRetailInstallmentContract = financePlanTermsDto.IsRetailInstallmentContract.GetValueOrDefault(true), // default this to true
                NumberMonthlyPayments = financePlanTermsDto.NumberMonthlyPayments,
                TermsCmsRegion = (CmsRegionEnum)cmsVersionDto.CmsRegionId,
                TermsCmsVersionId = cmsVersionDto.CmsVersionId,
                TermsCmsVersionNum = cmsVersionDto.VersionNum,
                Locale = Thread.CurrentThread.GetLocale(),
            };
        }

        private async Task<FinancePlanCreditAgreementDto> GetMaterialTermsAsync(FinancePlanTermsDto financePlanTermsDto)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(financePlanTermsDto.VpGuarantorId);
            VisitPayUserDto visitPayUserDto = Mapper.Map<VisitPayUserDto>(guarantor.User);

            CmsRegionEnum cmsRegion = financePlanTermsDto.IsRetailInstallmentContract.GetValueOrDefault(true) ?  CmsRegionEnum.RetailInstallmentContractMaterialTerms : CmsRegionEnum.FinancePlanSimpleTermsMaterialTerms;
            CmsVersionDto cmsVersionDto = await this.GetCreditAgreementCmsVersion(null, visitPayUserDto, cmsRegion);

            this.ReplaceInnerTilaTags(cmsVersionDto, financePlanTermsDto);
            this.ReplaceSimpleTermsTags(cmsVersionDto, financePlanTermsDto);
            this.ReplaceUnreplacedTags(cmsVersionDto);

            return new FinancePlanCreditAgreementDto
            {
                AgreementText = cmsVersionDto.ContentBody,
                ContractNumber = financePlanTermsDto.ContractNumber,
                IsRetailInstallmentContract = financePlanTermsDto.IsRetailInstallmentContract.GetValueOrDefault(true), // default this to true
                NumberMonthlyPayments = financePlanTermsDto.NumberMonthlyPayments,
                TermsCmsRegion = (CmsRegionEnum)cmsVersionDto.CmsRegionId,
                TermsCmsVersionId = cmsVersionDto.CmsVersionId,
                TermsCmsVersionNum = cmsVersionDto.VersionNum,
                Locale = Thread.CurrentThread.GetLocale(),
            };
        }

        private async Task<CmsVersionDto> GetCreditAgreementCmsVersion(int? cmsVersionId, VisitPayUserDto visitPayUserDto, CmsRegionEnum termsCmsRegionId)
        {
            IDictionary<string, string> replacementValues = new Dictionary<string, string>
            {
                {"[[UserFirstName]]", visitPayUserDto.FirstName},
                {"[[UserLastName]]", visitPayUserDto.LastName},
                {"[[UserAddress]]", visitPayUserDto.AddressStreet1},
                {"[[UserCity]]", visitPayUserDto.City},
                {"[[UserState]]", visitPayUserDto.State},
                {"[[UserZip]]", visitPayUserDto.Zip},
                {"[[UserPhonePrimary]]", visitPayUserDto.PhoneNumber},
                {"[[UserEmail]]", visitPayUserDto.Email}
            };

            CmsVersionDto cmsVersionDto;
            if (cmsVersionId.HasValue)
            {
                cmsVersionDto = await this._contentApplicationService.Value.GetSpecificVersionAsync(termsCmsRegionId, cmsVersionId.Value, true, replacementValues);
            }
            else
            {
                cmsVersionDto = await this._contentApplicationService.Value.GetCurrentVersionAsync(termsCmsRegionId, true, replacementValues);
            }

            return cmsVersionDto;
        }
        
        private async Task ReplaceTilaTag(CmsVersionDto cmsVersionDto, FinancePlanTermsDto financePlanTermsDto)
        {
            CmsVersionDto cmsVersionTilaBox = await this._contentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.VppCreditAgreementTilaBox);
            if (cmsVersionTilaBox != null)
            {
                // replace [[TilaBox]] with content of cms version
                cmsVersionDto.ContentBody = ReplacementUtility.Replace(cmsVersionDto.ContentBody, new Dictionary<string, string>
                {
                    {ReplacementTagTilaBox, cmsVersionTilaBox.ContentBody}
                }, null, false);

                this.ReplaceInnerTilaTags(cmsVersionDto, financePlanTermsDto);
            }
        }

        private void ReplaceInnerTilaTags(CmsVersionDto cmsVersionDto, FinancePlanTermsDto financePlanTermsDto)
        {
            if (financePlanTermsDto == null)
            {
                return;
            }
            
            decimal rolloverAmount = financePlanTermsDto.RolloverBalance;
            decimal financeCharge = financePlanTermsDto.TotalInterest;
            int numberOfPayments1 = financePlanTermsDto.NumberMonthlyPayments > 1 ? financePlanTermsDto.NumberMonthlyPayments - 1 : financePlanTermsDto.NumberMonthlyPayments;
            int numberOfPayments2 = financePlanTermsDto.NumberMonthlyPayments > 1 ? 1 : 0;
            DateTime paymentDate1 = financePlanTermsDto.NextPaymentDueDate.Date;
            DateTime paymentDate2 = financePlanTermsDto.FinalPaymentDate.Date == financePlanTermsDto.NextPaymentDueDate.Date ? financePlanTermsDto.FinalPaymentDate.Date : financePlanTermsDto.FinalPaymentDate.AddMonths(-1).Date;
            DateTime paymentDate3 = financePlanTermsDto.FinalPaymentDate.Date;
            decimal paymentAmount1 = financePlanTermsDto.MonthlyPaymentAmount;
            decimal paymentAmount2 = financePlanTermsDto.MonthlyPaymentAmountLast;
            decimal amountFinanced = financePlanTermsDto.FinancePlanBalance.GetValueOrDefault(0);
            decimal totalOfPayments = financeCharge + amountFinanced;
            decimal downPaymentAmount = financePlanTermsDto.DownpaymentAmount;

            cmsVersionDto.ContentBody = ReplacementUtility.Replace(cmsVersionDto.ContentBody, new Dictionary<string, string>
            {
                {"[[TilaApr]]", financePlanTermsDto.InterestRate.FormatPercentage()},
                {"[[TilaFinanceCharge]]", financeCharge.FormatCurrency()},
                {"[[TilaAmountFinanced]]", amountFinanced.FormatCurrency()},
                {"[[TilaTotalOfPayments]]", totalOfPayments.FormatCurrency()},
                {"[[TilaTotalSalesPrice]]", totalOfPayments.FormatCurrency()},
                {"[[TilaNumberOfPayments1]]", numberOfPayments1.ToString()},
                {"[[TilaNumberOfPayments2]]", numberOfPayments2.ToString()},
                {"[[TilaPaymentDate1]]", paymentDate1.ToString("MM/dd/yyyy")},
                {"[[TilaPaymentDate2]]", paymentDate2.ToString("MM/dd/yyyy")},
                {"[[TilaPaymentDate3]]", paymentDate3.ToString("MM/dd/yyyy")},
                {"[[TilaPaymentAmount1]]", paymentAmount1.FormatCurrency()},
                {"[[TilaPaymentAmount2]]", paymentAmount2.FormatCurrency()},
                {"[[TilaRolloverAmount]]", rolloverAmount.FormatCurrency()},
                {"[[TilaDownpaymentAmount]]", downPaymentAmount.FormatCurrency()}
            }, null, false);
        }
        
        private void ReplaceSimpleTermsTags(CmsVersionDto cmsVersionDto, FinancePlanTermsDto financePlanTermsDto)
        {
            if (financePlanTermsDto == null)
            {
                return;
            }

            const string numberMonthlyPaymentsTag = "[[SimpleTerms_NumberMonthlyPayments]]";
            if (cmsVersionDto.ContentBody.Contains(numberMonthlyPaymentsTag))
            {
                cmsVersionDto.ContentBody = ReplacementUtility.Replace(cmsVersionDto.ContentBody, new Dictionary<string, string>
                {
                    {numberMonthlyPaymentsTag, financePlanTermsDto.NumberMonthlyPayments.ToString()}
                }, null, false);
            }
        }

        private async Task ReplaceOfferTag(CmsVersionDto cmsVersionDto, int financePlanOfferId)
        {
            FinancePlanOffer financePlanOffer = this._financePlanOfferService.Value.GetOfferById(financePlanOfferId);
            if (financePlanOffer == null)
            {
                return;
            }

            // get other offers for correlation
            IList<FinancePlanOffer> offerSet = this._financePlanOfferService.Value.GetOffersWithCorrelationGuid(financePlanOffer.VpGuarantor.VpGuarantorId, financePlanOffer.CorrelationGuid);
            if (offerSet.IsNullOrEmpty())
            {
                return;
            }

            // get cms regions for line1 and lineN
            offerSet = offerSet.OrderBy(x => x.DurationRangeStart).ToList();
            IList<CmsVersionDto> offerLines = new List<CmsVersionDto>
            {
                await this._contentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.VppCreditAgreementTermsFirstLine, true, this.FinancePlanOfferReplacementValues(offerSet.First()))
            };

            foreach (FinancePlanOffer offer in offerSet.Skip(1))
            {
                IDictionary<string, string> replacementValues = this.FinancePlanOfferReplacementValues(offer);
                CmsVersionDto offerCms = await this._contentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.VppCreditAgreementTermsSubsequentLines, true, replacementValues);
                offerLines.Add(offerCms);
            }

            // join lines and build list
            cmsVersionDto.ContentBody = ReplacementUtility.Replace(cmsVersionDto.ContentBody, new Dictionary<string, string>
            {
                {ReplacementTagOfferList, $"<ul><li>{string.Join("</li><li>", offerLines.Select(x => x.ContentBody))}</li></ul>"}
            }, null, false);
        }

        private void ReplaceUnreplacedTags(CmsVersionDto cmsVersionDto)
        {
            if (cmsVersionDto.ContentBody.Contains(ReplacementTagTilaBox))
            {
                cmsVersionDto.ContentBody = cmsVersionDto.ContentBody.Replace(ReplacementTagTilaBox, string.Empty);
                this.LoggingService.Value.Warn(() =>  $"{nameof(CreditAgreementApplicationService)} - credit agreement with unreplaced Tila tag");
            }

            if (cmsVersionDto.ContentBody.Contains(ReplacementTagOfferList))
            {
                cmsVersionDto.ContentBody = cmsVersionDto.ContentBody.Replace(ReplacementTagOfferList, string.Empty);
                this.LoggingService.Value.Warn(() =>  $"{nameof(CreditAgreementApplicationService)} - credit agreement with unreplaced Offer tag");
            }

            if (cmsVersionDto.ContentBody.Contains(ReplacementTagContractDate))
            {
                cmsVersionDto.ContentBody = cmsVersionDto.ContentBody.Replace(ReplacementTagContractDate, string.Empty);
                this.LoggingService.Value.Warn(() =>  $"{nameof(CreditAgreementApplicationService)} - credit agreement with unreplaced Contract Date tag");
            }

            if (cmsVersionDto.ContentBody.Contains(ReplacementTagContractNumber))
            {
                cmsVersionDto.ContentBody = cmsVersionDto.ContentBody.Replace(ReplacementTagContractNumber, string.Empty);
                this.LoggingService.Value.Warn(() =>  $"{nameof(CreditAgreementApplicationService)} - credit agreement with unreplaced Contract Number tag");
            }
        }
       
        private IDictionary<string, string> FinancePlanOfferReplacementValues(FinancePlanOffer financePlanOffer)
        {
            Dictionary<string, string> replacements = new Dictionary<string, string>
            {
                {"[[FinancePlanOffer_DurationRange]]", $"{financePlanOffer.DurationRangeStart}-{financePlanOffer.DurationRangeEnd}"},
                {"[[FinancePlanOffer_InterestRate]]", financePlanOffer.InterestRate.FormatPercentage()},
                {"[[FinancePlanOffer_InterestRateMonthly]]", (financePlanOffer.InterestRate/12m).ToString("P3")},
            };

            return replacements;
        }

        private string GetCacheKey(int financePlanOfferId)
        {
            return $"CreditAgreement::{financePlanOfferId}";
        }
    }
}
namespace Ivh.Application.FinanceManagement.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Core.Common.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.User.Entities;
    using Domain.User.Services;
    using Domain.FinanceManagement.Interfaces;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Payment.Interfaces;
    using Domain.FinanceManagement.Statement.Entities;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Ivh.Common.Base.Constants;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.EventJournal;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using NHibernate;

    public class PaymentReversalApplicationService : ApplicationService, IPaymentReversalApplicationService
    {
        private readonly Lazy<IFinancePlanService> _financePlanService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IPaymentProcessorResponseService> _paymentProcessorResponseService;
        private readonly Lazy<IPaymentReversalService> _paymentReversalService;
        private readonly Lazy<IPaymentService> _paymentService;
        private readonly ISession _session;
        private readonly Lazy<IVpStatementService> _statementService;
        private readonly VisitPayUserService _visitPayUserService;
        private readonly Lazy<IVisitPayUserJournalEventApplicationService> _journalEventApplicationService;
        public PaymentReversalApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IFinancePlanService> financePlanService,
            Lazy<IPaymentReversalService> paymentReversalService,
            Lazy<IPaymentProcessorResponseService> paymentProcessorResponseService,
            Lazy<IPaymentService> paymentService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IVpStatementService> statementService,
            VisitPayUserService visitPayUserService,
            Lazy<IVisitPayUserJournalEventApplicationService> journalEventApplicationService,
            ISessionContext<VisitPay> sessionContext) : base(applicationServiceCommonService)
        {
            this._financePlanService = financePlanService;
            this._paymentProcessorResponseService = paymentProcessorResponseService;
            this._paymentReversalService = paymentReversalService;
            this._paymentService = paymentService;
            this._visitPayUserService = visitPayUserService;
            this._guarantorService = guarantorService;
            this._statementService = statementService;
            this._session = sessionContext.Session;
            this._journalEventApplicationService = journalEventApplicationService;
        }

        [Obsolete(ObsoleteDescription.VisitTransactions + " " + ObsoleteDescription.VisitTransactionVisitPayDiscount)]
        public async Task<VoidPaymentResponseDto> VoidPaymentAsync(int visitPayGuarantorId, int paymentProcessorResponseId, int visitPayUserId)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                PaymentProcessorResponse paymentProcessorResponse = this._paymentProcessorResponseService.Value.GetPaymentProcessorResponse(paymentProcessorResponseId);
                if (paymentProcessorResponse != null
                    && paymentProcessorResponse.PaymentPaymentProcessorResponses.IsNotNullOrEmpty())
                {
                    IList<Payment> payments = paymentProcessorResponse.PaymentPaymentProcessorResponses.Select(x => x.Payment).ToList();

                    if (payments.Any(x => x.Guarantor.VpGuarantorId != visitPayGuarantorId))
                    {
                        return new VoidPaymentResponseDto
                        {
                            IsError = true,
                            ErrorMessage = "Payments associated with this PaymentProcessorResponse do not belong to Guarantor."
                        };
                    }
                    //check for voidability
                    if (this._paymentReversalService.Value.GetPaymentReversalStatus(payments) == PaymentReversalStatusEnum.Voidable)
                    {
                        VisitPayUser visitPayUser = await this._visitPayUserService.FindByIdAsync(visitPayUserId.ToString());
                        IList<Payment> reversalPayments = await this._paymentReversalService.Value.VoidPaymentAsync(payments, visitPayUser);
                        if (reversalPayments.IsNotNullOrEmpty()
                            && (reversalPayments.All(x => x.PaymentStatus == PaymentStatusEnum.ActivePendingACHApproval)
                                || reversalPayments.All(x => x.PaymentStatus == PaymentStatusEnum.ClosedPaid)))
                        {
                            this.UpdateReversedPaymentStatuses(reversalPayments);

                            this.ProcessReagingForFinancePlans(reversalPayments);

                            unitOfWork.Commit();

                            return new VoidPaymentResponseDto
                            {
                                IsError = false,
                                TransactionId = string.Join(", ", reversalPayments.SelectMany(x => x.PaymentProcessorResponses.Select(z => z.PaymentProcessorSourceKey)))
                            };
                        }
                    }
                }
                unitOfWork.Rollback();
                return new VoidPaymentResponseDto
                {
                    IsError = true,
                    ErrorMessage = "Payment Cannot be voided"
                };
            }
        }

        public async Task<RefundPaymentResponseDto> RefundPaymentAsync(JournalEventHttpContextDto context, int vistiPayGuarantorId, int paymentProcessorResponseId, int visitPayUserId, decimal? amount = null)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                PaymentProcessorResponse paymentProcessorResponse = this._paymentProcessorResponseService.Value.GetPaymentProcessorResponse(paymentProcessorResponseId);
                if (paymentProcessorResponse != null
                    && paymentProcessorResponse.PaymentPaymentProcessorResponses.IsNotNullOrEmpty())
                {
                    IList<Payment> payments = paymentProcessorResponse.PaymentPaymentProcessorResponses.Select(x => x.Payment).ToList();

                    if (payments.Any(x => x.Guarantor.VpGuarantorId != vistiPayGuarantorId))
                    {
                        return new RefundPaymentResponseDto
                        {
                            IsError = true,
                            ErrorMessage = "Payments associated with this PaymentProcessorResponse do not belong to Guarantor."
                        };
                    }
                    if (amount.HasValue && amount.Value > payments.Sum(x => x.NetPaymentAmount))
                    {
                        return new RefundPaymentResponseDto
                        {
                            IsError = true,
                            ErrorMessage = "Refund cannot be greater than the original Net Payment Amount."
                        };
                    }
                    if (amount.HasValue && amount.Value <= 0m)
                    {
                        return new RefundPaymentResponseDto
                        {
                            IsError = true,
                            ErrorMessage = "Refund must be greater than zero."
                        };
                    }

                    // check for refundability
                    if (this._paymentReversalService.Value.GetPaymentReversalStatus(payments) == PaymentReversalStatusEnum.Refundable)
                    {
                        VisitPayUser visitPayUser = await this._visitPayUserService.FindByIdAsync(visitPayUserId.ToString());
                        IList<Payment> reversalPayments = await this._paymentReversalService.Value.RefundPaymentAsync(payments, visitPayUser, amount);

                        if (reversalPayments.IsNotNullOrEmpty() &&
                            (reversalPayments.All(x => x.PaymentStatus == PaymentStatusEnum.ActivePendingACHApproval)
                             || reversalPayments.All(x => x.PaymentStatus == PaymentStatusEnum.ClosedPaid)))
                        {
                            this.UpdateReversedPaymentStatuses(reversalPayments);

                            this.ProcessReagingForFinancePlans(reversalPayments);

                            unitOfWork.Commit();

                            reversalPayments.ForEach(reversedPayment =>
                            {
                                if(reversedPayment.ParentPayment.PaymentType == PaymentTypeEnum.LockBoxManualPromptSpecificFinancePlansBucketZero)
                                    this._journalEventApplicationService.Value.LogClientRefundedPayment(visitPayUserId, vistiPayGuarantorId, reversedPayment.ParentPayment.PaymentId, (int)reversedPayment.ParentPayment.PaymentType, context);
                            });
                            return new RefundPaymentResponseDto
                            {
                                IsError = false,
                                TransactionId = paymentProcessorResponse.PaymentProcessorSourceKey
                            };
                        }
                    }
                }
                unitOfWork.Commit();
                return new RefundPaymentResponseDto
                {
                    IsError = true,
                    ErrorMessage = "Payment Cannot be refunded"
                };
            }
        }

        public IDictionary<int, RefundPaymentStatusInfoDto> GetPaymentReversalStatus(IList<int> paymentProcessorResponseIds)
        {
            IList<PaymentProcessorResponse> paymentProcessorResponses = this._paymentProcessorResponseService.Value.GetPaymentProcessorResponses(paymentProcessorResponseIds);
            IDictionary<int, RefundPaymentStatusInfoDto> returnValues = new Dictionary<int, RefundPaymentStatusInfoDto>(paymentProcessorResponseIds.Count);
            foreach (PaymentProcessorResponse paymentProcessorResponse in paymentProcessorResponses)
            {
                IList<Payment> payments = paymentProcessorResponse.PaymentPaymentProcessorResponses.Select(x => x.Payment).ToList();
                returnValues[paymentProcessorResponse.PaymentProcessorResponseId] = new RefundPaymentStatusInfoDto
                {
                    PaymentReversalStatus = this._paymentReversalService.Value.GetPaymentReversalStatus(payments),
                    PaymentRedundableType = this._paymentReversalService.Value.GetPaymentRefundableType(payments)
                };
            }

            return returnValues;
        }

        private void ProcessReagingForFinancePlans(IList<Payment> reversalPayments)
        {
            int? actionVisitPayUserId = reversalPayments.Select(x => x.CreatedVisitPayUserId).FirstOrDefault();
            //TODO: reversal payment allocations are not getting populated with the FinancePlanId
            IEnumerable<Payment> parentPayments = reversalPayments.Select(x => x.ParentPayment);

            IEnumerable<int> financePlanIds = parentPayments.SelectMany(x => x.PaymentAllocations.Select(y => y.FinancePlanId))
                .Where(x => x.HasValue)
                .Select(x => (int)x)
                .Distinct();

            List<int> paymentIds = reversalPayments.Select(x => x.PaymentId).ToList();
            int guarantorId = reversalPayments.FirstOrDefault()?.Guarantor.VpGuarantorId ?? -1;

            foreach (int financePlanId in financePlanIds)
            {
                this.ReageFinancePlanPaymentBuckets(paymentIds, financePlanId, guarantorId, actionVisitPayUserId);
            }

        }

        private void UpdateReversedPaymentStatuses(IList<Payment> reversalPayments)
        {
            if (!reversalPayments.Any())
            {
                return;
            }
            int guarantorId = reversalPayments.First().Guarantor.VpGuarantorId;
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(guarantorId);
            VpStatement mostRecentStatement = this._statementService.Value.GetMostRecentStatement(guarantor);
            if (mostRecentStatement != null)
            {
                //Todo: This doesnt seem like the best way to do this. (Scar)

                DateTime dateToProcess = reversalPayments.Select(s => s.ScheduledPaymentDate).Min();
                List<Payment> scheduledPayments = reversalPayments.Where(w => !w.IsRecurringPayment()).ToList();
                List<Payment> recurringPayments = reversalPayments.Where(w => w.IsRecurringPayment()).ToList();

                string logMessage = $"PaymentReversalApplicationService::UpdateReversedPaymentStatuses - Payments Reversed: {string.Join(", ", reversalPayments.Select(x => x.PaymentId))}";
                this._statementService.Value.MarkStatementAsQueued(mostRecentStatement.VpStatementId, mostRecentStatement.VpGuarantorId, logMessage);

                this._paymentService.Value.PostProcessPayment(
                    guarantor,
                    mostRecentStatement,
                    dateToProcess,
                    scheduledPayments,
                    recurringPayments);
            }
        }

        public bool ReageFinancePlanPaymentBuckets(IList<int> reversalPaymentIds, int financePlanId, int guarantorId, int? actionVisitPayUserId = null)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(guarantorId);
            FinancePlan financePlan = this._financePlanService.Value.GetFinancePlan(guarantor, financePlanId);
            List<Payment> reversalPayments = this._paymentService.Value.GetPayments(reversalPaymentIds, guarantorId).ToList();

            // The parent payments will have all reversal new and old
            List<Payment> filteredPayments = this.FilterReversalPayments(reversalPayments);
            foreach (Payment originalPayment in filteredPayments)
            {
                List<FinancePlanAmountDueReversal> financePlanAmountDueReversals = new List<FinancePlanAmountDueReversal>();

                // Get the allocations specific to the finance plan
                IEnumerable<int> visitId = financePlan.FinancePlanVisits.Select(x => x.VisitId);
                IEnumerable<PaymentAllocation> allocationsFilteredByFpVisits = originalPayment.PaymentAllocationsAll.Where(x => visitId.Contains(x.PaymentVisit.VisitId));

                // build up a list of FinancePlanAmountDueReversals by "left" joining allocations to amounts due
                foreach (PaymentAllocation paymentAllocation in allocationsFilteredByFpVisits)
                {
                    List<FinancePlanAmountDue> financePlanAmountDues = financePlan.FinancePlanAmountsDue.Where(x => x.PaymentAllocationId == paymentAllocation.PaymentAllocationId).ToList();
                    decimal amountDue = financePlanAmountDues.Sum(x => x.AmountDue);
                    int? originalPaymentAllocationId = (financePlanAmountDues.Any()) ? paymentAllocation.PaymentAllocationId : (int?)null;
                    FinancePlanVisit financePlanVisit = financePlan.FinancePlanVisits.FirstOrDefault(x => x.VisitId == paymentAllocation.PaymentVisit.VisitId);
                    if (financePlanVisit == null)
                    {
                        continue;
                    }
                    FinancePlanAmountDueReversal financePlanAmountDueReversal = new FinancePlanAmountDueReversal
                    {
                        ActualPaymentAmount = paymentAllocation.ActualAmount,
                        AmountDue = amountDue,
                        PaymentAllocationId = originalPaymentAllocationId,
                        ReversalPaymentAllocationId = paymentAllocation.PaymentAllocationId,
                        FinancePlanVisitId = financePlanVisit.FinancePlanVisitId,
                        PaymentAllocationType = paymentAllocation.PaymentAllocationType,
                        PaymentId = originalPayment.PaymentId
                    };
                    financePlanAmountDueReversals.Add(financePlanAmountDueReversal);
                }

                FinancePlanAmountDueReversalGenerator financePlanAmountDueReversalGenerator = new FinancePlanAmountDueReversalGenerator(financePlanAmountDueReversals);
                List<FinancePlanAmountDueReversal> amountsDueToAdd = financePlanAmountDueReversalGenerator.GenerateFinancePlanAmountDueForReversals();

                foreach (FinancePlanAmountDueReversal amountDue in amountsDueToAdd)
                {
                    financePlan.OnPaymentReversalAllocation(amountDue.AmountDue,
                        amountDue.ActualPaymentAmount,
                        amountDue.ReversalPaymentAllocationId,
                        amountDue.PaymentId,
                        amountDue.FinancePlanVisitId,
                        amountDue.PaymentAllocationType,
                        actionVisitPayUserId);
                }
            }

            this._financePlanService.Value.CheckIfFinancePlansAreStillPastDueOrUncollectable(new List<FinancePlan> { financePlan });
            return true;
        }

        public PaymentDto ReallocateFromInterestToPrincipal(int vpGuarantorId, int? visitPayUserId, int financePlanId, decimal reallocateAmountAfterWriteOff, Dictionary<int, decimal> maxPerVisit)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            Payment reallocationPayment = this._paymentService.Value.CreateReallocationFinancePlanPayment(financePlanId, guarantor, visitPayUserId);

            this._paymentReversalService.Value.ReallocateInterestToPrincipal(reallocationPayment, reallocateAmountAfterWriteOff, maxPerVisit, financePlanId, visitPayUserId);
            this._paymentService.Value.AddDummyPaymentProcessorResponse(0m, reallocationPayment.PaymentMethod, null, reallocationPayment, PaymentProcessorResponseStatusEnum.Accepted);
            return Mapper.Map<PaymentDto>(reallocationPayment);
        }

        //this allows us to treat ach returns and reversal payments in the same manner
        private List<Payment> FilterReversalPayments(List<Payment> paymentsToBeReversed)
        {
            // In the case of a refund or void a new reversal payment is added to the parent payment
            // The parent payment has access to all allocations including reversals via the PaymentAllocationsAll
            // In these cases, we want to return just the parent to avoid looping through the individual reversal payments' allocations
            IEnumerable<Payment> reversalPayments = paymentsToBeReversed.Where(x => x.ParentPayment != null)
                .Where(x => !x.ParentPayment.PaymentMethod.IsAchType || x.PaymentType.IsInCategory(PaymentTypeEnumCategory.Reversal))
                .Select(x => x.ParentPayment)
                .Distinct();

            // In the case of an ACH Return a parent payment is not added, new, negating allocations are added to the existing payment.
            // There is no need to filter, however, in the case where paymentsToBeReversed have mixed types (reversals and return ach)
            // we need to combine the lists
            IEnumerable<Payment> achPayments = paymentsToBeReversed.Where(x => x.PaymentMethod?.IsAchType ?? false)
                                              .Where(x => !x.PaymentType.IsInCategory(PaymentTypeEnumCategory.Reversal));

            List<Payment> filteredPayments = new List<Payment>();

            filteredPayments.AddRange(reversalPayments ?? new List<Payment>());
            filteredPayments.AddRange(achPayments ?? new List<Payment>());
            return filteredPayments;

        }
    }
}
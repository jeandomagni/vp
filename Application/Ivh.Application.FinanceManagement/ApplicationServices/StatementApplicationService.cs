﻿namespace Ivh.Application.FinanceManagement.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Core.Common.Dtos;
    using Core.Common.Interfaces;
    using Domain.FinanceManagement.Statement.Entities;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.User.Entities;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using Magnum.Linq;
    using OfficeOpenXml;
    using OfficeOpenXml.Style;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Strings;

    public class StatementApplicationService : ApplicationService, IStatementApplicationService
    {
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IStatementDateService> _statementDateService;
        private readonly Lazy<IVisitService> _visitService;
        private readonly Lazy<IFinancePlanStatementService> _financePlanStatementService;
        private readonly Lazy<IServiceGroupApplicationService> _serviceGroupApplicationService;
        private readonly Lazy<IVpStatementService> _vpStatementService;
        private readonly Lazy<IVisitApplicationService> _visitApplicationService;

        public StatementApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IVpStatementService> vpStatementService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IVisitService> visitService,
            Lazy<IVisitApplicationService> visitApplicationService,
            Lazy<IStatementDateService> statementDateService,
            Lazy<IFinancePlanStatementService> financePlanStatementService,
            Lazy<IServiceGroupApplicationService> serviceGroupApplicationService
        ) : base(applicationServiceCommonService)
        {
            this._vpStatementService = vpStatementService;
            this._guarantorService = guarantorService;
            this._statementDateService = statementDateService;
            this._visitService = visitService;
            this._visitApplicationService = visitApplicationService;
            this._financePlanStatementService = financePlanStatementService;
            this._serviceGroupApplicationService = serviceGroupApplicationService;
        }

        public FinancePlanStatementDto GetFinancePlanStatementByPeriodStartDate(int guarantorId, DateTime periodStartDate)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(guarantorId);
            FinancePlanStatement fpStatement = this._financePlanStatementService.Value.GetFinancePlanStatementByPeriodStartDate(guarantorId, periodStartDate);

            if (fpStatement == null)
            {
                return null;
            }

            FinancePlanStatementDto financePlanStatementDto = this.MapToFinancePlanStatementDto(guarantor, fpStatement);
            return financePlanStatementDto;       
        }

        public FinancePlanStatementDto GetMostRecentFinancePlanStatement(int guarantorId)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(guarantorId);
            FinancePlanStatement fpStatement = this._financePlanStatementService.Value.GetMostRecentStatement(guarantor);
            
            if (fpStatement == null)
            {
                return null;
            }

            FinancePlanStatementDto financePlanStatementDto = this.MapToFinancePlanStatementDto(guarantor, fpStatement);
            return financePlanStatementDto;
        }

        public FinancePlanStatementDto GetFinancePlanStatement(int financePlanStatementId, int visitPayUserId)
        {
            int guarantorId = this._guarantorService.Value.GetGuarantorId(visitPayUserId);
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(guarantorId);
            FinancePlanStatement fpStatement = this._financePlanStatementService.Value.GetFinancePlanStatement(guarantorId, financePlanStatementId);

            FinancePlanStatementDto financePlanStatementDto = this.MapToFinancePlanStatementDto(guarantor, fpStatement);

            this.SetServiceGroupAttributes(fpStatement.VpGuarantorId, financePlanStatementDto, financePlanStatementDto.FinancePlanStatementVisits);

            return financePlanStatementDto;
        }

        private FinancePlanStatementDto MapToFinancePlanStatementDto(Guarantor guarantor, FinancePlanStatement fpStatement)
        {
            FinancePlanStatementDto fpStatementDto = Mapper.Map<FinancePlanStatementDto>(fpStatement);
            int vpStatementId = fpStatement.VpStatementId;
            //fpStatementDto = Mapper.Map(guarantor, fpStatementDto);

            fpStatementDto.GuarantorRegistrationYear = guarantor.RegistrationDate.Year;
            fpStatementDto.GuarantorVisitPayUserFirstName = guarantor.User.FirstName;
            fpStatementDto.GuarantorVisitPayUserLastName = guarantor.User.LastName;

            fpStatementDto.GuarantorVisitPayUserFirstName = guarantor.User.FirstName;
            fpStatementDto.GuarantorVisitPayUserLastName = guarantor.User.LastName;
            
            VisitPayUserAddress currentAddress = guarantor.User.CurrentPhysicalAddress;
            fpStatementDto.GuarantorAddressStreet1 = currentAddress?.AddressStreet1;
            fpStatementDto.GuarantorAddressStreet2 = currentAddress?.AddressStreet2;
            fpStatementDto.GuarantorCityStateZip =
                FormatHelper.CityStateZipLenient(
                    currentAddress?.City,
                    currentAddress?.StateProvince,
                    currentAddress?.PostalCode);

            fpStatementDto.Notifications = fpStatement.FinancePlanStatementNotifications.Select(x=>x.NotificationText).ToList();

            IReadOnlyList<Visit> visits = this._visitService.Value.GetVisits(guarantor.VpGuarantorId, fpStatementDto.FinancePlanStatementVisits.Select(x => x.VisitId).ToList());
            IList<VisitDto> visitDtos = Mapper.Map<IList<VisitDto>>(visits);
            foreach (VisitDto visit in visitDtos)
            {
                FinancePlanStatementVisitDto financePlanStatementVisitDto = fpStatementDto.FinancePlanStatementVisits.FirstOrDefault(x => x.VisitId == visit.VisitId);
                if (financePlanStatementVisitDto != null)
                {
                    // map to self for masking
                    VisitDto visitDto = Mapper.Map<VisitDto>(visit);

                    Mapper.Map(visitDto, financePlanStatementVisitDto);
                }
            }

            //TODO: These need to be snapshot values when creating a Vp3 statement
            // Not a trivial task, but I suspect passing interestAssessments
            // into StatementSnapshotCreationApplicationService.GenerateStatementForDateAsync()
            // would be a way to start implementing, but still might have issues with JSON, null values, etc
            // for previous snapshots
            foreach (FinancePlanStatementFinancePlanDto financePlanStatementFinancePlanDto in fpStatementDto.FinancePlanStatementFinancePlans)
            {
                int financePlanId = financePlanStatementFinancePlanDto.FinancePlanId;
                decimal interestDue = this._financePlanStatementService.Value.GetInterestDueForFinancePlanStatement(vpStatementId, financePlanId);
                decimal principalDue = (financePlanStatementFinancePlanDto.StatementedSnapshotFinancePlanMonthlyPaymentAmount ?? 0m) - interestDue;
                financePlanStatementFinancePlanDto.StatementedSnapshotFinancePlanInterestDue = interestDue;
                financePlanStatementFinancePlanDto.StatementedSnapshotFinancePlanPrincipalDue = principalDue;
            }
            
            return fpStatementDto;
        }
        
        public StatementDto GetStatement(int statementId, int visitPayUserId)
        {
            int guarantorId = this._guarantorService.Value.GetGuarantorId(visitPayUserId);
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(guarantorId);
            VpStatement vpStatement = this._vpStatementService.Value.GetStatement(guarantorId, statementId);
            StatementDto statementDto = new StatementDto();
            //In the case of the old statements, it was one document, make sure it maps into one object.
            if (vpStatement.StatementVersion == VpStatementVersionEnum.Vp2)
            {
                statementDto = Mapper.Map(Mapper.Map<StatementDto>(this._financePlanStatementService.Value.GetFinancePlanStatementByVpStatementId(guarantorId, statementId)), statementDto);
            }

            statementDto = Mapper.Map(vpStatement, statementDto);

            statementDto.GuarantorRegistrationYear = guarantor.RegistrationDate.Year;
            statementDto.GuarantorVisitPayUserFirstName = guarantor.User.FirstName;
            statementDto.GuarantorVisitPayUserLastName = guarantor.User.LastName;
            statementDto.NextStatementDate = this._statementDateService.Value.GetNextStatementDate(null, guarantor, statementDto.StatementDate);

            statementDto.GuarantorVisitPayUserFirstName = guarantor.User.FirstName;
            statementDto.GuarantorVisitPayUserLastName = guarantor.User.LastName;

            VisitPayUserAddress currentAddress = guarantor.User.CurrentPhysicalAddress;
            statementDto.GuarantorAddressStreet1 = currentAddress?.AddressStreet1;
            statementDto.GuarantorAddressStreet2 = currentAddress?.AddressStreet2;
            statementDto.GuarantorCityStateZip = 
                FormatHelper.CityStateZipLenient(
                    currentAddress?.City, 
                    currentAddress?.StateProvince, 
                    currentAddress?.PostalCode);

            this._visitApplicationService.Value.SetIsPastDue(statementDto.Visits.Select(x => x.Visit).ToList());

            // statement alerts
            this.FillInStatementDto(guarantor, statementDto);

            // service group attributes
            if (vpStatement.StatementVersion == VpStatementVersionEnum.Vp2)
            {
                this.SetServiceGroupAttributes(statementDto, statementDto.VisitsNotOnFinancePlan);
            }
            else
            {
                this.SetServiceGroupAttributes(statementDto, statementDto.ActiveVisitsNotOnFinancePlan);
            }

            return statementDto;
        }

        public IReadOnlyList<FinancePlanStatementDto> GetFinancePlanStatements(int visitPayUserId)
        {
            int guarantorId = this._guarantorService.Value.GetGuarantorId(visitPayUserId);
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(guarantorId);
            IReadOnlyList<FinancePlanStatement> financePlanStatements = this._financePlanStatementService.Value.GetFinancePlanStatements(guarantorId);
            return Mapper.Map<IReadOnlyList<FinancePlanStatementDto>>(financePlanStatements);
        }

        public IReadOnlyList<StatementDto> GetStatements(int visitPayUserId)
        {
            int guarantorId = this._guarantorService.Value.GetGuarantorId(visitPayUserId);
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(guarantorId);
            IReadOnlyList<VpStatement> statements = this._vpStatementService.Value.GetStatements(guarantorId);
            IReadOnlyList<StatementDto> statementDtos = Mapper.Map<IReadOnlyList<StatementDto>>(statements);

            statements
                .Zip(statementDtos, (s, sd) => new {Statement = s, StatementDto = sd})
                .ForEach(element => this.FillInStatementDto(guarantor, element.StatementDto));

            return statementDtos;
        }
        
        public int GetStatementTotals(int guarantorVisitPayUserId)
        {
            int guarantorId = this._guarantorService.Value.GetGuarantorId(guarantorVisitPayUserId);
            return this._vpStatementService.Value.GetStatementTotals(guarantorId);
        }

        public StatementDto GetLastStatement(int vpGuarantorId)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            return this.GetLastStatement(guarantor);
        }

        public StatementDto GetLastStatement(GuarantorDto guarantorDto)
        {
            Guarantor guarantor = Mapper.Map<Guarantor>(guarantorDto);
            return this.GetLastStatement(guarantor);
        }

        private StatementDto GetLastStatement(Guarantor guarantor)
        {
            VpStatement statement = this._vpStatementService.Value.GetMostRecentStatement(guarantor);

            if (statement == null || guarantor == null)
            {
                return null;
            }

            StatementDto statementDto = Mapper.Map<StatementDto>(statement);
            this.FillInStatementDto(guarantor, statementDto);

            return statementDto;
        }

        public bool DoesGuarantorHaveStatement(int vpGuarantorId)
        {
            return this._vpStatementService.Value.DoesGuarantorHaveStatement(vpGuarantorId);
        }
        
        public GuarantorWithLastStatementResultsDto GetGuarantorsWithLastStatement(GuarantorFilterDto guarantorFilterDto, int pageNumber, int pageSize)
        {
            GuarantorFilter guarantorFilter = Mapper.Map<GuarantorFilter>(guarantorFilterDto);
            IList<Guarantor> guarantors = this._guarantorService.Value.GetGuarantors(guarantorFilter);
            IReadOnlyList<GuarantorDto> guarantorDtos = Mapper.Map<IReadOnlyList<GuarantorDto>>(guarantors);
            int totalRecords = guarantors.Count;

            IReadOnlyList<GuarantorWithLastStatementDto> withStatements = Mapper.Map<IReadOnlyList<GuarantorWithLastStatementDto>>(guarantorDtos);
            foreach (GuarantorWithLastStatementDto guarantorWithStatement in withStatements)
            {
                Guarantor guarantor = guarantors.First(x => x.VpGuarantorId == guarantorWithStatement.Guarantor.VpGuarantorId);
                VpStatement statement = this._vpStatementService.Value.GetMostRecentStatement(guarantor);
                guarantorWithStatement.LastStatement = Mapper.Map<StatementDto>(statement);
                guarantorWithStatement.IsAwaitingStatement = this._vpStatementService.Value.IsAwaitingStatement(statement);
            }

            if ("LastStatementBalance".Equals(guarantorFilterDto.SortField, StringComparison.InvariantCultureIgnoreCase))
            {
                bool isAscendingOrder = "asc".Equals(guarantorFilterDto.SortOrder, StringComparison.InvariantCultureIgnoreCase);
                withStatements = (isAscendingOrder ? withStatements.OrderBy(x => x.LastStatement == null ? 0 : x.LastStatement.StatementedSnapshotTotalBalance) : withStatements.OrderByDescending(x => x.LastStatement == null ? 0 : x.LastStatement.StatementedSnapshotTotalBalance)).ToList();
            }

            List<IEnumerable<GuarantorWithLastStatementDto>> batches = withStatements.Batch(pageSize).ToList();
            IEnumerable<GuarantorWithLastStatementDto> batch = batches.Skip(pageNumber - 1).FirstOrDefault() ?? Enumerable.Empty<GuarantorWithLastStatementDto>();

            return new GuarantorWithLastStatementResultsDto
            {
                Guarantors = batch.ToList(),
                TotalRecords = totalRecords
            };
        }

        public GuarantorWithLastFinancePlanStatementResultsDto GetGuarantorsWithLastFinancePlanStatement(GuarantorFilterDto guarantorFilterDto, int pageNumber, int pageSize)
        {
            GuarantorFilter guarantorFilter = Mapper.Map<GuarantorFilter>(guarantorFilterDto);
            IList<Guarantor> guarantors = this._guarantorService.Value.GetGuarantors(guarantorFilter);
            IReadOnlyList<GuarantorDto> guarantorDtos = Mapper.Map<IReadOnlyList<GuarantorDto>>(guarantors);
            int totalRecords = guarantors.Count;

            IReadOnlyList<GuarantorWithLastFinancePlanStatementDto> withStatements = Mapper.Map<IReadOnlyList<GuarantorWithLastFinancePlanStatementDto>>(guarantorDtos);
            foreach (GuarantorWithLastFinancePlanStatementDto guarantorWithStatement in withStatements)
            {
                Guarantor guarantor = guarantors.First(x => x.VpGuarantorId == guarantorWithStatement.Guarantor.VpGuarantorId);
                FinancePlanStatementDto fpStatement = this.GetMostRecentFinancePlanStatement(guarantor.VpGuarantorId);
                guarantorWithStatement.LastFinancePlanStatement = fpStatement;
            }

            List<IEnumerable<GuarantorWithLastFinancePlanStatementDto>> batches = withStatements.Batch(pageSize).ToList();
            IEnumerable<GuarantorWithLastFinancePlanStatementDto> batch = batches.Skip(pageNumber - 1).FirstOrDefault() ?? Enumerable.Empty<GuarantorWithLastFinancePlanStatementDto>();

            return new GuarantorWithLastFinancePlanStatementResultsDto
            {
                Guarantors = batch.ToList(),
                TotalRecords = totalRecords
            };
        }

        public byte[] ExportGuarantorsWithLastStatement(GuarantorFilterDto guarantorFilterDto, string userName)
        {
            const int maxExportRecords = 10000;
            GuarantorWithLastStatementResultsDto guarantorResults = this.GetGuarantorsWithLastStatement(guarantorFilterDto, 1, maxExportRecords);

            using (ExcelPackage package = new ExcelPackage())
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Account List");

                // title
                worksheet.Cells["A1"].Value = "Account List";
                worksheet.Cells["A1:K1"].Merge = true;
                using (ExcelRange range = worksheet.Cells["A1:J1"])
                {
                    range.Style.Font.Size = 13;
                    range.Style.Font.Bold = true;
                }

                // export info
                worksheet.Cells["A3"].Value = "File Date:";
                worksheet.Cells["A3:B3"].Merge = true;
                worksheet.Cells["C3"].Value = DateTime.UtcNow.ToString(Format.MonthDayYearFormat);
                worksheet.Cells["C3:K3"].Merge = true;

                worksheet.Cells["A4"].Value = "Exported By:";
                worksheet.Cells["A4:B4"].Merge = true;
                worksheet.Cells["C4"].Value = userName;
                worksheet.Cells["C4:K4"].Merge = true;

                using (ExcelRange range = worksheet.Cells["A3:K4"])
                {
                    range.Style.Font.Size = 11;
                    range.Style.Font.Bold = true;
                }

                // data rows
                if (guarantorResults.TotalRecords > 0)
                {
                    // too many records
                    if (guarantorResults.TotalRecords > maxExportRecords)
                    {
                        worksheet.Cells["A6"].Value = string.Format("Incomplete download. {0} rows of {1} rows were downloaded. Only {0} rows can be downloaded.", maxExportRecords.ToString("N0"), guarantorResults.TotalRecords.ToString("N0"));
                        worksheet.Cells["A6:K6"].Merge = true;
                        using (ExcelRange range = worksheet.Cells["A6:K6"])
                        {
                            range.Style.Font.Italic = true;
                        }
                    }

                    int headerIndex = guarantorResults.TotalRecords > maxExportRecords ? 8 : 6;

                    // data headers
                    worksheet.Cells[$"A{headerIndex}"].Value = "Name";
                    worksheet.Cells[$"B{headerIndex}"].Value = "Status";
                    worksheet.Cells[$"C{headerIndex}"].Value = this.Client.Value.HsGuarantorPatientIdentifier;
                    worksheet.Cells[$"D{headerIndex}"].Value = this.Client.Value.VpGuarantorPatientIdentifier;
                    worksheet.Cells[$"E{headerIndex}"].Value = "Last 4 SSN";
                    worksheet.Cells[$"F{headerIndex}"].Value = "DOB";
                    worksheet.Cells[$"G{headerIndex}"].Value = "Username";
                    worksheet.Cells[$"H{headerIndex}"].Value = "Notification Email";
                    worksheet.Cells[$"I{headerIndex}"].Value = "Last Statement Bal";
                    worksheet.Cells[$"J{headerIndex}"].Value = "Last Statement Date";
                    worksheet.Cells[$"K{headerIndex}"].Value = "Statement Cycle Status";

                    using (ExcelRange range = worksheet.Cells[string.Format("A{0}:K{0}", headerIndex)])
                    {
                        range.Style.Font.Size = 11;
                        range.Style.Font.Bold = true;
                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        range.Style.Border.BorderAround(ExcelBorderStyle.Medium);
                        range.Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    }

                    int startRowIndex = headerIndex + 1;
                    int rowIndex = startRowIndex;

                    foreach (GuarantorWithLastStatementDto result in guarantorResults.Guarantors)
                    {
                        worksheet.Cells[$"A{rowIndex}"].Value = result.Guarantor.User.DisplayFullName;
                        worksheet.Cells[$"B{rowIndex}"].Value = result.Guarantor.VpGuarantorStatus == VpGuarantorStatusEnum.Active ? "Active" : "Closed";
                        worksheet.Cells[$"C{rowIndex}"].Value = string.Join(", ", result.Guarantor.HsGuarantorsSourceSystemKeys);
                        worksheet.Cells[$"D{rowIndex}"].Value = result.Guarantor.VpGuarantorId;
                        worksheet.Cells[$"E{rowIndex}"].Value = result.Guarantor.User.SSN4.HasValue ? $"{result.Guarantor.User.SSN4.Value.ToString().PadLeft(4, '0')}" : "";
                        worksheet.Cells[$"F{rowIndex}"].Value = result.Guarantor.User.DateOfBirth;
                        worksheet.Cells[$"G{rowIndex}"].Value = result.Guarantor.User.UserName;
                        worksheet.Cells[$"H{rowIndex}"].Value = result.Guarantor.User.Email;
                        worksheet.Cells[$"I{rowIndex}"].Value = result.LastStatement?.StatementedSnapshotTotalBalance;
                        worksheet.Cells[$"J{rowIndex}"].Value = result.LastStatement?.StatementDate;
                        worksheet.Cells[$"K{rowIndex}"].Value = result.LastStatement == null ? "Never Statemented" : result.IsAwaitingStatement ? "Pending Statement" : "Payment Due";

                        worksheet.Cells[$"C{rowIndex}"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        worksheet.Cells[$"D{rowIndex}"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        worksheet.Cells[$"E{rowIndex}"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        worksheet.Cells[$"F{rowIndex}"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        worksheet.Cells[$"F{rowIndex}"].Style.Numberformat.Format = "mm/dd/yyyy";
                        worksheet.Cells[$"J{rowIndex}"].Style.Numberformat.Format = Format.ExportMoneyFormat;
                        worksheet.Cells[$"K{rowIndex}"].Style.Numberformat.Format = "mm/dd/yyyy";

                        rowIndex++;
                    }

                    // border
                    using (ExcelRange range = worksheet.Cells[$"A{startRowIndex}:K{rowIndex - 1}"])
                    {
                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }
                }
                else
                {
                    worksheet.Cells["A6"].Value = "There are no accounts to display at this time.";
                    worksheet.Cells["A6:J6"].Merge = true;
                    using (ExcelRange range = worksheet.Cells["A6:K6"])
                    {
                        range.Style.Font.Size = 11;
                        range.Style.Font.Bold = true;
                    }
                }

                worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();

                return package.GetAsByteArray();
            }
        }

        #region dates

        public DateTime? GetNextStatementDate(int vpGuarantorId, DateTime dateToProcess)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            if (guarantor == null)
            {
                return null;
            }

            int statementCount = this._vpStatementService.Value.GetStatementTotals(guarantor.VpGuarantorId);
            return this._statementDateService.Value.GetNextStatementDate(null, guarantor, dateToProcess, false, statementCount == 0);
        }

        public DateTime? GetNextPaymentDate(int vpGuarantorId)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            return this._vpStatementService.Value.GetNextPaymentDate(guarantor);
        }

        #endregion

        public byte[] GetStatementContent(int vpGuarantorId, int vpStatementId)
        {
            return this._vpStatementService.Value.GetStatementContent(vpGuarantorId, vpStatementId);
        }

        private void FillInStatementDto(Guarantor guarantor, StatementDto statementDto)
        {
            statementDto.PaymentDueDay = guarantor.PaymentDueDay;

            this.SetStatementAlerts(statementDto);
        }

        private void SetStatementAlerts(StatementDto statementDto)
        {
            if (statementDto.StatementedSnapshotPastDueFinancePlanBalanceSum.GetValueOrDefault(0) > 0)
            {
                string pastDueFinancePlanAlertMessage = "You have past due finance plan payments.";
                statementDto.Alerts.Add(new StatementAlertDto
                {
                    AlertType = AlertTypeEnum.ActionRequired,
                    AlertMessagePdf = pastDueFinancePlanAlertMessage,
                    AlertMessageWeb = pastDueFinancePlanAlertMessage
                });
            }

            if (statementDto.StatementedSnapshotPastDueVisitBalanceSum.GetValueOrDefault(0) > 0)
            {
                string pastDueVisitAlertMessage = "You have a past due visit balance.";
                statementDto.Alerts.Add(new StatementAlertDto
                {
                    AlertType = AlertTypeEnum.ActionRequired,
                    AlertMessagePdf = pastDueVisitAlertMessage,
                    AlertMessageWeb = pastDueVisitAlertMessage
                });
            }

            if (statementDto.StatementedSnapshotVisitCreditBalanceSum.GetValueOrDefault(0) < 0)
            {
                string creditBalance = Math.Abs(statementDto.StatementedSnapshotVisitCreditBalanceSum.GetValueOrDefault(0)).ToString("C");
                string creditAlertMessagePdf = $"You have a credit balance totaling {creditBalance}.";
                string creditAlertMessageWeb = $"{creditAlertMessagePdf}  <a href=\"javascript:; \" title=\"Submit Support Request\" data-click=\"create-support-request\">Click here</a> to request a refund.";
                statementDto.Alerts.Add(new StatementAlertDto
                {
                    AlertType = AlertTypeEnum.Information,
                    AlertMessagePdf = creditAlertMessagePdf,
                    AlertMessageWeb = creditAlertMessageWeb
                });
            }

            if (statementDto.StatementedSnapshotPaymentGatewayErrorBalanceSum.GetValueOrDefault(0) > 0)
            {
                string paymentsNotAppliedToStatementsAmount = statementDto.StatementedSnapshotPaymentGatewayErrorBalanceSum.GetValueOrDefault(0).ToString("C");
                string paymentsNotAppliedToStatementsMessagePdf = $"You have {paymentsNotAppliedToStatementsAmount} in {this.Client.Value.ClientShortName} payments that are not applied to this statement.";
                string paymentsNotAppliedToStatementsMessageWeb = $"{paymentsNotAppliedToStatementsMessagePdf}  <a href=\"[[UrlPlaceholder]]\" title =\"Go to My Payments\">Check your payment history</a>.";

                statementDto.Alerts.Add(new StatementAlertDto
                {
                    AlertType = AlertTypeEnum.Information,
                    AlertMessagePdf = paymentsNotAppliedToStatementsMessagePdf,
                    AlertMessageWeb = paymentsNotAppliedToStatementsMessageWeb,
                    RequireUrlReplacement = true,
                    ControllerAction = "MyPayments",
                    ControllerName = "Payment"
                });
            }
        }

        private void SetServiceGroupAttributes(StatementDto statementDto, IList<StatementVisitDto> visits)
        {
            IList<ServiceGroupDto> serviceGroupDtos = this._serviceGroupApplicationService.Value.GetAllServiceGroupsWithContentAsync().Result;
            if (serviceGroupDtos == null || serviceGroupDtos.All(x => string.IsNullOrWhiteSpace(x.StatementDisclaimer)))
            {
                return;
            }

            List<int> serviceGroupIdsForStatementVisits = visits
                .Where(x => x.Visit.ServiceGroupId.HasValue)
                .Select(x => x.Visit.ServiceGroupId.Value)
                .Distinct()
                .Select(x => serviceGroupDtos.FirstOrDefault(z => z.ServiceGroupId == x))
                .Where(x => x != null && !string.IsNullOrWhiteSpace(x.StatementDisclaimer))
                .Select(x => x.ServiceGroupId)
                .ToList();
            
            foreach (StatementVisitDto statementVisitDto in visits)
            {
                ServiceGroupDto serviceGroupDto = serviceGroupDtos.FirstOrDefault(x => x.ServiceGroupId == statementVisitDto.Visit.ServiceGroupId);
                if (serviceGroupDto == null)
                {
                    continue;
                }

                if (string.IsNullOrWhiteSpace(serviceGroupDto.StatementDisclaimer))
                {
                    continue;
                }

                string indicator = new string('*', serviceGroupIdsForStatementVisits.IndexOf(serviceGroupDto.ServiceGroupId) + 1);
                statementVisitDto.ServiceGroupIndicator = indicator;

                string disclaimer = $"{indicator} {serviceGroupDto.StatementDisclaimer}";
                if (!statementDto.ServiceGroupDisclaimers.Contains(disclaimer))
                {
                    statementDto.ServiceGroupDisclaimers.Add(disclaimer);
                }
            }
        }

        private void SetServiceGroupAttributes(int vpGuarantorId, FinancePlanStatementDto statementDto, IList<FinancePlanStatementVisitDto> statementVisits)
        {
            IList<ServiceGroupDto> serviceGroupDtos = this._serviceGroupApplicationService.Value.GetAllServiceGroupsWithContentAsync().Result;
            if (serviceGroupDtos.All(x => string.IsNullOrWhiteSpace(x.StatementDisclaimer)))
            {
                return;
            }

            IReadOnlyList<Visit> visits = this._visitService.Value.GetVisits(vpGuarantorId, statementVisits.Select(x => x.VisitId).ToList());

            List<int> serviceGroupIdsForVisits = visits
                .Where(x => x.ServiceGroupId.HasValue)
                .Select(x => x.ServiceGroupId.Value)
                .Distinct()
                .Select(x => serviceGroupDtos.FirstOrDefault(z => z.ServiceGroupId == x))
                .Where(x => x != null && !string.IsNullOrWhiteSpace(x.StatementDisclaimer))
                .Select(x => x.ServiceGroupId)
                .ToList();
            
            foreach (FinancePlanStatementVisitDto statementVisitDto in statementVisits)
            {
                Visit visit = visits.FirstOrDefault(x => x.VisitId == statementVisitDto.VisitId);
                if (visit == null)
                {
                    continue;
                }

                ServiceGroupDto serviceGroupDto = serviceGroupDtos.FirstOrDefault(x => x.ServiceGroupId == visit.ServiceGroupId);
                if (serviceGroupDto == null)
                {
                    continue;
                }

                if (string.IsNullOrWhiteSpace(serviceGroupDto.StatementDisclaimer))
                {
                    continue;
                }

                string indicator = new string('*', serviceGroupIdsForVisits.IndexOf(serviceGroupDto.ServiceGroupId) + 1);
                statementVisitDto.ServiceGroupIndicator = indicator;

                string disclaimer = $"{indicator} {serviceGroupDto.StatementDisclaimer}";
                if (!statementDto.ServiceGroupDisclaimers.Contains(disclaimer))
                {
                    statementDto.ServiceGroupDisclaimers.Add(disclaimer);
                }
            }
        }
    }
}
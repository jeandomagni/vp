﻿namespace Ivh.Application.FinanceManagement.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Core.Common.Dtos;
    using Domain.User.Entities;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Interfaces;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Payment.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Ivh.Common.Base.Constants;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.BalanceTransfer;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;
    using MassTransit;
    using NHibernate;

    public class BalanceTransferApplicationService : ApplicationService, IBalanceTransferApplicationService
    {
        private readonly Lazy<IFinancePlanService> _financePlanService;
        private readonly Lazy<IPaymentRepository> _paymentRepository;
        private readonly Lazy<IBalanceTransferService> _balanceTransferService;
        private readonly Lazy<IVisitService> _visitService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly ISession _session;

        public BalanceTransferApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IFinancePlanService> financePlanService,
            Lazy<IPaymentRepository> paymentRepository,
            Lazy<IBalanceTransferService> balanceTransferService,
            ISessionContext<VisitPay> sessionContext,
            Lazy<IVisitService> visitService,
            Lazy<IGuarantorService> guarantorService) : base(applicationServiceCommonService)
        {
            this._financePlanService = financePlanService;
            this._paymentRepository = paymentRepository;
            this._balanceTransferService = balanceTransferService;
            this._session = sessionContext.Session;
            this._visitService = visitService;
            this._guarantorService = guarantorService;
        }

        public void SetVisitBalanceTransferStatus(int visitId, int visitPayUserId, int changedByVisitPayUserId, string notes, int balanceTransferStatusId)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantorEx(visitPayUserId);

            BalanceTransferStatusHistory balanceTransferStatusHistory = new BalanceTransferStatusHistory
            {
                Visit = this._visitService.Value.GetVisit(guarantor.VpGuarantorId, visitId),
                BalanceTransferStatus = new BalanceTransferStatus { BalanceTransferStatusId = balanceTransferStatusId },
                ChangedBy = new VisitPayUser { VisitPayUserId = changedByVisitPayUserId },
                InsertDate = DateTime.UtcNow,
                Notes = notes
            };

            if (balanceTransferStatusHistory.Visit != null)
            {
                using (UnitOfWork uow = new UnitOfWork(this._session))
                {
                    this._balanceTransferService.Value.SetVisitBalanceTransferStatus(balanceTransferStatusHistory);
                    uow.Commit();
                }
            }
        }

        public BalanceTransferStatusHistoryResultsDto GetBalanceTransferStatusHistory(BalanceTransferStatusHistoryFilterDto filterDto)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantorEx(filterDto.GuarantorVisitPayUserId);
            Visit visit = this._visitService.Value.GetVisit(guarantor.VpGuarantorId, filterDto.VisitId);

            return new BalanceTransferStatusHistoryResultsDto
            {
                BalanceTransferStatusHistories = visit.BalanceTransferStatusHistory
                    .OrderByDescending(x => x.InsertDate)
                    .ThenByDescending(x => x.BalanceTransferStatusHistoryId)
                    .Skip((filterDto.Page - 1) * filterDto.Rows)
                    .Take(filterDto.Rows)
                    .Select(x => Mapper.Map<BalanceTransferStatusHistoryDto>(x))
                    .ToList(),
                TotalRecords = visit.BalanceTransferStatusHistory.Count
            };
        }
        
        public void PublishBalanceTransferFinancePlanPaymentMessage(int vpGuarantorId, int paymentId)
        {
            if (this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.BalanceTransfer))
            {
                Payment payment = this._paymentRepository.Value.GetPayment(vpGuarantorId, paymentId);
                bool hasBalanceTransferableVisits = payment.PaymentAllocations
                    .Select(x => x.PaymentVisit)
                    .Where(x => x.BalanceTransferStatusEnum != null)
                    .Any(x => x.BalanceTransferStatusEnum != BalanceTransferStatusEnum.Ineligible);
                if (hasBalanceTransferableVisits)
                {
                    List<FinancePlanStatusEnum> listOfStatuses = EnumExt.GetAllInCategory<FinancePlanStatusEnum>(FinancePlanStatusEnumCategory.Active).Select(x => x).ToList();
                    listOfStatuses.AddRange(EnumExt.GetAllInCategory<FinancePlanStatusEnum>(FinancePlanStatusEnumCategory.Pending).Select(x => x).ToList());

                    FinancePlanVisitResult financePlanResult = this._financePlanService.Value.AreVisitsOnFinancePlan_FinancePlanVisitResult(payment.PaymentAllocations.Select(x => x.PaymentVisit.VisitId).ToList(), null, listOfStatuses).FirstOrDefault();
                    if (financePlanResult != null)
                    {
                        FinancePlan financePlan = this._financePlanService.Value.GetFinancePlanById(financePlanResult.FinancePlanId);
                        if (financePlan != null)
                        {
                            BalanceTransferFinancePlanMessage financePlanMessage = new BalanceTransferFinancePlanMessage
                            {
                                FinancePlanId = financePlan.FinancePlanId,
                                FinancePlanStatusId = financePlan.FinancePlanStatus.FinancePlanStatusId,
                                DurationRangeStart = financePlan.FinancePlanOffer.DurationRangeStart,
                                DurationRangeEnd = financePlan.FinancePlanOffer.DurationRangeEnd
                            };

                            List<BalanceTransferVisitMessage> visitMessages = payment.PaymentAllocations.Select(x => x.PaymentVisit).Distinct()
                                .Where(x => (x.BalanceTransferStatusEnum ?? BalanceTransferStatusEnum.Ineligible) != BalanceTransferStatusEnum.Ineligible)
                                .Select(x => new BalanceTransferVisitMessage
                                {
                                    VisitId = x.VisitId,
                                    VisitSourceSystemKey = x.SourceSystemKey,
                                    VisitBillingSystemId = x.BillingSystemId ?? 0,
                                    PrincipalPaymentAmount = payment.PaymentAllocations.Where(a => a.PaymentAllocationType == PaymentAllocationTypeEnum.VisitPrincipal && a.PaymentVisit.VisitId == x.VisitId)
                                        .Sum(p => p.ActualAmount),
                                    InterestPaymentAmount = payment.PaymentAllocations.Where(a => a.PaymentAllocationType == PaymentAllocationTypeEnum.VisitInterest && a.PaymentVisit.VisitId == x.VisitId)
                                        .Sum(p => p.ActualAmount),
                                    BalanceTransferStatusId = (int?)x.BalanceTransferStatusEnum ?? 0
                                })
                                .ToList();

                            BalanceTransferPaymentMessage balanceTransferPaymentMessage = new BalanceTransferPaymentMessage
                            {
                                PaymentType = payment.PaymentType
                            };

                            BalanceTransferFinancePlanPaymentSettledMessage message = new BalanceTransferFinancePlanPaymentSettledMessage
                            {
                                Visits = visitMessages,
                                FinancePlan = financePlanMessage,
                                Payment = balanceTransferPaymentMessage
                            };
                            this._session.Transaction.RegisterPostCommitSuccessAction(() => this.Bus.Value.PublishMessage(message).Wait());
                        }
                    }
                }
            }

        }

        public void SetBalanceTransferStatusActiveForVisit(string visitSourceSystemKey, int billingSystemId)
        {
            this._balanceTransferService.Value.SetBalanceTransferStatusActiveForVisit(visitSourceSystemKey, billingSystemId);
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(BalanceTransferSendFinancePlanPaymentSettledMessage message, ConsumeContext<BalanceTransferSendFinancePlanPaymentSettledMessage> consumeContext)
        {
            this.PublishBalanceTransferFinancePlanPaymentMessage(message.VpGuarantorId, message.PaymentId);
        }

        public bool IsMessageValid(BalanceTransferSendFinancePlanPaymentSettledMessage message, ConsumeContext<BalanceTransferSendFinancePlanPaymentSettledMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(BalanceTransferSetVisitActiveMessage message, ConsumeContext<BalanceTransferSetVisitActiveMessage> consumeContext)
        {
            this.SetBalanceTransferStatusActiveForVisit(message.VisitSourceSystemKey, message.VisitBillingSystemId);
        }

        public bool IsMessageValid(BalanceTransferSetVisitActiveMessage message, ConsumeContext<BalanceTransferSetVisitActiveMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion
    }
}
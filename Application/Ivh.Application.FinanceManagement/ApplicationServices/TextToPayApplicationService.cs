﻿namespace Ivh.Application.FinanceManagement.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.Email.Interfaces;
    using Domain.FinanceManagement.Entities;
    using Domain.FinanceManagement.Interfaces;
    using Domain.Guarantor.Interfaces;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Core;
    using MassTransit;

    public class TextToPayApplicationService : ApplicationService, ITextToPayApplicationService
    {

        private readonly Lazy<ICommunicationService> _communicationService;
        private readonly Lazy<IFinancialDataSummaryApplicationService> _financialDataSummaryApplicationService;
        private readonly Lazy<IPaymentMethodsService> _paymentMethodsService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IPaymentSubmissionApplicationService> _paymentSubmissionApplicationService;

        public TextToPayApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<ICommunicationService> communicationService,
            Lazy<IFinancialDataSummaryApplicationService> financialDataSummaryApplicationService,
            Lazy<IPaymentSubmissionApplicationService> paymentSubmissionApplicationService,
            Lazy<IPaymentMethodsService> paymentMethodsService,
            Lazy<IGuarantorService> guarantorService
        ) : base(applicationServiceCommonService)
        {
            this._communicationService = communicationService;
            this._financialDataSummaryApplicationService = financialDataSummaryApplicationService;
            this._paymentSubmissionApplicationService = paymentSubmissionApplicationService;
            this._paymentMethodsService = paymentMethodsService;
            this._guarantorService = guarantorService;
        }

        /// <summary>
        /// User responds "Pay" to a Pay by Text Message
        /// </summary>
        /// <param name="phoneNumber">expected format is +15554443333 (as it is received from twilio)</param>
        public void PayFromText(string phoneNumber)
        {
            try
            {
                string formattedPhoneNumber = FormatHelper.TwilioPhoneNumberToVisitPayUserPhoneNumberFormat(phoneNumber);
                int? visitPayUserId = this._communicationService.Value.GetVisitPayUserIdFromLatestTextToPayMessageByPhoneNumber(formattedPhoneNumber);

                if (visitPayUserId != null)
                {
                    this._paymentSubmissionApplicationService.Value.ChargeTextToPayPayment(visitPayUserId.Value);
                }
            }
            catch (Exception e)
            {
                this.LoggingService.Value.Fatal(() => $"{nameof(TextToPayApplicationService)}::{nameof(this.PayFromText)} failed with phone number {phoneNumber}, Exception = {ExceptionHelper.AggregateExceptionToString(e)}");
                throw;
            }
        }

        /// <summary>
        /// A trigger has determined that the user is a candidate to be sent
        /// a text message notifying them that a visit can be paid via text
        /// 
        /// This builds and publishes the SendTextToPayPaymentOptionMessage to be consumed by a communication/email service
        /// </summary>
        /// <param name="message"></param>
        public void SendTextToPayPaymentOptionMessage(TextToPayPaymentOptionMessage message)
        {
            if (!this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureTextToPayIsEnabled))
            {
                return;
            }

            int visitPayUserId = this._guarantorService.Value.GetVisitPayUserId(message.VpGuarantorId);
            if (!this._communicationService.Value.HasCommunicationPreference(visitPayUserId, CommunicationMethodEnum.Sms, CommunicationTypeEnum.TextToPayPaymentOption))
            {
                return;
            }

            PaymentMethod primaryPaymentMethod = this._paymentMethodsService.Value.GetPrimaryPaymentMethod(message.VpGuarantorId, false);
            if (primaryPaymentMethod == null)
            {
                return;
            }

            //TODO: this will need to be adjusted if TextToPayPaymentOptionMessage has a PaymentTypeId parameter
            FinancialDataSummaryDto financialDataSummaryDto = this._financialDataSummaryApplicationService.Value.GetFinancialDataSummaryByGuarantor(message.VpGuarantorId);
            List<VisitPaymentDto> visits = Mapper.Map<List<VisitPaymentDto>>(financialDataSummaryDto.SelectedVisits);
            decimal totalPayment = visits.Sum(x => x.PaymentAmount);
            if (totalPayment == 0m)
            {
                return;
            }

            PaymentSubmissionDto paymentSubmissionDto = new PaymentSubmissionDto
            {
                PaymentMethodId = primaryPaymentMethod.PaymentMethodId,
                //TODO: the payment type could be sent in via the message
                PaymentType = PaymentTypeEnum.ManualScheduledCurrentNonFinancedBalanceInFull,
                VisitPayments = visits,
                TotalPaymentAmount = visits.Sum(x => x.PaymentAmount),
                PaymentDate = DateTime.UtcNow
            };

            ProcessPaymentResponse processPaymentResponse = this._paymentSubmissionApplicationService.Value.SaveTextToPayPayment(paymentSubmissionDto, message.VpGuarantorId);
            if (!processPaymentResponse.IsError)
            {
                SendTextToPayPaymentOptionMessage sendTextToPayPaymentOptionMessage = new SendTextToPayPaymentOptionMessage
                {
                    VpGuarantorId = message.VpGuarantorId,
                    Description = "",//TODO. Figure out what to say here
                    Amount = financialDataSummaryDto.TotalBalance
                };
                this.Bus.Value.PublishMessage(sendTextToPayPaymentOptionMessage);
            }
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(ReceiveTextToPayPaymentOptionMessage message, ConsumeContext<ReceiveTextToPayPaymentOptionMessage> consumeContext)
        {
            this.PayFromText(message.PhoneNumber);
        }

        public bool IsMessageValid(ReceiveTextToPayPaymentOptionMessage message, ConsumeContext<ReceiveTextToPayPaymentOptionMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(TextToPayPaymentOptionMessage message, ConsumeContext<TextToPayPaymentOptionMessage> consumeContext)
        {
            this.SendTextToPayPaymentOptionMessage(message);
        }

        public bool IsMessageValid(TextToPayPaymentOptionMessage message, ConsumeContext<TextToPayPaymentOptionMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion
    }
}
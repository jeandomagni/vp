﻿namespace Ivh.Application.FinanceManagement.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.FinanceManagement.PaymentSummary.Entities;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;

    public class PaymentSummaryApplicationService : ApplicationService, IPaymentSummaryApplicationService
    {
        private readonly Lazy<IBaseVisitService> _paymentSummaryService;
        private readonly Lazy<IGuarantorService> _guarantorService;

        public PaymentSummaryApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IBaseVisitService> paymentSummaryService,
            Lazy<IGuarantorService> guarantorService
            ) : base(applicationServiceCommonService)
        {
            this._paymentSummaryService = paymentSummaryService;
            this._guarantorService = guarantorService;
        }

        public PaymentSummaryDto GetPaymentSummary(int vpGuarantorId, DateTime? postDateBegin, DateTime? postDateEnd)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            IList<int> hsGuarantorIds = guarantor.MatchedHsGuarantorMaps.Select(x => x.HsGuarantorId).ToList();

            IEnumerable<IGrouping<int, BaseVisit>> paymentSummaryVisits = this._paymentSummaryService.Value.GetBaseVisits(hsGuarantorIds).GroupBy(x => x.HsGuarantorId);
            List<PaymentSummaryTransaction> paymentSummaryTransactions = new List<PaymentSummaryTransaction>();

            // build dtos from matches, visits, and transactions
            foreach (IGrouping<int, BaseVisit> paymentSummaryVisitGroup in paymentSummaryVisits)
            {
                HsGuarantorMap hsGuarantorMap = guarantor.HsGuarantorMaps.FirstOrDefault(x => x.HsGuarantorId == paymentSummaryVisitGroup.Key);

                foreach (BaseVisit paymentSummaryVisit in paymentSummaryVisitGroup)
                {
                    IEnumerable<BaseVisitTransaction> paymentSummaryApplicableVisitTransactions = paymentSummaryVisit
                        .VisitTransactions
                        .Where(x => !postDateBegin.HasValue || x.PostDate >= postDateBegin.Value)
                        .Where(x => !postDateEnd.HasValue || x.PostDate <= postDateEnd.Value)
                        .Where(x => x.VpTransactionTypeId.IsInCategory(VpTransactionTypeEnumCategory.PaymentSummary));

                    foreach (BaseVisitTransaction paymentSummaryVisitTransaction in paymentSummaryApplicableVisitTransactions)
                    {
                        PaymentSummaryTransaction dto = new PaymentSummaryTransaction
                        {
                            // transaction
                            PostDate = paymentSummaryVisitTransaction.PostDate,
                            TransactionAmount = paymentSummaryVisitTransaction.TransactionAmount,
                            TransactionDescription = paymentSummaryVisitTransaction.TransactionDescription,

                            // visit
                            PatientFirstName = paymentSummaryVisit.PatientFirstName,
                            PatientLastName = paymentSummaryVisit.PatientLastName,
                            VisitBillingSystemId = paymentSummaryVisit.BillingSystemId,
                            VisitSourceSystemKey = paymentSummaryVisit.SourceSystemKey,
                            VisitSourceSystemKeyDisplay = paymentSummaryVisit.SourceSystemKeyDisplay ?? paymentSummaryVisit.SourceSystemKey,
                            VisitDate = paymentSummaryVisit.VisitDate,
                            VisitDescription = paymentSummaryVisit.VisitDescription,
                            IsPatientGuarantor = paymentSummaryVisit.IsPatientGuarantor,
                            IsPatientMinor = paymentSummaryVisit.IsPatientMinor,

                            // match
                            MatchOption =  hsGuarantorMap?.MatchOption
                        };

                        paymentSummaryTransactions.Add(dto);
                    }
                }
            }

            PaymentSummary paymentSummary = new PaymentSummary
            { 
                Payments = paymentSummaryTransactions,
                PostDateBegin = postDateBegin,
                PostDateEnd = postDateEnd,
                VpGuarantorId = vpGuarantorId
            };

            return Mapper.Map<PaymentSummaryDto>(paymentSummary);
        }
    }
}

﻿namespace Ivh.Application.FinanceManagement.ApplicationServices
{
    using System;
    using System.Threading.Tasks;
    using Base.Common.Interfaces;
    using Domain.FinanceManagement.Interfaces;
    using Domain.Guarantor.Entities;

    //This is intended only for consumption in application services, notice it returns a domain entity.
    //TODO: can this return non-domain object dtos and be moved to Ivh.Application.FinanceManagement.Common?
    public interface IStatementSnapshotCreationApplicationService : IApplicationService
    {
        Task<GeneratedStatementsResultDto> GenerateStatementForDateAsync(Guarantor guarantor, DateTime dateToProcess, bool isImmediateStatement);
    }
}
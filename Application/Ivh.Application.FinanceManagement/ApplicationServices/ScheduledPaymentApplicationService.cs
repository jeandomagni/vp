﻿namespace Ivh.Application.FinanceManagement.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Interfaces;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Statement.Entities;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Core;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;
    using NHibernate;
    using MassTransit;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.VisitPay.Jobs;

    public class ScheduledPaymentApplicationService : ApplicationService, IScheduledPaymentApplicationService
    {
        private readonly Lazy<IFinancePlanService> _financePlanService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IPaymentService> _paymentService;
        private readonly ISession _session;
        private readonly Lazy<IVpStatementService> _vpStatementService;

        public ScheduledPaymentApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            ISessionContext<VisitPay> sessionContext,
            Lazy<IVpStatementService> vpStatementService,
            Lazy<IPaymentService> paymentService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IFinancePlanService> financePlanService) : base(applicationServiceCommonService)
        {
            this._vpStatementService = vpStatementService;
            this._paymentService = paymentService;
            this._guarantorService = guarantorService;
            this._financePlanService = financePlanService;
            this._session = sessionContext.Session;
        }

        public void QueueScheduledPaymentsForGuarantorsChargedByDate(DateTime dateToProcess)
        {
            string methodName = nameof(this.QueueScheduledPaymentsForGuarantorsChargedByDate);

            List<GuarantorStatementInfo> statementsNeedingPayment = this._vpStatementService.Value.GetGuarantorStatementInfoToBeChargedWithDate(dateToProcess).ToList();
            List<FinancePlanAmountDueInfo> financePlansNeedingPayment = this._financePlanService.Value.GetFinancePlanAmountDueInfoWithRecurringPaymentDue(dateToProcess).ToList();
            List<int> guarantorsWithPayments = this._paymentService.Value.GetGuarantorIdsWithPaymentsScheduledFor(dateToProcess).Distinct().ToList();

            List<KeyValuePair<int, string>> discardedGuarantors = new List<KeyValuePair<int, string>>();
            HashSet<int> published = new HashSet<int>();

            this.ProcessStatementsNeedingPayment(statementsNeedingPayment, dateToProcess, published, discardedGuarantors);
            this.ProcessFinancePlansNeedingPayment(financePlansNeedingPayment, dateToProcess, published, discardedGuarantors);
            this.ProcessGuarantorScheduledPayments(guarantorsWithPayments, dateToProcess, published, discardedGuarantors);

            // Log the guarantor IDs so that it is easier to determine why a scheduled payment might have failed to be published
            this.LoggingService.Value.Info(() => $"{methodName}: financePlansNeedingPayment for {dateToProcess} {financePlansNeedingPayment.ToJSON(false, false, true)}");
            this.LoggingService.Value.Info(() => $"{methodName}: statementsNeedingPayment for {dateToProcess} {statementsNeedingPayment.ToJSON(false, false, true)}");
            this.LoggingService.Value.Info(() => $"{methodName}: guarantorsWithPayments for {dateToProcess} {guarantorsWithPayments.ToJSON(false, false, true)}");
            this.LoggingService.Value.Info(() => $"{methodName}: published for {dateToProcess} {published.ToJSON(false, false, true)}");
            this.LoggingService.Value.Info(() => $"{methodName}: discardedGuarantors for {dateToProcess} {discardedGuarantors.ToJSON(true, false, true)}");
        }

        public void ProcessScheduledPayment(int vpGuarantorId, DateTime dateToProcess, int? vpStatementId = null, int? visitPayUserId = null)
        {
            string methodName = nameof(this.ProcessScheduledPayment);

            if (vpGuarantorId <= 0)
            {
                this.LoggingService.Value.Info(() => $"{methodName} - Invalid vpGuarantorId argument VPGID: {vpGuarantorId}");
                return;
            }

            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);

            if (guarantor.IsClosed)
            {
                this.LoggingService.Value.Info(() => $"{methodName} - Guarantor account is closed VPGID: {vpGuarantorId}");
                return;
            }

            VpStatementForProcessPaymentResult vpStatement = this.GetVpStatementForProcessPaymentResult(guarantor, vpStatementId);
            this.CancelPendingFinancePlans(guarantor, dateToProcess, vpStatement);

            // Must process Unsent Recurring Payments first so we don't end up double processing a Recurring Payment 
            // that just failed due to Active Gateway error
            IList<Payment> unsentRecurringPayments = this.ProcessUnsentRecurringPayments(guarantor, dateToProcess, visitPayUserId);

            IList<Payment> recurringPayments = this.ProcessRecurringPayments(guarantor, dateToProcess, vpStatement, visitPayUserId);
            IList<Payment> manualScheduledPayments = this.ProcessManualScheduledPayments(guarantor, dateToProcess, visitPayUserId);          

            IList <Payment> scheduledPayments = manualScheduledPayments.Concat(unsentRecurringPayments).ToList();

            this.PostProcessPayments(guarantor, dateToProcess, vpStatement, recurringPayments, scheduledPayments);
        }

        public void QueueScheduledPaymentReminders(DateTime dateToProcess)
        {
            DateTime scheduledPaymentDate = dateToProcess.AddDays(this.Client.Value.PendingPaymentReminderDays).Date;

            List<SendPendingPaymentEmailMessage> messages = this.GetScheduledPaymentRemindersToQueue(scheduledPaymentDate).ToList();

            if (messages.Any())
            {
                // publish messages
                this._session.Transaction.RegisterPostCommitSuccessAction(messageList =>
                {
                    foreach (SendPendingPaymentEmailMessage message in messageList)
                    {
                        this.Bus.Value.PublishMessage(message).Wait();
                    }
                }, messages);
            }
        }

        public IEnumerable<SendPendingPaymentEmailMessage> GetScheduledPaymentRemindersToQueue(DateTime scheduledPaymentDate)
        {
            // scheduled non-recurring payments
            IEnumerable<Payment> scheduledPayments = this._paymentService.Value.GetScheduledPaymentsWithExactDate(scheduledPaymentDate.Date);
            IEnumerable<SendPendingPaymentEmailMessage> paymentMessages = this.CreatePendingPaymentEmailMessagesForPayments(scheduledPayments);

            // scheduled recurring payments
            IList<FinancePlanAmountDueInfo> financePlansNeedingPayment = this._financePlanService.Value.GetFinancePlanAmountDueInfoWithRecurringPaymentDueExactDate(scheduledPaymentDate.Date);
            IEnumerable<SendPendingPaymentEmailMessage> financePlanMessages = this.CreatePendingPaymentEmailMessagesForFinancePlans(financePlansNeedingPayment);

            // gruop by guarantor
            IEnumerable<SendPendingPaymentEmailMessage> messages = paymentMessages.Union(financePlanMessages)
                .Where(x => x.PaymentDate.Date == scheduledPaymentDate.Date) // ensure we're only sending for "checkDate"
                .GroupBy(x => x.VpGuarantorId)
                .Select(x => x.First());

            return messages;
        }

        #region Private Methods

        private void ProcessStatementsNeedingPayment(List<GuarantorStatementInfo> statementsNeedingPayment, DateTime dateToProcess, HashSet<int> published, List<KeyValuePair<int, string>> discardedGuarantors)
        {
            string methodName = nameof(this.ProcessStatementsNeedingPayment);

            foreach (GuarantorStatementInfo statementNeedingPayment in statementsNeedingPayment)
            {
                if (statementNeedingPayment.VpStatementId != null)
                {
                    using (UnitOfWork uow = new UnitOfWork(this._session))
                    {
                        string logMessage = $"{methodName} - Statement Needing Payment On {dateToProcess.ToShortDateString()}";
                        this._vpStatementService.Value.MarkStatementAsQueued(statementNeedingPayment.VpStatementId ?? 0, statementNeedingPayment.VpGuarantorId, logMessage);
                        this._session.Transaction.RegisterPostCommitSuccessAction((vpGuarantorId, vpStatementId, processDate, publishedGuarantorIds) =>
                        {
                            this.Bus.Value.PublishMessage(new ProcessScheduledPaymentWithGuarantorMessage { VpGuarantorId = vpGuarantorId, VpStatementId = vpStatementId, DateToProcess = processDate }).Wait();
                            publishedGuarantorIds.Add(vpGuarantorId);
                        }, statementNeedingPayment.VpGuarantorId, statementNeedingPayment.VpStatementId, dateToProcess, published);
                        uow.Commit();
                    }
                }
                else
                {
                    discardedGuarantors.Add(new KeyValuePair<int, string>(statementNeedingPayment.VpGuarantorId, "statementNeedingPayment - Did not find a statement"));
                }
            }
        }

        private void ProcessFinancePlansNeedingPayment(List<FinancePlanAmountDueInfo> financePlansNeedingPayment, DateTime dateToProcess, HashSet<int> published, List<KeyValuePair<int, string>> discardedGuarantors)
        {
            string methodName = nameof(this.ProcessFinancePlansNeedingPayment);

            List<int> guarantorsWithPaymentsDueForFinancePlans = financePlansNeedingPayment.Select(x => x.VpGuarantorId).Distinct().ToList();

            foreach (int guarantorIdWithRecurringPaymentDue in guarantorsWithPaymentsDueForFinancePlans.Distinct())
            {
                if (!published.Contains(guarantorIdWithRecurringPaymentDue))
                {
                    // Guarantor should have all finance plans on the same statement
                    // Only one row should ever be returned
                    IEnumerable<int?> statementIds = financePlansNeedingPayment
                        .Where(x => x.VpGuarantorId == guarantorIdWithRecurringPaymentDue)
                        .Select(x => x.StatementId)
                        .Distinct()
                        .ToList();

                    if (statementIds.Count() > 1)
                    {
                        this.LoggingService.Value.Warn(() => $"{methodName} - More than one statement found in FinancePlanAmountDue for VPGID: {guarantorIdWithRecurringPaymentDue}, StatementIds: {string.Join(", ", statementIds)} for date {dateToProcess.ToShortDateString()}");
                    }

                    int? vpStatementId = statementIds
                        .OrderByDescending(x => x)
                        .FirstOrDefault();

                    this.Bus.Value.PublishMessage(new ProcessScheduledPaymentWithGuarantorMessage { VpGuarantorId = guarantorIdWithRecurringPaymentDue, VpStatementId = vpStatementId, DateToProcess = dateToProcess }).Wait();
                    published.Add(guarantorIdWithRecurringPaymentDue);
                }
                else
                {
                    discardedGuarantors.Add(new KeyValuePair<int, string>(guarantorIdWithRecurringPaymentDue, "guarantorsWithPaymentsDueForFinancePlans - ProcessScheduledPaymentWithGuarantorMessage already published"));
                }
            }

            this.LoggingService.Value.Info(() => $"{methodName}: guarantorsWithPaymentsDueForFinancePlans for {dateToProcess} {guarantorsWithPaymentsDueForFinancePlans.ToJSON(false, false, true)}");
        }

        private void ProcessGuarantorScheduledPayments(List<int> guarantorsWithPayments, DateTime dateToProcess, HashSet<int> published, List<KeyValuePair<int, string>> discardedGuarantors)
        {
            foreach (int guarantorWithPayment in guarantorsWithPayments.Distinct())
            {
                if (!published.Contains(guarantorWithPayment))
                {
                    this.Bus.Value.PublishMessage(new ProcessScheduledPaymentWithGuarantorMessage { VpGuarantorId = guarantorWithPayment, DateToProcess = dateToProcess }).Wait();
                    published.Add(guarantorWithPayment);
                }
                else
                {
                    discardedGuarantors.Add(new KeyValuePair<int, string>(guarantorWithPayment, "guarantorsWithPayments - ProcessScheduledPaymentWithGuarantorMessage already published"));
                }
            }
        }

        private void SendSystemExceptionMessage(int vpGuarantorId, string message, SystemExceptionTypeEnum systemExceptionType = SystemExceptionTypeEnum.PaymentProcessingException)
        {
            this.Bus.Value.PublishMessage(
                new SystemExceptionMessage
                {
                    SystemExceptionType = systemExceptionType,
                    VpGuarantorId = vpGuarantorId,
                    SystemExceptionDescription = message,
                }).Wait();
        }

        /// <summary>
        /// Gets most recent statement unless vpStatementId is provided
        /// </summary>
        /// <param name="guarantor"></param>
        /// <param name="vpStatementId"></param>
        /// <returns></returns>
        private VpStatementForProcessPaymentResult GetVpStatementForProcessPaymentResult(Guarantor guarantor, int? vpStatementId)
        {
            VpStatementForProcessPaymentResult vpStatementForProcessPaymentResult = null;
            if (vpStatementId.HasValue)
            {
                vpStatementForProcessPaymentResult = this._vpStatementService.Value.GetVpStatementForProcessPaymentResult(guarantor.VpGuarantorId, vpStatementId.Value);
            }
            else
            {
                vpStatementForProcessPaymentResult = this._vpStatementService.Value.GetMostRecentStatementForProcessPayment(guarantor);
            }

            return vpStatementForProcessPaymentResult;
        }

        /// <summary>
        /// Cancels any pending acceptance finance plans
        /// </summary>
        /// <param name="guarantor"></param>
        /// <param name="dateToProcess"></param>
        /// <param name="vpStatement"></param>
        private void CancelPendingFinancePlans(Guarantor guarantor, DateTime dateToProcess, VpStatementForProcessPaymentResult vpStatement)
        {
            string methodName = nameof(this.CancelPendingFinancePlans);

            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                try
                {
                    this._financePlanService.Value.CancelPendingTermsAcceptanceFinancePlansForGuarantor(guarantor, vpStatement.PaymentDueDate, dateToProcess);
                    uow.Commit();
                }
                catch (Exception e)
                {
                    this.LoggingService.Value.Fatal(() => $"{methodName} Step: CancelPendingTermsAcceptanceFinancePlansForGuarantor - VPGID: {guarantor.VpGuarantorId} Exception: {ExceptionHelper.AggregateExceptionToString(e)}");
                    uow.Rollback();
                }
            }
        }

        private void PostProcessPayments(Guarantor guarantor, DateTime dateToProcess, VpStatementForProcessPaymentResult vpStatement, IList<Payment> recurringPayments, IList<Payment> scheduledPayments)
        {
            string methodName = nameof(this.PostProcessPayments);

            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                try
                {
                    this._paymentService.Value.PostProcessPayment(guarantor, vpStatement, dateToProcess, scheduledPayments, recurringPayments);
                    uow.Commit();
                }
                catch (Exception e)
                {
                    bool hasStatement = vpStatement != null;
                    this.LoggingService.Value.Fatal(() => $"{methodName} Step: PostScheduledPaymentProcess - VPGID: {guarantor.VpGuarantorId}, VpStatementId: {(hasStatement ? vpStatement?.VpStatementId : 0)} using the date {dateToProcess.ToShortDateString()} Exception: {ExceptionHelper.AggregateExceptionToString(e)}");
                    uow.Rollback();
                }
            }
        }

        private IList<Payment> CreateRecurringPayments(Guarantor guarantor, DateTime dateToProcess, VpStatementForProcessPaymentResult vpStatement, IList<FinancePlan> financePlans, int? visitPayUserId = null)
        {
            string methodName = nameof(this.CreateRecurringPayments);

            IList<Payment> newRecurringPayments = new List<Payment>();
            foreach (FinancePlan financePlan in financePlans)
            {
                if (financePlan.VpGuarantor.IsOfflineGuarantor() && !financePlan.VpGuarantor.UseAutoPay)
                {
                    // secondary check for criteria in ProcessRecurringPayments - shouldn't see these.
                    this.LoggingService.Value.Warn(() => $"{methodName} - VPGID: {guarantor.VpGuarantorId} tried to create a recurring payment for a non-autopay guarantor, FinancePlanId = {financePlan.FinancePlanId}");
                    continue;
                }

                bool hasLaterRescheduledPayment = financePlan.HasRescheduledPaymentLaterThan(dateToProcess);
                if (hasLaterRescheduledPayment)
                {
                    this.LoggingService.Value.Info(() => $"{methodName} - VPGID: {guarantor.VpGuarantorId} Payment skipped (rescheduled), FinancePlanId = {financePlan.FinancePlanId}");
                    this._financePlanService.Value.CheckIfFinancePlansAreStillPastDueOrUncollectable(financePlan.ToListOfOne());
                }
                else
                {
                    Payment payment = this._paymentService.Value.CreateRecurringPayment(financePlan.FinancePlanId, guarantor, financePlan.AmountDueAsOf(dateToProcess));
                    if (payment != null)
                    {
                        newRecurringPayments.Add(payment);
                    }
                    else
                    {
                        this.LoggingService.Value.Warn(() => $"{methodName} - Recurring payment was not made for VPGID: {guarantor.VpGuarantorId}, VpStatementId: {vpStatement.VpStatementId} using the date {dateToProcess.ToShortDateString()}");
                    }
                }
            }

            return newRecurringPayments;
        }

        // public for unit testing
        public IList<Payment> ProcessRecurringPayments(Guarantor guarantor, DateTime dateToProcess, VpStatementForProcessPaymentResult vpStatement, int? visitPayUserId = null)
        {
            const string methodName = nameof(this.ProcessRecurringPayments);
            const string nonAutoPayOfflineSkipped = "Offline, Non-AutoPay";

            IList<Payment> recurringPaymentsProcessed = null;

            bool hasStatement = vpStatement != null;
            bool hasQueuedStatement = vpStatement != null && vpStatement.ProcessingStatus == ChangeEventStatusEnum.Queued;

            if (hasQueuedStatement)
            {
                if (guarantor.IsOfflineGuarantor() && !guarantor.UseAutoPay)
                {
                    // don't want to process these, as the guarantor has specified non-autopay
                    using (UnitOfWork uow = new UnitOfWork(this._session))
                    {
                        this._vpStatementService.Value.MarkStatementAsProcessed(vpStatement.VpStatementId, guarantor.VpGuarantorId, nonAutoPayOfflineSkipped);
                        this.LoggingService.Value.Info(() => $"{methodName} - Marked statement as processed without creating recurring payment for offline/non-autopay VPGID: {guarantor.VpGuarantorId}, VpStatementId: {vpStatement.VpStatementId} using the date {dateToProcess.ToShortDateString()}");
                        uow.Commit();
                    }
                }
                else
                {
                    // Uses same repo logic that was used by QueueScheduledPaymentsForGuarantorsChargedByDate()
                    IList<FinancePlan> financePlans = this._financePlanService.Value.GetFinancePlansDueForRecurringPaymentByGuarantor(dateToProcess, guarantor.VpGuarantorId);
                    bool hasFinancePlans = financePlans.IsNotNullOrEmpty();
                    if (hasFinancePlans)
                    {
                        using (UnitOfWork uow = new UnitOfWork(this._session))
                        {
                            IList<Payment> newRecurringPayments = this.CreateRecurringPayments(guarantor, dateToProcess, vpStatement, financePlans, visitPayUserId);
                            try
                            {
                                recurringPaymentsProcessed = this._paymentService.Value.ProcessRecurringPaymentsForGuarantor(newRecurringPayments, guarantor, dateToProcess, visitPayUserId);
                                uow.Commit();
                            }
                            catch (Exception e)
                            {
                                this.SendSystemExceptionMessage(guarantor.VpGuarantorId, "System exception during recurring payments");
                                this.LoggingService.Value.Fatal(() => $"{methodName} - VPGID: {guarantor.VpGuarantorId}, VpStatementId: {vpStatement.VpStatementId} Exception: {ExceptionHelper.AggregateExceptionToString(e)}");
                                uow.Rollback();
                            }
                        }
                    }
                    else
                    {
                        this.LoggingService.Value.Warn(() => $"{methodName} - Did not find any finance plans with an amount due for VPGID: {guarantor.VpGuarantorId}, VpStatementId: {vpStatement.VpStatementId} using the date {dateToProcess.ToShortDateString()}");
                    }
                }
            }
            else
            {
                this.LoggingService.Value.Warn(() => $"{methodName} - Statement did not have Queued Processing Status for VPGID: {guarantor.VpGuarantorId}, VpStatementId: {(hasStatement ? vpStatement?.VpStatementId : 0)} using the date {dateToProcess.ToShortDateString()}");
            }

            return recurringPaymentsProcessed;
        }

        private IList<Payment> ProcessManualScheduledPayments(Guarantor guarantor, DateTime dateToProcess, int? visitPayUserId = null)
        {
            string methodName = nameof(this.ProcessManualScheduledPayments);

            IList<Payment> scheduledPaymentsProcessed = null;
            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                try
                {
                    scheduledPaymentsProcessed = this._paymentService.Value.ProcessScheduledPaymentsForGuarantor(guarantor, dateToProcess, visitPayUserId);
                    uow.Commit();
                }
                catch (Exception e)
                {
                    this.SendSystemExceptionMessage(guarantor.VpGuarantorId, "System exception during scheduled manual payments");
                    this.LoggingService.Value.Fatal(() => $"{methodName} - VPGID: {guarantor.VpGuarantorId} Exception: {ExceptionHelper.AggregateExceptionToString(e)}");
                    uow.Rollback();
                }
            }

            return scheduledPaymentsProcessed;
        }

        private IList<Payment> ProcessUnsentRecurringPayments(Guarantor guarantor, DateTime dateToProcess, int? visitPayUserId = null)
        {
            string methodName = nameof(this.ProcessUnsentRecurringPayments);

            IList<Payment> unsentRecurringPaymentsProcessed = null;
            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                try
                {
                    unsentRecurringPaymentsProcessed = this._paymentService.Value.ProcessUnsentRecurringPaymentsForGuarantor(guarantor, dateToProcess, visitPayUserId);
                    uow.Commit();
                }
                catch (Exception e)
                {
                    this.SendSystemExceptionMessage(guarantor.VpGuarantorId, "System exception during unsent recurring payments");
                    this.LoggingService.Value.Fatal(() => $"{methodName} - VPGID: {guarantor.VpGuarantorId} Exception: {ExceptionHelper.AggregateExceptionToString(e)}");
                    uow.Rollback();
                }
            }

            return unsentRecurringPaymentsProcessed;
        }

        private int? GetGuarantorIdForScheduledPaymentReminder(int paymentVpGuarantorId)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(paymentVpGuarantorId);
            if (guarantor == null)
            {
                return null;
            }

            if (guarantor.ConsolidationStatus == GuarantorConsolidationStatusEnum.Managed)
            {
                return guarantor.ManagingGuarantorId ?? guarantor.VpGuarantorId;
            }

            return guarantor.VpGuarantorId;
        }

        private IEnumerable<SendPendingPaymentEmailMessage> CreatePendingPaymentEmailMessagesForPayments(IEnumerable<Payment> scheduledPayments)
        {
            List<SendPendingPaymentEmailMessage> messages = new List<SendPendingPaymentEmailMessage>();

            foreach (Payment payment in scheduledPayments)
            {
                int? vpGuarantorId = this.GetGuarantorIdForScheduledPaymentReminder(payment.Guarantor.VpGuarantorId);
                if (vpGuarantorId == null)
                {
                    continue;
                }

                messages.Add(new SendPendingPaymentEmailMessage
                {
                    VpGuarantorId = vpGuarantorId.Value,
                    PaymentDate = payment.ScheduledPaymentDate
                });
            }

            return messages;
        }

        private IEnumerable<SendPendingPaymentEmailMessage> CreatePendingPaymentEmailMessagesForFinancePlans(IEnumerable<FinancePlanAmountDueInfo> financePlansNeedingPayment)
        {
            List<SendPendingPaymentEmailMessage> messages = new List<SendPendingPaymentEmailMessage>();

            foreach (FinancePlanAmountDueInfo financePlan in financePlansNeedingPayment)
            {
                int? vpGuarantorId = this.GetGuarantorIdForScheduledPaymentReminder(financePlan.VpGuarantorId);
                if (vpGuarantorId == null)
                {
                    continue;
                }

                if (!financePlan.StatementFinancePlanAmountDueDueDate.HasValue)
                {
                    continue;
                }

                if (financePlan.TotalAmountDue <= 0m)
                {
                    continue;
                }

                messages.Add(new SendPendingPaymentEmailMessage
                {
                    VpGuarantorId = vpGuarantorId.Value,
                    PaymentDate = financePlan.StatementFinancePlanAmountDueDueDate.Value
                });
            }

            return messages;
        }

        #endregion

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority10)]
        public void ConsumeMessage(FinancePlanPaymentAmountChangedMessage message, ConsumeContext<FinancePlanPaymentAmountChangedMessage> consumeContext)
        {
            // EMPTY CONSUMER. REMOVED WITH VP-252. We should consider not publishing this message at all.
        }

        public bool IsMessageValid(FinancePlanPaymentAmountChangedMessage message, ConsumeContext<FinancePlanPaymentAmountChangedMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(ProcessScheduledPaymentWithGuarantorMessage message, ConsumeContext<ProcessScheduledPaymentWithGuarantorMessage> consumeContext)
        {
            this.ProcessScheduledPayment(message.VpGuarantorId, message.DateToProcess, message.VpStatementId);
        }

        public bool IsMessageValid(ProcessScheduledPaymentWithGuarantorMessage message, ConsumeContext<ProcessScheduledPaymentWithGuarantorMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion

        #region Jobs

        void IJobRunnerService<QueueScheduledPaymentRemindersJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.QueueScheduledPaymentReminders(DateTime.Today);
        }

        void IJobRunnerService<QueueScheduledPaymentsByGuarantorJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.QueueScheduledPaymentsForGuarantorsChargedByDate(end);
        }

        #endregion
    }
}
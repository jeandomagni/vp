﻿namespace Ivh.Application.FinanceManagement.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Core.Common.Dtos;
    using Core.Common.Utilities.Builders;
    using Domain.Content.Interfaces;
    using Domain.FinanceManagement.Entities;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Interfaces;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Payment.Utilities;
    using Domain.FinanceManagement.ValueTypes;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Strings;
    using OfficeOpenXml;

    public class PaymentDetailApplicationService : ApplicationService, IPaymentDetailApplicationService
    {
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IFinancePlanService> _financePlanService;
        private readonly Lazy<IContentService> _contentService;
        private readonly Lazy<IPaymentService> _paymentService;
        private readonly Lazy<IPaymentEventService> _paymentEventService;
        private readonly Lazy<IPaymentMethodsService> _paymentMethodsService;
        private readonly Lazy<IImageService> _imageService;

        public PaymentDetailApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IPaymentService> paymentService,
            Lazy<IPaymentEventService> paymentEventService,
            Lazy<IPaymentMethodsService> paymentMethodsService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IFinancePlanService> financePlanService,
            Lazy<IContentService> contentService,
            Lazy<IImageService> imageService) : base(applicationServiceCommonService)
        {
            this._paymentService = paymentService;
            this._paymentEventService = paymentEventService;
            this._paymentMethodsService = paymentMethodsService;
            this._guarantorService = guarantorService;
            this._financePlanService = financePlanService;
            this._contentService = contentService;
            this._imageService = imageService;
        }


        #region scheduled payments

        public ScheduledPaymentDto GetScheduledManualPayment(int vpGuarantorId, int paymentId)
        {
            ScheduledPaymentResult scheduledPaymentResult = this._paymentService.Value.GetScheduledPaymentResult(vpGuarantorId, paymentId);
            ScheduledPaymentDto scheduledPaymentDto =  this.GetScheduledPaymentDto(scheduledPaymentResult);

            return scheduledPaymentDto;
        }

        public ScheduledPaymentDto GetScheduledRecurringPayment(int vpGuarantorId)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            ScheduledPaymentResult scheduledPaymentResult = this._financePlanService.Value.GetFinancePlanScheduledPayments(guarantor).FirstOrDefault();
            ScheduledPaymentDto scheduledPaymentDto =  this.GetScheduledPaymentDto(scheduledPaymentResult);
            
            return scheduledPaymentDto;
        }
        
        public IList<ScheduledPaymentDto> GetScheduledPayments(int vpGuarantorId)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            if (guarantor == null)
            {
                return new List<ScheduledPaymentDto>(0);
            }

            IReadOnlyList<ScheduledPaymentResult> manualPayments = this._paymentService.Value.GetScheduledPaymentResults(vpGuarantorId);
            IReadOnlyList<ScheduledPaymentResult> financePlanPayments = this._financePlanService.Value.GetFinancePlanScheduledPayments(guarantor);
            List<ScheduledPaymentResult> payments = manualPayments.Union(financePlanPayments).OrderBy(x => x.ScheduledPaymentDate).ToList();

            if (payments.IsNullOrEmpty())
            {
                return new List<ScheduledPaymentDto>(0);
            }
            
            List<ScheduledPaymentDto> scheduledPaymentDtos = new List<ScheduledPaymentDto>(payments.Count);
            scheduledPaymentDtos.AddRange(payments.Select(this.GetScheduledPaymentDto));
            
            return scheduledPaymentDtos;
        }
        
        public bool HasScheduledPayments(int vpGuarantorId)
        {
            int count = this._paymentService.Value.GetScheduledPaymentsCountByGuarantorPaymentMethod(vpGuarantorId);
            return count > 0;
        }

        private ScheduledPaymentDto GetScheduledPaymentDto(ScheduledPaymentResult scheduledPaymentResult)
        {
            if (scheduledPaymentResult == null)
            {
                return null;
            }

            GuarantorDto madeByGuarantorDto = null;
            if (scheduledPaymentResult.MadeByVpGuarantorId.HasValue)
            {
                madeByGuarantorDto = Mapper.Map<GuarantorDto>(this._guarantorService.Value.GetGuarantor(scheduledPaymentResult.MadeByVpGuarantorId.Value));
            }

            GuarantorDto madeForGuarantorDto = Mapper.Map<GuarantorDto>(this._guarantorService.Value.GetGuarantor(scheduledPaymentResult.MadeForVpGuarantorId));

            ScheduledPaymentDto scheduledPaymentDto = new ScheduledPaymentDto
            {
                PaymentId = scheduledPaymentResult.PaymentId,
                PaymentType = scheduledPaymentResult.PaymentType,
                ScheduledPaymentAmount = scheduledPaymentResult.ScheduledPaymentAmount,
                ScheduledPaymentDate = scheduledPaymentResult.ScheduledPaymentDate,
                MadeByGuarantor = madeByGuarantorDto,
                MadeForGuarantor = madeForGuarantorDto
            };

            if (scheduledPaymentResult.PaymentType == PaymentTypeEnum.RecurringPaymentFinancePlan)
            {
                if (scheduledPaymentResult.MadeByVpGuarantorId.HasValue)
                {
                    PaymentMethod primaryPaymentMethod = this._paymentMethodsService.Value.GetPrimaryPaymentMethod(scheduledPaymentResult.MadeByVpGuarantorId.Value, false);
                    scheduledPaymentDto.PaymentMethod = Mapper.Map<PaymentMethodDto>(primaryPaymentMethod);
                }
            }
            else if (scheduledPaymentResult.PaymentMethodId.HasValue)
            {
                PaymentMethod primaryPaymentMethod = this._paymentMethodsService.Value.GetById(scheduledPaymentResult.PaymentMethodId.Value);
                scheduledPaymentDto.PaymentMethod = Mapper.Map<PaymentMethodDto>(primaryPaymentMethod);
            }

            return scheduledPaymentDto;
        }

        #endregion

        #region payment history

        public PaymentHistoryResultsDto GetPaymentHistory(int madeForVpGuarantorId, int currentVpGuarantorId, PaymentProcessorResponseFilterDto filterDto, int pageNumber, int pageSize)
        {
            IList<int> madeForVpGuarantorIds = new List<int> { madeForVpGuarantorId };
            int? madeByVpGuarantorId = currentVpGuarantorId;

            Guarantor currentGuarantor = this._guarantorService.Value.GetGuarantor(currentVpGuarantorId);

            PaymentProcessorResponseFilter filter = Mapper.Map<PaymentProcessorResponseFilter>(filterDto);

            filter.MadeByVpGuarantorId = currentGuarantor.ConsolidationStatus == GuarantorConsolidationStatusEnum.Managing ? madeByVpGuarantorId : null;

            if (currentVpGuarantorId == madeForVpGuarantorId)
            {
                // if looking for current guarantor, also include any previously canceled managed consolidations
                if (currentGuarantor.ManagedConsolidationGuarantors.IsNotNullOrEmpty())
                {
                    List<int> previouslyConsolidatedGuarantors = currentGuarantor.ManagedConsolidationGuarantors.Where(x => x.ConsolidationGuarantorStatus == ConsolidationGuarantorStatusEnum.Cancelled).Select(x => x.ManagedGuarantor.VpGuarantorId).ToList();
                    if (previouslyConsolidatedGuarantors.IsNotNullOrEmpty())
                    {
                        madeForVpGuarantorIds.AddRange(previouslyConsolidatedGuarantors);
                    }
                }

                madeForVpGuarantorIds.Add(currentGuarantor.VpGuarantorId);
            }

            filter.VpGuarantorId = madeForVpGuarantorIds;

            return this.GetPaymentHistory(filter, pageNumber, pageSize);
        }

        private PaymentHistoryResultsDto GetPaymentHistory(PaymentProcessorResponseFilter filter, int pageNumber, int pageSize)
        {
            IReadOnlyList<PaymentProcessorResponse> responses = this._paymentService.Value.GetPaymentHistory(filter);

            IList<PaymentHistoryDto> paymentHistories = new List<PaymentHistoryDto>();
            IList<PaymentStatus> paymentStatuses = this._paymentService.Value.GetPaymentStatuses();

            IDictionary<int,GuarantorDto> guarantorDtos = new Dictionary<int, GuarantorDto>();

            foreach (PaymentProcessorResponse response in responses)
            {
                IList<Payment> payments = response.Payments.ToList();
                decimal snapshotPaymentAmount = response.SnapshotTotalPaymentAmount;
                
                PaymentHistoryDto paymentHistoryDto = Mapper.Map<PaymentHistoryDto>(payments);
                paymentHistoryDto.InsertDate = response.InsertDate;
                paymentHistoryDto.PaymentProcessorResponseId = response.PaymentProcessorResponseId;
                paymentHistoryDto.PaymentProcessorSourceKey = response.PaymentProcessorSourceKey;
                paymentHistoryDto.PaymentMethod = Mapper.Map<PaymentMethodDto>(payments.First().IsReversalPaymentType() ? payments.First().ParentPayment.PaymentMethod : response.PaymentMethod);
                paymentHistoryDto.PaymentStatus = (paymentHistoryDto.PaymentStatus == PaymentStatusEnum.ActivePending || paymentHistoryDto.PaymentStatus == PaymentStatusEnum.ActivePendingGuarantorAction) ? PaymentStatusEnum.ClosedFailed : paymentHistoryDto.PaymentStatus; // recurring payments that were in resubmit, but have since expired.  show as failed instead of pending.
                paymentHistoryDto.PaymentStatusMessage = Utility.GetStatusMessage(response, paymentHistoryDto.PaymentStatus);
                paymentHistoryDto.PaymentStatusDisplayName = paymentStatuses.First(x => x.PaymentStatusEnum == paymentHistoryDto.PaymentStatus).PaymentStatusDisplayName;
                paymentHistoryDto.SnapshotTotalPaymentAmount = snapshotPaymentAmount;

                if (paymentHistoryDto.PaymentMethod?.VpGuarantorId != null)
                {
                    int madeByVpGuarantorId = paymentHistoryDto.PaymentMethod.VpGuarantorId.Value;
                    GuarantorDto guarantorDto;
                    if (!guarantorDtos.TryGetValue(madeByVpGuarantorId, out guarantorDto))
                    {
                        guarantorDto = Mapper.Map<GuarantorDto>(this._guarantorService.Value.GetGuarantor(madeByVpGuarantorId));
                        guarantorDtos.Add(madeByVpGuarantorId, guarantorDto);
                    }

                    paymentHistoryDto.MadeByGuarantorDto = guarantorDto;
                }
                else
                {
                    paymentHistoryDto.MadeByGuarantorDto = paymentHistoryDto.GuarantorDto;
                }

                paymentHistories.Add(paymentHistoryDto);
            }

            foreach (PaymentHistoryDto paymentHistoryDto in paymentHistories)
            {
                paymentHistoryDto.IsPreviousConsolidation = paymentHistoryDto.GuarantorDto.ConsolidationStatus == GuarantorConsolidationStatusEnum.NotConsolidated && paymentHistoryDto.MadeByGuarantorDto.VpGuarantorId != paymentHistoryDto.GuarantorDto.VpGuarantorId;
            }

            PaymentHistoryResultsDto results = new PaymentHistoryResultsDto
            {
                Payments = SortPaymentHistory(paymentHistories, filter).Skip(Math.Max(pageSize*(pageNumber - 1), 0)).Take(pageSize).ToList(),
                TotalRecords = paymentHistories.Count
            };

            return results;
        }
        
        private static IList<PaymentHistoryDto> SortPaymentHistory(IEnumerable<PaymentHistoryDto> paymentHistoryDtos, PaymentProcessorResponseFilter filter)
        {
            Func<PaymentHistoryDto, object> orderByFunc = payment => payment.InsertDate;

            switch (filter.SortField ?? string.Empty)
            {
                case "IdDisplay":
                {
                    orderByFunc = payment => payment.ParentPaymentProcessorResponseId ?? payment.PaymentProcessorResponseId;
                    break;
                }
                case "NetPaymentAmount":
                {
                    orderByFunc = payment => payment.NetPaymentAmount;
                    break;
                }
                case "PaymentMethod":
                {
                    orderByFunc = payment => payment.PaymentMethod == null ? "" : $"{(string.IsNullOrEmpty(payment.PaymentMethod.AccountNickName) ? payment.PaymentMethod.PaymentMethodType.ToString() : payment.PaymentMethod.AccountNickName)}{payment.PaymentMethod.LastFour}";
                    break;
                }
                case "PaymentStatusDisplayName":
                {
                    orderByFunc = payment => payment.PaymentStatusDisplayName;
                    break;
                }
                case "PaymentType":
                {
                    orderByFunc = payment => payment.PaymentType.ToString();
                    break;
                }
                case "SnapshotTotalPaymentAmount":
                {
                    orderByFunc = payment => payment.SnapshotTotalPaymentAmount;
                    break;
                }
                case "SourceSystemKey":
                {
                    orderByFunc = payment => payment.PaymentProcessorSourceKey;
                    break;
                }
            }

            bool isAscendingOrder = "asc".Equals(filter.SortOrder, StringComparison.InvariantCultureIgnoreCase);
            return (isAscendingOrder ? paymentHistoryDtos.OrderBy(orderByFunc) : paymentHistoryDtos.OrderByDescending(orderByFunc)).ToList();
        }

        #endregion

        #region payment detail

        public PaymentDto GetPayment(int vpGuarantorId, int paymentId)
        {
            Payment payment = this._paymentService.Value.GetPayment(vpGuarantorId, paymentId);
            return Mapper.Map<PaymentDto>(payment);
        }

        public IList<PaymentEventDto> GetPaymentEvents(List<int> paymentIds, int vpGuarantorId)
        {
            return Mapper.Map<IList<PaymentEventDto>>(this._paymentEventService.Value.GetPaymentEvents(paymentIds, vpGuarantorId));
        }

        public PaymentDetailDto GetPaymentDetail(int madeForVpGuarantorId, int currentVpGuarantorId, int paymentProcessorResponseId)
        {
            PaymentHistoryDto history = this.GetPaymentHistory(madeForVpGuarantorId, currentVpGuarantorId, new PaymentProcessorResponseFilterDto
            {
                PaymentProcessorResponseId = paymentProcessorResponseId.ToString()
            }, 1, 1).Payments.FirstOrDefault();

            if (history == null)
            {
                return null;
            }
            
            PaymentDetailDto paymentDetailDto = Mapper.Map<PaymentDetailDto>(history);
            paymentDetailDto.PaymentAllocations = this.GetPaymentAllocations(history.GuarantorDto.VpGuarantorId, paymentProcessorResponseId);

            return paymentDetailDto;
        }
        
        private IList<PaymentAllocationGroupedDto> GetPaymentAllocations(int vpGuarantorId, int paymentProcessorResponseId)
        {
            PaymentProcessorResponseDto paymentProcessorResponseDto = Mapper.Map<PaymentProcessorResponseDto>(this._paymentService.Value.GetPaymentProcessorResponse(paymentProcessorResponseId, vpGuarantorId));

            if (paymentProcessorResponseDto == null)
            {
                return Enumerable.Empty<PaymentAllocationGroupedDto>().ToList();
            }

            // group by visit and payment allocation status
            List<PaymentAllocationGroupedDto> allocations = paymentProcessorResponseDto.Payments
                .SelectMany(x => x.PaymentAllocations)
                .Where(x => x.PaymentVisit != null)
                .Select(x => new
                {
                    x.PaymentVisit,
                    x.PaymentAllocationStatus,
                    x.PaymentAllocationType,
                    x.ActualAmount
                })
                .GroupBy(x => new
                {
                    x.PaymentVisit.SourceSystemKey,
                    x.PaymentAllocationType,
                })
                .Select(grouping => new PaymentAllocationGroupedDto
                {
                    AllocationAmount = grouping.Sum(x => x.ActualAmount),
                    PaymentAllocationType = grouping.First().PaymentAllocationType,
                    Visit = grouping.First().PaymentVisit
                }).ToList();

            if (this.ApplicationSettingsService.Value.IsClientApplication.Value)
            {
                IList<FinancePlanVisitResult> result = this._financePlanService.Value.AreVisitsOnFinancePlan_FinancePlanVisitResult(allocations.Select(x =>  x.Visit.VisitId).ToList(), paymentProcessorResponseDto.InsertDate);
                allocations.ForEach(allocation =>
                {
                    FinancePlanVisitResult r = result.FirstOrDefault(x => x.VisitId == allocation.Visit.VisitId);
                    allocation.FinancePlanId = r?.FinancePlanId;
                });
            }

            return allocations.OrderBy(x => x.Visit.VisitId).ToList();
        }

        #endregion
        
        #region exports

        public async Task<byte[]> ExportPaymentPendingAsync(int madeForVpGuarantorId, string userName)
        {
            IList<ScheduledPaymentDto> scheduledPayments = this.GetScheduledPayments(madeForVpGuarantorId).Take(iVinciExcelExportBuilder.MaxExportRecords).ToList();

            IDictionary<string, string> textRegions = await this._contentService.Value.GetTextRegionsAsync(
                TextRegionConstants.Amount,
                TextRegionConstants.Date, 
                TextRegionConstants.ExportedBy, 
                TextRegionConstants.FileDate, 
                TextRegionConstants.Guarantor,
                TextRegionConstants.Manual, 
                TextRegionConstants.ScheduledPayments, 
                TextRegionConstants.NoPaymentPendingShort,
                TextRegionConstants.RecurringFinancePlan, 
                TextRegionConstants.PaymentMethod,
                TextRegionConstants.PaymentType);
            
            System.Drawing.Image logo = await this._imageService.Value.GetExportLogoAsync();

            using (ExcelPackage package = new ExcelPackage())
            {
                iVinciExcelExportBuilder builder = new iVinciExcelExportBuilder(package, textRegions.GetValue(TextRegionConstants.ScheduledPayments))
                    .WithLogo(logo)
                    .AddInfoBlockRow(new ColumnBuilder(DateTime.UtcNow.ToString(Format.DateFormat), textRegions.GetValue(TextRegionConstants.FileDate)).WithStyle(StyleTypeEnum.AlignLeft))
                    .AddInfoBlockRow(new ColumnBuilder(userName, textRegions.GetValue(TextRegionConstants.ExportedBy)).WithStyle(StyleTypeEnum.AlignLeft))
                    .AddDataBlock(scheduledPayments, payment =>
                    {
                        string paymentMethodType = payment.PaymentMethod?.PaymentMethodType.GetDescription();
                        string paymentMethod = $"{(!string.IsNullOrEmpty(payment.PaymentMethod?.AccountNickName) ? payment.PaymentMethod?.AccountNickName : paymentMethodType)} X-{payment.PaymentMethod?.LastFour}";
                        string paymentType = payment.PaymentType == PaymentTypeEnum.RecurringPaymentFinancePlan ? textRegions.GetValue(TextRegionConstants.RecurringFinancePlan) : textRegions.GetValue(TextRegionConstants.Manual);

                        return new List<Column>
                        {
                            new ColumnBuilder(payment.ScheduledPaymentDate.Date, textRegions.GetValue(TextRegionConstants.Date)).WithDateFormat(),
                            new ColumnBuilder(payment.MadeForGuarantor.User.DisplayFirstNameLastName, textRegions.GetValue(TextRegionConstants.Guarantor)),
                            new ColumnBuilder(paymentType, textRegions.GetValue(TextRegionConstants.PaymentType)),
                            new ColumnBuilder(paymentMethod, textRegions.GetValue(TextRegionConstants.PaymentMethod)),
                            new ColumnBuilder(payment.ScheduledPaymentAmount, textRegions.GetValue(TextRegionConstants.Amount)).AlignRight().WithCurrencyFormat()
                        };
                    })
                    .WithNoRecordsFoundMessageOf(textRegions.GetValue(TextRegionConstants.NoPaymentPendingShort));

                builder.Build();
            
                return package.GetAsByteArray();
            }
        }

        public async Task<byte[]> ExportPaymentHistoryAsync(int madeForVpGuarantorId, int currentVpGuarantorId, PaymentProcessorResponseFilterDto filterDto, string userName)
        {
            PaymentHistoryResultsDto paymentHistoryResults = this.GetPaymentHistory(madeForVpGuarantorId, currentVpGuarantorId, filterDto, 1, iVinciExcelExportBuilder.MaxExportRecords);

            IDictionary<string, string> textRegions = await this._contentService.Value.GetTextRegionsAsync(
                TextRegionConstants.ConfirmationNumber,
                TextRegionConstants.Date, 
                TextRegionConstants.ExportedBy, 
                TextRegionConstants.FileDate, 
                TextRegionConstants.IdNumber,
                TextRegionConstants.Manual, 
                TextRegionConstants.NetPaymentAmount, 
                TextRegionConstants.NoPaymentHistory,
                TextRegionConstants.PaymentHistoryPageHeading,
                TextRegionConstants.RecurringFinancePlan, 
                TextRegionConstants.PaymentMethod,
                TextRegionConstants.PaymentType,
                TextRegionConstants.Status,
                TextRegionConstants.TransactionAmount);

            IDictionary<string, string> paymentStatuses = await this._contentService.Value.GetTextRegionsAsync(paymentHistoryResults.Payments.Select(x => x.PaymentStatusDisplayName).Distinct().ToArray());
            System.Drawing.Image logo = await this._imageService.Value.GetExportLogoAsync();

            using (ExcelPackage package = new ExcelPackage())
            {
                iVinciExcelExportBuilder builder = new iVinciExcelExportBuilder(package, textRegions.GetValue(TextRegionConstants.PaymentHistoryPageHeading))
                    .WithLogo(logo)
                    .AddInfoBlockRow(new ColumnBuilder(DateTime.UtcNow.ToString(Format.DateFormat), textRegions.GetValue(TextRegionConstants.FileDate)).WithStyle(StyleTypeEnum.AlignLeft))
                    .AddInfoBlockRow(new ColumnBuilder(userName, textRegions.GetValue(TextRegionConstants.ExportedBy)).WithStyle(StyleTypeEnum.AlignLeft))
                    .AddDataBlock(paymentHistoryResults.Payments, payment =>
                    {
                        string paymentMethodType = payment.PaymentMethod?.PaymentMethodType.GetDescription();
                        string paymentMethod = $"{(!string.IsNullOrEmpty(payment.PaymentMethod?.AccountNickName) ? payment.PaymentMethod?.AccountNickName : paymentMethodType)} X-{payment.PaymentMethod?.LastFour}";
                        string paymentProcessorResponseId = payment.PaymentProcessorResponseId == -1 ? string.Empty : payment.ParentPaymentProcessorResponseId.GetValueOrDefault(payment.PaymentProcessorResponseId).ToString();
                        string paymentProcessorSourceKey = !string.IsNullOrEmpty(payment.PaymentProcessorSourceKey) && !string.IsNullOrWhiteSpace(payment.PaymentProcessorSourceKey) ? $"#{payment.PaymentProcessorSourceKey}" : string.Empty;
                        string paymentStatus = paymentStatuses.GetValue(payment.PaymentStatusDisplayName);
                        string paymentType = payment.PaymentType == PaymentTypeEnum.RecurringPaymentFinancePlan ? textRegions.GetValue(TextRegionConstants.RecurringFinancePlan) : textRegions.GetValue(TextRegionConstants.Manual);
                        
                        return new List<Column>
                        {
                            new ColumnBuilder(paymentProcessorResponseId, textRegions.GetValue(TextRegionConstants.IdNumber)),
                            new ColumnBuilder(payment.InsertDate.Date, textRegions.GetValue(TextRegionConstants.Date)).WithDateFormat(),
                            new ColumnBuilder(paymentType, textRegions.GetValue(TextRegionConstants.PaymentType)),
                            new ColumnBuilder(paymentMethod, textRegions.GetValue(TextRegionConstants.PaymentMethod)),
                            new ColumnBuilder(payment.SnapshotTotalPaymentAmount, textRegions.GetValue(TextRegionConstants.TransactionAmount)).AlignRight().WithCurrencyFormat(),
                            new ColumnBuilder(payment.NetPaymentAmount, textRegions.GetValue(TextRegionConstants.NetPaymentAmount)).AlignRight().WithCurrencyFormat(),
                            new ColumnBuilder(paymentProcessorSourceKey, textRegions.GetValue(TextRegionConstants.ConfirmationNumber)).AlignRight(),
                            new ColumnBuilder(paymentStatus, textRegions.GetValue(TextRegionConstants.Status))
                        };
                    })
                    .WithNoRecordsFoundMessageOf(textRegions.GetValue(TextRegionConstants.NoPaymentHistory));

                builder.Build();

                return package.GetAsByteArray();
            }
        }

        public async Task<byte[]> ExportPaymentDetailAsync(int madeForVpGuarantorId, int currentVpGuarantorId, int paymentProcessorResponseId, string userName)
        {
            PaymentDetailDto paymentDetailDto = this.GetPaymentDetail(madeForVpGuarantorId, currentVpGuarantorId, paymentProcessorResponseId);
            
            IDictionary<string, string> textRegions = await this._contentService.Value.GetTextRegionsAsync(
                TextRegionConstants.AllocationAmount,
                TextRegionConstants.AllocationType,
                TextRegionConstants.ConfirmationNumber,
                TextRegionConstants.ExportedBy, 
                TextRegionConstants.FileDate, 
                TextRegionConstants.IdNumber,
                TextRegionConstants.NetPaymentAmount,
                TextRegionConstants.NoPaymentDetails,
                TextRegionConstants.PaymentDetails,
                TextRegionConstants.PaymentMethod,
                TextRegionConstants.PaymentStatus,
                TextRegionConstants.PaymentTransactionAmount,
                TextRegionConstants.PaymentType,
                TextRegionConstants.ProcessedDate,
                TextRegionConstants.Visit);
            
            string paymentMethodDisplay = FormatHelper.PaymentMethodDisplay(paymentDetailDto.PaymentMethod.PaymentMethodType, paymentDetailDto.PaymentMethod.LastFour);
            string paymentProcessorResponseIdDisplay = paymentDetailDto.PaymentProcessorResponseId == -1 ? string.Empty : paymentDetailDto.ParentPaymentProcessorResponseId.GetValueOrDefault(paymentDetailDto.PaymentProcessorResponseId).ToString();
            string paymentStatus = await this._contentService.Value.GetTextRegionAsync(paymentDetailDto.PaymentStatusDisplayName);
            string paymentType = await this._contentService.Value.GetTextRegionAsync(FormatHelper.PaymentTypeDisplay(paymentDetailDto.PaymentType));
            decimal? netPaymentAmount = paymentDetailDto.IsReversalPaymentType ? null : (decimal?)paymentDetailDto.NetPaymentAmount;
            
            System.Drawing.Image logo = await this._imageService.Value.GetExportLogoAsync();

            using (ExcelPackage package = new ExcelPackage())
            {
                iVinciExcelExportBuilder builder = new iVinciExcelExportBuilder(package, textRegions.GetValue(TextRegionConstants.PaymentDetails))
                    .WithLogo(logo)
                    .AddInfoBlockRow(new ColumnBuilder(DateTime.UtcNow.ToString(Format.DateFormat), textRegions.GetValue(TextRegionConstants.FileDate)).AlignLeft())
                    .AddInfoBlockRow(new ColumnBuilder(userName, textRegions.GetValue(TextRegionConstants.ExportedBy)).AlignLeft())
                    .AddBlankInfoBlockRow()
                    .AddInfoBlockRow(new ColumnBuilder($"#{paymentProcessorResponseIdDisplay}", textRegions.GetValue(TextRegionConstants.IdNumber)).AlignLeft())
                    .AddInfoBlockRow(new ColumnBuilder($"#{paymentDetailDto.PaymentProcessorSourceKey}", textRegions.GetValue(TextRegionConstants.ConfirmationNumber)).AlignLeft())
                    .AddInfoBlockRow(new ColumnBuilder(paymentDetailDto.InsertDate.ToString(Format.DateFormat), textRegions.GetValue(TextRegionConstants.ProcessedDate)).AlignLeft())
                    .AddInfoBlockRow(new ColumnBuilder(paymentType, textRegions.GetValue(TextRegionConstants.PaymentType)).AlignLeft())
                    .AddInfoBlockRow(new ColumnBuilder(paymentMethodDisplay, textRegions.GetValue(TextRegionConstants.PaymentMethod)).AlignLeft())
                    .AddInfoBlockRow(new ColumnBuilder(paymentDetailDto.SnapshotTotalPaymentAmount, textRegions.GetValue(TextRegionConstants.PaymentTransactionAmount)).WithCurrencyFormat().AlignLeft())
                    .AddInfoBlockRow(new ColumnBuilder(netPaymentAmount, textRegions.GetValue(TextRegionConstants.NetPaymentAmount)).WithCurrencyFormat().AlignLeft())
                    .AddInfoBlockRow(new ColumnBuilder(paymentStatus, textRegions.GetValue(TextRegionConstants.PaymentStatus)).AlignLeft());

                //obfuscate through automapper
                IList<PaymentAllocationGroupedDto> paymentAllocations = Mapper.Map<IList<PaymentAllocationGroupedDto>>(paymentDetailDto.PaymentAllocations);
                builder.AddDataBlock(paymentAllocations, distribution =>
                {
                    string sourceSystemKey = distribution.Visit.IsUnmatched ? this.Client.Value.RedactedVisitReplacementValue : $"#{distribution.Visit.SourceSystemKeyDisplay ?? distribution.Visit.SourceSystemKey}";

                    return new List<Column>
                    {
                        new ColumnBuilder(FormatHelper.PaymentAllocationTypeDisplay(distribution.PaymentAllocationType), textRegions.GetValue(TextRegionConstants.AllocationType)),
                        new ColumnBuilder(sourceSystemKey, textRegions.GetValue(TextRegionConstants.Visit)),
                        new ColumnBuilder(distribution.AllocationAmount, textRegions.GetValue(TextRegionConstants.AllocationAmount)).WithCurrencyFormat().AlignRight()
                    };
                }).WithNoRecordsFoundMessageOf(textRegions.GetValue(TextRegionConstants.NoPaymentDetails));

                builder.Build();

                return package.GetAsByteArray();
            }
        }
        
        #endregion

        public IList<PaymentStatusDto> GetPaymentStatuses(string category)
        {
            IList<PaymentStatusDto> paymentStatuses = Mapper.Map<IList<PaymentStatusDto>>(this._paymentService.Value.GetPaymentStatuses());

            return paymentStatuses.Where(x => EnumHelper<PaymentStatusEnum>.Contains(x.PaymentStatusEnum, category))
                .OrderBy(x => x.DisplayOrder)
                .ToList();
        }
    }
}
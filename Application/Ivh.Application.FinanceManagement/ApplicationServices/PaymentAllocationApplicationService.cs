﻿namespace Ivh.Application.FinanceManagement.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Interfaces;
    using Domain.FinanceManagement.Payment.Interfaces;
    using Ivh.Application.FinanceManagement.Common.Dtos;
    using Ivh.Common.Base.Utilities.Extensions;

    public class PaymentAllocationApplicationService : IPaymentAllocationApplicationService
    {
        private readonly Lazy<IPaymentAllocationService> _paymentAllocationService;

        public PaymentAllocationApplicationService(Lazy<IPaymentAllocationService> paymentAllocationService)
        {
            this._paymentAllocationService = paymentAllocationService;
        }

        public void ClearPaymentAllocations(IList<int> paymentAllocationIds)
        {
            if (paymentAllocationIds.IsNullOrEmpty())
            {
                return;
            }

            this._paymentAllocationService.Value.ClearPaymentAllocations(paymentAllocationIds);
        }

        #region Finance Plan Payment Allocation

        public List<FinancePlanPaymentDto> GetFinancePlanPaymentAmounts(decimal paymentAmount, FinancePlanStatementDto financePlanStatementDto)
        {
            decimal remainingPaymentAmount = paymentAmount;

            List<FinancePlanPaymentDto> financePlanPayments = financePlanStatementDto.FinancePlanStatementFinancePlans
                .Select(x => new FinancePlanPaymentDto() { FinancePlanId = x.FinancePlanId, PaymentAmount = 0m }).ToList();

            List<FinancePlanStatementFinancePlanDto> oldestFirstFinancePlans = financePlanStatementDto.FinancePlanStatementFinancePlans
                .OrderBy(x => x.StatementedSnapshotFinancePlanEffectiveDate).ToList();

            //
            this.AllocatePaymentAmountToFinancePlan(oldestFirstFinancePlans, financePlanPayments, FinancePlanBalanceTypeEnum.PastDueAmount, ref remainingPaymentAmount);
            this.AllocatePaymentAmountToFinancePlan(oldestFirstFinancePlans, financePlanPayments, FinancePlanBalanceTypeEnum.MonthlyPaymentAmount, ref remainingPaymentAmount);
            this.AllocatePaymentAmountToFinancePlan(oldestFirstFinancePlans, financePlanPayments, FinancePlanBalanceTypeEnum.RemainingAmountWhenCurrent, ref remainingPaymentAmount);

            return financePlanPayments;
        }

        private enum FinancePlanBalanceTypeEnum
        {
            PastDueAmount,
            MonthlyPaymentAmount,
            RemainingAmountWhenCurrent
        }

        private Func<FinancePlanStatementFinancePlanDto, decimal> GetFinancePlanBalanceFilter(FinancePlanBalanceTypeEnum financePlanBalanceTypeEnum)
        {
            Func<FinancePlanStatementFinancePlanDto, decimal> filterFinancePlanBalanceType;
            if (financePlanBalanceTypeEnum == FinancePlanBalanceTypeEnum.PastDueAmount)
            {
                filterFinancePlanBalanceType = x => x.StatementedSnapshotFinancePlanPastDueAmount ?? 0m;
            }
            else if (financePlanBalanceTypeEnum == FinancePlanBalanceTypeEnum.MonthlyPaymentAmount)
            {
                filterFinancePlanBalanceType = x => x.StatementedSnapshotFinancePlanMonthlyPaymentAmount ?? 0m;
            }
            else if (financePlanBalanceTypeEnum == FinancePlanBalanceTypeEnum.RemainingAmountWhenCurrent)
            {
                filterFinancePlanBalanceType = x =>
                    (x.StatementedSnapshotFinancePlanBalance ?? 0m)
                    - (x.StatementedSnapshotFinancePlanMonthlyPaymentAmount ?? 0m)
                    - (x.StatementedSnapshotFinancePlanPastDueAmount ?? 0m);
            }
            else //should not be here
            {
                filterFinancePlanBalanceType = x => 0m;
            }

            return filterFinancePlanBalanceType;
        }

        private void AllocatePaymentAmountToFinancePlan(
            List<FinancePlanStatementFinancePlanDto> oldestFirstFinancePlans,
            List<FinancePlanPaymentDto> financePlanPayments,
            FinancePlanBalanceTypeEnum financePlanBalanceTypeEnum,
            ref decimal remainingPaymentAmount)
        {
            Func<FinancePlanStatementFinancePlanDto, decimal> financePlanBalanceFilter = this.GetFinancePlanBalanceFilter(financePlanBalanceTypeEnum);
            foreach (int financePlanId in oldestFirstFinancePlans.Select(x => x.FinancePlanId))
            {
                decimal amountDue = oldestFirstFinancePlans
                    .Where(x => x.FinancePlanId == financePlanId)
                    .Select(x => financePlanBalanceFilter(x))
                    .First();
                if (amountDue > 0m && remainingPaymentAmount > 0m)
                {
                    decimal amountToPay = Math.Min(amountDue, remainingPaymentAmount);
                    financePlanPayments.First(x => x.FinancePlanId == financePlanId).PaymentAmount += amountToPay;
                    remainingPaymentAmount -= amountToPay;
                }
            }
        }

        #endregion
    }
}
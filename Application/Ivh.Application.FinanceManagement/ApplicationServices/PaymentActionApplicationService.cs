﻿namespace Ivh.Application.FinanceManagement.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Common.Dtos;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.FinanceManagement.Entities;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Interfaces;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Payment.Interfaces;
    using Domain.FinanceManagement.Statement.Entities;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using NHibernate;

    public class PaymentActionApplicationService : ApplicationService, IPaymentActionApplicationService
    {
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IFinancePlanService> _financePlanService;
        private readonly Lazy<IPaymentService> _paymentService;
        private readonly Lazy<IPaymentActionService> _paymentActionService;
        private readonly Lazy<IPaymentMethodsService> _paymentMethodsService;
        private readonly Lazy<IVpStatementService> _statementService;
        private readonly ISession _session;

        public PaymentActionApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IFinancePlanService> financePlanService,
            Lazy<IPaymentService> paymentService,
            Lazy<IPaymentActionService> paymentActionService,
            Lazy<IPaymentMethodsService> paymentMethodsService,
            Lazy<IVpStatementService> statementService,
            ISessionContext<VisitPay> sessionContext) : base(applicationServiceCommonService)
        {
            this._guarantorService = guarantorService;
            this._financePlanService = financePlanService;
            this._paymentService = paymentService;
            this._paymentActionService = paymentActionService;
            this._paymentMethodsService = paymentMethodsService;
            this._statementService = statementService;
            this._session = sessionContext.Session;
        }

        public IList<PaymentActionReasonDto> GetPaymentCancelReasons(PaymentTypeEnum paymentType)
        {
            return Mapper.Map<IList<PaymentActionReasonDto>>(this._paymentActionService.Value.GetPaymentCancelReasons(paymentType));
        }

        public IList<PaymentActionReasonDto> GetPaymentRescheduleReasons(PaymentTypeEnum paymentType)
        {
            return Mapper.Map<IList<PaymentActionReasonDto>>(this._paymentActionService.Value.GetPaymentRescheduleReasons(paymentType));
        }
        
        public bool IsEligibleToCancel(int vpGuarantorId, int visitPayUserId, PaymentTypeEnum paymentType)
        {
            return this._paymentActionService.Value.IsEligibleToCancel(vpGuarantorId, visitPayUserId, paymentType);
        }

        public bool IsEligibleToReschedule(int vpGuarantorId, int visitPayUserId, PaymentTypeEnum paymentType)
        {
            return this._paymentActionService.Value.IsEligibleToReschedule(vpGuarantorId, visitPayUserId, paymentType);
        }

        public DateTime GetMaxAllowedRescheduleDate(int vpGuarantorId, PaymentTypeEnum paymentType)
        {
            if (paymentType == PaymentTypeEnum.RecurringPaymentFinancePlan)
            {
                // allow recurring to be rescheduled up to next payment due date
                Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
                DateTime paymentDueDateOnNextStatement = this._statementService.Value.GetPaymentDueDateForNextStatement(guarantor);

                return paymentDueDateOnNextStatement.Date;
            }
            
            return DateTime.UtcNow.AddDays(this.Client.Value.MaxFuturePaymentIntervalInDays);
        }
        
        public bool CancelPayment(int vpGuarantorId, int actionVisitPayUserId, PaymentCancelDto paymentCancelDto)
        {
            if (paymentCancelDto.PaymentType == PaymentTypeEnum.RecurringPaymentFinancePlan)
            {
                return this.CancelPaymentFinancePlan(vpGuarantorId, actionVisitPayUserId, paymentCancelDto);
            }

            return this.CancelPaymentScheduled(vpGuarantorId, actionVisitPayUserId, paymentCancelDto);
        }

        private bool CancelPaymentScheduled(int vpGuarantorId, int actionVisitPayUserId, PaymentCancelDto paymentCancelDto)
        {
            if (paymentCancelDto.PaymentType == PaymentTypeEnum.RecurringPaymentFinancePlan || !paymentCancelDto.PaymentId.HasValue || !paymentCancelDto.PaymentCancelReasonId.HasValue)
            {
                return false;
            }
            
            Payment payment = this._paymentService.Value.GetPayment(vpGuarantorId, paymentCancelDto.PaymentId.Value);
            if (payment == null || payment.IsRecurringPayment())
            {
                return false;
            }

            using (IUnitOfWork uow = new UnitOfWork(this._session))
            {
                IList<Payment> cancelledPayments = new List<Payment>();

                this._paymentService.Value.CancelPayment(vpGuarantorId, payment.PaymentId.ToListOfOne(), actionVisitPayUserId);
                cancelledPayments.Add(payment);

                this._paymentActionService.Value.AddCancelAction(cancelledPayments, paymentCancelDto.PaymentCancelReasonId.Value, paymentCancelDto.PaymentCancelDescription, actionVisitPayUserId);

                uow.Commit();
            }

            return true;
        }

        private bool CancelPaymentFinancePlan(int vpGuarantorId, int actionVisitPayUserId, PaymentCancelDto paymentCancelDto)
        {
            if (paymentCancelDto.PaymentType != PaymentTypeEnum.RecurringPaymentFinancePlan || paymentCancelDto.PaymentId.HasValue || !paymentCancelDto.PaymentCancelReasonId.HasValue)
            {
                return false;
            }
            
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            DateTime nextPaymentDueDate = this._statementService.Value.GetNextPaymentDate(guarantor);

            IList<CancelRecurringPaymentDto> cancelledRecurringPaymentDtos = new List<CancelRecurringPaymentDto>();

            using (IUnitOfWork uow = new UnitOfWork(this._session))
            {
                IList<FinancePlan> financePlans = this._financePlanService.Value.GetAllOpenFinancePlansForGuarantor(new Guarantor {VpGuarantorId = vpGuarantorId}, false);
                List<FinancePlan> applicableFinancePlans = financePlans
                    .Where(x => x.Buckets.Count > 0)
                    .Where(x => x.Buckets.OrderBy(b => b.Key).Last().Value.Item1 > 0m)
                    .ToList();
                
                foreach (FinancePlan financePlan in applicableFinancePlans)
                {
                    decimal amountCancelled = financePlan.AddFinancePlanAmountsDueCancelPayment(actionVisitPayUserId);
                    if (amountCancelled <= 0m)
                    {
                        continue;
                    }
                    
                    this._financePlanService.Value.CheckIfFinancePlansAreStillPastDueOrUncollectable(financePlan.ToListOfOne());
                    this._financePlanService.Value.UpdateFinancePlan(financePlan);
                    
                    DateTime dueDate = financePlan.MaxScheduledPaymentDateOrNextPaymentDueDate(nextPaymentDueDate);
                    
                    cancelledRecurringPaymentDtos.Add(new CancelRecurringPaymentDto(financePlan.FinancePlanId, dueDate, amountCancelled));
                }
                
                if (cancelledRecurringPaymentDtos.Any())
                {
                    this._paymentActionService.Value.AddCancelAction(cancelledRecurringPaymentDtos.Select(x => x.FinancePlanId).ToList(), vpGuarantorId, paymentCancelDto.PaymentCancelReasonId.Value, paymentCancelDto.PaymentCancelDescription, actionVisitPayUserId);
                    this._paymentService.Value.CancelRecurringPayment(guarantor, actionVisitPayUserId, cancelledRecurringPaymentDtos);
                }
                
                uow.Commit();
            }

            return cancelledRecurringPaymentDtos.Any();
        }

        public bool ReschedulePayment(int vpGuarantorId, int actionVisitPayUserId, PaymentRescheduleDto paymentRescheduleDto)
        {
            using (IUnitOfWork uow = new UnitOfWork(this._session))
            {
                Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
                if (paymentRescheduleDto.PaymentType == PaymentTypeEnum.RecurringPaymentFinancePlan)
                {
                    if (paymentRescheduleDto.PaymentRescheduleReasonId.HasValue)
                    {
                        DateTime rescheduledDate = this.GetMaxAllowedRescheduleDate(vpGuarantorId, paymentRescheduleDto.PaymentType);
                        IList<int> financePlanIdsWithReschedule = new List<int>();

                        VpStatement statement = this._statementService.Value.GetMostRecentStatement(guarantor);
                        IList<FinancePlan> financePlans = this._financePlanService.Value.GetAllOpenFinancePlansForGuarantor(new Guarantor {VpGuarantorId = vpGuarantorId}, false);
                        foreach (FinancePlan financePlan in financePlans)
                        {
                            if (!financePlan.AddFinancePlanAmountsDueReschedulePayment(statement.VpStatementId, rescheduledDate, actionVisitPayUserId))
                            {
                                continue;
                            }

                            financePlanIdsWithReschedule.Add(financePlan.FinancePlanId);
                            this._financePlanService.Value.CheckIfFinancePlansAreStillPastDueOrUncollectable(financePlan.ToListOfOne());
                            this._financePlanService.Value.UpdateFinancePlan(financePlan);
                        }

                        if (financePlanIdsWithReschedule.Any())
                        {
                            this._paymentActionService.Value.AddRescheduleAction(financePlanIdsWithReschedule, vpGuarantorId, paymentRescheduleDto.PaymentRescheduleReasonId.Value, paymentRescheduleDto.PaymentRescheduleDescription, actionVisitPayUserId);
                        }
                    }
                }
                else if (paymentRescheduleDto.PaymentId.HasValue)
                {
                    Payment payment = this._paymentService.Value.GetPayment(vpGuarantorId, paymentRescheduleDto.PaymentId.Value);
                    if (guarantor == null || payment == null)
                    {
                        return false;
                    }

                    // update payment method
                    if (paymentRescheduleDto.PaymentMethodId.HasValue && (payment.PaymentMethod == null || payment.PaymentMethod.PaymentMethodId != paymentRescheduleDto.PaymentMethodId.Value))
                    {
                        PaymentMethod paymentMethod = this._paymentMethodsService.Value.GetById(paymentRescheduleDto.PaymentMethodId.Value);
                        if (paymentMethod != null)
                        {
                            payment.PaymentMethod = paymentMethod;
                        }
                    }

                    // update scheduled date
                    DateTime rescheduledDate = paymentRescheduleDto.RescheduledPaymentDate.GetValueOrDefault(payment.ScheduledPaymentDate.Date).Date;
                    if (rescheduledDate != payment.ScheduledPaymentDate.Date && paymentRescheduleDto.PaymentRescheduleReasonId.HasValue)
                    {
                        payment.ScheduledPaymentDate = rescheduledDate;
                        this._paymentActionService.Value.AddRescheduleAction(payment, paymentRescheduleDto.PaymentRescheduleReasonId.Value, paymentRescheduleDto.PaymentRescheduleDescription, actionVisitPayUserId);
                    }

                    this._paymentService.Value.UpdatePayment(guarantor, payment, actionVisitPayUserId);
                }
                
                uow.Commit();
            }

            return true;
        }
    }
}
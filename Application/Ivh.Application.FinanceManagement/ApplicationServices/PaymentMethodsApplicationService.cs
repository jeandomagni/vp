﻿namespace Ivh.Application.FinanceManagement.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Enums;
    using Common.Interfaces;
    using Content.Common.Interfaces;
    using Core.Common.Interfaces;
    using Domain.FinanceManagement.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Interfaces;
    using Domain.FinanceManagement.Payment.Utilities;
    using Domain.FinanceManagement.ValueTypes;
    using Domain.Guarantor.Entities;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.EventJournal;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Jobs;
    using Ivh.Common.VisitPay.Messages.Core;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;
    using Ivh.Domain.Guarantor.Interfaces;
    using Newtonsoft.Json;
    using NHibernate;
    using User.Common.Dtos;

    public class PaymentMethodsApplicationService : ApplicationService, IPaymentMethodsApplicationService
    {
        private readonly Lazy<IVisitPayUserJournalEventApplicationService> _visitPayUserJournalEventApplicationService;
        private readonly ISession _session;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IPaymentMethodsService> _paymentMethodsService;
        private readonly Lazy<IPaymentMethodAccountTypeService> _paymentMethodAccountTypeService;
        private readonly Lazy<IPaymentMethodProviderTypeService> _paymentMethodProviderTypeService;
        private readonly Lazy<IPaymentProvider> _paymentProvider;
        private readonly Lazy<ISsoApplicationService> _ssoApplicationService;
        private readonly Lazy<IFinancePlanService> _financePlanService;
        private readonly Lazy<IVisitBalanceBaseApplicationService> _visitBalanceBaseApplicationService;
        private readonly Lazy<IContentApplicationService> _contentApplicationService;

        public PaymentMethodsApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IPaymentMethodsService> paymentMethodsService,
            Lazy<IPaymentMethodAccountTypeService> paymentMethodAccountTypeService,
            Lazy<IPaymentMethodProviderTypeService> paymentMethodProviderTypeService,
            Lazy<IPaymentProvider> paymentProvider,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IVisitPayUserJournalEventApplicationService> visitPayUserJournalEventApplicationService,
            ISessionContext<VisitPay> sessionContext,
            Lazy<ISsoApplicationService> ssoApplicationService,
            Lazy<IFinancePlanService> financePlanService,
            Lazy<IVisitBalanceBaseApplicationService> visitBalanceBaseApplicationService,
            Lazy<IContentApplicationService> contentApplicationService) : base(applicationServiceCommonService)
        {
            this._paymentMethodsService = paymentMethodsService;
            this._paymentMethodAccountTypeService = paymentMethodAccountTypeService;
            this._paymentMethodProviderTypeService = paymentMethodProviderTypeService;
            this._paymentProvider = paymentProvider;
            this._visitPayUserJournalEventApplicationService = visitPayUserJournalEventApplicationService;
            this._session = sessionContext.Session;
            this._guarantorService = guarantorService;
            this._ssoApplicationService = ssoApplicationService;
            this._financePlanService = financePlanService;
            this._visitBalanceBaseApplicationService = visitBalanceBaseApplicationService;
            this._contentApplicationService = contentApplicationService;
        }

        #region get

        public PaymentMethodDto GetPaymentMethodById(int paymentMethodId)
        {
            return Mapper.Map<PaymentMethodDto>(this._paymentMethodsService.Value.GetById(paymentMethodId));
        }

        public PaymentMethodDto GetPaymentMethodById(int paymentMethodId, int vpGuarantorId)
        {
            return Mapper.Map<PaymentMethodDto>(this._paymentMethodsService.Value.GetById(paymentMethodId, vpGuarantorId));
        }

        public PaymentMethodDto GetPrimaryPaymentMethod(int vpGuarantorId)
        {
            PaymentMethod primaryPaymentMethod = this._paymentMethodsService.Value.GetPrimaryPaymentMethod(vpGuarantorId, false);
            return primaryPaymentMethod == null ? null : Mapper.Map<PaymentMethodDto>(primaryPaymentMethod);
        }

        public IList<PaymentMethodDto> GetPaymentMethods(int vpGuarantorId, int actionVisitPayUserId, bool includingInactive = false)
        {
            IList<PaymentMethod> paymentMethods = this._paymentMethodsService.Value.GetPaymentMethods(vpGuarantorId, actionVisitPayUserId, includingInactive);
            IList<PaymentMethodDto> paymentMethodDtos = Mapper.Map<IList<PaymentMethodDto>>(paymentMethods.OrderBy(x => x.DisplayName).ThenBy(x => x.LastFour));

            return paymentMethodDtos;
        }

        #endregion

        #region save/delete/primary

        public async Task<DeletePaymentMethodResponseDto> DeactivatePaymentMethodAsync(int paymentMethodId, int vpGuarantorId, int actionVisitPayUserId)
        {
            PaymentMethod paymentMethod = this._paymentMethodsService.Value.GetById(paymentMethodId, vpGuarantorId);
            if (paymentMethod == null)
            {
                string errorMessage = await this._contentApplicationService.Value.GetTextRegionAsync(TextRegionConstants.PaymentValidationInvalidPaymentMethod);
                return new DeletePaymentMethodResponseDto
                {
                    IsError = true,
                    ErrorMessage = errorMessage
                };
            }

            PaymentMethodResponse response = this._paymentMethodsService.Value.Delete(paymentMethod, actionVisitPayUserId);

            return Mapper.Map<DeletePaymentMethodResponseDto>(response);
        }

        public PaymentMethodResultDto SetPrimaryPaymentMethod(JournalEventHttpContextDto context, int paymentMethodId, int vpGuarantorId, int actionVisitPayUserId)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            PaymentMethod paymentMethod = this._paymentMethodsService.Value.GetById(paymentMethodId, vpGuarantorId);
            paymentMethod.IsPrimary = true;

            PaymentMethodResponse validateResponse = this._paymentMethodsService.Value.ValidatePrimaryPaymentMethod(paymentMethod);

            if (validateResponse.IsError)
            {
                return new PaymentMethodResultDto(false, validateResponse.ErrorMessage);
            }

            PaymentMethodResponse paymentMethodResponse = this._paymentMethodsService.Value.Save(paymentMethod, actionVisitPayUserId, null, false);

            bool didVpGuarantorPerformAction = guarantor.User.VisitPayUserId == actionVisitPayUserId;

            if (!didVpGuarantorPerformAction) {
                this._visitPayUserJournalEventApplicationService.Value.LogClientChangedPrimaryPaymentMethod(actionVisitPayUserId, vpGuarantorId, guarantor.User.UserName, paymentMethodId.ToString(), context);
            }
            else {
                this._visitPayUserJournalEventApplicationService.Value.LogGuarantorChangedPrimaryPaymentMethod(guarantor.User.VisitPayUserId, vpGuarantorId, guarantor.User.UserName, paymentMethodId.ToString(), context);
            }

            return Mapper.Map<PaymentMethodResultDto>(paymentMethodResponse);
        }

        public PaymentMethodResultDto SetPaymentMethodAccountProviderType(int paymentMethodId, int paymentMethodAccountTypeId, int paymentMethodProviderTypeId, int vpGuarantorId, int actionVisitPayUserId)
        {
            PaymentMethod paymentMethod = this._paymentMethodsService.Value.GetById(paymentMethodId, vpGuarantorId);
            PaymentMethodAccountType paymentMethodAccountType = this._paymentMethodAccountTypeService.Value.GetById(paymentMethodAccountTypeId);
            paymentMethod.PaymentMethodAccountType = paymentMethodAccountType;

            PaymentMethodProviderType paymentMethodProviderType = this._paymentMethodProviderTypeService.Value.GetById(paymentMethodProviderTypeId);
            paymentMethod.PaymentMethodProviderType = paymentMethodProviderType;

            PaymentMethodResponse paymentMethodResponse = this._paymentMethodsService.Value.Save(paymentMethod, actionVisitPayUserId, null, false);

            this.ShowAlertForHqyCards(paymentMethodProviderTypeId, vpGuarantorId);
            this.ShowNotificationForHqyCards(paymentMethodProviderTypeId, vpGuarantorId);

            return Mapper.Map<PaymentMethodResultDto>(paymentMethodResponse);
        }

        public async Task<PaymentMethodResultDto> SaveBankPaymentMethodAsync(JournalEventHttpContextDto context, PaymentMethodDto paymentMethodDto, int vpGuarantorId, int actionVisitPayUserId, string accountNumber, string routingNumber)
        {
            PaymentMethod paymentMethod = this.GetPaymentMethodFromDto(paymentMethodDto, vpGuarantorId, accountNumber);

            BillingIdResult result = await this.DoSaveBankPaymentMethodAsync(context, paymentMethod, accountNumber, routingNumber);
            if (!result.Succeeded)
            {
                string errorMessage = Utility.GetRealTimeStatusMessage(result.Response);

                if (string.IsNullOrEmpty(errorMessage))
                {
                    errorMessage = await this._contentApplicationService.Value.GetTextRegionAsync(TextRegionConstants.PaymentValidationInvalidBankAccountDetails);
                }

                return new PaymentMethodResultDto(false, errorMessage);
            }

            PaymentMethodResponse paymentMethodResponse = this._paymentMethodsService.Value.Save(paymentMethod, actionVisitPayUserId, null, false);
            this.LogPaymentAddingJournalEvent(vpGuarantorId, paymentMethod.IsPrimary, paymentMethod.PaymentMethodId.ToString(), actionVisitPayUserId, context);

            return Mapper.Map<PaymentMethodResultDto>(paymentMethodResponse);
        }

        public PaymentMethodResultDto SaveCardPaymentMethod(JournalEventHttpContextDto context, PaymentMethodDto paymentMethodDto, int vpGuarantorId, int actionVisitPayUserId)
        {
            PaymentMethod paymentMethod = this.GetPaymentMethodFromDto(paymentMethodDto, vpGuarantorId, paymentMethodDto.LastFour);

            if (paymentMethod.IsPrimary)
            {
                PaymentMethodResponse validateResponse = this._paymentMethodsService.Value.ValidatePrimaryPaymentMethod(paymentMethod);
                if (validateResponse.IsError)
                {
                    return new PaymentMethodResultDto(false, validateResponse.ErrorMessage);
                }
            }

            if (paymentMethod.IsExpired)
            {
                string message = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.PaymentValidationPaymentMethodExpired);
                return new PaymentMethodResultDto(false, message);
            }

            if (!this.SetCardPaymentMethodIdentifiers(paymentMethod, paymentMethodDto.BillingId, paymentMethodDto.GatewayToken))
            {
                string message = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.PaymentValidationInvalidCardDetails);
                return new PaymentMethodResultDto(false, message);
            }

            this.ShowAlertForHqyCards(paymentMethod.PaymentMethodProviderType.PaymentMethodProviderTypeId, vpGuarantorId);
            this.ShowNotificationForHqyCards(paymentMethod.PaymentMethodProviderType.PaymentMethodProviderTypeId, vpGuarantorId);

            PaymentMethodResponse paymentMethodResponse = this._paymentMethodsService.Value.Save(paymentMethod, actionVisitPayUserId, paymentMethodDto.CardCode, false);

            this.LogPaymentAddingJournalEvent(vpGuarantorId, paymentMethod.IsPrimary, paymentMethod.PaymentMethodId.ToString(), actionVisitPayUserId, context);

            return Mapper.Map<PaymentMethodResultDto>(paymentMethodResponse);
        }

        public PaymentMethodResultDto SaveTempLockBoxPaymentMethod(int vpGuarantorId, int actionVisitPayUserId)
        {
            PaymentMethod paymentMethod = new PaymentMethod
            {
                AccountNickName = "Lock Box Deposit",
                IsActive = false,
                VpGuarantorId = vpGuarantorId,
                PaymentMethodType = PaymentMethodTypeEnum.LockBoxDeposit,
                CreatedUserId = actionVisitPayUserId,
                UpdatedUserId = actionVisitPayUserId
            };

            PaymentMethodResponse paymentMethodResponse = this._paymentMethodsService.Value.Save(paymentMethod, actionVisitPayUserId, null, true);

            return Mapper.Map<PaymentMethodResultDto>(paymentMethodResponse);
        }

        private async Task<BillingIdResult> DoSaveBankPaymentMethodAsync(JournalEventHttpContextDto context, PaymentMethod paymentMethod, string accountNumber, string routingNumber)
        {
            if (!this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.PaymentsAch) || !paymentMethod.IsAchType)
            {
                return new BillingIdResult { Succeeded = false };
            }

            BillingIdResult billingResult = await this._paymentProvider.Value.StoreBankAccountBillingIdAsync(paymentMethod, accountNumber, routingNumber);
            if (billingResult.Succeeded)
            {
                paymentMethod.GatewayToken = billingResult.GatewayToken;
                if (!string.IsNullOrEmpty(billingResult.BillingID))
                {
                    paymentMethod.BillingId = billingResult.BillingID;
                }
            }
            else
            {
                if (paymentMethod.VpGuarantorId != null)
                {
                    this._visitPayUserJournalEventApplicationService.Value.LogBillingIdFailure(paymentMethod.CreatedUserId, paymentMethod.VpGuarantorId.Value, billingResult.Response, context);
                }
            }

            return billingResult;
        }

        private PaymentMethod GetPaymentMethodFromDto(PaymentMethodDto paymentMethodDto, int vpGuarantorId, string lastFour)
        {
            PaymentMethod paymentMethod = null;

            if (paymentMethodDto.PaymentMethodId.HasValue && paymentMethodDto.PaymentMethodId != default(int))
            {
                paymentMethod = this._paymentMethodsService.Value.GetById(paymentMethodDto.PaymentMethodId.Value, vpGuarantorId);
                if (paymentMethod != null)
                {
                    List<PaymentMethodBillingAddress> beforeAddresses = paymentMethod.BillingAddresses.ToList();

                    Mapper.Map(paymentMethodDto, paymentMethod);

                    List<PaymentMethodBillingAddressDto> afterAddresses = paymentMethodDto.BillingAddresses.ToList();

                    for (int i = 0; i < afterAddresses.Count; i++)
                    {
                        PaymentMethodBillingAddressDto afterAddress = afterAddresses[i];

                        int paymentMethodBillingAddressId = afterAddress.PaymentMethodBillingAddressID;

                        PaymentMethodBillingAddress beforeAddress = beforeAddresses.FirstOrDefault(x => x.PaymentMethodBillingAddressId == paymentMethodBillingAddressId);

                        if (beforeAddress != null)
                        {
                            paymentMethod.BillingAddresses[i] = Mapper.Map(paymentMethodDto.BillingAddresses[i], beforeAddress);
                        }
                    }
                }
            }

            if (paymentMethod == null)
            {
                paymentMethod = Mapper.Map<PaymentMethod>(paymentMethodDto);
                paymentMethod.VpGuarantorId = vpGuarantorId;
                paymentMethod.LastFour = LastCharacters(lastFour);
            }

            if (paymentMethod.IsCardType)
            {
                PaymentMethodAccountType paymentMethodAccountType = this._paymentMethodAccountTypeService.Value.GetById(paymentMethodDto.AccountTypeId ?? 0);
                paymentMethod.PaymentMethodAccountType = paymentMethodAccountType;

                PaymentMethodProviderType paymentMethodProviderType = this._paymentMethodProviderTypeService.Value.GetById(paymentMethodDto.ProviderTypeId ?? 0);
                paymentMethod.PaymentMethodProviderType = paymentMethodProviderType;
            }

            return paymentMethod;
        }

        private bool SetCardPaymentMethodIdentifiers(PaymentMethod paymentMethod, string billingId, string gatewayToken)
        {
            if (!this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.PaymentsCreditCard) || !paymentMethod.IsCardType)
            {
                return false;
            }

            paymentMethod.GatewayToken = gatewayToken;
            if (!string.IsNullOrWhiteSpace(billingId))
            {
                paymentMethod.BillingId = billingId;
            }

            return true;
        }

        #endregion

        #region registration

        public async Task<PaymentMethodResultDto> ProcessBankPaymentMethodRegistrationAsync(JournalEventHttpContextDto context, PaymentMethodDto paymentMethodDto, string accountNumber, string routingNumber)
        {
            PaymentMethod paymentMethod = Mapper.Map<PaymentMethod>(paymentMethodDto);

            BillingIdResult result = await this.DoSaveBankPaymentMethodAsync(context, paymentMethod, accountNumber, routingNumber);
            if (!result.Succeeded)
            {
                string errorMessage = Utility.GetRealTimeStatusMessage(result.Response);

                if (string.IsNullOrEmpty(errorMessage))
                {
                    errorMessage = await this._contentApplicationService.Value.GetTextRegionAsync(TextRegionConstants.PaymentValidationInvalidBankAccountDetails);
                }

                return new PaymentMethodResultDto(false, errorMessage);
            }

            Mapper.Map(paymentMethod, paymentMethodDto);

            paymentMethodDto.LastFour = LastCharacters(!string.IsNullOrEmpty(accountNumber) ? accountNumber : paymentMethodDto.LastFour);

            return new PaymentMethodResultDto(true, null, false, paymentMethodDto);
        }

        public PaymentMethodResultDto ProcessCardPaymentMethodRegistration(HttpContextDto context, PaymentMethodDto paymentMethodDto, string billingId, string gatewayToken, string securityNumber, string lastFour)
        {
            PaymentMethod paymentMethod = Mapper.Map<PaymentMethod>(paymentMethodDto);

            if (paymentMethod.IsExpired)
            {
                string errorMessage = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.PaymentValidationPaymentMethodExpired);
                return new PaymentMethodResultDto(false, errorMessage);
            }

            if (!this.SetCardPaymentMethodIdentifiers(paymentMethod, billingId, gatewayToken))
            {
                string message = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.PaymentValidationInvalidCardDetails);
                return new PaymentMethodResultDto(false, message);
            }

            Mapper.Map(paymentMethod, paymentMethodDto);

            paymentMethodDto.LastFour = LastCharacters(!string.IsNullOrEmpty(lastFour) ? lastFour : paymentMethodDto.LastFour);

            return new PaymentMethodResultDto(true, null, false, paymentMethodDto);
        }

        public void PersistPaymentMethodsFromRegistration(IList<PaymentMethodDto> paymentMethodDtos, int visitPayUserId, int vpGuarantorId)
        {
            if (!paymentMethodDtos.Any())
            {
                return;
            }

            foreach (PaymentMethodDto paymentMethodDto in paymentMethodDtos.OrderByDescending(x => x.IsPrimary).ThenBy(x => x.PaymentMethodId))
            {
                paymentMethodDto.PaymentMethodId = null; // not yet saved, using an incremented int in UI, so null it here
                PaymentMethod paymentMethod = this.GetPaymentMethodFromDto(paymentMethodDto, vpGuarantorId, paymentMethodDto.LastFour);

                this._paymentMethodsService.Value.Save(paymentMethod, visitPayUserId, null, true);
            }
        }

        #endregion

        public void AddAchAuthorization(int currentVisitPayUserId, int paymentMethodId, int cmsVersionId, IList<FinancePlanAchAuthorizationDto> authorizations)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantorEx(currentVisitPayUserId);
            using (IUnitOfWork uow = new UnitOfWork(this._session))
            {
                Guid correlationGuid = Guid.NewGuid();

                foreach (FinancePlanAchAuthorizationDto achAuthorization in authorizations)
                {
                    this._paymentMethodsService.Value.AddAchAuthorization(guarantor.VpGuarantorId, paymentMethodId, cmsVersionId, achAuthorization.MonthlyPaymentAmount, achAuthorization.MonthsRemaining, correlationGuid);
                }

                uow.Commit();
            }
        }

        private void LogPaymentAddingJournalEvent(int vpGuarantorId, bool isPrimary, string paymentMethodId, int actionVisitPayUserId, JournalEventHttpContextDto context)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            string guarantorUsername = guarantor.User.UserName;

            bool didVpGuarantorPerformAction = guarantor.User.VisitPayUserId == actionVisitPayUserId;

            if (!didVpGuarantorPerformAction) {
                this._visitPayUserJournalEventApplicationService.Value.LogClientAddedPaymentMethod(actionVisitPayUserId, vpGuarantorId, guarantorUsername, isPrimary, paymentMethodId, context);
            }
            else {
                this._visitPayUserJournalEventApplicationService.Value.LogGuarantorAddedPaymentMethod(guarantor.User.VisitPayUserId, vpGuarantorId, guarantorUsername, isPrimary, paymentMethodId, context);
            }
        }

        private void ShowAlertForHqyCards(int paymentMethodProviderTypeId, int vpGuarantorId)
        {
            if (this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.HealthEquityFeature)
                && paymentMethodProviderTypeId == (int)PaymentMethodProviderTypeEnum.Hqy)
            {
                SsoProviderEnum selectHealthEquitySsoProvider = this._ssoApplicationService.Value.SelectHealthEquitySsoProvider();
                Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
                this._ssoApplicationService.Value.IgnoreSso(selectHealthEquitySsoProvider, guarantor.User.VisitPayUserId, false);
            }
        }

        private void ShowNotificationForHqyCards(int paymentMethodProviderTypeId, int vpGuarantorId)
        {
            if (this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.HealthEquityFeature)
                && paymentMethodProviderTypeId == (int)PaymentMethodProviderTypeEnum.Hqy)
            {
                this.Bus.Value.PublishMessage(new AddSystemMessageVisitPayUserByGuarantorMessage { VpGuarantorId = vpGuarantorId, SystemMessageEnum = SystemMessageEnum.Hqy }).Wait();
            }
        }

        private static string LastCharacters(string input)
        {
            return string.IsNullOrEmpty(input) ? null : string.Join(string.Empty, input.Reverse().Take(4).Reverse());
        }

        #region jobs

        public void NotifyExpiringPrimaryPaymentMethods(DateTime processDate)
        {
            // check expired credit cards
            IEnumerable<SendCardExpiredEmailMessage> expiredMessages = this.GetPaymentMethodExpiredMessages(processDate);
            foreach (SendCardExpiredEmailMessage message in expiredMessages)
            {
                this._session.Transaction.RegisterPostCommitSuccessAction(m =>
                {
                    this.Bus.Value.PublishMessage(m).Wait();
                }, message);
            }

            // check for expiring credit cards
            IEnumerable<SendCardExpiringEmailMessage> expiringMessages = this.GetPaymentMethodExpiringMessages(processDate);
            foreach (SendCardExpiringEmailMessage message in expiringMessages)
            {
                this._session.Transaction.RegisterPostCommitSuccessAction(m =>
                {
                    this.Bus.Value.PublishMessage(m).Wait();
                }, message);
            }
        }

        public IEnumerable<SendCardExpiredEmailMessage> GetPaymentMethodExpiredMessages(DateTime processDate)
        {
            // check expired credit cards
            if (!processDate.IsLastDayOfMonth())
            {
                return Enumerable.Empty<SendCardExpiredEmailMessage>();
            }

            IList<SendCardExpiredEmailMessage> messages = new List<SendCardExpiredEmailMessage>();

            string expDate = processDate.ToExpDate();
            IList<PaymentMethod> expiredPaymentMethods = this._paymentMethodsService.Value.GetExpiringPrimaryPaymentMethods(expDate);
            foreach (PaymentMethod paymentMethod in expiredPaymentMethods)
            {
                if (!paymentMethod.VpGuarantorId.HasValue)
                {
                    continue;
                }

                bool notify = this.MeetsCriteriaForMessage(paymentMethod, expDate);
                if (notify)
                {
                    messages.Add(new SendCardExpiredEmailMessage
                    {
                        VpGuarantorId = paymentMethod.VpGuarantorId.Value,
                        ExpiryDate = paymentMethod.ExpDateForDisplay
                    });
                }
            }

            return messages;
        }

        public IEnumerable<SendCardExpiringEmailMessage> GetPaymentMethodExpiringMessages(DateTime processDate)
        {
            DateTime expiringDate = processDate.AddDays(this.Client.Value.ExpiringCreditCardReminderIntervalInDays);
            if (!expiringDate.IsLastDayOfMonth())
            {
                return Enumerable.Empty<SendCardExpiringEmailMessage>();
            }

            IList<SendCardExpiringEmailMessage> messages = new List<SendCardExpiringEmailMessage>();

            string expDate = expiringDate.ToExpDate();
            IList<PaymentMethod> expiringPaymentMethods = this._paymentMethodsService.Value.GetExpiringPrimaryPaymentMethods(expDate);
            foreach (PaymentMethod paymentMethod in expiringPaymentMethods)
            {
                if (!paymentMethod.VpGuarantorId.HasValue)
                {
                    continue;
                }

                bool notify = this.MeetsCriteriaForMessage(paymentMethod, expDate);
                if (notify)
                {
                    messages.Add(new SendCardExpiringEmailMessage
                    {
                        VpGuarantorId = paymentMethod.VpGuarantorId.Value,
                        ExpiryDate = paymentMethod.ExpDateForDisplay
                    });
                }
            }

            return messages;
        }

        private bool MeetsCriteriaForMessage(PaymentMethod paymentMethod, string expDate)
        {
            if (paymentMethod.IsActive && paymentMethod.IsPrimary && paymentMethod.ExpDate == expDate)
            {
                int financePlanCount = this._financePlanService.Value.GetActiveFinancePlanCount(paymentMethod.VpGuarantorId ?? 0);
                if (financePlanCount > 0)
                {
                    return true;
                }

                decimal totalBalance = this._visitBalanceBaseApplicationService.Value.GetTotalBalance(paymentMethod.VpGuarantorId ?? 0);
                if (totalBalance > 0)
                {
                    return true;
                }
            }

            return false;
        }

        void IJobRunnerService<QueuePaymentMethodJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.NotifyExpiringPrimaryPaymentMethods(DateTime.UtcNow.Date);
        }

        #endregion
    }
}
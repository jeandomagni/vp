﻿namespace Ivh.Application.FinanceManagement.ApplicationServices
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Content.Common.Dtos;
    using Content.Common.Interfaces;
    using Core.Common.Dtos;
    using Core.Common.Utilities.Builders;
    using Domain.AppIntelligence.Entities;
    using Domain.AppIntelligence.Interfaces;
    using Domain.Content.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Interfaces;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.RoboRefund.Entities;
    using Domain.FinanceManagement.RoboRefund.Interfaces;
    using Domain.FinanceManagement.Statement.Entities;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.Logging.Interfaces;
    using Domain.User.Interfaces;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.EventJournal;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Jobs;
    using Ivh.Common.VisitPay.Messages.AppIntelligence;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using Ivh.Common.VisitPay.Strings;
    using MassTransit;
    using Newtonsoft.Json;
    using NHibernate;
    using OfficeOpenXml;
    using User.Common.Dtos;

    public class FinancePlanApplicationService : ApplicationService, IFinancePlanApplicationService
    {
        private readonly Lazy<IContentApplicationService> _contentApplicationService;
        private readonly Lazy<IAmortizationService> _amortizationService;
        private readonly Lazy<IBalanceTransferService> _balanceTransferService;
        private readonly Lazy<IFinancePlanService> _financePlanService;
        private readonly Lazy<IFinancePlanOfferService> _financePlanOfferService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IVpStatementService> _statementService;
        private readonly Lazy<IStatementDateService> _statementDateService;
        private readonly Lazy<IVisitBalanceBaseApplicationService> _visitBalanceBaseApplicationService;
        private readonly Lazy<IPaymentService> _paymentService;
        private readonly Lazy<IPaymentReversalApplicationService> _paymentReversalApplicationService;
        private readonly ISession _session;
        private readonly Lazy<IVisitService> _visitService;
        private readonly Lazy<IInterestService> _interestService;
        private readonly Lazy<IVisitPayUserJournalEventService> _userJournalEventService;
        private readonly Lazy<IImageService> _imageService;
        private readonly Lazy<IMetricsProvider> _metricsProvider;
        private readonly Lazy<ICreditAgreementApplicationService> _creditAgreementApplicationService;
        private readonly Lazy<IRoboRefundService> _roboRefundService;
        private readonly Lazy<IRandomizedTestService> _randomizedTestService;
        private readonly Lazy<IGuarantorScoreService> _scoringService;
        private readonly Lazy<IFinancePlanIncentiveService> _financePlanIncentiveService;

        public FinancePlanApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IAmortizationService> amortizationService,
            Lazy<IBalanceTransferService> balanceTransferService,
            Lazy<IFinancePlanService> financePlanService,
            Lazy<IFinancePlanOfferService> financePlanOfferService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IPaymentService> paymentService,
            Lazy<IPaymentReversalApplicationService> paymentReversalApplicationService,
            Lazy<IVpStatementService> statementService,
            Lazy<IStatementDateService> statementDateService,
            Lazy<IVisitBalanceBaseApplicationService> visitBalanceBaseApplicationService,
            ISessionContext<VisitPay> sessionContext,
            Lazy<IVisitService> visitService,
            Lazy<IInterestService> interestService,
            Lazy<IVisitPayUserJournalEventService> userJournalEventService,
            Lazy<IImageService> imageService,
            Lazy<IMetricsProvider> metricsProvider,
            Lazy<ICreditAgreementApplicationService> creditAgreementApplicationService,
            Lazy<IRoboRefundService> roboRefundService,
            Lazy<IRandomizedTestService> randomizedTestService,
            Lazy<IGuarantorScoreService> scoringService,
            Lazy<IFinancePlanIncentiveService> financePlanIncentiveService
        ) : base(applicationServiceCommonService)
        {
            this._contentApplicationService = contentApplicationService;
            this._amortizationService = amortizationService;
            this._balanceTransferService = balanceTransferService;
            this._financePlanService = financePlanService;
            this._financePlanOfferService = financePlanOfferService;
            this._guarantorService = guarantorService;
            this._paymentService = paymentService;
            this._paymentReversalApplicationService = paymentReversalApplicationService;
            this._statementService = statementService;
            this._statementDateService = statementDateService;
            this._visitBalanceBaseApplicationService = visitBalanceBaseApplicationService;
            this._session = sessionContext.Session;
            this._visitService = visitService;
            this._interestService = interestService;
            this._userJournalEventService = userJournalEventService;
            this._imageService = imageService;
            this._metricsProvider = metricsProvider;
            this._creditAgreementApplicationService = creditAgreementApplicationService;
            this._roboRefundService = roboRefundService;
            this._randomizedTestService = randomizedTestService;
            this._scoringService = scoringService;
            this._financePlanIncentiveService = financePlanIncentiveService;
        }

        public int? GetFirstTierMaxDuration(FinancePlanCalculationParametersDto financePlanCalculationParametersDto)
        {
            FinancePlanCalculationParameters financePlanCalculationParameters = this.MapFinancePlanCalculationParameters(financePlanCalculationParametersDto);

            IList<FinancePlanOffer> allOffers = this._financePlanService.Value.GetOffers(financePlanCalculationParameters, false);
            allOffers = allOffers?.Where(x => x != null).ToList();

            FinancePlanOffer shortestOfferByTierDuration = allOffers?.OrderBy(x => x.DurationRangeEnd).FirstOrDefault();

            return shortestOfferByTierDuration?.DurationRangeEnd;
        }

        public bool IsEligibleForAnyFinancePlan(int vpGuarantorId, int currentVisitPayUserId)
        {
            // look for eligibility for a stacked plan
            FinancePlanCalculationParametersDto financePlanCalculationParametersDto = FinancePlanCalculationParametersDto.CreateForTermsCheck(vpGuarantorId, null, false);
            bool canCreateStackedPlan = this.IsEligibleForFinancePlan(financePlanCalculationParametersDto, currentVisitPayUserId);
            if (canCreateStackedPlan)
            {
                return true;
            }

            // look for eligibility for a combined plan
            bool canCreateCombinedPlan = this.IsGuarantorEligibleToCombineFinancePlans(currentVisitPayUserId, vpGuarantorId, true);

            return canCreateCombinedPlan;
        }

        public FinancePlanBoundaryDto GetMinimumPaymentAmount(FinancePlanCalculationParametersDto financePlanCalculationParametersDto)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(financePlanCalculationParametersDto.VpGuarantorId);
            VpStatement currentStatement = this._statementService.Value.GetMostRecentStatement(guarantor);
            if (currentStatement == null)
            {
                return new FinancePlanBoundaryDto(-1m, -1);
            }

            FinancePlanCalculationParameters financePlanCalculationParameters = this.MapFinancePlanCalculationParameters(financePlanCalculationParametersDto, guarantor, currentStatement);

            IList<FinancePlanOffer> offers = this._financePlanService.Value.GetOffers(financePlanCalculationParameters, false);

            if (offers.IsNullOrEmpty() || offers.All(x => x == null))
            {
                return new FinancePlanBoundaryDto(-1m, -1);
            }

            FinancePlanOffer foundOffer = offers.OrderBy(x => x.MinPayment).FirstOrDefault();
            if (foundOffer == null)
            {
                return new FinancePlanBoundaryDto(-1m, -1);
            }

            int amortizationDuration = this._amortizationService.Value.GetAmortizationDurationForAmounts(
                foundOffer.MinPayment,
                financePlanCalculationParameters.Visits,
                foundOffer.InterestRate,
                financePlanCalculationParameters.InterestFreePeriod,
                financePlanCalculationParameters.FirstPaymentDueDate,
                financePlanCalculationParameters.InterestCalculationMethod);

            return new FinancePlanBoundaryDto(foundOffer.MinPayment, amortizationDuration);
        }

        public bool IsEligibleForFinancePlan(FinancePlanCalculationParametersDto financePlanCalculationParametersDto, int currentVisitPayUserId)
        {
            return this.GetEligibleBalanceForFinancePlan(financePlanCalculationParametersDto, currentVisitPayUserId) > 0m;
        }

        public decimal GetEligibleBalanceForFinancePlan(
            FinancePlanCalculationParametersDto financePlanCalculationParametersDto,
            int currentVisitPayUserId)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(financePlanCalculationParametersDto.VpGuarantorId);
            if (guarantor.User.VisitPayUserId == currentVisitPayUserId && guarantor.IsManaged())
            {
                return 0m;
            }

            VpStatement statement = this._statementService.Value.GetMostRecentStatement(guarantor);
            if (statement == null)
            {
                return 0m;
            }

            FinancePlanBoundaryDto financePlanBoundaryDto = this.GetMinimumPaymentAmount(financePlanCalculationParametersDto);
            if (financePlanBoundaryDto.MinimumPaymentAmount <= 0)
            {
                return 0m;
            }

            FinancePlanCalculationParameters financePlanCalculationParameters = this.MapFinancePlanCalculationParameters(financePlanCalculationParametersDto, guarantor, statement);

            decimal eligibleBalance = financePlanCalculationParameters.Visits.Sum(x => x.CurrentBalance);
            if (eligibleBalance >= financePlanBoundaryDto.MinimumPaymentAmount)
            {
                if (financePlanCalculationParametersDto.CombineFinancePlans)
                {
                    decimal openFinancePlanBalance = this._financePlanService.Value.GetAllOpenFinancePlansForGuarantor(guarantor).Sum(x => x.CurrentFinancePlanBalance);
                    return eligibleBalance - openFinancePlanBalance;
                }
                return eligibleBalance;
            }

            return 0m;
        }

        public FinancePlanDto GetFinancePlan(int visitPayUserId, int financePlanId, int? vpGuarantorId = null)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantorEx(visitPayUserId, vpGuarantorId);

            FinancePlan financePlan = this._financePlanService.Value.GetFinancePlan(guarantor, financePlanId);
            IList<AmortizationMonth> amortizationMonths = this.GetAmortizationMonthsForFinancePlan(financePlan, guarantor);

            FinancePlanDto financePlanDto = Mapper.Map<FinancePlanDto>(financePlan);

            if (amortizationMonths.Any())
            {
                financePlanDto.LastPaymentDate = amortizationMonths.OrderByDescending(x => x.MonthDate).First().MonthDate;
                financePlanDto.MonthsRemaining = amortizationMonths.Count;
            }

            return Mapper.Map<FinancePlanDto>(financePlanDto);
        }

        public FinancePlanDto GetFinancePlanByOfferId(int financePlanOfferId)
        {
            FinancePlan financePlan = this._financePlanService.Value.GetFinancePlanByOfferId(financePlanOfferId);
            return Mapper.Map<FinancePlanDto>(financePlan);
        }

        public FinancePlanResultsDto GetFinancePlans(int visitPayUserId, FinancePlanFilterDto filterDto, int pageNumber, int pageSize, int? vpGuarantorId = null)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantorEx(visitPayUserId, vpGuarantorId);
            FinancePlanFilter filter = Mapper.Map<FinancePlanFilter>(filterDto);
            FinancePlanResults financePlanResults = this._financePlanService.Value.GetFinancePlans(guarantor.VpGuarantorId, filter, Math.Max(pageNumber - 1, 0), pageSize);

            IList<FinancePlanDto> financePlansDto = new List<FinancePlanDto>();
            foreach (FinancePlan financePlan in financePlanResults.FinancePlans)
            {
                IList<AmortizationMonth> amortizationMonths = this.GetAmortizationMonthsForFinancePlan(financePlan, guarantor);

                FinancePlanDto financePlanDto = Mapper.Map<FinancePlanDto>(financePlan);

                if (amortizationMonths.Any())
                {
                    financePlanDto.LastPaymentDate = amortizationMonths.OrderByDescending(x => x.MonthDate).First().MonthDate;
                }

                financePlansDto.Add(financePlanDto);
            }

            foreach (FinancePlanDto financePlanDto in financePlansDto)
            {
                financePlanDto.FinancePlanVisits = financePlanDto.FinancePlanVisits.Where(x => x.HardRemoveDate == null).ToList();
            }

            return new FinancePlanResultsDto
            {
                TotalRecords = financePlanResults.TotalRecords,
                TotalCurrentBalance = financePlanResults.TotalCurrentBalance,
                FinancePlans = financePlansDto
            };
        }

        public int GetActiveFinancePlanCount(int visitPayUserId, int? vpGuarantorId = null)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantorEx(visitPayUserId, vpGuarantorId);
            return this._financePlanService.Value.GetActiveFinancePlanCount(guarantor.VpGuarantorId);
        }

        public bool HasActiveFinancePlans(int visitPayUserId, int? vpGuarantorId = null, bool considerConsolidation = false)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantorEx(visitPayUserId, vpGuarantorId);
            return this._financePlanService.Value.AnyOpenFinancePlansForGuarantor(guarantor, considerConsolidation);
        }

        public bool HasFinancePlans(int visitPayUserId, IList<FinancePlanStatusEnum> statuses, int? vpGuarantorId = null)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantorEx(visitPayUserId, vpGuarantorId);
            return this._financePlanService.Value.HasFinancePlans(guarantor, statuses);
        }

        public bool IsGuarantorEligibleToCombineFinancePlans(int currentVisitPayUserId, int vpGuarantorId, bool checkTerms, string financePlanStateCode = null)
        {
            Guarantor currentGuarantor = this._guarantorService.Value.GetGuarantorEx(currentVisitPayUserId);
            if (currentGuarantor.ConsolidationStatus == GuarantorConsolidationStatusEnum.Managed)
            {
                return false;
            }

            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);

            VpStatement statement = this._statementService.Value.GetMostRecentStatement(guarantor);
            decimal totalBalance = this._visitBalanceBaseApplicationService.Value.GetTotalBalance(vpGuarantorId);

            if (!this._financePlanService.Value.IsGuarantorEligibleToCombineFinancePlans(guarantor, totalBalance, financePlanStateCode))
            {
                return false;
            }

            if (!checkTerms)
            {
                return true;
            }

            FinancePlanCalculationParametersDto financePlanCalculationParametersDto = FinancePlanCalculationParametersDto.CreateForTermsCheck(vpGuarantorId, null, true, stateCodeForVisits: financePlanStateCode);
            FinancePlanBoundaryDto boundary = this.GetMinimumPaymentAmount(financePlanCalculationParametersDto);
            if (boundary.MinimumPaymentAmount <= 0)
            {
                return false;
            }

            FinancePlanCalculationParameters parameters = this.MapFinancePlanCalculationParameters(financePlanCalculationParametersDto, guarantor, statement);
            if (parameters.Visits.Sum(x => x.CurrentBalance) < boundary.MinimumPaymentAmount)
            {
                return false;
            }

            return true;
        }

        public FinancePlanCountDto GetFinancePlanTotals(int visitPayUserId, FinancePlanFilterDto filterDto, int? vpGuarantorId = null)
        {
            vpGuarantorId = vpGuarantorId ?? this._guarantorService.Value.GetGuarantorId(visitPayUserId);
            FinancePlanFilter filter = Mapper.Map<FinancePlanFilter>(filterDto);
            return Mapper.Map<FinancePlanCountDto>(this._financePlanService.Value.GetFinancePlanTotals(vpGuarantorId.Value, filter));
        }

        public IList<int> GetVisitIdsOnPendingFinancePlans(int vpGuarantorId)
        {
            return this._financePlanService.Value.GetVisitIdsOnPendingFinancePlans(vpGuarantorId);
        }
        
        public IList<FinancePlanStatusDto> GetFinancePlanStatuses()
        {
            return Mapper.Map<List<FinancePlanStatusDto>>(this._financePlanService.Value.GetFinancePlanStatuses());
        }

        public IList<FinancePlanTypeDto> GetFinancePlanTypes()
        {
            return Mapper.Map<List<FinancePlanTypeDto>>(this._financePlanService.Value.GetFinancePlanTypes());
        }


        public IList<InterestRateDto> GetInterestRates(FinancePlanCalculationParametersDto financePlanCalculationParametersDto)
        {
            FinancePlanCalculationParameters parameters = this.MapFinancePlanCalculationParameters(financePlanCalculationParametersDto);

            bool hasFinancialAssistance = this._financePlanService.Value.HasFinancialAssistance(parameters.Visits);
            decimal totalAmountToConsider = parameters.Visits.Sum(x => x.CurrentBalance);

            IList<FinancePlan> allOpenFinancePlans = financePlanCalculationParametersDto.CombineFinancePlans ? new List<FinancePlan>() : this._financePlanService.Value.GetAllOpenFinancePlansForGuarantor(parameters.Guarantor);

            return Mapper.Map<List<InterestRateDto>>(this._interestService.Value.GetInterestRates(totalAmountToConsider, Mapper.Map<IList<FinancePlan>>(allOpenFinancePlans), hasFinancialAssistance, parameters.Guarantor.VpGuarantorId, parameters.FinancePlanOfferSetTypeId, parameters.VpGuarantorScore?.Score));
        }

        public decimal GetTotalPaymentAmountForOpenFinancePlans(int vpGuarantorId)
        {
            return this._financePlanService.Value.GetTotalPaymentAmountForOpenFinancePlans(vpGuarantorId);
        }

        public IList<ResubmitPaymentDto> GetPaymentResubmitDtosForFinancePlans(int vpGuarantorId)
        {
            IList<ResubmitPaymentDto> resubmits = new List<ResubmitPaymentDto>();
            IList<FinancePlan> financePlansDueForRecurringPaymentByGuarantor = this._financePlanService.Value.GetFinancePlansWithAmountDueByGuarantor(DateTime.UtcNow, vpGuarantorId);
            foreach (FinancePlan financePlan in financePlansDueForRecurringPaymentByGuarantor)
            {
                if (financePlan.HasPastDueBucketAndRecurringPaymentAttempted())
                {
                    resubmits.Add(new ResubmitPaymentDto()
                    {
                        FinancePlanId = financePlan.FinancePlanId,
                        LastPaymentFailureDate = financePlan.FindLastFailedPaymentAmountDue()?.InsertDate ?? DateTime.UtcNow,
                        PaymentType = PaymentTypeEnum.RecurringPaymentFinancePlan,
                        SnapshotTotalPaymentAmount = financePlan.AmountDue
                    });
                }
            }

            return resubmits;
        }

        public bool HasFinancePlansWhichShouldResubmit(int vpGuarantorId)
        {
            IList<FinancePlan> financePlansDueForRecurringPaymentByGuarantor = this._financePlanService.Value.GetFinancePlansWithAmountDueByGuarantor(DateTime.UtcNow, vpGuarantorId);
            if (financePlansDueForRecurringPaymentByGuarantor.Any(x => x.HasPastDueBucketAndRecurringPaymentAttempted()))
            {
                return true;
            }

            return false;
        }

        #region terms

        public async Task<CalculateFinancePlanTermsResponseDto> GetTermsByMonthlyPaymentAmountAsync(FinancePlanCalculationParametersDto financePlanCalculationParametersDto)
        {
            FinancePlanBoundaryDto boundary = this.GetMinimumPaymentAmount(financePlanCalculationParametersDto);
            CalculateFinancePlanTermsResponseDto response = null;
            if (boundary.MinimumPaymentAmount <= financePlanCalculationParametersDto.MonthlyPaymentAmount)
            {
                response = this.GetTerms(this.MapFinancePlanCalculationParameters(financePlanCalculationParametersDto), boundary);
                if (!this.ValidateResponseOffer(boundary, response))
                {
                    response = null;
                }
            }

            if (response != null)
            {
                await this._creditAgreementApplicationService.Value.CacheCreditAgreementTermsAsync(response.Terms);
                return response;
            }

            CmsVersionDto cmsVersionDto = await this._contentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.ArrangePaymentFinancePlanMinimumAmountError);

            CalculateFinancePlanTermsResponseDto dto = new CalculateFinancePlanTermsResponseDto(cmsVersionDto.ContentBody, boundary);
            return dto;
        }

        public async Task<CalculateFinancePlanTermsResponseDto> GetTermsByNumberOfMonthlyPaymentsAsync(FinancePlanCalculationParametersDto financePlanCalculationParametersDto)
        {
            FinancePlanBoundaryDto boundary = this.GetMinimumPaymentAmount(financePlanCalculationParametersDto);
            CalculateFinancePlanTermsResponseDto response = null;
            if (financePlanCalculationParametersDto.NumberOfMonthlyPayments <= boundary.MaximumNumberOfPayments)
            {
                FinancePlanCalculationParameters financePlanCalculationParameters = this.MapFinancePlanCalculationParameters(financePlanCalculationParametersDto);

                decimal monthlyPaymentAmount = this._financePlanService.Value.GetPaymentAmountForNumberOfPayments(financePlanCalculationParameters, financePlanCalculationParametersDto.NumberOfMonthlyPayments.GetValueOrDefault(0));
                financePlanCalculationParameters.MonthlyPaymentAmount = boundary.MinimumPaymentAmount > monthlyPaymentAmount ? boundary.MinimumPaymentAmount : monthlyPaymentAmount;

                response = this.GetTerms(financePlanCalculationParameters, boundary);
                if (!this.ValidateResponseOffer(boundary, response))
                {
                    response = null;
                }
            }

            if (response != null)
            {
                await this._creditAgreementApplicationService.Value.CacheCreditAgreementTermsAsync(response.Terms);
                return response;
            }

            CmsVersionDto cmsVersionDto = await this._contentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.ArrangePaymentFinancePlanMaximumPaymentsError);

            CalculateFinancePlanTermsResponseDto dto = new CalculateFinancePlanTermsResponseDto(cmsVersionDto.ContentBody, boundary);
            return dto;
        }

        public async Task<CalculateFinancePlanTermsResponseDto> GetTermsOnPendingFinancePlanAsync(int visitPayUserId, int? vpGuarantorId = null)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantorEx(visitPayUserId, vpGuarantorId);
            FinancePlan financePlan = this._financePlanService.Value.GetAllPendingFinancePlansForGuarantor(guarantor).FirstOrDefault();

            if (financePlan == null)
            {
                return null;
            }

            // this finance plan has already been saved, so, any payment due date change has already happened
            // just use the next payment date
            DateTime firstPaymentDueDate = this._statementService.Value.GetNextPaymentDate(guarantor);

            IList<AmortizationMonth> amortizationMonths = this._amortizationService.Value.GetAmortizationMonthsForPendingFinancePlan(
                financePlan,
                financePlan.FirstInterestPeriod(), 
                firstPaymentDueDate);

            FinancePlanTermsDto terms = this.CreateTerms(
                financePlan.FinancePlanOffer,
                amortizationMonths, 
                financePlan.CreatedVpStatement, 
                financePlan.PaymentAmount, 
                financePlan.IsCombined, 
                firstPaymentDueDate);
            
            terms.FinancePlanId = financePlan.FinancePlanId;

            await this._creditAgreementApplicationService.Value.CacheCreditAgreementTermsAsync(terms);

            CalculateFinancePlanTermsResponseDto response = new CalculateFinancePlanTermsResponseDto(terms, financePlan.FinancePlanStatus.FinancePlanStatusEnum, Mapper.Map<FinancePlanDto>(financePlan));

            return response;
        }

        private CalculateFinancePlanTermsResponseDto GetTerms(FinancePlanCalculationParameters parameters, FinancePlanBoundaryDto boundary)
        {
            FinancePlanOffer selectedOffer = this._financePlanService.Value.SelectBestOfferWithPaymentAmount(parameters, null);
            if (selectedOffer == null)
            {
                return null;
            }

            IList<AmortizationMonth> amortizationMonths = this._amortizationService.Value.GetAmortizationMonthsForAmounts(
                parameters.MonthlyPaymentAmount,
                parameters.Visits,
                selectedOffer.InterestRate,
                parameters.InterestFreePeriod,
                parameters.FirstPaymentDueDate,
                parameters.InterestCalculationMethod);

            FinancePlanTermsDto terms = this.CreateTerms(
                selectedOffer, 
                amortizationMonths, 
                parameters.Statement, 
                parameters.MonthlyPaymentAmount, 
                parameters.CombineFinancePlans, 
                parameters.FirstPaymentDueDate);

            CalculateFinancePlanTermsResponseDto response = new CalculateFinancePlanTermsResponseDto(terms, boundary);

            return response;
        }

        private FinancePlanTermsDto CreateTerms(FinancePlanOffer offer, IList<AmortizationMonth> amortizationMonths, VpStatement statement, decimal monthlyPaymentAmount, bool isCombinedFinancePlan, DateTime firstPaymentDueDate)
        {
            AmortizationMonth lastPayment = amortizationMonths.OrderByDescending(x => x.MonthDate).FirstOrDefault();
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(offer.VpGuarantor.VpGuarantorId);

            decimal monthlyPaymentAmountOtherPlans = this._financePlanService.Value.GetTotalPaymentAmountForOpenFinancePlans(offer.VpGuarantor.VpGuarantorId);
            decimal rolloverBalance = 0m;
            decimal rolloverInterest = 0m;
            int numberMonthlyPayments = amortizationMonths.Count;

            if (isCombinedFinancePlan)
            {
                // rollover amount - sum of any finance plans included in the new combo plan
                IList<FinancePlan> openFinancePlans = this._financePlanService.Value.GetAllOpenFinancePlansForGuarantor(guarantor);

                rolloverBalance = openFinancePlans.Sum(x => x.CurrentFinancePlanBalance);
                rolloverInterest = openFinancePlans.Sum(x => x.InterestDue);
            }

            CmsRegionEnum termsCmsRegionEnum;

            bool requireRetailInstallmentContract = this._financePlanService.Value.RequireRetailInstallmentContract(offer, numberMonthlyPayments);
            if (requireRetailInstallmentContract)
            {
                // find the appropriate RIC
                termsCmsRegionEnum = this._visitService.Value.GetRicCmsRegionEnum(offer.VpGuarantor.VpGuarantorId);
                if (termsCmsRegionEnum == CmsRegionEnum.Unknown)
                {
                    termsCmsRegionEnum = CmsRegionEnum.VppCreditAgreement;
                }
            }
            else
            {
                // use the material terms
                termsCmsRegionEnum = CmsRegionEnum.FinancePlanSimpleTerms;
            }

            CmsVersionDto termsCmsVersion = this._contentApplicationService.Value.GetCurrentVersionAsync(termsCmsRegionEnum, false).Result;
            string agreementTitle = this._creditAgreementApplicationService.Value.GetAgreementTitle(requireRetailInstallmentContract);

            FinancePlanTermsDto terms = new FinancePlanTermsDto
            {
                IsCombined = isCombinedFinancePlan,
                EffectiveDate = this._financePlanService.Value.GetOriginationDate(guarantor, statement),
                FinalPaymentDate = lastPayment?.MonthDate ?? DateTime.MinValue,
                FinancePlanBalance = offer.StatementedCurrentAccountBalance,
                FinancePlanOfferId = offer.FinancePlanOfferId,
                FinancePlanOfferSetType = Mapper.Map<FinancePlanOfferSetTypeDto>(offer.FinancePlanOfferSetType),
                InterestRate = offer.InterestRate,
                MonthlyPaymentAmount = monthlyPaymentAmount,
                MonthlyPaymentAmountLast = lastPayment?.Total ?? 0m,
                MonthlyPaymentAmountOtherPlans = monthlyPaymentAmountOtherPlans,
                MonthlyPaymentAmountPrevious = monthlyPaymentAmountOtherPlans,
                MonthlyPaymentAmountTotal = isCombinedFinancePlan ? monthlyPaymentAmount : monthlyPaymentAmount + monthlyPaymentAmountOtherPlans,
                NextPaymentDueDate = firstPaymentDueDate,
                NumberMonthlyPayments = numberMonthlyPayments,
                PaymentDueDay = firstPaymentDueDate.Day,
                RequireCreditAgreement = !this.ApplicationSettingsService.Value.IsClientApplication.Value,
                RolloverBalance = rolloverBalance,
                RolloverInterest = rolloverInterest,
                TotalInterest = Math.Max(0, amortizationMonths.Sum(x => x.InterestAmount) - rolloverInterest), // rollover interest is included in the balance
                VpGuarantorId = offer.VpGuarantor.VpGuarantorId,
                DownpaymentAmount = 0m,
                TermsCmsRegionEnum = termsCmsRegionEnum,
                TermsCmsVersionId = termsCmsVersion.CmsVersionId,
                TermsAgreementTitle = agreementTitle,
                IsRetailInstallmentContract = requireRetailInstallmentContract
            };

            return terms;
        }

        private bool ValidateResponseOffer(FinancePlanBoundaryDto boundaryDto, CalculateFinancePlanTermsResponseDto responseDto)
        {
            if (responseDto?.Terms == null)
            {
                return false;
            }

            if (boundaryDto.MaximumNumberOfPayments < responseDto.Terms.NumberMonthlyPayments)
            {
                return false;
            }

            if (boundaryDto.MinimumPaymentAmount > responseDto.Terms.MonthlyPaymentAmount)
            {
                return false;
            }

            return true;
        }

        #endregion

        #region create / update

        public async Task<CreateFinancePlanResponse> CreateFinancePlanAsync(int currentVisitPayUserId, int filteredVisitPayUserId, FinancePlanCalculationParametersDto financePlanCalculationParametersDto, int termsCmsVersionId, int financePlanOfferId, int loggedInVisitPayUserId, JournalEventHttpContextDto httpContext)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(financePlanCalculationParametersDto.VpGuarantorId);
            if (currentVisitPayUserId == filteredVisitPayUserId && guarantor.IsManaged())
            {
                throw new Exception("Managed user cannot create a finance plan.");
            }

            VpStatement currentStatement = this._statementService.Value.GetMostRecentStatement(guarantor);
            if (currentStatement?.VpStatementId != financePlanCalculationParametersDto.StatementId)
            {
                int statementId = currentStatement?.VpStatementId ?? -1;
                throw new Exception($"Current statement ({statementId}) was different than statementId passed in ({financePlanCalculationParametersDto.StatementId}).");
            }

            FinancePlanCalculationParameters financePlanCalculationParameters = this.MapFinancePlanCalculationParameters(financePlanCalculationParametersDto, guarantor, currentStatement);
            FinancePlanCreditAgreementCollectionDto creditAgreementDto = await this._creditAgreementApplicationService.Value.GetCachedCreditAgreementAsync(financePlanOfferId, termsCmsVersionId, false);
            FinancePlan financePlan = null;

            Exception workException = null;
            UnitOfWork unitOfWork = new UnitOfWork(this._session);
            try
            {
                financePlan = this._financePlanService.Value.CreateFinancePlan(
                    financePlanCalculationParameters,
                    loggedInVisitPayUserId,
                    creditAgreementDto,
                    httpContext);

                unitOfWork.Commit();
            }
            catch(Exception e)
            {
                workException = e;
                this.LoggingService.Value.Fatal(() => $"Failed To Create Finance Plan - {nameof(FinancePlanApplicationService)}::{nameof(this.CreateFinancePlanAsync)}" + 
                    Mapper.Map<FinancePlanDto>(financePlan).EntityToJsonSafe());
            }
            finally
            {
                unitOfWork.Dispose();
            }

            if (workException != null)
            {
                throw workException;
            }


            return this.PostFinancePlanCreate(financePlan);
        }

        public CreateFinancePlanResponse CreateFinancePlanPendingAcceptance(FinancePlanCalculationParametersDto financePlanCalculationParametersDto, int loggedInVisitPayUserId, JournalEventHttpContextDto httpContext)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(financePlanCalculationParametersDto.VpGuarantorId);
            VpStatement currentStatement = this._statementService.Value.GetMostRecentStatement(guarantor);
            if (currentStatement?.VpStatementId != financePlanCalculationParametersDto.StatementId)
            {
                int statementId = currentStatement?.VpStatementId ?? -1;
                throw new Exception($"Current statement ({statementId}) was different than statementId passed in ({financePlanCalculationParametersDto.StatementId}).");
            }

            FinancePlan financePlan;
            
            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                financePlan = this._financePlanService.Value.CreateFinancePlan(
                    this.MapFinancePlanCalculationParameters(financePlanCalculationParametersDto, guarantor, currentStatement),
                    loggedInVisitPayUserId,
                    null,
                    httpContext);

                uow.Commit();
            }

            if ((financePlan?.IsFinancePlanOfferTypeClientOnly).GetValueOrDefault(false))
            {
                this._metricsProvider.Value.Increment(Metrics.Increment.FinancePlan.ClientExtendedFinancePlanOffer);
            }

            this.SendFinancePlanCreationEmail(currentStatement, guarantor);

            return new CreateFinancePlanResponse(false, null, Mapper.Map<FinancePlanDto>(financePlan));
        }

        public async Task<CreateFinancePlanResponse> UpdatePendingFinancePlanAsync(FinancePlanCalculationParametersDto financePlanCalculationParametersDto, int financePlanId, int financePlanOfferId, int visitPayUserId, int? termsCmsVersionId, int loggedInVisitPayUserId, JournalEventHttpContextDto httpContext)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantorEx(visitPayUserId, financePlanCalculationParametersDto.VpGuarantorId);
            FinancePlan pendingFinancePlan = this._financePlanService.Value.GetAllPendingFinancePlansForGuarantor(guarantor).FirstOrDefault(x => x.FinancePlanId == financePlanId);

            bool clientIsUpdating = visitPayUserId != 0 && guarantor.User.VisitPayUserId != visitPayUserId;
            if (clientIsUpdating)
            {
                termsCmsVersionId = null;
            }

            if (pendingFinancePlan == null)
            {
                return new CreateFinancePlanResponse(true, $"This finance plan has been canceled by {this.Client.Value.ClientBrandName}.", null);
            }

            VpStatement currentStatement = this._statementService.Value.GetMostRecentStatement(guarantor);
            if (currentStatement?.VpStatementId != financePlanCalculationParametersDto.StatementId)
            {
                int statementId = currentStatement?.VpStatementId ?? -1;
                throw new Exception($"Current statement ({statementId}) was different than statementId passed in ({financePlanCalculationParametersDto.StatementId}).");
            }

            FinancePlanCalculationParameters financePlanCalculationParameters = this.MapFinancePlanCalculationParameters(financePlanCalculationParametersDto, guarantor, currentStatement);

            // if the combined flag has changed, update the visits
            this._financePlanService.Value.UpdateVisitsOnPendingFinancePlan(pendingFinancePlan, financePlanCalculationParameters);

            pendingFinancePlan.IsCombined = financePlanCalculationParameters.CombineFinancePlans;
            pendingFinancePlan.OfferCalculationStrategy = financePlanCalculationParameters.OfferCalculationStrategy;

            // update finance plan
            FinancePlan financePlan;
            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                if (pendingFinancePlan.RecalculateOfferOnUpdate(financePlanCalculationParameters.MonthlyPaymentAmount, financePlanOfferId))
                {
                    financePlan = this._financePlanService.Value.UpdateFinancePlan(pendingFinancePlan, financePlanCalculationParameters, false, loggedInVisitPayUserId, httpContext);
                }
                else
                {
                    financePlan = this._financePlanService.Value.UpdateFinancePlanWithOffer(pendingFinancePlan, pendingFinancePlan.FinancePlanOffer, currentStatement, loggedInVisitPayUserId, httpContext);
                }

                if (termsCmsVersionId.HasValue)
                {
                    // get credit agreement
                    FinancePlanCreditAgreementCollectionDto creditAgreementCollectionDto = await this._creditAgreementApplicationService.Value.GetCachedCreditAgreementAsync(financePlanOfferId, termsCmsVersionId.Value, false);

                    // terms accepted, originate finance plan
                    this._financePlanService.Value.OriginateFinancePlan(financePlan, creditAgreementCollectionDto, loggedInVisitPayUserId, "Finance Plan Terms Accepted");
                }
                else
                {
                    // client update, set to pending acceptance
                    this._financePlanService.Value.SetFinancePlanIntoPendingAcceptance(financePlan, "Client modified terms");
                }

                this._financePlanService.Value.UpdateFinancePlan(financePlan);

                uow.Commit();

                if (termsCmsVersionId.HasValue)
                {
                    JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(httpContext, loggedInVisitPayUserId);
                    this._userJournalEventService.Value.LogGuarantorAcceptFinancePlan(journalEventUserContext, guarantor.VpGuarantorId, guarantor.User.UserName, financePlan.FinancePlanId, financePlan.FinancePlanOfferSetTypeId, termsCmsVersionId.Value);
                }
            }

            // pending acceptance
            if (financePlan.FinancePlanStatus.IsPendingAcceptance())
            {
                this._paymentService.Value.RemovePendingPaymentAmountAssociatedWithFinancePlan(financePlan, guarantor.User.VisitPayUserId);
                this.SendFinancePlanCreationEmail(currentStatement, guarantor);

                if (financePlan.IsFinancePlanOfferTypeClientOnly)
                {
                    this._metricsProvider.Value.Increment(Metrics.Increment.FinancePlan.ClientExtendedFinancePlanOffer);
                }

                return new CreateFinancePlanResponse(false, null, Mapper.Map<FinancePlanDto>(financePlan));
            }
            // originated, post process
            return this.PostFinancePlanCreate(financePlan);
        }

        private CreateFinancePlanResponse PostFinancePlanCreate(FinancePlan financePlan)
        {
            // combined plan cleanup
            if (financePlan.IsCombined)
            {
                // cancel all other active finance plans
                using (UnitOfWork uow = new UnitOfWork(this._session))
                {
                    this._financePlanService.Value.UpdateFinancePlansBecauseOfCombination(financePlan);
                    uow.Commit();
                }
            }

            return new CreateFinancePlanResponse(false, null, Mapper.Map<FinancePlanDto>(financePlan));
        }

        #endregion

        public void CancelFinancePlan(int guarantorVisitPayUserId, int financePlanId, string reason, int? actionVisitPayUserId)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantorEx(guarantorVisitPayUserId);
            FinancePlan financePlan = this._financePlanService.Value.GetFinancePlan(guarantor, financePlanId);
            if (financePlan.IsClosed())
            {
                return;
            }

            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                this._financePlanService.Value.CancelFinancePlan(financePlan, reason);
                this._paymentService.Value.RemovePendingPaymentAmountAssociatedWithFinancePlan(financePlan, guarantorVisitPayUserId);

                this._session.Transaction.RegisterPostCommitSuccessAction((vpgid, avpuid, fp) =>
                {
                    if (fp.IsNotOriginated())
                    {
                        return;
                    }

                    if (avpuid.HasValue)
                    {
                        this.LogCancelFinancePlanEvent(avpuid.Value, vpgid, fp, HttpContext.Current);
                    }

                    try
                    {
                        this.Bus.Value.PublishMessage(new SendFinancePlanTerminationEmailMessage { VpGuarantorId = vpgid }).Wait();
                    }
                    catch
                    {
                        this.LoggingService.Value.Fatal(() => $"FinancePlanApplicationService::CancelFinancePlan - Failed to publish SendCancelFinancePlanMessage - VpGuarantorId = {vpgid}");
                    }
                }, guarantor.VpGuarantorId, actionVisitPayUserId, financePlan);

                uow.Commit();
            }
        }

        private void LogCancelFinancePlanEvent(int currentUserId, int vpGuarantorId, FinancePlan financePlan, HttpContext context)
        {
            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(Mapper.Map<JournalEventHttpContextDto>(context), currentUserId);
            this._userJournalEventService.Value.LogCancelFinancePlanEvent(journalEventUserContext, vpGuarantorId, financePlan.VpGuarantor.User.UserName, financePlan.FinancePlanId, financePlan.FinancePlanOfferSetTypeId);
        }

        public IList<FinancePlanDto> GetOpenFinancePlans(int visitPayUserId, int? vpGuarantorId = null, bool considerConsolidation = false, string financePlanStateCode = null)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantorEx(visitPayUserId, vpGuarantorId);

            IList<FinancePlan> financePlans = this._financePlanService.Value.GetAllOpenFinancePlansForGuarantor(guarantor, considerConsolidation);

            //If state code provided, limit to finance plans for the given state
            if (!string.IsNullOrEmpty(financePlanStateCode))
            {
                financePlans = this._financePlanService.Value.FilterFinancePlansForState(financePlans, financePlanStateCode);
            }

            return Mapper.Map<List<FinancePlanDto>>(financePlans);
        }

        public IList<FinancePlanDto> GetOpenFinancePlansForGuarantor(int vpGuarantorId, bool considerConsolidation = false)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);

            return Mapper.Map<List<FinancePlanDto>>(this._financePlanService.Value.GetAllOpenFinancePlansForGuarantor(guarantor, considerConsolidation));
        }

        public async Task<IList<FinancePlanAchAuthorizationDto>> GetFinancePlansRequiringAchAuthorizationAsync(int currentVisitPayUserId, int vpGuarantorId, int? financePlanOfferId, IList<int> financePlanIds, int? financePlanIdToReconfigure, int? consolidationGuarantorId)
        {
            Guarantor currentGuarantor = this._guarantorService.Value.GetGuarantorEx(currentVisitPayUserId);
            List<FinancePlan> financePlans = new List<FinancePlan>();

            if (currentGuarantor.ConsolidationStatus != GuarantorConsolidationStatusEnum.Managed)
            {
                financePlans.AddRange(this._financePlanService.Value.GetAllOpenFinancePlansForGuarantor(currentGuarantor));
            }

            if (currentGuarantor.ConsolidationStatus == GuarantorConsolidationStatusEnum.Managing)
            {
                foreach (ConsolidationGuarantor managedGuarantor in currentGuarantor.ActiveManagedConsolidationGuarantors)
                {
                    financePlans.AddRange(this._financePlanService.Value.GetAllOpenFinancePlansForGuarantor(managedGuarantor.ManagedGuarantor));
                }
            }

            if (consolidationGuarantorId.HasValue)
            {
                // during consolidation or deconsolidation
                Guarantor consolidatedGuarantor = null;

                if (currentGuarantor.ConsolidationStatus == GuarantorConsolidationStatusEnum.Managed)
                {
                    // deconsolidation - accepting terms
                    consolidatedGuarantor = currentGuarantor;
                }
                else if (currentGuarantor.ConsolidationStatus == GuarantorConsolidationStatusEnum.PendingManaging)
                {
                    // consolidation - accepting terms
                    consolidatedGuarantor = currentGuarantor.ManagedConsolidationGuarantors
                        .Where(x => x.ConsolidationGuarantorId == consolidationGuarantorId.Value)
                        .Where(x => x.IsPending())
                        .Select(x => x.ManagedGuarantor)
                        .FirstOrDefault();
                }

                if (consolidatedGuarantor != null)
                {
                    financePlans.AddRange(this._financePlanService.Value.GetAllOpenFinancePlansForGuarantor(consolidatedGuarantor));
                }
            }

            List<FinancePlanAchAuthorizationDto> authorizationDtos = new List<FinancePlanAchAuthorizationDto>();

            // check for finance plan being setup
            if (financePlanOfferId.HasValue)
            {
                Guarantor termsGuarantor = currentGuarantor.VpGuarantorId == vpGuarantorId ? currentGuarantor : currentGuarantor.ActiveManagedConsolidationGuarantors.Select(x => x.ManagedGuarantor).FirstOrDefault(x => x.VpGuarantorId == vpGuarantorId);
                if (termsGuarantor != null)
                {
                    FinancePlanTermsDto terms = await this._creditAgreementApplicationService.Value.GetCachedTermsForCreditAgreementAsync(financePlanOfferId.Value, termsGuarantor.VpGuarantorId);
                    if (terms != null)
                    {
                        authorizationDtos.Add(new FinancePlanAchAuthorizationDto
                        {
                            Guarantor = Mapper.Map<GuarantorDto>(termsGuarantor),
                            MonthlyPaymentAmount = terms.MonthlyPaymentAmount,
                            MonthsRemaining = terms.NumberMonthlyPayments,
                            OriginationDate = terms.EffectiveDate
                        });

                        if (terms.IsCombined)
                        {
                            // remove any other existing plans from the list b/c combining plans
                            financePlans.RemoveAll(x => x.VpGuarantor.VpGuarantorId == termsGuarantor.VpGuarantorId);
                        }
                    }
                }
            }

            // check for managed user fp being accepted
            if (financePlanIds.IsNotNullOrEmpty())
            {
                foreach (int financePlanId in financePlanIds)
                {
                    FinancePlan financePlan = this._financePlanService.Value.GetFinancePlanById(financePlanId);
                    if (financePlan != null)
                    {
                        financePlans.Add(financePlan);
                    }
                }
            }

            // check for reconfig
            if (financePlanIdToReconfigure.HasValue)
            {
                // just need to remove these from the list
                financePlans.RemoveAll(x => x.FinancePlanId == financePlanIdToReconfigure.Value);
            }

            //
            authorizationDtos.AddRange(financePlans.Select(financePlan =>
            {
                FinancePlanDto financePlanDto = Mapper.Map<FinancePlanDto>(financePlan);

                IList<AmortizationMonth> amortizationMonths = this.GetAmortizationMonthsForFinancePlan(financePlan, financePlan.VpGuarantor);
                if (amortizationMonths.Any())
                {
                    financePlanDto.MonthsRemaining = amortizationMonths.Count;
                }

                return new FinancePlanAchAuthorizationDto
                {
                    Guarantor = financePlanDto.VpGuarantor,
                    MonthlyPaymentAmount = financePlanDto.PaymentAmount,
                    MonthsRemaining = financePlanDto.MonthsRemaining,
                    OriginationDate = financePlanDto.OriginationDate.GetValueOrDefault(DateTime.UtcNow)
                };
            }));

            return authorizationDtos;
        }

        public IList<FinancePlanInterestRateHistoryDto> GetFinancePlanInterestRateHistory(int visitPayUserId, int financePlanId, int? vpGuarantorId = null)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantorEx(visitPayUserId, vpGuarantorId);

            FinancePlan financePlan = this._financePlanService.Value.GetFinancePlan(guarantor, financePlanId);

            if (financePlan == null)
            {
                return new List<FinancePlanInterestRateHistoryDto>();
            }

            IList<FinancePlanInterestRateHistoryDto> financePlanInterestRateHistoryDto = Mapper.Map<List<FinancePlanInterestRateHistoryDto>>(financePlan.FinancePlanInterestRateHistory);

            foreach (FinancePlanInterestRateHistoryDto history in financePlanInterestRateHistoryDto)
            {
                history.PaymentAmount = financePlan.PaymentAmountAsOf(history.InsertDate);
            }

            return financePlanInterestRateHistoryDto;
        }

        public FinancePlanLogResultsDto GetFinancePlanLog(FinancePlanLogFilterDto financePlanLogFilterDto)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantorEx(financePlanLogFilterDto.VisitPayUserId);
            FinancePlan financePlan = this._financePlanService.Value.GetFinancePlan(guarantor, financePlanLogFilterDto.FinancePlanId);

            //create 2 lists. one of FinancePlanVisitPrincipalAmount and one of FinancePlanVisitInterestDue then map each to the dto
            IEnumerable<FinancePlanVisitPrincipalAmount> financePlanVisitPrincipalAmounts = financePlan.ActiveFinancePlanVisits.SelectMany(x => x.FinancePlanVisitPrincipalAmounts);
            IEnumerable<FinancePlanVisitInterestDue> financePlanVisitInterestDues = financePlan.ActiveFinancePlanVisits.SelectMany(x => x.FinancePlanVisitInterestDues);
            List<FinancePlanLogDto> principalAsLog = Mapper.Map<List<FinancePlanLogDto>>(financePlanVisitPrincipalAmounts);
            List<FinancePlanLogDto> interestDueAsLog = Mapper.Map<List<FinancePlanLogDto>>(financePlanVisitInterestDues);

            //combine the two 
            List<FinancePlanLogDto> financePlanLogs = new List<FinancePlanLogDto>();
            financePlanLogs.AddRange(principalAsLog);
            financePlanLogs.AddRange(interestDueAsLog);

            return this.FilterAndSortFinancePlanLogDto(financePlanLogFilterDto, financePlanLogs);
        }

        private FinancePlanLogResultsDto FilterAndSortFinancePlanLogDto(FinancePlanLogFilterDto filter, IList<FinancePlanLogDto> financePlansLog)
        {

            bool isAscendingOrder = (filter.Sord == null) || "asc".Equals(filter.Sord, StringComparison.InvariantCultureIgnoreCase);

            switch (filter.Sidx)
            {
                case "InsertDate":
                    financePlansLog = financePlansLog.OrderBy(x => x.InsertDate, isAscendingOrder).ToList();
                    break;
            }

            if (filter.SourceSystemKeys.Count > 0)
            {
                financePlansLog = financePlansLog.Where(x => filter.SourceSystemKeys.Contains(x.SourceSystemKey)).ToList();
            }

            int batch = Math.Max(filter.Page - 1, 0);
            int batchSize = filter.Rows == 0 ? 25 : filter.Rows;

            int totalRecords = financePlansLog.Count;
            FinancePlanLogResultsDto financePlanLogResultsDto = new FinancePlanLogResultsDto
            {
                FinancePlanLogs = financePlansLog.Skip(batch * batchSize)
                    .Take(batchSize)
                    .ToList(),
                TotalRecords = totalRecords
            };

            return financePlanLogResultsDto;
        }

        public void CheckIfFinancePlanIsValid(IList<int> guarantorIds)
        {
            IList<Guarantor> guarantors = this._guarantorService.Value.GetGuarantorsFromIntList(guarantorIds);
            this._financePlanService.Value.CheckIfFinancePlanIsValid(guarantors);
        }

        public void CheckIfReconfigurationIsStillValid(IList<int> guarantorIds)
        {
            IList<Guarantor> guarantors = this._guarantorService.Value.GetGuarantorsFromIntList(guarantorIds);
            this._financePlanService.Value.CheckIfReconfigurationIsStillValid(guarantors);
        }

        private void SendFinancePlanCreationEmail(VpStatement currentStatement, Guarantor guarantor)
        {
            DateTime? actionDate = this.GetPendingFinancePlanCancellationDate(currentStatement, guarantor);

            if (actionDate.HasValue)
            {
                this._session.Transaction.RegisterPostCommitSuccessAction((d, g) =>
                {
                    this.Bus.Value.PublishMessage(new SendFinancePlanCreationEmailMessage
                    {
                        ActionDate = d,
                        VpGuarantorId = g
                    }).Wait();
                }, actionDate.Value, guarantor.VpGuarantorId);
            }
        }

        public DateTime? GetPendingFinancePlanCancellationDate(int vpGuarantorId)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            VpStatement statement = this._statementService.Value.GetMostRecentStatement(guarantor);

            return this.GetPendingFinancePlanCancellationDate(statement, guarantor);
        }

        private DateTime? GetPendingFinancePlanCancellationDate(VpStatement currentStatement, Guarantor guarantor)
        {
            bool isGracePeriod = currentStatement != null && currentStatement.IsGracePeriod;
            DateTime? paymentDueDate = currentStatement?.PaymentDueDate;
            DateTime? nextStatementDate = guarantor.NextStatementDate;
            DateTime? actionDate = isGracePeriod ? paymentDueDate : nextStatementDate;

            return actionDate;
        }

        private IList<AmortizationMonth> GetAmortizationMonthsForFinancePlan(FinancePlan financePlan, Guarantor guarantor)
        {
            DateTime nextPaymentDate = this._statementService.Value.GetNextPaymentDate(guarantor);

            if (financePlan.IsNotOriginated())
            {
                return this._amortizationService.Value.GetAmortizationMonthsForPendingFinancePlan(financePlan, financePlan.FirstInterestPeriod(), nextPaymentDate);
            }
            
            AmortizationVisitDateBalances visitDateBalances = this._financePlanService.Value.GetFinancePlanActiveVisitsBalancesDaily(financePlan);
            
            // this will account for rescheduled/cancelled payments (?)
            DateTime nextPaymentDueDate = financePlan.MaxScheduledPaymentDateOrNextPaymentDueDate(nextPaymentDate);
            return this._amortizationService.Value.GetAmortizationMonthsForOriginatedFinancePlan(financePlan, nextPaymentDueDate, visitDateBalances);
        }

        public void CancelPaymentBucketsForFinancePlan(int financePlanId, int guarantorId, int numberOfBucketsToKeep, int? visitPayUserId)
        {
            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                try
                {
                    Guarantor guarantor = this._guarantorService.Value.GetGuarantor(guarantorId);
                    if (guarantor == null)
                    {
                        this.LoggingService.Value.Info(() => $"FinancePlanApplicationService::CancelPaymentBucketsForFinancePlan::For GuarantorId {guarantorId}, couldnt find that guarantor.");
                        uow.Rollback();
                        return;
                    }
                    FinancePlan financePlan = this._financePlanService.Value.GetFinancePlan(guarantor, financePlanId);
                    if (financePlan == null)
                    {
                        this.LoggingService.Value.Info(() => $"FinancePlanApplicationService::CancelPaymentBucketsForFinancePlan::For GuarantorId {guarantorId}, on FinancePlanId {financePlanId}, keeping {numberOfBucketsToKeep} buckets, the finance plan doesnt exist or guarantor doesnt match the finance plan.");
                        uow.Rollback();
                        return;
                    }

                    //When this is true there is payments to cancel
                    if ((financePlan.Buckets.Count - numberOfBucketsToKeep) <= 0)
                    {
                        this.LoggingService.Value.Info(() => $"FinancePlanApplicationService::CancelPaymentBucketsForFinancePlan::For GuarantorId {guarantorId}, on FinancePlanId {financePlanId}, keeping {numberOfBucketsToKeep} buckets, there wasnt any payments to cancel.");
                        uow.Rollback();
                        return;
                    }

                    IDictionary<int, Tuple<decimal, int?>> financePlanBuckets = financePlan.Buckets;
                    foreach (int i in financePlanBuckets.Keys.OrderByDescending(x => x).Take(financePlanBuckets.Keys.Count - numberOfBucketsToKeep).ToList())
                    {
                        financePlan.AddFinancePlanAmountsDueOffset(financePlanBuckets[i].Item1, DateTime.UtcNow, financePlanBuckets[i].Item2, "FinancePlanApplicationService::CancelPaymentBucketsForFinancePlan");
                    }

                    this._financePlanService.Value.CheckIfFinancePlansAreUncollectable(financePlan.ToListOfOne());
                    this._financePlanService.Value.CheckIfFinancePlansAreStillPastDueOrUncollectable(financePlan.ToListOfOne());
                    this._financePlanService.Value.CheckIfFinancePlansShouldBeClosedUncollectable(financePlan.ToListOfOne());
                    this._financePlanService.Value.UpdateFinancePlan(financePlan);

                    uow.Commit();
                }
                catch (Exception)
                {
                    uow.Rollback();
                    throw;
                }
            }
        }

        public void PopulateFinancePlanInfoOnPaymentAllocationMessages(IList<PaymentAllocationMessage> messages)
        {
            this._financePlanService.Value.PopulateFinancePlanIdAndDurationForAllocationMessages(messages);
        }

        #region exports

        public async Task<byte[]> ExportFinancePlansClientAsync(int visitPayUserId, FinancePlanFilterDto filterDto, string userName, int? vpGuarantorId = null)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantorEx(visitPayUserId, vpGuarantorId);
            FinancePlanResultsDto financePlanResults = this.GetFinancePlans(visitPayUserId, filterDto, 1, iVinciExcelExportBuilder.MaxExportRecords, vpGuarantorId);

            IDictionary<string, string> textRegions = await this._contentApplicationService.Value.GetTextRegionsAsync(
                TextRegionConstants.ExportedBy,
                TextRegionConstants.FileDate,
                TextRegionConstants.FinancePlansPageHeading);

            System.Drawing.Image logo = await this._imageService.Value.GetExportLogoAsync();

            using (ExcelPackage package = new ExcelPackage())
            {
                iVinciExcelExportBuilder builder = new iVinciExcelExportBuilder(package, $"{guarantor.User.FirstNameLastName} {textRegions.GetValue(TextRegionConstants.FinancePlansPageHeading)}")
                    .WithWorkSheetNameOf(textRegions.GetValue(TextRegionConstants.FinancePlansPageHeading))
                    .WithLogo(logo)
                    .AddInfoBlockRow(new ColumnBuilder(DateTime.UtcNow.ToString(Format.DateFormat), textRegions.GetValue(TextRegionConstants.FileDate)).WithStyle(StyleTypeEnum.AlignLeft))
                    .AddInfoBlockRow(new ColumnBuilder(userName, textRegions.GetValue(TextRegionConstants.ExportedBy)).WithStyle(StyleTypeEnum.AlignLeft));

                await this.ExportFinancePlansDataAsync(builder, financePlanResults);

                builder.Build();

                return package.GetAsByteArray();
            }
        }

        public async Task<byte[]> ExportFinancePlansAsync(int visitPayUserId, FinancePlanFilterDto filterDto, string userName, int? vpGuarantorId = null)
        {
            FinancePlanResultsDto financePlanResults = this.GetFinancePlans(visitPayUserId, filterDto, 1, iVinciExcelExportBuilder.MaxExportRecords, vpGuarantorId);

            IDictionary<string, string> textRegions = await this._contentApplicationService.Value.GetTextRegionsAsync(
                TextRegionConstants.ExportedBy,
                TextRegionConstants.FileDate,
                TextRegionConstants.FinancePlansPageHeading);

            System.Drawing.Image logo = await this._imageService.Value.GetExportLogoAsync();

            using (ExcelPackage package = new ExcelPackage())
            {
                iVinciExcelExportBuilder builder = new iVinciExcelExportBuilder(package, textRegions.GetValue(TextRegionConstants.FinancePlansPageHeading));
                builder.WithLogo(logo)
                    .AddInfoBlockRow(new ColumnBuilder(DateTime.UtcNow.ToString(Format.DateFormat), textRegions.GetValue(TextRegionConstants.FileDate)).WithStyle(StyleTypeEnum.AlignLeft))
                    .AddInfoBlockRow(new ColumnBuilder(userName, textRegions.GetValue(TextRegionConstants.ExportedBy)).WithStyle(StyleTypeEnum.AlignLeft));

                await this.ExportFinancePlansDataAsync(builder, financePlanResults);

                builder.Build();

                return package.GetAsByteArray();
            }
        }

        private async Task ExportFinancePlansDataAsync(iVinciExcelExportBuilder builder, FinancePlanResultsDto financePlanResults)
        {
            IDictionary<string, string> textRegions = await this._contentApplicationService.Value.GetTextRegionsAsync(
                TextRegionConstants.EffectiveDate,
                TextRegionConstants.InterestCharged,
                TextRegionConstants.InterestPaid,
                TextRegionConstants.InterestRate,
                TextRegionConstants.FinalPaymentDate,
                TextRegionConstants.MonthlyAmountDue,
                TextRegionConstants.NoFinancePlans,
                TextRegionConstants.OriginalBalance,
                TextRegionConstants.PayOffBalance,
                TextRegionConstants.Pending,
                TextRegionConstants.PrincipalPaid,
                TextRegionConstants.Status);

            IDictionary<string, string> financePlanStatuses = await this._contentApplicationService.Value.GetTextRegionsAsync(financePlanResults.FinancePlans.Select(x => x.FinancePlanStatus.FinancePlanStatusDisplayName).Distinct().ToArray());

            builder.AddDataBlock(financePlanResults.FinancePlans, financePlan =>
            {
                string financePlanStatusDisplayName = financePlanStatuses.GetValue(financePlan.FinancePlanStatus.FinancePlanStatusDisplayName);

                List<Column> columns = new List<Column>
                {
                    new ColumnBuilder(financePlan.OriginationDate, textRegions.GetValue(TextRegionConstants.EffectiveDate)).WithStyle(StyleTypeEnum.AlignLeft).WithDateFormat(),
                    new ColumnBuilder(financePlan.FinancePlanOffer.InterestRate.FormatPercentage(), textRegions.GetValue(TextRegionConstants.InterestRate)),
                    new ColumnBuilder(financePlan.LastPaymentDate?.ToString(Format.DateFormat) ?? "", textRegions.GetValue(TextRegionConstants.FinalPaymentDate)),
                    new ColumnBuilder(financePlanStatusDisplayName, textRegions.GetValue(TextRegionConstants.Status))
                };

                bool isPending = financePlan.FinancePlanStatus.FinancePlanStatusEnum == FinancePlanStatusEnum.PendingAcceptance || financePlan.FinancePlanStatus.FinancePlanStatusEnum == FinancePlanStatusEnum.TermsAccepted;

                string originalBalanceHeader = textRegions.GetValue(TextRegionConstants.OriginalBalance);

                columns.Add(isPending ?
                    new ColumnBuilder(textRegions.GetValue(TextRegionConstants.Pending), originalBalanceHeader) :
                    new ColumnBuilder(financePlan.OriginatedFinancePlanBalance, originalBalanceHeader).WithCurrencyFormat());

                columns.Add(new ColumnBuilder(financePlan.InterestAssessed.GetValueOrDefault(0), textRegions.GetValue(TextRegionConstants.InterestCharged)).WithCurrencyFormat());
                columns.Add(new ColumnBuilder(financePlan.InterestPaidToDate, textRegions.GetValue(TextRegionConstants.InterestPaid)).WithCurrencyFormat());
                columns.Add(new ColumnBuilder(financePlan.PrincipalPaidToDate, textRegions.GetValue(TextRegionConstants.PrincipalPaid)).WithCurrencyFormat());
                columns.Add(new ColumnBuilder(financePlan.CurrentFinancePlanBalance, textRegions.GetValue(TextRegionConstants.PayOffBalance)).WithCurrencyFormat());
                columns.Add(new ColumnBuilder(financePlan.PaymentAmount, textRegions.GetValue(TextRegionConstants.MonthlyAmountDue)).WithCurrencyFormat());

                return columns;
            })
            .WithNoRecordsFoundMessageOf(textRegions.GetValue(TextRegionConstants.NoFinancePlans));
        }

        #endregion

        /// <summary>
        /// Gets a FP offer for the guarantor if the single payment they are making is eligible for a FP.
        /// </summary>
        /// <param name="currentVisitPayUserId"></param>
        /// <param name="vpGuarantorId"></param>
        /// <param name="paymentAmount">Amount the guarantor is making for a single payment</param>
        /// <returns>Details of the Finance Plan offer if one is available (with terms), else null to indicate no offer available.</returns>
        public async Task<CalculateFinancePlanTermsResponseDto> GetAvailableFinancePlanOfferForPayment(int currentVisitPayUserId, int vpGuarantorId, decimal paymentAmount)
        {
            CalculateFinancePlanTermsResponseDto financePlanOfferResponse = null;

            //Only continue checking for FP offer if we have no active FPs, and we are in the proper test group
            bool hasActiveFinancePlans = this.HasActiveFinancePlans(currentVisitPayUserId, vpGuarantorId);
            PartialPaymentFinancePlanOfferRandomizedTestGroupConfiguration testGroupConfiguration = this.GetTestGroupConfigurationForPartialPaymentFinancePlanOffer(vpGuarantorId);
            if (!hasActiveFinancePlans && testGroupConfiguration != null)
            {
                //Load current statement for the guarantor
                Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
                VpStatement currentStatement = this._statementService.Value.GetMostRecentStatement(guarantor);

                //Get the FP offer
                bool shouldCombineFinancePlans = this.IsGuarantorEligibleToCombineFinancePlans(currentVisitPayUserId, vpGuarantorId, true);
                FinancePlanCalculationParametersDto financePlanCalcuationParamsDto = FinancePlanCalculationParametersDto.CreateForMonthlyPayment(vpGuarantorId,
                                                                                                                 currentStatement.VpStatementId,
                                                                                                                 paymentAmount,
                                                                                                                 null,
                                                                                                                 shouldCombineFinancePlans);
                financePlanOfferResponse = await this.GetTermsByMonthlyPaymentAmountAsync(financePlanCalcuationParamsDto);

                //Validate the offer before returning 
                if (financePlanOfferResponse != null)
                {
                    /*
                     * VP-2308:
                     * If the FP has errors because the minimum payment is greater than the payment being made, we should check
                     * if we have a nudge percentage in the test. If we do, we can see if increasing the payment amount by that
                     * nudge percentage will meet the FP minimum, and if it does we will show that offer to the user.
                     */
                    decimal financePlanMinimumPaymentAmount = financePlanOfferResponse.MinimumPaymentAmount.GetValueOrDefault();
                    decimal nudgePercentage = testGroupConfiguration.NudgePercentage.GetValueOrDefault();
                    if (financePlanOfferResponse.IsError
                        && financePlanMinimumPaymentAmount > paymentAmount
                        && nudgePercentage > 0)
                    {
                        //Minimum amount for the FP is greater than the payment amount, and we have a nudge percentage. Check if we should nudge.
                        decimal nudgedPaymentAmount = paymentAmount * (1m + (nudgePercentage / 100m));
                        if (nudgedPaymentAmount >= financePlanMinimumPaymentAmount)
                        {
                            //The nudged payment amount is at least as much as the FP minimum, so recalculate the FP using the FP minimum
                            financePlanCalcuationParamsDto = FinancePlanCalculationParametersDto.CreateForMonthlyPayment(vpGuarantorId,
                                                                                                                         currentStatement.VpStatementId,
                                                                                                                         financePlanMinimumPaymentAmount,
                                                                                                                         null,
                                                                                                                         shouldCombineFinancePlans);
                            financePlanOfferResponse = await this.GetTermsByMonthlyPaymentAmountAsync(financePlanCalcuationParamsDto);
                        }
                    }

                    /*
                     * Check that there are no errors on the FP, that we have terms, and that terms are within
                     * the specified limits for showing the offer. If any fail, don't return an offer.
                     */
                    decimal minimumRequiredMonthlyPayments = testGroupConfiguration.MinimumRequiredMonthlyPayments.GetValueOrDefault();
                    if (financePlanOfferResponse == null //Recheck for null in case nudge FP check returned null
                        || financePlanOfferResponse.IsError
                        || financePlanOfferResponse.Terms == null
                        || financePlanOfferResponse.Terms.NumberMonthlyPayments < minimumRequiredMonthlyPayments)
                    {
                        financePlanOfferResponse = null;
                    }
                }
            }

            return financePlanOfferResponse;
        }

        /// <summary>
        /// Gets a test group configuration for PartialPaymentFinancePlanOffer, if the user falls into
        /// the correct test group for showing the offer.
        /// </summary>
        /// <param name="vpGuarantorId"></param>
        /// <returns>The test group configuration if the user is in the correct test group, else null.</returns>
        private PartialPaymentFinancePlanOfferRandomizedTestGroupConfiguration GetTestGroupConfigurationForPartialPaymentFinancePlanOffer(int vpGuarantorId)
        {
            PartialPaymentFinancePlanOfferRandomizedTestGroupConfiguration response = null;

            RandomizedTest randomizedTest = this._randomizedTestService.Value.GetRandomizedTest(RandomizedTestEnum.PartialPaymentFinancePlanOffer);
            if (randomizedTest != null)
            {
                //We have a test, so load the test group
                RandomizedTestGroup randomizedTestGroup = this._randomizedTestService.Value.GetRandomizedTestGroupMembership(randomizedTest, vpGuarantorId);
                if (randomizedTestGroup != null)
                {
                    //We have a test group, so ensure we are in the correct group being tested (test group A)
                    RandomizedTestGroupEnum randomizedTestGroupEnum = (RandomizedTestGroupEnum)randomizedTestGroup.RandomizedTestGroupId;
                    if (randomizedTestGroupEnum == RandomizedTestGroupEnum.PartialPaymentFinancePlanOfferA)
                    {
                        //We are in the test group that's being tested, so load the config to return
                        PartialPaymentFinancePlanOfferRandomizedTest test = new PartialPaymentFinancePlanOfferRandomizedTest(randomizedTest);
                        response = test.GetTestGroupConfiguration(randomizedTestGroupEnum);
                    }
                }
            }

            return response;
        }

        public void CheckIfFinancePlanIsPastDueOrUncollectable(IList<int> guarantorIds)
        {
            foreach (int guarantorId in guarantorIds)
            {
                IList<FinancePlan> financePlans = this._financePlanService.Value.GetAllActiveFinancePlansForGuarantor(new Guarantor() { VpGuarantorId = guarantorId });
                if (financePlans.IsNotNullOrEmpty())
                {
                    this._financePlanService.Value.CheckIfFinancePlansAreStillPastDueOrUncollectable(financePlans);
                }
            }
        }
        
        public FinancePlanSummaryDto GetFinancePlanSummary(int vpGuarantorId)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            IList<FinancePlan> financePlans = this._financePlanService.Value.GetAllOpenFinancePlansForGuarantor(guarantor);
            if (!financePlans.Any())
            {
                return new FinancePlanSummaryDto();
            }

            List<FinancePlanDto> financePlanDtos = new List<FinancePlanDto>();
            List<DateTime?> dueDates = new List<DateTime?>();

            DateTime nextPaymentDueDate = this._statementService.Value.GetNextPaymentDate(guarantor);

            foreach (FinancePlan financePlan in financePlans)
            {
                FinancePlanDto financePlanDto = Mapper.Map<FinancePlanDto>(financePlan);

                IList<AmortizationMonth> amortizationMonths = this.GetAmortizationMonthsForFinancePlan(financePlan, guarantor);
                if (amortizationMonths.Any())
                {
                    financePlanDto.LastPaymentDate = amortizationMonths.OrderByDescending(x => x.MonthDate).First().MonthDate;
                    financePlanDto.MonthsRemaining = amortizationMonths.Count;
                }

                this.SetAmountDueForDisplay(financePlanDto);

                dueDates.Add(financePlan.MaxScheduledPaymentDateOrNextPaymentDueDate(nextPaymentDueDate));
                financePlanDtos.Add(financePlanDto);
            }

            DateTime? nextDueDate = dueDates.OrderBy(x => x).FirstOrDefault();
            FinancePlanSummaryDto financePlanSummaryDto = new FinancePlanSummaryDto
            {
                FinancePlans = financePlanDtos,
                FinancePlansBalance = financePlanDtos.Sum(x => x.CurrentFinancePlanBalance),
                AmountDueSum = financePlanDtos.Sum(x => x.AmountDue),
                MonthsRemaining = financePlanDtos.Select(x => x.MonthsRemaining).DefaultIfEmpty(0).Max(),
                PaymentDueDate = nextDueDate,
                HasAmountDue = financePlanDtos.Any(x => x.AmountDue > 0m)
            };

            if (financePlanDtos.Any(x => x.IsActiveUncollectable))
            {
                financePlanSummaryDto.StatusDisplay = financePlanSummaryDto.FinancePlans.FirstOrDefault(x => x.IsActiveUncollectable)?.FinancePlanStatus?.FinancePlanStatusDisplayName;
            }
            else if (financePlanDtos.Any(x => x.IsPastDue))
            {
                financePlanSummaryDto.StatusDisplay = financePlanSummaryDto.FinancePlans.FirstOrDefault(x => x.IsPastDue)?.FinancePlanStatus?.FinancePlanStatusDisplayName;
            }
            else
            {
                // no status display
            }

            return financePlanSummaryDto;
        }

        public void CheckIfPendingFinancePlansNeedToClose(IList<int> visitIds)
        {
            IList<Visit> visits = this._visitService.Value.GetAllVisitsWithIntList(visitIds, false);
            IEnumerable<Guarantor> guarantors = visits.Select(x => x.VPGuarantor);
            foreach (Guarantor guarantor in guarantors)
            {
                IList<FinancePlan> financePlans = this._financePlanService.Value.GetAllPendingFinancePlansForGuarantor(guarantor);
                foreach (FinancePlan financePlan in financePlans)
                {
                    if (financePlan.IsNotOriginated())
                    {
                        if (!financePlan.HasActiveVisits())
                        {
                            this._financePlanService.Value.CancelFinancePlan(financePlan, "No visits were remaining.");
                        }
                    }
                }
            }
        }

        private static ConcurrentDictionary<int, SemaphoreSlim> RecalculateFinancePlanDueToFinancialAssistanceFlagChangeAsyncLocks = new ConcurrentDictionary<int, SemaphoreSlim>();

        public async Task RecalculateFinancePlanDueToFinancialAssistanceFlagChangeAsync(FinancePlanVisitFinancialAssistanceFlagChangedMessage financePlanVisitFinancialAssistanceFlagChangedMessage)
        {
            int vpGuarantorId = financePlanVisitFinancialAssistanceFlagChangedMessage.VpGuarantorId;
            int visitId = financePlanVisitFinancialAssistanceFlagChangedMessage.VisitId;

            if (!RecalculateFinancePlanDueToFinancialAssistanceFlagChangeAsyncLocks.ContainsKey(vpGuarantorId))
            {
                if (!RecalculateFinancePlanDueToFinancialAssistanceFlagChangeAsyncLocks.TryAdd(vpGuarantorId, new SemaphoreSlim(1, 1)))
                {
                    string message() =>  $"FinancePlanApplicationService::RecalculateFinancePlanDueToFinancialAssistanceFlagChangeAsyncLocks failed to add lock to dictionary, VPG: {vpGuarantorId}, Visit: {visitId} ";
                    this.LoggingService.Value.Fatal(message);
                    throw new Exception(message());
                }
            }
            SemaphoreSlim vpgSemaphore = RecalculateFinancePlanDueToFinancialAssistanceFlagChangeAsyncLocks[vpGuarantorId];

            try
            {
                await vpgSemaphore.WaitAsync();

                //This sets the Offer
                //Default if the FCRetro flag isnt set this will also write off all of the unpaid interest
                using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
                {
                    this._financePlanService.Value.RecalculateFinancePlanDueToFinancialAssistanceFlagChange(vpGuarantorId, visitId);
                    unitOfWork.Commit();
                }
            }
            finally
            {
                vpgSemaphore.Release();

                //Attempt to clean up, dont want this to grow like crazy.
                RecalculateFinancePlanDueToFinancialAssistanceFlagChangeAsyncLocks.TryRemove(vpGuarantorId, out vpgSemaphore);
            }
        }

        public FinancePlanBucketHistoryResultsDto GetBucketHistory(FinancePlanBucketHistoryFilterDto filterDto)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantorEx(filterDto.GuarantorVisitPayUserId);
            FinancePlan financePlan = this._financePlanService.Value.GetFinancePlan(guarantor, filterDto.FinancePlanId);

            return new FinancePlanBucketHistoryResultsDto
            {
                FinancePlanBucketHistories = financePlan.FinancePlanBucketHistories
                    .OrderByDescending(x => x.InsertDate)
                    .ThenByDescending(x => x.FinancePlanBucketHistoryId)
                    .Skip((filterDto.Page - 1) * filterDto.Rows)
                    .Take(filterDto.Rows)
                    .Select(x => new FinancePlanBucketHistoryDto
                    {
                        Bucket = x.Bucket,
                        Comment = x.ChangeDescription,
                        InsertDate = x.InsertDate,
                        VisitPayUser = Mapper.Map<VisitPayUserDto>(x.VisitPayUser)
                    })
                    .ToList(),
                TotalRecords = financePlan.FinancePlanBucketHistories.Count
            };
        }

        public IList<FinancePlanOfferSetTypeDto> GetFinancePlanOfferSetTypes()
        {
            return Mapper.Map<IList<FinancePlanOfferSetTypeDto>>(this._financePlanOfferService.Value.GetFinancePlanOfferSetTypes());
        }

        #region parameters

        // public to unit test
        public FinancePlanCalculationParameters MapFinancePlanCalculationParameters(
            FinancePlanCalculationParametersDto financePlanCalculationParametersDto,
            Guarantor guarantor = null,
            VpStatement statement = null)
        {
            if (guarantor == null)
            {
                guarantor = this._guarantorService.Value.GetGuarantor(financePlanCalculationParametersDto.VpGuarantorId);
            }

            if (statement == null)
            {
                statement = this._statementService.Value.GetMostRecentStatement(guarantor);
            }

            if (statement == null)
            {
                throw new Exception("Current statement is null");
            }

            if (financePlanCalculationParametersDto.StatementId.HasValue && statement.VpStatementId != financePlanCalculationParametersDto.StatementId)
            {
                throw new Exception("Current statement was different than statementId passed in");
            }

            decimal totalBalance = this._visitBalanceBaseApplicationService.Value.GetTotalBalance(guarantor.VpGuarantorId);
            
            DateTime firstPaymentDueDate;
            if (financePlanCalculationParametersDto.PaymentDueDay.HasValue && financePlanCalculationParametersDto.PaymentDueDay != guarantor.PaymentDueDay)
            {
                // override is supplied, use it
                firstPaymentDueDate = this._statementDateService.Value.GetNextPaymentDate(financePlanCalculationParametersDto.PaymentDueDay.Value);
            }
            else
            {
                // no override, get the next date
                firstPaymentDueDate = this._statementDateService.Value.GetNextPaymentDate(guarantor, statement);
            }

            FinancePlanCalculationParameters parameters = new FinancePlanCalculationParameters(
                guarantor,
                statement,
                financePlanCalculationParametersDto.MonthlyPaymentAmount.GetValueOrDefault(0),
                financePlanCalculationParametersDto.FinancePlanOfferSetTypeId,
                financePlanCalculationParametersDto.OfferCalculationStrategy,
                this.Client.Value.InterestCalculationMethod,
                firstPaymentDueDate);

            parameters.StateCodeForVisits = financePlanCalculationParametersDto.StateCodeForVisits;

            parameters.CombineFinancePlans = financePlanCalculationParametersDto.CombineFinancePlans &&
                                      this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.CombineFinancePlans) &&
                                      this._financePlanService.Value.IsGuarantorEligibleToCombineFinancePlans(guarantor, totalBalance, parameters.StateCodeForVisits);

            //If state code provided and we weren't already given specific visits, only select visits for that state.
            if (!string.IsNullOrEmpty(parameters.StateCodeForVisits) && financePlanCalculationParametersDto.SelectedVisits == null)
            {
                IList<Visit> allVisits = this._visitService.Value.GetAllActiveNonFinancedVisits(guarantor.VpGuarantorId);
                List<int> visitIdsForState = allVisits.Where(v => v.Facility?.RicState?.StateCode == parameters.StateCodeForVisits).Select(v => v.VisitId).ToList();
                parameters.SelectedVisits = visitIdsForState;
            }
            else
            {
                parameters.SelectedVisits = financePlanCalculationParametersDto.SelectedVisits;
            }

            parameters.Visits = this.GetFinancePlanCalculatedVisitTotal(parameters);
            int guarantorToScore = guarantor.VpGuarantorId;
            if (guarantor.IsManaged())
            {
                guarantorToScore = guarantor.ManagingGuarantorId ?? guarantorToScore;
            }
            parameters.VpGuarantorScore = this._scoringService.Value.GetScore(guarantorToScore, ScoreTypeEnum.PtpOriginal);

            parameters.FinancePlanTypeId = financePlanCalculationParametersDto.FinancePlanTypeId;//default handled with FinancePlanCalculationParameters.FinancePlanType

            return parameters;
        }

        private IList<IFinancePlanVisitBalance> GetFinancePlanCalculatedVisitTotal(FinancePlanCalculationParameters financePlanCalculationParameters)
        {
            // get all active visits
            List<Visit> visitsToFinance = this._visitService.Value.GetAllActiveVisits(financePlanCalculationParameters.Guarantor).ToList();

            // Check if guarantor has visits in multiple states
            List<string> distinctStateCodes = visitsToFinance.Select(v => v.Facility?.RicState?.StateCode).Distinct().ToList();
            distinctStateCodes.RemoveAll(string.IsNullOrEmpty);
            bool guarantorHasVisitsInMultipleStates = distinctStateCodes.Count > 1;

            //Get visits for the given state, if provided
            if (guarantorHasVisitsInMultipleStates)
            {
                //Remove any visits without a facility assigned. Cannot handle multi-state visits with no facility assigned.
                int numberRemoved = visitsToFinance.RemoveAll(v => v.Facility?.RicState?.StateCode == null);
                if (numberRemoved > 0)
                {
                    this.LoggingService.Value.Warn(() =>  $"{nameof(FinancePlanApplicationService)}::{nameof(this.GetFinancePlanCalculatedVisitTotal)} - Guarantor has visits both with and without a facility assigned. Visits without a facility will not be included in calculated visit total.");
                }

                //Remove any visits not in the applicable state from the list
                if (!string.IsNullOrEmpty(financePlanCalculationParameters.StateCodeForVisits))
                {
                    visitsToFinance = visitsToFinance.Where(v => v.Facility?.RicState?.StateCode == financePlanCalculationParameters.StateCodeForVisits).ToList();
                }
            }

            // get existing fp's
            IList<FinancePlan> openFinancePlans = this._financePlanService.Value.GetAllOpenFinancePlansForGuarantor(financePlanCalculationParameters.Guarantor);
            IList<FinancePlanVisit> financePlanVisits = openFinancePlans.SelectMany(x => x.ActiveFinancePlanVisits).GroupBy(x => x.FinancePlanVisitId).Select(x => x.First()).ToList();

            // if not combining, remove currently financed visits
            if (!financePlanCalculationParameters.CombineFinancePlans)
            {
                visitsToFinance.RemoveAll(x => financePlanVisits.Select(fpv => fpv.VisitId).Contains(x.VisitId));
            }

            if (financePlanCalculationParameters.SelectedVisits != null)
            {
                visitsToFinance.RemoveAll(x => !financePlanCalculationParameters.SelectedVisits.Contains(x.VisitId) && !financePlanVisits.Select(fpv => fpv.VisitId).Contains(x.VisitId));
            }
            else
            {
                IList<int> pendingFinancePlanVisits = this._financePlanService.Value.GetVisitIdsOnPendingFinancePlans(financePlanCalculationParameters.Guarantor.VpGuarantorId);
                if (pendingFinancePlanVisits?.Count > 0)
                {
                    visitsToFinance = visitsToFinance.Where(x => pendingFinancePlanVisits.Contains(x.VisitId)).ToList();
                }
            }
            // get uncleared payments for all visits
            IDictionary<int, decimal[]> unclearedPaymentsByVisitId = this._visitBalanceBaseApplicationService.Value.GetUnclearedPaymentsForVisits(visitsToFinance.Select(x => x.VisitId).ToList());

            // each visit should have 1 month interest free
            const decimal overrideInterestRate = 0m;
            const int overridesRemaining = 1;

            // add visits
            List<IFinancePlanVisitBalance> visits = new List<IFinancePlanVisitBalance>();
            foreach (Visit visit in visitsToFinance)
            {
                decimal currentVisitBalance = visit.CurrentBalance; // this will include any adjustments (stump amount) applied to a financed visit, which is intended here.
                decimal unclearedPaymentsSum = unclearedPaymentsByVisitId?.GetValue(visit.VisitId)?.Sum() ?? 0m;
                decimal interestDue = financePlanVisits.FirstOrDefault(x => x.VisitId == visit.VisitId)?.InterestDue ?? 0m;

                IFinancePlanVisitBalance setupVisit = new FinancePlanSetupVisit(
                    visit.VisitId,
                    visit.SourceSystemKey,
                    visit.BillingSystemId ?? default(int),
                    currentVisitBalance,
                    unclearedPaymentsSum,
                    interestDue,
                    visit.CurrentVisitState,
                    visit.BillingApplication,
                    visit.InterestZero,
                    overrideInterestRate,
                    overridesRemaining);

                if (setupVisit.CurrentBalance <= 0m)
                {
                    // exclude visits with a $0.00 total balance, uncleared payments included
                    continue;
                }

                visits.Add(setupVisit);
            }

            return visits;
        }

        #endregion

        public FinancePlanDto GetFinancePlanWithVisitId(int visitId)
        {
            return Mapper.Map<FinancePlanDto>(this._financePlanService.Value.GetFinancePlansAssociatedWithVisits(new List<Visit> { new Visit { VisitId = visitId } }).FirstOrDefault());
        }

        public void OnFinancePlanCreated(int vpGuarantorId, int financePlanId)
        {
            FinancePlan financePlan = this._financePlanService.Value.GetFinancePlan(new Guarantor { VpGuarantorId = vpGuarantorId }, financePlanId);
            if (financePlan == null)
            {
                return;
            }

            IReadOnlyList<Visit> visits = this._visitService.Value.GetVisits(vpGuarantorId, financePlan.FinancePlanVisits.Select(x => x.VisitId).ToList()) ?? new List<Visit>();

            // set balance transfer status and publish on an FP
            if (financePlan.IsActiveOriginated())
            {
                OutboundVisitMessageList outboundVisitMessageList = new OutboundVisitMessageList { Messages = new List<OutboundVisitMessage>() };
                IList<Visit> newlyFinancedVisits = visits.Where(x => !financePlan.OriginalFinancePlan?.HasFinancePlanVisitForVisitId(x.VisitId) ?? true).ToList();

                if (financePlan.IsCombined)
                {
                    IList<FinancePlan> originalFinancePlans = this._financePlanService.Value.GetAllFinancePlansForGuarantor(financePlan.VpGuarantor, new List<FinancePlanStatusEnum> { FinancePlanStatusEnum.Canceled });
                    originalFinancePlans = originalFinancePlans.Where(x => x.CombinedFinancePlan?.FinancePlanId == financePlanId).ToList();

                    //remove any visits that were on the original fp
                    newlyFinancedVisits = newlyFinancedVisits.Where(x => originalFinancePlans.All(fp => !fp.HasFinancePlanVisitForVisitId(x.VisitId))).ToList();
                }

                foreach (Visit visit in newlyFinancedVisits)
                {
                    this._balanceTransferService.Value.SetBalanceTransferStatusEligible(visit, "Finance Plan Created");
                    OutboundVisitMessage outboundVisitMessage = new OutboundVisitMessage
                    {
                        ActionDate = DateTime.UtcNow,
                        BillingSystemId = visit.BillingSystem.BillingSystemId,
                        SourceSystemKey = visit.SourceSystemKey ?? visit.MatchedSourceSystemKey,
                        VpOutboundVisitMessageType = VpOutboundVisitMessageTypeEnum.VisitAddedToFinancePlan,
                        VpVisitId = visit.VisitId,
                        OriginalFinancePlanDuration = financePlan.OriginalDuration,
                        IsCallCenter = financePlan.VpGuarantor.IsOfflineGuarantor(),
                        IsAutoPay = financePlan.VpGuarantor.UseAutoPay
                    };
                    outboundVisitMessageList.Messages.Add(outboundVisitMessage);
                }

                if (outboundVisitMessageList.Messages.Any())
                {
                    this.Bus.Value.PublishMessage(outboundVisitMessageList);
                }
            }


        }

        public void OnFinancePlanClosed(int vpGuarantorId, int financePlanId)
        {
            FinancePlan financePlan = this._financePlanService.Value.GetFinancePlan(new Guarantor { VpGuarantorId = vpGuarantorId }, financePlanId);
            if (financePlan == null || !financePlan.IsClosed())
            {
                return;
            }

            if (financePlan.FinancePlanStatus.FinancePlanStatusEnum == FinancePlanStatusEnum.NonPaidInFull ||
                financePlan.FinancePlanStatus.FinancePlanStatusEnum == FinancePlanStatusEnum.PaidInFull)
            {
                // send finance plan closed email
                this._session.Transaction.RegisterPostCommitSuccessAction(fp =>
                {
                    this.Bus.Value.PublishMessage(new SendFinancePlanClosedEmailMessage
                    {
                        VpGuarantorId = financePlan.VpGuarantor.VpGuarantorId
                    }).Wait();
                }, financePlan);
            }

            // send outbound visit message
            if (financePlan.FinancePlanStatusHistory.Any(x => x.FinancePlanStatus.IsActiveOriginated()))
            {
                foreach (FinancePlanVisit financePlanVisit in financePlan.ActiveFinancePlanVisits)
                {
                    this._session.Transaction.RegisterPostCommitSuccessAction(fpvv =>
                    {
                        this.Bus.Value.PublishMessage(new OutboundVisitMessage
                        {
                            ActionDate = DateTime.UtcNow,
                            BillingSystemId = Math.Abs(fpvv.BillingSystemId),
                            SourceSystemKey = fpvv.SourceSystemKey ?? fpvv.MatchedSourceSystemKey,
                            VpOutboundVisitMessageType = VpOutboundVisitMessageTypeEnum.VisitRemovedFromFinancePlan,
                            VpVisitId = fpvv.VisitId,
                            FinancePlanClosedReasonEnum = FinancePlanClosedReasonEnum.FpClosed,
                            IsCallCenter = financePlan.VpGuarantor.IsOfflineGuarantor(),
                            IsAutoPay = financePlan.VpGuarantor.UseAutoPay
                        }).Wait();
                    }, financePlanVisit.FinancePlanVisitVisit);
                }
            }

        }

        private void AfterChangeEventFinancePlan(IList<int> vpGuarantorIds, IList<int> visitIdsChanged)
        {
            List<FinancePlan> financePlans = new List<FinancePlan>();
            foreach (int vpg in vpGuarantorIds.Distinct().ToList())
            {
                Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpg);
                financePlans.AddRange(this._financePlanService.Value.GetAllOpenFinancePlansForGuarantor(guarantor));
            }

            IList<Visit> listOfVisits = this._visitService.Value.GetAllVisitsWithIntList(visitIdsChanged.Distinct().ToList(), false);

            IDictionary<int, FinancePlan> financePlansForVisits = this._financePlanService.Value.GetMostRecentFinancePlansForVisitsIds(visitIdsChanged);

            IDictionary<int, FinancePlanVisit> mostRecentFinancePlanVisitForNonFinancedVisitIds = new Dictionary<int, FinancePlanVisit>();
            foreach (KeyValuePair<int, FinancePlan> financePlanForVisit in financePlansForVisits)
            {
                IList<FinancePlan> openFinancePlansForVisit = financePlans.Where(x => x.FinancePlanVisits.Any(y => y.VisitId == financePlanForVisit.Key)).ToList();

                //fpv is still on an fp and will be processed normally
                if (openFinancePlansForVisit.Any(x => x.FinancePlanVisits.First(y => y.VisitId == financePlanForVisit.Key).HardRemoveDate == null))
                {
                    continue;
                }

                FinancePlanVisit mostRecentFinancePlanVisitForVisit = financePlanForVisit.Value.FinancePlanVisits.FirstOrDefault(y => y.VisitId == financePlanForVisit.Key);

                if (mostRecentFinancePlanVisitForVisit != null)
                {
                    mostRecentFinancePlanVisitForNonFinancedVisitIds.Add(financePlanForVisit.Key, mostRecentFinancePlanVisitForVisit);
                }
            }

            //Closed-end - account for any balance changes due to hospital adjustments
            using (IUnitOfWork uow = new UnitOfWork(this._session))
            {
                foreach (FinancePlan financePlan in financePlans)
                {
                    financePlan.AddFinancePlanVisitPrincipalAmountAdjustments();
                    this._financePlanService.Value.UpdateFinancePlan(financePlan);
                }

                //add principal amount adjustments on the most recent active fp for any inactive visits
                foreach (KeyValuePair<int, FinancePlanVisit> mostRecentFinancePlanVisitForVisitId in mostRecentFinancePlanVisitForNonFinancedVisitIds)
                {
                    FinancePlan financePlan = mostRecentFinancePlanVisitForVisitId.Value.FinancePlan;
                    FinancePlanVisit financePlanVisit = mostRecentFinancePlanVisitForVisitId.Value;

                    financePlan.AddFinancePlanVisitPrincipalAmountAdjustments(financePlanVisit);
                    this._financePlanService.Value.UpdateFinancePlan(financePlan);
                }

                foreach (Visit visit in listOfVisits)
                {
                    this._financePlanService.Value.UpdateFinancePlanDueToVisitChange(visit);
                }

                uow.Commit();
            }
        }

        // Introduced with VP-3938
        public void ApplyInterestRefundIncentive(int financePlanId)
        {
            FinancePlan financePlan = this._financePlanService.Value.GetFinancePlanById(financePlanId);
            FinancePlanInterestRefundIncentiveEligibilityEnum refundEligibilityEnum = this._financePlanIncentiveService.Value.IsFinancePlanEligibleForInterestRefundIncentive(financePlan);
            bool isEligible = refundEligibilityEnum.IsInCategory(FinancePlanInterestRefundIncentiveEligibilityEnumCategory.IsEligible);
            if(isEligible)
            {
                using (IUnitOfWork uow = new UnitOfWork(this._session))
                {
                    decimal amountToRefund = this._financePlanIncentiveService.Value.AmountOfInterestToReallocateToPrincipalForIncentive(financePlan);
                    if (amountToRefund < 0m)
                    {
                        this.ReallocateInterestToPrincipal(financePlan.VpGuarantor.User.VisitPayUserId, financePlan.VpGuarantor.VpGuarantorId, financePlanId, decimal.Negate(amountToRefund));
                        this.LoggingService.Value.Info(()=> $"{nameof(this.ApplyInterestRefundIncentive)} reallocating {amountToRefund} for FinancePlanId: {financePlanId}");
                    }

                    financePlan.SetFinancePlanIncentiveInterestRate(0m);
                    financePlan.WriteOffInterestDue();
                    this._financePlanService.Value.UpdateFinancePlan(financePlan);

                    int vpGuarantorId = financePlan.VpGuarantor.VpGuarantorId;
                    this._session.Transaction.RegisterPostCommitSuccessAction((g) =>
                    {
                        //TODO: wire up email (future story)
                        //this.Bus.Value.PublishMessage(new SendFinancePlanIncentiveRefundInterestEmailMessage
                        //{
                        //    VpGuarantorId = g
                        //}).Wait();

                        this._metricsProvider.Value.Increment(Metrics.Increment.FinancePlan.FinancePlanIncentiveRefundInterest);
                    }, vpGuarantorId );

                    uow.Commit();
                }
            }
        }

        // Reallocating interest to principal can be applied at the finance plan level or at the visit level 
        // When a financePlanVisitId is provided the reallocation will be applied at the visit level. 
        public ReallocateInterestToPrincipalDto ReallocateInterestToPrincipal(int visitPayUserId, int vpGuarantorId, int financePlanId, decimal amountToReallocated, int? financePlanVisitId = null)
        {
            ReallocateInterestToPrincipalDto model = new ReallocateInterestToPrincipalDto() { IsSuccessful = false, TotalToReallocate = amountToReallocated };
            using (UnitOfWork work = new UnitOfWork(this._session))
            {
                FinancePlan financePlan = this._financePlanService.Value.GetFinancePlan(new Guarantor { VpGuarantorId = vpGuarantorId }, financePlanId);
                if (financePlan == null)
                {
                    model.ErrorMessage = "Couldn't find FinancePlan.";
                    return model;
                }

                decimal? assessedInterest = 0;
                decimal interestPaid = 0;
                FinancePlanVisit targetFinancePlanVisit = financePlanVisitId.HasValue ? financePlan.FinancePlanVisits.FirstOrDefault(x => x.FinancePlanVisitId == financePlanVisitId.Value) : null;

                // To handle suspended or recalled visit we need to look at interest assessed on a non-active visit
                if (financePlanVisitId.HasValue)
                {
                    FinancePlanVisit financePlanVisit = financePlan.FinancePlanVisits.FirstOrDefault(x => x.FinancePlanVisitId == financePlanVisitId.Value);
                    assessedInterest = financePlanVisit?.InterestAssessed;
                    interestPaid = decimal.Negate(financePlanVisit?.InterestPaid ?? 0);
                }
                else
                {
                    assessedInterest = financePlan.InterestAssessed;
                    interestPaid = decimal.Negate(financePlan.InterestPaidForDate(DateTime.UtcNow));
                }

                if (amountToReallocated > assessedInterest)
                {
                    model.ErrorMessage = "Can't reallocate more than assessed.";
                }

                model.InterestAssessed = (assessedInterest ?? 0m);
                model.InterestPaid = interestPaid;

                if (model.AmountToReallocate > 0)
                {
                    IEnumerable<FinancePlanVisit> financePlanVisits = (financePlanVisitId.HasValue) ? targetFinancePlanVisit.ToListOfOne() : financePlan.FinancePlanVisits;
                    Dictionary<int, decimal> maxInterestPerVisit = financePlanVisits
                        .ToDictionary(
                        k => k.VisitId,
                        v => decimal.Negate(v.InterestPaid));
                    PaymentDto payment = this._paymentReversalApplicationService.Value.ReallocateFromInterestToPrincipal(financePlan.VpGuarantor.VpGuarantorId, visitPayUserId, financePlan.FinancePlanId, model.AmountToReallocate, maxInterestPerVisit);
                    financePlan.OnInterestReallocationPayment(0m, DateTime.UtcNow, Mapper.Map<Payment>(payment), "Reallocating interest to principal", visitPayUserId);

                    model.IsSuccessful = true;
                    model.PaymentId = payment.PaymentId;
                }
                if (model.AmountToWriteOff > 0)
                {
                    if (targetFinancePlanVisit != null)
                    {
                        financePlan.WriteOffInterestForFinancePlanVisit(model.AmountToWriteOff, targetFinancePlanVisit);
                    }
                    else
                    {
                        financePlan.WriteOffInterest(model.AmountToWriteOff);
                    }

                    model.IsSuccessful = true;
                }

                this._financePlanService.Value.UpdateFinancePlan(financePlan);
                work.Commit();
            }

            return model;
        }

        public void RoboRefund_AddNonNaturalQualifyingEvents(int vpGuarantorId)
        {
            this._roboRefundService.Value.AddNonNaturalQualifyingEvents(vpGuarantorId);
        }

        public IList<PaymentDto> RoboRefund(int vpGuarantorId, DateTime currentStatementEndDate)
        {
            IList<RoboRefundPayment> results = this._roboRefundService.Value.RoboRefund(vpGuarantorId);
            IList<PaymentDto> paymentDtos = new List<PaymentDto>();
            List<int> allFinancePlanIds = results.SelectMany(x => x.Visits)
                .SelectMany(y => y.RoboRefundFinancePlanVisits)
                .Select(y => y.FinancePlanId)
                .Distinct()
                .ToList();

            foreach (int financePlanId in allFinancePlanIds)
            {
                PaymentDto payment = null;
                //Begin reallocate to principal
                //Get all financePlanVisits for this FP.
                List<RoboRefundFinancePlanVisit> financePlanVisits = results.SelectMany(x => x.Visits).SelectMany(y => y.RoboRefundFinancePlanVisits).Where(z => z.FinancePlanId == financePlanId).Distinct().ToList();
                //Create dictionary per visit id
                Dictionary<int, decimal> maxPer = financePlanVisits.ToDictionary(k => k.RoboRefundVisit.VisitId, v => decimal.Negate(v.AmountToReallocateFromInterestToPrincipal));
                decimal totalAmountToReallocate = maxPer.Values.Sum();
                FinancePlan financePlan = this._financePlanService.Value.GetFinancePlan(new Guarantor { VpGuarantorId = vpGuarantorId }, financePlanId);
                if (totalAmountToReallocate > 0m)
                {
                    payment = this._paymentReversalApplicationService.Value.ReallocateFromInterestToPrincipal(financePlan.VpGuarantor.VpGuarantorId, null, financePlan.FinancePlanId, totalAmountToReallocate, maxPer);
                    financePlan.OnInterestReallocationPayment(0m, DateTime.UtcNow, Mapper.Map<Payment>(payment), "Reallocating interest to principal", null);
                    this._financePlanService.Value.UpdateFinancePlan(financePlan);
                    paymentDtos.Add(payment);
                }
                //End Reallocate to principal

                //Begin write off interest
                if (financePlanVisits.Any(y => y.AmountOfInterestToWriteOff > 0))
                {
                    foreach (RoboRefundFinancePlanVisit roboRefundFinancePlanVisit in financePlanVisits.Where(x => x.AmountOfInterestToWriteOff > 0))
                    {
                        FinancePlanVisit found = financePlan.FinancePlanVisits.FirstOrDefault(z => z.FinancePlanVisitId == roboRefundFinancePlanVisit.FinancePlanVisitId);
                        found?.WriteOffInterest(roboRefundFinancePlanVisit.AmountOfInterestToWriteOff, currentStatementEndDate.AddMilliseconds(-10));
                    }
                    this._financePlanService.Value.UpdateFinancePlan(financePlan);
                }
                //End write off interest.

                //House keeping, link the payments created to RoboRefundPayment
                if (payment != null)
                {
                    List<RoboRefundPayment> roboPayments = financePlanVisits.Select(x => x.RoboRefundVisit.RoboRefundPayment).Distinct().ToList();
                    foreach (RoboRefundPayment roboRefundPayment in roboPayments)
                    {
                        List<RoboRefundFinancePlanVisit> visitsInPayment = roboRefundPayment.Visits.SelectMany(x => x.RoboRefundFinancePlanVisits).Where(y => financePlanVisits.Contains(y)).Distinct().ToList();
                        Dictionary<int, decimal> paymentMaxPer = visitsInPayment.ToDictionary(k => k.RoboRefundFinancePlanVisitId, v => decimal.Negate(v.AmountToReallocateFromInterestToPrincipal));
                        this._roboRefundService.Value.AssociateReallocationPaymentToRoboPayment(payment.PaymentId, financePlanId, roboRefundPayment, paymentMaxPer);
                    }
                }
            }

            return paymentDtos;
        }
        
        public FinancePlanCancelPaymentDto GetFinancePlanCancelPayment(int vpGuarantorId)
        {
            FinancePlanCancelPaymentDto financePlanCancelPaymentDto = new FinancePlanCancelPaymentDto();

            IList<FinancePlan> financePlans = this._financePlanService.Value.GetAllOpenFinancePlansForGuarantor(new Guarantor { VpGuarantorId = vpGuarantorId });
            foreach (FinancePlan financePlan in financePlans)
            {
                decimal maxBucketAmountDue = 0m;
                if (financePlan.Buckets.Count > 0)
                {
                    maxBucketAmountDue = financePlan.Buckets.OrderBy(x => x.Key).Last().Value.Item1;
                }

                FinancePlanDto financePlanDto = Mapper.Map<FinancePlanDto>(financePlan);
                this.SetAmountDueForDisplay(financePlanDto);

                financePlanCancelPaymentDto.AmountDueBefore += financePlanDto.AmountDue;
                financePlanCancelPaymentDto.AmountDueAfter += financePlanDto.AmountDue - maxBucketAmountDue;
            }

            FinancePlanAmountDue lastAmountDue = financePlans.SelectMany(x => x.FinancePlanAmountsDue).Where(x => x.VpStatementId.HasValue).OrderByDescending(x => x.DueDate).FirstOrDefault();
            DateTime now = DateTime.UtcNow;
            DateTime dueDate = lastAmountDue?.OverrideDueDate ?? lastAmountDue?.DueDate ?? now;
            
            if (dueDate < now)
            {
                Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
                dueDate = this._statementService.Value.GetNextPaymentDate(guarantor);
            }

            financePlanCancelPaymentDto.PaymentDueDate = dueDate;
            financePlanCancelPaymentDto.HasPastDue = financePlans.Any(x => x.IsPastDue());

            return financePlanCancelPaymentDto;
        }

        public void SetAmountDueForDisplay(FinancePlanDto financePlanDto)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(financePlanDto.VpGuarantor.VpGuarantorId);
            VpStatement currentStatement = this._statementService.Value.GetMostRecentStatement(guarantor);

            decimal amountDue = financePlanDto.PastDueAmount;
            decimal paymentAmount = Math.Min(financePlanDto.PaymentAmount, financePlanDto.CurrentFinancePlanBalance);

            if (currentStatement.IsGracePeriod)
            {
                amountDue += financePlanDto.CurrentAmount;
            }
            else
            {
                amountDue += financePlanDto.CurrentAmount > 0m ? financePlanDto.CurrentAmount : paymentAmount;
            }

            financePlanDto.AmountDue = Math.Min(amountDue, financePlanDto.CurrentFinancePlanBalance);
        }

        public async Task LogTermsDisplayedAsync(FinancePlanTermsDto financePlanTermsDto, decimal? minimumMonthlyPaymentAmount, int? maximumNumberOfPayments)
        {
            bool isClient = this.ApplicationSettingsService.Value.Application.Value == ApplicationEnum.Client;

            FinancePlanTermsDisplayedMessage financePlanTermsDisplayedMessage = new FinancePlanTermsDisplayedMessage
            {
                EffectiveDate = financePlanTermsDto.EffectiveDate,
                FinalPaymentDate = financePlanTermsDto.FinalPaymentDate,
                FinancePlanBalance = financePlanTermsDto.FinancePlanBalance,
                FinancePlanOfferSetType = (FinancePlanOfferSetTypeEnum)financePlanTermsDto.FinancePlanOfferSetType.FinancePlanOfferSetTypeId,
                InsertDate = DateTime.UtcNow,
                InterestRate = financePlanTermsDto.InterestRate,
                IsClient = isClient,
                IsCombined = financePlanTermsDto.IsCombined,
                MaximumNumberOfPayments = maximumNumberOfPayments,
                MinimumMonthlyPaymentAmount = minimumMonthlyPaymentAmount,
                MonthlyPaymentAmount = financePlanTermsDto.MonthlyPaymentAmount,
                MonthlyPaymentAmountOtherPlans = financePlanTermsDto.MonthlyPaymentAmountOtherPlans,
                MonthlyPaymentAmountPrevious = financePlanTermsDto.MonthlyPaymentAmountPrevious,
                MonthlyPaymentAmountTotal = financePlanTermsDto.MonthlyPaymentAmountTotal,
                NumberMonthlyPayments = financePlanTermsDto.NumberMonthlyPayments,
                TotalInterest = financePlanTermsDto.TotalInterest,
                VpGuarantorId = financePlanTermsDto.VpGuarantorId,
            };

            await this.Bus.Value.PublishMessage(financePlanTermsDisplayedMessage);
        }

        private ReallocateInterestToPrincipalDto ReallocateInterestToPrincipalForFinancePlanVisit(int visitPayUserId, FinancePlan financePlan, FinancePlanVisit financePlanVisit)
        {
            //Note, passing InterestAssessed, not InterestPaid. This will ensure interest assess beyond what has been paid will be written off. 
            decimal amountToReallocate = financePlanVisit.InterestAssessed;

            if (amountToReallocate == 0m)
            {
                return null;
            }

            ReallocateInterestToPrincipalDto reallocateInterestToPrincipal = this.ReallocateInterestToPrincipal(visitPayUserId,
                financePlan.VpGuarantor.VpGuarantorId,
                financePlan.FinancePlanId,
                amountToReallocate,
                financePlanVisit.FinancePlanVisitId);
            return reallocateInterestToPrincipal;
        }

        public void ProcessFinancePlanVisitRefundInterestMessage(FinancePlanVisitRefundInterestMessage message)
        {
            IList<FinancePlanVisit> financePlanVisits = this._financePlanService.Value.GetVisitOnAnyFinancePlan(message.VisitId);

            FinancePlanVisit financePlanVisit = financePlanVisits?.Where(x => x.FinancePlan.IsActiveOriginated())
                .OrderByDescending(x => x.InsertDate)
                .FirstOrDefault();

            if (financePlanVisit == null)
            {
                return;
            }

            FinancePlan financePlan = financePlanVisit.FinancePlan;
            int visitPayUserId = financePlan.VpGuarantor.User.VisitPayUserId;

            ReallocateInterestToPrincipalDto reallocateInterestToPrincipal = this.ReallocateInterestToPrincipalForFinancePlanVisit(visitPayUserId,
                financePlan,
                financePlanVisit);
        }

        public bool MeetsCriteriaToChangePaymentDueDate(int vpGuarantorId)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            return this._financePlanService.Value.MeetsCriteriaToChangePaymentDueDate(guarantor);
        }

        public int? GetFinancePlanOfferId(string financePlanPublicIdPhi, int? vpGuarantorId)
        {
            if (!vpGuarantorId.HasValue)
            {
                return null;
            }

            string publicId = financePlanPublicIdPhi.RemoveNonNumericCharacters();
            FinancePlan financePlan = this._financePlanService.Value.GetFinancePlanByPublicIdPhi(publicId);
            return financePlan?.VpGuarantor.VpGuarantorId == vpGuarantorId ? financePlan.FinancePlanOffer.FinancePlanOfferId : (int?)null;
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(FinancePlanCreatedMessage message, ConsumeContext<FinancePlanCreatedMessage> consumeContext)
        {
            this.OnFinancePlanCreated(message.VpGuarantorId, message.FinancePlanId);
        }

        public bool IsMessageValid(FinancePlanCreatedMessage message, ConsumeContext<FinancePlanCreatedMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(FinancePlanClosedMessage message, ConsumeContext<FinancePlanClosedMessage> consumeContext)
        {
            this.OnFinancePlanClosed(message.VpGuarantorId, message.FinancePlanId);
        }

        public bool IsMessageValid(FinancePlanClosedMessage message, ConsumeContext<FinancePlanClosedMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(FinancePlanVisitFinancialAssistanceFlagChangedMessage message, ConsumeContext<FinancePlanVisitFinancialAssistanceFlagChangedMessage> consumeContext)
        {
#pragma warning disable 809
            Task.Run(() => this.RecalculateFinancePlanDueToFinancialAssistanceFlagChangeAsync(message)).Wait();
#pragma warning restore 809
        }

        public bool IsMessageValid(FinancePlanVisitFinancialAssistanceFlagChangedMessage message, ConsumeContext<FinancePlanVisitFinancialAssistanceFlagChangedMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(FinancePlanRefundInterestOnRecall message, ConsumeContext<FinancePlanRefundInterestOnRecall> consumeContext)
        {
            FinancePlan financePlan = this._financePlanService.Value.GetFinancePlanById(message.FinancePlanId);
            FinancePlanVisit financePlanVisit = financePlan.FinancePlanVisits.FirstOrDefault(x => x.FinancePlanVisitId == message.FinancePlanVisitId);

            if (financePlanVisit == null)
            {
                return;
            }

            int visitPayUserId = financePlan.VpGuarantor.User.VisitPayUserId;
            ReallocateInterestToPrincipalDto reallocateInterestToPrincipal = this.ReallocateInterestToPrincipalForFinancePlanVisit(visitPayUserId,
                financePlan,
                financePlanVisit);

        }

        public bool IsMessageValid(FinancePlanRefundInterestOnRecall message, ConsumeContext<FinancePlanRefundInterestOnRecall> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(FinancePlanVisitRefundInterestMessage message, ConsumeContext<FinancePlanVisitRefundInterestMessage> consumeContext)
        {
            this.ProcessFinancePlanVisitRefundInterestMessage(message);
        }

        public bool IsMessageValid(FinancePlanVisitRefundInterestMessage message, ConsumeContext<FinancePlanVisitRefundInterestMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(ChangeEventProcessedForVpGuarantor message, ConsumeContext<ChangeEventProcessedForVpGuarantor> consumeContext)
        {
            this.AfterChangeEventFinancePlan(message.VpGuarantorIds, message.VisitIds);
        }

        public bool IsMessageValid(ChangeEventProcessedForVpGuarantor message, ConsumeContext<ChangeEventProcessedForVpGuarantor> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion

        #region Jobs

        void IJobRunnerService<RemovePaymentBucketsForFinancePlanJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            string usageString = "Should be 'Ivh.Console.JobRunner.exe RemovePaymentBucketsForFinancePlan GuarantorId FinancePlanId NumberOfBucketsToKeep'";
            int financePlanId = -1;
            int guarantorId = -1;
            int bucketsToKeep = -1;
            try
            {
                this.LoggingService.Value.Info(() => $"RemovePaymentBucketsForFinancePlanJobRunner::Execute Start - Time = {DateTime.UtcNow}");
                System.Console.WriteLine($"RemovePaymentBucketsForFinancePlanJobRunner::Execute Start - Time = {DateTime.UtcNow}");

                if (args.Length < 2 || !int.TryParse(args[1], out guarantorId))
                {
                    this.LoggingService.Value.Info(() => string.Format("RemovePaymentBucketsForFinancePlanJobRunner::Execute failed to parse the guarantorId.  " + usageString));
                    System.Console.WriteLine(string.Format("RemovePaymentBucketsForFinancePlanJobRunner::Execute failed to parse the guarantorId.  " + usageString));
                    return;
                }

                if (args.Length < 3 || !int.TryParse(args[2], out financePlanId))
                {
                    this.LoggingService.Value.Info(() => string.Format("RemovePaymentBucketsForFinancePlanJobRunner::Execute failed to parse the financePlanId.  " + usageString));
                    System.Console.WriteLine(string.Format("RemovePaymentBucketsForFinancePlanJobRunner::Execute failed to parse the financePlanId.  " + usageString));
                    return;
                }

                if (args.Length < 4 || !int.TryParse(args[3], out bucketsToKeep))
                {
                    this.LoggingService.Value.Info(() => string.Format("RemovePaymentBucketsForFinancePlanJobRunner::Execute failed to parse the NumberOfBucketsToKeep.  " + usageString));
                    System.Console.WriteLine(string.Format("RemovePaymentBucketsForFinancePlanJobRunner::Execute failed to parse the NumberOfBucketsToKeep.  " + usageString));
                    return;
                }

                this.LoggingService.Value.Info(() => $"RemovePaymentBucketsForFinancePlanJobRunner::Execute removing buckets for Guarantor: {guarantorId}, FinancePlanId: {financePlanId}, BucketsToKeep: {bucketsToKeep}");
                Console.WriteLine($"RemovePaymentBucketsForFinancePlanJobRunner::Execute removing buckets for Guarantor: {guarantorId}, FinancePlanId: {financePlanId}, BucketsToKeep: {bucketsToKeep}");

                this.CancelPaymentBucketsForFinancePlan(financePlanId, guarantorId, bucketsToKeep, null);
            }
            catch (Exception e)
            {
                this.LoggingService.Value.Fatal(() => string.Format("RemovePaymentBucketsForFinancePlanJobRunner::Execute removing buckets FAILED for Guarantor: {0}, FinancePlanId: {1}, BucketsToKeep: {2}, Stack: ", guarantorId, financePlanId, bucketsToKeep, ExceptionHelper.AggregateExceptionToString(e)));
                System.Console.WriteLine(string.Format("RemovePaymentBucketsForFinancePlanJobRunner::Execute removing buckets FAILED for Guarantor: {0}, FinancePlanId: {1}, BucketsToKeep: {2}, Stack: ", guarantorId, financePlanId, bucketsToKeep, ExceptionHelper.AggregateExceptionToString(e)));
                throw;
            }


            this.LoggingService.Value.Info(() => $"RemovePaymentBucketsForFinancePlanJobRunner::Execute End - Time = {DateTime.UtcNow}");
            System.Console.WriteLine($"RemovePaymentBucketsForFinancePlanJobRunner::Execute End - Time = {DateTime.UtcNow}");
        }

        public IList<FinancePlanDto> GetFinancePlans(int vpGuarantorId)
        {
            IList<FinancePlan> financePlans = this._financePlanService.Value.GetFinancePlans(vpGuarantorId);
            IList<FinancePlanDto> financePlanDtos = Mapper.Map<IList<FinancePlanDto>>(financePlans);
            return financePlanDtos;
        }

        #endregion
    }
}
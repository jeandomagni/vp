﻿namespace Ivh.Application.FinanceManagement.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.Content.Entities;
    using Domain.Content.Interfaces;
    using Domain.FinanceManagement.ServiceGroup.Entities;
    using Domain.FinanceManagement.ServiceGroup.Interfaces;

    public class ServiceGroupApplicationService : IServiceGroupApplicationService
    {
        private const string ServiceGroupNameReplacementTag = "[[ServiceGroupName]]";

        private readonly Lazy<IContentService> _contentService;
        private readonly Lazy<IServiceGroupService> _serviceGroupService;

        public ServiceGroupApplicationService(
            Lazy<IServiceGroupService> serviceGroupService,
            Lazy<IContentService> contentService)
        {
            this._serviceGroupService = serviceGroupService;
            this._contentService = contentService;
        }

        public async Task<IList<ServiceGroupDto>> GetAllServiceGroupsWithContentAsync()
        {
            IList<ServiceGroup> serviceGroups = this._serviceGroupService.Value.GetAllServiceGroups();
            IList<ServiceGroupDto> serviceGroupDtos = Mapper.Map<IList<ServiceGroupDto>>(serviceGroups);

            foreach (ServiceGroupDto serviceGroupDto in serviceGroupDtos)
            {
                ServiceGroup serviceGroup = serviceGroups.FirstOrDefault(x => x.ServiceGroupId == serviceGroupDto.ServiceGroupId);
                if (serviceGroup == null)
                {
                    continue;
                }

                Dictionary<string, string> replacementTags = new Dictionary<string, string>
                {
                    {ServiceGroupNameReplacementTag, serviceGroup.ServiceGroupName}
                };

                if (serviceGroup.DescriptionCmsRegion.HasValue)
                {
                    CmsVersion cmsVersion = await this._contentService.Value.GetCurrentVersionAsync(serviceGroup.DescriptionCmsRegion.Value, true, replacementTags);
                    if (cmsVersion != null && !string.IsNullOrWhiteSpace(cmsVersion.ContentBody))
                    {
                        serviceGroupDto.Description = cmsVersion.ContentBody;
                    }
                }

                if (serviceGroup.StatementDisclaimerCmsRegion.HasValue)
                {
                    CmsVersion cmsVersion = await this._contentService.Value.GetCurrentVersionAsync(serviceGroup.StatementDisclaimerCmsRegion.Value, true, replacementTags);
                    if (cmsVersion != null && !string.IsNullOrWhiteSpace(cmsVersion.ContentBody))
                    {
                        serviceGroupDto.StatementDisclaimer = cmsVersion.ContentBody;
                    }
                }
            }

            return serviceGroupDtos;
        }
    }
}
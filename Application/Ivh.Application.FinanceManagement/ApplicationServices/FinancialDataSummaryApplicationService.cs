﻿namespace Ivh.Application.FinanceManagement.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Base.Common.Dtos;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Core.Common.Dtos;
    using Domain.FinanceManagement.Interfaces;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using Domain.FinanceManagement.Statement.Entities;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Jobs;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;
    using MassTransit;
    using NHibernate;

    /// <summary>
    ///     This is a read only class to aggregate the highly used summary data in the system.
    /// </summary>
    public class FinancialDataSummaryApplicationService : ApplicationService, IFinancialDataSummaryApplicationService
    {
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IVisitService> _visitService;
        private readonly Lazy<IVpStatementService> _vpStatementService;
        private readonly Lazy<IPaymentSubmissionApplicationService> _paymentSubmissionApplicationService;
        private readonly Lazy<IStatementApplicationService> _statementApplicationService;
        private readonly Lazy<IFinancePlanService> _financePlanService;
        private readonly Lazy<IPaymentService> _paymentService;
        private readonly Lazy<IVisitBalanceBaseApplicationService> _visitBalanceBaseApplicationService;
        private readonly Lazy<IStatementDateService> _statementDateService;
        private readonly ISession _session;

        public FinancialDataSummaryApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IVisitService> visitService,
            Lazy<IVpStatementService> vpStatementService,
            Lazy<IPaymentSubmissionApplicationService> paymentSubmissionApplicationService,
            Lazy<IStatementApplicationService> statementApplicationService,
            Lazy<IFinancePlanService> financePlanService,
            Lazy<IPaymentService> paymentService,
            Lazy<IVisitBalanceBaseApplicationService> visitBalanceBaseApplicationService,
            Lazy<IStatementDateService> statementDateService,
            ISessionContext<VisitPay> sessionContext) : base(applicationServiceCommonService)
        {
            this._guarantorService = guarantorService;
            this._visitService = visitService;
            this._vpStatementService = vpStatementService;
            this._paymentSubmissionApplicationService = paymentSubmissionApplicationService;
            this._statementApplicationService = statementApplicationService;
            this._financePlanService = financePlanService;
            this._paymentService = paymentService;
            this._visitBalanceBaseApplicationService = visitBalanceBaseApplicationService;
            this._statementDateService = statementDateService;
            this._session = sessionContext.Session;
        }

        public FinancialDataSummaryDto GetFinancialDataSummaryByGuarantor(int guarantorId)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(guarantorId);
            if (guarantor == null)
            {
                return null;
            }

            VpStatement currentStatement = this._vpStatementService.Value.GetMostRecentStatement(guarantor);

            IReadOnlyList<Visit> visits = this._visitService.Value.GetVisits(guarantor.VpGuarantorId);
            IList<int> pendingFinancedVisits = this._financePlanService.Value.GetVisitIdsOnPendingFinancePlans(guarantorId);
            IList<VisitDto> visitDtos = Mapper.Map<IList<VisitDto>>(visits);
            this._visitBalanceBaseApplicationService.Value.SetTotalBalance(visitDtos);

            IList<VisitDto> allActiveVisitsIncludingFinancedDtos = visitDtos.Where(x => x.CurrentVisitState == VisitStateEnum.Active).ToList();
            IList<int> financedVisitIds = this._financePlanService.Value.GetVisitIdsOnActiveFinancePlans(guarantorId);
            IList<VisitDto> allNonFinancedVisitDtos = allActiveVisitsIncludingFinancedDtos.Where(x => !financedVisitIds.Contains(x.VisitId)).ToList();

            bool isGracePeriod = currentStatement?.IsGracePeriod ?? false;
            List<VisitDto> selectedVisits = allNonFinancedVisitDtos.ToList();
            if (pendingFinancedVisits?.Count > 0)
            {
                selectedVisits = allNonFinancedVisitDtos.Where(x => pendingFinancedVisits.Contains(x.VisitId)).ToList();
            }

            FinancialDataSummaryDto financialDataSummary = new FinancialDataSummaryDto
            {
                TotalBalance = allNonFinancedVisitDtos.Sum(x => x.TotalBalance),
                BalanceInFull = allActiveVisitsIncludingFinancedDtos.Sum(x => x.TotalBalance),
                CurrentStatementId = currentStatement?.VpStatementId,
                StatementNextPaymentDueDate = currentStatement?.PaymentDueDate,
                IsGracePeriod = isGracePeriod,
                IsAwaitingStatement = !isGracePeriod,
                NextStatementDate = (guarantor.NextStatementDate ?? this._statementDateService.Value.GetNextStatementDate(null, guarantor, DateTime.UtcNow)).Date,
                NextPaymentDateForDisplay = this._statementApplicationService.Value.GetNextPaymentDate(guarantorId),
                Visits = allNonFinancedVisitDtos,
                SelectedVisits = selectedVisits
            };

            return financialDataSummary;
        }

        private bool HasFullPaymentScheduled(int vpGuarantorId, IList<VisitBalanceBaseDto> activeVisitBalances, DateTime checkDate)
        {
            List<Payment> scheduledPayments = this._paymentService.Value.GetScheduledPayments(vpGuarantorId).ToList();
            if (!scheduledPayments.Any())
            {
                return false;
            }

            decimal totalBalance = activeVisitBalances.Sum(x => x.TotalBalance);

            // specific visits (only allows payment on non-fp visits)
            IList<Payment> specificVisitPayments = scheduledPayments.Where(x => x.PaymentType == PaymentTypeEnum.ManualScheduledSpecificVisits).ToList();
            if (specificVisitPayments.Any())
            {
                List<int> specificVisitIds = specificVisitPayments
                    .SelectMany(x => x.PaymentScheduledAmounts.Select(psa => psa.VisitId)
                        .Where(visitId => visitId != null)
                        .Select(visitId => visitId.Value))
                    .ToList();

                bool specificVisitsContainsAllActiveVisits = activeVisitBalances.Select(x => x.VisitId).All(x => specificVisitIds.Contains(x));
                if (!specificVisitsContainsAllActiveVisits)
                {
                    return false;
                }

                decimal specificVisitPaymentSum = specificVisitPayments.Sum(x => x.ScheduledPaymentAmount);
                if (specificVisitPaymentSum >= totalBalance)
                {
                    return true;
                }
            }

            // specific amount (only allows payment on non-fp visits) (not until VP-601 but implementing here, now)
            decimal totalSpecificAmount = scheduledPayments.Where(x => x.PaymentType == PaymentTypeEnum.ManualScheduledSpecificAmount).Select(x => x.ScheduledPaymentAmount).Sum();
            if (totalSpecificAmount >= totalBalance)
            {
                return true;
            }

            if (scheduledPayments.Any(x => x.PaymentType.IsInCategory(PaymentTypeEnumCategory.DiscountEligible)))
            {
                DiscountOfferDto discountOffer = this._paymentSubmissionApplicationService.Value.GetDiscountPerVisitForStatementedBalanceInFull(vpGuarantorId);

                if (scheduledPayments.Any(x => x.PaymentType == PaymentTypeEnum.ManualScheduledCurrentNonFinancedBalanceInFull))
                {
                    decimal totalBalanceWithDiscount = totalBalance - (discountOffer?.DiscountTotal ?? 0);

                    IEnumerable<Payment> payments = scheduledPayments.Where(x => x.PaymentType == PaymentTypeEnum.ManualScheduledCurrentNonFinancedBalanceInFull && x.ScheduledPaymentAmount >= totalBalanceWithDiscount);
                    if (payments.Any())
                    {
                        return true;
                    }
                }

                if (scheduledPayments.Any(x => x.PaymentType == PaymentTypeEnum.ManualScheduledCurrentBalanceInFull))
                {
                    IList<int> allActiveVisitIdsIncludingFinanced = this._visitService.Value.GetVisitIds(vpGuarantorId, VisitStateEnum.Active);
                    decimal balanceInFull = this._visitBalanceBaseApplicationService.Value.GetTotalBalances(vpGuarantorId, allActiveVisitIdsIncludingFinanced).Sum(x => x.TotalBalance);
                    decimal balanceInFullWithDiscount = balanceInFull - (discountOffer?.DiscountTotal ?? 0);

                    IEnumerable<Payment> payments = scheduledPayments.Where(x => x.PaymentType == PaymentTypeEnum.ManualScheduledCurrentBalanceInFull && x.ScheduledPaymentAmount >= balanceInFullWithDiscount);
                    if (payments.Any())
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        #region balance due reminder

        public bool ProcessBalanceDueReminder(BalanceDueReminderMessage message)
        {
            FinancialDataSummaryDto financialDataSummary = this.GetFinancialDataSummaryByGuarantor(message.VpGuarantorId);

            if (financialDataSummary?.CurrentStatementId == null)
            {
                this.LoggingService.Value.Info(() => "BalanceDueReminderMessage not processed for Guarantor {0}. Financial Data Summary Null or CurrentStatementId is null.", message.VpGuarantorId);
                return false;
            }

            if (financialDataSummary.TotalBalance <= 0)
            {
                this.LoggingService.Value.Info(() => "BalanceDueReminderMessage not processed for Guarantor {0}. TotalBalance == 0.", message.VpGuarantorId);
                return false;
            }

            VpStatement statement = this._vpStatementService.Value.GetStatement(message.VpStatementId);
            if (statement == null)
            {
                this.LoggingService.Value.Info(() => "BalanceDueReminderMessage not processed for Guarantor {0}. Statement is null.", message.VpGuarantorId);
                return false;
            }

            if (statement.BalanceDueReminderMessageSent)
            {
                this.LoggingService.Value.Info(() => "BalanceDueReminderMessage already processed for Guarantor {0}.", message.VpGuarantorId);
                return false;
            }

            IList<VisitBalanceBaseDto> activeVisitBalances = this._visitBalanceBaseApplicationService.Value.GetTotalBalances(statement.VpGuarantorId);
            if (this.HasFullPaymentScheduled(statement.VpGuarantorId, activeVisitBalances, statement.PaymentDueDate))
            {
                this.LoggingService.Value.Info(() => "BalanceDueReminderMessage not processed for Guarantor {0}. Has Full Payment Scheduled.", message.VpGuarantorId);
                return false;
            }

            this.SendBalanceDueEmail(message.VpGuarantorId, message.PaymentDueDate.Date);

            statement.BalanceDueReminderMessageSent = true;
            this._vpStatementService.Value.InsertOrUpdate(statement);

            return true;
        }

        public void QueueBalanceDueReminders(DateTime dateToProcess)
        {
            foreach (BalanceDueReminderMessage message in this.GetBalanceDueReminderMessages(dateToProcess))
            {
                this.Bus.Value.PublishMessage(message).Wait();
            }
        }

        private void SendBalanceDueEmail(int vpGuarantorId, DateTime paymentDate)
        {
            this.Bus.Value.PublishMessage(new SendPaymentDueReminderEmailMessage
            {
                VpGuarantorId = vpGuarantorId,
                PaymentDate = paymentDate,
            }).Wait();
        }

        public IEnumerable<BalanceDueReminderMessage> GetBalanceDueReminderMessages(DateTime dateToProcess)
        {
            // totalbalance > 0 and no full payment or finance plan scheduled on or before the check is run
            DateTime dueDate = dateToProcess.Date.AddDays(this.Client.Value.BalanceDueNotificationInDays);
            IList<VpStatementPaymentDueDateResult> allStatements = this._vpStatementService.Value.GetStatementsWithPaymentDueDateResults(dueDate);

            int iterationCount = 0;
            foreach (IGrouping<int, VpStatementPaymentDueDateResult> vpStatementPaymentDueDateResult in allStatements.GroupBy(x => x.VpGuarantorId))
            {
                VpStatementPaymentDueDateResult selectedStatement;
                try
                {
                    selectedStatement = vpStatementPaymentDueDateResult.OrderByDescending(x => x.PaymentDueDate).FirstOrDefault();
                    if (selectedStatement == null)
                    {
                        continue;
                    }

                    if (this._guarantorService.Value.IsGuarantorAccountClosed(vpStatementPaymentDueDateResult.Key))
                    {
                        continue;
                    }

                    if (++iterationCount % 100 == 0)
                    {
                        this._session.Clear();
                    }

                    decimal totalBalance = this._visitBalanceBaseApplicationService.Value.GetTotalBalance(vpStatementPaymentDueDateResult.Key);
                    if (totalBalance <= 0)
                    {
                        continue;
                    }
                }
                catch (Exception ex)
                {
                    this.LoggingService.Value.Fatal(() => $"FinancialDataSummaryApplicationService::GetBalanceDueReminderMessages - failed for VpGuarantorId {vpStatementPaymentDueDateResult.Key}", ExceptionHelper.AggregateExceptionToString(ex));
                    continue;
                }

                yield return new BalanceDueReminderMessage
                {
                    VpGuarantorId = selectedStatement.VpGuarantorId,
                    VpStatementId = selectedStatement.VpStatementId,
                    PaymentDueDate = selectedStatement.PaymentDueDate
                };
            }
        }

        #endregion

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(BalanceDueReminderMessage message, ConsumeContext<BalanceDueReminderMessage> consumeContext)
        {
            this.ProcessBalanceDueReminder(message);
        }

        public bool IsMessageValid(BalanceDueReminderMessage message, ConsumeContext<BalanceDueReminderMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion

        #region Jobs
     
        void IJobRunnerService<QueueBalanceDueRemindersJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.QueueBalanceDueReminders(DateTime.UtcNow.Date);
        }

        #endregion
    }
}
﻿namespace Ivh.Application.FinanceManagement.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Core.Common.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.FinanceManagement.Consolidation.Entities;
    using Domain.FinanceManagement.Consolidation.Interfaces;
    using Domain.Settings.Entities;
    using Domain.Settings.Interfaces;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.ServiceBus;
    using Ivh.Common.ServiceBus.Common;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Consolidation;
    using Ivh.Common.VisitPay.Strings;
    using MassTransit;
    using NHibernate;

    public class ManagingUserApplicationService : ApplicationService, IManagingUserApplicationService
    {
        private readonly Lazy<IVisitPayUserApplicationService> _visitPayUserApplicationService;
        private readonly Lazy<IConsolidationGuarantorService> _consolidationGuarantorService;
        private readonly Lazy<IFinancePlanApplicationService> _financePlanApplicationService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly ISession _session;

        public ManagingUserApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IFinancePlanApplicationService> financePlanApplicationService,
            Lazy<IConsolidationGuarantorService> consolidationGuarantorService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            ISessionContext<VisitPay> sessionContext) : base(applicationServiceCommonService)
        {
            this._financePlanApplicationService = financePlanApplicationService;
            this._consolidationGuarantorService = consolidationGuarantorService;
            this._guarantorService = guarantorService;
            this._visitPayUserApplicationService = visitPayUserApplicationService;
            this._session = sessionContext.Session;
        }

        public Task InitiateRelationshipWithGuarantorAsync()
        {
            throw new NotImplementedException();
        }

        public async Task RejectConsolidationAsync(int consolidationGuarantorId, int visitPayUserId)
        {
            ConsolidationGuarantor consolidationGuarantor = await this._consolidationGuarantorService.Value.RejectConsolidationManagingAsync(consolidationGuarantorId, visitPayUserId).ConfigureAwait(false);

            if (consolidationGuarantor.IsInactive()
                && consolidationGuarantor.DateAcceptedByManagingGuarantor.HasValue
                && consolidationGuarantor.DateAcceptedByManagedGuarantor.HasValue
                && !consolidationGuarantor.DateAcceptedFinancePlanTermsByManagingGuarantor.HasValue)
            {
                this._session.Transaction.RegisterPostCommitSuccessAction(() =>
                {
                    this.Bus.Value.PublishMessage(new ConsolidationHouseholdRequestCancelledByManagingToManagedMessage
                    {
                        ManagingVpGuarantorId = consolidationGuarantor.ManagingGuarantor.VpGuarantorId,
                        ManagedVpGuarantorId = consolidationGuarantor.ManagedGuarantor.VpGuarantorId
                    }).Wait();
                });
            }
            else
            {
                this._session.Transaction.RegisterPostCommitSuccessAction(() =>
                {
                    this.Bus.Value.PublishMessage(new ConsolidationHouseholdRequestDeclinedToManagedMessage
                    {
                        ManagingVpGuarantorId = consolidationGuarantor.ManagingGuarantor.VpGuarantorId,
                        ManagedVpGuarantorId = consolidationGuarantor.ManagedGuarantor.VpGuarantorId
                    }).Wait();
                });
            }
        }

        public async Task CancelConsolidationAsync(int consolidationGuarantorId, int visitPayUserId)
        {
            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                ConsolidationGuarantor consolidationGuarantor = await this._consolidationGuarantorService.Value.GetConsolidationGuarantorAsync(consolidationGuarantorId, visitPayUserId).ConfigureAwait(false);
                bool isActive = consolidationGuarantor.IsPendingOrActive();
                bool isPending = consolidationGuarantor.IsPending();
                bool isFinancePlan = consolidationGuarantor.DateAcceptedFinancePlanTermsByManagingGuarantor.HasValue;

                consolidationGuarantor = await this._consolidationGuarantorService.Value.CancelConsolidationAsync(consolidationGuarantorId, visitPayUserId, ConsolidationGuarantorCancellationReasonEnum.CancelledByManaging).ConfigureAwait(false);
                this._session.Transaction.RegisterPostCommitSuccessAction((cg, ip, ia, ifp) =>
                {
                    if (ip)
                    {
                        this.Bus.Value.PublishMessage(new ConsolidationHouseholdRequestCancelledByManagingToManagedMessage
                        {
                            ManagingVpGuarantorId = cg.ManagingGuarantor.VpGuarantorId,
                            ManagedVpGuarantorId = cg.ManagedGuarantor.VpGuarantorId
                        }).Wait();
                    }
                    else if (ia)
                    {
                        this.Bus.Value.PublishMessage(new ConsolidationLinkageCancelledByManagingToManagingMessage
                        {
                            ManagingVpGuarantorId = cg.ManagingGuarantor.VpGuarantorId,
                            ManagedVpGuarantorId = cg.ManagedGuarantor.VpGuarantorId
                        }).Wait();

                        if (ifp)
                        {
                            this.Bus.Value.PublishMessage(new ConsolidationLinkageCancelledByManagingToManagedWithFpMessage
                            {
                                ManagingVpGuarantorId = cg.ManagingGuarantor.VpGuarantorId,
                                ManagedVpGuarantorId = cg.ManagedGuarantor.VpGuarantorId
                            }).Wait();
                        }
                        else
                        {
                            this.Bus.Value.PublishMessage(new ConsolidationLinkageCancelledByManagingToManagedMessage
                            {
                                ManagingVpGuarantorId = cg.ManagingGuarantor.VpGuarantorId,
                                ManagedVpGuarantorId = cg.ManagedGuarantor.VpGuarantorId
                            }).Wait();
                        }
                    }
                }, consolidationGuarantor, isPending, isActive, isFinancePlan);

                uow.Commit();
            }
        }

        public async Task<bool> AcceptTermsAsync(int consolidationGuarantorId, int visitPayUserId)
        {
            ConsolidationGuarantor consolidationGuarantor = await this._consolidationGuarantorService.Value.AcceptTermsManagingAsync(consolidationGuarantorId).ConfigureAwait(true);
            if (consolidationGuarantor == null)
            {
                return false;
            }

            if (consolidationGuarantor.ConsolidationGuarantorStatus == ConsolidationGuarantorStatusEnum.Accepted)
            {
                this.UpdateConsolidationStatusClaim(visitPayUserId, GuarantorConsolidationStatusEnum.Managing);

                this._session.Transaction.RegisterPostCommitSuccessAction((cg) =>
                {
                    this.Bus.Value.PublishMessage(new ConsolidationHouseholdAcceptedToManagedMessage
                    {
                        ManagingVpGuarantorId = cg.ManagingGuarantor.VpGuarantorId,
                        ManagedVpGuarantorId = cg.ManagedGuarantor.VpGuarantorId
                    }).Wait();
                }, consolidationGuarantor);
            }

            return true;
        }

        public async Task<bool> AcceptFinancePlanTermsAsync(int consolidationGuarantorId, int visitPayUserId)
        {
            ConsolidationGuarantor consolidationGuarantor = await this._consolidationGuarantorService.Value.AcceptFinanceFlanTermsManagingAsync(consolidationGuarantorId).ConfigureAwait(true);
            if (consolidationGuarantor == null)
            {
                return false;
            }

            if (consolidationGuarantor.ConsolidationGuarantorStatus == ConsolidationGuarantorStatusEnum.Accepted)
            {
                this.UpdateConsolidationStatusClaim(visitPayUserId, GuarantorConsolidationStatusEnum.Managing);
                this._session.Transaction.RegisterPostCommitSuccessAction((cg) =>
                {
                    this.Bus.Value.PublishMessage(new ConsolidationHouseholdAcceptedToManagedMessage
                    {
                        ManagingVpGuarantorId = cg.ManagingGuarantor.VpGuarantorId,
                        ManagedVpGuarantorId = cg.ManagedGuarantor.VpGuarantorId
                    }).Wait();
                }, consolidationGuarantor);
            }

            return true;
        }

        public async Task<ConsolidationMatchResponseDto> MatchGuarantorAsync(ConsolidationMatchRequestDto consolidationMatchRequestDto, int visitPayUserId)
        {
            ConsolidationMatchRequest consolidationMatchRequest = Mapper.Map<ConsolidationMatchRequest>(consolidationMatchRequestDto);
            ConsolidationMatchResponse consolidationMatchResponse = await this._consolidationGuarantorService.Value.MatchGuarantorAsync(consolidationMatchRequest, visitPayUserId).ConfigureAwait(true);
            if (consolidationMatchResponse.ConsolidationMatchStatus == ConsolidationMatchStatusEnum.Matched)
            {
                Guarantor managingGuarantor = this._guarantorService.Value.GetGuarantorEx(visitPayUserId);
                
                this._session.Transaction.RegisterPostCommitSuccessAction((mg, cmr) =>
                {
                    Client client = this.Client.Value;
                    this.Bus.Value.PublishMessage(new ConsolidationHouseholdRequestToManagedMessage
                    {
                        ManagingVpGuarantorId = mg.VpGuarantorId,
                        ManagedVpGuarantorId = cmr.MatchedGuarantor.VpGuarantorId,
                        ConsolidationRequestExpiryDay = DateTime.Today.AddDays(client.ConsolidationExpirationDays).ToString(Format.DateFormat)
                    }).Wait();
                }, managingGuarantor, consolidationMatchResponse);

                IList<ConsolidationGuarantor> allManagedGuarantors = this._consolidationGuarantorService.Value.GetAllManagedGuarantors(managingGuarantor).ToList();
                if (allManagedGuarantors.Any())
                {
                    if (allManagedGuarantors.Any(x => x.IsActive()))
                    {
                        this.UpdateConsolidationStatusClaim(visitPayUserId, GuarantorConsolidationStatusEnum.Managing);
                    }
                    else if (allManagedGuarantors.Any(x => x.IsPending()))
                    {
                        this.UpdateConsolidationStatusClaim(visitPayUserId, GuarantorConsolidationStatusEnum.PendingManaging);
                    }
                }
            }

            return Mapper.Map<ConsolidationMatchResponseDto>(consolidationMatchResponse);
        }

        public async Task<IList<FinancePlanDto>> GetConsolidationFinancePlansAsync(int consolidationGuarantorId, int visitPayUserId)
        {
            ConsolidationGuarantor consolidationGuarantor = await this._consolidationGuarantorService.Value.GetConsolidationGuarantorAsync(consolidationGuarantorId, visitPayUserId).ConfigureAwait(false);

            return Mapper.Map<IList<FinancePlanDto>>(this._financePlanApplicationService.Value.GetFinancePlans(consolidationGuarantor.ManagedGuarantor.User.VisitPayUserId, new FinancePlanFilterDto
            {
                FinancePlanStatusIds = new List<FinancePlanStatusEnum>
                {
                    FinancePlanStatusEnum.TermsAccepted,
                    FinancePlanStatusEnum.GoodStanding,
                    FinancePlanStatusEnum.PastDue,
                    FinancePlanStatusEnum.UncollectableActive
                }
            }, 1, 100, consolidationGuarantor.ManagedGuarantor.VpGuarantorId).FinancePlans);
        }

        private void UpdateConsolidationStatusClaim(int visitPayUserId, GuarantorConsolidationStatusEnum guarantorConsolidationStatus)
        {
            this._visitPayUserApplicationService.Value.UpdateClaim(ClaimTypeEnum.ConsolidationStatus, guarantorConsolidationStatus.ToString(), visitPayUserId);
        }
    }
}
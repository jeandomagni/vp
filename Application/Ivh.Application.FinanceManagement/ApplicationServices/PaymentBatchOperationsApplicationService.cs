﻿namespace Ivh.Application.FinanceManagement.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.Core.SystemException.Interfaces;
    using Domain.FinanceManagement.Interfaces;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Payment.Interfaces;
    using Domain.Settings.Entities;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Jobs;
    using Ivh.Common.VisitPay.Strings;
    using NHibernate;

    public class PaymentBatchOperationsApplicationService : ApplicationService, IPaymentBatchOperationsApplicationService
    {
        private readonly Lazy<IPaymentBatchOperationsService> _paymentBatchOperationsService;
        private readonly Lazy<Domain.GuestPay.Interfaces.IPaymentService> _paymentService;
        private readonly Lazy<Domain.GuestPay.Interfaces.IPaymentReversalService> _paymentReversalService;
        private readonly Lazy<IPaymentProcessorResponseService> _paymentProcessorResponseService;
        private readonly Lazy<ISystemExceptionService> _systemExceptionService;
        private readonly ISession _session;

        public PaymentBatchOperationsApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IPaymentBatchOperationsService> paymentBatchOperationsService,
            Lazy<IPaymentProcessorResponseService> paymentProcessorResponseService,
            Lazy<ISystemExceptionService> systemExceptionService,
            Lazy<Domain.GuestPay.Interfaces.IPaymentService> paymentService,
            Lazy<Domain.GuestPay.Interfaces.IPaymentReversalService> paymentReversalService,
            ISessionContext<VisitPay> sessionContext) : base(applicationServiceCommonService)
        {
            this._paymentBatchOperationsService = paymentBatchOperationsService;
            this._paymentProcessorResponseService = paymentProcessorResponseService;
            this._systemExceptionService = systemExceptionService;
            this._paymentService = paymentService;
            this._paymentReversalService = paymentReversalService;
            this._session = sessionContext.Session;
        }

        public async Task<int> ReconcilePaymentsAsync()
        {
            Client client = this.Client.Value;
            DateTime endDateTime = DateTime.UtcNow.SetTime(client.PaymentReconciliationCutoffTime);
            DateTime beginDateTime = endDateTime.AddDays(-1);

            int recordsProcessed = await this.ReconcilePaymentsAsync(beginDateTime, endDateTime).ConfigureAwait(false);
            return recordsProcessed;
        }

        private async Task LogUnsettledAchTransactions(DateTime beginDateTime, DateTime endDateTime)
        {
            ICollection<QueryPaymentTransaction> achTransactions = await this._paymentBatchOperationsService.Value.GetUnsettledAchPaymentTransactionsAsync(beginDateTime, endDateTime);
            foreach (QueryPaymentTransaction achTransaction in achTransactions)
            {
                this.LoggingService.Value.Info(() => $"{nameof(PaymentBatchOperationsApplicationService)}::{nameof(this.LogUnsettledAchTransactions)} - Start: {beginDateTime.ToString(Format.DateTimeFormat_MMddyyyyHHmmss)}, End: {endDateTime.ToString(Format.DateTimeFormat_MMddyyyyHHmmss)}, Transaction: {achTransaction.ToJSON(false, true, true)} ");
            }
        }

        public async Task<int> ReconcilePaymentsAsync(DateTime beginDateTime, DateTime endDateTime)
        {

            if (this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureOptimisticAchSettlementIsEnabled))
            {
                await this.LogUnsettledAchTransactions(beginDateTime, endDateTime).ConfigureAwait(false);
            }

            int recordsProcessed = 0;

            ICollection<QueryPaymentTransaction> queryPaymentTransactions = await this._paymentBatchOperationsService.Value.GetPaymentTransactionsAsync(beginDateTime, endDateTime, null);

            if (queryPaymentTransactions.Count == 0)
            {
                this.LoggingService.Value.Warn(() => "TrustCommerce QueryPaymentTransaction collection empty.");
            }

            foreach (QueryPaymentTransaction queryPaymentTransaction in queryPaymentTransactions)
            {
                using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
                {
                    PaymentProcessorResponse paymentProcessorResponse = this._paymentProcessorResponseService.Value.GetPaymentProcessorResponse(queryPaymentTransaction.TransactionId);

                    if (paymentProcessorResponse == null)
                    {
                        if (queryPaymentTransaction.PaymentType.IsInCategory(PaymentTypeEnumCategory.Reversal))
                        {
                            paymentProcessorResponse = this._paymentProcessorResponseService.Value.GetPaymentProcessorResponse(queryPaymentTransaction.RefTransid);

                            if (paymentProcessorResponse != null)
                            {
                                SystemExceptionTypeEnum systemExceptionType = SystemExceptionTypeEnum.Unknown;
                                switch (queryPaymentTransaction.PaymentType)
                                {
                                    case PaymentTypeEnum.Refund:
                                    case PaymentTypeEnum.PartialRefund:
                                        systemExceptionType = SystemExceptionTypeEnum.RefundGeneratedInTcVault;
                                        break;
                                    case PaymentTypeEnum.Void:
                                        systemExceptionType = SystemExceptionTypeEnum.VoidGeneratedInTcVault;
                                        break;
                                    default:
                                        break;
                                }

                                switch (paymentProcessorResponse.PaymentSystemType)
                                {
                                    case PaymentSystemTypeEnum.VisitPay:
                                        {
                                            foreach (Payment payment in paymentProcessorResponse.Payments)
                                            {
                                                if (systemExceptionType != SystemExceptionTypeEnum.Unknown)
                                                {
                                                    Ivh.Domain.Core.SystemException.Entities.SystemException systemException = new Ivh.Domain.Core.SystemException.Entities.SystemException
                                                    {
                                                        SystemExceptionType = systemExceptionType,
                                                        SystemExceptionDescription = $"Query Payment TransactionId: {queryPaymentTransaction.TransactionId} RefTransId: {queryPaymentTransaction.RefTransid ?? string.Empty} CustomerId: {queryPaymentTransaction.CustId}",
                                                        TransactionId = queryPaymentTransaction.RefTransid,
                                                        PaymentId = payment.PaymentId
                                                    };
                                                    this._systemExceptionService.Value.LogSystemException(systemException);
                                                }
                                            }
                                        }
                                        break;
                                    case PaymentSystemTypeEnum.GuestPay:
                                    case PaymentSystemTypeEnum.PaymentApi:
                                        {
                                            if (systemExceptionType != SystemExceptionTypeEnum.Unknown)
                                            {
                                                Domain.GuestPay.Entities.Payment payment = this._paymentService.Value.GetPayment(queryPaymentTransaction.RefTransid);
                                                if (payment != null)
                                                {
                                                    bool logException = false;
                                                    switch (queryPaymentTransaction.PaymentType)
                                                    {
                                                        case PaymentTypeEnum.Refund:
                                                        case PaymentTypeEnum.PartialRefund:
                                                            this._paymentReversalService.Value.RefundPayment(payment, decimal.Negate(Math.Abs(queryPaymentTransaction.Amount)));
                                                            logException = true;
                                                            break;
                                                        case PaymentTypeEnum.Void:
                                                            this._paymentReversalService.Value.VoidPayment(payment);
                                                            logException = true;

                                                            break;
                                                        default:
                                                            break;
                                                    }

                                                    if (logException)
                                                    {
                                                        this._systemExceptionService.Value.LogSystemException(new Ivh.Domain.Core.SystemException.Entities.SystemException
                                                        {
                                                            SystemExceptionType = systemExceptionType,
                                                            SystemExceptionDescription = $"Query Payment (GuestPay) TransactionId: {queryPaymentTransaction.TransactionId} RefTransId: {queryPaymentTransaction.RefTransid ?? string.Empty} CustomerId: {queryPaymentTransaction.CustId}",
                                                            TransactionId = queryPaymentTransaction.RefTransid,
                                                            GuestPayPaymentId = payment.PaymentId
                                                        });
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                    default:
                                        throw new ArgumentOutOfRangeException();
                                }
                            }
                        }
                    }
                    else if (paymentProcessorResponse.PaymentProcessorResponseStatus != queryPaymentTransaction.PaymentProcessorResponseStatus
                        && queryPaymentTransaction.PaymentProcessorResponseStatus == PaymentProcessorResponseStatusEnum.Decline)
                    {
                        {
                            if (paymentProcessorResponse.PaymentSystemType == PaymentSystemTypeEnum.VisitPay
                                && paymentProcessorResponse.Payments.IsNotNullOrEmpty())
                            {
                                foreach (Payment payment in paymentProcessorResponse.Payments)
                                {
                                    switch (payment.PaymentType)
                                    {
                                        case PaymentTypeEnum.Refund:
                                        case PaymentTypeEnum.PartialRefund:
                                            this._systemExceptionService.Value.LogSystemException(new Ivh.Domain.Core.SystemException.Entities.SystemException
                                            {
                                                SystemExceptionType = SystemExceptionTypeEnum.RefundRejectedByProcessor,
                                                SystemExceptionDescription = $"Query Payment TransactionId: {queryPaymentTransaction.TransactionId} CustomerId: {queryPaymentTransaction.CustId}",
                                                TransactionId = payment.TransactionId,
                                                PaymentId = payment.PaymentId,
                                                VpGuarantorId = payment.Guarantor.VpGuarantorId
                                            });
                                            break;
                                        case PaymentTypeEnum.Void:
                                            this._systemExceptionService.Value.LogSystemException(new Ivh.Domain.Core.SystemException.Entities.SystemException
                                            {
                                                SystemExceptionType = SystemExceptionTypeEnum.VoidRejectedByProcessor,
                                                SystemExceptionDescription = $"Query Payment TransactionId: {queryPaymentTransaction.TransactionId} CustomerId: {queryPaymentTransaction.CustId}",
                                                TransactionId = payment.TransactionId,
                                                PaymentId = payment.PaymentId,
                                                VpGuarantorId = payment.Guarantor.VpGuarantorId
                                            });
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                            else if (paymentProcessorResponse.PaymentSystemType == PaymentSystemTypeEnum.GuestPay
                                     || paymentProcessorResponse.PaymentSystemType == PaymentSystemTypeEnum.PaymentApi)
                            {
                                Domain.GuestPay.Entities.Payment payment = this._paymentService.Value.GetPayment(queryPaymentTransaction.TransactionId);
                                if (payment != null)
                                {
                                    switch (queryPaymentTransaction.PaymentType)
                                    {
                                        case PaymentTypeEnum.Refund:
                                        case PaymentTypeEnum.PartialRefund:
                                            this._paymentReversalService.Value.RefundPayment(payment, decimal.Negate(Math.Abs(queryPaymentTransaction.Amount)));
                                            this._systemExceptionService.Value.LogSystemException(new Ivh.Domain.Core.SystemException.Entities.SystemException
                                            {
                                                SystemExceptionType = SystemExceptionTypeEnum.RefundRejectedByProcessorGuestPay,
                                                SystemExceptionDescription = $"Query Payment (GuestPay) TransactionId: {queryPaymentTransaction.TransactionId} CustomerId: {queryPaymentTransaction.CustId}",
                                                TransactionId = payment.TransactionId,
                                                GuestPayPaymentId = payment.PaymentId
                                            });
                                            break;
                                        case PaymentTypeEnum.Void:
                                            this._paymentReversalService.Value.VoidPayment(payment);
                                            this._systemExceptionService.Value.LogSystemException(new Ivh.Domain.Core.SystemException.Entities.SystemException
                                            {
                                                SystemExceptionType = SystemExceptionTypeEnum.VoidRejectedByProcessorGuestPay,
                                                SystemExceptionDescription = $"Query Payment (GuestPay) TransactionId: {queryPaymentTransaction.TransactionId} CustomerId: {queryPaymentTransaction.CustId}",
                                                TransactionId = payment.TransactionId,
                                                GuestPayPaymentId = payment.PaymentId
                                            });
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                    }
                    recordsProcessed++;
                    unitOfWork.Commit();
                }
            }
            return recordsProcessed;
        }

        #region Jobs
        void IJobRunnerService<QueuePaymentReconciliationJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.ReconcilePaymentsAsync().Wait();
        }
        #endregion
    }
}

﻿namespace Ivh.Application.FinanceManagement.ApplicationServices
{
    using System;
    using System.Threading.Tasks;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.FinanceManagement.Consolidation.Entities;
    using Domain.FinanceManagement.Consolidation.Interfaces;
    using Domain.Settings.Entities;
    using Domain.Settings.Interfaces;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.ServiceBus;
    using Ivh.Common.ServiceBus.Common;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Consolidation;
    using Ivh.Common.VisitPay.Strings;
    using MassTransit;
    using NHibernate;

    public class ManagedUserApplicationService : ApplicationService, IManagedUserApplicationService
    {
        private readonly Lazy<IConsolidationGuarantorService> _consolidationGuarantorService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly ISession _session;

        public ManagedUserApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IConsolidationGuarantorService> consolidationGuarantorService,
            Lazy<IGuarantorService> guarantorService,
            ISessionContext<VisitPay> sessionContext) : base(applicationServiceCommonService)
        {
            this._consolidationGuarantorService = consolidationGuarantorService;
            this._guarantorService = guarantorService;
            this._session = sessionContext.Session;
        }

        public async Task RejectConsolidationAsync(int consolidationGuarantorId, int visitPayUserId)
        {
            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                ConsolidationGuarantor consolidationGuarantor = await this._consolidationGuarantorService.Value.GetConsolidationGuarantorAsync(consolidationGuarantorId, visitPayUserId).ConfigureAwait(false);
                bool isFinancePlan = consolidationGuarantor.ConsolidationGuarantorStatus == ConsolidationGuarantorStatusEnum.PendingFinancePlan;
                consolidationGuarantor = await this._consolidationGuarantorService.Value.RejectConsolidationManagedAsync(consolidationGuarantorId, visitPayUserId).ConfigureAwait(false);

                this._session.Transaction.RegisterPostCommitSuccessAction((cg, ifp) =>
                {
                    if (ifp)
                    {
                        this.Bus.Value.PublishMessage(new ConsolidationHouseholdRequestCancelledByManagedToManagingWithPendingFpTermsMessage()
                        {
                            ManagingVpGuarantorId = consolidationGuarantor.ManagingGuarantor.VpGuarantorId,
                            ManagedVpGuarantorId = consolidationGuarantor.ManagedGuarantor.VpGuarantorId,
                        }).Wait();
                    }
                    else
                    {
                        this.Bus.Value.PublishMessage(new ConsolidationHouseholdRequestDeclinedToManagingMessage()
                        {
                            ManagingVpGuarantorId = consolidationGuarantor.ManagingGuarantor.VpGuarantorId,
                            ManagedVpGuarantorId = consolidationGuarantor.ManagedGuarantor.VpGuarantorId,
                        }).Wait();
                    }
                }, consolidationGuarantor, isFinancePlan);

                uow.Commit();
            }
        }

        public async Task CancelConsolidationAsync(int consolidationGuarantorId, int visitPayUserId)
        {
            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                ConsolidationGuarantor consolidationGuarantor = await this._consolidationGuarantorService.Value.GetConsolidationGuarantorAsync(consolidationGuarantorId, visitPayUserId).ConfigureAwait(false);
                bool isActive = consolidationGuarantor.IsPendingOrActive();
                bool isPending = consolidationGuarantor.IsPending();
                bool isFinancePlan = consolidationGuarantor.DateAcceptedFinancePlanTermsByManagingGuarantor.HasValue;

                consolidationGuarantor = await this._consolidationGuarantorService.Value.CancelConsolidationAsync(consolidationGuarantorId, visitPayUserId, ConsolidationGuarantorCancellationReasonEnum.CancelledByManaged).ConfigureAwait(false);
                this._session.Transaction.RegisterPostCommitSuccessAction((cg, ip, ia, ifp) =>
                {
                    if (ip)
                    {
                        this.Bus.Value.PublishMessage(new ConsolidationHouseholdRequestCancelledByManagedToManagingMessage()
                        {
                            ManagingVpGuarantorId = cg.ManagingGuarantor.VpGuarantorId,
                            ManagedVpGuarantorId = cg.ManagedGuarantor.VpGuarantorId,
                        }).Wait();
                    }
                    else if (ia)
                    {
                        if (ifp)
                        {
                            this.Bus.Value.PublishMessage(new ConsolidationLinkageCancelledByManagedToManagedWithFpMessage
                            {
                                ManagingVpGuarantorId = cg.ManagingGuarantor.VpGuarantorId,
                                ManagedVpGuarantorId = cg.ManagedGuarantor.VpGuarantorId,
                            }).Wait();
                        }
                        else
                        {
                            this.Bus.Value.PublishMessage(new ConsolidationLinkageCancelledByManagedToManagedMessage
                            {
                                ManagingVpGuarantorId = cg.ManagingGuarantor.VpGuarantorId,
                                ManagedVpGuarantorId = cg.ManagedGuarantor.VpGuarantorId,
                            }).Wait();
                        }

                        this.Bus.Value.PublishMessage(new ConsolidationLinkageCancelledByManagedToManagingMessage
                        {
                            ManagingVpGuarantorId = cg.ManagingGuarantor.VpGuarantorId,
                            ManagedVpGuarantorId = cg.ManagedGuarantor.VpGuarantorId,
                        }).Wait();
                    }
                }, consolidationGuarantor, isPending, isActive, isFinancePlan);

                uow.Commit();
            }
        }

        public async Task<bool> AcceptTermsAsync(int consolidationGuarantorId, int visitPayUserId)
        {
            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                ConsolidationGuarantor consolidationGuarantor = await this._consolidationGuarantorService.Value.AcceptTermsManagedAsync(consolidationGuarantorId).ConfigureAwait(false);
                if (consolidationGuarantor == null)
                    return false;

                switch (consolidationGuarantor.ConsolidationGuarantorStatus)
                {
                    case ConsolidationGuarantorStatusEnum.Accepted:
                        this._session.Transaction.RegisterPostCommitSuccessAction((cg) =>
                        {
                            this.Bus.Value.PublishMessage(new ConsolidationHouseholdRequestAcceptedByManagedToManagingMessage()
                            {
                                ManagingVpGuarantorId = cg.ManagingGuarantor.VpGuarantorId,
                                ManagedVpGuarantorId = cg.ManagedGuarantor.VpGuarantorId,
                            }).Wait();
                        }, consolidationGuarantor);
                        break;
                    case ConsolidationGuarantorStatusEnum.PendingFinancePlan:
                        this._session.Transaction.RegisterPostCommitSuccessAction((cg,c) =>
                        {
                            this.Bus.Value.PublishMessage(new ConsolidationHouseholdRequestAcceptedByManagedWithFpToManagingMessage()
                            {
                                ManagingVpGuarantorId = cg.ManagingGuarantor.VpGuarantorId,
                                ManagedVpGuarantorId = cg.ManagedGuarantor.VpGuarantorId,
                                ConsolidationRequestExpiryDay = cg.MostRecentActionDate.Date.AddDays(c.ConsolidationExpirationDays).ToString(Format.DateFormat)
                            }).Wait();
                        }, consolidationGuarantor, this.Client.Value);
                        break;
                }

                uow.Commit();

                return true;
            }
        }

        public async Task<ConsolidationMatchResponseDto> MatchGuarantorAsync(ConsolidationMatchRequestDto consolidationMatchRequestDto, int visitPayUserId)
        {
            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                ConsolidationMatchRequest consolidationMatchRequest = Mapper.Map<ConsolidationMatchRequest>(consolidationMatchRequestDto);
                ConsolidationMatchResponse consolidationMatchResponse = await this._consolidationGuarantorService.Value.MatchGuarantorAsync(consolidationMatchRequest, visitPayUserId).ConfigureAwait(false);

                if (consolidationMatchResponse.ConsolidationMatchStatus == ConsolidationMatchStatusEnum.Matched)
                {
                    this._session.Transaction.RegisterPostCommitSuccessAction((cmr) =>
                    {
                        this.Bus.Value.PublishMessage(new ConsolidationHouseholdRequestToManagingMessage()
                        {
                            ManagingVpGuarantorId = cmr.MatchedGuarantor.VpGuarantorId,
                            ManagedVpGuarantorId = this._guarantorService.Value.GetGuarantorId(visitPayUserId),
                            ConsolidationRequestExpiryDay = DateTime.Today.AddDays(this.Client.Value.ConsolidationExpirationDays).ToString(Format.DateFormat)

                        }).Wait();

                    }, consolidationMatchResponse);
                }

                uow.Commit();

                return Mapper.Map<ConsolidationMatchResponseDto>(consolidationMatchResponse);
            }
        }
    }
}
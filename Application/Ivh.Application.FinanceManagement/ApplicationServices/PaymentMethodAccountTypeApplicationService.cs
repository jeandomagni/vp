﻿
namespace Ivh.Application.FinanceManagement.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Domain.FinanceManagement.Interfaces;
    using Common.Dtos;
    using Common.Interfaces;

    public class PaymentMethodAccountTypeApplicationService : ApplicationService, IPaymentMethodAccountTypeApplicationService
    {

        private readonly Lazy<IPaymentMethodAccountTypeService> _pyamentMethodAccountTypeService;

        public PaymentMethodAccountTypeApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IPaymentMethodAccountTypeService> paymentMethodAccountTypeService) : base(applicationServiceCommonService)
        {
            this._pyamentMethodAccountTypeService = paymentMethodAccountTypeService;
        }

        public IList<PaymentMethodAccountTypeDto> GetPaymentMethodAccountTypes()
        {
            return Mapper.Map<List<PaymentMethodAccountTypeDto>>(this._pyamentMethodAccountTypeService.Value.GetPaymentMethodAccountTypes());
        }

    }
}

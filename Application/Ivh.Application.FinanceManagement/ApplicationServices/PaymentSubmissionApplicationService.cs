namespace Ivh.Application.FinanceManagement.ApplicationServices
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Base.Common.Dtos;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Content.Common.Interfaces;
    using Domain.Base.Extensions;
    using Domain.FinanceManagement.Discount.Entities;
    using Domain.Visit.Interfaces;
    using Domain.FinanceManagement.Discount.Interfaces;
    using Domain.FinanceManagement.Entities;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Interfaces;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Payment.Utilities;
    using Domain.FinanceManagement.Statement.Entities;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.Logging.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Jobs;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using Newtonsoft.Json;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;
    using MassTransit;
    using NHibernate;

    public class PaymentSubmissionApplicationService : ApplicationService, IPaymentSubmissionApplicationService
    {
        private readonly Lazy<IConsolidationGuarantorApplicationService> _consolidationGuarantorApplicationService;
        private readonly Lazy<IVisitBalanceBaseApplicationService> _visitBalanceBaseApplicationService;
        private readonly Lazy<IFinancePlanService> _financePlanService;
        private readonly Lazy<IVisitService> _visitService;
        private readonly Lazy<IDiscountService> _discountService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IPaymentMethodsService> _paymentMethodsService;
        private readonly Lazy<IPaymentMethodsApplicationService> _paymentMethodsApplicationService;
        private readonly Lazy<IStatementApplicationService> _statementApplicationService;
        private readonly Lazy<IPaymentService> _paymentService;
        private readonly Lazy<IVpStatementService> _vpStatementService;
        private readonly Lazy<IContentApplicationService> _contentApplicationService;
        private readonly Lazy<IPaymentAllocationApplicationService> _paymentAllocationApplicationService;
        private readonly ISession _session;
        private readonly Lazy<IMetricsProvider> _metricsProvider;

        public PaymentSubmissionApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IPaymentService> paymentService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IPaymentMethodsService> paymentMethodsService,
            Lazy<IPaymentMethodsApplicationService> paymentMethodsApplicationService,
            Lazy<IPaymentAllocationApplicationService> paymentAllocationApplicationService,
            Lazy<IVpStatementService> statementService,
            Lazy<IFinancePlanService> financePlanService,
            Lazy<IStatementApplicationService> statementApplicationService,
            Lazy<IVisitService> visitService,
            Lazy<IDiscountService> discountService,
            Lazy<IConsolidationGuarantorApplicationService> consolidationGuarantorApplicationService,
            Lazy<IVisitBalanceBaseApplicationService> visitBalanceBaseApplicationService,
            Lazy<IContentApplicationService> contentApplicationService,
            ISessionContext<VisitPay> sessionContext,
            Lazy<IMetricsProvider> metricsProvider) : base(applicationServiceCommonService)
        {
            this._paymentService = paymentService;
            this._guarantorService = guarantorService;
            this._vpStatementService = statementService;
            this._paymentMethodsService = paymentMethodsService;
            this._paymentMethodsApplicationService = paymentMethodsApplicationService;
            this._paymentAllocationApplicationService = paymentAllocationApplicationService;
            this._financePlanService = financePlanService;
            this._statementApplicationService = statementApplicationService;
            this._visitService = visitService;
            this._discountService = discountService;
            this._consolidationGuarantorApplicationService = consolidationGuarantorApplicationService;
            this._visitBalanceBaseApplicationService = visitBalanceBaseApplicationService;
            this._contentApplicationService = contentApplicationService;
            this._session = sessionContext.Session;
            this._metricsProvider = metricsProvider;
        }

        #region Discounts

        public bool IsDiscountEligible(int vpGuarantorId)
        {
            DiscountOfferDto discountOfferDto = this.GetDiscountPerVisitForStatementedBalanceInFull(vpGuarantorId);
            if (discountOfferDto == null || discountOfferDto.DiscountTotalPercent <= 0)
            {
                return false;
            }

            return true;
        }

        public DiscountOfferDto GetDiscountPerVisitForStatementedBalanceInFull(int vpGuarantorId)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            decimal totalBalance = this._visitBalanceBaseApplicationService.Value.GetTotalBalance(vpGuarantorId);

            PaymentSubmissionDto fakeDto = new PaymentSubmissionDto
            {
                PaymentType = PaymentTypeEnum.ManualPromptCurrentNonFinancedBalanceInFull,
                PaymentDate = DateTime.UtcNow.ToClientDateTime(),
                TotalPaymentAmount = totalBalance
            };

            Payment payment = this.CreatePaymentFromPaymentDto(fakeDto, guarantor, new PaymentMethod(), -1).First();

            return Mapper.Map<DiscountOfferDto>(payment.DiscountOffer);
        }

        #endregion    

        #region payment validation

        public ValidatePaymentResponse ValidatePayment(PaymentSubmissionDto paymentSubmissionDto, int vpGuarantorId)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            if (guarantor == null)
            {
                //this.LoggingService.Value.Fatal();
                this.ValidatePaymentStringMessage(PaymentValidationMessageEnum.GeneralError);
            }

            if (paymentSubmissionDto.PaymentType == PaymentTypeEnum.HouseholdBalanceCurrentNonFinancedBalance)
            {
                paymentSubmissionDto.PaymentType = PaymentTypeEnum.ManualScheduledCurrentNonFinancedBalanceInFull;
                paymentSubmissionDto.IsHouseholdBalance = true;
            }

            IList<Payment> payments = this.CreatePaymentFromPaymentDto(paymentSubmissionDto, guarantor, null, -1);
            foreach (Payment payment in payments)
            {
                PaymentValidationMessageEnum validationMessage = this.ValidatePayment(payment, paymentSubmissionDto.PaymentDate, paymentSubmissionDto.PaymentMethodId);
                ValidatePaymentResponse validationResponse = this.ValidatePaymentStringMessage(validationMessage);

                if (validationResponse != null)
                {
                    return validationResponse;
                }
            }

            return null;
        }

        public ValidatePaymentResponse ValidateRescheduledPayment(int paymentId, int paymentMethodId, DateTime scheduledPaymentDate, int vpGuarantorId)
        {
            Payment payment = this._paymentService.Value.GetPayment(vpGuarantorId, paymentId);

            PaymentValidationMessageEnum validationMessage = this.ValidatePayment(payment, scheduledPaymentDate, paymentMethodId);

            return this.ValidatePaymentStringMessage(validationMessage);
        }

        private PaymentValidationMessageEnum ValidatePayment(Payment payment, DateTime scheduledPaymentDate, int paymentMethodId)
        {
            try
            {
                PaymentMethod paymentMethod = this._paymentMethodsService.Value.GetById(paymentMethodId);
                if (paymentMethod == null)
                {
                    //this.LoggingService.Value.Fatal();
                    return PaymentValidationMessageEnum.GeneralError;
                }

                if (paymentMethod.IsNearExpiry(scheduledPaymentDate.Date))
                {
                    return paymentMethod.IsExpired ? PaymentValidationMessageEnum.PaymentMethodExpired : PaymentValidationMessageEnum.PaymentMethodNearExpiry;
                }

                if (payment.IsRecurringPayment())
                {
                    return PaymentValidationMessageEnum.None;
                }

                if (DateTime.Compare(scheduledPaymentDate.Date, DateTime.UtcNow.AddDays(this.Client.Value.MaxFuturePaymentIntervalInDays).Date) > 0)
                {
                    return PaymentValidationMessageEnum.InvalidDate;
                }

                if (payment.PaymentType.IsInCategory(PaymentTypeEnumCategory.SpecificFinancePlan))
                {
                    return this.ValidateSpecificFinancePlansPayment(payment);
                }

                if (payment.PaymentType.IsInCategory(PaymentTypeEnumCategory.SpecificVisits))
                {
                    return this.ValidateSpecificVisitsPayment(payment);
                }

                return PaymentValidationMessageEnum.None;
            }
            catch (Exception)
            {
                //this.LoggingService.Value.Fatal();
                return PaymentValidationMessageEnum.GeneralError;
            }
        }

        private PaymentValidationMessageEnum ValidateSpecificFinancePlansPayment(Payment payment)
        {
            List<PaymentScheduledAmount> scheduledAmountsWithFinancePlans = payment.PaymentScheduledAmounts.Where(x => x.PaymentFinancePlan != null).ToList();

            // paying full balances
            if (scheduledAmountsWithFinancePlans.All(x => x.ScheduledAmount == x.PaymentFinancePlan.CurrentFinancePlanBalance))
            {
                return PaymentValidationMessageEnum.None;
            }

            // payment for more than a finance plan balance
            if (scheduledAmountsWithFinancePlans.Any(x => x.ScheduledAmount > x.PaymentFinancePlan.CurrentFinancePlanBalance))
            {
                return PaymentValidationMessageEnum.FinancePlanBalanceHasChanged;
            }

            // past due finance plans
            List<PaymentScheduledAmount> scheduledAmountsWithPastDueFinancePlans = scheduledAmountsWithFinancePlans.Where(x => x.PaymentFinancePlan.FinancePlanStatus.IsInCategory(FinancePlanStatusEnumCategory.PastDue)).ToList();
            foreach (PaymentScheduledAmount paymentScheduledAmount in scheduledAmountsWithPastDueFinancePlans)
            {
                FinancePlan financePlan = this._financePlanService.Value.GetFinancePlanById(paymentScheduledAmount.PaymentFinancePlan.FinancePlanId);
                if (paymentScheduledAmount.ScheduledAmount < financePlan.PastDueAmount)
                {
                    return PaymentValidationMessageEnum.PastDueFinancePlans;
                }
            }

            return PaymentValidationMessageEnum.None;
        }

        private PaymentValidationMessageEnum ValidateSpecificVisitsPayment(Payment payment)
        {
            IList<VisitBalanceBaseDto> visitBalances = this._visitBalanceBaseApplicationService.Value.GetTotalBalances(payment.Guarantor.VpGuarantorId);
            List<PaymentScheduledAmount> scheduledAmountsWithVisits = payment.PaymentScheduledAmounts.Where(x => x.PaymentVisit != null).ToList();

            // paying total balance - no validation required
            if (scheduledAmountsWithVisits.All(x => x.ScheduledAmount == (visitBalances.FirstOrDefault(b => b.VisitId == x.PaymentVisit.VisitId)?.TotalBalance ?? 0m)))
            {
                return PaymentValidationMessageEnum.None;
            }

            // payment for more than a visit balance
            if (scheduledAmountsWithVisits.Any(x => x.ScheduledAmount > (visitBalances.FirstOrDefault(b => b.VisitId == x.PaymentVisit.VisitId)?.TotalBalance ?? 0m)))
            {
                return PaymentValidationMessageEnum.VisitBalanceHasChanged;
            }

            // past due nonfinanced visits
            IList<int> financedVisitIds = this._financePlanService.Value.GetVisitIdsOnActiveFinancePlans(payment.Guarantor.VpGuarantorId) ?? new List<int>();
            List<PaymentScheduledAmount> scheduledAmountsWithPastDueVisits = scheduledAmountsWithVisits.Where(x => !financedVisitIds.Contains(x.VisitId ?? default(int)) && this._visitService.Value.IsVisitPastDue(x.PaymentVisit)).ToList();
            foreach (PaymentScheduledAmount paymentScheduledAmount in scheduledAmountsWithPastDueVisits)
            {
                decimal totalBalance = visitBalances.FirstOrDefault(x => x.VisitId == paymentScheduledAmount.VisitId)?.TotalBalance ?? 0m;
                if (paymentScheduledAmount.ScheduledAmount < totalBalance)
                {
                    return PaymentValidationMessageEnum.PastDueVisits;
                }
            }

            return PaymentValidationMessageEnum.None;
        }

        private ValidatePaymentResponse ValidatePaymentStringMessage(PaymentValidationMessageEnum paymentValidationMessageEnum)
        {
            if (paymentValidationMessageEnum == PaymentValidationMessageEnum.None)
            {
                return null;
            }

            bool promptUser = paymentValidationMessageEnum.IsInCategory(PaymentValidationMessageEnumCategory.PromptUser);
            string message = this.GetValidatePaymentMessage(paymentValidationMessageEnum);

            return new ValidatePaymentResponse(promptUser, message);
        }

        private string GetValidatePaymentMessage(PaymentValidationMessageEnum paymentValidationMessageEnum)
        {
            string message = null;
            switch (paymentValidationMessageEnum)
            {
                case PaymentValidationMessageEnum.GeneralError:
                    message = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.PaymentValidationGeneralError);
                    break;

                case PaymentValidationMessageEnum.FinancePlanBalanceHasChanged:
                    message = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.PaymentValidationFinancePlanBalanceHasChanged);
                    break;

                case PaymentValidationMessageEnum.VisitBalanceHasChanged:
                    message = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.PaymentValidationVisitBalanceHasChanged);
                    break;

                case PaymentValidationMessageEnum.PaymentMethodNearExpiry:
                    message = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.PaymentValidationPaymentMethodNearExpiry);
                    break;

                case PaymentValidationMessageEnum.PaymentMethodExpired:
                    message = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.PaymentValidationPaymentMethodExpired);
                    break;

                case PaymentValidationMessageEnum.PastDueVisits:
                    message = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.PaymentValidationPastDueVisits);
                    break;

                case PaymentValidationMessageEnum.PastDueFinancePlans:
                    message = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.PaymentValidationPastDueFinancePlans);
                    break;

                case PaymentValidationMessageEnum.InvalidDate:
                    message = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.PaymentValidationInvalidDate);
                    break;

                default:
                    message = null;
                    break;
            }

            return message;
        }

        private ProcessPaymentResponse ValidatePaymentMethod(PaymentMethod paymentMethod, Guarantor guarantor)
        {
            if (guarantor == null)
            {
                string message = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.PaymentValidationInvalidGuarantor);
                return new ProcessPaymentResponse(true, message);
            }

            if (paymentMethod == null)
            {
                string message = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.PaymentValidationInvalidPaymentMethod);
                return new ProcessPaymentResponse(true, message);
            }

            if (guarantor.ConsolidationStatus == GuarantorConsolidationStatusEnum.NotConsolidated)
            {
                if (paymentMethod.VpGuarantorId != guarantor.VpGuarantorId)
                {
                    string message = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.PaymentValidationInvalidPaymentMethod);
                    return new ProcessPaymentResponse(true, message);
                }
            }

            return new ProcessPaymentResponse(false, null);
        }

        #endregion

        #region Payment Scheduling

        public void SetupPaymentScheduledAmounts(PaymentSubmissionDto paymentSubmissionDto, Payment newPayment)
        {
            switch (newPayment.PaymentType)
            {
                case PaymentTypeEnum.ManualPromptSpecificAmount:
                case PaymentTypeEnum.ManualScheduledSpecificAmount:
                    {
                        newPayment.AddPaymentScheduledAmount(new PaymentScheduledAmount { ScheduledAmount = paymentSubmissionDto.TotalPaymentAmount });
                        break;
                    }

                case PaymentTypeEnum.RecurringPaymentFinancePlan:
                case PaymentTypeEnum.ManualPromptSpecificFinancePlans:
                case PaymentTypeEnum.ManualScheduledSpecificFinancePlans:
                case PaymentTypeEnum.ManualPromptSpecificFinancePlansBucketZero:
                case PaymentTypeEnum.LockBoxManualPromptSpecificFinancePlansBucketZero:
                    {
                        IList<int> financePlanIds = this._financePlanService.Value.GetAllActiveFinancePlanIds(newPayment.Guarantor.VpGuarantorId);
                        List<FinancePlan> financePlans = this._financePlanService.Value.GetFinancePlansById(financePlanIds).ToList();
                        foreach (FinancePlanPaymentDto planDto in paymentSubmissionDto.FinancePlanPayments)
                        {
                            if (planDto.FinancePlanId == null)
                            {
                                continue;
                            }

                            FinancePlan financePlan = financePlans.FirstOrDefault(x => x.FinancePlanId == planDto.FinancePlanId);
                            if (financePlan == null)
                            {
                                continue;
                            }

                            PaymentFinancePlan paymentFinancePlan = new PaymentFinancePlan
                            {
                                FinancePlanId = financePlan.FinancePlanId,
                                CurrentBucket = financePlan.CurrentBucket,
                                FinancePlanStatus = financePlan.FinancePlanStatus.FinancePlanStatusEnum,
                                CurrentFinancePlanBalance = financePlan.CurrentFinancePlanBalance,
                                OriginationDate = financePlan.OriginationDate
                            };

                            newPayment.AddPaymentScheduledAmount(new PaymentScheduledAmount
                            {
                                PaymentFinancePlan = paymentFinancePlan,
                                ScheduledAmount = planDto.PaymentAmount,
                                PaymentVisit = null
                            });
                        }

                        break;
                    }

                case PaymentTypeEnum.ManualPromptCurrentNonFinancedBalanceInFull:
                case PaymentTypeEnum.ManualScheduledCurrentNonFinancedBalanceInFull:
                    {
                        IList<VisitBalanceBaseDto> visitBalances = this._visitBalanceBaseApplicationService.Value.GetTotalBalances(newPayment.Guarantor.VpGuarantorId);
                        IList<PaymentVisit> paymentVisits = this._paymentService.Value.GetPaymentVisitsByVisitIds(visitBalances.Select(x => x.VisitId).ToList());

                        foreach (PaymentVisit paymentVisit in paymentVisits)
                        {
                            VisitBalanceBaseDto visitBalance = visitBalances.FirstOrDefault(x => x.VisitId == paymentVisit.VisitId);
                            decimal paymentAmount = visitBalance?.TotalBalance ?? 0m;

                            newPayment.AddPaymentScheduledAmount(new PaymentScheduledAmount
                            {
                                PaymentVisit = paymentVisit,
                                ScheduledAmount = paymentAmount
                            });
                        }

                        // assign discount
                        this._discountService.Value.AssignDiscountsToPayment(newPayment, newPayment.PaymentScheduledAmounts.Select(x => new DiscountVisitPayment(x.PaymentVisit, x.ScheduledAmount)).ToList());

                        break;
                    }

                case PaymentTypeEnum.ManualPromptSpecificVisits:
                case PaymentTypeEnum.ManualScheduledSpecificVisits:
                    {
                        IList<int> visitIds = paymentSubmissionDto.VisitPayments.Select(x => x.VisitId).ToList();
                        IList<PaymentVisit> paymentVisits = this._paymentService.Value.GetPaymentVisitsByVisitIds(visitIds);
                        foreach (PaymentVisit paymentVisit in paymentVisits)
                        {
                            VisitPaymentDto visitPaymentDto = paymentSubmissionDto.VisitPayments.FirstOrDefault(x => x.VisitId == paymentVisit.VisitId);
                            decimal paymentAmount = visitPaymentDto?.PaymentAmount ?? 0m;
                            newPayment.AddPaymentScheduledAmount(new PaymentScheduledAmount
                            {
                                PaymentFinancePlan = null,
                                PaymentVisit = paymentVisit,
                                ScheduledAmount = paymentAmount
                            });
                        }

                        break;
                    }

                case PaymentTypeEnum.ManualPromptCurrentBalanceInFull:
                case PaymentTypeEnum.ManualScheduledCurrentBalanceInFull:
                case PaymentTypeEnum.ResubmitPayments:
                    break;

                default:
                    throw new Exception("Cannot setup PaymentScheduleAmounts without a type.");
            }
        }

        private ProcessPaymentResponse SchedulePayment(Payment paymentEntity, int? visitPayUserId = null)
        {
            ProcessPaymentResponse processPaymentResponse = this.SchedulePayment((p, v) =>
                    this._paymentService.Value.SchedulePayment(paymentEntity, visitPayUserId)
                , paymentEntity
                , visitPayUserId);

            return processPaymentResponse;
        }

        private ProcessPaymentResponse SchedulePayment(Action<Payment, int?> action, Payment paymentEntity, int? visitPayUserId = null)
        {
            try
            {
                if (paymentEntity.PaymentMethod.IsNearExpiry(paymentEntity.ScheduledPaymentDate))
                {
                    string message = paymentEntity.PaymentMethod.IsExpired ?
                        this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.PaymentValidationPaymentMethodExpired) :
                        this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.PaymentValidationPaymentMethodNearExpiry);

                    return new ProcessPaymentResponse(true, message);
                }

                action.Invoke(paymentEntity, visitPayUserId);

                return new ProcessPaymentResponse(false, null, Mapper.Map<PaymentDto>(paymentEntity));
            }
            catch (Exception ex)
            {
                this.LoggingService.Value.Fatal(() => $"Failed in PaymentSubmissionApplicationService::SchedulePayment, Exception = {ExceptionHelper.AggregateExceptionToString(ex)}");

                string message = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.ErrorProcessingPayment);
                return new ProcessPaymentResponse(true, message);
            }
        }

        #endregion

        #region Payment Creation

        private IList<Payment> CreatePaymentFromPaymentDto(PaymentSubmissionDto paymentSubmissionDto, Guarantor guarantor, PaymentMethod paymentMethod, int actionVisitPayUserId)
        {
            List<Payment> payments = new List<Payment>();

            if (paymentSubmissionDto.PaymentType == PaymentTypeEnum.ManualPromptCurrentBalanceInFull ||
                paymentSubmissionDto.PaymentType == PaymentTypeEnum.ManualScheduledCurrentBalanceInFull)
            {
                // split this type into two payments
                // specific visits
                Payment specificVisitPayment = this.CreateAndSetupPayment(Mapper.Map<PaymentSubmissionDto>(paymentSubmissionDto), PaymentTypeEnum.ManualPromptCurrentNonFinancedBalanceInFull, guarantor, paymentMethod, actionVisitPayUserId);
                payments.Add(specificVisitPayment);

                // specific fp's
                Payment specificFinancePlansPayment = this.CreateAndSetupPayment(Mapper.Map<PaymentSubmissionDto>(paymentSubmissionDto), PaymentTypeEnum.ManualPromptSpecificFinancePlans, guarantor, paymentMethod, actionVisitPayUserId);
                payments.Add(specificFinancePlansPayment);
            }
            else
            {
                Payment payment = this.CreateAndSetupPayment(paymentSubmissionDto, null, guarantor, paymentMethod, actionVisitPayUserId);
                payments.Add(payment);
            }

            if (payments.Count == 0)
            {
                throw new Exception("Payments cannot be 0");
            }

            return payments;
        }

        private Payment CreateAndSetupPayment(PaymentSubmissionDto paymentSubmissionDto, PaymentTypeEnum? overridePaymentType, Guarantor guarantor, PaymentMethod paymentMethod, int actionVisitPayUserId)
        {
            PaymentTypeEnum paymentType = overridePaymentType.GetValueOrDefault(paymentSubmissionDto.PaymentType);

            Payment newPayment = new Payment
            {
                PaymentStatus = PaymentStatusEnum.ActivePending,
                PaymentType = this._paymentService.Value.AdjustPaymentType(paymentType, paymentSubmissionDto.PaymentDate),
                Guarantor = guarantor,
                ActualPaymentAmount = 0,
                ScheduledPaymentDate = paymentSubmissionDto.PaymentDate.Date,
                PaymentMethod = paymentMethod,
                IsHouseholdBalance = paymentSubmissionDto.IsHouseholdBalance ? (bool?)true : null,
                CreatedVisitPayUserId = actionVisitPayUserId
            };

            this.SetupPaymentScheduledAmounts(paymentSubmissionDto, newPayment);

            return newPayment;
        }

        #endregion

        #region Process Payment

        public IList<ProcessPaymentResponse> ProcessHouseholdBalancePayment(PaymentSubmissionDto paymentSubmissionDto, int visitPayUserId, int actionVisitPayUserId)
        {
            IDictionary<int, decimal> householdStatements = this._consolidationGuarantorApplicationService.Value.GetHouseholdBalances(visitPayUserId);
            IList<ProcessPaymentResponse> responseObjects = new List<ProcessPaymentResponse>();
            IDictionary<Payment, Func<Payment, IList<ProcessPaymentResponse>>> paymentsToProcess = new Dictionary<Payment, Func<Payment, IList<ProcessPaymentResponse>>>();

            PaymentMethod paymentMethod = this._paymentMethodsService.Value.GetById(paymentSubmissionDto.PaymentMethodId);
            if (paymentMethod == null)
            {
                string message = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.PaymentValidationInvalidPaymentMethod);
                return new ProcessPaymentResponse(true, message).ToListOfOne();
            }

            foreach (KeyValuePair<int, decimal> householdStatement in householdStatements.Where(x => x.Value > 0 && paymentSubmissionDto.HouseholdStatements.Contains(x.Key)).OrderByDescending(x => x.Value))
            {
                PaymentSubmissionDto individualPaymentDto = new PaymentSubmissionDto
                {
                    IsHouseholdBalance = true,
                    PaymentDate = paymentSubmissionDto.PaymentDate.Date,
                    PaymentMethodId = paymentSubmissionDto.PaymentMethodId,
                    PaymentType = PaymentTypeEnum.ManualScheduledCurrentNonFinancedBalanceInFull,
                    TotalPaymentAmount = householdStatement.Value
                };

                VpStatement statement = this._vpStatementService.Value.GetStatement(householdStatement.Key);
                Guarantor guarantor = this._guarantorService.Value.GetGuarantor(statement.VpGuarantorId);

                // apply discounts (we need to do this again here b/c we don't know which payment had a discount originally)
                DiscountOfferDto discountOfferDto = this.GetDiscountPerVisitForStatementedBalanceInFull(guarantor.VpGuarantorId);
                if (discountOfferDto != null && discountOfferDto.DiscountTotalPercent > 0)
                {
                    individualPaymentDto.TotalPaymentAmount -= discountOfferDto.VisitOffers.Sum(x => x.DiscountAmount);
                }

                IList<Payment> paymentEntities = this.CreatePaymentFromPaymentDto(individualPaymentDto, guarantor, paymentMethod, actionVisitPayUserId);
                foreach (Payment paymentEntity in paymentEntities)
                {
                    paymentsToProcess.Add(paymentEntity, p1 => this.ProcessPayment(individualPaymentDto, p1, visitPayUserId));
                }
            }

            // verify what we're charging matches what we told them we're charging.
            decimal actualAmount = paymentsToProcess.Sum(x => x.Key.ScheduledPaymentAmount);
            if (actualAmount != paymentSubmissionDto.TotalPaymentAmount)
            {
                this.LoggingService.Value.Fatal(() => $"Failed in PaymentSubmissionApplicationService::ProcessHouseHoldBalancePayment, Payment Amount Mismatch: {actualAmount}, {paymentSubmissionDto.TotalPaymentAmount}");

                string message = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.ErrorProcessingPayment);
                return new ProcessPaymentResponse(true, message).ToListOfOne();
            }

            // process
            foreach (KeyValuePair<Payment, Func<Payment, IList<ProcessPaymentResponse>>> x in paymentsToProcess)
            {
                IList<ProcessPaymentResponse> responses = x.Value(x.Key);
                foreach (ProcessPaymentResponse response in responses)
                {
                    responseObjects.Add(response);
                }
            }

            if (responseObjects.Any(x => !x.IsError) && !paymentMethod.IsActive)
            {
                // delete single use payment method
                this._paymentMethodsService.Value.Delete(paymentMethod, visitPayUserId);
            }

            return responseObjects;
        }

        public IList<ProcessPaymentResponse> ProcessPayment(PaymentSubmissionDto paymentSubmissionDto, int visitPayUserId, int actionVisitPayUserId, int? vpGuarantorId = null)
        {
            IList<ProcessPaymentResponse> responseObjects = new List<ProcessPaymentResponse>();

            try
            {
                Guarantor guarantor = this._guarantorService.Value.GetGuarantorEx(visitPayUserId, vpGuarantorId);
                PaymentMethod paymentMethod = this._paymentMethodsService.Value.GetById(paymentSubmissionDto.PaymentMethodId);

                ProcessPaymentResponse validationProcessPaymentResponse = this.ValidatePaymentMethod(paymentMethod, guarantor);
                if (validationProcessPaymentResponse.IsError)
                {
                    return validationProcessPaymentResponse.ToListOfOne();
                }

                if (paymentSubmissionDto.PaymentType == PaymentTypeEnum.ResubmitPayments)
                {
                    List<Payment> payments = new List<Payment>();
                    foreach (ResubmitPaymentDto resubmitPaymentDto in paymentSubmissionDto.PaymentsToResubmit)
                    {
                        if (resubmitPaymentDto.FinancePlanId != 0)
                        {
                            //Make sure it's their financePlan

                            FinancePlan financePlan = this._financePlanService.Value.GetFinancePlan(guarantor, resubmitPaymentDto.FinancePlanId);
                            if (financePlan != null)
                            {
                                //Dont charge more than amount due
                                decimal amountToCharge = Math.Min(resubmitPaymentDto.SnapshotTotalPaymentAmount, financePlan.AmountDue);
                                payments.Add(this._paymentService.Value.CreateRecurringPayment(resubmitPaymentDto.FinancePlanId, guarantor, amountToCharge, paymentMethod));
                            }
                        }
                    }

                    if (payments.IsNotNullOrEmpty())
                    {
                        responseObjects.AddRange(this.ChargeRecurringPayments(paymentSubmissionDto, payments, visitPayUserId));
                    }

                    //VP-7787: Set guarantor cure flag for all resubmitted payments, whether they failed or were successful. Used for monitoring by prod support.
                    this._paymentService.Value.SetGuarantorCure(guarantor.VpGuarantorId, responseObjects.Select(x => x.Payment.PaymentId).ToList());
                }
                else
                {
                    IList<Payment> paymentEntities = this.CreatePaymentFromPaymentDto(paymentSubmissionDto, guarantor, paymentMethod, actionVisitPayUserId);
                    foreach (Payment paymentEntity in paymentEntities)
                    {
                        responseObjects.AddRange(this.ProcessPayment(paymentSubmissionDto, paymentEntity, visitPayUserId));
                    }
                }

                if (responseObjects.Any(x => !x.IsError) && !paymentMethod.IsActive)
                {
                    // delete single use payment method
                    this._paymentMethodsService.Value.Delete(paymentMethod, visitPayUserId);
                }
            }
            catch (Exception e)
            {
                string message = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.ErrorProcessingPayment);
                responseObjects.Add(new ProcessPaymentResponse(true, message));

                JsonSerializerSettings settings = new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore };
                this.LoggingService.Value.Fatal(() => $"Failed in PaymentSubmissionApplicationService::ProcessPayment, PaymentSubmissionDto = {JsonConvert.SerializeObject(paymentSubmissionDto, settings)} , Exception = {ExceptionHelper.AggregateExceptionToString(e)}");
            }

            return responseObjects;
        }

        private IList<ProcessPaymentResponse> ProcessPayment(PaymentSubmissionDto paymentSubmissionDto, Payment paymentEntity, int visitPayUserId)
        {
            //if the payment is scheduled for the future and is actually a scheduled type then save the payment and let the payment processor pick it up
            if (paymentEntity.IsScheduledPayType() && paymentEntity.ScheduledPaymentDate.Date > this.ClientNow.Date)
            {
                return this.SchedulePayment(paymentEntity, visitPayUserId).ToListOfOne();
            }

            return this.ChargePayment(paymentSubmissionDto, paymentEntity, visitPayUserId);
        }

        private IList<ProcessPaymentResponse> ChargePayment(PaymentSubmissionDto paymentDto, Payment paymentEntity, int? visitPayUserId = null)
        {
            try
            {
                IList<ChargePaymentResponse> paymentsCharged = this._paymentService.Value.ChargePayment(paymentEntity, visitPayUserId);

                return this.PostCharge(paymentsCharged, visitPayUserId);
            }
            catch (Exception ex)
            {
                // Should watch for these, this shouldn't happen.  Everything in here should catch it's own errors for the most part.
                JsonSerializerSettings settings = new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore };
                this.LoggingService.Value.Fatal(() => $"Failed in PaymentSubmissionApplicationService::ChargePayment, PaymentSubmissionDto = {JsonConvert.SerializeObject(paymentDto, settings)} , Exception = {ExceptionHelper.AggregateExceptionToString(ex)}");

                string message = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.ErrorProcessingPayment);
                return new ProcessPaymentResponse(true, message).ToListOfOne();
            }
        }

        private IList<ProcessPaymentResponse> ChargeRecurringPayments(PaymentSubmissionDto paymentDto, IList<Payment> payments, int? visitPayUserId = null)
        {
            try
            {
                IList<ChargePaymentResponse> paymentsCharged = this._paymentService.Value.ChargeRecurringPayments(payments, visitPayUserId);

                return this.PostCharge(paymentsCharged, visitPayUserId);
            }
            catch (Exception ex)
            {
                // Should watch for these, this shouldn't happen.  Everything in here should catch it's own errors for the most part.
                this.LoggingService.Value.Fatal(() => $"Failed in PaymentSubmissionApplicationService::ChargeRecurringPayments, PaymentSubmissionDto = {JsonConvert.SerializeObject(paymentDto)} , Exception = {ExceptionHelper.AggregateExceptionToString(ex)}");

                string message = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.ErrorProcessingPayment);
                return new ProcessPaymentResponse(true, message).ToListOfOne();
            }
        }

        private IList<ProcessPaymentResponse> PostCharge(IList<ChargePaymentResponse> paymentsCharged, int? visitPayUserId)
        {
            // emails
            this._paymentService.Value.SendEmailsForPromptPayments(paymentsCharged.Select(x => x.Payment).ToList());

            // error messaging
            IList<ProcessPaymentResponse> responseObjects = new List<ProcessPaymentResponse>();
            foreach (IGrouping<PaymentMethod, ChargePaymentResponse> grouping in paymentsCharged.GroupBy(x => x.Payment.PaymentMethod))
            {
                string errorMessage = null;
                string defaultErrorMessage = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.ErrorProcessingPayment);

                Payment payment = grouping.First().Payment;
                switch (payment.PaymentStatus)
                {
                    case PaymentStatusEnum.ActivePendingGuarantorAction:
                    case PaymentStatusEnum.ClosedFailed:
                        PaymentProcessorResponse paymentProcessorResponse = payment.PaymentProcessorResponses.OrderByDescending(x => x.InsertDate).ThenByDescending(x => x.PaymentProcessorResponseId).FirstOrDefault();
                        if (paymentProcessorResponse != null)
                        {
                            errorMessage = payment.IsPromptPayType() ? Utility.GetRealTimeStatusMessage(paymentProcessorResponse) : Utility.GetStatusMessage(paymentProcessorResponse, payment);
                        }
                        break;
                    case PaymentStatusEnum.ActiveGatewayError:
                        // if this is a prompt-payment or we have exceeded the PaymentMaxAttemptCount, set to fail no reschedule
                        foreach (Payment p in grouping.Select(x => x.Payment).ToList())
                        {
                            if (p.PaymentAttemptCount > this.Client.Value.PaymentMaxAttemptCount)
                            {
                                p.PaymentStatus = PaymentStatusEnum.ClosedFailed;
                                this._paymentService.Value.UpdatePayment(p.Guarantor, p, visitPayUserId);
                            }
                        }
                        errorMessage = defaultErrorMessage;
                        break;
                    case PaymentStatusEnum.ActivePendingLinkFailure:
                        errorMessage = defaultErrorMessage;
                        break;
                    default:
                        errorMessage = defaultErrorMessage;
                        break;
                }

                if (payment.IsPromptPayType() && payment.ActualPaymentAmount != payment.ScheduledPaymentAmount && payment.ActualPaymentAmount != 0m)
                {
                    errorMessage = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.PaymentAmountChanged);
                }

                responseObjects.AddRange(grouping.Select(x => new ProcessPaymentResponse(!x.Success, errorMessage, Mapper.Map<PaymentDto>(x.Payment))));
            }

            return responseObjects;
        }

        #endregion

        #region Text (SMS) To Pay

        public ProcessPaymentResponse SaveTextToPayPayment(PaymentSubmissionDto paymentSubmissionDto, int vpGuarantorId)
        {

            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            if (guarantor == null)
            {
                return new ProcessPaymentResponse(true, $"Could not find guarantor");
            }

            PaymentMethod paymentMethod = this._paymentMethodsService.Value.GetById(paymentSubmissionDto.PaymentMethodId);
            if (paymentMethod == null)
            {
                return new ProcessPaymentResponse(true, $"Could not find payment method");
            }

            int actionVisitPayUserId = -1;
            Payment payment = this.CreateAndSetupPayment(paymentSubmissionDto, paymentSubmissionDto.PaymentType, guarantor, paymentMethod, actionVisitPayUserId);
            if (payment == null)
            {
                return new ProcessPaymentResponse(true, $"Could not find create payment");
            }
            payment.PaymentStatus = PaymentStatusEnum.PaymentPendingGuarantorResponse;

            ProcessPaymentResponse processPaymentResponse = this.SchedulePayment((p, v) => this._paymentService.Value.SavePendingGuarantorResponsePayment(payment, null), payment, null);

            return processPaymentResponse;
        }

        public IList<ProcessPaymentResponse> ChargeTextToPayPayment(int visitPayUserId)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantorEx(visitPayUserId);
            Payment payment = this._paymentService.Value.GetMostRecentTextToPayPaymentForGuarantor(guarantor.VpGuarantorId);
            if (payment == null)
            {
                return new ProcessPaymentResponse(true, $"Could not find TextToPayPayment for guarantorId {guarantor.VpGuarantorId}").ToListOfOne();
            }

            IList<ChargePaymentResponse> chargePaymentResponses = this._paymentService.Value.ChargePayment(payment);
            IList<ProcessPaymentResponse> postProcessPaymentResponses = this.PostCharge(chargePaymentResponses, visitPayUserId);
            if (!postProcessPaymentResponses.Any())
            {
                return new ProcessPaymentResponse(true, null, Mapper.Map<PaymentDto>(payment)).ToListOfOne();
            }

            return postProcessPaymentResponses;

        }

        #endregion

        #region LockBox Finance Plan Payment

        public void ReprocessPaymentImports(IList<PaymentImportDto> paymentImportDtos)
        {
            foreach (PaymentImportDto updatedPaymentImportDto in paymentImportDtos)
            {
                //Get the existing payment import row that has been updated by the DTO values
                PaymentImport existingPaymentImport = this.GetUpdatedExistingPaymentImport(updatedPaymentImportDto);

                if (existingPaymentImport == null)
                {
                    //Doesn't exist, can't update
                    this.LoggingService.Value.Warn(() => $"{nameof(PaymentSubmissionApplicationService)}::{nameof(this.ReprocessPaymentImports)}::No PaymentImport record found with PaymentImportId: {updatedPaymentImportDto.PaymentImportId}");
                    continue;
                }

                if (!existingPaymentImport.IsEditable)
                {
                    //Invalid status, can't update
                    this.LoggingService.Value.Warn(() => $"{nameof(PaymentSubmissionApplicationService)}::{nameof(this.ReprocessPaymentImports)}::Cannot update PaymentImport with PaymentImportId: {updatedPaymentImportDto.PaymentImportId}. Reason: Record is in invalid status for updating - {existingPaymentImport.PaymentImportStateEnum.ToString()}");
                    continue;
                }

                //Update the paymentImport
                this._paymentService.Value.SavePaymentImport(existingPaymentImport);

                //Attempt to post the updated payment import
                if (existingPaymentImport.CanPost)
                {
                    this.PostPaymentImport(existingPaymentImport);
                }
            }
        }

        private PaymentImport GetUpdatedExistingPaymentImport(PaymentImportDto paymentImportDto)
        {
            PaymentImport existingPaymentImport = this._paymentService.Value.GetPaymentImport(paymentImportDto.PaymentImportId);
            if (existingPaymentImport != null && existingPaymentImport.IsEditable)
            {
                //Only update fields that allowed to be updated
                existingPaymentImport.GuarantorAccountNumber = paymentImportDto.GuarantorAccountNumber;
                existingPaymentImport.GuarantorFirstName = paymentImportDto.GuarantorFirstName;
                existingPaymentImport.GuarantorLastName = paymentImportDto.GuarantorLastName;
                existingPaymentImport.VpGuarantor = this._guarantorService.Value.GetGuarantorBySsk(paymentImportDto.GuarantorAccountNumber);

                //Allow changing status to cancelled or failed. Need to be able to switch back to failed if a user needs to process a record they previously marked as cancelled.
                if (paymentImportDto.PaymentImportStateEnum == PaymentImportStateEnum.Cancelled || paymentImportDto.PaymentImportStateEnum == PaymentImportStateEnum.Failed)
                {
                    existingPaymentImport.PaymentImportStateEnum = paymentImportDto.PaymentImportStateEnum;
                }
            }
            return existingPaymentImport;
        }

        public IList<ProcessPaymentResponse> ImportLockBoxFinancePlanPayment(decimal paymentAmount, DateTime statementPeriodStateDate, int actionVisitPayUserId, int vpGuarantorId)
        {
            IList<ProcessPaymentResponse> responseObjects = new List<ProcessPaymentResponse>();

            if (!this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureIsLockBoxEnabled))
            {
                responseObjects.Add(new ProcessPaymentResponse()
                {
                    ErrorMessage = "Lock box deposits are not an allowed payment method",
                    IsError = true
                });
                return responseObjects;
            }

            FinancePlanStatementDto financePlanStatementDto = this._statementApplicationService.Value.GetFinancePlanStatementByPeriodStartDate(vpGuarantorId, statementPeriodStateDate);

            //Get most recent finance plan statement if statement wasn't found starting on the specified date
            if (financePlanStatementDto == null)
            {
                financePlanStatementDto = this._statementApplicationService.Value.GetMostRecentFinancePlanStatement(vpGuarantorId);
            }

            if (financePlanStatementDto == null)
            {
                responseObjects.Add(new ProcessPaymentResponse()
                {
                    ErrorMessage = "No Finance Plan Statement was found",
                    IsError = true
                });
                return responseObjects;
            }

            if (financePlanStatementDto != null && financePlanStatementDto.FinancePlanStatementFinancePlans.Count == 0)
            {
                responseObjects.Add(new ProcessPaymentResponse()
                {
                    ErrorMessage = "Finance Plan Statement does not contain any finance plans",
                    IsError = true
                });
                return responseObjects;
            }

            List<FinancePlanPaymentDto> financePlanPayments = this._paymentAllocationApplicationService.Value.GetFinancePlanPaymentAmounts(paymentAmount, financePlanStatementDto);

            PaymentSubmissionDto paymentSubmissionDto = new PaymentSubmissionDto()
            {
                TotalPaymentAmount = paymentAmount,
                PaymentDate = DateTime.UtcNow,
                PaymentType = PaymentTypeEnum.LockBoxManualPromptSpecificFinancePlansBucketZero,
                FinancePlanPayments = financePlanPayments
            };

            try
            {
                PaymentMethodResultDto paymentMethodResultDto = this._paymentMethodsApplicationService.Value.SaveTempLockBoxPaymentMethod(vpGuarantorId, actionVisitPayUserId);

                if (paymentMethodResultDto.IsError)
                {
                    return new ProcessPaymentResponse()
                    {
                        ErrorMessage = paymentMethodResultDto.ErrorMessage,
                        IsError = true
                    }.ToListOfOne();
                }

                Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
                PaymentMethod paymentMethod = this._paymentMethodsService.Value.GetById(paymentMethodResultDto.PaymentMethod.PaymentMethodId ?? 0, vpGuarantorId);

                ProcessPaymentResponse validationProcessPaymentResponse = this.ValidatePaymentMethod(paymentMethod, guarantor);
                if (validationProcessPaymentResponse.IsError)
                {
                    return validationProcessPaymentResponse.ToListOfOne();
                }

                paymentSubmissionDto.PaymentMethodId = paymentMethod.PaymentMethodId;

                IList<Payment> paymentEntities = this.CreatePaymentFromPaymentDto(paymentSubmissionDto, guarantor, paymentMethod, actionVisitPayUserId);
                foreach (Payment paymentEntity in paymentEntities)
                {
                    IList<ProcessPaymentResponse> responses = this.ProcessPayment(paymentSubmissionDto, paymentEntity, actionVisitPayUserId);
                    responseObjects.AddRange(responses);
                }
            }
            catch (Exception e)
            {
                string message = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.ErrorProcessingPayment);
                responseObjects.Add(new ProcessPaymentResponse(true, message));

                JsonSerializerSettings settings = new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore };
                this.LoggingService.Value.Fatal(() => $"Failed in PaymentSubmissionApplicationService::ImportLockboxPayment, PaymentSubmissionDto = {JsonConvert.SerializeObject(paymentSubmissionDto, settings)} , Exception = {ExceptionHelper.AggregateExceptionToString(e)}");
            }

            return responseObjects;
        }

        #endregion

        #region Helpers

        public void FixPaymentAllocations(int paymentId)
        {
            this._paymentService.Value.FixPaymentAllocations(paymentId);
        }

        public void PopulatePaymentMethodTypeOnPaymentAllocationMessages(IList<PaymentAllocationMessage> paymentAllocationMessages)
        {
            this._paymentService.Value.PopulatePaymentMethodTypeOnPaymentAllocationMessages(paymentAllocationMessages); ;
        }

        #endregion

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(PaymentImportFileMessage message, ConsumeContext<PaymentImportFileMessage> consumeContext)
        {
            // don't want to reprocess this message automatically. swallowing exceptions.
            try
            {
                // import payments
                IList<PaymentImport> paymentImports = this.ImportPayments(message.Messages);

                // send payment import count
                this._metricsProvider.Value.Increment(Metrics.Increment.PaymentImport.PaymentImportProcessed, paymentImports.Count);

                // process payments
                foreach (PaymentImport paymentImport in paymentImports)
                {
                    this.PostPaymentImport(paymentImport);
                }

                // send payment processed count
                int paymentImportPaymentsProcessed = paymentImports.Count(x => x.PaymentImportStateEnum != PaymentImportStateEnum.Pending);
                this._metricsProvider.Value.Increment(Metrics.Increment.PaymentImport.PaymentImportPaymentProcessed, paymentImportPaymentsProcessed);
            }
            catch (Exception e)
            {
                this.LoggingService.Value.Fatal(() => $"Failed in {nameof(PaymentSubmissionApplicationService)}::{nameof(this.ConsumeMessage)}<{nameof(PaymentImportFileMessage)}>, Exception = {ExceptionHelper.AggregateExceptionToString(e)}");
            }
        }

        public bool IsMessageValid(PaymentImportFileMessage message, ConsumeContext<PaymentImportFileMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        /// <summary>
        /// imports lockbox payment messages into the app.
        /// </summary>
        /// <param name="importMessages"></param>
        /// <returns>a list successfully imported payments</returns>
        private IList<PaymentImport> ImportPayments(IList<PaymentImportMessage> importMessages)
        {
            IList<PaymentImport> paymentImports = new List<PaymentImport>();
            try
            {
                foreach (PaymentImportMessage paymentImportMessage in importMessages)
                {
                    PaymentImport paymentImport = Mapper.Map<PaymentImport>(paymentImportMessage);

                    // Assign VpGuarantor by SourceSystemKey lookup 
                    // Currently BillingSystemId is not available from file source
                    paymentImport.VpGuarantor = this._guarantorService.Value.GetGuarantorBySsk(paymentImport.GuarantorAccountNumber);

                    // add to list for batch insert
                    paymentImports.Add(paymentImport);
                }

                // insert all new payment import records
                using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
                {
                    foreach (PaymentImport paymentImport in paymentImports)
                    {
                        this._paymentService.Value.SavePaymentImport(paymentImport);
                    }

                    unitOfWork.Commit();
                }
            }
            catch (Exception e)
            {
                // if there was an error, the transaction was rolled back or never started. return no successful entities.
                paymentImports.Clear();

                this.LoggingService.Value.Fatal(() => $"Failed in {nameof(PaymentSubmissionApplicationService)}::{nameof(this.ImportPayments)}, Exception = {ExceptionHelper.AggregateExceptionToString(e)}");
            }

            return paymentImports;
        }

        private void PostPaymentImport(PaymentImport paymentImport)
        {
            // swallowing exceptions here because we are expecting the client to manage those via the payment import workflow
            try
            {
                if (paymentImport.CanPost)
                {
                    if (paymentImport.VpGuarantor != null)
                    {
                        decimal amount = paymentImport.InvoiceAmount ?? 0;
                        if (amount > 0)
                        {
                            // process payment
                            IList<ProcessPaymentResponse> processPaymentResponses = this.ImportLockBoxFinancePlanPayment(amount, DateTime.MaxValue, SystemUsers.SystemUserId, paymentImport.VpGuarantor.VpGuarantorId);

                            // handle PaymentImportStateEnum
                            bool hasError = processPaymentResponses.Any(x => x.IsError);
                            paymentImport.PaymentImportStateEnum = hasError ? PaymentImportStateEnum.Failed : PaymentImportStateEnum.Success;

                            // handle paymentImportStateReason
                            if (hasError)
                            {
                                //Error posting payment, update import record with reason
                                List<string> errors = processPaymentResponses.Where(x => x.IsError).Select(x => x.ErrorMessage).ToList();
                                string allErrors = string.Join(", ", errors);
                                paymentImport.PaymentImportStateReason = allErrors;
                            }
                            else
                            {
                                //Payment posted successfully, clear out any old status reasons
                                paymentImport.PaymentImportStateReason = null;
                            }

                            // set payment reference -- may be null due to processing error
                            PaymentDto paymentDto = processPaymentResponses.FirstOrDefault(x => x.Payment != null && x.Payment.PaymentId > 0)?.Payment;
                            if (paymentDto != null)
                            {
                                paymentImport.Payment = Mapper.Map<Payment>(paymentDto);
                            }
                        }
                    }
                    else
                    {
                        //No guarantor found, set state to failed and add reason notes
                        paymentImport.PaymentImportStateEnum = PaymentImportStateEnum.Failed;
                        paymentImport.PaymentImportStateReason = "No guarantor found";
                    }

                    //Save
                    this._paymentService.Value.SavePaymentImport(paymentImport);
                }
            }
            catch (Exception e)
            {
                JsonSerializerSettings settings = new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore };
                this.LoggingService.Value.Fatal(() => $"Failed in {nameof(PaymentSubmissionApplicationService)}::{nameof(PostPaymentImport)}, PaymentImport = {JsonConvert.SerializeObject(paymentImport, settings)} , Exception = {ExceptionHelper.AggregateExceptionToString(e)}");

                //Attempt to update payment import record to failed so that clients can update and reprocess it. Catch exceptions in case error was data/connection related
                try
                {
                    paymentImport.PaymentImportStateEnum = PaymentImportStateEnum.Failed;
                    paymentImport.PaymentImportStateReason = "An unhandled error occurred";

                    //Save
                    this._paymentService.Value.SavePaymentImport(paymentImport);
                }
                catch (Exception ex)
                {
                    this.LoggingService.Value.Fatal(() => $"Failed in {nameof(PaymentSubmissionApplicationService)}::{nameof(this.PostPaymentImport)}, PaymentImport = {JsonConvert.SerializeObject(paymentImport, settings)} , Exception = {ExceptionHelper.AggregateExceptionToString(ex)}");
                }
            }
        }

        #endregion

        #region Jobs

        void IJobRunnerService<FixPaymentAllocationsForPaymentJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.LoggingService.Value.Info(() => $"FixPaymentAllocationsForPaymentJobRunner::Execute Start - Time = {DateTime.UtcNow}");
            foreach (string arg in args)
            {

                try
                {
                    this.LoggingService.Value.Info(() => $"FixPaymentAllocationsForPaymentJobRunner::Execute Fixing payment {arg}");
                    Console.WriteLine($"FixPaymentAllocationsForPaymentJobRunner::Execute Fixing payment {arg}", arg);
                    int paymentId;
                    if (int.TryParse(arg, out paymentId))
                    {
                        this.FixPaymentAllocations(paymentId);
                    }
                    else
                    {
                        this.LoggingService.Value.Warn(() => $"FixPaymentAllocationsForPaymentJobRunner::Execute Couldn't parse {arg}");
                        Console.WriteLine($"FixPaymentAllocationsForPaymentJobRunner::Execute Couldn't parse {arg}");
                    }
                }
                catch (Exception e)
                {
                    this.LoggingService.Value.Fatal(() => $"FixPaymentAllocationsForPaymentJobRunner::Execute Fixing payment {arg} failed! Error = {ExceptionHelper.AggregateExceptionToString(e)}");
                    throw;
                }

            }
            this.LoggingService.Value.Info(() => $"FixPaymentAllocationsForPaymentJobRunner::Execute End - Time = {DateTime.UtcNow}");
        }

        #endregion
    }
}
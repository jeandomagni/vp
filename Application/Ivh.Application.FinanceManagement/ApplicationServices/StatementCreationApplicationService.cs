﻿namespace Ivh.Application.FinanceManagement.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Core.Common.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Interfaces;
    using Domain.FinanceManagement.Statement.Entities;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Jobs;
    using Ivh.Common.VisitPay.Messages.Communication;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;
    using MassTransit;
    using NHibernate;

    public class StatementCreationApplicationService : ApplicationService, IStatementCreationApplicationService
    {
        private readonly Lazy<IVisitBalanceBaseApplicationService> _visitBalanceBaseApplicationService;
        private readonly ISession _session;

        private readonly Lazy<IFinancePlanService> _financePlanService;
        private readonly Lazy<IStatementDateService> _statementDateService;
        private readonly Lazy<IStatementSnapshotCreationApplicationService> _statementSnapshotCreationApplicationService;
        private readonly Lazy<IFinancePlanApplicationService> _financePlanApplicationService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IVpStatementService> _vpStatementService;
        private readonly Lazy<IFinancePlanStatementService> _financePlanStatementService;
        private readonly Lazy<IVisitPayUserApplicationService> _visitPayUserApplicationService;

        public StatementCreationApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IVisitBalanceBaseApplicationService> visitBalanceBaseApplicationService,
            ISessionContext<VisitPay> sessionContext,
            Lazy<IVpStatementService> vpStatementService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IFinancePlanService> financePlanService,
            Lazy<IStatementDateService> statementDateService,
            Lazy<IStatementSnapshotCreationApplicationService> statementSnapshotCreationApplicationService,
            Lazy<IFinancePlanApplicationService> financePlanApplicationService,
            Lazy<IFinancePlanStatementService> financePlanStatementService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService
            ) : base(applicationServiceCommonService)
        {
            this._visitBalanceBaseApplicationService = visitBalanceBaseApplicationService;
            this._session = sessionContext.Session;
            this._vpStatementService = vpStatementService;
            this._guarantorService = guarantorService;
            this._financePlanService = financePlanService;
            this._statementDateService = statementDateService;
            this._statementSnapshotCreationApplicationService = statementSnapshotCreationApplicationService;
            this._financePlanApplicationService = financePlanApplicationService;
            this._financePlanStatementService = financePlanStatementService;
            this._visitPayUserApplicationService = visitPayUserApplicationService;
        }

        public bool QueueAllPendingStatementGuarantors(DateTime date)
        {
            if (this.Bus == null || this._vpStatementService == null)
            {
                return false;
            }

            IList<int> ids = this._guarantorService.Value.GetActiveGuarantorsWithPendingStatement(date);
            foreach (int id in ids)
            {
                this.Bus.Value.PublishMessage(new CreateStatementMessage { VpGuarantorId = id, DateToProcess = date, ImmediateStatement = false }).Wait();
            }

            return true;
        }

        private const string StatementLockKey = "StatementGenerating";

        public async Task CreateStatementForGuarantorAsync(int vpGuarantorId, DateTime dateToProcess, bool isImmediateStatement)
        {
            if (vpGuarantorId > 0)
            {
                string statementLockString = $"{StatementLockKey}{vpGuarantorId}";
                if (!this.Cache.Value.GetLock(statementLockString))
                {
                    this.LoggingService.Value.Fatal(() => $"StatementCreationApplicationService::CreateStatementForGuarantorAsync failed to aquire distributed lock for VPG: {vpGuarantorId}");
                    throw new Exception("Failed to aquire lock.  Trying again...");
                }

                try
                {
                    Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);

                    VpStatement previousStatement = this._vpStatementService.Value.GetMostRecentStatement(guarantor);
                    bool isFirstStatement = previousStatement == null;
                    if (isImmediateStatement && previousStatement != null)
                    {
                        //Cannot create an ImmediateStatement if you already have a statement.
                        return;
                    }

                    DateTime nextStatementDate = this._statementDateService.Value.GetNextStatementDate(previousStatement?.OriginalPaymentDueDate, guarantor, dateToProcess, isImmediateStatement, isFirstStatement).Date;
                    DateTime nextPeriodEndDate = this._statementDateService.Value.GetStatementPeriodEndDate(previousStatement?.OriginalPaymentDueDate, guarantor, dateToProcess, isImmediateStatement, isFirstStatement);
                    DateTime nextPeriodStartDate = this._vpStatementService.Value.GetStatementPeriodStartDate(previousStatement, guarantor);

                    //Only check this rule if creating regular statements.
                    //Make sure that the statementdate is the less than or equal the dateToProcess, dont want to do this for the future.
                    if (!isImmediateStatement && guarantor.NextStatementDate != null &&
                        (
                            guarantor.NextStatementDate.Value.Date > dateToProcess.Date ||
                            nextStatementDate > dateToProcess.Date ||
                            nextPeriodStartDate > nextPeriodEndDate)
                        )
                    {
                        IDictionary<string, string> logValues = new Dictionary<string, string>
                        {
                            {"DateToProcess", dateToProcess.ToString()},
                            {"GuarantorNextStatementDate", guarantor.NextStatementDate.Value.ToString()},
                            {"CalculatedNextStatementDate", nextStatementDate.ToString()},
                            {"NextPeriodStartDate", nextPeriodStartDate.ToString()},
                            {"NextPeriodEndDate", nextPeriodEndDate.ToString()}
                        };
                        string logString = string.Join(", ", logValues.Select(x => $"{x.Key} = {x.Value}"));

                        //if (nextPeriodStartDate > nextPeriodEndDate && previousStatement != null && previousStatement.IsImmediateStatement)
                        //{
                        //    using (UnitOfWork uow = new UnitOfWork(this._session))
                        //    {
                        //        //We need to skip a day as that start date cant be before the end date, this only happens when a the previous statement was soft and the guarantor should statement right away.
                        //        this._guarantorService.Value.ChangeStatementDate(guarantor, nextStatementDate.AddDays(1), null);
                        //        this.LoggingService.Value.Warn(() =>  $"StatementCreationApplicationService::CreateStatementForGuarantorAsync Change Statement Date - Period Date Mismatch - {logString}");
                        //        uow.Commit();    
                        //    }

                        //}
                        //Statement was already generated or it's going to generate for the future or the start date is greater than end date (invalid case)
                        if (nextStatementDate > guarantor.NextStatementDate)
                        {
                            //Since this should have been pushed out, get it out of the queue for the next time it runs, so we're going to update the next statement date so it doesnt get picked up until it's supposed to generate.
                            using (UnitOfWork uow = new UnitOfWork(this._session))
                            {
                                this._guarantorService.Value.ChangeStatementDate(guarantor, nextStatementDate);
                                this.LoggingService.Value.Warn(() => $"StatementCreationApplicationService::CreateStatementForGuarantorAsync ChangeStatementDate - Statement Date Mismatch - {logString}");
                                uow.Commit();
                            }

                        }
                        else
                        {
                            this.LoggingService.Value.Warn(() => $"StatementCreationApplicationService::CreateStatementForGuarantorAsync ChangeStatementDate - Statement Not Created - {logString}");
                        }
                        return;
                    }

                    // kill any pending acceptance of terms fp's
                    using (UnitOfWork uow = new UnitOfWork(this._session))
                    {
                        IList<FinancePlan> pendingAcceptanceTermsFp = this._financePlanService.Value.GetAllPendingAcceptanceFinancePlansForGuarantor(guarantor);
                        foreach (FinancePlan financePlan in pendingAcceptanceTermsFp)
                        {
                            DateTime minimumResponseDate = financePlan.InsertDate.Date.AddDays(this.Client.Value.CreditAgreementMinResponsePeriod);
                            if (!guarantor.IsOfflineGuarantor() || minimumResponseDate.Date < dateToProcess.Date)
                            {
                                this._financePlanService.Value.CancelFinancePlan(financePlan, "Terms not accepted");
                            }
                        }
                        uow.Commit();
                    }

                    SendFinancePlanAdditionalBalanceEmailMessage fpAdditionalBalanceMessage = this.GetFinancePlanAdditionalBalanceMessage(guarantor);

                    using (UnitOfWork uow = new UnitOfWork(this._session))
                    {
                        //RoboREFUND - Step 1 - TOTALLY NOT REASSESSMENT.
                        //Need these committed to the db before the actual robo refund/reallocation happens
                        //This expands the naturallyQualifying events (negative adjustments to active financeplans) to the closed finance plans
                        this._financePlanApplicationService.Value.RoboRefund_AddNonNaturalQualifyingEvents(guarantor.VpGuarantorId);
                        uow.Commit();
                    }
                    using (UnitOfWork uow = new UnitOfWork(this._session))
                    {
                        //RoboREFUND - Step 2 - TOTALLY NOT REASSESSMENT.
                        //This logs what it's doing and reallocates/refunds interest
                        IList<PaymentDto> results = this._financePlanApplicationService.Value.RoboRefund(guarantor.VpGuarantorId, nextPeriodEndDate);
                        uow.Commit();
                    }

                    IList<FinancePlan> allOriginatedFinancePlans = this._financePlanService.Value.GetAllOpenFinancePlansForGuarantor(guarantor);

                    foreach (FinancePlan financePlan in allOriginatedFinancePlans)
                    {
                        this._financePlanApplicationService.Value.ApplyInterestRefundIncentive(financePlan.FinancePlanId);
                    }

                    IList<FinancePlanVisitInterestDue> interestAssessments = new List<FinancePlanVisitInterestDue>();
                    if (!guarantor.IsClosed)
                    {
                        using (UnitOfWork uow = new UnitOfWork(this._session))
                        {
                            foreach (FinancePlan financePlan in allOriginatedFinancePlans)
                            {
                                interestAssessments.AddRange(this._financePlanService.Value.AssessInterest(financePlan, nextPeriodStartDate, nextPeriodEndDate, dateToProcess));
                            }

                            uow.Commit();
                        }
                    }

                    VpStatement newStatement;
                    using (UnitOfWork uow = new UnitOfWork(this._session))
                    {
                        GeneratedStatementsResultDto statements = await this._statementSnapshotCreationApplicationService.Value.GenerateStatementForDateAsync(guarantor, dateToProcess, isImmediateStatement).ConfigureAwait(false);
                        newStatement = statements?.VpStatement;
                        FinancePlanStatement newFpStatement = statements?.FinancePlanStatement;

                        if (newStatement != null)
                        {
                            this._session.Transaction.RegisterPostCommitSuccessAction(s =>
                            {
                                using (UnitOfWork anotherUow = new UnitOfWork(this._session))
                                {
                                    this._financePlanStatementService.Value.FixMissingInterest(newStatement.VpStatementId);
                                    
                                    List<FinancePlan> financePlansToUpdate = new List<FinancePlan>();

                                    foreach (FinancePlanVisitInterestDue interestAssessment in interestAssessments)
                                    {
                                        interestAssessment.VpStatementId = newStatement.VpStatementId;
                                        financePlansToUpdate.Add(interestAssessment.FinancePlan);
                                    }

                                    foreach (FinancePlan financePlanToUpdate in financePlansToUpdate.GroupBy(x => x.FinancePlanId).Select(x => x.First()))
                                    {
                                        this._financePlanService.Value.UpdateFinancePlan(financePlanToUpdate);
                                    }

                                    anotherUow.Commit();
                                }
                            }, newStatement);
                        }

                        if (newStatement != null && !guarantor.IsClosed)
                        {
                            this.AddNextPaymentAmountForFinancePlans(guarantor, newStatement);

                            StatementNotificationMessage statementMessage = null;
                            FinancePlanStatementNotificationMessage fpStatementMessage = null;

                            //TODO(maybe) -- VP-7862
                            bool hasFinancePlans = allOriginatedFinancePlans.Any();
                            int visitPayUserId = guarantor.User.VisitPayUserId;
                            bool hasStatementMailPreference = this._visitPayUserApplicationService.Value.UserHasCommunicationPreference(visitPayUserId, CommunicationMethodEnum.Mail, CommunicationGroupEnum.StatementAndBalanceDue);
                            bool useAutopay = guarantor.UseAutoPay;

                            // flag this statement to be exported to a statement file later, if needed.
                            if (!isImmediateStatement && 
                                hasStatementMailPreference && 
                                hasFinancePlans && 
                                !useAutopay)
                            {
                                newStatement.CreatePaperStatement = true;
                            }

                            if (guarantor.IsOfflineGuarantor() && this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.OfflineVisitPay))
                            {
                                fpStatementMessage = this.GetFpStatementNotificationMessage(vpGuarantorId, newFpStatement, true);
                            }
                            else
                            {
                                statementMessage = this.GetStatementNotificationMessage(vpGuarantorId, newStatement);
                                fpStatementMessage = this.GetFpStatementNotificationMessage(vpGuarantorId, newFpStatement, false);
                            }

                            if (statementMessage != null || fpStatementMessage != null)
                            {
                                this._session.Transaction.RegisterPostCommitSuccessAction((sm, fpsm, fpabm) =>
                                {
                                    try
                                    {
                                        if (sm != null)
                                        {
                                            this.Bus.Value.PublishMessage(sm).Wait();
                                        }

                                        if (fpsm != null)
                                        {
                                            this.Bus.Value.PublishMessage(fpsm).Wait();
                                        }

                                        if (fpabm != null)
                                        {
                                            this.Bus.Value.PublishMessage(fpabm).Wait();
                                        }
                                    }
                                    catch
                                    {
                                        //Silently handle.  The result gets saved into the Email table.
                                    }
                                }, statementMessage, fpStatementMessage, fpAdditionalBalanceMessage);
                            }
                        }

                        uow.Commit();
                    }

                    // in the case of ImmediateStatement and when the guarantors next statement date is today, just generate the statement. VPNG-13128
                    if (newStatement != null && isImmediateStatement && guarantor?.NextStatementDate != null && guarantor.NextStatementDate.Value.Date == dateToProcess.Date)
                    {
                        this.Bus.Value.PublishMessage(new CreateStatementMessage
                        {
                            VpGuarantorId = vpGuarantorId,
                            DateToProcess = dateToProcess,
                            ImmediateStatement = false
                        }).Wait();
                    }
                }
                finally
                {
                    this.Cache.Value.ReleaseLock(statementLockString);
                }
            }
        }

        public StatementNotificationMessage GetStatementNotificationMessage(int vpGuarantorId, VpStatement statement)
        {
            if (statement == null)
            {
                return null;
            }

            if (!statement.ActiveVpStatementVisitsNotOnFinancePlan.Any())
            {
                // do not send if there are no active visits on the statement
                return null;
            }

            AgingTierEnum agingTier = statement.ActiveVpStatementVisitsNotOnFinancePlan.Any() ? statement.ActiveVpStatementVisitsNotOnFinancePlan.Max(x => x.Visit.AgingTier) : AgingTierEnum.Good;
            StatementNotificationTierEnum notificationTierEnum = StatementNotificationTierEnum.Good;

            switch (agingTier)
            {
                case AgingTierEnum.NotStatemented:
                case AgingTierEnum.Good:
                    notificationTierEnum = StatementNotificationTierEnum.Good;
                    break;
                case AgingTierEnum.PastDue:
                    notificationTierEnum = StatementNotificationTierEnum.PastDue;
                    break;
                case AgingTierEnum.FinalPastDue:
                    notificationTierEnum = StatementNotificationTierEnum.FinalPastDue;
                    break;
                case AgingTierEnum.MaxAge:
                    notificationTierEnum = StatementNotificationTierEnum.Uncollectable;
                    break;
                default:
                    break;
            }

            StatementNotificationMessage statementMessage = new StatementNotificationMessage
            {
                VpGuarantorId = vpGuarantorId,
                StatementDate = statement.StatementDate.Date,
                StatementDueDate = statement.PaymentDueDate,
                StatementBalance = statement.StatementBalance,
                VpStatementId = statement.VpStatementId,
                StatementNotificationTierEnum = notificationTierEnum
            };

            return statementMessage;
        }

        public FinancePlanStatementNotificationMessage GetFpStatementNotificationMessage(int vpGuarantorId, FinancePlanStatement statement, bool isOfflineGuarantor)
        {
            if (statement == null)
            {
                return null;
            }

            decimal totalBalance = statement.DisplayStatementBalance;
            if (totalBalance <= 0)
            {
                return null;
            }

            IList<FinancePlan> statementFinancePlans = statement
                .FinancePlanStatementFinancePlans
                .Where(x => !x.FinancePlan.FinancePlanStatus.IsClosed())
                .Select(x => x.FinancePlan)
                .ToList();

            IList<FinancePlanStatus> fpStatuses = statementFinancePlans
                .Select(x => x.FinancePlanStatus).Distinct().ToList();

            int maxBucket = statement
                                .FinancePlanStatementFinancePlans
                                .Where(x => !x.FinancePlan.FinancePlanStatus.IsClosed())
                                .Select(x => x.FinancePlan)
                                .Max(x => x.CurrentBucket as int?) ?? 0;

            bool pastDue = fpStatuses.Any(x => x.FinancePlanStatusEnum == FinancePlanStatusEnum.PastDue);
            bool finalPastDue = maxBucket == this.Client.Value.UncollectableMaxAgingCountFinancePlans - 1;
            bool uncollectable = fpStatuses.Any(x => x.FinancePlanStatusEnum == FinancePlanStatusEnum.UncollectableActive);

            StatementNotificationTierEnum notificationTierEnum = StatementNotificationTierEnum.Good;
            notificationTierEnum = pastDue ? StatementNotificationTierEnum.PastDue : notificationTierEnum;
            notificationTierEnum = finalPastDue ? StatementNotificationTierEnum.FinalPastDue : notificationTierEnum;
            notificationTierEnum = uncollectable ? StatementNotificationTierEnum.Uncollectable : notificationTierEnum;

            FinancePlanStatementNotificationMessage statementMessage = new FinancePlanStatementNotificationMessage
            {
                VpGuarantorId = vpGuarantorId,
                StatementDate = statement.StatementDate.Date,
                StatementDueDate = statement.PaymentDueDate,
                StatementBalance = statement.DisplayStatementBalance,
                VpStatementId = statement.VpStatementId,
                StatementNotificationTierEnum = notificationTierEnum,
                IsOfflineGuarantor = isOfflineGuarantor,
                VisitIds = statementFinancePlans.SelectMany(c=>c.ActiveFinancePlanVisits.Select(r=>r.VisitId)).ToList()
            };

            return statementMessage;
        }

        private SendFinancePlanAdditionalBalanceEmailMessage GetFinancePlanAdditionalBalanceMessage(Guarantor guarantor)
        {
            IList<FinancePlan> allOriginatedFinancePlans = this._financePlanService.Value.GetAllOpenFinancePlansForGuarantor(guarantor);

            bool hasAnyFinancePlanWithPositiveAdjustment = allOriginatedFinancePlans
                .Where(x => !x.FinancePlanStatus.IsClosed())
                .Where(x => x.CurrentFinancePlanBalance > 0m)
                .Any(x => x.HasPositiveVisitBalanceDifference());

            if (!hasAnyFinancePlanWithPositiveAdjustment)
            {
                return null;
            }

            SendFinancePlanAdditionalBalanceEmailMessage message = new SendFinancePlanAdditionalBalanceEmailMessage
            {
                VpGuarantorId = guarantor.VpGuarantorId
            };

            return message;
        }

        private void AddNextPaymentAmountForFinancePlans(Guarantor guarantor, VpStatement newStatement)
        {
            IList<FinancePlan> financePlans = this._financePlanService.Value.GetAllOpenFinancePlansForGuarantor(guarantor);
            if (financePlans != null && financePlans.Count > 0)
            {
                foreach (FinancePlan financePlan in financePlans)
                {
                    this._financePlanService.Value.AddTheAmountDueForFinancePlan(financePlan, newStatement, "Statemented");
                }
            }
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(CreateStatementMessage message, ConsumeContext<CreateStatementMessage> consumeContext)
        {
#pragma warning disable 809
            Task.Run(() => this.CreateStatementForGuarantorAsync(message.VpGuarantorId, message.DateToProcess, message.ImmediateStatement)).Wait();
#pragma warning restore 809
        }

        public bool IsMessageValid(CreateStatementMessage message, ConsumeContext<CreateStatementMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion

        #region Jobs

        void IJobRunnerService<QueueStatementsJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.QueueAllPendingStatementGuarantors(DateTime.UtcNow.Date);
        }

        #endregion
    }
}

﻿
namespace Ivh.Application.FinanceManagement.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Domain.FinanceManagement.Interfaces;
    using Common.Dtos;
    using Common.Interfaces;

    public class PaymentMethodProviderTypeApplicationService : ApplicationService, IPaymentMethodProviderTypeApplicationService
    {
        private readonly Lazy<IPaymentMethodProviderTypeService> _pyamentMethodProviderTypeService;

        public PaymentMethodProviderTypeApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IPaymentMethodProviderTypeService> paymentMethodProviderTypeService) : base(applicationServiceCommonService)
        {
            this._pyamentMethodProviderTypeService = paymentMethodProviderTypeService;
        }

        public IList<PaymentMethodProviderTypeDto> GetPaymentMethodProviderTypes()
        {
            return Mapper.Map<List<PaymentMethodProviderTypeDto>>(this._pyamentMethodProviderTypeService.Value.GetPaymentMethodProviderTypes());
        }
    }
}

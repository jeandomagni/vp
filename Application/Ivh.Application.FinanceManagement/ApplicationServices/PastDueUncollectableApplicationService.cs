﻿namespace Ivh.Application.FinanceManagement.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Core.Common.Dtos;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Statement.Entities;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.Settings.Entities;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Jobs;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;
    using MassTransit;
    using NHibernate;

    public class PastDueUncollectableApplicationService : ApplicationService, IPastDueUncollectableApplicationService
    {
        private readonly Lazy<IFinancePlanService> _financePlanService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IVisitService> _visitService;
        private readonly Lazy<IVpStatementService> _vpStatementService;
        private readonly ISession _session;

        public PastDueUncollectableApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IVpStatementService> vpStatementService,
            Lazy<IVisitService> visitService,
            Lazy<IFinancePlanService> financePlanService,
            Lazy<IGuarantorService> guarantorService,
            ISessionContext<VisitPay> sessionContext) : base(applicationServiceCommonService)
        {
            this._vpStatementService = vpStatementService;
            this._visitService = visitService;
            this._financePlanService = financePlanService;
            this._session = sessionContext.Session;
            this._guarantorService = guarantorService;
        }

        private void ProcessActiveUncollectableNotifications(int guarantorId, DateTime dateToProcess)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                Guarantor guarantor = this._guarantorService.Value.GetGuarantor(guarantorId);
                VpStatement mostRecentStatement = this._vpStatementService.Value.GetMostRecentStatement(guarantor);
                if (mostRecentStatement == null)
                {
                    this.LoggingService.Value.Info(() => "PastDueUncollectableApplicationService.ProcessActiveUncollectableNotifications: DateToProcess: {0}, VpgId: {1}, StatementId: NULL, VpStatement is NULL.", dateToProcess, guarantorId);
                }
                else if (mostRecentStatement.UncollectableEmailSent != null && mostRecentStatement.UncollectableEmailSent.Value)
                {
                    this.LoggingService.Value.Info(() => "PastDueUncollectableApplicationService.ProcessActiveUncollectableNotifications: DateToProcess: {0}, VpgId: {1}, StatementId: {2}, VpStatement.UncollectableEmailSent indicates e-mail already sent.", dateToProcess, guarantorId, mostRecentStatement.VpStatementId);
                }
                else
                {
                    IList<Visit> visitEntitiesOnStatement = mostRecentStatement.ActiveVpStatementVisits.Select(x => x.Visit).ToList();
                    this.RemoveVisitsOnFinancePlan(visitEntitiesOnStatement);

                    if (visitEntitiesOnStatement.Any())
                    {
                        if (visitEntitiesOnStatement.Any(x => x.AgingTier == AgingTierEnum.MaxAge))
                        {
                            this._vpStatementService.Value.MarkUncollectableEmailAsSent(mostRecentStatement);
                            this.PublishUncollectableEmailMessage(mostRecentStatement.VpGuarantorId, visitEntitiesOnStatement.Where(c=>c.AgingTier == AgingTierEnum.MaxAge).Select(c=>c.VisitId).ToList());
                        }
                        else
                        {
                            this.LoggingService.Value.Info(() => "PastDueUncollectableApplicationService.ProcessActiveUncollectableNotifications: DateToProcess: {0}, VpgId: {1}, StatementId: {2}, visit(s) aging tier ({3}) exceeds AgingTierEnum.MaxAge ({4}).",
                                dateToProcess,
                                guarantorId,
                                mostRecentStatement.VpStatementId,
                                string.Join(", ", visitEntitiesOnStatement.Select(x => x.AgingTier)),
                                AgingTierEnum.MaxAge);
                        }
                    }
                }

                unitOfWork.Commit();
            }
        }

        private void ProcessClosedUncollectableFinancePlanNotifications(IList<ProcessClosedUncollectableFinancePlanNotificationMessage> processClosedUncollectableFinancePlanNotificationMessages)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                foreach (ProcessClosedUncollectableFinancePlanNotificationMessage processClosedUncollectableFinancePlanNotificationMessage in processClosedUncollectableFinancePlanNotificationMessages)
                {
                    Guarantor guarantor = this._guarantorService.Value.GetGuarantor(processClosedUncollectableFinancePlanNotificationMessage.VpGuarantorId);
                    FinancePlan financePlan = this._financePlanService.Value.GetFinancePlan(guarantor, processClosedUncollectableFinancePlanNotificationMessage.FinancePlanId);
                    if (financePlan.CanGoUncollectable(processClosedUncollectableFinancePlanNotificationMessage.DateToProcess))
                    {
                        this._financePlanService.Value.SendUncollectableMessage(financePlan);
                    }
                }
                unitOfWork.Commit();
            }
        }

        private void ProcessFinalPastDueNotifications(int guarantorId, IList<int> visitIds, DateTime dateToProcess, bool isOnFinancePlan)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                this.PublishFinalPastDueNotificationEmailMessage(guarantorId, visitIds, isOnFinancePlan);
                if (visitIds != null && visitIds.Count > 0)
                {
                    foreach (int visitId in visitIds)
                    {
                        Visit visit = this._visitService.Value.GetVisit(visitId);
                        if (visit.FinalPastDueEmailSentDate != null)
                        {
                            this.LoggingService.Value.Info(() => "PastDueUncollectableApplicationService.ProcessFinalPastDueNotifications: DateToProcess: {0}, VpgId: {1}, VisitId: {2}, Visit.FinalPastDueEmailSentDate indicates e-mail already sent {3}.", dateToProcess, guarantorId, visitId, visit.FinalPastDueEmailSentDate.Value.ToString("G"));
                        }
                        else
                        {
                            this._visitService.Value.MarkFinalPastDueEmailAsSent(visit);
                        }
                    }
                }

                if (isOnFinancePlan)
                {
                    Guarantor guarantor = this._guarantorService.Value.GetGuarantor(guarantorId);
                    VpStatement mostRecentStatement = this._vpStatementService.Value.GetMostRecentStatement(guarantor);

                    if (mostRecentStatement == null)
                    {
                        this.LoggingService.Value.Info(() => "PastDueUncollectableApplicationService.ProcessFinalPastDueNotifications: DateToProcess: {0}, VpgId: {1}, StatementId: NULL, VpStatement is NULL.", dateToProcess, guarantorId);
                    }
                    else if (mostRecentStatement.PastDueEmailSent != null && mostRecentStatement.PastDueEmailSent.Value)
                    {
                        this.LoggingService.Value.Info(() => "PastDueUncollectableApplicationService.ProcessFinalPastDueNotifications: DateToProcess: {0}, VpgId: {1}, StatementId: {2}, VpStatement.PastDueEmailSent indicates e-mail already sent.", dateToProcess, guarantorId, mostRecentStatement.VpStatementId);
                    }
                    else
                    {
                        this._vpStatementService.Value.MarkPastDueEmailAsSent(mostRecentStatement);
                    }
                }

                unitOfWork.Commit();
            }
        }

        public void QueueFinalPastDueAndActiveUncollectablesNotifications(DateTime dateToProcess)
        {
            //first, send the final payment past due notice
            this.SendFinalPastDueNotifications(dateToProcess);

            //second, send the notifications that the account is now uncollectable
            this.SendActiveUncollectableNotification(dateToProcess);
        }

        private void SendFinalPastDueNotifications(DateTime dateToProcess)
        {
            IList<FinalPastDueNotificationInfoDto> visitGuarantorsToSendEmails = this.GetFinalPastDueNotificationsToQueue(dateToProcess);
            IList<int> guarantorsToSendEmails = visitGuarantorsToSendEmails.Select(x => x.VpGuarantorId).Distinct().ToList();
            if (guarantorsToSendEmails.IsNotNullOrEmpty())
            {
                foreach (int guarantorId in guarantorsToSendEmails.Distinct())
                {

                    ProcessFinalPastDueNotificationMessage message = new ProcessFinalPastDueNotificationMessage
                    {
                        VpGuarantorId = guarantorId,
                        DateToProcess = dateToProcess,
                        VisitIds = new List<int>()
                    };

                    IList<FinalPastDueNotificationInfoDto> infos = visitGuarantorsToSendEmails.Where(x => x.VpGuarantorId == guarantorId).ToList();
                    foreach (FinalPastDueNotificationInfoDto info in infos)
                    {
                        if (info.VisitIds != null && info.VisitIds.Count > 0)
                        {
                            message.VisitIds.AddRange(info.VisitIds);
                        }

                        if (info.IsOnFinancePlan)
                        {
                            message.IsOnFinancePlan = true;
                        }
                    }

                    this.Bus.Value.PublishMessage(message).Wait();
                }
            }
        }

        private void SendActiveUncollectableNotification(DateTime dateToProcess)
        {
            Client client = this.Client.Value;
            int maxAgingCountFinancePlans = client.UncollectableMaxAgingCountFinancePlans;

            DateTime statementDueDate = dateToProcess.Date.AddDays(-client.ActiveUncollectableDays);
            IList<int> guarantorIds = this._financePlanService.Value.GetFinancePlanGuarantorIdsByBucketAndStatementDueDate(maxAgingCountFinancePlans, statementDueDate);

            IList<int> guarantorsToSendEmails = new List<int>();
            if (guarantorIds.Any())
            {
                guarantorsToSendEmails.AddRange(this._vpStatementService.Value.GuarantorsIdsWhoHaventHadUncollectableEmailForCurrentStatement(guarantorIds).ToList());
            }
            IList<PendingUncollectableStatementsResult> uncollectable = this._vpStatementService.Value.GetAllPendingUncollectableStatements(maxAgingCountFinancePlans, statementDueDate);
            guarantorsToSendEmails.AddRange(uncollectable.Select(x => x.VpGuarantorId));
            guarantorsToSendEmails.AddRange(uncollectable.Where(x => !x.UncollectableEmailSent.GetValueOrDefault(false)).Select(x => x.VpGuarantorId));
            if (guarantorsToSendEmails.Any())
            {
                foreach (int guarantor in guarantorsToSendEmails.Distinct())
                {
                    this.Bus.Value.PublishMessage(new ProcessActiveUncollectableNotificationMessage
                    {
                        VpGuarantorId = guarantor,
                        DateToProcess = dateToProcess
                    }).Wait();
                }
            }
        }

        public IList<FinalPastDueNotificationInfoDto> GetFinalPastDueNotificationsToQueue(DateTime dateToProcess)
        {
            IList<FinalPastDueNotificationInfoDto> visitGuarantorsToSendEmails = new List<FinalPastDueNotificationInfoDto>();

            IEnumerable<IGrouping<int, FinancePlan>> finalPastDueFinancePlanGuarantors = this.GetFinalPastDueFinancePlans(dateToProcess).GroupBy(x => x.VpGuarantor.VpGuarantorId, fp => fp);
            visitGuarantorsToSendEmails.AddRange(
                finalPastDueFinancePlanGuarantors.Select(x => new FinalPastDueNotificationInfoDto()
                {
                    VpGuarantorId = x.Key,
                    IsOnFinancePlan = true
                })
            );

            // Not ideal, but no easy way to ask for VisitsNotOnFinancePlan
            IList<VisitResult> visitResults = this._visitService.Value.GetFinalPastDueVisitsMissingNotification();
            IDictionary<int, int> finalPastDueVisitGuarantors = visitResults != null ? visitResults.ToDictionary(x => x.VisitId, x => x.GuarantorId) : new Dictionary<int, int>();
            if (finalPastDueVisitGuarantors.Count > 0)
            {
                IList<int> financePlanVisitIds = this._financePlanService.Value.GetVisitIdsOnActiveFinancePlans(finalPastDueVisitGuarantors.Keys.ToList()) ?? new List<int>();
                IDictionary<int, int> finalPastDueVisitGuarantorsNotFinanced = finalPastDueVisitGuarantors.Where(x => !financePlanVisitIds.Contains(x.Key)).ToDictionary(x => x.Key, x => x.Value);
                ILookup<int, int> finalPastDueLookup = finalPastDueVisitGuarantorsNotFinanced.ToLookup(x => x.Value, x => x.Key);

                visitGuarantorsToSendEmails.AddRange(
                    finalPastDueLookup.Select(g => new FinalPastDueNotificationInfoDto()
                    {
                        VpGuarantorId = g.Key,
                        IsOnFinancePlan = false,
                        VisitIds = g.Select(v => v).ToList()
                    })
                );
            }

            return visitGuarantorsToSendEmails;
        }

        private IList<FinancePlan> GetFinalPastDueFinancePlans(DateTime dateToProcess)
        {
            Client client = this.Client.Value;
            int maxAgingCountFinancePlans = client.UncollectableMaxAgingCountFinancePlans - 1;

            DateTime statementDueDate = dateToProcess.Date.AddDays(-client.FinalPastDueNotificationDaysBeforeDueDate);
            IList<int> guarantorsToSendEmails = new List<int>();

            IList<FinancePlan> financePlans = this._financePlanService.Value.GetFinancePlansByBucketAndStatementDueDate(maxAgingCountFinancePlans, statementDueDate);
            if (financePlans.IsNotNullOrEmpty())
            {
                IList<int> guarantorIds = financePlans.Select(x => x.VpGuarantor.VpGuarantorId).ToList();
                IList<int> guarantorIdsWhoHaventHadPastDueEmailForCurrentStatement = this._vpStatementService.Value.GuarantorsIdsWhoHaventHadPastDueEmailForCurrentStatement(guarantorIds);
                if (guarantorIdsWhoHaventHadPastDueEmailForCurrentStatement.IsNotNullOrEmpty())
                {
                    guarantorsToSendEmails.AddRange(guarantorIdsWhoHaventHadPastDueEmailForCurrentStatement);
                }
            }

            return financePlans.Where(x => guarantorsToSendEmails.Contains(x.VpGuarantor.VpGuarantorId)).ToList();
        }

        public void QueueClosedUncollectableNotifications(DateTime dateToProcess)
        {
            Client client = this.Client.Value;
            DateTime checkDate = dateToProcess.AddDays(-client.ClosedUncollectableDays);

            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                IReadOnlyList<FinancePlan> financePlans = this._financePlanService.Value.GetFinancePlans(FinancePlanStatusEnum.UncollectableActive.ToListOfOne());
                List<ProcessClosedUncollectableFinancePlanNotificationMessage> processClosedUncollectableFinancePlanNotificationMessages = new List<ProcessClosedUncollectableFinancePlanNotificationMessage>(financePlans.Count);
                processClosedUncollectableFinancePlanNotificationMessages.AddRange(financePlans.Select(financePlan => new ProcessClosedUncollectableFinancePlanNotificationMessage
                {
                    FinancePlanId = financePlan.FinancePlanId,
                    VpGuarantorId = financePlan.VpGuarantor.VpGuarantorId,
                    DateToProcess = checkDate,
                }));

                if (processClosedUncollectableFinancePlanNotificationMessages.IsNotNullOrEmpty())
                {
                    this._session.Transaction.RegisterPostCommitSuccessAction(p1 =>
                    {
                        this.Bus.Value.PublishMessage(new ProcessClosedUncollectableFinancePlanNotificationMessageList
                        {
                            Messages = p1
                        }).Wait();
                    }, processClosedUncollectableFinancePlanNotificationMessages);
                }
                unitOfWork.Commit();
            }
        }

        private void RemoveVisitsOnFinancePlan(IList<Visit> visitEntitiesOnStatement)
        {
            IList<Visit> visitsOnFp = this._financePlanService.Value.VisitsOnFinancePlan(visitEntitiesOnStatement);
            //Visits in Finance plan inherit their status from the FP
            foreach (Visit visitOnFp in visitsOnFp)
            {
                visitEntitiesOnStatement.Remove(visitOnFp);
            }
        }

        private void PublishFinalPastDueNotificationEmailMessage(int vpGuarantorId, IList<int> visitIds, bool isOnFinancePlan)
        {
            this._session.Transaction.RegisterPostCommitSuccessAction((p1) =>
            {
                this.Bus.Value.PublishMessage(new SendFinalPastDueNotificationEmailMessage
                {
                    VpGuarantorId = p1,
                    VisitIds = visitIds,
                    IsOnFinancePlan = isOnFinancePlan
                }).Wait();
            }, vpGuarantorId);
        }

        private void PublishUncollectableEmailMessage(int vpGuarantorId, List<int> visitIds)
        {
            this._session.Transaction.RegisterPostCommitSuccessAction((p1) =>
            {
                this.Bus.Value.PublishMessage(new SendPendingUncollectableEmailMessage
                {
                    VpGuarantorId = p1,
                    VisitIds = visitIds
                }).Wait();
            }, vpGuarantorId);
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(ProcessActiveUncollectableNotificationMessage message, ConsumeContext<ProcessActiveUncollectableNotificationMessage> consumeContext)
        {
            this.ProcessActiveUncollectableNotifications(message.VpGuarantorId, message.DateToProcess);

        }

        public bool IsMessageValid(ProcessActiveUncollectableNotificationMessage message, ConsumeContext<ProcessActiveUncollectableNotificationMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(ProcessClosedUncollectableFinancePlanNotificationMessage message, ConsumeContext<ProcessClosedUncollectableFinancePlanNotificationMessage> consumeContext)
        {
            this.ProcessClosedUncollectableFinancePlanNotifications(message.ToListOfOne());
        }

        public bool IsMessageValid(ProcessClosedUncollectableFinancePlanNotificationMessage message, ConsumeContext<ProcessClosedUncollectableFinancePlanNotificationMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(ProcessClosedUncollectableFinancePlanNotificationMessageList message, ConsumeContext<ProcessClosedUncollectableFinancePlanNotificationMessageList> consumeContext)
        {
            this.ProcessClosedUncollectableFinancePlanNotifications(message.Messages);
        }
      
        public bool IsMessageValid(ProcessClosedUncollectableFinancePlanNotificationMessageList message, ConsumeContext<ProcessClosedUncollectableFinancePlanNotificationMessageList> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(ProcessFinalPastDueNotificationMessage message, ConsumeContext<ProcessFinalPastDueNotificationMessage> consumeContext)
        {
            this.ProcessFinalPastDueNotifications(message.VpGuarantorId, message.VisitIds, message.DateToProcess, message.IsOnFinancePlan);
        }

        public bool IsMessageValid(ProcessFinalPastDueNotificationMessage message, ConsumeContext<ProcessFinalPastDueNotificationMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion

        #region Jobs

        void IJobRunnerService<QueueClosedUncollectablesJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.QueueClosedUncollectableNotifications(DateTime.UtcNow);
        }

        void IJobRunnerService<QueuePendingUncollectableNotificationsJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.QueueFinalPastDueAndActiveUncollectablesNotifications(DateTime.UtcNow.AddDays(-1));
        }

        #endregion
    }
}
﻿namespace Ivh.Application.FinanceManagement.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Interfaces;
    using Ivh.Common.Base.Constants;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;
    using MassTransit;
    using NHibernate;

    public class FinancePlanStatusApplicationService : ApplicationService, IFinancePlanStatusApplicationService
    {
        private readonly Lazy<IFinancePlanService> _financePlanService;
        private readonly Lazy<IPaymentService> _paymentService;
        private readonly ISession _session;

        public FinancePlanStatusApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            ISessionContext<VisitPay> sessionContext,
            Lazy<IPaymentService> paymentService,
            Lazy<IFinancePlanService> financePlanService) : base(applicationServiceCommonService)
        {
            this._session = sessionContext.Session;
            this._paymentService = paymentService;
            this._financePlanService = financePlanService;
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority10)]
        public void ConsumeMessage(FinancePlanChangedStatusMessage message, ConsumeContext<FinancePlanChangedStatusMessage> consumeContext)
        {
            this.UpdatePaymentsAffectedByClosingFinancePlan(message.ToListOfOne());
            this.UpdateReconfiguredFinancePlansBecauseOfUncollectable(message.ToListOfOne());
        }

        public bool IsMessageValid(FinancePlanChangedStatusMessage message, ConsumeContext<FinancePlanChangedStatusMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority10)]
        public void ConsumeMessage(FinancePlanChangedStatusMessageList message, ConsumeContext<FinancePlanChangedStatusMessageList> consumeContext)
        {
            this.UpdatePaymentsAffectedByClosingFinancePlan(message.Messages);
            this.UpdateReconfiguredFinancePlansBecauseOfUncollectable(message.Messages);
        }

        public bool IsMessageValid(FinancePlanChangedStatusMessageList message, ConsumeContext<FinancePlanChangedStatusMessageList> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion

        private void UpdatePaymentsAffectedByClosingFinancePlan(IList<FinancePlanChangedStatusMessage> messages)
        {
            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                foreach (FinancePlanChangedStatusMessage message in messages)
                {
                    if (!message.NewStatus.IsInCategory(FinancePlanStatusEnumCategory.Closed))
                    {
                        continue;
                    }

                    FinancePlan financePlan = this._financePlanService.Value.GetFinancePlanById(message.FinancePlanId);
                    if (financePlan?.CurrentFinancePlanStatus.FinancePlanStatusEnum.IsInCategory(FinancePlanStatusEnumCategory.Closed) ?? false)
                    {
                        this._paymentService.Value.RemovePendingPaymentAmountAssociatedWithFinancePlan(financePlan);
                    }
                }

                uow.Commit();
            }
        }

        private void UpdateReconfiguredFinancePlansBecauseOfUncollectable(IList<FinancePlanChangedStatusMessage> messages)
        {
            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                foreach (FinancePlanChangedStatusMessage message in messages)
                {
                    if (message.NewStatus != FinancePlanStatusEnum.UncollectableActive)
                    {
                        continue;
                    }

                    FinancePlan financePlan = this._financePlanService.Value.GetFinancePlanById(message.FinancePlanId);
                    if (financePlan?.CurrentFinancePlanStatus.FinancePlanStatusEnum != FinancePlanStatusEnum.UncollectableActive)
                    {
                        continue;
                    }

                    if (financePlan.ReconfiguredFinancePlan != null)
                    {
                        this._financePlanService.Value.CancelReconfiguredFinancePlan(financePlan.ReconfiguredFinancePlan, "Parent finance plan went active active uncollectable", SystemUsers.SystemUserId, null);
                    }
                }

                uow.Commit();
            }
        }
    }
}
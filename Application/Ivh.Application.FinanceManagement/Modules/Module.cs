﻿namespace Ivh.Application.FinanceManagement.Modules
{
    using ApplicationServices;
    using Autofac;
    using Common.Interfaces;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CreditAgreementApplicationService>().As<ICreditAgreementApplicationService>();
            builder.RegisterType<FinancePlanApplicationService>().As<IFinancePlanApplicationService>();
            builder.RegisterType<PaymentActionApplicationService>().As<IPaymentActionApplicationService>();
            builder.RegisterType<PaymentAllocationApplicationService>().As<IPaymentAllocationApplicationService>();
            builder.RegisterType<PaymentBatchOperationsApplicationService>().As<IPaymentBatchOperationsApplicationService>();
            builder.RegisterType<PaymentDetailApplicationService>().As<IPaymentDetailApplicationService>();
            builder.RegisterType<PaymentMethodsApplicationService>().As<IPaymentMethodsApplicationService>();
            builder.RegisterType<PaymentMethodAccountTypeApplicationService>().As<IPaymentMethodAccountTypeApplicationService>();
            builder.RegisterType<PaymentMethodProviderTypeApplicationService>().As<IPaymentMethodProviderTypeApplicationService>();
            builder.RegisterType<PaymentOptionApplicationService>().As<IPaymentOptionApplicationService>();
            builder.RegisterType<PaymentReversalApplicationService>().As<IPaymentReversalApplicationService>();
            builder.RegisterType<PaymentSubmissionApplicationService>().As<IPaymentSubmissionApplicationService>();
            builder.RegisterType<ReconfigureFinancePlanApplicationService>().As<IReconfigureFinancePlanApplicationService>();
            builder.RegisterType<StatementApplicationService>().As<IStatementApplicationService>();
            builder.RegisterType<PaymentQueueApplicationService>().As<IPaymentQueueApplicationService>();
        }
    }
}
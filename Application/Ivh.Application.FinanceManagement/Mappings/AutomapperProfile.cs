﻿namespace Ivh.Application.FinanceManagement.Mappings
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Core.Common.Dtos;
    using Common.Dtos;
    using Domain.FinanceManagement.Ach.Entities;
    using Domain.FinanceManagement.Consolidation.Entities;
    using Domain.FinanceManagement.Discount.Entities;
    using Domain.FinanceManagement.Entities;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.PaymentSummary.Entities;
    using Domain.FinanceManagement.ServiceGroup.Entities;
    using Domain.FinanceManagement.Statement.Entities;
    using Domain.Guarantor.Entities;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;

    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            // Automapper mapping go here.  
            // Use this.CreateMap<,>().
            // DO NOT USE Mapper.CreateMap<,>().

            this.CreateMap<FinancePlan, FinancePlanDto>()
                .ForMember(dest => dest.InterestPaidToDate, opts => opts.MapFrom(src => src.InterestPaidForDate(DateTime.UtcNow)))
                .ForMember(dest => dest.PrincipalPaidToDate, opts => opts.MapFrom(src => src.PrincipalPaidForDate(DateTime.UtcNow)))
                .ForMember(dest => dest.IsEligibleForReconfiguration, opts => opts.MapFrom(src => src.IsEligibleForReconfiguration()))
                .ForMember(dest => dest.ReconfiguredFinancePlanId, opts => opts.MapFrom(src => src.ReconfiguredFinancePlan != null ? src.ReconfiguredFinancePlan.FinancePlanId : (int?)null))
                .ForMember(dest => dest.OriginalFinancePlanId, opts => opts.MapFrom(src => src.OriginalFinancePlan != null ? src.OriginalFinancePlan.FinancePlanId : (int?)null))
                .ForMember(dest => dest.IsCanceled, opts => opts.MapFrom(src => src.IsCanceled()))
                .ForMember(dest => dest.IsPending, opts => opts.MapFrom(src => src.IsPending()))
                .ForMember(dest => dest.IsPastDue, opts => opts.MapFrom(src => src.IsPastDue()))
                .ForMember(dest => dest.IsActiveUncollectable, opts => opts.MapFrom(src => src.IsActiveUncollectable()))
                .ForMember(dest => dest.IsUncollectable, opts => opts.MapFrom(src => src.IsUncollectable()))
                .ForMember(dest => dest.MaxScheduledPaymentDate, opts => opts.MapFrom(src => src.MaxScheduledPaymentDate()))
                .ForMember(dest => dest.IsRetailInstallmentContract, opts => opts.MapFrom(src => src.IsRetailInstallmentContract()));

            this.CreateMap<FinancePlanDto, FinancePlan>()
                .ForMember(dest => dest.FinancePlanInterestRateHistory, opts => opts.Ignore());

            this.CreateMap<FinancePlanVisit, FinancePlanVisitDto>()
                .ForMember(dest => dest.FacilityId, opts => opts.MapFrom(src => src.FinancePlanVisitVisit != null ? src.FinancePlanVisitVisit.FacilityId : (int?)null));
            this.CreateMap<FinancePlanVisitDto, FinancePlanVisit>();

            this.CreateMap<FinancePlanOffer, FinancePlanOfferDto>();
            this.CreateMap<FinancePlanOfferDto, FinancePlanOffer>();

            this.CreateMap<FinancePlanFilterDto, FinancePlanFilter>();

            this.CreateMap<FinancePlanResults, FinancePlanResultsDto>();

            this.CreateMap<FinancePlanStatus, FinancePlanStatusDto>();
            this.CreateMap<FinancePlanStatusDto, FinancePlanStatus>();

            this.CreateMap<FinancePlanType, FinancePlanTypeDto>()
                .ForMember(dest => dest.Name, opts => opts.MapFrom(src => src.FinancePlanTypeDisplayName));

            this.CreateMap<FinancePlanStatusHistory, FinancePlanStatusHistoryDto>();
            this.CreateMap<FinancePlanStatusHistoryDto, FinancePlanStatusHistory>();

            this.CreateMap<FinancePlanInterestRateHistory, FinancePlanInterestRateHistoryDto>();

            this.CreateMap<FinancePlanVisitPrincipalAmount, FinancePlanLogDto>()
                .ForMember(dest => dest.Amount, opts => opts.MapFrom(src => src.Amount))
                .ForMember(dest => dest.InsertDate, opts => opts.MapFrom(src => src.InsertDate))
                .ForMember(dest => dest.VisitId, opts => opts.MapFrom(src => src.FinancePlanVisit.VisitId))
                .ForMember(dest => dest.SourceSystemKey, opts => opts.MapFrom(src => src.FinancePlanVisit.SourceSystemKey))
                .ForMember(dest => dest.BillingApplication, opts => opts.MapFrom(src => src.FinancePlanVisit.BillingApplication))
                .ForMember(dest => dest.VisitBalanceAtTimeOfAssessement, opts => opts.Ignore())
                .ForMember(dest => dest.InterestRateUsedForAssessement, opts => opts.Ignore())
                .ForMember(dest => dest.FinancePlanVisitInterestDueType, opts => opts.Ignore())
                .ForMember(dest => dest.FinancePlanLogType, opts => opts.MapFrom(src => GetFinancePlanLogTypePrincipal(src.Amount, src.FinancePlanVisitPrincipalAmountType)));

            this.CreateMap<FinancePlanVisitInterestDue, FinancePlanLogDto>()
                .ForMember(dest => dest.Amount, opts => opts.MapFrom(src => src.InterestDue))
                .ForMember(dest => dest.InsertDate, opts => opts.MapFrom(src => src.OverrideInsertDate ?? src.InsertDate))
                .ForMember(dest => dest.VisitId, opts => opts.MapFrom(src => src.FinancePlanVisit.VisitId))
                .ForMember(dest => dest.SourceSystemKey, opts => opts.MapFrom(src => src.FinancePlanVisit.SourceSystemKey))
                .ForMember(dest => dest.BillingApplication, opts => opts.MapFrom(src => src.FinancePlanVisit.BillingApplication))
                .ForMember(dest => dest.VisitBalanceAtTimeOfAssessement, opts => opts.MapFrom(src => src.CurrentBalanceAtTimeOfAssessment))
                .ForMember(dest => dest.InterestRateUsedForAssessement, opts => opts.MapFrom(src => src.InterestRateUsedToAssess))
                .ForMember(dest => dest.FinancePlanLogType, opts => opts.MapFrom(src => GetFinancePlanLogTypeInterest(src.InterestDue, src.FinancePlanVisitInterestDueType)));

            this.CreateMap<FinancePlanOfferSetType, FinancePlanOfferSetTypeDto>();
            this.CreateMap<FinancePlanOfferSetTypeDto, FinancePlanOfferSetType>();

            this.CreateMap<Payment, PaymentDto>()
                .ForMember(dest => dest.ParentPaymentId, opts => opts.MapFrom(src => src.ParentPayment.PaymentId));

            this.CreateMap<PaymentDto, Payment>()
                .ForMember(dest => dest.ParentPayment, opts => opts.MapFrom(src => src.ParentPaymentId.HasValue ? new Payment() { PaymentId = src.ParentPaymentId.Value } : null));

            this.CreateMap<PaymentActionReason, PaymentActionReasonDto>();

            this.CreateMap<PaymentAllocation, PaymentAllocationDto>();
            this.CreateMap<PaymentAllocationDto, PaymentAllocation>();

            this.CreateMap<PaymentHistoryDto, PaymentDetailDto>();

            this.CreateMap<PaymentEvent, PaymentEventDto>();

            this.CreateMap<PaymentMethodBillingAddress, PaymentMethodBillingAddressDto>();
            this.CreateMap<PaymentMethodBillingAddressDto, PaymentMethodBillingAddress>();

            this.CreateMap<PaymentScheduledAmount, PaymentScheduledAmountDto>();
            this.CreateMap<PaymentScheduledAmountDto, PaymentScheduledAmount>();

            this.CreateMap<PaymentFinancePlan, PaymentFinancePlanDto>();
            this.CreateMap<PaymentFinancePlanDto, PaymentFinancePlan>();

            this.CreateMap<PaymentFilterDto, PaymentFilter>();

            this.CreateMap<PaymentProcessorResponseFilterDto, PaymentProcessorResponseFilter>();

            this.CreateMap<PaymentMethod, PaymentMethodDto>()
                .ForMember(dest => dest.VpGuarantorId, opts => opts.MapFrom(src => src.VpGuarantorId));

            this.CreateMap<PaymentMethodDto, PaymentMethod>()
                .ForMember(dest => dest.VpGuarantorId, opts => opts.MapFrom(src => src.VpGuarantorId));

            this.CreateMap<PaymentMethodAccountType, PaymentMethodAccountTypeDto>();
            this.CreateMap<PaymentMethodAccountTypeDto, PaymentMethodAccountType>();

            this.CreateMap<PaymentMethodProviderType, PaymentMethodProviderTypeDto>();
            this.CreateMap<PaymentMethodProviderTypeDto, PaymentMethodProviderType>();

            this.CreateMap<PaymentMethodResponse, PaymentMethodResultDto>()
                .ConstructUsing(src => new PaymentMethodResultDto(src.IsSuccess, src.ErrorMessage, src.PrimaryChanged, Mapper.Map<PaymentMethodDto>(src.PaymentMethod)));

            this.CreateMap<PaymentProcessorResponse, PaymentProcessorResponseDto>();

            this.CreateMap<PaymentSubmissionDto, PaymentSubmissionDto>(); // self mapping used to clone in PaymentSubmissionApplicationService.CreatePaymentFromPaymentDto

            this.CreateMap<VpStatementSnapshotVp2, StatementDto>();
            this.CreateMap<VpStatementSnapshotVp3V1, StatementDto>();
            this.CreateMap<VpStatement, StatementDto>()
                .ForMember(dest => dest.Visits, opts => opts.MapFrom(src => src.VpStatementVisits))
                .ForMember(dest => dest.VisitsNotOnFinancePlan, opts => opts.MapFrom(src => src.VpStatementVisitsNotOnFinancePlan))
                .ForMember(dest => dest.ActiveVisitsNotOnFinancePlan, opts => opts.MapFrom(src => src.ActiveVpStatementVisitsNotOnFinancePlan))
                .ForMember(dest => dest.Notifications, opts => opts.MapFrom(src => src.VpStatementNotifications.Select(x => x.NotificationText).ToList()))
                .AfterMap((src, dest) =>
                {
                    switch (src.StatementVersion)
                    {
                        case VpStatementVersionEnum.Vp2:
                            VpStatementSnapshotVp2 snapv2 = (VpStatementSnapshotVp2)src.GetSnapshotObject();
                            Mapper.Map(snapv2, dest);
                            break;
                        case VpStatementVersionEnum.Vp3V1:
                            VpStatementSnapshotVp3V1 snapv3 = (VpStatementSnapshotVp3V1)src.GetSnapshotObject();
                            Mapper.Map(snapv3, dest);
                            break;
                    }
                });

            this.CreateMap<StatementDto, VpStatement>();

            this.CreateMap<FinancePlanStatementSnapshotVp2, FinancePlanStatementDto>();
            this.CreateMap<FinancePlanStatementSnapshotVp2, StatementDto>();
            this.CreateMap<FinancePlanStatementSnapshotVp3V1, FinancePlanStatementDto>();
            this.CreateMap<FinancePlanStatementSnapshotVp3V1, StatementDto>();
            this.CreateMap<FinancePlanStatement, FinancePlanStatementDto>()
                .ForMember(dest => dest.Notifications, opts => opts.MapFrom(src => src.FinancePlanStatementNotifications.Select(x => x.NotificationText)))
                .AfterMap((src, dest) =>
                {
                    switch (src.StatementVersion)
                    {
                        case VpStatementVersionEnum.Vp2:
                            FinancePlanStatementSnapshotVp2 snapv2 = (FinancePlanStatementSnapshotVp2)src.GetSnapshotObject();
                            Mapper.Map(snapv2, dest);
                            break;
                        case VpStatementVersionEnum.Vp3V1:
                            FinancePlanStatementSnapshotVp3V1 snapv3 = (FinancePlanStatementSnapshotVp3V1)src.GetSnapshotObject();
                            Mapper.Map(snapv3, dest);
                            break;
                    }
                });
            this.CreateMap<FinancePlanStatement, StatementDto>()
                .ForMember(dest => dest.FinancePlans, opts => opts.MapFrom(src => src.FinancePlanStatementFinancePlans))
                .ForMember(dest => dest.Notifications, opts => opts.MapFrom(src => src.FinancePlanStatementNotifications.Select(x => x.NotificationText)))
                .AfterMap((src, dest) =>
                {
                    switch (src.StatementVersion)
                    {
                        case VpStatementVersionEnum.Vp2:
                            FinancePlanStatementSnapshotVp2 snapv2 = (FinancePlanStatementSnapshotVp2)src.GetSnapshotObject();
                            Mapper.Map(snapv2, dest);
                            break;
                        case VpStatementVersionEnum.Vp3V1:
                            FinancePlanStatementSnapshotVp3V1 snapv3 = (FinancePlanStatementSnapshotVp3V1)src.GetSnapshotObject();
                            Mapper.Map(snapv3, dest);
                            break;
                    }
                });

            //Need different way to map for different versions... cant assume vp2
            //Might be better way to do this...  [EnumCategoryThing(typeof(thetype)] => grab type from enum, do map. - Mike
            this.CreateMap<FinancePlanStatementFinancePlanSnapshotVp2, StatementFinancePlanDto>();
            this.CreateMap<FinancePlanStatementFinancePlanSnapshotVp2, FinancePlanStatementFinancePlanDto>();
            this.CreateMap<FinancePlanStatementFinancePlanSnapshotVp3V1, StatementFinancePlanDto>();
            this.CreateMap<FinancePlanStatementFinancePlanSnapshotVp3V1, FinancePlanStatementFinancePlanDto>();
            this.CreateMap<FinancePlanStatementFinancePlan, StatementFinancePlanDto>()
                .AfterMap((src, dest) =>
                {
                    if (src.FinancePlanStatement == null)
                    {
                        return;
                    }
                    switch (src.FinancePlanStatement.StatementVersion)
                    {
                        case VpStatementVersionEnum.Vp2:
                            FinancePlanStatementFinancePlanSnapshotVp2 snapv2 = (FinancePlanStatementFinancePlanSnapshotVp2)src.GetSnapshotObject();
                            Mapper.Map(snapv2, dest);
                            break;
                        case VpStatementVersionEnum.Vp3V1:
                            FinancePlanStatementFinancePlanSnapshotVp3V1 snapv3 = (FinancePlanStatementFinancePlanSnapshotVp3V1)src.GetSnapshotObject();
                            Mapper.Map(snapv3, dest);
                            break;
                    }
                });
            this.CreateMap<FinancePlanStatementFinancePlan, FinancePlanStatementFinancePlanDto>()
                .ForMember(dest => dest.FinancePlanId, opts => opts.MapFrom(src => src.FinancePlan.FinancePlanId))
                .AfterMap((src, dest) =>
                {
                    if (src.FinancePlanStatement == null)
                    {
                        return;
                    }
                    switch (src.FinancePlanStatement.StatementVersion)
                    {
                        case VpStatementVersionEnum.Vp2:
                            FinancePlanStatementFinancePlanSnapshotVp2 snapv2 = (FinancePlanStatementFinancePlanSnapshotVp2)src.GetSnapshotObject();
                            Mapper.Map(snapv2, dest);
                            break;
                        case VpStatementVersionEnum.Vp3V1:
                            FinancePlanStatementFinancePlanSnapshotVp3V1 snapv3 = (FinancePlanStatementFinancePlanSnapshotVp3V1)src.GetSnapshotObject();
                            Mapper.Map(snapv3, dest);
                            break;
                    }
                });

            this.CreateMap<FinancePlanStatementVisitSnapshotVp2, FinancePlanStatementVisitDto>();
            this.CreateMap<FinancePlanStatementVisitSnapshotVp2, StatementVisitDto>();
            this.CreateMap<FinancePlanStatementVisitSnapshotVp3V1, FinancePlanStatementVisitDto>();
            this.CreateMap<FinancePlanStatementVisitSnapshotVp3V1, StatementVisitDto>();

            this.CreateMap<FinancePlanStatementVisit, StatementVisitDto>()
                .AfterMap((src, dest) =>
                {
                    if (src.FinancePlanStatement == null)
                    {
                        return;
                    }
                    switch (src.FinancePlanStatement.StatementVersion)
                    {
                        case VpStatementVersionEnum.Vp2:
                            FinancePlanStatementVisitSnapshotVp2 snapv2 = (FinancePlanStatementVisitSnapshotVp2)src.GetSnapshotObject();
                            Mapper.Map(snapv2, dest);
                            break;
                        case VpStatementVersionEnum.Vp3V1:
                            FinancePlanStatementVisitSnapshotVp3V1 snapv3 = (FinancePlanStatementVisitSnapshotVp3V1)src.GetSnapshotObject();
                            Mapper.Map(snapv3, dest);
                            break;
                    }
                });
            this.CreateMap<FinancePlanStatementVisit, FinancePlanStatementVisitDto>()
                .ForMember(dest => dest.FinancePlanId, opts => opts.MapFrom(src => src.StatementedSnapshotFinancePlanId.Value))
                .AfterMap((src, dest) =>
                {
                    if (src.FinancePlanStatement == null)
                    {
                        return;
                    }
                    switch (src.FinancePlanStatement.StatementVersion)
                    {
                        case VpStatementVersionEnum.Vp2:
                            FinancePlanStatementVisitSnapshotVp2 snapv2 = (FinancePlanStatementVisitSnapshotVp2)src.GetSnapshotObject();
                            Mapper.Map(snapv2, dest);
                            break;
                        case VpStatementVersionEnum.Vp3V1:
                            FinancePlanStatementVisitSnapshotVp3V1 snapv3 = (FinancePlanStatementVisitSnapshotVp3V1)src.GetSnapshotObject();
                            Mapper.Map(snapv3, dest);
                            break;
                    }
                });

            this.CreateMap<VisitDto, FinancePlanStatementVisitDto>();

            this.CreateMap<VpStatementVisitSnapshotVp2, StatementVisitDto>();
            this.CreateMap<VpStatementVisitSnapshotVp3V1, StatementVisitDto>();
            this.CreateMap<VpStatementVisit, StatementVisitDto>()
                .AfterMap((src, dest) =>
                {
                    if (src.VpStatement == null)
                    {
                        return;
                    }

                    switch (src.VpStatement.StatementVersion)
                    {
                        case VpStatementVersionEnum.Vp2:
                            VpStatementVisitSnapshotVp2 snapv2 = (VpStatementVisitSnapshotVp2)src.GetSnapshotObject();
                            Mapper.Map(snapv2, dest);
                            break;
                        case VpStatementVersionEnum.Vp3V1:
                            VpStatementVisitSnapshotVp3V1 snapv3 = (VpStatementVisitSnapshotVp3V1)src.GetSnapshotObject();
                            Mapper.Map(snapv3, dest);
                            break;
                    }
                });

            this.CreateMap<FinancePlanMinimumPaymentTier, FinancePlanMinimumPaymentTier>();
            this.CreateMap<InterestRateDto, InterestRate>();
            this.CreateMap<InterestRate, InterestRateDto>();

            this.CreateMap<GuarantorDto, GuarantorWithLastStatementDto>()
                .ForMember(dest => dest.LastStatement, opts => opts.Ignore())
                .ForMember(dest => dest.Guarantor, opts => opts.MapFrom(x => x));

            this.CreateMap<GuarantorDto, GuarantorWithLastFinancePlanStatementDto>()
                .ForMember(dest => dest.LastFinancePlanStatement, opts => opts.Ignore())
                .ForMember(dest => dest.Guarantor, opts => opts.MapFrom(x => x));

            this.CreateMap<PaymentMethodResponse, DeletePaymentMethodResponseDto>();

            this.CreateMap<ConsolidationMatchRequestDto, ConsolidationMatchRequest>();
            this.CreateMap<ConsolidationMatchResponse, ConsolidationMatchResponseDto>();

            this.CreateMap<AchFileDto, AchFile>();
            this.CreateMap<AchFile, AchFileDto>();

            this.CreateMap<AchReturnDetail, AchReturnDetailDto>();

            this.CreateMap<AchReturnDetailDto, AchReturnDetail>();

            this.CreateMap<AchSettlementDetail, AchSettlementDetailDto>();
            this.CreateMap<AchSettlementDetailDto, AchSettlementDetail>();

            this.CreateMap<DiscountOffer, DiscountOfferDto>();
            this.CreateMap<DiscountVisitOffer, DiscountVisitOfferDto>();

            this.CreateMap<IList<Payment>, PaymentHistoryDto>()
                .ForMember(dest => dest.NetPaymentAmount, opts => opts.MapFrom(src => src.Sum(x => x.NetPaymentAmount)))
                .ForMember(dest => dest.ParentPaymentProcessorResponseId, opts => opts.MapFrom(src => src.First().ParentPayment != null ? src.First().ParentPayment.PaymentPaymentProcessorResponses.OrderByDescending(x => x.InsertDate).First().PaymentProcessorResponse.PaymentProcessorResponseId : (int?)null))
                .ForMember(dest => dest.IsReversalPaymentType, opts => opts.MapFrom(src => src.First().IsReversalPaymentType()))
                .ForMember(dest => dest.PaymentIds, opts => opts.MapFrom(src => src.Select(x => x.PaymentId).ToList()))
                .ForMember(dest => dest.PaymentStatus, opts => opts.MapFrom(src => src.Select(x => x.PaymentStatus).First()))
                .ForMember(dest => dest.PaymentType, opts => opts.MapFrom(src => src.Select(x => x.PaymentType).First()))
                .ForMember(dest => dest.UnallocatedAmountSum, opts => opts.MapFrom(src => src.SelectMany(x => x.PaymentAllocations).Sum(x => x.UnallocatedAmount)))
                .ForMember(dest => dest.GuarantorDto, opts => opts.MapFrom(src => src.Select(x => x.Guarantor).FirstOrDefault()))
                .ForMember(dest => dest.HasRemovedVisits, opts => opts.MapFrom(src => src.SelectMany(x => x.PaymentAllocations.Select(z => z.PaymentVisit)).Any(x => x.IsUnmatched)));

            this.CreateMap<ConsolidationGuarantor, ConsolidationGuarantorDto>()
                .ForMember(dest => dest.IsActive, opts => opts.MapFrom(src => src.IsPendingOrActive()))
                .ForMember(dest => dest.IsPending, opts => opts.MapFrom(src => src.IsPending()))
                .ForMember(dest => dest.IsInactive, opts => opts.MapFrom(src => src.IsInactive()));
            this.CreateMap<ConsolidationGuarantorDto, ConsolidationGuarantor>();

            this.CreateMap<PaymentStatus, PaymentStatusDto>();

            this.CreateMap<FinancePlanCount, FinancePlanCountDto>();

            this.CreateMap<PaymentOption, UserPaymentOptionDto>();

            this.CreateMap<PaymentMenuItem, PaymentMenuItemDto>()
                .ForMember(dest => dest.CmsVersion, opts => opts.Ignore())
                .ForMember(dest => dest.PaymentMenuItems, opts => opts.Ignore())
                .ForMember(dest => dest.UserPaymentOption, opts => opts.Ignore())
                .ForMember(dest => dest.ParentPaymentMenuItemId, opts => opts.MapFrom(src => src.ParentPaymentMenuItem == null ? (int?)null : src.ParentPaymentMenuItem.PaymentMenuItemId));

            this.CreateMap<PaymentVisit, PaymentVisitDto>();
            this.CreateMap<PaymentVisitDto, PaymentVisit>();

            this.CreateMap<PaymentSummaryTransaction, PaymentSummaryTransactionDto>();
            this.CreateMap<PaymentSummary, Ivh.Common.VisitPay.Messages.HospitalData.Dtos.PaymentSummaryDto>();

            this.CreateMap<ServiceGroup, ServiceGroupDto>()
                .ForMember(dest => dest.Description, opts => opts.Ignore())
                .ForMember(dest => dest.StatementDisclaimer, opts => opts.Ignore());

            this.CreateMap<PaymentImportFilterDto, PaymentImportFilter>();
            this.CreateMap<PaymentImport, PaymentImportDto>()
                .ForMember(dest => dest.Payment, opts => opts.MapFrom(src => Mapper.Map<PaymentDto>(src.Payment)))
                .ForMember(dest => dest.Guarantor, opts => opts.MapFrom(src => Mapper.Map<GuarantorDto>(src.VpGuarantor)));
        }

        public static string GetFinancePlanLogTypePrincipal(decimal amount, FinancePlanVisitPrincipalAmountTypeEnum financePlanVisitPrincipalAmountType)
        {
            if (financePlanVisitPrincipalAmountType == FinancePlanVisitPrincipalAmountTypeEnum.Payment && amount > 0)
            {
                return $"{financePlanVisitPrincipalAmountType.GetDescription()} Reversal";
            }
            return financePlanVisitPrincipalAmountType.GetDescription();
        }

        public static string GetFinancePlanLogTypeInterest(decimal amount, FinancePlanVisitInterestDueTypeEnum financePlanVisitInterestDueTypeEnum)
        {
            if (financePlanVisitInterestDueTypeEnum == FinancePlanVisitInterestDueTypeEnum.InterestPayment && amount > 0)
            {
                return $"{financePlanVisitInterestDueTypeEnum.GetDescription()} Reversal";
            }
            return financePlanVisitInterestDueTypeEnum.GetDescription();
        }
    }
}
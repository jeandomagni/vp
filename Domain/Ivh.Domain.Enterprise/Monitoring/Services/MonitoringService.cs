﻿namespace Ivh.Domain.Enterprise.Monitoring.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Entities;
    using Interfaces;

    public class MonitoringService : DomainService, IMonitoringService
    {
        private readonly Lazy<IPublishingService> _publishingService;
        private readonly Lazy<IGaugeRepository> _gaugeRepository;
        private readonly Lazy<IGaugeProvider> _gaugeProvider;

        public MonitoringService(
            Lazy<IDomainServiceCommonService> domainServiceCommonService,
            Lazy<IPublishingService> publishingService,
            Lazy<IGaugeRepository> gaugeRepository,
            Lazy<IGaugeProvider> gaugeProvider) : base(domainServiceCommonService)
        {
            this._publishingService = publishingService;
            this._gaugeRepository = gaugeRepository;
            this._gaugeProvider = gaugeProvider;
        }

        public void PublishGauge(string gaugeName)
        {
            Gauge gauge = this._gaugeRepository.Value.GetByName(gaugeName);
            decimal gaugeValue = this._gaugeProvider.Value.RunGauge(gauge);
            this.PublishGauge(gauge, () => gaugeValue);
        }

        public void PublishGauges(IEnumerable<string> gaugeNames)
        {
            IList<Gauge> gauges = this._gaugeRepository.Value.GetByNames(gaugeNames).ToList();
            if (gauges.IsNotNullOrEmpty())
            {
                this.PublishGaugesInternal(gauges);
            }
        }

        public void PublishGauge(string gaugeName, Func<decimal> gaugeValue)
        {
            this._publishingService.Value.PublishGauge(gaugeName, gaugeValue);
        }

        public void PublishGauges<T>(IDictionary<string, Func<decimal>> gauges)
        {
            foreach (KeyValuePair<string, Func<decimal>> gauge in gauges)
            {
                this._publishingService.Value.PublishGauge(gauge.Key, gauge.Value);
            }
        }

        public void PublishGaugeSet(string gaugeSetName)
        {
            IList<Gauge> gauges = this._gaugeRepository.Value.GetBySetName(gaugeSetName).ToList();
            if (gauges.IsNotNullOrEmpty())
            {
                this.PublishGaugesInternal(gauges);
            }
            else
            {
                this.LoggingService.Value.Warn(() =>  "Requested Gauge Set {0} not found in repository.", gaugeSetName);
            }
        }

        private void PublishGaugesInternal(IList<Gauge> gauges)
        {
            IDictionary<Gauge, decimal> gaugeResults = this._gaugeProvider.Value.RunGauges(gauges, true);
            if (gaugeResults.Count != gauges.Count())
            {
                // Warn of Missing Gauges
                foreach (Gauge gauge in gauges.Where(x => !gaugeResults.ContainsKey(x)))
                {
                    this.LoggingService.Value.Warn(() =>  $"Requested Gauge: {gauge.GaugeName} in GaugeSet: {gauge.GaugeSetName ?? "n/a"} failed to load.");
                }
            }

            foreach (KeyValuePair<Gauge, decimal> gaugeResult in gaugeResults)
            {
                this.PublishGauge(gaugeResult.Key, () => gaugeResult.Value);
            }
        }

        public void PublishGaugeSets(IEnumerable<string> gaugeSetNames)
        {
            foreach (string gaugeSetName in gaugeSetNames)
            {
                this.PublishGaugeSet(gaugeSetName);
            }
        }

        private void PublishGauge(Gauge gauge, Func<decimal> value)
        {
            if (gauge != null)
            {
                try
                {
                    this._publishingService.Value.PublishGauge(gauge.GaugeName, value());
                }
                catch (Exception ex)
                {
                    this.LoggingService.Value.Fatal(() => "Requested Gauge {0} unable to execute. Exception {1}.", gauge.GaugeName, ExceptionHelper.AggregateExceptionToString(ex));
                }
            }
        }
    }
}

﻿namespace Ivh.Domain.Enterprise.Monitoring.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Interfaces;
    using Logging.Interfaces;

    public class PublishingService : DomainService, IPublishingService
    {
        private readonly Lazy<IMetricsProvider> _metricsProvider;

        public PublishingService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IMetricsProvider> metricsProvider) : base(serviceCommonService)
        {
            this._metricsProvider = metricsProvider;
        }

        public void PublishGauge<T>(string gaugeName, T gaugeValue)
        {
            this._metricsProvider.Value.Gauge<T>(gaugeName, gaugeValue);
        }

        public void PublishGauges<T>(IDictionary<string, T> gauges)
        {
            foreach (KeyValuePair<string, T> gauge in gauges.AsParallel())
            {
                this._metricsProvider.Value.Gauge<T>(gauge.Key, gauge.Value);
            }
        }
    }
}

﻿namespace Ivh.Domain.Enterprise.Monitoring.Entities
{
    public class Gauge
    {
        public virtual int GaugeId { get; set; }
        public virtual string GaugeName { get; set; }
        public virtual string GaugeSetName { get; set; }
        public virtual string ConnectionStringKey { get; set; }
        public virtual string CommandText { get; set; }
        public virtual int? CommandTimeout { get; set; }
    }
}
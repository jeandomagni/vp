﻿namespace Ivh.Domain.Enterprise.Monitoring.Modules
{
    using Autofac;
    using Interfaces;
    using Services;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<PublishingService>().As<IPublishingService>();
            builder.RegisterType<MonitoringService>().As<IMonitoringService>();
        }
    }
}

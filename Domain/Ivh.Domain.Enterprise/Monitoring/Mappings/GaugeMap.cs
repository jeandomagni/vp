﻿namespace Ivh.Domain.Enterprise.Monitoring.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class GaugeMap : ClassMap<Gauge>
    {
        public GaugeMap()
        {
            this.Schema("dbo");
            this.Table("Gauge");
            this.Id(x => x.GaugeId);
            this.Map(x => x.GaugeName).Not.Nullable();
            this.Map(x => x.GaugeSetName).Not.Nullable();
            this.Map(x => x.ConnectionStringKey).Not.Nullable();
            this.Map(x => x.CommandText).Not.Nullable();
            this.Map(x => x.CommandTimeout).Nullable();
            this.Cache.ReadOnly();
        }
    }
}

﻿namespace Ivh.Domain.Enterprise.Monitoring.Interfaces
{
    using System.Collections.Generic;
    using Entities;

    public interface IGaugeProvider
    {
        decimal RunGauge(Gauge gauge);
        IDictionary<Gauge, decimal> RunGauges(IEnumerable<Gauge> gauges, bool continueAfterFailure = false);
    }
}

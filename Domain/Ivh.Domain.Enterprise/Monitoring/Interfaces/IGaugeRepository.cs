﻿namespace Ivh.Domain.Enterprise.Monitoring.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IGaugeRepository : IRepository<Gauge>
    {
        Gauge GetByName(string gaugeName);
        IEnumerable<Gauge> GetByNames(IEnumerable<string> gaugeNames);
        IEnumerable<Gauge> GetBySetName(string gaugeSetName);
        IEnumerable<Gauge> GetBySetNames(IEnumerable<string> gaugeSetNames);
    }
}

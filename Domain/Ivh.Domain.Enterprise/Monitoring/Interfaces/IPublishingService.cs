﻿namespace Ivh.Domain.Enterprise.Monitoring.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;

    public interface IPublishingService : IDomainService
    {
        void PublishGauge<T>(string gaugeName, T gaugeValue);
        void PublishGauges<T>(IDictionary<string, T> gauges);
    }
}

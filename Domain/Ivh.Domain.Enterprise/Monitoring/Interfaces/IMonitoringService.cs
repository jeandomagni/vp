﻿namespace Ivh.Domain.Enterprise.Monitoring.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Interfaces;

    public interface IMonitoringService : IDomainService
    {
        void PublishGauge(string gaugeName);
        void PublishGauges(IEnumerable<string> gaugeNames);
        void PublishGauge(string gaugeName, Func<decimal> gaugeValue);
        void PublishGauges<T>(IDictionary<string, Func<decimal>> gauges);
        void PublishGaugeSet(string gaugeSetName);
        void PublishGaugeSets(IEnumerable<string> gaugeSetNames);
    }
}

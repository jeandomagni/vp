﻿namespace Ivh.Domain.Enterprise.Api.Services
{
    using System;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;

    public class ApiService : DomainService, IApiService
    {
        private readonly Lazy<IApplicationRegistrationRepository> _applicationRegistrationRepository;

        public ApiService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IApplicationRegistrationRepository> applicationRegistrationRepository) : base(serviceCommonService)
        {
            this._applicationRegistrationRepository = applicationRegistrationRepository;
        }

        public ApplicationRegistration GetApplicationRegistration(ApiEnum api, string appId)
        {
            return this._applicationRegistrationRepository.Value.GetApplicationRegistration(api, appId);
        }
    }
}

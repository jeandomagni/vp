﻿namespace Ivh.Domain.Enterprise.Api.Entities
{
    using System;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class ApplicationRegistration
    {
        public ApplicationRegistration()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int ApplicationRegistrationId { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual ApiEnum Api { get; set; }
        public virtual string ApplicationName { get; set; }
        public virtual string AppId { get; set; }
        public virtual string AppKey { get; set; }
        public virtual bool Disabled { get; set; }
        public virtual DateTime ValidFrom { get; set; }
        public virtual DateTime ValidUntil { get; set; }
    }
}

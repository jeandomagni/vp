﻿namespace Ivh.Domain.Enterprise.Api.Interfaces
{
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IApplicationRegistrationRepository : IRepository<ApplicationRegistration>
    {
        ApplicationRegistration GetApplicationRegistration(ApiEnum api, string appId);
    }
}

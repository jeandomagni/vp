﻿namespace Ivh.Domain.Enterprise.Api.Modules
{
    using Autofac;
    using Interfaces;
    using Services;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ApiService>().As<IApiService>();
        }
    }
}

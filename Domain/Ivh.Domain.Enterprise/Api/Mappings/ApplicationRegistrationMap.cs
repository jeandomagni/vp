﻿namespace Ivh.Domain.Enterprise.Api.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class ApplicationRegistrationMap : ClassMap<ApplicationRegistration>
    {
        public ApplicationRegistrationMap()
        {
            this.Schema("Api");
            this.Table("ApplicationRegistration");
            this.Id(x => x.ApplicationRegistrationId);
            this.Map(x => x.InsertDate);
            this.Map(x => x.ApplicationName).Not.Nullable();
            this.Map(x => x.Api, "ApiId").Not.Nullable().CustomType<ApiEnum>();
            this.Map(x => x.AppId).Not.Nullable();
            this.Map(x => x.AppKey).Not.Nullable();
            this.Map(x => x.Disabled).Nullable();
            this.Map(x => x.ValidFrom).Nullable();
            this.Map(x => x.ValidUntil).Nullable();
            this.Cache.ReadOnly();
        }
    }
}
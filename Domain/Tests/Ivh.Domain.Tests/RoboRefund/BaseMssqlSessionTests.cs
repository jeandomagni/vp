﻿namespace Ivh.Domain.RoboRefund
{
    using System;
    using System.Globalization;
    using FluentNHibernate.Cfg;
    using FluentNHibernate.Cfg.Db;
    using FluentNHibernate.Conventions.Helpers;
    using NHibernate;
    using NHibernate.Cfg;
    using NHibernate.Tool.hbm2ddl;
    using NHibernate.AspNet.Identity.Helpers;
    using NUnit.Framework;
    using User.Entities;


    public class BaseMssqlSessionTests: IDisposable
    {
        private ITransaction _transaction;


        private const string MssqlUsername = "sa";
        private const string MssqlPassword = "DefaultComplex!@";

        protected ISession Session { get; set; }
        protected ISessionFactory SessionFactory { get; set; }

        [SetUp]
        protected void OpenTransaction()
        {
            this._transaction = this.Session.BeginTransaction();
            this._transaction.Begin();
        }
        [TearDown]
        protected void RollbackTransaction()
        {
            this._transaction.Rollback();
            this._transaction.Dispose();
        }

        ///<summary>
        ///     This method can be used for development and can assist with creating the tables
        ///     It can also help with developing and general troubleshooting
        ///     It is not referenced in the tests
        ///     Requires set up "test" database and run "create schema intelligence"
        ///
        ///     Example BuildConfiguration(m => {
        ///         m.FluentMappings.Add<SurveyMap>();
        ///     });
        ///</summary>
        protected void BuildConfiguration(Func<MappingConfiguration, bool> setupClasses, bool isInstance = true, bool createTables = false)
        {
            //This doesnt create the database: UnitTestCatalog, just create an empty one...
            string commandTimeout = Convert.ToInt32((TimeSpan.FromMinutes(10).TotalSeconds)).ToString(CultureInfo.InvariantCulture);

            string partOfConnectionString = isInstance ? "Data Source=LOCALHOST\\MSSQLDEV;Integrated Security=True;" : $"Data Source=LOCALHOST;User ID={MssqlUsername};Password={MssqlPassword};";
            Configuration configuration = Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql2012
                    .ConnectionString($"{partOfConnectionString}Initial Catalog=DEV_VP_APP;")
                )
                .Mappings(m =>
                {
                    m.FluentMappings.Conventions.Setup(a => a.Add(AutoImport.Never()));
                    setupClasses(m);
                    m.FluentMappings.Conventions.Add<Ivh.Common.Data.Conventions.DateTimeConvention>();
                })
                .ExposeConfiguration(
                    cfg =>
                    {
                        cfg.SetProperty("command_timeout", commandTimeout);
                        cfg.SetProperty("adonet.batch_size", "1");

                        string cs = cfg.GetProperty("connection.connection_string");
                        cfg.SetProperty("connection.connection_string", cs + $";Application Name=Testing");
                    })
                .ExposeConfiguration(
                    cfg => cfg.AddDeserializedMapping(MappingHelper.GetIdentityMappings(new Type[] { typeof(VisitPayUser)}), null))
                .BuildConfiguration();

            this.SessionFactory = configuration.BuildSessionFactory();
            this.Session = this.SessionFactory.OpenSession();

            if (createTables)
            {
                SchemaExport exporter = new SchemaExport(configuration);
                exporter.Execute(true, true, false, this.Session.Connection, null);
            }
        }

        public void Dispose()
        {
            if (this._transaction != null && this._transaction.IsActive)
            {
                this._transaction?.Rollback();

            }
            this._transaction?.Dispose();
            this.Session?.Dispose();
        }
    }
}
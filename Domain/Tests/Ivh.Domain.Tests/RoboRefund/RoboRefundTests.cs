﻿namespace Ivh.Domain.RoboRefund
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Threading;
    using AppIntelligence.Entities;
    using AppIntelligence.Survey.Mappings;
    using AutoMapper;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.State.Interfaces;
    using Common.Tests.Helpers.FinancePlan;
    using Common.Tests.Helpers.Guarantor;
    using Common.Tests.Helpers.RoboRefund;
    using Common.Tests.Helpers.Visit;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Content.Entities;
    using Core.KnowledgeBase.Entities;
    using Email.Entities;
    using EventJournal.Entities;
    using FinanceManagement.Entities;
    using FinanceManagement.FinancePlan.Entities;
    using FinanceManagement.FinancePlan.Mappings;
    using FinanceManagement.Payment.Entities;
    using FinanceManagement.RoboRefund.Entities;
    using FinanceManagement.RoboRefund.Interfaces;
    using FinanceManagement.RoboRefund.Mappings;
    using FinanceManagement.Statement.Entities;
    using Guarantor.Entities;
    using HospitalData.VpGuarantor.Entities;
    using Logging.Entities;
    using Moq;
    using NUnit.Framework;
    using Provider.FinanceManagement.RoboRefund;
    using Settings.Entities;
    using User.Entities;
    using Visit.Entities;

    [TestFixture]
    public class RoboRefundTests : DomainTestBase //: BaseMssqlSessionTests
    {
        //Todo, Is there a more graceful way to handle docker vs not
        [OneTimeSetUp]
        public void SetupTestSession()
        {
            //this.BuildConfiguration(m =>
            //{
            //    //Makes most nhibernate queries work
            //    //Might put this into it's own spot?
            //    m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(VisitPayUser)));
            //    m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(Guarantor)));
            //    m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(Visit)));
            //    m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(Communication)));
            //    m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(Payment)));
            //    m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(PaymentMethod)));
            //    m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(GuarantorMatchingLog)));
            //    m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(Monitor)));
            //    m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(RandomizedTest)));
            //    m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(RandomizedTestGroup)));
            //    m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(RandomizedTestGroupMembership)));
            //    m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(ClientSetting)));
            //    m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(CmsVersion)));
            //    m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(SurveyMap)));
            //    m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(SurveyQuestionMap)));
            //    m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(SurveyResultAnswerMap)));
            //    m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(SurveyResultTypeMap)));
            //    m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(SurveyResultMap)));
            //    m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(KnowledgeBaseCategory)));
            //    m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(ClientSetting)));
            //    m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(LogStackTrace)));
            //    m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(JournalEvent)));
            //    m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(Ivh.Domain.Rager.Entities.Visit)));
                
            //    return true;
            //},!System.Environment.MachineName.Contains("SCAR"));
        }

        private static int _statementCounter = 1;

        protected FinancePlanTestInfo CreateInterestBearingFinancePlan(IList<Tuple<Visit, IList<VisitTransaction>>> visits1,
            decimal paymentAmount, 
            decimal interestRate,
            DateTime now,
            bool assessInterestRightAway = true, 
            InterestCalculationMethodEnum interestCalculationMethod = InterestCalculationMethodEnum.Daily
            )
        {
            FinancePlan fp1 = FinancePlanFactory.CreateFinancePlan(visits1, 2, paymentAmount, now.AddMonths(-1), interestRate:interestRate);

            fp1.InterestCalculationMethod = interestCalculationMethod;

            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>();
            visits.AddRange(visits1);
            
            if(assessInterestRightAway)
            {
                this.AssessInterest(visits1, fp1, now.AddDays(-30), now);
            }

            return new FinancePlanTestInfo()
            {
                VisitsTuples = visits1,
                FinancePlan = fp1
            };
        }

        private void AssessInterest(IList<Tuple<Visit, IList<VisitTransaction>>> visits1, FinancePlan fp1, DateTime startDate, DateTime now)
        {
            FinancePlanServiceMockBuilder fpSvcBuilder = this.CreateFinancePlanServiceMockBuilderWithInterestService(visits1);

            IList<FinancePlanVisitInterestDue> assessments = fpSvcBuilder.CreateService().AssessInterest(fp1, startDate, now.AddMilliseconds(-10), now);

            int thisFpStatementId = _statementCounter++;
            foreach (FinancePlanVisitInterestDue financePlanVisitInterestDue in assessments)
            {
                financePlanVisitInterestDue.VpStatementId = thisFpStatementId;
            }
        }

        private FinancePlanServiceMockBuilder CreateFinancePlanServiceMockBuilderWithInterestService(IList<Tuple<Visit, IList<VisitTransaction>>> visits1)
        {
            FinancePlanServiceMockBuilder fpSvcBuilder = new FinancePlanServiceMockBuilder();
            fpSvcBuilder.Setup(builder =>
            {
                builder.InterestService = new InterestServiceMockBuilder().CreateService();
                builder.ApplicationSettingsMock.Setup(x => x.PreventInterest).Returns(new ApplicationSetting<bool>("PreventInterest", new Dictionary<string, string>(), () => false));
                builder.VisitServiceMock.Setup(x => x.GetVisits(It.IsAny<int>(), It.IsAny<IList<int>>())).Returns(visits1.Select(y => y.Item1).ToList());
            });
            return fpSvcBuilder;
        }

        private static int _visitPrincipalAmountCounter = 1;
        protected IRoboRefundService GetRefundService(Action<RoboRefundServiceMockBuilder> extraSetup = null, IList<FinancePlanTestInfo> testInfos = null)
        {
            
            RoboRefundServiceMockBuilder refundMockBuilder = new RoboRefundServiceMockBuilder();
            //Translate the adjustments from the FPs in testInfos to RoboRefundEventResults
            IList<RoboRefundEventResult> results = new List<RoboRefundEventResult>() { };
            if (testInfos != null)
            {
                //Increment the Ids for all FinancePlanVisitPrincipalAmounts.
                testInfos.SelectMany(x => x.FinancePlan.FinancePlanVisits)
                    .SelectMany(z => z.FinancePlanVisitPrincipalAmounts)
                    .ForEach(y => y.FinancePlanVisitPrincipalAmountId = _visitPrincipalAmountCounter++);

                IEnumerable<FinancePlanVisitPrincipalAmount> principalAmounts = testInfos
                    .Select(x => x.FinancePlan)
                    .SelectMany(y => y.FinancePlanVisits)
                    .SelectMany(z => z.FinancePlanVisitPrincipalAmounts)
                    .Where(a => a.FinancePlanVisitPrincipalAmountType == FinancePlanVisitPrincipalAmountTypeEnum.Adjustment && a.Amount < 0);

                results.AddRange(Mapper.Map<IList<RoboRefundEventResult>>(principalAmounts));
            }

            refundMockBuilder.Setup(builder =>
            {
                builder.RoboRefundRepositoryMock.Setup(x => x.GetUnRefundedEventResults(It.IsAny<int>())).Returns(results);
                
                if (testInfos != null)
                {
                    List<int?> vpgs = testInfos.Select(x => x.FinancePlan.VpGuarantor?.VpGuarantorId).Where(y => y.HasValue).ToList();
                    foreach (int? vpg in vpgs)
                    {
                        //Might want to change this to generate multiple StatementPeriodResults in the future
                        //Looking for all Interest assessments and their statements
                        IList<FinancePlanVisitInterestDue> allStatementFromDues = testInfos.Where(x => x.FinancePlan.VpGuarantor.VpGuarantorId == vpg.Value)
                            .SelectMany(y => y.FinancePlan.FinancePlanVisits)
                            .SelectMany(z => z.FinancePlanVisitInterestDues)
                            .Where(a => a.VpStatementId != null && a.FinancePlanVisitInterestDueType == FinancePlanVisitInterestDueTypeEnum.InterestAssessment).ToList();
                        List<int?> statementIds = allStatementFromDues.Select(x => x.VpStatementId).Distinct().ToList();
                        
                        List<StatementPeriodResult> statementPeriodResults = new List<StatementPeriodResult>();
                        foreach (int? statementId in statementIds)
                        {
                            FinancePlanVisitInterestDue financePlanVisitInterestDue = allStatementFromDues.FirstOrDefault(x => x.VpStatementId == statementId);
                            statementPeriodResults.Add(new StatementPeriodResult()
                            {
                                PeriodStartDate = (financePlanVisitInterestDue.OverrideInsertDate ?? financePlanVisitInterestDue.InsertDate).AddDays(-30),
                                PeriodEndDate = financePlanVisitInterestDue.OverrideInsertDate ?? financePlanVisitInterestDue.InsertDate,
                                VpGuarantorId = vpg.Value,
                                VpStatementId = financePlanVisitInterestDue.VpStatementId.Value
                            });
                        }

                        builder.VpStatementRepositoryMock.Setup(x => x.GetAllStatementPeriodsForGuarantor(vpg.Value))
                            .Returns(statementPeriodResults);

                        //.Returns(new List<StatementPeriodResult>() {
                        //    new StatementPeriodResult()
                        //    {
                        //        PeriodEndDate = DateTime.UtcNow.AddMilliseconds(-10), 
                        //        PeriodStartDate = DateTime.UtcNow.AddDays(-20), 
                        //        VpGuarantorId = vpg.Value,
                        //        VpStatementId = allStatementFromDues.FirstOrDefault() != null ? allStatementFromDues.FirstOrDefault().VpStatementId.Value : 0
                        //    }
                        //}); 
                    }
                    
                    builder.FinancePlanRepositoryMock.Setup(x => x.GetAllOriginatedFinancePlansIncludingClosedFromVisits(It.IsAny<IList<int>>())).Returns(testInfos.Select(y => y.FinancePlan).ToList());
                    //builder.FinancePlanRepositoryMock.Setup(x => x.GetDistinctFinancePlansFromVisitsIds(It.IsAny<IList<int>>())).Returns(testInfos.Select(y => y.FinancePlan).ToList());
                    
                    FinancePlanServiceMockBuilder fpSvcBuilder = this.CreateFinancePlanServiceMockBuilderWithInterestService(testInfos.SelectMany(x => x.VisitsTuples).ToList());
                    builder.FinancePlanService = fpSvcBuilder.CreateService();
                }


                if (extraSetup != null)
                {
                    extraSetup(builder);
                }
            });

            return refundMockBuilder.CreateService();
        }

        private static int paymentCounter = 1;
        private static int paymentAllocationCounter = 1;
        private Payment CreatePaymentForFinancePlanWithValuesForVisits(FinancePlan financePlan, IList<Tuple<FinancePlanVisit, decimal>> fpvActualAmounts)
        {
            Payment payment = new Payment
            {
                PaymentId = paymentCounter++,
                PaymentType = PaymentTypeEnum.ManualPromptSpecificAmount
            };
            foreach (Tuple<FinancePlanVisit, decimal> fpvActualAmount in fpvActualAmounts)
            {
                this.AddFinancePlanVisitPaymentAmount(financePlan, fpvActualAmount.Item1, fpvActualAmount.Item2, payment);
            }

            return payment;
        }
        
        private Payment CreatePaymentForFinancePlan(FinancePlan financePlan, decimal actualAmount)
        {
            decimal amountLeft = actualAmount;
            if (amountLeft > financePlan.CurrentFinancePlanBalance)
            {
                amountLeft = financePlan.CurrentFinancePlanBalance;
            }
            Payment payment = new Payment
            {
                PaymentId = paymentCounter++,
                PaymentType = PaymentTypeEnum.ManualPromptSpecificAmount
            };
            //Need to beef this up to do both types of payments and multiple visits
            foreach (FinancePlanVisit financePlanFinancePlanVisit in financePlan.FinancePlanVisits)
            {
                if (!(amountLeft > 0))
                {
                    continue;
                }

                amountLeft = this.AddFinancePlanVisitPaymentAmount(financePlan, financePlanFinancePlanVisit, amountLeft, payment);
            }
            
            return payment;
        }

        private decimal AddFinancePlanVisitPaymentAmount(FinancePlan financePlan, FinancePlanVisit financePlanFinancePlanVisit, decimal amountLeft, Payment payment)
        {
            decimal interestToUse = 0m;
            decimal principalToUse = 0m;
            if (financePlanFinancePlanVisit.InterestAssessed > 0)
            {
                interestToUse = financePlanFinancePlanVisit.InterestAssessed < amountLeft ? financePlanFinancePlanVisit.InterestAssessed : amountLeft;
            }

            amountLeft = amountLeft - interestToUse;

            principalToUse = financePlanFinancePlanVisit.MaxCurrentBalance > amountLeft ? amountLeft : financePlanFinancePlanVisit.MaxCurrentBalance;

            amountLeft = amountLeft - principalToUse;


            if (principalToUse > 0)
            {
                PaymentAllocation paymentAllocation = new PaymentAllocation
                {
                    PaymentAllocationId = paymentAllocationCounter++,
                    Payment = payment,
                    ActualAmount = principalToUse,
                    FinancePlanId = financePlan.FinancePlanId,
                    PaymentAllocationType = PaymentAllocationTypeEnum.VisitPrincipal,
                    PaymentVisit = new PaymentVisit {VisitId = financePlanFinancePlanVisit.VisitId},
                };
                payment.PaymentAllocations.Add(paymentAllocation);
            }

            if (interestToUse > 0)
            {
                PaymentAllocation paymentAllocation = new PaymentAllocation
                {
                    PaymentAllocationId = paymentAllocationCounter++,
                    Payment = payment,
                    ActualAmount = interestToUse,
                    FinancePlanId = financePlan.FinancePlanId,
                    PaymentAllocationType = PaymentAllocationTypeEnum.VisitInterest,
                    PaymentVisit = new PaymentVisit {VisitId = financePlanFinancePlanVisit.VisitId},
                };
                payment.PaymentAllocations.Add(paymentAllocation);
            }

            return amountLeft;
        }

        private void BackDatePaymentOnFinancePlan(FinancePlanTestInfo financePlanTestInfo, DateTime now, Payment payment)
        {
            foreach (FinancePlanAmountDue financePlanAmountDue in financePlanTestInfo.FinancePlan.FinancePlanAmountsDue.Where(x => x.PaymentAllocationId != null))
            {
                financePlanAmountDue.InsertDate = now.AddDays(-1);
            }

            // update FinancePlanAmountDue for principal allocations
            foreach (FinancePlanVisit financePlanFinancePlanVisit in financePlanTestInfo.FinancePlan.FinancePlanVisits)
            {
                foreach (FinancePlanVisitPrincipalAmount financePlanVisitInterestDue in financePlanFinancePlanVisit
                    .FinancePlanVisitPrincipalAmounts
                    .Where(x => x.PaymentAllocationId == payment.PaymentAllocations.First(pa => pa.PaymentAllocationType == PaymentAllocationTypeEnum.VisitPrincipal).PaymentAllocationId))
                {
                    financePlanVisitInterestDue.OverrideInsertDate = now;
                }
            }
        }

        private void BackDateAllFinancePlanTestInfo(FinancePlanTestInfo financePlanTestInfo, TimeSpan timeSpan)
        {
            foreach (FinancePlanAmountDue financePlanAmountDue in financePlanTestInfo.FinancePlan.FinancePlanAmountsDue)
            {
                financePlanAmountDue.InsertDate = financePlanAmountDue.InsertDate.Add(timeSpan);
                financePlanAmountDue.OverrideDueDate = financePlanAmountDue.OverrideDueDate?.Add(timeSpan);
            }
            foreach (FinancePlanVisit financePlanVisit in financePlanTestInfo.FinancePlan.FinancePlanVisits)
            {
                foreach (FinancePlanVisitInterestDue financePlanVisitInterestDue in financePlanVisit.FinancePlanVisitInterestDues)
                {
                    financePlanVisitInterestDue.InsertDate = financePlanVisitInterestDue.InsertDate.Add(timeSpan);
                    financePlanVisitInterestDue.OverrideInsertDate = financePlanVisitInterestDue.OverrideInsertDate?.Add(timeSpan);
                }

                foreach (FinancePlanVisitPrincipalAmount financePlanVisitPrincipalAmount in financePlanVisit.FinancePlanVisitPrincipalAmounts)
                {
                    financePlanVisitPrincipalAmount.InsertDate = financePlanVisitPrincipalAmount.InsertDate.Add(timeSpan);
                    financePlanVisitPrincipalAmount.OverrideInsertDate = financePlanVisitPrincipalAmount.OverrideInsertDate?.Add(timeSpan);
                }
            }
        }

        //TODO: test cases with amounts
        [TestCase(InterestCalculationMethodEnum.Daily)]
        //[TestCase(InterestCalculationMethodEnum.Monthly)]
        public void RoboRefund_WithSinglePayment_Interest(InterestCalculationMethodEnum interestCalculationMethod)
        {
            DateTime dateToTest = new DateTime(2019, 1, 1, 12, 0, 0, DateTimeKind.Utc);
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            IList<Tuple<Visit, IList<VisitTransaction>>> visits1 = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(1503.93m, 1, VisitStateEnum.Active, dateToTest.AddMonths(-10), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1001),
                VisitFactory.GenerateActiveVisit(1292.37m, 1, VisitStateEnum.Active, dateToTest.AddMonths(-10), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1002),
                VisitFactory.GenerateActiveVisit(433.11m, 1, VisitStateEnum.Active, dateToTest.AddMonths(-10), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB, visitId:1003),
            };

            FinancePlanTestInfo financePlanTestInfo = this.CreateInterestBearingFinancePlan(visits1, 50m, 0.05m, dateToTest, interestCalculationMethod: interestCalculationMethod);

            FinancePlanVisit financePlanVisit = financePlanTestInfo.FinancePlan.FinancePlanVisits.FirstOrDefault();
            financePlanVisit.CurrentVisitBalance = financePlanVisit.CurrentVisitBalance - 100m;
            //Create negative adjustment
            financePlanTestInfo.FinancePlan.AddFinancePlanVisitPrincipalAmountAdjustments();
            Payment payment = this.CreatePaymentForFinancePlan(financePlanTestInfo.FinancePlan, 100);
            financePlanTestInfo.FinancePlan.OnPayment(-100, dateToTest, payment);
            this.BackDatePaymentOnFinancePlan(financePlanTestInfo, dateToTest, payment);

            IRoboRefundService service = this.GetRefundService(null, financePlanTestInfo.ToListOfOne());

            IList<RoboRefundPayment> result = service.RoboRefund(financePlanTestInfo.FinancePlan.VpGuarantor.VpGuarantorId);

            //Should have reduced the interest 30c
            Assert.AreEqual(-.44m, result.Sum(y => y.InterestAssessedDifferentialTotal));

            //Remove the interest paid
            Assert.AreEqual(0.44m, result.Sum(y => y.InterestPaidDifferentialTotal));

            //Add to the principal paid
            Assert.AreEqual(-0.44m, result.Sum(y => y.PrincipalPaidDifferentialTotal));
            

            //RoboRefundRepository repo = new RoboRefundRepository(this.Session);
            //IList<RoboRefundEventResult> results = repo.GetUnRefundedEventResults();
        }

        [TestCase(InterestCalculationMethodEnum.Daily)]
        //[TestCase(InterestCalculationMethodEnum.Monthly)]
        public void RoboRefund_WithSinglePayment_Interest_Case1(InterestCalculationMethodEnum interestCalculationMethod)
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            DateTime dateToTest = new DateTime(2019, 1, 1, 12, 0, 0, DateTimeKind.Utc);
            Tuple<Visit, IList<VisitTransaction>> secondVisit = VisitFactory.GenerateActiveVisit(600m, 1, VisitStateEnum.Active, dateToTest.AddMonths(-10), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB, visitId:1003);
            Tuple<Visit, IList<VisitTransaction>> firstVisit = VisitFactory.GenerateActiveVisit(400m, 1, VisitStateEnum.Active, dateToTest.AddMonths(-10), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1001);
            IList<Tuple<Visit, IList<VisitTransaction>>> visits1 = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                firstVisit,
                secondVisit,
            };
            
            FinancePlanTestInfo financePlanTestInfo = this.CreateInterestBearingFinancePlan(visits1, 100m, 0.08m, dateToTest, assessInterestRightAway:false);

            FinancePlanVisit foundFirstFpv = financePlanTestInfo.FinancePlan.FinancePlanVisits.FirstOrDefault(x => x.VisitId == firstVisit.Item1.VisitId);
            FinancePlanVisit foundSecondFpv = financePlanTestInfo.FinancePlan.FinancePlanVisits.FirstOrDefault(x => x.VisitId == secondVisit.Item1.VisitId);
            
            IList<Tuple<FinancePlanVisit, decimal>> paymentAmounts = new List<Tuple<FinancePlanVisit, decimal>>()
            {
                new Tuple<FinancePlanVisit, decimal>(foundFirstFpv, 40m),
                new Tuple<FinancePlanVisit, decimal>(foundSecondFpv, 60m),
            };
            
            Payment paymentFirst = this.CreatePaymentForFinancePlanWithValuesForVisits(financePlanTestInfo.FinancePlan, paymentAmounts);
            financePlanTestInfo.FinancePlan.OnPayment(-100, dateToTest, paymentFirst);
            this.BackDatePaymentOnFinancePlan(financePlanTestInfo, dateToTest, paymentFirst);

            //set to statement time
            this.BackDateAllFinancePlanTestInfo(financePlanTestInfo, new TimeSpan(-10, 0, 0, 0));
            this.AssessInterest(visits1, financePlanTestInfo.FinancePlan, dateToTest.AddDays(-30), dateToTest);
            //
            
            this.BackDateAllFinancePlanTestInfo(financePlanTestInfo, new TimeSpan(-20, 0, 0, 0));
            Payment paymentSecond = this.CreatePaymentForFinancePlanWithValuesForVisits(financePlanTestInfo.FinancePlan, paymentAmounts);
            financePlanTestInfo.FinancePlan.OnPayment(-100, dateToTest, paymentSecond);
            
            this.BackDateAllFinancePlanTestInfo(financePlanTestInfo, new TimeSpan(-10, 0, 0, 0));
            
            foundSecondFpv.CurrentVisitBalance = foundSecondFpv.FinancePlanVisitPrincipalAmounts.Sum(x => x.Amount) - 100m;
            //Create negative adjustment
            financePlanTestInfo.FinancePlan.AddFinancePlanVisitPrincipalAmountAdjustments();
            
            IRoboRefundService service = this.GetRefundService(null, financePlanTestInfo.ToListOfOne());

            IList<RoboRefundPayment> result = service.RoboRefund(financePlanTestInfo.FinancePlan.VpGuarantor.VpGuarantorId);

            //Should have reduced the interest 30c
            Assert.AreEqual(-0.68m, result.Sum(y => y.AmountToReallocateFromInterestToPrincipalSum));

            //Remove the interest paid
            Assert.AreEqual(0.0m, result.Sum(y => y.AmountOfInterestToWriteOffSum));

            //Add to the principal paid
            Assert.AreEqual(-0.68m, result.Sum(y => y.PrincipalPaidDifferentialTotal));
            
        }

        [TestCase(InterestCalculationMethodEnum.Daily)]
        //[TestCase(InterestCalculationMethodEnum.Monthly)]
        public void RoboRefund_WithoutPayment_Interest(InterestCalculationMethodEnum interestCalculationMethod)
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            DateTime dateToTest = new DateTime(2019, 1, 1, 12, 0, 0, DateTimeKind.Utc);
            IList<Tuple<Visit, IList<VisitTransaction>>> visits1 = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(1503.93m, 1, VisitStateEnum.Active, dateToTest.AddMonths(-10), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1001),
                VisitFactory.GenerateActiveVisit(1292.37m, 1, VisitStateEnum.Active, dateToTest.AddMonths(-10), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1002),
                VisitFactory.GenerateActiveVisit(433.11m, 1, VisitStateEnum.Active, dateToTest.AddMonths(-10), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB, visitId:1003),
            };
            
            FinancePlanTestInfo financePlanTestInfo = this.CreateInterestBearingFinancePlan(visits1, 50m, 0.05m, dateToTest, interestCalculationMethod: interestCalculationMethod);
            
            FinancePlanVisit financePlanVisit = financePlanTestInfo.FinancePlan.FinancePlanVisits.FirstOrDefault();
            financePlanVisit.CurrentVisitBalance = financePlanVisit.CurrentVisitBalance - 100m;
            //Create negative adjustment
            financePlanTestInfo.FinancePlan.AddFinancePlanVisitPrincipalAmountAdjustments();
            
            IRoboRefundService service = this.GetRefundService(null, financePlanTestInfo.ToListOfOne());

            IList<RoboRefundPayment> result = service.RoboRefund(financePlanTestInfo.FinancePlan.VpGuarantor.VpGuarantorId);

            //Should have reduced the interest 29c
            //Slightly less as the one with a payment has -100 on the last day.
            Assert.AreEqual(-.43m, result.Sum(y => y.InterestAssessedDifferentialTotal));

            //If there wasnt any payment to reverse, dont do anything.
            Assert.AreEqual(0.0m, result.Sum(y => y.InterestPaidDifferentialTotal));

            //If there wasnt any payment to reverse, dont do anything.
            Assert.AreEqual(0.0m, result.Sum(y => y.PrincipalPaidDifferentialTotal));
            

            //RoboRefundRepository repo = new RoboRefundRepository(this.Session);
            //IList<RoboRefundEventResult> results = repo.GetUnRefundedEventResults();
        }

        [TestCase(InterestCalculationMethodEnum.Daily, false)]
        [TestCase(InterestCalculationMethodEnum.Daily, true)]
        public void RoboRefund_RemovedVisit_FullInterestRefund(InterestCalculationMethodEnum interestCalculationMethod, bool removeVisit)
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            DateTime dateToTest = new DateTime(2019, 1, 1, 12, 0, 0, DateTimeKind.Utc);
            IList<Tuple<Visit, IList<VisitTransaction>>> visits1 = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(300m, 1, VisitStateEnum.Active, dateToTest.AddMonths(-10), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1001),
                VisitFactory.GenerateActiveVisit(3822m, 1, VisitStateEnum.Active, dateToTest.AddMonths(-10), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB, visitId:1002),
            };
            decimal paymentAmount = 177.84m;
            decimal interestRate = 0.04m;

            FinancePlanTestInfo financePlanTestInfo = this.CreateInterestBearingFinancePlan(visits1, paymentAmount, interestRate, dateToTest, interestCalculationMethod: interestCalculationMethod);

            //FinancePlanVisit financePlanVisit = financePlanTestInfo.FinancePlan.FinancePlanVisits.FirstOrDefault(x => x.VisitId == 1001);
            FinancePlanVisit foundFirstFpv = financePlanTestInfo.FinancePlan.FinancePlanVisits.FirstOrDefault(x => x.VisitId == visits1.First().Item1.VisitId);
            FinancePlanVisit foundSecondFpv = financePlanTestInfo.FinancePlan.FinancePlanVisits.FirstOrDefault(x => x.VisitId == visits1.Last().Item1.VisitId);

            //pay visit in full
            IList<Tuple<FinancePlanVisit, decimal>> paymentAmounts = new List<Tuple<FinancePlanVisit, decimal>>()
            {
                new Tuple<FinancePlanVisit, decimal>(foundFirstFpv, 300m),
                new Tuple<FinancePlanVisit, decimal>(foundSecondFpv, 400m),
            };

            Payment payment = this.CreatePaymentForFinancePlanWithValuesForVisits(financePlanTestInfo.FinancePlan, paymentAmounts);
            financePlanTestInfo.FinancePlan.OnPayment(-700m, dateToTest, payment);
            this.BackDatePaymentOnFinancePlan(financePlanTestInfo, dateToTest, payment);

            //close and remove visit
            if (removeVisit)
            {
                foundFirstFpv.HardRemoveDate = dateToTest;
                ((IEntity<VisitStateEnum>)visits1.First(x => x.Item1.VisitId == 1001).Item1).SetState(VisitStateEnum.Inactive, string.Empty);
            }

            //Create negative adjustment
            foundFirstFpv.CurrentVisitBalance = -300m;
            financePlanTestInfo.FinancePlan.AddFinancePlanVisitPrincipalAmountAdjustments();
            financePlanTestInfo.FinancePlan.AddFinancePlanVisitPrincipalAmountAdjustments(foundFirstFpv);

            //Create service
            IRoboRefundService service = this.GetRefundService(null, financePlanTestInfo.ToListOfOne());

            //Add non natural adjustments
            service.AddNonNaturalQualifyingEvents(financePlanTestInfo.FinancePlan.VpGuarantor.VpGuarantorId);

            //refund
            IList<RoboRefundPayment> result = service.RoboRefund(financePlanTestInfo.FinancePlan.VpGuarantor.VpGuarantorId);

            //Should have reduced the interest
            Assert.AreEqual(-foundFirstFpv.InterestAssessed, result.Sum(y => y.InterestAssessedDifferentialTotal));

            //Remove the interest paid
            Assert.AreEqual(foundFirstFpv.InterestAssessed, result.Sum(y => y.InterestPaidDifferentialTotal));

            //Add to the principal paid
            Assert.AreEqual(-foundFirstFpv.InterestAssessed, result.Sum(y => y.PrincipalPaidDifferentialTotal));

        }


        [TestCase(InterestCalculationMethodEnum.Daily, false)]
        [TestCase(InterestCalculationMethodEnum.Daily, true)]
        public void RoboRefund_RemovedVisit_NonNatural_FullInterestRefund(InterestCalculationMethodEnum interestCalculationMethod, bool removeVisit)
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            DateTime dateToTest = new DateTime(2019, 1, 1, 12, 0, 0, DateTimeKind.Utc);
            IList<Tuple<Visit, IList<VisitTransaction>>> visits1 = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(300m, 1, VisitStateEnum.Active, dateToTest.AddMonths(-10), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId: 1001),
                VisitFactory.GenerateActiveVisit(3822m, 1, VisitStateEnum.Active, dateToTest.AddMonths(-10), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB, visitId: 1002),
            };
            //not really important in this test, but here for reference to the generated app values
            decimal paymentAmount = 177.84m;
            decimal interestRate = 0.04m;

            FinancePlanTestInfo originalFinancePlan = this.CreateInterestBearingFinancePlan(visits1, paymentAmount, interestRate, dateToTest, interestCalculationMethod: interestCalculationMethod);
            //FinancePlanVisit foundFirstFpv, foundSecondFpv;

            void CreateAndAllocatePaymentForFinancePlan(FinancePlan financePlan, params decimal[] amounts)
            {
                IList<Tuple<FinancePlanVisit, decimal>> paymentAmounts = new List<Tuple<FinancePlanVisit, decimal>>();
                int i = 0;
                foreach (decimal amount in amounts)
                {
                    paymentAmounts.Add(new Tuple<FinancePlanVisit, decimal>(financePlan.FinancePlanVisits[i++], amount));
                }

                //make a payment
                Payment payment = this.CreatePaymentForFinancePlanWithValuesForVisits(financePlan, paymentAmounts);
                financePlan.OnPayment(-500m, dateToTest, payment);
                this.BackDatePaymentOnFinancePlan(originalFinancePlan, dateToTest, payment);
            }

            //payment allocation for fp1
            CreateAndAllocatePaymentForFinancePlan(originalFinancePlan.FinancePlan, 200m, 300m);

            originalFinancePlan.FinancePlan.OriginationDate = originalFinancePlan.FinancePlan.OriginationDate.Value.AddMonths(-1);

            //new FP (reconfig)
            FinancePlanTestInfo secondFinancePlan = this.CreateInterestBearingFinancePlan(visits1, paymentAmount, interestRate, dateToTest, interestCalculationMethod: interestCalculationMethod);
            originalFinancePlan.FinancePlan.OnFinancePlanClose("no reason");
            originalFinancePlan.FinancePlan.FinancePlanVisits.ForEach(x => x.HardRemoveDate = dateToTest);

            //payment allocation for fp2
            CreateAndAllocatePaymentForFinancePlan(secondFinancePlan.FinancePlan, 100m, 200m);

            //get the fpv created for the refunded visit
            FinancePlanVisit foundFirstFpv = originalFinancePlan.FinancePlan.FinancePlanVisits.FirstOrDefault(x => x.VisitId == visits1.First().Item1.VisitId);
            FinancePlanVisit foundSecondFpv = secondFinancePlan.FinancePlan.FinancePlanVisits.FirstOrDefault(x => x.VisitId == visits1.First().Item1.VisitId);
            IList<FinancePlanVisit> financePlanVisitsForRefundedVisit = new List<FinancePlanVisit> { foundFirstFpv, foundSecondFpv };

            //close and remove visit
            if (removeVisit)
            {
                foundSecondFpv.HardRemoveDate = dateToTest;
                ((IEntity<VisitStateEnum>)visits1.First(x => x.Item1.VisitId == 1001).Item1).SetState(VisitStateEnum.Inactive, string.Empty);
            }

            //Create negative adjustment
            foundSecondFpv.CurrentVisitBalance = -300m;
            secondFinancePlan.FinancePlan.AddFinancePlanVisitPrincipalAmountAdjustments();
            secondFinancePlan.FinancePlan.AddFinancePlanVisitPrincipalAmountAdjustments(foundSecondFpv);

            //Create Service
            IRoboRefundService service = this.GetRefundService(null, new List<FinancePlanTestInfo> { originalFinancePlan, secondFinancePlan });

            //Add non natural adjustments
            service.AddNonNaturalQualifyingEvents(secondFinancePlan.FinancePlan.VpGuarantor.VpGuarantorId);

            //refund
            IList<RoboRefundPayment> result = service.RoboRefund(secondFinancePlan.FinancePlan.VpGuarantor.VpGuarantorId);

            //Should have reduced the interest
            Assert.AreEqual(financePlanVisitsForRefundedVisit.Sum(x => -x.InterestAssessed), result.Sum(y => y.InterestAssessedDifferentialTotal));

            //Remove the interest paid
            Assert.AreEqual(financePlanVisitsForRefundedVisit.Sum(x => x.InterestAssessed), result.Sum(y => y.InterestPaidDifferentialTotal));

            //Add to the principal paid
            Assert.AreEqual(financePlanVisitsForRefundedVisit.Sum(x => -x.InterestAssessed), result.Sum(y => y.PrincipalPaidDifferentialTotal));

        }

        private static int _visitId = 1;
        
        private static int _financePlanVisit = 1;
        private static int _roboRefundFinancePlanVisitId = 1;
        private RoboRefundFinancePlanVisit CreateRoboRefundFinancePlanVisit(decimal interestAssessedPrevious, decimal interestPaidPrevious, decimal principalPaidPrevious, decimal interestAddedForReconfigCombo, List<RoboRefundFinancePlanVisitRecalculatedInterest> recalculatedInterests, int? parentFinancePlanVisitId, bool isFinancePlanClosed = false)
        {
            RoboRefundFinancePlanVisit roboFpv = new RoboRefundFinancePlanVisit()
            {
                RoboRefundFinancePlanVisitId = _roboRefundFinancePlanVisitId++,
                InterestAssessedPrevious = interestAssessedPrevious,
                InterestPaidPrevious = interestPaidPrevious,
                PrincipalPaidPrevious = principalPaidPrevious,
                NewReCalculatedInterests = recalculatedInterests,
                FinancePlanVisitId = _financePlanVisit++,
                ParentFinancePlanVisitId = parentFinancePlanVisitId,
                FinancePlanIsClosed = isFinancePlanClosed,
                InterestAddedForReconfigCombo = interestAddedForReconfigCombo
            };

            return roboFpv;
        }
        
        private static int _roboRefundVisit = 1;
        private RoboRefundVisit CreateRoboRefundVisit(int visitId, IList<RoboRefundFinancePlanVisit> financePlanVisits)
        {
            RoboRefundVisit visit = new RoboRefundVisit()
            {
                RoboRefundVisitId = _roboRefundVisit++,
                RoboRefundFinancePlanVisits = financePlanVisits,
                VisitId = visitId
            };
            foreach (RoboRefundFinancePlanVisit roboRefundFinancePlanVisit in financePlanVisits)
            {
                roboRefundFinancePlanVisit.RoboRefundVisit = visit;
            }
            return visit;
        }

        private static int _roboRefundPayment = 1;
        private RoboRefundPayment CreateRoboRefundPayment(IList<RoboRefundVisit> visits)
        {
            RoboRefundPayment payment = new RoboRefundPayment()
            {
                RoboRefundPaymentId = _roboRefundPayment++,
                Visits = visits
            };
            foreach (RoboRefundVisit roboRefundVisit in visits)
            {
                roboRefundVisit.RoboRefundPayment = payment;
            }
            return payment;
        }
        
        [Test]
        public void RoboRefund_Entity_SingleFinancePlan_WriteOffAll()
        {
            //To Write off all interest, needed to have paid none.
            List<RoboRefundFinancePlanVisit> roboFpvs = new List<RoboRefundFinancePlanVisit>()
            {
                this.CreateRoboRefundFinancePlanVisit(1.00m, 0m, 0m, 0m, new List<RoboRefundFinancePlanVisitRecalculatedInterest>(){ new RoboRefundFinancePlanVisitRecalculatedInterest(){NewInterest = 0.00m, OldInterest = 1.00m}}, null),
            };
            List<RoboRefundVisit> roboVisits = new List<RoboRefundVisit>()
            {
                this.CreateRoboRefundVisit(_visitId++, roboFpvs),
            };
            RoboRefundPayment payment = this.CreateRoboRefundPayment(roboVisits);

            Assert.AreEqual(0m, payment.AmountToReallocateFromInterestToPrincipalSum);
            Assert.AreEqual(1.00m, payment.AmountOfInterestToWriteOffSum);
            
        }
        [Test]
        public void RoboRefund_Entity_SingleFinancePlan_WriteOffSome_ReallocateSome()
        {
            //Had to have paid some and have some left over unpaid
            List<RoboRefundFinancePlanVisit> roboFpvs = new List<RoboRefundFinancePlanVisit>()
            {
                this.CreateRoboRefundFinancePlanVisit(1.00m, -0.50m, 0m, 0m, new List<RoboRefundFinancePlanVisitRecalculatedInterest>(){ new RoboRefundFinancePlanVisitRecalculatedInterest(){NewInterest = 0.00m, OldInterest = 1.00m}}, null),
            };
            List<RoboRefundVisit> roboVisits = new List<RoboRefundVisit>()
            {
                this.CreateRoboRefundVisit(_visitId++, roboFpvs),
            };
            RoboRefundPayment payment = this.CreateRoboRefundPayment(roboVisits);

            Assert.AreEqual(-0.5m, payment.AmountToReallocateFromInterestToPrincipalSum);
            Assert.AreEqual(0.50m, payment.AmountOfInterestToWriteOffSum);
        }
        
        [Test]
        public void RoboRefund_Entity_SingleFinancePlan_ReallocateAll()
        {
            //All interest needs to be paid
            List<RoboRefundFinancePlanVisit> roboFpvs = new List<RoboRefundFinancePlanVisit>()
            {
                this.CreateRoboRefundFinancePlanVisit(1.00m, -1.00m, 0m, 0m, new List<RoboRefundFinancePlanVisitRecalculatedInterest>(){ new RoboRefundFinancePlanVisitRecalculatedInterest(){NewInterest = 0.00m, OldInterest = 1.00m}}, null),
            };
            List<RoboRefundVisit> roboVisits = new List<RoboRefundVisit>()
            {
                this.CreateRoboRefundVisit(_visitId++, roboFpvs),
            };
            RoboRefundPayment payment = this.CreateRoboRefundPayment(roboVisits);

            Assert.AreEqual(-1.0m, payment.AmountToReallocateFromInterestToPrincipalSum);
            Assert.AreEqual(0.0m, payment.AmountOfInterestToWriteOffSum);
        }
        
        [Test]
        public void RoboRefund_Entity_FinancePlan_ThenCombo_WriteOffAllOnCombo_ReallocateSomeOnOrig_NoneMovedFromOrig()
        {
            //Paid some on orig
            //Didnt pay any of the combo
            
            //Behavior, All paid on orig, None paid on combo
            RoboRefundFinancePlanVisit firstClosedFpv = this.CreateRoboRefundFinancePlanVisit(1.00m, -1.0m, 0m, 0m, new List<RoboRefundFinancePlanVisitRecalculatedInterest>() {new RoboRefundFinancePlanVisitRecalculatedInterest() {NewInterest = 0.00m, OldInterest = 1.00m}}, null, true);

            List<RoboRefundFinancePlanVisit> roboFpvs = new List<RoboRefundFinancePlanVisit>()
            {
                firstClosedFpv,
                this.CreateRoboRefundFinancePlanVisit(1.00m, 0m, 0m, 0m, new List<RoboRefundFinancePlanVisitRecalculatedInterest>() {new RoboRefundFinancePlanVisitRecalculatedInterest() {NewInterest = 0.00m, OldInterest = 1.00m}}, firstClosedFpv.FinancePlanVisitId, false),
            };
            List<RoboRefundVisit> roboVisits = new List<RoboRefundVisit>()
            {
                this.CreateRoboRefundVisit(_visitId++, roboFpvs),
            };
            RoboRefundPayment payment = this.CreateRoboRefundPayment(roboVisits);

            //1 buck back for paying close fp
            Assert.AreEqual(-1.0m, payment.AmountToReallocateFromInterestToPrincipalSum);
            //1 buck writeoff for active
            Assert.AreEqual(1.0m, payment.AmountOfInterestToWriteOffSum);
            
            //Since it's paid on the first, shouldnt try to move to parent.
            Assert.AreEqual(0m, firstClosedFpv.InterestToMoveToParentFinancePlan);
            
            
        }
        
        [Test]
        public void RoboRefund_Entity_FinancePlan_ThenCombo_WriteOffAllOnCombo_ReallocateSomeOnOrig_SomeMovedFromOrig()
        {
            //Paid some on orig
            //Didnt pay any one the combo
            
            //Behavior, Some paid on orig, all paid on combo
            RoboRefundFinancePlanVisit firstClosedFpv = this.CreateRoboRefundFinancePlanVisit(1.00m, -0.5m, 0m, 0m, new List<RoboRefundFinancePlanVisitRecalculatedInterest>() {new RoboRefundFinancePlanVisitRecalculatedInterest() {NewInterest = 0.00m, OldInterest = 1.00m}}, null, true);

            RoboRefundFinancePlanVisit secondOpenFpv = this.CreateRoboRefundFinancePlanVisit(1.00m, 0.00m, 0m, 0.5m, new List<RoboRefundFinancePlanVisitRecalculatedInterest>() {new RoboRefundFinancePlanVisitRecalculatedInterest() {NewInterest = 0.00m, OldInterest = 1.00m}}, firstClosedFpv.FinancePlanVisitId, false);
            List<RoboRefundFinancePlanVisit> roboFpvs = new List<RoboRefundFinancePlanVisit>()
            {
                firstClosedFpv,
                secondOpenFpv,
            };
            List<RoboRefundVisit> roboVisits = new List<RoboRefundVisit>()
            {
                this.CreateRoboRefundVisit(_visitId++, roboFpvs),
            };
            RoboRefundPayment payment = this.CreateRoboRefundPayment(roboVisits);

            //.50c back for paying close fp, since that's all that was paid
            Assert.AreEqual(-0.5m, payment.AmountToReallocateFromInterestToPrincipalSum);
            //1 buck writeoff for active
            Assert.AreEqual(1.5m, payment.AmountOfInterestToWriteOffSum);
            
            //Since it's paid on the first, shouldnt try to move to parent.
            Assert.AreEqual(-.5m, firstClosedFpv.InterestToMoveToParentFinancePlan);
            
            Assert.AreEqual(-0.0m, secondOpenFpv.AmountToReallocateFromInterestToPrincipal);
            Assert.AreEqual(1.5m, secondOpenFpv.AmountOfInterestToWriteOff);
        }
        
        [Test]
        public void RoboRefund_Entity_FinancePlan_ThenCombo_WriteOffSomeOnCombo_ReallocateSomeOnOrig()
        {
            //Paid some on orig
            //Paid some any one the combo
            RoboRefundFinancePlanVisit firstClosedFpv = this.CreateRoboRefundFinancePlanVisit(1.00m, -0.5m, 0m, 0m, new List<RoboRefundFinancePlanVisitRecalculatedInterest>() {new RoboRefundFinancePlanVisitRecalculatedInterest() {NewInterest = 0.00m, OldInterest = 1.00m}}, null, true);

            RoboRefundFinancePlanVisit secondOpenFpv = this.CreateRoboRefundFinancePlanVisit(1.00m, -1.00m, 0m, 0.5m, new List<RoboRefundFinancePlanVisitRecalculatedInterest>() {new RoboRefundFinancePlanVisitRecalculatedInterest() {NewInterest = 0.00m, OldInterest = 1.00m}}, firstClosedFpv.FinancePlanVisitId, false);
            List<RoboRefundFinancePlanVisit> roboFpvs = new List<RoboRefundFinancePlanVisit>()
            {
                firstClosedFpv,
                secondOpenFpv,
            };
            List<RoboRefundVisit> roboVisits = new List<RoboRefundVisit>()
            {
                this.CreateRoboRefundVisit(_visitId++, roboFpvs),
            };
            RoboRefundPayment payment = this.CreateRoboRefundPayment(roboVisits);

            //.50c back for paying close fp, since that's all that was paid
            Assert.AreEqual(-1.5m, payment.AmountToReallocateFromInterestToPrincipalSum);
            //1 buck writeoff for active
            Assert.AreEqual(0.5m, payment.AmountOfInterestToWriteOffSum);
            
            //Since it's paid on the first, shouldnt try to move to parent.
            Assert.AreEqual(-.5m, firstClosedFpv.InterestToMoveToParentFinancePlan);
            
            Assert.AreEqual(-1.0m, secondOpenFpv.AmountToReallocateFromInterestToPrincipal);
            Assert.AreEqual(0.5m, secondOpenFpv.AmountOfInterestToWriteOff);
        }
        
        [Test]
        public void RoboRefund_Entity_FinancePlan_ThenCombo_WriteOffAllOnCombo()
        {
            //None was paid on the first and on second
            //Behavior, orig was written off on first, then moved to second, all written off
            RoboRefundFinancePlanVisit firstClosedFpv = this.CreateRoboRefundFinancePlanVisit(1.00m, 0.0m, 0m, 0m, new List<RoboRefundFinancePlanVisitRecalculatedInterest>() {new RoboRefundFinancePlanVisitRecalculatedInterest() {NewInterest = 0.00m, OldInterest = 1.00m}}, null, true);

            RoboRefundFinancePlanVisit secondOpenFpv = this.CreateRoboRefundFinancePlanVisit(1.00m, 0.00m, 0m, 1.0m, new List<RoboRefundFinancePlanVisitRecalculatedInterest>() {new RoboRefundFinancePlanVisitRecalculatedInterest() {NewInterest = 0.00m, OldInterest = 1.00m}}, firstClosedFpv.FinancePlanVisitId, false);
            List<RoboRefundFinancePlanVisit> roboFpvs = new List<RoboRefundFinancePlanVisit>()
            {
                firstClosedFpv,
                secondOpenFpv,
            };
            List<RoboRefundVisit> roboVisits = new List<RoboRefundVisit>()
            {
                this.CreateRoboRefundVisit(_visitId++, roboFpvs),
            };
            RoboRefundPayment payment = this.CreateRoboRefundPayment(roboVisits);

            //.50c back for paying close fp, since that's all that was paid
            Assert.AreEqual(0.0m, payment.AmountToReallocateFromInterestToPrincipalSum);
            //1 buck writeoff for active
            Assert.AreEqual(2.0m, payment.AmountOfInterestToWriteOffSum);
            
            //Since it's paid on the first, shouldnt try to move to parent.
            Assert.AreEqual(-1.0m, firstClosedFpv.InterestToMoveToParentFinancePlan);
            
            Assert.AreEqual(0.0m, secondOpenFpv.AmountToReallocateFromInterestToPrincipal);
            Assert.AreEqual(2.0m, secondOpenFpv.AmountOfInterestToWriteOff);
        }
        
        [Test]
        public void RoboRefund_Entity_FinancePlan_AssessesMoreInterestThanBefore()
        {
            //None was paid on the first and on second
            //Behavior, orig was written off on first, then moved to second, all written off
            RoboRefundFinancePlanVisit firstFpv = this.CreateRoboRefundFinancePlanVisit(1.00m, 0.0m, 0m, 0m, new List<RoboRefundFinancePlanVisitRecalculatedInterest>() {new RoboRefundFinancePlanVisitRecalculatedInterest() {NewInterest = 2.00m, OldInterest = 1.00m}}, null, false);

            List<RoboRefundFinancePlanVisit> roboFpvs = new List<RoboRefundFinancePlanVisit>()
            {
                firstFpv,
            };
            List<RoboRefundVisit> roboVisits = new List<RoboRefundVisit>()
            {
                this.CreateRoboRefundVisit(_visitId++, roboFpvs),
            };
            RoboRefundPayment payment = this.CreateRoboRefundPayment(roboVisits);

            //Even though the interest went up in the calculation dont add more interest
            Assert.AreEqual(0.0m, payment.AmountToReallocateFromInterestToPrincipalSum);
            Assert.AreEqual(0.0m, payment.AmountOfInterestToWriteOffSum);
            
            //Definitely should pass this on to the parent.
            Assert.AreEqual(0.0m, firstFpv.InterestToMoveToParentFinancePlan);

        }
    }

    public class FinancePlanTestInfo
    {
        public FinancePlan FinancePlan { get; set; }

        public IList<Tuple<Visit, IList<VisitTransaction>>> VisitsTuples { get; set; }

        public IList<Visit> Visits
        {
            get { return this.VisitsTuples.Select(x => x.Item1).ToList(); }
        }
    }

}


﻿namespace Ivh.Domain.PaymentMethod
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.Tests.Helpers.PaymentMethod;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.FinanceManagement;
    using Guarantor.Entities;
    using FinanceManagement.Entities;
    using FinanceManagement.FinancePlan.Entities;
    using FinanceManagement.Interfaces;
    using FinanceManagement.Payment.Entities;
    using FinanceManagement.Services;
    using Moq;
    using NUnit.Framework;
    using User.Entities;

    [TestFixture]
    public class PaymentMethodServiceTests : DomainTestBase
    {
        private VisitPayUser _visitPayUser;
        private Guarantor _guarantor;

        private IList<FinancePlan> _financePlans;
        private IList<Payment> _payments;
        private IList<PaymentMethod> _paymentMethods;
        private IList<PaymentMethodEvent> _paymentMethodEvents;

        [SetUp]
        public void Setup()
        {
            this._visitPayUser = new VisitPayUser {VisitPayUserId = 1};
            this._guarantor = new Guarantor {VpGuarantorId = 1000, User = this._visitPayUser};

            this._financePlans = new List<FinancePlan>();
            this._payments = new List<Payment>();
            this._paymentMethods = new List<PaymentMethod>();
            this._paymentMethodEvents = new List<PaymentMethodEvent>();
        }

        private PaymentMethodsServiceMockBuilder CreateBuilder()
        {
            List<PaymentMethodTypeEnum> acceptedTypes = EnumHelper<PaymentMethodTypeEnum>.GetValues(PaymentMethodTypeEnumCategory.Card, PaymentMethodTypeEnumCategory.Ach).ToList();

            PaymentMethodsServiceMockBuilder builder = new PaymentMethodsServiceMockBuilder().WithAcceptedPaymentTypes(acceptedTypes).Setup(b =>
            {
                b.DomainServiceCommonServiceMockBuilder.EnableAllFeatures();
                b.GuarantorServiceMock.Setup(t => t.GetGuarantor(It.IsAny<int>())).Returns(() => this._guarantor);
                b.FinancePlanRepositoryMock.Setup(t => t.GetAllActiveFinancePlanIds(It.IsAny<int>())).Returns(() => this._financePlans.Where(x => EnumHelper<FinancePlanStatusEnum>.Contains(x.FinancePlanStatus.FinancePlanStatusEnum, FinancePlanStatusEnumCategory.Pending, FinancePlanStatusEnumCategory.Active)).Select(x => x.FinancePlanId).ToList());
                b.PaymentRepositoryMock.Setup(t => t.GetScheduledPayments(It.IsAny<int>(), It.IsAny<DateTime?>())).Returns(() => this._payments.Where(x => x.PaymentStatus == PaymentStatusEnum.ActivePending || x.PaymentStatus == PaymentStatusEnum.ActiveGatewayError).ToList());
                b.PaymentRepositoryMock.Setup(t => t.GetPaymentMethodIdsAssociatedWithPayments(It.IsAny<int>(), It.IsAny<IList<PaymentStatusEnum>>())).Returns(() => this._payments.Where(x => x.PaymentStatus == PaymentStatusEnum.ActivePending || x.PaymentStatus == PaymentStatusEnum.ActiveGatewayError).Select(x => x.PaymentMethod.PaymentMethodId).ToList());
                b.PaymentMethodsRepositoryMock.Setup(t => t.GetPaymentMethods(It.IsAny<int>(), It.IsAny<bool>())).Returns<int, bool>((vpGuarantorId, includingInactive) =>
                {
                    List<PaymentMethod> list = this._paymentMethods.ToList();
                    return includingInactive ? list : list.Where(x => x.IsActive).ToList();
                });
                b.PaymentMethodsRepositoryMock.Setup(t => t.GetPaymentMethodsCount(It.IsAny<int>())).Returns(() => this._paymentMethods.Count(x => x.IsActive));
                b.PaymentMethodsRepositoryMock.Setup(t => t.GetPrimaryPaymentMethod(It.IsAny<int>())).Returns(() => this._paymentMethods.FirstOrDefault(x => x.IsActive && x.IsPrimary));
                b.PaymentMethodsRepositoryMock.Setup(t => t.GetPrimaryPaymentMethodStateless(It.IsAny<int>())).Returns(() => this._paymentMethods.FirstOrDefault(x => x.IsActive && x.IsPrimary));
                b.PaymentMethodsRepositoryMock.Setup(t => t.GetById(It.IsAny<int>())).Returns((int paymentMethodId) => this._paymentMethods.FirstOrDefault(x => x.PaymentMethodId == paymentMethodId));
                b.PaymentMethodsRepositoryMock.Setup(t => t.GetByIdStateless(It.IsAny<int>())).Returns((int paymentMethodId) => this._paymentMethods.FirstOrDefault(x => x.PaymentMethodId == paymentMethodId));
                b.PaymentMethodsRepositoryMock.Setup(t => t.InsertOrUpdate(It.IsAny<PaymentMethod>())).Callback<PaymentMethod>(paymentMethod =>
                {
                    PaymentMethod found = this._paymentMethods.FirstOrDefault(x => x.PaymentMethodId == paymentMethod.PaymentMethodId);
                    if (found == null)
                    {
                        int newId = (this._paymentMethods.Count == 0 ? 0 : this._paymentMethods.Max(x => x.PaymentMethodId)) + 1;
                        paymentMethod.PaymentMethodId = newId;
                        this._paymentMethods.Add(paymentMethod);
                        return;
                    }

                    this._paymentMethods[this._paymentMethods.IndexOf(found)] = paymentMethod;
                });
                b.PaymentMethodEventRepositoryMock.Setup(t => t.InsertOrUpdate(It.IsAny<PaymentMethodEvent>())).Callback<PaymentMethodEvent>(paymentMethodEvent => { this._paymentMethodEvents.Add(paymentMethodEvent); });
                b.PaymentMethodAccountTypeRepositoryMock.Setup(t => t.GetById(It.IsAny<int>())).Returns(() => new PaymentMethodAccountType
                {
                    PaymentMethodAccountTypeId = 0
                });
                b.PaymentMethodProviderTypeRepositoryMock.Setup(t => t.GetById(It.IsAny<int>())).Returns(() => new PaymentMethodProviderType
                {
                    PaymentMethodProviderTypeId = 0
                });
            });

            return builder;
        }

        [TestCase(true)]
        [TestCase(false)]
        public void PaymentMethod_Save(bool suppressNotifications)
        {
            {
                PaymentMethodsServiceMockBuilder builder = this.CreateBuilder();
                IPaymentMethodsService svc = builder.CreateService();

                // save as primary with no existing payment methods
                // first payment method should default to primary
                svc.Save(this.CreatePaymentMethod(false, true), this._visitPayUser.VisitPayUserId, "123", suppressNotifications);

                Assert.AreEqual(1, this._paymentMethods.Count);
                Assert.NotNull(svc.GetPrimaryPaymentMethod(this._guarantor.VpGuarantorId, false));
                Assert.AreEqual(1, this._paymentMethodEvents.Count(x => x.PaymentMethodStatus == PaymentMethodStatusEnum.Activated));
                Assert.AreEqual(1, this._paymentMethodEvents.Count(x => x.PaymentMethodStatus == PaymentMethodStatusEnum.SetPrimary));
                this._paymentMethods[0].PaymentMethodId = 1;
                
                builder.MetricsProviderMock.Verify(x => x.Increment(Metrics.Increment.PaymentMethods.NewPaymentMethod, It.IsAny<int>(), It.IsAny<double>(), It.IsAny<string[]>()), Times.Once);

                if (suppressNotifications)
                {
                    // don't send
                    builder.DomainServiceCommonServiceMockBuilder.BusMock.Verify(x => x.PublishMessage(It.IsAny<SendPrimaryPaymentMethodChangeEmailMessage>()), Times.Never);
                    builder.DomainServiceCommonServiceMockBuilder.BusMock.Verify(x => x.PublishMessage(It.IsAny<SendPaymentMethodChangeEmailMessage>()), Times.Never);
                }
                else 
                {
                    // shouldn't send for first payment method
                    builder.DomainServiceCommonServiceMockBuilder.BusMock.Verify(x => x.PublishMessage(It.IsAny<SendPrimaryPaymentMethodChangeEmailMessage>()), Times.Never);
                    builder.DomainServiceCommonServiceMockBuilder.BusMock.Verify(x => x.PublishMessage(It.IsAny<SendPaymentMethodChangeEmailMessage>()), Times.Once);
                }
            }
            
            {
                PaymentMethodsServiceMockBuilder builder = this.CreateBuilder();
                IPaymentMethodsService svc = builder.CreateService();

                // save another, as primary
                svc.Save(this.CreatePaymentMethod(true, true), this._visitPayUser.VisitPayUserId, "123", suppressNotifications);

                Assert.AreEqual(2, this._paymentMethods.Count);
                Assert.AreEqual(1, this._paymentMethodEvents.Count(x => x.PaymentMethodStatus == PaymentMethodStatusEnum.Modified));
                Assert.AreEqual(2, this._paymentMethodEvents.Count(x => x.PaymentMethodStatus == PaymentMethodStatusEnum.Activated));
                Assert.AreEqual(2, this._paymentMethodEvents.Count(x => x.PaymentMethodStatus == PaymentMethodStatusEnum.SetPrimary));
                this._paymentMethods[1].PaymentMethodId = 2;

                builder.MetricsProviderMock.Verify(x => x.Increment(Metrics.Increment.PaymentMethods.NewPaymentMethod, It.IsAny<int>(), It.IsAny<double>(), It.IsAny<string[]>()), Times.Once);
                
                if (suppressNotifications)
                {
                    // don't send
                    builder.DomainServiceCommonServiceMockBuilder.BusMock.Verify(x => x.PublishMessage(It.IsAny<SendPrimaryPaymentMethodChangeEmailMessage>()), Times.Never);
                    builder.DomainServiceCommonServiceMockBuilder.BusMock.Verify(x => x.PublishMessage(It.IsAny<SendPaymentMethodChangeEmailMessage>()), Times.Never);
                }
                else
                {
                    // should send for new primary when primary exists
                    builder.DomainServiceCommonServiceMockBuilder.BusMock.Verify(x => x.PublishMessage(It.IsAny<SendPrimaryPaymentMethodChangeEmailMessage>()), Times.Once);
                    builder.DomainServiceCommonServiceMockBuilder.BusMock.Verify(x => x.PublishMessage(It.IsAny<SendPaymentMethodChangeEmailMessage>()), Times.Never);
                }

                // check that we still have a primary, and it's the new one
                PaymentMethod primary1 = svc.GetPrimaryPaymentMethod(this._guarantor.VpGuarantorId, false);
                Assert.IsTrue(primary1 != null && primary1.PaymentMethodId == 2);
            }
            
            {
                PaymentMethodsServiceMockBuilder builder = this.CreateBuilder();
                IPaymentMethodsService svc = builder.CreateService();

                // save another, not primary
                svc.Save(this.CreatePaymentMethod(false, true), this._visitPayUser.VisitPayUserId, "123", suppressNotifications);

                Assert.IsTrue(this._paymentMethods.Count == 3);
                Assert.IsTrue(this._paymentMethodEvents.Count(x => x.PaymentMethodStatus == PaymentMethodStatusEnum.Modified) == 1);
                Assert.IsTrue(this._paymentMethodEvents.Count(x => x.PaymentMethodStatus == PaymentMethodStatusEnum.Activated) == 3);
                Assert.IsTrue(this._paymentMethodEvents.Count(x => x.PaymentMethodStatus == PaymentMethodStatusEnum.SetPrimary) == 2);
                this._paymentMethods[2].PaymentMethodId = 3;

                builder.MetricsProviderMock.Verify(x => x.Increment(Metrics.Increment.PaymentMethods.NewPaymentMethod, It.IsAny<int>(), It.IsAny<double>(), It.IsAny<string[]>()), Times.Once);
                
                if (suppressNotifications)
                {
                    // don't send
                    builder.DomainServiceCommonServiceMockBuilder.BusMock.Verify(x => x.PublishMessage(It.IsAny<SendPrimaryPaymentMethodChangeEmailMessage>()), Times.Never);
                    builder.DomainServiceCommonServiceMockBuilder.BusMock.Verify(x => x.PublishMessage(It.IsAny<SendPaymentMethodChangeEmailMessage>()), Times.Never);
                }
                else
                {
                    // not primary, shouldn't send
                    builder.DomainServiceCommonServiceMockBuilder.BusMock.Verify(x => x.PublishMessage(It.IsAny<SendPrimaryPaymentMethodChangeEmailMessage>()), Times.Never);
                    // payment method change, should send
                    builder.DomainServiceCommonServiceMockBuilder.BusMock.Verify(x => x.PublishMessage(It.IsAny<SendPaymentMethodChangeEmailMessage>()), Times.Once);
                }

                // check that we still only have a primary, and it's still #2
                PaymentMethod primary2 = svc.GetPrimaryPaymentMethod(this._guarantor.VpGuarantorId, false);
                Assert.IsTrue(primary2 != null && primary2.PaymentMethodId == 2);
            }
        }
        
        [TestCase(PaymentMethodTypeEnum.AchChecking, false)]
        [TestCase(PaymentMethodTypeEnum.AchSavings, false)]
        [TestCase(PaymentMethodTypeEnum.AmericanExpress, true)]
        [TestCase(PaymentMethodTypeEnum.Discover, true)]
        [TestCase(PaymentMethodTypeEnum.Mastercard, true)]
        [TestCase(PaymentMethodTypeEnum.Visa, true)]
        public void PaymentMethod_Save_ByClientUser_WithNoExistingPaymentMethods(PaymentMethodTypeEnum paymentMethodType, bool expectPrimary)
        {
            const int clientUserId = 999;

            PaymentMethodsServiceMockBuilder builder = this.CreateBuilder();
            IPaymentMethodsService svc = builder.CreateService();

            // save as primary with no existing payment methods
            svc.Save(this.CreatePaymentMethod(false, true, paymentMethodType), clientUserId, null, false);
            
            if (expectPrimary)
            {
                // first card account should default to primary
                Assert.NotNull(svc.GetPrimaryPaymentMethod(this._guarantor.VpGuarantorId, false));
            }
            else
            {
                // first bank account should not default to primary
                Assert.Null(svc.GetPrimaryPaymentMethod(this._guarantor.VpGuarantorId, false));
            }
        }
        
        [TestCase(PaymentMethodTypeEnum.AchChecking, false)]
        [TestCase(PaymentMethodTypeEnum.AchSavings, false)]
        [TestCase(PaymentMethodTypeEnum.AmericanExpress, true)]
        [TestCase(PaymentMethodTypeEnum.Discover, true)]
        [TestCase(PaymentMethodTypeEnum.Mastercard, true)]
        [TestCase(PaymentMethodTypeEnum.Visa, true)]
        public void PaymentMethod_Save_ByClientUser_WithExistingPrimary(PaymentMethodTypeEnum paymentMethodType, bool expectPrimary)
        {
            const int clientUserId = 999;

            PaymentMethodsServiceMockBuilder builder = this.CreateBuilder();
            IPaymentMethodsService svc = builder.CreateService();

            svc.Save(this.CreatePaymentMethod(true, true, paymentMethodType), this._visitPayUser.VisitPayUserId, null, false);
            PaymentMethod primary1 = svc.GetPrimaryPaymentMethod(this._guarantor.VpGuarantorId, false);
            Assert.NotNull(primary1);

            // ensure a client saving a primary does not change it's primary flag
            svc.Save(primary1, clientUserId, null, false);
            PaymentMethod primary2 = svc.GetPrimaryPaymentMethod(this._guarantor.VpGuarantorId, false);
            Assert.NotNull(primary2);

            // ensure a client can't add a primary ACH
            PaymentMethodResponse response = svc.Save(this.CreatePaymentMethod(true, true, paymentMethodType), clientUserId, null, false);
            if (expectPrimary)
            {
                Assert.True(response.PaymentMethod.IsPrimary);
            }
            else
            {
                Assert.False(response.PaymentMethod.IsPrimary);
            }
        }

        [Test]
        public void PaymentMethod_Delete_MultipleMethods()
        {
            this._paymentMethods.Add(this.CreatePaymentMethod(true));
            this._paymentMethods[0].PaymentMethodId = 1;

            this._paymentMethods.Add(this.CreatePaymentMethod(false));
            this._paymentMethods[1].PaymentMethodId = 2;

            void Run(PaymentMethod paymentMethod, bool deleted)
            {
                PaymentMethodsServiceMockBuilder builder = this.CreateBuilder();
                IPaymentMethodsService svc = builder.CreateService();

                PaymentMethodResponse response = svc.Delete(paymentMethod, this._visitPayUser.VisitPayUserId);

                Assert.AreEqual(deleted, response.IsSuccess);
                if (deleted)
                {
                    Assert.AreEqual(PaymentMethodStatusEnum.Deactivated, this._paymentMethodEvents.Last().PaymentMethodStatus);
                    builder.DomainServiceCommonServiceMockBuilder.BusMock.Verify(x => x.PublishMessage(It.IsAny<SendPaymentMethodChangeEmailMessage>()), Times.Once);
                }
            }

            // cannot delete primary if 1+
            Run(this._paymentMethods[0], false);

            // can delete second one
            Run(this._paymentMethods[1], true);

            // can now delete the primary
            Run(this._paymentMethods[0], true);
        }

        [Test]
        public void PaymentMethod_Delete_CheckPayments()
        {            
            void Run(bool deleted)
            {
                PaymentMethodsServiceMockBuilder builder = this.CreateBuilder();
                IPaymentMethodsService svc = builder.CreateService();

                PaymentMethodResponse response = svc.Delete(this._paymentMethods[0], this._visitPayUser.VisitPayUserId);

                Assert.AreEqual(deleted, response.IsSuccess);
                if (deleted)
                {
                    Assert.AreEqual(PaymentMethodStatusEnum.Deactivated, this._paymentMethodEvents.Last().PaymentMethodStatus);
                    builder.DomainServiceCommonServiceMockBuilder.BusMock.Verify(x => x.PublishMessage(It.IsAny<SendPaymentMethodChangeEmailMessage>()), Times.Once);
                }

                // reset payment method
                this._paymentMethods[0].IsActive = true;
            }

            // create primary payment method
            this._paymentMethods.Add(this.CreatePaymentMethod(true));

            // scheduled payment
            this._payments.Add(this.CreatePayment(this._paymentMethods[0], PaymentStatusEnum.ActivePending, PaymentTypeEnum.ManualScheduledCurrentNonFinancedBalanceInFull));
            Run(false);

            // gateway error
            this._payments.Clear();
            this._payments.Add(this.CreatePayment(this._paymentMethods[0], PaymentStatusEnum.ActiveGatewayError, PaymentTypeEnum.ManualScheduledCurrentNonFinancedBalanceInFull));
            Run(false);

            // payment succeeds
            this._payments.Clear();
            this._payments.Add(this.CreatePayment(this._paymentMethods[0], PaymentStatusEnum.ClosedPaid, PaymentTypeEnum.ManualScheduledCurrentNonFinancedBalanceInFull));
            Run(true);

            // no payments
            this._payments.Clear();
            Run(true);
        }

        [Test]
        public void PaymentMethod_Delete_CheckFinancePlans()
        {
            void Run(bool deleted)
            {
                PaymentMethodsServiceMockBuilder builder = this.CreateBuilder();
                IPaymentMethodsService svc = builder.CreateService();

                PaymentMethodResponse response = svc.Delete(this._paymentMethods[0], this._visitPayUser.VisitPayUserId);

                Assert.AreEqual(deleted, response.IsSuccess);
                if (deleted)
                {
                    Assert.AreEqual(PaymentMethodStatusEnum.Deactivated, this._paymentMethodEvents.Last().PaymentMethodStatus);
                    builder.DomainServiceCommonServiceMockBuilder.BusMock.Verify(x => x.PublishMessage(It.IsAny<SendPaymentMethodChangeEmailMessage>()), Times.Once);
                }

                // reset payment method
                this._paymentMethods[0].IsActive = true;
            }

            // create primary payment method
            this._paymentMethods.Add(this.CreatePaymentMethod(true));

            // pending
            EnumHelper<FinancePlanStatusEnum>.GetValues(FinancePlanStatusEnumCategory.Pending, FinancePlanStatusEnumCategory.Active).ToList().ForEach(status =>
            {
                this._financePlans.Clear();
                this._financePlans.Add(this.CreateFinancePlan(status));
                Run(false);
            });

            // closed
            EnumHelper<FinancePlanStatusEnum>.GetValues(FinancePlanStatusEnumCategory.Closed).ToList().ForEach(status =>
            {
                this._financePlans.Clear();
                this._financePlans.Add(this.CreateFinancePlan(status));
                Run(true);
            });

            // mixed
            this._financePlans.Clear();
            this._financePlans.Add(this.CreateFinancePlan(FinancePlanStatusEnum.PaidInFull));
            this._financePlans.Add(this.CreateFinancePlan(FinancePlanStatusEnum.GoodStanding));
            Run(false);

            // no payments
            this._financePlans.Clear();
            Run(true);
        }

        [Test]
        public void PaymentMethods_GetWithDisabledFeatures()
        {
            // add one of each type
            EnumHelper<PaymentMethodTypeEnum>.GetValuesNot("Group").ToList().ForEach(paymentMethodType =>
            {
                this._paymentMethods.Add(this.CreatePaymentMethod(false, true, paymentMethodType));
            });
            
            PaymentMethodsServiceMockBuilder builder = this.CreateBuilder();

            // set accepted types - all
            List<PaymentMethodTypeEnum> bothTypes = EnumHelper<PaymentMethodTypeEnum>.GetValues(PaymentMethodTypeEnumCategory.Card, PaymentMethodTypeEnumCategory.Ach).ToList();
            PaymentMethodsService svc = builder.WithAcceptedPaymentTypes(bothTypes).CreateService();
            Assert.AreEqual(bothTypes.Count, svc.GetPaymentMethods(this._guarantor.VpGuarantorId, this._visitPayUser.VisitPayUserId).Count);

            // set accepted types - ach
            List<PaymentMethodTypeEnum> achTypes = EnumHelper<PaymentMethodTypeEnum>.GetValues(PaymentMethodTypeEnumCategory.Ach).ToList();
            svc = builder.WithAcceptedPaymentTypes(achTypes).CreateService();
            Assert.AreEqual(achTypes.Count, svc.GetPaymentMethods(this._guarantor.VpGuarantorId, this._visitPayUser.VisitPayUserId).Count);

            // set accepted types - card
            List<PaymentMethodTypeEnum> cardTypes = EnumHelper<PaymentMethodTypeEnum>.GetValues(PaymentMethodTypeEnumCategory.Card).ToList();
            svc = builder.WithAcceptedPaymentTypes(cardTypes).CreateService();
            Assert.AreEqual(cardTypes.Count, svc.GetPaymentMethods(this._guarantor.VpGuarantorId, this._visitPayUser.VisitPayUserId).Count);
        }

        [Test]
        public void PaymentMethod_ValidatePrimary()
        {            
            PaymentMethodsServiceMockBuilder builder = this.CreateBuilder();
            IPaymentMethodsService svc = builder.CreateService();

            void Run(bool success, PaymentMethod pm)
            {
                PaymentMethodResponse response = svc.ValidatePrimaryPaymentMethod(pm);
                Assert.AreEqual(success, response.IsSuccess);
            }

            // ach should pass
            Run(true, this.CreatePaymentMethod(true, true, PaymentMethodTypeEnum.AchChecking));

            // not expired card should pass
            PaymentMethod paymentMethod = this.CreatePaymentMethod(true, true);
            paymentMethod.ExpDate = DateTime.UtcNow.AddMonths(1).ToExpDate();
            Run(true, paymentMethod);

            // expired card should fail
            paymentMethod = this.CreatePaymentMethod(true, true);
            this._paymentMethods.Add(paymentMethod);
            paymentMethod.ExpDate = DateTime.UtcNow.AddMonths(-1).ToExpDate();
            Run(false, paymentMethod);
        }

        [Test]
        public void PaymentMethod_Expiration()
        {
            PaymentMethod paymentMethod = new PaymentMethod
            {
                ExpDate = "0419",
                PaymentMethodType = PaymentMethodTypeEnum.Visa
            };

            Assert.IsTrue(paymentMethod.IsNearExpiry(new DateTime(2019, 05, 01)));
            Assert.IsTrue(paymentMethod.IsNearExpiry(new DateTime(2019, 05, 01).AddDays(1)));
            Assert.IsFalse(paymentMethod.IsNearExpiry(new DateTime(2019, 05, 01).AddDays(-1)));

            Assert.IsTrue(new PaymentMethod
            {
                ExpDate = DateTime.UtcNow.AddMonths(-1).ToExpDate(),
                PaymentMethodType = PaymentMethodTypeEnum.Visa
            }.IsExpired);
            
            Assert.IsFalse(new PaymentMethod
            {
                ExpDate = DateTime.UtcNow.ToExpDate(),
                PaymentMethodType = PaymentMethodTypeEnum.Visa
            }.IsExpired);

            Assert.IsTrue(new PaymentMethod
            {
                ExpDate = DateTime.UtcNow.ToExpDate(),
                PaymentMethodType = PaymentMethodTypeEnum.Visa
            }.IsExpiring);

            Assert.IsTrue(new PaymentMethod
            {
                ExpDate = DateTime.UtcNow.AddMonths(1).AddDays(-1).ToExpDate(),
                PaymentMethodType = PaymentMethodTypeEnum.Visa
            }.IsExpiring);

            //As of 3/1/2017 this fails
            //010317 => 0317 + 59 days => 0417 => ExpDateTime is 5/1/2017 => so this is not expiring
            //Assert.IsTrue(new PaymentMethod
            //{
            //    ExpDate = DateTime.ParseExact("01" + DateTime.UtcNow.ToExpDate(), "ddMMyy", null, DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal).AddDays(59).ToExpDate(),
            //    PaymentMethodType = PaymentMethodTypeEnum.Visa
            //}.IsExpiring);

            //Assert.IsFalse(new PaymentMethod
            //{
            //    ExpDate = DateTime.ParseExact("01" + DateTime.UtcNow.ToExpDate(), "ddMMyy", null, DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal).AddDays(60).ToExpDate(),
            //    PaymentMethodType = PaymentMethodTypeEnum.Visa
            //}.IsExpiring);

            //Assert.IsFalse(new PaymentMethod
            //{
            //    ExpDate = DateTime.ParseExact("01" + DateTime.UtcNow.ToExpDate(), "ddMMyy", null, DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal).AddDays(61).ToExpDate(),
            //    PaymentMethodType = PaymentMethodTypeEnum.Visa
            //}.IsExpiring);
        }

        private FinancePlan CreateFinancePlan(FinancePlanStatusEnum financePlanStatus)
        {
            return new FinancePlan
            {
                VpGuarantor = this._guarantor,
                FinancePlanStatus = new FinancePlanStatus
                {
                    FinancePlanStatusId = (int)financePlanStatus
                }
            };
        }

        private Payment CreatePayment(PaymentMethod paymentMethod, PaymentStatusEnum paymentStatus, PaymentTypeEnum paymentType)
        {
            return new Payment
            {
                Guarantor = this._guarantor,
                PaymentMethod = paymentMethod,
                PaymentStatus = paymentStatus,
                PaymentType = paymentType
            };
        }

        private PaymentMethod CreatePaymentMethod(bool isPrimary = false, bool isActive = true, PaymentMethodTypeEnum paymentMethodType = PaymentMethodTypeEnum.Visa)
        {
            return new PaymentMethod
            {
                IsActive = isActive,
                IsPrimary = isPrimary,

                VpGuarantorId = this._guarantor.VpGuarantorId,
                BillingId = Guid.NewGuid().ToString(),
                CreatedUserId = this._visitPayUser.VisitPayUserId,
                CreatedDate = DateTime.UtcNow,
                PaymentMethodType = paymentMethodType
            };
        }
    }
}

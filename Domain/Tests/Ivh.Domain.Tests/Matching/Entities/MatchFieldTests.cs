﻿namespace Ivh.Domain.Matching.Entities
{
    using System;
    using Common.Base.Exceptions;
    using Common.Tests.Builders.Matching;
    using Match;
    using NUnit.Framework;
    using Validators;

    [TestFixture]
    public class MatchFieldTests
    {
        [Test]
        public void SsnIsInvalid()
        {
            foreach (string ssn in new [] {"222220000","222002222","000222222","002281852","042103580","062360749","078051120","095073645","111111111","123456789",
                "128036045","135016629","141186941","165167999","165187999","165207999","165227999","165247999","189092294","212097694","212099999",
                "222222222","306302348","308125070","333333333","444444444","468288779","549241889","555555555","666222222","777777777","888888888",
                "920222222","999999999", null })
            {
                MatchData data = new MatchData()
                {
                    Guarantor = new MatchGuarantorBuilder().WithSsn(ssn)
                };

                Assert.False(Fields.Guarantor.Ssn(null).IsValid(data), $"{ssn} failed");
            }
        }

        [Test]
        public void SsnIsValid()
        {
            foreach (string ssn in new [] {"222221000","222302222","090-22-2222"})
            {
                MatchData data = new MatchData()
                {
                    Guarantor = new MatchGuarantorBuilder().WithSsn(ssn)
                };

                Assert.True(Fields.Guarantor.Ssn(null).IsValid(data), $"{ssn} failed");
            }
        }

        [Test]
        public void AddressIsInvalid()
        {
            foreach (string address1 in new[] { "UNKNOWN","UNK","HOMELESS","NONE", "NONE GIVEN", "5 west" })
            {
                MatchData data = new MatchData()
                {
                    Guarantor = new MatchGuarantorBuilder().WithAddress(address1, "city", "state", "90001")
                };

                Assert.False(Fields.Guarantor.Address1(null).IsValid(data), $"{address1} failed");
            }
        }

        [Test]
        public void AddressIsValid()
        {
            foreach (string address1 in new[] { "123 Test Street."})
            {
                MatchData data = new MatchData()
                {
                    Guarantor = new MatchGuarantorBuilder().WithAddress(address1, "city", "state", "90001")
                };

                Assert.True(Fields.Guarantor.Address1(null).IsValid(data), $"{address1} failed");
            }
        }

        [Test]
        public void CityIsValid()
        {
            foreach (string city in new[] { "Reno" })
            {
                MatchData data = new MatchData()
                {
                    Guarantor = new MatchGuarantorBuilder().WithAddress("address1", city, "state", "90001")
                };

                Assert.True(Fields.Guarantor.City(null).IsValid(data), $"{city} failed");
            }
        }

        [Test]
        public void CityIsInvalid()
        {
            foreach (string city in new[] { "Ra" })
            {
                MatchData data = new MatchData()
                {
                    Guarantor = new MatchGuarantorBuilder().WithAddress("address1", city, "state", "90001")
                };

                Assert.False(Fields.Guarantor.City(null).IsValid(data), $"{city} failed");
            }
        }

        [Test]
        public void DobIsInvalid()
        {
            foreach (DateTime? dob in new DateTime?[] { null })
            {
                MatchData data = new MatchData()
                {
                    Guarantor = new MatchGuarantorBuilder().With(x => x.DOB = dob)
                };

                Assert.False(Fields.Guarantor.DOB(null).IsValid(data), $"{dob} failed");
            }
        }

        [Test]
        public void DobIsValid()
        {
            foreach (DateTime dob in new[] { DateTime.UtcNow.AddYears(-18), DateTime.UtcNow.AddYears(-110) })
            {
                MatchData data = new MatchData()
                {
                    Guarantor = new MatchGuarantorBuilder().With(x => x.DOB = dob)
                };

                Assert.True(Fields.Guarantor.DOB(null).IsValid(data), $"{dob} failed");
            }
        }

        // testing additional validation done at time of match
        [Test]
        public void DobIsInvalidForMatch()
        {
            DobValidator validator = new DobValidator();
            foreach (DateTime? dob in new DateTime?[] { null, DateTime.UtcNow.AddYears(-17), DateTime.UtcNow.AddYears(-111) })
            {
                Assert.False(validator.IsValid(dob), $"{dob} failed");
            }
        }

        [Test]
        public void DobIsValidForMatch()
        {
            DobValidator validator = new DobValidator();
            foreach (DateTime? dob in new DateTime?[] { DateTime.UtcNow.AddYears(-18), DateTime.UtcNow.AddYears(-20), DateTime.UtcNow.AddYears(-110) })
            {
                Assert.True(validator.IsValid(dob), $"{dob} failed");
            }
        }
    }
}

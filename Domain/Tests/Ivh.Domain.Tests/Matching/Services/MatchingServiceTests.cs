﻿namespace Ivh.Domain.Matching.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Utilities.Extensions;
    using Common.Tests.Builders.Matching;
    using Common.Tests.Helpers.Matching;
    using Entities;
    using Ivh.Domain.Matching.Interfaces;
    using Moq;
    using NUnit.Framework;
    using Ivh.Domain.Settings.Entities;
    using Match;
    using Newtonsoft.Json;
    using Sets;
    using System.Collections;
    using System.Linq.Expressions;
    using Common.VisitPay.Enums;
    using Sets.Patterns;

    [TestFixture]
    public class MatchingServiceTests : DomainTestBase
    {
        public static IEnumerable ConsistantMatchStringTestCases()
        {
            Dictionary<string, MatchSetConfigurationRegexEnum> configuration = new Dictionary<string, MatchSetConfigurationRegexEnum>();
            List<Set> set1 = new List<Set> { new GuarantorMatchingSet001(configuration, ApplicationEnum.VisitPay.ToListOfOne()) };
            Dictionary<MatchOptionEnum, string> expectedMatchStrings = new Dictionary<MatchOptionEnum, string>
            {
                [MatchOptionEnum.DobLnameSsn4SskZip] = "GUARANTOR.SOURCESYSTEMKEY=GUARANTORSSK&GUARANTOR.LASTNAME=LASTNAME&GUARANTOR.DOB=19700101&GUARANTOR.SSN4=7890&GUARANTOR.POSTALCODE=83705",
                [MatchOptionEnum.DobLnameSsnZip] = "GUARANTOR.LASTNAME=LASTNAME&GUARANTOR.DOB=19700101&GUARANTOR.SSN=123457890&GUARANTOR.POSTALCODE=83705",
            };

            yield return new ConsistantMatchStringTestCase(
                expectedMatchStrings: expectedMatchStrings,
                guarantor: new MatchGuarantorBuilder().WithDefaults(),
                patients: new List<Patient>
                {
                    new MatchPatientBuilder().WithDob(new DateTime(1990, 1, 1))
                })
            { MatchConfig = set1 };

            yield return new ConsistantMatchStringTestCase(
                expectedMatchStrings: expectedMatchStrings,
                guarantor: new MatchGuarantorBuilder().WithDefaults(),
                patients: new List<Patient>
                {
                    //twins i guess
                    new MatchPatientBuilder().WithDob(new DateTime(1990, 1, 1)),
                    new MatchPatientBuilder().WithDob(new DateTime(1990, 1, 2))
                })
            { MatchConfig = set1 };
        }

        [TestCaseSource(nameof(ConsistantMatchStringTestCases))]
        public void ProcessMatchUpdates_GeneratesConsistantMatchString(ConsistantMatchStringTestCase testCase)
        {
            IList<MatchRegistry> matches = new List<MatchRegistry>();
            IMatchingService service = this.CreateService(matches, testCase.MatchConfig);
            MatchDataGroup matchDataGroup = new MatchDataGroupBuilder().WithGuarantor(testCase.Guarantor).WithPatients(testCase.Patients);

            service.ProcessMatchUpdates(DateTime.UtcNow, matchDataGroup.ToListOfOne(), false, false);

            foreach (MatchRegistry match in matches)
            {
                Console.WriteLine(match.MatchString);
                Assert.AreEqual(testCase.ExpectedMatchStrings[match.MatchOptionId], match.MatchString, $"{match.MatchOptionId.ToString()}");
            }
        }

        [TestCaseSource(nameof(ConsistantMatchStringTestCases))]
        public void ProcessMatchUpdates_PopulatesRequiredFields(ConsistantMatchStringTestCase testCase)
        {
            IList<MatchRegistry> matches = new List<MatchRegistry>();
            IMatchingService service = this.CreateService(matches, testCase.MatchConfig);
            MatchDataGroup matchDataGroup = new MatchDataGroupBuilder().WithGuarantor(testCase.Guarantor).WithPatients(testCase.Patients);

            service.ProcessMatchUpdates(DateTime.UtcNow, matchDataGroup.ToListOfOne(), false, false);

            foreach (MatchRegistry match in matches)
            {
                Console.WriteLine(match.MatchString);

                void AssertNotDefault<T>(Expression<Func<MatchRegistry, T>> expression)
                {
                    MemberExpression memberExpression = (MemberExpression)expression.Body;
                    Assert.AreNotEqual(default(T), expression.Compile().Invoke(match), $"{memberExpression.Expression.Type.Name}.{memberExpression.Member.Name}");
                }

                AssertNotDefault(x => x.MatchOptionId);
                AssertNotDefault(x => x.PatternSetId);
                AssertNotDefault(x => x.PatternUseId);
                AssertNotDefault(x => x.BillingSystemId);
                AssertNotDefault(x => x.DataChangeDate);
                AssertNotDefault(x => x.InsertDate);
                AssertNotDefault(x => x.SourceSystemKey);
            }
        }

        /// <summary>
        /// generates matching configurations
        /// </summary>
        [TestCaseSource(typeof(MatchingTestCases), nameof(MatchingTestCases.MatchConfigurationTestCases))]
        public void MatchConfigurationGenerator(MatchingTestCases.MatchConfigTestCase configTestCase)
        {
            List<Set> config = configTestCase.Config;

            string json = config.ToJSON(new JsonSerializerSettings
            {
                PreserveReferencesHandling = PreserveReferencesHandling.None,
                NullValueHandling = NullValueHandling.Ignore
            }.WithStringEnumConverter(),
            true);

            Console.WriteLine(json);
        }

        [Test]
        public void GetRequiredFields_GetsFields()
        {
            Set testSet = new Set(
                PatternSetEnum.S001,
                0,
                ApplicationEnum.VisitPay.ToListOfOne(),
                null,
                new GuarantorMatchingPatternSskLnameDobSsn4Zip(null, PatternUseEnum.Initial, 1.00m),
                new GuarantorMatchingPatternSskLnamePatientDob(null, PatternUseEnum.Initial, 1.00m),
                new GuarantorMatchingPatternLnameDobSsnZip(null, PatternUseEnum.Ongoing, 0.98m));

            IList<string> requiredFields = testSet.GetRequiredFields().ToList();

            foreach (string requiredField in requiredFields)
            {
                Console.WriteLine(requiredField);
            }

            Assert.That(() => requiredFields.Except(new [] 
            {"Guarantor.SourceSystemKey"
            ,"Guarantor.LastName"
            ,"Guarantor.DOB"
            ,"Guarantor.Ssn4"
            ,"Guarantor.PostalCode"
            ,"Guarantor.Ssn"
            ,"Patient.DOB"}).IsNullOrEmpty());
        }

        [TestCase(1, 1)]
        [TestCase(1, 2)]
        [TestCase(2, 4)]
        public void GetMatchData_ReturnsDistinctPatients(int nGuarantors, int nPatients)
        {
            //
            Dictionary<string, MatchSetConfigurationRegexEnum> configuration = new Dictionary<string, MatchSetConfigurationRegexEnum>();
            List<Set> set1 = new List<Set> { new GuarantorMatchingSet001(configuration, ApplicationEnum.VisitPay.ToListOfOne()) };
            IList<MatchRegistry> matches = new List<MatchRegistry>();
            IList<MatchData> matchData = new List<MatchData>();

            nGuarantors.Do(ng =>
            {
                Guarantor guarantor = new MatchGuarantorBuilder().WithDefaults();
                IList<Patient> patients = new List<Patient>();
                nPatients.Do(x =>
                {
                    patients.Add(new MatchPatientBuilder().WithDob(new DateTime(1990, 1, 1)).WithHsGuarantorId(guarantor.HsGuarantorId));
                });

                matchData.AddRange(patients.Select(x => new MatchData()
                {
                    Guarantor = guarantor,
                    Patient = x
                }));
            });

            //
            IMatchingService service = this.CreateService(matches, set1, matchData: matchData);

            //
            IList<MatchDataGroup> results = service.GetMatchDataGroups(new List<int>(), null);

            Assert.AreEqual(nGuarantors, results.Count);
        }

        public static IEnumerable<GetMatchesTestCase> GetMatchesTestCases()
        {
            Dictionary<string, MatchSetConfigurationRegexEnum> configuration = new Dictionary<string, MatchSetConfigurationRegexEnum>();
            List<Set> matchConfig = new List<Set>()
            {
                new GuarantorMatchingSet003(configuration, ApplicationEnum.VisitPay.ToListOfOne()) // S003 uses patient dob for initial match
            };
            List<Set> matchConfigNoPatient = new List<Set>()
            {
                new GuarantorMatchingSet001(configuration, ApplicationEnum.VisitPay.ToListOfOne())
            };
            Guarantor guarantor1 = new MatchGuarantorBuilder().WithDefaults();
            Guarantor guarantor2 = new MatchGuarantorBuilder().WithDefaults().With(x => x.SourceSystemKey += "2");
            Patient patient1 = new MatchPatientBuilder().WithDob(new DateTime(1990, 1, 1));
            Patient patient2 = new MatchPatientBuilder().WithDob(new DateTime(1990, 1, 2));
            Patient patient3 = new MatchPatientBuilder().WithDob(new DateTime(1990, 1, 3));

            // with existing
            {
                IList<MatchDataGroup> existingMatchData = new List<MatchDataGroup>()
                {
                    new MatchDataGroupBuilder().WithGuarantor(guarantor1).WithPatients(patient1.ToListOfOne())
                };

                IList<MatchDataGroup> matchData = new List<MatchDataGroup>()
                {
                    new MatchDataGroupBuilder().WithGuarantor(guarantor1).WithPatients(patient1.ToListOfOne())
                };

                yield return new GetMatchesTestCase(matchConfig, matchData, existingMatchData, 1, PatternUseEnum.Ongoing) { TestCaseName = "with existing registry" };
                yield return new GetMatchesTestCase(matchConfig, matchData, existingMatchData, 1, PatternUseEnum.Initial) { TestCaseName = "with existing registry" };
            }

            // no existing
            {
                IList<MatchDataGroup> existingMatchData = new List<MatchDataGroup>();

                IList<MatchDataGroup> matchData = new List<MatchDataGroup>()
                {
                    new MatchDataGroupBuilder().WithGuarantor(guarantor1).WithPatients(patient1.ToListOfOne())
                };

                yield return new GetMatchesTestCase(matchConfig, matchData, existingMatchData, 0, PatternUseEnum.Ongoing) { TestCaseName = "no existing registry" };
                yield return new GetMatchesTestCase(matchConfig, matchData, existingMatchData, 0, PatternUseEnum.Initial) { TestCaseName = "no existing registry" };
            }

            // many existing
            {
                IList<MatchDataGroup> existingMatchData = new List<MatchDataGroup>()
                {
                    new MatchDataGroupBuilder().WithGuarantor(guarantor1).WithPatients(patient1.ToListOfOne()),
                    new MatchDataGroupBuilder().WithGuarantor(guarantor2).WithPatients(patient1.ToListOfOne())
                };

                IList<MatchDataGroup> matchData = new List<MatchDataGroup>()
                {
                    new MatchDataGroupBuilder().WithGuarantor(guarantor1).WithPatients(patient1.ToListOfOne())
                };

                yield return new GetMatchesTestCase(matchConfig, matchData, existingMatchData, 2, PatternUseEnum.Ongoing) { TestCaseName = "many similar existing registries" };
                yield return new GetMatchesTestCase(matchConfig, matchData, existingMatchData, 1, PatternUseEnum.Initial) { TestCaseName = "many similar existing registries" };
            }
        }

        [TestCaseSource(nameof(MatchRegistryTestCases))]
        public void GenerateMatches_CreatesNewRegistryRecords(MatchRegistryTestCase testCase)
        {
            IList<MatchRegistry> existingMatchRegistries = new List<MatchRegistry>();

            //create existing match registrys
            DateTime initialProcessDate = DateTime.UtcNow.AddDays(-1);
            IMatchingService service = this.CreateService(existingMatchRegistries, testCase.MatchConfig, matchData: testCase.ExistingMatches.SelectMany(x => x.ToMatchDatas()).ToList());
            service.ProcessMatchUpdates(initialProcessDate, testCase.ExistingMatches, false, false);

            //seed new service with existing registrys
            DateTime processDate = DateTime.UtcNow;
            IList<MatchRegistry> newMatches = new List<MatchRegistry>();
            service = this.CreateService(newMatches, testCase.MatchConfig, matchData: testCase.MatchDataGroups.SelectMany(x => x.ToMatchDatas()).ToList(), existingMatchRegistries: existingMatchRegistries);

            service.ProcessMatchUpdates(processDate, testCase.MatchDataGroups, false, false);

            Assert.AreEqual(testCase.ExpectedNewRegistryCount, newMatches.Except(existingMatchRegistries).Count(), "whoops, that's not the right amount of new matches");
        }


        public static IEnumerable<MatchRegistryTestCase> MatchRegistryTestCases()
        {
            Dictionary<string, MatchSetConfigurationRegexEnum> configuration = new Dictionary<string, MatchSetConfigurationRegexEnum>();
            List<Set> matchConfig = new List<Set>()
            {
                new GuarantorMatchingSet003(configuration, ApplicationEnum.VisitPay.ToListOfOne()) // S003 uses patient dob for initial match
            };
            List<Set> matchConfigNoPatient = new List<Set>()
            {
                new GuarantorMatchingSet001(configuration, ApplicationEnum.VisitPay.ToListOfOne())
            };
            Guarantor guarantor1 = new MatchGuarantorBuilder().WithDefaults();
            Patient patient1 = new MatchPatientBuilder().WithDob(new DateTime(1990, 1, 1));
            Patient patient2 = new MatchPatientBuilder().WithDob(new DateTime(1990, 1, 2));
            Patient patient3 = new MatchPatientBuilder().WithDob(new DateTime(1990, 1, 3));

            { // with existing, patient added
                IList<MatchDataGroup> existingMatchData = new List<MatchDataGroup>()
                {
                    new MatchDataGroupBuilder().WithGuarantor(guarantor1).WithPatients(patient1)
                };

                IList<MatchDataGroup> matchData = new List<MatchDataGroup>()
                {
                    new MatchDataGroupBuilder().WithGuarantor(guarantor1).WithPatients(patient2)
                };

                yield return new MatchRegistryTestCase(matchConfig, matchData, existingMatchData, 1) { TestCaseName = "with existing, patient added" };
            }

            { // with existing, patient changed
                IList<MatchDataGroup> existingMatchData = new List<MatchDataGroup>()
                {
                    new MatchDataGroupBuilder().WithGuarantor(guarantor1).WithPatients(patient3, patient1)
                };

                IList<MatchDataGroup> matchData = new List<MatchDataGroup>()
                {
                    new MatchDataGroupBuilder().WithGuarantor(guarantor1).WithPatients(patient1, patient2)
                };

                yield return new MatchRegistryTestCase(matchConfig, matchData, existingMatchData, 1) { TestCaseName = "with existing, patient changed" };
            }

            { // no existing
                IList<MatchDataGroup> existingMatchData = new List<MatchDataGroup>();

                IList<MatchDataGroup> matchData = new List<MatchDataGroup>()
                {
                    new MatchDataGroupBuilder().WithGuarantor(guarantor1).WithPatients(patient1, patient2)
                };

                yield return new MatchRegistryTestCase(matchConfig, matchData, existingMatchData, 6) { TestCaseName = "no existing" };
            }

            { // set doesnt use patient data, no existing

                IList<MatchDataGroup> existingMatchData = new List<MatchDataGroup>();

                IList<MatchDataGroup> matchData = new List<MatchDataGroup>()
                {
                    new MatchDataGroupBuilder().WithGuarantor(guarantor1).WithPatients(patient1, patient2)
                };

                yield return new MatchRegistryTestCase(matchConfigNoPatient, matchData, existingMatchData, 2) { TestCaseName = "set doesnt use patient data, no existing" };
            }

        }

        private IMatchingService CreateService(IList<MatchRegistry> matchStrings,
            List<Set> matchConfig,
            DateTime? processDate = null,
            IList<int> matchesToQueue = null,
            IList<MatchData> matchData = null,
            IList<MatchRegistry> existingMatchRegistries = null)
        {
            processDate = processDate ?? DateTime.UtcNow;
            IMatchingService service = new MatchingServiceMockBuilder().Setup(builder =>
            {
                builder.MatchRegistryRepositoryMock.Setup(x => x.GetLastUpdateDate(It.IsAny<IList<PatternSetEnum>>())).Returns(processDate);
                // might need to mock this later
                //builder.MatchRegistryRepositoryMock.Setup(x => x.UpdateMatchRegistryMatchString(It.IsAny<MatchRegistry>())).Callback<MatchRegistry>(matchStrings.AddDistinct);
                builder.MatchRegistryRepositoryMock.Setup(x => x.BulkInsert(It.IsAny<IList<MatchRegistry>>())).Callback<IList<MatchRegistry>>(matchStrings.AddRange);
                builder.MatchRegistryRepositoryMock.Setup(x => x.GetMatchRegistriesForSetAndPattern(It.IsAny<string>(), It.IsAny<MatchOptionEnum>(), It.IsAny<PatternSetEnum>(), It.IsAny<PatternUseEnum>()))
                    .Returns<string, MatchOptionEnum, PatternSetEnum, PatternUseEnum>((matchString,pattern,set,use) => 
                    {
                        return existingMatchRegistries?
                            .Where(x => x.MatchString == matchString)
                            .Where(x => x.MatchOptionId == pattern)
                            .Where(x => x.PatternSetId == set)
                            .Where(x => x.PatternUseId == use)
                            .ToList();
                    });

                builder.MatchGuarantorRepositoryMock.Setup(x => x.GetMatchesToQueue(It.IsAny<DateTime>(), It.IsAny<int>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<int>())).Returns(new List<IList<int>> { matchesToQueue });
                builder.MatchGuarantorRepositoryMock.Setup(x => x.GetMatchData(It.IsAny<IList<int>>(), It.IsAny<bool>())).Returns(matchData);

                string matchConfigJson = matchConfig.ToJSON(new JsonSerializerSettings
                {
                    PreserveReferencesHandling = PreserveReferencesHandling.None,
                    NullValueHandling = NullValueHandling.Ignore
                }, true);

                Console.WriteLine($@"Creating service with config:{Environment.NewLine}{matchConfigJson}");

                Mock<Client> client = new Mock<Client>();
                client.Setup(x => x.ClientName).Returns("Acme");
                client.Setup(x => x.MatchConfiguration).Returns(matchConfigJson);

                builder.DomainServiceCommonServiceMockBuilder.ClientServiceMock.Setup(x => x.GetClient()).Returns(client.Object);

            }).CreateService();

            return service;
        }
    }

    #region TestCases

    public class ConsistantMatchStringTestCase
    {
        public Dictionary<MatchOptionEnum, string> ExpectedMatchStrings { get; }
        public Guarantor Guarantor { get; }
        public IList<Patient> Patients { get; }
        public List<Set> MatchConfig { get; set; }

        public ConsistantMatchStringTestCase(Dictionary<MatchOptionEnum, string> expectedMatchStrings, Guarantor guarantor, IList<Patient> patients)
        {
            this.ExpectedMatchStrings = expectedMatchStrings;
            this.Guarantor = guarantor;
            this.Patients = patients;
        }

        public override string ToString()
        {
            return string.Join(" ", this.MatchConfig.Select(x => x.PatternSetEnum.ToString()));
        }
    }

    public class MatchServiceTestCase
    {
        public IList<MatchDataGroup> MatchDataGroups { get; }
        public List<Set> MatchConfig { get; set; }
        public IList<MatchDataGroup> ExistingMatches { get; }
        public string TestCaseName { get; set; }

        public MatchServiceTestCase(List<Set> matchConfig,
            IList<MatchDataGroup> matchDataGroups,
            IList<MatchDataGroup> existingMatches)
        {
            this.MatchDataGroups = matchDataGroups;
            this.MatchConfig = matchConfig;
            this.ExistingMatches = existingMatches;
        }

        public override string ToString()
        {
            return this.TestCaseName;
        }
    }

    public class GetMatchesTestCase : MatchServiceTestCase
    {
        public int ExpectedMatchResults { get; set; }
        public PatternUseEnum PatternUseEnum { get; set; }

        public GetMatchesTestCase(List<Set> matchConfig,
            IList<MatchDataGroup> matchDataGroups,
            IList<MatchDataGroup> existingMatches,
            int expectedMatchResults,
            PatternUseEnum patternUseEnum
        ) : base(matchConfig, matchDataGroups, existingMatches)
        {
            this.ExpectedMatchResults = expectedMatchResults;
            this.PatternUseEnum = patternUseEnum;
        }

        public override string ToString()
        {
            return $"{base.ToString()} returns {this.ExpectedMatchResults.ToString()} {this.PatternUseEnum.ToString()} match(es)";
        }
    }

    public class MatchRegistryTestCase : MatchServiceTestCase
    {
        public int ExpectedNewRegistryCount { get; }

        public MatchRegistryTestCase(List<Set> matchConfig,
            IList<MatchDataGroup> matchDataGroups,
            IList<MatchDataGroup> existingMatches,
            int expectedNewRegistryCount
            ) : base(matchConfig, matchDataGroups, existingMatches)
        {
            this.ExpectedNewRegistryCount = expectedNewRegistryCount;
        }
    }


    #endregion TestCases
}


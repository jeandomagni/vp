﻿namespace Ivh.Domain.Matching.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Utilities.Extensions;
    using Common.Tests.Builders.Matching;
    using Common.Tests.Helpers.Matching;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;
    using Match;
    using Moq;
    using Newtonsoft.Json;
    using NUnit.Framework;
    using Settings.Entities;

    [TestFixture]
    public class MatchReconciliationServiceTests
    {

        #region tests

        [Test]
        public void ExistingMatch_DoesNotChange()
        {
            //
            MatchOptionEnum originalMatchOptionEnum = MatchOptionEnum.DobLnameSsk;
            IList<MatchRegistry> matchRegistries = this.CreateMatchRegistry(1, originalMatchOptionEnum, PatternSetEnum.S001, PatternUseEnum.Initial).ToListOfOne();
            IList<MatchReconciliationMatchInfo> cdiMatchInfos = this.CreateCdiMatchInfo(this.CreateMatchReconciliationHsGuarantorMatch(1, MatchOptionEnum.SsnDobLname).ToListOfOne()).ToListOfOne();
            IList<MatchVpGuarantorMatchInfo> appMatchInfos = this.CreateAppMatchInfo(matchRegistries.ToArray()).ToListOfOne();

            IList<MatchVpGuarantorMatchInfo> changes = new List<MatchVpGuarantorMatchInfo>();
            IList<Set> config = new List<Set>
            {
                GuarantorMatchingSetBuilder.VisitPay(PatternSetEnum.S001),
                GuarantorMatchingSetBuilder.GuestPay(),
            };

            IMatchReconciliationService service = this.CreateService(config, changes, appMatchInfos, cdiMatchInfos, matchRegistries);

            //
            service.MatchReconciliation(1.ToListOfOne());

            //
            Assert.AreEqual(originalMatchOptionEnum, changes.First().Matches.First().Registry.MatchOptionId);
        }


        [Test]
        public void CdiMatch_AddedToApp()
        {
            // app
            IList<MatchRegistry> matchRegistries = new List<MatchRegistry>
            {
                this.CreateMatchRegistry(1, MatchOptionEnum.DobLnameSsk, PatternSetEnum.S001, PatternUseEnum.Initial),
                this.CreateMatchRegistry(2, MatchOptionEnum.DobLnameSsn, PatternSetEnum.S001, PatternUseEnum.Ongoing),
                this.CreateMatchRegistry(2, MatchOptionEnum.Add1CityDobFnameLnameState, PatternSetEnum.S001, PatternUseEnum.Ongoing),
            };
            IList<MatchVpGuarantorMatchInfo> appMatchInfos = new List<MatchVpGuarantorMatchInfo>();

            // cdi
            MatchOptionEnum initialMatchOptionEnum = MatchOptionEnum.AuthenticationMatch;
            MatchOptionEnum ongoingMatchOptionEnum = MatchOptionEnum.SsnDobLname;
            IList<MatchReconciliationMatchInfo> cdiMatchInfos = this.CreateCdiMatchInfo(new List<MatchReconciliationHsGuarantorMatch>
            {
                this.CreateMatchReconciliationHsGuarantorMatch(1, initialMatchOptionEnum),
                this.CreateMatchReconciliationHsGuarantorMatch(2, ongoingMatchOptionEnum),
            }).ToListOfOne();

            // config
            IList<MatchVpGuarantorMatchInfo> changes = new List<MatchVpGuarantorMatchInfo>();
            IList<Set> config = new List<Set>
            {
                GuarantorMatchingSetBuilder.VisitPay(PatternSetEnum.S001),
                GuarantorMatchingSetBuilder.GuestPay(),
            };

            IMatchReconciliationService service = this.CreateService(config, changes, appMatchInfos, cdiMatchInfos, matchRegistries);

            //
            service.MatchReconciliation(1.ToListOfOne());

            //
            Assert.AreEqual(MatchOptionEnum.DobLnameSsk, changes.First().Matches.First(x => x.Registry.PatternUseId == PatternUseEnum.Initial).Registry.MatchOptionId);
            Assert.AreEqual(MatchOptionEnum.DobLnameSsn, changes.First().Matches.First(x => x.Registry.PatternUseId == PatternUseEnum.Ongoing).Registry.MatchOptionId);
        }

        [Test]
        public void CdiMatches_UnmappedOptions_AddedToApp()
        {
            // app
            IList<MatchRegistry> matchRegistries = new List<MatchRegistry>
            {
                this.CreateMatchRegistry(1, MatchOptionEnum.DobLnameSsk, PatternSetEnum.S001, PatternUseEnum.Initial),
                this.CreateMatchRegistry(2, MatchOptionEnum.DobLnameSsn, PatternSetEnum.S001, PatternUseEnum.Ongoing),
                this.CreateMatchRegistry(2, MatchOptionEnum.Add1CityDobFnameLnameState, PatternSetEnum.S001, PatternUseEnum.Ongoing),
            };
            IList<MatchVpGuarantorMatchInfo> appMatchInfos = new List<MatchVpGuarantorMatchInfo>();

            // cdi
            IList<MatchReconciliationMatchInfo> cdiMatchInfos = this.CreateCdiMatchInfo(new List<MatchReconciliationHsGuarantorMatch>
            {
                this.CreateMatchReconciliationHsGuarantorMatch(1, null),
                this.CreateMatchReconciliationHsGuarantorMatch(2, null),
            }).ToListOfOne();

            // config
            IList<MatchVpGuarantorMatchInfo> changes = new List<MatchVpGuarantorMatchInfo>();
            IList<Set> config = new List<Set>
            {
                GuarantorMatchingSetBuilder.VisitPay(PatternSetEnum.S001),
                GuarantorMatchingSetBuilder.GuestPay(),
            };

            IMatchReconciliationService service = this.CreateService(config, changes, appMatchInfos, cdiMatchInfos, matchRegistries);

            //
            service.MatchReconciliation(1.ToListOfOne());

            //
            Assert.AreEqual(MatchOptionEnum.DobLnameSsk, changes.First().Matches.First(x => x.Registry.PatternUseId == PatternUseEnum.Initial).Registry.MatchOptionId);
            Assert.AreEqual(MatchOptionEnum.DobLnameSsn, changes.First().Matches.First(x => x.Registry.PatternUseId == PatternUseEnum.Ongoing).Registry.MatchOptionId);
        }

        #endregion

        #region helpers

        private MatchVpGuarantorMatchInfo CreateAppMatchInfo(params MatchRegistry[] matchRegistries)
        {
            MatchVpGuarantorMatchInfo matchInfo = new MatchVpGuarantorMatchInfo()
            {
                VpGuarantorId = 1
            };

            matchInfo.SetMatchGuarantorDob(DateTime.UtcNow);
            matchInfo.SetMatchSsn("111223333");

            foreach (MatchRegistry matchRegistry in matchRegistries)
            {
                matchInfo.AddMatch(matchRegistry);
            }

            return matchInfo;
        }
        private MatchReconciliationMatchInfo CreateCdiMatchInfo(IList<MatchReconciliationHsGuarantorMatch> matches)
        {
            MatchReconciliationMatchInfo matchInfo = new MatchReconciliationMatchInfo
            {
                DOB = DateTime.UtcNow,
                HsGuarantorMatches = matches,
                SSN = "111223333",
                UnderlyingMatchSSN = string.Empty,
                VpGuarantorId = 1,
            };

            return matchInfo;
        }
        private MatchRegistry CreateMatchRegistry(int hsGuarantorId, MatchOptionEnum matchOption, PatternSetEnum patternSet, PatternUseEnum patternUseEnum)
        {
            MatchRegistry matchRegistry = new MatchRegistry("matchString", "ssn", 1)
            {
                HsGuarantorId = hsGuarantorId,
                MatchOptionId = matchOption,
                PatternSetId = patternSet,
                PatternUseId = patternUseEnum
            };

            return matchRegistry;
        }

        private MatchReconciliationHsGuarantorMatch CreateMatchReconciliationHsGuarantorMatch(int hsGuarantorId, MatchOptionEnum? matchOption)
        {
            MatchReconciliationHsGuarantorMatch match = new MatchReconciliationHsGuarantorMatch
            {
                HsGuarantorId = hsGuarantorId,
                HsGuarantorMatchStatusId = HsGuarantorMatchStatusEnum.Matched,
                MatchedOn = DateTime.UtcNow,
                MatchOptionId = matchOption,
                VpGuarantorHsMatchId = 1,
                VpGuarantorId = 1,
            };

            return match;
        }

        private IMatchReconciliationService CreateService(
            IList<Set> matchConfig,
            IList<MatchVpGuarantorMatchInfo> changes,
            IList<MatchVpGuarantorMatchInfo> appMatchInfos,
            IList<MatchReconciliationMatchInfo> cdiMatchInfos,
            IList<MatchRegistry> matchRegistries)
        {
            MatchReconciliationServiceMockBuilder builder = new MatchReconciliationServiceMockBuilder();

            builder.MatchVpGuarantorMatchInfoRepositoryMock
                .Setup(x => x.GetById(It.IsAny<int>()))
                .Returns<int>(x => appMatchInfos.FirstOrDefault(m => m.VpGuarantorId == x));

            builder.MatchReconciliationRepositoryMock
                .Setup(x => x.GetMatchReconciliationMatchInfo(It.IsAny<IList<int>>()))
                .Returns<IList<int>>(vpgids => cdiMatchInfos.Where(x => vpgids.Contains(x.VpGuarantorId)).ToList());

            builder.MatchRegistryRepositoryMock
                .Setup(x => x.GetMatchRegistries(It.IsAny<IList<int>>()))
                .Returns<IList<int>>(hsgids => matchRegistries.Where(x => hsgids.Contains(x.HsGuarantorId)).ToList());

            builder.MatchVpGuarantorMatchInfoRepositoryMock.Setup(x => x.InsertOrUpdate(It.IsAny<MatchVpGuarantorMatchInfo>()))
                .Callback<MatchVpGuarantorMatchInfo>(changes.Add);

            string matchConfigJson = matchConfig.ToJSON(new JsonSerializerSettings
            {
                PreserveReferencesHandling = PreserveReferencesHandling.None,
                NullValueHandling = NullValueHandling.Ignore
            }.WithStringEnumConverter(), true);

            Console.WriteLine($@"Creating service with config:{Environment.NewLine}{matchConfigJson}");

            Mock<Client> client = new Mock<Client>();
            client.Setup(x => x.ClientName).Returns("Acme");
            client.Setup(x => x.MatchConfiguration).Returns(matchConfigJson);
            builder.DomainServiceCommonServiceMockBuilder.ClientServiceMock.Setup(x => x.GetClient()).Returns(client.Object);

            return builder.CreateService();
        }

        #endregion


    }
}

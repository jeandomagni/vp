﻿namespace Ivh.Domain.Visit.Services
{
    using System;
    using Common.Tests.Helpers.Visit;
    using NUnit.Framework;
    using System.Linq;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Entities;

    [TestFixture]
    public class VisitTransactionTests : DomainTestBase
    {
        [SetUp]
        public void BeforeEachTest()
        {
        }

        [Test]
        public void SourceSystemKeySetDateGetsSetOnSourceSystemKeySet()
        {
            var vt = new VisitTransaction();

            Assert.IsNull(vt.SourceSystemKey);
            Assert.IsNull(vt.SourceSystemKeySetDate);

            vt.SetSourceSystemKey("22132132");

            Assert.IsNotNull(vt.SourceSystemKeySetDate);
        }

        [Test]
        public void SourceSystemKeySetDateGetsRemovedOnSourceSystemKeyRemove()
        {
            var vt = new VisitTransaction();
            vt.SetSourceSystemKey("22132132");

            Assert.IsNotNull(vt.SourceSystemKey);
            Assert.IsNotNull(vt.SourceSystemKeySetDate);

            vt.SetSourceSystemKey(null);

            Assert.IsNull(vt.SourceSystemKeySetDate);
        }
    }
}

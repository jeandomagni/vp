﻿namespace Ivh.Domain.Visit.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Tests.Helpers.Base;
    using Entities;
    using HsResolutionServices.Client.Intermountain;
    using Interfaces;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class HsFacilityResolutionServiceTest : DomainTestBase
    {
        private Mock<HsFacilityResolutionService> HsFacilityResolutionService { get; set; }
        private Mock<IHsFacilityRepository> HsFacilityRepository { get; set; }

        private readonly IList<HsFacility> _facilities = new List<HsFacility>()
                {
                    new HsFacility() {FacilityCode = "123", FacilityDescription = "Logan Regional Hospital"},
                    new HsFacility() {FacilityCode = "000", FacilityDescription = "Facility 000"},
                    new HsFacility() {FacilityCode = "111", FacilityDescription = "Facility 111"},
                    new HsFacility() {FacilityCode = "222", FacilityDescription = "Facility 222"},
                    new HsFacility() {FacilityCode = "333", FacilityDescription = "Facility 333"},
                    new HsFacility() {FacilityCode = "444", FacilityDescription = "Facility 444"},
                    new HsFacility() {FacilityCode = "555", FacilityDescription = "Facility 555"},
                    new HsFacility() {FacilityCode = "666", FacilityDescription = "Facility 666"},
                    new HsFacility() {FacilityCode = "777", FacilityDescription = "Facility 777"},
                    new HsFacility() {FacilityCode = "888", FacilityDescription = "Facility 888"},
                    new HsFacility() {FacilityCode = "999", FacilityDescription = "Facility 999"},
                };

        [SetUp]
        public void BeforeEachTest()
        {
            this.HsFacilityRepository = new Mock<IHsFacilityRepository>();
            this.HsFacilityRepository.Setup(x => x.GetQueryable()).Returns(() => this._facilities.AsQueryable());
            this.HsFacilityResolutionService = new Mock<HsFacilityResolutionService>(
                new DomainServiceCommonServiceMockBuilder().CreateServiceLazy(),
                this.HsFacilityRepository.Object);
        }
       
       [Test]
        public void ResolveFacFacilityCodeToFacilityDescription()
        {
            HsFacility hsFacility = this.GetRandomHsFacility();
            string returnValue = this.HsFacilityResolutionService.Object.ResolveFacilityDescription(hsFacility.FacilityCode);
            Assert.AreEqual(returnValue, hsFacility.FacilityDescription);
        }
       
        [Test]
        public void ResolveFacilityDescription()
        {
            HsFacility hsFacility = this.GetRandomHsFacility();
            VisitItemizationStorage visitItemizationStorage = new VisitItemizationStorage() {FacilityCode = hsFacility.FacilityCode};
            this.HsFacilityResolutionService.Object.ResolveFacility(visitItemizationStorage);
            Assert.AreEqual(visitItemizationStorage.FacilityDescription, hsFacility.FacilityDescription);
        }

        [Test]
        public void ResolveFacilityCode()
        {
            HsFacility hsFacility = this.GetRandomHsFacility();
            VisitItemizationStorage visitItemizationStorage = new VisitItemizationStorage() { FacilityCode = hsFacility.FacilityCode };
            this.HsFacilityResolutionService.Object.ResolveFacility(visitItemizationStorage);
            Assert.AreEqual(visitItemizationStorage.FacilityCode, hsFacility.FacilityCode);
        }

        private HsFacility GetRandomHsFacility()
        {
            return this._facilities.Skip(new Random(0).Next(0, this._facilities.Count-1)).First();
        }
    }
}

﻿namespace Ivh.Domain.Visit.Services
{
    using System;
    using System.Collections;
    using System.Linq;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.State.Interfaces;
    using Common.State.Test;
    using Common.VisitPay.Enums;
    using Entities;
    using Guarantor.Entities;
    using NUnit.Framework;
    using Rules;
    using Visit = Ivh.Domain.Visit.Entities.Visit;

    [TestFixture]
    public class VisitStateMachineTests : UnitTestBase<
            Visit,
            VisitStateEnum,
            VisitStateRules,
            IMachine<Visit, VisitStateEnum>>
    {
        public VisitStateMachineTests() :
            base(VisitStateEnum.NotSet, "DEFAULT")
        {
        }

        public static IEnumerable VisitStateMachineTestCases
        {
            get
            {
                //NotSet
                yield return VisitStateMachineTestCase.TestRule(VisitStateRules.V3_101)
                    .WithInitialState(VisitStateEnum.NotSet)
                    .And(TestVisitFactory.Eligible)
                    .And(TestVisitFactory.PositiveBalance)
                    .And(TestVisitFactory.NoFlags)
                    .ExpectsFinalState(VisitStateEnum.Active);

                yield return VisitStateMachineTestCase.TestRule(VisitStateRules.V3_102)
                    .WithInitialState(VisitStateEnum.NotSet)
                    .And(TestVisitFactory.Eligible)
                    .And(TestVisitFactory.NoBalance)
                    .ExpectsFinalState(VisitStateEnum.Inactive);

                yield return VisitStateMachineTestCase.TestRule(VisitStateRules.V3_103)
                    .WithInitialState(VisitStateEnum.NotSet)
                    .And(TestVisitFactory.PositiveBalance)
                    .And(TestVisitFactory.Ineligible)
                    .ExpectsFinalState(VisitStateEnum.Inactive);

                yield return VisitStateMachineTestCase.TestRule(VisitStateRules.V3_104)
                    .WithInitialState(VisitStateEnum.NotSet)
                    .And(TestVisitFactory.Eligible)
                    .And(TestVisitFactory.PositiveBalance)
                    .And(TestVisitFactory.OnHold)
                    .ExpectsFinalState(VisitStateEnum.Inactive);

                //Unmatched
                yield return VisitStateMachineTestCase.TestRule(VisitStateRules.V3_105)
                    .WithInitialState(VisitStateEnum.Inactive)
                    .And(TestVisitFactory.Eligible)
                    .And(TestVisitFactory.PositiveBalance)
                    .And(TestVisitFactory.Unmatched)
                    .ExpectsFinalState(VisitStateEnum.Inactive);

                yield return VisitStateMachineTestCase.TestRule(VisitStateRules.V3_106)
                    .WithInitialState(VisitStateEnum.Active)
                    .And(TestVisitFactory.Eligible)
                    .And(TestVisitFactory.PositiveBalance)
                    .And(TestVisitFactory.Unmatched)
                    .ExpectsFinalState(VisitStateEnum.Inactive);

                //Recalled
                yield return VisitStateMachineTestCase.TestRule(VisitStateRules.V3_107)
                    .WithInitialState(VisitStateEnum.Inactive)
                    .And(TestVisitFactory.PositiveBalance)
                    .And(TestVisitFactory.Ineligible)
                    .ExpectsFinalState(VisitStateEnum.Inactive);

                yield return VisitStateMachineTestCase.TestRule(VisitStateRules.V3_108)
                    .WithInitialState(VisitStateEnum.Active)
                    .And(TestVisitFactory.PositiveBalance)
                    .And(TestVisitFactory.Ineligible)
                    .ExpectsFinalState(VisitStateEnum.Inactive);

                //OnHold
                yield return VisitStateMachineTestCase.TestRule(VisitStateRules.V3_109)
                    .WithInitialState(VisitStateEnum.Inactive)
                    .And(TestVisitFactory.Eligible)
                    .And(TestVisitFactory.PositiveBalance)
                    .And(TestVisitFactory.OnHold)
                    .ExpectsFinalState(VisitStateEnum.Inactive);

                yield return VisitStateMachineTestCase.TestRule(VisitStateRules.V3_110)
                    .WithInitialState(VisitStateEnum.Active)
                    .And(TestVisitFactory.Eligible)
                    .And(TestVisitFactory.PositiveBalance)
                    .And(TestVisitFactory.OnHold)
                    .ExpectsFinalState(VisitStateEnum.Inactive);

                //NoBalance
                yield return VisitStateMachineTestCase.TestRule(VisitStateRules.V3_111)
                    .WithInitialState(VisitStateEnum.Inactive)
                    .And(TestVisitFactory.Eligible)
                    .And(TestVisitFactory.NoBalance)
                    .ExpectsFinalState(VisitStateEnum.Inactive);

                yield return VisitStateMachineTestCase.TestRule(VisitStateRules.V3_112)
                    .WithInitialState(VisitStateEnum.Active)
                    .And(TestVisitFactory.Eligible)
                    .And(TestVisitFactory.NoBalance)
                    .ExpectsFinalState(VisitStateEnum.Inactive);

                //PositiveBalanceAndNoFlags
                yield return VisitStateMachineTestCase.TestRule(VisitStateRules.V3_113)
                    .WithInitialState(VisitStateEnum.Inactive)
                    .And(TestVisitFactory.Eligible)
                    .And(TestVisitFactory.PositiveBalance)
                    .And(TestVisitFactory.NoFlags)
                    .ExpectsFinalState(VisitStateEnum.Active);

                yield return VisitStateMachineTestCase.TestRule(VisitStateRules.V3_114)
                    .WithInitialState(VisitStateEnum.Active)
                    .And(TestVisitFactory.Eligible)
                    .And(TestVisitFactory.PositiveBalance)
                    .And(TestVisitFactory.NoFlags)
                    .ExpectsFinalState(VisitStateEnum.Active);
            }
        }

        [Test]
        [TestCaseSource(nameof(VisitStateMachineTestCases))]
        public void VisitStateMachine(StateMachineTestCase<Visit, VisitStateEnum> visitTestCase)
        {
            this.Machine.Evaluate(visitTestCase.Entity);
            Assert.AreEqual(visitTestCase.ExpectedStateAfterEvaluation, ((IEntity<VisitStateEnum>)visitTestCase.Entity).GetState());
            Assert.AreEqual(visitTestCase.RuleName, ((IEntity<VisitStateEnum>)visitTestCase.Entity).GetCurrentStateHistory().RuleName);
        }

    }

    public class VisitStateMachineTestCase : StateMachineTestCase<Visit, VisitStateEnum>
    {
    }

    public static class TestVisitFactory
    {
        public static Visit PositiveBalance(Visit visit)
        {
            visit.SetCurrentBalance(1m);
            return visit;
        }
        public static Visit NoBalance(Visit visit)
        {
            visit.SetCurrentBalance(0m);
            return visit;
        }
        public static Visit NoFlags(Visit visit)
        {
            visit.BillingSystem = new BillingSystem() { BillingSystemId = 1 };
            visit.VpEligible = true;
            visit.IsMaxAge = false;
            visit.BillingHold = false;
            return visit;
        }
        public static Visit Unmatched(Visit visit)
        {
            visit.BillingSystem = new BillingSystem() { BillingSystemId = -1 };
            visit.VPGuarantor = new Guarantor();
            return visit;
        }
        public static Visit Eligible(Visit visit)
        {
            visit.VpEligible = true;
            visit.VPGuarantor = new Guarantor();
            return visit;
        }
        public static Visit Ineligible(Visit visit)
        {
            visit.VpEligible = false;
            visit.VPGuarantor = new Guarantor();
            return visit;
        }

        public static Visit OnHold(Visit visit)
        {
            visit.BillingHold = true;
            return visit;
        }
    }
}
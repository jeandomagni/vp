﻿namespace Ivh.Domain.Visit.Services
{
    using System;
    using System.Collections.Generic;
    using Base.Interfaces;
    using Common.Tests.Helpers.Base;
    using Common.Tests.Helpers.Visit;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;
    using Moq;
    using NUnit.Framework;
    using Settings.Entities;
    using Settings.Interfaces;

    [TestFixture]
    public class VisitServiceTests : DomainTestBase
    {
        [SetUp]
        public void BeforeEachTest()
        {
            this._moqClientService = new Mock<IClientService>();
            this._moqVisitRepository = new Mock<IVisitRepository>();
            this._moqVisitStateMachineService = new Mock<IVisitStateMachineService>();
            this._moqVisitStateService = new Mock<IVisitStateService>();
            this._moqVisitTransactionRepository = new Mock<IVisitTransactionRepository>();
            
            this._visitService = new VisitService(
                new Lazy<IDomainServiceCommonService>(() => this._domainServiceCommonService),
                new Lazy<IVisitRepository>(() => this._moqVisitRepository.Object),
                new Lazy<IVisitTransactionRepository>(() => this._moqVisitTransactionRepository.Object),
                new Lazy<IVisitStateService>(() => this._moqVisitStateService.Object));

            this._moqApplicationSettingsService = new Mock<IApplicationSettingsService>();
            this._moqApplicationSettingsService.Setup(t => t.PastDueAgingCount).Returns(() => new ApplicationSetting<int>(() => new Lazy<int>(() => 1)));

            this._domainServiceCommonService = DomainServiceCommonServices.CreateDomainServiceCommonService(
                this._moqApplicationSettingsService,
                null,
                this._moqClientService
            );
        }

        private Mock<IApplicationSettingsService> _moqApplicationSettingsService;
        private Mock<IVisitRepository> _moqVisitRepository;
        private Mock<IVisitTransactionRepository> _moqVisitTransactionRepository;
        private Mock<IVisitStateService> _moqVisitStateService;
        private Mock<IVisitStateMachineService> _moqVisitStateMachineService;
        private Mock<IClientService> _moqClientService;

        private VisitService _visitService;
        private IDomainServiceCommonService _domainServiceCommonService;
        
        //[Test]
        //Should test SetVisitToUncollectable, SetVisitToCollectable, SetVisitToRecalled, and SetVisitToUnRecalled 
        public void VisitSetsHardRecall()
        {
            /*
             * Should merge the following fields:
                Update HsCurrentBalance to Zero
            
             */
            IVisitStateService visitStateService = new VisitStateService(
                 new Lazy<IDomainServiceCommonService>(() => this._domainServiceCommonService),
                 new Lazy<IVisitRepository>(() => this._moqVisitRepository.Object),
                new Lazy<IVisitStateMachineService>(() => this._moqVisitStateMachineService.Object));

            this._visitService = new VisitService(
                new Lazy<IDomainServiceCommonService>(() => this._domainServiceCommonService),
                new Lazy<IVisitRepository>(() => this._moqVisitRepository.Object),
                new Lazy<IVisitTransactionRepository>(() => this._moqVisitTransactionRepository.Object),
                new Lazy<IVisitStateService>(() => this._moqVisitStateService.Object));

            Tuple<Visit, IList<VisitTransaction>> visitsAndTransactions = VisitFactory.GenerateActiveVisit(1234, 1);

            string recallReason = "TEST";
            this._visitService.SetVisitToIneligible(visitsAndTransactions.Item1, recallReason);

            Assert.AreEqual((byte)VisitStateEnum.Inactive, visitsAndTransactions.Item1.CurrentVisitState);
        }

        private void SetupVisitService(bool withExclusions)
        {
            if (withExclusions)
            {
                this._moqApplicationSettingsService.Setup(t => t.HsCurrentBalanceSumExcludedTransactionTypes).Returns(() => new ApplicationSetting<IList<VpTransactionTypeEnum>>(() => new Lazy<IList<VpTransactionTypeEnum>>(() => new List<VpTransactionTypeEnum>()
                {
                    VpTransactionTypeEnum.VppInterestCharge
                })));
            }
            else
            {
                this._moqApplicationSettingsService.Setup(t => t.HsCurrentBalanceSumExcludedTransactionTypes).Returns(() => new ApplicationSetting<IList<VpTransactionTypeEnum>>(() => new Lazy<IList<VpTransactionTypeEnum>>(() => new List<VpTransactionTypeEnum>()
                {
                })));
            }
           
        }
    }
}


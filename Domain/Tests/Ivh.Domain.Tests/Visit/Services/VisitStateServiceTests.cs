﻿namespace Ivh.Domain.Visit.Services
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Common.Base.Utilities.Helpers;
    using Common.Tests.Helpers.Base;
    using Common.Tests.Helpers.Visit;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;
    using Moq;
    using NUnit.Framework;
    using Settings.Entities;
    using Settings.Interfaces;

    [TestFixture]
    public class VisitStateServiceTests : DomainTestBase
    {
        private VisitStateService createVisitStateService()
        {
            return new VisitStateService(
                new Lazy<IDomainServiceCommonService>(() => this._domainServiceCommonService),
                new Lazy<IVisitRepository>(() => this._visitRepositoryMock.Object),
                new Lazy<IVisitStateMachineService>(() => this._visitStateMachineServiceMock.Object));
        }

        private IDomainServiceCommonService _domainServiceCommonService;
        private Mock<IApplicationSettingsService> _applicationSettingsMock;
        private Mock<IClientService> _clientServiceMock;
        private Mock<IVisitRepository> _visitRepositoryMock;
        private Mock<IVisitStateMachineService> _visitStateMachineServiceMock;
        [SetUp]
        public void Setup()
        {
            this._visitRepositoryMock = new Mock<IVisitRepository>();
            this._clientServiceMock = new Mock<IClientService>();
            this._visitStateMachineServiceMock = new Mock<IVisitStateMachineService>();

            Lazy<TimeZoneHelper> timeZoneHelper = new Lazy<TimeZoneHelper>(() => new TimeZoneHelper(TimeZoneHelper.TimeZones.PacificStandardTime));
            this._clientServiceMock.Setup(t => t.GetClient()).Returns(() => new Client(new Dictionary<string, string>()
            {
                {
                    "UncollectableMaxAgingCountFinancePlans","4"
                }
            }, timeZoneHelper));

            this._applicationSettingsMock = new Mock<IApplicationSettingsService>();
            this._applicationSettingsMock.Setup(t => t.PastDueAgingCount).Returns(() => new ApplicationSetting<int>(() => new Lazy<int>(() => 1)));

            this._domainServiceCommonService = DomainServiceCommonServices.CreateDomainServiceCommonService(this._applicationSettingsMock, null, this._clientServiceMock);
        }

        [Ignore(ObsoleteDescription.VisitStateMachineTest)]
        [Test]
        public void VisitDoesntChangesStatus_PartialHsPatientCashPayment_VisitNotOnFinancePlan_PendingStatement()
        {
            DateTime twoMonthsAgo = DateTime.Today.AddMonths(-2);
            Visit visit = VisitFactory.GenerateActiveVisit(2000, 1, VisitStateEnum.NotSet, twoMonthsAgo, "PB", null, 0).Item1;
            visit.SetCurrentBalance(1800m);

            VisitStateService srv = this.createVisitStateService();
            this._visitRepositoryMock.Setup(t => t.GetStatementVisitStateModel(visit)).Returns(() => new StatementVisitStatusModel() { IsInGracePeriod = false });

            srv.EvaluateVisitState(visit);
            Assert.IsTrue(visit.CurrentVisitState == VisitStateEnum.Active);
        }

        [Test]
        public void VisitDoesntChangesStatus_PartialHsPatientCashPayment_VisitNotOnFinancePlan_OnStatement()
        {
            DateTime twoMonthsAgo = DateTime.Today.AddMonths(-2);
            Visit visit = VisitFactory.GenerateActiveVisit(2000, 1, VisitStateEnum.Active, twoMonthsAgo, "PB", null, 1).Item1;
            visit.SetCurrentBalance(1900m);

            VisitStateService srv = this.createVisitStateService();
            this._visitRepositoryMock.Setup(t => t.GetStatementVisitStateModel(visit)).Returns(() => new StatementVisitStatusModel() { StatementPeriodEndDate = DateTime.UtcNow.AddDays(-15), IsInGracePeriod = true });
            this._visitRepositoryMock.Setup(t => t.IsOnStatement(visit)).Returns(() => true);

            srv.EvaluateVisitState(visit);
            Assert.IsTrue(visit.CurrentVisitState == VisitStateEnum.Active);
        }

        [Ignore(ObsoleteDescription.VisitStateMachineTest)]
        [Test]
        public void VisitDoesntChangesStatus_PartialHsPatientCashPayment_VisitNotOnFinancePlan_PastDue()
        {
            //DateTime twoMonthsAgo = DateTime.Today.AddMonths(-2);
            //Visit visit = VisitFactory.GenerateActiveVisit(2000, 1, VisitStateEnum.PastDue, twoMonthsAgo, "PB", null, 3);
            //visit.Transactions[0].InsertDate = twoMonthsAgo;
            //visit.Transactions.Add(VisitTransactionFactory.GenerateVisitTransaction(-100m, VpTransactionTypeEnum.HsPatientCash));
            //visit.SetCurrentBalance(1900m);

            //VisitStateService srv = this.createVisitStateService();
            //this._visitRepositoryMock.Setup(t => t.GetStatementVisitStateModel(visit)).Returns(() => new StatementVisitStateModel() { StatementPeriodEndDate = DateTime.UtcNow.AddDays(-15), IsInGracePeriod = true });

            //srv.EvaluateVisitState(visit);
            //Assert.IsTrue(visit.VisitState.IsPastDue());

        }

        [Ignore(ObsoleteDescription.VisitStateMachineTest)]
        [Test]
        public void VisitDoesntChangesStatus_PartialHsPatientCashPayment_VisitNotOnFinancePlan_PastDue_AgingCountEqualsUncollectableMaxAgingCountFinancePlans()
        {
            //DateTime twoMonthsAgo = DateTime.Today.AddMonths(-2);
            //Visit visit = VisitFactory.GenerateActiveVisit(2000, 1, VisitStateEnum.PastDue, twoMonthsAgo, "PB", null, 4);
            //visit.Transactions[0].InsertDate = twoMonthsAgo;
            //visit.Transactions.Add(VisitTransactionFactory.GenerateVisitTransaction(-100m, VpTransactionTypeEnum.HsPatientCash));
            //visit.SetCurrentBalance(1900m);

            //VisitStateService srv2 = this.createVisitStateService();
            //this._visitRepositoryMock.Setup(t => t.GetStatementVisitStateModel(visit)).Returns(() => new StatementVisitStateModel() { StatementPeriodEndDate = DateTime.UtcNow.AddDays(-15), IsInGracePeriod = true });

            //srv2.SetVisitState(visit);
            //Assert.IsTrue(visit.VisitState.IsPastDue());

        }

        [Ignore(ObsoleteDescription.VisitStateMachineTest)]
        //[TestCase(5, false, VisitStateEnum.PendingUncollectable)]
        //[TestCase(5, true, VisitStateEnum.PendingUncollectable)]
        //[TestCase(4, false, VisitStateEnum.PendingUncollectable)]
        //[TestCase(4, true, VisitStateEnum.PastDue)]
        //[TestCase(3, false, VisitStateEnum.PastDue)]
        //[TestCase(3, true, VisitStateEnum.PastDue)]
        //[TestCase(2, false, VisitStateEnum.PastDue)]
        //[TestCase(2, true, VisitStateEnum.PastDue)]
        //[TestCase(1, false, VisitStateEnum.PastDue)]
        //[TestCase(1, true, VisitStateEnum.Active)]
        //[TestCase(0, false, VisitStateEnum.PendingStatement)]
        //[TestCase(0, true, VisitStateEnum.PendingStatement)]
        public void VisitDoesntChangeStatus_PartialHsPatientCashPayment_VisitNotOnFinancePlan(int agingCount, bool isGracePeriod, VisitStateEnum expectedState)
        {
            VisitStateEnum currentVisitStateEnum;
            if (agingCount == 4 && !isGracePeriod)
            {
                //currentVisitStateEnum = VisitStateEnum.PendingUncollectable;
                currentVisitStateEnum = VisitStateEnum.Active;
            }
            else if (agingCount > 1 || agingCount == 1 && !isGracePeriod)
            {
                //currentVisitStateEnum = VisitStateEnum.PastDue;
                currentVisitStateEnum = VisitStateEnum.Active;
            }
            else if (agingCount == 1)
            {
                currentVisitStateEnum = VisitStateEnum.Active;
            }
            else
            {
                currentVisitStateEnum = VisitStateEnum.Active;
            }

            DateTime twoMonthsAgo = DateTime.Today.AddMonths(-2);
            Visit visit = VisitFactory.GenerateActiveVisit(2000, 1, currentVisitStateEnum, twoMonthsAgo, "PB", null, agingCount).Item1;
            visit.SetCurrentBalance(1900m);

            this._visitRepositoryMock.Setup(x => x.IsOnStatement(It.IsAny<Visit>())).Returns(true);
            VisitStateService srv = this.createVisitStateService();

            this._visitRepositoryMock.Setup(t => t.GetStatementVisitStateModel(visit)).Returns(() => new StatementVisitStatusModel { StatementPeriodEndDate = DateTime.UtcNow.AddDays(-15), IsInGracePeriod = isGracePeriod });

            srv.EvaluateVisitState(visit);
            
            Assert.AreEqual(visit.CurrentVisitState, expectedState);
        }

        [Test]
        [Ignore(ObsoleteDescription.VisitTransactions + " " + ObsoleteDescription.VisitStateMachineTest)]
        public void VisitSetsCurrentBalanceToZero()
        {
            Visit visit = VisitFactory.GenerateActiveVisit(1234, 1).Item1;
            //Assert.AreNotEqual(visit.VisitState.VisitStateId, (int)VisitStateEnum.PendingClosure);
            visit.SetCurrentBalance(1m);

            VisitStateService srv = this.createVisitStateService();

            srv.EvaluateVisitState(visit);

            Assert.AreEqual(visit.CurrentVisitState, VisitStateEnum.Inactive);

        }

        [Ignore(ObsoleteDescription.VisitStateMachineTest)]
        [Test]
        public void VisitSetsHsCurrentBalanceToZero()
        {
            //Visit visit = VisitFactory.GenerateActiveVisit(1234, 1);
            //Assert.AreNotEqual(visit.VisitState.VisitStateId, (int)VisitStateEnum.PaidInFull);
            //visit.Transactions.Add(VisitTransactionFactory.GenerateVisitTransaction(-1234m));
            //visit.SetCurrentBalance(0m);
            //
            //VisitStateService srv = this.createVisitStateService();
            //
            //srv.SetVisitStateForZeroBalance(visit);
            //
            //Assert.AreEqual(visit.VisitState.VisitStateId, (int)VisitStateEnum.PaidInFull);

        }
        
        //[TestCase(VisitStateEnum.PastDue, 4, true)]
        //[TestCase(VisitStateEnum.PastDue, 3, false)]
        //[TestCase(VisitStateEnum.PastDue, 3, true)]
        //[TestCase(VisitStateEnum.PastDue, 2, false)]
        //[TestCase(VisitStateEnum.PastDue, 2, true)]
        //[TestCase(VisitStateEnum.PastDue, 1, false)]
        //[TestCase(VisitStateEnum.Active, 1, true)]
        [Ignore(ObsoleteDescription.VisitTransactions + " " + ObsoleteDescription.VisitStateMachineTest)]
        public void VisitWithPositiveAdjustmentRestatuses(VisitStateEnum visitState, AgingTierEnum agingTier, bool isGracePeriod)
        {
            DateTime insertDate = DateTime.UtcNow.AddDays(-20);
            DateTime statementPeriodEndDate = DateTime.UtcNow.AddDays(-1);

            Visit visit = VisitFactory.GenerateActiveVisit(1234, 1, visitState).Item1;
            visit.SetAgingTier(agingTier, "");
            visit.InsertDate = insertDate;
            
            Assert.AreEqual(true, visit.CurrentVisitState == visitState);

            VisitStateService svc = new VisitStateServiceMockBuilder().Setup(builder =>
            {
                builder.VisitRepositoryMock.Setup(x => x.IsOnStatement(It.IsAny<Visit>())).Returns(() => true);
                builder.VisitRepositoryMock.Setup(x => x.GetStatementVisitStateModel(It.IsAny<Visit>())).Returns(() => new StatementVisitStatusModel
                {
                    IsInGracePeriod = isGracePeriod,
                    StatementPeriodEndDate = statementPeriodEndDate
                });
            }).CreateService();
            
            // negative transaction
            VisitTransaction negativeTransaction = VisitTransactionFactory.GenerateVisitTransaction(-100m, VpTransactionTypeEnum.HsCharge);
            negativeTransaction.InsertDate = statementPeriodEndDate.AddDays(1);
            
            svc.EvaluateVisitState(visit);
            Assert.AreEqual(visitState, visit.CurrentVisitState);

            // positive transaction
            VisitTransaction positiveTransaction = VisitTransactionFactory.GenerateVisitTransaction(200m, VpTransactionTypeEnum.HsCharge);
            positiveTransaction.InsertDate = statementPeriodEndDate.AddDays(1);

            svc.EvaluateVisitState(visit);
            //Assert.AreEqual(VisitStateEnum.PendingStatement, visit.VisitState.VisitStateEnum);
        }
    }
}
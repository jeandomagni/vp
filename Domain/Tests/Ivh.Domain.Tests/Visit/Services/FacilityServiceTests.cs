﻿namespace Ivh.Domain.Visit.Services
{
    using System;
    using Base.Interfaces;
    using Common.Tests.Helpers.Base;
    using Entities;
    using Interfaces;
    using Moq;
    using NUnit.Framework;
    using Settings.Entities;
    using Settings.Interfaces;

    [TestFixture]
    public class FacilityServiceTests : DomainTestBase
    {
        private FacilityService _facilitySvc;
        private Mock<IFacilityRepository> _facilityRepositoryMock;
        private Mock<IClientService> _clientSvcMock;
        private Mock<IApplicationSettingsService> _appSettingsSvcMock;
        private IDomainServiceCommonService _domainSvcCommonSvc;

        [SetUp]
        public void BeforeEach()
        {
            this._clientSvcMock = new Mock<IClientService>();
            this._facilityRepositoryMock = new Mock<IFacilityRepository>();
            this._appSettingsSvcMock = new Mock<IApplicationSettingsService>();

            this._appSettingsSvcMock
                .Setup(t => t.PastDueAgingCount)
                .Returns(() => new ApplicationSetting<int>(() => new Lazy<int>(() => 1)));

            this._domainSvcCommonSvc =
                DomainServiceCommonServices
                    .CreateDomainServiceCommonService(this._appSettingsSvcMock, null, this._clientSvcMock);

            this._facilitySvc =
                new FacilityService(
                    new Lazy<IFacilityRepository>(() => this._facilityRepositoryMock.Object),
                    new Lazy<IDomainServiceCommonService>(() => this._domainSvcCommonSvc));
        }

        [Test]
        public void GetFacility_ValidId_ReturnsFacility()
        {
            Facility fakeFacility = new Facility
            {
                FacilityId = 15,
                LogoFilename = "sample-logo.jpg",
                FacilityDescription = "Medical Center"
            };

            this._facilityRepositoryMock
                .Setup(r => r.GetById(It.IsAny<int>()))
                .Returns(fakeFacility);

            Facility result = this._facilitySvc.GetFacility(1234);

            Assert.AreEqual(fakeFacility.FacilityId, result.FacilityId);
        }
    }
}
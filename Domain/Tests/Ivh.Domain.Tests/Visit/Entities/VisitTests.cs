﻿namespace Ivh.Domain.Visit.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using Common.State.Interfaces;
    using Common.Tests.Helpers.Visit;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.HospitalData;
    using Guarantor.Entities;
    using Interfaces;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class VisitTests : DomainTestBase
    {
        private Mock<IVisitTransactionService> _visitTransactionService;

        [SetUp]
        public void BeforeEachTest()
        {
            this._visitTransactionService = new Mock<IVisitTransactionService>();
        }

        [Test]
        public void SourceSystemKeySetDateGetsSetOnSourceSystemKeySet()
        {
            Visit v = new Visit();

            Assert.IsNull(v.HsCurrentBalanceChangedDate);

            v.SetCurrentBalance(1.00m);
            DateTime? date = v.HsCurrentBalanceChangedDate;
            Assert.IsNotNull(date);

            Thread.Sleep(2);//Sleep for a couple of ms
            //If it's a new value it should update the date
            v.SetCurrentBalance(2.00m);
            DateTime? date2 = v.HsCurrentBalanceChangedDate;
            Assert.AreNotEqual(date, date2);

            Thread.Sleep(2);//Sleep for a couple of ms

            v.SetCurrentBalance(2.00m);
            DateTime? date3 = v.HsCurrentBalanceChangedDate;
            //If it's the same value it shouldnt update the date.
            Assert.AreEqual(date2, date3);
        }

        [Test]
        public void VisitStatus_ChangeStatus_OutboundVisitMessageIsInserted()
        {
            Visit v = new Visit
            {
                CurrentVisitState = VisitStateEnum.Inactive,
                VPGuarantor = new Guarantor()
            };
            int count = v.OutboundVisitMessages.Count;
            int expectedCount = ++count;
            ((IEntity<VisitStateEnum>)v).SetState(VisitStateEnum.Active, null);
            v.InitializeActiveOutboundMessage();

            Assert.AreEqual(expectedCount, v.OutboundVisitMessages.Count);

            OutboundVisitMessage outboundMessage = v.OutboundVisitMessages[expectedCount-1];
            Assert.AreEqual(VpOutboundVisitMessageTypeEnum.VisitStatusSetToActive, outboundMessage.VpOutboundVisitMessageType);

            expectedCount++;
            ((IEntity<VisitStateEnum>)v).SetState(VisitStateEnum.Inactive, null);
            Assert.AreEqual(expectedCount, v.OutboundVisitMessages.Count);

            outboundMessage = v.OutboundVisitMessages[expectedCount-1];
            Assert.AreEqual(VpOutboundVisitMessageTypeEnum.VisitStatusSetToInactive, outboundMessage.VpOutboundVisitMessageType);

        }

        [Obsolete(ObsoleteDescription.VisitTransactions)]
        [Test]
        public void HsAdjustmentsExcludingInsuranceAndInsuranceSumToAdjustments()
        {
            Tuple<Visit, IList<VisitTransaction>> visit = VisitFactory.GenerateActiveVisit(1000m, 1);

            List<VpTransactionTypeEnum> allTransactionTypes = Enum.GetValues(typeof(VpTransactionTypeEnum)).Cast<VpTransactionTypeEnum>().ToList();
            foreach (VpTransactionTypeEnum transactionType in allTransactionTypes)
            {
                VpTransactionType vpTransactionType = new VpTransactionType
                {
                    VpTransactionTypeId = (int)transactionType
                };
            }

            decimal hsAdjustmentsExcludingInsuranceForDateRange = this._visitTransactionService.Object.GetHsAdjustmentsExcludingInsuranceForDateRange(visit.Item2, null, DateTime.UtcNow);
            decimal hsAdjustmentsInsuranceForDateRange = this._visitTransactionService.Object.GetHsAdjustmentsInsuranceForDateRange(visit.Item2, null, DateTime.UtcNow);
            decimal sum = hsAdjustmentsExcludingInsuranceForDateRange + hsAdjustmentsInsuranceForDateRange;
            decimal hsAdjustmentsForDateRange = this._visitTransactionService.Object.GetHsAdjustmentsForDateRange(visit.Item2, null, DateTime.UtcNow);
            Assert.AreEqual(hsAdjustmentsForDateRange, sum);
        }

        [TestCase(AgingTierEnum.NotStatemented, AgingTierEnum.PastDue, true)]
        [TestCase(AgingTierEnum.Good, AgingTierEnum.PastDue, true)]
        [TestCase(AgingTierEnum.MaxAge, AgingTierEnum.PastDue, false)]
        [TestCase(AgingTierEnum.NotStatemented, AgingTierEnum.MaxAge, false)]
        [TestCase(AgingTierEnum.NotStatemented, AgingTierEnum.FinalPastDue, false)]
        [Test]
        public void AgingTierIncreasedToPastDue(AgingTierEnum initialAgingTierEnum, AgingTierEnum endingAgingTierEnum, bool flagShouldBeSet)
        {
            Tuple<Visit, IList<VisitTransaction>> visit = VisitFactory.GenerateActiveVisit(10m, 1, agingCount: (int)initialAgingTierEnum);

            visit.Item1.SetAgingTier(endingAgingTierEnum, string.Empty);

            Assert.AreEqual(flagShouldBeSet, visit.Item1.AgingTierIncreasedToPastDue);
        }
        
        [TestCase(false, false)]
        [TestCase(false, null)]
        [TestCase(null, false)]
        public void ConsolidationMask_Required(bool? isPatientGuarantor, bool? isPatientMinor)
        {
            Mock<Guarantor> guarantor = new Mock<Guarantor>();
            guarantor.Setup(x => x.ConsolidationStatus).Returns(GuarantorConsolidationStatusEnum.Managed);

            Visit visit = new Visit
            {
                IsPatientGuarantor = isPatientGuarantor,
                IsPatientMinor = isPatientMinor,
                VPGuarantor = guarantor.Object
            };

            VisitTransaction transaction = new VisitTransaction
            {
                Visit = visit
            };

            Assert.True(visit.IsMaskedConsolidated);
            Assert.True(transaction.IsMaskedConsolidated);
        }

        [TestCase(true, true)]
        [TestCase(true, false)]
        [TestCase(false, true)]
        [TestCase(true, null)]
        [TestCase(null, true)]
        public void ConsolidationMask_NotRequired(bool? isPatientGuarantor, bool? isPatientMinor)
        {
            Mock<Guarantor> guarantor = new Mock<Guarantor>();
            guarantor.Setup(x => x.ConsolidationStatus).Returns(GuarantorConsolidationStatusEnum.Managed);

            Visit visit = new Visit
            {
                IsPatientGuarantor = isPatientGuarantor,
                IsPatientMinor = isPatientMinor,
                VPGuarantor = guarantor.Object
            };

            VisitTransaction transaction = new VisitTransaction
            {
                Visit = visit
            };

            Assert.False(visit.IsMaskedConsolidated);
            Assert.False(transaction.IsMaskedConsolidated);
        }

        [TestCase(GuarantorConsolidationStatusEnum.Managing)]
        [TestCase(GuarantorConsolidationStatusEnum.NotConsolidated)]
        [TestCase(GuarantorConsolidationStatusEnum.PendingManaging)]
        public void ConsolidationMask_NotRequired_WhenNotManaged(GuarantorConsolidationStatusEnum consolidationStatus)
        {
            Mock<Guarantor> guarantor = new Mock<Guarantor>();
            guarantor.Setup(x => x.ConsolidationStatus).Returns(consolidationStatus);

            Visit visit = new Visit
            {
                IsPatientGuarantor = false,
                IsPatientMinor = false,
                VPGuarantor = guarantor.Object
            };

            VisitTransaction transaction = new VisitTransaction
            {
                Visit = visit
            };

            Assert.False(visit.IsMaskedConsolidated);
            Assert.False(transaction.IsMaskedConsolidated);
        }
    }
}

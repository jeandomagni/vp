﻿namespace Ivh.Domain.Visit.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Enums;
    using Common.Base.Utilities.Helpers;
    using Common.Tests.Helpers.Visit;
    using Common.VisitPay.Enums;
    using NUnit.Framework;

    [TestFixture]
    public class VisitTransactionTests : DomainTestBase
    {
        [Test]
        public void VisitTransaction_DisplayDate_returns_correct_date_with_transaction_type_is_charge()
        {
            DateTime postDate = new DateTime(2017, 01, 01);
            DateTime transactionDate = new DateTime(2017, 02, 02);
            VisitTransaction visitTransaction = new VisitTransaction
            {
                VpTransactionType = new VpTransactionType
                {
                    TransactionType = "Charge"
                },
                PostDate = postDate,
                TransactionDate = transactionDate
            };

            Assert.AreEqual(transactionDate, visitTransaction.DisplayDate);
        }

        //VPNG-18696
        //Display TransactionDate for charges in transactions grid
        [Test]
        public void VisitTransaction_DisplayDate_returns_correct_date_with_transaction_type_not_charge()
        {
            DateTime postDate = new DateTime(2017, 01, 01);
            DateTime transactionDate = new DateTime(2017, 02, 02);
            VisitTransaction visitTransaction = new VisitTransaction
            {
                VpTransactionType = new VpTransactionType
                {
                    TransactionType = "anything-but-charge"
                },
                PostDate = postDate,
                TransactionDate = transactionDate
            };

            Assert.AreEqual(postDate, visitTransaction.DisplayDate);
        }

        //VPNG-22552
        [Test]
        public void InsuranceAdjustmentsIncludeCorrectTransactionTypes()
        {
            List<VpTransactionTypeEnum> allTransactionTypes = Enum.GetValues(typeof(VpTransactionTypeEnum)).Cast<VpTransactionTypeEnum>().ToList();
            foreach (VpTransactionTypeEnum transactionType in allTransactionTypes)
            {
                VpTransactionType vpTransactionType = new VpTransactionType
                {
                    VpTransactionTypeId = (int)transactionType
                };

                if (transactionType == VpTransactionTypeEnum.HsPayorCash ||
                    transactionType == VpTransactionTypeEnum.HsPayorNonCash ||
                    EnumHelper<VpTransactionTypeEnum>.Contains(transactionType, VpTransactionTypeEnumCategory.Insurance))
                {
                    Assert.True(vpTransactionType.IsHsInsuranceAdjustment(), transactionType.ToString());
                }
                else
                {
                    Assert.False(vpTransactionType.IsHsInsuranceAdjustment(), transactionType.ToString());
                }
            }
        }
    }
}
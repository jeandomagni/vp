﻿namespace Ivh.Domain.Async
{
    using Common.Base.Utilities.Extensions;
    using NUnit.Framework;
    using Visit.Entities;

    [TestFixture]
    public class AsyncTests : DomainTestBase
    {
        [Test]
        public void EnsureNoAsyncVoidTests()
        {
            //AssertExtensions.AssertNoAsyncVoidMethods(GetType().Assembly);
            AssertExtensions.AssertNoAsyncVoidMethods(typeof(Visit).Assembly);
            AssertExtensions.AssertNoAsyncVoidMethods(typeof(Domain.FinanceManagement.Payment.Entities.Payment).Assembly);
            AssertExtensions.AssertNoAsyncVoidMethods(typeof(Domain.Email.Entities.Communication).Assembly);
            AssertExtensions.AssertNoAsyncVoidMethods(typeof(Domain.HospitalData.Visit.Entities.Visit).Assembly);
        }
    }
}
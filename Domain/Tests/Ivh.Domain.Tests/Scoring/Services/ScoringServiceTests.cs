﻿namespace Ivh.Domain.Scoring.Services
{
    using System;
    using System.Collections.Generic;
    using Application.Base.Common.Interfaces;
    using Application.Base.Services;
    using Application.HospitalData.Common.Interfaces;
    using Base.Interfaces;
    using Common.Base.Utilities.Helpers;
    using Common.ServiceBus.Interfaces;
    using Common.Tests.Helpers.Base;
    using HospitalData.Scoring.Entities;
    using HospitalData.Scoring.Interfaces;
    using HospitalData.Scoring.Services;
    using HospitalData.Visit.Entities;
    using Logging.Interfaces;
    using Moq;
    using NUnit.Framework;
    using Settings.Entities;
    using Settings.Interfaces;

    [TestFixture]
    public class ScoringServiceTests : DomainTestBase
    {
        [SetUp]
        public void BeforeEachTest()
        {
            this._serviceBusMock = new Mock<IBus>();
            this._scoringEngineMock = new Mock<IRscriptEngine>();
            this._loggingServiceMock = new Mock<ILoggingService>();
            this._scoreRepositoryMock = new Mock<IScoreRepository>();
            this._applicationSettingsService = new Mock<IApplicationSettingsService>();
            this._clientService = new Mock<IClientService>();
            Lazy<TimeZoneHelper> timeZoneHelper = new Lazy<TimeZoneHelper>(() => new TimeZoneHelper(TimeZoneHelper.TimeZones.PacificStandardTime));
            this._clientService.Setup(t => t.GetClient()).Returns(() => new Client(new Dictionary<string, string>
            {
                {"Scoring.LogInfo.Enable", "FALSE"}
                ,{"ClientName", "Methodist"}
            }, timeZoneHelper));
            this._scoringService = new ScoringService(
                new DomainServiceCommonServiceMockBuilder
                {
                    ApplicationSettingsServiceMock = this._applicationSettingsService,
                    ClientServiceMock = this._clientService
                }.CreateServiceLazy(),
                new Lazy<IRscriptEngine>(() => this._scoringEngineMock.Object),
                new Lazy<ILoggingService>(() => this._loggingServiceMock.Object),
                new Lazy<IScoreRepository>(() => this._scoreRepositoryMock.Object)
                );
            
        }

        private Mock<IBus> _serviceBusMock;
        private Mock<IRscriptEngine> _scoringEngineMock;
        private Mock<ILoggingService> _loggingServiceMock;
        private Mock<IScoreRepository> _scoreRepositoryMock;
        private Mock<IApplicationSettingsService> _applicationSettingsService;
        private Mock<IClientService> _clientService;
        private ScoringService _scoringService;

        [Test]
        public void GuarantorAllBalance()
        {
            List<GuarantorAccountVisit> visits = new List<GuarantorAccountVisit>();
            visits.Add(this.GetVisit30Days());
            visits.Add(this.GetVisit31To60Days());
            visits.Add(this.GetVisit61To90Days());
            visits.Add(this.GetVisit91To180Days());
            visits.Add(this.GetVisit181To270Days());
            visits.Add(this.GetVisit271To365Days());
            visits.Add(this.GetTriggeringVisit());

            List<ScoringServiceInput> scoringServiceInputs = new List<ScoringServiceInput>();
            scoringServiceInputs.Add(new ScoringServiceInput
            {
                GuarantorAccountVisits = visits
            });
            IList<ScoringCalculatedInput> inputs = this._scoringService.CalculateScoringInput(scoringServiceInputs);
            Assert.AreEqual(850, inputs[0].GuarantorAllBalance30Days);
            Assert.AreEqual(850, inputs[0].GuarantorAllBalance31To60Days);
            Assert.AreEqual(850, inputs[0].GuarantorAllBalance61To90Days);
            Assert.AreEqual(850, inputs[0].GuarantorAllBalance91To180Days);
            Assert.AreEqual(850, inputs[0].GuarantorAllBalance181To270Days);
            Assert.AreEqual(850, inputs[0].GuarantorAllBalance271To365Days);
        }

        [Test]
        public void GuarantorInactive120DaysBalance()
        {

            List<GuarantorAccountVisit> visits = new List<GuarantorAccountVisit>();
            visits.Add(this.GetVisit91To180Days());
            visits.Add(this.GetVisit181To270Days());
            visits.Add(this.GetVisit271To365Days());
            visits.Add(this.GetVisit366Days());
            visits.Add(this.GetTriggeringVisit());

            List<ScoringServiceInput> scoringServiceInputs = new List<ScoringServiceInput>();
            scoringServiceInputs.Add(new ScoringServiceInput
            {
                GuarantorAccountVisits = visits
            });
            IList<ScoringCalculatedInput> inputs = this._scoringService.CalculateScoringInput(scoringServiceInputs);
            Assert.AreEqual(850, inputs[0].GuarantorInactive120DaysBalance91To180Days);
            Assert.AreEqual(850, inputs[0].GuarantorInactive120DaysBalance181To270Days);
            Assert.AreEqual(850, inputs[0].GuarantorInactive120DaysBalance271To365Days);
        }

        [Test]
        public void GuarantorSelfPayAllBalance()
        {

            List<GuarantorAccountVisit> visits = new List<GuarantorAccountVisit>();
            visits.Add(this.GetTriggeringVisit());
            visits.Add(this.GetTriggeringVisit());
            visits.Add(this.GetVisit30Days());

            List<ScoringServiceInput> scoringServiceInputs = new List<ScoringServiceInput>();
            scoringServiceInputs.Add(new ScoringServiceInput
            {
                GuarantorAccountVisits = visits
            });
            IList<ScoringCalculatedInput> inputs = this._scoringService.CalculateScoringInput(scoringServiceInputs);
            Assert.AreEqual(3000, inputs[0].SelfPayAllBalance);
        }

        [Test]
        public void GuarantorPaidOffAccountsWPatientPaymentsNum()
        {
            List<GuarantorAccountVisit> visits = new List<GuarantorAccountVisit>();
            visits.Add(this.PayOffNonTriggeringVisit(this.GetVisit30Days()));
            visits.Add(this.PayOffNonTriggeringVisit(this.GetVisit31To60Days()));
            visits.Add(this.PayOffNonTriggeringVisit(this.GetVisit61To90Days()));
            visits.Add(this.PayOffNonTriggeringVisit(this.GetVisit91To180Days()));
            visits.Add(this.GetTriggeringVisit());

            List<ScoringServiceInput> scoringServiceInputs = new List<ScoringServiceInput>();
            scoringServiceInputs.Add(new ScoringServiceInput
            {
                GuarantorAccountVisits = visits
            });
            IList<ScoringCalculatedInput> inputs = this._scoringService.CalculateScoringInput(scoringServiceInputs);
            Assert.AreEqual(1, inputs[0].GuarantorPaidOffAccountsWPatientPaymentsNum30Days);
            Assert.AreEqual(1, inputs[0].GuarantorPaidOffAccountsWPatientPaymentsNum31To60Days);
            Assert.AreEqual(1, inputs[0].GuarantorPaidOffAccountsWPatientPaymentsNum61To90Days);
            Assert.AreEqual(1, inputs[0].GuarantorPaidOffAccountsWPatientPaymentsNum91To180Days);
        }

        [Test]
        public void GuarantorPaidOffAccountsPatientPayments()
        {
            List<GuarantorAccountVisit> visits = new List<GuarantorAccountVisit>();
            visits.Add(this.PayOffNonTriggeringVisitWithAdjustments(this.GetVisit30Days()));
            visits.Add(this.PayOffNonTriggeringVisitWithAdjustments(this.GetVisit31To60Days()));
            visits.Add(this.PayOffNonTriggeringVisitWithAdjustments(this.GetVisit61To90Days()));
            visits.Add(this.PayOffNonTriggeringVisitWithAdjustments(this.GetVisit91To180Days()));
            visits.Add(this.GetTriggeringVisit());

            List<ScoringServiceInput> scoringServiceInputs = new List<ScoringServiceInput>();
            scoringServiceInputs.Add(new ScoringServiceInput
            {
                GuarantorAccountVisits = visits
            });
            IList<ScoringCalculatedInput> inputs = this._scoringService.CalculateScoringInput(scoringServiceInputs);
            Assert.AreEqual(500, inputs[0].GuarantorPaidOffAccountsPatientPayments30Days);
            Assert.AreEqual(500, inputs[0].GuarantorPaidOffAccountsPatientPayments31To60Days);
            Assert.AreEqual(500, inputs[0].GuarantorPaidOffAccountsPatientPayments61To90Days);
            Assert.AreEqual(500, inputs[0].GuarantorPaidOffAccountsPatientPayments91To180Days);
        }

        [Test]
        public void GuarantorPaymentsByDischarge()
        {
            List<GuarantorAccountVisit> visits = new List<GuarantorAccountVisit>();
            visits.Add(this.PayOffNonTriggeringVisitWithAdjustments(this.GetVisit30Days()));
            visits.Add(this.PayOffNonTriggeringVisitWithAdjustments(this.GetVisit31To60Days()));
            visits.Add(this.PayOffNonTriggeringVisitWithAdjustments(this.GetVisit61To90Days()));
            visits.Add(this.PayOffNonTriggeringVisitWithAdjustments(this.GetVisit91To180Days()));

            GuarantorAccountVisit triggeringVisit = this.GetTriggeringVisit();
            triggeringVisit.GuarantorAccountTransactions.Add(this.GetGuarantorAccountTransaction(DateTime.UtcNow.Date.AddDays(-5), -100, 4));
            visits.Add(triggeringVisit);

            List<ScoringServiceInput> scoringServiceInputs = new List<ScoringServiceInput>();
            scoringServiceInputs.Add(new ScoringServiceInput
            {
                GuarantorAccountVisits = visits
            });
            IList<ScoringCalculatedInput> inputs = this._scoringService.CalculateScoringInput(scoringServiceInputs);
            Assert.AreEqual(500, inputs[0].GuarantorPaymentsByDischarge30Days);
            Assert.AreEqual(500, inputs[0].GuarantorPaymentsByDischarge31To60Days);
            Assert.AreEqual(500, inputs[0].GuarantorPaymentsByDischarge61To90Days);
            Assert.AreEqual(500, inputs[0].GuarantorPaymentsByDischarge91To180Days);
        }

        [Test]
        public void NoPriorData()
        {
            // No data for any time frame.  Triggering visits are not included
            List<GuarantorAccountVisit> visits = new List<GuarantorAccountVisit>();
            visits.Add(this.GetTriggeringVisit());

            List<ScoringServiceInput> scoringServiceInputs = new List<ScoringServiceInput>();
            scoringServiceInputs.Add(new ScoringServiceInput
            {
                GuarantorAccountVisits = visits
            });
            IList<ScoringCalculatedInput> inputs = this._scoringService.CalculateScoringInput(scoringServiceInputs);
            Assert.AreEqual(true, inputs[0].NoPriorData30Days);
            Assert.AreEqual(true, inputs[0].NoPriorData31To60Days);
            Assert.AreEqual(true, inputs[0].NoPriorData61To90Days);
            Assert.AreEqual(true, inputs[0].NoPriorData91To180Days);

            // data for all time frames
            visits = new List<GuarantorAccountVisit>();
            visits.Add(this.GetVisit30Days());
            visits.Add(this.GetVisit31To60Days());
            visits.Add(this.GetVisit61To90Days());
            visits.Add(this.GetVisit91To180Days());
            visits.Add(this.GetTriggeringVisit());

            scoringServiceInputs = new List<ScoringServiceInput>();
            scoringServiceInputs.Add(new ScoringServiceInput
            {
                GuarantorAccountVisits = visits
            });
            inputs = this._scoringService.CalculateScoringInput(scoringServiceInputs);
            Assert.AreEqual(false, inputs[0].NoPriorData30Days);
            Assert.AreEqual(false, inputs[0].NoPriorData31To60Days);
            Assert.AreEqual(false, inputs[0].NoPriorData61To90Days);
            Assert.AreEqual(false, inputs[0].NoPriorData91To180Days);
        }

        [Test]
        public void PrimaryInsuranceType()
        {
            /*
            SelfPayAfterInsurance = 1,
            SelfPayAfterMedicare = 2,
            PureSelfPay = 3
            */

            // both with insurance
            List<GuarantorAccountVisit> visits = new List<GuarantorAccountVisit>();
            visits.Add(this.GetTriggeringVisit(2, 1));
            visits.Add(this.GetTriggeringVisit(2, 2));

            List<ScoringServiceInput> scoringServiceInputs = new List<ScoringServiceInput>();
            scoringServiceInputs.Add(new ScoringServiceInput
            {
                GuarantorAccountVisits = visits
            });
            IList<ScoringCalculatedInput> inputs = this._scoringService.CalculateScoringInput(scoringServiceInputs);
            Assert.AreEqual("AP", inputs[0].PrimaryInsuranceTypeN);
            Assert.AreEqual("AP", inputs[0].PrimaryInsuranceTypeA);
            Assert.AreEqual("AP", inputs[0].PrimaryInsuranceType1);

            // one insurance one selfpay, insurance is newest
            visits = new List<GuarantorAccountVisit>();
            visits.Add(this.GetTriggeringVisit(2, 1));
            visits.Add(this.GetTriggeringVisit(2, 3));

            scoringServiceInputs = new List<ScoringServiceInput>();
            scoringServiceInputs.Add(new ScoringServiceInput
            {
                GuarantorAccountVisits = visits
            });
            inputs = this._scoringService.CalculateScoringInput(scoringServiceInputs);
            Assert.AreEqual("AP", inputs[0].PrimaryInsuranceTypeN);
            Assert.AreEqual("AP", inputs[0].PrimaryInsuranceTypeA);
            Assert.AreEqual("S", inputs[0].PrimaryInsuranceType1);

            // both self pay
            visits = new List<GuarantorAccountVisit>();
            visits.Add(this.GetTriggeringVisit(2, 3));
            visits.Add(this.GetTriggeringVisit(2, 3));

            scoringServiceInputs = new List<ScoringServiceInput>();
            scoringServiceInputs.Add(new ScoringServiceInput
            {
                GuarantorAccountVisits = visits
            });
            inputs = this._scoringService.CalculateScoringInput(scoringServiceInputs);
            Assert.AreEqual("S", inputs[0].PrimaryInsuranceTypeN);
            Assert.AreEqual("S", inputs[0].PrimaryInsuranceTypeA);
            Assert.AreEqual("S", inputs[0].PrimaryInsuranceType1);

            // one insurance one selfpay, selfpay is newest
            visits = new List<GuarantorAccountVisit>();
            visits.Add(this.GetTriggeringVisit(2, 3));
            visits.Add(this.GetTriggeringVisit(2, 1));
            visits[1].FirstSelfPayDate = DateTime.UtcNow.Date.AddDays(-1);

            scoringServiceInputs = new List<ScoringServiceInput>();
            scoringServiceInputs.Add(new ScoringServiceInput
            {
                GuarantorAccountVisits = visits
            });
            inputs = this._scoringService.CalculateScoringInput(scoringServiceInputs);
            Assert.AreEqual("S", inputs[0].PrimaryInsuranceTypeN);
            Assert.AreEqual("AP", inputs[0].PrimaryInsuranceTypeA);
            Assert.AreEqual("S", inputs[0].PrimaryInsuranceType1);

            // one insurance visit
            visits = new List<GuarantorAccountVisit>();
            visits.Add(this.GetTriggeringVisit(2, 1));
            visits[0].FirstSelfPayDate = DateTime.UtcNow;

            scoringServiceInputs = new List<ScoringServiceInput>();
            scoringServiceInputs.Add(new ScoringServiceInput
            {
                GuarantorAccountVisits = visits
            });
            inputs = this._scoringService.CalculateScoringInput(scoringServiceInputs);
            Assert.AreEqual("AP", inputs[0].PrimaryInsuranceTypeN);
            Assert.AreEqual("AP", inputs[0].PrimaryInsuranceTypeA);
            Assert.AreEqual("AP", inputs[0].PrimaryInsuranceType1);

            // one SelfPay visit
            visits = new List<GuarantorAccountVisit>();
            visits.Add(this.GetTriggeringVisit(2, 3));
            visits[0].FirstSelfPayDate = DateTime.UtcNow;

            scoringServiceInputs = new List<ScoringServiceInput>();
            scoringServiceInputs.Add(new ScoringServiceInput
            {
                GuarantorAccountVisits = visits
            });
            inputs = this._scoringService.CalculateScoringInput(scoringServiceInputs);
            Assert.AreEqual("S", inputs[0].PrimaryInsuranceTypeN);
            Assert.AreEqual("S", inputs[0].PrimaryInsuranceTypeA);
            Assert.AreEqual("S", inputs[0].PrimaryInsuranceType1);

        }

        [Test]
        public void PatientType()
        {
            /*
            Unknown = 1,
            Inpatient = 2,
            Outpatient = 3,
            Emergency = 4
             */
            List<GuarantorAccountVisit> visits = new List<GuarantorAccountVisit>();
            visits.Add(this.GetTriggeringVisit(2));
            visits.Add(this.GetTriggeringVisit(3));
            visits.Add(this.GetTriggeringVisit(4));

            List<ScoringServiceInput> scoringServiceInputs = new List<ScoringServiceInput>();
            scoringServiceInputs.Add(new ScoringServiceInput
            {
                GuarantorAccountVisits = visits
            });
            IList<ScoringCalculatedInput> inputs = this._scoringService.CalculateScoringInput(scoringServiceInputs);
            Assert.AreEqual("E", inputs[0].PatientType2);

            //
            visits = new List<GuarantorAccountVisit>();
            visits.Add(this.GetTriggeringVisit(2));
            visits.Add(this.GetTriggeringVisit(3));

            scoringServiceInputs = new List<ScoringServiceInput>();
            scoringServiceInputs.Add(new ScoringServiceInput
            {
                GuarantorAccountVisits = visits
            });
            inputs = this._scoringService.CalculateScoringInput(scoringServiceInputs);
            Assert.AreEqual("I", inputs[0].PatientType2);

            //
            visits = new List<GuarantorAccountVisit>();
            visits.Add(this.GetTriggeringVisit(3));

            scoringServiceInputs = new List<ScoringServiceInput>();
            scoringServiceInputs.Add(new ScoringServiceInput
            {
                GuarantorAccountVisits = visits
            });
            inputs = this._scoringService.CalculateScoringInput(scoringServiceInputs);
            Assert.AreEqual("O", inputs[0].PatientType2);
        }


        [Test]
        public void ScoringAccountHasPayorTrans()
        {
            List<GuarantorAccountVisit> visits = new List<GuarantorAccountVisit>();
            visits.Add(this.GetTriggeringVisit());

            List<ScoringServiceInput> scoringServiceInputs = new List<ScoringServiceInput>();
            scoringServiceInputs.Add(new ScoringServiceInput
            {
                GuarantorAccountVisits = visits
            });
            IList<ScoringCalculatedInput> inputs = this._scoringService.CalculateScoringInput(scoringServiceInputs);
            Assert.AreEqual(false, inputs[0].ScoringAccountHasPayorTrans);


            GuarantorAccountVisit triggeringVisit = this.GetTriggeringVisit();
            triggeringVisit.GuarantorAccountTransactions.Add(this.GetGuarantorAccountTransaction(DateTime.UtcNow.Date.AddDays(-5), -100, 6));
            visits.Add(triggeringVisit);

            scoringServiceInputs = new List<ScoringServiceInput>();
            scoringServiceInputs.Add(new ScoringServiceInput
            {
                GuarantorAccountVisits = visits
            });
            inputs = this._scoringService.CalculateScoringInput(scoringServiceInputs);
            Assert.AreEqual(true, inputs[0].ScoringAccountHasPayorTrans);

        }


        private GuarantorAccountVisit CreateVisit(DateTime dischargeDate, DateTime firstSelfPayDate, decimal hsCurrentBalance, bool triggeredScoring, 
            int patientTypeId = 2, int selfPayClassId = 3, bool isEligibleToScore = true, int lifeCycleStageId = 6)
        {
            GuarantorAccountVisit visit = new GuarantorAccountVisit
            {
                VisitBatchId = 1,
                VisitId = 1,
                HsGuarantorId = 1,
                ScoringBatchId = 1,
                TriggeredScoring = triggeredScoring,
                IsEligibleToScore = isEligibleToScore,
                BillingSystemId = 1,
                SourceSystemKey = "ssk",
                DischargeDate = dischargeDate,
                HsCurrentBalance = hsCurrentBalance,
                SelfPayBalance = hsCurrentBalance,
                BillingApplication = "HB",
                LifeCycleStageId = lifeCycleStageId,
                PatientTypeId = patientTypeId,
                SelfPayClassId = selfPayClassId,
                FirstSelfPayDate = firstSelfPayDate,
                ScoringDate = DateTime.UtcNow
            };
            return visit;
        }


        private GuarantorAccountTransaction GetGuarantorAccountTransaction(DateTime postDate, decimal amount, int transactionTypeId)
        {
            GuarantorAccountTransaction transaction = new GuarantorAccountTransaction
            {
                VisitTransactionId = 1,
                BillingSystemId = 1,
                SourceSystemKey = "ssk",
                TransactionDate = postDate,
                PostDate = postDate,
                TransactionAmount = amount,
                VpTransactionType = new VpTransactionType { VpTransactionTypeId = transactionTypeId, TransactionGroup = "Patient Cash" },
                TransactionDescription = "",
                VpPaymentAllocationId = 1,
                TransactionCodeId = 1
            };

            return transaction;
        }

        private GuarantorAccountVisit PayOffNonTriggeringVisit(GuarantorAccountVisit visit)
        {
            GuarantorAccountTransaction lastTransaction = visit.GuarantorAccountTransactions[visit.GuarantorAccountTransactions.Count - 1];
            visit.GuarantorAccountTransactions.Add(this.GetGuarantorAccountTransaction(lastTransaction.PostDate, -850, 4));
            visit.HsCurrentBalance = 0;
            visit.SelfPayBalance = 0;
            return visit;
        }

        private GuarantorAccountVisit PayOffNonTriggeringVisitWithAdjustments(GuarantorAccountVisit visit)
        {
            GuarantorAccountTransaction lastTransaction = visit.GuarantorAccountTransactions[visit.GuarantorAccountTransactions.Count - 1];
            visit.GuarantorAccountTransactions.Add(this.GetGuarantorAccountTransaction(lastTransaction.PostDate, -350, 4));
            visit.GuarantorAccountTransactions.Add(this.GetGuarantorAccountTransaction(lastTransaction.PostDate, -500, 8));
            visit.HsCurrentBalance = 0;
            visit.SelfPayBalance = 0;
            return visit;
        }

        private GuarantorAccountVisit GetTriggeringVisit(int patientTypeId = 2, int selfPayClassId = 3)
        {
            // triggered visit
            GuarantorAccountVisit visit = this.CreateVisit(DateTime.UtcNow.Date.AddDays(-10), DateTime.UtcNow.Date, 1500, true, patientTypeId, selfPayClassId);
            List<GuarantorAccountTransaction> guarantorAccountTransactions = new List<GuarantorAccountTransaction>();
            guarantorAccountTransactions.Add(this.GetGuarantorAccountTransaction(DateTime.UtcNow.Date.AddDays(-10), 1000, 1));
            guarantorAccountTransactions.Add(this.GetGuarantorAccountTransaction(DateTime.UtcNow.Date.AddDays(-10), 500, 1));
            visit.GuarantorAccountTransactions = guarantorAccountTransactions;
            return visit;
        }

        private GuarantorAccountVisit GetVisit30Days()
        {
            // old visit 30Days
            GuarantorAccountVisit visit = this.CreateVisit(DateTime.UtcNow.Date.AddDays(-15), DateTime.UtcNow.Date.AddDays(-14), 850, false);
            List<GuarantorAccountTransaction> guarantorAccountTransactions = new List<GuarantorAccountTransaction>();
            guarantorAccountTransactions.Add(this.GetGuarantorAccountTransaction(DateTime.UtcNow.Date.AddDays(-15), 1000, 1));
            guarantorAccountTransactions.Add(this.GetGuarantorAccountTransaction(DateTime.UtcNow.Date.AddDays(-13), -150, 4));
            visit.GuarantorAccountTransactions = guarantorAccountTransactions;
            return visit;
        }

        private GuarantorAccountVisit GetVisit31To60Days()
        {
            // old visit 31To60Days
            GuarantorAccountVisit visit = this.CreateVisit(DateTime.UtcNow.Date.AddDays(-45), DateTime.UtcNow.Date.AddDays(-44), 850, false);
            List<GuarantorAccountTransaction> guarantorAccountTransactions = new List<GuarantorAccountTransaction>();
            guarantorAccountTransactions.Add(this.GetGuarantorAccountTransaction(DateTime.UtcNow.Date.AddDays(-45), 1000, 1));
            guarantorAccountTransactions.Add(this.GetGuarantorAccountTransaction(DateTime.UtcNow.Date.AddDays(-44), -150, 4));
            visit.GuarantorAccountTransactions = guarantorAccountTransactions;
            return visit;
        }

        private GuarantorAccountVisit GetVisit61To90Days()
        {
            // old visit 61To90Days
            GuarantorAccountVisit visit = this.CreateVisit(DateTime.UtcNow.Date.AddDays(-75), DateTime.UtcNow.Date.AddDays(-70), 850, false);
            List<GuarantorAccountTransaction> guarantorAccountTransactions = new List<GuarantorAccountTransaction>();
            guarantorAccountTransactions.Add(this.GetGuarantorAccountTransaction(DateTime.UtcNow.Date.AddDays(-75), 1000, 1));
            guarantorAccountTransactions.Add(this.GetGuarantorAccountTransaction(DateTime.UtcNow.Date.AddDays(-65), -150, 4));
            visit.GuarantorAccountTransactions = guarantorAccountTransactions;
            return visit;
        }

        private GuarantorAccountVisit GetVisit91To180Days()
        {
            // old visit 91To180Days
            GuarantorAccountVisit visit = this.CreateVisit(DateTime.UtcNow.Date.AddDays(-150), DateTime.UtcNow.Date.AddDays(-140), 850, false);
            List<GuarantorAccountTransaction> guarantorAccountTransactions = new List<GuarantorAccountTransaction>();
            guarantorAccountTransactions.Add(this.GetGuarantorAccountTransaction(DateTime.UtcNow.Date.AddDays(-150), 1000, 1));
            guarantorAccountTransactions.Add(this.GetGuarantorAccountTransaction(DateTime.UtcNow.Date.AddDays(-130), -150, 4));
            visit.GuarantorAccountTransactions = guarantorAccountTransactions;
            return visit;
        }

        private GuarantorAccountVisit GetVisit181To270Days()
        {
            // old visit 181To270Days
            GuarantorAccountVisit visit = this.CreateVisit(DateTime.UtcNow.Date.AddDays(-250), DateTime.UtcNow.Date.AddDays(-200), 850, false);
            List<GuarantorAccountTransaction> guarantorAccountTransactions = new List<GuarantorAccountTransaction>();
            guarantorAccountTransactions.Add(this.GetGuarantorAccountTransaction(DateTime.UtcNow.Date.AddDays(-250), 1000, 1));
            guarantorAccountTransactions.Add(this.GetGuarantorAccountTransaction(DateTime.UtcNow.Date.AddDays(-190), -150, 4));
            visit.GuarantorAccountTransactions = guarantorAccountTransactions;
            return visit;
        }

        private GuarantorAccountVisit GetVisit271To365Days()
        {
            // old visit 271To365Days
            GuarantorAccountVisit visit = this.CreateVisit(DateTime.UtcNow.Date.AddDays(-360), DateTime.UtcNow.Date.AddDays(-350), 850, false);
            List<GuarantorAccountTransaction> guarantorAccountTransactions = new List<GuarantorAccountTransaction>();
            guarantorAccountTransactions.Add(this.GetGuarantorAccountTransaction(DateTime.UtcNow.Date.AddDays(-360), 1000, 1));
            guarantorAccountTransactions.Add(this.GetGuarantorAccountTransaction(DateTime.UtcNow.Date.AddDays(-345), -150, 4));
            visit.GuarantorAccountTransactions = guarantorAccountTransactions;
            return visit;
        }

        private GuarantorAccountVisit GetVisit366Days()
        {
            // old visit 271To365Days
            GuarantorAccountVisit visit = this.CreateVisit(DateTime.UtcNow.Date.AddDays(-400), DateTime.UtcNow.Date.AddDays(-390), 850, false);
            List<GuarantorAccountTransaction> guarantorAccountTransactions = new List<GuarantorAccountTransaction>();
            guarantorAccountTransactions.Add(this.GetGuarantorAccountTransaction(DateTime.UtcNow.Date.AddDays(-400), 1000, 1));
            guarantorAccountTransactions.Add(this.GetGuarantorAccountTransaction(DateTime.UtcNow.Date.AddDays(-385), -150, 4));
            visit.GuarantorAccountTransactions = guarantorAccountTransactions;
            return visit;
        }
    }
}
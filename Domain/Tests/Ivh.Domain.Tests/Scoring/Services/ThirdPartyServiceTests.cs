﻿namespace Ivh.Domain.Scoring.Services
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using AutoMapper;
    using Common.Base.Utilities.Helpers;
    using Common.Tests.Helpers.Base;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using HospitalData.Scoring.Entities;
    using HospitalData.Scoring.Interfaces;
    using HospitalData.Scoring.Services;
    using Logging.Interfaces;
    using Moq;
    using NUnit.Framework;
    using Settings.Entities;
    using Settings.Interfaces;

    [TestFixture]
    public class ThirdPartyServiceTests : DomainTestBase
    {
        private ThirdPartyDataService _thirdPartyDataService;
        private Mock<IGuarantorBatchRepository> _guarantorBatchRepository;
        private Mock<IThirdPartyDataRepository> _thirdPartyRepository;
        private Mock<IApplicationSettingsService> _applicationSettingsService;
        private Mock<IThirdPartyDataProvider> _thirdPartyDataProvider;
        private Mock<IMetricsProvider> _metricsProvider;
        private Mock<IClientService> _clientService;
        private Mock<ILoggingService> _loggingService;

        private const decimal ProbScoreFromApi = 0.1m;
        private const decimal ProbScoreFromDb = 0.2m;
        private const decimal ProbScoreFromDbExpired = 0.3m;
        private const decimal ProbScoreZero = 0;


        [SetUp]
        public void Setup()
        {
            this._guarantorBatchRepository = new Mock<IGuarantorBatchRepository>();
            this._thirdPartyRepository = new Mock<IThirdPartyDataRepository>();
            this._applicationSettingsService = new Mock<IApplicationSettingsService>();
            this._thirdPartyDataProvider = new Mock<IThirdPartyDataProvider>();
            this._metricsProvider = new Mock<IMetricsProvider>();
            this._clientService = new Mock<IClientService>();
            this._loggingService = new Mock<ILoggingService>();
            Lazy<TimeZoneHelper> timeZoneHelper = new Lazy<TimeZoneHelper>(() => new TimeZoneHelper(TimeZoneHelper.TimeZones.PacificStandardTime));
            this._clientService.Setup(t => t.GetClient()).Returns(() => new Client(new Dictionary<string, string>
            {
                {"Scoring.UseThirdPartyApi", "FALSE"},
                {"Scoring.ThirdPartyRefreshAge", "90"}
            }, timeZoneHelper));

            this._thirdPartyRepository.Setup(t => t.GetThirdPartyData(It.IsAny<int>(), It.IsAny<ScoringThirdPartyDataSetTypeEnum>(), It.IsAny<int>())).Returns((ThirdPartyData)null);
            this._applicationSettingsService.Setup(t => t.ScoringThirdPartySecurityToken).Returns(() => new ApplicationSetting<string>(() => new Lazy<string>(() => "")));
            this._applicationSettingsService.Setup(t => t.ScoringThirdPartyUrl).Returns(() => new ApplicationSetting<string>(() => new Lazy<string>(() => "")));
            this._thirdPartyDataProvider.Setup(t => t.CallThirdPartyDataService(It.IsAny<ThirdPartyRequestInput>(), It.IsAny<string>())).Returns(
                (ThirdPartyRequestInput requestInput, string url) =>
                    Task.FromResult(new ThirdPartyData
                    {
                        HsGuarantorId = requestInput.HsGuarantorId,
                        ProbabilityScore = ProbScoreFromApi
                    }));
            this._applicationSettingsService.Setup(t => t.Client).Returns(() => new ApplicationSetting<string>(() => new Lazy<string>(() => "")));
            this._guarantorBatchRepository.Setup(x => x.GetByGuarantorIdScoringBatchId(It.IsAny<int>(), It.IsAny<int>())).Returns(new GuarantorBatch
            {
                ParentHsGuarantorId = 1,
                ChildHsGuarantorId = 1,
                FirstName = "FromDb",
                LastName = "Test",
                Address1 = "123 Test",
                City = "Boise",
                StateProvince = "Id",
                PostalCode = "12345"
            });
            /*Mapper.Initialize(x => { x.CreateMap<ThirdPartyData, ScoringThirdPartyDto>(); });*/
            this._thirdPartyDataService = new ThirdPartyDataService(
                new DomainServiceCommonServiceMockBuilder
                {
                    ApplicationSettingsServiceMock = this._applicationSettingsService,
                    ClientServiceMock = this._clientService
                }.CreateServiceLazy(),
                new Lazy<IGuarantorBatchRepository>(() => this._guarantorBatchRepository.Object),
                new Lazy<IThirdPartyDataRepository>(() => this._thirdPartyRepository.Object),
                new Lazy<IThirdPartyDataProvider>(() => this._thirdPartyDataProvider.Object),
                new Lazy<IMetricsProvider>(() => this._metricsProvider.Object),
                new Lazy<ILoggingService>(() => this._loggingService.Object));
        }


        [Test]
        public void ValidGeographicRequests()
        {
            // (City and State) or Postal
            this._guarantorBatchRepository.Setup(x => x.GetByGuarantorIdScoringBatchId(It.IsAny<int>(), It.IsAny<int>())).Returns(new GuarantorBatch
            {
                ParentHsGuarantorId = 1,
                FirstName = "FromDb",
                LastName = "Test",
                Address1 = "123 Test",
                City = "Boise",
                //StateProvince = "Id",
                PostalCode = "12345"
            });
            ScoringThirdPartyDto result = this._thirdPartyDataService.ExtractThirdParty(1, true, 1, ScoringThirdPartyDatasetEnum.Scoring, ScoringThirdPartyDataSetTypeEnum.Scoring);
            Assert.NotNull(result);

            this._guarantorBatchRepository.Setup(x => x.GetByGuarantorIdScoringBatchId(It.IsAny<int>(), It.IsAny<int>())).Returns(new GuarantorBatch
            {
                ParentHsGuarantorId = 1,
                FirstName = "FromDb",
                LastName = "Test",
                Address1 = "123 Test",
                //City = "Boise",
                StateProvince = "Id",
                PostalCode = "12345"
            });
            result = this._thirdPartyDataService.ExtractThirdParty(1, true, 1, ScoringThirdPartyDatasetEnum.Scoring, ScoringThirdPartyDataSetTypeEnum.Scoring);
            Assert.NotNull(result);

            this._guarantorBatchRepository.Setup(x => x.GetByGuarantorIdScoringBatchId(It.IsAny<int>(), It.IsAny<int>())).Returns(new GuarantorBatch
            {
                ParentHsGuarantorId = 1,
                FirstName = "FromDb",
                LastName = "Test",
                Address1 = "123 Test",
                //City = "Boise",
                //StateProvince = "Id",
                PostalCode = "12345"
            });
            result = this._thirdPartyDataService.ExtractThirdParty(1, true, 1, ScoringThirdPartyDatasetEnum.Scoring, ScoringThirdPartyDataSetTypeEnum.Scoring);
            Assert.NotNull(result);

            this._guarantorBatchRepository.Setup(x => x.GetByGuarantorIdScoringBatchId(It.IsAny<int>(), It.IsAny<int>())).Returns(new GuarantorBatch
            {
                ParentHsGuarantorId = 1,
                FirstName = "FromDb",
                LastName = "Test",
                Address1 = "123 Test",
                City = "Boise",
                StateProvince = "Id",
                //PostalCode = "12345"
            });
            result = this._thirdPartyDataService.ExtractThirdParty(1, true, 1, ScoringThirdPartyDatasetEnum.Scoring, ScoringThirdPartyDataSetTypeEnum.Scoring);
            Assert.NotNull(result);
        }

        [Test]
        public void InvalidGeographicRequests()
        {
            this._guarantorBatchRepository.Setup(x => x.GetByGuarantorIdScoringBatchId(It.IsAny<int>(), It.IsAny<int>())).Returns(new GuarantorBatch
            {
                ParentHsGuarantorId = 1,
                FirstName = "FromDb",
                LastName = "Test",
                Address1 = "123 Test",
                City = "Boise",
                //StateProvince = "Id",
                //PostalCode = "12345"
            });
            ScoringThirdPartyDto result = this._thirdPartyDataService.ExtractThirdParty(1, true, 1, ScoringThirdPartyDatasetEnum.Scoring, ScoringThirdPartyDataSetTypeEnum.Scoring);
            Assert.Null(result);

            this._guarantorBatchRepository.Setup(x => x.GetByGuarantorIdScoringBatchId(It.IsAny<int>(), It.IsAny<int>())).Returns(new GuarantorBatch
            {
                ParentHsGuarantorId = 1,
                FirstName = "FromDb",
                LastName = "Test",
                Address1 = "123 Test",
                //City = "Boise",
                StateProvince = "Id",
                //PostalCode = "12345"
            });
            result = this._thirdPartyDataService.ExtractThirdParty(1, true, 1, ScoringThirdPartyDatasetEnum.Scoring, ScoringThirdPartyDataSetTypeEnum.Scoring);
            Assert.Null(result);

            this._guarantorBatchRepository.Setup(x => x.GetByGuarantorIdScoringBatchId(It.IsAny<int>(), It.IsAny<int>())).Returns(new GuarantorBatch
            {
                ParentHsGuarantorId = 1,
                FirstName = "FromDb",
                LastName = "Test",
                Address1 = "123 Test",
                //City = "Boise",
                //StateProvince = "Id",
                //PostalCode = "12345"
            });
            result = this._thirdPartyDataService.ExtractThirdParty(1, true, 1, ScoringThirdPartyDatasetEnum.Scoring, ScoringThirdPartyDataSetTypeEnum.Scoring);
            Assert.Null(result);
        }

        [Test]
        public void ValidNameAddressRequests()
        {
            this._guarantorBatchRepository.Setup(x => x.GetByGuarantorIdScoringBatchId(It.IsAny<int>(), It.IsAny<int>())).Returns(new GuarantorBatch
            {
                ParentHsGuarantorId = 1,
                FirstName = "FromDb",
                LastName = "Test",
                Address1 = "123 Test",
                City = "Boise",
                StateProvince = "Id",
                PostalCode = "12345"
            });
            ScoringThirdPartyDto result = this._thirdPartyDataService.ExtractThirdParty(1, true, 1, ScoringThirdPartyDatasetEnum.Scoring, ScoringThirdPartyDataSetTypeEnum.Scoring);
            Assert.NotNull(result);
        }

        [Test]
        public void InValidNameAddressRequests()
        {
            this._guarantorBatchRepository.Setup(x => x.GetByGuarantorIdScoringBatchId(It.IsAny<int>(), It.IsAny<int>())).Returns(new GuarantorBatch
            {
                ParentHsGuarantorId = 1,
               // FirstName = "FromDb",
                LastName = "Test",
                Address1 = "123 Test",
                City = "Boise",
                StateProvince = "Id",
                PostalCode = "12345"
            });
            ScoringThirdPartyDto result = this._thirdPartyDataService.ExtractThirdParty(1, true, 1, ScoringThirdPartyDatasetEnum.Scoring, ScoringThirdPartyDataSetTypeEnum.Scoring);
            Assert.Null(result);

            this._guarantorBatchRepository.Setup(x => x.GetByGuarantorIdScoringBatchId(It.IsAny<int>(), It.IsAny<int>())).Returns(new GuarantorBatch
            {
                ParentHsGuarantorId = 1,
                FirstName = "FromDb",
                //LastName = "Test",
                Address1 = "123 Test",
                City = "Boise",
                StateProvince = "Id",
                PostalCode = "12345"
            });
            result = this._thirdPartyDataService.ExtractThirdParty(1, true, 1, ScoringThirdPartyDatasetEnum.Scoring, ScoringThirdPartyDataSetTypeEnum.Scoring);
            Assert.Null(result);

            this._guarantorBatchRepository.Setup(x => x.GetByGuarantorIdScoringBatchId(It.IsAny<int>(), It.IsAny<int>())).Returns(new GuarantorBatch
            {
                ParentHsGuarantorId = 1,
                FirstName = "FromDb",
                LastName = "Test",
               //Address1 = "123 Test",
                City = "Boise",
                StateProvince = "Id",
                PostalCode = "12345"
            });
            result = this._thirdPartyDataService.ExtractThirdParty(1, true, 1, ScoringThirdPartyDatasetEnum.Scoring, ScoringThirdPartyDataSetTypeEnum.Scoring);
            Assert.Null(result);

        }

        [Test]
        public void DataRequested_Purchase_DataExistsFromDb()
        {
            this._thirdPartyRepository.Setup(t => t.GetThirdPartyData(It.IsAny<int>(), It.IsAny<ScoringThirdPartyDataSetTypeEnum>(), It.IsAny<int>())).Returns(new ThirdPartyData
            {
                HsGuarantorId = 1,
                CreatedDate = DateTime.UtcNow,
                ProbabilityScore = ProbScoreFromDb
            });
            ScoringThirdPartyDto result = this._thirdPartyDataService.ExtractThirdParty(1, true, 1, ScoringThirdPartyDatasetEnum.Scoring, ScoringThirdPartyDataSetTypeEnum.Scoring);
            Assert.NotNull(result);
            Assert.AreEqual(1, result.HsGuarantorId);
            Assert.AreEqual(ProbScoreFromDb, result.ProbabilityScore);
        }

        [Test]
        public void DataRequested_Purchase_DataDoesNotExistFromDb()
        {
            ScoringThirdPartyDto result = this._thirdPartyDataService.ExtractThirdParty(1, true, 1, ScoringThirdPartyDatasetEnum.Scoring, ScoringThirdPartyDataSetTypeEnum.Scoring);
            Assert.NotNull(result);
            Assert.AreEqual(1, result.HsGuarantorId);
            Assert.AreEqual(ProbScoreFromApi, result.ProbabilityScore);
        }

        [Test]
        public void DataRequested_Purchase_DataExistsFromDbButExpired()
        {
            this._thirdPartyRepository.Setup(t => t.GetThirdPartyData(It.IsAny<int>(), It.IsAny<ScoringThirdPartyDataSetTypeEnum>(), It.IsAny<int>())).Returns(new ThirdPartyData
            {
                HsGuarantorId = 1,
                CreatedDate = DateTime.UtcNow.AddDays(-100),
                ProbabilityScore = ProbScoreFromDbExpired
            });
            ScoringThirdPartyDto result = this._thirdPartyDataService.ExtractThirdParty(1, true, 1, ScoringThirdPartyDatasetEnum.Scoring, ScoringThirdPartyDataSetTypeEnum.Scoring);
            Assert.NotNull(result);
            Assert.AreEqual(1, result.HsGuarantorId);
            Assert.AreEqual(ProbScoreFromDbExpired, result.ProbabilityScore);
        }

        [Test]
        public void DataRequested_Purchase_DataExistsFromDbButInvalidProbScore()
        {
            this._thirdPartyRepository.Setup(t => t.GetThirdPartyData(It.IsAny<int>(), It.IsAny<ScoringThirdPartyDataSetTypeEnum>(), It.IsAny<int>())).Returns(new ThirdPartyData
            {
                HsGuarantorId = 1,
                CreatedDate = DateTime.UtcNow,
                ProbabilityScore = ProbScoreZero
            });
            ScoringThirdPartyDto result = this._thirdPartyDataService.ExtractThirdParty(1, true, 1, ScoringThirdPartyDatasetEnum.Scoring, ScoringThirdPartyDataSetTypeEnum.Scoring);
            Assert.NotNull(result);
            Assert.AreEqual(1, result.HsGuarantorId);
            Assert.AreEqual(ProbScoreZero, result.ProbabilityScore);
        }

        [Test]
        public void DataRequested_DoNotPurchase_DataDoesNotExistFromDb()
        {
            ScoringThirdPartyDto result = this._thirdPartyDataService.ExtractThirdParty(1, false, 1, ScoringThirdPartyDatasetEnum.Scoring, ScoringThirdPartyDataSetTypeEnum.Scoring);
            Assert.Null(result);
        }

        [Test]
        public void DataRequested_DoNotPurchase_DataExistsFromDb()
        {
            this._thirdPartyRepository.Setup(t => t.GetThirdPartyData(It.IsAny<int>(), It.IsAny<ScoringThirdPartyDataSetTypeEnum>(), It.IsAny<int>())).Returns(new ThirdPartyData
            {
                HsGuarantorId = 1,
                CreatedDate = DateTime.UtcNow,
                ProbabilityScore = ProbScoreFromDb
            });
            ScoringThirdPartyDto result = this._thirdPartyDataService.ExtractThirdParty(1, false, 1, ScoringThirdPartyDatasetEnum.Scoring, ScoringThirdPartyDataSetTypeEnum.Scoring);
            Assert.NotNull(result);
            Assert.AreEqual(ProbScoreFromDb, result.ProbabilityScore);
        }

    }
}

﻿namespace Ivh.Domain.Scoring.Services
{
    using System;
    using System.Collections.Generic;
    using HospitalData.Scoring.Entities;
    using HospitalData.Scoring.Interfaces;
    using HospitalData.Scoring.Services;
    using HospitalData.Segmentation.Interfaces;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class ScoringStatisticalMonitoringServiceTests : DomainTestBase
    {

        private Mock<IScoreRepository> _scoreRespositoryMock;
        private Mock<IBaseScoreRepository> _baseScoreRespositoryMock;
        private Mock<IScoringStatisticalMonitoringRepository> _scoringStatisticalMonitoringRepositoryMock;
        private Mock<IScoringBatchRepository> _scoringBatchRepositoryMock;
        private Mock<IThirdPartyDataRepository> _thirdPartyDataRepositoryMock;
        private ScoringStatisticalMonitoringService _scoringStatisticalMonitoringService;

        [SetUp]
        public void BeforeEachTest()
        {
            this._scoreRespositoryMock = new Mock<IScoreRepository>();
            this._baseScoreRespositoryMock = new Mock<IBaseScoreRepository>();
            this._scoringStatisticalMonitoringRepositoryMock = new Mock<IScoringStatisticalMonitoringRepository>();
            this._scoringBatchRepositoryMock = new Mock<IScoringBatchRepository>();
            this._thirdPartyDataRepositoryMock = new Mock<IThirdPartyDataRepository>();
            this._scoringStatisticalMonitoringService = new ScoringStatisticalMonitoringService(
                new Lazy<IScoreRepository>(() => this._scoreRespositoryMock.Object),
                new Lazy<IBaseScoreRepository>(() => this._baseScoreRespositoryMock.Object),
                new Lazy<IScoringStatisticalMonitoringRepository>(() => this._scoringStatisticalMonitoringRepositoryMock.Object),
                new Lazy<IScoringBatchRepository>(() => this._scoringBatchRepositoryMock.Object),
                new Lazy<IThirdPartyDataRepository>(() => this._thirdPartyDataRepositoryMock.Object));

            this._scoringBatchRepositoryMock.Setup(x => x.GetLatestScoringBatch()).Returns(this.GetLatestScoringBatch);
            this._scoringStatisticalMonitoringRepositoryMock.Setup(x => x.InsertOrUpdate(It.IsAny<ScoringStatisticalMonitoring>()));//.Callback(Action<int>);
            this._thirdPartyDataRepositoryMock.Setup(x => x.GetThirdPartyDataByDate(It.IsAny<DateTime>())).Returns(new List<ThirdPartyDataMonitoring>());
        }

        [Test]
        public void GuarantorAllBalanceTest()
        {
            List<BaseScore> scores = new List<BaseScore>
            {
                this.CreateScore(guarantorAllBalance30Days: 0, guarantorAllBalance31To60Days: 0, guarantorAllBalance61To90Days : 0, guarantorAllBalance91To180Days: 0, guarantorAllBalance181To270Days: 0, guarantorAllBalance271To365Days: 0),
                this.CreateScore(guarantorAllBalance30Days: 10, guarantorAllBalance31To60Days: 20, guarantorAllBalance61To90Days : 30, guarantorAllBalance91To180Days: 40, guarantorAllBalance181To270Days: 50, guarantorAllBalance271To365Days: 60),
                this.CreateScore(guarantorAllBalance30Days: 10, guarantorAllBalance31To60Days: 20, guarantorAllBalance61To90Days : 30, guarantorAllBalance91To180Days: 40, guarantorAllBalance181To270Days: 50, guarantorAllBalance271To365Days: 60)
            };

            this._baseScoreRespositoryMock.Setup(x => x.GetScoresByBatchId(It.IsAny<int>())).Returns(scores);
            ScoringStatisticalMonitoring result = this._scoringStatisticalMonitoringService.RunDailyScoringStatisticalMonitoring(0);

            Assert.AreEqual(result.ThirdPartyDataCount, 0);
            Assert.AreEqual(result.ScoreCount, 3);
            Assert.AreEqual(result.GuarantorAllBalance30Days0Count, 1);
            Assert.AreEqual(result.GuarantorAllBalance30DaysMedian, 10);
            Assert.AreEqual(result.GuarantorAllBalance31To60Days0Count, 1);
            Assert.AreEqual(result.GuarantorAllBalance31To60DaysMedian, 20);
            Assert.AreEqual(result.GuarantorAllBalance61To90Days0Count, 1);
            Assert.AreEqual(result.GuarantorAllBalance61To90DaysMedian, 30);
            Assert.AreEqual(result.GuarantorAllBalance91To180Days0Count, 1);
            Assert.AreEqual(result.GuarantorAllBalance91To180DaysMedian, 40);
            Assert.AreEqual(result.GuarantorAllBalance181To270Days0Count, 1);
            Assert.AreEqual(result.GuarantorAllBalance181To270DaysMedian, 50);
            Assert.AreEqual(result.GuarantorAllBalance271To365Days0Count, 1);
            Assert.AreEqual(result.GuarantorAllBalance271To365DaysMedian, 60);
        }

        [Test]
        public void GuarantorInactive120DaysBalanceTest()
        {
            List<BaseScore> scores = new List<BaseScore>
            {
                this.CreateScore(guarantorInactive120DaysBalance91To180Days: 0, guarantorInactive120DaysBalance181To270Days: 0, guarantorInactive120DaysBalance271To365Days: 0),
                this.CreateScore(guarantorInactive120DaysBalance91To180Days: 40, guarantorInactive120DaysBalance181To270Days: 50, guarantorInactive120DaysBalance271To365Days: 60),
                this.CreateScore(guarantorInactive120DaysBalance91To180Days: 40, guarantorInactive120DaysBalance181To270Days: 50, guarantorInactive120DaysBalance271To365Days: 60)
            };

            this._baseScoreRespositoryMock.Setup(x => x.GetScoresByBatchId(It.IsAny<int>())).Returns(scores);
            ScoringStatisticalMonitoring result = this._scoringStatisticalMonitoringService.RunDailyScoringStatisticalMonitoring(0);

            Assert.AreEqual(result.ScoreCount, 3);
            Assert.AreEqual(result.GuarantorInactive120DaysBalance91To180Days0Count, 1);
            Assert.AreEqual(result.GuarantorInactive120DaysBalance91To180DaysMedian, 40);
            Assert.AreEqual(result.GuarantorInactive120DaysBalance181To270Days0Count, 1);
            Assert.AreEqual(result.GuarantorInactive120DaysBalance181To270DaysMedian, 50);
            Assert.AreEqual(result.GuarantorInactive120DaysBalance271To365Days0Count, 1);
            Assert.AreEqual(result.GuarantorInactive120DaysBalance271To365DaysMedian, 60);
        }

        [Test]
        public void GuarantorPaidOffAccountsWPatientPaymentsNumTest()
        {
            List<BaseScore> scores = new List<BaseScore>
            {
                this.CreateScore(guarantorPaidOffAccountsWPatientPaymentsNum30Days: 0, guarantorPaidOffAccountsWPatientPaymentsNum31To60Days: 0, guarantorPaidOffAccountsWPatientPaymentsNum61To90Days : 0, guarantorPaidOffAccountsWPatientPaymentsNum91To180Days: 0),
                this.CreateScore(guarantorPaidOffAccountsWPatientPaymentsNum30Days: 10, guarantorPaidOffAccountsWPatientPaymentsNum31To60Days: 20, guarantorPaidOffAccountsWPatientPaymentsNum61To90Days : 30, guarantorPaidOffAccountsWPatientPaymentsNum91To180Days: 40),
                this.CreateScore(guarantorPaidOffAccountsWPatientPaymentsNum30Days: 10, guarantorPaidOffAccountsWPatientPaymentsNum31To60Days: 20, guarantorPaidOffAccountsWPatientPaymentsNum61To90Days : 30, guarantorPaidOffAccountsWPatientPaymentsNum91To180Days: 40)
            };

            this._baseScoreRespositoryMock.Setup(x => x.GetScoresByBatchId(It.IsAny<int>())).Returns(scores);
            ScoringStatisticalMonitoring result = this._scoringStatisticalMonitoringService.RunDailyScoringStatisticalMonitoring(0);

            Assert.AreEqual(result.ScoreCount, 3);
            Assert.AreEqual(result.GuarantorPaidOffAccountsWPatientPaymentsNum30Days0Count, 1);
            Assert.AreEqual(result.GuarantorPaidOffAccountsWPatientPaymentsNum30DaysMedian, 10);
            Assert.AreEqual(result.GuarantorPaidOffAccountsWPatientPaymentsNum31To60Days0Count, 1);
            Assert.AreEqual(result.GuarantorPaidOffAccountsWPatientPaymentsNum31To60DaysMedian, 20);
            Assert.AreEqual(result.GuarantorPaidOffAccountsWPatientPaymentsNum61To90Days0Count, 1);
            Assert.AreEqual(result.GuarantorPaidOffAccountsWPatientPaymentsNum61To90DaysMedian, 30);
            Assert.AreEqual(result.GuarantorPaidOffAccountsWPatientPaymentsNum91To180Days0Count, 1);
            Assert.AreEqual(result.GuarantorPaidOffAccountsWPatientPaymentsNum91To180DaysMedian, 40);
        }

        [Test]
        public void GuarantorPaidOffAccountsPatientPaymentsTest()
        {
            List<BaseScore> scores = new List<BaseScore>
            {
                this.CreateScore(guarantorPaidOffAccountsPatientPayments30Days: 0, guarantorPaidOffAccountsPatientPayments31To60Days: 0, guarantorPaidOffAccountsPatientPayments61To90Days : 0, guarantorPaidOffAccountsPatientPayments91To180Days: 0),
                this.CreateScore(guarantorPaidOffAccountsPatientPayments30Days: 10, guarantorPaidOffAccountsPatientPayments31To60Days: 20, guarantorPaidOffAccountsPatientPayments61To90Days : 30, guarantorPaidOffAccountsPatientPayments91To180Days: 40),
                this.CreateScore(guarantorPaidOffAccountsPatientPayments30Days: 10, guarantorPaidOffAccountsPatientPayments31To60Days: 20, guarantorPaidOffAccountsPatientPayments61To90Days : 30, guarantorPaidOffAccountsPatientPayments91To180Days: 40)
            };

            this._baseScoreRespositoryMock.Setup(x => x.GetScoresByBatchId(It.IsAny<int>())).Returns(scores);
            ScoringStatisticalMonitoring result = this._scoringStatisticalMonitoringService.RunDailyScoringStatisticalMonitoring(0);

            Assert.AreEqual(result.ScoreCount, 3);
            Assert.AreEqual(result.GuarantorPaidOffAccountsPatientPayments30Days0Count, 1);
            Assert.AreEqual(result.GuarantorPaidOffAccountsPatientPayments30DaysMedian, 10);
            Assert.AreEqual(result.GuarantorPaidOffAccountsPatientPayments31To60Days0Count, 1);
            Assert.AreEqual(result.GuarantorPaidOffAccountsPatientPayments31To60DaysMedian, 20);
            Assert.AreEqual(result.GuarantorPaidOffAccountsPatientPayments61To90Days0Count, 1);
            Assert.AreEqual(result.GuarantorPaidOffAccountsPatientPayments61To90DaysMedian, 30);
            Assert.AreEqual(result.GuarantorPaidOffAccountsPatientPayments91To180Days0Count, 1);
            Assert.AreEqual(result.GuarantorPaidOffAccountsPatientPayments91To180DaysMedian, 40);
        }

        [Test]
        public void GuarantorPaymentsByDischargeTest()
        {
            List<BaseScore> scores = new List<BaseScore>
            {
                this.CreateScore(guarantorPaymentsByDischarge30Days: 0, guarantorPaymentsByDischarge31To60Days: 0, guarantorPaymentsByDischarge61To90Days : 0, guarantorPaymentsByDischarge91To180Days: 0),
                this.CreateScore(guarantorPaymentsByDischarge30Days: 10, guarantorPaymentsByDischarge31To60Days: 20, guarantorPaymentsByDischarge61To90Days : 30, guarantorPaymentsByDischarge91To180Days: 40),
                this.CreateScore(guarantorPaymentsByDischarge30Days: 10, guarantorPaymentsByDischarge31To60Days: 20, guarantorPaymentsByDischarge61To90Days : 30, guarantorPaymentsByDischarge91To180Days: 40)
            };

            this._baseScoreRespositoryMock.Setup(x => x.GetScoresByBatchId(It.IsAny<int>())).Returns(scores);
            ScoringStatisticalMonitoring result = this._scoringStatisticalMonitoringService.RunDailyScoringStatisticalMonitoring(0);

            Assert.AreEqual(result.ScoreCount, 3);
            Assert.AreEqual(result.GuarantorPaymentsByDischarge30Days0Count, 1);
            Assert.AreEqual(result.GuarantorPaymentsByDischarge30DaysMedian, 10);
            Assert.AreEqual(result.GuarantorPaymentsByDischarge31To60Days0Count, 1);
            Assert.AreEqual(result.GuarantorPaymentsByDischarge31To60DaysMedian, 20);
            Assert.AreEqual(result.GuarantorPaymentsByDischarge61To90Days0Count, 1);
            Assert.AreEqual(result.GuarantorPaymentsByDischarge61To90DaysMedian, 30);
            Assert.AreEqual(result.GuarantorPaymentsByDischarge91To180Days0Count, 1);
            Assert.AreEqual(result.GuarantorPaymentsByDischarge91To180DaysMedian, 40);
        }

        [Test]
        public void NoPriorDataTest()
        {
            List<BaseScore> scores = new List<BaseScore>
            {
                this.CreateScore(noPriorData30Days: true, noPriorData31To60Days: true, noPriorData61To90Days : true, noPriorData91To180Days: true),
                this.CreateScore(noPriorData30Days: true, noPriorData31To60Days: false, noPriorData61To90Days : false, noPriorData91To180Days: true),
                this.CreateScore(noPriorData30Days: false, noPriorData31To60Days: false, noPriorData61To90Days : false, noPriorData91To180Days: true)
            };

            this._baseScoreRespositoryMock.Setup(x => x.GetScoresByBatchId(It.IsAny<int>())).Returns(scores);
            ScoringStatisticalMonitoring result = this._scoringStatisticalMonitoringService.RunDailyScoringStatisticalMonitoring(0);

            Assert.AreEqual(result.ScoreCount, 3);
            Assert.AreEqual(Math.Round(result.NoPriorData30DaysMean, 3), 0.667);
            Assert.AreEqual(Math.Round(result.NoPriorData31To60DaysMean, 3), 0.333);
            Assert.AreEqual(Math.Round(result.NoPriorData61To90DaysMean, 3), 0.333);
            Assert.AreEqual(result.NoPriorData91To180DaysMean, 1);
        }

        [Test]
        public void PatientType2Test()
        {
            List<BaseScore> scores = new List<BaseScore>
            {
                this.CreateScore(patientType2: "E"),
                this.CreateScore(patientType2: "E"),
                this.CreateScore(patientType2: "E"),
                this.CreateScore(patientType2: null)
            };

            this._baseScoreRespositoryMock.Setup(x => x.GetScoresByBatchId(It.IsAny<int>())).Returns(scores);
            ScoringStatisticalMonitoring result = this._scoringStatisticalMonitoringService.RunDailyScoringStatisticalMonitoring(0);

            Assert.AreEqual(result.PatientType2NullCount, 1);
            Assert.AreEqual(Math.Round(result.PatientType2Mean, 3), 1.5);

            scores = new List<BaseScore>
            {
                this.CreateScore(patientType2: "I"),
                this.CreateScore(patientType2: "I"),
                this.CreateScore(patientType2: "I"),
            };

            this._baseScoreRespositoryMock.Setup(x => x.GetScoresByBatchId(It.IsAny<int>())).Returns(scores);
            result = this._scoringStatisticalMonitoringService.RunDailyScoringStatisticalMonitoring(0);
            Assert.AreEqual(result.PatientType2NullCount, 0);
            Assert.AreEqual(Math.Round(result.PatientType2Mean, 3), 1);

            scores = new List<BaseScore>
            {
                this.CreateScore(patientType2: "O"),
                this.CreateScore(patientType2: "O"),
                this.CreateScore(patientType2: "O"),
            };

            this._baseScoreRespositoryMock.Setup(x => x.GetScoresByBatchId(It.IsAny<int>())).Returns(scores);
            result = this._scoringStatisticalMonitoringService.RunDailyScoringStatisticalMonitoring(0);
            Assert.AreEqual(result.PatientType2NullCount, 0);
            Assert.AreEqual(Math.Round(result.PatientType2Mean, 3), 0);

            scores = new List<BaseScore>
            {
                this.CreateScore(patientType2: "E"),
                this.CreateScore(patientType2: "O"),
                this.CreateScore(patientType2: "I"),
                this.CreateScore(patientType2: "O"),
            };

            this._baseScoreRespositoryMock.Setup(x => x.GetScoresByBatchId(It.IsAny<int>())).Returns(scores);
            result = this._scoringStatisticalMonitoringService.RunDailyScoringStatisticalMonitoring(0);
            Assert.AreEqual(result.PatientType2NullCount, 0);
            Assert.AreEqual(Math.Round(result.PatientType2Mean, 3), 0.75);

        }

        [Test]
        public void PrimaryInsuranceTypeTest()
        {
            List<BaseScore> scores = new List<BaseScore>
            {
                this.CreateScore(primaryInsuranceTypeN: "S", primaryInsuranceTypeA: "AP", primaryInsuranceType1: "S"),
                this.CreateScore(primaryInsuranceTypeN: "AP", primaryInsuranceTypeA: "AP", primaryInsuranceType1: "S"),
                this.CreateScore(primaryInsuranceTypeN: "S", primaryInsuranceTypeA: "AP", primaryInsuranceType1: "S"),
                this.CreateScore(primaryInsuranceTypeN: null, primaryInsuranceTypeA: null, primaryInsuranceType1: null)
            };

            this._baseScoreRespositoryMock.Setup(x => x.GetScoresByBatchId(It.IsAny<int>())).Returns(scores);
            ScoringStatisticalMonitoring result = this._scoringStatisticalMonitoringService.RunDailyScoringStatisticalMonitoring(0);

            Assert.AreEqual(result.PrimaryInsuranceTypeNNullCount, 1);
            Assert.AreEqual(result.PrimaryInsuranceTypeNMean, .25);
            Assert.AreEqual(result.PrimaryInsuranceTypeANullCount, 1);
            Assert.AreEqual(result.PrimaryInsuranceTypeAMean, .75);
            Assert.AreEqual(result.PrimaryInsuranceType1NullCount, 1);
            Assert.AreEqual(result.PrimaryInsuranceType1Mean, 0);
        }

        [Test]
        public void ThirdPartyData()
        {
            List<BaseScore> scores = new List<BaseScore>
            {
                this.CreateScore()
            };

            List<ThirdPartyDataMonitoring> thirdParty = new List<ThirdPartyDataMonitoring>
            {
                this.CreateThirdPartyDataMonitoring(0.2m, 3, 40000, 1),
                this.CreateThirdPartyDataMonitoring(0.3m, 4, 50000, 2),
                this.CreateThirdPartyDataMonitoring(0.4m, 5, 60000, 3),
                this.CreateThirdPartyDataMonitoring(0.5m, 6, 70000, 4),
                this.CreateThirdPartyDataMonitoring(0.6m, 8, 90000, 6),
                this.CreateThirdPartyDataMonitoring(0, null, null, null),
            };


            this._baseScoreRespositoryMock.Setup(x => x.GetScoresByBatchId(It.IsAny<int>())).Returns(scores);
            this._thirdPartyDataRepositoryMock.Setup(x => x.GetThirdPartyDataByDate(It.IsAny<DateTime>())).Returns(thirdParty);
            ScoringStatisticalMonitoring result = this._scoringStatisticalMonitoringService.RunDailyScoringStatisticalMonitoring(0);

            Assert.AreEqual(result.ThirdPartyDataCount, 6);
            Assert.AreEqual(result.ProbabilityScoreMedian, .35m);
            Assert.AreEqual(result.OwnRentNullCount, 1);
            Assert.AreEqual(Math.Round(result.OwnRentMean, 2), 4.33m);
            Assert.AreEqual(result.HouseholdIncomeNullCount, 1);
            Assert.AreEqual(Math.Round(result.HouseholdIncomeMean, 2), 51666.67m);
            Assert.AreEqual(result.HouseholdMemberCountNullCount, 1);
            Assert.AreEqual(Math.Round(result.HouseholdMemberCountMean, 2), 2.67m);
        }


        private Score CreateScore(
            decimal guarantorAllBalance30Days = 0,
            decimal guarantorAllBalance31To60Days = 0,
            decimal guarantorAllBalance61To90Days = 0,
            decimal guarantorAllBalance91To180Days = 0,
            decimal guarantorAllBalance181To270Days = 0,
            decimal guarantorAllBalance271To365Days = 0,
            decimal guarantorInactive120DaysBalance91To180Days = 0,
            decimal guarantorInactive120DaysBalance181To270Days = 0,
            decimal guarantorInactive120DaysBalance271To365Days = 0,
            int guarantorPaidOffAccountsWPatientPaymentsNum30Days = 0,
            int guarantorPaidOffAccountsWPatientPaymentsNum31To60Days = 0,
            int guarantorPaidOffAccountsWPatientPaymentsNum61To90Days = 0,
            int guarantorPaidOffAccountsWPatientPaymentsNum91To180Days = 0,
            decimal guarantorPaidOffAccountsPatientPayments30Days = 0,
            decimal guarantorPaidOffAccountsPatientPayments31To60Days = 0,
            decimal guarantorPaidOffAccountsPatientPayments61To90Days = 0,
            decimal guarantorPaidOffAccountsPatientPayments91To180Days = 0,
            decimal guarantorPaymentsByDischarge30Days = 0,
            decimal guarantorPaymentsByDischarge31To60Days = 0,
            decimal guarantorPaymentsByDischarge61To90Days = 0,
            decimal guarantorPaymentsByDischarge91To180Days = 0,
            bool noPriorData30Days = false,
            bool noPriorData31To60Days = false,
            bool noPriorData61To90Days = false,
            bool noPriorData91To180Days = false,
            string patientType2 = "E",
            string primaryInsuranceTypeN = "S",
            string primaryInsuranceTypeA = "S",
            string primaryInsuranceType1 = "S"
            )
        {
            return new Score
            {
                GuarantorAllBalance30Days = guarantorAllBalance30Days,
                GuarantorAllBalance31To60Days = guarantorAllBalance31To60Days,
                GuarantorAllBalance61To90Days = guarantorAllBalance61To90Days,
                GuarantorAllBalance91To180Days = guarantorAllBalance91To180Days,
                GuarantorAllBalance181To270Days = guarantorAllBalance181To270Days,
                GuarantorAllBalance271To365Days = guarantorAllBalance271To365Days,
                GuarantorInactive120DaysBalance91To180Days = guarantorInactive120DaysBalance91To180Days,
                GuarantorInactive120DaysBalance181To270Days = guarantorInactive120DaysBalance181To270Days,
                GuarantorInactive120DaysBalance271To365Days = guarantorInactive120DaysBalance271To365Days,
                GuarantorPaidOffAccountsWPatientPaymentsNum30Days = guarantorPaidOffAccountsWPatientPaymentsNum30Days,
                GuarantorPaidOffAccountsWPatientPaymentsNum31To60Days = guarantorPaidOffAccountsWPatientPaymentsNum31To60Days,
                GuarantorPaidOffAccountsWPatientPaymentsNum61To90Days = guarantorPaidOffAccountsWPatientPaymentsNum61To90Days,
                GuarantorPaidOffAccountsWPatientPaymentsNum91To180Days = guarantorPaidOffAccountsWPatientPaymentsNum91To180Days,
                GuarantorPaidOffAccountsPatientPayments30Days = guarantorPaidOffAccountsPatientPayments30Days,
                GuarantorPaidOffAccountsPatientPayments31To60Days = guarantorPaidOffAccountsPatientPayments31To60Days,
                GuarantorPaidOffAccountsPatientPayments61To90Days = guarantorPaidOffAccountsPatientPayments61To90Days,
                GuarantorPaidOffAccountsPatientPayments91To180Days = guarantorPaidOffAccountsPatientPayments91To180Days,
                GuarantorPaymentsByDischarge30Days = guarantorPaymentsByDischarge30Days,
                GuarantorPaymentsByDischarge31To60Days = guarantorPaymentsByDischarge31To60Days,
                GuarantorPaymentsByDischarge61To90Days = guarantorPaymentsByDischarge61To90Days,
                GuarantorPaymentsByDischarge91To180Days = guarantorPaymentsByDischarge91To180Days,
                NoPriorData30Days = noPriorData30Days,
                NoPriorData31To60Days = noPriorData31To60Days,
                NoPriorData61To90Days = noPriorData61To90Days,
                NoPriorData91To180Days = noPriorData91To180Days,
                PatientType2 = patientType2,
                PrimaryInsuranceTypeN = primaryInsuranceTypeN,
                PrimaryInsuranceTypeA = primaryInsuranceTypeA,
                PrimaryInsuranceType1 = primaryInsuranceType1,
            };
        }

        private ThirdPartyDataMonitoring CreateThirdPartyDataMonitoring(decimal? probabiltyScore, int? ownRent, decimal? householdIncome, int? householdMemberCount)
        {
            return new ThirdPartyDataMonitoring
            {
                ProbabilityScore = probabiltyScore,
                OwnRent = ownRent,
                HouseholdIncome = householdIncome,
                HouseholdMemberCount = householdMemberCount
            };
        }

        private ScoringBatch GetLatestScoringBatch()
        {
            return new ScoringBatch
            {
                ScoringBatchId = 1,
                BatchStartDateTime = new DateTime(2018, 04, 25, 09, 09, 09)
            };
        }

    }
}

﻿namespace Ivh.Domain.Payment.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using FinanceManagement.Payment.Entities;
    using FinanceManagement.Payment.Interfaces;
    using FinanceManagement.Payment.Services.PaymentAllocation;
    using Interfaces;
    using NUnit.Framework;

    /// <summary>
    /// test scenarios based on https://ivincehealth.sharepoint.com/Products/vpp/_layouts/15/WopiFrame.aspx?sourcedoc={75200273-B1A4-452C-93C8-21C06482ECEC}&file=VPNG-949%20PA_Payment%20Distribution%20Illustration.xlsx&action=default
    /// expected outcomes in the link are for VisitType distribution, NOT ProRata distribution
    /// </summary>
    [TestFixture]
    public class ProRataPaymentAllocationServiceProductProductTests : PaymentAllocationServiceProductTestsBase<ProRataPaymentAllocationService>, IPaymentAllocationServiceProductTests
    {
        [Test]
        public void OffsetAllAllocationsInPayment_AfterReturnAch()
        {
            IList<Payment> payments = this.ProductDefined_RecurringFinancePlan(false, delegate(IPaymentAllocationService service, List<Payment> internalPayments)
            {
                foreach (Payment payment in internalPayments)
                {
                    service.OffsetAllAllocationsInPayment(payment);
                }
            });

            Payment payment1 = payments.Take(1).First();
            Payment payment2 = payments.Skip(1).First();

            Assert.AreEqual(0m, payment1.ActualPaymentAmount);
            Assert.AreEqual(0m, payment2.ActualPaymentAmount);
            Assert.AreEqual(0m, payment1.PaymentAllocations.Sum(x => x.ActualAmount));
            Assert.AreEqual(0m, payment2.PaymentAllocations.Sum(x => x.ActualAmount));
        }

        #region specific amount tests

        [Test]
        // specific amount example 1
        public void ProductDefined_SpecificAmount_NoFP_1()
        {
            Payment payment = this.ProductDefined_SpecificAmount_NoFP(250);
            
            Assert.AreEqual(41.21m, this.FindAllocationByVisitId(payment, 1001));
            Assert.AreEqual(98.90m, this.FindAllocationByVisitId(payment, 1002));
            Assert.AreEqual(109.89m, this.FindAllocationByVisitId(payment, 1003));
            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1004));
            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1005));
        }

        [Test]
        // specific amount example 2
        public void ProductDefined_SpecificAmount_NoFP_2()
        {
            Payment payment = this.ProductDefined_SpecificAmount_NoFP(500);

            Assert.AreEqual(75.00m, this.FindAllocationByVisitId(payment, 1001));
            Assert.AreEqual(180.00m, this.FindAllocationByVisitId(payment, 1002));
            Assert.AreEqual(200.00m, this.FindAllocationByVisitId(payment, 1003));
            Assert.AreEqual(31.50m, this.FindAllocationByVisitId(payment, 1004));
            Assert.AreEqual(13.50m, this.FindAllocationByVisitId(payment, 1005));
        }

        [Test]
        // specific amount example 3
        public void ProductDefined_SpecificAmount_WithFP()
        {
            Payment payment = this.ProductDefined_SpecificAmount_WithFP(2000);

            Assert.AreEqual(300.00m, this.FindAllocationByVisitId(payment, 1001));
            Assert.AreEqual(300.00m, this.FindAllocationByVisitId(payment, 1002));
            Assert.AreEqual(300.00m, this.FindAllocationByVisitId(payment, 1003));
            Assert.AreEqual(50.00m, this.FindAllocationByVisitId(payment, 1004));
            Assert.AreEqual(490.00m, this.FindAllocationByVisitId(payment, 1005));
            Assert.AreEqual(420.00m, this.FindAllocationByVisitId(payment, 1006));
            Assert.AreEqual(140.00m, this.FindAllocationByVisitId(payment, 1007));
            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1008));
            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1009));
        }

        [Test]
        // specific amount example 4
        public void ProductDefined_SpecificAmount_WithFP_WithPastDues()
        {
            Payment payment = this.ProductDefined_SpecificAmount_WithFP_WithPastDues(430);

            Assert.AreEqual(30.76m, this.FindAllocationByVisitId(payment, 1001));
            Assert.AreEqual(19.24m, this.FindAllocationByVisitId(payment, 1002));
            Assert.AreEqual(43.75m, this.FindAllocationByVisitId(payment, 1003));
            Assert.AreEqual(37.50m, this.FindAllocationByVisitId(payment, 1004));
            Assert.AreEqual(18.75m, this.FindAllocationByVisitId(payment, 1005));
            Assert.AreEqual(200.00m, this.FindAllocationByVisitId(payment, 1006));
            Assert.AreEqual(80.00m, this.FindAllocationByVisitId(payment, 1007));
            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1008));
            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1009));
        }

        [Test]
        // specific amount example 5
        public void ProductDefined_SpecificAmount_WithFP_WithInterest_1()
        {
            Payment payment = this.ProductDefined_SpecificAmount_WithFP_WithInterest(175);

            Assert.AreEqual(150.00m, this.FindAllocationByVisitId(payment, 1001));
            
            Assert.AreEqual(12.75m, this.FindAllocationByVisitId(payment, 1002));
            Assert.AreEqual(0.39m, this.FindAllocationByVisitId(payment, 1002, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(10.63m, this.FindAllocationByVisitId(payment, 1003));
            Assert.AreEqual(0.32m, this.FindAllocationByVisitId(payment, 1003, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1004));
            Assert.AreEqual(0.43m, this.FindAllocationByVisitId(payment, 1004, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1005));
            Assert.AreEqual(0.36m, this.FindAllocationByVisitId(payment, 1005, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1006));
            Assert.AreEqual(0.12m, this.FindAllocationByVisitId(payment, 1006, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1007));
            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1008));
        }

        [TestCase(225.57, 0, 225.57)]
        [TestCase(325.57, 0, 325.57)]
        [TestCase(325.57, 100, 325.57)]
        [TestCase(425.57, 100, 325.57)]
        [TestCase(325.57, -100, 225.57)]
        [TestCase(425.57, -100, 225.57)]
        // specific finance plan example 4
        public void ProductDefined_SpecificFinancePlan_WithInterest_WithPastDue_Adjustments(decimal paymentAmount, decimal adjustment, decimal expectedTotal)
        {
            //Total 325.57
            //Interest 2.16
            //prin 323.41
            Payment payment = this.ProductDefined_SpecificFinancePlan_WithInterest_WithPastDue(paymentAmount, adjustment);

            Assert.AreEqual(expectedTotal, payment.ActualPaymentAmount);
        }

        [Test]
        // specific amount example 6
        public void ProductDefined_SpecificAmount_WithFP_WithInterest_2()
        {
            Payment payment = this.ProductDefined_SpecificAmount_WithFP_WithInterest(400);

            Assert.AreEqual(150.00m, this.FindAllocationByVisitId(payment, 1001));

            Assert.AreEqual(115.81m, this.FindAllocationByVisitId(payment, 1002));
            Assert.AreEqual(0.39m, this.FindAllocationByVisitId(payment, 1002, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(96.49m, this.FindAllocationByVisitId(payment, 1003));
            Assert.AreEqual(0.32m, this.FindAllocationByVisitId(payment, 1003, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(16.84m, this.FindAllocationByVisitId(payment, 1004));
            Assert.AreEqual(0.43m, this.FindAllocationByVisitId(payment, 1004, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(14.43m, this.FindAllocationByVisitId(payment, 1005));
            Assert.AreEqual(0.36m, this.FindAllocationByVisitId(payment, 1005, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(4.81m, this.FindAllocationByVisitId(payment, 1006));
            Assert.AreEqual(0.12m, this.FindAllocationByVisitId(payment, 1006, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1007));
            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1008));
        }

        [Test]
        // specific amount example 7
        public void ProductDefined_SpecificAmount_WithFP_WithInterest_3()
        {
            Payment payment = this.ProductDefined_SpecificAmount_WithFP_WithInterest(151);

            Assert.AreEqual(150.00m, this.FindAllocationByVisitId(payment, 1001));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1002));
            Assert.AreEqual(0.39m, this.FindAllocationByVisitId(payment, 1002, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1003));
            Assert.AreEqual(0.32m, this.FindAllocationByVisitId(payment, 1003, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1004));
            Assert.AreEqual(0.14m, this.FindAllocationByVisitId(payment, 1004, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1005));
            Assert.AreEqual(0.11m, this.FindAllocationByVisitId(payment, 1005, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1006));
            Assert.AreEqual(0.04m, this.FindAllocationByVisitId(payment, 1006, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1007));
            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1008));
        }

        [Test]
        // specific amount example 8
        public void ProductDefined_SpecificAmount_WithFP_WithInterest_WithPastDue()
        {
            Payment payment = this.ProductDefined_SpecificAmount_WithFP_WithInterest_WithPastDue(100);

            Assert.AreEqual(37.32m, this.FindAllocationByVisitId(payment, 1001));
            Assert.AreEqual(1.54m, this.FindAllocationByVisitId(payment, 1001, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(29.86m, this.FindAllocationByVisitId(payment, 1002));
            Assert.AreEqual(1.28m, this.FindAllocationByVisitId(payment, 1002, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(12.99m, this.FindAllocationByVisitId(payment, 1003));
            Assert.AreEqual(1.01m, this.FindAllocationByVisitId(payment, 1003, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(11.14m, this.FindAllocationByVisitId(payment, 1004));
            Assert.AreEqual(0.86m, this.FindAllocationByVisitId(payment, 1004, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(03.71m, this.FindAllocationByVisitId(payment, 1005));
            Assert.AreEqual(0.29m, this.FindAllocationByVisitId(payment, 1005, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1006));
            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1007));
            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1008));
        }

        #endregion

        #region recurring finance plans

        [Test]
        // recurring finance plans example 1
        public void ProductDefined_RecurringFinancePlan()
        {
            IList<Payment> payments = this.ProductDefined_RecurringFinancePlan(false);

            Payment payment1 = payments.Take(1).First();
            Payment payment2 = payments.Skip(1).First();

            Assert.AreEqual(35.21m, this.FindAllocationByVisitId(payment1, 1001));
            Assert.AreEqual(56.34m, this.FindAllocationByVisitId(payment1, 1002));
            Assert.AreEqual(8.45m, this.FindAllocationByVisitId(payment1, 1003));

            Assert.AreEqual(4.55m, this.FindAllocationByVisitId(payment2, 1004));
            Assert.AreEqual(5.68m, this.FindAllocationByVisitId(payment2, 1005));
            Assert.AreEqual(39.77m, this.FindAllocationByVisitId(payment2, 1006));
        }




        [Test]
        // recurring finance plans example 2
        public void ProductDefined_RecurringFinancePlan_WithPastDue()
        {
            List<Payment> payments = this.ProductDefined_RecurringFinancePlan(true);

            Payment payment1 = payments.Take(1).First();
            Payment payment2 = payments.Skip(1).First();

            Assert.AreEqual(70.42m, this.FindAllocationByVisitId(payment1, 1001));
            Assert.AreEqual(112.68m, this.FindAllocationByVisitId(payment1, 1002));
            Assert.AreEqual(16.90m, this.FindAllocationByVisitId(payment1, 1003));

            Assert.AreEqual(9.10m, this.FindAllocationByVisitId(payment2, 1004));
            Assert.AreEqual(11.36m, this.FindAllocationByVisitId(payment2, 1005));
            Assert.AreEqual(79.54m, this.FindAllocationByVisitId(payment2, 1006));
        }

        [Test]
        // recurring finance plans example 3
        public void ProductDefined_RecurringFinancePlan_WithInterest()
        {
            List<Payment> payments = this.ProductDefined_RecurringFinancePlan_WithInterest_Base();

            Payment payment1 = payments.Take(1).First();
            Payment payment2 = payments.Skip(1).First();

            Assert.AreEqual(18.71m, this.FindAllocationByVisitId(payment1, 1001));
            Assert.AreEqual(0.39m, this.FindAllocationByVisitId(payment1, 1001, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(15.58m, this.FindAllocationByVisitId(payment1, 1002));
            Assert.AreEqual(0.32m, this.FindAllocationByVisitId(payment1, 1002, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(22.91m, this.FindAllocationByVisitId(payment2, 1003));
            Assert.AreEqual(0.43m, this.FindAllocationByVisitId(payment2, 1003, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(19.64m, this.FindAllocationByVisitId(payment2, 1004));
            Assert.AreEqual(0.36m, this.FindAllocationByVisitId(payment2, 1004, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(6.54m, this.FindAllocationByVisitId(payment2, 1005));
            Assert.AreEqual(0.12m, this.FindAllocationByVisitId(payment2, 1005, PaymentAllocationTypeEnum.VisitInterest));
        }

        [Test]
        // recurring finance plans example 4
        public void ProductDefined_RecurringFinancePlan_WithInterest_WithPastDue()
        {
            List<Payment> payments = this.ProductDefined_RecurringFinancePlan_WithInterest_WithPastDue_Base();

            Payment payment1 = payments.Take(1).First();
            Payment payment2 = payments.Skip(1).First();

            Assert.AreEqual(56.78m, this.FindAllocationByVisitId(payment1, 1001));
            Assert.AreEqual(1.54m, this.FindAllocationByVisitId(payment1, 1001, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(45.40m, this.FindAllocationByVisitId(payment1, 1002));
            Assert.AreEqual(1.28m, this.FindAllocationByVisitId(payment1, 1002, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(45.66m, this.FindAllocationByVisitId(payment2, 1003));
            Assert.AreEqual(1.01m, this.FindAllocationByVisitId(payment2, 1003, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(39.14m, this.FindAllocationByVisitId(payment2, 1004));
            Assert.AreEqual(0.86m, this.FindAllocationByVisitId(payment2, 1004, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(13.04m, this.FindAllocationByVisitId(payment2, 1005));
            Assert.AreEqual(0.29m, this.FindAllocationByVisitId(payment2, 1005, PaymentAllocationTypeEnum.VisitInterest));
        }

        #endregion

        #region specific finance plan tests

        [Test]
        // specific finance plan example 1
        public void ProductDefined_SpecificFinancePlan_1()
        {
            Payment payment = this.ProductDefined_SpecificFinancePlan(50);

            Assert.AreEqual(25.00m, this.FindAllocationByVisitId(payment, 1001));
            Assert.AreEqual(21.43m, this.FindAllocationByVisitId(payment, 1002));
            Assert.AreEqual(3.57m, this.FindAllocationByVisitId(payment, 1003));
        }

        [Test]
        // specific finance plan example 2
        public void ProductDefined_SpecificFinancePlan_2()
        {
            Payment payment = this.ProductDefined_SpecificFinancePlan(300);

            Assert.AreEqual(150m, this.FindAllocationByVisitId(payment, 1001));
            Assert.AreEqual(128.57m, this.FindAllocationByVisitId(payment, 1002));
            Assert.AreEqual(21.43m, this.FindAllocationByVisitId(payment, 1003));
        }

        [Test]
        // specific finance plan example 3
        public void ProductDefined_SpecificFinancePlan_WithPastDue()
        {
            Payment payment = this.ProductDefined_SpecificFinancePlan(20, true);

            Assert.AreEqual(10.00m, this.FindAllocationByVisitId(payment, 1001));
            Assert.AreEqual(8.57m, this.FindAllocationByVisitId(payment, 1002));
            Assert.AreEqual(1.43m, this.FindAllocationByVisitId(payment, 1003));
        }

        [Test]
        // specific finance plan example 4
        public void ProductDefined_SpecificFinancePlan_WithInterest_WithPastDue_1()
        {
            Payment payment = this.ProductDefined_SpecificFinancePlan_WithInterest_WithPastDue(30);

            Assert.AreEqual(12.99m, this.FindAllocationByVisitId(payment, 1001));
            Assert.AreEqual(1.01m, this.FindAllocationByVisitId(payment, 1001, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(11.14m, this.FindAllocationByVisitId(payment, 1002));
            Assert.AreEqual(0.86m, this.FindAllocationByVisitId(payment, 1002, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(3.71m, this.FindAllocationByVisitId(payment, 1003));
            Assert.AreEqual(0.29m, this.FindAllocationByVisitId(payment, 1003, PaymentAllocationTypeEnum.VisitInterest));
        }

        [Test]
        // specific finance plan example 5
        public void ProductDefined_SpecificFinancePlan_WithInterest_WithPastDue_2()
        {
            Payment payment = this.ProductDefined_SpecificFinancePlan_WithInterest_WithPastDue(2);

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1001));
            Assert.AreEqual(0.94m, this.FindAllocationByVisitId(payment, 1001, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1002));
            Assert.AreEqual(0.80m, this.FindAllocationByVisitId(payment, 1002, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1003));
            Assert.AreEqual(0.26m, this.FindAllocationByVisitId(payment, 1003, PaymentAllocationTypeEnum.VisitInterest));
        }

        #endregion

        #region specific visit tests

        [Test]
        // specific visit example 1
        public void ProductDefined_SpecificVisit()
        {
            Payment payment = this.ProductDefined_SpecificVisit(50);

            Assert.AreEqual(50.00m, this.FindAllocationByVisitId(payment, 1001));
        }

        [Test]
        // specific visit example 2
        public void ProductDefined_SpecificVisit_WithFP()
        {
            Payment payment = this.ProductDefined_SpecificVisit(50, true);

            Assert.AreEqual(50.00m, this.FindAllocationByVisitId(payment, 1001));
        }

        [Test]
        // specific visit example 3
        public void ProductDefined_SpecificVisit_WithFP_WithPastDue()
        {
            Payment payment = this.ProductDefined_SpecificVisit(50, true, true);

            Assert.AreEqual(50.00m, this.FindAllocationByVisitId(payment, 1001));
        }

        [Test]
        // specific visit example 4
        public void ProductDefined_SpecificVisit_WithFP_WithInterest()
        {
            Payment payment = this.ProductDefined_SpecificVisit_WithFP_WithInterest(50);

            Assert.AreEqual(49.57m, this.FindAllocationByVisitId(payment, 1001));
            Assert.AreEqual(0.43m, this.FindAllocationByVisitId(payment, 1001, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1002));
            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1002, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1003));
            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1003, PaymentAllocationTypeEnum.VisitInterest));
        }

        [Test]
        // specific visit example 5
        public void ProductDefined_SpecificVisit_WithFP_WithInterest_WithPastDue()
        {
            Payment payment = this.ProductDefined_SpecificVisit_WithFP_WithInterest_WithPastDue(50);

            Assert.AreEqual(48.99m, this.FindAllocationByVisitId(payment, 1001));
            Assert.AreEqual(1.01m, this.FindAllocationByVisitId(payment, 1001, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1002));
            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1002, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1003));
            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1003, PaymentAllocationTypeEnum.VisitInterest));
        }

        #endregion
    }
}
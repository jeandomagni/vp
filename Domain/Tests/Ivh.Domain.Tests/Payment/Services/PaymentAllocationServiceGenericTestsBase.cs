﻿namespace Ivh.Domain.Payment.Services
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Common.Tests.Helpers.Payment;
    using Common.Base.Utilities.Extensions;
    using Common.Tests.Helpers.FinancePlan;
    using Common.Tests.Helpers.Guarantor;
    using Common.Tests.Helpers.Visit;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using FinanceManagement.Discount.Entities;
    using FinanceManagement.FinancePlan.Entities;
    using FinanceManagement.Payment.Entities;
    using FinanceManagement.Payment.Interfaces;
    using Guarantor.Entities;
    using NUnit.Framework;
    using Settings.Entities;
    using Visit.Entities;

    public abstract class PaymentAllocationServiceGenericTestsBase<T> : PaymentAllocationServiceTestsBase<T> where T : IPaymentAllocationService
    {
        #region BIF

        [Test]
        [TestCase(ClientSettingDefault)]
        [TestCase(ClientSettingDischargeDate)]
        [TestCase(ClientSettingPaymentPriority)]
        [TestCase(ClientSettingPaymentPriorityDischargeDate)]
        public virtual void BIF_MustHaveVisits_CreatesNoAllocationsWhenThereIsNoVisits(ClientSettingEnum settingsType)
        {
            Dictionary<string, string> clientSettings = GetClientSettings(settingsType);

            List<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>();

            Payment payment = PaymentFactory.CreatePayment(PaymentTypeEnum.ManualScheduledCurrentBalanceInFull);

            IPaymentAllocationService svc = this.GetPaymentAllocationService(visits, null, null, clientSettings);
            PaymentAllocationResult result = svc.AllocatePayment(payment);
            result.CommitAllocationsToPayment();

            Assert.AreEqual(0, payment.PaymentAllocations.Count);
            Assert.AreEqual(0m, payment.ActualPaymentAmount);
        }

        [Test]
        [TestCase(ClientSettingDefault)]
        [TestCase(ClientSettingDischargeDate)]
        [TestCase(ClientSettingPaymentPriority)]
        [TestCase(ClientSettingPaymentPriorityDischargeDate)]
        public virtual void BIF_AttemptsToPayAllVisits_DoesntSetActualHigherThanScheduled(ClientSettingEnum settingsType)
        {
            Dictionary<string, string> clientSettings = GetClientSettings(settingsType);

            List<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>
            {
                this.CreateVisit(12.34m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(12.34m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(133.33m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(133.33m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(1000m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(1000.01m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1)
            };

            decimal sumOfVisits = visits.Sum(x => x.Item1.CurrentBalance);

            Payment payment = PaymentFactory.CreatePayment(PaymentTypeEnum.ManualScheduledCurrentBalanceInFull);
            payment.PaymentScheduledAmounts.Add(new PaymentScheduledAmount
            {
                PaymentFinancePlan = null,
                PaymentVisit = null,
                ScheduledAmount = sumOfVisits - 10m
            });

            IPaymentAllocationService svc = this.GetPaymentAllocationService(visits, null, null, clientSettings);
            PaymentAllocationResult result = svc.AllocatePayment(payment);
            result.CommitAllocationsToPayment();

            Assert.AreNotEqual(sumOfVisits, payment.ScheduledPaymentAmount);
            Assert.AreEqual(payment.ScheduledPaymentAmount, payment.ActualPaymentAmount);
        }

        [Test]
        [TestCase(ClientSettingDefault)]
        [TestCase(ClientSettingDischargeDate)]
        [TestCase(ClientSettingPaymentPriority)]
        [TestCase(ClientSettingPaymentPriorityDischargeDate)]
        public virtual void BIF_AttemptsToPayAllVisits_DoesntPayMoreThanCurrentBalanceOnVisits(ClientSettingEnum settingsType)
        {
            Dictionary<string, string> clientSettings = GetClientSettings(settingsType);

            List<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>
            {
                this.CreateVisit(12.34m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(12.34m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(133.33m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(133.33m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(1000m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(1000.01m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1)
            };

            decimal sumOfVisits = visits.Sum(x => x.Item1.CurrentBalance);

            Payment payment = PaymentFactory.CreatePayment(PaymentTypeEnum.ManualScheduledCurrentBalanceInFull);
            payment.PaymentScheduledAmounts.Add(new PaymentScheduledAmount
            {
                PaymentFinancePlan = null,
                PaymentVisit = null,
                ScheduledAmount = sumOfVisits + 10m
            });

            IPaymentAllocationService svc = this.GetPaymentAllocationService(visits, null, null, clientSettings);
            PaymentAllocationResult result = svc.AllocatePayment(payment);
            result.CommitAllocationsToPayment();

            Assert.AreNotEqual(sumOfVisits, payment.ScheduledPaymentAmount);
            Assert.AreEqual(sumOfVisits, payment.ActualPaymentAmount);
        }

        [Test]
        [TestCase(ClientSettingDefault)]
        [TestCase(ClientSettingDischargeDate)]
        [TestCase(ClientSettingPaymentPriority)]
        [TestCase(ClientSettingPaymentPriorityDischargeDate)]
        public virtual void BIF_AttemptsToPayPastDueVisitsFirst(ClientSettingEnum settingsType)
        {
            Dictionary<string, string> clientSettings = GetClientSettings(settingsType);

            List<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>
            {
                this.CreateVisit(12.34m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(12.34m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(133.33m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(133.33m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(1000m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(1000.01m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),

                //this.CreateVisit(12.34m, 1, VisitStateEnum.PastDue, null, BillingApplicationConstants.HB, null, 2),
                //this.CreateVisit(12.34m, 1, VisitStateEnum.PastDue, null, BillingApplicationConstants.HB, null, 2),
                //this.CreateVisit(133.33m, 1, VisitStateEnum.PastDue, null, BillingApplicationConstants.HB, null, 2),
                //this.CreateVisit(133.33m, 1, VisitStateEnum.PastDue, null, BillingApplicationConstants.HB, null, 2),
                //this.CreateVisit(1000m, 1, VisitStateEnum.PastDue, null, BillingApplicationConstants.HB, null, 2),
                //this.CreateVisit(1000.01m, 1, VisitStateEnum.PastDue, null, BillingApplicationConstants.HB, null, 2)
            };

            decimal sumOfPastDueVisits = visits.Where(x => x.Item1.AgingCount > 1).Sum(x => x.Item1.CurrentBalance);

            Payment payment = PaymentFactory.CreatePayment(PaymentTypeEnum.ManualScheduledCurrentBalanceInFull);
            payment.PaymentScheduledAmounts.Add(new PaymentScheduledAmount
            {
                PaymentFinancePlan = null,
                PaymentVisit = null,
                ScheduledAmount = sumOfPastDueVisits
            });

            IPaymentAllocationService svc = this.GetPaymentAllocationService(visits, null, null, clientSettings);
            PaymentAllocationResult result = svc.AllocatePayment(payment);
            result.CommitAllocationsToPayment();

            Assert.AreEqual(sumOfPastDueVisits, payment.ActualPaymentAmount);

            foreach (PaymentAllocation paymentAllocation in payment.PaymentAllocations)
            {
                Assert.IsTrue(paymentAllocation.PaymentVisit.AgingCount > 1);
            }
        }

        #endregion

        #region StatementedBIF

        /*[Test]
        public virtual void StatementBIF_MustHaveVisits_CreatesNoAllocationsWhenThereIsNoVisits()
        {
            Payment payment = PaymentFactory.CreatePayment(PaymentTypeEnum.ManualScheduledStatementBalanceInFull);

            IPaymentAllocationService svc = this.GetPaymentAllocationService();
            PaymentAllocationResult result = svc.AllocatePayment(payment);
            result.CommitAllocationsToPayment();

            Assert.AreEqual(0, payment.PaymentAllocations.Count);
            Assert.AreEqual(0m, payment.ActualPaymentAmount);
        }*/

        /*[Test]
        public virtual void StatementBIF_AttemptsToPayAllVisits_DoesntSetActualHigherThanScheduled()
        {
            List<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>
            {
                this.CreateVisit(12.34m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(12.34m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(133.33m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(133.33m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(1000m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(1000.01m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1)
            };

            decimal sumOfVisits = visits.Sum(x => x.CurrentBalance);

            Payment payment = PaymentFactory.CreatePayment(PaymentTypeEnum.ManualScheduledStatementBalanceInFull);
            payment.PaymentScheduledAmounts.Add(new PaymentScheduledAmount
            {
                PaymentFinancePlan = null,
                PaymentVisit = null,
                ScheduledAmount = sumOfVisits - 10m
            });

            IPaymentAllocationService svc = this.GetPaymentAllocationService(visits);
            PaymentAllocationResult result = svc.AllocatePayment(payment);
            result.CommitAllocationsToPayment();

            Assert.AreNotEqual(sumOfVisits, payment.ScheduledPaymentAmount);
            Assert.AreEqual(payment.ScheduledPaymentAmount, payment.ActualPaymentAmount);
        }*/

        // todo: refactor this one - seems applicable
        /*[Test]
        public virtual void StatementBIF_AttemptsToPayAllVisits_DoesntPayMoreThanCurrentBalanceOnVisits()
        {
            List<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>
            {
                this.CreateVisit(12.34m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(12.34m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(133.33m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(133.33m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(1000m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(1000.01m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1)
            };

            decimal sumOfVisits = visits.Sum(x => x.CurrentBalance);

            Payment payment = PaymentFactory.CreatePayment(PaymentTypeEnum.ManualScheduledStatementBalanceInFull);
            payment.PaymentScheduledAmounts.Add(new PaymentScheduledAmount
            {
                PaymentFinancePlan = null,
                PaymentVisit = null,
                ScheduledAmount = sumOfVisits + 10m
            });

            IPaymentAllocationService svc = this.GetPaymentAllocationService(visits);
            PaymentAllocationResult result = svc.AllocatePayment(payment);
            result.CommitAllocationsToPayment();

            Assert.AreNotEqual(sumOfVisits, payment.ScheduledPaymentAmount);
            Assert.AreEqual(sumOfVisits, payment.ActualPaymentAmount);
        }*/

        // todo: refactor this one - seems applicable
        /*[Ignore(ObsoleteDescription.VisitStateMachineTest)]
        [Test]
        public virtual void StatementBIF_AttemptsToPayPastDueVisitsFirst()
        {
            List<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>
            {
                this.CreateVisit(12.34m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(12.34m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(133.33m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(133.33m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(1000m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(1000.01m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                
                //this.CreateVisit(12.34m, 1, VisitStateEnum.PastDue, null, BillingApplicationConstants.HB, null, 2),
                //this.CreateVisit(12.34m, 1, VisitStateEnum.PastDue, null, BillingApplicationConstants.HB, null, 2),
                //this.CreateVisit(133.33m, 1, VisitStateEnum.PastDue, null, BillingApplicationConstants.HB, null, 2),
                //this.CreateVisit(133.33m, 1, VisitStateEnum.PastDue, null, BillingApplicationConstants.HB, null, 2),
                //this.CreateVisit(1000m, 1, VisitStateEnum.PastDue, null, BillingApplicationConstants.HB, null, 2),
                //this.CreateVisit(1000.01m, 1, VisitStateEnum.PastDue, null, BillingApplicationConstants.HB, null, 2)
            };
            
            decimal sumOfPastDueVisits = visits.Where(x => x.AgingTier > 1).Sum(x => x.CurrentBalance);

            Payment payment = PaymentFactory.CreatePayment(PaymentTypeEnum.ManualScheduledStatementBalanceInFull);
            payment.PaymentScheduledAmounts.Add(new PaymentScheduledAmount
            {
                PaymentFinancePlan = null,
                PaymentVisit = null,
                ScheduledAmount = sumOfPastDueVisits
            });

            IPaymentAllocationService svc = this.GetPaymentAllocationService(visits);
            PaymentAllocationResult result = svc.AllocatePayment(payment);
            result.CommitAllocationsToPayment();

            Assert.AreEqual(sumOfPastDueVisits, payment.ActualPaymentAmount);

            foreach (PaymentAllocation paymentAllocation in payment.PaymentAllocations)
            {
                Assert.IsTrue(paymentAllocation.PaymentVisit.AgingTier > 1);
            }
        }*/

        #endregion

        #region SpecificVisits

        [Test]
        [TestCase(ClientSettingDefault)]
        [TestCase(ClientSettingDischargeDate)]
        [TestCase(ClientSettingPaymentPriority)]
        [TestCase(ClientSettingPaymentPriorityDischargeDate)]
        public virtual void SpecificVisits_MustHaveVisits_DoesntCreateAllocationsWhenThereAreNoVisits(ClientSettingEnum settingsType)
        {
            Dictionary<string, string> clientSettings = GetClientSettings(settingsType);

            Payment payment = PaymentFactory.CreatePayment(PaymentTypeEnum.ManualScheduledSpecificFinancePlans, specificAmount: 10m);

            IPaymentAllocationService svc = this.GetPaymentAllocationService(null, null, null, clientSettings);
            PaymentAllocationResult result = svc.AllocatePayment(payment);
            result.CommitAllocationsToPayment();

            Assert.AreEqual(0, payment.PaymentAllocations.Count);
            Assert.AreEqual(0m, payment.ActualPaymentAmount);
        }

        [Test]
        [TestCase(ClientSettingDefault)]
        [TestCase(ClientSettingDischargeDate)]
        [TestCase(ClientSettingPaymentPriority)]
        [TestCase(ClientSettingPaymentPriorityDischargeDate)]
        public virtual void SpecificVisits_DoesntChargeFullCurrentBalanceIfLowerSupplied(ClientSettingEnum settingsType)
        {
            Dictionary<string, string> clientSettings = GetClientSettings(settingsType);

            List<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>
            {
                this.CreateVisit(12.34m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(12.34m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(133.33m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(133.33m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(1000m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(1000.01m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
            };

            IList<Visit> visits1 = visits.Select(x => x.Item1).ToList();
            decimal sumOfVisits = visits1.Sum(x => x.CurrentBalance);

            IDictionary<Visit, decimal> visitsWithAmounts = visits1.ToDictionary(x => x, x => x.CurrentBalance);
            visitsWithAmounts[visits1[5]] = Math.Round(visits1[5].CurrentBalance / 2, 2);

            Payment payment = PaymentFactory.CreatePayment(PaymentTypeEnum.ManualScheduledSpecificVisits, visits.ToDictionary(x => x, x => Math.Round(x.Item1.CurrentBalance / 2, 2)));

            IPaymentAllocationService svc = this.GetPaymentAllocationService(visits, null, null, clientSettings);
            PaymentAllocationResult result = svc.AllocatePayment(payment);
            result.CommitAllocationsToPayment();

            Assert.AreNotEqual(sumOfVisits, payment.ScheduledPaymentAmount);
            Assert.AreEqual(payment.ScheduledPaymentAmount, payment.ActualPaymentAmount);
        }

        [Test]
        [TestCase(ClientSettingDefault)]
        [TestCase(ClientSettingDischargeDate)]
        [TestCase(ClientSettingPaymentPriority)]
        [TestCase(ClientSettingPaymentPriorityDischargeDate)]
        public virtual void SpecificVisits_DoesntChargeMoreThanCurrentBalanceOfVisit(ClientSettingEnum settingsType)
        {
            Dictionary<string, string> clientSettings = GetClientSettings(settingsType);

            List<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>
            {
                this.CreateVisit(12.34m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(12.34m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(133.33m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(133.33m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(1000m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(1000.01m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
            };

            decimal sumOfVisits = visits.Sum(x => x.Item1.CurrentBalance);

            Dictionary<Tuple<Visit, IList<VisitTransaction>>, decimal> visitsWithAmounts = visits.ToDictionary(x => x, x => x.Item1.CurrentBalance);
            visitsWithAmounts[visitsWithAmounts.Last().Key] = Math.Round(visitsWithAmounts.Last().Key.Item1.CurrentBalance * 2, 2);

            Payment payment = PaymentFactory.CreatePayment(PaymentTypeEnum.ManualScheduledSpecificVisits, visitsWithAmounts);

            IPaymentAllocationService svc = this.GetPaymentAllocationService(visits, null, null, clientSettings);

            PaymentAllocationResult result = svc.AllocatePayment(payment);
            result.CommitAllocationsToPayment();

            Assert.AreNotEqual(sumOfVisits, payment.ScheduledPaymentAmount);
            Assert.AreEqual(sumOfVisits, payment.ActualPaymentAmount);
        }

        #endregion

        #region PaymentBuckets/FinancePlans

        [Test]
        [TestCase(ClientSettingDefault)]
        [TestCase(ClientSettingDischargeDate)]
        [TestCase(ClientSettingPaymentPriority)]
        [TestCase(ClientSettingPaymentPriorityDischargeDate)]
        public virtual void FinancePlans_SingleFinancePlan_PaySpecificFinancePlan(ClientSettingEnum settingsType)
        {
            Dictionary<string, string> clientSettings = GetClientSettings(settingsType);

            Guarantor guarantor = GuarantorFactory.GenerateOnline();

            List<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>
            {
                VisitFactory.GenerateActiveVisit(12.34m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
                VisitFactory.GenerateActiveVisit(12.34m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
                VisitFactory.GenerateActiveVisit(133.32m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
                VisitFactory.GenerateActiveVisit(133.32m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
                VisitFactory.GenerateActiveVisit(1000m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
                VisitFactory.GenerateActiveVisit(1000.01m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
            };
            FinancePlan fp1 = FinancePlanFactory.CreateFinancePlan(visits, 3, 55.55m, DateTime.UtcNow.AddMonths(-1));

            IDictionary<FinancePlan, decimal> financePlansWithAmounts = ToDictionaryWithAmount(fp1);

            Payment payment = PaymentFactory.CreatePayment(PaymentTypeEnum.ManualScheduledSpecificFinancePlans, financePlansWithAmounts: financePlansWithAmounts);
            payment.PaymentScheduledAmounts[0].ScheduledAmount = 10;
            payment.PaymentScheduledAmounts[0].ScheduledPrincipal = 10;
            payment.PaymentScheduledAmounts[0].ScheduledInterest = 10;

            IPaymentAllocationService svc = this.GetPaymentAllocationService(visits, fp1.ToListOfOne(), null, clientSettings);

            PaymentAllocationResult result = svc.AllocatePayment(payment);
            result.CommitAllocationsToPayment();

            Assert.AreEqual(10m, payment.ActualPaymentAmount);
            Assert.AreEqual(6m, payment.PaymentAllocations.Count);
        }

        [Test]
        [TestCase(ClientSettingDefault)]
        [TestCase(ClientSettingDischargeDate)]
        [TestCase(ClientSettingPaymentPriority)]
        [TestCase(ClientSettingPaymentPriorityDischargeDate)]
        public virtual void FinancePlans_MultipleFinancePlans_WithSameBuckets(ClientSettingEnum settingsType)
        {
            Dictionary<string, string> clientSettings = GetClientSettings(settingsType);

            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(133.32m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
                VisitFactory.GenerateActiveVisit(1000m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
                VisitFactory.GenerateActiveVisit(1000.01m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB)
            };
            FinancePlan fp1 = FinancePlanFactory.CreateFinancePlan(visits, 3, 55.55m, DateTime.UtcNow.AddMonths(-1));

            IList<Tuple<Visit, IList<VisitTransaction>>> visits2 = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(12.34m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
                VisitFactory.GenerateActiveVisit(12.35m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
                VisitFactory.GenerateActiveVisit(133.34m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB)
            };
            FinancePlan fp2 = FinancePlanFactory.CreateFinancePlan(visits2, 3, 55.55m, DateTime.UtcNow.AddMonths(-1));

            Payment payment = PaymentFactory.CreatePayment(PaymentTypeEnum.ManualScheduledSpecificFinancePlans);
            payment.PaymentScheduledAmounts.Add(new PaymentScheduledAmount
            {
                ScheduledAmount = 10m,
                ScheduledPrincipal = 10m,
                ScheduledInterest = 0,
                PaymentFinancePlan = new PaymentFinancePlan { FinancePlanId = fp1.FinancePlanId, CurrentBucket = fp1.CurrentBucket }
            });
            payment.PaymentScheduledAmounts.Add(new PaymentScheduledAmount
            {
                ScheduledAmount = 10m,
                ScheduledPrincipal = 10m,
                ScheduledInterest = 0,
                PaymentFinancePlan = new PaymentFinancePlan { FinancePlanId = fp2.FinancePlanId, CurrentBucket = fp2.CurrentBucket }
            });
            visits.AddRange(visits2);
            IPaymentAllocationService svc = this.GetPaymentAllocationService(visits, new List<FinancePlan> { fp1, fp2 }, null, clientSettings);

            PaymentAllocationResult result = svc.AllocatePayment(payment);
            result.CommitAllocationsToPayment();

            Assert.AreEqual(payment.ActualPaymentAmount, 20m);
            Assert.AreEqual(payment.PaymentAllocations.Count, 6);
        }

        [Test]
        [TestCase(ClientSettingDefault)]
        [TestCase(ClientSettingDischargeDate)]
        [TestCase(ClientSettingPaymentPriority)]
        [TestCase(ClientSettingPaymentPriorityDischargeDate)]
        public virtual void SpecificPayment_MultipleFinancePlans_WithSameBuckets(ClientSettingEnum settingsType)
        {
            Dictionary<string, string> clientSettings = GetClientSettings(settingsType);

            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(133.32m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
                VisitFactory.GenerateActiveVisit(1000m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
                VisitFactory.GenerateActiveVisit(1000.01m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB)
            };
            FinancePlan fp1 = FinancePlanFactory.CreateFinancePlan(visits, 3, 55.55m, DateTime.UtcNow.AddMonths(-1));

            IList<Tuple<Visit, IList<VisitTransaction>>> visits2 = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(12.34m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
                VisitFactory.GenerateActiveVisit(12.35m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
                VisitFactory.GenerateActiveVisit(133.34m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB)
            };
            FinancePlan fp2 = FinancePlanFactory.CreateFinancePlan(visits2, 3, 55.55m, DateTime.UtcNow.AddMonths(-1));

            List<FinancePlan> financePlans = new List<FinancePlan> { fp1, fp2 };

            Payment payment = PaymentFactory.CreatePayment(PaymentTypeEnum.ManualScheduledSpecificAmount, specificAmount: 10m);

            visits.AddRange(visits2);
            IPaymentAllocationService svc = this.GetPaymentAllocationService(visits, financePlans, null, clientSettings);
            PaymentAllocationResult result = svc.AllocatePayment(payment);
            result.CommitAllocationsToPayment();

            // allocate to fp1 because it's older
            Assert.AreEqual(true, payment.PaymentAllocations.All(x => fp1.FinancePlanId == x.FinancePlanId));

            Assert.AreEqual(payment.ActualPaymentAmount, 10m);
            Assert.AreEqual(payment.PaymentAllocations.Count, 3);
        }

        [Test]
        [TestCase(ClientSettingDefault)]
        [TestCase(ClientSettingDischargeDate)]
        [TestCase(ClientSettingPaymentPriority)]
        [TestCase(ClientSettingPaymentPriorityDischargeDate)]
        public virtual void SpecificPayment_MultipleFinancePlans_OneFPWithPastDueBuckets(ClientSettingEnum settingsType)
        {
            Dictionary<string, string> clientSettings = GetClientSettings(settingsType);

            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(133.32m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
                VisitFactory.GenerateActiveVisit(1000m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
                VisitFactory.GenerateActiveVisit(1000.01m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB)
            };
            FinancePlan fp1 = FinancePlanFactory.CreateFinancePlan(visits, 1, 55.55m, DateTime.UtcNow.AddMonths(-1));

            IList<Tuple<Visit, IList<VisitTransaction>>> visits2 = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(12.34m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
                VisitFactory.GenerateActiveVisit(12.35m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
                VisitFactory.GenerateActiveVisit(133.34m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB)
            };
            FinancePlan fp2 = FinancePlanFactory.CreateFinancePlan(visits2, 3, 55.55m, DateTime.UtcNow.AddMonths(-1));

            List<FinancePlan> financePlans = new List<FinancePlan> { fp1, fp2 };

            Payment payment = PaymentFactory.CreatePayment(PaymentTypeEnum.ManualScheduledSpecificAmount, specificAmount: 10m);
            visits.AddRange(visits2);
            IPaymentAllocationService svc = this.GetPaymentAllocationService(visits, financePlans, null, clientSettings);
            PaymentAllocationResult result = svc.AllocatePayment(payment);
            result.CommitAllocationsToPayment();

            // allocate to pd fp first
            Assert.AreEqual(true, payment.PaymentAllocations.All(x => fp2.FinancePlanId == x.FinancePlanId));

            Assert.AreEqual(payment.ActualPaymentAmount, 10m);
            Assert.AreEqual(payment.PaymentAllocations.Count, 3);
        }

        [Test]
        [TestCase(ClientSettingDefault)]
        [TestCase(ClientSettingDischargeDate)]
        [TestCase(ClientSettingPaymentPriority)]
        [TestCase(ClientSettingPaymentPriorityDischargeDate)]
        public virtual void SpecificPayment_MultipleFinancePlans_WithNoBuckets(ClientSettingEnum settingsType)
        {
            Dictionary<string, string> clientSettings = GetClientSettings(settingsType);

            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(133.32m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
                VisitFactory.GenerateActiveVisit(1000m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
                VisitFactory.GenerateActiveVisit(1000.01m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB)
            };
            FinancePlan fp1 = FinancePlanFactory.CreateFinancePlan(visits, 0, 55.55m, DateTime.UtcNow.AddMonths(-1));
            fp1.InsertDate = DateTime.UtcNow.AddYears(-1);

            IList<Tuple<Visit, IList<VisitTransaction>>> visits2 = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(12.34m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
                VisitFactory.GenerateActiveVisit(12.35m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
                VisitFactory.GenerateActiveVisit(133.34m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB)
            };
            FinancePlan fp2 = FinancePlanFactory.CreateFinancePlan(visits2, 0, 55.55m, DateTime.UtcNow.AddMonths(-1));


            List<FinancePlan> financePlans = new List<FinancePlan> { fp1, fp2 };

            Payment payment = PaymentFactory.CreatePayment(PaymentTypeEnum.ManualScheduledSpecificAmount, specificAmount: 10m);
            visits.AddRange(visits2);
            IPaymentAllocationService svc = this.GetPaymentAllocationService(visits, financePlans, null, clientSettings);
            PaymentAllocationResult result = svc.AllocatePayment(payment);
            result.CommitAllocationsToPayment();

            //Should have all of the payments because it's the older fp.
            Assert.AreEqual(true, payment.PaymentAllocations.All(x => fp1.FinancePlanId == x.FinancePlanId));

            Assert.AreEqual(payment.ActualPaymentAmount, 10m);
            Assert.AreEqual(payment.PaymentAllocations.Count, 3);
        }

        [Test]
        [TestCase(ClientSettingDefault)]
        [TestCase(ClientSettingDischargeDate)]
        [TestCase(ClientSettingPaymentPriority)]
        [TestCase(ClientSettingPaymentPriorityDischargeDate)]
        public virtual void SpecificPayment_MultipleFinancePlans_PaysPastDueVisitsBeforeFP(ClientSettingEnum settingsType)
        {
            Dictionary<string, string> clientSettings = GetClientSettings(settingsType);

            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(133.32m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
                VisitFactory.GenerateActiveVisit(1000m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
                VisitFactory.GenerateActiveVisit(1000.01m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB)
            };
            FinancePlan fp1 = FinancePlanFactory.CreateFinancePlan(visits, 1, 55.55m, DateTime.UtcNow.AddMonths(-1));

            IList<Tuple<Visit, IList<VisitTransaction>>> visits2 = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(12.34m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
                VisitFactory.GenerateActiveVisit(12.35m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
                VisitFactory.GenerateActiveVisit(133.34m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB)
            };
            FinancePlan fp2 = FinancePlanFactory.CreateFinancePlan(visits2, 1, 55.55m, DateTime.UtcNow.AddMonths(-1));

            List<FinancePlan> financePlans = new List<FinancePlan> { fp1, fp2 };

            IList<Tuple<Visit, IList<VisitTransaction>>> visits3 = new List<Tuple<Visit, IList<VisitTransaction>>>()
                {
                this.CreateVisit(12.00m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 2),
                this.CreateVisit(12.01m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 2),
                this.CreateVisit(12.02m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 2)
            };

            Payment payment = PaymentFactory.CreatePayment(PaymentTypeEnum.ManualScheduledSpecificAmount, specificAmount: 10m);


            visits.AddRange(visits2);
            visits.AddRange(visits3);
            IPaymentAllocationService svc = this.GetPaymentAllocationService(visits, financePlans, null, clientSettings);
            PaymentAllocationResult result = svc.AllocatePayment(payment);
            result.CommitAllocationsToPayment();

            //None of the visits on fp's should have been Allocated.
            //Pays past due visits before paying finance plans.
            Assert.AreEqual(true, payment.PaymentAllocations.All(x => x.FinancePlanId == null));

            Assert.AreEqual(payment.ActualPaymentAmount, 10m);
            Assert.AreEqual(payment.PaymentAllocations.Count, 3);
        }

        [Test]
        [TestCase(ClientSettingDefault)]
        [TestCase(ClientSettingDischargeDate)]
        [TestCase(ClientSettingPaymentPriority)]
        [TestCase(ClientSettingPaymentPriorityDischargeDate)]
        public virtual void SpecificPayment_MultipleFinancePlans_PaysPastDueFPBeforePastDueVisits(ClientSettingEnum settingsType)
        {
            Dictionary<string, string> clientSettings = GetClientSettings(settingsType);

            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(133.32m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
                VisitFactory.GenerateActiveVisit(1000m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
                VisitFactory.GenerateActiveVisit(1000.01m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB)
            };
            FinancePlan fp1 = FinancePlanFactory.CreateFinancePlan(visits, 2, 55.55m, DateTime.UtcNow.AddMonths(-1));

            IList<Tuple<Visit, IList<VisitTransaction>>> visits2 = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(12.34m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
                VisitFactory.GenerateActiveVisit(12.35m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
                VisitFactory.GenerateActiveVisit(133.34m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB)
            };
            FinancePlan fp2 = FinancePlanFactory.CreateFinancePlan(visits2, 1, 55.55m, DateTime.UtcNow.AddMonths(-1));

            List<FinancePlan> financePlans = new List<FinancePlan> { fp1, fp2 };

            Payment payment = PaymentFactory.CreatePayment(PaymentTypeEnum.ManualScheduledSpecificAmount, specificAmount: 10m);
            visits.AddRange(visits2);

            IPaymentAllocationService svc = this.GetPaymentAllocationService(visits, financePlans, null, clientSettings);
            PaymentAllocationResult result = svc.AllocatePayment(payment);
            result.CommitAllocationsToPayment();

            //Should allocate to the pd fp first
            Assert.AreEqual(true, payment.PaymentAllocations.All(x => x.FinancePlanId == fp1.FinancePlanId));

            Assert.AreEqual(payment.ActualPaymentAmount, 10m);
            Assert.AreEqual(payment.PaymentAllocations.Count, 3);
        }

        [Test]
        [TestCase(ClientSettingDefault)]
        [TestCase(ClientSettingDischargeDate)]
        [TestCase(ClientSettingPaymentPriority)]
        [TestCase(ClientSettingPaymentPriorityDischargeDate)]
        public virtual void SpecificPayment_MultipleFinancePlans_PaysPastDueFPsBeforePastDueVisits(ClientSettingEnum settingsType)
        {
            Dictionary<string, string> clientSettings = GetClientSettings(settingsType);

            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(133.32m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
                VisitFactory.GenerateActiveVisit(1000m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
                VisitFactory.GenerateActiveVisit(1000.01m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB)
            };
            FinancePlan fp1 = FinancePlanFactory.CreateFinancePlan(visits, 2, 55.55m, DateTime.UtcNow.AddMonths(-1));
            fp1.InsertDate = DateTime.UtcNow.AddYears(-1);

            IList<Tuple<Visit, IList<VisitTransaction>>> visits2 = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(12.34m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
                VisitFactory.GenerateActiveVisit(12.35m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB),
                VisitFactory.GenerateActiveVisit(133.34m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB)
            };
            FinancePlan fp2 = FinancePlanFactory.CreateFinancePlan(visits2, 2, 55.55m, DateTime.UtcNow.AddMonths(-1));

            List<FinancePlan> financePlans = new List<FinancePlan> { fp1, fp2 };

            decimal specificAmountValue = Math.Round(55.55m * 2, 2);
            Payment payment = PaymentFactory.CreatePayment(PaymentTypeEnum.ManualScheduledSpecificAmount, specificAmount: specificAmountValue);

            visits.AddRange(visits2);
            IPaymentAllocationService svc = this.GetPaymentAllocationService(visits, financePlans, null, clientSettings);
            PaymentAllocationResult result = svc.AllocatePayment(payment);
            result.CommitAllocationsToPayment();

            // should allocate fp1 first since it's pd and older
            Assert.AreEqual(true, payment.PaymentAllocations.Count(x => x.FinancePlanId == fp1.FinancePlanId) > 0);

            // should allocate to fp2 next (pd, but newer)
            Assert.AreEqual(true, payment.PaymentAllocations.Count(x => x.FinancePlanId == fp2.FinancePlanId) > 0);

            // shouldnt pay past due visits because there's past due fps.
            Assert.AreEqual(true, payment.PaymentAllocations.All(x => x.FinancePlanId.HasValue));

            Assert.AreEqual(payment.ActualPaymentAmount, specificAmountValue);
            Assert.AreEqual(payment.PaymentAllocations.Count, 6);
        }

        #endregion

        #region Discounts

        // todo: refactor - seems applicable
        /*[Test]
        public virtual void StatementBIF_AttemptsToPayAllVisits_DoesntSetActualHigherThanScheduled_WithDiscount()
        {
            List<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>
            {
                this.CreateVisit(12.34m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(12.34m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(133.33m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(133.33m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(1000m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(1000.01m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1)
            };

            Payment payment = PaymentFactory.CreatePayment(PaymentTypeEnum.ManualScheduledStatementBalanceInFull);
            payment.DiscountOffer = new DiscountOffer();
            foreach (Visit visit in visits)
            {
                payment.DiscountOffer.VisitOffers.Add(new DiscountVisitOffer { DiscountPercent = 0.03m, PaymentVisit = AutoMapper.Mapper.Map<PaymentVisit>(visit) });
            }

            payment.PaymentScheduledAmounts.Add(new PaymentScheduledAmount
            {
                PaymentFinancePlan = null,
                PaymentVisit = null,
                ScheduledAmount = visits.Sum(x => x.CurrentBalance) - payment.DiscountOffer.DiscountTotal - 10m
            });

            IPaymentAllocationService svc = this.GetPaymentAllocationService(visits);
            PaymentAllocationResult result = svc.AllocatePayment(payment);
            result.CommitAllocationsToPayment();
            
            Assert.AreEqual(payment.ScheduledPaymentAmount, payment.ActualPaymentAmount);
        }*/

        // todo: refactor - seems applicable
        /*[Test]
        public virtual void StatementBIF_AttemptsToPayAllVisits_DoesntPayMoreThanCurrentBalanceOnVisits_WithDiscount()
        {
            List<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>
            {
                this.CreateVisit(12.34m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(12.34m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(133.33m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(133.33m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(1000m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1),
                this.CreateVisit(1000.01m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, null, 1)
            };

            decimal sumOfVisits = visits.Sum(x => x.CurrentBalance);

            Payment payment = PaymentFactory.CreatePayment(PaymentTypeEnum.ManualScheduledStatementBalanceInFull);
            payment.DiscountOffer = new DiscountOffer();
            foreach (Visit visit in visits)
            {
                payment.DiscountOffer.VisitOffers.Add(new DiscountVisitOffer { DiscountPercent = 0.03m, PaymentVisit = AutoMapper.Mapper.Map<PaymentVisit>(visit) });
            }

            payment.PaymentScheduledAmounts.Add(new PaymentScheduledAmount
            {
                PaymentFinancePlan = null,
                PaymentVisit = null,
                ScheduledAmount = visits.Sum(x => x.CurrentBalance) - payment.DiscountOffer.DiscountTotal + 10m
            });

            IPaymentAllocationService svc = this.GetPaymentAllocationService(visits);
            PaymentAllocationResult result = svc.AllocatePayment(payment);
            result.CommitAllocationsToPayment();

            Assert.AreNotEqual(payment.ScheduledPaymentAmount, payment.ActualPaymentAmount);
            Assert.AreEqual(sumOfVisits - payment.DiscountOffer.DiscountTotal, payment.ActualPaymentAmount);
        }*/

        #endregion

        #region AllocatePaymentReverse Tests

        [TestCase(true)]
        [TestCase(false)]
        public void AllocatePaymentReverse_for_payment_with_2_allocations_of_2_visits_on_FP_CreatesInverseAllocations(bool withFinancePlan)
        {
            PaymentAllocationServiceMockBuilder paymentAllocationServiceMockBuilder = new PaymentAllocationServiceMockBuilder();
            IPaymentAllocationService paymentAllocationService = paymentAllocationServiceMockBuilder.GetPaymentAllocationService(typeof(T));
            Payment originalPaymentEntity = this.GetOriginalPaymentForReversalTests(withFinancePlan);
            IList<PaymentAllocation> paymentAllocations = originalPaymentEntity.PaymentAllocationsAll;
            int financePlanId = (withFinancePlan) ? originalPaymentEntity.PaymentScheduledAmounts.First().PaymentFinancePlan.FinancePlanId : -1;

            // full refund
            PaymentScheduledAmount secondPartialRefund = new PaymentScheduledAmount { ScheduledAmount = -70.00m };
            if (withFinancePlan)
            {
                secondPartialRefund.PaymentFinancePlan = new PaymentFinancePlan { FinancePlanId = financePlanId };
            }
            Payment reversalPaymentEntity = new Payment
            {
                ParentPayment = originalPaymentEntity,
                ActualPaymentAmount = 0.00m,
                PaymentScheduledAmounts = new List<PaymentScheduledAmount>
                {
                    secondPartialRefund
                }
            };

            PaymentAllocationResult paymentAllocationResult = paymentAllocationService.AllocatePaymentReverse(originalPaymentEntity, reversalPaymentEntity);
            Assert.True(paymentAllocationResult.ActualPaymentAmount == -70.00m);

            // We are expecting 2 allocations to match the inverse of the input allocations
            IEnumerable<decimal> reversedAmounts = paymentAllocationResult.Allocations.Select(x => decimal.Negate(x.ActualAmount));
            int matchingOriginalToInverseAllocationsCount = paymentAllocations.Count(x => reversedAmounts.Contains(x.ActualAmount));
            Assert.True(matchingOriginalToInverseAllocationsCount == 2);
        }

        [TestCase(true)]
        [TestCase(false)]
        public void AllocatePaymentReverse_partial_for_payment_with_2_allocations_of_2_visits_on_FP_CreatesInverseAllocations(bool withFinancePlan)
        {
            PaymentAllocationServiceMockBuilder paymentAllocationServiceMockBuilder = new PaymentAllocationServiceMockBuilder();
            IPaymentAllocationService paymentAllocationService = paymentAllocationServiceMockBuilder.GetPaymentAllocationService(typeof(T));
            Payment originalPaymentEntity = this.GetOriginalPaymentForReversalTests(withFinancePlan);
            int financePlanId = (withFinancePlan) ? originalPaymentEntity.PaymentScheduledAmounts.First().PaymentFinancePlan.FinancePlanId : -1;

            //firsrt parial
            PaymentScheduledAmount firstPartialRefund = new PaymentScheduledAmount { ScheduledAmount = -10.00m };
            if (withFinancePlan)
            {
                firstPartialRefund.PaymentFinancePlan = new PaymentFinancePlan { FinancePlanId = financePlanId };
            }

            Payment reversalPaymentEntity = new Payment
            {
                ParentPayment = originalPaymentEntity,
                ActualPaymentAmount = 0.00m,
                PaymentScheduledAmounts = new List<PaymentScheduledAmount>
                {
                    firstPartialRefund
                }
            };

            PaymentAllocationResult paymentAllocationResult = paymentAllocationService.AllocatePaymentReverse(originalPaymentEntity, reversalPaymentEntity);
            // We have an expectation later in the test that depends upon paymentAllocationResult, verify intial state is correct
            Assert.True(paymentAllocationResult.ActualPaymentAmount == -10.00m);

            paymentAllocationResult.CommitAllocationsToPayment();
            originalPaymentEntity.ReversalPayments.Add(reversalPaymentEntity);

            //second partial
            PaymentScheduledAmount secondPartialRefund = new PaymentScheduledAmount { ScheduledAmount = -15.00m };
            if (withFinancePlan)
            {
                secondPartialRefund.PaymentFinancePlan = new PaymentFinancePlan { FinancePlanId = financePlanId };
            }
            Payment reversalPaymentEntity2 = new Payment
            {
                ParentPayment = originalPaymentEntity,
                ActualPaymentAmount = 0.00m,
                PaymentScheduledAmounts = new List<PaymentScheduledAmount>
                {
                    secondPartialRefund
                }
            };

            PaymentAllocationResult paymentAllocationResult2 = paymentAllocationService.AllocatePaymentReverse(originalPaymentEntity, reversalPaymentEntity2);

            Assert.True(paymentAllocationResult2.ActualPaymentAmount == -15.00m);

            //We are expecting 2 allocations of -9.90 and -5.10
            decimal[] expectedAllocationsValues = new decimal[] { -9.90m, -5.10m };
            int matchingExpectedAllocationsCount = paymentAllocationResult2.Allocations.Count(x => expectedAllocationsValues.Contains(x.ActualAmount));
            Assert.True(matchingExpectedAllocationsCount == 2);
        }

        [TestCase(true)]
        [TestCase(false)]
        public void AllocatePaymentReverse_partial_exceeding_available_should_throw(bool withFinancePlan)
        {
            PaymentAllocationServiceMockBuilder paymentAllocationServiceMockBuilder = new PaymentAllocationServiceMockBuilder();
            IPaymentAllocationService paymentAllocationService = paymentAllocationServiceMockBuilder.GetPaymentAllocationService(typeof(T));
            Payment originalPaymentEntity = this.GetOriginalPaymentForReversalTests(withFinancePlan);
            int financePlanId = (withFinancePlan) ? originalPaymentEntity.PaymentScheduledAmounts.First().PaymentFinancePlan.FinancePlanId : -1;

            //first partial
            PaymentScheduledAmount firstPartialRefund = new PaymentScheduledAmount { ScheduledAmount = -10.00m };
            if (withFinancePlan)
            {
                firstPartialRefund.PaymentFinancePlan = new PaymentFinancePlan { FinancePlanId = financePlanId };
            }

            Payment reversalPaymentEntity = new Payment
            {
                ParentPayment = originalPaymentEntity,
                ActualPaymentAmount = 0.00m,
                PaymentScheduledAmounts = new List<PaymentScheduledAmount>
                {
                    firstPartialRefund
                }
            };

            PaymentAllocationResult paymentAllocationResult = paymentAllocationService.AllocatePaymentReverse(originalPaymentEntity, reversalPaymentEntity);
            // We have an expectation later in the test that depends upon paymentAllocationResult, verify intial state is correct
            Assert.True(paymentAllocationResult.ActualPaymentAmount == -10.00m);

            paymentAllocationResult.CommitAllocationsToPayment();
            originalPaymentEntity.ReversalPayments.Add(reversalPaymentEntity);

            //second partial
            PaymentScheduledAmount secondPartialRefund = new PaymentScheduledAmount { ScheduledAmount = -75.00m };
            if (withFinancePlan)
            {
                secondPartialRefund.PaymentFinancePlan = new PaymentFinancePlan { FinancePlanId = financePlanId };
            }
            Payment reversalPaymentEntity2 = new Payment
            {
                ParentPayment = originalPaymentEntity,
                ActualPaymentAmount = 0.00m,
                PaymentScheduledAmounts = new List<PaymentScheduledAmount>
                {
                    secondPartialRefund
                }
            };

            Assert.Throws(Is.TypeOf<Exception>(),
                delegate
                {
                    paymentAllocationService.AllocatePaymentReverse(originalPaymentEntity, reversalPaymentEntity2);
                });
        }

        private Payment GetOriginalPaymentForReversalTests(bool withFinancePlan)
        {
            PaymentVisit paymentVisit1 = new PaymentVisit { VisitId = 3006671 };
            PaymentVisit paymentVisit2 = new PaymentVisit { VisitId = 3006672 };

            PaymentAllocation paymentAllocation1 = new PaymentAllocation { ActualAmount = 46.20m, OriginalAmount = 46.20m, PaymentVisit = paymentVisit1 };
            PaymentAllocation paymentAllocation2 = new PaymentAllocation { ActualAmount = 23.80m, OriginalAmount = 23.80m, PaymentVisit = paymentVisit2 };

            List<PaymentAllocation> paymentAllocations = new List<PaymentAllocation> { paymentAllocation1, paymentAllocation2 };
            int financePlanId = 3799;


            List<PaymentScheduledAmount> paymentScheduledAmounts = new List<PaymentScheduledAmount>();

            if (withFinancePlan)
            {
                paymentScheduledAmounts.Add(new PaymentScheduledAmount { PaymentFinancePlan = new PaymentFinancePlan { FinancePlanId = financePlanId } });
            }


            Payment originalPaymentEntity = new Payment
            {
                ActualPaymentAmount = 70.00m,
                PaymentScheduledAmounts = paymentScheduledAmounts,
                PaymentAllocations = paymentAllocations
            };

            return originalPaymentEntity;
        }

        #endregion

        #region Test Case Parameters

        public class AllocationTestParameters
        {
            public string TestCaseDescription { get; set; }
            public decimal PaymentAmount { get; set; }
            public PaymentTypeEnum PaymentType { get; set; }
            public int ExpectedAllocationCount => this.AllocationVisits.Count(x => x.ExpectedAllocationAmount != 0.00m);
            public string PaymentAllocationOrderedPriorityGrouping { get; set; }
            public IDictionary<string, string> ClientSettings { get; set; }
            public IList<AllocationVisit> AllocationVisits { get; set; }
            public IList<AllocationFinancePlan> AllocationFinancePlans { get; set; }

            public override string ToString()
            {
                return this.TestCaseDescription;
            }
        }

        public class AllocationVisit
        {
            public AllocationVisit(int visitId, decimal amount, string billingApplication, DateTime insertDate, decimal expectedAllocationAmount, int? financePlanId = null, int? paymentPriority = null)
            {
                this.VisitId = visitId;
                this.Amount = amount;
                this.BillingApplication = billingApplication;
                this.InsertDate = insertDate;
                this.ExpectedAllocationAmount = expectedAllocationAmount;
                this.FinancePlanId = financePlanId;
                this.PaymentPriority = paymentPriority;
            }

            public int VisitId { get; set; }
            public DateTime InsertDate { get; set; }
            public string BillingApplication { get; set; }
            public int? PaymentPriority { get; set; }
            public decimal Amount { get; set; }
            public decimal ExpectedAllocationAmount { get; set; }
            public int? FinancePlanId { get; set; }
        }

        public class AllocationFinancePlan
        {
            public AllocationFinancePlan(int financePlanId, int numberOfBuckets, decimal paymentAmount, DateTime? originationDate = null, decimal? interestRate = null)
            {
                this.FinancePlanId = financePlanId;
                this.NumberOfBuckets = numberOfBuckets;
                this.PaymentAmount = paymentAmount;
                this.OriginationDate = originationDate;
                this.InterestRate = interestRate;
            }

            public int FinancePlanId { get; set; }
            public int NumberOfBuckets { get; set; }
            public decimal PaymentAmount { get; set; }
            public DateTime? OriginationDate { get; set; }
            public decimal? InterestRate { get; set; }
        }

        #endregion
    }
}

﻿namespace Ivh.Domain.Payment.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Base.Interfaces;
    using AutoMapper;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Common.Base.Utilities.Helpers;
    using Common.Tests.Helpers.FinancePlan;
    using Common.Tests.Helpers.Guarantor;
    using Common.Tests.Helpers.Payment;
    using Common.Tests.Helpers.Visit;
    using Common.VisitPay.Enums;
    using Guarantor.Entities;
    using FinanceManagement.Entities;
    using FinanceManagement.FinancePlan.Entities;
    using FinanceManagement.Payment.Entities;
    using FinanceManagement.Services;
    using FinanceManagement.Statement.Entities;
    using Moq;
    using NUnit.Framework;
    using Visit.Entities;
    using Visit.Interfaces;
    using Visit.Services;

    [TestFixture]
    public class PaymentServiceTests : DomainTestBase
    {
        private Payment CreatePayment(decimal amountToChargeFor = 0m, PaymentMethodTypeEnum paymentMethodType = PaymentMethodTypeEnum.Visa, DateTime? insertDate = null, PaymentTypeEnum? paymentTypeEnum = null)
        {
            return new Payment
            {
                PaymentId = new Random().Next(10000, 100000),
                Guarantor = new Guarantor
                {
                    ManagedConsolidationGuarantors = new List<ConsolidationGuarantor>(),
                    ManagingConsolidationGuarantors = new List<ConsolidationGuarantor>(),
                    VpGuarantorId = 1
                },
                PaymentMethod = new PaymentMethod() {PaymentMethodType = paymentMethodType, VpGuarantorId = 1},
                ActualPaymentDate = DateTime.UtcNow,
                ActualPaymentAmount = amountToChargeFor,
                InsertDate = insertDate ?? DateTime.UtcNow,
                PaymentScheduledAmounts = new List<PaymentScheduledAmount>(),
                PaymentType = paymentTypeEnum.GetValueOrDefault()
            };
        }

        private Payment CreatePendingPayment(decimal amountToChargeFor, PaymentTypeEnum paymentType, PaymentMethodTypeEnum paymentMethodType = PaymentMethodTypeEnum.Visa, DateTime? insertDate = null)
        {
            Payment payment = new Payment
            {
                PaymentId = new Random().Next(10000, 100000),
                Guarantor = new Guarantor
                {
                    ManagedConsolidationGuarantors = new List<ConsolidationGuarantor>(),
                    ManagingConsolidationGuarantors = new List<ConsolidationGuarantor>(),
                    VpGuarantorId = 1
                },
                PaymentType = paymentType,
                PaymentMethod = new PaymentMethod {PaymentMethodType = paymentMethodType, VpGuarantorId = 1},
                InsertDate = insertDate ?? DateTime.UtcNow,
                PaymentScheduledAmounts = new List<PaymentScheduledAmount>()
            };

            payment.AddPaymentScheduledAmount(new PaymentScheduledAmount
            {
                ScheduledAmount = amountToChargeFor-10m,
            });
            payment.AddPaymentScheduledAmount(new PaymentScheduledAmount
            {
                ScheduledAmount = 10m,
            });

            return payment;
        }

        private Payment CreatePendingPaymentForFinancePlan(decimal amountToChargeFor, FinancePlan fp, PaymentMethodTypeEnum paymentMethodType = PaymentMethodTypeEnum.Visa, DateTime? insertDate = null)
        {
            Payment pendingPaymentForFinancePlan = new Payment
            {
                PaymentId = new Random().Next(10000, 100000),
                Guarantor = new Guarantor
                {
                    ManagedConsolidationGuarantors = new List<ConsolidationGuarantor>(),
                    ManagingConsolidationGuarantors = new List<ConsolidationGuarantor>(),
                    VpGuarantorId = 1
                },
                PaymentMethod = new PaymentMethod() { PaymentMethodType = paymentMethodType, VpGuarantorId = 1 },
                ActualPaymentDate = null,
                InsertDate = insertDate ?? DateTime.UtcNow,
                PaymentScheduledAmounts = new List<PaymentScheduledAmount>()
            };
            //VP-56 convert to PaymentFinancePlan
            PaymentFinancePlan paymentFinancePlan = new PaymentFinancePlan { FinancePlanId = fp.FinancePlanId };
            pendingPaymentForFinancePlan.AddPaymentScheduledAmount(new PaymentScheduledAmount() { PaymentFinancePlan = paymentFinancePlan, ScheduledAmount = amountToChargeFor, InsertDate = pendingPaymentForFinancePlan.InsertDate} );

            return pendingPaymentForFinancePlan;
        }

        [Test]
        public void HandlesFailures_PaymentProviderRespondsWithClientError()
        {
            PaymentServiceMockBuilder builder = new PaymentServiceMockBuilder();
            
            Payment payment = this.CreatePayment(10m);
            builder.Setup((x) =>
            {
                x.PaymentProviderMock.Setup(t => t.SubmitPaymentsAsync(It.IsAny<IList<Payment>>(), It.IsAny<PaymentProcessor>()))
                    .Returns<IList<Payment>, PaymentProcessor>((ps, pp) => this.CreatePaymentProcessorResponse(ps, pp, PaymentProcessorResponseStatusEnum.CallToProcessorFailed));
                x.MerchantAccountAllocationServiceMock.Setup(t => t.SplitPayments(It.IsAny<IList<Payment>>())).Returns<IList<Payment>>(paymentEntities => paymentEntities);
            });
            this.SetupBasicVisitAndAllocation(builder);

            PaymentService paymentService = builder.CreatePaymentService();
            int? visitPayUserId = 81; //statement Guy
            bool success = paymentService.ChargePayment(payment, visitPayUserId).First().Success;

            Assert.AreEqual(false, success);
            Assert.AreEqual(payment.PaymentStatus, PaymentStatusEnum.ActiveGatewayError);
        }

        [Test]
        public void HandlesFailures_PaymentProviderRespondsWithServerError()
        {
            PaymentServiceMockBuilder builder = new PaymentServiceMockBuilder();
            Payment payment = this.CreatePayment(10m);
            builder.Setup((x) =>
            {
                x.PaymentProviderMock.Setup(t => t.SubmitPaymentsAsync(It.IsAny<IList<Payment>>(), It.IsAny<PaymentProcessor>()))
                .Returns<IList<Payment>, PaymentProcessor>((ps, pp) => this.CreatePaymentProcessorResponse(ps, pp, PaymentProcessorResponseStatusEnum.LinkFailure));
                x.MerchantAccountAllocationServiceMock.Setup(t => t.SplitPayments(It.IsAny<IList<Payment>>())).Returns<IList<Payment>>(paymentEntities => paymentEntities);
            });
            
            this.SetupBasicVisitAndAllocation(builder);

            PaymentService paymentService = builder.CreatePaymentService();
            int? visitPayUserId = 81; //statement Guy
            bool success = paymentService.ChargePayment(payment, visitPayUserId).First().Success;

            Assert.AreEqual(false, success);
            Assert.AreEqual(payment.PaymentStatus, PaymentStatusEnum.ActivePendingLinkFailure);
        }

        [Test]
        public void HandlesFailures_PaymentProviderRespondsDecline()
        {
            PaymentServiceMockBuilder builder = new PaymentServiceMockBuilder();
            Payment payment = this.CreatePayment(10m);
            builder.Setup((x) =>
            {
                x.PaymentProviderMock.Setup(t => t.SubmitPaymentsAsync(It.IsAny<IList<Payment>>(), It.IsAny<PaymentProcessor>()))
                    .Returns<IList<Payment>, PaymentProcessor>((ps, pp) => this.CreatePaymentProcessorResponse(ps, pp, PaymentProcessorResponseStatusEnum.Decline));
                x.MerchantAccountAllocationServiceMock.Setup(t => t.SplitPayments(It.IsAny<IList<Payment>>())).Returns<IList<Payment>>(paymentEntities => paymentEntities);
            });
            this.SetupBasicVisitAndAllocation(builder);

            PaymentService paymentService = builder.CreatePaymentService();
            int? visitPayUserId = 81; //statement Guy
            bool success = paymentService.ChargePayment(payment, visitPayUserId).First().Success;

            Assert.AreEqual(false, success);
            Assert.AreEqual(PaymentStatusEnum.ClosedFailed, payment.PaymentStatus);
        }

        [Test]
        public void HandlesFailures_PaymentProviderRespondsBadData()
        {
            PaymentServiceMockBuilder builder = new PaymentServiceMockBuilder();
            Payment payment = this.CreatePayment(10m);
            builder.Setup((x) =>
            {
                x.PaymentProviderMock.Setup(t => t.SubmitPaymentsAsync(It.IsAny<IList<Payment>>(), It.IsAny<PaymentProcessor>()))
                    .Returns<IList<Payment>, PaymentProcessor>((ps, pp) => this.CreatePaymentProcessorResponse(ps, pp, PaymentProcessorResponseStatusEnum.BadData));
                x.MerchantAccountAllocationServiceMock.Setup(t => t.SplitPayments(It.IsAny<IList<Payment>>())).Returns<IList<Payment>>(paymentEntities => paymentEntities);
            });
            this.SetupBasicVisitAndAllocation(builder);

            PaymentService paymentService = builder.CreatePaymentService();
            int? visitPayUserId = 81; //statement Guy
            bool success = paymentService.ChargePayment(payment, visitPayUserId).First().Success;

            Assert.AreEqual(false, success);
            Assert.AreEqual(PaymentStatusEnum.ClosedFailed, payment.PaymentStatus);
        }

        [Test]
        public void HandlesFailures_PaymentProviderRespondsError()
        {
            PaymentServiceMockBuilder builder = new PaymentServiceMockBuilder();
            Payment payment = this.CreatePayment(10m);
            builder.Setup((x) =>
            {
                x.PaymentProviderMock.Setup(t => t.SubmitPaymentsAsync(It.IsAny<IList<Payment>>(), It.IsAny<PaymentProcessor>()))
                    .Returns<IList<Payment>, PaymentProcessor>((ps, pp) => this.CreatePaymentProcessorResponse(ps, pp, PaymentProcessorResponseStatusEnum.Error));
                x.MerchantAccountAllocationServiceMock.Setup(t => t.SplitPayments(It.IsAny<IList<Payment>>())).Returns<IList<Payment>>(paymentEntities => paymentEntities);
            });
            this.SetupBasicVisitAndAllocation(builder);

            PaymentService paymentService = builder.CreatePaymentService();
            int? visitPayUserId = 81; //statement Guy
            bool success = paymentService.ChargePayment(payment, visitPayUserId).First().Success;

            Assert.AreEqual(false, success);
            Assert.AreEqual(PaymentStatusEnum.ClosedFailed, payment.PaymentStatus);
        }

        [Test]
        public void HandlesFailures_PaymentProviderReturnsNullResponses()
        {
            PaymentServiceMockBuilder builder = new PaymentServiceMockBuilder();
            Payment payment = this.CreatePayment(10m);
            builder.Setup((x) =>
            {
                x.PaymentProviderMock.Setup(t => t.SubmitPaymentsAsync(It.IsAny<IList<Payment>>(), It.IsAny<PaymentProcessor>()))
                    .Returns<IList<Payment>, PaymentProcessor>((ps, pp) => null);
                x.MerchantAccountAllocationServiceMock.Setup(t => t.SplitPayments(It.IsAny<IList<Payment>>())).Returns<IList<Payment>>(paymentEntities => paymentEntities);
            });
            this.SetupBasicVisitAndAllocation(builder);

            PaymentService paymentService = builder.CreatePaymentService();
            int? visitPayUserId = 81; //statement Guy
            bool success = paymentService.ChargePayment(payment, visitPayUserId).First().Success;

            Assert.AreEqual(false, success);
            Assert.AreEqual(PaymentStatusEnum.ActivePendingLinkFailure, payment.PaymentStatus);
        }

        [Test]
        public void HandlesFailures_PaymentProviderThrows()
        {
            PaymentServiceMockBuilder builder = new PaymentServiceMockBuilder();
            builder.Setup((x) =>
            {
                x.PaymentProviderMock.Setup(t => t.SubmitPaymentsAsync(It.IsAny<IList<Payment>>(), It.IsAny<PaymentProcessor>())).Throws(new Exception());
                x.MerchantAccountAllocationServiceMock.Setup(t => t.SplitPayments(It.IsAny<IList<Payment>>())).Returns<IList<Payment>>(paymentEntities => paymentEntities);
            });
            Payment payment = this.CreatePayment(10m);
            this.SetupBasicVisitAndAllocation(builder);

            PaymentService paymentService = builder.CreatePaymentService();
            int? visitPayUserId = 81; //statement Guy
            bool success = paymentService.ChargePayment(payment, visitPayUserId).First().Success;

            Assert.AreEqual(false, success);
            Assert.AreEqual(PaymentStatusEnum.ActivePendingLinkFailure, payment.PaymentStatus);
        }

        [Test]
        public void HandlesSuccess_PaymentProviderReturnsSuccessACH()
        {
            PaymentServiceMockBuilder builder = new PaymentServiceMockBuilder();
            Payment payment = this.CreatePayment(123m, PaymentMethodTypeEnum.AchChecking);
            builder.Setup((x) =>
            {
                x.PaymentProviderMock.Setup(t => t.SubmitPaymentsAsync(It.IsAny<IList<Payment>>(), It.IsAny<PaymentProcessor>()))
                    .Returns<IList<Payment>, PaymentProcessor>((ps, pp) => this.CreatePaymentProcessorResponse(ps, pp, PaymentProcessorResponseStatusEnum.Approved));
                x.MerchantAccountAllocationServiceMock.Setup(t => t.SplitPayments(It.IsAny<IList<Payment>>())).Returns<IList<Payment>>(paymentEntities => paymentEntities);
            });

            this.SetupBasicVisitAndAllocation(builder);

            PaymentService paymentService = builder.CreatePaymentService();
            int? visitPayUserId = 81; //statement Guy
            bool success = paymentService.ChargePayment(payment, visitPayUserId).First().Success;

            Assert.AreEqual(true, success);
            Assert.AreEqual(PaymentStatusEnum.ActivePendingACHApproval, payment.PaymentStatus);
            //Should have added an additional transaction from the distribution.
            //Assert.AreEqual(payment.PaymentAllocations.Select(x => x.PaymentVisit).FirstOrDefault().Transactions.Count, 3);
        }

        [Test]
        public void HandlesSuccess_PaymentProviderReturnAchPaymentFull()
        {
            PaymentServiceMockBuilder builder = new PaymentServiceMockBuilder();
            Payment payment = this.CreatePayment(123m, PaymentMethodTypeEnum.AchChecking);
            bool calledOffset = false;
            builder.Setup((x) =>
            {
                x.PaymentProviderMock.Setup(t => t.SubmitPaymentsAsync(It.IsAny<IList<Payment>>(), It.IsAny<PaymentProcessor>()))
                    .Returns<IList<Payment>, PaymentProcessor>((ps, pp) => this.CreatePaymentProcessorResponse(ps, pp, PaymentProcessorResponseStatusEnum.Approved));
                x.MerchantAccountAllocationServiceMock.Setup(t => t.SplitPayments(It.IsAny<IList<Payment>>())).Returns<IList<Payment>>(paymentEntities => paymentEntities);
                x.PaymentAllocationServiceMock.Setup(t => t.OffsetAllAllocationsInPayment(It.IsAny<Payment>())).Callback(() => { calledOffset = true; });

            });

            this.SetupBasicVisitAndAllocation(builder);

            PaymentService paymentService = builder.CreatePaymentService();
            int? visitPayUserId = 81; //statement Guy
            bool success = paymentService.ChargePayment(payment, visitPayUserId).First().Success;

            Assert.AreEqual(true, success);
            Assert.AreEqual(PaymentStatusEnum.ActivePendingACHApproval, payment.PaymentStatus);
            //Should have added an additional transaction from the distribution.
            Assert.AreEqual(payment.PaymentAllocations.Sum(x => x.ActualAmount), 123m);

            paymentService.ReturnAchPayment(payment, "R01", payment.TransactionId);
            Assert.AreEqual(PaymentStatusEnum.ClosedFailed, payment.PaymentStatus);
            Assert.AreEqual(true, calledOffset);
            
            //Since this isnt mocking up the allocation service, this will not happen. Instead we check that the method that does this gets called.  The allocation service tests have tests for this.
            //Assert.AreEqual(payment.PaymentAllocations.Sum(x => x.ActualAmount), 0m);
        }

        [Test]
        public void HandlesSuccess_PaymentProviderReturnAchPaymentFull_for_multiple_finance_plans_ScheduledAmount_does_not_change()
        {
            PaymentServiceMockBuilder builder = new PaymentServiceMockBuilder();
            Payment payment = this.CreatePayment(30m, PaymentMethodTypeEnum.AchChecking);
            IList<FinancePlan> financePlans = new List<FinancePlan> { new FinancePlan { FinancePlanId = 1 }, new FinancePlan { FinancePlanId = 2 } };
            builder.Setup((x) =>
            {
                x.PaymentProviderMock.Setup(t => t.SubmitPaymentsAsync(It.IsAny<IList<Payment>>(), It.IsAny<PaymentProcessor>()))
                    .Returns<IList<Payment>, PaymentProcessor>((ps, pp) => this.CreatePaymentProcessorResponse(ps, pp, PaymentProcessorResponseStatusEnum.Approved));
                x.FinancePlanServiceMock.Setup(t => t.GetFinancePlansById(It.IsAny<IList<int>>())).Returns(financePlans);
                x.MerchantAccountAllocationServiceMock.Setup(t => t.SplitPayments(It.IsAny<IList<Payment>>())).Returns<IList<Payment>>(paymentEntities => paymentEntities);
            });

            PaymentService paymentService = builder.CreatePaymentService();
            int? visitPayUserId = 81; //statement Guy
            Dictionary<int, decimal> financePlanAmount = new Dictionary<int, decimal>()
            {
                {1, 10m},
                {2, 20m}
            };
            this.SetupBasicVisitAndAllocationWithFunction(builder,
                (callbackPayment, result, status) =>
                {
                    result.Allocations.Clear();
                    result.ActualPaymentAmount = 0m;
                    result.Payment = callbackPayment;
                    result.ActualPaymentDate = DateTime.UtcNow;
                    foreach (KeyValuePair<int, decimal> keyValuePair in financePlanAmount)
                    {
                        Visit visit = VisitFactory.GenerateActiveVisit(keyValuePair.Value, 2).Item1;
                        visit.VPGuarantor = new Guarantor();
                        visit.SetBalanceTransferStatus(new BalanceTransferStatusHistory { BalanceTransferStatus = new BalanceTransferStatus { BalanceTransferStatusId = (int)status } });

                        result.ActualPaymentAmount += keyValuePair.Value;

                        result.Allocations.Add(new PaymentAllocation
                        {
                            ActualAmount = keyValuePair.Value,
                            PaymentAllocationStatus = PaymentAllocationStatusEnum.PendingHsAcknowledgement,
                            FinancePlanId = keyValuePair.Key,
                            Payment = callbackPayment,
                            PaymentVisit = Mapper.Map<PaymentVisit>(visit)
                        });
                    }
                    return true;
                },
                (callbackPayments, results, status) =>
                {
                    results.Clear();
                    foreach (Payment callbackPayment in callbackPayments)
                    {
                        PaymentAllocationResult localResult = new PaymentAllocationResult();

                        localResult.Allocations.Clear();
                        localResult.ActualPaymentAmount = 0m;
                        localResult.Payment = callbackPayment;
                        localResult.ActualPaymentDate = DateTime.UtcNow;
                        foreach (KeyValuePair<int, decimal> keyValuePair in financePlanAmount)
                        {
                            Visit visit = VisitFactory.GenerateActiveVisit(keyValuePair.Value, 2).Item1;
                            visit.VPGuarantor = new Guarantor();
                            visit.SetBalanceTransferStatus(new BalanceTransferStatusHistory {BalanceTransferStatus = new BalanceTransferStatus {BalanceTransferStatusId = (int) status}});

                            localResult.ActualPaymentAmount += keyValuePair.Value;

                            localResult.Allocations.Add(new PaymentAllocation
                            {
                                ActualAmount = keyValuePair.Value,
                                PaymentAllocationStatus = PaymentAllocationStatusEnum.PendingHsAcknowledgement,
                                FinancePlanId = keyValuePair.Key,
                                Payment = callbackPayment,
                                PaymentVisit = Mapper.Map<PaymentVisit>(visit)
                            });
                        }
                        results.Add(localResult);
                    }

                    return true;
                }
                );

            bool success = paymentService.ChargePayment(payment, visitPayUserId).First().Success;

            Assert.AreEqual(true, success);
            Assert.AreEqual(PaymentStatusEnum.ActivePendingACHApproval, payment.PaymentStatus);

            int fp1Id = financePlans.First(x => x.FinancePlanId == 1).FinancePlanId;
            int fp2Id = financePlans.First(x => x.FinancePlanId == 2).FinancePlanId;
            //Setting the allocations after the fact as the return needs it, but we're just mocking everything so didnt need it before
            List<PaymentScheduledAmount> paymentScheduledAmounts = new List<PaymentScheduledAmount>
            {
                new PaymentScheduledAmount
                {
                    // VP-56 convert to PaymentFinancePlan
                    PaymentFinancePlan = new PaymentFinancePlan { FinancePlanId = fp1Id },
                    Payment = payment,
                    ScheduledAmount = 10
                },
                new PaymentScheduledAmount
                {
                    // VP-56 convert to PaymentFinancePlan
                    PaymentFinancePlan =new PaymentFinancePlan { FinancePlanId = fp2Id },
                    Payment = payment,
                    ScheduledAmount = 20
                }
            };
            payment.PaymentScheduledAmounts = paymentScheduledAmounts;
            paymentService.ReturnAchPayment(payment, "R01", payment.TransactionId);

            // Bug VPNG-16918 Assert that the PaymentScheduledAmount.ScheduledAmount did not get changed
            PaymentScheduledAmount scheduledAmoutFp1 = payment.PaymentScheduledAmounts.First(x => x.PaymentFinancePlan.FinancePlanId == 1);
            PaymentScheduledAmount scheduledAmoutFp2 = payment.PaymentScheduledAmounts.First(x => x.PaymentFinancePlan.FinancePlanId == 2);
            Assert.AreEqual(scheduledAmoutFp1.ScheduledAmount, 10m);
            Assert.AreEqual(scheduledAmoutFp2.ScheduledAmount, 20m);
        }

        [Test]
        public void HandlesSuccess_PaymentProviderReturnAchPaymentPartial()
        {
            PaymentServiceMockBuilder builder = new PaymentServiceMockBuilder();
            Payment payment = this.CreatePayment(123m, PaymentMethodTypeEnum.AchChecking);
            bool calledOffset = false;
            builder.Setup((x) =>
            {
                x.PaymentProviderMock.Setup(t => t.SubmitPaymentsAsync(It.IsAny<IList<Payment>>(), It.IsAny<PaymentProcessor>()))
                    .Returns<IList<Payment>, PaymentProcessor>((ps, pp) => this.CreatePaymentProcessorResponse(ps, pp, PaymentProcessorResponseStatusEnum.Approved));
                x.PaymentAllocationServiceMock.Setup(t => t.OffsetAllAllocationsInPayment(It.IsAny<Payment>())).Callback(() => { calledOffset = true; });

            });

            this.SetupBasicVisitAndAllocation(builder);

            PaymentService paymentService = builder.CreatePaymentService();
            int? visitPayUserId = 81; //statement Guy
            bool success = paymentService.ChargePayment(payment, visitPayUserId).First().Success;

            Assert.AreEqual(true, success);
            Assert.AreEqual(payment.PaymentStatus, PaymentStatusEnum.ActivePendingACHApproval);
            //Should have added an additional transaction from the distribution.
            Assert.AreEqual(payment.PaymentAllocations.Sum(x => x.ActualAmount), 123m);

            paymentService.ReturnAchPayment(payment, "R01", payment.TransactionId, true);
            Assert.AreEqual(payment.PaymentStatus, PaymentStatusEnum.ClosedFailed);
            Assert.AreEqual(true, calledOffset);

            //Since this isnt mocking up the allocation service, this will not happen. Instead we check that the method that does this gets called.  The allocation service tests have tests for this.
            //Assert.AreEqual(payment.PaymentAllocations.Sum(x => x.ActualAmount), 0);
        }

        private void SetupBasicVisitAndAllocation(PaymentServiceMockBuilder builder)
        {
            this.SetupBasicVisitAndAllocationWithAmountAndFinancePlanIds(builder, 123m, null);
        }

        private void SetupBasicVisitAndAllocationWithAmountAndFinancePlanIds(PaymentServiceMockBuilder builder, decimal allocatedAmount, int? financePlanId, BalanceTransferStatusEnum btStatus = BalanceTransferStatusEnum.Eligible)
        {
            builder.Setup((x) =>
            {
                x.MerchantAccountAllocationServiceMock.Setup(t => t.SplitPayments(It.IsAny<IList<Payment>>())).Returns<IList<Payment>>(paymentEntities => paymentEntities);
                PaymentAllocationResult result = new PaymentAllocationResult();
                IList<PaymentAllocationResult> results = new List<PaymentAllocationResult>();
                x.PaymentAllocationServiceMock.Setup(t => t.AllocatePayment(It.IsAny<Payment>())).Callback((Payment callbackPayment) =>
                {
                    Visit visit = VisitFactory.GenerateActiveVisit(allocatedAmount, 2).Item1;
                    visit.VPGuarantor = new Guarantor();
                    visit.SetBalanceTransferStatus(new BalanceTransferStatusHistory {BalanceTransferStatus = new BalanceTransferStatus {BalanceTransferStatusId = (int)btStatus}});
                    
                    result.Allocations.Clear();
                    result.ActualPaymentAmount = allocatedAmount;
                    result.Payment = callbackPayment;
                    result.ActualPaymentDate = DateTime.UtcNow;

                    result.Allocations.Add(new PaymentAllocation
                    {
                        ActualAmount = allocatedAmount,
                        PaymentAllocationStatus = PaymentAllocationStatusEnum.PendingHsAcknowledgement,
                        FinancePlanId = financePlanId,
                        Payment = callbackPayment,
                        PaymentVisit = Mapper.Map<PaymentVisit>(visit)
                    });
                }).Returns(result);
                x.PaymentAllocationServiceMock.Setup(t => t.AllocatePayments(It.IsAny<IList<Payment>>())).Callback((IList<Payment> callbackPayments) =>
                {
                    results.Clear();
                    
                    foreach (Payment callbackPayment in callbackPayments)
                    {
                        PaymentAllocationResult localResult = new PaymentAllocationResult();
                        Visit visit = VisitFactory.GenerateActiveVisit(allocatedAmount, 2).Item1;
                        visit.VPGuarantor = new Guarantor();
                        visit.SetBalanceTransferStatus(new BalanceTransferStatusHistory { BalanceTransferStatus = new BalanceTransferStatus { BalanceTransferStatusId = (int)btStatus } });

                        localResult.Allocations.Clear();
                        localResult.ActualPaymentAmount = allocatedAmount;
                        localResult.Payment = callbackPayment;
                        localResult.ActualPaymentDate = DateTime.UtcNow;

                        localResult.Allocations.Add(new PaymentAllocation
                        {
                            ActualAmount = allocatedAmount,
                            PaymentAllocationStatus = PaymentAllocationStatusEnum.PendingHsAcknowledgement,
                            FinancePlanId = financePlanId,
                            Payment = callbackPayment,
                            PaymentVisit = Mapper.Map<PaymentVisit>(visit)
                        });
                        results.Add(localResult);
                    }
                    
                }).Returns(results);
            });
        }

        private void SetupBasicVisitAndAllocationWithFunction(PaymentServiceMockBuilder builder, Func<Payment, PaymentAllocationResult, BalanceTransferStatusEnum, bool> allocationImp, Func<IList<Payment>, IList<PaymentAllocationResult>, BalanceTransferStatusEnum, bool> allocationsImp, BalanceTransferStatusEnum btStatus = BalanceTransferStatusEnum.Eligible)
        {
            builder.Setup((x) =>
            {
                PaymentAllocationResult result = new PaymentAllocationResult();
                x.PaymentAllocationServiceMock.Setup(t => t.AllocatePayment(It.IsAny<Payment>())).Callback((Payment callbackPayment) =>
                {
                    allocationImp(callbackPayment, result, btStatus);
                }).Returns(result);
                IList<PaymentAllocationResult> results = new List<PaymentAllocationResult>();
                x.PaymentAllocationServiceMock.Setup(t => t.AllocatePayments(It.IsAny<IList<Payment>>())).Callback((IList<Payment> callbackPayments) =>
                {
                    allocationsImp(callbackPayments, results, btStatus);
                }).Returns(results);
            });
        }

        [Test]
        public void HandlesSuccess_PaymentProviderReturnsSuccessCard()
        {
            PaymentServiceMockBuilder builder = new PaymentServiceMockBuilder();
            Payment payment = this.CreatePayment(123m);
            builder.Setup((x) =>
            {
                x.PaymentProviderMock.Setup(t => t.SubmitPaymentsAsync(It.IsAny<IList<Payment>>(), It.IsAny<PaymentProcessor>()))
                    .Returns<IList<Payment>, PaymentProcessor>((ps, pp) => this.CreatePaymentProcessorResponse(ps, pp, PaymentProcessorResponseStatusEnum.Approved));

                x.MerchantAccountAllocationServiceMock.Setup(t => t.SplitPayments(It.IsAny<IList<Payment>>())).Returns<IList<Payment>>(paymentEntities => paymentEntities);
            });
            this.SetupBasicVisitAndAllocationWithFunction(builder,
                (callbackPayment, result, status) =>
                {
                    result.Allocations.Clear();
                    result.ActualPaymentAmount = 123m;
                    result.Payment = callbackPayment;
                    result.ActualPaymentDate = DateTime.UtcNow;
                    Tuple<Visit, IList<VisitTransaction>> visit = VisitFactory.GenerateActiveVisit(123m, 2);
                    visit.Item1.VPGuarantor = new Guarantor();
                    result.Allocations.Add(new PaymentAllocation
                    {
                        ActualAmount = 123m,
                        PaymentAllocationStatus = PaymentAllocationStatusEnum.PendingHsAcknowledgement,
                        Payment = callbackPayment,
                        PaymentVisit = Mapper.Map<PaymentVisit>(visit)
                    });
                    
                    return true;
                }
                ,
                (callbackPayments, results, status) =>
                {
                    results.Clear();
                    foreach (Payment callbackPayment in callbackPayments)
                    {
                        PaymentAllocationResult localResult = new PaymentAllocationResult();

                        localResult.Allocations.Clear();
                        localResult.ActualPaymentAmount = 123m;
                        localResult.Payment = callbackPayment;
                        localResult.ActualPaymentDate = DateTime.UtcNow;
                        Tuple<Visit, IList<VisitTransaction>> visit = VisitFactory.GenerateActiveVisit(123m, 2);
                        visit.Item1.VPGuarantor = new Guarantor();
                        localResult.Allocations.Add(new PaymentAllocation
                        {
                            ActualAmount = 123m,
                            PaymentAllocationStatus = PaymentAllocationStatusEnum.PendingHsAcknowledgement,
                            Payment = callbackPayment,
                            PaymentVisit = Mapper.Map<PaymentVisit>(visit.Item1)
                        });
                        results.Add(localResult);
                    }
                    return true;
                }
                );

            PaymentService paymentService = builder.CreatePaymentService();
            int? visitPayUserId = 81; //statement Guy
            bool success = paymentService.ChargePayment(payment, visitPayUserId).First().Success;

            Assert.AreEqual(true, success);
            Assert.AreEqual(payment.PaymentStatus, PaymentStatusEnum.ClosedPaid);
            //Should have added an additional transaction from the distribution.
            Assert.AreEqual(payment.PaymentAllocations.Sum(x => x.ActualAmount), 123m);
        }

        /*[Test]
        [Ignore("Not Working" + " " + ObsoleteDescription.VisitStateMachineTest)]
        public void HandlesSuccess_PaymentTakesBalanceToZero()
        {
            PaymentServiceMockBuilder builder = new PaymentServiceMockBuilder();
            decimal amount = 1234m;
            Tuple<Visit, IList<VisitTransaction>> visit = VisitFactory.GenerateActiveVisit(amount, 1);
            visit.Item1.VPGuarantor = new Guarantor();
            //Assert.AreNotEqual(visit.VisitState.VisitStateId, (int) VisitStateEnum.PendingClosure);
            builder.Setup((x) =>
            {
                x.MerchantAccountAllocationServiceMock.Setup(t => t.SplitPayments(It.IsAny<IList<Payment>>())).Returns<IList<Payment>>(paymentEntities => paymentEntities);

                x.PaymentProviderMock.Setup(t => t.SubmitPaymentsAsync(It.IsAny<IList<Payment>>(), It.IsAny<PaymentProcessor>()))
                    .Returns<IList<Payment>, PaymentProcessor>((ps, pp) => this.CreatePaymentProcessorResponse(ps, pp, PaymentProcessorResponseStatusEnum.Approved));

                x.PaymentAllocationServiceMock.Setup(t => t.AllocatePayment(It.IsAny<Payment>())).Callback((Payment callbackPayment) =>
                {
                    callbackPayment.PaymentAllocations.Add(new PaymentAllocation
                    {
                        ActualAmount = amount,
                        PaymentAllocationStatus = PaymentAllocationStatusEnum.PendingHsAcknowledgement,
                        Payment = callbackPayment,
                        PaymentVisit = Mapper.Map<PaymentVisit>(visit)//new PaymentVisit() { VisitId = visit.VisitId, VisitState = visit.VisitState.VisitStateEnum }
                    });
                });
            });
            Payment payment = this.CreatePayment(amount);
            payment.ActualPaymentAmount = amount;
            builder.StatusServiceOverride = new VisitStateService(
                new Lazy<IDomainServiceCommonService>(() => builder.DomainServiceCommonService),
                new Lazy<IVisitRepository>(() => builder.VisitRepositoryMock.Object),
                new Lazy<IVisitStateMachineService>(() => builder.VisitStateMachineService.Object));

            // List<VisitPayment> visitPayments1 = new Visit[] {visit}.Select(visit1 => new VisitPayment() {Visit = visit, PaymentAmount = amount}).ToList();
            PaymentService paymentService = builder.CreatePaymentService();

            int? visitPayUserId = 81; //statement Guy
            bool success = paymentService.ChargePayment(payment, visitPayUserId).First().Success;

            Assert.AreEqual(true, success);
            //Assert.AreEqual(visit.VisitState.VisitStateId, (int) VisitStateEnum.PendingClosure);
        }*/

        private Task<IList<PaymentProcessorResponse>> CreatePaymentProcessorResponse(IEnumerable<Payment> ps, PaymentProcessor pp, PaymentProcessorResponseStatusEnum paymentProcessorResponseStatus)
        {
            PaymentProcessorResponse paymentProcessorResponse = new PaymentProcessorResponse()
            {
                PaymentProcessorResponseStatus = paymentProcessorResponseStatus,
                PaymentProcessorSourceKey = RandomSourceSystemKeyGenerator.New("999-NNNNNNNNNN"),
                PaymentSystemType = PaymentSystemTypeEnum.VisitPay
            };

            foreach (Payment p in ps)
            {
                if (paymentProcessorResponse.PaymentProcessorResponseStatus == PaymentProcessorResponseStatusEnum.Approved && p.PaymentMethod.IsAchType)
                {
                    paymentProcessorResponse.PaymentProcessorResponseStatus = PaymentProcessorResponseStatusEnum.Accepted;
                }

                PaymentPaymentProcessorResponse current = new PaymentPaymentProcessorResponse
                {
                    InsertDate = DateTime.UtcNow,
                    Payment = p,
                    PaymentProcessorResponse = paymentProcessorResponse,
                    SnapshotPaymentAmount = p.ActualPaymentAmount
                };
                p.PaymentPaymentProcessorResponses.Add(current);
                paymentProcessorResponse.PaymentPaymentProcessorResponses.Add(current);
            }
            IList<PaymentProcessorResponse> paymentRespList = new List<PaymentProcessorResponse> {paymentProcessorResponse};
            return Task.FromResult(paymentRespList);
        }

        [Ignore("Enable pipeline while we work on fixing this area of code")]
        [Test]
        public void ReducePastDuePaymentAmountWhenPaymentComesFromHS_UpdatePaymentBucketsForFinancePlans()
        {
            //VPNG-15462
            //Baiscally the goal here is that we have 2 pending payments, after calling UpdatePaymentBucketsForFinancePlans, one of the payments should go to 0.

            Guarantor theGuarantor = GuarantorFactory.GenerateOnline();
            
            List<FinancePlan> financePlans = new List<FinancePlan>();
            List<VisitTransaction> visitTransactions = new List<VisitTransaction>();
            List<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>();

            Tuple<Visit, IList<VisitTransaction>> visitInQuestion = VisitFactory.GenerateActiveVisit(100, 1, VisitStateEnum.Active, guarantor: theGuarantor);
            VisitTransaction visitTransactionThatWasAdded = VisitTransactionFactory.GenerateVisitTransaction(-10);
            visitTransactionThatWasAdded.VpTransactionType = new VpTransactionType() {VpTransactionTypeId = (int)VpTransactionTypeEnum.HsPatientCash};
            //visitInQuestion.AddVisitTransaction(visitTransactionThatWasAdded);
            visitTransactions.Add(visitTransactionThatWasAdded);
            visits.Add(visitInQuestion);
            visits.Add(VisitFactory.GenerateActiveVisit(100, 1, VisitStateEnum.Active, guarantor: theGuarantor));
            visits.Add(VisitFactory.GenerateActiveVisit(100, 1, VisitStateEnum.Active, guarantor: theGuarantor));

            FinancePlan financePlan = FinancePlanFactory.GenerateWithStatus(visits, new FinancePlanOffer(), DateTime.Today, new VpStatement(), 10m, FinancePlanStatusEnum.PastDue);
            financePlan.AddFinancePlanAmountsDueOffset(10, DateTime.UtcNow.AddDays(-60), 0);
            financePlan.AddFinancePlanAmountsDueOffset(10, DateTime.UtcNow.AddDays(-30), 0);
            financePlans.Add(financePlan);


            List<Payment> recurringPayments = new List<Payment>();
            recurringPayments.Add(this.CreatePendingPaymentForFinancePlan(10m, financePlan, PaymentMethodTypeEnum.Visa, DateTime.UtcNow.AddDays(-60)));
            recurringPayments.Add(this.CreatePendingPaymentForFinancePlan(10m, financePlan, PaymentMethodTypeEnum.Visa, DateTime.UtcNow.AddDays(-30)));

            PaymentServiceMockBuilder builder = new PaymentServiceMockBuilder();
            builder.Setup((x) =>
            {
                x.PaymentRepositoryMock.Setup(y => y.GetPaymentsFromPaymentAllocationIds(It.IsAny<IList<int>>())).Returns(recurringPayments);
            });
            
            // Took a stab at making this work (was tagged with igonre)  
            // added some paymentRepositoryMock.Setups just to get it to fail here 
            // don't have time to finish at the moment - Damian
            Assert.AreEqual(0m, recurringPayments.OrderBy(x => x.InsertDate).First().ScheduledPaymentAmount);
        }

        private readonly Dictionary<PaymentTypeEnum, PaymentTypeEnum> _sameDayPaymentTypeEnumLookup = new Dictionary<PaymentTypeEnum, PaymentTypeEnum>
        {
            [PaymentTypeEnum.ManualScheduledCurrentBalanceInFull] = PaymentTypeEnum.ManualPromptCurrentBalanceInFull,
            [PaymentTypeEnum.ManualScheduledCurrentNonFinancedBalanceInFull] = PaymentTypeEnum.ManualPromptCurrentNonFinancedBalanceInFull,
            [PaymentTypeEnum.ManualScheduledSpecificAmount] = PaymentTypeEnum.ManualPromptSpecificAmount,
            [PaymentTypeEnum.ManualScheduledSpecificFinancePlans] = PaymentTypeEnum.ManualPromptSpecificFinancePlans,
            [PaymentTypeEnum.ManualScheduledSpecificVisits] = PaymentTypeEnum.ManualPromptSpecificVisits
        };

        [TestCase(PaymentTypeEnum.ManualPromptSpecificAmount)]
        [TestCase(PaymentTypeEnum.ManualPromptSpecificVisits)]
        [TestCase(PaymentTypeEnum.ManualScheduledSpecificAmount)]
        [TestCase(PaymentTypeEnum.ManualScheduledSpecificVisits)]
        [TestCase(PaymentTypeEnum.ManualPromptSpecificFinancePlans)]
        [TestCase(PaymentTypeEnum.ManualPromptCurrentBalanceInFull)]
        [TestCase(PaymentTypeEnum.ManualScheduledSpecificFinancePlans)]
        [TestCase(PaymentTypeEnum.ManualScheduledCurrentBalanceInFull)]
        [TestCase(PaymentTypeEnum.RecurringPaymentFinancePlan)]
        [TestCase(PaymentTypeEnum.ManualPromptCurrentNonFinancedBalanceInFull)]
        [TestCase(PaymentTypeEnum.ManualScheduledCurrentNonFinancedBalanceInFull)]
        [TestCase(PaymentTypeEnum.Void)]
        [TestCase(PaymentTypeEnum.Refund)]
        [TestCase(PaymentTypeEnum.PartialRefund)]
        public void AdjustPaymentType_with_scheduled_date_of_today(PaymentTypeEnum paymentTypeEnum)
        {
            bool isInDictionary = this._sameDayPaymentTypeEnumLookup.ContainsKey(paymentTypeEnum);
            PaymentTypeEnum expectedPaymentTypeEnum = (isInDictionary)?this._sameDayPaymentTypeEnumLookup[paymentTypeEnum]: paymentTypeEnum;

            PaymentServiceMockBuilder mockBuilder = new PaymentServiceMockBuilder();
            PaymentService paymentService = mockBuilder.CreatePaymentService();

            DateTime paymentDate = mockBuilder.TimeZoneHelper.UtcToClient(DateTime.UtcNow);

            PaymentTypeEnum actualPaymentTypeEnum = paymentService.AdjustPaymentType(paymentTypeEnum, paymentDate);

            Assert.AreEqual(expectedPaymentTypeEnum,actualPaymentTypeEnum);
        }
        
        [TestCase(PaymentTypeEnum.ManualPromptSpecificAmount)]
        [TestCase(PaymentTypeEnum.ManualPromptSpecificVisits)]
        [TestCase(PaymentTypeEnum.ManualScheduledSpecificAmount)]
        [TestCase(PaymentTypeEnum.ManualScheduledSpecificVisits)]
        [TestCase(PaymentTypeEnum.ManualPromptSpecificFinancePlans)]
        [TestCase(PaymentTypeEnum.ManualPromptCurrentBalanceInFull)]
        [TestCase(PaymentTypeEnum.ManualScheduledSpecificFinancePlans)]
        [TestCase(PaymentTypeEnum.ManualScheduledCurrentBalanceInFull)]
        [TestCase(PaymentTypeEnum.RecurringPaymentFinancePlan)]
        [TestCase(PaymentTypeEnum.ManualPromptCurrentNonFinancedBalanceInFull)]
        [TestCase(PaymentTypeEnum.ManualScheduledCurrentNonFinancedBalanceInFull)]
        [TestCase(PaymentTypeEnum.Void)]
        [TestCase(PaymentTypeEnum.Refund)]
        [TestCase(PaymentTypeEnum.PartialRefund)]
        public void AdjustPaymentType_with_scheduled_date_gt_today(PaymentTypeEnum paymentTypeEnum)
        {
            //basically, inverse the dictionary
            Dictionary<PaymentTypeEnum, PaymentTypeEnum> futureScheduledPaymentTypeEnumLookup = this._sameDayPaymentTypeEnumLookup.ToDictionary(x => x.Value, x => x.Key);

            bool isInDictionary = futureScheduledPaymentTypeEnumLookup.ContainsKey(paymentTypeEnum);
            PaymentTypeEnum expectedPaymentTypeEnum = (isInDictionary) ? futureScheduledPaymentTypeEnumLookup[paymentTypeEnum] : paymentTypeEnum;
            
            PaymentServiceMockBuilder mockBuilder = new PaymentServiceMockBuilder();
            PaymentService paymentService = mockBuilder.CreatePaymentService();
            
            DateTime paymentDate = mockBuilder.TimeZoneHelper.UtcToClient(DateTime.UtcNow.AddDays(1));

            PaymentTypeEnum actualPaymentTypeEnum = paymentService.AdjustPaymentType(paymentTypeEnum, paymentDate);

            Assert.AreEqual(expectedPaymentTypeEnum, actualPaymentTypeEnum);
        }
        
        [TestCase(PaymentTypeEnum.ManualPromptSpecificAmount)]
        [TestCase(PaymentTypeEnum.ManualPromptSpecificVisits)]
        [TestCase(PaymentTypeEnum.ManualScheduledSpecificAmount)]
        [TestCase(PaymentTypeEnum.ManualScheduledSpecificVisits)]
        [TestCase(PaymentTypeEnum.ManualPromptSpecificFinancePlans)]
        [TestCase(PaymentTypeEnum.ManualPromptCurrentBalanceInFull)]
        [TestCase(PaymentTypeEnum.ManualScheduledSpecificFinancePlans)]
        [TestCase(PaymentTypeEnum.ManualScheduledCurrentBalanceInFull)]
        [TestCase(PaymentTypeEnum.RecurringPaymentFinancePlan)]
        [TestCase(PaymentTypeEnum.ManualPromptCurrentNonFinancedBalanceInFull)]
        [TestCase(PaymentTypeEnum.ManualScheduledCurrentNonFinancedBalanceInFull)]
        [TestCase(PaymentTypeEnum.Void)]
        [TestCase(PaymentTypeEnum.Refund)]
        [TestCase(PaymentTypeEnum.PartialRefund)]
        public void AdjustPaymentType_with_scheduled_in_the_past(PaymentTypeEnum paymentTypeEnum)
        {
            PaymentServiceMockBuilder mockBuilder = new PaymentServiceMockBuilder();
            PaymentService paymentService = mockBuilder.CreatePaymentService();
            
            DateTime paymentDate = mockBuilder.TimeZoneHelper.UtcToClient(DateTime.UtcNow.AddDays(-1));

            PaymentTypeEnum actualPaymentTypeEnum = paymentService.AdjustPaymentType(paymentTypeEnum, paymentDate);
            Assert.AreEqual(paymentTypeEnum, actualPaymentTypeEnum);
        }
    }
}
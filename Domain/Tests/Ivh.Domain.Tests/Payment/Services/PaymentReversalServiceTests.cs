﻿namespace Ivh.Domain.Payment.Services
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Base.Interfaces;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.Data.Connection;
    using Common.Data.Connection.Connections;
    using Common.ServiceBus.Interfaces;
    using Common.Tests.Helpers.Base;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Ivh.Domain.Guarantor.Entities;
    using FinanceManagement.Entities;
    using FinanceManagement.FinancePlan.Interfaces;
    using FinanceManagement.Interfaces;
    using FinanceManagement.Payment.Entities;
    using FinanceManagement.Payment.Interfaces;
    using FinanceManagement.Services;
    using FinanceManagement.Statement.Entities;
    using FinanceManagement.Statement.Interfaces;
    using Logging.Interfaces;
    using Moq;
    using NHibernate;
    using NUnit.Framework;
    using Settings.Entities;
    using Settings.Interfaces;
    using User.Entities;
    using Ivh.Domain.FinanceManagement.Ach.Interfaces;

    [TestFixture]
    public class PaymentReversalServiceTests : DomainTestBase
    {
        [Obsolete(ObsoleteDescription.VisitTransactionVisitPayDiscount)]
        [SetUp]
        public void BeforeEachTest()
        {
            this._serviceBusMock = new Mock<IBus>();
            this._paymentRepositoryMock = new Mock<IPaymentRepository>();
            this._paymentEventServiceMock = new Mock<IPaymentEventService>();
            this._paymentReversalProviderMock = new Mock<IPaymentReversalProvider>();
            this._paymentProcessorRepositoryMock = new Mock<IPaymentProcessorRepository>();
            this._paymentProcessorResponseRepositoryMock = new Mock<IPaymentProcessorResponseRepository>();
            this._paymentAllocationServiceMock = new Mock<IPaymentAllocationService>();
            this._paymentAllocationServiceMock.Setup(x => x.AllocatePayment(It.IsAny<Payment>())).Returns(new PaymentAllocationResult());
            this._paymentAllocationServiceMock.Setup(x => x.AllocatePayments(It.IsAny<IList<Payment>>())).Returns(new List<PaymentAllocationResult>());
            //this._paymentAllocationServiceMock.Setup(x => x.AllocatePaymentReverse(It.IsAny<Payment>(), It.IsAny<Payment>())).Returns(new PaymentAllocationResult());
            this._applicationSettingsServiceMock = new Mock<IApplicationSettingsService>();
            this._paymentProviderMock = new Mock<IPaymentProvider>();
            this._vpStatementService = new Mock<IVpStatementService>();
            this._financePlanService = new Mock<IFinancePlanService>();
            this._paymentMethodsService = new Mock<IPaymentMethodsService>();
            this._achService = new Mock<IAchService>();
            this._session = new Mock<ISession>();
            this._loggingService = new Mock<ILoggingService>();

            this._clientServiceMock = new Mock<IClientService>();
            Mock<Client> clientMock = new Mock<Client>();
            TimeZoneHelper timeZoneHelper = new TimeZoneHelper(string.Empty);

            DateTime settlementWindowBeginAch = DateTime.Parse("2000-01-01T17:30:00.000000");
            settlementWindowBeginAch = timeZoneHelper.Client.ToUniversalDateTime(settlementWindowBeginAch);
            clientMock.Setup(t => t.SettlementWindowBeginAch).Returns(settlementWindowBeginAch);

            DateTime settlementWindowEndAch = DateTime.Parse("2000-01-01T18:00:00.000000");
            settlementWindowEndAch = timeZoneHelper.Client.ToUniversalDateTime(settlementWindowEndAch);
            clientMock.Setup(t => t.SettlementWindowEndAch).Returns(settlementWindowEndAch);

            DateTime settlementWindowBeginCc = DateTime.Parse("2000-01-01T19:59:00.000000");
            settlementWindowBeginCc = timeZoneHelper.Client.ToUniversalDateTime(settlementWindowBeginCc);
            clientMock.Setup(t => t.SettlementWindowBeginCc).Returns(settlementWindowBeginCc);

            DateTime settlementWindowEndCc = DateTime.Parse("2000-01-01T20:30:00.000000");
            settlementWindowEndCc = timeZoneHelper.Client.ToUniversalDateTime(settlementWindowEndCc);
            clientMock.Setup(t => t.SettlementWindowEndCc).Returns(settlementWindowEndCc);

            DateTime settlementWindowBeginCcRaw = DateTime.Parse("2000-01-01T19:59:00.000000");
            clientMock.Setup(t => t.SettlementWindowBeginCcRaw).Returns(settlementWindowBeginCcRaw);


            this._clientServiceMock.Setup(t => t.GetClient()).Returns(clientMock.Object);
            this._vpStatementService.Setup(t => t.GetMostRecentStatement(It.IsAny<Guarantor>())).Returns(new VpStatement());
            this._paymentProcessorRepositoryMock.Setup(t => t.GetByName(It.IsAny<string>())).Returns(() => new PaymentProcessor());
            this._paymentReversalProviderMock.Setup(t => t.SubmitPaymentsVoidAsync(It.IsAny<IList<Payment>>(), It.IsAny<IList<Payment>>(), It.IsAny<PaymentProcessor>())).Returns(() => Task.FromResult(new PaymentProcessorResponse
            {
                PaymentProcessorResponseStatus = PaymentProcessorResponseStatusEnum.Accepted,
                PaymentSystemType = PaymentSystemTypeEnum.VisitPay
            }));
            this._paymentReversalProviderMock.Setup(t => t.SubmitPaymentsRefundAsync(It.IsAny<IList<Payment>>(), It.IsAny<IList<Payment>>(), It.IsAny<PaymentProcessor>(), It.IsAny<decimal?>())).Returns(() => Task.FromResult(new PaymentProcessorResponse { PaymentProcessorResponseStatus = PaymentProcessorResponseStatusEnum.Accepted }));

            PaymentAllocationResult result = new PaymentAllocationResult();
            this._paymentAllocationServiceMock.Setup(t => t.AllocatePaymentReverse(It.IsAny<Payment>(), It.IsAny<Payment>())).Callback<Payment, Payment>((o, r) =>
            {
                result.Payment = r;
                result.ActualPaymentAmount = r.ScheduledPaymentAmount;
                result.ActualPaymentDate = DateTime.UtcNow;

            }).Returns(result);

            this._domainServiceCommonService = DomainServiceCommonServices.CreateDomainServiceCommonService(
                applicationSettingsServiceMock: this._applicationSettingsServiceMock,
                featureServiceMock: null,
                clientServiceMock: this._clientServiceMock,
                loggingServiceMock: this._loggingService,
                busMock: this._serviceBusMock,
                cacheMock: null,
                timeZoneHelperMock: null);
        }

        private Mock<IPaymentRepository> _paymentRepositoryMock;
        private Mock<IPaymentEventService> _paymentEventServiceMock;
        private Mock<IPaymentReversalProvider> _paymentReversalProviderMock;
        private Mock<IPaymentProcessorRepository> _paymentProcessorRepositoryMock;
        private Mock<IPaymentProcessorResponseRepository> _paymentProcessorResponseRepositoryMock;
        private Mock<IPaymentAllocationService> _paymentAllocationServiceMock;
        private Mock<IPaymentProvider> _paymentProviderMock;
        private Mock<IClientService> _clientServiceMock;
        private Mock<IBus> _serviceBusMock;
        private Mock<IApplicationSettingsService> _applicationSettingsServiceMock;
        private Mock<IVpStatementService> _vpStatementService;
        private Mock<IFinancePlanService> _financePlanService;
        private Mock<IPaymentMethodsService> _paymentMethodsService;
        private Mock<ISession> _session;
        private Mock<ILoggingService> _loggingService;
        private Mock<IAchService> _achService;
        private IDomainServiceCommonService _domainServiceCommonService;
        
        private IList<Payment> CreateBasicPayments(decimal amount, DateTime paymentDateTime, PaymentMethodTypeEnum paymentMethodType, PaymentStatusEnum paymentStatus)
        {
            Payment payment = new Payment
            {
                PaymentId = new Random().Next(10000, 100000),
                Guarantor = new Guarantor(),
                PaymentMethod = new PaymentMethod { PaymentMethodType = paymentMethodType },
                ActualPaymentDate = paymentDateTime,
                ActualPaymentAmount = amount,
                PaymentType = PaymentTypeEnum.ManualPromptSpecificVisits,
                PaymentStatus = paymentStatus
            };

            payment.PaymentProcessorResponses.Add(new PaymentProcessorResponse
            {
                PaymentProcessorSourceKey = RandomSourceSystemKeyGenerator.New("NNNNNNNNNN"),
                PaymentSystemType = PaymentSystemTypeEnum.VisitPay
            });

            return new List<Payment> { payment };
        }

        private PaymentReversalService CreatePaymentReversalService()
        {
            Mock<ITransaction> transaction = new Mock<ITransaction>();
            transaction.Setup(x => x.IsActive).Returns(true);
            this._session.Setup(x => x.IsConnected).Returns(true);
            this._session.Setup(x => x.IsOpen).Returns(true);
            this._session.Setup(x => x.BeginTransaction(It.IsAny<System.Data.IsolationLevel>())).Returns(transaction.Object);

            return new PaymentReversalService(
                new Lazy<IDomainServiceCommonService>(() => this._domainServiceCommonService),
                new Lazy<IPaymentRepository>(() => this._paymentRepositoryMock.Object),
                new Lazy<IPaymentEventService>(() => this._paymentEventServiceMock.Object),
                new Lazy<IPaymentReversalProvider>(() => this._paymentReversalProviderMock.Object),
                new Lazy<IPaymentProcessorRepository>(() => this._paymentProcessorRepositoryMock.Object),
                new Lazy<IPaymentProcessorResponseRepository>(() => this._paymentProcessorResponseRepositoryMock.Object),
                new Lazy<IPaymentAllocationService>(() => this._paymentAllocationServiceMock.Object),
                new Lazy<IPaymentProvider>(() => this._paymentProviderMock.Object),
                new Lazy<IPaymentMethodsService>(() => this._paymentMethodsService.Object),
                new Lazy<IFinancePlanService>(() => this._financePlanService.Object),
                new Lazy<IAchService>(() => this._achService.Object),
                new SessionContext<VisitPay>(this._session.Object));
        }

        [Test]
        public void PaymentCanBeRefundedCc()
        {
            DateTime paymentDateTime = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 19, 58, 30).ToUniversalTime(); // TODAY at 7:58:30 PM GMT-7
            paymentDateTime = paymentDateTime.AddDays(-1); // YESTERDAY at 7:58:30 PM GMT-7
            IList<Payment> payments = this.CreateBasicPayments(100m, paymentDateTime, PaymentMethodTypeEnum.Visa, PaymentStatusEnum.ClosedPaid);
            PaymentReversalService paymentReversalService = this.CreatePaymentReversalService();
            paymentReversalService.Now = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 12, 00, 00).ToUniversalTime(); // NOW is 12:00:00 PM GMT-7
            PaymentReversalStatusEnum paymentReversalStatusEnum = paymentReversalService.GetPaymentReversalStatus(payments);
            Assert.AreEqual(PaymentReversalStatusEnum.Refundable, paymentReversalStatusEnum);
        }

        [Test]
        public void PaymentCanBeVoidedAch()
        {
            DateTime paymentDateTime = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 16, 29, 00); // TODAY at 5:29:00 PM GMT-7
            paymentDateTime = DateTime.SpecifyKind(paymentDateTime, DateTimeKind.Utc);
            IList<Payment> payments = this.CreateBasicPayments(100m, paymentDateTime, PaymentMethodTypeEnum.AchChecking, PaymentStatusEnum.ActivePendingACHApproval);
            PaymentReversalService paymentReversalService = this.CreatePaymentReversalService();
            paymentReversalService.Now = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 17, 00, 00); // NOW is 5:45:00 PM GMT-7
            paymentReversalService.Now = DateTime.SpecifyKind(paymentReversalService.Now, DateTimeKind.Utc);
            PaymentReversalStatusEnum paymentReversalStatusEnum = paymentReversalService.GetPaymentReversalStatus(payments);

            Assert.AreEqual(PaymentReversalStatusEnum.Voidable, paymentReversalStatusEnum);
        }

        [Test]
        public void PaymentCanBeVoidedAchInWindow()
        {
            DateTime paymentDateTime = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 17, 31, 00); // TODAY at 5:31:00 PM GMT-7
            paymentDateTime = DateTime.SpecifyKind(paymentDateTime, DateTimeKind.Utc);
            IList<Payment> payment = this.CreateBasicPayments(100m, paymentDateTime, PaymentMethodTypeEnum.AchChecking, PaymentStatusEnum.ActivePendingACHApproval);
            PaymentReversalService paymentReversalService = this.CreatePaymentReversalService();
            paymentReversalService.Now = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 17, 45, 00); // NOW is 5:45:00 PM GMT-7
            paymentReversalService.Now = DateTime.SpecifyKind(paymentReversalService.Now, DateTimeKind.Utc);
            PaymentReversalStatusEnum paymentReversalStatusEnum = paymentReversalService.GetPaymentReversalStatus(payment);

            Assert.AreEqual(PaymentReversalStatusEnum.Voidable, paymentReversalStatusEnum);
        }

        public class PaymentReversalTestCase
        {
            public DateTime PaymentDateTime { get; set; }
            public DateTime ReversalDateTime { get; set; }
            public PaymentReversalStatusEnum ExpectedPaymentReversalStatusEnum { get; set; }
            public string TestCaseName { get; set; }

            public override string ToString()
            {
                return this.TestCaseName;
            }
        }

        public static IEnumerable<PaymentReversalTestCase> PaymentVoidTestCases {
            get
            {
                DateTime today = new DateTime(2018, 10, 8).AddHours(7); //mountain time
                DateTime settlementBegin = today.AddHours(19).AddMinutes(59);
                DateTime settlementEnd = today.AddHours(20).AddMinutes(30);

                yield return new PaymentReversalTestCase {
                    PaymentDateTime = settlementBegin.AddMinutes(-5),
                    ReversalDateTime = settlementBegin.AddMinutes(5),
                    ExpectedPaymentReversalStatusEnum = PaymentReversalStatusEnum.Refundable,
                    TestCaseName = "Payment made before settlement begins cannot be voided after settlement begins"
                };

                yield return new PaymentReversalTestCase
                {
                    PaymentDateTime = settlementBegin.AddMinutes(5),
                    ReversalDateTime = settlementBegin.AddMinutes(10),
                    ExpectedPaymentReversalStatusEnum = PaymentReversalStatusEnum.Voidable,
                    TestCaseName = "Payment made in settlement window can be voided after settlement begins"
                };

                /* TODO: this fails. not sure if it's the test or the code
                yield return new PaymentReversalTestCase
                {
                    PaymentDateTime = settlementEnd.AddMinutes(-5),
                    ReversalDateTime = settlementEnd.AddMinutes(5),
                    ExpectedPaymentReversalStatusEnum = PaymentReversalStatusEnum.Voidable,
                    TestCaseName = "Payment made in settlement window can be voided after settlement ends"
                };
                */
                yield return new PaymentReversalTestCase
                {
                    PaymentDateTime = settlementEnd.AddMinutes(5),
                    ReversalDateTime = settlementEnd.AddMinutes(10),
                    ExpectedPaymentReversalStatusEnum = PaymentReversalStatusEnum.Voidable,
                    TestCaseName = "Payment made after settlement window can be voided"
                };
            } 
        }

        [TestCaseSource(nameof(PaymentVoidTestCases))]
        public void PaymentCanBeVoidedCc(PaymentReversalTestCase testCase)
        {
            
            DateTime paymentDateTime = testCase.PaymentDateTime;
            IList<Payment> payments = this.CreateBasicPayments(100m, paymentDateTime, PaymentMethodTypeEnum.Visa, PaymentStatusEnum.ClosedPaid);
            PaymentReversalService paymentReversalService = this.CreatePaymentReversalService();
            paymentReversalService.Now = testCase.ReversalDateTime;
            PaymentReversalStatusEnum paymentReversalStatusEnum = paymentReversalService.GetPaymentReversalStatus(payments);

            Assert.AreEqual(testCase.ExpectedPaymentReversalStatusEnum, paymentReversalStatusEnum);
        }

        [Test]
        public void PaymentCannotBeVoidedAch()
        {
            DateTime paymentDateTime = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 17, 31, 00); // TODAY at 5:31:00 PM GMT-7
            IList<Payment> payments = this.CreateBasicPayments(100m, paymentDateTime, PaymentMethodTypeEnum.AchChecking, PaymentStatusEnum.ClosedPaid);
            PaymentReversalService paymentReversalService = this.CreatePaymentReversalService();
            PaymentReversalStatusEnum paymentReversalStatusEnum = paymentReversalService.GetPaymentReversalStatus(payments);

            Assert.AreNotEqual(PaymentReversalStatusEnum.Voidable, paymentReversalStatusEnum);
        }

        //This unit test seems to fail after 9 mst
        public void PaymentCannotBeVoidedCc()
        {
            DateTime paymentDateTime = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 20, 0, 30); // Yesterday at 8:00:30 PM GMT-7
            IList<Payment> payments = this.CreateBasicPayments(100m, paymentDateTime, PaymentMethodTypeEnum.Visa, PaymentStatusEnum.ClosedPaid);
            PaymentReversalService paymentReversalService = this.CreatePaymentReversalService();
            paymentReversalService.Now = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 17, 45, 00); // NOW is 5:45:00 PM GMT-7
            PaymentReversalStatusEnum paymentReversalStatusEnum = paymentReversalService.GetPaymentReversalStatus(payments);

            Assert.AreNotEqual(PaymentReversalStatusEnum.Voidable, paymentReversalStatusEnum);
        }

        [Test]
        public void RefundFullSuccessful()
        {
            DateTime paymentDateTime = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 19, 58, 30); // TODAY at 7:58:30 PM GMT-7
            paymentDateTime = paymentDateTime.AddDays(-2);
            IList<Payment> originalPayments = this.CreateBasicPayments(100m, paymentDateTime, PaymentMethodTypeEnum.Visa, PaymentStatusEnum.ClosedPaid);
            PaymentReversalService paymentReversalService = this.CreatePaymentReversalService();
            PaymentReversalStatusEnum paymentReversalStatusEnum = paymentReversalService.GetPaymentReversalStatus(originalPayments);

            Assert.AreEqual(PaymentReversalStatusEnum.Refundable, paymentReversalStatusEnum);

            IList<Payment> payments = paymentReversalService.RefundPaymentAsync(originalPayments, new VisitPayUser { VisitPayUserId = SystemUsers.SystemUserId }, null).Result;
            Assert.IsTrue(payments.All(x => x.PaymentStatus == PaymentStatusEnum.ClosedPaid));
        }

        public void RefundPartialSuccessful()
        {
            DateTime paymentDateTime = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 19, 58, 30); // TODAY at 7:58:30 PM GMT-7
            paymentDateTime = paymentDateTime.AddDays(-2);
            IList<Payment> originalPayments = this.CreateBasicPayments(100m, paymentDateTime, PaymentMethodTypeEnum.Visa, PaymentStatusEnum.ClosedPaid);
            PaymentReversalService paymentReversalService = this.CreatePaymentReversalService();
            PaymentReversalStatusEnum paymentReversalStatusEnum = paymentReversalService.GetPaymentReversalStatus(originalPayments);

            Assert.AreEqual(PaymentReversalStatusEnum.Refundable, paymentReversalStatusEnum);

            IList<Payment> payments = paymentReversalService.RefundPaymentAsync(originalPayments, new VisitPayUser { VisitPayUserId = SystemUsers.SystemUserId }, originalPayments.Sum(x => x.ActualPaymentAmount) / 2).Result;
            CollectionAssert.AllItemsAreNotNull(payments);
            Assert.AreEqual(originalPayments.SelectMany(x => x.ReversalPayments).Sum(x => x.ActualPaymentAmount), originalPayments.Sum(x => x.ActualPaymentAmount) / -2);
            Assert.IsTrue(payments.All(x => x.PaymentStatus == PaymentStatusEnum.ClosedPaid));
        }

        public void RefundPartialRoundingEqualPayments()
        {
            DateTime paymentDateTime = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 19, 58, 30); // TODAY at 7:58:30 PM GMT-7
            paymentDateTime = paymentDateTime.AddDays(-2);

            IList<Payment> originalPayments = new List<Payment>();
            originalPayments.AddRange(this.CreateBasicPayments(100m, paymentDateTime, PaymentMethodTypeEnum.Visa, PaymentStatusEnum.ClosedPaid));
            originalPayments.AddRange(this.CreateBasicPayments(100m, paymentDateTime, PaymentMethodTypeEnum.Visa, PaymentStatusEnum.ClosedPaid));
            originalPayments.AddRange(this.CreateBasicPayments(100m, paymentDateTime, PaymentMethodTypeEnum.Visa, PaymentStatusEnum.ClosedPaid));
            
            PaymentReversalService paymentReversalService = this.CreatePaymentReversalService();
            PaymentReversalStatusEnum paymentReversalStatusEnum = paymentReversalService.GetPaymentReversalStatus(originalPayments);

            Assert.AreEqual(PaymentReversalStatusEnum.Refundable, paymentReversalStatusEnum);

            decimal amount = 40m; // 40/3 is 13.3-, $0.01 remaining after equal distribution in this scenario

            IList<Payment> payments = paymentReversalService.RefundPaymentAsync(originalPayments, new VisitPayUser { VisitPayUserId = SystemUsers.SystemUserId }, amount).Result;
            
            CollectionAssert.AllItemsAreNotNull(payments);

            Assert.AreEqual(amount / -1, originalPayments.SelectMany(x => x.ReversalPayments).Sum(x => x.ActualPaymentAmount));
            Assert.IsTrue(payments.GroupBy(x => x.ActualPaymentAmount).Count() == 2);
            Assert.IsTrue(payments.All(x => x.PaymentStatus == PaymentStatusEnum.ClosedPaid));
        }

        public void RefundPartialRoundingNotEqualPayments()
        {
            DateTime paymentDateTime = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 19, 58, 30); // TODAY at 7:58:30 PM GMT-7
            paymentDateTime = paymentDateTime.AddDays(-2);

            IList<Payment> originalPayments = new List<Payment>();
            originalPayments.AddRange(this.CreateBasicPayments(103m, paymentDateTime, PaymentMethodTypeEnum.Visa, PaymentStatusEnum.ClosedPaid));
            originalPayments.AddRange(this.CreateBasicPayments(102m, paymentDateTime, PaymentMethodTypeEnum.Visa, PaymentStatusEnum.ClosedPaid));
            originalPayments.AddRange(this.CreateBasicPayments(101m, paymentDateTime, PaymentMethodTypeEnum.Visa, PaymentStatusEnum.ClosedPaid));
            originalPayments.AddRange(this.CreateBasicPayments(99m, paymentDateTime, PaymentMethodTypeEnum.Visa, PaymentStatusEnum.ClosedPaid));
            originalPayments.AddRange(this.CreateBasicPayments(98m, paymentDateTime, PaymentMethodTypeEnum.Visa, PaymentStatusEnum.ClosedPaid));
            originalPayments.AddRange(this.CreateBasicPayments(97m, paymentDateTime, PaymentMethodTypeEnum.Visa, PaymentStatusEnum.ClosedPaid));
            
            PaymentReversalService paymentReversalService = this.CreatePaymentReversalService();
            PaymentReversalStatusEnum paymentReversalStatusEnum = paymentReversalService.GetPaymentReversalStatus(originalPayments);

            Assert.AreEqual(PaymentReversalStatusEnum.Refundable, paymentReversalStatusEnum);

            decimal amount = 31m; // 31/6 is 5.1666666666666666666666666666667, $0.02 remaining after equal distribution in this scenario.

            IList<Payment> payments = paymentReversalService.RefundPaymentAsync(originalPayments, new VisitPayUser { VisitPayUserId = SystemUsers.SystemUserId }, amount).Result;

            CollectionAssert.AllItemsAreNotNull(payments);

            Assert.AreEqual(amount / -1, originalPayments.SelectMany(x => x.ReversalPayments).Sum(x => x.ActualPaymentAmount));
            Assert.IsTrue(payments.All(x => x.PaymentStatus == PaymentStatusEnum.ClosedPaid));
        }

        public void RefundFullSuccessfulTwoPayments()
        {
            DateTime paymentDateTime = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 19, 58, 30); // TODAY at 7:58:30 PM GMT-7
            paymentDateTime = paymentDateTime.AddDays(-2);

            IList<Payment> originalPayments = new List<Payment>();
            originalPayments.AddRange(this.CreateBasicPayments(100m, paymentDateTime, PaymentMethodTypeEnum.Visa, PaymentStatusEnum.ClosedPaid));
            originalPayments.AddRange(this.CreateBasicPayments(200m, paymentDateTime, PaymentMethodTypeEnum.Visa, PaymentStatusEnum.ClosedPaid));

            PaymentReversalService paymentReversalService = this.CreatePaymentReversalService();
            PaymentReversalStatusEnum paymentReversalStatusEnum = paymentReversalService.GetPaymentReversalStatus(originalPayments);

            Assert.AreEqual(PaymentReversalStatusEnum.Refundable, paymentReversalStatusEnum);

            IList<Payment> payments = paymentReversalService.RefundPaymentAsync(originalPayments, new VisitPayUser { VisitPayUserId = SystemUsers.SystemUserId }).Result;

            CollectionAssert.AllItemsAreNotNull(payments);

            Assert.AreEqual(originalPayments.Sum(x => x.ActualPaymentAmount) / -1, originalPayments.SelectMany(x => x.ReversalPayments).Sum(x => x.ActualPaymentAmount));
            Assert.IsTrue(payments.All(x => x.PaymentStatus == PaymentStatusEnum.ClosedPaid));
        }

        [Test]
        public void VoidFailure()
        {
            DateTime paymentDateTime = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 19, 58, 30); // TODAY at 7:58:30 PM GMT-7
            paymentDateTime = paymentDateTime.AddDays(-2);
            IList<Payment> originalPayments = this.CreateBasicPayments(100m, paymentDateTime, PaymentMethodTypeEnum.Visa, PaymentStatusEnum.ClosedPaid);
            PaymentReversalService paymentReversalService = this.CreatePaymentReversalService();
            PaymentReversalStatusEnum paymentReversalStatusEnum = paymentReversalService.GetPaymentReversalStatus(originalPayments);

            Assert.AreNotEqual(PaymentReversalStatusEnum.Voidable, paymentReversalStatusEnum);

            IList<Payment> payments = paymentReversalService.VoidPaymentAsync(originalPayments, new VisitPayUser { VisitPayUserId = SystemUsers.SystemUserId }).Result;
            Assert.IsTrue(payments.IsNullOrEmpty());
        }

        //Todo: This test fails intermittently, needs to be fixed.
        public void VoidSuccessful()
        {
            DateTime paymentDateTime = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 19, 58, 30); // TODAY at 7:58:30 PM GMT-7
            IList<Payment> originalPayments = this.CreateBasicPayments(100m, paymentDateTime, PaymentMethodTypeEnum.Visa, PaymentStatusEnum.ClosedPaid);
            PaymentReversalService paymentReversalService = this.CreatePaymentReversalService();
            PaymentReversalStatusEnum paymentReversalStatusEnum = paymentReversalService.GetPaymentReversalStatus(originalPayments);

            Assert.AreEqual(PaymentReversalStatusEnum.Voidable, paymentReversalStatusEnum);

            IList<Payment> payments = paymentReversalService.VoidPaymentAsync(originalPayments, new VisitPayUser { VisitPayUserId = SystemUsers.SystemUserId }).Result;
            Assert.IsTrue(payments.All(x => x.PaymentStatus == PaymentStatusEnum.ClosedPaid));
        }
    }
}
﻿using Ivh.Common.Base.Enums;
using Ivh.Domain.FinanceManagement.Payment.Interfaces;
using Ivh.Domain.FinanceManagement.Payment.Services.PaymentAllocation;
using Ivh.Domain.Logging.Interfaces;
using Ivh.Domain.FinanceManagement.Payment.Entities;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Ivh.Common.Base.Utilities.Extensions;
using AutoMapper;

namespace Ivh.Domain.Payment.Services
{
    using Base.Interfaces;
    using Common.Base.Constants;
    using Common.Tests.Helpers.Base;
    using Common.Tests.Helpers.Payment;
    using Common.Tests.Helpers.Visit;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using FinanceManagement.ServiceGroup.Interfaces;
    using Settings.Interfaces;
    using Visit.Entities;
    using IPaymentAllocationRepository = FinanceManagement.Payment.Interfaces.IPaymentAllocationRepository;
    using Payment = FinanceManagement.Payment.Entities.Payment;

    public class MerchantAccountAllocationServiceMockBuilder
    {
        public Mock<IMerchantAccountRepository> _merchantAccountRepositoryMock;
        public Mock<IBalanceTransferStatusMerchantAccountRepository> _balanceTransferStatusMerchantAccountRepositoryMock;
        public Mock<IServiceGroupMerchantAccountRepository> _serviceGroupMerchantAccountRepositoryMock;
        public Mock<IPaymentAllocationRepository> _paymentAllocationRepositoryMock;
        public Mock<IFeatureService> _featureServiceMock;
        public Mock<ILoggingService> _loggingServiceMock;
        public Mock<ILoggingProvider> _loggingProviderMock;

        public IDomainServiceCommonService _domainServiceCommonService;

        public MerchantAccountAllocationServiceMockBuilder()
        {
            this._merchantAccountRepositoryMock = new Mock<IMerchantAccountRepository>();
            this._balanceTransferStatusMerchantAccountRepositoryMock = new Mock<IBalanceTransferStatusMerchantAccountRepository>();
            this._serviceGroupMerchantAccountRepositoryMock = new Mock<IServiceGroupMerchantAccountRepository>();
            this._paymentAllocationRepositoryMock = new Mock<IPaymentAllocationRepository>();
            this._featureServiceMock = new Mock<IFeatureService>();
            this._loggingProviderMock = new Mock<ILoggingProvider>();
            this._loggingServiceMock = new Mock<ILoggingService>();

            this._domainServiceCommonService = DomainServiceCommonServices.CreateDomainServiceCommonService(
                applicationSettingsServiceMock: null,
                featureServiceMock: this._featureServiceMock,
                clientServiceMock: null,
                loggingServiceMock: this._loggingServiceMock,
                busMock: null,
                cacheMock: null,
                timeZoneHelperMock: null);

        }

        public void Setup(Action<MerchantAccountAllocationServiceMockBuilder> configAction)
        {
            configAction(this);
        }

        public MerchantAccountAllocationService CreateMerchantAccountAllocationService()
        {
            return new MerchantAccountAllocationService(
                new Lazy<IDomainServiceCommonService>(() => this._domainServiceCommonService),
                new Lazy<IMerchantAccountRepository>(() => this._merchantAccountRepositoryMock.Object),
                new Lazy<IBalanceTransferStatusMerchantAccountRepository>(() => this._balanceTransferStatusMerchantAccountRepositoryMock.Object),
                new Lazy<IServiceGroupMerchantAccountRepository>(() => this._serviceGroupMerchantAccountRepositoryMock.Object),
                new Lazy<IPaymentAllocationRepository>(() => this._paymentAllocationRepositoryMock.Object)
           );
        }
    }

    [TestFixture]
    public class MerchantAccountAllocationServiceTests : DomainTestBase
    {

        private int MainAccountId = 1;
        private int SweepAccountId = 2;
        private int BalanceTransferAccountId = 3;

        private IQueryable<MerchantAccount> GetMerchantAccounts(bool includeBalanceTransfer)
        {
            List<MerchantAccount> merchantAccountList = new List<MerchantAccount>();
            merchantAccountList.Add(new MerchantAccount { MerchantAccountId = MainAccountId, Account = "0000", Description = "Main Account" });
            merchantAccountList.Add(new MerchantAccount { MerchantAccountId = SweepAccountId, Account = "0001", Description = "Sweep Account" });
            if (includeBalanceTransfer)
            {
                merchantAccountList.Add(new MerchantAccount { MerchantAccountId = BalanceTransferAccountId, Account = "9999", Description = "Balance Transfer Account" });
            }
            return merchantAccountList.AsQueryable();
        }

        private BalanceTransferStatus GetActiveBalanceTransfer()
        {
            return new BalanceTransferStatus { BalanceTransferStatusId = 2, BalanceTransferStatusName = "ActiveBalanceTransfer", BalanceTransferStatusDisplayName = "Visit on BT" };
        }

        private IQueryable<BalanceTransferStatusMerchantAccount> GetBalanceTransferStatusMerchantAccounts()
        {
            List<BalanceTransferStatusMerchantAccount> balanceTransferStatusMerchantAccountList = new List<BalanceTransferStatusMerchantAccount>();
            BalanceTransferStatus btStatus = GetActiveBalanceTransfer();
            MerchantAccount btMerchantAccount = new MerchantAccount { MerchantAccountId = BalanceTransferAccountId, Account = "9999", Description = "Balance Transfer Account" };
            balanceTransferStatusMerchantAccountList.Add(new BalanceTransferStatusMerchantAccount { BalanceTransferStatusMerchantAccountId = 1, MerchantAccount = btMerchantAccount, BalanceTransferStatus = btStatus });
            return balanceTransferStatusMerchantAccountList.AsQueryable();
        }

        /*private void SetupAutoMapper()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile(new FinanceManagement.Mappings.AutomapperProfile());
                cfg.CreateMap<Visit, PaymentVisit>();
                cfg.CreateMap<PaymentVisit, Visit>();
            });
        }*/

        /// <summary>
        /// Cloning the payment object requires certain children to have their child payment object nullified 
        /// and the PaymentAllocations cleared.
        /// </summary>
        [Test]
        public void PaymentCloneUsingAutoMapper()
        {
            string json = Ivh.Domain.Properties.Resources.SpecificAmountPayment500;
            FinanceManagement.Payment.Entities.Payment paymentEntity = JsonConvert.DeserializeObject<FinanceManagement.Payment.Entities.Payment>(json);
            FinanceManagement.Payment.Entities.Payment paymentClone = AutoMapper.Mapper.Map<FinanceManagement.Payment.Entities.Payment>(paymentEntity);
            
            Assert.AreNotSame(paymentEntity, paymentClone);

            Assert.True(paymentClone.PaymentId == 0);
            Assert.True(paymentClone.PaymentAllocations.Count() == 0);
            foreach (PaymentScheduledAmount paymentScheduledAmount in paymentClone.PaymentScheduledAmounts)
            {
                Assert.True(paymentScheduledAmount.PaymentScheduledAmountId == 0);
                Assert.True(paymentScheduledAmount.Payment == null);
            }
            foreach (PaymentAllocation paymentAllocation in paymentClone.PaymentAllocations)
            {
                Assert.True(paymentAllocation.PaymentAllocationId == 0);
                Assert.True(paymentAllocation.Payment == null);
            }
            foreach (PaymentStatusHistory paymentStatusHistory in paymentClone.PaymentStatusHistories)
            {
                Assert.True(paymentStatusHistory.PaymentStatusHistoryId == 0);
                Assert.True(paymentStatusHistory.Payment == null);
            }
        }

        [Test]
        public void PaymentSplitAcrossMultipleMerchantAccounts()
        {
            IQueryable<MerchantAccount> merchantAccounts = this.GetMerchantAccounts(false);
            IQueryable<BalanceTransferStatusMerchantAccount> balanceTransferStatusMerchantAccounts = this.GetBalanceTransferStatusMerchantAccounts();

            MerchantAccountAllocationServiceMockBuilder builder = new MerchantAccountAllocationServiceMockBuilder();
            builder.Setup((x) =>
            {
                x._featureServiceMock.Setup(t => t.IsFeatureEnabled(It.IsIn(VisitPayFeatureEnum.MerchantAccountSplittingIsEnabled))).Returns(true);
                x._merchantAccountRepositoryMock.Setup(t => t.GetAll(false)).Returns(merchantAccounts);
                x._balanceTransferStatusMerchantAccountRepositoryMock.Setup(t => t.GetBalanceTransferMerchantAccountId()).Returns(balanceTransferStatusMerchantAccounts.Last().MerchantAccount.MerchantAccountId);
                x._serviceGroupMerchantAccountRepositoryMock.Setup(t => t.GetMerchantAccountId(It.IsAny<int?>())).Returns((int? serviceGroupId) => serviceGroupId);
            });

            const decimal visitBalance = 1000m;
            const decimal hbVisitPaymentAmount = 600m;
            const decimal pbVisitPaymentAmount = 400m;

            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit,IList<VisitTransaction>>>{
                VisitFactory.GenerateActiveVisit(visitBalance, 1, billingApplication: BillingApplicationConstants.HB).WithServiceGroup(this.MainAccountId),
                VisitFactory.GenerateActiveVisit(visitBalance, 1, billingApplication: BillingApplicationConstants.PB).WithServiceGroup(this.SweepAccountId)
            };

            Payment payment = new PaymentBuilder().WithDefaultValues()
                .AsManualScheduledSpecificVisits(
                    new PaymentScheduledAmount
                    {
                        ScheduledAmount = hbVisitPaymentAmount,
                        ScheduledPrincipal = hbVisitPaymentAmount,
                        ScheduledInterest = 0m,
                        PaymentVisit = Mapper.Map<PaymentVisit>(visits[0].Item1)
                    },
                    new PaymentScheduledAmount
                    {
                        ScheduledAmount = pbVisitPaymentAmount,
                        ScheduledPrincipal = pbVisitPaymentAmount,
                        ScheduledInterest = 0m,
                        PaymentVisit = Mapper.Map<PaymentVisit>(visits[1].Item1)
                    })
                .WithPaymentAllocations()
                .Build();

            MerchantAccountAllocationService merchantAccountAllocationService = builder.CreateMerchantAccountAllocationService();

            IList<FinanceManagement.Payment.Entities.Payment> splitPayments = merchantAccountAllocationService.SplitPayments(payment.ToListOfOne());

            Assert.True(merchantAccounts.ToList().Count == splitPayments.Count, "Should have the same number of merchant accounts and split payments");

            foreach (MerchantAccount merchantAccount in merchantAccounts)
            {
                IEnumerable<Payment> accountPayments = splitPayments.Where(x => x.MerchantAccount.Account == merchantAccount.Account);
                Assert.True(accountPayments.Count() == 1, "There should be one accountPayment per merchant account");
            }

            decimal accountPaymentAmount1 = splitPayments.Where(x => x.MerchantAccountId == MainAccountId).Sum(x => x.ActualPaymentAmount);
            Assert.AreEqual(hbVisitPaymentAmount, accountPaymentAmount1, $"accountPaymentAmount1 should be {hbVisitPaymentAmount}");
            
            decimal accountPaymentAmount2 = splitPayments.Where(x => x.MerchantAccountId == SweepAccountId).Sum(x => x.ActualPaymentAmount);
            Assert.AreEqual(pbVisitPaymentAmount, accountPaymentAmount2,  $"accountPaymentAmount2 should be {pbVisitPaymentAmount}");
        }

        [TestCase(600,0,400,0)]
        [TestCase(550,50,350,50)]
        public void PaymentSplitAcrossMultipleMerchantAccountsAndBalanceTransfer(decimal hbVisitPaymentAmount, decimal hbVisitPaymentAmountInterest, decimal pbVisitPaymentAmount, decimal pbVisitPaymentAmountInterest)
        {
            IQueryable<MerchantAccount> merchantAccounts = this.GetMerchantAccounts(true);
            IQueryable<BalanceTransferStatusMerchantAccount> balanceTransferStatusMerchantAccounts = this.GetBalanceTransferStatusMerchantAccounts();

            MerchantAccountAllocationServiceMockBuilder builder = new MerchantAccountAllocationServiceMockBuilder();
            builder.Setup((x) =>
            {
                x._featureServiceMock.Setup(t => t.IsFeatureEnabled(It.IsIn(VisitPayFeatureEnum.MerchantAccountSplittingIsEnabled, VisitPayFeatureEnum.BalanceTransfer))).Returns(true);
                x._merchantAccountRepositoryMock.Setup(t => t.GetAll(false)).Returns(merchantAccounts);
                x._serviceGroupMerchantAccountRepositoryMock.Setup(t => t.GetMerchantAccountId(It.IsAny<int?>())).Returns((int? serviceGroupId) => serviceGroupId);
                x._balanceTransferStatusMerchantAccountRepositoryMock.Setup(t => t.GetBalanceTransferMerchantAccountId()).Returns(balanceTransferStatusMerchantAccounts.Last().MerchantAccount.MerchantAccountId);
            });

            const decimal visitBalance = 1000m;

            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>{
                VisitFactory.GenerateActiveVisit(visitBalance, 1, billingApplication: BillingApplicationConstants.HB).WithServiceGroup(this.MainAccountId).WithActiveBalanceTransferStatus(),
                VisitFactory.GenerateActiveVisit(visitBalance, 1, billingApplication: BillingApplicationConstants.PB).WithServiceGroup(this.SweepAccountId).WithActiveBalanceTransferStatus()
            };

            Payment payment = new PaymentBuilder().WithDefaultValues()
                .AsManualScheduledSpecificVisits(
                    new PaymentScheduledAmount
                    {
                        ScheduledAmount = hbVisitPaymentAmount + hbVisitPaymentAmountInterest,
                        ScheduledPrincipal = hbVisitPaymentAmount,
                        ScheduledInterest = hbVisitPaymentAmountInterest,
                        PaymentVisit = Mapper.Map<PaymentVisit>(visits[0].Item1)
                    },
                    new PaymentScheduledAmount
                    {
                        ScheduledAmount = pbVisitPaymentAmount + pbVisitPaymentAmountInterest,
                        ScheduledPrincipal = pbVisitPaymentAmount,
                        ScheduledInterest = pbVisitPaymentAmountInterest,
                        PaymentVisit = Mapper.Map<PaymentVisit>(visits[1].Item1)
                    })
                .WithPaymentAllocations();

            MerchantAccountAllocationService merchantAccountAllocationService = builder.CreateMerchantAccountAllocationService();

            IList<FinanceManagement.Payment.Entities.Payment> splitPayments = merchantAccountAllocationService.SplitPayments(payment.ToListOfOne());

            int btNonInterestAllocationCount = (
                from p in splitPayments
                from pa in p.PaymentAllocations
                where p.MerchantAccountId == BalanceTransferAccountId
                && pa.PaymentAllocationType != PaymentAllocationTypeEnum.VisitInterest
                select pa
            ).Count();

            int btAllAllocationCount = (
                from p in splitPayments
                from pa in p.PaymentAllocations
                where p.MerchantAccountId == BalanceTransferAccountId
                select pa
            ).Count();

            int interestAllocationCount = (
                from p in splitPayments
                from pa in p.PaymentAllocations
                where pa.PaymentAllocationType != PaymentAllocationTypeEnum.VisitInterest
                select pa
            ).Count();

            Assert.True(btNonInterestAllocationCount == btAllAllocationCount, "Balance transfer should not include interest payments");

            Assert.True(merchantAccounts.ToList().Count == splitPayments.Count, "Should have the same number of merchant accounts and split payments");

            foreach (MerchantAccount merchantAccount in merchantAccounts)
            {
                //The BT merchant should get all principal allocations, and any interest allocations will be split to the remaining merchants
                bool expectSplitPaymentForAccount = interestAllocationCount > 0 || merchantAccount.MerchantAccountId == this.BalanceTransferAccountId;

                if (expectSplitPaymentForAccount)
                {
                    IEnumerable<Payment> accountPayments = splitPayments.Where(x => x.MerchantAccount.Account == merchantAccount.Account);
                    Assert.True(accountPayments.Count() == 1, "There should be one accountPayment per merchant account");
                }
            }

            decimal accountPaymentAmount1 = splitPayments.Where(x => x.MerchantAccountId == MainAccountId).Sum(x => x.ActualPaymentAmount);
            Assert.AreEqual(hbVisitPaymentAmountInterest, accountPaymentAmount1, $"MainAccount payment should be {hbVisitPaymentAmountInterest}");

            decimal accountPaymentAmount2 = splitPayments.Where(x => x.MerchantAccountId == SweepAccountId).Sum(x => x.ActualPaymentAmount);
            Assert.AreEqual(pbVisitPaymentAmountInterest, accountPaymentAmount2, $"SweepAccount payment  should be {pbVisitPaymentAmountInterest}");
        }

        [Test]
        public void PaymentSplitAcrossSingleMerchantAccount()
        {
            IQueryable<MerchantAccount> merchantAccounts = this.GetMerchantAccounts(false).Where(x => x.MerchantAccountId == this.MainAccountId);
            IQueryable<BalanceTransferStatusMerchantAccount> balanceTransferStatusMerchantAccounts = this.GetBalanceTransferStatusMerchantAccounts();

            MerchantAccountAllocationServiceMockBuilder builder = new MerchantAccountAllocationServiceMockBuilder();
            builder.Setup((x) =>
            {
                x._featureServiceMock.Setup(t => t.IsFeatureEnabled(It.IsIn(VisitPayFeatureEnum.MerchantAccountSplittingIsEnabled))).Returns(true);
                x._merchantAccountRepositoryMock.Setup(t => t.GetAll(false)).Returns(merchantAccounts);
                x._balanceTransferStatusMerchantAccountRepositoryMock.Setup(t => t.GetBalanceTransferMerchantAccountId()).Returns(balanceTransferStatusMerchantAccounts.Last().MerchantAccount.MerchantAccountId);
                x._serviceGroupMerchantAccountRepositoryMock.Setup(t => t.GetMerchantAccountId(It.IsAny<int?>())).Returns((int? serviceGroupId) => serviceGroupId);
            });

            const decimal visitBalance = 1000m;
            const decimal hbVisitPaymentAmount = 600m;
            const decimal pbVisitPaymentAmount = 400m;
            const decimal totalPaymentAmount = hbVisitPaymentAmount + pbVisitPaymentAmount;

            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>{
                VisitFactory.GenerateActiveVisit(visitBalance, 1, billingApplication: BillingApplicationConstants.HB).WithServiceGroup(this.MainAccountId),
                VisitFactory.GenerateActiveVisit(visitBalance, 1, billingApplication: BillingApplicationConstants.PB).WithServiceGroup(this.MainAccountId)
            };

            Payment payment = new PaymentBuilder().WithDefaultValues()
                .AsManualScheduledSpecificVisits(
                    new PaymentScheduledAmount
                    {
                        ScheduledAmount = hbVisitPaymentAmount,
                        ScheduledPrincipal = hbVisitPaymentAmount,
                        ScheduledInterest = 0m,
                        PaymentVisit = Mapper.Map<PaymentVisit>(visits[0].Item1)
                    },
                    new PaymentScheduledAmount
                    {
                        ScheduledAmount = pbVisitPaymentAmount,
                        ScheduledPrincipal = pbVisitPaymentAmount,
                        ScheduledInterest = 0m,
                        PaymentVisit = Mapper.Map<PaymentVisit>(visits[1].Item1)
                    })
                .WithPaymentAllocations();

            MerchantAccountAllocationService merchantAccountAllocationService = builder.CreateMerchantAccountAllocationService();

            IList<FinanceManagement.Payment.Entities.Payment> splitPayments = merchantAccountAllocationService.SplitPayments(payment.ToListOfOne());

            Assert.True(merchantAccounts.ToList().Count == splitPayments.Count, "Should have the same number of merchant accounts and split payments");

            foreach (MerchantAccount merchantAccount in merchantAccounts)
            {
                IEnumerable<Payment> accountPayments = splitPayments.Where(x => x.MerchantAccount.Account == merchantAccount.Account);
                Assert.True(accountPayments.Count() == 1, "There should be one accountPayment per merchant account");
            }

            decimal accountPaymentAmount1 = splitPayments.Where(x => x.MerchantAccountId == this.MainAccountId).Sum(x => x.ActualPaymentAmount);
            Assert.AreEqual(totalPaymentAmount, accountPaymentAmount1, $"MainAccount payment should be {totalPaymentAmount}");

            decimal accountPaymentAmount2 = splitPayments.Where(x => x.MerchantAccountId == this.SweepAccountId).Sum(x => x.ActualPaymentAmount);
            Assert.AreEqual(0m, accountPaymentAmount2, $"SweepAccount payment should be {0m}");
        }
    }
}

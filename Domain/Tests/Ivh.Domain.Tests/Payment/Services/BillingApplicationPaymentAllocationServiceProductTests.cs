﻿namespace Ivh.Domain.Payment.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using FinanceManagement.Payment.Entities;
    using FinanceManagement.Payment.Interfaces;
    using FinanceManagement.Payment.Services.PaymentAllocation;
    using Interfaces;
    using NUnit.Framework;

    /// <summary>
    /// test scenarios based on https://ivincehealth.sharepoint.com/Products/vpp/_layouts/15/WopiFrame.aspx?sourcedoc={75200273-B1A4-452C-93C8-21C06482ECEC}&file=VPNG-949%20PA_Payment%20Distribution%20Illustration.xlsx&action=default
    /// expected outcomes in the link are for VisitType distribution
    /// </summary>
    [TestFixture]
    public class BillingApplicationPaymentAllocationServiceProductTests : PaymentAllocationServiceProductTestsBase<BillingApplicationPaymentAllocationService>, IPaymentAllocationServiceProductTests
    {
        [Test]
        public void OffsetAllAllocationsInPayment_AfterReturnAch()
        {
            IList<Payment> payments = this.ProductDefined_RecurringFinancePlan(false, delegate (IPaymentAllocationService service, List<Payment> internalPayments)
            {
                foreach (Payment payment in internalPayments)
                {
                    service.OffsetAllAllocationsInPayment(payment);
                }
            });

            Payment payment1 = payments.Take(1).First();
            Payment payment2 = payments.Skip(1).First();

            Assert.AreEqual(0m, payment1.ActualPaymentAmount);
            Assert.AreEqual(0m, payment2.ActualPaymentAmount);
            Assert.AreEqual(0m, payment1.PaymentAllocations.Sum(x => x.ActualAmount));
            Assert.AreEqual(0m, payment2.PaymentAllocations.Sum(x => x.ActualAmount));
        }

        #region specific amount tests

        [Test]
        // specific amount example 1
        public void ProductDefined_SpecificAmount_NoFP_1()
        {
            Payment payment = this.ProductDefined_SpecificAmount_NoFP(250);

            decimal allocationForVisi1001 = this.FindAllocationByVisitId(payment, 1001);//HB
            decimal allocationForVisi1002 = this.FindAllocationByVisitId(payment, 1002);//PB
            decimal allocationForVisi1003 = this.FindAllocationByVisitId(payment, 1003);//PB
            decimal allocationForVisi1004 = this.FindAllocationByVisitId(payment, 1004);//HB
            decimal allocationForVisi1005 = this.FindAllocationByVisitId(payment, 1005);//PB

            Assert.AreEqual(75.00m, allocationForVisi1001);
            Assert.AreEqual(82.89m, allocationForVisi1002);
            Assert.AreEqual(92.11m, allocationForVisi1003);
            Assert.AreEqual(0m,     allocationForVisi1004);
            Assert.AreEqual(0m, allocationForVisi1005);
        }


        [Test]
        // specific amount example 2
        public void ProductDefined_SpecificAmount_NoFP_2()
        {
            Payment payment = this.ProductDefined_SpecificAmount_NoFP(500);
            //this failed- creating allocations that sum > 500
            Assert.AreEqual(75.00m, this.FindAllocationByVisitId(payment, 1001));
            Assert.AreEqual(180.00m, this.FindAllocationByVisitId(payment, 1002));
            Assert.AreEqual(200.00m, this.FindAllocationByVisitId(payment, 1003));
            Assert.AreEqual(29.70m, this.FindAllocationByVisitId(payment, 1004));
            Assert.AreEqual(15.30m, this.FindAllocationByVisitId(payment, 1005));
        }


        [Test]
        // specific amount example 3
        public void ProductDefined_SpecificAmount_WithFP()
        {
            Payment payment = this.ProductDefined_SpecificAmount_WithFP(2000);

            Assert.AreEqual(300.00m, this.FindAllocationByVisitId(payment, 1001));
            Assert.AreEqual(300.00m, this.FindAllocationByVisitId(payment, 1002));
            Assert.AreEqual(300.00m, this.FindAllocationByVisitId(payment, 1003));
            Assert.AreEqual(50.00m, this.FindAllocationByVisitId(payment, 1004));
            Assert.AreEqual(457.69m, this.FindAllocationByVisitId(payment, 1005));
            Assert.AreEqual(392.31m, this.FindAllocationByVisitId(payment, 1006));
            Assert.AreEqual(200.00m, this.FindAllocationByVisitId(payment, 1007));
            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1008));
            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1009));
        }


        [Test]
        // specific amount example 4
        public void ProductDefined_SpecificAmount_WithFP_WithPastDues()
        {
            Payment payment = this.ProductDefined_SpecificAmount_WithFP_WithPastDues(430);

            // NOTE ON ROUNDING: When we are rounding for multiple bucket allocations, we round each bucket's allocation first and then sum to get the total.
            // The examples in the test scenarios may be slightly different. They are allocating the sum of all buckets and then rounding.
            Assert.AreEqual(30.76m, this.FindAllocationByVisitId(payment, 1001));
            Assert.AreEqual(19.24m, this.FindAllocationByVisitId(payment, 1002));
            Assert.AreEqual(18.31m, this.FindAllocationByVisitId(payment, 1003));
            Assert.AreEqual(15.69m, this.FindAllocationByVisitId(payment, 1004));
            Assert.AreEqual(66.00m, this.FindAllocationByVisitId(payment, 1005));
            Assert.AreEqual(200.00m, this.FindAllocationByVisitId(payment, 1006));
            Assert.AreEqual(80.00m, this.FindAllocationByVisitId(payment, 1007));
            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1008));
            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1009));
        }


        [Test]
        // specific amount example 5
        public void ProductDefined_SpecificAmount_WithFP_WithInterest_1()
        {
            Payment payment = this.ProductDefined_SpecificAmount_WithFP_WithInterest(175);

            Assert.AreEqual(150.00m, this.FindAllocationByVisitId(payment, 1001));

            Assert.AreEqual(7.95m, this.FindAllocationByVisitId(payment, 1002, PaymentAllocationTypeEnum.VisitPrincipal));
            Assert.AreEqual(0.39m, this.FindAllocationByVisitId(payment, 1002, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(15.43m, this.FindAllocationByVisitId(payment, 1003));
            Assert.AreEqual(0.32m, this.FindAllocationByVisitId(payment, 1003, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1004));
            Assert.AreEqual(0.43m, this.FindAllocationByVisitId(payment, 1004, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1005));
            Assert.AreEqual(0.36m, this.FindAllocationByVisitId(payment, 1005, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1006));
            Assert.AreEqual(0.12m, this.FindAllocationByVisitId(payment, 1006, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1007));
            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1008));
        }


        [Test]
        // specific amount example 6
        public void ProductDefined_SpecificAmount_WithFP_WithInterest_2()
        {
            Payment payment = this.ProductDefined_SpecificAmount_WithFP_WithInterest(400);

            Assert.AreEqual(150.00m, this.FindAllocationByVisitId(payment, 1001));

            Assert.AreEqual(115.81m, this.FindAllocationByVisitId(payment, 1002));
            Assert.AreEqual(0.39m, this.FindAllocationByVisitId(payment, 1002, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(96.49m, this.FindAllocationByVisitId(payment, 1003));
            Assert.AreEqual(0.32m, this.FindAllocationByVisitId(payment, 1003, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(6.61m, this.FindAllocationByVisitId(payment, 1004));
            Assert.AreEqual(0.43m, this.FindAllocationByVisitId(payment, 1004, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(5.66m, this.FindAllocationByVisitId(payment, 1005));
            Assert.AreEqual(0.36m, this.FindAllocationByVisitId(payment, 1005, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(23.81m, this.FindAllocationByVisitId(payment, 1006));
            Assert.AreEqual(0.12m, this.FindAllocationByVisitId(payment, 1006, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1007));
            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1008));
        }


        [Test]
        // specific amount example 7
        public void ProductDefined_SpecificAmount_WithFP_WithInterest_3()
        {
            Payment payment = this.ProductDefined_SpecificAmount_WithFP_WithInterest(151);

            Assert.AreEqual(150.00m, this.FindAllocationByVisitId(payment, 1001));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1002));
            Assert.AreEqual(0.39m, this.FindAllocationByVisitId(payment, 1002, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1003));
            Assert.AreEqual(0.32m, this.FindAllocationByVisitId(payment, 1003, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1004));
            Assert.AreEqual(0.09m, this.FindAllocationByVisitId(payment, 1004, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1005));
            Assert.AreEqual(0.08m, this.FindAllocationByVisitId(payment, 1005, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1006));
            Assert.AreEqual(0.12m, this.FindAllocationByVisitId(payment, 1006, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1007));
            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1008));
        }

        
        [Test]
        // specific amount example 8
        public void ProductDefined_SpecificAmount_WithFP_WithInterest_WithPastDue()
        {
            Payment payment = this.ProductDefined_SpecificAmount_WithFP_WithInterest_WithPastDue(100);

            Assert.AreEqual(22.84m, this.FindAllocationByVisitId(payment, 1001));
            Assert.AreEqual(1.54m, this.FindAllocationByVisitId(payment, 1001, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(44.34m, this.FindAllocationByVisitId(payment, 1002));
            Assert.AreEqual(1.28m, this.FindAllocationByVisitId(payment, 1002, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(5.10m, this.FindAllocationByVisitId(payment, 1003));
            Assert.AreEqual(1.01m, this.FindAllocationByVisitId(payment, 1003, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(4.37m, this.FindAllocationByVisitId(payment, 1004));
            Assert.AreEqual(0.86m, this.FindAllocationByVisitId(payment, 1004, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(18.37m, this.FindAllocationByVisitId(payment, 1005));
            Assert.AreEqual(0.29m, this.FindAllocationByVisitId(payment, 1005, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1006));
            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1007));
            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1008));
        }

        #endregion

        #region recurring finance plans       
        [Test]
        // recurring finance plans example 1
        public void ProductDefined_RecurringFinancePlan()
        {
            List<Payment> payments = this.ProductDefined_RecurringFinancePlan(false);

            Payment payment1 = payments.Take(1).First();
            Payment payment2 = payments.Skip(1).First();

            Assert.AreEqual(25.38m, this.FindAllocationByVisitId(payment1, 1001));
            Assert.AreEqual(40.62m, this.FindAllocationByVisitId(payment1, 1002));
            Assert.AreEqual(34.00m, this.FindAllocationByVisitId(payment1, 1003));

            Assert.AreEqual(7.56m, this.FindAllocationByVisitId(payment2, 1004));
            Assert.AreEqual(9.44m, this.FindAllocationByVisitId(payment2, 1005));
            Assert.AreEqual(33.00m, this.FindAllocationByVisitId(payment2, 1006));
        }

        
        [Test]
        // recurring finance plans example 2
        public void ProductDefined_RecurringFinancePlan_WithPastDue()
        {
            List<Payment> payments = this.ProductDefined_RecurringFinancePlan(true);

            Payment payment1 = payments.Take(1).First();
            Payment payment2 = payments.Skip(1).First();

            Assert.AreEqual(50.76m, this.FindAllocationByVisitId(payment1, 1001));
            Assert.AreEqual(81.24m, this.FindAllocationByVisitId(payment1, 1002));
            Assert.AreEqual(68.00m, this.FindAllocationByVisitId(payment1, 1003));

            Assert.AreEqual(15.12m, this.FindAllocationByVisitId(payment2, 1004));
            Assert.AreEqual(18.88m, this.FindAllocationByVisitId(payment2, 1005));
            Assert.AreEqual(66.00m, this.FindAllocationByVisitId(payment2, 1006));
        }

        
        [Test]
        // recurring finance plans example 3
        public void ProductDefined_RecurringFinancePlan_WithInterest()
        {
            List<Payment> payments = this.ProductDefined_RecurringFinancePlan_WithInterest_Base();

            Payment payment1 = payments.Take(1).First();
            Payment payment2 = payments.Skip(1).First();

            Assert.AreEqual(11.66m, this.FindAllocationByVisitId(payment1, 1001));
            Assert.AreEqual(0.39m, this.FindAllocationByVisitId(payment1, 1001, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(22.63m, this.FindAllocationByVisitId(payment1, 1002));
            Assert.AreEqual(0.32m, this.FindAllocationByVisitId(payment1, 1002, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(8.99m, this.FindAllocationByVisitId(payment2, 1003));
            Assert.AreEqual(0.43m, this.FindAllocationByVisitId(payment2, 1003, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(7.70m, this.FindAllocationByVisitId(payment2, 1004));
            Assert.AreEqual(0.36m, this.FindAllocationByVisitId(payment2, 1004, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(32.40m, this.FindAllocationByVisitId(payment2, 1005));
            Assert.AreEqual(0.12m, this.FindAllocationByVisitId(payment2, 1005, PaymentAllocationTypeEnum.VisitInterest));
        }

        
        [Test]
        // recurring finance plans example 4
        public void ProductDefined_RecurringFinancePlan_WithInterest_WithPastDue()
        {
            List<Payment> payments = this.ProductDefined_RecurringFinancePlan_WithInterest_WithPastDue_Base();

            Payment payment1 = payments.Take(1).First();
            Payment payment2 = payments.Skip(1).First();

            Assert.AreEqual(34.74m, this.FindAllocationByVisitId(payment1, 1001));
            Assert.AreEqual(1.54m, this.FindAllocationByVisitId(payment1, 1001, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(67.44m, this.FindAllocationByVisitId(payment1, 1002));
            Assert.AreEqual(1.28m, this.FindAllocationByVisitId(payment1, 1002, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(29.47m, this.FindAllocationByVisitId(payment2, 1003));
            Assert.AreEqual(1.01m, this.FindAllocationByVisitId(payment2, 1003, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(25.26m, this.FindAllocationByVisitId(payment2, 1004));
            Assert.AreEqual(0.86m, this.FindAllocationByVisitId(payment2, 1004, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(43.11m, this.FindAllocationByVisitId(payment2, 1005));
            Assert.AreEqual(0.29m, this.FindAllocationByVisitId(payment2, 1005, PaymentAllocationTypeEnum.VisitInterest));
        }

        #endregion

        #region specific finance plan tests

        
        [Test]
        // specific finance plan example 1
        public void ProductDefined_SpecificFinancePlan_1()
        {
            Payment payment = this.ProductDefined_SpecificFinancePlan(50);

            Assert.AreEqual(17.77m, this.FindAllocationByVisitId(payment, 1001));
            Assert.AreEqual(15.23m, this.FindAllocationByVisitId(payment, 1002));
            Assert.AreEqual(17.00m, this.FindAllocationByVisitId(payment, 1003));
        }

        
        [Test]
        // specific finance plan example 2
        public void ProductDefined_SpecificFinancePlan_2()
        {
            Payment payment = this.ProductDefined_SpecificFinancePlan(300);

            Assert.AreEqual(107.69m, this.FindAllocationByVisitId(payment, 1001));
            Assert.AreEqual(92.31m, this.FindAllocationByVisitId(payment, 1002));
            Assert.AreEqual(100.00m, this.FindAllocationByVisitId(payment, 1003));
        }

        
        [Test]
        // specific finance plan example 3
        public void ProductDefined_SpecificFinancePlan_WithPastDue()
        {
            Payment payment = this.ProductDefined_SpecificFinancePlan(20, true);

            Assert.AreEqual(7.11m, this.FindAllocationByVisitId(payment, 1001));
            Assert.AreEqual(6.09m, this.FindAllocationByVisitId(payment, 1002));
            Assert.AreEqual(6.80m, this.FindAllocationByVisitId(payment, 1003));
        }

        
        [Test]
        // specific finance plan example 4
        public void ProductDefined_SpecificFinancePlan_WithInterest_WithPastDue_1()
        {
            Payment payment = this.ProductDefined_SpecificFinancePlan_WithInterest_WithPastDue(30);

            Assert.AreEqual(5.10m, this.FindAllocationByVisitId(payment, 1001));
            Assert.AreEqual(1.01m, this.FindAllocationByVisitId(payment, 1001, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(4.37m, this.FindAllocationByVisitId(payment, 1002));
            Assert.AreEqual(0.86m, this.FindAllocationByVisitId(payment, 1002, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(18.37m, this.FindAllocationByVisitId(payment, 1003));
            Assert.AreEqual(0.29m, this.FindAllocationByVisitId(payment, 1003, PaymentAllocationTypeEnum.VisitInterest));
        }
                
        [TestCase(225.57, 0, 225.57)]
        [TestCase(325.57, 0, 325.57)]
        [TestCase(325.57, 100, 325.57)]
        [TestCase(425.57, 100, 325.57)]
        [TestCase(325.57, -100, 225.57)]
        [TestCase(425.57, -100, 225.57)]
        // specific finance plan example 4
        public void ProductDefined_SpecificFinancePlan_WithInterest_WithPastDue_Adjustments(decimal paymentAmount, decimal adjustment, decimal expectedTotal)
        {
            //Total 325.57
            //Interest 2.16
            //prin 323.41
            Payment payment = this.ProductDefined_SpecificFinancePlan_WithInterest_WithPastDue(paymentAmount, adjustment);

            Assert.AreEqual(expectedTotal, payment.ActualPaymentAmount);
        }

        [Test]
        // specific finance plan example 5
        public void ProductDefined_SpecificFinancePlan_WithInterest_WithPastDue_2()
        {
            Payment payment = this.ProductDefined_SpecificFinancePlan_WithInterest_WithPastDue(2);

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1001));
            Assert.AreEqual(0.92m, this.FindAllocationByVisitId(payment, 1001, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1002));
            Assert.AreEqual(0.79m, this.FindAllocationByVisitId(payment, 1002, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1003));
            Assert.AreEqual(0.29m, this.FindAllocationByVisitId(payment, 1003, PaymentAllocationTypeEnum.VisitInterest));
        }

        #endregion

        #region specific visit tests

        
        [Test]
        // specific visit example 1
        public void ProductDefined_SpecificVisit()
        {
            Payment payment = this.ProductDefined_SpecificVisit(50);

            Assert.AreEqual(50.00m, this.FindAllocationByVisitId(payment, 1001));
        }

        
        [Test]
        // specific visit example 2
        public void ProductDefined_SpecificVisit_WithFP()
        {
            Payment payment = this.ProductDefined_SpecificVisit(50, true);

            Assert.AreEqual(50.00m, this.FindAllocationByVisitId(payment, 1001));
        }

        
        [Test]
        // specific visit example 3
        public void ProductDefined_SpecificVisit_WithFP_WithPastDue()
        {
            Payment payment = this.ProductDefined_SpecificVisit(50, true, true);

            Assert.AreEqual(50.00m, this.FindAllocationByVisitId(payment, 1001));
        }

        
        [Test]
        // specific visit example 4
        public void ProductDefined_SpecificVisit_WithFP_WithInterest()
        {
            Payment payment = this.ProductDefined_SpecificVisit_WithFP_WithInterest(50);

            Assert.AreEqual(49.57m, this.FindAllocationByVisitId(payment, 1001));
            Assert.AreEqual(0.43m, this.FindAllocationByVisitId(payment, 1001, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1002));
            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1002, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1003));
            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1003, PaymentAllocationTypeEnum.VisitInterest));
        }

        
        [Test]
        // specific visit example 5
        public void ProductDefined_SpecificVisit_WithFP_WithInterest_WithPastDue()
        {
            Payment payment = this.ProductDefined_SpecificVisit_WithFP_WithInterest_WithPastDue(50);

            Assert.AreEqual(48.99m, this.FindAllocationByVisitId(payment, 1001));
            Assert.AreEqual(1.01m, this.FindAllocationByVisitId(payment, 1001, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1002));
            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1002, PaymentAllocationTypeEnum.VisitInterest));

            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1003));
            Assert.AreEqual(0m, this.FindAllocationByVisitId(payment, 1003, PaymentAllocationTypeEnum.VisitInterest));
        }

        #endregion
    }
}

﻿namespace Ivh.Domain.Payment.Services
{
    using System;
    using System.Collections.Generic;
    using Common.Tests.Helpers.Payment;
    using Common.VisitPay.Enums;
    using FinanceManagement.Payment.Entities;
    using FinanceManagement.Payment.Services;
    using Moq;
    using NUnit.Framework;
    using Settings.Entities;
    using User.Entities;

    [TestFixture]
    public class PaymentActionServiceTests : DomainTestBase
    {
        private const int VpGuarantorId = 1;
        private const int VisitPayUserId = 2;

        #region reschedule

        private const int RescheduleRecurringPaymentMaximum = 2;
        private const int RescheduleRecurringPaymentWindow = 365;

        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public void IsEligibleToReschedule_FinancePlan(int numberOfHistories)
        {
            IList<PaymentActionHistory> paymentActionHistories = new List<PaymentActionHistory>();

            if (numberOfHistories > 0)
            {
                for (int i = 0; i < numberOfHistories; i++)
                {
                    // ensure correlation guid groups
                    Guid correlationGuid = Guid.NewGuid();

                    for (int x = 0; x < 2; x++)
                    {
                        paymentActionHistories.Add(new PaymentActionHistory
                        {
                            PaymentActionHistoryId = i,
                            PaymentActionType = PaymentActionTypeEnum.Reschedule,
                            CorrelationGuid = correlationGuid,
                            InsertDate = DateTime.UtcNow.AddDays(i * -1),
                            FinancePlanId = 1,
                            VpGuarantorId = VpGuarantorId,
                            ActionVisitPayUser = new VisitPayUser {VisitPayUserId = VisitPayUserId}
                        });
                    }
                }
            }

            // round 1
            bool result = this.IsEligibleToReschedule(paymentActionHistories, PaymentTypeEnum.RecurringPaymentFinancePlan);
            if (numberOfHistories < RescheduleRecurringPaymentMaximum)
            {
                Assert.True(result);
            }
            else
            {
                Assert.False(result);
            }

            // round 2 - rollback the dates, all cases should be eligible
            foreach (PaymentActionHistory paymentActionHistory in paymentActionHistories)
            {
                paymentActionHistory.InsertDate = paymentActionHistory.InsertDate.AddDays(RescheduleRecurringPaymentWindow * -1);
            }

            result = this.IsEligibleToReschedule(paymentActionHistories, PaymentTypeEnum.RecurringPaymentFinancePlan);
            Assert.True(result);
        }

        private bool IsEligibleToReschedule(IList<PaymentActionHistory> paymentActionHistories, PaymentTypeEnum paymentType)
        {
            Mock<Client> mockClient = new Mock<Client>();
            mockClient.Setup(x => x.RescheduleRecurringPaymentMaximum).Returns(RescheduleRecurringPaymentMaximum);
            mockClient.Setup(x => x.RescheduleRecurringPaymentWindow).Returns(RescheduleRecurringPaymentWindow);

            PaymentActionServiceMockBuilder builder = new PaymentActionServiceMockBuilder();
            builder.DomainServiceCommonServiceMockBuilder.ClientServiceMock.Setup(x => x.GetClient()).Returns(mockClient.Object);
            builder.PaymentActionHistoryRepositoryMock.Setup(x => x.GetPaymentActionsByType(It.IsAny<int>(), It.IsAny<int>(), PaymentActionTypeEnum.Reschedule)).Returns(paymentActionHistories);

            PaymentActionService svc = builder.CreateService();
            bool result = svc.IsEligibleToReschedule(VpGuarantorId, VisitPayUserId, paymentType);

            return result;
        }

        #endregion

        #region cancel

        private const int CancelRecurringPaymentMaximum = 2;
        private const int CancelRecurringPaymentWindow = 365;

        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public void IsEligibleToCancel_FinancePlan(int numberOfHistories)
        {
            IList<PaymentActionHistory> paymentActionHistories = new List<PaymentActionHistory>();

            if (numberOfHistories > 0)
            {
                for (int i = 0; i < numberOfHistories; i++)
                {
                    // ensure correlation guid groups
                    Guid correlationGuid = Guid.NewGuid();

                    for (int x = 0; x < 2; x++)
                    {
                        paymentActionHistories.Add(new PaymentActionHistory
                        {
                            PaymentActionHistoryId = i,
                            PaymentActionType = PaymentActionTypeEnum.Cancel,
                            CorrelationGuid = correlationGuid,
                            InsertDate = DateTime.UtcNow.AddDays(i * -1),
                            FinancePlanId = 1,
                            VpGuarantorId = VpGuarantorId,
                            ActionVisitPayUser = new VisitPayUser {VisitPayUserId = VisitPayUserId}
                        });
                    }
                }
            }

            // round 1
            bool result = this.IsEligibleToCancel(paymentActionHistories, PaymentTypeEnum.RecurringPaymentFinancePlan);
            if (numberOfHistories < CancelRecurringPaymentMaximum)
            {
                Assert.True(result);
            }
            else
            {
                Assert.False(result);
            }

            // round 2 - rollback the dates, all cases should be eligible
            foreach (PaymentActionHistory paymentActionHistory in paymentActionHistories)
            {
                paymentActionHistory.InsertDate = paymentActionHistory.InsertDate.AddDays(CancelRecurringPaymentWindow * -1);
            }

            result = this.IsEligibleToCancel(paymentActionHistories, PaymentTypeEnum.RecurringPaymentFinancePlan);
            Assert.True(result);
        }

        private bool IsEligibleToCancel(IList<PaymentActionHistory> paymentActionHistories, PaymentTypeEnum paymentType)
        {
            Mock<Client> mockClient = new Mock<Client>();
            mockClient.Setup(x => x.CancelRecurringPaymentMaximum).Returns(CancelRecurringPaymentMaximum);
            mockClient.Setup(x => x.CancelRecurringPaymentWindow).Returns(CancelRecurringPaymentWindow);

            PaymentActionServiceMockBuilder builder = new PaymentActionServiceMockBuilder();
            builder.DomainServiceCommonServiceMockBuilder.ClientServiceMock.Setup(x => x.GetClient()).Returns(mockClient.Object);
            builder.PaymentActionHistoryRepositoryMock.Setup(x => x.GetPaymentActionsByType(It.IsAny<int>(), It.IsAny<int>(), PaymentActionTypeEnum.Cancel)).Returns(paymentActionHistories);

            PaymentActionService svc = builder.CreateService();
            bool result = svc.IsEligibleToCancel(VpGuarantorId, VisitPayUserId, paymentType);

            return result;
        }
        
        #endregion
    }
}
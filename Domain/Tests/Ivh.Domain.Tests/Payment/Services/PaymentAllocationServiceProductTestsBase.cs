﻿namespace Ivh.Domain.Payment.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.Tests.Helpers.FinancePlan;
    using Common.Tests.Helpers.Guarantor;
    using Common.Tests.Helpers.Payment;
    using Common.Tests.Helpers.Visit;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using FinanceManagement.FinancePlan.Entities;
    using FinanceManagement.Payment.Entities;
    using FinanceManagement.Payment.Interfaces;
    using Guarantor.Entities;
    using NUnit.Framework;
    using Visit.Entities;

    public abstract class PaymentAllocationServiceProductTestsBase<T> : PaymentAllocationServiceTestsBase<T> where T : IPaymentAllocationService
    {
        private void AddInterestToFinancePlanVisits(Dictionary<decimal, decimal> balanceInterest, IList<Tuple<Visit, IList<VisitTransaction>>> visits, List<FinancePlanVisit> allFinancePlanVisits)
        {
            foreach (decimal amountToLookFor in balanceInterest.Keys)
            {
                Tuple<Visit, IList<VisitTransaction>> visit = visits.FirstOrDefault(x => x.Item1.CurrentBalance == amountToLookFor);
                if (visit != null)
                {
                    FinancePlanVisit fpv = allFinancePlanVisits.FirstOrDefault(v => v.VisitId == visit.Item1.VisitId);
                    if (fpv != null)
                    {
                        fpv.AddFinancePlanVisitInterestDue(balanceInterest[amountToLookFor], null, FinancePlanVisitInterestDueTypeEnum.InterestAssessment);
                    }
                }
            }
        }

        #region tests
        protected List<Payment> ProductDefined_RecurringFinancePlan(bool pastDue, Action<IPaymentAllocationService, List<Payment>> afterAction = null)
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            IList<Tuple<Visit, IList<VisitTransaction>>> visits1 = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(500m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB, visitId:1001),
                VisitFactory.GenerateActiveVisit(800m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB, visitId:1002),
                VisitFactory.GenerateActiveVisit(120m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1003)
            };
            FinancePlan fp1 = FinancePlanFactory.CreateFinancePlan(visits1, pastDue ? 2 : 1, 100m, DateTime.UtcNow.AddMonths(-1));

            IList<Tuple<Visit, IList<VisitTransaction>>> visits2 = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(80m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1004),
                VisitFactory.GenerateActiveVisit(100m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1005),
                VisitFactory.GenerateActiveVisit(700m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB, visitId:1006)
            };
            FinancePlan fp2 = FinancePlanFactory.CreateFinancePlan(visits2,  pastDue ? 2 : 1, 50m, DateTime.UtcNow.AddMonths(-1));
            

            IDictionary<FinancePlan, decimal> financePlansWithAmounts = ToDictionaryWithAmount(fp1, fp2);
            
            Payment payment1 = PaymentFactory.CreatePayment(PaymentTypeEnum.RecurringPaymentFinancePlan, financePlansWithAmounts: financePlansWithAmounts.Take(1).ToDictionary(x => x.Key, x => x.Value));
            Payment payment2 = PaymentFactory.CreatePayment(PaymentTypeEnum.RecurringPaymentFinancePlan, financePlansWithAmounts: financePlansWithAmounts.Skip(1).ToDictionary(x => x.Key, x => x.Value));

            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>();
            visits.AddRange(visits1);
            visits.AddRange(visits2);
            IPaymentAllocationService paymentAllocationService = this.GetPaymentAllocationService(visits, financePlansWithAmounts.Select(x => x.Key).ToList());
            PaymentAllocationResult result1 = paymentAllocationService.AllocatePayment(payment1);
            result1.CommitAllocationsToPayment();
            PaymentAllocationResult result2 = paymentAllocationService.AllocatePayment(payment2);
            result2.CommitAllocationsToPayment();

            this.WriteResults(payment1);
            this.WriteResults(payment2);

            List<Payment> payments = new List<Payment> {payment1, payment2};

            Assert.AreEqual(financePlansWithAmounts.Sum(x => x.Value), payments.Sum(x => x.PaymentAllocations.Sum(z => z.ActualAmount)));

            afterAction?.Invoke(paymentAllocationService, payments);

            return payments;
        }

        protected List<Payment> ProductDefined_RecurringFinancePlan_WithInterest_Base()
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            IList<Tuple<Visit, IList<VisitTransaction>>> visits1 = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(116.20m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1001),
                VisitFactory.GenerateActiveVisit(96.81m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB, visitId:1002),
            };
            FinancePlan fp1 = FinancePlanFactory.CreateFinancePlan(visits1, 1, 35m, DateTime.UtcNow.AddMonths(-1));
            
            IList<Tuple<Visit, IList<VisitTransaction>>> visits2 = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(128.54m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1003),
                VisitFactory.GenerateActiveVisit(110.16m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1004),
                VisitFactory.GenerateActiveVisit(36.71m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB, visitId:1005)
            };
            FinancePlan fp2 = FinancePlanFactory.CreateFinancePlan(visits2, 1, 50m, DateTime.UtcNow.AddMonths(-1));
            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>();
            visits.AddRange(visits1);
            visits.AddRange(visits2);

            //Add interest to the visits
            Dictionary<decimal, decimal> balanceInterest = new Dictionary<decimal, decimal>()
            {
                {116.20m, 0.39m},
                {96.81m, 0.32m},
                {128.54m, 0.43m},
                {110.16m, 0.36m},
                {36.71m, 0.12m},
            };
            List<FinancePlanVisit> allFinancePlanVisits = fp1.FinancePlanVisits.Union(fp2.FinancePlanVisits).ToList();
            this.AddInterestToFinancePlanVisits(balanceInterest, visits, allFinancePlanVisits);
            //Done adding interest
            
            IDictionary<FinancePlan, decimal> financePlansWithAmounts = ToDictionaryWithAmount(fp1, fp2);
            
            Payment payment1 = PaymentFactory.CreatePayment(PaymentTypeEnum.RecurringPaymentFinancePlan, financePlansWithAmounts: financePlansWithAmounts.Take(1).ToDictionary(x => x.Key, x => x.Value));
            Payment payment2 = PaymentFactory.CreatePayment(PaymentTypeEnum.RecurringPaymentFinancePlan, financePlansWithAmounts: financePlansWithAmounts.Skip(1).ToDictionary(x => x.Key, x => x.Value));

            IPaymentAllocationService paymentAllocationService = this.GetPaymentAllocationService(visits, financePlansWithAmounts.Select(x => x.Key).ToList());
            PaymentAllocationResult result1 = paymentAllocationService.AllocatePayment(payment1);
            result1.CommitAllocationsToPayment();
            PaymentAllocationResult result2 = paymentAllocationService.AllocatePayment(payment2);
            result2.CommitAllocationsToPayment();

            this.WriteResults(payment1);
            this.WriteResults(payment2);

            List<Payment> payments = new List<Payment> {payment1, payment2};

            Assert.AreEqual(financePlansWithAmounts.Sum(x => x.Value), payments.Sum(x => x.PaymentAllocations.Sum(z => z.ActualAmount)));

            return payments;
        }



        protected List<Payment> ProductDefined_RecurringFinancePlan_WithInterest_WithPastDue_Base()
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            IList<Tuple<Visit, IList<VisitTransaction>>> visits1 = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(155.87m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1001),
                VisitFactory.GenerateActiveVisit(124.67m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB, visitId:1002),
            };
            FinancePlan fp1 = FinancePlanFactory.CreateFinancePlan(visits1, 3, 35m, DateTime.UtcNow.AddMonths(-1));
            
            IList<Tuple<Visit, IList<VisitTransaction>>> visits2 = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(150.93m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1003),
                VisitFactory.GenerateActiveVisit(129.37m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1004),
                VisitFactory.GenerateActiveVisit(43.11m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB, visitId:1005)
            };
            FinancePlan fp2 = FinancePlanFactory.CreateFinancePlan(visits2, 2, 50m, DateTime.UtcNow.AddMonths(-1));
            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>();
            visits.AddRange(visits1);
            visits.AddRange(visits2);

            //Add interest to the visits
            Dictionary<decimal, decimal> balanceInterest = new Dictionary<decimal, decimal>()
            {
                {155.87m, 1.54m},
                {124.67m, 1.28m},
                {150.93m, 1.01m},
                {129.37m, 0.86m},
                {43.11m, 0.29m},
            };
            List<FinancePlanVisit> allFinancePlanVisits = fp1.FinancePlanVisits.Union(fp2.FinancePlanVisits).ToList();
            this.AddInterestToFinancePlanVisits(balanceInterest, visits, allFinancePlanVisits);
            //Done adding interest
            
            IDictionary<FinancePlan, decimal> financePlansWithAmounts = ToDictionaryWithAmount(fp1, fp2);
            
            Payment payment1 = PaymentFactory.CreatePayment(PaymentTypeEnum.RecurringPaymentFinancePlan, financePlansWithAmounts: financePlansWithAmounts.Take(1).ToDictionary(x => x.Key, x => x.Value));
            Payment payment2 = PaymentFactory.CreatePayment(PaymentTypeEnum.RecurringPaymentFinancePlan, financePlansWithAmounts: financePlansWithAmounts.Skip(1).ToDictionary(x => x.Key, x => x.Value));

            IList<PaymentAllocationResult> results = new List<PaymentAllocationResult>();
            IPaymentAllocationService paymentAllocationService = this.GetPaymentAllocationService(visits, financePlansWithAmounts.Select(x => x.Key).ToList(), results);

            PaymentAllocationResult result2 = paymentAllocationService.AllocatePayment(payment2);
            result2.CommitAllocationsToPayment();
            results.Add(result2);
            PaymentAllocationResult result1 = paymentAllocationService.AllocatePayment(payment1);
            result1.CommitAllocationsToPayment();
            results.Add(result1);

            this.WriteResults(payment1);
            this.WriteResults(payment2);

            List<Payment> payments = new List<Payment> {payment1, payment2};

            Assert.AreEqual(financePlansWithAmounts.Sum(x => x.Value), payments.Sum(x => x.PaymentAllocations.Sum(z => z.ActualAmount)));

            return payments;
        }

        protected Payment ProductDefined_SpecificAmount_NoFP(decimal paymentAmount)
        {
            List<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>
            {
                this.CreateVisit(75m, 1,  VisitStateEnum.Active, null, BillingApplicationConstants.HB, 1001, 1),
                this.CreateVisit(180m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.PB, 1002, 1),
                this.CreateVisit(200m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.PB, 1003, 1),
                this.CreateVisit(350m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, 1004, 0),
                this.CreateVisit(150m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.PB, 1005, 0)
            };
            
            IPaymentAllocationService svc = this.GetPaymentAllocationService(visits);

            Payment payment = PaymentFactory.CreatePayment(PaymentTypeEnum.ManualScheduledSpecificAmount, specificAmount: paymentAmount);
            PaymentAllocationResult result = svc.AllocatePayment(payment);
            result.CommitAllocationsToPayment();

            this.WriteResults(payment);

            Assert.AreEqual(paymentAmount, payment.PaymentAllocations.Sum(x => x.ActualAmount));

            return payment;
        }

        //[Ignore(ObsoleteDescription.VisitStateMachineTest)]
        protected Payment ProductDefined_SpecificAmount_WithFP(decimal paymentAmount)
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            IList<Tuple<Visit, IList<VisitTransaction>>> visits1 = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(300m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1002),
                VisitFactory.GenerateActiveVisit(300m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1003),
                VisitFactory.GenerateActiveVisit(50m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1004),
            };
            FinancePlan fp1 = FinancePlanFactory.CreateFinancePlan(visits1, 1, 25, DateTime.UtcNow.AddMonths(-5));
            
            IList<Tuple<Visit, IList<VisitTransaction>>> visits2 = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(700m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB, visitId:1005),
                VisitFactory.GenerateActiveVisit(600m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB, visitId:1006),
                VisitFactory.GenerateActiveVisit(200m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1007)
            };
            FinancePlan fp2 = FinancePlanFactory.CreateFinancePlan(visits2, 1, 25, DateTime.UtcNow.AddMonths(-1));
            
            List<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>
            {
                this.CreateVisit(300m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, 1001, 1),
                
                this.CreateVisit(350m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, 1008, null),
                this.CreateVisit(150m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, 1009, null)
            };
            visits.AddRange(visits1);
            visits.AddRange(visits2);

            List<FinancePlan> financePlans = new List<FinancePlan> {fp1, fp2};

            IPaymentAllocationService svc = this.GetPaymentAllocationService(visits, financePlans);

            Payment payment = PaymentFactory.CreatePayment(PaymentTypeEnum.ManualScheduledSpecificAmount, specificAmount: paymentAmount);
            PaymentAllocationResult result = svc.AllocatePayment(payment);
            result.CommitAllocationsToPayment();

            this.WriteResults(payment);

            Assert.AreEqual(paymentAmount, payment.PaymentAllocations.Sum(x => x.ActualAmount));

            return payment;
        }


        protected Payment ProductDefined_SpecificAmount_WithFP_WithPastDues(decimal paymentAmount)
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            IList<Tuple<Visit, IList<VisitTransaction>>> visits1 = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(800m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1001),
                VisitFactory.GenerateActiveVisit(500m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1002),
            };
            FinancePlan fp1 = FinancePlanFactory.CreateFinancePlan(visits1, 3, 25, DateTime.UtcNow.AddMonths(-1));
            
            IList<Tuple<Visit, IList<VisitTransaction>>> visits2 = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(700m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1003),
                VisitFactory.GenerateActiveVisit(600m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1004),
                VisitFactory.GenerateActiveVisit(300m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB, visitId:1005)
            };
            FinancePlan fp2 = FinancePlanFactory.CreateFinancePlan(visits2, 2, 100, DateTime.UtcNow.AddMonths(-1));
            
            List<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>
            {
                this.CreateVisit(200m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.PB, 1006, 3),
                this.CreateVisit(150m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, 1007, 1),

                this.CreateVisit(350m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.PB, 1008, null),
                this.CreateVisit(150m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, 1009, null)
            };
            visits.AddRange(visits1);
            visits.AddRange(visits2);
            
            List<FinancePlan> financePlans = new List<FinancePlan> {fp1, fp2};
            
            IPaymentAllocationService svc = this.GetPaymentAllocationService(visits, financePlans);

            Payment payment = PaymentFactory.CreatePayment(PaymentTypeEnum.ManualScheduledSpecificAmount, specificAmount: paymentAmount);
            PaymentAllocationResult result = svc.AllocatePayment(payment);
            result.CommitAllocationsToPayment();

            this.WriteResults(payment);

            Assert.AreEqual(paymentAmount, payment.PaymentAllocations.Sum(x => x.ActualAmount));

            return payment;
        }

        protected Payment ProductDefined_SpecificAmount_WithFP_WithInterest(decimal paymentAmount)
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            IList<Tuple<Visit, IList<VisitTransaction>>> visits1 = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(115.81m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1002),
                VisitFactory.GenerateActiveVisit(96.49m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB, visitId:1003),
            };
            FinancePlan fp1 = FinancePlanFactory.CreateFinancePlan(visits1, 1, 35m, DateTime.UtcNow.AddMonths(-1));
            
            IList<Tuple<Visit, IList<VisitTransaction>>> visits2 = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(128.11m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1004),
                VisitFactory.GenerateActiveVisit(109.80m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1005),
                VisitFactory.GenerateActiveVisit(36.59m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB, visitId:1006)
            };
            FinancePlan fp2 = FinancePlanFactory.CreateFinancePlan(visits2, 1, 50m, DateTime.UtcNow.AddMonths(-1));
            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>
            {
                this.CreateVisit(150m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, 1001, 1),
                this.CreateVisit(350m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, 1007, null),
                this.CreateVisit(150m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.PB, 1008, null)
            };
            visits.AddRange(visits1);
            visits.AddRange(visits2);

            //Add interest to the visits
            Dictionary<decimal, decimal> balanceInterest = new Dictionary<decimal, decimal>()
            {
                {115.81m, 0.39m},
                {96.49m, 0.32m},
                {128.11m, 0.43m},
                {109.80m, 0.36m},
                {36.59m, 0.12m},
            };
            List<FinancePlanVisit> allFinancePlanVisits = fp1.FinancePlanVisits.Union(fp2.FinancePlanVisits).ToList();
            this.AddInterestToFinancePlanVisits(balanceInterest, visits, allFinancePlanVisits);
            //Done adding interest

            List<FinancePlan> financePlans = new List<FinancePlan> {fp1, fp2};

            IPaymentAllocationService svc = this.GetPaymentAllocationService(visits, financePlans);

            Payment payment = PaymentFactory.CreatePayment(PaymentTypeEnum.ManualScheduledSpecificAmount, specificAmount: paymentAmount);
            PaymentAllocationResult result = svc.AllocatePayment(payment);
            result.CommitAllocationsToPayment();

            this.WriteResults(payment);

            Assert.AreEqual(paymentAmount, payment.PaymentAllocations.Sum(x => x.ActualAmount));

            return payment;
        }

        //[Ignore(ObsoleteDescription.VisitStateMachineTest)]
        protected Payment ProductDefined_SpecificAmount_WithFP_WithInterest_WithPastDue(decimal paymentAmount)
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            IList<Tuple<Visit, IList<VisitTransaction>>> visits1 = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(157.41m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1001),
                VisitFactory.GenerateActiveVisit(125.95m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB, visitId:1002),
            };
            FinancePlan fp1 = FinancePlanFactory.CreateFinancePlan(visits1, 3, 35m, DateTime.UtcNow.AddMonths(-1));
            
            IList<Tuple<Visit, IList<VisitTransaction>>> visits2 = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(151.94m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1003),
                VisitFactory.GenerateActiveVisit(130.23m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1004),
                VisitFactory.GenerateActiveVisit(43.40m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB, visitId:1005)
            };
            FinancePlan fp2 = FinancePlanFactory.CreateFinancePlan(visits2, 2, 50m, DateTime.UtcNow.AddMonths(-1));
            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>
            {
                this.CreateVisit(150m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, 1006, 1),
                this.CreateVisit(350m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, 1007, null),
                this.CreateVisit(150m, 1, VisitStateEnum.Active, null, BillingApplicationConstants.PB, 1008, null)
            };
            visits.AddRange(visits1);
            visits.AddRange(visits2);

            //Add interest to the visits
            Dictionary<decimal, decimal> balanceInterest = new Dictionary<decimal, decimal>()
            {
                {157.41m, 1.54m},
                {125.95m, 1.28m},
                {151.94m, 1.01m},
                {130.23m, 0.86m},
                {43.40m, 0.29m},
            };
            List<FinancePlanVisit> allFinancePlanVisits = fp1.FinancePlanVisits.Union(fp2.FinancePlanVisits).ToList();
            this.AddInterestToFinancePlanVisits(balanceInterest, visits, allFinancePlanVisits);
            //Done adding interest
            
            List<FinancePlan> financePlans = new List<FinancePlan> {fp1, fp2};

            Payment payment = PaymentFactory.CreatePayment(PaymentTypeEnum.ManualScheduledSpecificAmount, specificAmount: paymentAmount);

            IPaymentAllocationService svc = this.GetPaymentAllocationService(visits, financePlans);
            PaymentAllocationResult result = svc.AllocatePayment(payment);
            result.CommitAllocationsToPayment();

            this.WriteResults(payment);

            Assert.AreEqual(paymentAmount, payment.PaymentAllocations.Sum(x => x.ActualAmount));

            return payment;
        }

        protected Payment ProductDefined_SpecificFinancePlan(decimal specificPaymentAmount, bool pastDue = false)
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            IList<Tuple<Visit, IList<VisitTransaction>>> visits1 = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(700m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB, visitId:1001),
                VisitFactory.GenerateActiveVisit(600m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB, visitId:1002),
                VisitFactory.GenerateActiveVisit(100m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1003),
            };
            FinancePlan fp1 = FinancePlanFactory.CreateFinancePlan(visits1, pastDue ? 2 : 1, 25m, DateTime.UtcNow.AddMonths(-1));

            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>();
            visits.AddRange(visits1);
            
            IDictionary<FinancePlan, decimal> financePlansWithAmounts = new Dictionary<FinancePlan, decimal>
            {
                {fp1, specificPaymentAmount}
            };

            IPaymentAllocationService svc = this.GetPaymentAllocationService(visits, financePlansWithAmounts.Select(x => x.Key).ToList());

            Payment payment = PaymentFactory.CreatePayment(PaymentTypeEnum.ManualScheduledSpecificFinancePlans, financePlansWithAmounts: financePlansWithAmounts, specificAmount: specificPaymentAmount);
            PaymentAllocationResult result = svc.AllocatePayment(payment);
            result.CommitAllocationsToPayment();

            this.WriteResults(payment);

            Assert.AreEqual(specificPaymentAmount, payment.PaymentAllocations.Sum(x => x.ActualAmount));

            return payment;
        }

        protected Payment ProductDefined_SpecificFinancePlan_WithInterest_WithPastDue(decimal specificPaymentAmount, decimal? upwardAdjustment = null)
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            Tuple<Visit, IList<VisitTransaction>> firstVisit = VisitFactory.GenerateActiveVisit(150.93m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1001);
            IList<Tuple<Visit, IList<VisitTransaction>>> visits1 = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                firstVisit,
                VisitFactory.GenerateActiveVisit(129.37m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1002),
                VisitFactory.GenerateActiveVisit(43.11m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB, visitId:1003),
            };
            FinancePlan fp1 = FinancePlanFactory.CreateFinancePlan(visits1, 2, 50m, DateTime.UtcNow.AddMonths(-1));
            
            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>();
            visits.AddRange(visits1);

            //Add interest to the visits
            Dictionary<decimal, decimal> balanceInterest = new Dictionary<decimal, decimal>()
            {
                {150.93m, 1.01m},
                {129.37m, 0.86m},
                {43.11m, 0.29m},
            };
            List<FinancePlanVisit> allFinancePlanVisits = fp1.FinancePlanVisits.ToList();
            this.AddInterestToFinancePlanVisits(balanceInterest, visits, allFinancePlanVisits);
            //Done adding interest

            //Total 325.57
            //Interest 2.16
            //prin 323.41
            if (upwardAdjustment.HasValue)
            {
                firstVisit.Item1.CurrentBalance = firstVisit.Item1.CurrentBalance + upwardAdjustment.Value;
                FinancePlanVisit firstFinancePlanVisit = fp1.FinancePlanVisits.FirstOrDefault(x => x.VisitId == firstVisit.Item1.VisitId);
                firstFinancePlanVisit.CurrentVisitBalance = firstFinancePlanVisit.CurrentVisitBalance + upwardAdjustment.Value;
                fp1.AddFinancePlanVisitPrincipalAmountAdjustments();
            }

            IDictionary<FinancePlan, decimal> financePlansWithAmounts = new Dictionary<FinancePlan, decimal>
            {
                {fp1, specificPaymentAmount}
            };

            IPaymentAllocationService svc = this.GetPaymentAllocationService(visits, financePlansWithAmounts.Select(x => x.Key).ToList());

            Payment payment = PaymentFactory.CreatePayment(PaymentTypeEnum.ManualScheduledSpecificFinancePlans, financePlansWithAmounts: financePlansWithAmounts, specificAmount: specificPaymentAmount);
            PaymentAllocationResult result = svc.AllocatePayment(payment);
            result.CommitAllocationsToPayment();

            this.WriteResults(payment);

            if (!upwardAdjustment.HasValue)
            {
                Assert.AreEqual(specificPaymentAmount, payment.PaymentAllocations.Sum(x => x.ActualAmount));
            }

            return payment;
        }
        
        protected Payment ProductDefined_SpecificVisit(decimal paymentAmountPerVisit, bool onFinancePlan = false, bool pastDue = false)
        {
            List<FinancePlan> financePlans = null;
            Tuple<Visit, IList<VisitTransaction>> visit;

            if (onFinancePlan)
            {
                Guarantor guarantor = GuarantorFactory.GenerateOnline();
                IList<Tuple<Visit, IList<VisitTransaction>>> visits1 = new List<Tuple<Visit, IList<VisitTransaction>>>()
                {
                    VisitFactory.GenerateActiveVisit(300, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB, visitId:1001),
                };
                FinancePlan fp1 = FinancePlanFactory.CreateFinancePlan(visits1, pastDue ? 2 : 1, 25m, DateTime.UtcNow.AddMonths(-1));
            
                IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>();
                visits.AddRange(visits1);
                visit = visits1.FirstOrDefault();


                financePlans = new List<FinancePlan> {fp1};
            }
            else
            {
                visit = this.CreateVisit(300, 1, VisitStateEnum.Active, null, BillingApplicationConstants.HB, 1001, 1);
            }

            IDictionary<Tuple<Visit, IList<VisitTransaction>>, decimal> visitsWithAmounts = new Dictionary<Tuple<Visit, IList<VisitTransaction>>, decimal>
            {
                [visit] = paymentAmountPerVisit
            };

            IPaymentAllocationService svc = this.GetPaymentAllocationService(visitsWithAmounts.Select(x => x.Key).ToList(), financePlans);

            Payment payment = PaymentFactory.CreatePayment(PaymentTypeEnum.ManualScheduledSpecificVisits, visitsWithAmounts);
            PaymentAllocationResult result = svc.AllocatePayment(payment);
            result.CommitAllocationsToPayment();

            this.WriteResults(payment);

            Assert.AreEqual(visitsWithAmounts.Sum(x => x.Value), payment.PaymentAllocations.Sum(x => x.ActualAmount));

            return payment;
        }

        protected Payment ProductDefined_SpecificVisit_WithFP_WithInterest(decimal paymentAmount)
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            IList<Tuple<Visit, IList<VisitTransaction>>> visits1 = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(128.11m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1001),
                VisitFactory.GenerateActiveVisit(109.80m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1002),
                VisitFactory.GenerateActiveVisit(36.59m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB, visitId:1003),
            };
            FinancePlan fp1 = FinancePlanFactory.CreateFinancePlan(visits1, 1, 25m, DateTime.UtcNow.AddMonths(-1));
            
            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>();
            visits.AddRange(visits1);

            //Add interest to the visits
            Dictionary<decimal, decimal> balanceInterest = new Dictionary<decimal, decimal>()
            {
                {128.11m, 0.43m},
                {109.80m, 0.36m},
                {36.59m, 0.12m},
            };
            List<FinancePlanVisit> allFinancePlanVisits = fp1.FinancePlanVisits.ToList();
            this.AddInterestToFinancePlanVisits(balanceInterest, visits, allFinancePlanVisits);
            //Done adding interest

            IDictionary<Tuple<Visit, IList<VisitTransaction>>, decimal> visitsWithAmounts = new Dictionary<Tuple<Visit, IList<VisitTransaction>>, decimal>
            {
                {visits1.Take(1).FirstOrDefault(), paymentAmount},
                {visits1.Skip(1).Take(1).FirstOrDefault(), 0},
                {visits1.Skip(2).Take(1).FirstOrDefault(), 0},
            };
            

            Dictionary<Tuple<Visit, IList<VisitTransaction>>, decimal> visitsToPayWithAmounts = visitsWithAmounts.Take(1).ToDictionary(x => x.Key, x => x.Value);

            IPaymentAllocationService svc = this.GetPaymentAllocationService(visitsWithAmounts.Select(x => x.Key).ToList(), fp1.ToListOfOne());

            Payment payment = PaymentFactory.CreatePayment(PaymentTypeEnum.ManualScheduledSpecificVisits, visitsToPayWithAmounts);
            PaymentAllocationResult result = svc.AllocatePayment(payment);
            result.CommitAllocationsToPayment();

            this.WriteResults(payment);

            Assert.AreEqual(visitsToPayWithAmounts.Sum(x => x.Value), payment.PaymentAllocations.Sum(x => x.ActualAmount));

            return payment;
        }

        protected Payment ProductDefined_SpecificVisit_WithFP_WithInterest_WithPastDue(decimal paymentAmount)
        {
                        Guarantor guarantor = GuarantorFactory.GenerateOnline();
            IList<Tuple<Visit, IList<VisitTransaction>>> visits1 = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(151.94m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1001),
                VisitFactory.GenerateActiveVisit(130.23m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.PB, visitId:1002),
                VisitFactory.GenerateActiveVisit(43.40m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor, billingApplication: BillingApplicationConstants.HB, visitId:1003),
            };
            FinancePlan fp1 = FinancePlanFactory.CreateFinancePlan(visits1, 1, 25m, DateTime.UtcNow.AddMonths(-1));
            
            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>();
            visits.AddRange(visits1);

            //Add interest to the visits
            Dictionary<decimal, decimal> balanceInterest = new Dictionary<decimal, decimal>()
            {
                {151.94m, 1.01m},
                {130.23m, 0.86m},
                {43.40m, 0.29m},
            };
            List<FinancePlanVisit> allFinancePlanVisits = fp1.FinancePlanVisits.ToList();
            this.AddInterestToFinancePlanVisits(balanceInterest, visits, allFinancePlanVisits);
            //Done adding interest

            IDictionary<Tuple<Visit, IList<VisitTransaction>>, decimal> visitsWithAmounts = new Dictionary<Tuple<Visit, IList<VisitTransaction>>, decimal>
            {
                {visits1.Take(1).FirstOrDefault(), paymentAmount},
                {visits1.Skip(1).Take(1).FirstOrDefault(), 0},
                {visits1.Skip(2).Take(1).FirstOrDefault(), 0},
            };

            Dictionary<Tuple<Visit, IList<VisitTransaction>>, decimal> visitsToPayWithAmounts = visitsWithAmounts.Take(1).ToDictionary(x => x.Key, x => x.Value);

            IPaymentAllocationService svc = this.GetPaymentAllocationService(visitsWithAmounts.Select(x => x.Key).ToList(), fp1.ToListOfOne());

            Payment payment = PaymentFactory.CreatePayment(PaymentTypeEnum.ManualScheduledSpecificVisits, visitsToPayWithAmounts);
            PaymentAllocationResult result = svc.AllocatePayment(payment);
            result.CommitAllocationsToPayment();

            this.WriteResults(payment);

            Assert.AreEqual(visitsToPayWithAmounts.Sum(x => x.Value), payment.PaymentAllocations.Sum(x => x.ActualAmount));

            return payment;
        }

        #endregion
    }
}
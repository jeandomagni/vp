﻿namespace Ivh.Domain.Payment.Services
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Utilities.Extensions;
    using Common.Tests.Helpers.FinancePlan;
    using Common.Tests.Helpers.Guarantor;
    using Common.Tests.Helpers.Payment;
    using Common.Tests.Helpers.Visit;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using FinanceManagement.FinancePlan.Entities;
    using FinanceManagement.Payment.Entities;
    using FinanceManagement.Payment.Interfaces;
    using FinanceManagement.Payment.Services.PaymentAllocation;
    using Guarantor.Entities;
    using NUnit.Framework;
    using Settings.Entities;
    using Visit.Entities;

    [TestFixture]
    public class ProRataPaymentAllocationServiceGenericTests : PaymentAllocationServiceGenericTestsBase<ProRataPaymentAllocationService>
    {

        public static IEnumerable PaymentAllocationPriorityServiceTestCases
        {
            get
            {
                DateTime insertDate = DateTime.UtcNow.AddMonths(-1);
                DateTime insertDate2 = insertDate.AddDays(1);
                DateTime originationDate = DateTime.UtcNow.AddMonths(-1);

                IDictionary<string, string> clientSettings = new Dictionary<string, string>()
                {
                    {nameof(Client.BillingApplicationHBAllocationPercentage),"66"},
                    {nameof(Client.BillingApplicationPBAllocationPercentage),"34"},
                    {nameof(Client.BillingApplicationHBAllocationPriority),"1"},
                    {nameof(Client.BillingApplicationPBAllocationPriority),"2"},
                    {nameof(Client.PaymentAllocationOrderedPriorityGrouping),"[]"},
                };

                yield return new AllocationTestParameters
                {
                    TestCaseDescription = "SpecificPayment_MultipleFinancePlans_OneFPWithPastDueBuckets_PaymentPriority_Then_DischargeDate",
                    PaymentAmount = 100m,
                    PaymentType = PaymentTypeEnum.ManualScheduledSpecificAmount,
                    PaymentAllocationOrderedPriorityGrouping = "['PaymentPriority','DischargeDate']",
                    ClientSettings = clientSettings,
                    AllocationVisits = new List<AllocationVisit>
                    {
                        new AllocationVisit(1, 35.00m, BillingApplicationConstants.HB, insertDate, 0.00m, 1, 1),
                        new AllocationVisit(2, 85.85m, BillingApplicationConstants.HB, insertDate, 0.00m, 1, 1),
                        new AllocationVisit(3, 100.01m, BillingApplicationConstants.HB, insertDate, 0.00m, 1, 2),

                        new AllocationVisit(4, 300.00m, BillingApplicationConstants.HB, insertDate2, 0.00m, 2, 4),
                        new AllocationVisit(5, 700.00m, BillingApplicationConstants.HB, insertDate, 0.00m, 2, 4),
                        new AllocationVisit(6, 30.00m, BillingApplicationConstants.PB, insertDate2, 20.40m, 2, 3),
                        new AllocationVisit(7, 70.00m, BillingApplicationConstants.PB, insertDate2, 47.60m, 2, 3),
                        new AllocationVisit(8, 18.00m, BillingApplicationConstants.PB, insertDate, 18.00m, 2, 3),
                        new AllocationVisit(9, 14.00m, BillingApplicationConstants.PB, insertDate2, 14.00m, 2, 2),
                    },
                    AllocationFinancePlans = new List<AllocationFinancePlan>
                    {
                        new AllocationFinancePlan(1, 1, 50m, originationDate),
                        new AllocationFinancePlan(2, 3, 50m, originationDate),
                    }
                };

                yield return new AllocationTestParameters
                {
                    TestCaseDescription = "SpecificPayment_MultipleFinancePlans_OneFPWithPastDueBuckets_PaymentPriority",
                    PaymentAmount = 100m,
                    PaymentType = PaymentTypeEnum.ManualScheduledSpecificAmount,
                    PaymentAllocationOrderedPriorityGrouping = "['PaymentPriority']",
                    ClientSettings = clientSettings,
                    AllocationVisits = new List<AllocationVisit>
                    {
                        new AllocationVisit(1, 35.00m, BillingApplicationConstants.HB, insertDate, 0.00m, 1, 1),
                        new AllocationVisit(2, 85.85m, BillingApplicationConstants.HB, insertDate, 0.00m, 1, 1),
                        new AllocationVisit(3, 100.01m, BillingApplicationConstants.HB, insertDate, 0.00m, 1, 2),

                        new AllocationVisit(4, 300.00m, BillingApplicationConstants.HB, insertDate, 20.73m, 2, 4),
                        new AllocationVisit(5, 700.00m, BillingApplicationConstants.HB, insertDate, 48.37m, 2, 4),
                        new AllocationVisit(6, 30.00m, BillingApplicationConstants.PB, insertDate, 2.07m, 2, 4),
                        new AllocationVisit(7, 70.00m, BillingApplicationConstants.PB, insertDate, 4.83m, 2, 4),
                        new AllocationVisit(8, 24.00m, BillingApplicationConstants.PB, insertDate, 24.00m, 2, 2),
                    },
                    AllocationFinancePlans = new List<AllocationFinancePlan>
                    {
                        new AllocationFinancePlan(1, 1, 50m, originationDate),
                        new AllocationFinancePlan(2, 3, 50m, originationDate),
                    }
                };

                yield return new AllocationTestParameters
                {
                    TestCaseDescription = "SpecificPayment_MultipleFinancePlans_OneFPWithPastDueBuckets_DischargeDate",
                    PaymentAmount = 100m,
                    PaymentType = PaymentTypeEnum.ManualScheduledSpecificAmount,
                    PaymentAllocationOrderedPriorityGrouping = "['DischargeDate']",
                    ClientSettings = clientSettings,
                    AllocationVisits = new List<AllocationVisit>
                    {
                        new AllocationVisit(1, 35.00m, BillingApplicationConstants.HB, insertDate, 0.00m, 1),
                        new AllocationVisit(2, 85.85m, BillingApplicationConstants.HB, insertDate, 0.00m, 1),
                        new AllocationVisit(3, 100.01m, BillingApplicationConstants.HB, insertDate, 0.00m, 1),

                        new AllocationVisit(4, 300.00m, BillingApplicationConstants.HB, insertDate2, 0.00m, 2),
                        new AllocationVisit(5, 700.00m, BillingApplicationConstants.HB, insertDate, 95.89m, 2),
                        new AllocationVisit(6, 30.00m, BillingApplicationConstants.PB, insertDate, 4.11m, 2),
                        new AllocationVisit(7, 60.00m, BillingApplicationConstants.PB, insertDate2, 0.00m, 2),
                        new AllocationVisit(8, 40.00m, BillingApplicationConstants.PB, insertDate2, 0.00m, 2),
                    },
                    AllocationFinancePlans = new List<AllocationFinancePlan>
                    {
                        new AllocationFinancePlan(1, 1, 50m, originationDate),
                        new AllocationFinancePlan(2, 3, 50m, originationDate),
                    }
                };

                yield return new AllocationTestParameters
                {
                    TestCaseDescription = "RecurringPayment_SingleFinancePlan_DischargeDate",
                    PaymentAmount = 300m,
                    PaymentType = PaymentTypeEnum.RecurringPaymentFinancePlan,
                    PaymentAllocationOrderedPriorityGrouping = "['DischargeDate']",
                    ClientSettings = clientSettings,
                    AllocationVisits = new List<AllocationVisit>
                    {
                        new AllocationVisit(1, 100.00m, BillingApplicationConstants.PB, insertDate, 100m, 1),
                        new AllocationVisit(2, 1500.00m, BillingApplicationConstants.HB, insertDate.AddDays(1), 200m, 1),
                        new AllocationVisit(3, 5000.00m, BillingApplicationConstants.HB, insertDate.AddDays(2), 0.00m, 1),
                    },
                    AllocationFinancePlans = new List<AllocationFinancePlan>
                    {
                        new AllocationFinancePlan(1, 1, 300m, originationDate),
                    }
                };

                yield return new AllocationTestParameters
                {
                    TestCaseDescription = "RecurringPayment_SingleFinancePlan_DischargeDate_AllGetSomePayment",
                    PaymentAmount = 1800m,
                    PaymentType = PaymentTypeEnum.RecurringPaymentFinancePlan,
                    PaymentAllocationOrderedPriorityGrouping = "['DischargeDate']",
                    ClientSettings = clientSettings,
                    AllocationVisits = new List<AllocationVisit>
                    {
                        new AllocationVisit(1, 100.00m, BillingApplicationConstants.PB, insertDate, 100m, 1),
                        new AllocationVisit(2, 1500.00m, BillingApplicationConstants.HB, insertDate.AddDays(1), 1500m, 1),
                        new AllocationVisit(3, 5000.00m, BillingApplicationConstants.HB, insertDate.AddDays(2), 200m, 1),
                    },
                    AllocationFinancePlans = new List<AllocationFinancePlan>
                    {
                        new AllocationFinancePlan(1, 1, 1800m, originationDate),
                    }
                };
            }
        }

        [Test]
        [TestCaseSource(nameof(PaymentAllocationPriorityServiceTestCases))]
        public virtual void PaymentAllocationServiceTest(AllocationTestParameters testParams)
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            List<FinancePlan> financePlans = new List<FinancePlan>();
            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>();
            IDictionary<FinancePlan, decimal> financePlansWithAmounts = new Dictionary<FinancePlan, decimal>();

            foreach (AllocationFinancePlan allocationFinancePlan in testParams.AllocationFinancePlans)
            {
                IList<Tuple<Visit, IList<VisitTransaction>>> fpVisits = new List<Tuple<Visit, IList<VisitTransaction>>>();
                IEnumerable<AllocationVisit> allocationVisits = testParams.AllocationVisits.Where(x => x.FinancePlanId == allocationFinancePlan.FinancePlanId);
                foreach (AllocationVisit allocationVisit in allocationVisits)
                {
                    Tuple<Visit, IList<VisitTransaction>> fpVisit = VisitFactory.GenerateActiveVisit(allocationVisit.Amount, 1, VisitStateEnum.Active, allocationVisit.InsertDate, guarantor: guarantor, billingApplication: allocationVisit.BillingApplication, paymentPriority: allocationVisit.PaymentPriority);
                    fpVisit.Item1.VisitId = allocationVisit.VisitId;
                    fpVisits.Add(fpVisit);
                }
                FinancePlan fp = FinancePlanFactory.CreateFinancePlan(fpVisits, allocationFinancePlan.NumberOfBuckets, allocationFinancePlan.PaymentAmount, allocationFinancePlan.OriginationDate);
                financePlans.Add(fp);
                financePlansWithAmounts.Add(fp, allocationFinancePlan.PaymentAmount);
                visits.AddRange(fpVisits);
            }

            string priorityGroupingKey = nameof(Client.PaymentAllocationOrderedPriorityGrouping);
            if (testParams.ClientSettings.ContainsKey(priorityGroupingKey))
            {
                testParams.ClientSettings[priorityGroupingKey] = testParams.PaymentAllocationOrderedPriorityGrouping;
            }
            else
            {
                testParams.ClientSettings.Add(priorityGroupingKey, testParams.PaymentAllocationOrderedPriorityGrouping);
            }

            Payment payment = PaymentFactory.CreatePayment(testParams.PaymentType, specificAmount: testParams.PaymentAmount, financePlansWithAmounts: financePlansWithAmounts);
            IPaymentAllocationService svc = this.GetPaymentAllocationService(visits, financePlans, null, testParams.ClientSettings);
            PaymentAllocationResult result = svc.AllocatePayment(payment);
            result.CommitAllocationsToPayment();

            // allocate to past due finance plan first
            int? pastDueFinancePlanId = financePlans.Where(x => x.IsPastDue()).FirstOrDefault()?.FinancePlanId;
            if (pastDueFinancePlanId != null)
            {
                Assert.AreEqual(true, payment.PaymentAllocations.All(x => pastDueFinancePlanId == x.FinancePlanId));
            }

            Assert.AreEqual(testParams.PaymentAmount, payment.ActualPaymentAmount);
            Assert.AreEqual(testParams.ExpectedAllocationCount, payment.PaymentAllocations.Count);

            foreach (AllocationVisit allocationVisit in testParams.AllocationVisits)
            {
                decimal allocatedAmount = payment.PaymentAllocations.Where(x => x.PaymentVisit.VisitId == allocationVisit.VisitId).Select(x => x.ActualAmount).Sum();
                Assert.AreEqual(allocationVisit.ExpectedAllocationAmount, allocatedAmount);
            }
        }

    }
}
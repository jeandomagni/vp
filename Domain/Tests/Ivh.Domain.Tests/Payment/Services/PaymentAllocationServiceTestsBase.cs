﻿namespace Ivh.Domain.Payment.Services
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Diagnostics;
    using System.Linq;
    using AutoMapper;
    using Common.Base.Utilities.Helpers;
    using Common.Tests.Helpers.FinancePlan;
    using Common.Tests.Helpers.Payment;
    using Common.Tests.Helpers.Visit;
    using Common.Tests.Helpers.VpStatement;
    using Common.VisitPay.Enums;
    using FinanceManagement.FinancePlan.Entities;
    using FinanceManagement.Payment.Entities;
    using FinanceManagement.Payment.Interfaces;
    using FinanceManagement.Statement.Entities;
    using Guarantor.Entities;
    using Moq;
    using Settings.Entities;
    using Visit.Entities;

    public abstract class PaymentAllocationServiceTestsBase<T> : DomainTestBase where T : IPaymentAllocationService
    {
        protected PaymentVisit VisitToPaymentVisit(Visit visit)
        {
            //thinking about adding an automap profile and allowing automapper to handle this
            Mock<PaymentVisit> paymentVisitMock = new Mock<PaymentVisit>();
            paymentVisitMock.SetupGet(s => s.CurrentBalance).Returns(visit.CurrentBalance);
            paymentVisitMock.SetupGet(s => s.CurrentVisitState).Returns(visit.CurrentVisitState);
            paymentVisitMock.SetupGet(s => s.AgingTier).Returns(visit.AgingTier);
            paymentVisitMock.SetupGet(s => s.AgingCount).Returns(visit.AgingCount);
            paymentVisitMock.SetupGet(s => s.VisitId).Returns(visit.VisitId);
            paymentVisitMock.SetupGet(s => s.BillingApplication).Returns(visit.BillingApplication);
            PaymentVisit paymentVisit = paymentVisitMock.Object;
            return paymentVisit;
        }


        public const int ClientSettingDefault = 0;
        public const int ClientSettingPaymentPriority = 1;
        public const int ClientSettingDischargeDate = 2;
        public const int ClientSettingPaymentPriorityDischargeDate = 3;

        public enum ClientSettingEnum
        {
            Default,
            PaymentPriority,
            DischargeDate,
            PaymentPriorityDischargeDate
        }

        protected static Dictionary<string, string> GetClientSettings(ClientSettingEnum settingsType)
        {
            Dictionary<string, string> clientSettings = new Dictionary<string, string>
            {
                {nameof(Client.BillingApplicationHBAllocationPercentage),"66"},
                {nameof(Client.BillingApplicationPBAllocationPercentage),"34"},
                {nameof(Client.BillingApplicationHBAllocationPriority),"1"},
                {nameof(Client.BillingApplicationPBAllocationPriority),"2"},
                {nameof(Client.PaymentAllocationOrderedPriorityGrouping),"[]"}
            };
            switch (settingsType)
            {
                case ClientSettingEnum.Default:
                    break;
                case ClientSettingEnum.PaymentPriority:
                    clientSettings[nameof(Client.PaymentAllocationOrderedPriorityGrouping)] = "['PaymentPriority']";
                    break;
                case ClientSettingEnum.DischargeDate:
                    clientSettings[nameof(Client.PaymentAllocationOrderedPriorityGrouping)] = "['DischargeDate']";
                    break;
                case ClientSettingEnum.PaymentPriorityDischargeDate:
                    clientSettings[nameof(Client.PaymentAllocationOrderedPriorityGrouping)] = "['PaymentPriority','DischargeDate']";
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(settingsType), settingsType, null);
            }
            return clientSettings;
        }

        protected IPaymentAllocationService GetPaymentAllocationService(IList<Tuple<Visit, IList<VisitTransaction>>> visitsAndTransactions = null, IList<FinancePlan> financePlans = null, IList<PaymentAllocationResult> results = null, IDictionary<string, string> clientSettings = null)
        {
            visitsAndTransactions = visitsAndTransactions ?? new List<Tuple<Visit, IList<VisitTransaction>>>();
            financePlans = financePlans ?? new List<FinancePlan>();
            results = results ?? new List<PaymentAllocationResult>();

            //todo- this may go -> maybe relying on PaymentVisitRepository.GetCurrentStatementPaymentVisits
            VpStatement statement = VpStatementFactory.CreateStatement(visitsAndTransactions.Select(x => x).Where(x => x.Item1.CurrentVisitState != VisitStateEnum.NotSet).ToList());
            List<PaymentVisit> paymentVisit = visitsAndTransactions.Select(x => x.Item1).Select(this.VisitToPaymentVisit).ToList();

            //TODO: VP-149 - clean this up, because it's probably broken now. paymentVisitsOnStatement is wrong
            IList<int> activeFinancePlanVisitIds = financePlans.SelectMany(x => x.ActiveFinancePlanVisits).Select(fpv => fpv.VisitId).ToList();
            List<PaymentVisit> paymentVisitsOnStatement = paymentVisit.Where(x => x.CurrentVisitState != VisitStateEnum.NotSet && !activeFinancePlanVisitIds.Contains(x.VisitId) && x.AgingTier > 0).ToList();

            PaymentAllocationServiceMockBuilder builder = new PaymentAllocationServiceMockBuilder();
            builder.Setup(mock =>
            {
                mock.VisitServiceMock.Setup(t => t.GetAllActiveVisits(It.IsAny<Guarantor>())).Returns(visitsAndTransactions.Select(x => x.Item1).ToList);
                mock.FinancePlanServiceMock.Setup(t => t.GetAllOpenFinancePlansForGuarantor(It.IsAny<Guarantor>(), false)).Returns(financePlans);
                mock.FinancePlanServiceMock.Setup(t => t.GetFinancePlansAssociatedWithVisits(It.IsAny<IList<Visit>>())).Returns(financePlans);
                mock.FinancePlanServiceMock.Setup(t => t.GetFinancePlansById(It.IsAny<IList<int>>())).Returns((IList<int> ids) => { return financePlans.Where(x => ids.Contains(x.FinancePlanId)).ToList(); });

                mock.PaymentFinancePlanRepository.Setup(t => t.GetFinancePlanVisitsOnActiveFiancePlans(It.IsAny<IList<PaymentVisit>>())).Returns(Mapper.Map<IList<PaymentFinancePlanVisit>>(financePlans.SelectMany(x => x.FinancePlanVisits).ToList()));
                mock.PaymentFinancePlanRepository.Setup(t => t.GetPaymentFinancePlans(It.IsAny<IList<int>>())).Returns(Mapper.Map<IList<PaymentFinancePlan>>(financePlans));
                mock.PaymentFinancePlanRepository.Setup(t => t.GetPaymentFinancePlans(It.IsAny<IList<int>>())).Returns(Mapper.Map<IList<PaymentFinancePlan>>(financePlans));
                mock.PaymentFinancePlanRepository.Setup(t => t.GetActivePaymentFinancePlansFromVisits(It.IsAny<IList<PaymentVisit>>())).Returns(Mapper.Map<IList<PaymentFinancePlan>>(financePlans));

                mock.VpStatementServiceMock.Setup(t => t.GetMostRecentStatement(It.IsAny<Guarantor>())).Returns(statement);
                mock.PaymentVisitRepositoryMock.Setup(t => t.GetPaymentVisitsByVpGuarantorId(It.IsAny<int>())).Returns(paymentVisit);
                //This guy has a todo within the method. may not be reliable after future rework
                mock.PaymentVisitRepositoryMock.Setup(t => t.GetCurrentStatementPaymentVisits(It.IsAny<int>(), It.IsAny<bool>())).Returns(paymentVisitsOnStatement);
                mock.PaymentVisitRepositoryMock.Setup(t => t.GetPaymentVisits(It.IsAny<IList<int>>())).Returns((IList<int> ids) =>
                {
                    return Mapper.Map<IList<PaymentVisit>>(visitsAndTransactions.Select(x => x.Item1).Where(x => ids.Contains(x.VisitId)).ToList());
                });
                mock.PaymentAllocationRepositoryMock.Setup(t => t.GetAllPaymentAllocationAmountsForVisits(It.IsAny<IList<int>>(), It.IsAny<PaymentBatchStatusEnum>())).Returns((IList<int> ids, PaymentBatchStatusEnum status) =>
                {
                    List<PaymentAllocationVisitAmountResult> allAllocationsInResults = results.SelectMany(x => x.Allocations).Where(a => ids.Contains(a.PaymentVisit.VisitId))
                        .GroupBy(x => x.PaymentVisit.VisitId)
                        .Select((x, y) => new PaymentAllocationVisitAmountResult { ActualAmount = x.Sum(z => z.ActualAmount), VisitId = y }).ToList();
                    return allAllocationsInResults;
                });
                if (clientSettings == null)
                {
                    clientSettings = GetClientSettings(ClientSettingEnum.Default);
                }
                mock.ClientService.Setup(t => t.GetClient()).Returns(new Client(clientSettings, new Lazy<TimeZoneHelper>()));
            });

            return builder.GetPaymentAllocationService(typeof(T));
        }

        protected Tuple<Visit, IList<VisitTransaction>> CreateVisit(decimal currentBalance, int numberOfTransactions, VisitStateEnum visitState, DateTime? insertDate, string billingApplication, int? visitId, int? agingCount)
        {
            Tuple<Visit, IList<VisitTransaction>> visitAndTransactions = VisitFactory.GenerateActiveVisit(currentBalance, numberOfTransactions, visitState, insertDate, billingApplication, visitId, agingCount);

            return visitAndTransactions;
        }

        protected Tuple<Visit, IList<VisitTransaction>> CreateFinancedVisit(FinancePlan financePlan, decimal currentBalance, decimal? interestDue, int numberOfTransactions, DateTime? insertDate, string billingApplication, int? visitId)
        {
            Tuple<Visit, IList<VisitTransaction>> visitAndTransactions = this.CreateVisit(currentBalance, numberOfTransactions, VisitStateEnum.Active, insertDate, billingApplication, visitId, null);

            if (financePlan != null)
            {
                FinancePlanVisit fpv = FinancePlanFactory.VisitToFinancePlanVisit(visitAndTransactions, financePlan, interestDue.GetValueOrDefault(0));
                financePlan.FinancePlanVisits.Add(fpv);
            }

            return visitAndTransactions;
        }

        protected static IDictionary<FinancePlan, decimal> ToDictionaryWithAmount(params FinancePlan[] fps)
        {
            List<FinancePlan> list = new List<FinancePlan>();
            list.AddRange(fps);

            return list.ToDictionary(k => k, v => v.FinancePlanAmountsDue.Count == 0 ? v.PaymentAmount : v.PaymentAmount * v.FinancePlanAmountsDue.Count);
        }

        protected decimal FindAllocationByVisitId(Payment payment, int id, PaymentAllocationTypeEnum paymentAllocationType = PaymentAllocationTypeEnum.VisitPrincipal)
        {
            if (payment?.PaymentAllocations == null || !payment.PaymentAllocations.Any())
            {
                return decimal.MinValue;
            }

            PaymentAllocation allocation = payment.PaymentAllocations.FirstOrDefault(x =>
                x.PaymentVisit?.VisitId == id &&
                x.PaymentAllocationType == paymentAllocationType);

            return allocation?.ActualAmount ?? 0;
        }

        protected void WriteResults(Payment payment)
        {
            //for debugging
            foreach (IGrouping<int, PaymentAllocation> paymentAllocations in payment.PaymentAllocations.OrderBy(x => x.PaymentVisit.VisitId).GroupBy(x => x.PaymentVisit.VisitId).OrderBy(x => x.Key))
            {
                foreach (PaymentAllocation paymentAllocation in paymentAllocations)
                {
                    Trace.WriteLine(string.Format("VisitID: {0} AllocationType: {1} ({2})", paymentAllocations.Key, paymentAllocation.PaymentAllocationType, paymentAllocation.ActualAmount));
                }
            }

            Trace.WriteLine(Environment.NewLine);
        }
    }
}
﻿namespace Ivh.Domain.Payment.Entities
{
    using Common.VisitPay.Enums;
    using FinanceManagement.Payment.Entities;
    using NUnit.Framework;

    [TestFixture]
    public class PaymentTests : DomainTestBase
    {
        [Test]
        public void Payment_HasDefaultPaymentStatusOfActivePending()
        {
            Payment payment = new Payment();

            Assert.AreEqual(PaymentStatusEnum.ActivePending, payment.CurrentPaymentStatus);
            Assert.AreEqual(0, payment.PaymentStatusHistories.Count);
        }

        [Test]
        public void Payment_AssignsPaymentStatusHistory()
        {
            Payment payment = new Payment {PaymentStatus = PaymentStatusEnum.ActivePending};

            Assert.AreEqual(PaymentStatusEnum.ActivePending, payment.CurrentPaymentStatus);
            Assert.AreEqual(1, payment.PaymentStatusHistories.Count);
            Assert.AreEqual(PaymentStatusEnum.ActivePending, payment.PaymentStatusHistories[0].PaymentStatus);

            payment.PaymentStatus = PaymentStatusEnum.ClosedPaid;
            Assert.AreEqual(PaymentStatusEnum.ClosedPaid, payment.CurrentPaymentStatus);
            Assert.AreEqual(2, payment.PaymentStatusHistories.Count);
            Assert.AreEqual(PaymentStatusEnum.ClosedPaid, payment.PaymentStatusHistories[1].PaymentStatus);
            
            payment.PaymentStatus = PaymentStatusEnum.ActivePending;
            Assert.AreEqual(PaymentStatusEnum.ActivePending, payment.CurrentPaymentStatus);
            Assert.AreEqual(3, payment.PaymentStatusHistories.Count);
            Assert.AreEqual(PaymentStatusEnum.ActivePending, payment.PaymentStatusHistories[2].PaymentStatus);
        }

        [TestCase(PaymentStatusEnum.ActivePending)]
        [TestCase(PaymentStatusEnum.ClosedPaid)]
        public void Payment_DoesNotAssignPaymentStatusHistoryWhenSame(PaymentStatusEnum paymentStatus)
        {
            Payment payment = new Payment {PaymentStatus = paymentStatus};

            Assert.AreEqual(paymentStatus, payment.CurrentPaymentStatus);
            Assert.AreEqual(1, payment.PaymentStatusHistories.Count);
            Assert.AreEqual(paymentStatus, payment.PaymentStatusHistories[0].PaymentStatus);

            // set it again - shouldn't add to status history
            payment.PaymentStatus = paymentStatus;
            
            Assert.AreEqual(paymentStatus, payment.CurrentPaymentStatus);
            Assert.AreEqual(1, payment.PaymentStatusHistories.Count);
            Assert.AreEqual(paymentStatus, payment.PaymentStatusHistories[0].PaymentStatus);
        }
    }
}
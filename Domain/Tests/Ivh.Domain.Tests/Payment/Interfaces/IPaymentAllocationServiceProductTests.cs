﻿namespace Ivh.Domain.Payment.Interfaces
{
    public interface IPaymentAllocationServiceProductTests
    {
        void ProductDefined_SpecificAmount_NoFP_1();
        void ProductDefined_SpecificAmount_NoFP_2();
        void ProductDefined_SpecificAmount_WithFP();
        void ProductDefined_SpecificAmount_WithFP_WithPastDues();
        void ProductDefined_SpecificAmount_WithFP_WithInterest_1();
        void ProductDefined_SpecificAmount_WithFP_WithInterest_2();
        void ProductDefined_SpecificAmount_WithFP_WithInterest_3();
        void ProductDefined_SpecificAmount_WithFP_WithInterest_WithPastDue();


        void ProductDefined_RecurringFinancePlan();
        void ProductDefined_RecurringFinancePlan_WithPastDue();
        void ProductDefined_RecurringFinancePlan_WithInterest();
        void ProductDefined_RecurringFinancePlan_WithInterest_WithPastDue();


        void ProductDefined_SpecificFinancePlan_1();
        void ProductDefined_SpecificFinancePlan_2();
        void ProductDefined_SpecificFinancePlan_WithPastDue();
        void ProductDefined_SpecificFinancePlan_WithInterest_WithPastDue_1();
        void ProductDefined_SpecificFinancePlan_WithInterest_WithPastDue_2();


        void ProductDefined_SpecificVisit();
        void ProductDefined_SpecificVisit_WithFP();
        void ProductDefined_SpecificVisit_WithFP_WithPastDue();
        void ProductDefined_SpecificVisit_WithFP_WithInterest();
        void ProductDefined_SpecificVisit_WithFP_WithInterest_WithPastDue();
    }
}
﻿
namespace Ivh.Domain.Enterprise.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AppIntelligence.Entities;
    using AppIntelligence.Interfaces;
    using AppIntelligence.Services;
    using Base.Interfaces;
    using Common.Base.Utilities.Extensions;
    using Logging.Interfaces;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class RandomizedTestTests : DomainTestBase
    {
        private Mock<IRandomizedTestRepository> _randomizedTestRepository;
        private readonly Mock<IDomainServiceCommonService> _domainServiceCommonService = new Mock<IDomainServiceCommonService>();
        private Mock<IRandomizedTestGroupMembershipRepository> _randomizedTestGroupMembershipRepository;
        private readonly IDictionary<int,RandomizedTestGroupMembership> _randomizedTestGroupMemberships = new Dictionary<int, RandomizedTestGroupMembership>();
        private readonly Mock<IMetricsProvider> _metricsProvider = new Mock<IMetricsProvider>();

        [SetUp]
        public void Setup()
        {
            this._randomizedTestRepository = new Mock<IRandomizedTestRepository>();
            this._randomizedTestGroupMembershipRepository = new Mock<IRandomizedTestGroupMembershipRepository>();
            this._randomizedTestGroupMembershipRepository.Setup(x => x.GetManagingGuarantorIds(It.Is<int>(y => y == 100))).Returns(new List<int>() { 200 });
            this._randomizedTestGroupMembershipRepository.Setup(x => x.GetManagedGuarantorIds(It.Is<int>(y => y == 100))).Returns(new List<int>() { 300 });

            this._randomizedTestGroupMembershipRepository.Setup(x => x.GetManagingGuarantorIds(It.Is<int>(y => y == 200))).Returns(new List<int>());
            this._randomizedTestGroupMembershipRepository.Setup(x => x.GetManagedGuarantorIds(It.Is<int>(y => y == 200))).Returns(new List<int>() { 100 });
            
            this._randomizedTestGroupMembershipRepository.Setup(x => x.GetManagingGuarantorIds(It.Is<int>(y => y == 300))).Returns(new List<int>() { 100 });
            this._randomizedTestGroupMembershipRepository.Setup(x => x.GetManagedGuarantorIds(It.Is<int>(y => y == 300))).Returns(new List<int>());

            this._randomizedTestGroupMembershipRepository.Setup(x => x.Insert(It.IsAny<RandomizedTestGroupMembership>()))
                .Callback<RandomizedTestGroupMembership>((a) =>
            {
                this._randomizedTestGroupMemberships[a.VpGuarantorId] = a;
            });

            this._randomizedTestGroupMembershipRepository.Setup(x => x.GetRandomizedTestGroupMembership(It.IsAny<RandomizedTest>(), It.IsAny<int>()))
                .Returns<RandomizedTest, int>((test, i) =>
                {
                    if (!this._randomizedTestGroupMemberships.ContainsKey(i)) return null;
                    return this._randomizedTestGroupMemberships[i];
                });

        }

        [Test]
        public async Task AssignGroupMembership()
        {
            await Task.Run(() =>
            {
                decimal[] weightings = new decimal[] {0.3m, 0.25m, 0.45m};
                RandomizedTest randomizedTest = this.CreateTest(weightings);

                RandomizedTestService randomizedTestingService = new RandomizedTestService(
                    new Lazy<IDomainServiceCommonService>(() => this._domainServiceCommonService.Object),
                    new Lazy<IRandomizedTestRepository>(() => this._randomizedTestRepository.Object),
                    new Lazy<IRandomizedTestGroupMembershipRepository>(() => this._randomizedTestGroupMembershipRepository.Object),
                    new Lazy<IMetricsProvider>(() => this._metricsProvider.Object));

                randomizedTestingService.GetRandomizedTestGroupMembership(randomizedTest, 100);

                RandomizedTestGroupMembership randomizedTestGroupMembership1 = this._randomizedTestGroupMembershipRepository.Object.GetRandomizedTestGroupMembership(randomizedTest, 100);

                Assert.AreEqual(randomizedTestGroupMembership1.VpGuarantorId, 100);
                Assert.IsTrue(randomizedTestGroupMembership1.RandomizedTestGroup.Name.IsNotNullOrEmpty());
            });
        }

        [Test]
        public async Task AssignGroupMembershipConsolidatedUsers()
        {
            await Task.Run(() =>
            {
                decimal[] weightings = new decimal[] {0.3m, 0.25m, 0.45m};
                RandomizedTest randomizedTest = this.CreateTest(weightings);

                RandomizedTestService randomizedTestingService = new RandomizedTestService(
                    new Lazy<IDomainServiceCommonService>(() => this._domainServiceCommonService.Object),
                    new Lazy<IRandomizedTestRepository>(() => this._randomizedTestRepository.Object),
                    new Lazy<IRandomizedTestGroupMembershipRepository>(() => this._randomizedTestGroupMembershipRepository.Object),
                    new Lazy<IMetricsProvider>(() => this._metricsProvider.Object));

                randomizedTestingService.GetRandomizedTestGroupMembership(randomizedTest, 100);

                RandomizedTestGroupMembership randomizedTestGroupMembership1 = this._randomizedTestGroupMembershipRepository.Object.GetRandomizedTestGroupMembership(randomizedTest, 100);
                RandomizedTestGroupMembership randomizedTestGroupMembership2 = this._randomizedTestGroupMembershipRepository.Object.GetRandomizedTestGroupMembership(randomizedTest, 200);
                RandomizedTestGroupMembership randomizedTestGroupMembership3 = this._randomizedTestGroupMembershipRepository.Object.GetRandomizedTestGroupMembership(randomizedTest, 300);

                Assert.AreEqual(randomizedTestGroupMembership1.VpGuarantorId, 100);
                Assert.AreEqual(randomizedTestGroupMembership2.VpGuarantorId, 200);
                Assert.AreEqual(randomizedTestGroupMembership3.VpGuarantorId, 300);

                Assert.AreEqual(randomizedTestGroupMembership1.RandomizedTestGroup.RandomizedTestGroupId, randomizedTestGroupMembership2.RandomizedTestGroup.RandomizedTestGroupId);

                Assert.AreEqual(randomizedTestGroupMembership1.RandomizedTestGroup.RandomizedTestGroupId, randomizedTestGroupMembership3.RandomizedTestGroup.RandomizedTestGroupId);
            });

        }

        //[Test]
        /* hard ignore
         * [Ignore("fails randomly")]
        public async Task GetRandomizedWeightedGroup()
        {
            decimal[] weightings = new decimal[] { 0.3m, 0.25m, 0.45m };
            RandomizedTest randomizedTest = this.CreateTest(weightings);

            IDictionary<int, int> results = new Dictionary<int, int>();
            results[0] = 0;
            results[1] = 0;
            results[2] = 0;

            RandomizedTestService randomizedTestingService = new RandomizedTestService(
                  new Lazy<IRandomizedTestRepository>(() => this._randomizedTestRepository.Object),
                  new Lazy<IRandomizedTestGroupMembershipRepository>(() => this._randomizedTestGroupMembershipRepository.Object));

            int totalCount = 10000;
            for (int i = 0; i < totalCount; i++)
            {
                int group = randomizedTestingService.GetWeightedRandomizedTestGroup(randomizedTest).RandomizedTestGroupId;
                results[group]++;
            }
            decimal tolerance = 0.01m;
            Action<int> assert = (i) =>
            {
                decimal actualWeight = (decimal)results[i] / (decimal)totalCount;
                Console.WriteLine("Weighting for Group {0}: Expected {1:P2}; Actual {2:P2} Difference {3:P2}", i, weightings[i], actualWeight, (actualWeight - weightings[i]) / weightings[i]);
                Assert.GreaterOrEqual(actualWeight, weightings[i] - tolerance);
                Assert.LessOrEqual(actualWeight, weightings[i] + tolerance);
            };
            assert(0);
            assert(1);
            assert(2);
        }*/

        private RandomizedTest CreateTest(int numberOfGroups)
        {
            decimal weighting = 1m / (decimal)numberOfGroups;
            return this.CreateTest(Enumerable.Range(0, numberOfGroups).Select(x => weighting).ToList());
        }

        private RandomizedTest CreateTest(IList<decimal> groupWeightings)
        {
            RandomizedTest randomizedTest = new RandomizedTest()
            {
                RandomizedTestId = 1,
                BeginDateTime = DateTime.UtcNow.AddDays(-1),
                EndDateTime = DateTime.UtcNow.AddDays(1),
                Name = "Test"
            };

            for (int i = 0; i < groupWeightings.Count; i++)
            {
                randomizedTest.RandomizedTestGroups.Add(new RandomizedTestGroup()
                {
                    RandomizedTest = randomizedTest,
                    RandomizedTestGroupId = i,
                    Name = string.Format("Group {0}", i),
                    Weighting = groupWeightings[i]
                });
            }

            decimal remainder = 1m - randomizedTest.RandomizedTestGroups.Sum(x => x.Weighting);
            randomizedTest.RandomizedTestGroups[0].Weighting += remainder;

            return randomizedTest;
        }
    }
}

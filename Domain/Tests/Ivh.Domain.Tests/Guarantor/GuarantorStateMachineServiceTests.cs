﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.Guarantor
{
    using System.Collections;
    using Common.Base.Enums;
    using Common.State.Interfaces;
    using Common.State.Test;
    using Common.VisitPay.Enums;
    using Entities;
    using NUnit.Framework;
    using Rules;
    using Visit.Rules;
    using Visit.Services;

    [TestFixture]
    public class GuarantorStateMachineServiceTests : UnitTestBase<
        Guarantor,
        VpGuarantorStatusEnum,
        VpGuarantorStateRules,
        IMachine<Guarantor, VpGuarantorStatusEnum>>
    {

        public GuarantorStateMachineServiceTests() :
            base(VpGuarantorStatusEnum.NotSet, "DEFAULT")
        {
        }

        public class GuarantorStateMachineServiceTestCase : StateMachineTestCase<Guarantor, VpGuarantorStatusEnum>
        {
        }

        public static class TestGuarantorFactory
        {
            public static Guarantor NoFlags(Guarantor guarantor)
            {
                return guarantor;
            }

            public static Guarantor ChangeStateToActiveRequested(Guarantor guarantor)
            {
                guarantor.ChangeStateToActive = true;
                return guarantor;
            }

            public static Guarantor ChangeStateToClosedRequested(Guarantor guarantor)
            {
                guarantor.ChangeStateToClosed = true;
                return guarantor;
            }

        }

        public static IEnumerable GuarantorStateMachineServiceTestCases
        {
            get
            {

                yield return GuarantorStateMachineServiceTestCase.TestRule(VpGuarantorStateRules.GS_101)
                    .WithInitialState(VpGuarantorStatusEnum.NotSet)
                    .And(TestGuarantorFactory.ChangeStateToActiveRequested)
                    .ExpectsFinalState(VpGuarantorStatusEnum.Active);

                yield return GuarantorStateMachineServiceTestCase.TestRule(VpGuarantorStateRules.GS_102)
                    .WithInitialState(VpGuarantorStatusEnum.Active)
                    .And(TestGuarantorFactory.ChangeStateToClosedRequested)
                    .ExpectsFinalState(VpGuarantorStatusEnum.Closed);

                yield return GuarantorStateMachineServiceTestCase.TestRule(VpGuarantorStateRules.GS_103)
                    .WithInitialState(VpGuarantorStatusEnum.Closed)
                    .And(TestGuarantorFactory.ChangeStateToActiveRequested)
                    .ExpectsFinalState(VpGuarantorStatusEnum.Active);

                yield return GuarantorStateMachineServiceTestCase.TestRule(VpGuarantorStateRules.NOT_SET)
                    .WithInitialState(VpGuarantorStatusEnum.NotSet)
                    .And(TestGuarantorFactory.NoFlags)
                    .ExpectsFinalState(VpGuarantorStatusEnum.NotSet);

                yield return GuarantorStateMachineServiceTestCase.TestRule(VpGuarantorStateRules.ACTIVE)
                    .WithInitialState(VpGuarantorStatusEnum.Active)
                    .And(TestGuarantorFactory.NoFlags)
                    .ExpectsFinalState(VpGuarantorStatusEnum.Active);

                yield return GuarantorStateMachineServiceTestCase.TestRule(VpGuarantorStateRules.CLOSED)
                    .WithInitialState(VpGuarantorStatusEnum.Closed)
                    .And(TestGuarantorFactory.NoFlags)
                    .ExpectsFinalState(VpGuarantorStatusEnum.Closed);

            }
        }

        [Test]
        [TestCaseSource(nameof(GuarantorStateMachineServiceTestCases))]
        public void GuarantorStateMachine(StateMachineTestCase<Guarantor, VpGuarantorStatusEnum> guarantorTestCase)
        {
            this.Machine.Evaluate(guarantorTestCase.Entity);
            Assert.AreEqual(guarantorTestCase.ExpectedStateAfterEvaluation, ((IEntity<VpGuarantorStatusEnum>)guarantorTestCase.Entity).GetState() );
            Assert.AreEqual(guarantorTestCase.RuleName, ((IEntity<VpGuarantorStatusEnum>)guarantorTestCase.Entity).GetCurrentStateHistory().RuleName);
        }

    }
}

﻿namespace Ivh.Domain.Guarantor
{
    using System;
    using Common.Base.Enums;
    using Common.EventJournal.Interfaces;
    using Common.Tests.Builders;
    using Common.Tests.Helpers.Base;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.Matching.Dtos;
    using Moq;
    using NUnit.Framework;
    using Entities;
    using Interfaces;
    using Rules;
    using Services;

    [TestFixture]
    public class GuarantorServiceTests : DomainTestBase
    {
        private Guarantor _guarantor;
        private Mock<IGuarantorRepository> _guarantorRepositoryMock;
        private Mock<IEventJournalService> _eventJournalServiceMock;

        private Mock<IBillingSystemRepository> _billingSystemRepositoryMock;
        private Mock<IHsGuarantorMapRepository> _hsGuarantorMapRepositoryMock;
        private IGuarantorStateMachineService _guarantorStateMachineService;
        private const int VpGuarantorId = 1234;

        [SetUp]
        public void SetUp()
        {
            this._billingSystemRepositoryMock = new Mock<IBillingSystemRepository>();
            this._hsGuarantorMapRepositoryMock = new Mock<IHsGuarantorMapRepository>();
            this._guarantorRepositoryMock = new Mock<IGuarantorRepository>();
            this._eventJournalServiceMock = new Mock<IEventJournalService>();
            this._guarantorStateMachineService = new GuarantorStateMachineService(null, new VpGuarantorStateRules(), new Lazy<IEventJournalService>(() => this._eventJournalServiceMock.Object));
        }

        private void CreateGuarantor(VpGuarantorStatusEnum initialStatusEnum)
        {
            this._guarantor = new GuarantorBuilder().WithDefaults(VpGuarantorId).AsOnline();

            switch (initialStatusEnum)
            {
                case VpGuarantorStatusEnum.Active:
                    this._guarantor.ChangeStateToActive = true;
                    this._guarantorStateMachineService.Evaluate(this._guarantor);
                    break;
                case VpGuarantorStatusEnum.Closed:
                    this._guarantor.ChangeStateToClosed = true;
                    this._guarantorStateMachineService.Evaluate(this._guarantor);
                    break;
                case VpGuarantorStatusEnum.NotSet:
                    break;
            }
        }

        private GuarantorService CreateGuarantorService()
        {
            return new GuarantorService(
                new DomainServiceCommonServiceMockBuilder().CreateServiceLazy(),
                new Lazy<IGuarantorRepository>(() => this._guarantorRepositoryMock.Object),
                new Lazy<IGuarantorStateMachineService>(() => this._guarantorStateMachineService),
                new Lazy<IBillingSystemRepository>(() => this._billingSystemRepositoryMock.Object),
                new Lazy<IHsGuarantorMapRepository>(() => this._hsGuarantorMapRepositoryMock.Object));
        }

        [Test]
        public void WhenVpGuarantorStatus_IsAlreadyActive()
        {
            GuarantorService svc = this.CreateGuarantorService();
            this.CreateGuarantor(VpGuarantorStatusEnum.Active);

            svc.SetToActiveHsAcknowledgedPendingVisits(this._guarantor);

            Assert.AreEqual(this._guarantor.VpGuarantorStatus, VpGuarantorStatusEnum.Active);
        }

        [Test]
        public void WhenVpGuarantorStatus_IsNotSet()
        {
            GuarantorService svc = this.CreateGuarantorService();
            this.CreateGuarantor(VpGuarantorStatusEnum.NotSet);

            svc.SetToActiveHsAcknowledgedPendingVisits(this._guarantor);

            Assert.AreEqual(this._guarantor.VpGuarantorStatus, VpGuarantorStatusEnum.Active);
        }
    }
}
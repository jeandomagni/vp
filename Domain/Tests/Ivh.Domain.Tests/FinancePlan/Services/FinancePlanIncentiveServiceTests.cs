﻿namespace Ivh.Domain.FinancePlan.Services
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Tests.Helpers.FinancePlan;
    using Common.Tests.Helpers.Guarantor;
    using Common.Tests.Helpers.Payment;
    using Common.Tests.Helpers.Visit;
    using Common.VisitPay.Enums;
    using FinanceManagement.FinancePlan.Entities;
    using FinanceManagement.FinancePlan.Interfaces;
    using FinanceManagement.Payment.Entities;
    using Guarantor.Entities;
    using NUnit.Framework;
    using Visit.Entities;

    [TestFixture]
    public class FinancePlanIncentiveServiceTests
    {
        [TestCaseSource(nameof(InvalidFinancePlanStatusTestCases))]
        public void IsFinancePlanEligibleForInterestRefundIncentive_Is_False_When_Not_InGoodStanding(FinancePlanStatusEnum financePlanStatusEnum)
        {
            FinancePlanIncentiveServiceMockBuilder financePlanIncentiveServiceMockBuilder = new FinancePlanIncentiveServiceMockBuilder()
                .WithFeatureEnabled();

            IFinancePlanIncentiveService financePlanIncentiveService = financePlanIncentiveServiceMockBuilder.CreateService();

            FinancePlan financePlan = new FinancePlan();
            FinancePlanFactory.SetFinancePlanIntoStatusEnum(financePlan, financePlanStatusEnum);

            FinancePlanInterestRefundIncentiveEligibilityEnum isFinancePlanEligibleForInterestRefundIncentive = financePlanIncentiveService.IsFinancePlanEligibleForInterestRefundIncentive(financePlan);

            Assert.AreEqual(FinancePlanInterestRefundIncentiveEligibilityEnum.FinancePlanIsNotInGoodStanding, isFinancePlanEligibleForInterestRefundIncentive);
        }

        private static IEnumerable InvalidFinancePlanStatusTestCases
        {
            get
            {
                IEnumerable<FinancePlanStatusEnum> financePlanStatusEnums = Enum.GetValues(typeof(FinancePlanStatusEnum))
                    .Cast<FinancePlanStatusEnum>()
                    //Guarantor must be Good Standing to qualify
                    .Where(x => x != FinancePlanStatusEnum.GoodStanding);

                foreach (FinancePlanStatusEnum financePlanStatusEnum in financePlanStatusEnums)
                {
                    yield return new TestCaseData(
                        financePlanStatusEnum
                    );
                }
            }
        }


        [Test]
        //Guarantor is not at the end of the fp or is not within the window that triggers the refund
        public void IsFinancePlanEligibleForInterestRefundIncentive_FinancePlanDoesNotMeetRefundTriggerCriteria()
        {
            FinancePlanIncentiveServiceMockBuilder financePlanIncentiveServiceMockBuilder = new FinancePlanIncentiveServiceMockBuilder()
                .WithFeatureEnabled()
                .WithDefaultFeatureConfiguration();

            IFinancePlanIncentiveService financePlanIncentiveService = financePlanIncentiveServiceMockBuilder.CreateService();

            decimal paymentAmount = 150m;

            List<Visit> visits = new List<Tuple<Visit, IList<VisitTransaction>>>
            {
                VisitFactory.GenerateActiveVisit(5000m, 2)
            }.Select(x => x.Item1).ToList();

            FinancePlan financePlan = FinancePlanFactory.CreateFinancePlan(visits, 0, paymentAmount, DateTime.UtcNow.AddMonths(-1), 0.05m);
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            financePlan.VpGuarantor = guarantor;

            FinancePlanVisit financePlanVisit = financePlan.FinancePlanVisits.First();
            PaymentAllocation interestPaymentAllocation = new PaymentAllocationBuilder()
                .WithDefaultValues(financePlanVisit.VisitId)
                .AsRecurringFinancePlanPayment()
                .WithAmount(20)
                .AsInterest();

            financePlan.OnPaymentAllocation(-interestPaymentAllocation.ActualAmount, DateTime.UtcNow, interestPaymentAllocation);

            FinancePlanInterestRefundIncentiveEligibilityEnum isFinancePlanEligibleForInterestRefundIncentive = financePlanIncentiveService.IsFinancePlanEligibleForInterestRefundIncentive(financePlan);

            Assert.AreEqual(FinancePlanInterestRefundIncentiveEligibilityEnum.FinancePlanDoesNotMeetRefundTriggerCriteria, isFinancePlanEligibleForInterestRefundIncentive);
        }

        // We need to do 2 things: 1. refund interest. 2. Set interest rate to 0
        // If there is nothing to refund and interest rate is 0
        // then we want to return Not Eligible 
        [Test]
        public void IsFinancePlanEligibleForInterestRefundIncentive_HasNoPaidInterestAndInterestRateIsZero()
        {
            FinancePlanIncentiveServiceMockBuilder financePlanIncentiveServiceMockBuilder = new FinancePlanIncentiveServiceMockBuilder()
                .WithFeatureEnabled()
                .WithDefaultFeatureConfiguration();

            IFinancePlanIncentiveService financePlanIncentiveService = financePlanIncentiveServiceMockBuilder.CreateService();

            decimal paymentAmount = 150m;

            List<Visit> visits = new List<Tuple<Visit, IList<VisitTransaction>>>
            {
                VisitFactory.GenerateActiveVisit(5000m, 2)
            }.Select(x => x.Item1).ToList();


            //Interest Rate 0
            FinancePlan financePlan = FinancePlanFactory.CreateFinancePlan(visits, 0, paymentAmount, DateTime.UtcNow.AddMonths(-1), 0m);
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            financePlan.VpGuarantor = guarantor;

            FinancePlanVisit financePlanVisit = financePlan.FinancePlanVisits.First();

            // Pay down to where if all other conditions were met it would be eligible
            decimal amountThatReducesBalanceWithinThreshold = financePlan.CurrentFinancePlanBalance - paymentAmount * 2;
            PaymentAllocation principalPaymentAllocation = new PaymentAllocationBuilder()
                .WithDefaultValues(financePlanVisit.VisitId)
                .AsRecurringFinancePlanPayment()
                .WithAmount(amountThatReducesBalanceWithinThreshold)
                .WithPaymentAllocationType(PaymentAllocationTypeEnum.VisitPrincipal);

            financePlan.OnPaymentAllocation(-principalPaymentAllocation.ActualAmount, DateTime.UtcNow, principalPaymentAllocation);

            FinancePlanInterestRefundIncentiveEligibilityEnum isFinancePlanEligibleForInterestRefundIncentive = financePlanIncentiveService.IsFinancePlanEligibleForInterestRefundIncentive(financePlan);

            Assert.AreEqual(FinancePlanInterestRefundIncentiveEligibilityEnum.HasNoPaidInterestAndInterestRateIsZero, isFinancePlanEligibleForInterestRefundIncentive);
        }

        [Test]
        public void IsFinancePlanEligibleForInterestRefundIncentive_Is_False_FeatureIsDisabled()
        {
            FinancePlanIncentiveServiceMockBuilder financePlanIncentiveServiceMockBuilder = new FinancePlanIncentiveServiceMockBuilder()
                .WithFeatureEnabled(false);

            IFinancePlanIncentiveService financePlanIncentiveService = financePlanIncentiveServiceMockBuilder.CreateService();

            FinancePlanInterestRefundIncentiveEligibilityEnum isFinancePlanEligibleForInterestRefundIncentive = financePlanIncentiveService.IsFinancePlanEligibleForInterestRefundIncentive(new FinancePlan());

            Assert.AreEqual(FinancePlanInterestRefundIncentiveEligibilityEnum.FeatureIsNotEnabled, isFinancePlanEligibleForInterestRefundIncentive);
        }

        [Test]
        public void IsFinancePlanEligibleForInterestRefundIncentive_IsEligible()
        {
            FinancePlanIncentiveServiceMockBuilder financePlanIncentiveServiceMockBuilder = new FinancePlanIncentiveServiceMockBuilder()
                .WithFeatureEnabled()
                .WithDefaultFeatureConfiguration();

            IFinancePlanIncentiveService financePlanIncentiveService = financePlanIncentiveServiceMockBuilder.CreateService();

            decimal paymentAmount = 150m;

            List<Visit> visits = new List<Tuple<Visit, IList<VisitTransaction>>>
            {
                VisitFactory.GenerateActiveVisit(5000m, 2)
            }.Select(x => x.Item1).ToList();

            FinancePlan financePlan = FinancePlanFactory.CreateFinancePlan(visits, 0, paymentAmount, DateTime.UtcNow.AddMonths(-1), 0.05m);
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            financePlan.VpGuarantor = guarantor;

            FinancePlanVisit financePlanVisit = financePlan.FinancePlanVisits.First();
            PaymentAllocation interestPaymentAllocation = new PaymentAllocationBuilder()
                .WithDefaultValues(financePlanVisit.VisitId)
                .AsRecurringFinancePlanPayment()
                .WithAmount(20)
                .AsInterest();

            decimal amountThatReducesBalanceWithinThreshold = financePlan.CurrentFinancePlanBalance - 20 - paymentAmount * 2;
            PaymentAllocation principalPaymentAllocation = new PaymentAllocationBuilder()
                .WithDefaultValues(financePlanVisit.VisitId)
                .AsRecurringFinancePlanPayment()
                .WithAmount(amountThatReducesBalanceWithinThreshold)
                .WithPaymentAllocationType(PaymentAllocationTypeEnum.VisitPrincipal);


            financePlan.OnPaymentAllocation(-interestPaymentAllocation.ActualAmount, DateTime.UtcNow, interestPaymentAllocation);
            financePlan.OnPaymentAllocation(-principalPaymentAllocation.ActualAmount, DateTime.UtcNow, principalPaymentAllocation);

            FinancePlanInterestRefundIncentiveEligibilityEnum isFinancePlanEligibleForInterestRefundIncentive = financePlanIncentiveService.IsFinancePlanEligibleForInterestRefundIncentive(financePlan);

            Assert.AreEqual(FinancePlanInterestRefundIncentiveEligibilityEnum.IsEligible, isFinancePlanEligibleForInterestRefundIncentive);
        }

        // We need to do 2 things: 1. refund interest. 2. Set interest to 0
        // We want the finance plan to be eligible even if the person has not paid interest (perhaps the client has refunded it already)
        // if the finance plan has an interest rate. We want to prevent any future interest assessments (set interest rate to 0)
        [Test]
        public void IsFinancePlanEligibleForInterestRefundIncentive_IsEligible_Without_Paying_Interest()
        {
            FinancePlanIncentiveServiceMockBuilder financePlanIncentiveServiceMockBuilder = new FinancePlanIncentiveServiceMockBuilder()
                .WithFeatureEnabled()
                .WithDefaultFeatureConfiguration();

            IFinancePlanIncentiveService financePlanIncentiveService = financePlanIncentiveServiceMockBuilder.CreateService();


            decimal paymentAmount = 150m;

            List<Visit> visits = new List<Tuple<Visit, IList<VisitTransaction>>>
            {
                VisitFactory.GenerateActiveVisit(5000m, 2)
            }.Select(x => x.Item1).ToList();

            FinancePlan financePlan = FinancePlanFactory.CreateFinancePlan(visits, 0, paymentAmount, DateTime.UtcNow.AddMonths(-1), 0.05m);
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            financePlan.VpGuarantor = guarantor;

            FinancePlanVisit financePlanVisit = financePlan.FinancePlanVisits.First();

            decimal amountThatReducesBalanceWithinThreshold = financePlan.CurrentFinancePlanBalance - paymentAmount * 2;
            PaymentAllocation principalPaymentAllocation = new PaymentAllocationBuilder()
                .WithDefaultValues(financePlanVisit.VisitId)
                .AsRecurringFinancePlanPayment()
                .WithAmount(amountThatReducesBalanceWithinThreshold)
                .WithPaymentAllocationType(PaymentAllocationTypeEnum.VisitPrincipal);

            financePlan.OnPaymentAllocation(-principalPaymentAllocation.ActualAmount, DateTime.UtcNow, principalPaymentAllocation);

            FinancePlanInterestRefundIncentiveEligibilityEnum isFinancePlanEligibleForInterestRefundIncentive = financePlanIncentiveService.IsFinancePlanEligibleForInterestRefundIncentive(financePlan);

            Assert.AreEqual(FinancePlanInterestRefundIncentiveEligibilityEnum.IsEligible, isFinancePlanEligibleForInterestRefundIncentive);
        }
    }
}
﻿namespace Ivh.Domain.FinancePlan.Services
{
    using System;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.Tests.Helpers.FinancePlan;
    using Common.VisitPay.Enums;
    using FinanceManagement.FinancePlan.Interfaces;
    using NUnit.Framework;

    [TestFixture]
    public class InterestServiceTests : DomainTestBase
    {
        private static IInterestService CreateInterestService()
        {
            return new InterestServiceMockBuilder().CreateService();
        }

        #region Factor tests

        [TestCase(13, FirstInterestPeriodEnum.PushOnePeriod)]
        public void GetFactorForInterest_WithoutInterest(int duration, FirstInterestPeriodEnum interestFreePeriod)
        {
            IInterestService svc = CreateInterestService();

            decimal result = svc.GetFactorForInterest(duration, 0m, interestFreePeriod);
            Assert.AreEqual(result, 0m);
        }

        [TestCase(1320, 13, 0.05, (FirstInterestPeriodEnum)7, 12.92)]
        [TestCase(1320, 24, 0.05, (FirstInterestPeriodEnum)7, 23.38)]
        public void GetFactorForInterest_WithInterest(decimal total, int duration, decimal interestRate, FirstInterestPeriodEnum interestFreePeriod, decimal expectedResult)
        {
            IInterestService svc = CreateInterestService();
            decimal result = svc.GetFactorForInterest(duration, interestRate, interestFreePeriod).RoundUp();
            Assert.AreEqual(expectedResult, result);
        }

        [TestCase(12, 1.01, FirstInterestPeriodEnum.PushOnePeriod)]
        [TestCase(12, -0.05, FirstInterestPeriodEnum.PushOnePeriod)]
        [TestCase(12, 0.05, (FirstInterestPeriodEnum)(-1))]
        [TestCase(-1, 0.05, FirstInterestPeriodEnum.PushOnePeriod)]
        public void GetFactorForInterest_Throws(int duration, decimal interestRate, FirstInterestPeriodEnum interestFreePeriod)
        {
            IInterestService svc = CreateInterestService();
            Assert.Throws<Exception>(() => svc.GetFactorForInterest(duration, interestRate, interestFreePeriod));
        }

        #endregion
    }
}
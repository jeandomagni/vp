﻿namespace Ivh.Domain.FinancePlan.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Exceptions;
    using Common.Base.Utilities.Extensions;
    using Common.Tests.Helpers.FinancePlan;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Ivh.Domain.Guarantor.Entities;
    using FinanceManagement.FinancePlan.Entities;
    using FinanceManagement.FinancePlan.Interfaces;
    using FinanceManagement.FinancePlan.Services;
    using FinanceManagement.Payment.Entities;
    using FinanceManagement.Statement.Entities;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class AmortizationServiceTests : DomainTestBase
    {
        [TestCase(100.01, 1100.10, 0.0, true, 0)]
        [TestCase(100.01, 1100.10, 0.0, false, 0)]
        [TestCase(100.01, 982.03, 0.04, true, 0)]
        [TestCase(100.01, 982.03, 0.04, false, 0)]
        [TestCase(100.01, 1078.44, 0.04, true, 0)]
        [TestCase(100.01, 1078.44, 0.04, false, 0)]
        [TestCase(100.01, 985.30, 0.04, true, 0)]
        [TestCase(100.01, 985.30, 0.04, false, 0)]
        [TestCase(100.01, 1082.03, 0.04, true, 0)]
        [TestCase(100.01, 1082.03, 0.04, false, 0)]
        [TestCase(100.01, 1082.03, 0.04, false, 5.0)]
        public void AllEntryPointsToAmortizeReturnSameValues(decimal monthlyPaymentAmount, decimal visitAmount, decimal interestRate, bool isGracePeriodOfCreatedStatement, decimal interestDue)
        {
            Action<bool> runTest = (isCombined) =>
            {
                DateTime paymentDueDate = DateTime.UtcNow;

                IFinancePlanVisitBalance visit = CreateVisit(visitAmount, 0m, 1, BillingApplicationConstants.HB, interestDue);
                IList<IFinancePlanVisitBalance> visits = visit.ToListOfOne();
                FinancePlan originatedFinancePlan = CreateFinancePlan(visits, monthlyPaymentAmount, interestRate, isGracePeriodOfCreatedStatement, isCombined);
                FirstInterestPeriodEnum interestFreePeriod = originatedFinancePlan.FirstInterestPeriod();
                
                // amortize by months/amount
                IList<AmortizationMonth> months1 = CreateService(false).GetAmortizationMonthsForAmounts(monthlyPaymentAmount, visits, interestRate, interestFreePeriod, paymentDueDate, InterestCalculationMethodEnum.Monthly);
                Assert.AreEqual(visitAmount, months1.Sum(x => x.PrincipalAmount));

                // verify with fp
                IList<AmortizationMonth> months2 = CreateService(false).GetAmortizationMonthsForOriginatedFinancePlan(originatedFinancePlan, paymentDueDate, null);
                Assert.AreEqual(months1.Count, months2.Count);
                Assert.AreEqual(months1.Sum(x => x.PrincipalAmount), months2.Sum(x => x.PrincipalAmount));
                Assert.AreEqual(months1.Sum(x => x.InterestAmount), months2.Sum(x => x.InterestAmount));

                // total duration for fp
                int? duration3 = CreateService(false).CalculateNumberOfTotalCurrentDuration(originatedFinancePlan, null);
                Assert.AreEqual(months1.Count, duration3);

                // pending fp
                FinancePlan pendingFinancePlan = CreateFinancePlan(visits, monthlyPaymentAmount, interestRate, false, isCombined);
                IList<AmortizationMonth> months3 = CreateService(false).GetAmortizationMonthsForPendingFinancePlan(pendingFinancePlan, interestFreePeriod, paymentDueDate);
                Assert.AreEqual(months1.Count, months3.Count);
                Assert.AreEqual(months1.Sum(x => x.PrincipalAmount), months3.Sum(x => x.PrincipalAmount));
                Assert.AreEqual(months1.Sum(x => x.InterestAmount), months3.Sum(x => x.InterestAmount));

                // verify the duration with amounts
                int durationWithAmounts = CreateService(false).GetAmortizationDurationForAmounts(monthlyPaymentAmount, visits, originatedFinancePlan.FinancePlanOffer.InterestRate, interestFreePeriod, DateTime.UtcNow, InterestCalculationMethodEnum.Monthly);
                Assert.AreEqual(months1.Count, durationWithAmounts);
            };

            runTest(true);
            runTest(false);
        }

        [TestCase(57.02, 1320, 0.05, 4, 24, InterestCalculationMethodEnum.Monthly)]
        [TestCase(2099.99, 10000, 0.05, 1, 5, InterestCalculationMethodEnum.Monthly)]

        [TestCase(2009.99, 10000, 0.05, 1, 6, InterestCalculationMethodEnum.Daily)]
        [TestCase(2013.99, 10000, 0.05, 1, 5, InterestCalculationMethodEnum.Daily)]
        [TestCase(2013.00, 10000, 0.05, 1, 6, InterestCalculationMethodEnum.Daily)]
        [TestCase(109.1, 3501.05, 0.08, 1, 36, InterestCalculationMethodEnum.Daily)]
        [TestCase(108.75, 3501.05, 0.08, 1, 36, InterestCalculationMethodEnum.Daily)]
        [TestCase(2016.67, 10000, 0.05, 0, 6, InterestCalculationMethodEnum.Daily)]
        [TestCase(2511.36, 10000, 0.05, 1, 4, InterestCalculationMethodEnum.Daily)]
        public void GetAmortizationMonthsForAmounts_WithKnownValues_Amortizes(decimal monthlyPaymentAmount, decimal visitBalance, decimal interestRate, int interestFreePeriod, int expectedMonths, InterestCalculationMethodEnum interestCalculationMethod)
        {
            IAmortizationService svc = CreateService(false);
            IList<AmortizationMonth> check = svc.GetAmortizationMonthsForAmounts(monthlyPaymentAmount,
                CreateFinancePlanVisitsFromAmount(visitBalance),
                interestRate,
                (FirstInterestPeriodEnum)interestFreePeriod,
                new DateTime(2018,7,31),
                //new DateTime(2018,10,7),
                interestCalculationMethod);
            
            Assert.AreEqual(expectedMonths, check.Count);
        }
        
        [TestCase(57.02, -10, 0.05, 4)]
        [TestCase(57.02, 0, 0.05, 4)]
        [TestCase(-0.02, 900, 0.05, 4)]
        [TestCase(0, 900, 0.05, 4)]
        public void GetAmortizationMonthsForAmounts_DoesNotAmortize(decimal monthlyPaymentAmount, decimal visitBalance, decimal interestRate, int interestFreePeriod)
        {
            IAmortizationService svc = CreateService(false);
            IList<AmortizationMonth> result = svc.GetAmortizationMonthsForAmounts(monthlyPaymentAmount, CreateFinancePlanVisitsFromAmount(visitBalance), interestRate, (FirstInterestPeriodEnum)interestFreePeriod, DateTime.MinValue, InterestCalculationMethodEnum.Monthly);
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Count, 0);
        }

        //[TestCase(InterestCalculationMethodEnum.Daily)]
        [TestCase(InterestCalculationMethodEnum.Monthly)]
        [Test]
        public void GetAmortizationMonthsForAmounts_LastMonthIsAlwaysTheSameAmount(InterestCalculationMethodEnum interestCalculationMethod)
        {
            IAmortizationService svc = CreateService(false);

            const int duration = 36;
            const decimal monthlyAmount = 93.40m;
            const decimal startingBalance = 3000m;
            const decimal yearlyInterestRate = 0.085m;
            
            int interestFreePeriod = (int)FirstInterestPeriodEnum.PushOnePeriod;
            decimal balance = startingBalance;
            for (int i = 0; i < duration; i++)
            {
                int overridesRemaining = i < 2 ? 1 : 0;
                IFinancePlanVisitBalance visit = CreateVisit(balance, 0m, overridesRemaining);
                IList<AmortizationMonth> check = svc.GetAmortizationMonthsForAmounts(monthlyAmount, visit.ToListOfOne(), yearlyInterestRate, (FirstInterestPeriodEnum)interestFreePeriod, DateTime.MinValue, interestCalculationMethod: interestCalculationMethod);
                balance = balance - check.First().PrincipalAmount;

                Assert.AreEqual(93.24m, check.Last().Total);

                interestFreePeriod = Math.Max(interestFreePeriod - 1, 0);
            }
        }
        
        [TestCase(57.02, 1320, 1.05, 4)]
        [TestCase(57.02, 1320, -0.05, 4)]
        public void Amortize_Throws(decimal monthlyPaymentAmount, decimal visitBalance, decimal interestRate, int interestFreePeriod)
        {
            AmortizationService svc = (AmortizationService) CreateService(false);
            Assert.Throws<Exception>(() => svc.Amortize(monthlyPaymentAmount, CreateFinancePlanVisitsFromAmount(visitBalance), interestRate, interestFreePeriod, DateTime.UtcNow, null, InterestCalculationMethodEnum.Monthly));
        }
        
        [TestCase(100, 12400, 0.05, 4, InterestCalculationMethodEnum.Monthly)]
        [TestCase(100, 15098.51, 0.04, 1, InterestCalculationMethodEnum.Monthly)]
        [TestCase(50, 15098.51, 0.04, 1, InterestCalculationMethodEnum.Daily)]
        [TestCase(50, 12400, 0.05, 4, InterestCalculationMethodEnum.Daily)]
        public void Amortize_Throws_NegativeAmortization(decimal monthlyPaymentAmount, decimal visitBalance, decimal interestRate, int interestFreePeriod, InterestCalculationMethodEnum interestCalculationMethod)
        {
            AmortizationService svc = (AmortizationService) CreateService(false);
            Assert.Throws<NegativeAmortizationException>(() => svc.Amortize(monthlyPaymentAmount, CreateFinancePlanVisitsFromAmount(visitBalance, 0), interestRate, interestFreePeriod, DateTime.UtcNow, null, interestCalculationMethod));
        }

        // expected results are what the app does when processing this plan all the way through, month by month
        [TestCase(true, true, 94.90, InterestCalculationMethodEnum.Monthly)]
        [TestCase(true, false, 107.43, InterestCalculationMethodEnum.Monthly)]
        [TestCase(false, true, 94.91, InterestCalculationMethodEnum.Monthly)]
        [TestCase(false, false, 107.42, InterestCalculationMethodEnum.Monthly)]
        public void Stacked_Case1(bool useBillingApplications, bool createdInGracePeriod, decimal expectedInterest, InterestCalculationMethodEnum interestCalculationMethod)
        {
            FirstInterestPeriodEnum interestFreePeriod = FinancePlan.FirstInterestPeriod(createdInGracePeriod);
            const int duration = 19;
            const decimal monthlyPaymentAmount = 100m;
            const decimal interestRate = 0.08m;

            IList<IFinancePlanVisitBalance> visits = new List<IFinancePlanVisitBalance>
            {
                CreateVisit(1000m, 0m, 1, BillingApplicationConstants.HB),
                CreateVisit(777.77m, 0m, 1, BillingApplicationConstants.PB)
            };

            this.ValidateAmortization(useBillingApplications, visits, duration, monthlyPaymentAmount, interestRate, interestFreePeriod, expectedInterest, interestCalculationMethod: interestCalculationMethod);
        }

        // expected results are what the app does when processing this plan all the way through, month by month
        [TestCase(false, false, 78.44, InterestCalculationMethodEnum.Monthly)]
        [TestCase(true, false, 78.45, InterestCalculationMethodEnum.Monthly)]
        public void Stacked_Case2(bool useBillingApplications, bool createdInGracePeriod, decimal expectedInterest, InterestCalculationMethodEnum interestCalculationMethod)
        {
            FirstInterestPeriodEnum interestFreePeriod = FinancePlan.FirstInterestPeriod(createdInGracePeriod);
            const int duration = 10;
            const decimal monthlyPaymentAmount = 303m;
            const decimal interestRate = 0.08m;

            IList<IFinancePlanVisitBalance> visits = new List<IFinancePlanVisitBalance>
            {
                CreateVisit(827.17m, 0m, 1, BillingApplicationConstants.HB),
                CreateVisit(1567.89m, 0m, 1, BillingApplicationConstants.HB),
                CreateVisit(323.01m, 0m, 1, BillingApplicationConstants.PB),
                CreateVisit(56.23m, 0m, 1, BillingApplicationConstants.PB)
            };

            this.ValidateAmortization(useBillingApplications, visits, duration, monthlyPaymentAmount, interestRate, interestFreePeriod, expectedInterest, interestCalculationMethod: interestCalculationMethod);
        }

        // expected results are what the app does when processing this plan all the way through, month by month
        [TestCase(true, false, 110.96, InterestCalculationMethodEnum.Monthly)]
        [TestCase(false, false, 110.98, InterestCalculationMethodEnum.Monthly)]
        public void Stacked_Case3(bool useBillingApplications, bool createdInGracePeriod, decimal expectedInterest, InterestCalculationMethodEnum interestCalculationMethod)
        {
            FirstInterestPeriodEnum interestFreePeriod = FinancePlan.FirstInterestPeriod(createdInGracePeriod);
            const int duration = 18;
            const decimal monthlyPaymentAmount = 200m;
            const decimal interestRate = 0.05m;

            IList<IFinancePlanVisitBalance> visits = new List<IFinancePlanVisitBalance>
            {
                CreateVisit(1543.78m, 0m, 1, BillingApplicationConstants.PB),
                CreateVisit(1739.13m, 0m, 1, BillingApplicationConstants.PB),
                CreateVisit(12.24m, 0m, 1, BillingApplicationConstants.HB)
            };

            this.ValidateAmortization(useBillingApplications, visits, duration, monthlyPaymentAmount, interestRate, interestFreePeriod, expectedInterest, interestCalculationMethod: interestCalculationMethod);
        }

        // expected results are what the app does when processing this plan all the way through, month by month
        //[TestCase(InterestCalculationMethodEnum.Daily)]
        [TestCase(InterestCalculationMethodEnum.Monthly)]
        [Test]
        public void Combo_Case1(InterestCalculationMethodEnum interestCalculationMethod)
        {
            FirstInterestPeriodEnum interestFreePeriod = FinancePlan.FirstInterestPeriod(false);
            const int duration = 36;
            const decimal monthlyPaymentAmount = 198.30m;
            const decimal interestRate = 0.03m;

            IList<IFinancePlanVisitBalance> visits = new List<IFinancePlanVisitBalance>
            {
                CreateVisit(578.07m, 0.05m, 1, BillingApplicationConstants.HB),
                CreateVisit(359.42m, 0.05m, 1, BillingApplicationConstants.PB),
                CreateVisit(1070.68m, 0.05m, 1, BillingApplicationConstants.HB),

                CreateVisit(694.89m, 0.08m, 1, BillingApplicationConstants.PB),

                CreateVisit(1600.49m, 0.00m, 1, BillingApplicationConstants.HB),
                CreateVisit(1701.42m, 0.00m, 1, BillingApplicationConstants.HB),
                CreateVisit(817.32m, 0.00m, 1, BillingApplicationConstants.PB),
            };

            this.ValidateAmortization(true, visits, duration, monthlyPaymentAmount, interestRate, interestFreePeriod, 315.77m, true, interestCalculationMethod: interestCalculationMethod);
        }

        // expected results are what the app does when processing this plan all the way through, month by month
        [TestCase(InterestCalculationMethodEnum.Monthly)]
        [Test]
        public void Combo_Case2_WithInterestCarriedFromPreviousPlan_InGracePeriod(InterestCalculationMethodEnum interestCalculationMethod)
        {
            FirstInterestPeriodEnum interestFreePeriod = FinancePlan.FirstInterestPeriod(true);
            const int duration = 6;
            const decimal monthlyPaymentAmount = 500m;
            const decimal interestRate = 0.08m;

            IList<IFinancePlanVisitBalance> visits = new List<IFinancePlanVisitBalance>
            {
                CreateVisit(1077.11m, 0.03m, 1, BillingApplicationConstants.PB, 2.69m),
                CreateVisit(934.00m, 0.03m, 1, BillingApplicationConstants.HB, 2.34m),

                CreateVisit(555.00m, 0.0m, 1, BillingApplicationConstants.HB)
            };

            this.ValidateAmortization(true, visits, duration, monthlyPaymentAmount, interestRate, interestFreePeriod, 31.45m, true, interestCalculationMethod: interestCalculationMethod);
        }

        [TestCase(InterestCalculationMethodEnum.Daily)]
        [Test]
        public void Combo_Case1_Daily_WithInterestCarriedFromPreviousPlan_InAwaitingStatement(InterestCalculationMethodEnum interestCalculationMethod)
        {
            FirstInterestPeriodEnum interestFreePeriod = FinancePlan.FirstInterestPeriod(false);
            const int duration = 4;
            const decimal monthlyPaymentAmount = 4034.67m;
            const decimal interestRate = 0.05m;

            IList<IFinancePlanVisitBalance> visits = new List<IFinancePlanVisitBalance>
            {
                CreateVisit(5973.22m, 0m, 1, BillingApplicationConstants.HB, 30.34m),

                CreateVisit(10000m, 0m, 1, BillingApplicationConstants.HB)
            };

            this.ValidateAmortization(true, visits, duration, monthlyPaymentAmount, interestRate, interestFreePeriod, 164.91m, true, dateToCheck: new DateTime(2018,11,7),  interestCalculationMethod: interestCalculationMethod);
        }

        //[TestCase(InterestCalculationMethodEnum.Daily)]
        [TestCase(InterestCalculationMethodEnum.Monthly)]
        [Test]
        public void Combo_Case3_WithInterestCarriedFromPreviousPlan_InGracePeriod(InterestCalculationMethodEnum interestCalculationMethod)
        {
            FirstInterestPeriodEnum interestFreePeriod = FinancePlan.FirstInterestPeriod(true);
            const int duration = 36;
            const decimal monthlyPaymentAmount = 71.56m;
            const decimal interestRate = 0.00m;

            IList<IFinancePlanVisitBalance> visits = new List<IFinancePlanVisitBalance>
            {
                CreateVisit(1077.11m, 0.03m, 1, BillingApplicationConstants.PB, 2.69m),
                CreateVisit(934.00m, 0.03m, 1, BillingApplicationConstants.HB, 2.34m),

                CreateVisit(555.00m, 0.0m, 1, BillingApplicationConstants.HB)
            };

            this.ValidateAmortization(true, visits, duration, monthlyPaymentAmount, interestRate, interestFreePeriod, 9.94m, true, interestCalculationMethod: interestCalculationMethod);
        }

        //[TestCase(InterestCalculationMethodEnum.Daily)]
        [TestCase(InterestCalculationMethodEnum.Monthly)]
        [Test]
        public void Combo_Case4_WithInterestCarriedFromPreviousPlan_InGracePeriod(InterestCalculationMethodEnum interestCalculationMethod)
        {
            FirstInterestPeriodEnum interestFreePeriod = FinancePlan.FirstInterestPeriod(true);
            const int duration = 36;
            const decimal monthlyPaymentAmount = 74.55m;
            const decimal interestRate = 0.03m;

            IList<IFinancePlanVisitBalance> visits = new List<IFinancePlanVisitBalance>
            {
                CreateVisit(1077.11m, 0.03m, 1, BillingApplicationConstants.PB, 2.69m),
                CreateVisit(934.00m, 0.03m, 1, BillingApplicationConstants.HB, 2.34m),

                CreateVisit(555.00m, 0.0m, 1, BillingApplicationConstants.HB)
            };

            this.ValidateAmortization(true, visits, duration, monthlyPaymentAmount, interestRate, interestFreePeriod, 117.57m, true, interestCalculationMethod: interestCalculationMethod);
        }

        //[TestCase(InterestCalculationMethodEnum.Daily)]
        [TestCase(InterestCalculationMethodEnum.Monthly)]
        [Test]
        public void Reconfig_Case1_WithInterestCarriedFromPreviousPlan_InGracePeriod(InterestCalculationMethodEnum interestCalculationMethod)
        {
            FirstInterestPeriodEnum interestFreePeriod = FinancePlan.FirstInterestPeriod(true);
            const int duration = 36;
            const decimal monthlyPaymentAmount = 56.14m;
            const decimal interestRate = 0.00m;

            IList<IFinancePlanVisitBalance> visits = new List<IFinancePlanVisitBalance>
            {
                CreateVisit(1077.11m, 0.03m, 1, BillingApplicationConstants.PB, 2.69m),
                CreateVisit(934.00m, 0.03m, 1, BillingApplicationConstants.HB, 2.34m)
            };

            this.ValidateAmortization(true, visits, duration, monthlyPaymentAmount, interestRate, interestFreePeriod, 9.93m, true, interestCalculationMethod: interestCalculationMethod);
        }

        //[TestCase(InterestCalculationMethodEnum.Daily)]
        [TestCase(InterestCalculationMethodEnum.Monthly)]
        [Test]
        public void Reconfig_Case2_WithInterestCarriedFromPreviousPlan_InGracePeriod(InterestCalculationMethodEnum interestCalculationMethod)
        {
            FirstInterestPeriodEnum interestFreePeriod = FinancePlan.FirstInterestPeriod(true);
            const int duration = 36;
            const decimal monthlyPaymentAmount = 58.49m;
            const decimal interestRate = 0.03m;

            IList<IFinancePlanVisitBalance> visits = new List<IFinancePlanVisitBalance>
            {
                CreateVisit(1077.11m, 0.03m, 1, BillingApplicationConstants.PB, 2.69m),
                CreateVisit(934.00m, 0.03m, 1, BillingApplicationConstants.HB, 2.34m)
            };

            this.ValidateAmortization(true, visits, duration, monthlyPaymentAmount, interestRate, interestFreePeriod, 94.36m, true, interestCalculationMethod: interestCalculationMethod);
        }

        [TestCase(true, false, InterestCalculationMethodEnum.Daily)]
        [TestCase(true, true, InterestCalculationMethodEnum.Daily)]
        [TestCase(false, false, InterestCalculationMethodEnum.Daily)]
        [TestCase(false, true, InterestCalculationMethodEnum.Daily)]
        [TestCase(true, false, InterestCalculationMethodEnum.Monthly)]
        [TestCase(true, true, InterestCalculationMethodEnum.Monthly)]
        [TestCase(false, false, InterestCalculationMethodEnum.Monthly)]
        [TestCase(false, true, InterestCalculationMethodEnum.Monthly)]
        public void WithCalculatedMinimumPayment_Amortizes_1(bool useBillingApplications, bool createdInGracePeriod, InterestCalculationMethodEnum interestCalculationMethod)
        {
            FirstInterestPeriodEnum interestFreePeriod = FinancePlan.FirstInterestPeriod(createdInGracePeriod);
            const int duration = 36;
            const decimal interestRate = 0.08m;

            IList<IFinancePlanVisitBalance> visits = new List<IFinancePlanVisitBalance>
            {
                CreateVisit(2000.05m, 0m, 1, BillingApplicationConstants.PB),
                CreateVisit(999.78m, 0m, 1, BillingApplicationConstants.PB),
                CreateVisit(123.45m, 0m, 1, BillingApplicationConstants.HB),
                CreateVisit(377.77m, 0m, 1, BillingApplicationConstants.HB)
            };

            this.ValidateMinimum(useBillingApplications, visits, duration, interestRate, interestFreePeriod, interestCalculationMethod: interestCalculationMethod);
        }

        [TestCase(true, false)]
        [TestCase(true, true)]
        [TestCase(false, false)]
        [TestCase(false, true)]
        public void WithCalculatedMinimumPayment_Amortizes_2(bool useBillingApplications, bool createdInGracePeriod)
        {
            FirstInterestPeriodEnum interestFreePeriod = FinancePlan.FirstInterestPeriod(createdInGracePeriod);
            const int duration = 36;
            const decimal interestRate = 0.05m;

            IList<IFinancePlanVisitBalance> visits = new List<IFinancePlanVisitBalance>
            {
                CreateVisit(666.55m, 0m, 1, BillingApplicationConstants.HB),
                CreateVisit(489.42m, 0m, 1, BillingApplicationConstants.PB),
                CreateVisit(1234.56m, 0m, 1, BillingApplicationConstants.HB)
            };

            this.ValidateMinimum(useBillingApplications, visits, duration, interestRate, interestFreePeriod);
        }

        [TestCase(true, false)]
        [TestCase(true, true)]
        [TestCase(false, false)]
        [TestCase(false, true)]
        public void WithCalculatedMinimumPayment_Amortizes_3(bool useBillingApplications, bool createdInGracePeriod)
        {
            FirstInterestPeriodEnum interestFreePeriod = FinancePlan.FirstInterestPeriod(createdInGracePeriod);
            const int duration = 36;
            const decimal interestRate = 0.03m;

            IList<IFinancePlanVisitBalance> visits = new List<IFinancePlanVisitBalance>
            {
                CreateVisit(578.07m, 0.05m, 1, BillingApplicationConstants.HB),
                CreateVisit(359.42m, 0.05m, 1, BillingApplicationConstants.PB),
                CreateVisit(1070.68m, 0.05m, 1, BillingApplicationConstants.HB),
                CreateVisit(694.89m, 0.08m, 1, BillingApplicationConstants.PB),
                CreateVisit(1600.49m, 0m, 1, BillingApplicationConstants.HB),
                CreateVisit(1701.42m, 0m, 1, BillingApplicationConstants.HB),
                CreateVisit(817.32m, 0m, 1, BillingApplicationConstants.PB),
            };

            this.ValidateMinimum(useBillingApplications, visits, duration, interestRate, interestFreePeriod);
        }


        [TestCase(true, false)]
        [TestCase(true, true)]
        [TestCase(false, false)]
        [TestCase(false, true)]
        public void WithCalculatedMinimumPayment_Amortizes_4(bool useBillingApplications, bool createdInGracePeriod)
        {
            FirstInterestPeriodEnum interestFreePeriod = FinancePlan.FirstInterestPeriod(createdInGracePeriod);
            const int duration = 36;
            const decimal interestRate = 0.03m;

            IList<IFinancePlanVisitBalance> visits = new List<IFinancePlanVisitBalance>
            {
                CreateVisit(578.07m, 0.05m, 1, BillingApplicationConstants.HB),
                CreateVisit(359.42m, 0.05m, 1, BillingApplicationConstants.PB)
            };

            this.ValidateMinimum(useBillingApplications, visits, duration, interestRate, interestFreePeriod);
        }

        [TestCase(true, false)]
        [TestCase(true, true)]
        [TestCase(false, false)]
        [TestCase(false, true)]
        public void WithCalculatedMinimumPayment_Amortizes_5(bool useBillingApplications, bool createdInGracePeriod)
        {
            FirstInterestPeriodEnum interestFreePeriod = FinancePlan.FirstInterestPeriod(createdInGracePeriod);
            const int duration = 36;
            const decimal interestRate = 0.08m;

            IList<IFinancePlanVisitBalance> visits = new List<IFinancePlanVisitBalance>
            {
                CreateVisit(1919.63m, 0.05m, 1, BillingApplicationConstants.HB),
                CreateVisit(9229.12m, 0.05m, 1, BillingApplicationConstants.PB),
                CreateVisit(7585.89m, 0.08m, 1, BillingApplicationConstants.PB),
                CreateVisit(601.34m, 0.0m, 1, BillingApplicationConstants.PB),
            };

            this.ValidateMinimum(useBillingApplications, visits, duration, interestRate, interestFreePeriod);
        }

        [TestCase(true, false)]
        [TestCase(true, true)]
        [TestCase(false, false)]
        [TestCase(false, true)]
        public void WithCalculatedMinimumPayment_Amortizes_6(bool useBillingApplications, bool createdInGracePeriod)
        {
            FirstInterestPeriodEnum interestFreePeriod = FinancePlan.FirstInterestPeriod(createdInGracePeriod);
            const int duration = 36;
            const decimal interestRate = 0.08m;

            IList<IFinancePlanVisitBalance> visits = new List<IFinancePlanVisitBalance>
            {
                CreateVisit(1919.63m, 0.05m, 1, BillingApplicationConstants.HB),
                CreateVisit(9229.12m, 0.05m, 1, BillingApplicationConstants.PB),
                CreateVisit(7585.89m, 0.08m, 1, BillingApplicationConstants.PB),
                CreateVisit(601.34m, 0.0m, 1, BillingApplicationConstants.PB),
                CreateVisit(2662.54m, 0.05m, 1, BillingApplicationConstants.HB),
                CreateVisit(7671.23m, 0.05m, 1, BillingApplicationConstants.PB),
                CreateVisit(1035.99m, 0.08m, 1, BillingApplicationConstants.PB),
                CreateVisit(2810m, 0.0m, 1, BillingApplicationConstants.PB),
            };

            this.ValidateMinimum(useBillingApplications, visits, duration, interestRate, interestFreePeriod);
        }

        [TestCase(true, false)]
        [TestCase(true, true)]
        [TestCase(false, false)]
        [TestCase(false, true)]
        public void WithCalculatedMinimumPayment_Amortizes_Daily(bool useBillingApplications, bool createdInGracePeriod)
        {
            FirstInterestPeriodEnum interestFreePeriod = FinancePlan.FirstInterestPeriod(createdInGracePeriod);
            const int duration = 4;
            const decimal interestRate = 0.05m;

            IList<IFinancePlanVisitBalance> visits = new List<IFinancePlanVisitBalance>
            {
                CreateVisit(10000m, 0m, 1, BillingApplicationConstants.HB)
            };

            this.ValidateMinimum(useBillingApplications, visits, duration, interestRate, interestFreePeriod, InterestCalculationMethodEnum.Daily);
        }


        [TestCase(100)]
        [TestCase(50)]
        public void WithInterestDueOnExistingFP_Amortizes(decimal monthlyPaymentAmount)
        {
            DateTime paymentDueDate = DateTime.UtcNow;
            DateTime statementPeriodEndDate = paymentDueDate.ToPaymentDueDateForDay(paymentDueDate.Day).AddDays(-21);
            DateTime statementPeriodStartDate = paymentDueDate.AddMonths(-1).ToPaymentDueDateForDay(paymentDueDate.Day).AddDays(-21);

            const decimal interestRate = 0.05m;

            IList<IFinancePlanVisitBalance> visits = new List<IFinancePlanVisitBalance>
            {
                CreateVisit(1200m, 0m, 0, BillingApplicationConstants.HB, 5m)
            };
            FinancePlan originatedFinancePlan = CreateFinancePlan(visits, monthlyPaymentAmount, interestRate, false, false);
            AmortizationVisitDateBalances visitDateBalances = new AmortizationVisitDateBalances();
            foreach (FinancePlanVisit financePlanVisit in originatedFinancePlan.FinancePlanVisits)
            {
                IList<Tuple<DateTime, decimal>> dateBalances = new List<Tuple<DateTime, decimal>>();
                foreach (DateTime dateTime in statementPeriodStartDate.ThroughEachDayUntil(statementPeriodEndDate.AddDays(-1)))
                {
                    dateBalances.Add(new Tuple<DateTime, decimal>(dateTime, financePlanVisit.PrincipalBalance));
                }

                visitDateBalances.Add(financePlanVisit, dateBalances);
            }

            IList<AmortizationMonth> months = CreateService(false).GetAmortizationMonthsForOriginatedFinancePlan(originatedFinancePlan, paymentDueDate, visitDateBalances);
            IList<AmortizationMonth> monthsFromBalance = CreateService(false).GetAmortizationMonthsForAmounts(monthlyPaymentAmount,
                visits,
                interestRate, 
                FirstInterestPeriodEnum.NoAction, 
                paymentDueDate, 
                originatedFinancePlan.InterestCalculationMethod,
                visitDateBalances);

            Assert.AreEqual(visits.Sum(x => x.PrincipalBalance), months.Sum(x => x.PrincipalAmount));
            Assert.AreEqual(monthsFromBalance.Sum(x => x.PrincipalAmount), months.Sum(x => x.PrincipalAmount));
            Assert.AreEqual(monthsFromBalance.Sum(x => x.InterestAmount), months.Sum(x => x.InterestAmount));
        }

        #region helpers

        private void ValidateMinimum(bool useBillingApplications, 
            IList<IFinancePlanVisitBalance> visits, 
            int duration,
            decimal interestRate, 
            FirstInterestPeriodEnum interestFreePeriod,
            InterestCalculationMethodEnum interestCalculationMethod = InterestCalculationMethodEnum.Monthly)
        {
            FinancePlanOfferService svc = (FinancePlanOfferService)CreateFinancePlanPlanOfferService(useBillingApplications);
            decimal minimumMonthlyPaymentAmount = svc.GetFactoredPaymentAmount(visits, duration, interestRate, false, interestFreePeriod).RoundUp();
            minimumMonthlyPaymentAmount = svc.ValidatePaymentAmount(false, visits, minimumMonthlyPaymentAmount, interestRate, duration, interestFreePeriod, DateTime.UtcNow, interestCalculationMethod);

            Console.WriteLine(minimumMonthlyPaymentAmount);

            // the number of months match
            IList<AmortizationMonth> months = this.ValidateAmortization(useBillingApplications,
                visits,
                duration,
                minimumMonthlyPaymentAmount,
                interestRate,
                interestFreePeriod, 
                null, 
                false,
                interestCalculationMethod: interestCalculationMethod);
            
            Assert.AreEqual(duration, months.Count);

            // decreasing minimum amount by a $0.01 increases the number of months
            decimal checkAmount = minimumMonthlyPaymentAmount - 0.01m;
            months = this.ValidateAmortization(useBillingApplications, visits, duration, checkAmount, interestRate, interestFreePeriod, null, false, interestCalculationMethod: interestCalculationMethod);
            Assert.AreEqual(duration + 1, months.Count);
        }
        
        private IList<AmortizationMonth> ValidateAmortization(bool useBillingApplications,
            IList<IFinancePlanVisitBalance> visits, 
            int duration, 
            decimal monthlyPaymentAmount,
            decimal interestRate, 
            FirstInterestPeriodEnum interestFreePeriod,
            decimal? expectedInterest,
            bool assert = true, 
            DateTime? dateToCheck = null,
            InterestCalculationMethodEnum interestCalculationMethod = InterestCalculationMethodEnum.Daily)
        {
            dateToCheck = dateToCheck ?? DateTime.UtcNow;
            IAmortizationService interestService = CreateService(useBillingApplications);
            IList<AmortizationMonth> months = interestService.GetAmortizationMonthsForAmounts(monthlyPaymentAmount, visits, interestRate, interestFreePeriod, dateToCheck.Value, interestCalculationMethod);

            foreach (AmortizationMonth month in months)
            {
                Console.WriteLine(month.MonthNumber + "\t" + month.InterestAmount.ToString("0.00") + "\t" + month.PrincipalAmount.ToString("0.00") + "\t" + month.Total.ToString("0.00"));
            }
            
            if (assert)
            {
                Assert.AreEqual(duration, months.Count);
                Assert.AreEqual(visits.Sum(x => x.CurrentVisitBalance), months.Sum(x => x.PrincipalAmount));
                if (expectedInterest.HasValue)
                {
                    Assert.AreEqual(expectedInterest, months.Sum(x => x.InterestAmount));
                }
            }

            return months;
        }

        private static IAmortizationService CreateService(bool useBillingApplications)
        {
            IList<BillingApplication> billingApplications = new List<BillingApplication>();

            if (useBillingApplications)
            {
                billingApplications.AddRange(new[]
                {
                    new BillingApplication(BillingApplicationConstants.HB, 0.66m, 1),
                    new BillingApplication(BillingApplicationConstants.PB, 0.34m, 2)
                });
            }

            return new AmortizationServiceMockBuilder().Setup(builder =>
            {
                builder.InterestService = new InterestServiceMockBuilder().CreateService();
                builder.PaymentAllocationServiceMock.Setup(x => x.BillingApplications()).Returns(() => billingApplications);
            }).CreateService();
        }

        private static IFinancePlanOfferService CreateFinancePlanPlanOfferService(bool useBillingApplications)
        {
            IInterestService interestService = new InterestServiceMockBuilder().CreateService();
            FinancePlanOfferService financePlanOfferService = new FinancePlanOfferServiceMockBuilder().Setup(builder =>
            {
                builder.AmortizationService = CreateService(useBillingApplications);
                builder.InterestService = interestService;
            }).CreateService();

            return financePlanOfferService;

        }

        private static FinancePlan CreateFinancePlan(IEnumerable<IFinancePlanVisitBalance> visits, decimal monthlyPaymentAmount, decimal interestRate, bool createdDuringGracePeriod, bool isCombined)
        {
            // in grace period?
            Mock<VpStatement> statement = new Mock<VpStatement>();
            statement.Setup(x => x.IsGracePeriod).Returns(createdDuringGracePeriod);

            //
            FinancePlan financePlan = new FinancePlan
            {
                CreatedVpStatement = statement.Object,
                IsCombined = isCombined,
                OriginationDate = DateTime.UtcNow,
                PaymentAmount = monthlyPaymentAmount,
                VpGuarantor = new Guarantor()
            };
            financePlan.SetInterestRate(interestRate, string.Empty);

            //
            financePlan.FinancePlanVisits.AddRange(visits.Select(v =>
            {
                Mock<FinancePlanVisit> fpv = new Mock<FinancePlanVisit>();
                fpv.Setup(x => x.BillingApplication).Returns(v.BillingApplication);
                fpv.Setup(x => x.CurrentBalance).Returns(v.CurrentBalance);
                fpv.Setup(x => x.OverrideInterestRate).Returns(v.OverrideInterestRate);
                fpv.Setup(x => x.OverridesRemaining).Returns(v.OverridesRemaining);
                fpv.Setup(x => x.PrincipalBalance).Returns(v.PrincipalBalance);
                fpv.Setup(x => x.InterestDue).Returns(v.InterestDue);
                return fpv.Object;
            }));

            //
            financePlan.FinancePlanOffer = new FinancePlanOffer
            {
                InterestRate = interestRate
            };

            //
            return financePlan;
        }

        private static IFinancePlanVisitBalance CreateVisit(decimal currentVisitBalance, decimal? overrideInterestRate, int overridesRemaining)
        {
            return CreateVisit(currentVisitBalance, overrideInterestRate, overridesRemaining, BillingApplicationConstants.HB);
        }

        private static IFinancePlanVisitBalance CreateVisit(decimal currentVisitBalance, decimal? overrideInterestRate, int overridesRemaining, string billingApplication)
        {
            return CreateVisit(currentVisitBalance, overrideInterestRate, overridesRemaining, billingApplication, 0m);
        }

        private static IFinancePlanVisitBalance CreateVisit(decimal currentVisitBalance, decimal? overrideInterestRate, int overridesRemaining, string billingApplication, decimal interestDue)
        {
            IFinancePlanVisitBalance setupVisit = new FinancePlanSetupVisit(
                0,
                null,
                0,
                currentVisitBalance,
                0,
                interestDue,
                VisitStateEnum.Active,
                billingApplication,
                false,
                overrideInterestRate,
                overridesRemaining);

            return setupVisit;

            /*Mock<IFinancePlanVisitBalance> visit = new Mock<IFinancePlanVisitBalance>();
            visit.Setup(x => x.BillingApplication).Returns(billingApplication);
            visit.Setup(x => x.CurrentBalance).Returns(currentVisitBalance + interestDue);
            visit.Setup(x => x.CurrentVisitBalance).Returns(currentVisitBalance);
            visit.Setup(x => x.OverrideInterestRate).Returns(overrideInterestRate);
            visit.Setup(x => x.OverridesRemaining).Returns(overridesRemaining);
            visit.Setup(x => x.InterestDue).Returns(interestDue);*/

            //return visit.Object;
        }

        private static IList<IFinancePlanVisitBalance> CreateFinancePlanVisitsFromAmount(decimal amount, int interestFreePeriods = 1)
        {
            IFinancePlanVisitBalance visit = CreateVisit(amount, 0m, interestFreePeriods);
            return visit.ToListOfOne();
        }
        
        #endregion
    }
}
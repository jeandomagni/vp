﻿namespace Ivh.Domain.FinancePlan.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Tests.Helpers.FinancePlan;
    using Common.VisitPay.Enums;
    using FinanceManagement.FinancePlan.Entities;
    using FinanceManagement.FinancePlan.Interfaces;
    using FinanceManagement.FinancePlan.Services;
    using FinanceManagement.Statement.Entities;
    using Guarantor.Entities;
    using Moq;
    using NUnit.Framework;
    using Settings.Entities;

    [TestFixture]
    public class FinancePlanOfferServiceTests : DomainTestBase
    {
        [Test]
        public void GenerateOffers_2Tiers_3InterestRates_HappyCase()
        {
            IFinancePlanOfferService svc = CreateService(FinancePlanOfferSetTypeEnum.GuarantorOffers);

            Mock<IFinancePlanVisitBalance> visit = new Mock<IFinancePlanVisitBalance>();
            visit.Setup(x => x.CurrentBalance).Returns(1320m);
            visit.Setup(x => x.OverridesRemaining).Returns(1);
            visit.Setup(x => x.OverrideInterestRate).Returns(0m);
            List<IFinancePlanVisitBalance> visits = new List<IFinancePlanVisitBalance>
            {
                visit.Object
            };

            VpStatement statement = new VpStatement
            {
                PaymentDueDate = DateTime.UtcNow
            };
            
            FinancePlanCalculationParameters parameters = new FinancePlanCalculationParameters(
                new Guarantor(), 
                new VpStatement(), 
                0, 
                null, 
                OfferCalculationStrategyEnum.MonthlyPayment, 
                InterestCalculationMethodEnum.Monthly,
                statement.PaymentDueDate)
            {
                Visits = visits
            };

            IList<FinancePlanOffer> result = svc.GenerateOffersForUser(
                parameters,
                new List<FinancePlan>(),
                false,
                false,
                FinancePlan.FirstInterestPeriod(false));

            Assert.AreEqual(3, result.Count);
        }

        [Test]
        public void GenerateScoreBasedOffer_ValidScore_ValidBalance_UseScoreBasedInterestRate()
        {
            IFinancePlanOfferService svc = CreateService(FinancePlanOfferSetTypeEnum.ScoreBasedOffer);

            Mock<IFinancePlanVisitBalance> visit = new Mock<IFinancePlanVisitBalance>();
            visit.Setup(x => x.CurrentBalance).Returns(1320m);
            visit.Setup(x => x.OverridesRemaining).Returns(1);
            visit.Setup(x => x.OverrideInterestRate).Returns(0m);
            List<IFinancePlanVisitBalance> visits = new List<IFinancePlanVisitBalance>
            {
                visit.Object
            };

            VpStatement statement = new VpStatement
            {
                PaymentDueDate = DateTime.UtcNow
            };

            FinancePlanCalculationParameters parameters = new FinancePlanCalculationParameters(
                new Guarantor(),
                new VpStatement(),
                0,
                null,
                OfferCalculationStrategyEnum.MonthlyPayment,
                InterestCalculationMethodEnum.Monthly,
                statement.PaymentDueDate)
            {
                Visits = visits
            };
            parameters.VpGuarantorScore = new VpGuarantorScore {Score = 201};

            IList<FinancePlanOffer> result = svc.GenerateOffersForUser(
                parameters,
                new List<FinancePlan>(),
                false,
                false,
                FinancePlan.FirstInterestPeriod(false));

            Assert.AreEqual(1, result.Count);
        }

        [Test]
        public void GenerateScoreBasedOffer_ValidScore_BalanceOverMax_UseDefaultInterestRate()
        {
            IFinancePlanOfferService svc = CreateService(FinancePlanOfferSetTypeEnum.GuarantorOffers);

            Mock<IFinancePlanVisitBalance> visit = new Mock<IFinancePlanVisitBalance>();
            visit.Setup(x => x.CurrentBalance).Returns(5000m);
            visit.Setup(x => x.OverridesRemaining).Returns(1);
            visit.Setup(x => x.OverrideInterestRate).Returns(0m);
            List<IFinancePlanVisitBalance> visits = new List<IFinancePlanVisitBalance>
            {
                visit.Object
            };

            VpStatement statement = new VpStatement
            {
                PaymentDueDate = DateTime.UtcNow
            };

            FinancePlanCalculationParameters parameters = new FinancePlanCalculationParameters(
                new Guarantor(),
                new VpStatement(),
                0,
                null,
                OfferCalculationStrategyEnum.MonthlyPayment,
                InterestCalculationMethodEnum.Monthly,
                statement.PaymentDueDate)
            {
                Visits = visits
            };
            parameters.VpGuarantorScore = new VpGuarantorScore { Score = 201 };

            IList<FinancePlanOffer> result = svc.GenerateOffersForUser(
                parameters,
                new List<FinancePlan>(),
                false,
                false,
                FinancePlan.FirstInterestPeriod(false));

            Assert.AreEqual(3, result.Count);
        }

        private static IFinancePlanOfferService CreateService(FinancePlanOfferSetTypeEnum financePlanOfferSetTypeId)
        {
            IInterestService interestService = new InterestServiceMockBuilder().Setup(builder =>
            {
                builder.ApplicationSettingsServiceMock.Setup(x => x.EnableFinancialAssistanceInterestRates).Returns(() => new ApplicationSetting<bool>(() => new Lazy<bool>(() => false)));
                builder.FinancePlanOfferSetTypeRepositoryMock.Setup(x => x.GetQueryable()).Returns(() => new List<FinancePlanOfferSetType>
                {
                    new FinancePlanOfferSetType {FinancePlanOfferSetTypeId = 1, IsDefault = true, IsActive = true, IsClient = true, IsPatient = true, Priority = 2}, // Guarantor offer
                    new FinancePlanOfferSetType {FinancePlanOfferSetTypeId = 2, IsDefault = false, IsActive = true, IsClient = true, IsPatient = true, Priority = 3}, // Extended offer
                    new FinancePlanOfferSetType {FinancePlanOfferSetTypeId = 3, IsDefault = false, IsActive = true, IsClient = false, IsPatient = true, Priority = 1} // Score based offer
                }.AsQueryable());
                builder.InterestRateRepositoryMock.Setup(x => x.GetQueryable()).Returns(SetupInterestRates());
                builder.InterestRateConsiderationServiceMock.Setup(x => x.GetTotalConsiderationAmount(It.IsAny<decimal>(), It.IsAny<IList<FinancePlan>>())).Returns((decimal d, IList<FinancePlan> fps) => d);
                builder.ApplicationSettingsServiceMock.Setup(x => x.IsClientApplication).Returns(() => new ApplicationSetting<bool>(() => new Lazy<bool>(() => false)));
            }).CreateService();
            FinancePlanOfferService financePlanOfferService = new FinancePlanOfferServiceMockBuilder().Setup(builder =>
            {
                builder.AmortizationService = new AmortizationServiceMockBuilder().CreateService();
                builder.InterestService = interestService;
                builder.MinimumPaymentTierRepositoryMock.Setup(x => x.GetQueryable()).Returns(Setup2Tiers(financePlanOfferSetTypeId));
                builder.FinancePlanOfferSetTypeRepositoryMock.Setup(x => x.GetQueryable()).Returns(() => new List<FinancePlanOfferSetType>
                {
                    new FinancePlanOfferSetType {FinancePlanOfferSetTypeId = 1, IsDefault = true, IsActive = true, IsClient = true, IsPatient = true},
                    new FinancePlanOfferSetType {FinancePlanOfferSetTypeId = 2, IsDefault = false, IsActive = true, IsClient = true, IsPatient = false},
                    new FinancePlanOfferSetType {FinancePlanOfferSetTypeId = 3, IsDefault = false, IsActive = true, IsClient = false, IsPatient = true}
                }.AsQueryable());
            }).CreateService();

            return financePlanOfferService;

        }

        private static IQueryable<InterestRate> SetupInterestRates()
        {
            IList<InterestRate> listOfRates = new List<InterestRate>()
            {
                new InterestRate
                {
                    DurationRangeStart = 1,
                    DurationRangeEnd = 12,
                    Rate = 0.00m,
                    MinimumBalance = null,
                    MaximumBalance = null,
                    FinancePlanOfferSetType = new FinancePlanOfferSetType { FinancePlanOfferSetTypeId = 1, IsDefault = true, IsActive = true, IsClient = true, IsPatient = true }
                },
                new InterestRate
                {
                    DurationRangeStart = 13,
                    DurationRangeEnd = 24,
                    Rate = 0.05m,
                    MinimumBalance = null,
                    MaximumBalance = null,
                    FinancePlanOfferSetType = new FinancePlanOfferSetType { FinancePlanOfferSetTypeId = 1, IsDefault = true, IsActive = true, IsClient = true, IsPatient = true }
                },
                new InterestRate
                {
                    DurationRangeStart = 25,
                    DurationRangeEnd = 36,
                    Rate = 0.08m,
                    MinimumBalance = null,
                    MaximumBalance = null,
                    FinancePlanOfferSetType = new FinancePlanOfferSetType { FinancePlanOfferSetTypeId = 1, IsDefault = true, IsActive = true, IsClient = true, IsPatient = true }
                },
                new InterestRate
                {
                DurationRangeStart = 0,
                DurationRangeEnd = 4,
                Rate = 0.00m,
                MinimumBalance = null,
                MaximumBalance = 4000,
                MinimumPtpScore = 200,
                MaximumPtpScore = 203,
                FinancePlanOfferSetType = new FinancePlanOfferSetType { FinancePlanOfferSetTypeId = 3, IsDefault = false, IsActive = true, IsClient = false, IsPatient = true }
            }
            };

            return listOfRates.AsQueryable();
        }

        private static IQueryable<FinancePlanMinimumPaymentTier> Setup2Tiers(FinancePlanOfferSetTypeEnum financePlanOfferSetTypeId)
        {
            List<FinancePlanMinimumPaymentTier> listOfTiers = new List<FinancePlanMinimumPaymentTier>()
            {
                new FinancePlanMinimumPaymentTier
                {
                    ExisitingRecurringPaymentTotal = 0m,
                    MinimumPayment = 25m,
                    FinancePlanOfferSetType = new FinancePlanOfferSetType { FinancePlanOfferSetTypeId = (int)financePlanOfferSetTypeId, IsDefault = true, IsActive = true, IsClient = true, IsPatient = true }
                },
                new FinancePlanMinimumPaymentTier
                {
                    ExisitingRecurringPaymentTotal = 50m,
                    MinimumPayment = 10m,
                    FinancePlanOfferSetType = new FinancePlanOfferSetType { FinancePlanOfferSetTypeId = (int)financePlanOfferSetTypeId, IsDefault = true, IsActive = true, IsClient = true, IsPatient = true }
                },
            };

            return listOfTiers.AsQueryable();
        }
    }
}
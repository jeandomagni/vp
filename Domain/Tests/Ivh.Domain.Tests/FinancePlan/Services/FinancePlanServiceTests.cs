﻿namespace Ivh.Domain.FinancePlan.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AppIntelligence.Interfaces;
    using Application.FinanceManagement.Common.Dtos;
    using Application.FinanceManagement.Common.Interfaces;
    using Base.Interfaces;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.Data.Connection;
    using Common.Data.Connection.Connections;
    using Common.State.Interfaces;
    using Common.Tests.Helpers;
    using Common.Tests.Helpers.Base;
    using Common.Tests.Helpers.Client;
    using Common.Tests.Helpers.FinancePlan;
    using Common.Tests.Helpers.Guarantor;
    using Common.Tests.Helpers.Visit;
    using Common.Tests.Helpers.VpStatement;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.FinanceManagement;
    using Common.VisitPay.Messages.HospitalData;
    using Ivh.Domain.Guarantor.Entities;
    using FinanceManagement.FinancePlan.Interfaces;
    using FinanceManagement.FinancePlan.Services;
    using Moq;
    using NUnit.Framework;
    using Entities;
    using FinanceManagement.FinancePlan.Entities;
    using FinanceManagement.Payment.Entities;
    using FinanceManagement.Statement.Entities;
    using FinanceManagement.Statement.Interfaces;
    using FinanceManagement.ValueTypes;
    using Logging.Interfaces;
    using NHibernate;
    using Settings.Entities;
    using Settings.Interfaces;
    using User.Interfaces;
    using Visit.Entities;
    using Visit.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Application.FinanceManagement.ApplicationServices;
    using Ivh.Common.Tests.Builders;

    [TestFixture]
    public class FinancePlanServiceTests : DomainTestBase
    {
        private const int UncollectableMaxAgingCountFinancePlans = 4;
        private const int CreditAgreementMinResponsePeriod = 10;

        private Mock<IAmortizationService> _amortizationService;
        private Mock<IClientService> _clientService;
        private Mock<IFinancePlanOfferService> _financePlanOfferService;
        private Mock<IFinancePlanRepository> _financePlanRepository;
        private Mock<IFinancePlanReadOnlyRepository> _financePlanReadOnlyRepository;
        private IInterestService _interestService;
        private Mock<IVisitService> _visitService;
        private Mock<IVisitPayUserJournalEventService> _visitPayUserJournalEventService;
        private Mock<IBus> _serviceBus;
        private Mock<ISession> _moqSession;
        private Mock<IMetricsProvider> _metricsProvider;
        private Mock<IFinancePlanOfferSetTypeRepository> _financePlanOfferSetTypeRepository;
        private Mock<IRandomizedTestService> _randomizedTestService;
        private Mock<IFeatureService> _featureService;
        private Mock<IVpStatementService> _vpStatementServiceMock;
        private Mock<IStatementDateService> _statementDateServiceMock;
        private Mock<IFacilityService> _facilityServiceMock;
        private IDomainServiceCommonService _domainServiceCommonService;

        //Test Variables
        private Client _client;

        #region ReassessmentVariables
        private Guarantor Guarantor;
        private Mock<IApplicationSettingsService> _applicationSettings;

        #endregion

        [SetUp]
        public void Setup()
        {
            this._serviceBus = new Mock<IBus>();
            this._moqSession = new Mock<ISession>();
            this._visitPayUserJournalEventService = new Mock<IVisitPayUserJournalEventService>();
            this._applicationSettings = new Mock<IApplicationSettingsService>();

            this._amortizationService = new Mock<IAmortizationService>();
            this._financePlanOfferService = new Mock<IFinancePlanOfferService>();
            this._financePlanRepository = new Mock<IFinancePlanRepository>();
            this._financePlanReadOnlyRepository = new Mock<IFinancePlanReadOnlyRepository>();
            this._randomizedTestService = new Mock<IRandomizedTestService>();
            this._financePlanOfferSetTypeRepository = new Mock<IFinancePlanOfferSetTypeRepository>();
            this._visitService = new Mock<IVisitService>();
            this._clientService = new Mock<IClientService>();
            this._featureService = new Mock<IFeatureService>();
            this._vpStatementServiceMock = new Mock<IVpStatementService>();
            this._statementDateServiceMock = new Mock<IStatementDateService>();
            this._facilityServiceMock = new Mock<IFacilityService>();

            this._domainServiceCommonService = DomainServiceCommonServices.CreateDomainServiceCommonService(
                applicationSettingsServiceMock: this._applicationSettings,
                featureServiceMock: this._featureService,
                clientServiceMock: this._clientService,
                loggingServiceMock: null,
                busMock: this._serviceBus,
                cacheMock: null,
                timeZoneHelperMock: null);

            this._interestService = new InterestServiceMockBuilder().Setup(builder =>
            {
                builder.DomainServiceCommonService = this._domainServiceCommonService;
                builder.FinancePlanOfferSetTypeRepository = this._financePlanOfferSetTypeRepository.Object;
                builder.RandomizedTestService = this._randomizedTestService.Object;

            }).CreateService();

            /*new InterestService(
                new Lazy<IDomainServiceCommonService>(() => this._domainServiceCommonService),
                new Lazy<IFinancePlanOfferSetTypeRepository>(() => this._financePlanOfferSetTypeRepository.Object),
                new Lazy<IInterestRateRepository>(() => new Mock<IInterestRateRepository>().Object),
                new Lazy<IInterestRateConsiderationService>(() => new Mock<IInterestRateConsiderationService>().Object),
                new Lazy<IPaymentAllocationService>(() => this._paymentAllocationService.Object),
                new Lazy<IRandomizedTestService>(() => this._randomizedTestService.Object));*/

            Mock<IApplicationSetting<bool>> falseAppSetting = new Mock<IApplicationSetting<bool>>();
            falseAppSetting.Setup(x => x.Value).Returns(false);
            this._applicationSettings.Setup(x => x.PreventInterest).Returns(falseAppSetting.Object);

            this._metricsProvider = new Mock<IMetricsProvider>();

            this.Guarantor = GuarantorFactory.GenerateOnline();
        }

        private IFinancePlanService CreateFinancePlanService()
        {
            this.SetupClient();

            this._financePlanRepository.Setup(x => x.GetFinancePlanStatuses()).Returns(() =>
            {
                return new List<FinancePlanStatus>
                {
                    new FinancePlanStatus {FinancePlanStatusId = 1},
                    new FinancePlanStatus {FinancePlanStatusId = 2},
                    new FinancePlanStatus {FinancePlanStatusId = 3},
                    new FinancePlanStatus {FinancePlanStatusId = 4},
                    new FinancePlanStatus {FinancePlanStatusId = 5},
                    new FinancePlanStatus {FinancePlanStatusId = 6},
                    new FinancePlanStatus {FinancePlanStatusId = 7},
                    new FinancePlanStatus {FinancePlanStatusId = 8},
                    new FinancePlanStatus {FinancePlanStatusId = 9}
                };
            });

            return new FinancePlanService(
                new Lazy<IDomainServiceCommonService>(() => this._domainServiceCommonService),
                new Lazy<IAmortizationService>(() => this._amortizationService.Object),
                new Lazy<IFinancePlanOfferService>(() => this._financePlanOfferService.Object),
                new Lazy<IFinancePlanRepository>(() => this._financePlanRepository.Object),
                new Lazy<IFinancePlanReadOnlyRepository>(() => this._financePlanReadOnlyRepository.Object),
                new Lazy<IInterestService>(() => this._interestService),
                new Lazy<IVisitService>(() => this._visitService.Object),
                new Lazy<IVisitPayUserJournalEventService>(() => this._visitPayUserJournalEventService.Object),
                new Lazy<IVpStatementService>(() => this._vpStatementServiceMock.Object),
                new Lazy<IStatementDateService>(() => this._statementDateServiceMock.Object),
                new SessionContext<VisitPay>(this._moqSession.Object),
                new Lazy<IMetricsProvider>(() => this._metricsProvider.Object),
                new Lazy<IFacilityService>(() => this._facilityServiceMock.Object)
            );

        }

        private void SetupClient()
        {
            Dictionary<string, string> clientSettings = new Dictionary<string, string>
            {
                {"ClientName", string.Empty},
                {"UncollectableMaxAgingCountFinancePlans", UncollectableMaxAgingCountFinancePlans.ToString()},
                {"CreditAgreementMinResponsePeriod" , CreditAgreementMinResponsePeriod.ToString()},
                {"BalanceTransferIsEnabled", true.ToString() }
            };

            Lazy<TimeZoneHelper> timeZoneHelper = new Lazy<TimeZoneHelper>(() => new TimeZoneHelper(TimeZoneHelper.TimeZones.PacificStandardTime));
            this._client = new Client(clientSettings, timeZoneHelper);

            this._clientService.Setup(x => x.GetClient()).Returns(() => this._client);
        }

        #region assess interest

        private static IFinancePlanService CreateFinancePlanServiceForAssessInterest(bool isInterestEnabled = true)
        {
            IFinancePlanService financePlanService = new FinancePlanServiceMockBuilder().Setup(builder =>
            {
                builder.ApplicationSettingsMock.Setup(x => x.PreventInterest).Returns(() => new ApplicationSetting<bool>(() => new Lazy<bool>(() => !isInterestEnabled)));
                builder.InterestService = new InterestServiceMockBuilder().CreateService();
            }).CreateService();

            return financePlanService;
        }

        [Test]
        public void FinancePlan_AssessInterest_InterestNotEnabled()
        {
            IFinancePlanService svc = CreateFinancePlanServiceForAssessInterest(false);

            IList<FinancePlanVisitInterestDue> assessments = svc.AssessInterest(null, DateTime.MinValue, DateTime.MaxValue, DateTime.MinValue);

            Assert.AreEqual(0, assessments.Count);
        }

        [Test]
        public void FinancePlan_AssessInterest_NullFinancePlan()
        {
            IFinancePlanService svc = CreateFinancePlanServiceForAssessInterest();

            IList<FinancePlanVisitInterestDue> assessments = svc.AssessInterest(null, DateTime.MinValue, DateTime.MaxValue, DateTime.MinValue);

            Assert.AreEqual(0, assessments.Count);
        }

        [Test]
        public void FinancePlan_AssessInterest_ShouldNotAssess()
        {
            IFinancePlanService svc = CreateFinancePlanServiceForAssessInterest();

            FinancePlan financePlan = new FinancePlan
            {
                OriginationDate = DateTime.UtcNow,
                VpGuarantor = new Guarantor()
            };

            // still in interest free period
            IList<FinancePlanVisitInterestDue> assessments1 = svc.AssessInterest(financePlan, DateTime.MinValue, DateTime.MaxValue, DateTime.UtcNow);
            Assert.AreEqual(0, assessments1.Count);

            // messed up statement dates
            IList<FinancePlanVisitInterestDue> assessments2 = svc.AssessInterest(financePlan, DateTime.MaxValue, DateTime.MinValue, DateTime.UtcNow);
            Assert.AreEqual(0, assessments2.Count);
        }

        [Test]
        public void FinancePlan_AssessInterest_ShouldNotAssess_Removed()
        {
            Guarantor guarantor = new Guarantor();

            // add active visits
            List<Tuple<Visit, IList<VisitTransaction>>> visitsAndTransactions = Enumerable.Range(0, 2)
                .Select(x => VisitFactory.GenerateActiveVisit(1000m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor))
                .ToList();

            FinancePlan financePlan = FinancePlanFactory.CreateFinancePlan(visitsAndTransactions, 0, 1000m / 8, DateTime.UtcNow.AddMonths(-1), 0.05m);
            financePlan.VpGuarantor = guarantor;

            IFinancePlanService svc = new FinancePlanServiceMockBuilder().Setup(builder =>
            {
                builder.ApplicationSettingsMock.Setup(x => x.PreventInterest).Returns(() => new ApplicationSetting<bool>(() => new Lazy<bool>(() => false)));
                builder.VisitServiceMock.Setup(x => x.GetVisits(It.IsAny<int>(), It.IsAny<List<int>>())).Returns(visitsAndTransactions.Select(x => x.Item1).ToList());
                builder.InterestService = new InterestServiceMockBuilder().CreateService();
            }).CreateService();


            Visit removedVisit = visitsAndTransactions.First().Item1;
            removedVisit.BillingHold = true;
            ((IEntity<VisitStateEnum>)removedVisit).SetState(VisitStateEnum.Inactive,"");
            //visit removed
            financePlan.FinancePlanVisits.First(x => x.VisitId == removedVisit.VisitId).HardRemoveDate = DateTime.UtcNow.AddDays(-1);
            removedVisit.BillingHold = false;
            ((IEntity<VisitStateEnum>)removedVisit).SetState(VisitStateEnum.Active, "");

            // still in interest free period
            IList<FinancePlanVisitInterestDue> assessments1 = svc.AssessInterest(financePlan, DateTime.UtcNow.AddMonths(-1), DateTime.UtcNow, DateTime.UtcNow);
            Assert.AreEqual(1, assessments1.Count);

        }

        public class AssessInterestTestCase
        {
            public decimal InterestRate { get; set; }
            public decimal ExpectedInterestChargedPerVisit { get; set; }
            public bool ShouldAssessInterest { get; set; } = true;
            public decimal VisitBalance { get; set; }
            public decimal? AdjustmentAmount { get; set; }
            public DateTime? AdjustmentDateTime { get; set; }

            public DateTime DateToProcess { get; set; }
            public DateTime StatementPeriodStart { get; set; }
            public DateTime StatementPeriodEnd { get; set; }
            public InterestCalculationMethodEnum InterestCalculationMethod { get; set; } = InterestCalculationMethodEnum.Daily;

            public AssessInterestTestCase(decimal interestRate,
                decimal expectedInterestChargedPerVisit,
                decimal visitBalance,
                DateTime dateToProcess,
                DateTime? statementPeriodStart = null
                , DateTime? statementPeriodEnd = null
                , DateTime? adjustmentDateTime = null)
            {
                this.InterestRate = interestRate;
                this.ExpectedInterestChargedPerVisit = expectedInterestChargedPerVisit;
                this.VisitBalance = visitBalance;
                this.DateToProcess = dateToProcess;
                this.StatementPeriodStart = statementPeriodStart ?? dateToProcess;
                this.StatementPeriodEnd = statementPeriodEnd ?? this.StatementPeriodStart.AddMonths(1).AddDays(-1);
                this.AdjustmentDateTime = adjustmentDateTime ?? this.StatementPeriodStart.AddDays((double)(this.StatementPeriodEnd - this.StatementPeriodStart).Days / 2);
            }

            public override string ToString()
            {
                return $"{this.VisitBalance} at {this.InterestRate * 100}% is "
                    + (this.ShouldAssessInterest ? $"assessed {this.ExpectedInterestChargedPerVisit} interest " : "not assessed interest")
                    + $"(calculated {this.InterestCalculationMethod.ToString().ToLower()}) "
                    + $"from {this.StatementPeriodStart.ToShortDateString()} to {this.StatementPeriodEnd.ToShortDateString()}"
                    + (this.AdjustmentAmount.HasValue && this.AdjustmentDateTime.HasValue ? $" with an adjustment of {this.AdjustmentAmount.Value} on {this.AdjustmentDateTime.Value.ToShortDateString()}" : string.Empty);
            }

        }

        public static IEnumerable<AssessInterestTestCase> AssessInterestTestCases
        {
            get
            {
                decimal visitBalance = 1000m;
                
                //today
                DateTime dateToProcess = new DateTime(2018, 1, 1);
                yield return new AssessInterestTestCase(0.04m, 3.40m, visitBalance, dateToProcess);
                yield return new AssessInterestTestCase(0.08m, 6.79m, visitBalance, dateToProcess);

                //today with adjustment to 0
                yield return new AssessInterestTestCase(0.04m, 3.40m, visitBalance, dateToProcess) { AdjustmentAmount = -visitBalance, ShouldAssessInterest = false};
                yield return new AssessInterestTestCase(0.08m, 6.79m, visitBalance, dateToProcess) { AdjustmentAmount = -visitBalance, ShouldAssessInterest = false};

                //today with adjustment
                yield return new AssessInterestTestCase(0.04m, 1.70m, visitBalance, dateToProcess) { AdjustmentAmount = -visitBalance/2};
                yield return new AssessInterestTestCase(0.08m, 3.40m, visitBalance, dateToProcess) { AdjustmentAmount = -visitBalance/2};

                //not a leap year
                dateToProcess = new DateTime(2018, 12, 20);
                yield return new AssessInterestTestCase(0.04m, 3.40m, visitBalance, dateToProcess);
                yield return new AssessInterestTestCase(0.08m, 6.79m, visitBalance, dateToProcess);

                //leap year
                dateToProcess = new DateTime(2019, 12, 20);
                yield return new AssessInterestTestCase(0.04m, 3.39m, visitBalance, dateToProcess);
                yield return new AssessInterestTestCase(0.08m, 6.78m, visitBalance, dateToProcess);

                //VP-1679 - upward adjustments should not change the assessed amount
                //leap year with adjustment
                dateToProcess = new DateTime(2019, 12, 20);
                yield return new AssessInterestTestCase(0.04m, 3.39m, visitBalance, dateToProcess) { AdjustmentAmount = 100m };
                yield return new AssessInterestTestCase(0.08m, 6.78m, visitBalance, dateToProcess) { AdjustmentAmount = 100m };

                //checking each month
                visitBalance = 100m;
                foreach (DateTime processDate in new DateTime(2018, 1, 1).ThroughEachMonthUntil(new DateTime(2018, 12, 1)))
                {
                    decimal expectedInterest = DateTime.DaysInMonth(processDate.Year, processDate.Month) == 31 ? 0.34m :
                                               DateTime.DaysInMonth(processDate.Year, processDate.Month) == 30 ? 0.33m :
                                               0.31m;

                    yield return new AssessInterestTestCase(0.04m, expectedInterest, visitBalance, processDate);
                }

                //monthly
                yield return new AssessInterestTestCase(0.04m, 0.33m, visitBalance, DateTime.UtcNow) { InterestCalculationMethod = InterestCalculationMethodEnum.Monthly };
                yield return new AssessInterestTestCase(0.08m, 0.67m, visitBalance, DateTime.UtcNow) { InterestCalculationMethod = InterestCalculationMethodEnum.Monthly };
            }
        }

        [TestCaseSource(nameof(AssessInterestTestCases))]
        public void FinancePlan_AssessInterest(AssessInterestTestCase testCase)
        {
            const int numberOfVisitsExpectedToHaveInterest = 5;
            const int numberOfTransactions = 2;

            Guarantor guarantor = new Guarantor();

            // add active visits
            List<Tuple<Visit, IList<VisitTransaction>>> visitsAndTransactions = Enumerable.Range(0, numberOfVisitsExpectedToHaveInterest)
                .Select(x => VisitFactory.GenerateActiveVisit(testCase.VisitBalance, numberOfTransactions, VisitStateEnum.Active, testCase.DateToProcess.AddMonths(-1), guarantor: guarantor))
                .ToList();

            FinancePlan financePlan = FinancePlanFactory.CreateFinancePlan(visitsAndTransactions, 0, testCase.VisitBalance / 4, testCase.DateToProcess.AddMonths(-1), testCase.InterestRate);
            financePlan.VpGuarantor = guarantor;

            if (testCase.AdjustmentAmount.HasValue)
            {
                foreach (Tuple<Visit, IList<VisitTransaction>> visitAndTransactions in visitsAndTransactions)
                {
                    Visit visit = visitAndTransactions.Item1;
                    VisitBalanceHistory newBalance = new VisitBalanceHistory()
                    {
                        Visit = visit,
                        CurrentBalance = testCase.VisitBalance + testCase.AdjustmentAmount.Value,
                        InsertDate = testCase.AdjustmentDateTime.Value
                    };
                    visit.VisitBalances.Add(newBalance);
                }

                foreach (FinancePlanVisit financePlanVisit in financePlan.FinancePlanVisits)
                {
                    financePlanVisit.FinancePlanVisitVisit.CurrentBalance += testCase.AdjustmentAmount.Value;
                }

                financePlan.AddFinancePlanVisitPrincipalAmountAdjustments();

                foreach (FinancePlanVisitPrincipalAmount financePlanVisitPrincipalAmount in financePlan.FinancePlanVisits.SelectMany(x => x.FinancePlanVisitPrincipalAmounts).Where(x => x.Amount == testCase.AdjustmentAmount))
                {
                    financePlanVisitPrincipalAmount.InsertDate = testCase.AdjustmentDateTime.Value;
                }
            }

            Mock<VpStatement> statementMock = new Mock<VpStatement>();
            statementMock.Setup(x => x.PeriodStartDate).Returns(() => DateTime.MinValue);
            financePlan.CreatedVpStatement = statementMock.Object;
            financePlan.InterestCalculationMethod = testCase.InterestCalculationMethod;

            // add suspended
            FinancePlanFactory.AddVisitsToFinancePlan(VisitFactory.GenerateInactiveVisit(testCase.VisitBalance, 2, guarantor: guarantor, insertDate: testCase.DateToProcess).Item1.ToListOfOne(), financePlan);

            // add closed statuses
            FinancePlanFactory.AddVisitsToFinancePlan(VisitFactory.GenerateActiveVisit(testCase.VisitBalance, 2, VisitStateEnum.Active, guarantor: guarantor, insertDate: testCase.DateToProcess).Item1.ToListOfOne(), financePlan);

            decimal financePlanVisitBalanceBeforeAssessement = financePlan.FinancePlanVisits.Sum(x => x.CurrentBalance);
            decimal financePlanBalance = financePlan.CurrentFinancePlanBalance;

            IFinancePlanService svc = new FinancePlanServiceMockBuilder().Setup(builder =>
            {
                builder.ApplicationSettingsMock.Setup(x => x.PreventInterest).Returns(() => new ApplicationSetting<bool>(() => new Lazy<bool>(() => false)));
                builder.VisitServiceMock.Setup(x => x.GetVisits(It.IsAny<int>(), It.IsAny<List<int>>())).Returns(visitsAndTransactions.Select(x => x.Item1).ToList());
                builder.InterestService = new InterestServiceMockBuilder().CreateService();
            }).CreateService();

            IList<FinancePlanVisitInterestDue> assessments = svc.AssessInterest(financePlan, testCase.StatementPeriodStart, testCase.StatementPeriodEnd, testCase.DateToProcess);

            if (!testCase.ShouldAssessInterest)
            {
                Assert.AreEqual(assessments.Count, 0);
                return;
            }

            // interest was assessed
            Assert.Greater(assessments.Count, 0);
            decimal financePlanVisitBalanceAfterAssessement = financePlan.FinancePlanVisits.Sum(x => x.CurrentBalance);

            // balance goes up
            Assert.True(financePlanVisitBalanceAfterAssessement > financePlanVisitBalanceBeforeAssessement);
            Assert.True(financePlan.CurrentFinancePlanBalance > financePlanBalance);

            decimal? totalInterest = 0m;
            foreach (Visit visit in visitsAndTransactions.Select(x => x.Item1).ToList())
            {
                // interest transactions
                FinancePlanVisit fpVisit = financePlan.FinancePlanVisits.FirstOrDefault(x => x.VisitId == visit.VisitId);
                IList<FinancePlanVisitInterestDue> interestDue = fpVisit?.FinancePlanVisitInterestDues;
                if (visit.CurrentVisitState == VisitStateEnum.Active)
                {
                    Assert.True(interestDue?.Count > 0);
                }
                else
                {
                    Assert.True(interestDue?.Count == 0);
                }

                // charged interest
                decimal? interestCharged = interestDue?.Sum(x => x.InterestDue);
                Assert.AreEqual(testCase.ExpectedInterestChargedPerVisit, interestCharged);

                totalInterest += interestCharged;
            }

            // only charged where should
            Assert.AreEqual(numberOfVisitsExpectedToHaveInterest * testCase.ExpectedInterestChargedPerVisit, totalInterest);

            // finance plan has interest assessed
            Assert.AreEqual(totalInterest, financePlan.InterestAssessed);
        }

        [TestCase(0.04, 0.00, 1)]
        [TestCase(0.04, 0.00, 2)]
        [TestCase(0.04, 0.02, 1)]
        [TestCase(0.00, 0.08, 1)]
        [TestCase(0.00, 0.08, 2)]
        public void FinancePlan_AssessInterest_WithOverrides(decimal interestRate, decimal overrideInterestRate, int numberOfOverrides)
        {
            Guarantor guarantor = new Guarantor();

            // add active visits
            List<Tuple<Visit, IList<VisitTransaction>>> visitsAndTransactions = Enumerable.Range(0, 1).Select(x => VisitFactory.GenerateActiveVisit(100m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor)).ToList();

            FinancePlan financePlan = FinancePlanFactory.CreateFinancePlan(visitsAndTransactions, 0, 25m, DateTime.UtcNow.AddMonths(-1));
            financePlan.SetInterestRate(interestRate, "");
            financePlan.VpGuarantor = guarantor;

            Mock<VpStatement> statementMock = new Mock<VpStatement>();
            statementMock.Setup(x => x.PeriodStartDate).Returns(() => DateTime.MinValue);
            financePlan.CreatedVpStatement = statementMock.Object;

            // add interest overrides
            foreach (FinancePlanVisit fpv in financePlan.FinancePlanVisits)
            {
                fpv.FinancePlanVisitInterestRateHistories.Add(new FinancePlanVisitInterestRateHistory
                {
                    InsertDate = DateTime.UtcNow,
                    InterestRate = financePlan.CurrentInterestRate,
                    OverrideInterestRate = overrideInterestRate,
                    OverridesRemaining = numberOfOverrides
                });
            }

            IFinancePlanService svc = new FinancePlanServiceMockBuilder().Setup(builder =>
            {
                builder.ApplicationSettingsMock.Setup(x => x.PreventInterest).Returns(() => new ApplicationSetting<bool>(() => new Lazy<bool>(() => false)));
                builder.VisitServiceMock.Setup(x => x.GetVisits(It.IsAny<int>(), It.IsAny<List<int>>())).Returns(visitsAndTransactions.Select(x => x.Item1).ToList());
                builder.InterestService = new InterestServiceMockBuilder().CreateService();
            }).CreateService();

            DateTime dt = DateTime.UtcNow;

            int loopCounter = 10;
            while (financePlan.FinancePlanVisits.First().OverridesRemaining > 0)
            {
                loopCounter--;
                if (loopCounter == 0)
                {
                    Assert.Fail($"loop count reached {financePlan.FinancePlanVisits.First().OverridesRemaining}");
                    break;
                }

                IList<FinancePlanVisitInterestDue> assessments = svc.AssessInterest(financePlan, dt, dt.AddMonths(1), dt);
                dt = dt.AddMonths(1).AddSeconds(1);

                Assert.AreEqual(overrideInterestRate, assessments.First().InterestRateUsedToAssess);

                // fix the dates b/c unit test doesn't use id's for sorting and it's fast enough the dates can be the same.
                foreach (FinancePlanVisit fpv in financePlan.FinancePlanVisits)
                {
                    foreach (FinancePlanVisitInterestRateHistory history in fpv.FinancePlanVisitInterestRateHistories)
                    {
                        history.InsertDate = history.InsertDate.AddSeconds(-1);
                    }
                }
            }

            IList<FinancePlanVisitInterestDue> assessments1 = svc.AssessInterest(financePlan, dt, dt.AddMonths(1), dt);
            Assert.AreEqual(interestRate, assessments1.FirstOrDefault()?.InterestRateUsedToAssess ?? interestRate);

        }

        #endregion

        #region reassessment - commented out

        /*

        [Test]
        public void FinancePlan_ReAssessInterest_WithoutNetNegativeAdjustments()
        {
            //Arrange
            this.SetupMockForReassessment();
            IFinancePlanService service = this.CreateFinancePlanService();

            //Act
            service.ReAssessInterest(this.Guarantor, this.previousStatementEndDate, this.nextEndDate, this.statementPeriodResults, new List<VisitTransaction>());

            //Assert
            //since there wasnt any net negative adjustments it shouldnt even look up FinancePlans
            Assert.AreEqual(this.FinancePlansWereRetrieved, false);
        }

        [Test]
        public void FinancePlan_ReAssessInterest_WithNetNegative_WhenNoInterest_NothingHappens()
        {
            //Arrange
            this.SetupMockForReassessment();
            this.SetupVisitsAndFinancePlans(new List<decimal>() { 3000m, 2000m, 100m }, new List<decimal>() { -100m }, false);
            IFinancePlanService service = this.CreateFinancePlanService();

            int transactionCountBefore = this.visitsList.SelectMany(x => x.Transactions).Count();
            //Act
            service.ReAssessInterest(this.Guarantor, this.previousStatementEndDate, this.nextEndDate, this.statementPeriodResults, new List<VisitTransaction>());

            int transactionCountAfter = this.visitsList.SelectMany(x => x.Transactions).Count();
            //Assert
            Assert.AreEqual(transactionCountAfter, transactionCountBefore);
        }

        [Test]
        [Ignore(ObsoleteDescription.VisitTransactions)]
        public void FinancePlan_ReAssessInterest_WithNetNegative_WhenInterest_InterestRateDoesntChange()
        {
            //Arrange
            this.SetupMockForReassessment();
            this.SetupVisitsAndFinancePlans(new List<decimal>() { 3000m, 2000m, 100m }, new List<decimal>() { -100m }, true);
            IFinancePlanService service = this.CreateFinancePlanService();
            FinancePlan financePlan = financePlans.FirstOrDefault();
            FinancePlanOffer selectedOffer = financePlan.FinancePlanOffer;
            FinancePlanOffer newOfferThatShouldGetSelected = adjustedOffers.FirstOrDefault(x => x.InterestRate == selectedOffer.InterestRate && x.InterestFreePeriod == selectedOffer.InterestFreePeriod);
            newOfferThatShouldGetSelected.MaxPayment = financePlan.PaymentAmount + 0.01m;
            newOfferThatShouldGetSelected.MinPayment = financePlan.PaymentAmount - 0.01m;

            int transactionCountBefore = this.visitsList.SelectMany(x => x.Transactions).Count();
            decimal totalFpBefore = this.financePlans.Sum(x => x.CurrentFinancePlanBalance);
            //Act
            service.ReAssessInterest(this.Guarantor, this.previousStatementEndDate, this.nextEndDate, this.statementPeriodResults, new List<VisitTransaction>());

            int transactionCountAfter = this.visitsList.SelectMany(x => x.Transactions).Count();
            decimal totalFpAfter = this.financePlans.Sum(x => x.CurrentFinancePlanBalance);
            //Assert
            Assert.AreNotEqual(transactionCountBefore, transactionCountAfter);
            Assert.Greater(totalFpBefore, totalFpAfter);
        }

        [Test]
        [Ignore(ObsoleteDescription.VisitTransactions)]
        public void FinancePlan_ReAssessInterest_WithNetNegative_WhenInterest_InterestRateChanges()
        {
            //Arrange
            this.SetupMockForReassessment();
            this.SetupVisitsAndFinancePlans(new List<decimal>() { 3000m, 2000m, 100m }, new List<decimal>() { -100m }, true);
            IFinancePlanService service = this.CreateFinancePlanService();
            FinancePlan financePlan = financePlans.FirstOrDefault();
            FinancePlanOffer selectedOffer = financePlan.FinancePlanOffer;
            FinancePlanOffer newOfferThatShouldGetSelected = adjustedOffers.OrderByDescending(x => x.InterestRate).FirstOrDefault(x => x.InterestRate < selectedOffer.InterestRate);
            newOfferThatShouldGetSelected.MaxPayment = financePlan.PaymentAmount + 0.01m;
            newOfferThatShouldGetSelected.MinPayment = financePlan.PaymentAmount - 0.01m;

            int transactionCountBefore = this.visitsList.SelectMany(x => x.Transactions).Count();
            decimal totalFpBefore = this.financePlans.Sum(x => x.CurrentFinancePlanBalance);
            //Act
            service.ReAssessInterest(this.Guarantor, this.previousStatementEndDate, this.nextEndDate, this.statementPeriodResults, new List<VisitTransaction>());

            int transactionCountAfter = this.visitsList.SelectMany(x => x.Transactions).Count();
            decimal totalFpAfter = this.financePlans.Sum(x => x.CurrentFinancePlanBalance);
            //Assert
            Assert.AreNotEqual(transactionCountAfter, transactionCountBefore);
            Assert.Greater(totalFpBefore, totalFpAfter);
        }

        //Failing randomly.  Need to look into.
        //Seems like it's working now?  gonna uncomment it for now
        [Test]
        [Ignore(ObsoleteDescription.VisitTransactions)]
        public void FinancePlan_ReAssessInterest_WithNetNegative_WhenInterest_InterestRateGoesToZero()
        {
            //Arrange
            this.SetupMockForReassessment();
            this.SetupVisitsAndFinancePlans(new List<decimal>() { 3000m, 2000m, 100m }, new List<decimal>() { -100m }, true);
            IFinancePlanService service = this.CreateFinancePlanService();
            FinancePlan financePlan = financePlans.FirstOrDefault();
            FinancePlanOffer selectedOffer = financePlan.FinancePlanOffer;
            FinancePlanOffer newOfferThatShouldGetSelected = adjustedOffers.OrderBy(x => x.InterestRate).FirstOrDefault(x => x.InterestRate < selectedOffer.InterestRate);
            newOfferThatShouldGetSelected.MaxPayment = financePlan.PaymentAmount + 0.01m;
            newOfferThatShouldGetSelected.MinPayment = financePlan.PaymentAmount - 0.01m;

            int transactionCountBefore = this.visitsList.SelectMany(x => x.Transactions).Count();
            decimal totalFpBefore = this.financePlans.Sum(x => x.CurrentFinancePlanBalance);
            //Act
            service.ReAssessInterest(this.Guarantor, this.previousStatementEndDate, this.nextEndDate, this.statementPeriodResults, new List<VisitTransaction>());

            int transactionCountAfter = this.visitsList.SelectMany(x => x.Transactions).Count();
            decimal totalFpAfter = this.financePlans.Sum(x => x.CurrentFinancePlanBalance);
            //Assert
            Assert.AreNotEqual(transactionCountAfter, transactionCountBefore);
            Assert.Greater(totalFpBefore, totalFpAfter);
            Assert.AreEqual(financePlan.InterestAssessed, 0);
        }

        [Test]
        [Ignore(ObsoleteDescription.VisitTransactions)]
        public void FinancePlan_ReAssessInterest_WithNetNegative_WhenInterest_InterestRateDoesntChange_MultiplePeriods()
        {
            IList<StatementPeriodResult> usedResults = new List<StatementPeriodResult>();
            StatementPeriodResult previousStatement;
            //Default is 100 a month

            //Arrange
            this.SetupMockForReassessment();
            this.SetupVisitsAndFinancePlans(new List<decimal>() { 3000m, 2000m, 100m }, null, false);
            FinancePlan financePlan = financePlans.FirstOrDefault();
            IFinancePlanService service = this.CreateFinancePlanService();
            FinancePlanOffer selectedOffer = financePlan.FinancePlanOffer;
            FinancePlanOffer newOfferThatShouldGetSelected = adjustedOffers.FirstOrDefault(x => x.InterestRate == selectedOffer.InterestRate && x.InterestFreePeriod == selectedOffer.InterestFreePeriod);
            newOfferThatShouldGetSelected.MaxPayment = financePlan.PaymentAmount + 0.01m;
            newOfferThatShouldGetSelected.MinPayment = financePlan.PaymentAmount - 0.01m;


            //loop through a few periods

            StatementPeriodResult nextStatement = this.statementPeriodResults.OrderBy(x => x.PeriodEndDate).FirstOrDefault(x => usedResults.All(y => y.PeriodEndDate != x.PeriodEndDate));
            usedResults.Add(nextStatement);
            //Assess interest.
            this.AssessInterestOnSingleStatementPeriod(financePlan, nextStatement);
            //make payment
            this.MakePaymentOnFinancePlan(financePlan, this.visitsList.Where(x => financePlan.FinancePlanVisits.Select(z => z.VisitId).Contains(x.VisitId)).ToList());

            previousStatement = nextStatement;
            nextStatement = this.statementPeriodResults.OrderBy(x => x.PeriodEndDate).FirstOrDefault(x => usedResults.All(y => y.PeriodEndDate != x.PeriodEndDate));
            usedResults.Add(nextStatement);
            //Assess interest.
            this.AssessInterestOnSingleStatementPeriod(financePlan, nextStatement);
            //make payment
            this.MakePaymentOnFinancePlan(financePlan, this.visitsList.Where(x => financePlan.FinancePlanVisits.Select(z => z.VisitId).Contains(x.VisitId)).ToList());
            List<decimal> netNegative = new List<decimal>() { -100m };
            this.AddNetNegativeTransactions(netNegative, nextStatement.PeriodEndDate.AddDays(1));
            this.GenerateFinancePlanVisitPeriodTotals(netNegative, financePlan);


            int transactionCountBefore = this.visitsList.SelectMany(x => x.Transactions).Count();
            decimal totalFpBefore = this.financePlans.Sum(x => x.CurrentFinancePlanBalance);
            decimal interestBeforeAndInterestPayment = this.visitsList.SelectMany(x => x.Transactions).Where(x => x.VpTransactionType.IsInterest()).Sum(y => y.VisitTransactionAmount.TransactionAmount);
            decimal interestBeforeAssessed = this.visitsList.SelectMany(x => x.Transactions).Where(x => x.VpTransactionType.IsInterestAssessed()).Sum(y => y.VisitTransactionAmount.TransactionAmount);


            //Act
            service.ReAssessInterest(this.Guarantor, nextStatement.PeriodEndDate, this.statementPeriodResults.OrderBy(x => x.PeriodEndDate).FirstOrDefault(x => usedResults.All(y => y.PeriodEndDate != x.PeriodEndDate)).PeriodEndDate, usedResults, new List<VisitTransaction>());

            int transactionCountAfter = this.visitsList.SelectMany(x => x.Transactions).Count();
            decimal interestAfterAndInterestPayment = this.visitsList.SelectMany(x => x.Transactions).Where(x => x.VpTransactionType.IsInterest()).Sum(y => y.VisitTransactionAmount.TransactionAmount);
            decimal interestAfterAssessed = this.visitsList.SelectMany(x => x.Transactions).Where(x => x.VpTransactionType.IsInterestAssessed()).Sum(y => y.VisitTransactionAmount.TransactionAmount);
            decimal totalFpAfter = this.financePlans.Sum(x => x.CurrentFinancePlanBalance);
            //Assert
            Assert.AreNotEqual(transactionCountBefore, transactionCountAfter);
            Assert.Greater(totalFpBefore, totalFpAfter);


            //loop through a few periods (2 more)

            previousStatement = nextStatement;
            nextStatement = this.statementPeriodResults.OrderBy(x => x.PeriodEndDate).FirstOrDefault(x => usedResults.All(y => y.PeriodEndDate != x.PeriodEndDate));
            usedResults.Add(nextStatement);
            //Assess interest.
            this.AssessInterestOnSingleStatementPeriod(financePlan, nextStatement);
            //make payment
            this.MakePaymentOnFinancePlan(financePlan, this.visitsList.Where(x => financePlan.FinancePlanVisits.Select(z => z.VisitId).Contains(x.VisitId)).ToList());

            previousStatement = nextStatement;
            nextStatement = this.statementPeriodResults.OrderBy(x => x.PeriodEndDate).FirstOrDefault(x => usedResults.All(y => y.PeriodEndDate != x.PeriodEndDate));
            usedResults.Add(nextStatement);
            //Assess interest.
            this.AssessInterestOnSingleStatementPeriod(financePlan, nextStatement);
            //make payment
            this.MakePaymentOnFinancePlan(financePlan, this.visitsList.Where(x => financePlan.FinancePlanVisits.Select(z => z.VisitId).Contains(x.VisitId)).ToList());
            this.AddNetNegativeTransactions(netNegative, nextStatement.PeriodEndDate.AddDays(1));
            this.GenerateFinancePlanVisitPeriodTotals(netNegative, financePlan);

            decimal interestBeforeAssessed2 = this.visitsList.SelectMany(x => x.Transactions).Where(x => x.VpTransactionType.IsInterestAssessed()).Sum(y => y.VisitTransactionAmount.TransactionAmount);

            //ReAssess
            service.ReAssessInterest(this.Guarantor, nextStatement.PeriodEndDate, this.statementPeriodResults.OrderBy(x => x.PeriodEndDate).FirstOrDefault(x => usedResults.All(y => y.PeriodEndDate != x.PeriodEndDate)).PeriodEndDate, usedResults, new List<VisitTransaction>());

            int transactionCountAfter2 = this.visitsList.SelectMany(x => x.Transactions).Count();
            decimal interestAfterAssessed2 = this.visitsList.SelectMany(x => x.Transactions).Where(x => x.VpTransactionType.IsInterestAssessed()).Sum(y => y.VisitTransactionAmount.TransactionAmount);

            Assert.GreaterOrEqual(interestBeforeAssessed2, interestAfterAssessed2);
        }

        [Test]
        public void FinancePlanService_BuildReassessmentNodes()
        {
            this.SetupMockForReassessment();
            IFinancePlanService service = this.CreateFinancePlanService();

            this.nextEndDate = new DateTime(2016, 11, 8, 0, 0, 0);
            IList<StatementPeriodResult> statementPeriods = this.GenerateStatementPeriodsList(5);
            StatementPeriodResult firstStatementPeriod = statementPeriods.OrderBy(x => x.PeriodStartDate).First();
            this.SetupVisitsAndFinancePlans(new List<decimal>() { 3000m, 300m, 10m, 1.45m, 51.44m, 24.22m }, new List<decimal>() { -100m }, true);

            IList<InterestAssessPeriodVisitNode> nodes = new List<InterestAssessPeriodVisitNode>();

            service.BuildInterestAssessNodes(this.nextEndDate, statementPeriods, this.financePlans, nodes);
            //Expect it to generate finance
            //This assumes all statement periods for the finance plan which isnt super realistic.
            int numberOfNodes = financePlans.Count * (statementPeriods.Count + 1) * financePlans.Sum(x => x.FinancePlanVisits.Count);
            Assert.AreEqual(numberOfNodes, nodes.Count);
        }

        */

        #endregion

        #region cancel

        [Test]
        public void CancelPendingFinancePlans_Online_Cancels()
        {
            FinancePlanServiceMockBuilder builder = new FinancePlanServiceMockBuilder();
            decimal monthlyPaymentAmount = 100m;
            FinancePlan fp = this.CreateFinancePlanWithActiveAndUncollectableVisits(monthlyPaymentAmount, FinancePlanStatusEnum.PendingAcceptance);
            this.SetupClient();

            builder.Setup(mock =>
            {
                this.SetupMockMethodsForFinancePlanService(mock);
                mock.FinancePlanRepositoryMock.Setup(x => x.GetAllPendingAcceptanceFinancePlans(It.IsAny<int>(), It.IsAny<DateTime>())).Returns(new List<FinancePlan> { fp });
                mock.ClientServiceMock.Setup(x => x.GetClient()).Returns(this._client);
            });

            IFinancePlanService svc = builder.CreateService();

            svc.CancelPendingTermsAcceptanceFinancePlansForGuarantor(fp.VpGuarantor, fp.CreatedVpStatement.PaymentDueDate, DateTime.UtcNow);

            Assert.AreEqual(FinancePlanStatusEnum.Canceled, fp.FinancePlanStatus.FinancePlanStatusEnum);
        }

        [Test]
        public void CancelPendingFinancePlans_Online_StaysPending()
        {
            FinancePlanServiceMockBuilder builder = new FinancePlanServiceMockBuilder();
            decimal monthlyPaymentAmount = 100m;
            FinancePlan fp = this.CreateFinancePlanWithActiveAndUncollectableVisits(monthlyPaymentAmount, FinancePlanStatusEnum.PendingAcceptance, DateTime.UtcNow.AddDays(14));
            this.SetupClient();

            builder.Setup(mock =>
            {
                this.SetupMockMethodsForFinancePlanService(mock);
                mock.FinancePlanRepositoryMock.Setup(x => x.GetAllPendingAcceptanceFinancePlans(It.IsAny<int>(), It.IsAny<DateTime>())).Returns(new List<FinancePlan> { fp });
                mock.ClientServiceMock.Setup(x => x.GetClient()).Returns(this._client);
            });

            IFinancePlanService svc = builder.CreateService();
            fp.VpGuarantor.NextStatementDate = DateTime.UtcNow;
            svc.CancelPendingTermsAcceptanceFinancePlansForGuarantor(fp.VpGuarantor, fp.CreatedVpStatement.PaymentDueDate, DateTime.UtcNow);

            Assert.AreEqual(FinancePlanStatusEnum.Canceled, fp.FinancePlanStatus.FinancePlanStatusEnum);
        }

        [Test]
        public void CancelPendingFinancePlans_Online_NextStatement_Cancels()
        {
            FinancePlanServiceMockBuilder builder = new FinancePlanServiceMockBuilder();
            decimal monthlyPaymentAmount = 100m;
            FinancePlan fp = this.CreateFinancePlanWithActiveAndUncollectableVisits(monthlyPaymentAmount, FinancePlanStatusEnum.PendingAcceptance);
            this.SetupClient();

            builder.Setup(mock =>
            {
                this.SetupMockMethodsForFinancePlanService(mock);
                mock.FinancePlanRepositoryMock.Setup(x => x.GetAllPendingAcceptanceFinancePlans(It.IsAny<int>(), It.IsAny<DateTime>())).Returns(new List<FinancePlan> { fp });
                mock.ClientServiceMock.Setup(x => x.GetClient()).Returns(this._client);
            });

            IFinancePlanService svc = builder.CreateService();
            VpStatement nextStatement = VpStatementFactory.GenerateStatementPaymentDueDate(fp.CreatedVpStatement.PaymentDueDate.Date.AddMonths(1));
            svc.CancelPendingTermsAcceptanceFinancePlansForGuarantor(fp.VpGuarantor, fp.CreatedVpStatement.PaymentDueDate, DateTime.UtcNow);

            Assert.AreEqual(FinancePlanStatusEnum.Canceled, fp.FinancePlanStatus.FinancePlanStatusEnum);
        }

        [Test]
        public void CancelPendingFinancePlans_Offline_StaysPending()
        {
            FinancePlanServiceMockBuilder builder = new FinancePlanServiceMockBuilder();
            decimal monthlyPaymentAmount = 100m;
            FinancePlan fp = this.CreateFinancePlanWithActiveAndUncollectableVisits(monthlyPaymentAmount, FinancePlanStatusEnum.PendingAcceptance, DateTime.UtcNow, GuarantorTypeEnum.Offline);
            this.SetupClient();

            builder.Setup(mock =>
            {
                this.SetupMockMethodsForFinancePlanService(mock);
                mock.FinancePlanRepositoryMock.Setup(x => x.GetAllPendingAcceptanceFinancePlans(It.IsAny<int>(), It.IsAny<DateTime>())).Returns(new List<FinancePlan> { fp });
                mock.ClientServiceMock.Setup(x => x.GetClient()).Returns(this._client);
            });

            IFinancePlanService svc = builder.CreateService();

            svc.CancelPendingTermsAcceptanceFinancePlansForGuarantor(fp.VpGuarantor, fp.CreatedVpStatement.PaymentDueDate, DateTime.UtcNow);

            Assert.AreEqual(FinancePlanStatusEnum.PendingAcceptance, fp.FinancePlanStatus.FinancePlanStatusEnum);
        }

        [Test]
        public void CancelPendingFinancePlans_Offline_Cancels()
        {
            FinancePlanServiceMockBuilder builder = new FinancePlanServiceMockBuilder();
            decimal monthlyPaymentAmount = 100m;
            FinancePlan fp = this.CreateFinancePlanWithActiveAndUncollectableVisits(monthlyPaymentAmount, FinancePlanStatusEnum.PendingAcceptance, DateTime.UtcNow.AddDays(-5), GuarantorTypeEnum.Offline);
            this.SetupClient();

            builder.Setup(mock =>
            {
                this.SetupMockMethodsForFinancePlanService(mock);
                mock.FinancePlanRepositoryMock.Setup(x => x.GetAllPendingAcceptanceFinancePlans(It.IsAny<int>(), It.IsAny<DateTime>())).Returns(new List<FinancePlan> { fp });
                mock.ClientServiceMock.Setup(x => x.GetClient()).Returns(this._client);
            });

            IFinancePlanService svc = builder.CreateService();

            svc.CancelPendingTermsAcceptanceFinancePlansForGuarantor(fp.VpGuarantor, fp.CreatedVpStatement.PaymentDueDate, DateTime.UtcNow);

            Assert.AreEqual(FinancePlanStatusEnum.Canceled, fp.FinancePlanStatus.FinancePlanStatusEnum);
        }

        [Test]
        public void CancelPendingFinancePlans_Offline_NextStatement_Cancels()
        {
            FinancePlanServiceMockBuilder builder = new FinancePlanServiceMockBuilder();
            decimal monthlyPaymentAmount = 100m;
            FinancePlan fp = this.CreateFinancePlanWithActiveAndUncollectableVisits(monthlyPaymentAmount, FinancePlanStatusEnum.PendingAcceptance, guarantorType: GuarantorTypeEnum.Offline);
            this.SetupClient();

            builder.Setup(mock =>
            {
                this.SetupMockMethodsForFinancePlanService(mock);
                mock.FinancePlanRepositoryMock.Setup(x => x.GetAllPendingAcceptanceFinancePlans(It.IsAny<int>(), It.IsAny<DateTime>())).Returns(new List<FinancePlan> { fp });
                mock.ClientServiceMock.Setup(x => x.GetClient()).Returns(this._client);
            });


            IFinancePlanService svc = builder.CreateService();
            VpStatement nextStatement = VpStatementFactory.GenerateStatementPaymentDueDate(fp.CreatedVpStatement.PaymentDueDate.Date.AddMonths(1));
            svc.CancelPendingTermsAcceptanceFinancePlansForGuarantor(fp.VpGuarantor, nextStatement.PaymentDueDate, DateTime.UtcNow);

            Assert.AreEqual(FinancePlanStatusEnum.Canceled, fp.FinancePlanStatus.FinancePlanStatusEnum);
        }

        #endregion

        #region PaymentBucketTests

        private IList<FinancePlanOffer> GenerateOffers()
        {
            Guid aGuid = Guid.NewGuid();
            return new List<FinancePlanOffer>()
            {
                new FinancePlanOffer()
                {
                    CorrelationGuid = aGuid,
                    MinPayment = 0,
                    MaxPayment = 1000,
                    InterestRate = .02m
                },
                new FinancePlanOffer()
                {
                    CorrelationGuid = aGuid,
                    MinPayment = 1001,
                    MaxPayment = 2000,
                    InterestRate = .03m
                },
                new FinancePlanOffer()
                {
                    CorrelationGuid = aGuid,
                    MinPayment = 2001,
                    MaxPayment = 4000,
                    InterestRate = .04m
                }
            };
        }

        private void SetupMockMethodsForFinancePlanService(FinancePlanServiceMockBuilder mock)
        {
            mock.ClientServiceMock = new ClientServiceMockBuilder().CreateMock();
            mock.FinancePlanRepositoryMock.Setup(x => x.GetAllOpenFinancePlans(It.IsAny<IList<int>>())).Returns(new List<FinancePlan>());
            mock.FinancePlanOfferServiceMock.Setup(x => x.GenerateOffersForUser(
                It.IsAny<FinancePlanCalculationParameters>(),
                It.IsAny<IList<FinancePlan>>(),
                It.IsAny<bool>(),
                It.IsAny<bool>(),
                //It.IsAny<bool>(),
                It.IsAny<FirstInterestPeriodEnum>())).Returns(this.GenerateOffers());

            mock.FinancePlanRepositoryMock.Setup(x => x.GetFinancePlanStatuses()).Returns(new List<FinancePlanStatus>()
            {
                FinancePlanFactory.GenerateFinancePlanStatus(FinancePlanStatusEnum.GoodStanding),
                FinancePlanFactory.GenerateFinancePlanStatus(FinancePlanStatusEnum.PendingAcceptance),
                FinancePlanFactory.GenerateFinancePlanStatus(FinancePlanStatusEnum.Canceled),
                FinancePlanFactory.GenerateFinancePlanStatus(FinancePlanStatusEnum.PendingAcceptance),
                FinancePlanFactory.GenerateFinancePlanStatus(FinancePlanStatusEnum.NonPaidInFull),
                FinancePlanFactory.GenerateFinancePlanStatus(FinancePlanStatusEnum.PastDue),
                FinancePlanFactory.GenerateFinancePlanStatus(FinancePlanStatusEnum.PaidInFull),
                FinancePlanFactory.GenerateFinancePlanStatus(FinancePlanStatusEnum.TermsAccepted),
                FinancePlanFactory.GenerateFinancePlanStatus(FinancePlanStatusEnum.UncollectableActive),
                FinancePlanFactory.GenerateFinancePlanStatus(FinancePlanStatusEnum.UncollectableClosed),
            });
            mock.FinancePlanRepositoryMock.Setup(x => x.AreVisitsOnActiveFinancePlan(It.IsAny<IList<Visit>>(), It.IsAny<DateTime?>())).Returns(new List<Visit>());
            mock.FinancePlanRepositoryMock.Setup(x => x.AreVisitsOnActiveOrPendingFinancePlan(It.IsAny<IList<Visit>>(), It.IsAny<DateTime?>())).Returns(new List<Visit>());
        }

        private FinancePlan CreateFinancePlanWithActiveVisits(decimal monthlyPaymentAmount)
        {
            VpStatement statement = VpStatementFactory.GenerateStatementPaymentDueDateInXDays(10);
            List<Tuple<Visit, IList<VisitTransaction>>> visitsAndTransactions = this.CreateActiveVisits().Select(x => new Tuple<Visit, IList<VisitTransaction>>(x, new List<VisitTransaction>())).ToList();
            VpStatementFactory.AddVisitsToStatement(visitsAndTransactions, statement);

            FinancePlan fp = FinancePlanFactory.GenerateWithStatus(visitsAndTransactions, this.GenerateOffers().FirstOrDefault(), statement.PaymentDueDate, statement, monthlyPaymentAmount, FinancePlanStatusEnum.TermsAccepted);
            return fp;
        }

        private FinancePlan CreateFinancePlanWithActiveAndUncollectableVisits(
            decimal monthlyPaymentAmount,
            FinancePlanStatusEnum fpStatus = FinancePlanStatusEnum.TermsAccepted,
            DateTime? insertDate = null,
            GuarantorTypeEnum guarantorType = GuarantorTypeEnum.Online)
        {
            VpStatement statement = VpStatementFactory.GenerateStatementPaymentDueDateInXDays(10);
            Guarantor guarantor = new GuarantorBuilder().WithDefaults().WithType(guarantorType);
            List<Tuple<Visit, IList<VisitTransaction>>> visitsAndTransactions = this.CreateActiveVisits(guarantor).Select(x => new Tuple<Visit, IList<VisitTransaction>>(x, new List<VisitTransaction>())).ToList();
            visitsAndTransactions.Add(VisitFactory.GenerateActiveUncollectableVisit(111m, 1, guarantor: guarantor));
            VpStatementFactory.AddVisitsToStatement(visitsAndTransactions, statement);

            FinancePlan fp = FinancePlanFactory.GenerateWithStatus(visitsAndTransactions, this.GenerateOffers().FirstOrDefault(), statement.PaymentDueDate, statement, monthlyPaymentAmount, fpStatus, insertDate);
            return fp;
        }

        private List<Visit> CreateActiveVisits(Guarantor guarantor = null)
        {
            guarantor = guarantor ?? GuarantorFactory.GenerateOnline();
            List<Visit> activeVisits = new List<Visit>();
            for (int i = 0; i < 5; i++)
            {
                activeVisits.Add(VisitFactory.GenerateActiveVisit(100m + (decimal)i, 2, VisitStateEnum.Active, guarantor: guarantor).Item1);
            }
            return activeVisits;
        }

        [Test]
        public void FinancePlan_OriginatePendingFinancePlan_ActiveUncollectable()
        {
            FinancePlanServiceMockBuilder builder = new FinancePlanServiceMockBuilder();
            decimal monthlyPaymentAmount = 100m;
            builder.Setup(mock =>
            {
                this.SetupMockMethodsForFinancePlanService(mock);
            });
            FinancePlan fp = this.CreateFinancePlanWithActiveAndUncollectableVisits(monthlyPaymentAmount);

            IFinancePlanService svc = builder.CreateService();
            FinancePlan originatedFp = svc.OriginateFinancePlan(fp, CreateCreditAgreement(), 0);

            Assert.AreEqual(621, originatedFp.OriginatedFinancePlanBalance);
        }

        [Test]
        public void FinancePlan_OriginatePendingFinancePlan_Originates()
        {
            FinancePlanServiceMockBuilder builder = new FinancePlanServiceMockBuilder();
            decimal monthlyPaymentAmount = 100m;
            builder.Setup(mock =>
            {
                this.SetupMockMethodsForFinancePlanService(mock);
            });
            FinancePlan fp = this.CreateFinancePlanWithActiveVisits(monthlyPaymentAmount);


            IFinancePlanService svc = builder.CreateService();

            FinancePlan originatedFp = svc.OriginateFinancePlan(fp, CreateCreditAgreement(), 0);

            Assert.AreEqual(FinancePlanStatusEnum.GoodStanding, originatedFp.FinancePlanStatus.FinancePlanStatusEnum);
        }

        [Test]
        public void FinancePlan_OriginatePendingFinancePlan_Cancels()
        {
            FinancePlanServiceMockBuilder builder = new FinancePlanServiceMockBuilder();
            decimal monthlyPaymentAmount = 100m;
            builder.Setup(mock =>
            {
                this.SetupMockMethodsForFinancePlanService(mock);
            });
            FinancePlan fp = this.CreateFinancePlanWithActiveVisits(monthlyPaymentAmount);

            foreach (FinancePlanVisit fpActiveFinancePlanVisit in fp.ActiveFinancePlanVisits)
            {
                fpActiveFinancePlanVisit.CurrentVisitState = VisitStateEnum.Inactive;
            }

            IFinancePlanService svc = builder.CreateService();

            FinancePlan originatedFp = svc.OriginateFinancePlan(fp, CreateCreditAgreement(), 0);

            Assert.AreEqual(FinancePlanStatusEnum.Canceled, originatedFp.FinancePlanStatus.FinancePlanStatusEnum);
        }

        [Test]
        public void FinancePlan_RemovePastDueBucket()
        {
            FinancePlan financePlan = FinancePlanTests.CreateFinancePlan(1000, 100);

            int vpStatementId = 1;

            //Create an Amount Due for the statement 3 months ago
            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddMonths(-3), vpStatementId++);
            //Create an Amount Due for the statement 2 months ago
            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddMonths(-2), vpStatementId++);
            //Create an Amount Due for the statement 1 months ago
            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddMonths(-1), vpStatementId++);
            //Create an Amount Due for the statement 0 months ago (current)
            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddMonths(0), vpStatementId++);

            //Check to see that there are 4 Bucket(s), $100 current, $300 past due, $100 in Bucket 0, $100 in Bucket 1, $100 in Bucket 2 and $100 in Bucket 3
            FinancePlanTests.AssertCommon(financePlan, 4, 100, 300, 100, 100, 100, 100);

            IFinancePlanService service = this.CreateFinancePlanService();

            service.RemovePastDueBucket(financePlan, 3, "Remove 3", 1);
            FinancePlanTests.AssertCommon(financePlan, 3, 100, 200, 100, 100, 100, 0);

            service.RemovePastDueBucket(financePlan, 2, "Remove 2", 1);
            FinancePlanTests.AssertCommon(financePlan, 2, 100, 100, 100, 100, 0, 0);

            service.RemovePastDueBucket(financePlan, 1, "Remove 1", 1);
            FinancePlanTests.AssertCommon(financePlan, 1, 100, 0, 100, 0, 0, 0);

            service.RemovePastDueBucket(financePlan, 0, "Remove 0", 1);
            FinancePlanTests.AssertCommon(financePlan, 0, 0, 0, 0, 0, 0, 0);
        }

        /*[Test]
        [Ignore(ObsoleteDescription.VisitTransactions)]
        public void FinancePlan_PostponePastDueBucket()
        {
            FinancePlan financePlan = FinancePlanTests.CreateFinancePlan(1000, 100);

            int vpStatementId = 1;

            //Create an Amount Due for the statement 3 months ago
            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddMonths(-3), vpStatementId++);
            //Create an Amount Due for the statement 2 months ago
            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddMonths(-2), vpStatementId++);
            //Create an Amount Due for the statement 1 months ago
            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddMonths(-1), vpStatementId++);
            //Create an Amount Due for the statement 0 months ago (current)
            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddMonths(0), vpStatementId++);

            //Check to see that there are 4 Bucket(s), $100 current, $300 past due, $100 in Bucket 0, $100 in Bucket 1, $100 in Bucket 2 and $100 in Bucket 3
            FinancePlanTests.AssertCommon(financePlan, 4, 100, 300, 100, 100, 100, 100);

            IFinancePlanService service = this.CreateFinancePlanService();

            service.PostponePastDueBucket(financePlan, 3, DateTime.UtcNow.AddMonths(1), "test 3 => 2", 1);
            FinancePlanTests.AssertCommon(financePlan, 3, 100, 300, 100, 100, 200, 0);

            service.PostponePastDueBucket(financePlan, 2, DateTime.UtcNow.AddMonths(1), "test 2 => 1", 1);
            FinancePlanTests.AssertCommon(financePlan, 2, 100, 300, 100, 300, 0, 0);

            service.PostponePastDueBucket(financePlan, 1, DateTime.UtcNow.AddMonths(1), "test 1 => 0", 1);
            FinancePlanTests.AssertCommon(financePlan, 1, 400, 0, 400, 0, 0, 0);
        }*/

        #endregion

        #region create / update (checks buckets, status)

        [Test]
        public void FinancePlan_CreateFinancePlan_GracePeriod()
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            VpStatement statement = VpStatementFactory.GenerateStatementPaymentDueDateInXDays(10);

            List<Tuple<Visit, IList<VisitTransaction>>> visitsAndTransactions = Enumerable.Range(0, 5).Select(i => VisitFactory.GenerateActiveVisit(100m + i, 2, VisitStateEnum.Active, guarantor: guarantor)).ToList();
            VpStatementFactory.AddVisitsToStatement(visitsAndTransactions, statement);

            IFinancePlanService svc = new FinancePlanServiceMockBuilder().Setup(this.SetupMockMethodsForFinancePlanService).CreateService();

            FinancePlanCalculationParameters financePlanCalculationParameters = new FinancePlanCalculationParameters(
                guarantor, 
                statement, 
                100m, 
                1, 
                OfferCalculationStrategyEnum.MonthlyPayment, 
                InterestCalculationMethodEnum.Monthly,
                statement.PaymentDueDate)
            {
                Visits = this.GetFinancePlanCalculatedVisitTotal(statement)
            };
            FinancePlan result = svc.CreateFinancePlan(financePlanCalculationParameters, -1, CreateCreditAgreement(), null);

            Assert.AreEqual(FinancePlanStatusEnum.GoodStanding, result.FinancePlanStatus.FinancePlanStatusEnum);
            Assert.AreEqual(0, result.CurrentBucket);
            Assert.AreEqual(100m, result.AmountDue);
            Assert.AreEqual(1, result.FinancePlanAmountsDue.Count);
            Assert.IsNotNull(result.FinancePlanPublicIdPhi);
            Assert.True(FinancePlanPublicIdPhiSource.IsValidFormat(result.FinancePlanPublicIdPhi));
        }

        private IList<IFinancePlanVisitBalance> GetFinancePlanCalculatedVisitTotal(VpStatement newStatement)
        {
            IList<VpStatementVisit> nonFinancedVpStatementVisits = newStatement.VpStatementVisits;

            return nonFinancedVpStatementVisits
                .Select(x => x.Visit)
                .Select(x => new FinancePlanSetupVisit(
                    x.VisitId,
                    x.CurrentBalance,
                    0,
                    0,
                    VisitStateEnum.Active,
                    x.BillingApplication,
                    x.InterestZero
                )).Cast<IFinancePlanVisitBalance>().ToList();
        }

        [Test]
        public void FinancePlan_CreateFinancePlan_AwaitingStatement()
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            VpStatement statement = VpStatementFactory.GenerateStatementPaymentDueDateInXDays(-5);
            VpStatementFactory.AddVisitsToStatement(Enumerable.Range(0, 5).Select(i => VisitFactory.GenerateActiveVisit(100m + i, 2, VisitStateEnum.Active, guarantor: guarantor)).ToList(), statement);

            IFinancePlanService svc = new FinancePlanServiceMockBuilder().Setup(this.SetupMockMethodsForFinancePlanService).CreateService();
            
            FinancePlanCalculationParameters financePlanCalculationParameters = new FinancePlanCalculationParameters(
                guarantor,
                statement, 
                100m, 1, 
                OfferCalculationStrategyEnum.MonthlyPayment, 
                InterestCalculationMethodEnum.Monthly,
                statement.PaymentDueDate.AddMonths(1))
            {
                Visits = this.GetFinancePlanCalculatedVisitTotal(statement)
            };

            FinancePlan result = svc.CreateFinancePlan(financePlanCalculationParameters, -1, CreateCreditAgreement(), null);

            Assert.AreEqual(FinancePlanStatusEnum.GoodStanding, result.FinancePlanStatus.FinancePlanStatusEnum);
            Assert.AreEqual(0, result.CurrentBucket);
            Assert.AreEqual(0m, result.AmountDue);
            Assert.AreEqual(0, result.FinancePlanAmountsDue.Count);
            Assert.IsNotNull(result.FinancePlanPublicIdPhi);
            Assert.True(FinancePlanPublicIdPhiSource.IsValidFormat(result.FinancePlanPublicIdPhi));
        }
        
        [Test]
        public void FinancePlan_CreateFinancePlan_GracePeriod_Pending_Then_Update()
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            VpStatement statement = VpStatementFactory.GenerateStatementPaymentDueDateInXDays(10);
            VpStatementFactory.AddVisitsToStatement(Enumerable.Range(0, 5).Select(i => VisitFactory.GenerateActiveVisit(100m + i, 2, VisitStateEnum.Active, guarantor: guarantor)).ToList(), statement);

            IFinancePlanService svc = new FinancePlanServiceMockBuilder().Setup(this.SetupMockMethodsForFinancePlanService).CreateService();

            FinancePlanCalculationParameters financePlanCalculationParameters = new FinancePlanCalculationParameters(
                guarantor,
                statement,
                100m,
                1, 
                OfferCalculationStrategyEnum.MonthlyPayment, 
                InterestCalculationMethodEnum.Monthly,
                statement.PaymentDueDate)
            {
                Visits = this.GetFinancePlanCalculatedVisitTotal(statement)
            };

            FinancePlan result = svc.CreateFinancePlan(financePlanCalculationParameters, -1, null, null);

            // initial
            int countOfDue = result.FinancePlanAmountsDue.Count;
            Assert.AreEqual(1, countOfDue);
            Assert.AreEqual(100m, result.AmountDue);
            Assert.AreEqual(FinancePlanStatusEnum.PendingAcceptance, result.FinancePlanStatus.FinancePlanStatusEnum);

            this.AdjustHistoryDates(result);

            // update
            financePlanCalculationParameters.MonthlyPaymentAmount = 50m;
            result = svc.UpdateFinancePlan(result, financePlanCalculationParameters, true, -1, null);

            // there should be 2 more item in that list as it should be doing offsetting entries
            Assert.AreEqual(countOfDue + 2, result.FinancePlanAmountsDue.Count);
            Assert.AreEqual(50m, result.AmountDue);

            // originate
            svc.OriginateFinancePlan(result, CreateCreditAgreement(), -1);

            Assert.AreEqual(countOfDue + 2, result.FinancePlanAmountsDue.Count);
            Assert.AreEqual(50m, result.AmountDue);
            Assert.AreEqual(FinancePlanStatusEnum.GoodStanding, result.FinancePlanStatus.FinancePlanStatusEnum);
            Assert.IsNotNull(result.FinancePlanPublicIdPhi);
            Assert.True(FinancePlanPublicIdPhiSource.IsValidFormat(result.FinancePlanPublicIdPhi));
        }

        [Test]
        public void FinancePlan_CreateFinancePlan_AwaitingStatement_Pending_Then_Update()
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            VpStatement statement = VpStatementFactory.GenerateStatementPaymentDueDateInXDays(-5);
            VpStatementFactory.AddVisitsToStatement(Enumerable.Range(0, 5).Select(i => VisitFactory.GenerateActiveVisit(100m + i, 2, VisitStateEnum.Active, guarantor: guarantor)).ToList(), statement);

            IFinancePlanService svc = new FinancePlanServiceMockBuilder().Setup(this.SetupMockMethodsForFinancePlanService).CreateService();

            // initial
            FinancePlanCalculationParameters financePlanCalculationParameters = new FinancePlanCalculationParameters(
                guarantor, 
                statement, 
                100m, 
                1, 
                OfferCalculationStrategyEnum.MonthlyPayment, 
                InterestCalculationMethodEnum.Monthly,
                statement.PaymentDueDate.AddMonths(1))
            {
                Visits = this.GetFinancePlanCalculatedVisitTotal(statement)
            };

            FinancePlan result = svc.CreateFinancePlan(financePlanCalculationParameters, -1, null, null);
            Assert.AreEqual(0, result.FinancePlanAmountsDue.Count);
            Assert.AreEqual(0, result.AmountDue);
            Assert.AreEqual(FinancePlanStatusEnum.PendingAcceptance, result.FinancePlanStatus.FinancePlanStatusEnum);

            this.AdjustHistoryDates(result);
            
            // update
            financePlanCalculationParameters.MonthlyPaymentAmount = 50m;
            result = svc.UpdateFinancePlan(result, financePlanCalculationParameters, false, -1, null);
            Assert.AreEqual(0, result.FinancePlanAmountsDue.Count);
            Assert.AreEqual(0, result.AmountDue);

            // originate
            svc.OriginateFinancePlan(result, CreateCreditAgreement(), -1);

            Assert.AreEqual(0, result.FinancePlanAmountsDue.Count);
            Assert.AreEqual(0, result.AmountDue);
            Assert.AreEqual(FinancePlanStatusEnum.GoodStanding, result.FinancePlanStatus.FinancePlanStatusEnum);
            Assert.IsNotNull(result.FinancePlanPublicIdPhi);
            Assert.True(FinancePlanPublicIdPhiSource.IsValidFormat(result.FinancePlanPublicIdPhi));
        }

        [Test]
        public void FinancePlan_CreateFinancePlanAndUpdate_GracePeriod_AddAmountDue_PaymentAmountMoreThanDue()
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            VpStatement statement = VpStatementFactory.GenerateStatementPaymentDueDateInXDays(10);
            VpStatementFactory.AddVisitsToStatement(Enumerable.Range(0, 5).Select(i => VisitFactory.GenerateActiveVisit(100m + i, 2, VisitStateEnum.Active, guarantor: guarantor)).ToList(), statement);

            decimal totalCurrentBalance = statement.VpStatementVisits.Sum(x => x.Visit.CurrentBalance);

            IFinancePlanService svc = new FinancePlanServiceMockBuilder().Setup(this.SetupMockMethodsForFinancePlanService).CreateService();

            // first time adding an amount for the FP
            FinancePlanCalculationParameters financePlanCalculationParameters = new FinancePlanCalculationParameters(
                guarantor, 
                statement, 
                1000m, 
                1, 
                OfferCalculationStrategyEnum.MonthlyPayment, 
                InterestCalculationMethodEnum.Monthly,
                statement.PaymentDueDate)
            {
                Visits = this.GetFinancePlanCalculatedVisitTotal(statement)
            };

            FinancePlan result = svc.CreateFinancePlan(financePlanCalculationParameters, -1, null, null);
            Assert.AreEqual(1, result.FinancePlanAmountsDue.Count);
            Assert.AreEqual(totalCurrentBalance, result.AmountDue);
            Assert.AreEqual(FinancePlanStatusEnum.PendingAcceptance, result.FinancePlanStatus.FinancePlanStatusEnum);

            this.AdjustHistoryDates(result);

            // update, there should be 2 more item in that list as it should be doing offsetting entries
            financePlanCalculationParameters.MonthlyPaymentAmount = 50m;
            result = svc.UpdateFinancePlan(result, financePlanCalculationParameters, false, -1, null);
            Assert.AreEqual(3, result.FinancePlanAmountsDue.Count);
            Assert.AreEqual(50m, result.AmountDue);
            Assert.AreEqual(FinancePlanStatusEnum.PendingAcceptance, result.FinancePlanStatus.FinancePlanStatusEnum);

            this.AdjustHistoryDates(result);

            // update again, there should be 2 more item in that list as it should be doing offsetting entries
            financePlanCalculationParameters.MonthlyPaymentAmount = 750m;
            result = svc.UpdateFinancePlan(result, financePlanCalculationParameters, false, -1, null);
            Assert.AreEqual(5, result.FinancePlanAmountsDue.Count);
            Assert.AreEqual(totalCurrentBalance, result.AmountDue);
            Assert.AreEqual(FinancePlanStatusEnum.PendingAcceptance, result.FinancePlanStatus.FinancePlanStatusEnum);

            this.AdjustHistoryDates(result);

            // accept terms, there should be 2 more item in that list as it should be doing offsetting entries
            financePlanCalculationParameters.MonthlyPaymentAmount = 1000m;
            result = svc.UpdateFinancePlan(result, financePlanCalculationParameters, false, -1, null);
            Assert.AreEqual(7, result.FinancePlanAmountsDue.Count);
            Assert.AreEqual(totalCurrentBalance, result.AmountDue);

            svc.OriginateFinancePlan(result, CreateCreditAgreement(), -1);
            Assert.AreEqual(7, result.FinancePlanAmountsDue.Count);
            Assert.AreEqual(totalCurrentBalance, result.AmountDue);
            Assert.AreEqual(FinancePlanStatusEnum.GoodStanding, result.FinancePlanStatus.FinancePlanStatusEnum);
            Assert.IsNotNull(result.FinancePlanPublicIdPhi);
            Assert.True(FinancePlanPublicIdPhiSource.IsValidFormat(result.FinancePlanPublicIdPhi));
        }

        // Push back any existing FinancePlan[x]History dates that are used to derive a current value for a property
        // When tests run there are some cases when the InsertDate is too close to determine the latest and returns the wrong value
        private void AdjustHistoryDates(FinancePlan financePlan)
        {
            foreach (FinancePlanPaymentAmountHistory financePlanPaymentAmountHistory in financePlan.FinancePlanPaymentAmountHistories)
            {
                financePlanPaymentAmountHistory.InsertDate = financePlanPaymentAmountHistory.InsertDate.AddSeconds(-1);
            }

            foreach (FinancePlanStatusHistory financePlanStatusHistory in financePlan.FinancePlanStatusHistory)
            {
                financePlanStatusHistory.InsertDate = financePlanStatusHistory.InsertDate.AddSeconds(-1);
            }
        }

        #endregion

        #region origination date tests
        
        [Test]
        public void OriginationDate_GracePeriod()
        {
            DateTime paymentDueDate = DateTime.Now.AddDays(1).Date;
            
            VpStatement statement = new VpStatement
            {
                OriginalPaymentDueDate = paymentDueDate,
                PaymentDueDate = paymentDueDate
            };
            statement.StatementDate = statement.PaymentDueDate.AddDays(-21);
            
            Guarantor guarantor = new Guarantor
            {
                PaymentDueDay = paymentDueDate.Day.ToValidPaymentDueDay(),
                NextStatementDate = paymentDueDate.AddMonths(1).AddDays(-21)
            };
        
            DateTime result = this.CreateFinancePlanService().GetOriginationDate(guarantor, statement);

            Console.WriteLine(result.Date);

            Assert.AreEqual(paymentDueDate, result.Date);
        }

        [Test]
        public void OriginationDate_AwaitingStatement()
        {
            DateTime paymentDueDate = DateTime.Now.AddDays(-1).Date;
            
            VpStatement statement = new VpStatement
            {
                OriginalPaymentDueDate = paymentDueDate,
                PaymentDueDate = paymentDueDate
            };
            statement.StatementDate = statement.PaymentDueDate.AddDays(-21);
            
            Guarantor guarantor = new Guarantor
            {
                PaymentDueDay = paymentDueDate.Day.ToValidPaymentDueDay(),
                NextStatementDate = paymentDueDate.AddMonths(1).AddDays(-21)
            };
        
            DateTime result = this.CreateFinancePlanService().GetOriginationDate(guarantor, statement);

            Console.WriteLine(result.Date);

            Assert.AreEqual(guarantor.NextStatementDate, result.Date);
        }

        #endregion

        // VPNG-21551
        [TestCase(true)] // normal statement, grace period
        [TestCase(false)] // normal statement, awaiting statement
        [TestCase(true, true)] // normal statement, grace period, combined plan
        [TestCase(true, true, true)] // normal statement, grace period, combined plan, past due plans
        [TestCase(false, true)] // normal statement, awaiting statement, combined plan
        public void FinancePlan_UpdatePendingPlan_CheckAmountDue(bool isGracePeriod, bool isCombined = false, bool hasPastDuePlans = false)
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();

            // create statement
            int dateOffset = isGracePeriod ? 7 : -7;
            VpStatement statement = VpStatementFactory.GenerateStatementPaymentDueDateInXDays(dateOffset);

            // add visits
            VpStatementFactory.AddVisitsToStatement(Enumerable.Range(0, 5).Select(x => VisitFactory.GenerateActiveVisit(100m + x, 2, VisitStateEnum.Active, guarantor: guarantor)).ToList(), statement);

            // create finance plan service
            FinancePlanServiceMockBuilder builder = new FinancePlanServiceMockBuilder().Setup(this.SetupMockMethodsForFinancePlanService);
            builder.FinancePlanRepositoryMock.Setup(x => x.GetAllActiveFinancePlans(It.IsAny<int>())).Returns(() => new List<FinancePlan>());
            builder.FinancePlanRepositoryMock.Setup(x => x.HasFinancePlans(It.IsAny<int>(), It.IsAny<IList<FinancePlanStatusEnum>>())).Returns(() => hasPastDuePlans);
            IFinancePlanService svc = builder.CreateService();

            // create finance plan (pending acceptance)
            decimal monthlyPaymentAmount = 100m;

            FinancePlanCalculationParameters financePlanCalculationParameters = new FinancePlanCalculationParameters(
                guarantor, 
                statement, 
                monthlyPaymentAmount, 
                1, 
                OfferCalculationStrategyEnum.MonthlyPayment, 
                InterestCalculationMethodEnum.Monthly,
                statement.PaymentDueDate)
            {
                Visits = this.GetFinancePlanCalculatedVisitTotal(statement),
                CombineFinancePlans = isCombined
            };

            FinancePlan financePlan = svc.CreateFinancePlan(financePlanCalculationParameters, 0, null, null);

            // update finance plan as guarantor
            financePlan = svc.UpdateFinancePlanWithOffer(financePlan, null, statement, 1, null);

            Action hasAmountDue = () =>
            {
                Assert.AreEqual(monthlyPaymentAmount, financePlan.AmountDue);
            };

            Action doesNotHaveAmountDue = () =>
            {
                Assert.AreEqual(0, financePlan.AmountDue);
            };

            // grace period, amount due
            if (isGracePeriod)
            {
                hasAmountDue();
                return;
            }

            // awaiting statement, combined, with past due plans, amount due
            if (isCombined && hasPastDuePlans)
            {
                hasAmountDue();
                return;
            }

            doesNotHaveAmountDue();
        }

        [Test]
        public void GetInterestDueForVisits_Aggregates()
        {
            IList<FinancePlanVisitInterestDueResult> results = new List<FinancePlanVisitInterestDueResult>();
            results.Add(new FinancePlanVisitInterestDueResult { InterestDue = 1m, VisitId = 1 });
            results.Add(new FinancePlanVisitInterestDueResult { InterestDue = 2m, VisitId = 1 });
            results.Add(new FinancePlanVisitInterestDueResult { InterestDue = 3m, VisitId = 1 });
            results.Add(new FinancePlanVisitInterestDueResult { InterestDue = 4m, VisitId = 2 });
            results.Add(new FinancePlanVisitInterestDueResult { InterestDue = 5m, VisitId = 2 });
            results.Add(new FinancePlanVisitInterestDueResult { InterestDue = 6m, VisitId = 2 });
            results.Add(new FinancePlanVisitInterestDueResult { InterestDue = 10m, VisitId = 3 });

            IFinancePlanService svc = new FinancePlanServiceMockBuilder().Setup(builder =>
            {
                builder.FinancePlanRepositoryMock.Setup(x => x.GetFinancePlanVisitInterestDues(It.IsAny<IList<int>>(), It.IsAny<IList<FinancePlanStatusEnum>>(),null, null)).Returns(() => results);
            }).CreateService();


            IList<FinancePlanVisitInterestDueResult> list = svc.GetInterestDueForVisits(results.Select(x => x.VisitId).ToList());

            Assert.AreEqual(3, list.Count);
            Assert.AreEqual(3, list.Select(x => x.VisitId).Distinct().Count());
            Assert.AreEqual(6m, list.Where(x => x.VisitId == 1).Sum(x => x.InterestDue));
            Assert.AreEqual(15m, list.Where(x => x.VisitId == 2).Sum(x => x.InterestDue));
            Assert.AreEqual(10m, list.Where(x => x.VisitId == 3).Sum(x => x.InterestDue));
        }

        #region UpdateFinancePlanDueToVisitChange

        [Test]
        public void UpdateFinancePlanDueToVisitChange_DoesNotHardRemoveTwice()
        {
            const decimal balance = 100m;

            Mock<Visit> visit = new Mock<Visit>();
            visit.Setup(x => x.BillingSystem).Returns(new BillingSystem { BillingSystemId = 1 });
            visit.Setup(x => x.VpEligible).Returns(false);
            visit.Setup(x => x.CurrentVisitState).Returns(VisitStateEnum.Inactive);
            visit.Setup(x => x.CurrentBalance).Returns(balance);

            DateTime hardRemoveDate = DateTime.UtcNow.AddSeconds(-2);

            FinancePlanVisit fpv = new FinancePlanVisit 
            {
                CurrentVisitBalance = balance,
                UnclearedPaymentsSum = 0m, 
                HardRemoveDate = hardRemoveDate, // not already soft removed
                FinancePlanVisitPrincipalAmounts = new List<FinancePlanVisitPrincipalAmount>
                {
                    new FinancePlanVisitPrincipalAmount
                    {
                        Amount = balance
                    }
                }
            };
            
            FinancePlanServiceMockBuilder builder = this.SetupUpdateFinancePlanDueToVisitChangeTest(fpv, new Guarantor());
            IFinancePlanService financePlanService = builder.CreateService();
            
            financePlanService.UpdateFinancePlanDueToVisitChange(visit.Object);
            Assert.AreEqual(hardRemoveDate, fpv.HardRemoveDate);
            
            builder.BusMock.Verify(x => x.PublishMessage(It.IsAny<OutboundVisitMessage>()), Times.Never);
            builder.BusMock.Verify(x => x.PublishMessage(It.IsAny<SendFinancePlanVisitNotEligibleEmailMessage>()), Times.Never);
            builder.BusMock.Verify(x => x.PublishMessage(It.IsAny<FinancePlanRefundInterestOnRecall>()), Times.Never);
        }

        /// <summary>
        /// VP-1689
        /// full interest refund on billing hold/ineligible
        ///
        /// VP-4825
        /// it should not do a full interest refund when the above happens AND $0, at the same time
        /// </summary>
        [TestCase(false, false, 0, false)]  
        [TestCase(false, false, 100, true)]
        [TestCase(false, true, 0, false)]
        [TestCase(false, true, 100, true)]
        public void UpdateFinancePlanDueToVisitChange_HardRemoves_RefundsInterest(bool vpEligible, bool billingHold, decimal balance, bool expectInterestRefund)
        {
            Guarantor vpGuarantor = new Guarantor();

            Mock<Visit> visit = new Mock<Visit>();
            visit.Setup(x => x.BillingSystem).Returns(new BillingSystem { BillingSystemId = 1 });
            visit.Setup(x => x.VpEligible).Returns(false);
            visit.Setup(x => x.CurrentVisitState).Returns(VisitStateEnum.Inactive);
            visit.Setup(x => x.BillingHold).Returns(billingHold);
            visit.Setup(x => x.VpEligible).Returns(vpEligible);
            visit.Setup(x => x.CurrentBalance).Returns(balance);
            visit.Setup(x => x.VPGuarantor).Returns(vpGuarantor);

            FinancePlanVisit fpv = new FinancePlanVisit 
            {
                CurrentVisitBalance = balance,
                UnclearedPaymentsSum = 0m, 
                HardRemoveDate = null, // not already soft removed
                FinancePlanVisitPrincipalAmounts = new List<FinancePlanVisitPrincipalAmount>
                {
                    new FinancePlanVisitPrincipalAmount
                    {
                        Amount = balance
                    }
                }
            };
            
            FinancePlanServiceMockBuilder builder = this.SetupUpdateFinancePlanDueToVisitChangeTest(fpv,vpGuarantor);
            IFinancePlanService financePlanService = builder.CreateService();
            
            financePlanService.UpdateFinancePlanDueToVisitChange(visit.Object);
            Assert.NotNull(fpv.HardRemoveDate);

            Times times = expectInterestRefund ? Times.Once() : Times.Never();
            builder.BusMock.Verify(x => x.PublishMessage(It.IsAny<FinancePlanRefundInterestOnRecall>()), times);
        }

        private FinancePlanServiceMockBuilder SetupUpdateFinancePlanDueToVisitChangeTest(FinancePlanVisit fpv, Guarantor vpGuarantor)
        {
            FinancePlan financePlan = new FinancePlan 
            {
                VpGuarantor = vpGuarantor
            };
            financePlan.SetFinancePlanStatus(new FinancePlanStatus {FinancePlanStatusId = (int) FinancePlanStatusEnum.GoodStanding}, string.Empty);
            
            fpv.FinancePlan = financePlan;
            financePlan.FinancePlanVisits.Add(fpv);
            
            FinancePlanServiceMockBuilder builder = new FinancePlanServiceMockBuilder()
                .WithFinancePlanStatuses()
                .Setup(x =>
                {
                    x.SessionMock = new SessionMockBuilder().UseSynchronizations().CreateMock();
                    x.FinancePlanRepositoryMock.Setup(s => s.IsVisitOnAnyStatusFinancePlan(It.IsAny<int>())).Returns(true);
                    x.FinancePlanRepositoryMock.Setup(s => s.GetFinancePlansIncludingClosedFromVisits(It.IsAny<IList<int>>())).Returns(financePlan.ToListOfOne());
                });

            return builder;
        }
        
        [Test]
        public void FinancePlanVisit_HardRemoveDate_is_populated_when_visit_is_inactive()
        {
            decimal paymentAmount = 200m;
            decimal balance = 1000m;
            FinancePlan financePlan = new FinancePlan()
            {
                PaymentAmount = paymentAmount,
                CurrentFinancePlanBalance = 1000,
                FinancePlanStatus = new FinancePlanStatus { FinancePlanStatusId = (int)FinancePlanStatusEnum.GoodStanding},
                VpGuarantor = GuarantorFactory.GenerateOnline(),
                CreatedVpStatement = new VpStatement()
            };
            
            this.AdjustHistoryDates(financePlan);

            Tuple<Visit, IList<VisitTransaction>> visitTuple = new Tuple<Visit, IList<VisitTransaction>>(new Visit(), new List<VisitTransaction>());
            Visit visit = visitTuple.Item1;
            visit.BillingSystem = new BillingSystem { BillingSystemId = 1 };
            visit.VPGuarantor = new Guarantor { VpGuarantorId = 123 };
            visit.SetCurrentBalance(balance);
            ((IEntity<VisitStateEnum>)visit).SetState(VisitStateEnum.Active, "");
            financePlan.FinancePlanVisits.Add(FinancePlanFactory.VisitToFinancePlanVisit(visitTuple, financePlan));

            FinancePlanServiceMockBuilder svc = new FinancePlanServiceMockBuilder().Setup(this.SetupMockMethodsForFinancePlanService);
            svc.FinancePlanRepositoryMock.Setup(x => x.IsVisitOnAnyStatusFinancePlan(It.IsAny<int>())).Returns(true);
            svc.FinancePlanRepositoryMock.Setup(x => x.GetFinancePlansIncludingClosedFromVisits(It.IsAny<IList<int>>())).Returns(financePlan.ToListOfOne());

            IFinancePlanService financePlanService = svc.CreateService();
            financePlanService.UpdateFinancePlanDueToVisitChange(visit, VisitStateEnum.Inactive);

            Assert.AreEqual(FinancePlanStatusEnum.NonPaidInFull, financePlan.CurrentFinancePlanStatus.FinancePlanStatusEnum);
            FinancePlanVisit financePlanVisit = financePlan.FinancePlanVisits.FirstOrDefault();
            Assert.NotNull(financePlanVisit?.HardRemoveDate);
        }

        [Test]
        public void FinancePlanVisit_is_not_added_back_to_Fp_after_visit_when_inactive_to_active()
        {
            decimal paymentAmount = 200m;
            decimal balance = 1000m;
            FinancePlan financePlan = new FinancePlan()
            {
                PaymentAmount = paymentAmount,
                CurrentFinancePlanBalance = 1000,
                FinancePlanStatus = new FinancePlanStatus { FinancePlanStatusId = (int)FinancePlanStatusEnum.GoodStanding },
                VpGuarantor = GuarantorFactory.GenerateOnline(),
                CreatedVpStatement = new VpStatement()
            };

            Tuple<Visit, IList<VisitTransaction>> visitTuple = VisitFactory.GenerateActiveVisit(balance, 1);
            Tuple<Visit, IList<VisitTransaction>> visitTuple2 = VisitFactory.GenerateActiveVisit(0m, 1);

            Visit visit = visitTuple.Item1;
            FinancePlanFactory.VisitToFinancePlanVisit(visitTuple, financePlan);
            FinancePlanFactory.VisitToFinancePlanVisit(visitTuple2, financePlan);

            FinancePlanServiceMockBuilder svc = new FinancePlanServiceMockBuilder().Setup(this.SetupMockMethodsForFinancePlanService);
            svc.FinancePlanRepositoryMock.Setup(x => x.IsVisitOnAnyStatusFinancePlan(It.IsAny<int>())).Returns(true);
            svc.FinancePlanRepositoryMock.Setup(x => x.GetFinancePlansIncludingClosedFromVisits(It.IsAny<IList<int>>())).Returns(financePlan.ToListOfOne());

            Assert.AreEqual(2, financePlan.ActiveFinancePlanVisits.Count);
            IFinancePlanService financePlanService = svc.CreateService();
            financePlanService.UpdateFinancePlanDueToVisitChange(visit, VisitStateEnum.Inactive);
            Assert.AreEqual(1, financePlan.ActiveFinancePlanVisits.Count);

            //Visit is reactivated. The FP should not include it. 
            financePlanService.UpdateFinancePlanDueToVisitChange(visit, VisitStateEnum.Active);
            Assert.AreEqual(1, financePlan.ActiveFinancePlanVisits.Count);
        }

        [TestCase(FinancePlanStatusEnum.Canceled)]
        [TestCase(FinancePlanStatusEnum.PendingAcceptance)]
        [TestCase(FinancePlanStatusEnum.GoodStanding)]
        [TestCase(FinancePlanStatusEnum.PastDue)]
        [TestCase(FinancePlanStatusEnum.PaidInFull)]
        [TestCase(FinancePlanStatusEnum.UncollectableClosed)]
        public void FinancePlanVisit_sends_outbound_removed_when_active(FinancePlanStatusEnum financePlanStatus)
        {
            decimal paymentAmount = 200m;
            decimal balance = 1000m;
            FinancePlan financePlan = new FinancePlan()
            {
                PaymentAmount = paymentAmount,
                CurrentFinancePlanBalance = 1000,
                FinancePlanStatus = new FinancePlanStatus { FinancePlanStatusId = (int)financePlanStatus },
                VpGuarantor = GuarantorFactory.GenerateOnline(),
                CreatedVpStatement = new VpStatement()
            };

            bool isActiveOriginated = financePlan.FinancePlanStatus.IsActiveOriginated();

            Tuple<Visit, IList<VisitTransaction>> visitTuple = VisitFactory.GenerateActiveVisit(balance, 1);
            Tuple<Visit, IList<VisitTransaction>> visitTuple2 = VisitFactory.GenerateActiveVisit(0m, 1);

            Visit visit = visitTuple.Item1;
            FinancePlanFactory.VisitToFinancePlanVisit(visitTuple, financePlan);

            FinancePlanServiceMockBuilder builder = new FinancePlanServiceMockBuilder();
            FinancePlanServiceMockBuilder svc = builder.Setup(this.SetupMockMethodsForFinancePlanService);
            svc.FinancePlanRepositoryMock.Setup(x => x.IsVisitOnAnyStatusFinancePlan(It.IsAny<int>())).Returns(true);
            svc.FinancePlanRepositoryMock.Setup(x => x.GetFinancePlansIncludingClosedFromVisits(It.IsAny<IList<int>>())).Returns(financePlan.ToListOfOne());
            
            IFinancePlanService financePlanService = svc.CreateService();
            financePlanService.UpdateFinancePlanDueToVisitChange(visit, VisitStateEnum.Inactive);

            Times times = isActiveOriginated ? Times.Once() : Times.Never();
            builder.BusMock.Verify(x => x.PublishMessage(It.Is<OutboundVisitMessage>(m => m.VpOutboundVisitMessageType == VpOutboundVisitMessageTypeEnum.VisitRemovedFromFinancePlan && m.VpVisitId == visit.VisitId)), times);
        }

        [TestCase(VisitStateEnum.Active, true, 1000.00, false)]
        [TestCase(VisitStateEnum.Active, true, 0.00, false)]
        [TestCase(VisitStateEnum.Active, false, 1000.00, true)]
        [TestCase(VisitStateEnum.Active, false, 0.00, false)]
        [TestCase(VisitStateEnum.Inactive, false, 1000.00, false)]
        public void FinancePlanVisit_NotEligibleEmailsSends_WhenVisitGoesNotEligible(VisitStateEnum initialState, bool hasBillingHold, decimal visitBalance, bool shouldEmailSend)
        {
            bool didEmailSend = false;

            Guarantor guarantor = new Guarantor();

            FinancePlan financePlan = new FinancePlan
            {
                FinancePlanStatus = new FinancePlanStatus {FinancePlanStatusId = (int) FinancePlanStatusEnum.GoodStanding},
                VpGuarantor = guarantor,
                CreatedVpStatement = new VpStatement()
            };

            Visit visit = new Visit();
            visit.CurrentBalance = visitBalance;
            visit.SetCurrentBalance(visitBalance);
            visit.BillingSystem = new BillingSystem();
            visit.VPGuarantor = guarantor;
            visit.BillingHold = hasBillingHold;

            ((IEntity<VisitStateEnum>) visit).SetState(initialState, "");
            financePlan.FinancePlanVisits.Add(FinancePlanFactory.VisitToFinancePlanVisit(new Tuple<Visit, IList<VisitTransaction>>(visit, new List<VisitTransaction>()), financePlan));
            if (initialState == VisitStateEnum.Inactive)
            {
                financePlan.FinancePlanVisits[0].HardRemoveDate = DateTime.UtcNow.AddSeconds(-1);
            }

            FinancePlanServiceMockBuilder builder = new FinancePlanServiceMockBuilder().Setup(this.SetupMockMethodsForFinancePlanService);
            builder.FinancePlanRepositoryMock.Setup(x => x.IsVisitOnAnyStatusFinancePlan(It.IsAny<int>())).Returns(true);
            builder.FinancePlanRepositoryMock.Setup(x => x.GetFinancePlansIncludingClosedFromVisits(It.IsAny<IList<int>>())).Returns(financePlan.ToListOfOne());
            builder.SessionMock = new SessionMockBuilder().UseSynchronizations().CreateMock();
            builder.BusMock.Setup(x => x.PublishMessage(It.IsAny<SendFinancePlanVisitNotEligibleEmailMessage>())).Returns(() =>
            {
                didEmailSend = true;
                return Task.CompletedTask;
            });

            foreach (VisitStateHistory visitState in visit.VisitStates)
            {
                visitState.DateTime = visitState.DateTime.AddMinutes(-1);
            }

            ((IEntity<VisitStateEnum>)visit).SetState(VisitStateEnum.Inactive, "");

            Assert.AreEqual(VisitStateEnum.Inactive, visit.CurrentVisitState);

            IFinancePlanService svc = builder.CreateService();
            svc.UpdateFinancePlanDueToVisitChange(visit, VisitStateEnum.Inactive);

            Assert.AreEqual(shouldEmailSend, didEmailSend);
        }

        [Ignore("this test needs to be refactored - not close to an actual code path")]
        [Test]
        public void OutboundVisitMessage_Sent_ForRemovedFinancePlanVisit()
        {
            decimal paymentAmount = 200m;
            decimal balance = 1000m;
            decimal interestRate = 0.05m;
            Tuple<Visit, IList<VisitTransaction>> visitTuple = new Tuple<Visit, IList<VisitTransaction>>(new Visit(), new List<VisitTransaction>());
            Visit visit = visitTuple.Item1;
            visit.SetCurrentBalance(balance);
            visit.BillingSystem = new Mock<BillingSystem>().Object;
            ((IEntity<VisitStateEnum>)visit).SetState(VisitStateEnum.Active, "");
            IList<FinancePlan> financePlans = new List<FinancePlan>();
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            visit.VPGuarantor = guarantor;
            Mock<VpStatement> statement = new Mock<VpStatement>();
            statement.Setup(x => x.IsGracePeriod).Returns(true);
            statement.Setup(x => x.PaymentDueDate).Returns(DateTime.UtcNow.ToPaymentDueDateForDay(1));
            statement.Setup(x => x.VpStatementVisits).Returns(() =>
            {
                return visit.ToListOfOne().Select(v => new VpStatementVisit
                {
                    Visit = v
                }).ToList();
            });
            IList<OutboundVisitMessage> outboundVisitMessagesSent = new List<OutboundVisitMessage>();

            // create services
            FinancePlanServiceMockBuilder svc = new FinancePlanServiceMockBuilder().Setup(this.SetupMockMethodsForFinancePlanService);
            svc.FinancePlanRepositoryMock.Setup(x => x.IsVisitOnAnyStatusFinancePlan(It.IsAny<int>())).Returns(true);
            svc.FinancePlanRepositoryMock.Setup(x => x.GetFinancePlansIncludingClosedFromVisits(It.IsAny<IList<int>>())).Returns(financePlans);
            svc.SessionMock = new SessionMockBuilder().UseSynchronizations().CreateMock();
            svc.BusMock.Setup(x => x.PublishMessage(It.IsAny<OutboundVisitMessage>())).Returns<OutboundVisitMessage>(m =>
            {
                outboundVisitMessagesSent.Add(m);
                return Task.CompletedTask;
            });
            IFinancePlanService financePlanService = svc.CreateService();
            IReconfigureFinancePlanApplicationService reconfigService = new ReconfigureFinancePlanApplicationServiceMockBuilder().Setup(builder =>
            {
                builder.ApplicationServiceCommonServiceMockBuilder.ClientServiceMock.Setup(x => x.GetClient()).Returns(() =>
                {
                    Mock<Client> clientMock = new Mock<Client>();
                    clientMock.Setup(x => x.InterestCalculationMethod).Returns(InterestCalculationMethodEnum.Monthly);
                    return clientMock.Object;
                });
                builder.FinancePlanService = financePlanService;
                builder.GuarantorServiceMock.Setup(x => x.GetGuarantor(It.IsAny<int>())).Returns(guarantor);
                builder.VpStatementServiceMock.Setup(x => x.GetMostRecentStatement(It.IsAny<Guarantor>())).Returns(statement.Object);
            }).CreateService();


            // original plan
            FinancePlan financePlan = new FinancePlan()
            {
                PaymentAmount = paymentAmount,
                CurrentFinancePlanBalance = 1000,
                FinancePlanStatus = new FinancePlanStatus {FinancePlanStatusId = (int) FinancePlanStatusEnum.GoodStanding},
                VpGuarantor = GuarantorFactory.GenerateOnline(),
                CreatedVpStatement = new VpStatement()
            };
            financePlan.FinancePlanVisits.Add(FinancePlanFactory.VisitToFinancePlanVisit(visitTuple, financePlan));

            // reconfig plan
            FinancePlanCalculationParametersDto fpParametersDto = FinancePlanCalculationParametersDto.CreateForMonthlyPayment(guarantor.VpGuarantorId, statement.Object.VpStatementId, paymentAmount, null, false);
            FinancePlanCalculationParameters fpParameters = ((ReconfigureFinancePlanApplicationService)reconfigService).MapFinancePlanCalculationParameters(
                fpParametersDto,
                financePlan,
                guarantor,
                statement.Object);
            FinancePlan reconfigFp = new FinancePlan
            {
                CreatedVpStatement = financePlan.CreatedVpStatement,
                OriginationDate = DateTime.UtcNow,
                OriginalFinancePlan = financePlan,
                PaymentAmount = paymentAmount * 1.1m,
            };
            reconfigFp.OriginalFinancePlan = financePlan;
            reconfigFp.AddFinancePlanVisits(fpParameters, financePlan);
            reconfigFp.FinancePlanInterestRateHistory.Add(new FinancePlanInterestRateHistory { InterestRate = interestRate });
 
            financePlan.ReconfiguredFinancePlan = reconfigFp;
            //update repository mock
            financePlans.Add(financePlan);
            financePlans.Add(reconfigFp);

            //cancel the old fp (for reconfig)
            financePlanService.CancelFinancePlan(financePlan, string.Empty);
            
            //test this thing
            financePlanService.UpdateFinancePlanDueToVisitChange(visit, VisitStateEnum.Inactive);

            //should just send one closed message
            Assert.AreEqual(1, outboundVisitMessagesSent.Count(x => x.VpOutboundVisitMessageType == VpOutboundVisitMessageTypeEnum.VisitRemovedFromFinancePlan));
        }

        #endregion

        #region CheckIfPendingFinancePlanIsValid

        [TestCase(FinancePlanStatusEnum.PendingAcceptance, true)]
        [TestCase(FinancePlanStatusEnum.GoodStanding, false)] // nothing should happen
        public void CheckIfPendingFinancePlanIsValid_PaymentAmount_GreaterThanBalance_Cancels(FinancePlanStatusEnum financePlanStatusEnum, bool isCanceled)
        {
            FinancePlanServiceMockBuilder builder = new FinancePlanServiceMockBuilder();
            FinancePlan financePlan = this.RunCheckIfPendingFinancePlanIsValid(builder, financePlanStatusEnum, 100m, 100m, 200m);
            Assert.AreEqual(isCanceled, financePlan.IsCanceled());
        }

        [TestCase(FinancePlanStatusEnum.PendingAcceptance, true)]
        [TestCase(FinancePlanStatusEnum.GoodStanding, false)] // nothing should happen
        public void CheckIfPendingFinancePlanIsValid_HasPositiveVisitBalanceDifference_Cancels(FinancePlanStatusEnum financePlanStatusEnum, bool isCanceled)
        {
            FinancePlanServiceMockBuilder builder = new FinancePlanServiceMockBuilder();
            FinancePlan financePlan = this.RunCheckIfPendingFinancePlanIsValid(builder, financePlanStatusEnum, 101m, 100m);
            Assert.AreEqual(isCanceled, financePlan.IsCanceled());
        }

        [TestCase(FinancePlanStatusEnum.PendingAcceptance, false, true)]
        [TestCase(FinancePlanStatusEnum.GoodStanding, false, false)] // nothing should happen
        public void CheckIfPendingFinancePlanIsValid_HasNegativeVisitBalanceDifference_Updates(FinancePlanStatusEnum financePlanStatusEnum, bool isCanceled, bool updatesOffer)
        {
            FinancePlanServiceMockBuilder builder = new FinancePlanServiceMockBuilder();
            FinancePlan financePlan = this.RunCheckIfPendingFinancePlanIsValid(builder, financePlanStatusEnum, 90m, 100m);

            if (updatesOffer)
            {
                // this is probably pretty brittle
                // it's the first method call to something outside FinancePlanService, but it's 4-5 methods deep
                // easier than mocking half the world right now
                builder.FinancePlanOfferServiceMock.Verify(x => x.GenerateOffersForUser(
                    It.IsAny<FinancePlanCalculationParameters>(),
                    It.IsAny<IList<FinancePlan>>(),
                    It.IsAny<bool>(),
                    It.IsAny<bool>(),
                    It.IsAny<FirstInterestPeriodEnum>()
                ), Times.AtLeastOnce());
            }

            Assert.AreEqual(isCanceled, financePlan.IsCanceled());
        }

        private FinancePlan RunCheckIfPendingFinancePlanIsValid(FinancePlanServiceMockBuilder builder, FinancePlanStatusEnum financePlanStatusEnum, decimal visitBalance, decimal financePlanVisitBalance, decimal paymentAmount = 25m)
        {
            builder.FinancePlanRepositoryMock.Setup(x => x.GetFinancePlanStatuses()).Returns(() => Enum.GetValues(typeof(FinancePlanStatusEnum)).Cast<FinancePlanStatusEnum>().Select(x => new FinancePlanStatus { FinancePlanStatusId = (int)x}).ToList());
            builder.FinancePlanRepositoryMock.Setup(x => x.GetAllOpenFinancePlans(It.IsAny<IList<int>>())).Returns(() => new List<FinancePlan>());
            builder.FinancePlanOfferServiceMock.Setup(x => x.GenerateOffersForUser(
                It.IsAny<FinancePlanCalculationParameters>(),
                It.IsAny<IList<FinancePlan>>(),
                It.IsAny<bool>(),
                It.IsAny<bool>(),
                It.IsAny<FirstInterestPeriodEnum>())).Returns(() => new List<FinancePlanOffer>
            {
                new FinancePlanOffer {MinPayment = 0m, MaxPayment = decimal.MaxValue}
            });
            IFinancePlanService svc = builder.CreateService();

            FinancePlan financePlan = new FinancePlan
            {
                VpGuarantor = new Guarantor {VpGuarantorId = 1},
                CreatedVpStatement = new VpStatement {VpStatementId = 1}
            };
            financePlan.SetFinancePlanStatus(new FinancePlanStatus {FinancePlanStatusId = (int) financePlanStatusEnum}, string.Empty);
            financePlan.FinancePlanStatusHistory[0].InsertDate = DateTime.UtcNow.AddMinutes(-5);
            financePlan.FinancePlanVisits.Add(new FinancePlanVisit
            {
                CurrentVisitBalance = visitBalance,
                FinancePlan = financePlan,
                FinancePlanVisitPrincipalAmounts = new List<FinancePlanVisitPrincipalAmount>
                {
                    new FinancePlanVisitPrincipalAmount
                    {
                        Amount = financePlanVisitBalance,
                    }
                }
            });
            financePlan.PaymentAmount = paymentAmount;
            
            svc.CheckIfPendingFinancePlanIsValid(financePlan.ToListOfOne());

            return financePlan;
        }

        #endregion

        [TestCase(FinancePlanStatusEnum.PendingAcceptance, 990)]
        [TestCase(FinancePlanStatusEnum.GoodStanding, 1000)] // nothing should happen
        public void UpdatePendingFinancePlanPrincipalAmount_IncludesAllocationsWithoutFinancePlanAllocations(FinancePlanStatusEnum financePlanStatusEnum, decimal expectedBalance)
        {
            const decimal paymentAmount = 10m;

            FinancePlan financePlan = new FinancePlan
            {
                VpGuarantor = new Guarantor {VpGuarantorId = 1},
                CreatedVpStatement = new VpStatement {VpStatementId = 1}
            };
            financePlan.SetFinancePlanStatus(new FinancePlanStatus {FinancePlanStatusId = (int) financePlanStatusEnum}, string.Empty);
            financePlan.FinancePlanStatusHistory[0].InsertDate = DateTime.UtcNow.AddMinutes(-5);
            financePlan.FinancePlanVisits.Add(new FinancePlanVisit
            {
                VisitId = 1,
                FinancePlan = financePlan,
                FinancePlanVisitPrincipalAmounts = new List<FinancePlanVisitPrincipalAmount>
                {
                    new FinancePlanVisitPrincipalAmount
                    {
                        Amount = 1000m,
                    }
                }
            });

            IList<PaymentAllocation> paymentAllocations = new List<PaymentAllocation>
            {
                new PaymentAllocation
                {
                    ActualAmount = paymentAmount,
                    PaymentAllocationType = PaymentAllocationTypeEnum.VisitPrincipal,
                    PaymentVisit = new PaymentVisit
                    {
                        VisitId = 1
                    }
                },
                // this one should be ignored
                new PaymentAllocation
                {
                    ActualAmount = paymentAmount,
                    FinancePlanId = 1234,
                    PaymentAllocationType = PaymentAllocationTypeEnum.VisitPrincipal,
                    PaymentVisit = new PaymentVisit
                    {
                        VisitId = 1
                    }
                }
            };

            FinancePlanServiceMockBuilder builder = new FinancePlanServiceMockBuilder();
            builder.FinancePlanRepositoryMock.Setup(x => x.GetAllOpenFinancePlans(It.IsAny<IList<int>>())).Returns(() => new List<FinancePlan>());
            builder.FinancePlanRepositoryMock.Setup(x => x.GetFinancePlanStatuses()).Returns(() => Enum.GetValues(typeof(FinancePlanStatusEnum)).Cast<FinancePlanStatusEnum>().Select(x => new FinancePlanStatus {FinancePlanStatusId = (int) x}).ToList());
            builder.FinancePlanRepositoryMock.Setup(x => x.GetFinancePlansIncludingClosedFromVisits(It.IsAny<IList<int>>())).Returns(() => financePlan.ToListOfOne());
            builder.FinancePlanOfferServiceMock.Setup(x => x.GenerateOffersForUser(
                It.IsAny<FinancePlanCalculationParameters>(),
                It.IsAny<IList<FinancePlan>>(),
                It.IsAny<bool>(),
                It.IsAny<bool>(),
                It.IsAny<FirstInterestPeriodEnum>())).Returns(() => new List<FinancePlanOffer>
            {
                new FinancePlanOffer {MinPayment = 0m, MaxPayment = decimal.MaxValue}
            });
            IFinancePlanService svc = builder.CreateService();

            svc.UpdatePendingFinancePlanPrincipalAmount(paymentAllocations);

            Assert.AreEqual(expectedBalance, financePlan.FinancePlanVisits[0].PrincipalBalance);
        }

        [Test]
        public void AddExternalPrincipalAmountDue_Adds_FinancePlanVisitPrincipalAmount()
        {
            FinancePlanServiceMockBuilder builder = new FinancePlanServiceMockBuilder();
            FinancePlan financePlan = this.SharedAddExternalPrincipalAmountDueTestSetup(builder);

            IFinancePlanService financePlanService = builder.CreateService();
            financePlanService.AddExternalPrincipalAmountDue(-100, 1, 2);

            FinancePlanVisit financePlanVisit = financePlan.FinancePlanVisits.First();
            Assert.True(financePlanVisit.FinancePlanVisitPrincipalAmounts.Count == 2);
            FinancePlanVisitPrincipalAmount principalAmountFromExternalPayment = financePlanVisit.FinancePlanVisitPrincipalAmounts.Where(x => x.Amount == -100)
                                  .FirstOrDefault(x => x.FinancePlanVisitPrincipalAmountType == FinancePlanVisitPrincipalAmountTypeEnum.Payment);
            Assert.IsNotNull(principalAmountFromExternalPayment);
        }

        [Test]
        public void AddExternalPrincipalAmountDue_Does_Not_Add_FinancePlanVisitPrincipalAmount_With_Positive_Amount()
        {
            FinancePlanServiceMockBuilder builder = new FinancePlanServiceMockBuilder();
            FinancePlan financePlan = this.SharedAddExternalPrincipalAmountDueTestSetup(builder);

            IFinancePlanService financePlanService = builder.CreateService();
            financePlanService.AddExternalPrincipalAmountDue(100, 1, 2);

            FinancePlanVisit financePlanVisit = financePlan.FinancePlanVisits.First();
            Assert.True(financePlanVisit.FinancePlanVisitPrincipalAmounts.Count == 1);
            FinancePlanVisitPrincipalAmount principalAmountFromExternalPayment = financePlanVisit.FinancePlanVisitPrincipalAmounts.Where(x => x.Amount == -100)
                                  .FirstOrDefault(x => x.FinancePlanVisitPrincipalAmountType == FinancePlanVisitPrincipalAmountTypeEnum.Payment);
            Assert.Null(principalAmountFromExternalPayment);
        }

        [TestCase(1, 1)]
        [TestCase(2, 1)]
        [TestCase(3, 1)]
        [TestCase(1, 2)]
        [TestCase(2, 2)]
        [TestCase(3, 2)]
        [TestCase(1, 3)]
        [TestCase(2, 3)]
        [TestCase(3, 3)]
        public void FinancePlanScheduledPayments(int numberOfPayments, int numberOfFinancePlans)
        {
            const decimal monthlyPaymentAmount = 1000m;

            decimal expectedAmountDue = monthlyPaymentAmount * numberOfPayments * numberOfFinancePlans;
            DateTime expectedDate = DateTime.UtcNow.AddDays(numberOfPayments).Date;

            Mock<Guarantor> guarantor = new Mock<Guarantor>();
            guarantor.Setup(x => x.VpGuarantorId).Returns(8);
            guarantor.Setup(x => x.IsManaged()).Returns(false);

            IList<FinancePlan> financePlans = new List<FinancePlan>();
            for (int f = 0; f < numberOfFinancePlans; f++)
            {
                FinancePlan financePlan = new FinancePlan();
                for (int i = 1; i <= numberOfPayments; i++)
                {
                    financePlan.FinancePlanAmountsDue.Add(new FinancePlanAmountDue
                    {
                        AmountDue = monthlyPaymentAmount,
                        DueDate = DateTime.UtcNow.AddDays(i).Date
                    });
                }

                financePlans.Add(financePlan);
            }
            
            FinancePlanServiceMockBuilder builder = new FinancePlanServiceMockBuilder();
            builder.FinancePlanRepositoryMock.Setup(x => x.GetAllOpenFinancePlans(It.IsAny<IList<int>>())).Returns(() => financePlans);
            builder.ApplicationSettingsMock.Setup(x => x.Application).Returns(() => new ApplicationSetting<ApplicationEnum>(() => new Lazy<ApplicationEnum>(() => ApplicationEnum.VisitPay)));

            IFinancePlanService svc = builder.CreateService();
            IReadOnlyList<ScheduledPaymentResult> list = svc.GetFinancePlanScheduledPayments(guarantor.Object);
            Assert.AreEqual(1, list.Count);

            ScheduledPaymentResult result = list.First();
            Assert.AreEqual(expectedAmountDue, result.ScheduledPaymentAmount);
            Assert.AreEqual(expectedDate, result.ScheduledPaymentDate);
            Assert.AreEqual(guarantor.Object.VpGuarantorId, result.MadeForVpGuarantorId);
            Assert.AreEqual(guarantor.Object.VpGuarantorId, result.MadeByVpGuarantorId);
        }

        [TestCase(true)]
        [TestCase(false)]
        public void RequireRetailInstallmentContract_RespectsFeatureBinding(bool isFeatureEnabled)
        {
            FinancePlanServiceMockBuilder builder = new FinancePlanServiceMockBuilder();
            builder.ClientServiceMock.Setup(x => x.GetClient()).Returns(() =>
            {
                Mock<Client> client = new Mock<Client>();
                client.Setup(x => x.SimpleTermsMaximumMonths).Returns(4);
                return client.Object;
            });
            builder.FeatureServiceMock.Setup(x => x.IsFeatureEnabled(VisitPayFeatureEnum.FeatureSimpleTermsIsEnabled)).Returns(isFeatureEnabled);
            IFinancePlanService svc = builder.CreateService();

            FinancePlanOffer offer = new FinancePlanOffer
            {
                InterestRate = 0.0m
            };

            if (isFeatureEnabled)
            {
                Assert.False(svc.RequireRetailInstallmentContract(offer, 3));
            }
            else
            {
                Assert.True(svc.RequireRetailInstallmentContract(offer, 3));
            }
        }

        [Test]
        public void RequireRetailInstallmentContract_WhenSettingIsGreaterThanLegal_DoesNotAllow()
        {
            int incorrectSetting = 99;

            FinancePlanServiceMockBuilder builder = new FinancePlanServiceMockBuilder();
            builder.ClientServiceMock.Setup(x => x.GetClient()).Returns(() =>
            {
                Mock<Client> client = new Mock<Client>();
                client.Setup(x => x.SimpleTermsMaximumMonths).Returns(incorrectSetting);
                return client.Object;
            });
            builder.FeatureServiceMock.Setup(x => x.IsFeatureEnabled(VisitPayFeatureEnum.FeatureSimpleTermsIsEnabled)).Returns(true);
            IFinancePlanService svc = builder.CreateService();

            FinancePlanOffer offer = new FinancePlanOffer
            {
                InterestRate = 0.0m
            };

            // the service corrects "99" to "4"
            // 5 is greater than 4.
            Assert.True(svc.RequireRetailInstallmentContract(offer, 5));

            Assert.False(svc.RequireRetailInstallmentContract(offer, 4));
        }

        [TestCase(3, 0, false)]
        [TestCase(4, 0, false)]
        [TestCase(4, 0.05, true)]
        [TestCase(5, 0, true)]
        public void RequireRetailInstallmentContract_IsRequired(int numberMonthlyPayments, decimal interestRate, bool expectedResult)
        {
            FinancePlanServiceMockBuilder builder = new FinancePlanServiceMockBuilder();
            builder.ClientServiceMock.Setup(x => x.GetClient()).Returns(() =>
            {
                Mock<Client> client = new Mock<Client>();
                client.Setup(x => x.SimpleTermsMaximumMonths).Returns(4);
                return client.Object;
            });
            builder.FeatureServiceMock.Setup(x => x.IsFeatureEnabled(VisitPayFeatureEnum.FeatureSimpleTermsIsEnabled)).Returns(true);
            IFinancePlanService svc = builder.CreateService();

            FinancePlanOffer offer = new FinancePlanOffer
            {
                InterestRate = interestRate
            };

            bool requireRetailInstallmentContract = svc.RequireRetailInstallmentContract(offer, numberMonthlyPayments);
            Assert.AreEqual(expectedResult, requireRetailInstallmentContract);
        }

        private FinancePlan SharedAddExternalPrincipalAmountDueTestSetup(FinancePlanServiceMockBuilder builder)
        {
            FinancePlan financePlan = new FinancePlan
            {
                VpGuarantor = new Guarantor { VpGuarantorId = 1 },
                CreatedVpStatement = new VpStatement { VpStatementId = 1 }
            };
            financePlan.SetFinancePlanStatus(new FinancePlanStatus { FinancePlanStatusId = (int)FinancePlanStatusEnum.GoodStanding }, string.Empty);
            financePlan.FinancePlanStatusHistory[0].InsertDate = DateTime.UtcNow.AddMinutes(-5);
            financePlan.FinancePlanVisits.Add(new FinancePlanVisit
            {
                VisitId = 1,
                FinancePlan = financePlan,
                FinancePlanVisitPrincipalAmounts = new List<FinancePlanVisitPrincipalAmount>
                {
                    new FinancePlanVisitPrincipalAmount
                    {
                        Amount = 1000m,
                        FinancePlanVisitPrincipalAmountType = FinancePlanVisitPrincipalAmountTypeEnum.FinancePlanCreated
                    }
                },
                FinancePlanVisitVisit = new FinancePlanVisitVisit { VisitId = 1 }
            });

            builder.FinancePlanRepositoryMock.Setup(x => x.AreVisitsOnActiveOrPendingFinancePlan(It.IsAny<IList<Visit>>(), It.IsAny<DateTime?>())).Returns(new List<Visit> { new Visit { VisitId = 1 } });
            builder.FinancePlanRepositoryMock.Setup(x => x.GetFinancePlanWithVisit(It.IsAny<Visit>())).Returns(financePlan);

            return financePlan;
        }

        private static FinancePlanCreditAgreementCollectionDto CreateCreditAgreement()
        {
            return new FinancePlanCreditAgreementCollectionDto
            {
                UserLocale = new FinancePlanCreditAgreementDto
                {
                    AgreementText = string.Empty,
                    ContractNumber = Guid.NewGuid(),
                    Locale = "en-US",
                    TermsCmsVersionId = 0
                }
            };
        }
    }
}
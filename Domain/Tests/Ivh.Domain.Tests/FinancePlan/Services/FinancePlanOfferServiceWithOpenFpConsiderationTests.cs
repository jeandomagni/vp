﻿/*namespace Ivh.Domain.FinancePlan.Services
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading.Tasks;
    using AppIntelligence.Interfaces;
    using Base.Interfaces;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.Data;
    using Common.Tests.Builders;
    using Common.Tests.Helpers.Base;
    using Common.Tests.Helpers.FinancePlan;
    using Common.Tests.Helpers.Visit;
    using Common.Tests.Helpers.VpStatement;
    using Ivh.Domain.Guarantor.Entities;
    using FinanceManagement.FinancePlan.Entities;
    using FinanceManagement.FinancePlan.Interfaces;
    using FinanceManagement.FinancePlan.Services;
    using FinanceManagement.Statement.Entities;
    using Logging.Interfaces;
    using Moq;
    using NHibernate;
    using NUnit.Framework;
    using Settings.Entities;
    using Settings.Interfaces;
    using Visit.Entities;

    [TestFixture]
    public class FinancePlanOfferServiceWithOpenFpConsiderationTests : DomainTestBase
    {
        [SetUp]
        public void Setup()
        {
            this._interestRateRepositoryMock = new Mock<IInterestRateRepository>();
            this._interestRateRepositoryMock.Setup(x => x.GetAll()).Returns(GetInterestRates);

            this._minimumTierRepositoryMock = new Mock<IFinancePlanMinimumPaymentTierRepository>();
            this._minimumTierRepositoryMock.Setup(t => t.GetAll()).Returns(GetTiers);

            this._applicationSettingServiceMock = new Mock<IApplicationSettingsService>();
            this._applicationSettingServiceMock.Setup(x => x.EnableFinancialAssistanceInterestRates).Returns(() => new ApplicationSetting<bool>(() => new Lazy<bool>(() => false)));

            this._financePlanOfferSetTypeRepositoryMock = new Mock<IFinancePlanOfferSetTypeRepository>();
            this._financePlanOfferSetTypeRepositoryMock.Setup(x => x.GetAll()).Returns(() => new List<FinancePlanOfferSetType>
            {
                new FinancePlanOfferSetType {FinancePlanOfferSetTypeId = 1, IsDefault = true, IsActive = true, IsClient = true, IsPatient = true}
            }.AsQueryable());

            this._featureServiceMock = new Mock<IFeatureService>();

            Mock<ITransaction> moqTransaction = new Mock<ITransaction>();
            moqTransaction.Setup(t => t.IsActive).Returns(() => true);

            Mock<ISession> moqSession = new Mock<ISession>();
            moqSession.Setup(t => t.Transaction).Returns(() => moqTransaction.Object);
            moqSession.Setup(t => t.BeginTransaction(Ivh.Common.Data.Constants.IsolationLevel.Default)).Returns(() => moqTransaction.Object);

            this._financePlanOfferRepository = new MockFinancePlanOfferRepository(moqSession.Object);
            this.SetCalculateCombinedFinancePlanMinimumIsEnabled(true);

            this._domainServiceCommonService = DomainServiceCommonServices.CreateDomainServiceCommonService(
                applicationSettingsServiceMock: this._applicationSettingServiceMock,
                featureServiceMock: this._featureServiceMock,
                clientServiceMock: null,
                loggingServiceMock: null,
                busMock: null,
                cacheMock: null,
                timeZoneHelperMock: null);
        }

        #region helpers

        private IDomainServiceCommonService _domainServiceCommonService;
        private Mock<IInterestRateRepository> _interestRateRepositoryMock;
        private Mock<IApplicationSettingsService> _applicationSettingServiceMock;
        private Mock<IFinancePlanMinimumPaymentTierRepository> _minimumTierRepositoryMock;
        private MockFinancePlanOfferRepository _financePlanOfferRepository;
        private Mock<IFinancePlanOfferSetTypeRepository> _financePlanOfferSetTypeRepositoryMock;
        private Mock<IFeatureService> _featureServiceMock;
        public Mock<IRandomizedTestService> _randomizedTestService;
        //public Mock<IAmortizationService> _amortizationtService;

        private async Task RunTest(decimal totalAmount, decimal openFinancePlanBalance, int expectedOffers = 3)
        {
            IList<FinancePlan> financePlan = CreateFinancePlans(openFinancePlanBalance);
            IList<IFinancePlanVisitBalance> applicableVisits = financePlan.SelectMany(x => x.ActiveFinancePlanVisits).Cast<IFinancePlanVisitBalance>().ToList();
            
            FinancePlanCalculationParameters parameters = new FinancePlanCalculationParameters(new Guarantor(), new VpStatement(), 0, null, OfferCalculationStrategyEnum.MonthlyPayment)
            {
                FpTotal = new FinancePlanCalculatedVisitTotal
                {
                    StatementedVisits = applicableVisits
                }
            };

            bool hasFinancialAssistance = applicableVisits.Any(x => x.HasFinancialAssistance);


            IList<FinancePlanOffer> offers = await this.CreatePlanOfferService().GenerateOffersForUserAsync(
                parameters,
                financePlan,
                hasFinancialAssistance,
                true,
                false,
                FinancePlan.FirstInterestPeriod(false, false));

            Assert.AreEqual(expectedOffers, offers.Count);
            Assert.AreEqual(true, offers.Select(x => x.InterestRate).Distinct().Count() == expectedOffers);
            Assert.AreEqual(true, offers.Select(x => x.DurationRangeStart).Distinct().Count() == expectedOffers);
            Assert.AreEqual(true, offers.Select(x => x.DurationRangeEnd).Distinct().Count() == expectedOffers);

            foreach (FinancePlanOffer offer in offers)
            {
                Trace.WriteLine($"{(totalAmount + openFinancePlanBalance):C}: {offer.InterestRate}% ({offer.DurationRangeStart}-{offer.DurationRangeEnd} months)");

            }
            Trace.WriteLine(Environment.NewLine);
        }

        private IFinancePlanOfferService CreatePlanOfferService()
        {
            this._randomizedTestService = new Mock<IRandomizedTestService>();

            IInterestService interestService = new InterestServiceMockBuilder
            {
                FinancePlanOfferSetTypeRepositoryMock = this._financePlanOfferSetTypeRepositoryMock,
                InterestRateConsiderationService = new InterestRateConsiderationFinancePlansBalanceService(new Lazy<IDomainServiceCommonService>(() => this._domainServiceCommonService)),
                InterestRateRepositoryMock = this._interestRateRepositoryMock,
                ApplicationSettingsServiceMock = this._applicationSettingServiceMock
            }.CreateService();

            return new FinancePlanOfferServiceMockBuilder
            {
                InterestService = interestService,
                MinimumPaymentTierRepositoryMock = this._minimumTierRepositoryMock,
                ApplicationSettingsServiceMock = this._applicationSettingServiceMock,
                FinancePlanOfferRepository = this._financePlanOfferRepository,
                AmortizationService = new AmortizationServiceMockBuilder { InterestService = interestService }.CreateService(),
                FeatureServiceMock = this._featureServiceMock
            }.CreateFinancePlanOfferServiceBuilder();

        }

        private static IQueryable<InterestRate> GetInterestRates()
        {
            return new List<InterestRate>
            {
                CreateInterestRate(1, 4, 0, 0m, 150.00m, 500.00m),
                CreateInterestRate(1, 4, 0, 0m, 500.01m, 2250.00m),
                CreateInterestRate(1, 4, 0, 0m, 2250.01m, 5000.00m),
                CreateInterestRate(1, 4, 0, 0m, 5000.01m, 8750.00m),
                CreateInterestRate(1, 4, 0, 0m, 8750.01m, 10000.00m),
                CreateInterestRate(1, 4, 0, 0m, 10000.01m, null),

                CreateInterestRate(5, 12, 0, 0.02m, 500.01m, 2250.00m),
                CreateInterestRate(5, 12, 0, 0.02m, 2250.01m, 5000.00m),
                CreateInterestRate(5, 24, 0, 0.02m, 5000.01m, 8750.00m),
                CreateInterestRate(5, 24, 0, 0.02m, 8750.01m, 10000.00m),
                CreateInterestRate(5, 24, 0, 0.02m, 10000.01m, null),

                CreateInterestRate(5, 28, 0, 0.04m, 150.00m, 500.00m),
                CreateInterestRate(13, 28, 0, 0.04m, 500.01m, 2250.00m),
                CreateInterestRate(13, 30, 0, 0.04m, 2250.01m, 5000.00m),
                CreateInterestRate(13, 36, 0, 0.04m, 5000.01m, 8750.00m),
                CreateInterestRate(25, 40, 0, 0.04m, 8750.01m, 10000.00m),
                CreateInterestRate(25, 59, 0, 0.04m, 10000.01m, null),

                CreateInterestRate(0, 360, 0, 0.00m, 00000.00m, decimal.MaxValue,true)
            }.AsQueryable();
        }

        private static IQueryable<FinancePlanMinimumPaymentTier> GetTiers()
        {
            return new List<FinancePlanMinimumPaymentTier>
            {
                new FinancePlanMinimumPaymentTier
                {
                    ExisitingRecurringPaymentTotal = 0,
                    MinimumPayment = 70m,
                    FinancePlanOfferSetType = new FinancePlanOfferSetType {  FinancePlanOfferSetTypeId = 1, IsActive = true, IsClient = true, IsPatient = true }
                },
                new FinancePlanMinimumPaymentTier
                {
                    ExisitingRecurringPaymentTotal = 70m,
                    MinimumPayment = 30m,
                    FinancePlanOfferSetType = new FinancePlanOfferSetType {  FinancePlanOfferSetTypeId = 1, IsActive = true, IsClient = true, IsPatient = true }
                },
                new FinancePlanMinimumPaymentTier
                {
                    ExisitingRecurringPaymentTotal = 150m,
                    MinimumPayment = 15m,
                    FinancePlanOfferSetType = new FinancePlanOfferSetType {  FinancePlanOfferSetTypeId = 1, IsActive = true, IsClient = true, IsPatient = true }
                }
            }.AsQueryable();
        }

        private static InterestRate CreateInterestRate(int durationRangeStart, int durationRangeEnd, int interestFreePeriod, decimal rate, decimal? minimumBalance, decimal? maximumBalance, bool? isCharity = null)
        {
            return new InterestRate
            {
                DurationRangeStart = durationRangeStart,
                DurationRangeEnd = durationRangeEnd,
                InterestFreePeriod = interestFreePeriod,
                Rate = rate,
                MinimumBalance = minimumBalance,
                MaximumBalance = maximumBalance,
                IsCharity = isCharity,
                FinancePlanOfferSetType = new FinancePlanOfferSetType { FinancePlanOfferSetTypeId = 1, IsActive = true, IsClient = true, IsPatient = true }
            };
        }

        private static IList<FinancePlan> CreateFinancePlans(decimal amount)
        {
            Tuple<Visit, IList<VisitTransaction>> visit = VisitFactory.GenerateActiveVisit(amount, 1, VisitStateEnum.Active);

            FinancePlan financePlan = new FinancePlan
            {
                VpGuarantor = new Guarantor()
            };

            financePlan.FinancePlanVisits.Add(FinancePlanFactory.VisitToFinancePlanVisit(visit, financePlan));

            return financePlan.ToListOfOne();
        }

        internal class MockFinancePlanOfferRepository : RepositoryBase<FinancePlanOffer,VisitPay>, IFinancePlanOfferRepository
        {
            public MockFinancePlanOfferRepository(ISessionContext<VisitPay> sessionContext)
                : base(session)
            {
            }

            public async Task<FinancePlanOffer> InsertAsync(FinancePlanOffer entity)
            {
                return await Task.Run(() => entity).ConfigureAwait(false);
            }

            public IList<FinancePlanOffer> GetOffersWithCorrelationGuid(int vpGuarantorId, Guid guid)
            {
                throw new NotImplementedException();
            }
        }

        private void SetCalculateCombinedFinancePlanMinimumIsEnabled(bool isEnabled)
        {
            this._featureServiceMock.Setup(t => t.IsFeatureEnabled(VisitPayFeatureEnum.CalculateCombinedFinancePlanMinimumIsEnabled)).Returns(() => isEnabled);
        }

        #endregion

        [Test]
        public async Task GenerateOffers_NoOffers()
        {
            IFinancePlanOfferService financePlanOfferService = this.CreatePlanOfferService();

            IList<FinancePlanOffer> result = await financePlanOfferService.GenerateOffersForUserAsync(
                new FinancePlanCalculationParameters(new Guarantor(), new VpStatement(), 0, null, OfferCalculationStrategyEnum.MonthlyPayment),
                new Random().Next(0, 149),
                0,
                new List<FinancePlan>(),
                false,
                true,
                false,
                FinancePlan.FirstInterestPeriod(false, false));

            Assert.AreEqual(0, result.Count);
        }

        [Test]
        public async Task GenerateOffers_Tier1()
        {
            await this.RunTest(150, 0, 2);
            await this.RunTest(500, 0, 2);
            await this.RunTest(new Random().Next(150, 500), 0, 2);
        }

        [Test]
        [Ignore(ObsoleteDescription.VisitTransactions)]
        public async Task GenerateOffers_Tier2()
        {
            await this.RunTest(500.01m, 0);
            await this.RunTest(2250, 0);
            await this.RunTest(new Random().Next(501, 2250), 0);

            await this.RunTest(150, 500);
        }

        [Test]
        public async Task GenerateOffers_Tier3()
        {
            await this.RunTest(2250.01m, 0);
            await this.RunTest(5000, 0);
            await this.RunTest(new Random().Next(2251, 5000), 0);

            await this.RunTest(1500, 1000);
        }

        [Test]
        public async Task GenerateOffers_Tier4()
        {
            await this.RunTest(5000.01m, 0);
            await this.RunTest(8750, 0);
            await this.RunTest(new Random().Next(5001, 8750), 0);

            await this.RunTest(1500, 4000);
        }

        [Test]
        public async Task GenerateOffers_Tier5()
        {
            await this.RunTest(8750.01m, 0);
            await this.RunTest(10000, 0);
            await this.RunTest(new Random().Next(8751, 10000), 0);

            await this.RunTest(5000, 4000);
        }

        [Test]
        public async Task GenerateOffers_Tier6()
        {
            await this.RunTest(10000.01m, 0);
            await this.RunTest(new Random().Next(10001, int.MaxValue), 0);

            await this.RunTest(5000, 6000);
        }

        [Test]
        [Ignore(ObsoleteDescription.VisitTransactions)]
        public async Task GenerateOffersForUserAsync_calculates_minimum_properly_for_scenario1_feature_enabled()
        {
            //  Scenario 1
            //  1 existing plan $100/mo payment with any length remaining duration.
            //  New visit balance available to finance.
            //  Combo calculated min is $100

            IList<FinancePlanOffer> offers = await this.GetOffersScenarioOne();

            decimal calculatedMinimum = offers.Min(x => x.MinPayment);
            Assert.AreEqual(100, calculatedMinimum);
        }

        [Test]
        public async Task GenerateOffersForUserAsync_calculates_minimum_properly_for_scenario1_feature_disabled()
        {
            //  Scenario 1
            //  1 existing plan $100/mo payment with any length remaining duration.
            //  New visit balance available to finance.
            //  Combo calculated min is $100
            this.SetCalculateCombinedFinancePlanMinimumIsEnabled(false);
            IList<FinancePlanOffer> offers = await this.GetOffersScenarioOne();
            decimal calculatedMinimum = offers.Min(x => x.MinPayment);
            Assert.True(calculatedMinimum != 100);
        }

        [Test]
        [Ignore(ObsoleteDescription.VisitTransactions)]
        public async Task GenerateOffersForUserAsync_calculates_minimum_properly_for_scenario2_feature_enabled()
        {
            // Scenario 2			
            //   2 existing plans.
            //   FP1 has $100/mo payment with 6mo remaining.
            //   FP2 has $100/mo payment with 12mo remaining.
            //   New visit balance available to finance.
            //   Combo calculated min is $150 ( ( $100*6 + $100*12 ) / 12).

            IList<FinancePlanOffer> offers = await this.GetOffersScenarioTwo();

            decimal calculatedMinimum = offers.Min(x => x.MinPayment);
            Assert.AreEqual(150, calculatedMinimum);
        }


        [Test]
        [Ignore(ObsoleteDescription.VisitTransactions)]
        public async Task GenerateOffersForUserAsync_for_combined_works_with_boundary_case()
        {
            // A total amount of 2850.3 = 100 min monthly payment with financePlanCalculationParameters.CombineFinancePlans = false; 
            // The minimum tier is 70 which shows when the amount is large enough the configured minimum is ignored in order to pay off the loan in the time alloted
            // We want to make sure that around this boundary financePlanCalculationParameters.CombineFinancePlans = true works as expected. 
            // The expectation is when we go over this amount of 2850.3 the combo fp calculated minimum is ignored and  returns the minimum required to pay off the loan in the alloted time. 

            decimal currentMonthlyPayment = 100M;
            Tuple<Visit, IList<VisitTransaction>> newVisitNotOnFinancePlan = VisitFactory.GenerateActiveVisit(currentMonthlyPayment, 1);
            bool hasFinancialAssistance = this.HasFinancialAssistance(newVisitNotOnFinancePlan.Item1.ToListOfOne());
            FinancePlan fp1 = this.GetFinancePlanForCombinedMinimumPayment(DateTime.UtcNow, 1200, currentMonthlyPayment, 12);

            FinancePlanCalculationParameters financePlanCalculationParameters = new FinancePlanCalculationParameters(new Guarantor(), new VpStatement(), 0, null, OfferCalculationStrategyEnum.MonthlyPayment) {CombineFinancePlans = true};

            IList<FinancePlanOffer> stackedOfferAndCombinedIsTheSame = await this.CreatePlanOfferService().GenerateOffersForUserAsync(
                financePlanCalculationParameters,
                (decimal)2850.3,
                0,
                fp1.ToListOfOne(),
                hasFinancialAssistance,
                true,
                false,
                FinancePlan.FirstInterestPeriod(false, true));

            Assert.AreEqual(currentMonthlyPayment, stackedOfferAndCombinedIsTheSame.Min(x => x.MinPayment));

            //with stacked the min is 99.99 with 2850
            IList<FinancePlanOffer> calculatedMinShouldBeUsed = await this.CreatePlanOfferService().GenerateOffersForUserAsync(
                financePlanCalculationParameters,
                (decimal)2850,
                0,
                fp1.ToListOfOne(),
                hasFinancialAssistance,
                true,
                false,
                FinancePlan.FirstInterestPeriod(false, true));

            Assert.AreEqual(currentMonthlyPayment, calculatedMinShouldBeUsed.Min(x => x.MinPayment));

            // when the boundary is crossed the expectation is the calculated minimum is ignored
            IList<FinancePlanOffer> minimumPaymentShouldBeGreaterThanCalculatedMinimum = await this.CreatePlanOfferService().GenerateOffersForUserAsync(
                financePlanCalculationParameters,
                (decimal)2851,
                0,
                fp1.ToListOfOne(),
                hasFinancialAssistance,
                true,
                false,
                FinancePlan.FirstInterestPeriod(false, true));

            Assert.True(minimumPaymentShouldBeGreaterThanCalculatedMinimum.Min(x => x.MinPayment) > currentMonthlyPayment);

        }

        [Test]
        public async Task GenerateOffersForUserAsync_calculates_minimum_properly_for_scenario2_feature_disabled()
        {
            // Scenario 2			
            //   2 existing plans.
            //   FP1 has $100/mo payment with 6mo remaining.
            //   FP2 has $100/mo payment with 12mo remaining.
            //   New visit balance available to finance.
            //   Combo calculated min is $150 ( ( $100*6 + $100*12 ) / 12).
            this.SetCalculateCombinedFinancePlanMinimumIsEnabled(false);
            IList<FinancePlanOffer> offers = await this.GetOffersScenarioTwo();

            decimal calculatedMinimum = offers.Min(x => x.MinPayment);
            Assert.True(calculatedMinimum != 150);
        }

        private FinancePlan GetFinancePlanForCombinedMinimumPayment(DateTime dateToStartFinancePlan, decimal amountOfVisit, decimal monthlyPayment, int durationInMonths)
        {
            Guarantor guarantor = new Guarantor();

            FinancePlanOffer financePlanOffer = new FinancePlanOffer
            {
                DurationRangeStart = 1,
                DurationRangeEnd = 12,
                InterestFreePeriod = 0,
                InterestRate = 0.00m
            };

            Tuple<Visit, IList<VisitTransaction>> visit1 = VisitFactory.GenerateActiveVisit(amountOfVisit, 1, VisitStateEnum.Active, insertDate: dateToStartFinancePlan);

            VpStatement statement = VpStatementFactory.CreateStatement(visit1.ToListOfOne().ToList());

            FinancePlan financePlan = FinancePlanFactory.CreateFinancePlan(new List<Tuple<Visit, IList<VisitTransaction>>> { visit1 }, 0, monthlyPayment, dateToStartFinancePlan.Date);
            financePlan.CreatedVpStatement = statement;
            financePlan.OriginalDuration = durationInMonths;
            financePlan.FinancePlanOffer = financePlanOffer;
            financePlan.PaymentAmount = monthlyPayment;
            financePlan.VpGuarantor = guarantor;

            return financePlan;
        }

        private async Task<IList<FinancePlanOffer>> GetOffersScenarioOne()
        {
            //  this scenario comes from VPNG-19279 
            //  Scenario 1
            //  1 existing plan $100/mo payment with any length remaining duration.
            //  New visit balance available to finance.
            //  Combo calculated min is $100

            Tuple<Visit, IList<VisitTransaction>> newVisitNotOnFinancePlan = VisitFactory.GenerateActiveVisit(100, 1);
            bool hasFinancialAssistance = this.HasFinancialAssistance(newVisitNotOnFinancePlan.Item1.ToListOfOne());
            FinancePlan fp1 = this.GetFinancePlanForCombinedMinimumPayment(DateTime.UtcNow.AddMonths(-6), 1200, 100, 12);

            FinancePlanCalculationParameters financePlanCalculationParameters = new FinancePlanCalculationParameters(new Guarantor(), new VpStatement(), 0, null, OfferCalculationStrategyEnum.MonthlyPayment);
            financePlanCalculationParameters.CombineFinancePlans = true;

            return await this.CreatePlanOfferService().GenerateOffersForUserAsync(
                financePlanCalculationParameters,
                1300,
                0,
                fp1.ToListOfOne(),
                hasFinancialAssistance,
                true,
                false,
                FinancePlan.FirstInterestPeriod(false, true));
        }

        private async Task<IList<FinancePlanOffer>> GetOffersScenarioTwo()
        {
            // this scenario comes from VPNG-19279 
            // Scenario 2			
            //   2 existing plans.
            //   FP1 has $100/mo payment with 6mo remaining.
            //   FP2 has $100/mo payment with 12mo remaining.
            //   New visit balance available to finance.
            //   Combo calculated min is $150 ( ( $100*6 + $100*12 ) / 12).

            FinancePlan fp1 = this.GetFinancePlanForCombinedMinimumPayment(DateTime.UtcNow.AddMonths(-6), 1200, 100, 12);
            //simulate paying down for 6 months
            for (int month = 6; month > 0; month--)
            {
                FinancePlanVisit fpVisit = fp1.FinancePlanVisits.First();

                fpVisit.FinancePlanAmountDues.Add(new FinancePlanAmountDue
                {
                    FinancePlan = fp1,
                    FinancePlanVisit = fpVisit,
                    InsertDate = DateTime.UtcNow,
                    AmountDue = -100m,
                    PaymentAllocationId = 0
                });

                //VisitTransaction visitTransaction = new VisitTransactionBuilder().AsPrincipalPayment(fp1.FinancePlanVisits.First().Visit, 100, DateTime.UtcNow.AddMonths(month * -1));
               // fp1.FinancePlanVisits.First().Visit.Transactions.Add(visitTransaction);
            }

            FinancePlan fp2 = this.GetFinancePlanForCombinedMinimumPayment(DateTime.UtcNow, 1200, 100, 12);
            List<FinancePlan> allApplicableFinancePlans = new List<FinancePlan> { fp1, fp2 };

            Tuple<Visit, IList<VisitTransaction>> newVisitNotOnFinancePlan = VisitFactory.GenerateActiveVisit(100, 1);
            bool hasFinancialAssistance = this.HasFinancialAssistance(newVisitNotOnFinancePlan.Item1.ToListOfOne());

            FinancePlanCalculationParameters financePlanCalculationParameters = new FinancePlanCalculationParameters(new Guarantor(), new VpStatement(), 0, null, OfferCalculationStrategyEnum.MonthlyPayment);
            financePlanCalculationParameters.CombineFinancePlans = true;

            return await this.CreatePlanOfferService().GenerateOffersForUserAsync(
                financePlanCalculationParameters,
                2500,
                0,
                allApplicableFinancePlans,
                hasFinancialAssistance,
                true,
                false,
                FinancePlan.FirstInterestPeriod(false, true));
        }

        private bool HasFinancialAssistance(IList<Visit> visitsToConsider)
        {
            return (visitsToConsider.IsNotNullOrEmpty() && visitsToConsider.Any(x => x.InterestZero));
        }

    }
}*/
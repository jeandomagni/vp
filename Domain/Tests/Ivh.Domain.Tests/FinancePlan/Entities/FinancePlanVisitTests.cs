﻿namespace Ivh.Domain.FinancePlan.Entities
{
    using System;
    using Common.VisitPay.Enums;
    using FinanceManagement.FinancePlan.Entities;
    using NUnit.Framework;

    [TestFixture]
    public class FinancePlanVisitTests : DomainTestBase
    {
        private FinancePlanVisit _financePlanVisit;

        [SetUp]
        public void Initialize()
        {
            this._financePlanVisit = new FinancePlanVisit { FinancePlan = new FinancePlan() };
        }

        [Test]
        public void AddFinancePlanVisitInterestDue_PaymentAllocationIdNull_AddsFinancePlanVisitInterestDueItem()
        {
            const FinancePlanVisitInterestDueTypeEnum type = FinancePlanVisitInterestDueTypeEnum.InterestAssessment;
            this._financePlanVisit.AddFinancePlanVisitInterestDue(10m, null, type);
            Assert.NotZero(this._financePlanVisit.FinancePlan.FinancePlanVisitInterestDueItems.Count);
        }

        [Test]
        public void AddFinancePlanVisitInterestAssessment_InterestAssessedGreaterThanZero_AddsFinancePlanVisitInterestDueItem()
        {
            this._financePlanVisit.AddFinancePlanVisitInterestAssessment(250m, 3m, DateTime.Today);
            Assert.NotZero(this._financePlanVisit.FinancePlan.FinancePlanVisitInterestDueItems.Count);
        }
    }
}
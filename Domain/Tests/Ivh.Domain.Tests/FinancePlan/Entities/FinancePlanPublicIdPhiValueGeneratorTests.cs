﻿namespace Ivh.Domain.FinancePlan.Entities
{
    using Ivh.Application.Core.Common.Interfaces;
    using Ivh.Domain.FinanceManagement.FinancePlan.Entities;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class FinancePlanPublicIdPhiValueGeneratorTests : DomainTestBase
    {
        [Test]
        public void GenerateRandomIdentifierValue_ValidValueAssigned()
        {
            const long unassignedIdValue = 3456734567;
            Mock<IIdentifierValueSource<long>> valueGeneratorMock = new Mock<IIdentifierValueSource<long>>();

            valueGeneratorMock
                .Setup(g => g.GenerateIdentifier())
                .Returns(unassignedIdValue);

            FinancePlanPublicIdPhiValueGenerator source = new FinancePlanPublicIdPhiValueGenerator();
            FinancePlanPublicIdPhiSource publicIdSource = new FinancePlanPublicIdPhiSource(valueGeneratorMock.Object);

            Assert.AreEqual(unassignedIdValue, publicIdSource.RawValue);
            Assert.IsTrue(FinancePlanPublicIdPhiSource.IsValidFormat(publicIdSource.RawValue.ToString()));
        }
    }
}
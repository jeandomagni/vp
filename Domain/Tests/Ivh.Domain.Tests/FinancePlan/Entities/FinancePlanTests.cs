﻿namespace Ivh.Domain.FinancePlan.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Utilities.Extensions;
    using Common.Tests.Builders;
    using Common.Tests.Helpers.FinancePlan;
    using Common.Tests.Helpers.Guarantor;
    using Common.Tests.Helpers.Payment;
    using Common.Tests.Helpers.Visit;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.HospitalData;
    using FinanceManagement.FinancePlan.Entities;
    using FinanceManagement.Statement.Entities;
    using Guarantor.Entities;
    using NUnit.Framework;
    using Ivh.Domain.FinanceManagement.Payment.Entities;
    using Visit.Entities;

    [TestFixture]
    public class FinancePlanTests : DomainTestBase
    {
        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public void FinancePlan_BucketsBucket0()
        {
            FinancePlan financePlan = CreateFinancePlan(1000, 100);

            int vpStatementId = 1;

            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddDays(1), vpStatementId++);

            Assert.AreEqual(0, financePlan.Buckets.Keys.Max());
        }

        [Test]
        public void FinancePlan_BucketsBucket1()
        {
            FinancePlan financePlan = CreateFinancePlan(1000, 100);

            int vpStatementId = 1;

            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddDays(-1), vpStatementId++);
            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddDays(1), vpStatementId++);

            Assert.AreEqual(1, financePlan.Buckets.Keys.Max());
            Assert.AreEqual(100, financePlan.CurrentAmount);
            Assert.AreEqual(100, financePlan.PastDueAmount);
        }

        [Test]
        public void FinancePlan_BucketsBucket2()
        {
            FinancePlan financePlan = CreateFinancePlan(1000, 100);

            int vpStatementId = 1;

            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddDays(-2), vpStatementId++);
            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddDays(-1), vpStatementId++);
            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddDays(1), vpStatementId++);

            Assert.AreEqual(2, financePlan.Buckets.Keys.Max());
            Assert.AreEqual(100, financePlan.CurrentAmount);
            Assert.AreEqual(200, financePlan.PastDueAmount);
        }

        [Test]
        public void FinancePlan_BucketsBucket3()
        {
            FinancePlan financePlan = CreateFinancePlan(1000, 100);

            int vpStatementId = 1;

            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddDays(-3), vpStatementId++);
            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddDays(-2), vpStatementId++);
            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddDays(-1), vpStatementId++);
            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddDays(1), vpStatementId++);


            Assert.AreEqual(3, financePlan.Buckets.Keys.Max());
            Assert.AreEqual(100, financePlan.CurrentAmount);
            Assert.AreEqual(300, financePlan.PastDueAmount);
        }

        [Test]
        public void FinancePlan_Buckets_Comprehensive()
        {
            //CREATE A Finance Plan with 1000 balance and 100 payments;
            FinancePlan financePlan = CreateFinancePlan(1000, 100);

            int vpStatementId = 1;
            int paymentId = 1;

            //Create an Amount Due for the statement 3 months ago
            financePlan.AddFinancePlanAmountsDueStatement(400, DateTime.UtcNow.AddMonths(-3), vpStatementId++);
            //Create an Amount Due for the statement 2 months ago
            financePlan.AddFinancePlanAmountsDueStatement(300, DateTime.UtcNow.AddMonths(-2), vpStatementId++);
            //Create an Amount Due for the statement 1 months ago
            financePlan.AddFinancePlanAmountsDueStatement(200, DateTime.UtcNow.AddMonths(-1), vpStatementId++);
            //Create an Amount Due for the statement 0 months ago (current)
            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddMonths(0), vpStatementId++);

            //Check to see that there are 4 Bucket(s), $100 current, $900 past due, $100 in Bucket 0, $200 in Bucket 1, $300 in Bucket 2 and $400 in Bucket 3
            AssertCommon(financePlan, 4, 100, 900, 100, 200, 300, 400);

            // reduce amount owed by $100
            Payment payment1 = this.GetPaymentForBucketTests2(paymentId++, financePlan, 100);
            financePlan.OnPayment(-100, DateTime.UtcNow, payment1);
            //Check to see that there are 4 Bucket(s), $100 current, $800 past due, $100 in Bucket 0, $200 in Bucket 1, $300 in Bucket 2 and $300 in Bucket 3
            AssertCommon(financePlan, 4, 100, 800, 100, 200, 300, 300);

            // reduce amount owed by $100
            Payment payment2 = this.GetPaymentForBucketTests2(paymentId++, financePlan, 100);
            financePlan.OnPayment(-100, DateTime.UtcNow, payment2);
            //Check to see that there are 4 Bucket(s), $100 current, $700 past due, $100 in Bucket 0, $200 in Bucket 1, $300 in Bucket 2 and $200 in Bucket 3
            AssertCommon(financePlan, 4, 100, 700, 100, 200, 300, 200);

            // reduce amount owed by $100
            Payment payment3 = this.GetPaymentForBucketTests2(paymentId++, financePlan, 100);
            financePlan.OnPayment(-100, DateTime.UtcNow, payment3);
            //Check to see that there are 4 Bucket(s), $100 current, $600 past due, $100 in Bucket 0, $200 in Bucket 1, $300 in Bucket 2 and $100 in Bucket 3
            AssertCommon(financePlan, 4, 100, 600, 100, 200, 300, 100);

            // reduce amount owed by $100
            Payment payment4 = this.GetPaymentForBucketTests2(paymentId++, financePlan, 100);
            financePlan.OnPayment(-100, DateTime.UtcNow, payment4);
            //Check to see that there are 3 Bucket(s), $100 current, $500 past due, $100 in Bucket 0, $200 in Bucket 1, $300 in Bucket 2
            AssertCommon(financePlan, 3, 100, 500, 100, 200, 300, 0);

            // reduce amount owed by $100
            Payment payment5 = this.GetPaymentForBucketTests2(paymentId++, financePlan, 100);
            financePlan.OnPayment(-100, DateTime.UtcNow, payment5);
            //Check to see that there are 3 Bucket(s), $100 current, $400 past due, $100 in Bucket 0, $200 in Bucket 1, $200 in Bucket 2
            AssertCommon(financePlan, 3, 100, 400, 100, 200, 200, 0);

            // reduce amount owed by $100
            Payment payment6 = this.GetPaymentForBucketTests2(paymentId++, financePlan, 100);
            financePlan.OnPayment(-100, DateTime.UtcNow, payment6);
            //Check to see that there are 3 Bucket(s), $100 current, $300 past due, $100 in Bucket 0, $200 in Bucket 1, $100 in Bucket 2
            AssertCommon(financePlan, 3, 100, 300, 100, 200, 100, 0);

            // reduce amount owed by $100
            Payment payment7 = this.GetPaymentForBucketTests2(paymentId++, financePlan, 100);
            financePlan.OnPayment(-100, DateTime.UtcNow, payment7);
            //Check to see that there are 2 Bucket(s), $100 current, $200 past due, $100 in Bucket 0, $200 in Bucket 1
            AssertCommon(financePlan, 2, 100, 200, 100, 200, 0, 0);

            // reduce amount owed by $100
            Payment payment8 = this.GetPaymentForBucketTests2(paymentId++, financePlan, 100);
            financePlan.OnPayment(-100, DateTime.UtcNow, payment8); ;
            //Check to see that there are 2 Bucket(s), $100 current, $100 past due, $100 in Bucket 0, $100 in Bucket 1
            AssertCommon(financePlan, 2, 100, 100, 100, 100, 0, 0);

            // reduce amount owed by $100
            Payment payment9 = this.GetPaymentForBucketTests2(paymentId++, financePlan, 100);
            financePlan.OnPayment(-100, DateTime.UtcNow, payment9);
            //Check to see that there are 1 Bucket(s), $100 current, $0 past due, $100 in Bucket 0
            AssertCommon(financePlan, 1, 100, 0, 100, 0, 0, 0);

            // reduce amount owed by $100
            Payment payment10 = this.GetPaymentForBucketTests2(paymentId++, financePlan, 100);
            financePlan.OnPayment(-100, DateTime.UtcNow, payment10);
            //Check to see that there is still 1 Bucket(s), $100 current, $0 past due
            //Even after paying the last 100 off it doesnt decrement because that's the current bucket an we can only clear that bucket with a recurring payment.
            AssertCommon(financePlan, 1, 100, 0, 100, 0, 0, 0);
        }

        [Test]
        public void FinancePlan_Buckets_ClearsLastBucket()
        {
            //Should only clear the current or last bucket if it is a recurring payment type.
            //CREATE A Finance Plan with 1000 balance and 100 payments;
            FinancePlan financePlan = CreateFinancePlan(1000, 100);

            int vpStatementId = 1;
            int paymentId = 1;

            //Create an Amount Due for the statement 0 months ago (current)
            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddMonths(0), vpStatementId++);

            //Check to see that there is 1 Bucket(s), $100 current, $0 past due, $100 in Bucket 0, $0 in Bucket 1, $0 in Bucket 2 and $0 in Bucket 3            
            AssertCommon(financePlan, 1, 100, 0, 100, 0, 0, 0);

            // reduce amount owed by $100
            Payment payment = GetPaymentForBucketTests(paymentId, financePlan, 100);
            payment.PaymentType = PaymentTypeEnum.RecurringPaymentFinancePlan;
            financePlan.OnPayment(-100, DateTime.UtcNow, payment);

            AssertCommon(financePlan, 0, 0, 0, 0, 0, 0, 0);
        }


        [Test]
        public void FinancePlan_Buckets_ClearsLastBucket_PromptFp()
        {
            //Should only clear the current or last bucket if it is a recurring payment type.
            //CREATE A Finance Plan with 1000 balance and 100 payments;
            FinancePlan financePlan = CreateFinancePlan(1000, 100);

            int vpStatementId = 1;
            int paymentId = 1;

            //Create an Amount Due for the statement 0 months ago (current)
            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddMonths(0), vpStatementId++);

            //Check to see that there is 1 Bucket(s), $100 current, $0 past due, $100 in Bucket 0, $0 in Bucket 1, $0 in Bucket 2 and $0 in Bucket 3            
            AssertCommon(financePlan, 1, 100, 0, 100, 0, 0, 0);

            // reduce amount owed by $100
            Payment payment = GetPaymentForBucketTests(paymentId, financePlan, 100);
            payment.PaymentType = PaymentTypeEnum.ManualPromptSpecificFinancePlansBucketZero;
            financePlan.OnPayment(-100, DateTime.UtcNow, payment);

            AssertCommon(financePlan, 0, 0, 0, 0, 0, 0, 0);
        }

        [Test]
        public void FinancePlan_has_current_and_past_due_bucket_when_pastdue()
        {
            //When a user recieves statement, then goes past due the user should have 2 buckets
            //one bucket to represent the current amount and one to hold the past due amount

            //CREATE A Finance Plan with 1000 balance and 100 payments;
            FinancePlan financePlan = CreateFinancePlan(1000, 100);

            int vpStatementId = 1;
            int paymentId = 1;

            //Create an Amount Due for the statement 1 month ago (current)
            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddMonths(-1), vpStatementId++);
            //Check to see that there is 2 Bucket(s), $100 current, $100 past due, $100 in Bucket 0, $100 in Bucket 1, $0 in Bucket 2 and $0 in Bucket 3            
            AssertCommon(
                plan: financePlan, 
                bucketCount:2, 
                currentAmount: 0, 
                pastDueAmount: 100, 
                bucket0Balance: 0, 
                bucket1Balance: 100, 
                bucket2Balance: 0, 
                bucket3Balance: 0);

            // reduce amount owed by $100
            // it will let us pay down bucket 0 without a recurring payment type because we are past due
            Payment payment = GetPaymentForBucketTests(paymentId, financePlan, 100);
            financePlan.OnPayment(-100, DateTime.UtcNow, payment);
            AssertCommon(financePlan, 0, 0, 0, 0, 0, 0, 0);
        }

        [Test]
        public void FinancePlan_bucket2_when_2_statements_2_missed_payments_and_awaiting_statement()
        {
            //When a user recieves statement, misses payment, then statemented again, misses payment, then passes current due date
            //user should be in bucket 2 because he is past the second due date

            //CREATE A Finance Plan with 1000 balance and 100 payments;
            FinancePlan financePlan = CreateFinancePlan(1000, 100);

            int vpStatementId = 1;

            //Create an Amount Due for the statement 1 month ago (current)
            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddMonths(-1), vpStatementId++);
            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddDays(-1), vpStatementId++);
            //Check to see that there is 2 Bucket(s), $100 current, $100 past due, $100 in Bucket 0, $100 in Bucket 1, $0 in Bucket 2 and $0 in Bucket 3            
            AssertCommon(
                plan: financePlan,
                bucketCount: 3,
                currentAmount: 0,
                pastDueAmount: 200,
                bucket0Balance: 0,
                bucket1Balance: 100,
                bucket2Balance: 100,
                bucket3Balance: 0);

        }

        [Test]
        public void FinancePlan_has_current_bucket_in_grace_is_not_past_due()
        {
            //A user not previously past due, in grace period should have 1 bucket (bucket 0)

            //CREATE A Finance Plan with 1000 balance and 100 payments;
            FinancePlan financePlan = CreateFinancePlan(1000, 100);

            int vpStatementId = 1;

            //Create an Amount Due for the statement 0 months ago (current)
            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddDays(10), vpStatementId++);
            //initial assert         
            AssertCommon(
                plan: financePlan,
                bucketCount: 1,
                currentAmount: 100,
                pastDueAmount: 0,
                bucket0Balance: 100,
                bucket1Balance: 0,
                bucket2Balance: 0,
                bucket3Balance: 0);

        }

        [Test]
        public void FinancePlan_negate_then_reset_amount_due()
        {
            FinancePlan financePlan = CreateFinancePlan(1000, 100);
            
            // client creates
            financePlan.AddFinancePlanAmountsDueStatement(100m, DateTime.UtcNow.AddMinutes(-1), 0, null, null);

            // guarantor accepts for different amount
            financePlan.AddFinancePlanAmountsDueStatement(-100m, DateTime.UtcNow, 0, null, null);
            financePlan.AddFinancePlanAmountsDueStatement(200m, DateTime.UtcNow, 0, null, null);

            Assert.AreEqual(0, financePlan.CurrentBucket);
        }

        [Test]
        public void FinancePlan_negate_then_reset_amount_due_1()
        {
            FinancePlan financePlan = CreateFinancePlan(1000, 100);
            financePlan.SetFinancePlanStatus(new FinancePlanStatus {FinancePlanStatusId = (int) FinancePlanStatusEnum.PendingAcceptance}, string.Empty);

            VpStatement statement = new VpStatement {VpStatementId = 1, PaymentDueDate = DateTime.UtcNow.AddDays(1)};

            // client creates
            financePlan.PaymentAmount = 100m;
            financePlan.SetTheAmountDueForFinancePlan(statement);
            foreach (FinancePlanPaymentAmountHistory h in financePlan.FinancePlanPaymentAmountHistories)
            {
                h.InsertDate = h.InsertDate.AddMinutes(-1);
            }

            // guarantor accepts for different amount
            financePlan.PaymentAmount = 200m;
            financePlan.SetTheAmountDueForFinancePlan(statement);

            Assert.AreEqual(0, financePlan.CurrentBucket);
        }

        [Test]
        public void FinancePlan_has_current_bucket_is_not_relieved_by_non_recurringpayment()
        {
            //A user not previously past due, in grace period should have 1 bucket (bucket 0)
            // bucket 0 can only be relieved by PaymentTypeEnum.RecurringPaymentFinancePlan
            //CREATE A Finance Plan with 1000 balance and 100 payments;
            FinancePlan financePlan = CreateFinancePlan(1000, 100);

            int vpStatementId = 1;
            int paymentId = 1;

            //Create an Amount Due for the statement 0 months ago (current)
            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddDays(10), vpStatementId++);

            //Make a payment that is not PaymentTypeEnum.RecurringPaymentFinancePlan
            Payment payment = GetPaymentForBucketTests(paymentId, financePlan, 100);
            Assert.True(payment.IsNotRecurringPayment());
            financePlan.OnPayment(-100, DateTime.UtcNow, payment);
            // We expect a bucket 0 as it can only be cleared with PaymentTypeEnum.RecurringPaymentFinancePlan
            AssertCommon(
                plan: financePlan,
                bucketCount: 1,
                currentAmount: 100,
                pastDueAmount: 0,
                bucket0Balance: 100,
                bucket1Balance: 0,
                bucket2Balance: 0,
                bucket3Balance: 0);

        }

        private Payment GetPaymentForBucketTests(int paymentId, FinancePlan financePlan, decimal actualAmount)
        {
            Payment payment = new Payment
            {
                PaymentId = paymentId,
                // most of tests are not requiring PaymentTypeEnum.RecurringPaymentFinancePlan
                PaymentType = PaymentTypeEnum.ManualPromptSpecificAmount
            };
            PaymentAllocation paymentAllocation = new PaymentAllocation
            {
                PaymentAllocationId = paymentId,
                Payment = payment,
                ActualAmount = actualAmount,
                FinancePlanId = financePlan.FinancePlanId,
                PaymentVisit = new PaymentVisit {VisitId = financePlan.FinancePlanVisits.First().VisitId}
            };
            payment.PaymentAllocations.Add(paymentAllocation);
            return payment;
        }

        private Payment GetPaymentForBucketTests2(int paymentId, FinancePlan financePlan, decimal actualAmount)
        {
            Payment payment = new Payment
            {
                PaymentId = paymentId,
                PaymentType = PaymentTypeEnum.ManualPromptSpecificAmount
            };
            PaymentAllocation paymentAllocation = new PaymentAllocation
            {
                PaymentAllocationId = paymentId,
                Payment = payment,
                ActualAmount = actualAmount,
                FinancePlanId = financePlan.FinancePlanId,
                PaymentVisit = new PaymentVisit { VisitId = financePlan.FinancePlanVisits.First().VisitId }
            };
            payment.PaymentAllocations.Add(paymentAllocation);
            return payment;
        }

        #region balance tests
        
        [TestCase(1000.00, 00.00, 00.00, 1000.00)]
        [TestCase(1000.00, 10.00, 00.00, 1010.00)]
        [TestCase(1000.00, 10.00, -20.00, 990.00)]
        public void FinancePlan_CurrentBalance_SingleVisit(decimal currentVisitBalance, decimal interestDue, decimal unclearedPaymentsSum, decimal expected)
        {
            FinancePlan financePlan = FinancePlanFactory.CreateFinancePlan(Enumerable.Range(0, 1).Select(x => VisitFactory.GenerateActiveVisit(1000m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: GuarantorFactory.GenerateOnline())).ToList(), 0, 100m, DateTime.UtcNow.AddMonths(-1));

            financePlan.FinancePlanVisits.First().UnclearedPaymentsSum = unclearedPaymentsSum;
            
            if (interestDue != 0)
            {
                financePlan.FinancePlanVisits.First().AddFinancePlanVisitInterestAssessment(interestDue, DateTime.UtcNow.AddDays(10));
            }            
            Assert.AreEqual(expected, financePlan.CurrentFinancePlanBalance);
        }

        [Test]
        public void FinancePlan_CurrentBalance_MultipleVisits_CurrentVisitBalanceOnly()
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(1000m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor),
                VisitFactory.GenerateActiveVisit(500m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor),
                VisitFactory.GenerateActiveVisit(3.07m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor)
            };
            FinancePlan financePlan = FinancePlanFactory.CreateFinancePlan(visits, 0, 100m, DateTime.UtcNow.AddMonths(-1));

            Assert.AreEqual(1503.07, financePlan.CurrentFinancePlanBalance);
        }

        [Test]
        public void FinancePlan_CurrentBalance_MultipleVisits_WithInterestDue()
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(1000m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor),
                VisitFactory.GenerateActiveVisit(500m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor),
                VisitFactory.GenerateActiveVisit(3.07m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor)
            };
            FinancePlan financePlan = FinancePlanFactory.CreateFinancePlan(visits, 0, 100m, DateTime.UtcNow.AddMonths(-1));

            foreach (FinancePlanVisit financePlanVisit in financePlan.FinancePlanVisits)
            {
                financePlanVisit.AddFinancePlanVisitInterestAssessment(10m, DateTime.UtcNow);
            }

            Assert.AreEqual(1533.07, financePlan.CurrentFinancePlanBalance);
        }

        [Test]
        public void FinancePlan_CurrentBalance_MultipleVisits_WithUnclearedPaymentsAndInterestDue()
        {

            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            Tuple<Visit, IList<VisitTransaction>> firstVisit = VisitFactory.GenerateActiveVisit(1000m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor);
            Tuple<Visit, IList<VisitTransaction>> secondVisit = VisitFactory.GenerateActiveVisit(500m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor);
            Tuple<Visit, IList<VisitTransaction>> thirdVisit = VisitFactory.GenerateActiveVisit(3.07m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor);
            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                firstVisit,
                secondVisit,
                thirdVisit
            };
            FinancePlan financePlan = FinancePlanFactory.CreateFinancePlan(visits, 0, 100m, DateTime.UtcNow.AddMonths(-1));

            financePlan.FinancePlanVisits.Where(x => x.VisitId == firstVisit.Item1.VisitId).FirstOrDefault().UnclearedPaymentsSum = decimal.Negate(20m);
            financePlan.FinancePlanVisits.Where(x => x.VisitId == secondVisit.Item1.VisitId).FirstOrDefault().UnclearedPaymentsSum = decimal.Negate(30m);
            financePlan.FinancePlanVisits.Where(x => x.VisitId == thirdVisit.Item1.VisitId).FirstOrDefault().UnclearedPaymentsSum = decimal.Negate(1.01m);
            
            foreach (FinancePlanVisit financePlanVisit in financePlan.FinancePlanVisits)
            {
                financePlanVisit.AddFinancePlanVisitInterestAssessment(10m, DateTime.UtcNow);
            }

            Assert.AreEqual(1482.06, financePlan.CurrentFinancePlanBalance);
        }

        [Test]
        public void FinancePlan_CurrentBalance_HardRemoveSet()
        {
            const decimal visit1Balance = 1000m;
            const decimal visit2Balance = 777m;

            FinancePlan financePlan = new FinancePlan();
            financePlan.FinancePlanVisits.Add(new FinancePlanVisit
            {
                FinancePlanVisitPrincipalAmounts = new List<FinancePlanVisitPrincipalAmount>
                {
                    new FinancePlanVisitPrincipalAmount
                    {
                        Amount = visit1Balance,
                    }
                },
                CurrentVisitBalance = visit1Balance,
                HardRemoveDate = DateTime.UtcNow
            });
            financePlan.FinancePlanVisits.Add(new FinancePlanVisit
            {
                FinancePlanVisitPrincipalAmounts = new List<FinancePlanVisitPrincipalAmount>
                {
                    new FinancePlanVisitPrincipalAmount
                    {
                        Amount = visit2Balance
                    }
                },
                CurrentVisitBalance = visit2Balance,
            });

            Assert.AreEqual(visit2Balance, financePlan.CurrentFinancePlanBalance);
            Assert.AreEqual(visit2Balance, financePlan.CurrentFinancePlanBalanceIncludingAllAdjustments);

            financePlan.FinancePlanVisits[0].HardRemoveDate = null;

            Assert.AreEqual(visit1Balance + visit2Balance, financePlan.CurrentFinancePlanBalance);
            Assert.AreEqual(visit1Balance + visit2Balance, financePlan.CurrentFinancePlanBalanceIncludingAllAdjustments);
        }

        [Test]
        public void FinancePlan_InterestAssessed_InterestDue()
        {
            FinancePlan financePlan = new FinancePlan {OriginationDate = DateTime.UtcNow.AddMonths(-1)};

            FinancePlanVisit fpv1 = new FinancePlanVisit
            {
                FinancePlan = financePlan,
                VisitId = 1,
                CurrentVisitBalance = 1000m,
                CurrentVisitState = VisitStateEnum.Active,
            };
            fpv1.AddFinancePlanVisitInterestAssessment(50m, DateTime.UtcNow);
            fpv1.AddFinancePlanVisitInterestPayment(decimal.Negate(50m), 1);

            FinancePlanVisit fpv2 = new FinancePlanVisit
            {
                FinancePlan = financePlan,
                VisitId = 2,
                CurrentVisitBalance = 2000m
            };
            fpv2.AddFinancePlanVisitInterestAssessment(100m, DateTime.UtcNow);
            fpv2.AddFinancePlanVisitInterestPayment(decimal.Negate(100m), 1);

            financePlan.FinancePlanVisits.AddRange(new[] {fpv1, fpv2});

            Assert.AreEqual(150m, financePlan.InterestAssessed);
            Assert.AreEqual(0m, financePlan.InterestDue);

            fpv1.AddFinancePlanVisitInterestAssessment(45m, DateTime.UtcNow);
            fpv2.AddFinancePlanVisitInterestAssessment(95m, DateTime.UtcNow);

            Assert.AreEqual(290m, financePlan.InterestAssessed);
            Assert.AreEqual(140m, financePlan.InterestDue);
        }

        [Test]
        public void FinancePlan_Principal()
        {
            FinancePlan financePlan = new FinancePlan { InsertDate = DateTime.UtcNow.AddMonths(-1), OriginationDate = DateTime.UtcNow.AddMonths(-1) };

            FinancePlanVisit fpv1 = new FinancePlanVisit
            {
                FinancePlan = financePlan,
                VisitId = 1,
                CurrentVisitBalance = 1000m,
                CurrentVisitState = VisitStateEnum.Active,
                FinancePlanVisitPrincipalAmounts = new List<FinancePlanVisitPrincipalAmount>
                {
                    new FinancePlanVisitPrincipalAmount
                    {
                        Amount = decimal.Negate(45m),
                        FinancePlan = financePlan,
                        PaymentAllocationId = 1,
                        FinancePlanVisitPrincipalAmountType = FinancePlanVisitPrincipalAmountTypeEnum.Payment,
                        InsertDate = DateTime.UtcNow
                    },
                    new FinancePlanVisitPrincipalAmount
                    {
                        Amount = decimal.Negate(1m),
                        FinancePlan = financePlan,
                        FinancePlanVisitPrincipalAmountType = FinancePlanVisitPrincipalAmountTypeEnum.Adjustment,
                        InsertDate = DateTime.UtcNow
                    },
                    new FinancePlanVisitPrincipalAmount
                    {
                        Amount = decimal.Negate(5000m),
                        FinancePlan = financePlan,
                        FinancePlanVisitPrincipalAmountType = FinancePlanVisitPrincipalAmountTypeEnum.FinancePlanCreated,
                        InsertDate = DateTime.UtcNow
                    },
                    new FinancePlanVisitPrincipalAmount
                    {
                        Amount = decimal.Negate(5m),
                        FinancePlan = financePlan,
                        FinancePlanVisitPrincipalAmountType = FinancePlanVisitPrincipalAmountTypeEnum.FinancePlanClosed,
                        InsertDate = DateTime.UtcNow
                    },
                },
            };

            FinancePlanVisit fpv2 = new FinancePlanVisit
            {
                FinancePlan = financePlan,
                VisitId = 2,
                CurrentVisitBalance = 2000m,
                FinancePlanVisitPrincipalAmounts = new List<FinancePlanVisitPrincipalAmount>
                {
                    new FinancePlanVisitPrincipalAmount
                    {
                        Amount = decimal.Negate(55m),
                        FinancePlan = financePlan,
                        PaymentAllocationId = 2,
                        FinancePlanVisitPrincipalAmountType = FinancePlanVisitPrincipalAmountTypeEnum.Payment,
                        InsertDate = DateTime.UtcNow.AddDays(-100),
                        OverrideInsertDate = DateTime.UtcNow
                    },
                    new FinancePlanVisitPrincipalAmount
                    {
                        Amount = decimal.Negate(1m),
                        FinancePlan = financePlan,
                        FinancePlanVisitPrincipalAmountType = FinancePlanVisitPrincipalAmountTypeEnum.Adjustment,
                        InsertDate = DateTime.UtcNow.AddMinutes(-10)
                    },
                    new FinancePlanVisitPrincipalAmount
                    {
                        Amount = decimal.Negate(5000m),
                        FinancePlan = financePlan,
                        FinancePlanVisitPrincipalAmountType = FinancePlanVisitPrincipalAmountTypeEnum.FinancePlanCreated,
                        InsertDate = DateTime.UtcNow.AddMinutes(-10)
                    },
                    new FinancePlanVisitPrincipalAmount
                    {
                        Amount = decimal.Negate(5m),
                        FinancePlan = financePlan,
                        FinancePlanVisitPrincipalAmountType = FinancePlanVisitPrincipalAmountTypeEnum.FinancePlanClosed,
                        InsertDate = DateTime.UtcNow.AddMinutes(-10)
                    },
                }
            };

            financePlan.FinancePlanVisits.AddRange(new[] { fpv1, fpv2 });
            
            Assert.AreEqual(-100m, financePlan.PrincipalPaidForDate(DateTime.UtcNow));
        }

        [Test]
        public void FinancePlan_AdjustedAmount_HS_Balance_Reduction()
        {
            FinancePlan financePlan = new FinancePlan
            {
                InsertDate = DateTime.UtcNow.AddMonths(-1),
                OriginationDate = DateTime.UtcNow.AddMonths(-1),
                OriginatedFinancePlanBalance = 1000  //visit balance will be 100 less to simulate a hospital transaction change            
            };

            FinancePlanVisit fpv1 = new FinancePlanVisit
            {
                FinancePlan = financePlan,
                VisitId = 1,
                CurrentVisitBalance = 900m,//100 lower to  simulate a hospital change to the balance (originally 1000)
                CurrentVisitState = VisitStateEnum.Active,
                UnclearedPaymentsSum = -100m,//10 interest 90 principal
                InterestDue = 10
            };
            fpv1.FinancePlanVisitPrincipalAmounts.Add(new FinancePlanVisitPrincipalAmount()
            {
                Amount = 1000,
                FinancePlan = financePlan,
                FinancePlanVisit = fpv1,
                FinancePlanVisitPrincipalAmountType = FinancePlanVisitPrincipalAmountTypeEnum.FinancePlanCreated,

            });

            financePlan.FinancePlanVisits.Add(fpv1);
            fpv1.AddFinancePlanVisitInterestAssessment(10m, DateTime.UtcNow);
            
            // add payments
            fpv1.FinancePlanAmountDues.AddRange(new[]
            {
                new FinancePlanAmountDue
                {
                    AmountDue = 0,
                    FinancePlan = financePlan,
                    FinancePlanVisit = fpv1,
                    PaymentAllocationId = 1,
                    PaymentAllocationActualAmount = 90m,
                    PaymentAllocationType = PaymentAllocationTypeEnum.VisitPrincipal,
                    InsertDate = DateTime.UtcNow.AddDays(-1)
                },
                new FinancePlanAmountDue
                {
                    AmountDue = 0,
                    FinancePlan = financePlan,
                    FinancePlanVisit = fpv1,
                    PaymentAllocationId = 2,
                    PaymentAllocationActualAmount = 10m,
                    PaymentAllocationType = PaymentAllocationTypeEnum.VisitInterest,
                    InsertDate = DateTime.UtcNow.AddDays(-1)
                }
            });
            //When changeSet imports it'll offset the to reduce the "sales price"
            fpv1.FinancePlanVisitPrincipalAmounts.Add(new FinancePlanVisitPrincipalAmount()
            {
                Amount = -100,
                FinancePlan = financePlan,
                FinancePlanVisit = fpv1,
                FinancePlanVisitPrincipalAmountType = FinancePlanVisitPrincipalAmountTypeEnum.Adjustment,

            });
            Assert.AreEqual(-100m, financePlan.AdjustmentsSinceOrigination);
        }

        [Test]
        public void FinancePlan_AdjustedAmount__HS_Balance_Increase()
        {
            FinancePlan financePlan = new FinancePlan
            {
                InsertDate = DateTime.UtcNow.AddMonths(-1),
                OriginationDate = DateTime.UtcNow.AddMonths(-1),
                OriginatedFinancePlanBalance = 1000  //visit balance will be 100 more to simulate a hospital transaction change            
            };

            FinancePlanVisit fpv1 = new FinancePlanVisit
            {
                FinancePlan = financePlan,
                VisitId = 1,
                CurrentVisitBalance = 1200m,//100 more to  simulate a hospital change to the balance (originally 1000)
                CurrentVisitState = VisitStateEnum.Active,
                UnclearedPaymentsSum = -100m,
                InterestDue = 10
            };
            fpv1.FinancePlanVisitPrincipalAmounts.Add(new FinancePlanVisitPrincipalAmount()
            {
                Amount = 1000,
                FinancePlan = financePlan,
                FinancePlanVisit = fpv1,
                FinancePlanVisitPrincipalAmountType = FinancePlanVisitPrincipalAmountTypeEnum.FinancePlanCreated,

            });
            financePlan.FinancePlanVisits.Add(fpv1);
            fpv1.AddFinancePlanVisitInterestAssessment(10m, DateTime.UtcNow);
            // add payments
            fpv1.FinancePlanAmountDues.AddRange(new[]
            {
                new FinancePlanAmountDue
                {
                    AmountDue = 0,
                    FinancePlan = financePlan,
                    FinancePlanVisit = fpv1,
                    PaymentAllocationId = 1,
                    PaymentAllocationActualAmount = 90m,
                    PaymentAllocationType = PaymentAllocationTypeEnum.VisitPrincipal,
                    InsertDate = DateTime.UtcNow.AddDays(-1)
                },
                new FinancePlanAmountDue
                {
                    AmountDue = 0,
                    FinancePlan = financePlan,
                    FinancePlanVisit = fpv1,
                    PaymentAllocationId = 2,
                    PaymentAllocationActualAmount = 10m,
                    PaymentAllocationType = PaymentAllocationTypeEnum.VisitInterest,
                    InsertDate = DateTime.UtcNow.AddDays(-1)
                }
            });

            //You cant adjust up with closed end FPs
            Assert.AreEqual(0m, financePlan.AdjustmentsSinceOrigination);
            //This is showing that it adjusted up
            Assert.AreEqual(100m, fpv1.VisitBalanceDifference);
        }
        
        [Test]
        public void FinancePlan_WriteOffInterestDue()
        {
            FinancePlan financePlan = new FinancePlan();

            FinancePlanVisit fpv1 = new FinancePlanVisit
            {
                FinancePlan = financePlan,
                VisitId = 1,
                CurrentVisitBalance = 1000m,
                CurrentVisitState = VisitStateEnum.Active
            };
            fpv1.AddFinancePlanVisitInterestAssessment(50m, DateTime.UtcNow);
            fpv1.AddFinancePlanVisitInterestAssessment(45m, DateTime.UtcNow);

            FinancePlanVisit fpv2 = new FinancePlanVisit
            {
                FinancePlan = financePlan,
                VisitId = 2,
                CurrentVisitBalance = 2000m
            };
            fpv2.AddFinancePlanVisitInterestAssessment(100m, DateTime.UtcNow);
            fpv1.AddFinancePlanVisitInterestAssessment(90m, DateTime.UtcNow);

            financePlan.FinancePlanVisits.AddRange(new[] { fpv1, fpv2 });
            
            financePlan.WriteOffInterestDue();

            Assert.AreEqual(0m, financePlan.InterestDue);
        }

        [Test]
        public void FinancePlan_WriteOffInterest_With_Amout()
        {
            FinancePlan financePlan = new FinancePlan();

            FinancePlanVisit fpv1 = new FinancePlanVisit
            {
                FinancePlan = financePlan,
                VisitId = 1,
                CurrentVisitBalance = 1000m,
                CurrentVisitState = VisitStateEnum.Active
            };
            fpv1.AddFinancePlanVisitInterestAssessment(10m, DateTime.UtcNow);
 

            FinancePlanVisit fpv2 = new FinancePlanVisit
            {
                FinancePlan = financePlan,
                VisitId = 2,
                CurrentVisitBalance = 2000m
            };
            fpv1.AddFinancePlanVisitInterestAssessment(20m, DateTime.UtcNow);

            financePlan.FinancePlanVisits.AddRange(new[] { fpv1, fpv2 });

            financePlan.WriteOffInterest(25m);

            Assert.AreEqual(5m, financePlan.InterestDue);
        }


        [Test]
        public void FinancePlanVisit_WriteOffInterestOnlyBalance_DoesNotWriteOff()
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                VisitFactory.GenerateActiveVisit(1000m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor),
            };
            FinancePlan financePlan = FinancePlanFactory.CreateFinancePlan(visits, 0, 100m, DateTime.UtcNow.AddMonths(-1));
            FinancePlanVisit fpv1 = financePlan.FinancePlanVisits.First();
            fpv1.AddFinancePlanVisitInterestAssessment(50m, DateTime.UtcNow);
            fpv1.AddFinancePlanVisitInterestAssessment(45m, DateTime.UtcNow);

            // interest not less than current balance
            Assert.False(fpv1.WriteOffInterestOnlyBalance());

            // interest due <= 0
            fpv1.AddFinancePlanVisitInterestPayment(decimal.Negate(50m), 1);
            fpv1.AddFinancePlanVisitInterestPayment(decimal.Negate(45m), 2);
            Assert.False(fpv1.WriteOffInterestOnlyBalance());
        }

        [Test]
        public void FinancePlanVisit_WriteOffInterestOnlyBalance_WritesOff()
        {
            FinancePlan financePlan = new FinancePlan();
            FinancePlanVisit fpv1 = new FinancePlanVisit
            {
                FinancePlan = financePlan,
                VisitId = 1,
                CurrentVisitBalance = 0,
                CurrentVisitState = VisitStateEnum.Active
            };
            fpv1.AddFinancePlanVisitInterestAssessment(50m, DateTime.UtcNow);
            fpv1.AddFinancePlanVisitInterestAssessment(45m, DateTime.UtcNow);

            Assert.True(fpv1.WriteOffInterestOnlyBalance());
        }

        #endregion
        
        [Test]
        public void FinancePlan_AmountDue_HasPastDueBucket_RecurringPaymentNotAttempted_BorderCase()
        {
            FinancePlan financePlan = CreateFinancePlan(1000, 100);

            int vpStatementId = 1;

            //Create an Amount Due for the statement 1 months ago
            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddMonths(-1), vpStatementId++);
            //Create an Amount Due for the statement 0 months ago (current)
            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddMonths(0), vpStatementId++);

            Assert.AreEqual(1, financePlan.Buckets.Keys.Max());
            Assert.AreEqual(false, financePlan.HasPastDueBucketAndRecurringPaymentAttempted());
        }
        [Test]
        public void FinancePlan_AmountDue_HasPastDueBucket_RecurringPaymentAttempted_BorderCase()
        {
            FinancePlan financePlan = CreateFinancePlan(1000, 100);

            int vpStatementId = 1;

            //Create an Amount Due for the statement 1 months ago
            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddMonths(-1), vpStatementId++);
            //Create an Amount Due for the statement 0 months ago (current)
            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddMonths(0), vpStatementId++);

            //Need to add a failed payment to the amount due
            financePlan.AddFinancePlanAmountsDueFailedPayment(DateTime.UtcNow, 1, "Payment Failed");

            Assert.AreEqual(1, financePlan.Buckets.Keys.Max());
            Assert.AreEqual(true, financePlan.HasPastDueBucketAndRecurringPaymentAttempted());
        }
        [Test]
        public void FinancePlan_AmountDue_HasPastDueBucket_RecurringPaymentAttempted_VeryPastDue()
        {
            FinancePlan financePlan = CreateFinancePlan(1000, 100);

            int vpStatementId = 1;
            //Create an Amount Due for the statement 2 months ago
            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddMonths(-2), vpStatementId++);
            //Create an Amount Due for the statement 1 months ago
            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddMonths(-1), vpStatementId++);
            //Create an Amount Due for the statement 0 months ago (current)
            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddMonths(0), vpStatementId++);
            
            Assert.AreEqual(true, financePlan.HasPastDueBucketAndRecurringPaymentAttempted());
        }
        [Test]
        public void FinancePlan_AmountDue_DoesntHavePastDueBucket_RecurringPaymentAttempted()
        {
            FinancePlan financePlan = CreateFinancePlan(1000, 100);

            int vpStatementId = 1;

            //Create an Amount Due for the statement 0 months ago (current)
            financePlan.AddFinancePlanAmountsDueStatement(100, DateTime.UtcNow.AddMonths(0), vpStatementId++);

            //Need to add a failed payment to the amount due
            financePlan.AddFinancePlanAmountsDueFailedPayment(DateTime.UtcNow, 1, "Payment Failed");

            Assert.AreEqual(0, financePlan.Buckets.Keys.Max());
            Assert.AreEqual(false, financePlan.HasPastDueBucketAndRecurringPaymentAttempted());
        }

        [TestCase(true)]
        [TestCase(false)]
        public void FinancePlan_AddFinancePlanAmountsDueConsolidatation(bool isGracePeriod)
        {
            FinancePlan financePlan = CreateFinancePlan(1000m, 100m);

            DateTime anchor = DateTime.UtcNow;
            if (!isGracePeriod)
            {
                anchor = anchor.AddDays(-1);
            }

            int vpStatementId = 1;
            //Create an Amount Due for the statement 2 months ago
            financePlan.AddFinancePlanAmountsDueStatement(100, anchor.AddMonths(-2), vpStatementId++);
            //Create an Amount Due for the statement 1 months ago
            financePlan.AddFinancePlanAmountsDueStatement(100, anchor.AddMonths(-1), vpStatementId++);
            //Create an Amount Due for the statement 0 months ago (current)
            financePlan.AddFinancePlanAmountsDueStatement(100, anchor.AddMonths(0), vpStatementId++);
            
            Assert.AreEqual(isGracePeriod ? 2 : 3, financePlan.CurrentBucket);
            
            financePlan.AddFinancePlanAmountsDueConsolidatation(true);

            Assert.AreEqual(0, financePlan.CurrentBucket);
        }

        [Test]
        public void FinancePlan_InterestFreePeriod()
        {
            Assert.AreEqual(0, (int)FirstInterestPeriodEnum.NoAction);
            Assert.AreEqual(1, (int)FirstInterestPeriodEnum.PushOnePeriod);

            Assert.AreEqual(FirstInterestPeriodEnum.PushOnePeriod, FinancePlan.FirstInterestPeriod(true));
            Assert.AreEqual(FirstInterestPeriodEnum.NoAction, FinancePlan.FirstInterestPeriod(false));
        }

        [Test]
        public void FinancePlan_StatusChange_OutboundVisitMessageInserted()
        {
            FinancePlan financePlan = CreateFinancePlan(1000m, 100m);
            financePlan.SetFinancePlanStatus(new FinancePlanStatus { FinancePlanStatusId = (int)FinancePlanStatusEnum.GoodStanding }, string.Empty);
            foreach (FinancePlanStatusHistory h in financePlan.FinancePlanStatusHistory)
            { 
                h.InsertDate = h.InsertDate.AddMinutes(-1);
            }

            int expectedCount = 1;
            Assert.AreEqual(expectedCount, financePlan.OutboundVisitMessages.Count);
            OutboundVisitMessage outboundMessage = financePlan.OutboundVisitMessages[expectedCount - 1];
            Assert.AreEqual(VpOutboundVisitMessageTypeEnum.FinancePlanVisitStatusChanged, outboundMessage.VpOutboundVisitMessageType);
            Assert.AreEqual(FinancePlanStatusEnum.GoodStanding.ToString(), outboundMessage.OutboundValue);

            financePlan.SetFinancePlanStatus(new FinancePlanStatus { FinancePlanStatusId = (int)FinancePlanStatusEnum.PaidInFull }, string.Empty);
            expectedCount += 1;
            outboundMessage = financePlan.OutboundVisitMessages[expectedCount - 1];
            Assert.AreEqual(VpOutboundVisitMessageTypeEnum.FinancePlanVisitStatusChanged, outboundMessage.VpOutboundVisitMessageType);
            Assert.AreEqual(FinancePlanStatusEnum.PaidInFull.ToString(), outboundMessage.OutboundValue);
        }

        [Test]
        public void FinancePlan_BucketChange_OutboundVisitMessageInserted()
        {
            FinancePlan financePlan = CreateFinancePlan(1000m, 100m);
            financePlan.AddFinancePlanAmountsDueStatement(100m, DateTime.UtcNow, 1);
            foreach (FinancePlanAmountDue amountDue in financePlan.FinancePlanAmountsDue)
            {
                amountDue.InsertDate = amountDue.InsertDate.AddMinutes(-1);
            }

            int expectedCount = 1;
            Assert.AreEqual(expectedCount, financePlan.OutboundVisitMessages.Count);
            OutboundVisitMessage outboundMessage = financePlan.OutboundVisitMessages[expectedCount - 1];
            Assert.AreEqual(VpOutboundVisitMessageTypeEnum.FinancePlanVisitBucketChanged, outboundMessage.VpOutboundVisitMessageType);
            Assert.AreEqual("0", outboundMessage.OutboundValue);

            financePlan.AddFinancePlanAmountsDueStatement(100m, DateTime.UtcNow.AddMonths(1), 2);

            expectedCount += 1;
            outboundMessage = financePlan.OutboundVisitMessages[expectedCount - 1];
            Assert.AreEqual(VpOutboundVisitMessageTypeEnum.FinancePlanVisitBucketChanged, outboundMessage.VpOutboundVisitMessageType);
            Assert.AreEqual("1", outboundMessage.OutboundValue);
        }

        #region Derived Properties

        [Test]
        public void FinacePlan_FinancePlanOfferSetId_calculates_correctly()
        {
            // Handles the default
            FinancePlan financePlan = CreateFinancePlan(1000, 100);
            Assert.AreEqual(0, financePlan.FinancePlanOfferSetTypeId);

            // Handles expected derivative 
            financePlan.FinancePlanOffer = new FinancePlanOffer { FinancePlanOfferSetType = new FinancePlanOfferSetType { FinancePlanOfferSetTypeId = 5 } };
            Assert.AreEqual(5, financePlan.FinancePlanOfferSetTypeId);

            // Handles nulls in derivative 
            financePlan.FinancePlanOffer = new FinancePlanOffer { FinancePlanOfferSetType = new FinancePlanOfferSetType() };
            Assert.AreEqual(0, financePlan.FinancePlanOfferSetTypeId);
        }

        [Test]
        public void FinancePlan_IsFinancePlanOfferTypePatient_calculates_correctly()
        {
            // Handles the default
            FinancePlan financePlan = CreateFinancePlan(1000, 100);
            Assert.True(financePlan.IsFinancePlanOfferTypePatient);

            // Handles expected derivative 
            financePlan.FinancePlanOffer = new FinancePlanOffer { FinancePlanOfferSetType = new FinancePlanOfferSetType { IsPatient = false} };
            Assert.False(financePlan.IsFinancePlanOfferTypePatient);

            // Handles expected derivative 
            financePlan.FinancePlanOffer = new FinancePlanOffer { FinancePlanOfferSetType = new FinancePlanOfferSetType { IsPatient = true } };
            Assert.True(financePlan.IsFinancePlanOfferTypePatient);
        }

        [Test]
        public void FinancePlan_IsFinancePlanOfferTypeClientOnly_calculates_property()
        {
            // Handles the default
            FinancePlan financePlan = CreateFinancePlan(1000, 100);
            Assert.False(financePlan.IsFinancePlanOfferTypeClientOnly);

            // Handles expected derivative 
            financePlan.FinancePlanOffer = new FinancePlanOffer { FinancePlanOfferSetType = new FinancePlanOfferSetType { IsPatient = false, IsClient = true} };
            Assert.True(financePlan.IsFinancePlanOfferTypeClientOnly);

            // Handles expected derivative 
            financePlan.FinancePlanOffer = new FinancePlanOffer { FinancePlanOfferSetType = new FinancePlanOfferSetType { IsPatient = false, IsClient = false } };
            Assert.False(financePlan.IsFinancePlanOfferTypeClientOnly);
        }
        
        [Test]
        public void FinancePlan_CurrentInterestRate_calculates_correctly()
        {
            // This test basically adds 3 rate histories and and asserts that the latest rate is returned after each addition 

            decimal firstInterestRate = 8.50m;
            decimal secondInterestRate = 4m;
            decimal thirdInterestRate = 0;

            FinancePlanBuilder financePlanBuilder = new FinancePlanBuilder();
            FinancePlan financePlan = financePlanBuilder.WithDefaultValues();
            // there are no FinancePlanInterestRateHistory
            Assert.AreEqual(0, financePlan.FinancePlanInterestRateHistory.Count);

            //add first history
            financePlanBuilder.AddFinancePlanInterestRateHistory(firstInterestRate);
            decimal interestRate = financePlan.CurrentInterestRate;
            Assert.AreEqual(firstInterestRate, interestRate);

            //add second history
            financePlanBuilder.AddFinancePlanInterestRateHistory(secondInterestRate);
            interestRate = financePlan.CurrentInterestRate;
            Assert.AreEqual(secondInterestRate, interestRate);

            //add third history
            financePlanBuilder.AddFinancePlanInterestRateHistory(thirdInterestRate);
            interestRate = financePlan.CurrentInterestRate;
            Assert.AreEqual(thirdInterestRate, interestRate);
        }

        [Test]
        public void FinancePlan_AddFinancePlanVisitInterestDuePaymentAllocation()
        {
            //Arrange
            decimal interestDue = 30m;
            FinancePlan financePlan = CreateFinancePlan(5000, 160);
            financePlan.AddFinancePlanAmountsDueStatement(160, DateTime.UtcNow.AddMinutes(-1), 1);
            FinancePlanVisit financePlanVisit = financePlan.FinancePlanVisits.First();

            //interest due from assessed interest (statement)
            FinancePlanVisitInterestDue financePlanVisitInterestDue = new FinancePlanVisitInterestDue
            {
                CurrentBalanceAtTimeOfAssessment = financePlanVisit.CurrentBalance,
                FinancePlan = financePlan,
                InterestDue = interestDue,
                VpStatementId = 1,
                FinancePlanVisit = financePlanVisit,
                InterestRateUsedToAssess = financePlan.CurrentInterestRate,
                InsertDate = DateTime.UtcNow
            };
            financePlanVisit.FinancePlanVisitInterestDues.Add(financePlanVisitInterestDue);

            PaymentAllocation paymentAllocation = new PaymentAllocation
            {
                ActualAmount = interestDue,
                FinancePlanId = financePlan.FinancePlanId,
                PaymentAllocationId = 123,
                PaymentAllocationType = PaymentAllocationTypeEnum.VisitInterest,
                PaymentVisit = new PaymentVisit { VisitId = financePlanVisit.VisitId }
            };

            //Act
            financePlan.AddFinancePlanVisitInterestDuePaymentAllocation(paymentAllocation);
            FinancePlanVisitInterestDue financePlanVisitInterestDueTest = financePlanVisit.FinancePlanVisitInterestDues.FirstOrDefault(x => x.PaymentAllocationId == paymentAllocation.PaymentAllocationId);

            //Assert
            Assert.True(financePlanVisit.FinancePlanVisitInterestDues.Count == 2, "Expected 2 FinancePlanVisitInterestDue, one for statement and one for payment");
            decimal interestRelief = decimal.Negate(financePlanVisitInterestDueTest?.InterestDue ?? 0m);
            Assert.True(interestRelief == interestDue, "FinancePlanVisitInterestDue was not added correctly");
        }
        
        #endregion

        #region Principal Amounts

        [Test]
        public void FinancePlan_PrincipalAmounts_NegativeAdjustmentShowBalanceDifference()
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            Tuple<Visit, IList<VisitTransaction>> firstVisit = VisitFactory.GenerateActiveVisit(1000m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor);
            Tuple<Visit, IList<VisitTransaction>> secondVisit = VisitFactory.GenerateActiveVisit(500m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor);
            Tuple<Visit, IList<VisitTransaction>> thirdVisit = VisitFactory.GenerateActiveVisit(3.07m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor);
            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                firstVisit,
                secondVisit,
                thirdVisit
            };
            FinancePlan financePlan = FinancePlanFactory.CreateFinancePlan(visits, 0, 100m, DateTime.UtcNow.AddMonths(-1));

            FinancePlanVisit firstFinancePlanVisit = financePlan.FinancePlanVisits.FirstOrDefault(x => x.VisitId == firstVisit.Item1.VisitId);
            //Set it from 1000 HS balance to 900
            firstFinancePlanVisit.CurrentVisitBalance = 900m;

            //Difference between what we think the principal amount should be and the hs balance
            Assert.AreEqual(-100, firstFinancePlanVisit.VisitBalanceDifference);
            //When the HS amount goes down, the CurrentFinancePlanBalance follows
            Assert.AreEqual(1403.07m, financePlan.CurrentFinancePlanBalance);
            //Insert the adjustment
            financePlan.AddFinancePlanVisitPrincipalAmountAdjustments();
            //Since the adjustment went in should now be zero
            Assert.AreEqual(0, firstFinancePlanVisit.VisitBalanceDifference);
            //Balance shouldnt have changed
            Assert.AreEqual(1403.07m, financePlan.CurrentFinancePlanBalance);

            //Adjust back up from 900 HS balance to 1000
            firstFinancePlanVisit.CurrentVisitBalance = 1000m;
            //Balance shouldnt have changed
            Assert.AreEqual(1403.07m, financePlan.CurrentFinancePlanBalance);

            //Even if more comes in, cannot go up.
            //Adjust back up from 1000 HS balance to 1100
            firstFinancePlanVisit.CurrentVisitBalance = 1100m;
            //Balance shouldnt have changed
            Assert.AreEqual(1403.07m, financePlan.CurrentFinancePlanBalance);
        }

        [Test]
        public void FinancePlan_PrincipalAmounts_PaymentLowersCurrentFinancePlanBalance()
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            Tuple<Visit, IList<VisitTransaction>> firstVisit = VisitFactory.GenerateActiveVisit(1000m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor);
            Tuple<Visit, IList<VisitTransaction>> secondVisit = VisitFactory.GenerateActiveVisit(500m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor);
            Tuple<Visit, IList<VisitTransaction>> thirdVisit = VisitFactory.GenerateActiveVisit(3.07m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor);
            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                firstVisit,
                secondVisit,
                thirdVisit
            };
            FinancePlan financePlan = FinancePlanFactory.CreateFinancePlan(visits, 0, 100m, DateTime.UtcNow.AddMonths(-1));

            FinancePlanVisit firstFinancePlanVisit = financePlan.FinancePlanVisits.FirstOrDefault(x => x.VisitId == firstVisit.Item1.VisitId);

            PaymentAllocation newAllocation = new PaymentAllocationBuilder().WithDefaultValues(firstFinancePlanVisit.VisitId).WithAmount(100m);

            financePlan.OnPaymentAllocation(newAllocation.ActualAmount, DateTime.UtcNow, newAllocation);
            //Because of the allocation here the projection would naturally update the uncleared amount, but test entity doesnt.
            firstFinancePlanVisit.UnclearedPaymentsSum = -100m;
            
            //Difference between what we think the principal amount should be and the hs balance
            Assert.AreEqual(0m, firstFinancePlanVisit.VisitBalanceDifference);
            //When payment happens, the CurrentFinancePlanBalance follows
            Assert.AreEqual(1403.07m, financePlan.CurrentFinancePlanBalance);

            //After Payment happens and the balance goes up
            //Adjust back up from 1000 HS balance to 1100
            firstFinancePlanVisit.CurrentVisitBalance = 1100m;
            //Balance shouldnt have changed
            Assert.AreEqual(1403.07m, financePlan.CurrentFinancePlanBalance);
        }

        
        [Test]
        public void FinancePlan_PrincipalAmounts_AmountIncreasesAfterOrig()
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            Tuple<Visit, IList<VisitTransaction>> firstVisit = VisitFactory.GenerateActiveVisit(1000m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor);
            Tuple<Visit, IList<VisitTransaction>> secondVisit = VisitFactory.GenerateActiveVisit(500m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor);
            Tuple<Visit, IList<VisitTransaction>> thirdVisit = VisitFactory.GenerateActiveVisit(3.07m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor);
            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                firstVisit,
                secondVisit,
                thirdVisit
            };
            FinancePlan financePlan = FinancePlanFactory.CreateFinancePlan(visits, 0, 100m, DateTime.UtcNow.AddMonths(-1));

            FinancePlanVisit firstFinancePlanVisit = financePlan.FinancePlanVisits.FirstOrDefault(x => x.VisitId == firstVisit.Item1.VisitId);
            //Baseline check
            Assert.AreEqual(1503.07m, financePlan.CurrentFinancePlanBalance);

            //Adjust up from 1000 HS balance to 1100
            firstFinancePlanVisit.CurrentVisitBalance = 1100m;
            
            //Difference between what we think the principal amount should be and the hs balance
            Assert.AreEqual(100m, firstFinancePlanVisit.VisitBalanceDifference);
            
            //The CurrentFP balance shouldnt have gone up
            Assert.AreEqual(1503.07m, financePlan.CurrentFinancePlanBalance);

        }

        
        [TestCase(900)]
        [TestCase(1000)]
        [TestCase(1100)]
        public void FinancePlan_PrincipalAmounts_RemovesPrincipalWhenFinancePlanCloses(decimal amountToChangeVisitBalance)
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            Tuple<Visit, IList<VisitTransaction>> firstVisit = VisitFactory.GenerateActiveVisit(1000m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor);
            Tuple<Visit, IList<VisitTransaction>> secondVisit = VisitFactory.GenerateActiveVisit(500m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor);
            Tuple<Visit, IList<VisitTransaction>> thirdVisit = VisitFactory.GenerateActiveVisit(3.07m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor);
            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>()
            {
                firstVisit,
                secondVisit,
                thirdVisit
            };
            FinancePlan financePlan = FinancePlanFactory.CreateFinancePlan(visits, 0, 100m, DateTime.UtcNow.AddMonths(-1));

            FinancePlanVisit firstFinancePlanVisit = financePlan.FinancePlanVisits.FirstOrDefault(x => x.VisitId == firstVisit.Item1.VisitId);
            //Baseline check
            Assert.AreEqual(1503.07m, financePlan.CurrentFinancePlanBalance);

            //Adjust up from 1000 HS balance to 1100
            firstFinancePlanVisit.CurrentVisitBalance = amountToChangeVisitBalance;
            
            //The CurrentFP balance shouldnt have gone up
            Assert.LessOrEqual(financePlan.CurrentFinancePlanBalance, 1503.07m);

            financePlan.OnFinancePlanClose("Closing");

            //Should have gone to 0
            Assert.AreEqual(0m, financePlan.CurrentFinancePlanBalance);

        }

        #endregion

        #region reconfig

        [Test]
        public void FinancePlan_IsEligibleForReconfiguration_ByStatus()
        {
            FinancePlan financePlan = new FinancePlan();

            IEnumerable<FinancePlanStatus> allStatuses = Enum.GetValues(typeof(FinancePlanStatusEnum)).Cast<FinancePlanStatusEnum>().Select(x => new FinancePlanStatus {FinancePlanStatusId = (int) x});
            foreach (FinancePlanStatus financePlanStatus in allStatuses)
            {
                financePlan.FinancePlanStatusHistory.Clear();
                financePlan.SetFinancePlanStatus(financePlanStatus, "");
                if (financePlanStatus.IsActiveOriginated())
                {
                    Assert.True(financePlan.IsEligibleForReconfiguration(), $"{(FinancePlanStatusEnum) financePlanStatus.FinancePlanStatusId} SHOULD be eligible for reconfiguration");
                }
                else
                {
                    Assert.False(financePlan.IsEligibleForReconfiguration(), $"{(FinancePlanStatusEnum) financePlanStatus.FinancePlanStatusId} should NOT be eligible for reconfiguration");
                }
            }
        }

        [Test]
        public void FinancePlan_IsEligibleForReconfiguration_WithReconfiguredFinancePlan()
        {
            FinancePlan financePlan = new FinancePlan();
            financePlan.SetFinancePlanStatus(new FinancePlanStatus {FinancePlanStatusId = (int) FinancePlanStatusEnum.GoodStanding}, "");

            // no reconfiguration
            Assert.True(financePlan.IsEligibleForReconfiguration());

            // with a pending reconfig
            financePlan.ReconfiguredFinancePlan = new FinancePlan {OriginalFinancePlan = financePlan};
            financePlan.ReconfiguredFinancePlan.SetFinancePlanStatus(new FinancePlanStatus {FinancePlanStatusId = (int) FinancePlanStatusEnum.PendingAcceptance}, "");
            Assert.True(financePlan.IsEligibleForReconfiguration());

            // with a reconfig that's already happened
            financePlan.ReconfiguredFinancePlan.FinancePlanStatusHistory.Clear();
            financePlan.ReconfiguredFinancePlan.SetFinancePlanStatus(new FinancePlanStatus {FinancePlanStatusId = (int) FinancePlanStatusEnum.GoodStanding}, "");
            Assert.False(financePlan.IsEligibleForReconfiguration());
        }

        [TestCase(true)]
        [TestCase(false)]
        public void FinancePlan_IsReconfiguration(bool isReconfiguration)
        {
            FinancePlan financePlan = new FinancePlan();
            if (isReconfiguration)
            {
                financePlan.OriginalFinancePlan = new FinancePlan();
            }

            Assert.AreEqual(isReconfiguration, financePlan.IsReconfiguration());
        }

        [Test]
        public void FinancePlan_IsPendingReconfiguration_WithPending()
        {
            FinancePlan financePlan = new FinancePlan {OriginalFinancePlan = new FinancePlan()};
            financePlan.SetFinancePlanStatus(new FinancePlanStatus {FinancePlanStatusId = (int) FinancePlanStatusEnum.PendingAcceptance}, "");

            Assert.True(financePlan.IsPendingReconfiguration());
        }

        [Test]
        public void FinancePlan_IsPendingReconfiguration_WithOriginatedReconfiguration()
        {
            FinancePlan financePlan = new FinancePlan {OriginalFinancePlan = new FinancePlan()};
            financePlan.SetFinancePlanStatus(new FinancePlanStatus {FinancePlanStatusId = (int) FinancePlanStatusEnum.GoodStanding}, "");

            Assert.False(financePlan.IsPendingReconfiguration());
        }

        [Test]
        public void FinancePlan_IsPendingReconfiguration_WithoutPending()
        {
            FinancePlan financePlan = new FinancePlan();
            Assert.False(financePlan.IsPendingReconfiguration());
        }

        #endregion

        public static FinancePlan CreateFinancePlan(decimal balance, decimal paymentAmount)
        {
            FinancePlan financePlan = new FinancePlan
            { 
                PaymentAmount = paymentAmount, 
                CurrentFinancePlanBalance = 1000, 
                VpGuarantor = GuarantorFactory.GenerateOnline(),
                CreatedVpStatement = new VpStatement()
            };

            Tuple<Visit, IList<VisitTransaction>> visit = VisitFactory.GenerateActiveVisit(balance, 1);
            visit.Item1.SetCurrentBalance(balance);
            FinancePlanFactory.VisitToFinancePlanVisit(visit, financePlan);

            return financePlan;
        }

        public static void AssertCommon(FinancePlan plan, int bucketCount, decimal currentAmount, decimal pastDueAmount, decimal bucket0Balance, decimal bucket1Balance, decimal bucket2Balance, decimal bucket3Balance)
        {
            Assert.AreEqual(bucketCount, plan.Buckets.Count);
            Assert.AreEqual(currentAmount, plan.CurrentAmount);
            Assert.AreEqual(pastDueAmount, plan.PastDueAmount);

            // plan.Buckets[index]
            if (plan.Buckets.Count > 0) Assert.AreEqual(bucket0Balance, plan.Buckets[0].Item1);
            if (plan.Buckets.Count > 1) Assert.AreEqual(bucket1Balance, plan.Buckets[1].Item1);
            if (plan.Buckets.Count > 2) Assert.AreEqual(bucket2Balance, plan.Buckets[2].Item1);
            if (plan.Buckets.Count > 3) Assert.AreEqual(bucket3Balance, plan.Buckets[3].Item1);

            Func<IDictionary<int, Tuple<decimal, int?>>, int, decimal> safe = (t, i) => t.ContainsKey(i) ? t[i].Item1 : 0;

            Console.WriteLine("Buckets: {0}; Current: {1}; PastDue: {2}; Bucket 0: {3}; Bucket 1: {4}; Bucket 2: {5}; Bucket 3: {6}", plan.Buckets.Count, plan.CurrentAmount, plan.PastDueAmount, safe(plan.Buckets, 0), safe(plan.Buckets, 1), safe(plan.Buckets, 2), safe(plan.Buckets, 3));
        }
    }
}
﻿namespace Ivh.Domain.Enrollment
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Common.Tests.Helpers.Base;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using Guarantor.Interfaces;
    using Guarantor.Services;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class GuarantorEnrollmentServiceTests : DomainTestBase
    {
        [SetUp]
        public void SetUp()
        {
            Mock<IGuarantorEnrollmentProvider> guarantorEnrollmentProvider = new Mock<IGuarantorEnrollmentProvider>();
            guarantorEnrollmentProvider.Setup(x => x.EnrollGuarantorAsync(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>())).Returns((string key, int billing, int vpGuarantorId) =>
            {
                EnrollmentResponseDto response = this.EnrollGuarantorAsyncReturnValues.ContainsKey(key) ? this.EnrollGuarantorAsyncReturnValues[key] : null;
                return Task.FromResult(response);
            });
            this.GuarantorEnrollmentService = new GuarantorEnrollmentService(
                new DomainServiceCommonServiceMockBuilder().CreateServiceLazy(),
                guarantorEnrollmentProvider.Object);
        }

        private IGuarantorEnrollmentService GuarantorEnrollmentService;

        private readonly Dictionary<string, EnrollmentResponseDto> EnrollGuarantorAsyncReturnValues = new Dictionary<string, EnrollmentResponseDto>
        {
            {"Success", new EnrollmentResponseDto{EnrollmentResult = EnrollmentResultEnum.Success}},
            {"Error", new EnrollmentResponseDto{EnrollmentResult = EnrollmentResultEnum.Error}}
        };

        [Test]
        public void EnrollGuarantorAsync_should_throw_exception_when_response_has_error()
        {
            Assert.That(async () => await this.GuarantorEnrollmentService.EnrollGuarantorAsync("Error", 1, 100), Throws.TypeOf<Exception>());
        }

        [Test]
        public async Task EnrollGuarantorAsync_should_not_throw_exception_when_response_has_success()
        {
            EnrollmentResponseDto enrollmentResponseDto = await this.GuarantorEnrollmentService.EnrollGuarantorAsync("Success", 1, 100);
            Assert.AreEqual(EnrollmentResultEnum.Success, enrollmentResponseDto.EnrollmentResult);
        }
    }

}
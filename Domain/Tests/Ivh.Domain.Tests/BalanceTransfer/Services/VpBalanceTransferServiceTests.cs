﻿namespace Ivh.Domain.BalanceTransfer.Services
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.Tests.Helpers.BalanceTransfer;
    using Common.Tests.Helpers.FinancePlan;
    using Common.Tests.Helpers.Visit;
    using Common.VisitPay.Enums;
    using FinanceManagement.FinancePlan.Entities;
    using Moq;
    using NUnit.Framework;
    using Visit.Entities;

    [TestFixture]
    public class VpBalanceTransferServiceTests : DomainTestBase
    {
        [Test]
        public void SetActive_EligibleVisit_MovesActive()
        {
            Tuple<Visit, IList<VisitTransaction>> visitAndTransactions = VisitFactory.GenerateActiveVisit(1234, 1);
            visitAndTransactions.Item1.SetBalanceTransferStatus(new BalanceTransferStatusHistory
            {
                BalanceTransferStatus = new BalanceTransferStatus { BalanceTransferStatusId = (int)BalanceTransferStatusEnum.Eligible }
            });

            BalanceTransferServiceMockBuilder builder = new BalanceTransferServiceMockBuilder();
            builder.WithDefaults().Setup(mock =>
            {
                mock.FinancePlanRepositoryMock.Setup(t => t.GetFinancePlanWithVisit(It.IsAny<Visit>())).Returns(FinancePlanFactory.CreateFinancePlanWithDuration(13, 36));
                mock.VisitRepositoryMock.Setup(t => t.IsOnFinancePlanInStatus(It.IsAny<Visit>(), It.IsAny<IList<FinancePlanStatusEnum>>())).Returns(true);
                mock.VisitRepositoryMock.Setup(t => t.GetBySourceSystemKey(It.IsAny<String>(), It.IsAny<int>())).Returns(visitAndTransactions.Item1);
            });

            builder.CreateService().SetBalanceTransferStatusActiveForVisit(visitAndTransactions.Item1.SourceSystemKey, visitAndTransactions.Item1.BillingSystemId ?? 0);

            Assert.AreEqual(BalanceTransferStatusEnum.ActiveBalanceTransfer, visitAndTransactions.Item1.BalanceTransferStatus.BalanceTransferStatusEnum);
        }

        [Test]
        public void SetActive_IneligibleVisit_StaysIneligible()
        {
            Tuple<Visit, IList<VisitTransaction>> visitAndTransactions = VisitFactory.GenerateActiveVisit(1234, 1);
            visitAndTransactions.Item1.SetBalanceTransferStatus(new BalanceTransferStatusHistory
            {
                BalanceTransferStatus = new BalanceTransferStatus { BalanceTransferStatusId = (int)BalanceTransferStatusEnum.Ineligible }
            });

            BalanceTransferServiceMockBuilder builder = new BalanceTransferServiceMockBuilder();
            builder.Setup(mock =>
            {
                mock.FinancePlanRepositoryMock.Setup(t => t.GetFinancePlanWithVisit(It.IsAny<Visit>())).Returns(FinancePlanFactory.CreateFinancePlanWithDuration(13, 36));
                mock.VisitRepositoryMock.Setup(t => t.IsOnFinancePlanInStatus(It.IsAny<Visit>(), It.IsAny<IList<FinancePlanStatusEnum>>())).Returns(true);
                mock.VisitRepositoryMock.Setup(t => t.GetBySourceSystemKey(It.IsAny<String>(), It.IsAny<int>())).Returns(visitAndTransactions.Item1);
            });

            builder.CreateService().SetBalanceTransferStatusActiveForVisit(visitAndTransactions.Item1.SourceSystemKey, visitAndTransactions.Item1.BillingSystemId ?? 0);

            Assert.AreEqual(BalanceTransferStatusEnum.Ineligible, visitAndTransactions.Item1.BalanceTransferStatus.BalanceTransferStatusEnum);
        }

        [Test]
        public void SetActive_UnsetVisit_WithoutFP_StaysUnset()
        {
            Tuple<Visit, IList<VisitTransaction>> visitAndTransactions = VisitFactory.GenerateActiveVisit(1234, 1);

            BalanceTransferServiceMockBuilder builder = new BalanceTransferServiceMockBuilder();
            builder.Setup(mock =>
            {
                mock.FinancePlanRepositoryMock.Setup(t => t.GetFinancePlanWithVisit(It.IsAny<Visit>())).Returns((Func<FinancePlan>) null);
                mock.VisitRepositoryMock.Setup(t => t.IsOnFinancePlanInStatus(It.IsAny<Visit>(), It.IsAny<IList<FinancePlanStatusEnum>>())).Returns(false);
                mock.VisitRepositoryMock.Setup(t => t.GetBySourceSystemKey(It.IsAny<String>(), It.IsAny<int>())).Returns(visitAndTransactions.Item1);
            });

            builder.CreateService().SetBalanceTransferStatusActiveForVisit(visitAndTransactions.Item1.SourceSystemKey, visitAndTransactions.Item1.BillingSystemId ?? 0);

            Assert.AreEqual(visitAndTransactions.Item1.BalanceTransferStatus, null);
        }

        [Test]
        public void SetEligible_IneligibleVisit_StaysIneligible()
        {
            Tuple<Visit, IList<VisitTransaction>> visitAndTransactions = VisitFactory.GenerateActiveVisit(1234, 1);
            visitAndTransactions.Item1.SetBalanceTransferStatus(new BalanceTransferStatusHistory
            {
                BalanceTransferStatus = new BalanceTransferStatus { BalanceTransferStatusId = (int)BalanceTransferStatusEnum.Ineligible }
            });

            BalanceTransferServiceMockBuilder builder = new BalanceTransferServiceMockBuilder();
            builder.WithDefaults().Setup(mock =>
            {
                mock.FinancePlanRepositoryMock.Setup(t => t.GetFinancePlanWithVisit(It.IsAny<Visit>())).Returns(FinancePlanFactory.CreateFinancePlanWithDuration(13, 36));
                mock.VisitRepositoryMock.Setup(t => t.IsOnFinancePlanInStatus(It.IsAny<Visit>(), It.IsAny<IList<FinancePlanStatusEnum>>())).Returns(true);
                mock.VisitRepositoryMock.Setup(t => t.GetBySourceSystemKey(It.IsAny<String>(), It.IsAny<int>())).Returns(visitAndTransactions.Item1);
            });
            builder.CreateService().SetBalanceTransferStatusEligible(visitAndTransactions.Item1);

            Assert.AreEqual(BalanceTransferStatusEnum.Ineligible, visitAndTransactions.Item1.BalanceTransferStatus.BalanceTransferStatusEnum);
        }

        [Test]
        public void SetEligible_ActiveVisit_MovesEligible()
        {
            Tuple<Visit, IList<VisitTransaction>> visitAndTransactions = VisitFactory.GenerateActiveVisit(1234, 1);
            visitAndTransactions.Item1.SetBalanceTransferStatus(new BalanceTransferStatusHistory
            {
                BalanceTransferStatus = new BalanceTransferStatus { BalanceTransferStatusId = (int)BalanceTransferStatusEnum.ActiveBalanceTransfer }
            });

            BalanceTransferServiceMockBuilder builder = new BalanceTransferServiceMockBuilder();
            builder.WithDefaults().Setup(mock =>
            {
                mock.FinancePlanRepositoryMock.Setup(t => t.GetFinancePlanWithVisit(It.IsAny<Visit>())).Returns(FinancePlanFactory.CreateFinancePlanWithDuration(13, 36));
                mock.VisitRepositoryMock.Setup(t => t.IsOnFinancePlanInStatus(It.IsAny<Visit>(), It.IsAny<IList<FinancePlanStatusEnum>>())).Returns(true);
                mock.VisitRepositoryMock.Setup(t => t.GetBySourceSystemKey(It.IsAny<String>(), It.IsAny<int>())).Returns(visitAndTransactions.Item1);
            });
            builder.CreateService().SetBalanceTransferStatusEligible(visitAndTransactions.Item1);

            Assert.AreEqual(BalanceTransferStatusEnum.Eligible, visitAndTransactions.Item1.BalanceTransferStatus.BalanceTransferStatusEnum);
        }

        [Test]
        public void SetEligible_UnsetVisit_MovesEligible()
        {
            Tuple<Visit, IList<VisitTransaction>> visitAndTransactions = VisitFactory.GenerateActiveVisit(1234, 1);

            BalanceTransferServiceMockBuilder builder = new BalanceTransferServiceMockBuilder();
            builder.WithDefaults().Setup(mock =>
            {
                mock.FinancePlanRepositoryMock.Setup(t => t.GetFinancePlanWithVisit(It.IsAny<Visit>())).Returns(FinancePlanFactory.CreateFinancePlanWithDuration(13, 36));
                mock.VisitRepositoryMock.Setup(t => t.IsOnFinancePlanInStatus(It.IsAny<Visit>(), It.IsAny<IList<FinancePlanStatusEnum>>())).Returns(true);
                mock.VisitRepositoryMock.Setup(t => t.GetBySourceSystemKey(It.IsAny<String>(), It.IsAny<int>())).Returns(visitAndTransactions.Item1);
            });
            builder.CreateService().SetBalanceTransferStatusEligible(visitAndTransactions.Item1);

            Assert.AreEqual(BalanceTransferStatusEnum.Eligible, visitAndTransactions.Item1.BalanceTransferStatus.BalanceTransferStatusEnum);
        }

        [Test]
        public void SetEligible_FinancePlanTooShort()
        {
            Tuple<Visit, IList<VisitTransaction>> visitAndTransactions = VisitFactory.GenerateActiveVisit(1234, 1);

            BalanceTransferServiceMockBuilder builder = new BalanceTransferServiceMockBuilder();
            builder.Setup(mock =>
            {
                mock.FinancePlanRepositoryMock.Setup(t => t.GetFinancePlanWithVisit(It.IsAny<Visit>())).Returns(FinancePlanFactory.CreateFinancePlanWithDuration(1, 12));
                mock.VisitRepositoryMock.Setup(t => t.IsOnFinancePlanInStatus(It.IsAny<Visit>(), It.IsAny<IList<FinancePlanStatusEnum>>())).Returns(true);
                mock.VisitRepositoryMock.Setup(t => t.GetBySourceSystemKey(It.IsAny<String>(), It.IsAny<int>())).Returns(visitAndTransactions.Item1);
            });
            builder.CreateService().SetBalanceTransferStatusEligible(visitAndTransactions.Item1);

            Assert.AreEqual(null, visitAndTransactions.Item1.BalanceTransferStatus);
        }
        [Test]
        public void SetEligible_FinancePlanTooLong()
        {
            Tuple<Visit, IList<VisitTransaction>> visitAndTransactions = VisitFactory.GenerateActiveVisit(1234, 1);

            BalanceTransferServiceMockBuilder builder = new BalanceTransferServiceMockBuilder();
            builder.Setup(mock =>
            {
                mock.FinancePlanRepositoryMock.Setup(t => t.GetFinancePlanWithVisit(It.IsAny<Visit>())).Returns(FinancePlanFactory.CreateFinancePlanWithDuration(37, 60));
                mock.VisitRepositoryMock.Setup(t => t.IsOnFinancePlanInStatus(It.IsAny<Visit>(), It.IsAny<IList<FinancePlanStatusEnum>>())).Returns(true);
                mock.VisitRepositoryMock.Setup(t => t.GetBySourceSystemKey(It.IsAny<string>(), It.IsAny<int>())).Returns(visitAndTransactions.Item1);
            });
            builder.CreateService().SetBalanceTransferStatusEligible(visitAndTransactions.Item1);

            Assert.AreEqual(null, visitAndTransactions.Item1.BalanceTransferStatus);
        }
    }
}

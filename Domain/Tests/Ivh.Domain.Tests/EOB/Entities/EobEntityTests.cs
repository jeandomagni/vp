﻿namespace Ivh.Domain.EOB.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Utilities.Helpers;
    using HospitalData.Eob.Entities;
    using NUnit.Framework;

    [TestFixture]
    public class EobEntityTests : DomainTestBase
    {
        [Test]
        public void TransactionSetHeaderTrailer835_St02TransactionSetControlNumber_GetsTruncatedIfLong()
        {
            TransactionSetHeaderTrailer835 entity = new TransactionSetHeaderTrailer835
            {
                PayeeDetails = new PayeeIdentification
                {
                    N101EntityIdentifierCode = Guid.NewGuid().ToString()
                },
                Transactions = new List<HeaderNumber>
                {
                    new HeaderNumber
                    {
                        ClaimPaymentInformations = new List<ClaimPaymentInformation>
                        {
                            new ClaimPaymentInformation
                            {
                                Clp02ClaimStatusCode = Guid.NewGuid().ToString()
                            }
                        }
                    }
                },
                St02TransactionSetControlNumber = Guid.NewGuid().ToString()
            };
            int length = entity.St02TransactionSetControlNumber.Length;
            MaxLengthTruncator.TruncateProperties(entity);

            Assert.GreaterOrEqual(length, entity.St02TransactionSetControlNumber.Length);
            //Should have been truncated
            Assert.AreEqual(9, entity.St02TransactionSetControlNumber.Length);
            //Should have been truncated
            Assert.AreEqual(3, entity.PayeeDetails.N101EntityIdentifierCode.Length);
            Assert.AreEqual(2, entity.Transactions.FirstOrDefault().ClaimPaymentInformations.FirstOrDefault().Clp02ClaimStatusCode.Length);
        }
    }
}

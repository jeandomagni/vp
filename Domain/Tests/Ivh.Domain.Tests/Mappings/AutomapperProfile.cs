﻿namespace Ivh.Domain.Mappings
{
    using System.Linq;
    using AutoMapper;
    using FinanceManagement.FinancePlan.Entities;
    using FinanceManagement.Payment.Entities;
    using FinanceManagement.RoboRefund.Entities;
    using Visit.Entities;

    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            this.CreateMap<Visit, PaymentVisit>()
                .ForMember(dest => dest.CurrentVisitState, opts => opts.MapFrom(f => f.CurrentVisitState))
                .ForMember(dest => dest.DiscountPercentage, opts => opts.MapFrom(f =>(f.VisitInsurancePlans != null && f.VisitInsurancePlans.Count > 0) ?f.VisitInsurancePlans.FirstOrDefault().InsurancePlan.DiscountPercentage ?? 0:0));

            this.CreateMap<FinancePlanVisit, PaymentFinancePlanVisit>()
                .ForMember(dest => dest.VisitId, opts => opts.MapFrom(f => f.VisitId))
                .ForMember(dest => dest.HardRemoveDate, opts => opts.MapFrom(f => f.HardRemoveDate))
                .ForMember(dest => dest.FinancePlanId, opts => opts.MapFrom(f => f.FinancePlan.FinancePlanId));

            this.CreateMap<FinancePlan, PaymentFinancePlan>()
                .ForMember(dest => dest.FinancePlanStatus, opts => opts.MapFrom(f => f.FinancePlanStatus.FinancePlanStatusEnum));

            this.CreateMap<FinancePlanVisitPrincipalAmount, RoboRefundEventResult>()
                    .ForMember(dest => dest.VisitId, opts => opts.MapFrom(src => src.FinancePlanVisit.VisitId))
                    .ForMember(dest => dest.FinancePlanVisitId, opts => opts.MapFrom(src => src.FinancePlanVisit.FinancePlanVisitId))
                    .ForMember(dest => dest.FinancePlanVisitPrincipalAmountInsertDate, opts => opts.MapFrom(src => src.InsertDate))
                    .ForMember(dest => dest.FinancePlanVisitPrincipalAmountOverrideInsertDate, opts => opts.MapFrom(src => src.OverrideInsertDate))
                    .ForMember(dest => dest.VpGuarantorId, opts => opts.MapFrom(src => src.FinancePlanVisit.VpGuarantorId));
        }
    }
}

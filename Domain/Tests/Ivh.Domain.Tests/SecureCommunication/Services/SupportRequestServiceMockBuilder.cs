﻿namespace Ivh.Domain.SecureCommunication.Services
{
    using Base.Interfaces;
    using Common.Tests.Helpers;
    using Common.Tests.Helpers.Base;
    using GuarantorSupportRequests.Interfaces;
    using GuarantorSupportRequests.Services;
    using Moq;
    using Settings.Interfaces;
    using System;
    using User.Interfaces;
    using Visit.Interfaces;

    public class SupportRequestServiceMockBuilder : IMockBuilder<SupportRequestService, SupportRequestServiceMockBuilder>
    {
        public IDomainServiceCommonService DomainServiceCommonService;
        public ISupportRequestRepository SupportRequestRepository;
        public ISupportRequestMessageRepository SupportRequestMessageRepository;
        public IVisitPayUserRepository VisitPayUserRepository;
        public IVisitRepository VisitRepository;

        public Mock<IApplicationSettingsService> ApplicationSettingsServiceMock;
        public Mock<ISupportRequestRepository> SupportRequestRepositoryMock;
        public Mock<ISupportRequestMessageRepository> SupportRequestMessageRepositoryMock;
        public Mock<IVisitPayUserRepository> VisitPayUserRepositoryMock;
        public Mock<IVisitRepository> VisitRepositoryMock;

        public SupportRequestServiceMockBuilder()
        {
            this.ApplicationSettingsServiceMock = new Mock<IApplicationSettingsService>();
            this.VisitPayUserRepositoryMock = new Mock<IVisitPayUserRepository>();
            this.VisitRepositoryMock = new Mock<IVisitRepository>();
            this.SupportRequestRepositoryMock = new Mock<ISupportRequestRepository>();
            this.SupportRequestMessageRepositoryMock = new Mock<ISupportRequestMessageRepository>();

            this.DomainServiceCommonService = DomainServiceCommonServices.CreateDomainServiceCommonService(this.ApplicationSettingsServiceMock);
        }

        public SupportRequestService CreateService()
        {
            return new SupportRequestService(
                new Lazy<IDomainServiceCommonService>(() => this.DomainServiceCommonService),
                new Lazy<ISupportRequestRepository>(() => this.SupportRequestRepository ?? this.SupportRequestRepositoryMock.Object),
                new Lazy<ISupportRequestMessageRepository>(() => this.SupportRequestMessageRepository ?? this.SupportRequestMessageRepositoryMock.Object),
                new Lazy<IVisitPayUserRepository>(() => this.VisitPayUserRepository ?? this.VisitPayUserRepositoryMock.Object),
                new Lazy<IVisitRepository>(() => this.VisitRepository ?? this.VisitRepositoryMock.Object));
        }

        public SupportRequestServiceMockBuilder Setup(Action<SupportRequestServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
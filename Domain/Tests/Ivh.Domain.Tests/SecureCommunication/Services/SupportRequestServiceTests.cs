﻿namespace Ivh.Domain.SecureCommunication.Services
{
    using Common.VisitPay.Enums;
    using Guarantor.Entities;
    using GuarantorSupportRequests.Entities;
    using GuarantorSupportRequests.Interfaces;
    using GuarantorSupportRequests.Services;
    using Moq;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Utilities.Extensions;
    using Common.Tests.Builders;
    using User.Entities;
    using Visit.Entities;
    using Visit.Interfaces;

    [TestFixture]
    public class SupportRequestServiceTests
    {
        private SupportRequestServiceMockBuilder _supportRequestServiceMockBuilder;
        private Guarantor _fakeGuarantor;

        [SetUp]
        public void BeforeEach()
        {
            this._supportRequestServiceMockBuilder = new SupportRequestServiceMockBuilder();
            this._fakeGuarantor = GenerateFakeGuarantor();
        }

        [Test]
        public void GetSupportRequests_FilterByFacilityNotAssociatedWithGuarantor_ZeroResults()
        {
            List<SupportRequest> allRequests = new List<SupportRequest>();
            allRequests.AddRange(this.GetFakeSupportRequestsWithSingleFacility());
            allRequests.AddRange(this.GetFakeSupportRequestsWithNoFacilities());

            SupportRequestResults fakeResultsToReturn = new SupportRequestResults
            {
                SupportRequests = allRequests
            };

            IEnumerable<Visit> fakeVisitsToReturn = this.GetFakeVisitsIncludingFacilityData();

            const string facilitySourceSystemKeyNotAssociatedWithGuarantor = "AAAA";
            SupportRequestFilter filter = new SupportRequestFilter
            {
                FacilitySourceSystemKey = facilitySourceSystemKeyNotAssociatedWithGuarantor,
                SortField = nameof(SupportRequest.FacilitySourceSystemKeys)
            };

            SupportRequestService svc =
                this.CreateSupportRequestService(null, null, fakeVisitsToReturn, fakeResultsToReturn);

            SupportRequestResults results = svc.GetSupportRequests(this._fakeGuarantor, filter, 1, 1);

            Assert.AreEqual(0, results.TotalRecords);
            Assert.IsEmpty(results.SupportRequests);
        }

        [Test]
        public void GetSupportRequests_FilterByFacility_ReturnsFilteredResults()
        {
            List<SupportRequest> allRequests = new List<SupportRequest>();
            allRequests.AddRange(this.GetFakeSupportRequestsWithSingleFacility());
            allRequests.AddRange(this.GetFakeSupportRequestsWithNoFacilities());

            SupportRequestResults fakeResultsToReturn = new SupportRequestResults
            {
                SupportRequests = allRequests
            };

            IEnumerable<Visit> fakeVisitsToReturn = this.GetFakeVisitsIncludingFacilityData();

            const string facilitySourceSystemKeyAssociatedWithGuarantor = "IJKL";
            SupportRequestFilter filter = new SupportRequestFilter
            {
                FacilitySourceSystemKey = facilitySourceSystemKeyAssociatedWithGuarantor,
                SortField = nameof(SupportRequest.FacilitySourceSystemKeys)
            };

            SupportRequestService svc =
                this.CreateSupportRequestService(null, null, fakeVisitsToReturn, fakeResultsToReturn);

            SupportRequestResults results = svc.GetSupportRequests(this._fakeGuarantor, filter, 1, 1);
            List<string> actualFacilitiesForAllRequests = results.SupportRequests.SelectMany(r => r.FacilitySourceSystemKeys).Distinct().ToList();

            // expect 2 filtered results, because 2 support requests have no visits/facilities assigned,
            // and therefore get the list of distinct facilities from all the guarantor's active visits
            Assert.AreEqual(2, results.TotalRecords);
            Assert.Contains(facilitySourceSystemKeyAssociatedWithGuarantor, actualFacilitiesForAllRequests);
        }

        [Test]
        public void GetSupportRequests_FilterByFacilitySortByFacilityAsc_ReturnsFilteredResults()
        {
            const string facilitySourceSystemKeyAssociatedWithGuarantor = "IJKL";

            List<SupportRequest> fakeSupportRequestsWithSingleFacility = this.GetFakeSupportRequestsWithSingleFacility().ToList();
            List<SupportRequest> fakeSupportRequestsWithNoFacilities = this.GetFakeSupportRequestsWithNoFacilities().ToList();

            SupportRequest supportRequestWithVisitAndFacilityAssigned = new SupportRequest
            {
                SupportRequestStatus = SupportRequestStatusEnum.Open,
                VpGuarantor = this._fakeGuarantor,
                InsertDate = new DateTime(),
                Visit = new Visit
                {
                    Facility = this.GetFakeFacility(facilitySourceSystemKeyAssociatedWithGuarantor),
                    CurrentVisitState = VisitStateEnum.Active
                },
                FacilitySourceSystemKeys = null
            };

            int supportRequestsAfterAddingFacilitiesAndFilteringExpectedCount =
                fakeSupportRequestsWithNoFacilities.Union(supportRequestWithVisitAndFacilityAssigned.ToListOfOne()).Count();

            List<SupportRequest> allRequests = new List<SupportRequest>();
            allRequests.AddRange(fakeSupportRequestsWithSingleFacility);
            allRequests.AddRange(fakeSupportRequestsWithNoFacilities);
            
            allRequests.Add(supportRequestWithVisitAndFacilityAssigned);

            SupportRequestResults fakeResultsToReturn = new SupportRequestResults
            {
                SupportRequests = allRequests
            };

            IEnumerable<Visit> fakeVisitsToReturn = this.GetFakeVisitsIncludingFacilityData();

            SupportRequestFilter filter = new SupportRequestFilter
            {
                FacilitySourceSystemKey = facilitySourceSystemKeyAssociatedWithGuarantor,
                SortField = nameof(SupportRequest.FacilitySourceSystemKeys),
                SortOrder = "asc"
            };

            SupportRequestService svc =
                this.CreateSupportRequestService(null, null, fakeVisitsToReturn, fakeResultsToReturn);

            SupportRequestResults results = svc.GetSupportRequests(this._fakeGuarantor, filter, 0, 1);
            List<string> actualFacilitiesForAllRequests = results.SupportRequests.SelectMany(r => r.FacilitySourceSystemKeys).Distinct().ToList();

            Assert.AreEqual(supportRequestsAfterAddingFacilitiesAndFilteringExpectedCount, results.TotalRecords);
            Assert.Contains(facilitySourceSystemKeyAssociatedWithGuarantor, actualFacilitiesForAllRequests);
            Assert.AreEqual(3, results.SupportRequests.Last().FacilitySourceSystemKeys.Count);
            Assert.AreEqual("EFGH", results.SupportRequests.Last().FacilitySourceSystemKeys.First());
        }

        [Test]
        public void GetSupportRequests_FilterByFacilitySortByFacilityDesc_ReturnsFilteredResults()
        {
            const string facilitySourceSystemKeyAssociatedWithGuarantor = "IJKL";

            List<SupportRequest> fakeSupportRequestsWithSingleFacility = this.GetFakeSupportRequestsWithSingleFacility().ToList();
            List<SupportRequest> fakeSupportRequestsWithNoFacilities = this.GetFakeSupportRequestsWithNoFacilities().ToList();

            SupportRequest supportRequestWithVisitAndFacilityAssigned = new SupportRequest
            {
                SupportRequestStatus = SupportRequestStatusEnum.Open,
                VpGuarantor = this._fakeGuarantor,
                InsertDate = new DateTime(),
                Visit = new Visit
                {
                    Facility = this.GetFakeFacility(facilitySourceSystemKeyAssociatedWithGuarantor),
                    CurrentVisitState = VisitStateEnum.Active
                },
                FacilitySourceSystemKeys = null
            };

            int supportRequestsAfterAddingFacilitiesAndFilteringExpectedCount =
                fakeSupportRequestsWithNoFacilities.Union(supportRequestWithVisitAndFacilityAssigned.ToListOfOne()).Count();

            List<SupportRequest> allRequests = new List<SupportRequest>();
            allRequests.AddRange(fakeSupportRequestsWithSingleFacility);
            allRequests.AddRange(fakeSupportRequestsWithNoFacilities);

            allRequests.Add(supportRequestWithVisitAndFacilityAssigned);

            SupportRequestResults fakeResultsToReturn = new SupportRequestResults
            {
                SupportRequests = allRequests
            };

            IEnumerable<Visit> fakeVisitsToReturn = this.GetFakeVisitsIncludingFacilityData();

            SupportRequestFilter filter = new SupportRequestFilter
            {
                FacilitySourceSystemKey = facilitySourceSystemKeyAssociatedWithGuarantor,
                SortField = nameof(SupportRequest.FacilitySourceSystemKeys),
                SortOrder = "desc"
            };

            SupportRequestService svc =
                this.CreateSupportRequestService(null, null, fakeVisitsToReturn, fakeResultsToReturn);

            SupportRequestResults results = svc.GetSupportRequests(this._fakeGuarantor, filter, 0, 1);
            List<string> actualFacilitiesForAllRequests = results.SupportRequests.SelectMany(r => r.FacilitySourceSystemKeys).Distinct().ToList();

            Assert.AreEqual(supportRequestsAfterAddingFacilitiesAndFilteringExpectedCount, results.TotalRecords);
            Assert.Contains(facilitySourceSystemKeyAssociatedWithGuarantor, actualFacilitiesForAllRequests);
            Assert.AreEqual(1, results.SupportRequests.First().FacilitySourceSystemKeys.Count);
            Assert.AreEqual(facilitySourceSystemKeyAssociatedWithGuarantor, results.SupportRequests.First().FacilitySourceSystemKeys.First());
        }

        [Test]
        public void GetSupportRequests_VisitWithFacilityOnSupportRequest_FacilitySourceSystemKeysAssigned()
        {
            const string expectedFacilitySourceSystemKey = "WXYZ";
            Visit fakeVisit = new Visit { Facility = this.GetFakeFacility(expectedFacilitySourceSystemKey) };

            SupportRequestResults supportRequestResults = this.GetFakeSupportRequestResults(fakeVisit, this._fakeGuarantor);

            SupportRequestService svc =
                this.CreateSupportRequestService(fakeVisit, null, null, supportRequestResults);

            SupportRequestResults results = svc.GetSupportRequests(this._fakeGuarantor, new SupportRequestFilter(), 1, 1);

            Assert.AreEqual(supportRequestResults.SupportRequests.Count, results.TotalRecords);
            Assert.AreEqual(fakeVisit.Facility.SourceSystemKey, results.SupportRequests.First().FacilitySourceSystemKeys.First());
        }

        [Test]
        public void GetSupportRequests_VisitWithNoFacilityOnSupportRequestAndActiveVisitWithFacility_FacilitySourceSystemKeysAssigned()
        {
            const string expectedFacilitySourceSystemKey = "ABCD";

            Visit fakeVisit = new Visit
            {
                Facility = null
            };

            IEnumerable<Visit> fakeVisits = new List<Visit>
            {
                new Visit
                {
                    Facility = this.GetFakeFacility(expectedFacilitySourceSystemKey),
                    CurrentVisitState = VisitStateEnum.Active
                }
            };

            SupportRequestResults fakeSupportRequestResults = this.GetFakeSupportRequestResults(fakeVisit, this._fakeGuarantor);

            SupportRequestService svc =
                this.CreateSupportRequestService(fakeVisit, null, fakeVisits, fakeSupportRequestResults);

            SupportRequestResults results = svc.GetSupportRequests(this._fakeGuarantor, new SupportRequestFilter(), 1, 1);

            Assert.AreEqual(fakeSupportRequestResults.SupportRequests.Count, results.TotalRecords);
            Assert.NotNull(results.SupportRequests.FirstOrDefault());
            Assert.IsNotEmpty(results.SupportRequests.First().FacilitySourceSystemKeys);
            Assert.AreEqual(expectedFacilitySourceSystemKey, results.SupportRequests.First().FacilitySourceSystemKeys.First());
        }

        [Test]
        public void GetSupportRequests_NoVisitOnSupportRequestAndActiveVisitWithFacility_FacilitySourceSystemKeysAssigned()
        {
            const string expectedFacilitySourceSystemKey = "AAAA";

            IEnumerable<Visit> fakeVisits = new List<Visit>
            {
                new Visit
                {
                    Facility = this.GetFakeFacility(expectedFacilitySourceSystemKey),
                    CurrentVisitState = VisitStateEnum.Active
                }
            };

            SupportRequestResults fakeSupportRequestResults = this.GetFakeSupportRequestResults(null, this._fakeGuarantor);

            SupportRequestService svc =
                this.CreateSupportRequestService(null, null, fakeVisits, fakeSupportRequestResults);

            SupportRequestResults results = svc.GetSupportRequests(this._fakeGuarantor, new SupportRequestFilter(), 1, 1);

            Assert.AreEqual(fakeSupportRequestResults.SupportRequests.Count, results.TotalRecords);
            Assert.NotNull(results.SupportRequests.FirstOrDefault());
            Assert.IsNotEmpty(results.SupportRequests.First().FacilitySourceSystemKeys);
            Assert.AreEqual(expectedFacilitySourceSystemKey, results.SupportRequests.First().FacilitySourceSystemKeys.First());
        }

        [Test]
        public void GetSupportRequests_NoVisitOnSupportRequestAndActiveVisitWithNoFacility_NoFacilitySourceSystemKeysAssigned()
        {
            IEnumerable<Visit> fakeVisits = new List<Visit>
            {
                new Visit
                {
                    Facility = null,
                    CurrentVisitState = VisitStateEnum.Active
                }
            };

            SupportRequestResults fakeSupportRequestResults = this.GetFakeSupportRequestResults(null, this._fakeGuarantor);

            SupportRequestService svc =
                this.CreateSupportRequestService(null, null, fakeVisits, fakeSupportRequestResults);

            SupportRequestResults results = svc.GetSupportRequests(this._fakeGuarantor, new SupportRequestFilter(), 1, 1);

            Assert.AreEqual(fakeSupportRequestResults.SupportRequests.Count, results.TotalRecords);
            Assert.NotNull(results.SupportRequests.FirstOrDefault());
            Assert.IsEmpty(results.SupportRequests.First().FacilitySourceSystemKeys);
            Assert.NotNull(results.SupportRequests.First().FacilitySourceSystemKeys);
        }

        private SupportRequestResults GetFakeSupportRequestResults(Visit visit, Guarantor guarantor)
        {
            List<SupportRequest> supportRequests = new List<SupportRequest>
            {
                new SupportRequest
                {
                    SupportRequestStatus = SupportRequestStatusEnum.Open,
                    VpGuarantor = guarantor,
                    InsertDate = new DateTime(),
                    Visit = visit,
                    FacilitySourceSystemKeys = null
                },
                new SupportRequest
                {
                    SupportRequestStatus = SupportRequestStatusEnum.Open,
                    VpGuarantor = guarantor,
                    InsertDate = new DateTime(),
                    Visit = visit,
                    FacilitySourceSystemKeys = null
                }
            };

            SupportRequestResults results = new SupportRequestResults
            {
                SupportRequests = supportRequests,
                TotalRecords = supportRequests.Count
            };

            return results;
        }

        private SupportRequestService CreateSupportRequestService(
            Visit fakeVisit,
            Guarantor fakeGuarantor,
            IEnumerable<Visit> fakeVisitsToReturn,
            SupportRequestResults fakeSupportRequestResults)
        {
            if (fakeGuarantor == null)
            {
                fakeGuarantor = this._fakeGuarantor;
            }

            fakeSupportRequestResults = fakeSupportRequestResults ?? this.GetFakeSupportRequestResults(fakeVisit, fakeGuarantor);

            Mock<IVisitRepository> visitRepoMock = new Mock<IVisitRepository>();
            Mock<ISupportRequestRepository> supportRequestRepoMock = new Mock<ISupportRequestRepository>();

            visitRepoMock
                .Setup(r => r.GetAllActiveVisitsForVpGuarantor(It.IsAny<int>()))
                .Returns(fakeVisitsToReturn.ToList);

            supportRequestRepoMock
                .Setup(r => r.GetSupportRequests(
                    this._fakeGuarantor.VpGuarantorId, It.IsAny<SupportRequestFilter>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(fakeSupportRequestResults);

            this._supportRequestServiceMockBuilder.Setup(builder =>
            {
                builder.SupportRequestRepository = supportRequestRepoMock.Object;
                builder.VisitRepository = visitRepoMock.Object;
            });

            SupportRequestService supportRequestService = this._supportRequestServiceMockBuilder.CreateService();

            return supportRequestService;
        }

        private Facility GetFakeFacility(string sourceSystemKey)
        {
            return new Facility
            {
                FacilityId = 12345,
                LogoFilename = "sample.jpg",
                FacilityDescription = "Medical Center",
                RicState = new State { StateCode = "CO", CmsRegionId = 2},
                SourceSystemKey = sourceSystemKey
            };
        }

        private IEnumerable<Visit> GetFakeVisitsIncludingFacilityData()
        {
            return new List<Visit>
            {
                new Visit
                {
                    Facility = new Facility
                    {
                        SourceSystemKey = "EFGH"
                    },
                    CurrentVisitState = VisitStateEnum.Active
                },
                new Visit
                {
                    Facility = new Facility
                    {
                        SourceSystemKey = "IJKL"
                    },
                    CurrentVisitState = VisitStateEnum.Active
                },
                new Visit
                {
                    Facility = new Facility
                    {
                        SourceSystemKey = "MNOP"
                    },
                    CurrentVisitState = VisitStateEnum.Active
                }
            };
        }

        private IEnumerable<SupportRequest> GetFakeSupportRequestsWithNoFacilities()
        {
            return new List<SupportRequest>
            {
                new SupportRequest
                {
                    SupportRequestStatus = SupportRequestStatusEnum.Open,
                    VpGuarantor = this._fakeGuarantor,
                    InsertDate = new DateTime(),
                    Visit = null
                },
                new SupportRequest
                {
                    SupportRequestStatus = SupportRequestStatusEnum.Open,
                    VpGuarantor = this._fakeGuarantor,
                    InsertDate = new DateTime(),
                    Visit = null
                }
            };
        }

        private IEnumerable<SupportRequest> GetFakeSupportRequestsWithSingleFacility()
        {
            return new List<SupportRequest>
            {
                new SupportRequest
                {
                    SupportRequestStatus = SupportRequestStatusEnum.Open,
                    VpGuarantor = this._fakeGuarantor,
                    InsertDate = new DateTime(),
                    Visit = new Visit { Facility = this.GetFakeFacility("CCCC")}
                },
                new SupportRequest
                {
                    SupportRequestStatus = SupportRequestStatusEnum.Open,
                    VpGuarantor = this._fakeGuarantor,
                    InsertDate = new DateTime(),
                    Visit = new Visit { Facility = this.GetFakeFacility("DDDD")}
                }
            };
        }

        private static Guarantor GenerateFakeGuarantor()
        {
            return new GuarantorBuilder()
                .WithDefaults(54321)
                .AsOnline()
                .WithInitialStatus(VpGuarantorStatusEnum.Active);
            /*{
                VpGuarantorId = 54321,
                User = new VisitPayUser(),
                TermsOfUseCmsVersionId = 0,
                PaymentDueDay = 0,
                NextStatementDate = null,
                RegistrationDate = new DateTime(),
                VpGuarantorTypeEnum = GuarantorTypeEnum.Online,
                HsGuarantorMaps = null,
                Cancellations = null,
                MostRecentVpStatementId = null,
                ManagingConsolidationGuarantors = null,
                ManagedConsolidationGuarantors = null,
                AccountClosedDate = null,
                AccountCancellationDate = null,
                AccountReactivationDate = null,
                PaymentDueDayHistories = null,
                VpGuarantorStatus = VpGuarantorStatusEnum.Active,
                ChangeStateToActive = false,
                ChangeStateToClosed = false,
                VpGuarantorStateHistories = null
            };*/
        }
    }
}

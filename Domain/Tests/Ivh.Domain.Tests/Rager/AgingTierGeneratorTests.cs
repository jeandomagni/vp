﻿namespace Ivh.Domain.Rager
{
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.Tests.Builders.Rager;
    using Common.Tests.Helpers.Rager;
    using Common.VisitPay.Enums;
    using Entities;
    using NUnit.Framework;

    [TestFixture]
    public class AgingTierGeneratorTests : DomainTestBase
    {
        [Test]
        //TODO: this is a WIP, need to add test cases
        public void AgingTierGenerator_Tests_good()
        {
            IList<AgingTierConfiguration> agingTierConfigurations = AgingTierConfigurationFactory.AgingTierConfigurations();
            Dictionary<AgingCommunicationTypeEnum, int> communications = new Dictionary<AgingCommunicationTypeEnum, int>
            {
                [AgingCommunicationTypeEnum.PastDueNotification] = 0,
                [AgingCommunicationTypeEnum.StatementNotification] = 1
            };
            Visit visit = new VisitBuilder().WithDefaultValues().WithCommunications(communications).Build();
            AgingTierGenerator agingTierGenerator = new AgingTierGenerator(agingTierConfigurations, visit, 31);
            AgingTierConfiguration agingTierConfiguration = agingTierGenerator.CalculateAgingTier();
            Assert.AreEqual(AgingTierEnum.Good, agingTierConfiguration.ResultingAgingTier.AgingTierEnum);
        }
    }
}
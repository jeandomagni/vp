﻿namespace Ivh.Domain.Features
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using Common.Base.Enums;
    using Common.Tests.Helpers.Features;
    using Common.VisitPay.Enums;
    using Moq;
    using NUnit.Framework;
    using Settings.Entities;
    using Settings.Interfaces;

    [TestFixture]
    public class FeatureServiceTests : DomainTestBase
    {
        [Test]
        public void FeatureService_DemoFeatureRespectsEnvironment()
        {
            // check valid
            IList<IvhEnvironmentEnum> validEnvironments = new List<IvhEnvironmentEnum>
            {
                IvhEnvironmentEnum.Development,
                IvhEnvironmentEnum.Dev,
                IvhEnvironmentEnum.Demo,
                IvhEnvironmentEnum.demo01,
                IvhEnvironmentEnum.demo02,
                IvhEnvironmentEnum.demo03,
                IvhEnvironmentEnum.demo04,
                IvhEnvironmentEnum.Test,
                IvhEnvironmentEnum.test01,
                IvhEnvironmentEnum.test02,
            };
            foreach (IvhEnvironmentEnum validEnvironment in validEnvironments)
            {
                bool isFeatureEnabled = this.CheckFeature(validEnvironment.ToString(), x => x.FeatureDemoPreServiceUiIsEnabled, VisitPayFeatureEnum.DemoPreServiceUiIsEnabled);
                Assert.IsTrue(isFeatureEnabled, "demo feature is disabled for " + validEnvironment);
            }

            // check invalid
            IList<IvhEnvironmentEnum> invalidEnvironments = Enum.GetValues(typeof(IvhEnvironmentEnum)).Cast<IvhEnvironmentEnum>().Where(x => !validEnvironments.Contains(x)).ToList();
            foreach (IvhEnvironmentEnum invalidEnvironment in invalidEnvironments)
            {
                bool isFeatureEnabled = this.CheckFeature(invalidEnvironment.ToString(), x => x.FeatureDemoPreServiceUiIsEnabled, VisitPayFeatureEnum.DemoPreServiceUiIsEnabled);
                Assert.IsFalse(isFeatureEnabled, "demo feature is enabled for " + invalidEnvironment);
            }
        }

        [Test]
        public void FeatureService_UsabilityFeatureRespectsEnvironment()
        {
            // check valid
            IList<string> validEnvironments = new List<string>
            {
                "usabil01",
                IvhEnvironmentEnum.Development.ToString(),
                IvhEnvironmentEnum.Dev.ToString(),
                IvhEnvironmentEnum.Test.ToString(),
                IvhEnvironmentEnum.test01.ToString(),
                IvhEnvironmentEnum.test02.ToString(),
            };

            foreach (string validEnvironment in validEnvironments)
            {
                bool isFeatureEnabled = this.CheckFeature(validEnvironment, x => x.FeatureDebrandedUiIsEnabled, VisitPayFeatureEnum.FeatureDebrandedUiIsEnabled);
                Assert.IsTrue(isFeatureEnabled, "usability feature is disabled for " + validEnvironment);
            }

            // check invalid
            IList<IvhEnvironmentEnum> invalidEnvironments = Enum.GetValues(typeof(IvhEnvironmentEnum)).Cast<IvhEnvironmentEnum>().Where(x => !validEnvironments.Contains(x.ToString())).ToList();
            foreach (IvhEnvironmentEnum invalidEnvironment in invalidEnvironments)
            {
                bool isFeatureEnabled = this.CheckFeature(invalidEnvironment.ToString(), x => x.FeatureDebrandedUiIsEnabled, VisitPayFeatureEnum.FeatureDebrandedUiIsEnabled);
                Assert.IsFalse(isFeatureEnabled, "usability feature is enabled for " + invalidEnvironment);
            }
        }

        private bool CheckFeature(string currentEnvironment, Expression<Func<Client,bool>> clientSetting, VisitPayFeatureEnum featureEnum)
        {
            IFeatureService svc = new FeatureServiceMockBuilder().Setup(builder =>
            {
                builder.ApplicationSettingsServiceMock.Setup(x => x.Environment).Returns(() =>
                {
                    Mock<IApplicationSetting<string>> currentEnvironmentMock = new Mock<IApplicationSetting<string>>();
                    currentEnvironmentMock.Setup(x => x.Value).Returns(() => currentEnvironment);

                    return currentEnvironmentMock.Object;
                });
                builder.ClientServiceMock.Setup(x => x.GetClient()).Returns(() =>
                {
                    Mock<Client> clientMock = new Mock<Client>();
                    clientMock.Setup(clientSetting).Returns(() => true);

                    return clientMock.Object;
                });
            }).CreateService();

            return svc.IsFeatureEnabled(featureEnum);
        }


    }
}
﻿// ReSharper disable LocalizableElement

namespace Ivh.Domain.Statement.Services
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Utilities.Extensions;
    using Common.Tests.Helpers.VpStatement;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using FinanceManagement.Statement.Entities;
    using FinanceManagement.Statement.Interfaces;
    using Guarantor.Entities;
    using NUnit.Framework;

    [TestFixture]
    public class StatementDateServiceTests
    {
        private const int TwentyOne = 21;

        [Test]
        public void GetNewPaymentDateForPaymentDueDayChange_AwaitingStatement()
        {
            DateTime now = DateTime.UtcNow.Date;

            // in awaiting statement
            DateTime paymentDueDate = now.AddDays(-1);

            // the statement exists before the payment due date changes
            VpStatement statement = new VpStatement
            {
                PaymentDueDate = paymentDueDate,
                StatementDate = paymentDueDate.AddDays(-21)
            };

            Console.WriteLine($"original payment due date: {statement.PaymentDueDate.ToShortDateString()}");
            Console.WriteLine("===");
            
            IStatementDateService svc = new StatementDateServiceMockBuilder().CreateService();
            
            int daysInMonth = DateTime.DaysInMonth(now.Year, now.Month);
            IEnumerable<int> validPaymentDueDays = Enumerable.Range(1, daysInMonth).Where(day => day <= DateConstants.LatestValidPaymentDueDay || day == daysInMonth);

            foreach (int paymentDueDay in validPaymentDueDays)
            {
                Guarantor guarantor = new Guarantor
                {
                    PaymentDueDay = paymentDueDay
                };

                DateTime newPaymentDueDate = svc.GetNewPaymentDateForPaymentDueDayChange(guarantor, statement);

                Console.WriteLine($"change to: {guarantor.PaymentDueDay}, pdd is: {newPaymentDueDate.Date.ToShortDateString()}");

                // pdd should not change
                Assert.AreEqual(statement.PaymentDueDate, statement.PaymentDueDate);
            }
        }

        [Test]
        public void GetNewPaymentDateForPaymentDueDayChange_InGracePeriod()
        {
            DateTime now = DateTime.UtcNow.Date;

            // in grace period
            DateTime paymentDueDate = now.AddDays(3);

            // the statement exists before the payment due date changes
            VpStatement statement = new VpStatement
            {
                PaymentDueDate = paymentDueDate,
                StatementDate = paymentDueDate.AddDays(-21)
            };

            Console.WriteLine($"original payment due date: {statement.PaymentDueDate.ToShortDateString()}");
            Console.WriteLine("===");

            IStatementDateService svc = new StatementDateServiceMockBuilder().CreateService();
            
            int daysInMonth = DateTime.DaysInMonth(now.Year, now.Month);
            IEnumerable<int> validPaymentDueDays = Enumerable.Range(1, daysInMonth).Where(day => day <= DateConstants.LatestValidPaymentDueDay || day == daysInMonth);

            foreach (int paymentDueDay in validPaymentDueDays)
            {
                Guarantor guarantor = new Guarantor
                {
                    PaymentDueDay = paymentDueDay
                };

                DateTime newPaymentDueDate = svc.GetNewPaymentDateForPaymentDueDayChange(guarantor, statement);

                Console.WriteLine($"change to: {guarantor.PaymentDueDay}, pdd is: {newPaymentDueDate.Date.ToShortDateString()}");
                
                if (!statement.IsGracePeriod || guarantor.PaymentDueDay == statement.PaymentDueDate.Day)
                {
                    // pdd should not change
                    Assert.AreEqual(statement.PaymentDueDate, newPaymentDueDate);
                    Assert.AreEqual(newPaymentDueDate.Date, statement.PaymentDueDate.Date);
                }
                else
                {
                    // pdd should push forward
                    Assert.AreEqual(guarantor.PaymentDueDay.ToValidPaymentDueDay(), newPaymentDueDate.Day.ToValidPaymentDueDay());

                    // pdd should be at least 21 days ahead of the statement date
                    Assert.IsTrue(newPaymentDueDate.Date.AddDays(-TwentyOne).Date > statement.StatementDate);

                    // pdd should not be more than 1 month ahead (ex: 1/15 should not turn into 3/x)
                    Assert.AreEqual(newPaymentDueDate.Month, statement.PaymentDueDate.Month, 1);
                }
            }
        }
        
        public static IEnumerable<int> EachValidDayOfTheMonth
        {
            get
            {
                for (int i = 1; i <= DateConstants.LatestValidPaymentDueDay; i++)
                {
                    yield return i;
                }

                yield return DateConstants.EndOfMonthDay;
            }
        }

        [TestCaseSource(nameof(EachValidDayOfTheMonth))]
        public void GetDates_WithPreviousStatement(int paymentDueDay)
        {
            IStatementDateService svc = new StatementDateServiceMockBuilder().CreateService();

            Guarantor guarantor = new Guarantor
            {
                PaymentDueDay = paymentDueDay
            };

            DateTime firstPaymentDueDate = new DateTime(DateTime.UtcNow.Year, 1, guarantor.PaymentDueDay);
            DateTime firstStatementDate = firstPaymentDueDate.AddDays(-21);

            VpStatement statement = new VpStatement
            {
                PaymentDueDate = firstPaymentDueDate,
                StatementDate = firstStatementDate
            };
            
            Console.WriteLine($"Statement\t\tPayment");
            Console.WriteLine($"{statement.StatementDate.ToShortDateString()}\t\t{statement.PaymentDueDate.ToShortDateString()}");

            for (int i = 1; i <= 12 * 15; i++) // iterate over months
            {
                DateTime previousPaymentDueDate = statement.PaymentDueDate;
                DateTime previousStatementDate = statement.StatementDate;
                
                statement.PaymentDueDate = svc.GetNextPaymentDate(previousPaymentDueDate, guarantor, previousStatementDate);
                statement.StatementDate = svc.GetNextStatementDate(previousPaymentDueDate, guarantor, previousStatementDate);
                
                Console.WriteLine($"{statement.StatementDate.ToShortDateString()}\t\t{statement.PaymentDueDate.ToShortDateString()}");
                
                // verify we aren't skipping months
                Assert.AreEqual(previousPaymentDueDate.AddMonths(1).Month, statement.PaymentDueDate.Month);
                Assert.AreEqual(previousStatementDate.AddMonths(1).Month, statement.StatementDate.Month);
                
                // verify day remains the same
                int validPaymentDueDay = guarantor.PaymentDueDay == DateConstants.EndOfMonthDay ? DateTime.DaysInMonth(statement.PaymentDueDate.Year, statement.PaymentDueDate.Month) : guarantor.PaymentDueDay;
                Assert.AreEqual(validPaymentDueDay, statement.PaymentDueDate.Day);
            }
        }

        [Test]
        public void GetDates_ForImmediateStatement()
        {
            IStatementDateService svc = new StatementDateServiceMockBuilder().CreateService();
            
            Console.WriteLine("Registration\tStatement\t\tPayment");

            for (int year = DateTime.UtcNow.Year; year <= DateTime.UtcNow.AddYears(5).Year; year++)
            {
                for (int month = 1; month <= 12; month++)
                {
                    for (int day = 1; day <= DateTime.DaysInMonth(year, month); day++)
                    {
                        DateTime now = new DateTime(year, month, day);
                        
                        Guarantor guarantor = new Guarantor
                        {
                            // VisitPayUserApplicationService.CreatePatient does this
                            PaymentDueDay = now.AddDays(TwentyOne).Day.ToValidPaymentDueDay()
                        };

                        DateTime statementDate = svc.GetNextStatementDate(null, guarantor, now, true, true).Date;
                        DateTime paymentDueDate = svc.GetNextPaymentDate(null, guarantor, statementDate, true, true).Date;
                        
                        Console.WriteLine($"{now.ToShortDateString()}\t\t{statementDate.ToShortDateString()}\t\t{paymentDueDate.ToShortDateString()}");
                        
                        int daysInMonth = DateTime.DaysInMonth(paymentDueDate.Year, paymentDueDate.Month);
                        if (paymentDueDate.Day == daysInMonth)
                        {
                            Assert.True(paymentDueDate.Date.IsBetween(statementDate.AddDays(TwentyOne).Date, statementDate.AddDays(24).Date, true));
                        }
                        else
                        {
                            Assert.AreEqual(statementDate.AddDays(TwentyOne).Date, paymentDueDate.Date);
                        }
                    }
                }
            }
        }

        public static IEnumerable StatementDateTestCaseSource
        {
            get
            {
                yield return new TestCaseData("Due Day less than Today", new DateTime(2015, 5, 10), 31, new DateTime(2015, 5, 31), new DateTime(2015, 6, 9));
                yield return new TestCaseData("Due Day greater than Today", new DateTime(2015, 5, 23), 13, new DateTime(2015, 6, 13), new DateTime(2015, 6, 22));
            }
        }

        [TestCaseSource(nameof(StatementDateTestCaseSource))]
        [Test]
        public void GenerateDatesWithNoPreviousStatement(string testCaseName, DateTime dateToProcess, int paymentDueDay, DateTime expectedPaymentDueDate, DateTime expectedNextStatementDate)
        {
            StatementDateServiceMockBuilder builder = new StatementDateServiceMockBuilder();
            IStatementDateService svc = builder.CreateService();

            Guarantor guarantor = new Guarantor
            {
                PaymentDueDay = paymentDueDay,
                RegistrationDate = dateToProcess
            };

            DateTime paymentDate = svc.GetNextPaymentDate(null, guarantor, dateToProcess, isFirstStatement: true, isImmediateStatement: true);
            DateTime statementDate = svc.GetNextStatementDate(null, guarantor, dateToProcess, isFirstStatement: true, isImmediateStatement: true);
            DateTime nextStatementDate = svc.GetNextStatementDate(paymentDate, guarantor, dateToProcess, isFirstStatement: true, isImmediateStatement: false);

            Assert.AreEqual(paymentDate, expectedPaymentDueDate);
            Assert.AreEqual(statementDate, dateToProcess);
            Assert.AreEqual(nextStatementDate, expectedNextStatementDate);
        }

        [Test]
        public void GeneratePaymentDateWithPreviousStatement()
        {
            StatementDateServiceMockBuilder builder = new StatementDateServiceMockBuilder();
            IStatementDateService svc = builder.CreateService();

            Guarantor guarantor = new Guarantor
            {
                // updated the payment day as the code below rolls back 21 days for statement but then adds a month...since May has 31 days and June only has 30,
                // the statement date sent into GetNextPaymentDate isn't 21 days before July 15 so it pushes the next payment to August
                PaymentDueDay = 16,
                RegistrationDate = new DateTime(2015, 5, 14)
            };
            VpStatement statement = new VpStatement
            {
                StatementDate = new DateTime(2015, 6, 15).AddDays(-21),
                PaymentDueDate = new DateTime(2015, 6, 16),
                OriginalPaymentDueDate = new DateTime(2015, 6, 16),
                VpStatementType = VpStatementTypeEnum.Standard
            };

            DateTime paymentDate = svc.GetNextPaymentDate(statement.OriginalPaymentDueDate, guarantor, statement.StatementDate.AddMonths(1), false, false);

            Assert.AreEqual(paymentDate.Year, 2015);
            Assert.AreEqual(paymentDate.Month, 7);
            Assert.AreEqual(paymentDate.Day, 16);

            paymentDate = svc.GetNextPaymentDate(statement.OriginalPaymentDueDate, guarantor, new DateTime(2015, 6, 25), false, false);

            Assert.AreEqual(paymentDate.Year, 2015);
            Assert.AreEqual(paymentDate.Month, 7);
            Assert.AreEqual(paymentDate.Day, 16);
        }

        [Test]
        public void GetNextStatementDate_handles_border_case_VPNG_18204()
        {
            // per VPNG-18204
            // Registration Date - 6/4
            // Payment due day - 25
            //  Immediate Statement Date - 6/4
            // VpGuarantor.NextStatementDate should be 7/4
            // The first hard statement should be on 7/4
            // with Payment Due Date 7/25

            StatementDateServiceMockBuilder builder = new StatementDateServiceMockBuilder();
            IStatementDateService svc = builder.CreateService();

            Guarantor guarantor = new Guarantor
            {
                PaymentDueDay = 25,
                RegistrationDate = new DateTime(2017, 6, 4),
                NextStatementDate = new DateTime(2017, 6, 24)
            };

            DateTime nextPaymentDate = svc.GetNextPaymentDate(null, guarantor, new DateTime(2017, 6, 4), isImmediateStatement: false, isFirstStatement: true);
            DateTime nextStatementDate = svc.GetNextStatementDate(null, guarantor, new DateTime(2017, 6, 4), isImmediateStatement: false, isFirstStatement: true);

            Assert.AreEqual(new DateTime(2017, 07, 25), nextPaymentDate);
            Assert.AreEqual(new DateTime(2017, 07, 04), nextStatementDate.Date);
        }

        [Test]
        public void GetNextStatementDate_handles_border_case_VPNG_16524()
        {
            //per VPNG_16524
            //Registration Date - 3/26
            //Payment due day - 5
            // Immediate Statement Date - 3/26
            //VpGuarantor.NextStatementDate should be 04/14
            //The first hard statement should be on 04/14
            //with Payment Due Date 05/05

            StatementDateServiceMockBuilder builder = new StatementDateServiceMockBuilder();
            IStatementDateService svc = builder.CreateService();

            Guarantor guarantor = new Guarantor
            {
                PaymentDueDay = 5,
                RegistrationDate = new DateTime(2017, 3, 26),
                NextStatementDate = null
            };

            DateTime nextPaymentDate = svc.GetNextPaymentDate(null, guarantor, new DateTime(2017, 3, 26), isImmediateStatement: false, isFirstStatement: true);
            DateTime nextStatementDate = svc.GetNextStatementDate(null, guarantor, new DateTime(2017, 3, 26), isImmediateStatement: false, isFirstStatement: true);

            Assert.AreEqual(new DateTime(2017, 05, 05), nextPaymentDate);
            Assert.AreEqual(new DateTime(2017, 04, 14), nextStatementDate.Date);
        }
    }
}
﻿namespace Ivh.Domain.Statement.Services
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Utilities.Extensions;
    using Common.Tests.Helpers.VpStatement;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Domain;
    using FinanceManagement.Statement.Entities;
    using FinanceManagement.Statement.Interfaces;
    using FinanceManagement.Statement.Services;
    using FinanceManagement.ValueTypes;
    using Guarantor.Entities;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class VpStatementServiceTests : DomainTestBase
    {
        public static IEnumerable<int> EachValidDayOfTheMonth
        {
            get
            {
                for (int i = 1; i <= DateConstants.LatestValidPaymentDueDay; i++)
                {
                    yield return i;
                }

                yield return DateConstants.EndOfMonthDay;
            }
        }

        [TestCaseSource(nameof(EachValidDayOfTheMonth))]
        public void ProcessPaymentDueDayChanged_FinancePlan_InGracePeriod(int paymentDueDay)
        {
            this.ProcessPaymentDueDayChanged_FinancePlan(paymentDueDay, true);
        }

        [TestCaseSource(nameof(EachValidDayOfTheMonth))]
        public void ProcessPaymentDueDayChanged_FinancePlan_AwaitingStatement(int paymentDueDay)
        {
            this.ProcessPaymentDueDayChanged_FinancePlan(paymentDueDay, false);
        }

        private void ProcessPaymentDueDayChanged_FinancePlan(int paymentDueDay, bool isGracePeriod)
        {
            // guarantor has already updated
            Guarantor guarantor = new Guarantor
            {
                PaymentDueDay = paymentDueDay,
                VpGuarantorId = 8
            };
            
            DateTime now = DateTime.UtcNow.Date.AddDays(isGracePeriod ? 0 : 1);
            int originalPaymentDueDay = now.Day.ToValidPaymentDueDay();
            if (originalPaymentDueDay == DateConstants.EndOfMonthDay)
            {
                originalPaymentDueDay = DateTime.DaysInMonth(now.Year, now.Month);
            }

            VpStatement currentStatement = new VpStatement
            {
                VpGuarantorId = guarantor.VpGuarantorId,
                PaymentDueDate = new DateTime(now.Year, now.Month, originalPaymentDueDay),
                VpStatementId = 77
            };

            FinancePlanStatement currentFpStatement = new FinancePlanStatement
            {
                VpGuarantorId = guarantor.VpGuarantorId,
                PaymentDueDate = new DateTime(now.Year, now.Month, originalPaymentDueDay),
                VpStatementId = currentStatement.VpStatementId
            };
            
            currentStatement.StatementDate = currentStatement.PaymentDueDate.AddDays(-21);
            currentFpStatement.StatementDate = currentFpStatement.PaymentDueDate.AddDays(-21);
            
            DateTime originalStatementDate = currentStatement.StatementDate;
            DateTime originalPaymentDueDate = currentStatement.PaymentDueDate;
            
            IStatementDateService statementDateService = new StatementDateServiceMockBuilder().CreateService();
            VpStatementServiceMockBuilder builder = new VpStatementServiceMockBuilder { StatementDateService = statementDateService };
            
            builder.FinancePlanServiceMock.Setup(x => x.MeetsCriteriaToChangePaymentDueDate(guarantor)).Returns(true);
            builder.FinancePlanStatementRepositoryMock.Setup(x => x.GetFinancePlanStatementByVpStatementId(guarantor.VpGuarantorId, currentStatement.VpStatementId)).Returns(currentFpStatement);
            builder.VpStatementRepositoryMock.Setup(x => x.GetMostRecentStatement(guarantor)).Returns(currentStatement);

            VpStatementService svc = builder.GetMockedVpStatementService();
            
            ProcessPaymentDueDayChangedResult result = svc.ProcessPaymentDueDayChanged(guarantor, true, 1);
            
            // it should always update, except for same the date
            if (guarantor.PaymentDueDay.ToValidPaymentDueDay() == originalPaymentDueDay.ToValidPaymentDueDay())
            {
                Assert.False(result.IsChanged);

                Assert.AreEqual(originalPaymentDueDate, currentStatement.PaymentDueDate);
                Assert.AreEqual(originalStatementDate, currentStatement.StatementDate);

                Assert.AreEqual(originalPaymentDueDate, currentFpStatement.PaymentDueDate);
                Assert.AreEqual(originalStatementDate, currentFpStatement.StatementDate);
            }
            else
            {
                Assert.True(result.IsChanged);

                Assert.AreEqual(guarantor.PaymentDueDay.ToValidPaymentDueDay(), result.NewPaymentDueDate.Day.ToValidPaymentDueDay());
                Assert.AreEqual(guarantor.PaymentDueDay.ToValidPaymentDueDay(), currentStatement.PaymentDueDate.Day.ToValidPaymentDueDay());
                Assert.AreEqual(result.NewPaymentDueDate.Date, currentStatement.PaymentDueDate.Date);
                
                Assert.AreEqual(guarantor.PaymentDueDay.ToValidPaymentDueDay(), currentFpStatement.PaymentDueDate.Day.ToValidPaymentDueDay());
                Assert.AreEqual(result.NewPaymentDueDate.Date, currentFpStatement.PaymentDueDate.Date);
            }

            // verify next statement will generate with appropriate dates
            DateTime nextStatementDate = statementDateService.GetNextStatementDate(currentStatement.PaymentDueDate, guarantor, currentStatement.StatementDate);
            DateTime nextStatementPaymentDate = statementDateService.GetNextPaymentDate(currentStatement.PaymentDueDate, guarantor, nextStatementDate);
            
            Assert.AreEqual(guarantor.PaymentDueDay.ToValidPaymentDueDay(), nextStatementPaymentDate.Day.ToValidPaymentDueDay());
        }

        [TestCaseSource(nameof(EachValidDayOfTheMonth))]
        public void ProcessPaymentDueDayChanged_NotFinancePlan_InGracePeriod(int paymentDueDay)
        {
            this.ProcessPaymentDueDayChanged_NotFinancePlan(paymentDueDay, true);
        }

        [TestCaseSource(nameof(EachValidDayOfTheMonth))]
        public void ProcessPaymentDueDayChanged_NotFinancePlan_AwaitingStatement(int paymentDueDay)
        {
            this.ProcessPaymentDueDayChanged_NotFinancePlan(paymentDueDay, false);
        }

        private void ProcessPaymentDueDayChanged_NotFinancePlan(int paymentDueDay, bool inGracePeriod)
        {
            // guarantor has already updated
            Guarantor guarantor = new Guarantor
            {
                PaymentDueDay = paymentDueDay,
                VpGuarantorId = 8
            };
            
            DateTime now = DateTime.UtcNow.Date.AddDays(inGracePeriod ? 0 : -1);
            int originalPaymentDueDay = now.Day.ToValidPaymentDueDay();
            if (originalPaymentDueDay == DateConstants.EndOfMonthDay)
            {
                originalPaymentDueDay = DateTime.DaysInMonth(now.Year, now.Month);
            }

            VpStatement currentStatement = new VpStatement
            {
                OriginalPaymentDueDate = new DateTime(now.Year, now.Month, originalPaymentDueDay),
                PaymentDueDate = new DateTime(now.Year, now.Month, originalPaymentDueDay)
            };
            currentStatement.StatementDate = currentStatement.PaymentDueDate.AddDays(-21);
            
            DateTime originalStatementDate = currentStatement.StatementDate;
            DateTime originalPaymentDueDate = currentStatement.OriginalPaymentDueDate;
            bool isGracePeriod = currentStatement.IsGracePeriod;
            
            IStatementDateService statementDateService = new StatementDateServiceMockBuilder().CreateService();
            VpStatementServiceMockBuilder builder = new VpStatementServiceMockBuilder { StatementDateService = statementDateService };
            
            builder.FinancePlanServiceMock.Setup(x => x.MeetsCriteriaToChangePaymentDueDate(guarantor)).Returns(false);
            builder.VpStatementRepositoryMock.Setup(x => x.GetMostRecentStatement(guarantor)).Returns(currentStatement);

            VpStatementService svc = builder.GetMockedVpStatementService();
            
            ProcessPaymentDueDayChangedResult result = svc.ProcessPaymentDueDayChanged(guarantor, false, 1);
            
            // original due date doesn't change
            Assert.AreEqual(originalPaymentDueDate, currentStatement.OriginalPaymentDueDate);

            // {graceperiod or awaiting statement} doesn't change
            Assert.AreEqual(isGracePeriod, currentStatement.IsGracePeriod);
            
            if (result.IsChanged)
            {
                // payment due day matches
                Assert.AreEqual(guarantor.PaymentDueDay.ToValidPaymentDueDay(), result.NewPaymentDueDate.Day.ToValidPaymentDueDay());
                Assert.AreEqual(guarantor.PaymentDueDay.ToValidPaymentDueDay(), currentStatement.PaymentDueDate.Day.ToValidPaymentDueDay());
                
                // payment due date is recalculated
                Assert.AreEqual(result.NewPaymentDueDate.Date, currentStatement.PaymentDueDate.Date);
            }
            else
            {
                // the dates on the current statement don't change
                Assert.AreEqual(currentStatement.OriginalPaymentDueDate, currentStatement.PaymentDueDate);
                Assert.AreEqual(originalStatementDate, currentStatement.StatementDate);
            }
            
            // verify next statement will generate with appropriate dates
            DateTime nextStatementDate = statementDateService.GetNextStatementDate(currentStatement.PaymentDueDate, guarantor, currentStatement.StatementDate);
            DateTime nextStatementPaymentDate = statementDateService.GetNextPaymentDate(currentStatement.PaymentDueDate, guarantor, nextStatementDate);
            
            Assert.AreEqual(guarantor.PaymentDueDay.ToValidPaymentDueDay(), nextStatementPaymentDate.Day.ToValidPaymentDueDay());
        }
        
        [Test]
        public void UpdateStatementPaymentDueDate_UpdatesWhenDifferent()
        {
            VpStatement statement = new VpStatement
            {
                PaymentDueDate = DateTime.UtcNow.Date.AddDays(-1),
                ProcessingStatus = ChangeEventStatusEnum.Processed
            };

            FinancePlanStatement fpStatement = new FinancePlanStatement
            {
                PaymentDueDate = DateTime.UtcNow.Date.AddDays(-1),
                ProcessingStatus = ChangeEventStatusEnum.Processed
            };

            VpStatementServiceMockBuilder builder = new VpStatementServiceMockBuilder
            {
                StatementDateService = new StatementDateServiceMockBuilder().CreateService()
            };
            builder.FinancePlanStatementRepositoryMock.Setup(x => x.GetFinancePlanStatementByVpStatementId(It.IsAny<int>(), It.IsAny<int>())).Returns(fpStatement);
            VpStatementService svc = builder.GetMockedVpStatementService();
            
            DateTime newPaymentDueDate = statement.PaymentDueDate.AddDays(2);
            svc.UpdateStatementPaymentDueDate(statement, newPaymentDueDate, true);

            Assert.AreEqual(newPaymentDueDate, statement.PaymentDueDate);
            Assert.AreEqual(newPaymentDueDate, statement.OriginalPaymentDueDate);
            Assert.AreEqual(ChangeEventStatusEnum.Unprocessed, statement.ProcessingStatus);
            builder.VpStatementRepositoryMock.Verify(x => x.InsertOrUpdate(statement), Times.Once);

            Assert.AreEqual(newPaymentDueDate, fpStatement.PaymentDueDate);
            Assert.AreEqual(newPaymentDueDate, fpStatement.OriginalPaymentDueDate);
            Assert.AreEqual(ChangeEventStatusEnum.Unprocessed, fpStatement.ProcessingStatus);
            builder.FinancePlanStatementRepositoryMock.Verify(x => x.InsertOrUpdate(fpStatement), Times.Once);
        }

        [Test]
        public void UpdateStatementPaymentDueDate_StaysTheSame()
        {
            VpStatementServiceMockBuilder builder = new VpStatementServiceMockBuilder
            {
                StatementDateService = new StatementDateServiceMockBuilder().CreateService()
            };
            VpStatementService svc = builder.GetMockedVpStatementService();

            VpStatement statement = new VpStatement
            {
                PaymentDueDate = DateTime.UtcNow.Date
            };

            DateTime newPaymentDueDate = statement.PaymentDueDate;

            svc.UpdateStatementPaymentDueDate(statement, newPaymentDueDate, true);

            Assert.AreEqual(newPaymentDueDate, statement.PaymentDueDate);
            builder.VpStatementRepositoryMock.Verify(x => x.InsertOrUpdate(statement), Times.Never);
        }

        [TestCase(-7)]
        [TestCase(7)]
        public void RecalculateGuarantorNextStatementDate(int addDays)
        {
            VpStatementServiceMockBuilder builder = new VpStatementServiceMockBuilder
            {
                StatementDateService = new StatementDateServiceMockBuilder().CreateService()
            };
            VpStatementService svc = builder.GetMockedVpStatementService();

            DateTime originalPaymentDueDate = new DateTime(2019, 01, 27);
            DateTime newPaymentDueDate = originalPaymentDueDate.AddDays(addDays);

            Guarantor guarantor = new Guarantor
            {
                PaymentDueDay = newPaymentDueDate.Day
            };

            VpStatement statement = new VpStatement
            {
                OriginalPaymentDueDate = originalPaymentDueDate,
                PaymentDueDate = newPaymentDueDate.Date,
                StatementDate = originalPaymentDueDate.AddDays(-21)
            };

            svc.RecalculateGuarantorNextStatementDate(statement, guarantor, new DateTime(2019, 01, 23));

            DateTime expectedNextStatementDate = newPaymentDueDate.AddMonths(1).AddDays(-21);
            Assert.AreEqual(expectedNextStatementDate.Date, guarantor.NextStatementDate.GetValueOrDefault(DateTime.MinValue).Date);
        }
    }
}
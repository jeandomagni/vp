﻿namespace Ivh.Domain.Discount.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Common.Base.Utilities.Extensions;
    using Common.Tests.Helpers.Discount;
    using Common.VisitPay.Enums;
    using Guarantor.Entities;
    using FinanceManagement.Discount.Entities;
    using FinanceManagement.Discount.Interfaces;
    using FinanceManagement.Payment.Entities;
    using Moq;
    using NUnit.Framework;
    using Visit.Entities;

    [TestFixture]
    public class BalanceDiscountServiceTests : DomainTestBase
    {
        private Mock<IDiscountBalanceTierRepository> _moqDiscountBalanceTierRepository;
        private BalanceDiscountServiceMockBuilder _balanceDiscountServiceMockBuilder;

        private static readonly IList<DiscountBalanceTier> Tiers = new List<DiscountBalanceTier>
        {
            new DiscountBalanceTier {DiscountBalanceTierId = 1, DiscountPercent = 0.05m, MinimumBalance = 500m, MaximumBalance = 2999.99m},
            new DiscountBalanceTier {DiscountBalanceTierId = 2, DiscountPercent = 0.10m, MinimumBalance = 3000m, MaximumBalance = 9999.99m},
            new DiscountBalanceTier {DiscountBalanceTierId = 3, DiscountPercent = 0.15m, MinimumBalance = 100000m, MaximumBalance = null}
        };

        [SetUp]
        public void Setup()
        {
            this._moqDiscountBalanceTierRepository = new Mock<IDiscountBalanceTierRepository>();
            this._moqDiscountBalanceTierRepository.Setup(t => t.GetQueryable()).Returns(() => Tiers.AsQueryable());
            
            this._balanceDiscountServiceMockBuilder = new BalanceDiscountServiceMockBuilder
            {
                DiscountBalanceTierRepositoryMock = this._moqDiscountBalanceTierRepository
            };
        }
        
        [TestCase(  499.99, 0.00)]
        [TestCase(  500.00, 0.05)]
        [TestCase( 2999.99, 0.05)]
        [TestCase( 3000.00, 0.10)]
        [TestCase( 9999.99, 0.10)]
        [TestCase(10000.00, 0.15)]
        [TestCase(99999.99, 0.15)]
        public void BalanceDiscount_DifferentAgingTiers_AllVisitsEligible(decimal balance, decimal expectedPercentage)
        {
            decimal visit1Balance = Math.Round(balance / 2m, 2);
            decimal visit2Balance = balance - visit1Balance;

            Visit visit1 = CreateMockVisit(visit1Balance, AgingTierEnum.NotStatemented, 1).Object;
            Visit visit2 = CreateMockVisit(visit2Balance, AgingTierEnum.Good, 2).Object;
            List<Visit> visits = new List<Visit> { visit1, visit2 };

            List<PaymentVisit> paymentVisits = visits.Select(Mapper.Map<PaymentVisit>).ToList();
            Payment payment = CreatePayment(paymentVisits);

            IDiscountService balanceDiscountService = this._balanceDiscountServiceMockBuilder.CreateService();
            balanceDiscountService.AssignDiscountsToPayment(payment, paymentVisits.Select(x => new DiscountVisitPayment(x, x.CurrentBalance)).ToList());

            decimal? discountPercentage = payment.DiscountOffer?.DiscountTotalPercent ?? 0m;
            
            Assert.AreEqual(expectedPercentage, discountPercentage);
        }
        
        [TestCase(AgingTierEnum.NotStatemented, true)]
        [TestCase(AgingTierEnum.Good, true)]
        [TestCase(AgingTierEnum.PastDue, true)]
        [TestCase(AgingTierEnum.FinalPastDue, false)]
        [TestCase(AgingTierEnum.MaxAge, false)]
        public void BalanceDiscount_RespectsMaximumAgingCount(AgingTierEnum agingTier, bool expectDiscount)
        {
            List<Visit> visits = CreateMockVisit(1000m, agingTier, 1).ToListOfOne().Select(x => x.Object).ToList();

            List<PaymentVisit> paymentVisits = visits.Select(Mapper.Map<PaymentVisit>).ToList();
            Payment payment = CreatePayment(paymentVisits);

            IDiscountService balanceDiscountService = this._balanceDiscountServiceMockBuilder
                .WithDiscountMaximumAgingTier(AgingTierEnum.PastDue)
                .CreateService();

            balanceDiscountService.AssignDiscountsToPayment(payment, paymentVisits.Select(x => new DiscountVisitPayment(x, x.CurrentBalance)).ToList());

            decimal? discountPercentage = payment.DiscountOffer?.DiscountTotalPercent ?? 0m;
            
            Assert.AreEqual(expectDiscount, discountPercentage.GetValueOrDefault(0m) > 0m);
        }

        [TestCase(AgingTierEnum.NotStatemented, 2)]
        [TestCase(AgingTierEnum.Good, 2)]
        [TestCase(AgingTierEnum.PastDue, 2)]
        [TestCase(AgingTierEnum.FinalPastDue, 1)]
        [TestCase(AgingTierEnum.MaxAge, 1)]
        public void BalanceDiscount_RespectsMaximumAgingCount_WithOneEligibleVisit(AgingTierEnum agingTier, int numberOfDiscountedVisits)
        {
            // this visit always eligible
            Visit visit1 = CreateMockVisit(1000m, AgingTierEnum.Good, 1).Object;
            
            // eligible depending on agingTier
            Visit visit2 = CreateMockVisit(2000m, agingTier, 2).Object;
            
            List<Visit> visits = new List<Visit> { visit1, visit2 };

            List<PaymentVisit> paymentVisits = visits.Select(Mapper.Map<PaymentVisit>).ToList();
            Payment payment = CreatePayment(paymentVisits);

            IDiscountService balanceDiscountService = this._balanceDiscountServiceMockBuilder
                .WithDiscountMaximumAgingTier(AgingTierEnum.PastDue)
                .CreateService();

            balanceDiscountService.AssignDiscountsToPayment(payment, paymentVisits.Select(x => new DiscountVisitPayment(x, x.CurrentBalance)).ToList());

            Assert.AreEqual(numberOfDiscountedVisits, payment.DiscountOffer.VisitOffers.Count(x => x.DiscountPercent > 0m));
        }

        [TestCase(true)]
        [TestCase(false)]
        public void BalanceDiscount_WithPromptPaymentOnlyEnabled(bool isPromptPayment)
        {
            Visit visit1 = CreateMockVisit(1000m, AgingTierEnum.Good, 1).Object;
            List<Visit> visits = new List<Visit> { visit1 };
            
            List<PaymentVisit> paymentVisits = visits.Select(Mapper.Map<PaymentVisit>).ToList();

            PaymentTypeEnum paymentType = isPromptPayment ?  PaymentTypeEnum.ManualPromptCurrentNonFinancedBalanceInFull :  PaymentTypeEnum.ManualScheduledCurrentNonFinancedBalanceInFull;
            Payment payment = CreatePayment(paymentVisits, paymentType);

            IDiscountService balanceDiscountService = this._balanceDiscountServiceMockBuilder
                .WithDiscountPromptPayOnly(true)
                .CreateService();
            
            balanceDiscountService.AssignDiscountsToPayment(payment, paymentVisits.Select(x => new DiscountVisitPayment(x, x.CurrentBalance)).ToList());

            if (isPromptPayment)
            {
                Assert.True(payment.DiscountOffer.DiscountTotalPercent > 0m);
            }
            else
            {
                Assert.True(payment.DiscountOffer == null || payment.DiscountOffer.DiscountTotalPercent == 0m);
            }
        }
        
        [TestCase(true)]
        [TestCase(false)]
        public void BalanceDiscount_WithPromptPaymentOnlyDisabled(bool isPromptPayment)
        {
            Visit visit1 = CreateMockVisit(1000m, AgingTierEnum.Good, 1).Object;
            List<Visit> visits = new List<Visit> { visit1 };
            
            List<PaymentVisit> paymentVisits = visits.Select(Mapper.Map<PaymentVisit>).ToList();

            PaymentTypeEnum paymentType = isPromptPayment ?  PaymentTypeEnum.ManualPromptCurrentNonFinancedBalanceInFull :  PaymentTypeEnum.ManualScheduledCurrentNonFinancedBalanceInFull;
            Payment payment = CreatePayment(paymentVisits, paymentType);

            IDiscountService balanceDiscountService = this._balanceDiscountServiceMockBuilder
                .WithDiscountPromptPayOnly(false)
                .CreateService();
            
            balanceDiscountService.AssignDiscountsToPayment(payment, paymentVisits.Select(x => new DiscountVisitPayment(x, x.CurrentBalance)).ToList());
            
            Assert.True(payment.DiscountOffer.DiscountTotalPercent > 0m);
        }

        private static Mock<Visit> CreateMockVisit(decimal currentBalance, AgingTierEnum agingTier, int visitId)
        {
            Mock<Visit> moqVisit = new Mock<Visit>();
            moqVisit.Setup(x => x.VisitId).Returns(visitId);
            moqVisit.Setup(x => x.VPGuarantor).Returns(new Guarantor());
            moqVisit.Setup(x => x.CurrentBalance).Returns(currentBalance);
            moqVisit.Setup(x => x.AgingTier).Returns(agingTier);
            moqVisit.Setup(x => x.IsMaxAge).Returns(agingTier == AgingTierEnum.MaxAge);
            moqVisit.Setup(x => x.CurrentVisitState).Returns(VisitStateEnum.Active);

            return moqVisit;
        }
        
        private static Payment CreatePayment(IList<PaymentVisit> paymentVisits, PaymentTypeEnum paymentType = PaymentTypeEnum.ManualPromptCurrentNonFinancedBalanceInFull)
        {
            Payment payment = new Payment
            {
                PaymentType = paymentType,
                PaymentScheduledAmounts = new List<PaymentScheduledAmount>(),
                Guarantor = new Guarantor {  VpGuarantorId = 1}
            };

            foreach (PaymentVisit paymentVisit in paymentVisits)
            {
                payment.PaymentScheduledAmounts.Add(new PaymentScheduledAmount { ScheduledAmount = paymentVisit.CurrentBalance, PaymentVisit = paymentVisit });
            }

            return payment;
        }
    }
}
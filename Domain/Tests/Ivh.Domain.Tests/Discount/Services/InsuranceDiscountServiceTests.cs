﻿namespace Ivh.Domain.Discount.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.Tests.Helpers.Discount;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using FinanceManagement.Discount.Entities;
    using FinanceManagement.Discount.Interfaces;
    using FinanceManagement.Payment.Entities;
    using Guarantor.Entities;
    using Moq;
    using NUnit.Framework;
    using Visit.Entities;

    [TestFixture]
    public class InsuranceDiscountServiceTests : DomainTestBase
    {
        private InsuranceDiscountServiceMockBuilder _insuranceDiscountServiceMockBuilder;
        
        [SetUp]
        public void BeforeEachTest()
        {
            this._insuranceDiscountServiceMockBuilder = new InsuranceDiscountServiceMockBuilder().Setup(builder =>
            {
            });
        }

        private static Mock<Visit> CreateVisit(int visitId, decimal amount, string billingApplication, bool withInsurance, AgingTierEnum agingTier = AgingTierEnum.Good)
        {
            Mock<Visit> moqVisit = new Mock<Visit>();
            moqVisit.Setup(x => x.VisitId).Returns(visitId);
            moqVisit.Setup(x => x.AgingTier).Returns(agingTier);
            moqVisit.Setup(x => x.IsMaxAge).Returns(false);
            moqVisit.Setup(x => x.CurrentVisitState).Returns(VisitStateEnum.Active);
            moqVisit.Setup(x => x.CurrentBalance).Returns(amount);
            moqVisit.Setup(x => x.BillingApplication).Returns(billingApplication);
            if (withInsurance)
            {
                moqVisit.Setup(x => x.VisitInsurancePlans).Returns(() => new List<VisitInsurancePlan>
                {
                    new VisitInsurancePlan
                    {
                        InsurancePlan = new InsurancePlan
                        {
                            DiscountPercentage = 0.03m
                        }
                    }
                });
            }
            else
            {
                moqVisit.Setup(x => x.VisitInsurancePlans).Returns(() => new List<VisitInsurancePlan>());
            }
            
            return moqVisit;
        }

        private static Payment CreatePayment(IList<PaymentVisit> paymentVisits, PaymentTypeEnum paymentType = PaymentTypeEnum.ManualPromptCurrentNonFinancedBalanceInFull)
        {
            Payment payment = new Payment
            {
                PaymentType = paymentType,
                PaymentScheduledAmounts = new List<PaymentScheduledAmount>(),
                Guarantor = new Guarantor { VpGuarantorId = 1 }
            };

            foreach (PaymentVisit paymentVisit in paymentVisits)
            {
                payment.PaymentScheduledAmounts.Add(new PaymentScheduledAmount {ScheduledAmount = paymentVisit.CurrentBalance, PaymentVisit = paymentVisit});
            }

            return payment;
        }

        [Test]
        public void HB_WithInsurance_IsDiscountEligible()
        {
            PaymentVisit paymentVisit = Mapper.Map<PaymentVisit>(CreateVisit(1, 500m, BillingApplicationConstants.HB, true).Object);
            Payment payment = CreatePayment(paymentVisit.ToListOfOne());

            IDiscountService insuranceDiscountService = this._insuranceDiscountServiceMockBuilder.CreateService();
            
            insuranceDiscountService.AssignDiscountsToPayment(payment, new DiscountVisitPayment(paymentVisit, paymentVisit.CurrentBalance).ToListOfOne());

            Assert.NotNull(payment.DiscountOffer);
            Assert.IsTrue(payment.DiscountOffer.DiscountTotal > 0m);
        }

        [Test]
        public void HB_WithoutInsurance_IsNotDiscountEligible()
        {
            PaymentVisit paymentVisit = Mapper.Map<PaymentVisit>(CreateVisit(1, 500m, BillingApplicationConstants.HB, false).Object);
            Payment payment = CreatePayment(paymentVisit.ToListOfOne());

            IDiscountService insuranceDiscountService = this._insuranceDiscountServiceMockBuilder.CreateService();

            insuranceDiscountService.AssignDiscountsToPayment(payment, new DiscountVisitPayment(paymentVisit, paymentVisit.CurrentBalance).ToListOfOne());

            Assert.IsNull(payment.DiscountOffer);
        }

        [Test]
        public void PB_IsNotDiscountEligible()
        {
            PaymentVisit paymentVisit = Mapper.Map<PaymentVisit>(CreateVisit(1, 500m, BillingApplicationConstants.PB, true).Object);
            Payment payment = CreatePayment(paymentVisit.ToListOfOne());
            
            IDiscountService insuranceDiscountService = this._insuranceDiscountServiceMockBuilder.CreateService();

            insuranceDiscountService.AssignDiscountsToPayment(payment, new DiscountVisitPayment(paymentVisit, paymentVisit.CurrentBalance).ToListOfOne());

            Assert.IsNull(payment.DiscountOffer);
        }
        
        [Test]
        public void OffersAreRounded()
        {
            IList<PaymentVisit> paymentVisits = this.GetDefaultMockVisits().Select(x => Mapper.Map<PaymentVisit>(x.Object)).ToList();
            Payment payment = CreatePayment(paymentVisits);

            IDiscountService insuranceDiscountService = this._insuranceDiscountServiceMockBuilder.CreateService();

            insuranceDiscountService.AssignDiscountsToPayment(payment, paymentVisits.Select(x => new DiscountVisitPayment(x, x.CurrentBalance)).ToList());
            
            List<decimal> amounts = new List<decimal>
            {
                payment.DiscountOffer.DiscountTotal,
                payment.DiscountOffer.DiscountedTotalCurrentBalance
            };

            amounts.AddRange(payment.DiscountOffer.VisitOffers.Select(x => x.DiscountAmount));
            amounts.AddRange(payment.DiscountOffer.VisitOffers.Select(x => x.DiscountedCurrentBalance));
            
            foreach (decimal amount in amounts)
            {
                decimal value = amount * 1000;
                Assert.AreEqual(Math.Floor(value), value);
            }
        }
        
        [Test]
        public void TheMathWorks()
        {
            IList<PaymentVisit> paymentVisits = new List<Mock<Visit>>
            {
                CreateVisit(1, 498.17m, BillingApplicationConstants.HB, true),
                CreateVisit(2, 498.13m, BillingApplicationConstants.HB, true),
                CreateVisit(3, 321.53m, BillingApplicationConstants.HB, true),
                CreateVisit(4, 932.25m, BillingApplicationConstants.HB, true)
            }.Select(x => Mapper.Map<PaymentVisit>(x.Object)).ToList();

            Payment payment = CreatePayment(paymentVisits);

            IDiscountService insuranceDiscountService = this._insuranceDiscountServiceMockBuilder.CreateService();

            insuranceDiscountService.AssignDiscountsToPayment(payment, paymentVisits.Select(x => new DiscountVisitPayment(x, x.CurrentBalance)).ToList());

            Assert.AreEqual(498.17m + 498.13m + 321.53m + 932.25m, payment.DiscountOffer.DiscountTotal + payment.DiscountOffer.DiscountedTotalCurrentBalance);
            
            foreach (DiscountVisitOffer visitOffer in payment.DiscountOffer.VisitOffers)
            {
                decimal value = visitOffer.DiscountAmount + visitOffer.DiscountedCurrentBalance;
                Assert.AreEqual(value, visitOffer.TotalBalance);
            }
        }
        
        [TestCase(PaymentTypeEnum.ManualPromptSpecificAmount)]
        [TestCase(PaymentTypeEnum.ManualPromptSpecificVisits)]
        [TestCase(PaymentTypeEnum.ManualPromptSpecificFinancePlans)]
        [TestCase(PaymentTypeEnum.ManualPromptCurrentBalanceInFull)]
        [TestCase(PaymentTypeEnum.ManualScheduledSpecificAmount)]
        [TestCase(PaymentTypeEnum.ManualScheduledSpecificVisits)]
        [TestCase(PaymentTypeEnum.ManualScheduledSpecificFinancePlans)]
        [TestCase(PaymentTypeEnum.ManualScheduledCurrentBalanceInFull)]
        [TestCase(PaymentTypeEnum.RecurringPaymentFinancePlan)]
        [TestCase(PaymentTypeEnum.ManualPromptCurrentNonFinancedBalanceInFull)]
        [TestCase(PaymentTypeEnum.ManualScheduledCurrentNonFinancedBalanceInFull)]
        [TestCase(PaymentTypeEnum.Void)]
        [TestCase(PaymentTypeEnum.Refund)]
        [TestCase(PaymentTypeEnum.PartialRefund)]
        public void AssignDiscountsToPayment_assigns_correctly(PaymentTypeEnum paymentType)
        {
            bool expectDiscountOffer = EnumHelper<PaymentTypeEnum>.Contains(paymentType, PaymentTypeEnumCategory.DiscountEligible);

            List<PaymentVisit> paymentVisits = this.GetDefaultMockVisits().Select(x => Mapper.Map<PaymentVisit>(x.Object)).ToList();
            Payment payment = CreatePayment(paymentVisits, paymentType);

            IDiscountService insuranceDiscountService = this._insuranceDiscountServiceMockBuilder.CreateService();

            insuranceDiscountService.AssignDiscountsToPayment(payment, paymentVisits.Select(x => new DiscountVisitPayment(x, x.CurrentBalance)).ToList());

            DiscountOffer result = payment.DiscountOffer;

            if (expectDiscountOffer)
            {
                Assert.True(result != null, $"{paymentType.ToString()} is expected to return a DiscountOffer but did not.");
            }
            else
            {
                Assert.True(result == null, $"{paymentType.ToString()} is not expected to return a DiscountOffer but did.");
            }
        }
        
        private IList<Mock<Visit>> GetDefaultMockVisits()
        {
            //has a PB visit
            IList<Mock<Visit>> moqVisits = new List<Mock<Visit>>
            {
                CreateVisit(1, 498.17m, BillingApplicationConstants.HB, true),
                CreateVisit(2, 498.13m, BillingApplicationConstants.HB, true),
                CreateVisit(3, 321.53m, BillingApplicationConstants.PB, true),
                CreateVisit(4, 932.25m, BillingApplicationConstants.HB, true)
            };

            return moqVisits;
        }
        
        [TestCase(AgingTierEnum.NotStatemented, true)]
        [TestCase(AgingTierEnum.Good, true)]
        [TestCase(AgingTierEnum.PastDue, true)]
        [TestCase(AgingTierEnum.FinalPastDue, false)]
        [TestCase(AgingTierEnum.MaxAge, false)]
        public void InsuranceDiscount_RespectsMaximumAgingCount(AgingTierEnum agingTier, bool expectDiscount)
        {
            List<Visit> visits = CreateVisit(1, 1000m, BillingApplicationConstants.HB, true, agingTier).ToListOfOne().Select(x => x.Object).ToList();

            List<PaymentVisit> paymentVisits = visits.Select(Mapper.Map<PaymentVisit>).ToList();
            Payment payment = CreatePayment(paymentVisits);

            IDiscountService balanceDiscountService = this._insuranceDiscountServiceMockBuilder
                .WithDiscountMaximumAgingTier(AgingTierEnum.PastDue)
                .CreateService();

            balanceDiscountService.AssignDiscountsToPayment(payment, paymentVisits.Select(x => new DiscountVisitPayment(x, x.CurrentBalance)).ToList());

            decimal? discountPercentage = payment.DiscountOffer?.DiscountTotalPercent ?? 0m;
            
            Assert.AreEqual(expectDiscount, discountPercentage.GetValueOrDefault(0m) > 0m);
        }
        
        [TestCase(AgingTierEnum.NotStatemented, 2)]
        [TestCase(AgingTierEnum.Good, 2)]
        [TestCase(AgingTierEnum.PastDue, 2)]
        [TestCase(AgingTierEnum.FinalPastDue, 1)]
        [TestCase(AgingTierEnum.MaxAge, 1)]
        public void InsuranceDiscount_RespectsMaximumAgingCount_WithOneEligibleVisit(AgingTierEnum agingTier, int numberOfDiscountedVisits)
        {
            // this visit always eligible
            Visit visit1 = CreateVisit(1, 1000m, BillingApplicationConstants.HB, true).Object;
            
            // eligible depending on agingTier
            Visit visit2 = CreateVisit(2, 2000m, BillingApplicationConstants.HB, true, agingTier).Object;
            
            List<Visit> visits = new List<Visit> { visit1, visit2 };

            List<PaymentVisit> paymentVisits = visits.Select(Mapper.Map<PaymentVisit>).ToList();
            Payment payment = CreatePayment(paymentVisits);

            IDiscountService balanceDiscountService = this._insuranceDiscountServiceMockBuilder
                .WithDiscountMaximumAgingTier(AgingTierEnum.PastDue)
                .CreateService();

            balanceDiscountService.AssignDiscountsToPayment(payment, paymentVisits.Select(x => new DiscountVisitPayment(x, x.CurrentBalance)).ToList());

            Assert.AreEqual(numberOfDiscountedVisits, payment.DiscountOffer.VisitOffers.Count(x => x.DiscountPercent > 0m));
        }
        
        [TestCase(true)]
        [TestCase(false)]
        public void InsuranceDiscount_WithPromptPaymentOnlyEnabled(bool isPromptPayment)
        {
            Visit visit1 = CreateVisit(1, 1000m, BillingApplicationConstants.HB, true).Object;
            List<Visit> visits = new List<Visit> { visit1 };
            
            List<PaymentVisit> paymentVisits = visits.Select(Mapper.Map<PaymentVisit>).ToList();

            PaymentTypeEnum paymentType = isPromptPayment ?  PaymentTypeEnum.ManualPromptCurrentNonFinancedBalanceInFull :  PaymentTypeEnum.ManualScheduledCurrentNonFinancedBalanceInFull;
            Payment payment = CreatePayment(paymentVisits, paymentType);

            IDiscountService balanceDiscountService = this._insuranceDiscountServiceMockBuilder
                .WithDiscountPromptPayOnly(true)
                .CreateService();
            
            balanceDiscountService.AssignDiscountsToPayment(payment, paymentVisits.Select(x => new DiscountVisitPayment(x, x.CurrentBalance)).ToList());

            if (isPromptPayment)
            {
                Assert.True(payment.DiscountOffer.DiscountTotalPercent > 0m);
            }
            else
            {
                Assert.True(payment.DiscountOffer == null || payment.DiscountOffer.DiscountTotalPercent == 0m);
            }
        }
        
        [TestCase(true)]
        [TestCase(false)]
        public void InsuranceDiscount_WithPromptPaymentOnlyDisabled(bool isPromptPayment)
        {
            Visit visit1 = CreateVisit(1, 1000m, BillingApplicationConstants.HB, true).Object;
            List<Visit> visits = new List<Visit> { visit1 };
            
            List<PaymentVisit> paymentVisits = visits.Select(Mapper.Map<PaymentVisit>).ToList();

            PaymentTypeEnum paymentType = isPromptPayment ?  PaymentTypeEnum.ManualPromptCurrentNonFinancedBalanceInFull :  PaymentTypeEnum.ManualScheduledCurrentNonFinancedBalanceInFull;
            Payment payment = CreatePayment(paymentVisits, paymentType);

            IDiscountService balanceDiscountService = this._insuranceDiscountServiceMockBuilder
                .WithDiscountPromptPayOnly(false)
                .CreateService();
            
            balanceDiscountService.AssignDiscountsToPayment(payment, paymentVisits.Select(x => new DiscountVisitPayment(x, x.CurrentBalance)).ToList());
            
            Assert.True(payment.DiscountOffer.DiscountTotalPercent > 0m);
        }
    }
}
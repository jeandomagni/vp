﻿namespace Ivh.Domain.User
{
    using Entities;
    using Ivh.Application.Core.Common.Interfaces;
    using Moq;
    using NUnit.Framework;
    using System.Collections.Generic;
    using System.Linq;

    [TestFixture]
    public class VisitPayUserAccessTokenPhiTests
    {
        [Test]
        public void VisitPayUserAccessTokenPhi_NoAccessTokensYetAssigned_ValidValueAssigned()
        {
            // simulate first read of assigned tokens (null for each VP user);
            // also incorporate empty strings into this test
            List<string> tokens = new List<string> { null, null, string.Empty, null, string.Empty };

            VisitPayUserAccessTokenPhi token = new VisitPayUserAccessTokenPhi(new AccessTokenPhiValueGenerator());

            Assert.IsTrue(VisitPayUserAccessTokenPhi.IsValid(token.RawValue.ToString()));
        }
    }
}
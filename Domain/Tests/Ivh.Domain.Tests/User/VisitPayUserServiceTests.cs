﻿namespace Ivh.Domain.User
{
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.Encryption.Enums;
    using Common.Tests.Helpers.User;
    using Entities;
    using Moq;
    using NUnit.Framework;
    using Services;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    [TestFixture]
    public class VisitPayUserServiceTests : DomainTestBase
    {
        [Test]
        public void FindByDetailsAndToken_CurrentPasswordMatches()
        {
            const string password = "password";
            VisitPayUser user = this.CreateTestUser(password);

            VisitPayUserService svc = this.CreateBuilder(password, user.ToListOfOne()).CreateService();
            VisitPayUser foundUser = svc.FindByDetailsAndToken(user.LastName, user.DateOfBirth.GetValueOrDefault(DateTime.UtcNow), password);

            Assert.NotNull(foundUser);
            Assert.AreEqual(user.VisitPayUserId, foundUser.VisitPayUserId);
        }

        [Test]
        public void FindByDetailsAndToken_PasswordHistoryMatches()
        {
            const string password = "password";
            VisitPayUser user = this.CreateTestUser(RandomStringGeneratorUtility.GetRandomString(10));
            IList<VisitPayUserPasswordHistory> passwordHistories = new List<VisitPayUserPasswordHistory>();
            passwordHistories.Add(new VisitPayUserPasswordHistory { PasswordHash = RandomStringGeneratorUtility.GetRandomString(10) });
            passwordHistories.Add(new VisitPayUserPasswordHistory { PasswordHash = password });

            VisitPayUserService svc = this.CreateBuilder(password, user.ToListOfOne(), passwordHistories).CreateService();
            VisitPayUser foundUser = svc.FindByDetailsAndToken(user.LastName, user.DateOfBirth.GetValueOrDefault(DateTime.UtcNow), password);

            Assert.NotNull(foundUser);
            Assert.AreEqual(user.VisitPayUserId, foundUser.VisitPayUserId);
        }

        [Test]
        public void FindByDetailsAndToken_ManyMatches()
        {
            const string password = "password";

            VisitPayUser user = this.CreateTestUser(RandomStringGeneratorUtility.GetRandomString(10));
            IList<VisitPayUserPasswordHistory> passwordHistories = new List<VisitPayUserPasswordHistory>();
            passwordHistories.Add(new VisitPayUserPasswordHistory { PasswordHash = RandomStringGeneratorUtility.GetRandomString(10) });
            passwordHistories.Add(new VisitPayUserPasswordHistory { PasswordHash = password });

            IList<VisitPayUser> users = user.ToListOfOne();
            users.Add(user);
            users.Add(user);

            VisitPayUserService svc = this.CreateBuilder(password, users, passwordHistories).CreateService();
            VisitPayUser foundUser = svc.FindByDetailsAndToken(user.LastName, user.DateOfBirth.GetValueOrDefault(DateTime.UtcNow), password);

            Assert.IsNull(foundUser);
        }

        [Test]
        public void FindByDetailsAndToken_NoMatches()
        {
            const string password = "password";
            VisitPayUser user = this.CreateTestUser(RandomStringGeneratorUtility.GetRandomString(10));
            IList<VisitPayUserPasswordHistory> passwordHistories = new List<VisitPayUserPasswordHistory>();
            passwordHistories.Add(new VisitPayUserPasswordHistory { PasswordHash = RandomStringGeneratorUtility.GetRandomString(10) });

            VisitPayUserService svc = this.CreateBuilder(password, user.ToListOfOne(), passwordHistories).CreateService();
            VisitPayUser foundUser = svc.FindByDetailsAndToken(user.LastName, user.DateOfBirth.GetValueOrDefault(DateTime.UtcNow), password);

            Assert.IsNull(foundUser);
        }

        [Test]
        public void GeneratesAccessToken_UntilAnAccessTokenDoesNotExistInRepository()
        {
            List<string> valuesToReturn = new List<string>()
            {
                //Need the duplicates. On the Assert it'll burn the last queued item.
                "23412324", "23412324", "47344422", "12345678"
            };
            Mock<VisitPayUser> visitPayUserMock = new Mock<VisitPayUser>();

            visitPayUserMock.SetupSet(s => s.AccessTokenPhi = It.IsAny<string>());
            visitPayUserMock.Setup(c => c.GenerateValidAccessTokenPhiValue()).Returns(new Queue<string>(valuesToReturn).Dequeue);
            visitPayUserMock.SetupGet(s => s.AccessTokenPhi).Returns(visitPayUserMock.Object.GenerateValidAccessTokenPhiValue());

            VisitPayUser visitPayUser = visitPayUserMock.Object;

            VisitPayUserServiceMockBuilder vpUserSvcMockBuilder = this.CreateBuilder(null, visitPayUser.ToListOfOne(), null);
            vpUserSvcMockBuilder.VisitPayUserRepositoryMock.Setup(r => r.FindById(It.IsAny<string>())).Returns(visitPayUserMock.Object);
            vpUserSvcMockBuilder.VisitPayUserRepositoryMock.SetupSequence(c => c.AccessTokenPhiValueExists(It.IsAny<string>(), It.IsAny<int>()))
                .Returns(true)
                .Returns(true)
                .Returns(false);

            VisitPayUserService svc = vpUserSvcMockBuilder.CreateService();

            svc.GenerateAccessTokenPhiForUser(visitPayUser.VisitPayUserId);
            Assert.True(visitPayUser.AccessTokenPhi == "23412324");
        }

        [Test]
        public void GenerateAccessTokenPhiForUser_TokenValueIsValid()
        {
            VisitPayUser vpUser = this.CreateTestUser(RandomStringGeneratorUtility.GetRandomString(10));

            VisitPayUserServiceMockBuilder vpUserSvcMockBuilder = this.CreateBuilder(null, vpUser.ToListOfOne(), null);
            vpUserSvcMockBuilder.VisitPayUserRepositoryMock.Setup(r => r.FindById(It.IsAny<string>())).Returns(vpUser);
            VisitPayUserService svc = vpUserSvcMockBuilder.CreateService();

            svc.GenerateAccessTokenPhiForUser(vpUser.VisitPayUserId);

            Assert.True(VisitPayUserAccessTokenPhi.IsValid(vpUser.AccessTokenPhi));
        }

        [Test]
        public void GenerateAccessTokenPhiForUser_VpUserWithInvalidAccessTokenPhi_NewTokenValueAssigned()
        {
            VisitPayUser vpUser = this.CreateTestUser(RandomStringGeneratorUtility.GetRandomString(10));
            const string originalInvalidValue = "bad123";
            vpUser.AccessTokenPhi = originalInvalidValue;

            VisitPayUserServiceMockBuilder vpUserSvcMockBuilder = this.CreateBuilder(null, vpUser.ToListOfOne(), null);
            vpUserSvcMockBuilder.VisitPayUserRepositoryMock.Setup(r => r.FindById(It.IsAny<string>())).Returns(vpUser);
            VisitPayUserService svc = vpUserSvcMockBuilder.CreateService();

            svc.GenerateAccessTokenPhiForUser(vpUser.VisitPayUserId);

            Assert.IsFalse(vpUser.AccessTokenPhi.Equals(originalInvalidValue));
            Assert.True(VisitPayUserAccessTokenPhi.IsValid(vpUser.AccessTokenPhi));
        }

        private VisitPayUserServiceMockBuilder CreateBuilder(string checkPassword, IList<VisitPayUser> users, IList<VisitPayUserPasswordHistory> passwordHistories = null)
        {
            VisitPayUserServiceMockBuilder builder = new VisitPayUserServiceMockBuilder();
            builder.Setup(b =>
            {
                b.VisitPayUserRepositoryMock.Setup(s => s.FindByNameAndDob(It.IsAny<string>(), It.IsAny<DateTime>())).Returns(users.ToList);
                b.VisitPayUserPasswordHistoryRepositoryMock.Setup(x => x.GetUserPasswordHistory(It.IsAny<int>(), It.IsAny<int>())).Returns(passwordHistories);
                b.PasswordHasherServiceMock.Setup(s => s.VerifyHashedPassword(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<HashEnum>())).Returns((string temp, string provided, int p3, int p4, HashEnum p5) => temp == checkPassword && provided == checkPassword);
            });

            return builder;
        }

        private VisitPayUser CreateTestUser(string tempPassword)
        {
            return new VisitPayUser
            {
                FirstName = "first",
                LastName = "last",
                DateOfBirth = new DateTime(1985, 05, 21),
                TempPassword = tempPassword,
                VisitPayUserId = 1
            };
        }
    }
}
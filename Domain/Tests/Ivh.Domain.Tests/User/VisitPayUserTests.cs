﻿namespace Ivh.Domain.User
{
    using Entities;
    using NUnit.Framework;
    using System.Collections.Generic;

    [TestFixture]
    public class VisitPayUserTests
    {
        /// <summary>
        /// While the result will vary every time,
        /// this test simply provides a sanity check to sample what values the method is producing.
        /// </summary>
        [Test]
        public void GenerateAndSetAccessTokenPhi_ResultIsWithinPermittedRange()
        {
            VisitPayUser user = new VisitPayUser();
            user.GenerateAndSetAccessTokenPhi();
            Assert.IsTrue(VisitPayUserAccessTokenPhi.IsValid(user.AccessTokenPhi));
        }

        [Test]
        public void AccessTokenPhiPropertySetter_ValidValue_ResultIsWithinPermittedRange()
        {
            const string legalValue = "12345678";
            VisitPayUser user = new VisitPayUser { AccessTokenPhi = legalValue };
            Assert.IsTrue(VisitPayUserAccessTokenPhi.IsValid(user.AccessTokenPhi));
        }

        [Test]
        public void AccessTokenPhiPropertySetter_InvalidValues_ResultIsNull()
        {
            // define invalid string values which cannot legally be assigned to AccessTokenPhi
            const string alphaLengthEqualsEight = "abcdefgh";
            const string alphaLengthGreaterThanEight = "invalid1234";
            const string numericLeadingZero = "00001234";
            const string numericGreaterThanMax = "100000000";

            List<string> invalidValues = new List<string>
            {
                alphaLengthEqualsEight,
                alphaLengthGreaterThanEight,
                numericLeadingZero,
                numericGreaterThanMax
            };

            foreach (string invalidValue in invalidValues)
            {
                VisitPayUser user = new VisitPayUser { AccessTokenPhi = invalidValue };
                Assert.IsNull(user.AccessTokenPhi);
            }
        }
    }
}

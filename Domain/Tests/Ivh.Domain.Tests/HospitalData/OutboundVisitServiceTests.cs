﻿namespace Ivh.Domain.HospitalData
{
    using System;
    using Common.Tests.Helpers.HospitalData;
    using Common.VisitPay.Enums;
    using Moq;
    using NUnit.Framework;
    using Visit.Entities;
    using Visit.Interfaces;

    [TestFixture]
    public class OutboundVisitServiceTests
    {
        [TestCase(VpOutboundVisitMessageTypeEnum.VisitAgingCountChange, true)]
        [TestCase(VpOutboundVisitMessageTypeEnum.VisitAgingCountChange, false)]
        [TestCase(VpOutboundVisitMessageTypeEnum.VisitAddedToFinancePlan, true)]
        [TestCase(VpOutboundVisitMessageTypeEnum.VisitAddedToFinancePlan, false)]
        public void OutboundMessage_SendsWhenAppropriate(VpOutboundVisitMessageTypeEnum messageType, bool isVisitPayAgingEnabled)
        {
            int visitId = 1;
            int billingSystemId = 1;
            string sourceSystemKey = nameof(sourceSystemKey);
            string outboundValue = ((int)AgingTierEnum.Good).ToString();
            
            Mock<IOutboundVisitRepository> outboundVisitRepositoryMock = new Mock<IOutboundVisitRepository>();
            Mock<IVisitService> visitServiceMock = new Mock<IVisitService>();
            visitServiceMock.Setup(x => x.GetVisit(billingSystemId, sourceSystemKey)).Returns(new Visit.Entities.Visit {
                
                BillingSystemId = billingSystemId,
                SourceSystemKey = sourceSystemKey
            });
            
            IOutboundVisitService svc = new OutboundVisitServiceMockBuilder().Setup(builder => { 
                builder.DomainServiceCommonServiceMockBuilder.FeatureServiceMock.Setup(x => x.IsFeatureEnabled(VisitPayFeatureEnum.FeatureVisitPayAgingIsEnabled)).Returns(isVisitPayAgingEnabled);
                builder.OutboundVisitRepositoryMock = outboundVisitRepositoryMock;
                builder.VisitServiceMock = visitServiceMock;
            }).CreateService();

            VpOutboundVisit vpOutboundVisit = new VpOutboundVisit
            {
                VpVisitId = visitId,
                BillingSystemId = billingSystemId,
                SourceSystemKey = sourceSystemKey,
                VpOutboundVisitMessageType = messageType,
                ActionDate = DateTime.UtcNow,
                OutboundValue = outboundValue
            };

            svc.CreateOutboundVisit(vpOutboundVisit);
            
            Times times = Times.Once();
            if (messageType == VpOutboundVisitMessageTypeEnum.VisitAgingCountChange && !isVisitPayAgingEnabled)
            {
                times = Times.Never();
            }
            visitServiceMock.Verify(x => x.GetVisit(It.IsAny<int>(), It.IsAny<string>()), times);
            outboundVisitRepositoryMock.Verify(x => x.InsertOrUpdate(It.IsAny<VpOutboundVisit>()), times);
        }
    }
}
﻿namespace Ivh.Domain.Inbound
{
    using System.Collections.Generic;
    using HospitalData.FileLoadTracking.Entities;
    using HospitalData.Inbound.Entities;
    using HospitalData.Inbound.Interfaces;
    using HospitalData.Inbound.Services;
    using NUnit.Framework;

    [TestFixture]
    public class InboundFileServiceTests
    {
        private IInboundFileService _inboundFileService;

        private IList<FileTypeConfigInbound> _fileTypeConfigInbounds;
        [SetUp]
        public void Setup()
        {

            this._inboundFileService = new InboundFileService(
                );

            this._fileTypeConfigInbounds = new List<FileTypeConfigInbound>
            {
                new FileTypeConfigInbound
                {
                    FileNamePattern = "PayorCategoryList",
                    IsActive = true,
                    FileType = new FileType {FileTypeId = 1000},
                    DestinationTable = "DestinationTable"
                }
            };

        }

        [Test]
        public void GetFiles()
        {
#if DEBUG
            string inboundStagingDirectory = @"C:\repos\visitpay\Utilities\PowerShell\DataIntegration2\TestFiles\THR\20171101";
            IList<FilesByDestination> filesByDestination = this._inboundFileService.GetFiles(this._fileTypeConfigInbounds, inboundStagingDirectory);
            Assert.AreEqual(1, filesByDestination.Count);
            Assert.AreEqual(1, filesByDestination[0].Files.Count);
            Assert.AreEqual("THR_Epic_VP_PayorCategoryList_001_20171101_1200.txt", filesByDestination[0].Files[0].FileName);
#endif
        }
    }
}
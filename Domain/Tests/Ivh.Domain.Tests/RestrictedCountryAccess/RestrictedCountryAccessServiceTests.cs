﻿namespace Ivh.Domain.RestrictedCountryAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Enums;
    using Common.Tests.Helpers.RestrictedCountryAccess;
    using Common.VisitPay.Enums;
    using Core.RestrictedCountryAccess.Entities;
    using Core.RestrictedCountryAccess.Interfaces;
    using NUnit.Framework;

    [TestFixture]
    public class RestrictedCountryAccessServiceTests : DomainTestBase
    {
        private const string RussianIp = "5.2.63.255";
        private const string RussianCountryCode = "RU";

        private List<RestrictedCountryAccess> listOfRestrictedCountryAccesses = new List<RestrictedCountryAccess>()
        {
            new RestrictedCountryAccess() { CountryCodeIso2 = RussianCountryCode, CountryName = ""}
        };

        private const string UsIp = "66.194.79.18";
        private const string UsCountryCode = "US";

        private const string InvalidIp = "222.";

        private IRestrictedCountryAccessService GetRestrictedCountryAccessService(bool enableFeature)
        {
            RestrictedCountryAccessServiceMockBuilder mock = new RestrictedCountryAccessServiceMockBuilder();
            mock.Setup(x =>
            {
                x.FeatureServiceMock.Setup(y => y.IsFeatureEnabled(VisitPayFeatureEnum.FeatureRestrictIpAddressByCountry)).Returns(enableFeature);
                x.CountryIpLookupProviderMock.Setup(y => y.GetCountryForIp(RussianIp)).Returns(RussianCountryCode);
                x.CountryIpLookupProviderMock.Setup(y => y.GetCountryForIp(UsIp)).Returns(UsCountryCode);
                x.CountryIpLookupProviderMock.Setup(y => y.GetCountryForIp(InvalidIp)).Returns(string.Empty);
                x.RestrictedCountryAccessRepositoryMock.Setup(y => y.GetQueryable()).Returns(this.listOfRestrictedCountryAccesses.AsQueryable());
            });

            IRestrictedCountryAccessService service = mock.CreateService();
            return service;
        }

        [Test]
        public void WithFeatureOffNeverRestricts()
        {
            IRestrictedCountryAccessService service = this.GetRestrictedCountryAccessService(false);

            bool russianResult = service.IsIpRestrictedByCountry(RussianIp);
            Assert.AreEqual(false, russianResult);

            bool usResult = service.IsIpRestrictedByCountry(UsIp);
            Assert.AreEqual(false, usResult);
        }

        [Test]
        public void RestrictsRussiaAndDoesntRestrictUs()
        {
            IRestrictedCountryAccessService service = this.GetRestrictedCountryAccessService(true);

            bool russianResult = service.IsIpRestrictedByCountry(RussianIp);
            Assert.AreEqual(true, russianResult);

            bool usResult = service.IsIpRestrictedByCountry(UsIp);
            Assert.AreEqual(false, usResult);
        }

        [Test]
        public void DoesntRestrictPrivateIpAddress()
        {
            IRestrictedCountryAccessService service = this.GetRestrictedCountryAccessService(true);

            bool privateResult = service.IsIpRestrictedByCountry("10.10.10.10");
            Assert.AreEqual(false, privateResult);

            privateResult = service.IsIpRestrictedByCountry("172.16.10.10");
            Assert.AreEqual(false, privateResult);

            privateResult = service.IsIpRestrictedByCountry("192.168.10.10");
            Assert.AreEqual(false, privateResult);
        }

        [Test]
        public void DoesntRestrictInvalidNullIpAddress()
        {
            IRestrictedCountryAccessService service = this.GetRestrictedCountryAccessService(true);

            bool invalidResult = service.IsIpRestrictedByCountry(null);
            Assert.AreEqual(false, invalidResult);

            invalidResult = service.IsIpRestrictedByCountry(InvalidIp);
            Assert.AreEqual(false, invalidResult);
        }

    }
}
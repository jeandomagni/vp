﻿namespace Ivh.Domain.Core.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Base.Interfaces;
    using IpAccess.Entities;
    using IpAccess.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Domain.Async;
    using Ivh.Domain.Core.ThirdPartyKey.Interfaces;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class ThirdPartyKeyService : DomainTestBase
    {
        private IThirdPartyKeyService _thirdPartyKeyService;
        private Mock<IDomainServiceCommonService> _domainServiceCommonService;
        private Mock<IThirdPartyKeyRepository> _thirdPartyKeyRepository;
        private IList<Ivh.Domain.Core.ThirdPartyKey.Entities.ThirdPartyKey> _allowedThirdPartyKeys;

        [SetUp]
        public void Setup()
        {
            this._allowedThirdPartyKeys = new List<Ivh.Domain.Core.ThirdPartyKey.Entities.ThirdPartyKey>();
            this._domainServiceCommonService = new Mock<IDomainServiceCommonService>();
            this._thirdPartyKeyRepository = new Mock<IThirdPartyKeyRepository>();
            this._thirdPartyKeyRepository.Setup(x => x.GetAllStateless()).Returns(() => this._allowedThirdPartyKeys.AsQueryable());
            this._thirdPartyKeyService = new Core.ThirdPartyKey.Services.ThirdPartyKeyService(new Lazy<IDomainServiceCommonService>(() => this._domainServiceCommonService.Object), this._thirdPartyKeyRepository.Object);
        }

        private void AddThirdPartyKey(string key, ThirdPartyKeyTypeEnum typeId, bool ignore)
        {
            if (!ignore)
            {
                this._allowedThirdPartyKeys.Add(new Ivh.Domain.Core.ThirdPartyKey.Entities.ThirdPartyKey() { Value = key, ThirdPartyKeyType = typeId });
            }
        }

        [TestCase(true, "12345", ThirdPartyKeyTypeEnum.CardReaderDeviceKey, false, "12345")]
        [TestCase(true, "", ThirdPartyKeyTypeEnum.ApiKey, true, "12345")]
        [TestCase(true, "12345", ThirdPartyKeyTypeEnum.ApiKey, false, "12345")]
        [TestCase(false, "12345", ThirdPartyKeyTypeEnum.CardReaderDeviceKey, false, "54321")]
        
        public void IsMachineKeyAllowed(bool allowed, string key, ThirdPartyKeyTypeEnum typeId, bool ignore, string passedKey)
        {
            this.AddThirdPartyKey(key, typeId, ignore);
            bool isAllowed = this._thirdPartyKeyService.IsCardReaderDeviceKeyAllowed(passedKey);
            Assert.AreEqual(allowed, isAllowed);
        }

        [TestCase(true, "12345", ThirdPartyKeyTypeEnum.UserAccount, false, "12345")]
        [TestCase(true, "", ThirdPartyKeyTypeEnum.ApiKey, true, "12345")]
        [TestCase(true, "12345", ThirdPartyKeyTypeEnum.ApiKey, false, "12345")]
        [TestCase(false, "12345", ThirdPartyKeyTypeEnum.UserAccount, false, "54321")]

        public void IsUserAccountAllowed(bool allowed, string userAccount, ThirdPartyKeyTypeEnum typeId, bool ignore, string passedUserAccount)
        {
            this.AddThirdPartyKey(userAccount, typeId, ignore);
            bool isAllowed = this._thirdPartyKeyService.IsCardReaderUserAccountAllowed(passedUserAccount);
            Assert.AreEqual(allowed, isAllowed);
        }
    }
}
﻿namespace Ivh.Domain.Core.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using IpAccess.Entities;
    using IpAccess.Interfaces;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class IpAccessService : DomainTestBase
    {
        private IIpAccessService _ipAccessService;
        private Mock<IDomainServiceCommonService> _domainServiceCommonService;
        private Mock<IIpAccessRepository> _ipAccessRepository;
        private IList<IpAccess> _allowedIpAccesses;

        [SetUp]
        public void Setup()
        {
            this._allowedIpAccesses = new List<IpAccess>();
            this._domainServiceCommonService = new Mock<IDomainServiceCommonService>();
            this._ipAccessRepository = new Mock<IIpAccessRepository>();
            this._ipAccessRepository.Setup(x => x.GetQueryable()).Returns(() => this._allowedIpAccesses.AsQueryable());
            this._ipAccessService = new Core.IpAccess.Services.IpAccessService(new Lazy<IDomainServiceCommonService>(() => this._domainServiceCommonService.Object), this._ipAccessRepository.Object);
        }

        private void AddIpAccess(string ipAddress, string ipAddressStartRange, string ipAddressEndRange)
        {
            this._allowedIpAccesses.Add(new IpAccess() { IpAddress = ipAddress, IpAddressStartRange = ipAddressStartRange, IpAddressEndRange = ipAddressEndRange });
        }

        [TestCase(true, "172.16.0.0", "172.16.0.0", "172.16.0.0", "172.31.255.255")]
        [TestCase(true, "172.31.255.255", "172.16.0.0", "172.16.0.0", "172.31.255.255")]
        [TestCase(true, "172.23.1.1", "172.16.0.0", "172.16.0.0", "172.31.255.255")]
        [TestCase(true, "172.23.1.1", "172.23.1.1", null, null)]
        [TestCase(false, "172.15.0.0", "172.16.0.0", "172.16.0.0", "172.31.255.255")]
        [TestCase(false, "172.32.255.255", "172.16.0.0", "172.16.0.0", "172.31.255.255")]
        [TestCase(false, "173.23.1.1", "172.16.0.0", "172.16.0.0", "172.31.255.255")]
        [TestCase(false, "173.23.1.1", "172.23.1.1", null, null)]
        public void IpAddressInRange(bool allowed, string addressToCheck, string ipAddress, string ipAddressStartRange, string ipAddressEndRange)
        {
            this.AddIpAccess(ipAddress, ipAddressStartRange, ipAddressEndRange);
            bool isAllowed = this._ipAccessService.IsIpAddressAllowed(addressToCheck);
            Assert.AreEqual(allowed, isAllowed);
        }
    }
}
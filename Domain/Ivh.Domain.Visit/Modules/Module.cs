﻿namespace Ivh.Domain.Visit.Modules
{
    using Autofac;
    using Interfaces;
    using Rules;
    using Services;
    using Services.HsResolutionServices;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<VisitStateRules>().AsSelf().SingleInstance();
            builder.RegisterType<VisitStateMachineService>().As<IVisitStateMachineService>();
            builder.RegisterType<VisitService>().As<IVisitService>();
            builder.RegisterType<VisitTransactionService>().As<IVisitTransactionService>();
            builder.RegisterType<VisitTransactionService>().As<IVisitTransactionWriteService>();
            builder.RegisterType<VisitStateHistoryService>().As<IVisitStateHistoryService>();
            builder.RegisterType<VisitAgingHistoryService>().As<IVisitAgingHistoryService>();
            builder.RegisterType<VisitPayUserIssueResultService>().As<IVisitPayUserIssueResultService>();
            builder.RegisterType<InsuranceService>().As<IInsuranceService>();
            builder.RegisterType<HsBillingSystemResolutionService>().As<IHsBillingSystemResolutionService>();
            builder.RegisterType<FacilityService>().As<IFacilityService>();
        }
    }
}

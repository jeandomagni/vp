﻿namespace Ivh.Domain.Visit.Unmatching.Services
{
    using System;
    using System.Web;
    using AutoMapper;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Constants;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.EventJournal;
    using Common.ServiceBus.Common;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Messages.Communication;
    using FileStorage.Entities;
    using FileStorage.Interfaces;
    using Guarantor.Entities;
    using Interfaces;
    using NHibernate;
    using User.Entities;
    using User.Interfaces;
    using Visit.Entities;
    using Visit.Interfaces;

    public class RedactionService : DomainService, IRedactionService
    {
        private readonly Lazy<IVisitService> _visitService;
        private readonly Lazy<IVisitTransactionWriteRepository> _visitTransactionRepository;
        private readonly Lazy<IVisitItemizationStorageRepository> _visitItemizationStorageRepository;
        private readonly Lazy<IFileStorageRepository> _fileStorageRepository;
        private readonly Lazy<IVisitPayUserJournalEventService> _userJournalEventService;
        private readonly ISession _session;

        public RedactionService(
            Lazy<IDomainServiceCommonService> domainServiceCommonService,
            Lazy<IVisitService> visitService,
            Lazy<IVisitTransactionWriteRepository> visitTransactionRepository,
            Lazy<IVisitItemizationStorageRepository> visitItemizationStorageRepository,
            Lazy<IFileStorageRepository> fileStorageRepository,
            Lazy<IVisitPayUserJournalEventService> userJournalEventService,
            ISessionContext<VisitPay> sessionContext) : base(domainServiceCommonService)
        {
            this._visitService = visitService;
            this._visitTransactionRepository = visitTransactionRepository;
            this._visitItemizationStorageRepository = visitItemizationStorageRepository;
            this._fileStorageRepository = fileStorageRepository;
            this._userJournalEventService = userJournalEventService;
            this._session = sessionContext.Session;
        }

        public void RedactVisit(string sourceSystemKey, int billingSystemId, VisitPayUser actionVisitPayUser)
        {
            Visit visit = this._visitService.Value.GetVisitBySystemSourceKey(sourceSystemKey, billingSystemId);
            if (visit != null)
            {
                this.RedactVisit(visit, actionVisitPayUser);
            }
        }

        public void RedactVisit(Visit visit, VisitPayUser actionVisitPayUser)
        {
            string replacementValue = this.Client.Value.RedactedVisitReplacementValue;
            RedactFieldAttribute.RedactFields(visit, replacementValue);

            foreach (VisitTransaction transaction in this._visitTransactionRepository.Value.GetVisitTransactions(visit.VpGuarantorId, visit.VisitId))
            {
                RedactFieldAttribute.RedactFields(transaction, replacementValue);
                this._visitTransactionRepository.Value.InsertOrUpdate(transaction);
            }

            foreach (HsGuarantorMap hsGuarantorMap in visit.VPGuarantor.HsGuarantorMaps)
            {
                VisitItemizationStorage visitItemizationStorage = this._visitItemizationStorageRepository.Value.GetItemizationForVisit(visit.VPGuarantor.VpGuarantorId, visit.SourceSystemKey, hsGuarantorMap.SourceSystemKey, 0);
                if (visitItemizationStorage != null)
                {
                    RedactFieldAttribute.RedactFields(visitItemizationStorage, replacementValue);
                    this._visitItemizationStorageRepository.Value.InsertOrUpdate(visitItemizationStorage);
                    this.RedactStoredFile(visitItemizationStorage.ExternalStorageKey, replacementValue);
                }
            }

            this._visitService.Value.SaveVisit(visit);

            this.LogVisitRedacted(actionVisitPayUser, visit.HsGuarantorMap.HsGuarantorId, visit.VPGuarantor.VpGuarantorId, visit.MatchedSourceSystemKey, visit.VisitId);

            this._session.RegisterPostCommitSuccessAction((vpgid, vid) =>
            {
                this.Bus.Value.PublishMessage(new SupportRequestRedactMessage
                {
                    VpGuarantorId = vpgid,
                    VisitId = vid
                }).Wait();
            }, visit.VPGuarantor.VpGuarantorId, visit.VisitId);
        }

        public void RedactStoredFile(Guid externalFileReference, string replacementValue = null)
        {
            FileStored fileStored = this._fileStorageRepository.Value.GetByKey(externalFileReference);

            if (fileStored != null)
            {
                RedactFieldAttribute.RedactFields(fileStored, replacementValue ?? this.Client.Value.RedactedVisitReplacementValue);
                fileStored.FileContents = new byte[] { };
                fileStored.RemoveDate = DateTime.UtcNow;
                fileStored.ExternalReferenceDate = null;
                this._fileStorageRepository.Value.InsertOrUpdate(fileStored);
            }
        }

        private void LogVisitRedacted(VisitPayUser actionVisitPayUser, int hsGuarantorId, int vpGuarantorId, string visitSourceSystemKey, int visitId)
        {
            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(Mapper.Map<JournalEventHttpContextDto>(HttpContext.Current), null);
            journalEventUserContext.UserName = (actionVisitPayUser == null) ? SystemUsers.SystemUserName : actionVisitPayUser.UserName;
            journalEventUserContext.EventVisitPayUserId = actionVisitPayUser?.VisitPayUserId ?? SystemUsers.SystemUserId;
            this._userJournalEventService.Value.LogVisitRedacted(journalEventUserContext, hsGuarantorId, vpGuarantorId, visitSourceSystemKey, visitId);
        }
    }
}
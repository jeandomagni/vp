﻿namespace Ivh.Domain.Visit.Unmatching.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using AutoMapper;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.EventJournal;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.FinanceManagement;
    using Common.VisitPay.Messages.HospitalData;
    using Entities;
    using FileStorage.Entities;
    using FileStorage.Interfaces;
    using Guarantor.Entities;
    using Guarantor.Interfaces;
    using Interfaces;
    using NHibernate;
    using User.Entities;
    using User.Interfaces;
    using Visit.Entities;
    using Visit.Interfaces;

    public class UnmatchingService : DomainService, IUnmatchingService
    {
        private readonly Lazy<IHsGuarantorMatchDiscrepancyRepository> _hsGuarantorMatchDiscrepancyRepository;
        private readonly Lazy<IVisitTransactionWriteRepository> _visitTransactionRepository;
        private readonly Lazy<IVisitRepository> _visitRepository;
        private readonly Lazy<IVisitStateService> _visitStateService;
        private readonly Lazy<IGuarantorRepository> _guarantorRepository;
        private readonly Lazy<IFileStorageRepository> _fileStorageRepository;
        private readonly ISession _session;
        private readonly Lazy<IRedactionService> _redactionService;
        private readonly Lazy<IVisitPayUserJournalEventService> _userJournalEventService;
        private readonly Lazy<IVisitItemizationStorageRepository> _visitItemizationStorageRepository;


        public UnmatchingService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IHsGuarantorMatchDiscrepancyRepository> hsGuarantorMatchDiscrepancyRepository,
            Lazy<IVisitRepository> visitRepository,
            Lazy<IRedactionService> redactionService,
            Lazy<IVisitPayUserJournalEventService> userJournalEventService,
            Lazy<IGuarantorRepository> guarantorRepository,
            Lazy<IVisitItemizationStorageRepository> visitItemizationStorageRepository,
            Lazy<IVisitTransactionWriteRepository> visitTransactionRepository,
            Lazy<IVisitStateService> visitStateService,
            Lazy<IFileStorageRepository> fileStorageRepository,
            ISessionContext<VisitPay> session) : base(serviceCommonService)
        {
            this._hsGuarantorMatchDiscrepancyRepository = hsGuarantorMatchDiscrepancyRepository;
            this._redactionService = redactionService;
            this._userJournalEventService = userJournalEventService;
            this._visitRepository = visitRepository;
            this._guarantorRepository = guarantorRepository;
            this._session = session.Session;
            this._fileStorageRepository = fileStorageRepository;
            this._visitStateService = visitStateService;
            this._visitTransactionRepository = visitTransactionRepository;
            this._visitItemizationStorageRepository = visitItemizationStorageRepository;
        }

        public HsGuarantorMatchDiscrepancyResults GetAllHsGuarantorMatchDiscrepancies(HsGuarantorMatchDiscrepancyFilter filter, bool attributeChangeOnly, int pageNumber, int pageSize)
        {
            return this._hsGuarantorMatchDiscrepancyRepository.Value.GetAllHsGuarantorMatchDiscrepancies(filter, attributeChangeOnly, pageNumber, pageSize);
        }

        public VisitUnmatchResults GetAllVisitUnmatches(VisitUnmatchFilter filter, int pageNumber, int pageSize)
        {
            return this._visitRepository.Value.GetAllVisitUnmatches(filter, pageNumber, pageSize);
        }

        public IReadOnlyList<HsGuarantorMatchDiscrepancyStatus> GetAllHsGuarantorMatchDiscrepancyStatuses()
        {
            return this._hsGuarantorMatchDiscrepancyRepository.Value.GetAllHsGuarantorMatchDiscrepancyStatuses();
        }

        public void QueueHsGuarantorMatchDiscrepancy(HsGuarantorMatchDiscrepancy hsGuarantorMatchDiscrepancy)
        {
            this._hsGuarantorMatchDiscrepancyRepository.Value.Insert(hsGuarantorMatchDiscrepancy);
        }

        public HsGuarantorMatchDiscrepancy GetHsGuarantorMatchDiscrepancy(HsGuarantorMap hsGuarantorMap, int hsGuarantorMatchDiscrepancyId)
        {
            return this._hsGuarantorMatchDiscrepancyRepository.Value.GetHsGuarantorMatchDiscrepancy(hsGuarantorMap.HsGuarantorId, hsGuarantorMap.VpGuarantor.VpGuarantorId, hsGuarantorMatchDiscrepancyId);
        }

        public HsGuarantorMatchDiscrepancy GetOpenHsGuarantorMatchDiscrepancy(int hsGuarantorId, int vpGuarantorId, int? hsGuarantorMatchDiscrepancyId = null)
        {
            return this._hsGuarantorMatchDiscrepancyRepository.Value.GetHsGuarantorMatchDiscrepancy(hsGuarantorId, vpGuarantorId, hsGuarantorMatchDiscrepancyId, HsGuarantorMatchDiscrepancyStatusEnum.Open);
        }

        public IList<HsGuarantorMatchDiscrepancy> GetHsGuarantorMatchDiscrepancies(HsGuarantorMap hsGuarantorMap)
        {
            return this._hsGuarantorMatchDiscrepancyRepository.Value.GetHsGuarantorMatchDiscrepancies(hsGuarantorMap.HsGuarantorId, hsGuarantorMap.VpGuarantor.VpGuarantorId);
        }

        public void UpdateHsGuarantorMatchDiscrepancy(HsGuarantorMatchDiscrepancy hsGuarantorMatchDiscrepancy)
        {
            this._hsGuarantorMatchDiscrepancyRepository.Value.InsertOrUpdate(hsGuarantorMatchDiscrepancy);
        }

        public void UnmatchVisit(HsGuarantorMap hsGuarantorMap, int visitId, HsGuarantorMatchStatusEnum hsGuarantorMatchStatus, HsGuarantorUnmatchReasonEnum hsGuarantorUnmatchReasonEnum, VisitPayUser actionVisitPayUser = null, string actionNotes = null)
        {
            Visit visit = this._visitRepository.Value.GetVisit(hsGuarantorMap.VpGuarantor.VpGuarantorId, visitId);
            this.UnmatchVisit(hsGuarantorMap, visit, hsGuarantorMatchStatus, hsGuarantorUnmatchReasonEnum, actionVisitPayUser, actionNotes);
        }

        public void UnmatchVisit(HsGuarantorMap hsGuarantorMap, Visit visit, HsGuarantorMatchStatusEnum hsGuarantorMatchStatus, HsGuarantorUnmatchReasonEnum hsGuarantorUnmatchReasonEnum, VisitPayUser actionVisitPayUser = null, string actionNotes = null)
        {
            if (visit == null || (visit.HsGuarantorMap != null && visit.HsGuarantorMap.HsGuarantorId != hsGuarantorMap.HsGuarantorId))
            {
                throw new ArgumentOutOfRangeException(nameof(visit), $"Invalid Visit => VpGuarantorId: {hsGuarantorMap.VpGuarantor.VpGuarantorId}; HsGuarantorId: {hsGuarantorMap.HsGuarantorId}; VisitId: {visit}");
            }
            
            if (visit.IsUnmatched)
            {
                return;
            }
            
            string sourceSystemKey = visit.SourceSystemKey;

            this._session.Transaction.RegisterPostCommitSuccessAction((vpgid, hsgid, bsid, ssk, hsgms) =>
            {
                this.Bus.Value.PublishMessage(new HsGuarantorVisitUnmatchMessage()
                {
                    VpGuarantorId = vpgid,
                    HsGuarantorId = hsgid,
                    BillingSystemId = bsid,
                    SourceSystemKey = ssk,
                    HsGuarantorMatchStatus = hsgms,
                    VpVisitId = visit.VisitId,
                    ActionVisitPayUserId = actionVisitPayUser?.VisitPayUserId
                }).Wait();
            }, hsGuarantorMap.VpGuarantor.VpGuarantorId, hsGuarantorMap.HsGuarantorId, visit.BillingSystem.BillingSystemId, sourceSystemKey, hsGuarantorMatchStatus);

            this.UnmatchVisit(visit, actionVisitPayUser);

            visit.HsGuarantorUnmatchReason = new HsGuarantorUnmatchReason() { HsGuarantorUnmatchReasonEnum = hsGuarantorUnmatchReasonEnum };
            visit.HsGuarantorMatchStatus = hsGuarantorMatchStatus;
            visit.UnmatchActionDate = DateTime.UtcNow;
            visit.UnmatchActionVisitPayUser = new VisitPayUser { VisitPayUserId = actionVisitPayUser?.VisitPayUserId ?? SystemUsers.SystemUserId };
            visit.UnmatchActionNotes = actionNotes;
            visit.IsUnmatched = true;

            this.LogVisitUnmatch(actionVisitPayUser, hsGuarantorMap.HsGuarantorId, hsGuarantorMap.VpGuarantor.VpGuarantorId, sourceSystemKey, visit.VisitId);


        }

        private void UnmatchVisit(Visit visit, VisitPayUser visitPayUser)
        {
            // redact the visit, transactions and itemizations
            this._redactionService.Value.RedactVisit(visit, visitPayUser);

            // unmatch the visit, transactions and itemizations
            visit.MatchedBillingSystem = visit.BillingSystem;
            visit.MatchedSourceSystemKey = visit.SourceSystemKey;

            foreach (VisitTransaction transaction in this._visitTransactionRepository.Value.GetVisitTransactions(visit.VpGuarantorId, visit.VisitId))
            {
                transaction.MatchedBillingSystem = transaction.BillingSystem;
                transaction.MatchedSourceSystemKey = transaction.SourceSystemKey;
                transaction.BillingSystem = null;
                transaction.SetSourceSystemKey(null);
                this._visitTransactionRepository.Value.InsertOrUpdate(transaction);
            }

            foreach (HsGuarantorMap hsGuarantorMap in visit.VPGuarantor.HsGuarantorMaps)
            {
                VisitItemizationStorage visitItemizationStorage = this._visitItemizationStorageRepository.Value.GetItemizationForVisit(visit.VPGuarantor.VpGuarantorId, visit.SourceSystemKey, hsGuarantorMap.SourceSystemKey, 0);
                if (visitItemizationStorage != null)
                {
                    visitItemizationStorage.MatchedEmpi = visitItemizationStorage.Empi;
                    visitItemizationStorage.MatchedAccountNumber = visitItemizationStorage.AccountNumber;
                    visitItemizationStorage.MatchedVisitSourceSystemKeyDisplay = visitItemizationStorage.VisitSourceSystemKeyDisplay;
                    visitItemizationStorage.DateUnmatched = DateTime.UtcNow;

                    this._visitItemizationStorageRepository.Value.InsertOrUpdate(visitItemizationStorage);

                    FileStored fileStored = this._fileStorageRepository.Value.GetByKey(visitItemizationStorage.ExternalStorageKey);

                    if (fileStored != null)
                    {
                        fileStored.FileContents = new byte[] { };
                        fileStored.RemoveDate = DateTime.UtcNow;
                        fileStored.ExternalReferenceDate = null;
                        this._fileStorageRepository.Value.InsertOrUpdate(fileStored);
                    }
                }
            }

            visit.BillingSystem = new BillingSystem { BillingSystemId = visit.BillingSystem.BillingSystemId * -1 }; //unmatched billing system id is the negative value of the billing system id
            visit.SourceSystemKey = null;

            // we need to notify that the visit has been changed for the VpGuarantor
            this._session.Transaction.RegisterPostCommitSuccessAction(() =>
            {
                ChangeEventProcessedForVpGuarantor message = new ChangeEventProcessedForVpGuarantor()
                {
                    ChangeSetId = 0,
                    TypeOfChangeSet = nameof(UnmatchingService),
                    VpGuarantorIds = new List<int>() { visit.VpGuarantorId },
                    VisitIds = new List<int>() { visit.VisitId }
                };
                this.Bus.Value.PublishMessage(message).Wait();
            });

            this._visitStateService.Value.EvaluateVisitState(visit);
            this._visitRepository.Value.InsertOrUpdate(visit);
        }

        public void UnmatchHsGuarantor(HsGuarantorMap hsGuarantorMap, HsGuarantorMatchStatusEnum hsGuarantorMatchStatus, HsGuarantorUnmatchReasonEnum visitUnmatchReason, VisitPayUser actionVisitPayUser = null, string actionNotes = null)
        {
            Guarantor vpGuarantor = this._guarantorRepository.Value.GetById(hsGuarantorMap.VpGuarantor.VpGuarantorId);
            hsGuarantorMap = vpGuarantor.HsGuarantorMaps.FirstOrDefault(
                x => x.HsGuarantorMapId == hsGuarantorMap.HsGuarantorMapId
                    && x.HsGuarantorId == hsGuarantorMap.HsGuarantorId
                    && x.VpGuarantor.VpGuarantorId == vpGuarantor.VpGuarantorId);

            if (hsGuarantorMap == null)
                return;

            hsGuarantorMap.HsGuarantorMatchStatus = hsGuarantorMatchStatus;
            this._guarantorRepository.Value.InsertOrUpdate(vpGuarantor);

            // GET VISITS
            foreach (Visit visit in this._visitRepository.Value.GetVisitsForGuarantor(hsGuarantorMap.VpGuarantor.VpGuarantorId).Where(x => x.HsGuarantorMap != null && x.HsGuarantorMap.HsGuarantorMapId == hsGuarantorMap.HsGuarantorMapId))
            {
                this.UnmatchVisit(hsGuarantorMap, visit.VisitId, hsGuarantorMatchStatus, visitUnmatchReason, actionVisitPayUser, actionNotes);
            }

            this._session.RegisterPostCommitSuccessAction((vpgid, hsgid, hsgms) =>
            {
                this.Bus.Value.PublishMessage(new HsGuarantorUnmatchMessage()
                {
                    VpGuarantorId = vpgid,
                    HsGuarantorId = hsgid,
                    HsGuarantorMatchStatus = hsgms
                }).Wait();
            }, hsGuarantorMap.VpGuarantor.VpGuarantorId, hsGuarantorMap.HsGuarantorId, hsGuarantorMatchStatus);

            this.LogGuarantorUnmatch(actionVisitPayUser, hsGuarantorMap.HsGuarantorId, hsGuarantorMap.VpGuarantor.VpGuarantorId);
        }

        private void LogGuarantorUnmatch(VisitPayUser actionVisitPayUser, int hsGuarantorId, int vpGuarantorId)
        {
            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(Mapper.Map<JournalEventHttpContextDto>(HttpContext.Current), actionVisitPayUser);
            this._userJournalEventService.Value.LogGuarantorUnmatch(journalEventUserContext, hsGuarantorId, vpGuarantorId);
        }

        private void LogVisitUnmatch(VisitPayUser actionVisitPayUser, int hsGuarantorId, int vpGuarantorId, string visitSourceSystemKey, int visitId)
        {
            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(Mapper.Map<JournalEventHttpContextDto>(HttpContext.Current), actionVisitPayUser);
            this._userJournalEventService.Value.LogVisitUnmatch(journalEventUserContext, hsGuarantorId, vpGuarantorId, visitSourceSystemKey, visitId);
        }
    }
}
﻿namespace Ivh.Domain.Visit.Unmatching.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IHsGuarantorMatchDiscrepancyRepository : IRepository<HsGuarantorMatchDiscrepancy>
    {
        IReadOnlyList<HsGuarantorMatchDiscrepancyStatus> GetAllHsGuarantorMatchDiscrepancyStatuses();
        HsGuarantorMatchDiscrepancyResults GetAllHsGuarantorMatchDiscrepancies(HsGuarantorMatchDiscrepancyFilter filter, bool attributeChangeOnly, int pageNumber, int pageSize);
        IList<HsGuarantorMatchDiscrepancy> GetHsGuarantorMatchDiscrepancies(int hsGuarantorId, int vpGuarantorId);
        HsGuarantorMatchDiscrepancy GetHsGuarantorMatchDiscrepancy(int hsGuarantorId, int vpGuarantorId, int? hsGuarantorMatchDiscrepancyId = null, HsGuarantorMatchDiscrepancyStatusEnum? status = null);
    }
}
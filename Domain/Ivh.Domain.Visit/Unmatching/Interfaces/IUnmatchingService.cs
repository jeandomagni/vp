﻿namespace Ivh.Domain.Visit.Unmatching.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;
    using Guarantor.Entities;
    using User.Entities;
    using Visit.Entities;
    using Visit.Interfaces;

    public interface IUnmatchingService : IDomainService
    {
        void QueueHsGuarantorMatchDiscrepancy(HsGuarantorMatchDiscrepancy hsGuarantorMatchDiscrepancy);
        void UpdateHsGuarantorMatchDiscrepancy(HsGuarantorMatchDiscrepancy hsGuarantorMatchDiscrepancy);
        HsGuarantorMatchDiscrepancy GetHsGuarantorMatchDiscrepancy(HsGuarantorMap hsGuarantorMap, int hsGuarantorMatchDiscrepancyId);
        HsGuarantorMatchDiscrepancy GetOpenHsGuarantorMatchDiscrepancy(int hsGuarantorId, int vpGuarantorId, int? hsGuarantorMatchDiscrepancyId = null);
        HsGuarantorMatchDiscrepancyResults GetAllHsGuarantorMatchDiscrepancies(HsGuarantorMatchDiscrepancyFilter filter, bool attributeChangeOnly, int pageNumber, int pageSize);
        VisitUnmatchResults GetAllVisitUnmatches(VisitUnmatchFilter filter, int pageNumber, int pageSize);
        IReadOnlyList<HsGuarantorMatchDiscrepancyStatus> GetAllHsGuarantorMatchDiscrepancyStatuses();
        IList<HsGuarantorMatchDiscrepancy> GetHsGuarantorMatchDiscrepancies(HsGuarantorMap hsGuarantorMap);
        void UnmatchVisit(HsGuarantorMap hsGuarantorMap, Visit visit, HsGuarantorMatchStatusEnum hsGuarantorMatchStatus, HsGuarantorUnmatchReasonEnum visitUnmatchReason, VisitPayUser actionVisitPayUser = null, string actionNotes = null);
        void UnmatchVisit(HsGuarantorMap hsGuarantorMap, int visitId, HsGuarantorMatchStatusEnum hsGuarantorMatchStatus, HsGuarantorUnmatchReasonEnum visitUnmatchReason, VisitPayUser actionVisitPayUser = null, string actionNotes = null);
        void UnmatchHsGuarantor(HsGuarantorMap hsGuarantorMap, HsGuarantorMatchStatusEnum hsGuarantorMatchStatus, HsGuarantorUnmatchReasonEnum visitUnmatchReason, VisitPayUser actionVisitPayUser = null, string actionNotes = null);
    }
}
﻿namespace Ivh.Domain.Visit.Unmatching.Interfaces
{
    using System;
    using Common.Base.Interfaces;
    using User.Entities;
    using Visit.Entities;

    public interface IRedactionService : IDomainService
    {
        void RedactVisit(string sourceSystemKey, int billingSystemId, VisitPayUser actionVisitPayUser);
        void RedactVisit(Visit visit, VisitPayUser actionVisitPayUser);
        void RedactStoredFile(Guid externalFileReference, string replacementValue = null);
    }
}
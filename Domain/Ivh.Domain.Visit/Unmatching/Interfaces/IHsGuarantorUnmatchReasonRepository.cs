﻿namespace Ivh.Domain.Visit.Unmatching.Interfaces
{
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IHsGuarantorUnmatchReasonRepository : IRepository<HsGuarantorUnmatchReason>
    {
        HsGuarantorUnmatchReason GetById(HsGuarantorUnmatchReasonEnum visitUnmatchReasonEnum);
    }
}
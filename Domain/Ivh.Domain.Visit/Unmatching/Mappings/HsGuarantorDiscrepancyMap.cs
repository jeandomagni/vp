﻿namespace Ivh.Domain.Visit.Unmatching.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class HsGuarantorMatchDiscrepancyMap : ClassMap<HsGuarantorMatchDiscrepancy>
    {
        public HsGuarantorMatchDiscrepancyMap()
        {
            this.Schema("dbo");
            this.Table("HsGuarantorMatchDiscrepancy");
            this.Id(x => x.HsGuarantorMatchDiscrepancyId);
            this.Map(x => x.InsertDate);
            this.References(x => x.VpGuarantor, "VpGuarantorId").Not.Nullable();
            this.References(x => x.HsGuarantorMap, "HsGuarantorMapId").Not.Nullable();
            this.Map(x => x.HsGuarantorMatchDiscrepancyStatus, "HsGuarantorMatchDiscrepancyStatusId").CustomType<HsGuarantorMatchDiscrepancyStatusEnum>();
            this.References(x => x.HsGuarantorMatchDiscrepancyStatusSource).Column("HsGuarantorMatchDiscrepancyStatusId").Not.Nullable().Not.Insert().Not.Update();
            this.Map(x => x.ActionDate).Nullable();
            this.References(x => x.ActionVisitPayUser, "ActionVisitPayUserId").Nullable();
            this.Map(x => x.ActionNotes).Nullable();
            this.HasMany(x => x.HsGuarantorMatchDiscrepancyMatchFields)
                .KeyColumn("HsGuarantorMatchDiscrepancyId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .Cascade.AllDeleteOrphan();

            this.References(x => x.HsGuarantorUnmatchReason)
                .Column("HsGuarantorUnmatchReasonId")
               .Nullable();
        }
    }
}
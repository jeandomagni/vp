﻿namespace Ivh.Domain.Visit.Unmatching.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class HsGuarantorMatchDiscrepancyMatchFieldMap : ClassMap<HsGuarantorMatchDiscrepancyMatchField>
    {
        public HsGuarantorMatchDiscrepancyMatchFieldMap()
        {
            this.Schema("dbo");
            this.Table("HsGuarantorMatchDiscrepancyMatchField");
            this.Id(x => x.HsGuarantorMatchDiscrepancyMatchFieldId);
            this.Map(x => x.InsertDate);
            this.Map(x => x.HsGuarantorMatchDiscrepancyMatchFieldName).Not.Nullable();
            this.Map(x => x.HsGuarantorMatchDiscrepancyMatchFieldCurrentValue).Nullable();
            this.Map(x => x.HsGuarantorMatchDiscrepancyMatchFieldNewValue).Nullable();
            this.References(x => x.HsGuarantorMatchDiscrepancy, "HsGuarantorMatchDiscrepancyId");
        }
    }
}
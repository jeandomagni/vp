﻿namespace Ivh.Domain.Visit.Unmatching.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class HsGuarantorMatchDiscrepancyStatusMap : ClassMap<HsGuarantorMatchDiscrepancyStatus>
    {
        public HsGuarantorMatchDiscrepancyStatusMap()
        {
            this.Schema(Schemas.VisitPay.Dbo);
            this.Table(nameof(HsGuarantorMatchDiscrepancyStatus));
            this.ReadOnly();
            this.Id(x => x.HsGuarantorMatchDiscrepancyStatusId);
            this.Map(x => x.HsGuarantorMatchDiscrepancyStatusName).Not.Nullable();
        }
    }
}
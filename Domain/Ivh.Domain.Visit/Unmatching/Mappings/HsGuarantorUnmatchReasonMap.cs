namespace Ivh.Domain.Visit.Unmatching.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class HsGuarantorUnmatchReasonMap : ClassMap<HsGuarantorUnmatchReason>
    {
        public HsGuarantorUnmatchReasonMap()
        {
            this.Schema("dbo");
            this.Table("HsGuarantorUnmatchReason");
            this.Cache.ReadOnly();
            this.Id(x => x.HsGuarantorUnmatchReasonId);
            this.Map(x => x.HsGuarantorUnmatchReasonName).Not.Nullable();
            this.Map(x => x.HsGuarantorUnmatchReasonDisplayName).Not.Nullable();
        }
    }
}
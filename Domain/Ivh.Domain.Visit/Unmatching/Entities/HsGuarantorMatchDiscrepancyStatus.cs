﻿namespace Ivh.Domain.Visit.Unmatching.Entities
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class HsGuarantorMatchDiscrepancyStatus
    {
        public virtual int HsGuarantorMatchDiscrepancyStatusId { get; set; }
        public virtual string HsGuarantorMatchDiscrepancyStatusName { get; set; }

        public virtual HsGuarantorMatchDiscrepancyStatusEnum HsGuarantorMatchDiscrepancyStatusEnum
        {
            get { return (HsGuarantorMatchDiscrepancyStatusEnum) this.HsGuarantorMatchDiscrepancyStatusId; }
        }
    }
}
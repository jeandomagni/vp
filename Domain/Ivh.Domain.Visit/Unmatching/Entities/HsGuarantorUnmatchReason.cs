﻿namespace Ivh.Domain.Visit.Unmatching.Entities
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class HsGuarantorUnmatchReason
    {
        public virtual int HsGuarantorUnmatchReasonId { get; set; }

        public virtual HsGuarantorUnmatchReasonEnum HsGuarantorUnmatchReasonEnum
        {
            get { return (HsGuarantorUnmatchReasonEnum)this.HsGuarantorUnmatchReasonId; }
            set { this.HsGuarantorUnmatchReasonId = (int)value; }
        }
        public virtual string HsGuarantorUnmatchReasonName { get; set; }
        public virtual string HsGuarantorUnmatchReasonDisplayName { get; set; }

    }
}

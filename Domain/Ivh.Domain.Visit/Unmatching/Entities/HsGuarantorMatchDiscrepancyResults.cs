﻿namespace Ivh.Domain.Visit.Unmatching.Entities
{
    using System.Collections.Generic;

    public class HsGuarantorMatchDiscrepancyResults
    {
        public IReadOnlyList<HsGuarantorMatchDiscrepancy> HsGuarantorMatchDiscrepancies { get; set; }
        public int TotalRecords { get; set; }
    }
}
﻿namespace Ivh.Domain.Visit.Unmatching.Entities
{
    using System;

    public class HsGuarantorMatchDiscrepancyMatchField
    {
        public HsGuarantorMatchDiscrepancyMatchField()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int HsGuarantorMatchDiscrepancyMatchFieldId { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual string HsGuarantorMatchDiscrepancyMatchFieldName { get; set; }
        public virtual string HsGuarantorMatchDiscrepancyMatchFieldCurrentValue { get; set; }
        public virtual string HsGuarantorMatchDiscrepancyMatchFieldNewValue { get; set; }
        public virtual HsGuarantorMatchDiscrepancy HsGuarantorMatchDiscrepancy { get; set; }
    }
}
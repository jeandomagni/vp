﻿namespace Ivh.Domain.Visit.Unmatching.Entities
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Guarantor.Entities;
    using User.Entities;

    public class HsGuarantorMatchDiscrepancy
    {
        public HsGuarantorMatchDiscrepancy()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int HsGuarantorMatchDiscrepancyId { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual Guarantor VpGuarantor { get; set; }
        public virtual HsGuarantorMap HsGuarantorMap { get; set; }
        public virtual HsGuarantorMatchDiscrepancyStatusEnum HsGuarantorMatchDiscrepancyStatus { get; set; }
        public virtual DateTime? ActionDate { get; set; }
        public virtual VisitPayUser ActionVisitPayUser { get; set; }
        public virtual string ActionNotes { get; set; }
        public virtual HsGuarantorMatchDiscrepancyStatus HsGuarantorMatchDiscrepancyStatusSource { get; set; }
        public virtual IList<HsGuarantorMatchDiscrepancyMatchField> HsGuarantorMatchDiscrepancyMatchFields { get; set; }
        public virtual HsGuarantorUnmatchReason HsGuarantorUnmatchReason { get; set; }
    }
}
﻿namespace Ivh.Domain.Visit.Entities
{
	/// <summary>
	/// This is a mapping class that is used to associate the <see cref="CmsRegionId"/> to the state specific RIC.
	/// </summary>
	public class State
	{
		public virtual string StateCode { get; set; }

		public virtual int CmsRegionId { get; set; }
		
	}
}

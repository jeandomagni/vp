﻿namespace Ivh.Domain.Visit.Entities
{
    using System;
    using User.Entities;

    public class BalanceTransferStatusHistory
    {
        public BalanceTransferStatusHistory()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual Visit Visit { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual BalanceTransferStatus BalanceTransferStatus { get; set; }
        public virtual int BalanceTransferStatusHistoryId { get; set; }
        public virtual VisitPayUser ChangedBy { get; set; }
        public virtual string Notes { get; set; }
    }
}
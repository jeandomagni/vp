﻿namespace Ivh.Domain.Visit.Entities
{
    public class VisitInsurancePlan
    {
        public virtual int VisitInsurancePlanId { get; set; }
        public virtual Visit Visit { get; set; }
        public virtual int PayOrder { get; set; }
        public virtual InsurancePlan InsurancePlan { get; set; }
    }
}
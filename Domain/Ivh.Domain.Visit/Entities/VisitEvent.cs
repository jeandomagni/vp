﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.Visit.Entities
{
    using Common.VisitPay.Enums;

    public class VisitEvent
    {
        public virtual int VisitEventId { get; set; }
        public virtual int VisitId { get; set; }
        public virtual int? CommunicationTypeId { get; set; }
        public virtual int? VpOutboundVisitMessageTypeId { get; set; }
        public virtual string AdditionalData { get; set; }
        public virtual DateTime ActionDate { get; set; }
    }
}

﻿namespace Ivh.Domain.Visit.Entities
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// readonly copy of HS Visit data
    /// </summary>
    public class BaseVisit
    {
        private IList<BaseVisitTransaction> _visitTransactions;

        public BaseVisit()
        {
            this._visitTransactions = new List<BaseVisitTransaction>();
        }

        public virtual int VisitId { get; set; }
        public virtual int HsGuarantorId { get; set; }
        public virtual string PatientFirstName { get; set; }
        public virtual string PatientLastName { get; set; }
        public virtual int BillingSystemId { get; set; }
        public virtual string SourceSystemKey { get; set; }
        public virtual string SourceSystemKeyDisplay { get; set; }
        public virtual DateTime? AdmitDate { get; set; }
        public virtual DateTime? DischargeDate { get; set; }
        public virtual DateTime? VisitDate => this.DischargeDate ?? this.AdmitDate;
        public virtual string VisitDescription { get; set; }
        public virtual bool IsPatientGuarantor { get; set; }
        public virtual bool IsPatientMinor { get; set; }

        public virtual IList<BaseVisitTransaction> VisitTransactions
        {
            get => this._visitTransactions;
            set => this._visitTransactions = value;
        }

            
    }
}

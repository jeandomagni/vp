namespace Ivh.Domain.Visit.Entities
{
    using System;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using User.Entities;

    public class VisitAgingHistory
    {
        public VisitAgingHistory()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        public virtual int VisitAgingHistoryId { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual AgingTierEnum AgingTier { get; set; }
        public virtual Visit Visit { get; set; }

        public virtual string ChangeDescription { get; set; }
        public virtual VisitPayUser VisitPayUser { get; set; }
    }
}
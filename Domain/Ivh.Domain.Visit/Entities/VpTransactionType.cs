﻿namespace Ivh.Domain.Visit.Entities
{
    using System;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;

    public class VpTransactionType
    {
        public virtual int VpTransactionTypeId { get; set; }

        public virtual VpTransactionTypeEnum VpTransactionTypeEnum => (VpTransactionTypeEnum)this.VpTransactionTypeId;

        public virtual string TransactionType { get; set; }
        public virtual string TransactionGroup { get; set; }
        public virtual string VpTransactionTypeName { get; set; }
        public virtual string DisplayName { get; set; }
        public virtual string TransactionSource { get; set; }
        public virtual string Notes { get; set; }

        public virtual bool IsInterestAssessed()
        {
            return this.VpTransactionTypeEnum == VpTransactionTypeEnum.HsInterestAssessed
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestCharge
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestWriteoff
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestAssessmentReallocation
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestAssessmentReversal;
        }

        public virtual bool IsVpInterestAssessed()
        {
            return this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestCharge
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestWriteoff
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestAssessmentReallocation
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestAssessmentReversal;
        }

        /// <summary>
        /// Checks for any type that is a reversal
        /// </summary>
        /// <returns></returns>
        public virtual bool IsReversal()
        {
            return this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppDiscountHsReversal
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppPrincipalPaymentHsReversal
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestPaymentHsReversal
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestPaymentReassessmentReversal
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppPrincipalPaymentReassessmentReversal
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestAssessmentReversal;

        }

        /// <summary>
        /// This is all interest, payments, adjustments, etc.  So you would use this to calculate interest due.
        /// </summary>
        public virtual bool IsInterest()
        {
            return this.VpTransactionTypeEnum == VpTransactionTypeEnum.HsInterestAssessed
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.HsInterestPayment
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestCharge
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestPaymentHsReallocation
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestPaymentHsReversal
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestPayment
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestWriteoff
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestAssessmentReallocation
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestAssessmentReversal
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestPaymentReassessmentReversal
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestPaymentReassessmentReallocation;

        }

        [Obsolete(ObsoleteDescription.VisitTransactionVisitPayInterestPayment)]
        public virtual bool IsInterestPayment()
        {
            return this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestPaymentHsReallocation
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestPaymentHsReversal
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestPayment
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestPaymentReassessmentReallocation
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestPaymentReassessmentReversal
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.HsInterestPayment;
        }

        public virtual bool IsInterestWriteOff()
        {
            return this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestWriteoff;
        }

        public virtual bool IsPrincipal()
        {
            return !this.IsInterest();
        }

        [Obsolete(ObsoleteDescription.VisitTransactions)]
        public virtual bool IsPrincipalPayment()
        {
            return this.VpTransactionTypeEnum == VpTransactionTypeEnum.HsPatientCash
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppPrincipalPaymentHsReallocation
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppPrincipalPaymentHsReversal
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppPrincipalPayment
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppPrincipalPaymentReassessmentReallocation
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppPrincipalPaymentReassessmentReversal;

        }

        public virtual bool IsAdjustment()
        {
            return this.VpTransactionTypeEnum == VpTransactionTypeEnum.HsPayorCash
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.HsPayorNonCash
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.HsCharge
                   || this.IsHsCharity()
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.HsPatientNonCash
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.OtherTransaction
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.HsWriteoffTransaction;

        }

        /// <summary>
        /// This is an insurance payment
        /// </summary>
        /// <returns></returns>
        public virtual bool IsPayorCash()
        {
            return this.VpTransactionTypeEnum == VpTransactionTypeEnum.HsPayorCash;
        }

        [Obsolete(ObsoleteDescription.VisitTransactionVisitPayInterestPayment)]
        public virtual bool IsVpDiscount()
        {
            return this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppDiscountHsReversal
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppDiscount
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppDiscountHsReallocation;
        }

        public virtual bool IsHs()
        {
            return this.IsHsAdjustment() || this.IsHsCharge() || this.IsHsPayment() || this.IsHsInterest();
        }

        public virtual bool IsHsPayment()
        {
            return this.VpTransactionTypeEnum == VpTransactionTypeEnum.HsPatientCash
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.HsInterestPayment;
        }

        public virtual bool IsHsCharge()
        {
            return this.VpTransactionTypeEnum == VpTransactionTypeEnum.HsCharge;
        }

        public virtual bool IsHsInterest()
        {
            return this.VpTransactionTypeEnum == VpTransactionTypeEnum.HsInterestAssessed;
        }

        public virtual bool IsHsAdjustment()
        {
            return this.VpTransactionTypeEnum == VpTransactionTypeEnum.HsPayorCash ||
                   this.VpTransactionTypeEnum == VpTransactionTypeEnum.HsPayorNonCash ||
                   this.IsHsCharity() ||
                   this.VpTransactionTypeEnum == VpTransactionTypeEnum.HsPatientNonCash ||
                   this.VpTransactionTypeEnum == VpTransactionTypeEnum.OtherTransaction ||
                   this.VpTransactionTypeEnum == VpTransactionTypeEnum.HsWriteoffTransaction;
        }

        public virtual bool IsHsInsuranceAdjustment()
        {
            return this.VpTransactionTypeEnum == VpTransactionTypeEnum.HsPayorCash ||
                   this.VpTransactionTypeEnum == VpTransactionTypeEnum.HsPayorNonCash;
        }

        [Obsolete(ObsoleteDescription.VisitTransactionVisitPayInterestPayment)]
        public virtual bool IsVpPayment()
        {
            return this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppPrincipalPaymentHsReallocation
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppPrincipalPaymentHsReversal
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppPrincipalPayment
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestPaymentHsReallocation
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestPaymentHsReversal
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestPayment
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestPaymentReassessmentReversal
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestPaymentReassessmentReallocation
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppPrincipalPaymentReassessmentReallocation
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppPrincipalPaymentReassessmentReversal;
        }

        public virtual bool IsInitialTransactionType()
        {
            return this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppPrincipalPayment
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestPayment
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppDiscount;
        }

        public virtual bool IsReassessment()
        {
            return this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestPaymentReassessmentReversal
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppPrincipalPaymentReassessmentReversal
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestPaymentReassessmentReallocation
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppPrincipalPaymentReassessmentReallocation
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestAssessmentReversal
                   || this.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestAssessmentReallocation;

        }

        public virtual bool IsHsCharity() => 
            this.VpTransactionTypeEnum == VpTransactionTypeEnum.HsCharity ||
            this.VpTransactionTypeEnum == VpTransactionTypeEnum.HsNonPresumptiveCharity;
    }
}
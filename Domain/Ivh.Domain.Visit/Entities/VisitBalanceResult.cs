﻿namespace Ivh.Domain.Visit.Entities
{
    public class VisitBalanceResult
    {
        public int VisitId { get; set; }
        public decimal CurrentBalance { get; set; }
    }
}
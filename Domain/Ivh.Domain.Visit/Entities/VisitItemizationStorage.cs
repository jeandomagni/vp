﻿namespace Ivh.Domain.Visit.Entities
{
    using System;
    using Common.Base.Utilities.Helpers;
    using Common.VisitPay.Attributes;
    using Guarantor.Entities;

    public class VisitItemizationStorage
    {
        public VisitItemizationStorage()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        public virtual int VisitFileStoredId { get; set; }
        [RedactField(3)]
        public virtual string SystemId { get; set; }
        [RedactField(25)]
        public virtual string FacilityCode { get; set; }
        [RedactField(250)]
        public virtual string FacilityDescription { get; set; }
        [RedactField(1024)]
        public virtual string AccountNumber { get; set; }
        [RedactField(50)]
        public virtual string Empi { get; set; }
        public virtual string MatchedEmpi { get; set; }
        public virtual string MatchedAccountNumber { get; set; }
        public virtual Guid ExternalStorageKey { get; set; }
        public virtual Guarantor VpGuarantor { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual DateTime? DateMatched { get; set; }
        public virtual DateTime? DateUnmatched { get; set; }

        [RedactField(400)]
        public virtual string VisitSourceSystemKeyDisplay { get; set; }
        public virtual string MatchedVisitSourceSystemKeyDisplay { get; set; }
        public virtual int? BillingSystemId { get; set; }
        
        //Not Persisted in the VP_APP database. DO NOT MAP
        public virtual string PatientFirstName { get; set; }
        public virtual string PatientLastName { get; set; }
        public virtual string PatientLastNameFirstName => FormatHelper.LastNameFirstName(this.PatientLastName, this.PatientFirstName);
        public virtual string PatientFirstNameLastName => FormatHelper.FirstNameLastName(this.PatientFirstName, this.PatientLastName);
        public virtual string PatientName => this.PatientFirstNameLastName;
        public virtual DateTime? VisitAdmitDate { get; set; }
        public virtual DateTime? VisitDischargeDate { get; set; }
        public virtual bool? IsPatientGuarantor { get; set; }
        public virtual bool? IsPatientMinor { get; set; }

        public virtual string VisitSourceSystemKey
        {
            get { return this.AccountNumber; }
        }

        public virtual string HsGuarantorSourceSystemKey
        {
            get { return this.Empi; }
        }

        public virtual bool IsRemoved
        {
            get { return this.DateUnmatched.HasValue; }
        }
    }
}
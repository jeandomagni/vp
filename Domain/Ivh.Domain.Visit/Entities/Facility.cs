﻿namespace Ivh.Domain.Visit.Entities
{
    public class Facility
    {
        public virtual int FacilityId { get; set; }

        public virtual string LogoFilename { get; set; }

        public virtual string FacilityDescription { get; set; }
        
        public virtual string SourceSystemKey { get; set; }

		public virtual State RicState { get; set; }

        public virtual string CityState { get; set; }

        public virtual string ContactPhone { get; set; }

        public virtual string FacilityHours { get; set; }
    }
}
﻿namespace Ivh.Domain.Visit.Entities
{
    using System;
    using Common.VisitPay.Enums;

    /// <summary>
    /// readonly copy of HS VisitTransaction data
    /// </summary>
    /// <remarks>
    /// NOTE: NOT ALL HS VISIT TRANSACTIONS ARE CURRENTLY LOADED INTO THE APP DATABASE.
    /// To reduce dataload time, this currently only includes transactions required to generate the payment summary report.
    /// </remarks>
    public class BaseVisitTransaction
    {
        public virtual int VisitTransactionId { get; set; }
        public virtual int VisitId { get; set; }
        public virtual DateTime PostDate { get; set; }
        public virtual decimal TransactionAmount { get; set; }
        public virtual string TransactionDescription { get; set; }
        public virtual VpTransactionTypeEnum VpTransactionTypeId { get; set; }

    }
}

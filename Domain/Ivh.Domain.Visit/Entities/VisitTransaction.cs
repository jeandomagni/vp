namespace Ivh.Domain.Visit.Entities
{
    using System;
    using System.Collections.Generic;
    using Application.Base.Common.Interfaces;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;
    using Guarantor.Entities;
    
    public class VisitTransaction : 
        IMaskConsolidated,
        IMaskMatching
    {
        public VisitTransaction()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        public virtual int VisitTransactionId { get; set; }
        public virtual int VpGuarantorId { get; set; }
        public virtual DateTime TransactionDate { get; set; }
        public virtual DateTime PostDate { get; set; }
        public virtual VpTransactionType VpTransactionType { get; set; }
        [RedactField]
        public virtual string TransactionDescription { get; set; }
        public virtual string SourceSystemKey { get; protected set; }
        public virtual void SetSourceSystemKey(string sourceSystemKey)
        {
            if (this.SourceSystemKey.IsNullOrEmpty() && !sourceSystemKey.IsNullOrEmpty())
            {
                this.SourceSystemKeySetDate = DateTime.UtcNow;
            }
            if (sourceSystemKey.IsNullOrEmpty())
            {
                this.SourceSystemKeySetDate = null;
            }
            this.SourceSystemKey = sourceSystemKey;
        }
        public virtual DateTime? SourceSystemKeySetDate { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual DateTime? OverrideInsertDate { get; set; }
        public virtual Visit Visit { get; set; }
        public virtual BillingSystem BillingSystem { get; set; }
        public virtual int BillingSystemId { get; set; }
        //Note: PaymentAllocationId may not always be supplied by the hospital and should not be relied on for any business rules
        public virtual int? PaymentAllocationId { get; set; }
        public virtual VisitTransactionAmount VisitTransactionAmount { get; set; }
        public virtual IList<VisitTransactionAmount> VisitTransactionAmounts { get; set; }
        public virtual BillingSystem MatchedBillingSystem { get; set; }
        public virtual string MatchedSourceSystemKey { get; set; }
        public virtual int? TransactionCodeBillingSystemId { get; set; }
        public virtual string TransactionCodeSourceSystemKey { get; set; }
        public virtual bool CanIgnore { get; set; }
        public virtual BalanceTransferStatus BalanceTransferStatus { get; set; }
        public virtual void SetTransactionAmount(decimal transactionAmount, string reason = null)
        {
            if (this.VisitTransactionAmounts == null)
            {
                this.VisitTransactionAmounts = new List<VisitTransactionAmount>();
            }

            this.VisitTransactionAmount = new VisitTransactionAmount()
            {
                InsertDate = DateTime.UtcNow,
                Reason = reason,
                TransactionAmount = transactionAmount,
                VisitTransaction = this
            };

            this.VisitTransactionAmounts.Add(this.VisitTransactionAmount);
        }
        public virtual bool Offsets(VisitTransaction otherTransaction)
        {
            if (this.CanIgnore || otherTransaction.CanIgnore || this.PaymentAllocationId.HasValue || otherTransaction.PaymentAllocationId.HasValue)
            {
                // this transaction has already been offset
                return false;
            }

            return this.VisitTransactionAmount?.TransactionAmount == -otherTransaction.VisitTransactionAmount.TransactionAmount &&
                   this.TransactionCodeSourceSystemKey == otherTransaction.TransactionCodeSourceSystemKey &&
                   this.TransactionCodeBillingSystemId == otherTransaction.TransactionCodeBillingSystemId;
        }
        //VPNG-18696 Display TransactionDate for charges in transactions grid
        public virtual DateTime DisplayDate => (this.VpTransactionType?.TransactionType == "Charge") ? this.TransactionDate : this.PostDate;

        #region masking indicators
        
        public virtual bool IsMaskedConsolidated => this.Visit?.IsMaskedConsolidated ?? false;
        
        public virtual MatchOptionEnum? MatchOption => this.Visit?.MatchOption;

        #endregion
    }
}
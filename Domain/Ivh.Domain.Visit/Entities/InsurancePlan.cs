﻿namespace Ivh.Domain.Visit.Entities
{
    public class InsurancePlan
    {
        public virtual int InsurancePlanId { get; set; }
        public virtual int BillingSystemId { get; set; }
        public virtual string SourceSystemKey { get; set; }
        public virtual string InsurancePlanName { get; set; }
        public virtual PrimaryInsuranceType PrimaryInsuranceType { get; set; }
        public virtual decimal? DiscountPercentage { get; set; }
        public virtual ExternalLink EobExternalLink { get; set; }
    }
}
namespace Ivh.Domain.Visit.Entities
{
    public class VisitBalanceChangedEvent
    {
        public Visit Visit { get; set; }
    }
}
﻿namespace Ivh.Domain.Visit.Entities
{
    public class ExternalLink
    {
        public virtual int ExternalLinkId { get; set; }
        public virtual string Url { get; set; }
        public virtual string Text { get; set; }
        public virtual string Icon { get; set; }
        public virtual int? PersistDays { get; set; }
    }
}
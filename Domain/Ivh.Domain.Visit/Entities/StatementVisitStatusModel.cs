namespace Ivh.Domain.Visit.Entities
{
    using System;

    public class StatementVisitStatusModel
    {
        public int VisitId { get; set; }
        public bool IsInGracePeriod { get; set; }
        public DateTime? StatementPeriodEndDate { get; set; }
    }
}
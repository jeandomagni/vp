namespace Ivh.Domain.Visit.Entities
{
    using System;

    public class VisitTransactionAmount
    {
        public VisitTransactionAmount()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        public virtual int VisitTransactionAmountId { get; set; }
        public virtual VisitTransaction VisitTransaction { get; set; }
        public virtual decimal TransactionAmount { get; set; }
        public virtual string Reason { get; set; }
        public virtual DateTime InsertDate { get; set; }
    }
}
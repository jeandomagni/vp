﻿namespace Ivh.Domain.Visit.Entities
{
    public class HsFacility
    {
        public virtual string FacilityCode { get; set; }
        public virtual string FacilityDescription { get; set; }
    }
}

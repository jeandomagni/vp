﻿namespace Ivh.Domain.Visit.Entities
{
    using System;

    public class VisitBalanceHistory
    {
        public VisitBalanceHistory()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        public virtual int VisitBalanceHistoryId { get; set; }
        public virtual Visit Visit { get; set; }
        public virtual decimal CurrentBalance { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual string ChangeDescription { get; set; }
        public virtual bool Ignore { get; set; }
    }
}

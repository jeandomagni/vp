﻿namespace Ivh.Domain.Visit.Entities
{
    public class PrimaryInsuranceType
    {
        public virtual int PrimaryInsuranceTypeId { get; set; }
        public virtual string PrimaryInsuranceTypeName { get; set; }
        public virtual string SelfPayClassName { get; set; }
    }
}
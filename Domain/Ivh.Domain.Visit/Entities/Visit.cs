﻿﻿namespace Ivh.Domain.Visit.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Application.Base.Common.Interfaces;
    using Application.Base.Common.Interfaces.Entities.Visit;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.State.Interfaces;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.Core;
    using Common.VisitPay.Messages.HospitalData;
    using Common.VisitPay.Strings;
    using Guarantor.Entities;
    using Unmatching.Entities;
    using User.Entities;

    public class Visit :
        IEntity<VisitStateEnum>,
        IVisit,
        IBalanceTransferVisit,
        IPaymentAllocationVisit,
        IPaymentVisit,
        IFinancePlanVisitVisit,
        IMaskConsolidated,
        IMaskMatching
    {
        public Visit()
        {
            this.Initialize();
        }

        #region Value Type Properties

        public virtual int VisitId { get; set; }

        public virtual int? ServiceGroupId { get; set; }

        public virtual AgingTierEnum AgingTier
        {
            get
            {
                VisitAgingHistory check = this.VisitAgingHistories.OrderByDescending(x => x.InsertDate).ThenByDescending(x => x.VisitAgingHistoryId).FirstOrDefault();
                if (check != null)
                {
                    return check.AgingTier;
                }
                return AgingTierEnum.NotStatemented;
            }
        }

        public virtual int? PrimaryInsuranceTypeId { get; set; }

        public virtual decimal? TotalCharges { get; set; }

        public virtual HsGuarantorMatchStatusEnum HsGuarantorMatchStatus { get; set; }

        public virtual bool DiscrepancyAdjustment { get; set; }

        public virtual bool VpEligible { get; set; }

        public virtual bool BillingHold { get; set; }

        public virtual bool Redact { get; set; }

        public virtual bool? IsPatientGuarantor { get; set; }

        public virtual bool? IsPatientMinor { get; set; }

        public virtual bool InterestZero { get; set; }

        public virtual DateTime? AdmitDate { get; set; }

        public virtual DateTime? DischargeDate { get; set; }

        public virtual DateTime? HsCurrentBalanceChangedDate { get; set; }

        public virtual DateTime InsertDate { get; set; }

        public virtual DateTime? UnmatchActionDate { get; set; }

        public virtual DateTime? FinalPastDueEmailSentDate { get; set; }

        public virtual string SourceSystemKey { get; set; }

        public virtual string MatchedSourceSystemKey { get; set; }

        public virtual string BillingApplication { get; set; }

        public virtual int? PaymentPriority { get; set; }

        public virtual string UnmatchActionNotes { get; set; }

        protected virtual string VisitAttributesJsonOriginal { get; set; }

        [RedactField]
        public virtual string PatientFirstName { get; set; }

        [RedactField]
        public virtual string PatientLastName { get; set; }

        [RedactField]
        public virtual string SourceSystemKeyDisplay { get; set; }

        [RedactField]
        public virtual string VisitAttributesJson { get; protected set; }

        [RedactField]
        public virtual string VisitDescription { get; set; }

        #endregion

        #region Complex Type Properties

        public virtual BillingSystem BillingSystem { get; set; }

        public virtual BillingSystem MatchedBillingSystem { get; set; }

        public virtual Guarantor VPGuarantor { get; set; }

        public virtual HsGuarantorMap HsGuarantorMap { get; set; }

        public virtual HsGuarantorUnmatchReason HsGuarantorUnmatchReason { get; set; }

        public virtual VisitAgingHistory CurrentVisitAgingHistory { get; set; }

        public virtual VisitPayUser UnmatchActionVisitPayUser { get; set; }

        public virtual Facility Facility { get; set; }

        #endregion

        #region Collection Properties

        public virtual List<OutboundVisitMessage> OutboundVisitMessages { get; set; }

        public virtual IList<VisitBalanceHistory> VisitBalances { get; set; }

        public virtual IList<VisitChangedStatusMessage> UnpublishedStatusChangeMessages { get; set; }

        public virtual IList<VisitBalanceChangedEvent> UnpublishedVisitBalanceChanged { get; set; }

        public virtual IList<VisitAgingHistory> VisitAgingHistories { get; set; }

        public virtual IList<VisitInsurancePlan> VisitInsurancePlans { get; set; }

        #endregion

        #region Methods

        private void Initialize()
        {
            this.InsertDate = DateTime.UtcNow;
            this.VisitStates = new List<VisitStateHistory>();
            this.VisitAgingHistories = new List<VisitAgingHistory>();
            this.UnpublishedStatusChangeMessages = new List<VisitChangedStatusMessage>();
            this.UnpublishedVisitBalanceChanged = new List<VisitBalanceChangedEvent>();
            this.VisitInsurancePlans = new List<VisitInsurancePlan>();
            this.OutboundVisitMessages = new List<OutboundVisitMessage>();
            this.BalanceTransferStatusHistory = new List<BalanceTransferStatusHistory>();
            this.VisitBalances = new List<VisitBalanceHistory>();
        }

        public virtual decimal CurrentBalanceAsOf(DateTime date)
        {
            return this.VisitBalances.IsNotNullOrEmpty()
                ? this.VisitBalances
                    .Where(x => x.InsertDate <= date)
                    .OrderByDescending(x => x.InsertDate)
                    .Select(x => x.CurrentBalance).FirstOrDefault()
                : 0;
        }

        public virtual void InitializeActiveOutboundMessage()
        {
            this.OutboundVisitMessages.Add(new OutboundVisitMessage
            {
                ActionDate = DateTime.UtcNow,
                BillingSystemId = Math.Abs((this.BillingSystemId ?? this.MatchedBillingSystemId) ?? default(int)),
                SourceSystemKey = this.SourceSystemKey ?? this.MatchedSourceSystemKey,
                VpOutboundVisitMessageType = VpOutboundVisitMessageTypeEnum.VisitStatusSetToActive,
                VpVisitId = this.VisitId,
                IsCallCenter = this.VPGuarantor.IsOfflineGuarantor(),
                IsAutoPay = this.VPGuarantor.UseAutoPay
            });
        }

        public virtual void SetCurrentBalance(decimal? currentBalance, DateTime? currentAsOf = null)
        {
            if (this.CurrentBalance != currentBalance)
            {
                currentAsOf = currentAsOf ?? DateTime.UtcNow;
                this.AddBalanceHistory(currentBalance ?? 0m, currentAsOf.Value);
                this.HsCurrentBalanceChangedDate = this.VisitBalances
                    .Select(x => x.InsertDate)
                    .Where(x => x <= currentAsOf.Value)
                    .OrderByDescending(x => x)
                    .FirstOrDefault();
            }
        }

        private void AddBalanceHistory(decimal hsCurrentBalance, DateTime insertDate)
        {
            VisitBalanceHistory latestBalance = this.VisitBalances.OrderByDescending(x => x.InsertDate).FirstOrDefault();
            bool isSameAmount = (latestBalance != null && latestBalance.CurrentBalance == hsCurrentBalance);
            bool hasNewBalanceHistory = (latestBalance != null && latestBalance.VisitBalanceHistoryId == 0);
            if (!isSameAmount)
            {
                if (hasNewBalanceHistory)
                {
                    // Update the existing VisitBalanceHistory entity since it has not been committed to the DB yet
                    latestBalance.CurrentBalance = hsCurrentBalance;
                    latestBalance.InsertDate = insertDate;
                }
                else
                {
                    // Add a new VisitBalanceHistory entity
                    VisitBalanceHistory newBalance = new VisitBalanceHistory()
                    {
                        Visit = this,
                        CurrentBalance = hsCurrentBalance,
                        InsertDate = insertDate
                    };
                    this.VisitBalances.Add(newBalance);
                }
            }
        }

        public virtual VisitStateEnum VisitStateEnumForDate(DateTime endDate)
        {
            VisitStateHistory history = this.VisitStates
                .OrderByDescending(y => y.DateTime)
                .ThenByDescending(x => x.HistoryId)
                .FirstOrDefault(x => x.DateTime <= endDate);
            if (history != null)
            {
                return history.EvaluatedState;
            }
            return VisitStateEnum.NotSet;
        }

        public virtual void SetAgingTier(AgingTierEnum agingTier, string changeDescription, int? visitPayUserId = null)
        {
            if (!this.VisitAgingHistories.Any() || this.AgingTier != agingTier)
            {
                VisitAgingHistory visitAgingHistory = new VisitAgingHistory
                {
                    AgingTier = agingTier,
                    Visit = this,
                    ChangeDescription = changeDescription,
                    VisitPayUser = new VisitPayUser { VisitPayUserId = visitPayUserId ?? SystemUsers.SystemUserId },
                    InsertDate = DateTime.UtcNow, // nhibernate Generated.Always
                };
                AgingTierEnum oldAgingTier = this.AgingTier;
                this.VisitAgingHistories.Add(visitAgingHistory);
                this.CurrentVisitAgingHistory = visitAgingHistory;

                if (oldAgingTier <= AgingTierEnum.Good && agingTier == AgingTierEnum.PastDue)
                {
                    this._agingTierIncreasedToPastDue = true;
                }

                if (agingTier == AgingTierEnum.MaxAge)
                {
                    this.OutboundVisitMessages.Add(new OutboundVisitMessage
                    {
                        ActionDate = DateTime.UtcNow,
                        BillingSystemId = this.BillingSystemId ?? default(int),
                        SourceSystemKey = this.SourceSystemKey,
                        VpOutboundVisitMessageType = VpOutboundVisitMessageTypeEnum.UncollectableNotification,
                        VpVisitId = this.VisitId,
                        IsCallCenter = this.VPGuarantor.IsOfflineGuarantor(),
                        IsAutoPay = this.VPGuarantor.UseAutoPay
                    });
                }

                this.OutboundVisitMessages.Add(new OutboundVisitMessage
                {
                    ActionDate = DateTime.UtcNow,
                    BillingSystemId = this.BillingSystemId ?? default(int),
                    SourceSystemKey = this.SourceSystemKey,
                    VpOutboundVisitMessageType = VpOutboundVisitMessageTypeEnum.VisitAgingCountChange,
                    VpVisitId = this.VisitId,
                    OutboundValue = this.AgingCount.ToString(),
                    IsCallCenter = this.VPGuarantor.IsOfflineGuarantor(),
                    IsAutoPay = this.VPGuarantor.UseAutoPay
                });
            }
        }

        public virtual bool WasActiveBetween(DateTime periodStartDate, DateTime periodEndDate)
        {
            //Need to look at all of the visitStates for that time period.
            if (this.VisitStates == null) return false;
            List<VisitStateHistory> elegableStatusHistories = this.VisitStates.Where(x => x.DateTime.IsBetweenLeftInclusive(periodStartDate, periodEndDate)).ToList();

            //also take the last status from before periodStartDate.
            VisitStateHistory lastStateBeforeStatement = this.VisitStates.OrderByDescending(x => x.DateTime).FirstOrDefault(x => x.DateTime < periodStartDate);
            if (lastStateBeforeStatement != null)
            {
                elegableStatusHistories.Add(lastStateBeforeStatement);
            }
            if (elegableStatusHistories.Count > 0)
            {
                //If there were any statuses in that time period use this logic.
                return elegableStatusHistories.Any(x => x.EvaluatedState == VisitStateEnum.Active);
            }
            return false;
        }

        public virtual int AgingTierAsOf(DateTime periodEndDate)
        {
            VisitAgingHistory lastForThatTime = this.VisitAgingHistories
                .OrderByDescending(x => x.InsertDate)
                .ThenByDescending(x => x.VisitAgingHistoryId)
                .FirstOrDefault(x => x.InsertDate < periodEndDate);

            if (lastForThatTime != null)
            {
                return (int)lastForThatTime.AgingTier;
            }
            return 0;
        }

        public virtual bool HasAgingCountChangedBetween(DateTime startPeriod, DateTime? endPeriod, bool? changedBySystem)
        {
            IEnumerable<VisitAgingHistory> histories = this.VisitAgingHistories
                .OrderByDescending(x => x.InsertDate)
                .ThenByDescending(x => x.VisitAgingHistoryId)
                .Where(x => x.InsertDate > startPeriod);

            if (endPeriod.HasValue)
                histories = histories.Where(x => x.InsertDate < endPeriod);

            if (changedBySystem.HasValue)
            {
                if (changedBySystem.Value)
                {
                    histories = histories.Where(x => x.VisitPayUser.VisitPayUserId == SystemUsers.SystemUserId);
                }
                else
                {
                    histories = histories.Where(x => x.VisitPayUser.VisitPayUserId != SystemUsers.SystemUserId);
                }
            }

            return histories.Any();
        }

        public virtual bool HasConsolidationActionBetween(DateTime startPeriod, DateTime endPeriod)
        {
            IEnumerable<VisitAgingHistory> histories = this.VisitAgingHistories
                .OrderByDescending(x => x.InsertDate)
                .ThenByDescending(x => x.VisitAgingHistoryId)
                .Where(x => x.InsertDate > startPeriod && x.InsertDate < endPeriod);

            if (histories.Any(x => x.ChangeDescription == "Deconsolidation"))
                return true;

            return false;
        }

        public virtual void SetVisitAttributesJson(string visitAttributesJson)
        {
            if (this.VisitAttributesJson == null)
            {
                this.VisitAttributesJsonOriginal = this.VisitAttributesJson;
            }
            this.VisitAttributesJson = visitAttributesJson;
        }

        #endregion

        #region Derived Properties

        public virtual int? BillingSystemId => this.BillingSystem?.BillingSystemId;

        public virtual int? MatchedBillingSystemId => this.MatchedBillingSystem?.BillingSystemId;

        public virtual string PatientLastNameFirstName => FormatHelper.LastNameFirstName(this.PatientLastName, this.PatientFirstName);

        public virtual string PatientFirstNameLastName => FormatHelper.FirstNameLastName(this.PatientFirstName, this.PatientLastName);

        public virtual string PatientName => this.PatientFirstNameLastName;

        public virtual string GuarantorName => $"{(this.VPGuarantor?.User != null ? this.VPGuarantor.User.FirstNameLastName : "")}";

        public virtual int VpGuarantorId => this.VPGuarantor?.VpGuarantorId ?? 0;

        /// <summary>
        /// This contains the balance known by the Hospital. It DOES NOT include the amount of unreconciled VisitPay payments.
        /// </summary>
        public virtual decimal CurrentBalance
        {
            //get { return this.Transactions.Sum(x => x.VisitTransactionAmount?.TransactionAmount ?? 0); }
            get => this.CurrentBalanceAsOf(DateTime.UtcNow);
            set { /* ignore set as we only want to save off the calculated current balance */ }
        }

        public virtual DateTime? VisitDisplayDateUnformatted
        {
            get
            {
                if (this.DischargeDate.HasValue)
                {
                    return this.DischargeDate.Value;
                }
                else if (this.AdmitDate.HasValue)
                {
                    return this.AdmitDate.Value;
                }
                return null;
            }
        }

        public virtual string VisitDisplayDate
        {
            get
            {
                if (!this.VisitDisplayDateUnformatted.HasValue)
                {
                    return "Not Available";
                }

                return this.VisitDisplayDateUnformatted.Value.ToString(Format.DateFormat);

            }
        }

        [Obsolete("Use AgingTier instead")]
        public virtual int AgingCount => (int) this.AgingTier;

        public virtual bool IsFinalPastDue => this.AgingTier == AgingTierEnum.FinalPastDue;

        public virtual bool AreVisitAttributesDirty => (this.VisitAttributesJsonOriginal != null && !this.VisitAttributesJsonOriginal.Equals(this.VisitAttributesJson))
                                                       || (this.VisitAttributesJsonOriginal == null && this.VisitAttributesJson != null);
        
        private bool _agingTierIncreasedToPastDue = false;

        public virtual bool AgingTierIncreasedToPastDue
        {
            get { return this._agingTierIncreasedToPastDue; }
            set { }
        }

        #endregion

        #region Balance Transfer

        public virtual IList<BalanceTransferStatusHistory> BalanceTransferStatusHistory { get; set; }

        public virtual BalanceTransferStatusEnum? BalanceTransferStatusEnum => this.BalanceTransferStatus?.BalanceTransferStatusEnum;

        public virtual BalanceTransferStatus BalanceTransferStatus
        {
            get
            { 
                return this.BalanceTransferStatusHistory.OrderByDescending(y => y.InsertDate).ThenByDescending(x => x.BalanceTransferStatusHistoryId).Select(x => x.BalanceTransferStatus).FirstOrDefault();
            }
        }

        public virtual bool IsActiveBalanceTransfer()
        {
            return this.BalanceTransferStatus?.IsActive() ?? false;
        }

        public virtual void SetBalanceTransferStatus(BalanceTransferStatusHistory balanceTranferStatusHistory)
        {
            this.BalanceTransferStatusHistory.Add(balanceTranferStatusHistory);
        }

        #endregion

        #region State Machine Flags

        public virtual bool HasPositiveBalance => this.CurrentBalance > 0;
        public virtual bool IsUnmatched
        {
            get => this.BillingSystemId < 0;
            set { }
        }

        public virtual bool IsMaxAge
        {
            get => this.AgingTier == AgingTierEnum.MaxAge;
            set { }
        }

        public virtual bool IsMaxAgeActive => this.IsMaxAge && this.CurrentVisitState == VisitStateEnum.Active;

        public virtual bool IsMaxAgeInactive => this.IsMaxAge && this.CurrentVisitState != VisitStateEnum.Active;

        #endregion

        #region State Machine

        public virtual IList<VisitStateHistory> VisitStates { get; set; }

        public virtual VisitStateEnum CurrentVisitState
        {
            get => this.CurrentVisitStateHistory?.EvaluatedState ?? VisitStateEnum.NotSet;
            set { }
        }

        public virtual VisitStateHistory CurrentVisitStateHistory { get; set; }

        VisitStateEnum IEntity<VisitStateEnum>.GetState()
        {
            return this.CurrentVisitState;
        }

        void IEntity<VisitStateEnum>.SetState(VisitStateEnum resultingState, string ruleName)
        {
            if (resultingState != this.CurrentVisitState)
            {
                this.AddVisitChangedStatusMessage(this.CurrentVisitState, resultingState);
                if (this.CurrentVisitState != VisitStateEnum.NotSet) // this avoids sending outbound message while visit is not yet active in vp
                {
                    this.OutboundVisitMessages.Add(new OutboundVisitMessage
                    {
                        ActionDate = DateTime.UtcNow,
                        BillingSystemId = Math.Abs((this.BillingSystemId ?? this.MatchedBillingSystemId) ?? default(int)),
                        SourceSystemKey = this.SourceSystemKey ?? this.MatchedSourceSystemKey,
                        VpOutboundVisitMessageType = resultingState == VisitStateEnum.Active ? VpOutboundVisitMessageTypeEnum.VisitStatusSetToActive : VpOutboundVisitMessageTypeEnum.VisitStatusSetToInactive,
                        VpVisitId = this.VisitId,
                        IsCallCenter = this.VPGuarantor.IsOfflineGuarantor(),
                        IsAutoPay = this.VPGuarantor.UseAutoPay
                    });
                }
            }

            VisitStateHistory visitStateHistory = new VisitStateHistory
            {
                Entity = this,
                InitialState = this.CurrentVisitState,
                EvaluatedState = resultingState,
                RuleName = ruleName,
                DateTime = DateTime.UtcNow
            };

            this.VisitStates.Add(visitStateHistory);
            this.CurrentVisitStateHistory = visitStateHistory;       
        }

        IHistory<IEntity<VisitStateEnum>, VisitStateEnum> IEntity<VisitStateEnum>.GetCurrentStateHistory()
        {
            return this.CurrentVisitStateHistory;
        }

        private void AddVisitChangedStatusMessage(VisitStateEnum previousState, VisitStateEnum newState)
        {
            this.UnpublishedStatusChangeMessages.Add(new VisitChangedStatusMessage()
            {
                VisitId = this.VisitId,
                NewState = newState,
                PreviousState = previousState,
                EventDate = DateTime.UtcNow
            });
        }


        public virtual bool CanResetAgingTierUponVpEligible()
        {
            VisitStateHistory currentVisitStateHistory = this.VisitStates.OrderByDescending(x => x.DateTime)
                //There's chatter in the VisitStateHistory
                //We only want to see when the state actually changed
                .FirstOrDefault(x => x.EvaluatedState != x.InitialState);
            bool wentFromInActiveToActive = (currentVisitStateHistory?.InitialState == VisitStateEnum.Inactive)
                                            && (currentVisitStateHistory?.EvaluatedState == VisitStateEnum.Active);
            
            return wentFromInActiveToActive && this.IsMaxAge;
        }

        #endregion

        #region Removed VisitTransaction Dependencies

        /* [Obsolete(ObsoleteDescription.VisitTransactions)]
        public virtual decimal InterestDue
        {
            get { return this.Transactions.Where(x => x.VpTransactionType.IsInterest()).Sum(x => x.VisitTransactionAmount.TransactionAmount); }
        } */

        /*[Obsolete(ObsoleteDescription.VisitTransactions)]
        public virtual decimal InterestDueForDate(DateTime endDate)
        {
            return this.Transactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsInterest()
                                                                         && (x.OverrideInsertDate ?? x.InsertDate).HasValue
                                                                         && (x.OverrideInsertDate ?? x.InsertDate).Value <= endDate)
                ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
        }*/

        /*[Obsolete(ObsoleteDescription.VisitTransactions)]
        public virtual decimal PrincipalDueForDate(DateTime endDate)
        {
            return this.Transactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsPrincipal()
                                                             && (x.OverrideInsertDate ?? x.InsertDate).HasValue
                                                             && (x.OverrideInsertDate ?? x.InsertDate).Value <= endDate)
                ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
        }*/

        /*[Obsolete(ObsoleteDescription.VisitTransactions)]
        public virtual decimal PrincipalPaidForDateRange(DateTime? startingDate, DateTime endingDate)
        {
            if (startingDate.HasValue)
            {
                return this.Transactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsPrincipalPayment()
                                                                             && (x.OverrideInsertDate ?? x.InsertDate).HasValue
                                                                             && (x.OverrideInsertDate ?? x.InsertDate).Value >= startingDate.Value
                                                                             && (x.OverrideInsertDate ?? x.InsertDate).Value <= endingDate)
                    ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
            }
            return this.Transactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsPrincipalPayment()
                                                                         && (x.OverrideInsertDate ?? x.InsertDate).HasValue
                                                                         && (x.OverrideInsertDate ?? x.InsertDate).Value <= endingDate)
                ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
        }*/

        /*[Obsolete(ObsoleteDescription.VisitTransactions)]
        public virtual decimal InterestPaidForDateRange(DateTime? startingDate, DateTime endingDate)
        {
            if (startingDate.HasValue)
            {
                return this.Transactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsInterestPayment()
                                                                             && (x.OverrideInsertDate ?? x.InsertDate).HasValue
                                                                             && (x.OverrideInsertDate ?? x.InsertDate).Value >= startingDate.Value
                                                                             && (x.OverrideInsertDate ?? x.InsertDate).Value <= endingDate)
                    ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
            }
            return this.Transactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsInterestPayment()
                                                                         && (x.OverrideInsertDate ?? x.InsertDate).HasValue
                                                                         && (x.OverrideInsertDate ?? x.InsertDate).Value <= endingDate)
                ).Sum(x => x.VisitTransactionAmount.TransactionAmount);

        }*/

        /*[Obsolete(ObsoleteDescription.VisitTransactions)]
        public virtual decimal InterestAssessed
        {
            get { return this.Transactions.Where(x => x.VpTransactionType.IsInterestAssessed()).Sum(x => x.VisitTransactionAmount.TransactionAmount); }
        }*/

        /*[Obsolete(ObsoleteDescription.VisitTransactions)]
        public virtual decimal InterestAssessedForDateRange(DateTime? startingDate, DateTime endingDate)
        {
            if (startingDate.HasValue)
            {
                return this.Transactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsInterestAssessed()
                                                                             && (x.OverrideInsertDate ?? x.InsertDate).HasValue
                                                                             && (x.OverrideInsertDate ?? x.InsertDate).Value >= startingDate.Value
                                                                             && (x.OverrideInsertDate ?? x.InsertDate).Value <= endingDate)
                ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
            }
            return this.Transactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsInterestAssessed()
                                                                         && (x.OverrideInsertDate ?? x.InsertDate).HasValue
                                                                         && (x.OverrideInsertDate ?? x.InsertDate).Value <= endingDate)
            ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
        }*/

        /*[Obsolete(ObsoleteDescription.VisitTransactions)]
        public virtual decimal CurrentBalanceUsingPostDateAsOf(DateTime periodEndDate)
        {
            return this.Transactions.Where(x => x.PostDate.Date <= periodEndDate.Date
                ).Sum(t => t.VisitTransactionAmount.TransactionAmount);
        }*/

        /*[Obsolete(ObsoleteDescription.VisitTransactions)]
        public virtual decimal InterestAssessedForDateRangeStartingDateWithPostDate(DateTime? startingDate, DateTime endingDate)
        {
            if (startingDate.HasValue)
            {
                return this.Transactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsInterestAssessed()
                                                                             && (x.OverrideInsertDate ?? x.InsertDate).HasValue
                                                                             && x.PostDate.Date >= startingDate.Value.Date
                                                                             && (x.OverrideInsertDate ?? x.InsertDate).Value <= endingDate)
                ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
            }
            return this.Transactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsInterestAssessed()
                                                                         && (x.OverrideInsertDate ?? x.InsertDate).HasValue
                                                                         && (x.OverrideInsertDate ?? x.InsertDate).Value <= endingDate)
            ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
        }*/

        /*[Obsolete(ObsoleteDescription.VisitTransactions)]
        public virtual decimal VpDiscountsForDateRangeStartingDateWithPostDate(DateTime? startingDate, DateTime endingDate)
        {
            if (startingDate.HasValue)
            {
                return this.Transactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsVpDiscount()
                                                                             && (x.OverrideInsertDate ?? x.InsertDate).HasValue
                                                                             && x.PostDate.Date >= startingDate.Value.Date
                                                                             && (x.OverrideInsertDate ?? x.InsertDate).Value <= endingDate)
                    ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
            }
            return this.Transactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsVpDiscount()
                                                                         && (x.OverrideInsertDate ?? x.InsertDate).HasValue
                                                                         && (x.OverrideInsertDate ?? x.InsertDate).Value <= endingDate)
                ).Sum(x => x.VisitTransactionAmount.TransactionAmount);

        }*/

        /*[Obsolete(ObsoleteDescription.VisitTransactions)]
        public virtual decimal VpPaymentsForDateRangeStartingDateWithPostDate(DateTime? startingDate, DateTime endingDate)
        {
            if (startingDate.HasValue)
            {
                return this.Transactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsVpPayment()
                                                                             && (x.OverrideInsertDate ?? x.InsertDate).HasValue
                                                                             && x.PostDate.Date >= startingDate.Value.Date
                                                                             && (x.OverrideInsertDate ?? x.InsertDate).Value <= endingDate)
                    ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
            }
            return this.Transactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsVpPayment()
                                                                         && (x.OverrideInsertDate ?? x.InsertDate).HasValue
                                                                         && (x.OverrideInsertDate ?? x.InsertDate).Value <= endingDate)
                ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
        }*/

        /*[Obsolete(ObsoleteDescription.VisitTransactions)]
        public virtual decimal HsPaymentsForDateRangeStartingDateWithPostDate(DateTime? startingDate, DateTime endingDate)
        {
            if (startingDate.HasValue)
            {
                return this.Transactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsHsPayment()
                                                                             && (x.OverrideInsertDate ?? x.InsertDate).HasValue
                                                                             && x.PostDate.Date >= startingDate.Value.Date
                                                                             && (x.OverrideInsertDate ?? x.InsertDate).Value <= endingDate)
                    ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
            }
            return this.Transactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsHsPayment()
                                                                         && (x.OverrideInsertDate ?? x.InsertDate).HasValue
                                                                         && (x.OverrideInsertDate ?? x.InsertDate).Value <= endingDate)
                ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
        }*/

        /*[Obsolete(ObsoleteDescription.VisitTransactions)]
        public virtual decimal HsChargesForDateRangeStartingDateWithPostDate(DateTime? startingDate, DateTime endingDate)
        {
            if (startingDate.HasValue)
            {
                return this.Transactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsHsCharge()
                                                                             && (x.OverrideInsertDate ?? x.InsertDate).HasValue
                                                                             && x.PostDate.Date >= startingDate.Value.Date
                                                                             && (x.OverrideInsertDate ?? x.InsertDate).Value <= endingDate)
                    ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
            }
            return this.Transactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsHsCharge()
                                                                         && (x.OverrideInsertDate ?? x.InsertDate).HasValue
                                                                         && (x.OverrideInsertDate ?? x.InsertDate).Value <= endingDate)
                ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
        }*/

        /*[Obsolete(ObsoleteDescription.VisitTransactions)]
        public virtual decimal HsAdjustmentsForDateRangeStartingDateWithPostDate(DateTime? startingDate, DateTime endingDate)
        {
            if (startingDate.HasValue)
            {
                return this.Transactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsHsAdjustment()
                                                                             && (x.OverrideInsertDate ?? x.InsertDate).HasValue
                                                                             && x.PostDate.Date >= startingDate.Value.Date
                                                                             && (x.OverrideInsertDate ?? x.InsertDate).Value <= endingDate)
                    ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
            }
            return this.Transactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsHsAdjustment()
                                                                         && (x.OverrideInsertDate ?? x.InsertDate).HasValue
                                                                         && (x.OverrideInsertDate ?? x.InsertDate).Value <= endingDate)
                ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
        }*/

        /*[Obsolete(ObsoleteDescription.VisitTransactions)]
        public virtual decimal HsAdjustmentsExcludingInsuranceForDateRangeStartingDateWithPostDate(DateTime? startingDate, DateTime endingDate)
        {
            List<VisitTransaction> transactions = this.Transactions.Where(x => x.InsertDate != null &&
                                                                               x.VpTransactionType.IsHsAdjustment() &&
                                                                               !x.VpTransactionType.IsHsInsuranceAdjustment() &&
                                                                               (x.OverrideInsertDate ?? x.InsertDate).HasValue &&
                                                                               (x.OverrideInsertDate ?? x.InsertDate).Value <= endingDate).ToList();

            if (startingDate.HasValue)
            {
                transactions = transactions.Where(x => x.InsertDate != null && x.PostDate.Date >= startingDate.Value.Date).ToList();
            }

            return transactions.Sum(x => x.VisitTransactionAmount.TransactionAmount);
        }*/

        /*[Obsolete(ObsoleteDescription.VisitTransactions)]
        public virtual decimal HsAdjustmentsInsuranceForDateRangeStartingDateWithPostDate(DateTime? startingDate, DateTime endingDate)
        {
            List<VisitTransaction> transactions = this.Transactions.Where(x => x.InsertDate != null &&
                                                                               x.VpTransactionType.IsHsInsuranceAdjustment() &&
                                                                               (x.OverrideInsertDate ?? x.InsertDate).HasValue &&
                                                                               (x.OverrideInsertDate ?? x.InsertDate).Value <= endingDate).ToList();

            if (startingDate.HasValue)
            {
                transactions = transactions.Where(x => x.InsertDate != null && x.PostDate.Date >= startingDate.Value.Date).ToList();
            }

            return transactions.Sum(x => x.VisitTransactionAmount.TransactionAmount);
        }*/

        /*[Obsolete(ObsoleteDescription.VisitTransactions)]
        public virtual decimal PrincipalPaidForDateRangeStartingDateWithPostDate(DateTime? startingDate, DateTime endingDate)
        {
            if (startingDate.HasValue)
            {
                return this.Transactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsPrincipalPayment()
                                                                             && (x.OverrideInsertDate ?? x.InsertDate).HasValue
                                                                             && x.PostDate.Date >= startingDate.Value.Date
                                                                             && (x.OverrideInsertDate ?? x.InsertDate).Value <= endingDate)
                    ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
            }
            return this.Transactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsPrincipalPayment()
                                                                         && (x.OverrideInsertDate ?? x.InsertDate).HasValue
                                                                         && (x.OverrideInsertDate ?? x.InsertDate).Value <= endingDate)
                ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
        }*/

        /*[Obsolete(ObsoleteDescription.VisitTransactions)]
        public virtual decimal InterestPaidForDateRangeStartingDateWithPostDate(DateTime? startingDate, DateTime endingDate)
        {
            if (startingDate.HasValue)
            {
                return this.Transactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsInterestPayment()
                                                                             && (x.OverrideInsertDate ?? x.InsertDate).HasValue
                                                                             && x.PostDate.Date >= startingDate.Value.Date
                                                                             && (x.OverrideInsertDate ?? x.InsertDate).Value <= endingDate)
                    ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
            }
            return this.Transactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsInterestPayment()
                                                                         && (x.OverrideInsertDate ?? x.InsertDate).HasValue
                                                                         && (x.OverrideInsertDate ?? x.InsertDate).Value <= endingDate)
                ).Sum(x => x.VisitTransactionAmount.TransactionAmount);

        }*/

        /* [Obsolete(ObsoleteDescription.VisitTransactions)]
        public virtual decimal InterestAssessedForDateRangeForStatementing(DateTime? startingDate, DateTime endingDate)
        {
            if (startingDate.HasValue)
            {
                return this.Transactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsVpInterestAssessed()
                                                                             && (x.OverrideInsertDate ?? x.InsertDate).HasValue
                                                                             && (x.OverrideInsertDate ?? x.InsertDate).Value >= startingDate.Value
                                                                             && (x.OverrideInsertDate ?? x.InsertDate).Value <= endingDate)
                ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
            }
            return this.Transactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsVpInterestAssessed()
                                                                         && (x.OverrideInsertDate ?? x.InsertDate).HasValue
                                                                         && (x.OverrideInsertDate ?? x.InsertDate).Value <= endingDate)
            ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
        }*/

        #endregion

        #region masking indicators

        public virtual bool IsMaskedConsolidated => !this.IsPatientGuarantor.GetValueOrDefault(false) &&
                                                     !this.IsPatientMinor.GetValueOrDefault(false) &&
                                                     (this.VPGuarantor?.ConsolidationStatus.IsInCategory(GuarantorConsolidationStatusEnumCategory.Mask) ?? false);
        
        public virtual MatchOptionEnum? MatchOption => this.HsGuarantorMap?.MatchOption;

        #endregion
    }
}
﻿namespace Ivh.Domain.Visit.Entities
{
    using System;
    using Common.State.Interfaces;
    using Common.VisitPay.Enums;

    public class VisitStateHistory : IHistory<IEntity<VisitStateEnum>, VisitStateEnum>
    {
        public virtual int HistoryId { get; set; }
        public virtual IEntity<VisitStateEnum> Entity { get; set; }
        public virtual VisitStateEnum InitialState { get; set; }
        public virtual VisitStateEnum EvaluatedState { get; set; }
        public virtual DateTime DateTime { get; set; }
        public virtual string RuleName { get; set; }
    }
}

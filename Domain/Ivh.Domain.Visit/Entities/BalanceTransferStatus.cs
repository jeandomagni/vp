﻿namespace Ivh.Domain.Visit.Entities
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class BalanceTransferStatus
    {
        public virtual int BalanceTransferStatusId { get; set; }
        public virtual string BalanceTransferStatusName { get; set; }
        public virtual string BalanceTransferStatusDisplayName { get; set; }

        public virtual BalanceTransferStatusEnum BalanceTransferStatusEnum => (BalanceTransferStatusEnum)this.BalanceTransferStatusId;

        public virtual bool IsEligible()
        {
            return this.BalanceTransferStatusEnum == BalanceTransferStatusEnum.Eligible;
        }

        public virtual bool IsIneligible()
        {
            return this.BalanceTransferStatusEnum == BalanceTransferStatusEnum.Ineligible;
        }

        public virtual bool IsActive()
        {
            return this.BalanceTransferStatusEnum == BalanceTransferStatusEnum.ActiveBalanceTransfer;
        }
    }
}

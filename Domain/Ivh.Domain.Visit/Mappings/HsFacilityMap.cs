﻿namespace Ivh.Domain.Visit.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class HsFacilityMap : ClassMap<HsFacility>
    {
        public HsFacilityMap()
        {
            this.Cache.ReadOnly();
            this.Schema("dbo");
            this.Table("HsFacility");
            this.Id(x => x.FacilityCode);
            this.Map(x => x.FacilityDescription).Not.Nullable(); // Char
        }
    }
}

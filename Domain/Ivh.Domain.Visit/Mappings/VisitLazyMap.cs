﻿namespace Ivh.Domain.Visit.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    /// <summary>
    /// WARNING: COUPLING
    /// This is coupled with VisitMap
    /// DO NOT PUT "Not.LazyLoad()" in the mapping below
    /// </summary>
    public class VisitLazyMap : ClassMap<VisitLazy>
    {
        public VisitLazyMap()
        {
            //This prevents nHibernate from finding this map from inherited types
            this.Polymorphism.Explicit();

            this.BatchSize(50);
            this.Schema("dbo");
            this.Table("Visit");
            this.Id(x => x.VisitId);

            this.Map(x => x.SourceSystemKey)
                .Nullable();
            this.References(x => x.BillingSystem)
                     .Column("BillingSystemId")
                     .Nullable();

            this.Map(x => x.BillingApplication)
                .Not.Nullable(); // Char


            this.Map(x => x.MatchedSourceSystemKey)
                .Nullable();

            this.Map(x => x.VpEligible).Not.Nullable();
            this.Map(x => x.BillingHold).Not.Nullable();
            this.Map(x => x.Redact).Not.Nullable();

            this.References(x => x.MatchedBillingSystem)
                .Column("MatchedBillingSystemId")
                .Nullable();


            this.Map(x => x.VisitDescription)
                .Not.Nullable();

            this.Map(x => x.PatientFirstName);

            this.Map(x => x.PatientLastName)
                .Not.Nullable();

            this.Map(x => x.DischargeDate)
                .Nullable();
            this.Map(x => x.AdmitDate)
                .Nullable();
            this.Map(x => x.FinalPastDueEmailSentDate)
                .Nullable();

            this.Map(x => x.TotalCharges);

            this.Map(x => x.CurrentBalance)
                .Not.Nullable();

            //this.Map(x => x.HsCurrentBalance);
            this.Map(x => x.HsCurrentBalanceChangedDate);

            this.Map(x => x.InsertDate);

            this.Map(x => x.DiscrepancyAdjustment)
                .Not.Nullable();

            this.Map(x => x.PrimaryInsuranceTypeId)
                .Nullable();

            this.Map(x => x.UnmatchActionDate)
                .Nullable();

            this.Map(x => x.UnmatchActionNotes)
                .Nullable();

            this.Map(x => x.SourceSystemKeyDisplay).Length(400)
                .Nullable();

            this.Map(x => x.IsPatientGuarantor).Nullable();
            this.Map(x => x.IsPatientMinor).Nullable();
            this.Map(x => x.InterestZero).Not.Nullable();

            this.Map(x => x.VisitAttributesJson, "VisitAttributes")
                .Nullable();

            this.References(x => x.UnmatchActionVisitPayUser, "UnmatchActionVisitPayUserId").Nullable();

            this.HasMany(x => x.VisitStates)
                .KeyColumn("VisitID")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                //.Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();

            this.HasMany(x => x.VisitAgingHistories)
                .KeyColumn("VisitID")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                //.Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();

            this.References(x => x.VPGuarantor)
                .Column("VPGuarantorId")
                .Not.Nullable();

            this.References(x => x.HsGuarantorMap)
                .Column("HsGuarantorMapId")
                .Nullable();

            this.References(x => x.HsGuarantorUnmatchReason)
                .Column("HsGuarantorUnmatchReasonId")
               .Nullable();

            this.Map(x => x.HsGuarantorMatchStatus, "HsGuarantorMatchStatusId")
                .CustomType<HsGuarantorMatchStatusEnum>()
                .Not.Nullable();

            this.HasMany(x => x.VisitInsurancePlans)
                .KeyColumn("VisitId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                //.Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();

            this.HasMany(x => x.BalanceTransferStatusHistory)
                .KeyColumn("VisitId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                //.Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();
        }
    }
}

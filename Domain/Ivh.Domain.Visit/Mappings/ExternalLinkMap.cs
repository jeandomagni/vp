﻿namespace Ivh.Domain.Visit.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class ExternalLinkMap : ClassMap<ExternalLink>
    {
        public ExternalLinkMap()
        {
            this.Schema("dbo");
            this.Table("ExternalLink");
            this.Id(x => x.ExternalLinkId);
            this.Map(x => x.Icon).Length(100).Nullable();
            this.Map(x => x.Text).Length(100).Not.Nullable();
            this.Map(x => x.Url).Length(500).Not.Nullable();
            this.Map(x => x.PersistDays).Nullable();
        }
    }
}
namespace Ivh.Domain.Visit.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class VisitTransactionAmountMap : ClassMap<VisitTransactionAmount>
    {
        public VisitTransactionAmountMap()
        {
            this.Schema("dbo");
            this.Table("VisitTransactionAmount");
            this.Id(x => x.VisitTransactionAmountId);

            this.References(x => x.VisitTransaction)
                .Column("VisitTransactionId")
                .Not.Nullable();

            this.Map(x => x.TransactionAmount)
                .Not.Nullable();

            this.Map(x => x.Reason)
                .Nullable();

            this.Map(x => x.InsertDate);
        }
    }
}
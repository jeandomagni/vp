﻿namespace Ivh.Domain.Visit.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class FacilityMap : ClassMap<Facility>
    {
        public FacilityMap()
        {
            this.Schema(Schemas.VisitPay.Dbo);
            this.Table(nameof(Facility));
            this.ReadOnly();

            this.Id(x => x.FacilityId);
            this.Map(x => x.FacilityDescription).Length(200).Nullable();
            this.Map(x => x.LogoFilename).Length(100).Nullable();
            this.Map(x => x.SourceSystemKey).Length(100).Nullable();

            this.References(x => x.RicState)
	            .Column("StateCode")
	            .Nullable();
            this.Map(x => x.CityState).Length(75).Nullable();
            this.Map(x => x.ContactPhone).Length(20).Nullable();
            this.Map(x => x.FacilityHours).Length(50).Nullable();
            this.Cache.ReadOnly();
        }
    }
}
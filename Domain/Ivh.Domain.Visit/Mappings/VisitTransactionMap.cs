namespace Ivh.Domain.Visit.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class VisitTransactionMap : ClassMap<VisitTransaction>
    {
        public VisitTransactionMap()
        {
            this.BatchSize(50);
            this.Schema("dbo");
            this.Table("VisitTransaction");
            this.Id(x => x.VisitTransactionId);

            this.References(x => x.BillingSystem)
                .Column("BillingsystemId")
                .Nullable();

            this.Map(x => x.VpGuarantorId)
                .Not.Nullable();

            this.Map(x => x.TransactionDate)
                .Not.Nullable();

            this.Map(x => x.PostDate)
                .Not.Nullable();

            this.HasMany(x => x.VisitTransactionAmounts)
                .KeyColumn("VisitTransactionId")
                .BatchSize(50)
                .Cascade.All();

            this.References(x => x.VisitTransactionAmount)
                .Fetch.Join()
                .Not.LazyLoad()
                .Column("VisitTransactionAmountId");

            this.Map(x => x.InsertDate);

            this.Map(x => x.OverrideInsertDate)
                .Nullable();

            this.Map(x => x.TransactionDescription);
            this.Map(x => x.SourceSystemKey);
            this.Map(x => x.SourceSystemKeySetDate);

            this.References(x => x.VpTransactionType)
                .Column("VpTransactionTypeId")
                .Not.Nullable();

            this.References(x => x.Visit)
                .Column("VisitId")
                .Not.Nullable();

            this.Map(x => x.PaymentAllocationId)
                .Nullable();
            
            this.References(x => x.MatchedBillingSystem)
                .Column("MatchedBillingSystemId")
                .Nullable();

            this.Map(x => x.MatchedSourceSystemKey).Nullable();

            this.Map(x => x.TransactionCodeSourceSystemKey).Nullable();
            this.Map(x => x.TransactionCodeBillingSystemId).Nullable();
            this.Map(x => x.CanIgnore).Not.Nullable();

            this.References(x => x.BalanceTransferStatus)
                .Column("BalanceTransferStatusId")
                .Nullable();


        }
    }
}
﻿
namespace Ivh.Domain.Visit.Mappings
{
	using Entities;
	using FluentNHibernate.Mapping;

	public class StateMap : ClassMap<State>
	{
		public StateMap()
		{
			this.Schema("dbo");
			this.Table("State");
			this.ReadOnly();

			this.Id(x => x.StateCode);
			this.Map(x => x.CmsRegionId).Not.Nullable();
			
			//this.References(x => x.RicState)
			//	.Column("StateCode")
			//	.Nullable();

			this.Cache.ReadOnly();
		}
	}
}

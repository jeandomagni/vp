﻿namespace Ivh.Domain.Visit.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class VisitItemizationStorageMap : ClassMap<VisitItemizationStorage>
    {
        public VisitItemizationStorageMap()
        {
            this.Schema("dbo");
            this.Table("VisitItemizationStorage");

            this.Id(x => x.VisitFileStoredId);
            this.Map(x => x.SystemId);
            this.Map(x => x.FacilityCode).Nullable();
            this.Map(x => x.FacilityDescription).Nullable();
            this.Map(x => x.AccountNumber);
            this.Map(x => x.Empi);
            this.Map(x => x.MatchedEmpi).Nullable();
            this.Map(x => x.MatchedAccountNumber).Nullable();
            this.Map(x => x.ExternalStorageKey);
            this.Map(x => x.InsertDate);
            this.Map(x => x.DateMatched).Nullable();
            this.Map(x => x.DateUnmatched).Nullable();
            this.Map(x => x.VisitSourceSystemKeyDisplay).Nullable();
            this.Map(x => x.MatchedVisitSourceSystemKeyDisplay).Nullable();
            this.Map(x => x.BillingSystemId).Nullable();
            
            this.References(x => x.VpGuarantor)
                .Column("VpGuarantorId");
        }
    }
}
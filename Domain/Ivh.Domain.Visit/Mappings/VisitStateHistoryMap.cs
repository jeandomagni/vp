﻿namespace Ivh.Domain.Visit.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class VisitStateHistoryMap : ClassMap<VisitStateHistory>
    {
        public VisitStateHistoryMap()
        {
            this.Schema("dbo");
            this.Table("VisitStateHistory");
            this.Id(x => x.HistoryId).Column("VisitStateHistoryId");
            this.References<Visit>(x => x.Entity).Column("VisitId").Not.Nullable();
            this.Map(x => x.InitialState).CustomType<VisitStateEnum>().Not.Nullable();
            this.Map(x => x.EvaluatedState).CustomType<VisitStateEnum>().Not.Nullable();
            this.Map(x => x.DateTime).Generated.Always();
            this.Map(x => x.RuleName).Nullable();
        }
    }
}

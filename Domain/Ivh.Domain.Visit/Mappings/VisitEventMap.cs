﻿
namespace Ivh.Domain.Visit.Mappings
{
	using Entities;
	using FluentNHibernate.Mapping;
    using Ivh.Common.VisitPay.Enums;
    using NHibernate.Linq;

    public class VisitEventMap : ClassMap<VisitEvent>
	{
		public VisitEventMap()
		{
			this.Schema("dbo");
			this.Table("VisitEventTracker");

			this.Id(x => x.VisitEventId).Not.Nullable();
            this.Map(x => x.ActionDate).Not.Nullable();
            this.Map(x => x.VisitId).Not.Nullable();
            this.Map(x => x.AdditionalData).Not.Nullable();
            this.Map(x => x.CommunicationTypeId).Nullable();
            this.Map(x => x.VpOutboundVisitMessageTypeId).Nullable();
        }
    }
}

﻿namespace Ivh.Domain.Visit.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class BalanceTransferStatusHistoryMap : ClassMap<BalanceTransferStatusHistory>
    {
        public BalanceTransferStatusHistoryMap()
        {
            this.Schema("dbo");
            this.Table("BalanceTransferStatusHistory");

            this.Id(x => x.BalanceTransferStatusHistoryId);
            this.Map(x => x.InsertDate);
            this.Map(x => x.Notes).Nullable();

            this.References(x => x.Visit)
                .Column("VisitID")
                .Not.Nullable();

            this.References(x => x.ChangedBy)
                .Column("ChangedBy")
                .Nullable();

            this.References(x => x.BalanceTransferStatus)
                .Column("BalanceTransferStatusId")
                .Not.Nullable();
        }
    }
}
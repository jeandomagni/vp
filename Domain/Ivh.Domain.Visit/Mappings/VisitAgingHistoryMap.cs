namespace Ivh.Domain.Visit.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class VisitAgingHistoryMap : ClassMap<VisitAgingHistory>
    {
        public VisitAgingHistoryMap()
        {
            this.Schema("dbo");
            this.Table("VisitAgingHistory");
            this.Id(x => x.VisitAgingHistoryId);

            this.References(x => x.Visit)
                .Column("VisitID");
                //.Not.Nullable();

            this.Map(x => x.InsertDate);

            this.Map(x => x.AgingTier)
                .Column("AgingTier")
                .CustomType<AgingTierEnum>()
                .Not.Nullable();

            this.Map(x => x.ChangeDescription);

            this.References(x => x.VisitPayUser)
                .Column("VisitPayUserId")
                .Nullable();
        }
    }
}
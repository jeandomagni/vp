﻿namespace Ivh.Domain.Visit.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class BalanceTransferStatusMap : ClassMap<BalanceTransferStatus>
    {
        public BalanceTransferStatusMap()
        {
            this.Schema("dbo");
            this.Table("BalanceTransferStatus");

            this.Id(x => x.BalanceTransferStatusId);
            this.Map(x => x.BalanceTransferStatusName);
            this.Map(x => x.BalanceTransferStatusDisplayName);

            this.Cache.ReadOnly();
        }
    }
}

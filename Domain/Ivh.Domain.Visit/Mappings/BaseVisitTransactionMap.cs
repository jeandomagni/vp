﻿namespace Ivh.Domain.Visit.Mappings
{
    using Common.Data.Constants;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class BaseVisitTransactionMap : ClassMap<BaseVisitTransaction>
    {
        public BaseVisitTransactionMap()
        {
            this.Polymorphism.Explicit();
            this.ReadOnly();

            this.Schema(Schemas.VisitPay.Base);
            this.Table("VisitTransaction");

            this.Id(x => x.VisitTransactionId);

            this.Map(x => x.TransactionDescription);
            this.Map(x => x.TransactionAmount);
            this.Map(x => x.PostDate);
            this.Map(x => x.VisitId);
            this.Map(x => x.VpTransactionTypeId).CustomType<VpTransactionTypeEnum>();
        }

    }
}

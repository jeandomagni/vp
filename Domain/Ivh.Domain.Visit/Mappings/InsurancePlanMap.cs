﻿namespace Ivh.Domain.Visit.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class InsurancePlanMap : ClassMap<InsurancePlan>
    {
        public InsurancePlanMap()
        {
            this.Schema("dbo");
            this.Table("InsurancePlan");
            this.Id(x => x.InsurancePlanId).Not.Nullable().GeneratedBy.Assigned();
            this.Map(x => x.BillingSystemId).Not.Nullable();
            this.Map(x => x.SourceSystemKey).Length(100).Not.Nullable();
            this.Map(x => x.InsurancePlanName).Length(100).Nullable();
            this.Map(x => x.DiscountPercentage).Nullable();
            this.References(x => x.PrimaryInsuranceType)
                .Column("PrimaryInsuranceTypeId")
                .Not.Nullable().Not.LazyLoad();
            this.References(x => x.EobExternalLink)
                .Column("EobExternalLinkId")
                .Nullable().Not.LazyLoad();
        }
    }
}
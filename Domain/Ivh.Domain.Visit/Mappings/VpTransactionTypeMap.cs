namespace Ivh.Domain.Visit.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class VpTransactionTypeMap : ClassMap<VpTransactionType>
    {
        public VpTransactionTypeMap()
        {
            this.Schema(Schemas.VisitPay.Dbo);
            this.Table(nameof(VpTransactionType));

            this.ReadOnly();
            this.Cache.ReadOnly();

            this.Id(x => x.VpTransactionTypeId);


            this.Map(x => x.TransactionType);

            this.Map(x => x.TransactionGroup)
                .Not.Nullable();

            this.Map(x => x.VpTransactionTypeName)
                .Column("VpTransactionType");

            this.Map(x => x.DisplayName);

            this.Map(x => x.TransactionSource);

            this.Map(x => x.Notes);

        }
    }
}
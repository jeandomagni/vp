﻿namespace Ivh.Domain.Visit.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class VisitInsurancePlanMap : ClassMap<VisitInsurancePlan>
    {
        public VisitInsurancePlanMap()
        {
            this.BatchSize(50);
            this.Schema("dbo");
            this.Table("VisitInsurancePlan");
            this.Id(x => x.VisitInsurancePlanId);
            this.References(x => x.Visit)
                .Column("VisitId")
                .Not.Nullable();
            this.Map(x => x.PayOrder).Not.Nullable();
            this.References(x => x.InsurancePlan)
                .Column("InsurancePlanId")
                .Not.Nullable()
                .NotFound.Ignore();
        }
    }
}
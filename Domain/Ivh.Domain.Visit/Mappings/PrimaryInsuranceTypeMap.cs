﻿namespace Ivh.Domain.Visit.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class PrimaryInsuranceTypeMap : ClassMap<PrimaryInsuranceType>
    {
        public PrimaryInsuranceTypeMap()
        {
            this.Schema("dbo");
            this.Table("PrimaryInsuranceType");
            this.Id(x => x.PrimaryInsuranceTypeId).Not.Nullable().GeneratedBy.Assigned();
            this.Map(x => x.PrimaryInsuranceTypeName)
                .Nullable();
            this.Map(x => x.SelfPayClassName)
                .Nullable();
        }
    }
}
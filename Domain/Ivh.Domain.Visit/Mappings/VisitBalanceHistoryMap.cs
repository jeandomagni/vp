﻿namespace Ivh.Domain.Visit.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class VisitBalanceHistoryMap : ClassMap<VisitBalanceHistory>
    {
        public VisitBalanceHistoryMap()
        {
            this.Schema("dbo");
            this.Table("VisitBalanceHistory");

            this.Id(x => x.VisitBalanceHistoryId);

            this.Map(x => x.InsertDate);
            this.Map(x => x.ChangeDescription);
            this.Map(x => x.CurrentBalance);
            this.Map(x => x.Ignore).Nullable();

            this.References(x => x.Visit)
                .Column("VisitID");
        }
    }
}
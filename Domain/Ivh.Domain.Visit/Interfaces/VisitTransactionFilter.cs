﻿namespace Ivh.Domain.Visit.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class VisitTransactionFilter
    {
        public string TransactionDateRangeFrom { get; set; }
        public string TransactionDateRangeTo { get; set; }
        public IList<VpTransactionTypeEnum> TransactionTypes { get; set; }
        public int? VisitId { get; set; }
        public int? VpGuarantorId { get; set; }
        public bool HideOffsettingVisitTransactions { get; set; }

        public string SortField { get; set; }

        /// <summary>
        /// "asc" or "desc", not case sensitive
        /// </summary>
        public string SortOrder { get; set; }
    }
}
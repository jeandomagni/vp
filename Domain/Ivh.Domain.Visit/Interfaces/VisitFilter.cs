﻿namespace Ivh.Domain.Visit.Interfaces
{
    using System.Collections.Generic;

    public class VisitFilter
    {
        public IList<int> VisitStateIds { get; set; }
        public int? GuarantorId { get; set; }
        public string BillingApplication { get; set; }
        public string VisitDateRangeFrom { get; set; }
        public string VisitDateRangeTo { get; set; }
        public IList<int> VisitIds { get; set; }
        public string SortField { get; set; }

        /// <summary>
        /// "asc" or "desc", not case sensitive
        /// </summary>
        public string SortOrder { get; set; }

        public bool? IsUnmatched { get; set; }
        public bool? IsMaxAge { get; set; }
        public bool? BillingHold { get; set; }
        public bool? IsPastDue { get; set; }
        public int? AgingCountMin { get; set; }
        public int? AgingCountMax { get; set; }
    }
}

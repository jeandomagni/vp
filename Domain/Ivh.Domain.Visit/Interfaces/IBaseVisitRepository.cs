﻿namespace Ivh.Domain.Visit.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IBaseVisitRepository : IRepository<BaseVisit>
    {
        IList<BaseVisit> GetPaymentSummaryVisits(IList<int> hsGuarantorIds);
        BaseVisit GetVisit(string visitSourceSystemKey, int visitBillingSystemId);
    }
}

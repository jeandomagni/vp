﻿namespace Ivh.Domain.Visit.Interfaces
{
    using System.Collections.Generic;
    using Entities;

    public class VisitTransactionResults
    {
        public IList<VisitTransaction> VisitTransactions { get; set; }
        public int TotalRecords { get; set; }
    }
}
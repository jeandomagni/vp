﻿namespace Ivh.Domain.Visit.Interfaces
{
    using Entities;

    public interface IVisitTransactionWriteService : IVisitTransactionService
    {
        void SaveVisitTransaction(Visit visit, VisitTransaction visitTransaction);
    }
}
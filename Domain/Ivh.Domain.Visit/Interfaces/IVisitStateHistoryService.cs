﻿namespace Ivh.Domain.Visit.Interfaces
{
    using Common.Base.Interfaces;

    public interface IVisitStateHistoryService : IDomainService
    {
        VisitStateHistoryResults GetAllVisitStateHistory(int vpGuarantorId, VisitStateHistoryFilter filter, int batch, int batchSize, bool onlyChanges = false);
    }
}
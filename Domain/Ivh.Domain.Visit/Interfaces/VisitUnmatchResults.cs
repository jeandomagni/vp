﻿namespace Ivh.Domain.Visit.Interfaces
{
    using System.Collections.Generic;
    using Entities;

    public class VisitUnmatchResults
    {
        public IReadOnlyList<Visit> VisitUnmatches { get; set; }
        public int TotalRecords { get; set; }
    }
}
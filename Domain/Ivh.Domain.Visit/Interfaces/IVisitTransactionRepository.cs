﻿namespace Ivh.Domain.Visit.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IVisitTransactionRepository : IReadOnlyRepository<VisitTransaction>
    {
        IReadOnlyList<string> GetTransactionTypesAsStrings();
        VisitTransactionResults GetTransactions(int vpGuarantorId, VisitTransactionFilter filter, int batch, int batchSize);
        int GetVisitTransactionTotals(int vpGuarantorId, VisitTransactionFilter filter);
        VisitTransaction GetBySourceSystemKey(string sourceSystemKey, int billingSystemId);
        IList<VisitTransaction> GetBySourceSystemKey(IList<string> sourceSystemKeys, int billingSystemId);
        IList<VisitTransaction> GetBySourceSystemKeyUnmatch(IList<string> sourceSystemKeys, int billingSystemId);
        IList<VpTransactionType> GetTransactionTypes();
        decimal GetSumOfAllInterestAssessedForDateRange(int vpGuarantorId, DateTime startDate, DateTime endDate);
        IList<VisitTransaction> GetVisitTransactions(int vpGuarantorId, int? visitId = null);
    }

    public interface IVisitTransactionWriteRepository : IVisitTransactionRepository, IRepository<VisitTransaction>
    {
    }
}
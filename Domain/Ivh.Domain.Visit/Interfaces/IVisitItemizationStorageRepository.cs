﻿namespace Ivh.Domain.Visit.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Application.HospitalData.Common.Models;
    using Common.Base.Interfaces;
    using Entities;

    public interface IVisitItemizationStorageRepository : IRepository<VisitItemizationStorage>
    {
        VisitItemizationStorage GetItemization(int vpGuarantorId, int visitFileStoreId, int currentVisitPayUserId);
        VisitItemizationStorage GetItemizationForVisit(int vpGuarantorId, string accountNumber, string empi, int currentVisitPayUserId);
        VisitItemizationStorage GetByExternalKey(Guid externalKey, int currentVisitPayUserId);
        VisitItemizationStorageResults GetItemizations(int vpGuarantorId, VisitItemizationStorageFilter filter, int pageNumber, int pageSize);
        VisitItemizationStorageResults GetItemizationTotals(int vpGuarantorId, VisitItemizationStorageFilter filter);
        IList<VisitItemizationStorage> GetItemizations(int vpGuarantorId, int currentVisitPayUserId);
        VisitItemizationDetailsResultModel GetVisitItemizationDetails(int billingSystemId, string hsGuarantorSourceSystemKey, string visitSourceSystemKey);
    }
}
﻿namespace Ivh.Domain.Visit.Interfaces
{
    public class VisitAgingHistoryFilter
    {
        public int VisitId { get; set; }

        public string SortField { get; set; }

        /// <summary>
        /// "asc" or "desc", not case sensitive
        /// </summary>
        public string SortOrder { get; set; }
    }
}

﻿namespace Ivh.Domain.Visit.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Guarantor.Entities;

    public interface IVisitPayUserIssueResultRepository : IRepository<IssueResult>
    {
        IReadOnlyList<IssueResult> AdjustAllGuarantorDates(int guarantorId, string datePart, int adjustment);
        IReadOnlyList<IssueResult> ClearGuarantorData(int guarantorId);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.Visit.Interfaces
{
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;

    public interface IVisitEventTrackerService : IDomainService
    {
        void LogCommunicationMessage(IList<int> visitIds, CommunicationTypeEnum communicationType, string additionalData);
    }
}

﻿namespace Ivh.Domain.Visit.Interfaces
{
    using System.Collections.Generic;
    using Entities;

    public class VisitAgingHistoryResults
    {
        public IReadOnlyList<VisitAgingHistory> VisitAgingHistories { get; set; }
        public int TotalRecords { get; set; }
    }
}
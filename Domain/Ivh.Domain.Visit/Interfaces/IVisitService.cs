﻿namespace Ivh.Domain.Visit.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Application.Base.Common.Interfaces.Entities.Visit;
    using Application.Core.Common.Dtos;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;
    using Guarantor.Entities;

    public interface IVisitService : IDomainService
    {
        IReadOnlyList<Visit> GetVisits(int vpGuarantorId);
        IReadOnlyList<Visit> GetVisits(int vpGuarantorId, IList<int> visitIds);
        IList<int> GetVisitIds(int vpGuarantorId, VisitStateEnum visitStateEnum);
        //IReadOnlyList<Visit> GetVisits(VisitFilter visitFilter);
        VisitResults GetVisits(int vpGuarantorId, VisitFilter filter, int? batch, int? batchSize);
        int GetVisitTotals(int vpGuarantorId, VisitFilter filter);
        IReadOnlyList<Visit> GetVisits(IList<VisitStateEnum> visitStates);
        IReadOnlyList<Visit> GetVisitsUnmatched(int vpGuarantorId);
        Visit GetVisit(int vpGuarantorId, int visitId);
        int GetVisitCount(int vpGuarantorId);
        //int GetVisitCount(int vpGuarantorId, IEnumerable<VisitStateEnum> visitStates);
        bool HasVisits(int vpGuarantorId, IEnumerable<VisitStateEnum> visitStates);
        bool HasVisitsWithFilter(int vpGuarantorId, VisitFilter visitFilter);
        IList<Visit> GetAllVisitsWithIntList(IList<int> toList, bool useLazyLoading);
        void SaveVisit(Visit visit);
        void UpdateTotalCharges(int visitId);
        IList<Visit> GetAllActiveVisits(Guarantor guarantor);
        IList<Visit> GetAllActiveNonFinancedVisits(int vpGuarantorId);
        IList<string> GetUniqueFacilityVisitNames(int vpGuarantorId);
        IList<Facility> GetUniqueFacilityVisits(int vpGuarantorId);
        int? GetRicCmsRegionId(int vpGuarantorId);
        CmsRegionEnum GetRicCmsRegionEnum(int vpGuarantorId);
        Visit GetVisitBySystemSourceKey(string systemSourceKey, int billingSystemId);
        Visit GetVisitBySystemSourceKeyUnmatched(int vpGuarantorId, string sourceSystemKey, int billingSystemId);
        IList<Visit> GetVisitsBySystemSourceKeyUnmatched(int vpGuarantorId, string sourceSystemKey, int billingSystemId);
        bool SetVisitToIneligible(Visit visit, string ineligibleReason);
        bool SetVisitToEligible(Visit visit);
        bool SetVisitToMaxAge(Visit visit);
        void Insert(Visit visit);
        IList<VisitResult> GetAllVisitsResults();
        bool DoesGuarantorHaveVisitWithInsurancePlanInList(int vpGuarantorId, List<string> insurancePlanSsks);
        Visit GetVisit(int visitId);
        //bool DoesVisitHaveUncollectableAgingCount(Visit visit);
        //bool IsVisitPendingUncollectable(Visit visit);
        bool IsVisitPastDue(IVisit visit);
        bool IsVisitFinalPastDue(IVisit visit);
        IList<VisitBalanceResult> GetVisitBalances(int vpGuarantorId, IList<int> visitIds = null);
        IList<int> GetVisitsThatWereActiveForDateRange(int vpGuarantorId, DateTime startDate, DateTime endDate);
        IList<VisitResult> GetFinalPastDueVisitsMissingNotification();
        void MarkFinalPastDueEmailAsSent(Visit visit);
        bool GuarantorHasVisitsInMultipleStates(int vpGuarantorId);
        IList<string> GetStateCodesForGuarantorVisits(int vpGuarantorId);
    }
}
﻿namespace Ivh.Domain.Visit.Interfaces
{
    using System.Collections.Generic;
    using Entities;

    public class VisitResults
    {
        public IList<Visit> Visits { get; set; }
        public int TotalRecords { get; set; }
    }
}
﻿namespace Ivh.Domain.Visit.Interfaces
{
    public interface IVisitAgingHistoryRepository
    {
        VisitAgingHistoryResults GetAllVisitAgingHistory(int vpGuarantorId, VisitAgingHistoryFilter filter, int batch, int batchSize);
    }
}
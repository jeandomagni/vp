﻿namespace Ivh.Domain.Visit.Interfaces
{
    public interface IHsBillingSystemResolutionProvider
    {
        int ResolveBillingSystemId(string visitSourceSystemKey);
    }
}
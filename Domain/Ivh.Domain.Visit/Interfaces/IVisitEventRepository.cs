﻿namespace Ivh.Domain.Visit.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IVisitEventRepository : IReadOnlyRepository<VisitEvent>
    {
        IList<VisitEvent> GetEventsForVisit(int vpVisitId);
        void Add(VisitEvent message);
    }
}
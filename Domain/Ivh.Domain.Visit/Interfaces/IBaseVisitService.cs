﻿namespace Ivh.Domain.Visit.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IBaseVisitService : IDomainService
    {
        IList<BaseVisit> GetBaseVisits(IList<int> hsGuarantorIds);
        BaseVisit GetVisit(int billingSystemId, string visitSourceSystemKey);
    }
}

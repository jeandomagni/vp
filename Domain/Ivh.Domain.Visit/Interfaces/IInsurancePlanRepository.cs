﻿namespace Ivh.Domain.Visit.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IInsurancePlanRepository : IRepository<InsurancePlan>
    {
        IReadOnlyList<VisitInsurancePlan> GetInsurancePlansForVisits(IList<VisitStateEnum> visitStates, int vpGuarantorId);
        InsurancePlan GetInsurancePlan(string sourceSystemKey, int billingSystemId);
    }
}
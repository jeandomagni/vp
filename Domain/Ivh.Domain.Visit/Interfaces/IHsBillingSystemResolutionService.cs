﻿namespace Ivh.Domain.Visit.Interfaces
{
    using Common.Base.Interfaces;

    public interface IHsBillingSystemResolutionService : IDomainService
    {
        int ResolveBillingSystemId(string visitSourceSystemKey);
    }
}
﻿namespace Ivh.Domain.Visit.Interfaces
{
    using System;

    public class VisitItemizationStorageFilter
    {
        public int? DateRange { get; set; }
        public DateTime? DateRangeFrom { get; set; }
        public DateTime? DateRangeTo { get; set; }
        public string FacilityCode { get; set; }
        public string PatientName { get; set; }
        public string SortField { get; set; }
        public string SortOrder { get; set; }
        public int CurrentVisitPayUserId { get; set; }
    }
}
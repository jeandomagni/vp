﻿namespace Ivh.Domain.Visit.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IHsFacilityRepository : IRepository<HsFacility>
    {
    }
}

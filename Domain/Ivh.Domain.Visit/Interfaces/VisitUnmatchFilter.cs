﻿namespace Ivh.Domain.Visit.Interfaces
{
    using System;

    public class VisitUnmatchFilter
    {
        public int? DateRangeFrom { get; set; }

        public DateTime? SpecificDateFrom { get; set; }

        public DateTime? SpecificDateTo { get; set; }

        public string MatchedSourceSystemKey { get; set; }

        public string HsGuarantorSourceSystemKey { get; set; }

        public int? VpGuarantorId { get; set; }

        public string SortField { get; set; }

        /// <summary>
        /// "asc" or "desc", not case sensitive
        /// </summary>
        public string SortOrder { get; set; }
    }
}
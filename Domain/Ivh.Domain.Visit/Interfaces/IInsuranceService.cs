﻿namespace Ivh.Domain.Visit.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IInsuranceService : IDomainService
    {
        IList<InsurancePlan> GetInsurancePlans();
        IList<PrimaryInsuranceType> GetPrimaryInsuranceTypes();
        void AddInsurancePlan(InsurancePlan insurancePlan, bool isUpdate);
        void AddPrimaryInsuranceType(PrimaryInsuranceType primaryInsuranceType, bool isUpdate);
        IReadOnlyList<VisitInsurancePlan> GetInsurancePlansForVisits(IList<VisitStateEnum> visitStates, int vpGuarantorId);
        InsurancePlan GetInsurancePlan(string sourceSystemKey, int billingSystemId);
    }
}
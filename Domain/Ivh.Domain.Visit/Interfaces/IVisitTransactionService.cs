﻿namespace Ivh.Domain.Visit.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IVisitTransactionService : IDomainService
    {
        IReadOnlyList<string> GetTransactionTypesAsStrings();
        void SetTransactionDescription(VisitTransaction transaction, bool isVoid = false, bool isRefund = false);
        string GetTransactionDescription(VpTransactionType vpTransactionType, bool isVoid = false, bool isRefund = false);
        VisitTransactionResults GetTransactions(int vpGuarantorId, VisitTransactionFilter filter, int batchNumber, int batchSize);
        int GetTransactionTotals(int vpGuarantorId, VisitTransactionFilter filter);
        IList<VpTransactionType> GetTransactionTypes();
        VisitTransaction GetBySourceSystemKey(string sourceSystemKey, int billingSystemId);
        IList<VisitTransaction> GetBySourceSystemKey(IList<string> sourceSystemKeys, int billingSystemId);
        IList<VisitTransaction> GetBySourceSystemKeyUnmatch(IList<string> sourceSystemKeys, int billingSystemId);
        
        VpTransactionType GetReversalTransactionType(VpTransactionTypeEnum currentType);
        VpTransactionType GetReallocationTransactionType(VpTransactionTypeEnum currentType);
        decimal GetSumOfAllInterestAssessedForDateRange(int vpGuarantorId, DateTime startDate, DateTime endDate);
        VisitTransaction GetById(int visitTransactionId);
        
        decimal? GetTotalCharges(int vpGuarantorId, int visitId);
        decimal? GetTotalCharges(IEnumerable<VisitTransaction> visitTransactions);

        decimal GetHsPaymentsForDateRange(IEnumerable<VisitTransaction> visitTransactions, DateTime? startingDate, DateTime endingDate);

        decimal GetReassessmentTotalsGreaterThanDateUsingInsertDate(int vpGuarantorId, int visitId, DateTime endingDate);

        decimal GetHsChargesForDateRange(IEnumerable<VisitTransaction> visitTransactions, DateTime? startingDate, DateTime endingDate);


        decimal GetHsAdjustmentsForDateRange(IEnumerable<VisitTransaction> visitTransactions, DateTime? startingDate, DateTime endingDate);

        decimal GetHsAdjustmentsExcludingInsuranceForDateRange(IEnumerable<VisitTransaction> visitTransactions, DateTime? startingDate, DateTime endingDate);

        decimal GetVpPaymentsForDateRange(int vpGuarantorId, int visitId, DateTime? startingDate, DateTime endingDate);

        decimal GetAdjustmentsForDateRange(IEnumerable<VisitTransaction> visitTransactions, DateTime? startingDate, DateTime endingDate);

        decimal GetHsAdjustmentsInsuranceForDateRange(IEnumerable<VisitTransaction> visitTransactions, DateTime? startingDate, DateTime endingDate);

        decimal GetVpDiscountsForDateRange(int vpGuarantorId, int visitId, DateTime? startingDate, DateTime endingDate);
        decimal GetVpDiscountsForDateRange(IEnumerable<VisitTransaction> visitTransactions, DateTime? startingDate, DateTime endingDate);

        decimal GetHsTransactionsAdjustmentsForDateRange(int vpGuarantorId, int visitId, DateTime startingDate, DateTime endingDate);
        decimal GetHsTransactionsAdjustmentsForDateRange(IEnumerable<VisitTransaction> visitTransactions, DateTime startingDate, DateTime endingDate);
        
        decimal GetCurrentBalanceAsOf(int vpGuarantorId, int visitId, DateTime periodEndDate);
        decimal GetCurrentBalanceAsOf(IEnumerable<VisitTransaction> visitTransactions, DateTime periodEndDate);

        IList<VisitTransaction> GetTransactionsForDateRange(int vpGuarantorId, int visitId, DateTime? startingDate, DateTime endingDate);
        void ResetCanIgnoreFlag(int vpGuarantorId, int visitId);
        IList<VisitTransaction> GetVisitTransactions(int vpGuarantorId, int? visitId = null);
    }
}
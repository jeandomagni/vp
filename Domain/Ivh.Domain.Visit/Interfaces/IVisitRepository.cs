﻿namespace Ivh.Domain.Visit.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Application.Core.Common.Dtos;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IVisitRepository : IRepository<Visit>
    {
        IReadOnlyList<Visit> GetVisits(int vpGuarantorId);
        IReadOnlyList<Visit> GetVisits(int vpGuarantorId, IList<int> visitIds);
        IReadOnlyList<Visit> GetVisits(IList<VisitStateEnum> visitStates);
        IReadOnlyList<Visit> GetVisits(VisitFilter filter);
        VisitResults GetVisits(int vpGuarantorId, VisitFilter filter, int? batch, int? batchSize);
        IList<int> GetVisitIds(int vpGuarantorId, VisitStateEnum visitStateEnum);
        IList<Visit> GetVisitsForGuarantor(int vpGuarantorId);
        IReadOnlyList<Visit> GetVisitsUnmatched(int vpGuarantorId);
        int GetVisitCount(int vpGuarantorId, IEnumerable<VisitStateEnum> visitStates);
        bool HasVisits(int vpGuarantorId, IEnumerable<VisitStateEnum> visitStates);
        int GetVisitTotals(int vpGuarantorId, VisitFilter filter);
        Visit GetVisit(int vpGuarantorId, int visitId);
        Visit GetBySourceSystemKey(string sourceSystemKey, int billingSystemId);
        Visit GetVisitBySystemSourceKeyUnmatched(int vpGuarantorId, string sourceSystemKey, int billingSystemId);
        IList<Visit> GetVisitsBySystemSourceKeyUnmatched(int vpGuarantorId, string sourceSystemKey, int billingSystemId);
        bool IsOnFinancePlan(Visit visit);
        //bool IsOnFinancePlan(int visitId);
        bool IsOnFinancePlanInStatus(Visit visit, IList<FinancePlanStatusEnum> financePlanStatuses);
        //bool IsInGracePeriod(Visit visit);
        bool IsOnStatement(Visit visit);
        IList<Visit> GetAllWithIntList(IList<int> toList, bool useLazyLoading);
        //DateTime? GetFirstStatementPeriodEndDate(Visit visit);
        //DateTime? GetMostRecentStatementPeriodEndDate(Visit visit);
        IList<VisitResult> GetFinalPastDueVisitsMissingNotification(int? vpGuarantorId = null);
        IList<VisitResult> GetAllVisitResults();
        VisitUnmatchResults GetAllVisitUnmatches(VisitUnmatchFilter filter, int pageNumber, int pageSize);
        StatementVisitStatusModel GetStatementVisitStateModel(Visit visit);
        bool DoesGuarantorHaveVisitWithInsurancePlanInList(int vpGuarantorId, List<string> insurancePlanIds);
        IList<VisitBalanceResult> GetVisitBalances(int vpGuarantorId, IList<int> visitIds);
        IList<int> GetVisitsThatWereActiveForDateRange(int vpGuarantorId, DateTime startDate, DateTime endDate);
        IList<Facility> GetFacilitiesForGuarantor(int vpGuarantorId);
        IReadOnlyList<Visit> GetAllActiveVisitsForVpGuarantor(int vpGuarantorId);
        bool GuarantorHasVisitsInMultipleStates(int vpGuarantorId);
        IList<string> GetStateCodesForGuarantorVisits(int vpGuarantorId);
    }
}
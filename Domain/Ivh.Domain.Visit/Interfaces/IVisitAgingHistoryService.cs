﻿namespace Ivh.Domain.Visit.Interfaces
{
    using Common.Base.Interfaces;

    public interface IVisitAgingHistoryService : IDomainService
    {
        VisitAgingHistoryResults GetAllVisitAgingHistory(int vpGuarantorId, VisitAgingHistoryFilter filter, int batch, int batchSize);
    }
}
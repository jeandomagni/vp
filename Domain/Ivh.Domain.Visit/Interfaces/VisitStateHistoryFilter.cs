﻿namespace Ivh.Domain.Visit.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class VisitStateHistoryFilter
    {
        public int VisitId { get; set; }
        public IList<VisitStateEnum> VisitStates { get; set; }

        public string SortField { get; set; }

        /// <summary>
        /// "asc" or "desc", not case sensitive
        /// </summary>
        public string SortOrder { get; set; }
    }
}

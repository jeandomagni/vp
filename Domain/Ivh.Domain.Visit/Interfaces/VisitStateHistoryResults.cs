﻿namespace Ivh.Domain.Visit.Interfaces
{
    using System.Collections.Generic;
    using Entities;

    public class VisitStateHistoryResults
    {
        public IReadOnlyList<VisitStateHistory> VisitStateHistories { get; set; }
        public int TotalRecords { get; set; }
    }
}
﻿namespace Ivh.Domain.Visit.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IFacilityService : IDomainService
    {
        Facility GetFacility(int facilityId);
        IReadOnlyList<Facility> GetAllFacilities();
        IReadOnlyList<Facility> GetFacilitiesByStateCode(string stateCode);
    }
}
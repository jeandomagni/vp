﻿namespace Ivh.Domain.Visit.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IHsFacilityResolutionService : IDomainService
    {
        void ResolveFacility(VisitItemizationStorage visitItemizationStorage);
        void ResolveFacilities(IList<VisitItemizationStorage> visitItemizationStorage);
        string ResolveFacilityDescription(string facilityCode);
    }
}
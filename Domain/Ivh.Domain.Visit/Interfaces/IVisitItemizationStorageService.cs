﻿namespace Ivh.Domain.Visit.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Application.HospitalData.Common.Models;
    using Common.Base.Interfaces;
    using Entities;

    public interface IVisitItemizationStorageService : IDomainService
    {
        VisitItemizationStorage GetItemization(int vpGuarantorId, int visitFileStoreId, int currentVisitPayUserId);
        VisitItemizationStorage GetItemizationForVisit(int vpGuarantorId, string accountNumber, string empi, int currentVisitPayUserId);
        VisitItemizationStorageResults GetItemizations(int vpGuarantorId, VisitItemizationStorageFilter filterDto, int pageNumber, int pageSize);
        int GetItemizationTotals(int vpGuarantorId, VisitItemizationStorageFilter filter);
        IDictionary<string, string> GetAllFacilities(int vpGuarantorId, int currentVisitPayUserId);
        IList<string> GetAllPatientNames(int vpGuarantorId, int currentVisitPayUserId);
        Task<VisitItemizationStorage> GetByExternalKeyAsync(Guid key, int currentVisitPayUserId);
        Task SaveAsync(VisitItemizationStorage visitItemization);
        VisitItemizationDetailsResultModel GetVisitItemizationDetails(int billingSystemId, string hsGuarantorSourceSystemKey, string visitSourceSystemKey);
    }
}
﻿namespace Ivh.Domain.Visit.Interfaces
{
    using Common.Base.Enums;
    using Common.State.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IVisitStateMachineService : IMachine<Visit, VisitStateEnum>
    {
    }
}

﻿namespace Ivh.Domain.Visit.Interfaces
{
    public interface IVisitStateHistoryRepository
    {
        VisitStateHistoryResults GetAllVisitStateHistory(int vpGuarantorId, VisitStateHistoryFilter filter, int batch, int batchSize, bool onlyChanges = false);
    }
}
﻿namespace Ivh.Domain.Visit.Interfaces
{
    using System.Collections.Generic;
    using Entities;

    public class VisitItemizationStorageResults
    {
        public IList<VisitItemizationStorage> Itemizations { get; set; }
        public int TotalRecords { get; set; }
    }
}
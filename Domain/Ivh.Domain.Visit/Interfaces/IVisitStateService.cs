﻿namespace Ivh.Domain.Visit.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IVisitStateService : IDomainService
    {
        bool EvaluateVisitState(Visit visit);
    }
}
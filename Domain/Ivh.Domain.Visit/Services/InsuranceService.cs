﻿namespace Ivh.Domain.Visit.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;

    public class InsuranceService : DomainService, IInsuranceService
    {
        private readonly IInsurancePlanRepository _insurancePlanRepository;
        private readonly IPrimaryInsuranceTypeRepository _primaryInsuranceTypeRepository;

        public InsuranceService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            IInsurancePlanRepository insurancePlanRepository,
            IPrimaryInsuranceTypeRepository primaryInsuranceTypeRepository) : base(serviceCommonService)
        {
            this._insurancePlanRepository = insurancePlanRepository;
            this._primaryInsuranceTypeRepository = primaryInsuranceTypeRepository;
        }

        public IList<InsurancePlan> GetInsurancePlans()
        {
            return this._insurancePlanRepository.GetQueryable().ToList();
        }

        public InsurancePlan GetInsurancePlan(string sourceSystemKey, int billingSystemId)
        {
            return this._insurancePlanRepository.GetInsurancePlan(sourceSystemKey, billingSystemId);
        }

        public IList<PrimaryInsuranceType> GetPrimaryInsuranceTypes()
        {
            return this._primaryInsuranceTypeRepository.GetQueryable().ToList();
        }

        public void AddInsurancePlan(InsurancePlan insurancePlan, bool isUpdate)
        {
            if (isUpdate)
            {
                this._insurancePlanRepository.Update(insurancePlan);
            }
            else
            {
                this._insurancePlanRepository.Insert(insurancePlan);
            }
        }

        public void AddPrimaryInsuranceType(PrimaryInsuranceType primaryInsuranceType, bool isUpdate)
        {
            if (isUpdate)
            {
                this._primaryInsuranceTypeRepository.Update(primaryInsuranceType);
            }
            else
            {
                this._primaryInsuranceTypeRepository.Insert(primaryInsuranceType);
            }
        }

        public IReadOnlyList<VisitInsurancePlan> GetInsurancePlansForVisits(IList<VisitStateEnum> visitStates, int vpGuarantorId)
        {
            return this._insurancePlanRepository.GetInsurancePlansForVisits(visitStates, vpGuarantorId);
        }
    }
}
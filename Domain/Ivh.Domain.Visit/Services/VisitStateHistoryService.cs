﻿namespace Ivh.Domain.Visit.Services
{
    using System;
    using Base.Interfaces;
    using Base.Services;
    using Interfaces;

    public class VisitStateHistoryService : DomainService, IVisitStateHistoryService
    {
        private readonly IVisitStateHistoryRepository _visitStateHistoryRepository;
        public VisitStateHistoryService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            IVisitStateHistoryRepository visitStateHistoryRepository) : base(serviceCommonService)
        {
            this._visitStateHistoryRepository = visitStateHistoryRepository;
        }

        public VisitStateHistoryResults GetAllVisitStateHistory(int vpGuarantorId, VisitStateHistoryFilter filter, int batch, int batchSize, bool onlyChanges = false)
        {
            return this._visitStateHistoryRepository.GetAllVisitStateHistory(vpGuarantorId, filter, batch, batchSize, onlyChanges);
        }
    }
}
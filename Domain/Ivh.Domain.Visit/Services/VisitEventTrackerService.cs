﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.Visit.Services
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;
    using NHibernate;

    public class VisitEventTrackerService : IVisitEventTrackerService
    {
        private readonly Lazy<IVisitEventRepository> _visitEventRepository;
        private readonly ISession _session;

        public VisitEventTrackerService(Lazy<IVisitEventRepository> visitEventRepository, ISessionContext<VisitPay> sessionContext)
        {
            this._visitEventRepository = visitEventRepository;
            this._session = sessionContext.Session;
        }

        /// <summary>
        /// Used to log visits that are involved in communication events related to dbo.Communication
        /// </summary>
        public void LogCommunicationMessage(IList<int> visitIds, CommunicationTypeEnum communicationType, string additionalData)
        {
            if (visitIds == null) {
                return;
            }

            Exception workException = null;
            UnitOfWork unitOfWork = new UnitOfWork(this._session);
            try
            {
                foreach (int visitId in visitIds)
                {
                    this._visitEventRepository.Value.Add(new VisitEvent()
                    {
                        VisitId = visitId,
                        CommunicationTypeId = (int)communicationType,
                        ActionDate = DateTime.Now,
                        AdditionalData = additionalData
                    });
                }
                unitOfWork.Commit();
            }
            catch (Exception e)
            {
                workException = e;
            }
            finally
            {
                unitOfWork.Dispose();

                if (workException != null)
                {
                    throw workException;
                }
            }
        }
    }
}

﻿namespace Ivh.Domain.Visit.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Entities;
    using Interfaces;

    public class FacilityService : DomainService, IFacilityService
    {
        private readonly Lazy<IFacilityRepository> _facilityRepo;

        public FacilityService(
            Lazy<IFacilityRepository> facilityRepo,
            Lazy<IDomainServiceCommonService> domainSvcCommonSvc)
            : base(domainSvcCommonSvc)
        {
            this._facilityRepo = facilityRepo;
        }

        public Facility GetFacility(int facilityId) => this._facilityRepo.Value.GetById(facilityId);

        public IReadOnlyList<Facility> GetAllFacilities() => this._facilityRepo.Value.GetQueryable().ToList();

        public IReadOnlyList<Facility> GetFacilitiesByStateCode(string stateCode)
        {
            return this._facilityRepo.Value.GetQueryable().ToList().Where(f => f.RicState?.StateCode == stateCode).ToList();
        }
    }
}
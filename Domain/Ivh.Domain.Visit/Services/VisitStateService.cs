﻿namespace Ivh.Domain.Visit.Services
{
    using System;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;

    public class VisitStateService : DomainService, IVisitStateService
    {
        private readonly Lazy<IVisitRepository> _visitRepository;
        private readonly Lazy<IVisitStateMachineService> _visitStateMachineService;

        public VisitStateService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IVisitRepository> visitRepository,
            Lazy<IVisitStateMachineService> visitStateMachineService) : base(serviceCommonService)
        {
            this._visitRepository = visitRepository;
            this._visitStateMachineService = visitStateMachineService;
        }

        public bool EvaluateVisitState(Visit visit)
        {
            VisitStateEnum initialVisitStateEnum = visit.CurrentVisitState;
            bool statusChanged = initialVisitStateEnum != this._visitStateMachineService.Value.Evaluate(visit);
            if (statusChanged)
            {
                // ensure QueueUnpublishedMessages
                if (visit.VisitId != default(int))
                {
                    this._visitRepository.Value.InsertOrUpdate(visit);
                }
            }
            return statusChanged;
        }

        private bool IsOnFinancePlan(Visit visit)
        {
            return this._visitRepository.Value.IsOnFinancePlan(visit);
        }

        private bool IsOnStatement(Visit visit)
        {
            return this._visitRepository.Value.IsOnStatement(visit);
        }
    }
}
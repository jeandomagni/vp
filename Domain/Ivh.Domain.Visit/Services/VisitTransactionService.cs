﻿namespace Ivh.Domain.Visit.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Common.Base.Utilities.Helpers;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;

    public class VisitTransactionService : DomainService, IVisitTransactionService, IVisitTransactionWriteService
    {
        private readonly Lazy<IVisitTransactionRepository> _visitTransactionRepository;
        private readonly Lazy<IVisitTransactionWriteRepository> _visitTransactionWriteRepository;
        private readonly Lazy<IVisitRepository> _visitRepository;

        public VisitTransactionService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IVisitTransactionRepository> visitTransactionRepository,
            Lazy<IVisitTransactionWriteRepository> visitTransactionWriteRepository,
            Lazy<IVisitRepository> visitRepository
            ) : base(serviceCommonService)
        {
            this._visitTransactionRepository = visitTransactionRepository;
            this._visitTransactionWriteRepository = visitTransactionWriteRepository;
            this._visitRepository = visitRepository;
        }

        public IReadOnlyList<string> GetTransactionTypesAsStrings()
        {
            return this._visitTransactionRepository.Value.GetTransactionTypesAsStrings();
        }

        public void SetTransactionDescription(VisitTransaction transaction, bool isVoid = false, bool isRefund = false)
        {
            if (transaction.VpTransactionType.IsHs())
            {
                return;
            }

            string displayDescription = this.GetTransactionDescription(transaction.VpTransactionType, isVoid, isRefund);

            transaction.TransactionDescription = displayDescription;
        }

        public string GetTransactionDescription(VpTransactionType vpTransactionType, bool isVoid = false, bool isRefund = false)
        {
            if (vpTransactionType == null)
            {
                return string.Empty;
            }

            string clientShortName = this.Client != null ? this.Client.Value.ClientShortName : string.Empty;
            string displayDescription = vpTransactionType.DisplayName;

            if (!string.IsNullOrEmpty(vpTransactionType.DisplayName) && vpTransactionType.DisplayName.Contains("{0}"))
            {
                displayDescription = string.Format(vpTransactionType.DisplayName, clientShortName);
            }

            if (isVoid)
            {
                displayDescription = string.Concat("Voided ", displayDescription);
            }

            if (isRefund)
            {
                displayDescription = string.Concat("Refunded ", displayDescription);
            }

            return displayDescription;
        }

        public VisitTransactionResults GetTransactions(int vpGuarantorId, VisitTransactionFilter filter, int batch, int batchSize)
        {
            VisitTransactionResults result = this._visitTransactionRepository.Value.GetTransactions(vpGuarantorId, filter, batch, batchSize);
            return result;
        }

        public int GetTransactionTotals(int vpGuarantorId, VisitTransactionFilter filter)
        {
            return this._visitTransactionRepository.Value.GetVisitTransactionTotals(vpGuarantorId, filter);
        }

        public IList<VpTransactionType> GetTransactionTypes()
        {
            return this._visitTransactionRepository.Value.GetTransactionTypes();
        }

        public VisitTransaction GetBySourceSystemKey(string sourceSystemKey, int billingSystemId)
        {
            return this._visitTransactionRepository.Value.GetBySourceSystemKey(sourceSystemKey, billingSystemId);
        }

        public IList<VisitTransaction> GetBySourceSystemKey(IList<string> sourceSystemKeys, int billingSystemId)
        {
            return this._visitTransactionRepository.Value.GetBySourceSystemKey(sourceSystemKeys, billingSystemId).ToList();
        }
        
        public IList<VisitTransaction> GetBySourceSystemKeyUnmatch(IList<string> sourceSystemKeys, int billingSystemId)
        {
            return this._visitTransactionRepository.Value.GetBySourceSystemKeyUnmatch(sourceSystemKeys, billingSystemId);
        }
        
        public VisitTransaction GetById(int visitTransactionId)
        {
            return this._visitTransactionRepository.Value.GetById(visitTransactionId);
        }

        public VpTransactionType GetTransactionTypeForPendingPatientCash(PaymentAllocationTypeEnum typeEnum)
        {
            //todo: This is probably a bit of a hack..  Should fix (Scar)
            if (typeEnum == PaymentAllocationTypeEnum.VisitPrincipal)
            {
                return this.GetTransactionTypes().FirstOrDefault(x => x.VpTransactionTypeId == (int)VpTransactionTypeEnum.VppPrincipalPayment);
            }
            if (typeEnum == PaymentAllocationTypeEnum.VisitInterest)
            {
                return this.GetTransactionTypes().FirstOrDefault(x => x.VpTransactionTypeId == (int)VpTransactionTypeEnum.VppInterestPayment);
            }
            if (typeEnum == PaymentAllocationTypeEnum.VisitDiscount)
            {
                return this.GetTransactionTypes().FirstOrDefault(x => x.VpTransactionTypeId == (int)VpTransactionTypeEnum.VppDiscount);
            }
            return null;
        }

        public VpTransactionType GetReversalTransactionType(VpTransactionTypeEnum currentType)
        {
            switch (currentType)
            {
                case VpTransactionTypeEnum.VppDiscount:
                    return this.GetTransactionTypes().FirstOrDefault(x => x.VpTransactionTypeId == (int)VpTransactionTypeEnum.VppDiscountHsReversal);
                case VpTransactionTypeEnum.VppInterestPayment:
                case VpTransactionTypeEnum.VppInterestPaymentReassessmentReallocation:
                case VpTransactionTypeEnum.VppInterestPaymentReassessmentReversal:
                    return this.GetTransactionTypes().FirstOrDefault(x => x.VpTransactionTypeId == (int)VpTransactionTypeEnum.VppInterestPaymentHsReversal);
                case VpTransactionTypeEnum.VppPrincipalPayment:
                case VpTransactionTypeEnum.VppPrincipalPaymentReassessmentReallocation:
                case VpTransactionTypeEnum.VppPrincipalPaymentReassessmentReversal:
                    return this.GetTransactionTypes().FirstOrDefault(x => x.VpTransactionTypeId == (int)VpTransactionTypeEnum.VppPrincipalPaymentHsReversal);
                default:
                    return this.GetTransactionTypes().FirstOrDefault(x => x.VpTransactionTypeId == (int)VpTransactionTypeEnum.VppPrincipalPaymentHsReversal);
            }
        }

        public VpTransactionType GetReallocationTransactionType(VpTransactionTypeEnum currentType)
        {
            switch (currentType)
            {
                case VpTransactionTypeEnum.VppDiscount:
                    return this.GetTransactionTypes().FirstOrDefault(x => x.VpTransactionTypeId == (int)VpTransactionTypeEnum.VppDiscountHsReallocation);
                case VpTransactionTypeEnum.VppInterestPayment:
                case VpTransactionTypeEnum.VppInterestPaymentReassessmentReallocation:
                case VpTransactionTypeEnum.VppInterestPaymentReassessmentReversal:
                    return this.GetTransactionTypes().FirstOrDefault(x => x.VpTransactionTypeId == (int)VpTransactionTypeEnum.VppInterestPaymentHsReallocation);
                case VpTransactionTypeEnum.VppPrincipalPayment:
                case VpTransactionTypeEnum.VppPrincipalPaymentReassessmentReallocation:
                case VpTransactionTypeEnum.VppPrincipalPaymentReassessmentReversal:
                    return this.GetTransactionTypes().FirstOrDefault(x => x.VpTransactionTypeId == (int)VpTransactionTypeEnum.VppPrincipalPaymentHsReallocation);
                default:
                    return this.GetTransactionTypes().FirstOrDefault(x => x.VpTransactionTypeId == (int)VpTransactionTypeEnum.VppPrincipalPaymentHsReallocation);
            }
        }
        
        public decimal GetSumOfAllInterestAssessedForDateRange(int vpGuarantorId, DateTime startDate, DateTime endDate)
        {
            return this._visitTransactionRepository.Value.GetSumOfAllInterestAssessedForDateRange(vpGuarantorId, startDate, endDate);
        }
        
        public void SaveVisitTransaction(Visit visit, VisitTransaction visitTransaction)
        {
            visitTransaction.BalanceTransferStatus = visit.BalanceTransferStatus;
            visitTransaction.Visit = visit;
            this._visitTransactionWriteRepository.Value.InsertOrUpdate(visitTransaction);
            
            visit.UnpublishedVisitBalanceChanged.Add(new VisitBalanceChangedEvent
            {
                Visit = visit
            });
        }

        public decimal? GetTotalCharges(int vpGuarantorId, int visitId)
        {
            return this.GetTotalCharges(this._visitTransactionRepository.Value.GetVisitTransactions(vpGuarantorId, visitId));
        }

        public decimal? GetTotalCharges(IEnumerable<VisitTransaction> visitTransactions)
        {
            return visitTransactions.Where(x => x.VpTransactionType.IsHsCharge()).Sum(x => x.VisitTransactionAmount?.TransactionAmount ?? 0);
        }

        [Obsolete(ObsoleteDescription.VisitTransactions)]
        public decimal GetHsPaymentsForDateRange(IEnumerable<VisitTransaction> visitTransactions, DateTime? startingDate, DateTime endingDate)
        {
            if (startingDate.HasValue)
            {
                return visitTransactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsHsPayment()
                                                                             && (x.OverrideInsertDate ?? x.InsertDate) >= startingDate.Value
                                                                             && (x.OverrideInsertDate ?? x.InsertDate) <= endingDate)
                ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
            }
            return visitTransactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsHsPayment()
                                                                         && (x.OverrideInsertDate ?? x.InsertDate) <= endingDate)
            ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
        }

        [Obsolete(ObsoleteDescription.VisitTransactions)]
        public decimal GetReassessmentTotalsGreaterThanDateUsingInsertDate(int vpGuarantorId, int visitId, DateTime endingDate)
        {
            return this.GetReassessmentTotalsGreaterThanDateUsingInsertDate(this._visitTransactionRepository.Value.GetVisitTransactions(vpGuarantorId, visitId), endingDate);
        }

        [Obsolete(ObsoleteDescription.VisitTransactions)]
        public decimal GetReassessmentTotalsGreaterThanDateUsingInsertDate(IEnumerable<VisitTransaction> visitTransactions, DateTime endingDate)
        {
            return visitTransactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsReassessment()
                                                                         && (x.InsertDate) > endingDate)
            ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
        }
        
        [Obsolete(ObsoleteDescription.VisitTransactions)]
        public decimal GetHsChargesForDateRange(IEnumerable<VisitTransaction> visitTransactions, DateTime? startingDate, DateTime endingDate)
        {
            if (startingDate.HasValue)
            {
                return visitTransactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsHsCharge()
                                                                             && (x.OverrideInsertDate ?? x.InsertDate) >= startingDate.Value
                                                                             && (x.OverrideInsertDate ?? x.InsertDate) <= endingDate)
                ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
            }
            return visitTransactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsHsCharge()
                                                                         && (x.OverrideInsertDate ?? x.InsertDate) <= endingDate)
            ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
        }

        [Obsolete(ObsoleteDescription.VisitTransactions)]
        public decimal GetHsAdjustmentsForDateRange(IEnumerable<VisitTransaction> visitTransactions, DateTime? startingDate, DateTime endingDate)
        {
            if (startingDate.HasValue)
            {
                return visitTransactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsHsAdjustment()
                                                                             && (x.OverrideInsertDate ?? x.InsertDate) >= startingDate.Value
                                                                             && (x.OverrideInsertDate ?? x.InsertDate) <= endingDate)
                ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
            }
            return visitTransactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsHsAdjustment()
                                                                         && (x.OverrideInsertDate ?? x.InsertDate) <= endingDate)
            ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
        }

        [Obsolete(ObsoleteDescription.VisitTransactions)]
        public virtual decimal GetHsAdjustmentsExcludingInsuranceForDateRange(IEnumerable<VisitTransaction> visitTransactions, DateTime? startingDate, DateTime endingDate)
        {

            visitTransactions = visitTransactions.Where(x => x.InsertDate != null &&
                                                                               x.VpTransactionType.IsHsAdjustment() &&
                                                                               !x.VpTransactionType.IsHsInsuranceAdjustment() &&
                                                                               (x.OverrideInsertDate ?? x.InsertDate) <= endingDate).ToList();

            if (startingDate.HasValue)
            {
                visitTransactions = visitTransactions.Where(x => x.InsertDate != null && (x.OverrideInsertDate ?? x.InsertDate) >= startingDate.Value).ToList();
            }

            return visitTransactions.Sum(x => x.VisitTransactionAmount.TransactionAmount);
        }

        [Obsolete(ObsoleteDescription.VisitTransactions)]
        public decimal GetVpPaymentsForDateRange(int vpGuarantorId, int visitId, DateTime? startingDate, DateTime endingDate)
        {
            return this.GetVpPaymentsForDateRange(this._visitTransactionRepository.Value.GetVisitTransactions(vpGuarantorId, visitId), startingDate, endingDate);
        }

        [Obsolete(ObsoleteDescription.VisitTransactions)]
        public decimal GetVpPaymentsForDateRange(IEnumerable<VisitTransaction> visitTransactions, DateTime? startingDate, DateTime endingDate)
        {
            if (startingDate.HasValue)
            {
                return visitTransactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsVpPayment()
                                                                             && (x.OverrideInsertDate ?? x.InsertDate) >= startingDate.Value
                                                                             && (x.OverrideInsertDate ?? x.InsertDate) <= endingDate)
                ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
            }
            return visitTransactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsVpPayment()
                                                                         && (x.OverrideInsertDate ?? x.InsertDate) <= endingDate)
            ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
        }

        [Obsolete(ObsoleteDescription.VisitTransactions)]
        public decimal GetAdjustmentsForDateRange(IEnumerable<VisitTransaction> visitTransactions, DateTime? startingDate, DateTime endingDate)
        {
            if (startingDate.HasValue)
            {
                return visitTransactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsAdjustment()
                                                                             && (x.OverrideInsertDate ?? x.InsertDate) >= startingDate.Value
                                                                             && (x.OverrideInsertDate ?? x.InsertDate) <= endingDate)
                ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
            }
            return visitTransactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsAdjustment()
                                                                         && (x.OverrideInsertDate ?? x.InsertDate) <= endingDate)
            ).Sum(x => x.VisitTransactionAmount.TransactionAmount);

        }
        
        [Obsolete(ObsoleteDescription.VisitTransactions)]
        public decimal GetHsAdjustmentsInsuranceForDateRange(IEnumerable<VisitTransaction> visitTransactions, DateTime? startingDate, DateTime endingDate)
        {

            List<VisitTransaction> transactions = visitTransactions.Where(x => x.InsertDate != null &&
                                                                               x.VpTransactionType.IsHsInsuranceAdjustment() &&
                                                                               (x.OverrideInsertDate ?? x.InsertDate) <= endingDate).ToList();

            if (startingDate.HasValue)
            {
                transactions = transactions.Where(x => x.InsertDate != null && (x.OverrideInsertDate ?? x.InsertDate) >= startingDate.Value).ToList();
            }

            return transactions.Sum(x => x.VisitTransactionAmount.TransactionAmount);
        }

        [Obsolete(ObsoleteDescription.VisitTransactions)]
        public decimal GetVpDiscountsForDateRange(int vpGuarantorId, int visitId, DateTime? startingDate, DateTime endingDate)
        {
            return this.GetVpDiscountsForDateRange(this._visitTransactionRepository.Value.GetVisitTransactions(vpGuarantorId, visitId), startingDate, endingDate);
        }

        [Obsolete(ObsoleteDescription.VisitTransactions)]
        public decimal GetVpDiscountsForDateRange(IEnumerable<VisitTransaction> visitTransactions, DateTime? startingDate, DateTime endingDate)
        {
            if (startingDate.HasValue)
            {
                return visitTransactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsVpDiscount()
                                                                             && (x.OverrideInsertDate ?? x.InsertDate) >= startingDate.Value
                                                                             && (x.OverrideInsertDate ?? x.InsertDate) <= endingDate)
                ).Sum(x => x.VisitTransactionAmount.TransactionAmount);
            }
            return visitTransactions.Where(x => x.InsertDate != null && (x.VpTransactionType.IsVpDiscount()
                                                                         && (x.OverrideInsertDate ?? x.InsertDate) <= endingDate)
            ).Sum(x => x.VisitTransactionAmount.TransactionAmount);

        }

        [Obsolete(ObsoleteDescription.VisitTransactions)]
        public decimal GetHsTransactionsAdjustmentsForDateRange(int vpGuarantorId, int visitId, DateTime startingDate, DateTime endingDate)
        {
            decimal transactionsForDateRange = this.GetTransactionsForDateRange(vpGuarantorId, visitId, startingDate, endingDate)
                .Where(x => EnumHelper<VpTransactionTypeEnum>.ContainsIntersect(x.VpTransactionType.VpTransactionTypeEnum, VpTransactionTypeEnumCategory.Hospital, VpTransactionTypeEnumCategory.Adjustment))
                .Sum(x => x.VisitTransactionAmount.TransactionAmount);

            return transactionsForDateRange;
        }

        [Obsolete(ObsoleteDescription.VisitTransactions)]
        public decimal GetHsTransactionsAdjustmentsForDateRange(IEnumerable<VisitTransaction> visitTransactions, DateTime startingDate, DateTime endingDate)
        {
            decimal transactionsForDateRange = this.GetTransactionsForDateRange(visitTransactions, startingDate, endingDate)
                .Where(x => EnumHelper<VpTransactionTypeEnum>.ContainsIntersect(x.VpTransactionType.VpTransactionTypeEnum, VpTransactionTypeEnumCategory.Hospital, VpTransactionTypeEnumCategory.Adjustment))
                .Sum(x => x.VisitTransactionAmount.TransactionAmount);

            return transactionsForDateRange;
        }

        [Obsolete(ObsoleteDescription.VisitTransactions)]
        public IList<VisitTransaction> GetTransactionsForDateRange(int vpGuarantorId, int visitId, DateTime? startingDate, DateTime endingDate)
        {
            return this.GetTransactionsForDateRange(this._visitTransactionRepository.Value.GetVisitTransactions(vpGuarantorId, visitId), startingDate, endingDate);
        }

        [Obsolete(ObsoleteDescription.VisitTransactions)]
        public IList<VisitTransaction> GetTransactionsForDateRange(IEnumerable<VisitTransaction> visitTransactions, DateTime? startingDate, DateTime endingDate)
        {
            if (startingDate.HasValue)
            {
                return visitTransactions.Where(x => x.InsertDate != null && ((x.OverrideInsertDate ?? x.InsertDate) >= startingDate.Value
                                                                             && (x.OverrideInsertDate ?? x.InsertDate) <= endingDate)
                ).ToList();
            }
            return visitTransactions.Where(x => x.InsertDate != null && ((x.OverrideInsertDate ?? x.InsertDate) <= endingDate)
            ).ToList();
        }

        // TODO: THIS IS WRONG. Should be moved to VisitService
        [Obsolete(ObsoleteDescription.VisitTransactions)]
        public decimal GetCurrentBalanceAsOf(int vpGuarantorId, int visitId, DateTime periodEndDate)
        {
            return this.GetCurrentBalanceAsOf(this._visitTransactionRepository.Value.GetVisitTransactions(vpGuarantorId, visitId), periodEndDate);
        }

        public decimal GetCurrentBalanceAsOf(IEnumerable<VisitTransaction> visitTransactions, DateTime periodEndDate)
        {
            return visitTransactions.Where(x =>
                x.InsertDate != null && ((x.OverrideInsertDate ?? x.InsertDate) <= periodEndDate)
            ).Sum(t => t.VisitTransactionAmount.TransactionAmount);
        }

        [Obsolete(ObsoleteDescription.VisitTransactions)]
        public void ResetCanIgnoreFlag(int vpGuarantorId, int visitId)
        {
            IList<VisitTransaction> visitTransactions = this._visitTransactionRepository.Value.GetVisitTransactions(vpGuarantorId, visitId);
            foreach (VisitTransaction transaction in visitTransactions)
            {
                transaction.CanIgnore = false;
                this._visitTransactionWriteRepository.Value.InsertOrUpdate(transaction);
            }
        }

        public IList<VisitTransaction> GetVisitTransactions(int vpGuarantorId, int? visitId = null)
        {
            return this._visitTransactionRepository.Value.GetVisitTransactions(vpGuarantorId, visitId);
        }
    }
}
﻿namespace Ivh.Domain.Visit.Services
{
    using System;
    using Common.Base.Enums;
    using Common.EventJournal.Interfaces;
    using Common.State;
    using Common.State.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;
    using Rules;

    public class VisitStateMachineService : Machine<Visit, VisitStateEnum>, IVisitStateMachineService
    {
        public VisitStateMachineService(
            IContextProvider<Visit, VisitStateEnum> contextProvider,
            VisitStateRules rules,
            Lazy<IEventJournalService> eventJournalService)
            : base(contextProvider, rules, eventJournalService, VisitStateEnum.NotSet)
        {
        }
    }
}

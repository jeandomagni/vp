﻿namespace Ivh.Domain.Visit.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Application.Base.Common.Interfaces.Entities.Visit;
    using Application.Core.Common.Dtos;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using Guarantor.Entities;
    using Interfaces;

    public class VisitService : DomainService, IVisitService
    {
        private readonly Lazy<IVisitRepository> _visitRepository;
        private readonly Lazy<IVisitTransactionRepository> _visitTransactionRepository;
        private readonly Lazy<IVisitStateService> _visitStateService;

        public VisitService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IVisitRepository> visitRepository,
            Lazy<IVisitTransactionRepository> visitTransactionRepository,
            Lazy<IVisitStateService> visitStateService) : base(serviceCommonService)
        {
            this._visitRepository = visitRepository;
            this._visitTransactionRepository = visitTransactionRepository;
            this._visitStateService = visitStateService;
        }

        public IReadOnlyList<Visit> GetVisits(int vpGuarantorId)
        {
            return this._visitRepository.Value.GetVisits(vpGuarantorId);
        }

        public IReadOnlyList<Visit> GetVisits(int vpGuarantorId, IList<int> visitIds)
        {
            return this._visitRepository.Value.GetVisits(vpGuarantorId, visitIds);
        }

        public IList<int> GetVisitIds(int vpGuarantorId, VisitStateEnum visitStateEnum)
        {
            return this._visitRepository.Value.GetVisitIds(vpGuarantorId, visitStateEnum);
        }

        public VisitResults GetVisits(int vpGuarantorId, VisitFilter filter, int? batch, int? batchSize)
        {
            return this._visitRepository.Value.GetVisits(vpGuarantorId, filter, batch, batchSize);
        }

        public int GetVisitCount(int vpGuarantorId)
        {
            return this._visitRepository.Value.GetVisitTotals(vpGuarantorId, new VisitFilter());
        }

        /*public int GetVisitCount(int vpGuarantorId, IEnumerable<VisitStateEnum> visitStates)
        {
            return this._visitRepository.Value.GetVisitCount(vpGuarantorId, visitStates);
        }*/

        public bool HasVisits(int vpGuarantorId, IEnumerable<VisitStateEnum> visitStates)
        {
            return this._visitRepository.Value.HasVisits(vpGuarantorId, visitStates);
        }

        public bool HasVisitsWithFilter(int vpGuarantorId, VisitFilter visitFilter)
        {
            return this._visitRepository.Value.GetVisitTotals(vpGuarantorId, visitFilter) > 0;
        }

        public int GetVisitTotals(int vpGuarantorId, VisitFilter filter)
        {
            return this._visitRepository.Value.GetVisitTotals(vpGuarantorId, filter);
        }

        public Visit GetVisit(int vpGuarantorId, int visitId)
        {
            return this._visitRepository.Value.GetVisit(vpGuarantorId, visitId);
        }

        public Visit GetVisit(int visitId)
        {
            return this._visitRepository.Value.GetById(visitId);
        }

        public Visit GetVisitBySystemSourceKey(string systemSourceKey, int billingSystemId)
        {
            return this._visitRepository.Value.GetBySourceSystemKey(systemSourceKey, billingSystemId);
        }

        public Visit GetVisitBySystemSourceKeyUnmatched(int vpGuarantorId, string sourceSystemKey, int billingSystemId)
        {
            return this._visitRepository.Value.GetVisitBySystemSourceKeyUnmatched(vpGuarantorId, sourceSystemKey, billingSystemId);
        }

        public IList<Visit> GetVisitsBySystemSourceKeyUnmatched(int vpGuarantorId, string sourceSystemKey, int billingSystemId)
        {
            return this._visitRepository.Value.GetVisitsBySystemSourceKeyUnmatched(vpGuarantorId, sourceSystemKey, billingSystemId);
        }

        public void Insert(Visit visit)
        {
            this.SaveVisit(visit);
        }

        public IList<VisitResult> GetAllVisitsResults()
        {
            return this._visitRepository.Value.GetAllVisitResults();
        }

        public bool SetVisitToIneligible(Visit visit, string ineligibleReason)
        {
            visit.VpEligible = false;
            return this._visitStateService.Value.EvaluateVisitState(visit);
        }

        public bool SetVisitToEligible(Visit visit)
        {
            visit.VpEligible = true;
            return this._visitStateService.Value.EvaluateVisitState(visit);
        }

        public bool SetVisitToMaxAge(Visit visit)
        {
            visit.IsMaxAge = true;
            return this._visitStateService.Value.EvaluateVisitState(visit);
        }

        public IList<Visit> GetAllVisitsWithIntList(IList<int> toList, bool useLazyLoading = false)
        {
            return this._visitRepository.Value.GetAllWithIntList(toList, useLazyLoading);
        }

        public void SaveVisit(Visit visit)
        {
            this._visitRepository.Value.InsertOrUpdate(visit);
        }

        public void UpdateTotalCharges(int visitId)
        {
            Visit visit = this._visitRepository.Value.GetById(visitId);
            if (visit == null)
            {
                return;
            }

            IList<VisitTransaction> transactions = this._visitTransactionRepository.Value.GetVisitTransactions(visit.VpGuarantorId, visit.VisitId);
            decimal totalCharges = transactions.Where(x => x.VpTransactionType.IsHsCharge()).Sum(x => x.VisitTransactionAmount?.TransactionAmount ?? 0);

            visit.TotalCharges = totalCharges;
            this._visitRepository.Value.InsertOrUpdate(visit);
        }

        public IReadOnlyList<Visit> GetVisits(IList<VisitStateEnum> visitStates)
        {
            return this._visitRepository.Value.GetVisits(visitStates);
        }

        /*public IReadOnlyList<Visit> GetVisits(VisitFilter visitFilter)
        {
            return this._visitRepository.Value.GetVisits(visitFilter);
        }*/

        public IReadOnlyList<Visit> GetVisitsUnmatched(int vpGuarantorId)
        {
            IReadOnlyList<Visit> unmatchedVisits = this._visitRepository.Value.GetVisitsUnmatched(vpGuarantorId);
            return unmatchedVisits;
        }

        public IList<Visit> GetAllActiveVisits(Guarantor guarantor)
        {
            return this.GetVisits(guarantor.VpGuarantorId).Where(x => x.CurrentVisitState == VisitStateEnum.Active).ToList();
        }

        public IList<Visit> GetAllActiveNonFinancedVisits(int vpGuarantorId)
        {
            return this.GetVisits(vpGuarantorId)
                .Where(x => x.CurrentVisitState == VisitStateEnum.Active)
                .Where(x => !this._visitRepository.Value.IsOnFinancePlan(x))
                .ToList();
        }

        /// <summary>
        /// This returns the list of unique facility names associated with Visits tied to the <param name="vpGuarantorId"></param> parameter.
        /// </summary>
        /// <param name="vpGuarantorId"></param>
        /// <returns></returns>
        public IList<string> GetUniqueFacilityVisitNames(int vpGuarantorId)
        {
            return this._visitRepository.Value.GetFacilitiesForGuarantor(vpGuarantorId)
                .Select(s => s.FacilityDescription)
                .Where(w => !string.IsNullOrWhiteSpace(w))
                .Distinct()
                .ToList();
        }

        /// <summary>
        /// This returns the list of unique facilities associated with Visits tied to the <param name="vpGuarantorId"></param> parameter.
        /// </summary>
        /// <param name="vpGuarantorId"></param>
        /// <returns></returns>
        public IList<Facility> GetUniqueFacilityVisits(int vpGuarantorId)
        {
            return this._visitRepository.Value.GetFacilitiesForGuarantor(vpGuarantorId)
                .Distinct()
                .OrderBy(x => x.FacilityDescription)
                .ToList();
        }
        public int? GetRicCmsRegionId(int vpGuarantorId)
        {
            int? cmsRegionId = this._visitRepository.Value.GetFacilitiesForGuarantor(vpGuarantorId)?
                .Select(s => s.RicState?.CmsRegionId)
                .FirstOrDefault();

            return cmsRegionId;
        }

        public CmsRegionEnum GetRicCmsRegionEnum(int vpGuarantorId)
        {
            int? ricCmsRegionId = this.GetRicCmsRegionId(vpGuarantorId);
            return !ricCmsRegionId.HasValue || ricCmsRegionId == default(int) ? CmsRegionEnum.VppCreditAgreement : (CmsRegionEnum)ricCmsRegionId;
        }

        public IList<VisitBalanceResult> GetVisitBalances(int vpGuarantorId, IList<int> visitIds = null)
        {
            IList<VisitBalanceResult> visitBalances = this._visitRepository.Value.GetVisitBalances(vpGuarantorId, visitIds);

            return visitBalances;
        }

        public bool DoesGuarantorHaveVisitWithInsurancePlanInList(int vpGuarantorId, List<string> insurancePlanSsks)
        {
            return this._visitRepository.Value.DoesGuarantorHaveVisitWithInsurancePlanInList(vpGuarantorId, insurancePlanSsks);
        }

        public bool IsVisitPastDue(IVisit visit)
        {
            return visit.CurrentVisitState == VisitStateEnum.Active &&
                   visit.AgingCount > this.ApplicationSettingsService.Value.PastDueAgingCount.Value;
        }

        public bool IsVisitFinalPastDue(IVisit visit)
        {
            // coupled to CommonWebMappings.MapVisitPastDue
            return visit.CurrentVisitState == VisitStateEnum.Active &&
                   visit.AgingCount == (int)AgingTierEnum.FinalPastDue;
        }

        public IList<int> GetVisitsThatWereActiveForDateRange(int vpGuarantorId, DateTime startDate, DateTime endDate)
        {
            return this._visitRepository.Value.GetVisitsThatWereActiveForDateRange(vpGuarantorId, startDate, endDate);
        }

        public IList<VisitResult> GetFinalPastDueVisitsMissingNotification()
        {
            return this._visitRepository.Value.GetFinalPastDueVisitsMissingNotification();
        }

        public void MarkFinalPastDueEmailAsSent(Visit visit)
        {
            if (visit == null)
            {
                return;
            }

            if (!visit.FinalPastDueEmailSentDate.HasValue)
            {
                visit.FinalPastDueEmailSentDate = DateTime.UtcNow;
                this._visitRepository.Value.InsertOrUpdate(visit);
            }
        }

        public bool GuarantorHasVisitsInMultipleStates(int vpGuarantorId)
        {
            return this._visitRepository.Value.GuarantorHasVisitsInMultipleStates(vpGuarantorId);
        }

        public IList<string> GetStateCodesForGuarantorVisits(int vpGuarantorId)
        {
            return this._visitRepository.Value.GetStateCodesForGuarantorVisits(vpGuarantorId);
        }
    }
}
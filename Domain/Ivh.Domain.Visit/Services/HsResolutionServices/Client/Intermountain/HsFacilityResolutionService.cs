﻿namespace Ivh.Domain.Visit.Services.HsResolutionServices.Client.Intermountain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Utilities.Extensions;
    using Entities;
    using Interfaces;

    public class HsFacilityResolutionService : DomainService, IHsFacilityResolutionService
    {
        private readonly Lazy<IDictionary<string, string>> _forward;

        public HsFacilityResolutionService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            IHsFacilityRepository hsFacilityRepository) : base(serviceCommonService)
        {
            this._forward = new Lazy<IDictionary<string, string>>(() =>
            {
                return hsFacilityRepository.GetQueryable().ToDictionary(k => k.FacilityCode, v => v.FacilityDescription, StringComparer.OrdinalIgnoreCase);
            });
        }

        public void ResolveFacility(VisitItemizationStorage visitItemizationStorage)
        {
            if (visitItemizationStorage == null)
            {
                return;
            }

            if (visitItemizationStorage.FacilityCode.IsNotNullOrEmpty() && visitItemizationStorage.FacilityDescription.IsNullOrEmpty())
            {
                visitItemizationStorage.FacilityDescription = this._forward.Value[visitItemizationStorage.FacilityCode];
            }
        }

        public void ResolveFacilities(IList<VisitItemizationStorage> visitItemizationStorages)
        {
            foreach (VisitItemizationStorage visitItemizationStorage in visitItemizationStorages)
            {
                this.ResolveFacility(visitItemizationStorage);
            }
        }
        public string ResolveFacilityDescription(string facilityCode)
        {
            return this._forward.Value[facilityCode] ?? string.Empty;
        }
    }
}

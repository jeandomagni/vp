﻿namespace Ivh.Domain.Visit.Services.HsResolutionServices
{
    using System;
    using Base.Interfaces;
    using Base.Services;
    using Interfaces;

    public class HsBillingSystemResolutionService : DomainService, IHsBillingSystemResolutionService
    {
        private readonly Lazy<IHsBillingSystemResolutionProvider> _hsBillingSystemResolutionProvider;

        public HsBillingSystemResolutionService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IHsBillingSystemResolutionProvider> hsBillingSystemResolutionProvider) : base(serviceCommonService)
        {
            this._hsBillingSystemResolutionProvider = hsBillingSystemResolutionProvider;
        }

        public int ResolveBillingSystemId(string visitSourceSystemKey)
        {
            return this._hsBillingSystemResolutionProvider.Value.ResolveBillingSystemId(visitSourceSystemKey);
        }
    }
}

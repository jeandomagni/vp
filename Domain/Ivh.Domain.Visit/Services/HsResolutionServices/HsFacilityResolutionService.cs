﻿namespace Ivh.Domain.Visit.Services.HsResolutionServices
{
    using System;
    using Base.Interfaces;
    using Base.Services;
    using Interfaces;

    public class HsFacilityResolutionService : DomainService, IHsFacilityResolutionService
    {
        public void ResolveFacility(Entities.VisitItemizationStorage visitItemizationStorage)
        {
            //pass through
        }

        public void ResolveFacilities(System.Collections.Generic.IList<Entities.VisitItemizationStorage> visitItemizationStorage)
        {
            //pass through
        }

        public string ResolveFacilityDescription(string facilityCode)
        {
            return facilityCode;
        }

        public HsFacilityResolutionService(Lazy<IDomainServiceCommonService> serviceCommonService) : base(serviceCommonService)
        {
        }
    }
}

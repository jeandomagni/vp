﻿namespace Ivh.Domain.Visit.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Application.HospitalData.Common.Models;
    using Base.Interfaces;
    using Base.Services;
    using Entities;
    using Interfaces;

    public class VisitItemizationStorageService : DomainService, IVisitItemizationStorageService
    {
        private readonly Lazy<IVisitItemizationStorageRepository> _visitItemizationStorageRepository;
        private readonly Lazy<IHsFacilityResolutionService> _hsFacilityResolutionService;

        public VisitItemizationStorageService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IVisitItemizationStorageRepository> visitItemizationStorageRepository,
            Lazy<IHsFacilityResolutionService> hsFacilityResolutionService) : base(serviceCommonService)
        {
            this._visitItemizationStorageRepository = visitItemizationStorageRepository;
            this._hsFacilityResolutionService = hsFacilityResolutionService;
        }

        public VisitItemizationStorage GetItemization(int vpGuarantorId, int visitFileStoreId, int currentVisitPayUserId)
        {
            VisitItemizationStorage visitItemizationStorage = this._visitItemizationStorageRepository.Value.GetItemization(vpGuarantorId, visitFileStoreId, currentVisitPayUserId);
            this._hsFacilityResolutionService.Value.ResolveFacility(visitItemizationStorage);
            return visitItemizationStorage;
        }

        public VisitItemizationStorage GetItemizationForVisit(int vpGuarantorId, string accountNumber, string empi, int currentVisitPayUserId)
        {
            VisitItemizationStorage visitItemizationStorage = this._visitItemizationStorageRepository.Value.GetItemizationForVisit(vpGuarantorId, accountNumber, empi, currentVisitPayUserId);
            this._hsFacilityResolutionService.Value.ResolveFacility(visitItemizationStorage);
            return visitItemizationStorage;
        }

        public VisitItemizationStorageResults GetItemizations(int vpGuarantorId, VisitItemizationStorageFilter filter, int pageNumber, int pageSize)
        {
            VisitItemizationStorageResults visitItemizationStorageResults = this._visitItemizationStorageRepository.Value.GetItemizations(vpGuarantorId, filter, pageNumber, pageSize);
            this._hsFacilityResolutionService.Value.ResolveFacilities(visitItemizationStorageResults.Itemizations);
            return visitItemizationStorageResults;
        }

        public int GetItemizationTotals(int vpGuarantorId, VisitItemizationStorageFilter filter)
        {
            return this._visitItemizationStorageRepository.Value.GetItemizationTotals(vpGuarantorId, filter).TotalRecords;
        }

        public IDictionary<string, string> GetAllFacilities(int vpGuarantorId, int currentVisitPayUserId)
        {
            IList<VisitItemizationStorage> facilities = this._visitItemizationStorageRepository.Value.GetItemizations(vpGuarantorId, currentVisitPayUserId);
            this._hsFacilityResolutionService.Value.ResolveFacilities(facilities);

            return facilities
                .GroupBy(x => x.FacilityCode)
                .Select(x => x.First())
                .Where(x => !string.IsNullOrEmpty(x.FacilityCode) || !string.IsNullOrEmpty(x.FacilityDescription))
                .ToDictionary(k => k.FacilityCode, v => v.FacilityDescription);
        }

        public IList<string> GetAllPatientNames(int vpGuarantorId, int currentVisitPayUserId)
        {
            return this._visitItemizationStorageRepository.Value.GetItemizations(vpGuarantorId, currentVisitPayUserId).Select(x => x.PatientName).Distinct().ToList();
        }

        public async Task SaveAsync(VisitItemizationStorage visitItemization)
        {
            await Task.Run(() =>
            {
                this._visitItemizationStorageRepository.Value.InsertOrUpdate(visitItemization);
            }).ConfigureAwait(true);
        }

        public async Task<VisitItemizationStorage> GetByExternalKeyAsync(Guid externalKey, int currentVisitPayUserId)
        {
            VisitItemizationStorage itemization = default(VisitItemizationStorage);
            await Task.Run(() =>
            {
                itemization = this._visitItemizationStorageRepository.Value.GetByExternalKey(externalKey, currentVisitPayUserId);
            }).ConfigureAwait(true);

            return itemization;
        }

        public VisitItemizationDetailsResultModel GetVisitItemizationDetails(int billingSystemId, string hsGuarantorSourceSystemKey, string visitSourceSystemKey)
        {
            return this._visitItemizationStorageRepository.Value.GetVisitItemizationDetails(billingSystemId,hsGuarantorSourceSystemKey, visitSourceSystemKey);
        }
    }
}
﻿namespace Ivh.Domain.Visit.Services
{
    using System;
    using System.Collections.Generic;
    using Entities;
    using Interfaces;

    public class BaseVisitService : IBaseVisitService
    {
        private readonly Lazy<IBaseVisitRepository> _baseVisitRepository;

        public BaseVisitService(Lazy<IBaseVisitRepository> baseVisitRepository)
        {
            this._baseVisitRepository = baseVisitRepository;
        }

        public IList<BaseVisit> GetBaseVisits(IList<int> hsGuarantorIds)
        {
            return this._baseVisitRepository.Value.GetPaymentSummaryVisits(hsGuarantorIds);
        }

        public BaseVisit GetVisit(int billingSystemId, string visitSourceSystemKey)
        {
            return this._baseVisitRepository.Value.GetVisit(visitSourceSystemKey, billingSystemId);
        }
    }
}
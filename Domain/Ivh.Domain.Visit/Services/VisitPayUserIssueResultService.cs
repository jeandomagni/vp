﻿namespace Ivh.Domain.Visit.Services
{
    using System;
    using System.Collections.Generic;
    using Base.Interfaces;
    using Base.Services;
    using Guarantor.Entities;
    using Interfaces;

    public class VisitPayUserIssueResultService : DomainService, IVisitPayUserIssueResultService
    {
        private readonly IVisitPayUserIssueResultRepository _visitPayUserIssueResultRepository;

        public VisitPayUserIssueResultService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            IVisitPayUserIssueResultRepository visitPayUserIssueResultRepository) : base(serviceCommonService)
        {
            this._visitPayUserIssueResultRepository = visitPayUserIssueResultRepository;
        }

        public IReadOnlyList<IssueResult> AdjustAllGuarantorDates(int guarantorId, string datePart, int adjustment)
        {
            return this._visitPayUserIssueResultRepository.AdjustAllGuarantorDates(guarantorId, datePart, adjustment);
        }

        public IReadOnlyList<IssueResult> ClearGuarantorData(int guarantorId)
        {
            return this._visitPayUserIssueResultRepository.ClearGuarantorData(guarantorId);
        }
    }
}
﻿namespace Ivh.Domain.Visit.Services
{
    using System;
    using Base.Interfaces;
    using Base.Services;
    using Interfaces;

    public class VisitAgingHistoryService : DomainService, IVisitAgingHistoryService
    {
        private readonly IVisitAgingHistoryRepository _visitAgingHistoryRepository;
        public VisitAgingHistoryService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            IVisitAgingHistoryRepository visitAgingHistoryRepository) : base(serviceCommonService)
        {
            this._visitAgingHistoryRepository = visitAgingHistoryRepository;
        }

        public VisitAgingHistoryResults GetAllVisitAgingHistory(int vpGuarantorId, VisitAgingHistoryFilter filter, int batch, int batchSize)
        {
            return this._visitAgingHistoryRepository.GetAllVisitAgingHistory(vpGuarantorId, filter, batch, batchSize);
        }
    }
}
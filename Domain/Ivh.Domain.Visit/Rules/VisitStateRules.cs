﻿namespace Ivh.Domain.Visit.Rules
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using Ivh.Common.State;

    public class VisitStateRules : Rules<Visit, VisitStateEnum>
    {
        //https://ivincehealth.sharepoint.com/Products/vpp/VP3%20Documentation/VP3%20States.xlsx

        //StateNotSet
        public const string V3_101 = "V3-101";
        public const string V3_102 = "V3-102";
        public const string V3_103 = "V3-103";
        public const string V3_104 = "V3-104";

        //Unmatched
        public const string V3_105 = "V3-105";
        public const string V3_106 = "V3-106";

        //Recalled
        public const string V3_107 = "V3-107";
        public const string V3_108 = "V3-108";

        //OnHold
        public const string V3_109 = "V3-109";
        public const string V3_110 = "V3-110";

        //NoBalance
        public const string V3_111 = "V3-111";
        public const string V3_112 = "V3-112";

        //PostiveBalanceAndNoFlags
        public const string V3_113 = "V3-113";
        public const string V3_114 = "V3-114";


        public VisitStateRules()
        {
            //StateNotSet
            this.AddRule(V3_101)
                .When(VisitStateEnum.NotSet)
                .And((ec) => ec.Entity.HasPositiveBalance)
                .And((ec) => !ec.Entity.IsUnmatched)
                .And((ec) => ec.Entity.VpEligible)
                .And((ec) => !ec.Entity.BillingHold)
                .Then(VisitStateEnum.Active);
                //.Then(VisitStateEnum.Active, (s, ec) =>
                //{
                //    s.AddEvent<Event_Test_VisitAdded>()
                //});


            this.AddRule(V3_102)
                .When(VisitStateEnum.NotSet)
                .And((ec) => !ec.Entity.HasPositiveBalance)
                .Then(VisitStateEnum.Inactive);

            this.AddRule(V3_103)
                .When(VisitStateEnum.NotSet)
                .And((ec) => !ec.Entity.VpEligible)
                .Then(VisitStateEnum.Inactive);

            this.AddRule(V3_104)
                .When(VisitStateEnum.NotSet)
                .And((ec) => ec.Entity.BillingHold)
                .Then(VisitStateEnum.Inactive);

            //Unmatched
            this.AddRule(V3_105)
                .When(VisitStateEnum.Inactive)
                .And((ec) => ec.Entity.IsUnmatched)
                .Then(VisitStateEnum.Inactive);

            this.AddRule(V3_106)
                .When(VisitStateEnum.Active)
                .And((ec) => ec.Entity.IsUnmatched)
                .Then(VisitStateEnum.Inactive);

            //Recalled
            this.AddRule(V3_107)
                .When(VisitStateEnum.Inactive)
                .And((ec) => !ec.Entity.VpEligible)
                .Then(VisitStateEnum.Inactive);

            this.AddRule(V3_108)
                .When(VisitStateEnum.Active)
                .And((ec) => !ec.Entity.VpEligible)
                .Then(VisitStateEnum.Inactive);

            //OnHold
            this.AddRule(V3_109)
                .When(VisitStateEnum.Inactive)
                .And((ec) => ec.Entity.BillingHold)
                .Then(VisitStateEnum.Inactive);

            this.AddRule(V3_110)
                .When(VisitStateEnum.Active)
                .And((ec) => ec.Entity.BillingHold)
                .Then(VisitStateEnum.Inactive);

            //NoBalance
            this.AddRule(V3_111)
                .When(VisitStateEnum.Inactive)
                .And((ec) => !ec.Entity.HasPositiveBalance)
                .Then(VisitStateEnum.Inactive);

            this.AddRule(V3_112)
                .When(VisitStateEnum.Active)
                .And((ec) => !ec.Entity.HasPositiveBalance)
                .Then(VisitStateEnum.Inactive);

            //PostiveBalanceAndNoFlags
            this.AddRule(V3_113)
                .When(VisitStateEnum.Inactive)
                .And((ec) => ec.Entity.HasPositiveBalance)
                .And((ec) => !ec.Entity.IsUnmatched)
                .And((ec) => ec.Entity.VpEligible)
                .And((ec) => !ec.Entity.BillingHold)
                .Then(VisitStateEnum.Active);

            this.AddRule(V3_114)
                .When(VisitStateEnum.Active)
                .And((ec) => ec.Entity.HasPositiveBalance)
                .And((ec) => !ec.Entity.IsUnmatched)
                .And((ec) => ec.Entity.VpEligible)
                .And((ec) => !ec.Entity.BillingHold)
                .Then(VisitStateEnum.Active);
        }
    }
}

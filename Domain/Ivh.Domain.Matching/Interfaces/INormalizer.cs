﻿namespace Ivh.Domain.Matching.Interfaces
{
    public interface INormalizer
    {
        string Normalize(string input);
    }
}
﻿namespace Ivh.Domain.Matching.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IMatchVpGuarantorMatchInfoRepository : IRepository<MatchVpGuarantorMatchInfo>
    {
        bool ActiveVpGuarantorRegistryMatchExists(int registryId);
        MatchVpGuarantorMatchInfo GetVpGuarantorRegistryMatch(int hsGuarantorId);
        string GetGuarantorMatchSsn(int vpGuarantorId);
    }
}

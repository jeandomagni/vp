﻿namespace Ivh.Domain.Matching.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IMatchGuarantorRepository : IRepository<Guarantor>
    {
        IEnumerable<IEnumerable<int>> GetMatchesToQueue(DateTime processDate, int maxHsGuarantorId, bool patientDataRequired, bool updatedHsGuarantors, int chunkSize);
        IList<MatchData> GetMatchData(IList<int> hsGuarantorIds, bool includePatient);
        IList<MatchData> GetMatchData(string sourceSystemKey, int billingSystemId, bool includePatient);
    }
}

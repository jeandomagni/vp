﻿namespace Ivh.Domain.Matching.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using Application.Matching.Common.Dtos;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IMatchRegistryRepository : IRepository<MatchRegistry>
    {
        DateTime? GetLastUpdateDate(IList<PatternSetEnum> enabledPatternSetEnums);

        IList<MatchRegistry> GetMatchRegistriesForSetAndPattern(string matchString, MatchOptionEnum patternEnum, PatternSetEnum setEnum, PatternUseEnum patternUseEnum);
        IList<MatchRegistry> GetMatchRegistries(int hsGuarantorId);
        IList<MatchRegistry> GetMatchRegistries(IList<int> hsGuarantorIds);
        int? GetMaxHsGuarantorId(IList<PatternSetEnum> enabledPatternSetEnums);
        int? UpdateMatchRegistryMatchString(MatchRegistryDto dto);
    }
}

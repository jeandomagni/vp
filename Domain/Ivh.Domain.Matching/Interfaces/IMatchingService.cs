﻿

namespace Ivh.Domain.Matching.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.Matching.Dtos;
    using Entities;
    using Ivh.Common.Base.Interfaces;
    using Match;

    public interface IMatchingService : IDomainService
    {
        /// <summary>
        /// find the last time match registries were updated
        /// </summary>
        DateTime GetLastUpdateDate();

        IEnumerable<IEnumerable<int>> GetMatchesToQueue(DateTime processDate, bool updatesOnly);

        /// <summary>
        /// </summary>
        /// <param name="processDate"></param>
        /// <param name="matchDatas"></param>
        /// <param name="sendRetryMessage">create and send a new message for any failed matchdatas</param>
        /// <param name="isUpdate">true if all changes are updates of existing guarantors or patients.</param>
        void ProcessMatchUpdates(DateTime processDate, IList<MatchDataGroup> matchDatas, bool sendRetryMessage, bool isUpdate);

        IList<MatchDataGroup> GetMatchDataGroups(IList<int> hsGuarantorIds, ApplicationEnum? applicationEnum);

        IList<string> GetRequiredMatchFields(ApplicationEnum? applicationEnum, PatternUseEnum? matchType = null, FieldSourceEnum? fieldSource = null);

        IList<GuarantorMatchResult> AddInitialMatches(int vpGuarantorId, MatchDataGroup matchData, ApplicationEnum applicationEnum);

        void UpdateMatchStatus(int vpGuarantorId, MatchDataGroup matchData, HsGuarantorMatchStatusEnum hsGuarantorMatchStatus);
        IEnumerable<int> GetMatchedVpguarantorIds();
        void FindNewHsGuarantorMatches(IList<int> matchedVpGuarantorIds, bool sendRetryMessage = false);
        IList<GuarantorMatchResult> UpdateMatchInfo(int vpGuarantorId, MatchDataGroup matchData, ApplicationEnum applicationEnum);
        string GetGuarantorMatchSsn(int vpGuarantorId);
    }
}

﻿namespace Ivh.Domain.Matching.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;

    public interface IMatchReconciliationService : IDomainService
    {
        IList<int> GetMatchReconciliationVpGuarantorIds();
        void MatchReconciliation(IList<int> vpGuarantorIds);
    }
}

﻿namespace Ivh.Domain.Matching.Interfaces
{
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.Matching.Dtos;
    using Entities;

    /// <summary>
    /// Service to provide initial match results. For licensing reasons this should not make requests to the CDI database.
    /// </summary>
    public interface IInitialMatchService : IDomainService
    {
        GuarantorMatchResult GetInitialMatchResult(MatchDataGroup initialMatchData, ApplicationEnum applicationEnum);
    }
}

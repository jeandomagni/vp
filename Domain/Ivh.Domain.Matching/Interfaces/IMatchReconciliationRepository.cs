﻿namespace Ivh.Domain.Matching.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IMatchReconciliationRepository : IRepository<MatchReconciliationMatchInfo>
    {
        IList<int> GetAllMatchReconciliationVpGuarantorIds();
        IList<MatchReconciliationMatchInfo> GetMatchReconciliationMatchInfo(IList<int> vpGuarantorIds);
    }
}

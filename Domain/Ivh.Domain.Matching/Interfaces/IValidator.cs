﻿namespace Ivh.Domain.Matching.Interfaces
{
    public interface IInputValidator<T>
    {
        bool IsValid(T input);
    }

    public interface IOutputValidator
    {
        bool IsValid(string output);
    }
}

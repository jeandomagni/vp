﻿namespace Ivh.Domain.Matching.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Application.Matching.Common.Dtos;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;
    using Match;
    using Newtonsoft.Json;
    using Sets;
    using Validators;

    public abstract class BaseMatchingService : DomainService
    {
        protected readonly Lazy<IMatchRegistryRepository> MatchingRepository;
        protected readonly Lazy<IMatchVpGuarantorMatchInfoRepository> VpGuarantorMatchInfoRepository;
        protected readonly Lazy<DobValidator> DobValidator = new Lazy<DobValidator>(() => new DobValidator());
        protected readonly Lazy<SsnValidator> SsnValidator = new Lazy<SsnValidator>(() => new SsnValidator());

        protected BaseMatchingService(
            Lazy<IMatchRegistryRepository> matchingRepository,
            Lazy<IMatchVpGuarantorMatchInfoRepository> vpGuarantorMatchInfoRepository,
            Lazy<IDomainServiceCommonService> serviceCommonService
            ) : base(serviceCommonService)
        {
            this.MatchingRepository = matchingRepository;
            this.VpGuarantorMatchInfoRepository = vpGuarantorMatchInfoRepository;
        }

        protected void SetMatchSsn(MatchVpGuarantorMatchInfo matchInfo, string ssn)
        {
            if (this.SsnValidator.Value.IsValidNormalized(ssn))
            {
                matchInfo.SetMatchSsn(ssn);
            }
        }

        protected void SetMatchDob(MatchVpGuarantorMatchInfo matchInfo, DateTime? dob)
        {
            if (this.DobValidator.Value.IsValid(dob))
            {
                matchInfo.SetMatchGuarantorDob(dob.Value);
            }
        }

        /// <summary>
        /// Guarantor Date of Birth: Range is reflective of ages between 18 years old and 110 years old at time of match
        /// </summary>
        /// <param name="matchData"></param>
        /// <param name="patternUseEnum"></param>
        /// <param name="applicationEnum"></param>
        /// <returns></returns>
        protected bool CheckGuarantorDob(MatchDataGroup matchData, PatternUseEnum patternUseEnum, ApplicationEnum applicationEnum)
        {
            bool dobNotRequired = this.GetClientConfiguredMatchingSets(applicationEnum)
                .All(x => 
                    !x.IsFieldRequired(patternUseEnum, FieldSourceEnum.Guarantor, FieldPropertyEnum.DOB));

            bool guarantorHasNoDob = !matchData.Guarantor.DOB.HasValue;

            bool dobIsValid = this.DobValidator.Value.IsValid(matchData.Guarantor.DOB);

            return dobNotRequired ||
                   guarantorHasNoDob ||
                   dobIsValid;
        }

        protected IEnumerable<T> GetMatchesForMatchData<T>(MatchDataGroup matchData, Func<GuarantorMatchResultEnum, MatchRegistry, T> resultSelector, ApplicationEnum applicationEnum, PatternUseEnum? matchType = null)
        {
            IEnumerable<MatchRegistryDto> matchRegistriesForType = this.GenerateMatchRegistries(DateTime.UtcNow, matchData, applicationEnum, matchType);
            IEnumerable<T> matchResults = this.GetMatches<T>(matchRegistriesForType, resultSelector);

            return matchResults;
        }

        protected IEnumerable<T> GetMatches<T>(IEnumerable<MatchRegistryDto> matchRegistryDtos, Func<GuarantorMatchResultEnum, MatchRegistry, T> resultSelector)
        {
            //TODO: VP-3437 - client setting?
            const decimal minimumConfidence = 0.0m;

            // generate new matches for the given data
            IOrderedEnumerable<MatchRegistryDto> matchRegistriesForType = matchRegistryDtos.OrderByDescending(x => x.PatternConfidence);

            // find matching registry records
            IEnumerable<T> matchResults = matchRegistriesForType.SelectMany(matchRegistryDto =>
            {
                IEnumerable<MatchRegistry> matchRegistriesForThisDto = this.MatchingRepository.Value.GetMatchRegistriesForSetAndPattern(
                    matchRegistryDto.MatchString,
                    matchRegistryDto.MatchOptionId,
                    matchRegistryDto.PatternSetId,
                    matchRegistryDto.PatternUseId);

                IEnumerable<T> matchResultsForDto = matchRegistriesForThisDto.Select(matchRegistry =>
                {
                    GuarantorMatchResultEnum matchResultEnum = matchRegistryDto.PatternConfidence >= minimumConfidence ? GuarantorMatchResultEnum.Matched : GuarantorMatchResultEnum.Ambiguous;
                    matchRegistry.PatternConfidence = matchRegistryDto.PatternConfidence;

                    MatchVpGuarantorMatchInfo vpMatch = this.VpGuarantorMatchInfoRepository.Value.GetVpGuarantorRegistryMatch(matchRegistry.HsGuarantorId);

                    if (vpMatch != null && vpMatch.Matches.Any(x => x.HsGuarantorMatchStatusId == HsGuarantorMatchStatusEnum.UnmatchedHard))
                    {
                        matchResultEnum = GuarantorMatchResultEnum.NoMatchFound;
                    }
                    else if (vpMatch != null && vpMatch.Matches.Any(x => x.HsGuarantorMatchStatusId == HsGuarantorMatchStatusEnum.Matched))
                    {
                        matchResultEnum = GuarantorMatchResultEnum.AlreadyRegistered;
                    }

                    T result = resultSelector(matchResultEnum, matchRegistry);
                    return result;
                });

                return matchResultsForDto;
            });

            return matchResults;
        }


        protected IList<MatchRegistryDto> GenerateMatchRegistries(DateTime processDate, MatchDataGroup matchData, ApplicationEnum? applicationEnum, PatternUseEnum? patternUse = null)
        {
            //
            IList<Set> sets = this.GetClientConfiguredMatchingSets(applicationEnum);

            //
            IList<MatchRegistryDto> matchResults = new List<MatchRegistryDto>();
            IList<MatchData> matchDatas = matchData.ToMatchDatas().ToList();

            // generate match registries for each match set
            foreach (Set set in sets)
            {
                IEnumerable<MatchRegistryDto> matchResultsForSet = this.GetMatchResults(matchDatas, set, patternUse).Distinct();
                matchResults.AddRange(matchResultsForSet);
            }

            // save the date
            foreach (MatchRegistryDto matchRegistry in matchResults)
            {
                matchRegistry.DataChangeDate = matchData.Guarantor.DataChangeDate ?? matchData.Visits.Select(x => x.Patient).Max(x => (DateTime?)x?.DataChangeDate) ?? processDate;
            }

            return matchResults;
        }

        /// <summary>
        /// Generates match registry dtos and logs warnings for any patterns not generated due to invalid fields
        /// </summary>
        /// <param name="matchDatas"></param>
        /// <param name="set"></param>
        /// <param name="patternUseEnum"></param>
        /// <returns></returns>
        protected IEnumerable<MatchRegistryDto> GetMatchResults(IList<MatchData> matchDatas, Set set, PatternUseEnum? patternUseEnum)
        {
            IEnumerable<Pattern> patterns = patternUseEnum.HasValue
                ? set.Patterns.Where(x => x.PatternUse == patternUseEnum.Value)
                : set.Patterns;

            foreach (Pattern pattern in patterns)
            {
                foreach (MatchData matchData in matchDatas)
                {
                    MatchRegistryDto result = pattern.GetMatchResult(matchData);

                    if (string.IsNullOrEmpty(result.MatchString))
                    {
                        continue;
                    }

                    result.PatternSetId = set.PatternSetEnum;
                    yield return result;
                }
            }
        }

        protected decimal GetPatternConfidenceFromClientConfiguredMatchingSets(PatternSetEnum set, ApplicationEnum applicationEnum, MatchOptionEnum matchPattern)
        {
            decimal confidence = this.GetClientConfiguredMatchingSets(applicationEnum)
                .FirstOrDefault(x => x.PatternSetEnum == set)?
                .Patterns
                .FirstOrDefault(x => x.MatchOptionEnum == matchPattern)?
                .Confidence ?? default(decimal);

            return confidence;
        }

        public IList<string> GetRequiredMatchFields(ApplicationEnum? applicationEnum, PatternUseEnum? matchType = null, FieldSourceEnum? fieldSource = null)
        {
            IList<string> requiredFields = this.GetClientConfiguredMatchingSets(applicationEnum).SelectMany(x => x.GetRequiredFields(matchType, fieldSource)).ToList();
            return requiredFields;
        }

        private IDictionary<ApplicationEnum, List<Set>> _matchingConfiguration;
        
        /// <summary>
        /// Gets a list of matching sets configured for the given application
        /// </summary>
        /// <param name="applicationEnum">enum of the requesting application, or null for all types</param>
        /// <returns></returns>
        protected IList<Set> GetClientConfiguredMatchingSets(ApplicationEnum? applicationEnum = null)
        {
            if (this._matchingConfiguration != null)
            {
                return GetSetsForApplication(applicationEnum);
            }

            // get client config json
            string matchingConfigurationJson = this.Client.Value.MatchConfiguration;
            IList<Set> matchConfig = JsonConvert.DeserializeObject<List<Set>>(matchingConfigurationJson);

            // client config doesnt include entire object graph. build up the rest here.
            IList<Set> sets = matchConfig.Select(this.BuildSet).ToList();

            // save this for later
            this._matchingConfiguration = Enum.GetValues(typeof(ApplicationEnum)).Cast<ApplicationEnum>()
                .ToDictionary(
                    appEnum => appEnum,
                    appEnum => sets.Where(set => set.Applications.Contains(appEnum)).ToList());

            return GetSetsForApplication(applicationEnum);

            //
            IList<Set> GetSetsForApplication(ApplicationEnum? app)
            {
                return app.HasValue
                    ? this._matchingConfiguration[app.Value]
                    : this._matchingConfiguration.Values.SelectMany(x => x).ToList();
            }
        }

        /// <summary>
        /// build new Set from the source Set
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        private Set BuildSet(Set source)
        {
            switch (source.PatternSetEnum)
            {
                case PatternSetEnum.S001:
                    return new GuarantorMatchingSet001(source.Configuration, source.Applications);
                case PatternSetEnum.S002:                                  
                    return new GuarantorMatchingSet002(source.Configuration, source.Applications);
                case PatternSetEnum.S003:                                  
                    return new GuarantorMatchingSet003(source.Configuration, source.Applications);
                case PatternSetEnum.S004:                                  
                    return new GuarantorMatchingSet004(source.Configuration, source.Applications);
                case PatternSetEnum.S005:                                  
                    return new GuarantorMatchingSet005(source.Configuration, source.Applications);
                default:
                    throw new ArgumentException($"{source.PatternSetEnum.ToString()} is not valid", nameof(source.PatternSetEnum));
            }
        }
    }
}

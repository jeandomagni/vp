﻿namespace Ivh.Domain.Matching.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Application.Matching.Common.Dtos;
    using Base.Interfaces;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.Matching.Dtos;
    using Entities;
    using Interfaces;

    /// <summary>
    /// Service to provide initial match results. For licensing reasons this should not make requests to the CDI database.
    /// </summary>
    public class InitialMatchService : BaseMatchingService, IInitialMatchService
    {
        public InitialMatchService(
            Lazy<IMatchRegistryRepository> matchingRepository,
            Lazy<IMatchVpGuarantorMatchInfoRepository> vpGuarantorMatchInfoRepository,
            Lazy<IDomainServiceCommonService> serviceCommonService
        ) : base(
            matchingRepository,
            vpGuarantorMatchInfoRepository,
            serviceCommonService
            )
        {
        }

        public GuarantorMatchResult GetInitialMatchResult(MatchDataGroup initialMatchData, ApplicationEnum applicationEnum)
        {
            if (!this.CheckGuarantorDob(initialMatchData, PatternUseEnum.Initial, applicationEnum))
            {
                return GuarantorMatchResult.NoMatchFound;
            }

            IList<(GuarantorMatchResult matchResult, PatternSetEnum patternUsed)> initialMatches = this.GetInitialMatchResultsForMatchData(initialMatchData, applicationEnum).ToList();

            bool AlreadyRegistered(GuarantorMatchResult matchResult) => matchResult.GuarantorMatchResultEnum == GuarantorMatchResultEnum.AlreadyRegistered;
            if (initialMatches.Any(x => AlreadyRegistered(x.matchResult)))
            {
                return initialMatches.First(x => AlreadyRegistered(x.matchResult)).matchResult;
            }

            // check for ambiguous matches
            foreach (IGrouping<PatternSetEnum, GuarantorMatchResult> guarantorMatchResults in initialMatches.GroupBy(x => x.patternUsed, x => x.matchResult))
            {
                // if multiple hsguarantors matched with an initial match pattern, validate that they also match by ongoing
                if (guarantorMatchResults.Select(x => x.MatchedHsGuarantorId).Distinct().Count() > 1)
                {
                    foreach (GuarantorMatchResult matchResult in guarantorMatchResults)
                    {
                        // 
                        IList<GuarantorMatchResult> ongoingMatches = this.GetOngoingMatchResultsForInitialMatch(matchResult).ToList();
                        
                        // compare guarantors matched by initial and ongoing patterns
                        IEnumerable<int> missingMatches = guarantorMatchResults.Select(x => x.MatchedHsGuarantorId)
                            .Except(ongoingMatches.Select(x => x.MatchedHsGuarantorId));

                        if (missingMatches.IsNullOrEmpty())
                        {
                            // ongoing will find a match for all ambiguous initial matches, so return positive match
                            return initialMatches
                                       .Select(x => x.matchResult)
                                       .FirstOrDefault(x => x.GuarantorMatchResultEnum == GuarantorMatchResultEnum.Matched) ?? GuarantorMatchResult.NoMatchFound;
                        }
                    }

                    return GuarantorMatchResult.Ambiguous;
                }
            }

            return initialMatches
                       .Select(x => x.matchResult)
                       .FirstOrDefault(x => x.GuarantorMatchResultEnum == GuarantorMatchResultEnum.Matched) ?? GuarantorMatchResult.NoMatchFound;
        }

        private IEnumerable<GuarantorMatchResult> GetOngoingMatchResultsForInitialMatch(GuarantorMatchResult initialMatchResult)
        {
            // for this guarantor, find ongoing matches
            IList<MatchRegistry> matchRegistries = this.MatchingRepository.Value
                .GetMatchRegistries(initialMatchResult.MatchedHsGuarantorId)
                .Where(x => x.PatternUseId == PatternUseEnum.Ongoing)
                .ToList();
            IEnumerable<MatchRegistryDto> matchRegistryDtos = AutoMapper.Mapper.Map<IList<MatchRegistryDto>>(matchRegistries);

            return this.GetMatches(matchRegistryDtos, (matchResult, matchRegistry) => new GuarantorMatchResult
            {
                GuarantorMatchResultEnum = matchResult,
                MatchedHsGuarantorSourceSystemKey = matchRegistry.SourceSystemKey,
                MatchedHsGuarantorBillingSystemId = matchRegistry.BillingSystemId,
                MatchedHsGuarantorId = matchRegistry.HsGuarantorId,
                MatchType = matchRegistry.PatternUseId
            });
        }

        private IEnumerable<(GuarantorMatchResult, PatternSetEnum)> GetInitialMatchResultsForMatchData(MatchDataGroup matchData, ApplicationEnum applicationEnum)
        {
            (GuarantorMatchResult, PatternSetEnum) SelectGuarantorMatchResult(GuarantorMatchResultEnum matchResult, MatchRegistry matchRegistry) => 
                (new GuarantorMatchResult
                {
                    GuarantorMatchResultEnum = matchResult,
                    MatchedHsGuarantorSourceSystemKey = matchRegistry.SourceSystemKey,
                    MatchedHsGuarantorBillingSystemId = matchRegistry.BillingSystemId,
                    MatchedHsGuarantorId = matchRegistry.HsGuarantorId,
                    MatchType = PatternUseEnum.Initial
                }, matchRegistry.PatternSetId);

            // find match registry records that match the given data
            IEnumerable<(GuarantorMatchResult, PatternSetEnum)> matchResults = this.GetMatchesForMatchData(matchData, SelectGuarantorMatchResult, applicationEnum, PatternUseEnum.Initial);

            return matchResults;
        }

    }
}

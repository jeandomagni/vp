﻿namespace Ivh.Domain.Matching.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Application.Matching.Common.Dtos;
    using AutoMapper;
    using Base.Interfaces;
    using Common.Base.Utilities.Extensions;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Messages.Matching;
    using Common.VisitPay.Messages.Matching.Dtos;
    using Entities;
    using Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using Logging.Interfaces;
    using Match;
    using NHibernate;
    using Validators;

    public class MatchingService : BaseMatchingService, IMatchingService
    {
        private readonly Lazy<IMatchGuarantorRepository> _guarantorRepository;
        private readonly Lazy<IMetricsProvider> _metricsProvider;
        private readonly Lazy<SsnValidator> _ssnValidator = new Lazy<SsnValidator>(() => new SsnValidator());
        private readonly IStatelessSession _statelessSession;
        private readonly ISession _session;

        public MatchingService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IMatchRegistryRepository> matchingRepository,
            Lazy<IMatchGuarantorRepository> guarantorRepository,
            Lazy<IMatchVpGuarantorMatchInfoRepository> vpGuarantorMatchInfoRepository,
            Lazy<IMetricsProvider> metricsProvider,
            IStatelessSessionContext<VisitPay> statelessSessionContext,
            ISessionContext<VisitPay> sessionContext
            ) : base(
            matchingRepository,
            vpGuarantorMatchInfoRepository,
            serviceCommonService
            )
        {
            this._guarantorRepository = guarantorRepository;
            this._metricsProvider = metricsProvider;
            this._statelessSession = statelessSessionContext.Session;
            this._session = sessionContext.Session;
        }

        public DateTime GetLastUpdateDate()
        {
            IList<PatternSetEnum> enabledPatternSetEnums = this.GetClientConfiguredMatchingSets().Select(x => x.PatternSetEnum).ToList();

            return this.MatchingRepository.Value.GetLastUpdateDate(enabledPatternSetEnums) ?? DateTime.MinValue;
        }

        public IEnumerable<IEnumerable<int>> GetMatchesToQueue(DateTime processDate, bool updatesOnly)
        {
            IList<PatternSetEnum> enabledPatternSetEnums = this.GetClientConfiguredMatchingSets().Select(x => x.PatternSetEnum).ToList();

            int maxHsGuarantorId = this.MatchingRepository.Value.GetMaxHsGuarantorId(enabledPatternSetEnums) ?? int.MinValue;

            return this._guarantorRepository.Value.GetMatchesToQueue(processDate, maxHsGuarantorId, this.IsPatientDataRequired(null), updatesOnly, 100);
        }

        public IList<MatchDataGroup> GetMatchDataGroups(IList<int> hsGuarantorIds, ApplicationEnum? applicationEnum)
        {
            IList<MatchData> data = this._guarantorRepository.Value.GetMatchData(hsGuarantorIds, this.IsPatientDataRequired(applicationEnum));

            //filter distinct patients
            IList<MatchDataGroup> matchDataGroups = this.MatchDataToMatchDataGroup(data);

            return matchDataGroups;
        }

        private MatchDataGroup GetMatchDataGroup(string sourceSystemKey, int billingSystemId, ApplicationEnum applicationEnum)
        {
            IList<MatchData> data = this._guarantorRepository.Value.GetMatchData(sourceSystemKey, billingSystemId, this.IsPatientDataRequired(applicationEnum));

            MatchDataGroup matchDataGroup = this.MatchDataToMatchDataGroup(data).SingleOrDefault();

            return matchDataGroup;
        }

        private IList<MatchDataGroup> MatchDataToMatchDataGroup(IEnumerable<MatchData> data)
        {
            return data.GroupBy(x => x.Guarantor, x => x)
                .Select(x => new MatchDataGroup
                {
                    Guarantor = x.Key,
                    //Patients = x.Where(patient => patient != null).Distinct().ToList()
                    Visits = x
                        .Where(md => md.Patient != null)
                        .GroupBy(md => new { md.Patient })
                        .SelectMany(md => md.Select(vd => new MatchVisitData { Patient = vd.Patient }))
                        .ToList()
                })
                .ToList();
        }

        public void UpdateMatchStatus(int vpGuarantorId, MatchDataGroup matchData, HsGuarantorMatchStatusEnum hsGuarantorMatchStatus)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                // find the existing match
                MatchVpGuarantorMatchInfo matchInfo = this.VpGuarantorMatchInfoRepository.Value.GetById(vpGuarantorId);
                IList<MatchRegistryVpGuarantorMatch> matchesToUpdate = matchInfo?.Matches
                    .Where(x =>
                        x.Registry.SourceSystemKey == matchData.Guarantor.SourceSystemKey
                        && x.Registry.BillingSystemId == matchData.Guarantor.HsBillingSystemId)
                    .ToList();
                
                // 
                if (!matchesToUpdate?.Any() ?? true)
                {
                    return;
                }

                // set the match status
                foreach (MatchRegistryVpGuarantorMatch matchToUpdate in matchesToUpdate)
                {
                    matchToUpdate.HsGuarantorMatchStatusId = hsGuarantorMatchStatus;
                    this.VpGuarantorMatchInfoRepository.Value.InsertOrUpdate(matchInfo);
                }

                //  
                unitOfWork.Commit();
            }

        }

        public IEnumerable<int> GetMatchedVpguarantorIds()
        {
            return this.VpGuarantorMatchInfoRepository.Value.GetQueryable().Select(x => x.VpGuarantorId).ToList();
        }

        private IList<MatchVpGuarantorMatchInfo> GetMatchedGuarantorMatchInfos(IEnumerable<int> matchedVpGuarantorIds)
        {
            return this.VpGuarantorMatchInfoRepository.Value.GetQueryable().Where(x => matchedVpGuarantorIds.Contains(x.VpGuarantorId)).ToList();
        }

        public void FindNewHsGuarantorMatches(IList<int> matchedVpGuarantorIds, bool sendRetryMessage = false)
        {
            // TODO: ongoing matching for each configured app (VP + Scoring)
            IList<MatchVpGuarantorMatchInfo> matchInfos = this.GetMatchedGuarantorMatchInfos(matchedVpGuarantorIds);

            this.FindNewMatches(matchInfos, ApplicationEnum.VisitPay, sendRetryMessage);
        }

        private IEnumerable<GuarantorMatchResult> FindNewMatches(IList<MatchVpGuarantorMatchInfo> matchInfos, ApplicationEnum applicationEnum, bool sendRetryMessage, Action<MatchDataGroup> matchDataTransform = null)
        {
            IList<GuarantorMatchResult> matchResults = new List<GuarantorMatchResult>();

            foreach (MatchVpGuarantorMatchInfo matchedVpGuarantor in matchInfos)
            {
                using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
                {
                    IList<GuarantorMatchResult> matchResultsForVpGuarantor = new List<GuarantorMatchResult>();

                    try
                    {
                        foreach (MatchRegistryVpGuarantorMatch matchRegistryVpGuarantorMatch in matchedVpGuarantor.UniqueMatchedHsGuarantors)
                        {
                            // get match data for this hs guarantor
                            MatchDataGroup matchData = this.GetMatchDataGroup(matchRegistryVpGuarantorMatch.Registry.SourceSystemKey, matchRegistryVpGuarantorMatch.Registry.BillingSystemId, applicationEnum);

                            // transform match data (probably just for updating personal information)
                            matchDataTransform?.Invoke(matchData);

                            // find matching registries
                            IList<MatchRegistry> newOngoingMatchRegistries = this.GetOngoingMatchRegistriesForMatchData(matchData, matchedVpGuarantor, applicationEnum).ToList();

                            // set the match info if it hasn't been set
                            if (matchedVpGuarantor.MatchSsn == null || !matchedVpGuarantor.MatchGuarantorDob.HasValue)
                            {
                                this.CheckMatchInfo(matchedVpGuarantor, matchData, applicationEnum);
                            }

                            // add to new matches collection if the hsguarantor wasnt already matched
                            IEnumerable<GuarantorMatchResult> guarantorMatchResults = newOngoingMatchRegistries
                                .Where(x => this.TryAddMatchToMatchInfo(matchedVpGuarantor, x, applicationEnum))
                                .Select(x => new GuarantorMatchResult
                                {
                                    MatchedHsGuarantorBillingSystemId = x.BillingSystemId,
                                    MatchedHsGuarantorSourceSystemKey = x.SourceSystemKey,
                                    MatchType = x.PatternUseId,
                                    GuarantorMatchResultEnum = GuarantorMatchResultEnum.Matched,
                                    VpGuarantorId = matchedVpGuarantor.VpGuarantorId,
                                    MatchOption = x.MatchOptionId
                                })
                                .Distinct();

                            matchResultsForVpGuarantor.AddRange(guarantorMatchResults);

                            //TODO: what if it's an updated match that moved from low to high confidence?
                        }

                        // 
                        unitOfWork.Commit();

                        // add to results collection
                        matchResults.AddRange(matchResultsForVpGuarantor);
                    }
                    catch (Exception e)
                    {
                        if (sendRetryMessage)
                        {
                            matchResults = matchResults.Except(matchResultsForVpGuarantor).ToList();

                            this.LoggingService.Value.Fatal(() => $"{nameof(MatchingService)}::{nameof(this.FindNewHsGuarantorMatches)} - failed for VpGuarantorId = {matchedVpGuarantor.VpGuarantorId}, retrying. {e.Message}");

                            //retry with an individual message for this hsGuarantorId and continue
                            this.Bus.Value.PublishMessage(new QueueFindNewHsGuarantorMatchesMessage()
                            {
                                VpGuarantorId = matchedVpGuarantor.VpGuarantorId
                            }).Wait();
                        }
                        else // let the bus manage retry
                        {
                            throw;
                        }
                    }
                }
            }

            // notify listeners that matches were made
            if (matchResults.IsNotNullOrEmpty())
            {
                NewGuarantorMatchCreatedMessageList messageList = new NewGuarantorMatchCreatedMessageList
                {
                    Messages = matchResults
                        .Distinct()
                        .Select(x => new NewGuarantorMatchCreatedMessage { MatchResult = x })
                        .ToList()
                };

                this.Bus.Value.PublishMessage(messageList).Wait();
                foreach (GuarantorMatchResult matchResult in matchResults)
                {
                    string[] tags = { $"{matchResult.MatchOption.ToString()} ({(int)matchResult.MatchOption})" };
                    this._metricsProvider.Value.Increment(Metrics.Increment.Matching.OngoingMatch.OngoingMatchMatchOption, tags: tags);
                }

            }

            return matchResults.Distinct();
        }

        private void UpdateMatchDataPersonalInformation(MatchDataGroup matchData, MatchDataGroup matchDataUpdates)
        {
            T GetGuarantorProperty<T>(Func<Guarantor, T> propertySelector, MatchDataGroup matchDataToUpdate)
            {
                return EqualityComparer<T>.Default.Equals(propertySelector(matchDataUpdates.Guarantor), default(T))
                    ? propertySelector(matchDataToUpdate.Guarantor)
                    : propertySelector(matchDataUpdates.Guarantor);
            }

            matchData.Guarantor.AddressLine1 = GetGuarantorProperty(x => x.AddressLine1, matchData);
            matchData.Guarantor.AddressLine2 = GetGuarantorProperty(x => x.AddressLine2, matchData);
            matchData.Guarantor.City = GetGuarantorProperty(x => x.City, matchData);
            matchData.Guarantor.DOB = GetGuarantorProperty(x => x.DOB, matchData);
            matchData.Guarantor.SSN = GetGuarantorProperty(x => x.SSN, matchData);
            matchData.Guarantor.SSN4 = GetGuarantorProperty(x => x.SSN4, matchData);
            matchData.Guarantor.FirstName = GetGuarantorProperty(x => x.FirstName, matchData);
            matchData.Guarantor.LastName = GetGuarantorProperty(x => x.LastName, matchData);
            matchData.Guarantor.PostalCode = GetGuarantorProperty(x => x.PostalCode, matchData);
            matchData.Guarantor.StateProvince = GetGuarantorProperty(x => x.StateProvince, matchData);
        }

        public IList<GuarantorMatchResult> UpdateMatchInfo(int vpGuarantorId, MatchDataGroup updatedMatchData, ApplicationEnum applicationEnum)
        {
            MatchVpGuarantorMatchInfo matchInfo = this.GetMatchedGuarantorMatchInfos(vpGuarantorId.ToListOfOne()).Single();
            IEnumerable<GuarantorMatchResult> matchResults = this.FindNewMatches(
                matchInfo.ToListOfOne(), 
                applicationEnum,
                false, 
                x => this.UpdateMatchDataPersonalInformation(x, updatedMatchData));

            return matchResults.ToList();
        }

        public string GetGuarantorMatchSsn(int vpGuarantorId)
        {
            return this.VpGuarantorMatchInfoRepository.Value.GetGuarantorMatchSsn(vpGuarantorId);
        }

        private IEnumerable<MatchRegistry> GetOngoingMatchRegistriesForMatchData(MatchDataGroup matchData, MatchVpGuarantorMatchInfo matchInfo, ApplicationEnum applicationEnum)
        {
            MatchRegistry SelectMatchRegistry(GuarantorMatchResultEnum matchResult, MatchRegistry matchRegistry) => matchResult == GuarantorMatchResultEnum.Matched ? matchRegistry : null;

            // find match registry records that match the given data
            IEnumerable<MatchRegistry> matchRegistries = this.GetMatchesForMatchData(matchData, SelectMatchRegistry, applicationEnum, PatternUseEnum.Ongoing)
                .Where(x => x != null);

            // validate additional data
            matchRegistries = matchRegistries.Where(x =>
            {
                MatchDataGroup matchDataGroupForRegistry = this.GetMatchDataGroup(x.SourceSystemKey, x.BillingSystemId, applicationEnum);
                bool matchInfoValid = this.CheckMatchInfo(matchInfo, matchDataGroupForRegistry, applicationEnum);

                return matchInfoValid;
            });

            //take top matches
            matchRegistries = this.GetMostConfidentMatchRegistries(matchRegistries);

            return matchRegistries;
        }

        private IEnumerable<MatchRegistry> GetMostConfidentMatchRegistries(IEnumerable<MatchRegistry> matchRegistries)
        {
            return matchRegistries
                .GroupBy(x => x.HsGuarantorId)
                .Select(x => x.OrderByDescending(r => r.PatternConfidence).First());
        }

        private IEnumerable<MatchRegistry> GetInitialMatchRegistriesForMatchData(MatchDataGroup matchData, ApplicationEnum applicationEnum)
        {
            MatchRegistry SelectMatchRegistry(GuarantorMatchResultEnum matchResult, MatchRegistry matchRegistry) => matchResult == GuarantorMatchResultEnum.Matched ? matchRegistry : null;

            // find match registry records that match the given data
            IEnumerable<MatchRegistry> matchRegistries = this.GetMatchesForMatchData(matchData, SelectMatchRegistry, applicationEnum, PatternUseEnum.Initial)
                .Where(x => x != null);

            return matchRegistries;
        }

        public void GetAllMatchRegistriesForInitialMatch(MatchDataGroup initialMatchData, ref MatchVpGuarantorMatchInfo matchInfo, ApplicationEnum applicationEnum)
        {
            // get auth matches
            IList<MatchRegistry> initialMatches = this.GetInitialMatchRegistriesForMatchData(initialMatchData, applicationEnum).ToList();
            IList<MatchRegistry> allMatches = new List<MatchRegistry>(initialMatches);

            // after finding the auth match based on user input, look up the match data from CDI and find any additional matches
            foreach (MatchRegistry guarantorMatchResult in initialMatches)
            {
                // get match data for initial match
                MatchDataGroup matchDataGroup = this.GetMatchDataGroup(guarantorMatchResult.SourceSystemKey, guarantorMatchResult.BillingSystemId, applicationEnum);

                // update match ssn
                this.SetMatchSsn(matchInfo, matchDataGroup.Guarantor.SSN);
                
                // update match dob
                this.SetMatchDob(matchInfo, matchDataGroup.Guarantor.DOB);

                // find new ongoing matches
                IEnumerable<MatchRegistry> newMatches = this.GetOngoingMatchRegistriesForMatchData(matchDataGroup, matchInfo, applicationEnum);

                // save em for later
                allMatches.AddRange(newMatches);
            }

            //
            IList<MatchRegistry> distinctMatches = this.GetMostConfidentMatchRegistries(allMatches).ToList();

            //
            matchInfo.AddMatches(distinctMatches);
        }

        private bool CheckMatchInfo(MatchVpGuarantorMatchInfo matchInfo, MatchDataGroup proposedMatchData, ApplicationEnum applicationEnum)
        {
            bool dobMatchesExisting = this.CheckMatchInfoDob(matchInfo, proposedMatchData, applicationEnum);
            bool ssnMatchesExisting = this.CheckMatchInfoSsn(matchInfo, proposedMatchData);

            return dobMatchesExisting && ssnMatchesExisting;
        }

        private bool CheckMatchInfoDob(MatchVpGuarantorMatchInfo matchInfo, MatchDataGroup proposedMatchData, ApplicationEnum applicationEnum)
        {
            if (matchInfo.MatchGuarantorDob.HasValue)
            {
                DateTime? dob = proposedMatchData?.Guarantor?.DOB;

                bool dobMatches = !dob.HasValue || matchInfo.MatchGuarantorDob.Value.Date == dob.Value.Date;
                bool dobValid = this.CheckGuarantorDob(proposedMatchData, PatternUseEnum.Ongoing, applicationEnum);

                return dobValid && dobMatches;
            }
            else
            {
                this.SetMatchDob(matchInfo, proposedMatchData?.Guarantor?.DOB);
                return true;
            }
        }

        private bool CheckMatchInfoSsn(MatchVpGuarantorMatchInfo matchInfo, MatchDataGroup proposedMatchData)
        {
            if (matchInfo.MatchSsn != null)
            {
                string ssn = proposedMatchData?.Guarantor?.SSN;
                bool isInvalidSsn = !this._ssnValidator.Value.IsValidNormalized(ssn);

                // if the new hsguarantor ssn is invalid we are relying on the strength of the match string
                return isInvalidSsn || matchInfo.MatchSsn == ssn;
            }
            else
            {
                this.SetMatchSsn(matchInfo, proposedMatchData?.Guarantor?.SSN);
                return true;
            }
        }

        private bool TryAddMatchToMatchInfo(MatchVpGuarantorMatchInfo matchInfo, MatchRegistry matchToAdd, ApplicationEnum applicationEnum)
        {
            MatchRegistryVpGuarantorMatch existingMatch = matchInfo.Matches.FirstOrDefault(x => x.HsGuarantorId == matchToAdd.HsGuarantorId);

            if (existingMatch != null)
            {
                decimal existingMatchConfidence = this.GetPatternConfidenceFromClientConfiguredMatchingSets(existingMatch.Registry.PatternSetId, applicationEnum, existingMatch.Registry.MatchOptionId);

                if (existingMatchConfidence < matchToAdd.PatternConfidence)
                {
                    existingMatch.Registry = matchToAdd;
                }

                // existing match
                return false;
            }
            else
            {
                matchInfo.AddMatch(matchToAdd);
            }

            // new match
            return true;
        } 

        public IList<GuarantorMatchResult> AddInitialMatches(int vpGuarantorId, MatchDataGroup matchData, ApplicationEnum applicationEnum)
        {
            using(UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                // 
                MatchVpGuarantorMatchInfo vpGuarantorMatchInfo = this.VpGuarantorMatchInfoRepository.Value.GetById(vpGuarantorId);

                // already a match
                if (vpGuarantorMatchInfo != null && vpGuarantorMatchInfo.Matches.IsNotNullOrEmpty())
                {
                    return new List<GuarantorMatchResult> { new GuarantorMatchResult { GuarantorMatchResultEnum = GuarantorMatchResultEnum.AlreadyRegistered } };
                }

                // 
                vpGuarantorMatchInfo = vpGuarantorMatchInfo ?? new MatchVpGuarantorMatchInfo()
                {
                    VpGuarantorId = vpGuarantorId
                };

                // populate root with initial match, and any additional (ongoing) matches
                this.GetAllMatchRegistriesForInitialMatch(matchData, ref vpGuarantorMatchInfo, applicationEnum);

                // 
                this.SaveMatchInfo(vpGuarantorMatchInfo);

                //
                IList<GuarantorMatchResult> matches = vpGuarantorMatchInfo.Matches.Select(x => new GuarantorMatchResult
                {
                    VpGuarantorId = vpGuarantorId,
                    MatchedHsGuarantorBillingSystemId = x.Registry.BillingSystemId,
                    MatchedHsGuarantorSourceSystemKey = x.Registry.SourceSystemKey,
                    MatchType = x.Registry.PatternUseId,
                    GuarantorMatchResultEnum = GuarantorMatchResultEnum.Matched,
                    MatchOption = x.Registry.MatchOptionId,
                    MatchedHsGuarantorId = x.HsGuarantorId,
                }).Distinct().ToList();

                if (matches.Count > 0)
                {
                    this._session.RegisterPostCommitSuccessAction(matchList =>
                    {
                        this._metricsProvider.Value.Increment(Metrics.Increment.Matching.AddInitialMatch.AddInitialMatchCount, matchList.Count);
                        foreach (GuarantorMatchResult match in matchList)
                        {
                            string[] tags = {$"{match.MatchOption.ToString()} ({(int) match.MatchOption})"};
                            this._metricsProvider.Value.Increment(Metrics.Increment.Matching.AddInitialMatch.AddInitialMatchMatchOption, tags: tags);
                        }
                    }, matches);
                }

                unitOfWork.Commit();

                //
                return matches;
            }
        }

        public void ProcessMatchUpdates(DateTime processDate, IList<MatchDataGroup> matchDatas, bool sendRetryMessage, bool isUpdate)
        {
            int processedCount = 0;
            
            using (StatelessUnitOfWork unitOfWork = new StatelessUnitOfWork(this._statelessSession))
            {
                foreach (MatchDataGroup matchData in matchDatas)
                {
                    try
                    {
                        this.GenerateAndPersistMatchRegistries(processDate, matchData, isUpdate);
                        processedCount++;
                    }
                    catch (Exception e)
                    {
                        if (sendRetryMessage)
                        {
                            this.LoggingService.Value.Fatal(() => $"{nameof(MatchingService)}::{nameof(this.ProcessMatchUpdates)} - failed for HsGuarantorId = {matchData.Guarantor.HsGuarantorId}, retrying. {e.Message}: {e.StackTrace}");

                            //retry with an individual message for this hsGuarantorId and continue
                            this.Bus.Value.PublishMessage(new PopulateMatchRegistryMessage
                            {
                                HsGuarantorId = matchData.Guarantor.HsGuarantorId,
                                ProcessDate = processDate,
                                IsUpdate = isUpdate
                            }).Wait();
                        }
                        else // let the bus manage retry
                        {
                            throw;
                        }
                    }
                    
                }

                unitOfWork.Commit();
            }

            this._metricsProvider.Value.Increment(Metrics.Increment.Matching.RegistryPopulation.ProcessedChanges, processedCount);
        }

        private void GenerateAndPersistMatchRegistries(DateTime processDate, MatchDataGroup matchData, bool isUpdate)
        {
            // generate
            IList<MatchRegistryDto> matches = this.GenerateMatchRegistries(processDate, matchData, null);

            // persist
            this.PersistMatchRegistry(matches, isUpdate);
        }

        /// <summary>
        /// saves match registries generated for a single MatchData entity
        /// </summary>
        private void PersistMatchRegistry(IList<MatchRegistryDto> matchRegistry, bool isUpdate)
        {
            IList<GuarantorMatchResult> matchDiscrepancies = new List<GuarantorMatchResult>();

            if (isUpdate)
            {
                foreach (MatchRegistryDto matchRegistryDto in matchRegistry)
                {
                    int? changedRegistryId = this.MatchingRepository.Value.UpdateMatchRegistryMatchString(matchRegistryDto);
                    if (changedRegistryId.HasValue)
                    {
                        this.ProcessMatchStringChange(matchRegistryDto, ref matchDiscrepancies);
                    }
                }
            }
            else
            {
                IList<MatchRegistry> matchRegistryEntities = AutoMapper.Mapper.Map<IList<MatchRegistry>>(matchRegistry);
                this.MatchingRepository.Value.BulkInsert(matchRegistryEntities);
            }

            // inbound will handle these discrepancies if this feature is disabled.
            bool featureEnabled = this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureMatchingApiIsEnabled);
            bool matchDiscrepanciesExist = matchDiscrepancies.IsNotNullOrEmpty();
            
            if (matchDiscrepanciesExist && featureEnabled)
            {
                // unique guarantor matches
                matchDiscrepancies = matchDiscrepancies
                    .GroupBy(x => new { x.VpGuarantorId, x.MatchedHsGuarantorBillingSystemId, x.MatchedHsGuarantorSourceSystemKey })
                    .Select(x => x.First())
                    .ToList();

                HsGuarantorMatchDiscrepancyMessageList message = new HsGuarantorMatchDiscrepancyMessageList
                {
                    Messages = matchDiscrepancies.Select(x => new HsGuarantorMatchDiscrepancyMessage{ MatchResult = x }).ToList()
                };

                this._session.RegisterPostCommitSuccessAction((x,matchDiscrepancyList) =>
                {
                    this.Bus.Value.PublishMessage(x).Wait();
                    this._metricsProvider.Value.Increment(Metrics.Increment.Matching.UnMatch.UnMatchCount, matchDiscrepancyList.Count);
                }, message, matchDiscrepancies);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newMatchRegistryDto"></param>
        /// <param name="matchDiscrepancies">ref to a list to store any match discrepancies found</param>
        private void ProcessMatchStringChange(MatchRegistryDto newMatchRegistryDto, ref IList<GuarantorMatchResult> matchDiscrepancies)
        {
            // could try stateless or sql if this is a bottleneck.
            MatchVpGuarantorMatchInfo matchInfo = this.VpGuarantorMatchInfoRepository.Value.GetVpGuarantorRegistryMatch(newMatchRegistryDto.HsGuarantorId);
            if (matchInfo != null)
            {
                // find a match associated with this HsGuarantor
                MatchRegistryVpGuarantorMatch match = matchInfo.Matches.First(x => x.Registry.HsGuarantorId == newMatchRegistryDto.HsGuarantorId);

                // publish message(s) to bus for match changes. ignoring changes to unmatched types
                if (match.HsGuarantorMatchStatusId.IsInCategory(HsGuarantorMatchStatusEnumCategory.Allowed))
                {
                    matchDiscrepancies.Add(new GuarantorMatchResult()
                    {
                        MatchedHsGuarantorBillingSystemId = newMatchRegistryDto.BillingSystemId,
                        MatchedHsGuarantorSourceSystemKey = newMatchRegistryDto.SourceSystemKey,
                        MatchType = newMatchRegistryDto.PatternUseId,
                        GuarantorMatchResultEnum = GuarantorMatchResultEnum.Matched,
                        VpGuarantorId = matchInfo.VpGuarantorId
                    });
                }
            }
        }

        private void SaveMatchInfo(MatchVpGuarantorMatchInfo matchInfo)
        {
            using(UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                this.VpGuarantorMatchInfoRepository.Value.InsertOrUpdate(matchInfo);

                unitOfWork.Commit();
            }
        }

        private bool IsPatientDataRequired(ApplicationEnum? applicationEnum) => this.GetRequiredMatchFields(applicationEnum, fieldSource: FieldSourceEnum.Patient).IsNotNullOrEmpty();

    }
}

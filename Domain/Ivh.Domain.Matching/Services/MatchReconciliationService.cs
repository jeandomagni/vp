﻿namespace Ivh.Domain.Matching.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Common.Base.Utilities.Extensions;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;
    using Logging.Interfaces;
    using NHibernate;

    public class MatchReconciliationService : BaseMatchingService, IMatchReconciliationService
    {
        private readonly Lazy<IMatchReconciliationRepository> _matchReconciliationRepository;
        private readonly Lazy<IMetricsProvider> _metricsProvider;
        private readonly ISession _session;

        public MatchReconciliationService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IMatchReconciliationRepository> matchReconciliationRepository,
            Lazy<IMatchVpGuarantorMatchInfoRepository> matchRegistryMatchInfoRepository,
            Lazy<IMatchRegistryRepository> matchingRepository,
            Lazy<IMetricsProvider> metricsProvider,
            ISessionContext<VisitPay> sessionContext
            ) : base(
            matchingRepository,
            matchRegistryMatchInfoRepository,
            serviceCommonService
            )
        {
            this._matchReconciliationRepository = matchReconciliationRepository;
            this._metricsProvider = metricsProvider;
            this._session = sessionContext.Session;
        }

        public IList<int> GetMatchReconciliationVpGuarantorIds()
        {
            IList<int> batches = this._matchReconciliationRepository.Value.GetAllMatchReconciliationVpGuarantorIds();

            return batches;
        }

        public void MatchReconciliation(IList<int> vpGuarantorIds)
        {
            IList<MatchReconciliationMatchInfo> reconciliationMatchInfos = this._matchReconciliationRepository.Value.GetMatchReconciliationMatchInfo(vpGuarantorIds);

            foreach (MatchReconciliationMatchInfo matchReconciliationMatchInfo in reconciliationMatchInfos)
            {
                this.MatchReconciliation(matchReconciliationMatchInfo);
            }
            
            this._metricsProvider.Value.Increment(Metrics.Increment.Matching.Reconciliation.Processed, vpGuarantorIds.Count);
        }

        private void MatchReconciliation(MatchReconciliationMatchInfo matchInfo)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                MatchVpGuarantorMatchInfo registryMatchInfo = this.VpGuarantorMatchInfoRepository.Value.GetById(matchInfo.VpGuarantorId);

                // auth matchOption may not be set by og matching. ensuring that here.
                MatchReconciliationHsGuarantorMatch authenticationMatch = matchInfo.InitialMatch();
                if (authenticationMatch != null)
                {
                    authenticationMatch.MatchOptionId = MatchOptionEnum.AuthenticationMatch;
                }

                // set match info
                if (registryMatchInfo == null)
                {
                    registryMatchInfo = new MatchVpGuarantorMatchInfo();
                    registryMatchInfo.VpGuarantorId = matchInfo.VpGuarantorId;
                    this.SetMatchSsn(registryMatchInfo, matchInfo.SSN);
                    this.SetMatchDob(registryMatchInfo, matchInfo.DOB);
                }

                // get registries for hsguarantors
                IList<int> hsGuarantorIds = matchInfo.HsGuarantorMatches
                    .Select(x => x.HsGuarantorId)
                    .Distinct()
                    .ToList();

                Dictionary<int, List<MatchRegistry>> registries = this.MatchingRepository.Value.GetMatchRegistries(hsGuarantorIds)
                    .GroupBy(x => x.HsGuarantorId)
                    .ToDictionary(x => x.Key, x => x.ToList());

                // add matches if they dont exist
                foreach (MatchReconciliationHsGuarantorMatch matchReconciliationHsGuarantorMatch in matchInfo.HsGuarantorMatches)
                {
                    bool matchDoesNotExist = registryMatchInfo.Matches.All(x => x.HsGuarantorId != matchReconciliationHsGuarantorMatch.HsGuarantorId);
                    bool registriesForHsGuarantorExist = registries.ContainsKey(matchReconciliationHsGuarantorMatch.HsGuarantorId);

                    if (matchDoesNotExist && registriesForHsGuarantorExist)
                    {
                        IList<MatchRegistry> registriesForHsGuarantor = registries[matchReconciliationHsGuarantorMatch.HsGuarantorId];
                        MatchRegistry matchToAdd = this.GetMatchRegistryForMatchOption(registriesForHsGuarantor, matchReconciliationHsGuarantorMatch.MatchOptionId);

                        if (matchToAdd != null)
                        {
                            registryMatchInfo.AddMatch(matchToAdd, matchReconciliationHsGuarantorMatch.HsGuarantorMatchStatusId);
                        }
                        else
                        {
                            this.LoggingService.Value.Warn(() => $"{nameof(MatchReconciliationService)}::{nameof(MatchReconciliation)}" +
                                                                 $" -- unable to find registry for hsguarantorId: {matchReconciliationHsGuarantorMatch.HsGuarantorId}" +
                                                                 $" and matchOption: {matchReconciliationHsGuarantorMatch.MatchOptionId.ToString()}");
                        }
                    }
                }

                this.VpGuarantorMatchInfoRepository.Value.InsertOrUpdate(registryMatchInfo);

                unitOfWork.Commit();
            }
        }

        private MatchRegistry GetMatchRegistryForMatchOption(IList<MatchRegistry> matchRegistries, MatchOptionEnum? option)
        {
            if (matchRegistries.IsNullOrEmpty())
            {
                return null;
            }

            // if og matching set this flag, it was the registration authentication match. so, use an initial match pattern
            if (option == MatchOptionEnum.AuthenticationMatch)
            {
                return matchRegistries.FirstOrDefault(x => x.PatternUseId == PatternUseEnum.Initial);
            }

            // see if this was matched with a valid matching B option
            MatchRegistry foundMatchRegistry = matchRegistries.FirstOrDefault(x => x.MatchOptionId == option);

            if (foundMatchRegistry != null)
            {
                return foundMatchRegistry;
            }

            // check for old match option that maps to a new option
            bool hasValidMap = option.HasValue && OriginalMatchOptionToMatchingBMatchOptionMap.ContainsKey(option.Value);
            if (hasValidMap)
            {
                MatchOptionEnum newMatchOptionEnum = OriginalMatchOptionToMatchingBMatchOptionMap[option.Value];
                foundMatchRegistry = matchRegistries.FirstOrDefault(x => x.MatchOptionId == newMatchOptionEnum);

                if (foundMatchRegistry != null)
                {
                    return foundMatchRegistry;
                }
            }

            // no direct mapping, so just pick the highest confidence ongoing match pattern
            return matchRegistries
                .Where(x => x.PatternUseId == PatternUseEnum.Ongoing)
                .OrderByDescending(x => this.GetPatternConfidenceFromClientConfiguredMatchingSets(x.PatternSetId, ApplicationEnum.VisitPay, x.MatchOptionId))
                .FirstOrDefault(x => x.PatternUseId == PatternUseEnum.Ongoing);
        }

        private static readonly IDictionary<MatchOptionEnum, MatchOptionEnum> OriginalMatchOptionToMatchingBMatchOptionMap = new Dictionary<MatchOptionEnum, MatchOptionEnum>
        {
            [MatchOptionEnum.SsnDobLname] = MatchOptionEnum.DobLnameSsn,
            [MatchOptionEnum.DobLnameNoSsn] = MatchOptionEnum.Add1CityDobFnameLnameState,
            [MatchOptionEnum.LnameSsnNoDob] = MatchOptionEnum.CityLnameSsnState,
            [MatchOptionEnum.LnameMinorNoSsnNoDob] = MatchOptionEnum.Add1CityFnameLnameState,
        };
    }
}

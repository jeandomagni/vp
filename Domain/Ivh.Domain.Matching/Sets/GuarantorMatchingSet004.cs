﻿namespace Ivh.Domain.Matching.Sets
{
    using System.Collections.Generic;
    using Common.VisitPay.Enums;
    using Match;
    using Patterns;

    public class GuarantorMatchingSet004 : Set
    {
        public GuarantorMatchingSet004(IDictionary<string, MatchSetConfigurationRegexEnum> configuration, IList<ApplicationEnum> applications, decimal priority = 1.0m) : base(
            PatternSetEnum.S004,
            priority,
            applications,
            configuration,
            new GuarantorMatchingPatternSskLnameStatementId(configuration, PatternUseEnum.Initial, 0.90m)
            )
        {
        }
    }
}

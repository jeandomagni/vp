﻿namespace Ivh.Domain.Matching.Sets
{
    using Common.VisitPay.Enums;
    using Match;
    using Patterns;
    using System.Collections.Generic;

    public class GuarantorMatchingSet002 : Set
    {
        public GuarantorMatchingSet002(IDictionary<string, MatchSetConfigurationRegexEnum> configuration, IList<ApplicationEnum> applications, decimal priority = 1.0m) : base(
            PatternSetEnum.S002,
            priority,
            applications,
            configuration,
            new GuarantorMatchingPatternSskLname(configuration, PatternUseEnum.Initial, 1.00m),
            new GuarantorMatchingPatternLnameDobSsn(configuration, PatternUseEnum.Ongoing, 0.95m))
        {
        }
    }
}

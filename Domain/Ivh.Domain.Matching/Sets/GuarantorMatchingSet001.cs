﻿namespace Ivh.Domain.Matching.Sets
{
    using System.Collections.Generic;
    using Common.VisitPay.Enums;
    using Match;
    using Patterns;

    public class GuarantorMatchingSet001 : Set
    {
        public GuarantorMatchingSet001(IDictionary<string, MatchSetConfigurationRegexEnum> configuration, IList<ApplicationEnum> applications, decimal priority = 1.0m) : base(
            PatternSetEnum.S001,
            priority,
            applications,
            configuration,
            new GuarantorMatchingPatternSskLnameDobSsn4Zip(configuration, PatternUseEnum.Initial, 1.00m),
            new GuarantorMatchingPatternLnameDobSsnZip(configuration, PatternUseEnum.Ongoing, 0.98m))
        {
        }
    }
}

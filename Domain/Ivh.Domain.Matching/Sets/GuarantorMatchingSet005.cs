﻿namespace Ivh.Domain.Matching.Sets
{
    using System.Collections.Generic;
    using Common.VisitPay.Enums;
    using Match;
    using Patterns;

    public class GuarantorMatchingSet005 : Set
    {
        public GuarantorMatchingSet005(IDictionary<string, MatchSetConfigurationRegexEnum> configuration, IList<ApplicationEnum> applications, decimal priority = 1.0m) : base(
            PatternSetEnum.S005,
            priority,
            applications,
            configuration,
            new GuarantorMatchingPatternSskLnameDobZip(configuration, PatternUseEnum.Initial, 1.00m),
            new GuarantorMatchingPatternLnameDobSsn(configuration, PatternUseEnum.Ongoing, 0.99m),
            new GuarantorMatchingPatternFnameLnameDobAdd1CityState(configuration, PatternUseEnum.Ongoing, 0.98m),
            new GuarantorMatchingPatternLnameSsnCityState(configuration, PatternUseEnum.Ongoing, 0.97m)
            )
        {
        }
    }
}

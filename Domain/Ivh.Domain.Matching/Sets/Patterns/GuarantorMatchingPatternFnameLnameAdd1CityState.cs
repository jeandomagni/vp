﻿namespace Ivh.Domain.Matching.Sets.Patterns
{
    using System.Collections.Generic;
    using Common.VisitPay.Enums;
    using Match;

    public class GuarantorMatchingPatternFnameLnameAdd1CityState : Pattern
    {
        public GuarantorMatchingPatternFnameLnameAdd1CityState(IDictionary<string, MatchSetConfigurationRegexEnum> configuration, PatternUseEnum patternUseEnum, decimal confidence) : base(
            MatchOptionEnum.Add1CityFnameLnameState,
            patternUseEnum,
            confidence,
            configuration,
            Fields.Guarantor.FirstName(configuration),
            Fields.Guarantor.LastName(configuration),
            Fields.Guarantor.Address1(configuration),
            Fields.Guarantor.City(configuration),
            Fields.Guarantor.State(configuration)
            )
        {
        }
    }
}

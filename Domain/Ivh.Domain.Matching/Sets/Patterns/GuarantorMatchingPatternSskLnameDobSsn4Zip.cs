﻿namespace Ivh.Domain.Matching.Sets.Patterns
{
    using System.Collections.Generic;
    using Common.VisitPay.Enums;
    using Match;

    public class GuarantorMatchingPatternSskLnameDobSsn4Zip : Pattern
    {
        public GuarantorMatchingPatternSskLnameDobSsn4Zip(IDictionary<string, MatchSetConfigurationRegexEnum> configuration, PatternUseEnum patternUseEnum, decimal confidence) : base(
            MatchOptionEnum.DobLnameSsn4SskZip,
            patternUseEnum,
            confidence,
            configuration,
            Fields.Guarantor.SourceSystemKey(configuration),
            Fields.Guarantor.LastName(configuration),
            Fields.Guarantor.DOB(configuration),
            Fields.Guarantor.Ssn4(configuration),
            Fields.Guarantor.Zip(configuration)
            )
        {
        }
    }
}

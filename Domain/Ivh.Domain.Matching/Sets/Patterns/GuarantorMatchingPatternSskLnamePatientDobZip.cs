﻿namespace Ivh.Domain.Matching.Sets.Patterns
{
    using System.Collections.Generic;
    using Common.VisitPay.Enums;
    using Match;

    public class GuarantorMatchingPatternSskLnamePatientDobZip : Pattern
    {
        public GuarantorMatchingPatternSskLnamePatientDobZip(IDictionary<string, MatchSetConfigurationRegexEnum> configuration, PatternUseEnum patternUseEnum, decimal confidence) : base(
            MatchOptionEnum.LnamePatientDobSskZip,
            patternUseEnum,
            confidence,
            configuration,
            Fields.Guarantor.SourceSystemKey(configuration),
            Fields.Guarantor.LastName(configuration),
            Fields.Patient.DOB(configuration),
            Fields.Guarantor.Zip(configuration)
            )
        {
        }
    }
}

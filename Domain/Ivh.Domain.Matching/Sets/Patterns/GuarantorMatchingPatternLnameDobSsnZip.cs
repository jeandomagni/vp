﻿namespace Ivh.Domain.Matching.Sets.Patterns
{
    using System.Collections.Generic;
    using Common.VisitPay.Enums;
    using Match;

    public class GuarantorMatchingPatternLnameDobSsnZip : Pattern
    {
        public GuarantorMatchingPatternLnameDobSsnZip(IDictionary<string, MatchSetConfigurationRegexEnum> configuration, PatternUseEnum patternUseEnum, decimal confidence) : base(
            MatchOptionEnum.DobLnameSsnZip,
            patternUseEnum,
            confidence,
            configuration,
            Fields.Guarantor.LastName(configuration),
            Fields.Guarantor.DOB(configuration),
            Fields.Guarantor.Ssn(configuration),
            Fields.Guarantor.Zip(configuration)
            )
        {
        }
    }
}

﻿namespace Ivh.Domain.Matching.Sets.Patterns
{
    using System.Collections.Generic;
    using Common.VisitPay.Enums;
    using Match;

    public class GuarantorMatchingPatternSskLnameDobZip : Pattern
    {
        public GuarantorMatchingPatternSskLnameDobZip(IDictionary<string, MatchSetConfigurationRegexEnum> configuration, PatternUseEnum patternUseEnum, decimal confidence) : base(
            MatchOptionEnum.DobLnameSskZip,
            patternUseEnum,
            confidence,
            configuration,
            Fields.Guarantor.SourceSystemKey(configuration),
            Fields.Guarantor.LastName(configuration),
            Fields.Guarantor.DOB(configuration),
            Fields.Guarantor.Zip(configuration)
            )
        {
        }
    }
}

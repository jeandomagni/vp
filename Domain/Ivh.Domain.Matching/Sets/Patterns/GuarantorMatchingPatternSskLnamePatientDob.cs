﻿namespace Ivh.Domain.Matching.Sets.Patterns
{
    using System.Collections.Generic;
    using Common.VisitPay.Enums;
    using Match;

    public class GuarantorMatchingPatternSskLnamePatientDob : Pattern
    {
        public GuarantorMatchingPatternSskLnamePatientDob(IDictionary<string, MatchSetConfigurationRegexEnum> configuration, PatternUseEnum patternUseEnum, decimal confidence) : base(
            MatchOptionEnum.DobLnameSsk,
            patternUseEnum,
            confidence,
            configuration,
            Fields.Guarantor.SourceSystemKey(configuration),
            Fields.Guarantor.LastName(configuration),
            Fields.Patient.DOB(configuration)
            )
        {
        }
    }
}

﻿namespace Ivh.Domain.Matching.Sets.Patterns
{
    using System.Collections.Generic;
    using Common.VisitPay.Enums;
    using Match;

    public class GuarantorMatchingPatternFnameLnameDobAdd1CityState : Pattern
    {
        public GuarantorMatchingPatternFnameLnameDobAdd1CityState(IDictionary<string, MatchSetConfigurationRegexEnum> configuration, PatternUseEnum patternUseEnum, decimal confidence) : base(
            MatchOptionEnum.Add1CityDobFnameLnameState,
            patternUseEnum,
            confidence,
            configuration,
            Fields.Guarantor.FirstName(configuration),
            Fields.Guarantor.LastName(configuration),
            Fields.Guarantor.DOB(configuration),
            Fields.Guarantor.Address1(configuration),
            Fields.Guarantor.City(configuration),
            Fields.Guarantor.State(configuration)            
            )
        {
        }
    }
}

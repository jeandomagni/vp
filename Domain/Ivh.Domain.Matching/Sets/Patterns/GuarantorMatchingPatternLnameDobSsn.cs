﻿namespace Ivh.Domain.Matching.Sets.Patterns
{
    using System.Collections.Generic;
    using Common.VisitPay.Enums;
    using Match;

    public class GuarantorMatchingPatternLnameDobSsn : Pattern
    {
        public GuarantorMatchingPatternLnameDobSsn(IDictionary<string, MatchSetConfigurationRegexEnum> configuration, PatternUseEnum patternUseEnum, decimal confidence) : base(
            MatchOptionEnum.DobLnameSsn,
            patternUseEnum,
            confidence,
            configuration,
            Fields.Guarantor.LastName(configuration),
            Fields.Guarantor.DOB(configuration),
            Fields.Guarantor.Ssn(configuration)
            )
        {
        }
    }
}

﻿namespace Ivh.Domain.Matching.Sets.Patterns
{
    using System.Collections.Generic;
    using Common.VisitPay.Enums;
    using Match;

    public class GuarantorMatchingPatternSskLnameStatementId : Pattern
    {
        public GuarantorMatchingPatternSskLnameStatementId(IDictionary<string, MatchSetConfigurationRegexEnum> configuration, PatternUseEnum patternUseEnum, decimal confidence) : base(
            MatchOptionEnum.LnameSsk,
            patternUseEnum,
            confidence,
            configuration,
            Fields.Guarantor.SourceSystemKey(configuration),
            Fields.Guarantor.LastName(configuration),
            Fields.Patient.StatementIdentifierId(configuration)
            )
        {
        }
    }
}

﻿namespace Ivh.Domain.Matching.Sets.Patterns
{
    using System.Collections.Generic;
    using Common.VisitPay.Enums;
    using Match;

    public class GuarantorMatchingPatternLnameSsnCityState : Pattern
    {
        public GuarantorMatchingPatternLnameSsnCityState(IDictionary<string, MatchSetConfigurationRegexEnum> configuration, PatternUseEnum patternUseEnum, decimal confidence) : base(
            MatchOptionEnum.CityLnameSsnState,
            patternUseEnum,
            confidence,
            configuration,
            Fields.Guarantor.LastName(configuration),
            Fields.Guarantor.Ssn(configuration),
            Fields.Guarantor.City(configuration),
            Fields.Guarantor.State(configuration)
            )
        {
        }
    }
}

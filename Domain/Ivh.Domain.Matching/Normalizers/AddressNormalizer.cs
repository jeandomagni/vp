﻿namespace Ivh.Domain.Matching.Normalizers
{
    using System.Text;
    using Interfaces;

    public class AddressNormalizer : INormalizer
    {
        public string Normalize(string input)
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (string token in input.Split(null)) //specifying null as the delimiter assumes all white space
            {
                switch (token.ToLower())
                {
                    case "street":
                        stringBuilder.Append("st");
                        break;
                    case "avenue":
                        stringBuilder.Append("ave");
                        break;
                    case "north":
                        stringBuilder.Append("n");
                        break;
                    case "south":
                        stringBuilder.Append("s");
                        break;
                    case "east":
                        stringBuilder.Append("e");
                        break;
                    case "west":
                        stringBuilder.Append("w");
                        break;
                    case ".":
                        //remove periods
                        break;
                    default:
                        stringBuilder.Append(token);
                        break;
                }
            }
            return stringBuilder.ToString();
        }
    }
}
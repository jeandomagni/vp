﻿namespace Ivh.Domain.Matching.Modules
{
    using Autofac;
    using Interfaces;
    using Services;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<MatchingService>().As<IMatchingService>();
            builder.RegisterType<InitialMatchService>().As<IInitialMatchService>();
            builder.RegisterType<MatchReconciliationService>().As<IMatchReconciliationService>();
        }
    }
}

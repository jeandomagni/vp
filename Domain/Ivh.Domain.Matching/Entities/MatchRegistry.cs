﻿namespace Ivh.Domain.Matching.Entities
{
    using System;
    using System.Text;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;

    public class MatchRegistry
    {
        public MatchRegistry()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        public MatchRegistry(string matchString, string sourceSystemKey, int billingSystemId) : this()
        {
            this.MatchString = matchString;
            this.SourceSystemKey = sourceSystemKey;
            this.BillingSystemId = billingSystemId;
        }


        public virtual int RegistryId { get; set; }
        public virtual string MatchString { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual DateTime DataChangeDate { get; set; }
        public virtual DateTime? DeletedDate { get; set; }
        public virtual decimal PartitionKey { get; set; }
        public virtual MatchOptionEnum MatchOptionId { get; set; }
        public virtual PatternSetEnum PatternSetId { get; set; }
        public virtual PatternUseEnum PatternUseId { get; set; }
        public virtual decimal PatternConfidence { get; set; }


        public virtual int BillingSystemId { get; set; }
        public virtual string SourceSystemKey { get; set; }
        public virtual int HsGuarantorId { get; set; }
        public virtual int? BaseVisitId { get; set; }

        #region equality
        protected bool Equals(MatchRegistry other)
        {
            return string.Equals(this.MatchString, other.MatchString) && this.MatchOptionId == other.MatchOptionId && this.PatternSetId == other.PatternSetId && this.PatternUseId == other.PatternUseId && this.BillingSystemId == other.BillingSystemId && string.Equals(this.SourceSystemKey, other.SourceSystemKey) && this.BaseVisitId == other.BaseVisitId;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != this.GetType())
            {
                return false;
            }

            return Equals((MatchRegistry)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = (this.MatchString != null ? this.MatchString.GetConsistentHashCode() : 0);
                hashCode = (hashCode * 397) ^ (int)this.MatchOptionId;
                hashCode = (hashCode * 397) ^ (int)this.PatternSetId;
                hashCode = (hashCode * 397) ^ (int)this.PatternUseId;
                hashCode = (hashCode * 397) ^ (this.BaseVisitId != null ? this.BaseVisitId.Value : 0);
                hashCode = (hashCode * 397) ^ this.BillingSystemId;
                hashCode = (hashCode * 397) ^ (this.SourceSystemKey != null ? this.SourceSystemKey.GetConsistentHashCode() : 0);
                return hashCode;
            }
        }
        #endregion equality

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendKeyValuePair(nameof(this.SourceSystemKey), this.SourceSystemKey);
            sb.AppendKeyValuePair(nameof(this.MatchString), this.MatchString);
            sb.AppendKeyValuePair(nameof(this.PatternSetId), this.PatternSetId.ToString());
            sb.AppendKeyValuePair(nameof(this.PatternUseId), this.PatternUseId.ToString());

            if (this.DeletedDate.HasValue)
            {
                sb.AppendKeyValuePair(nameof(this.DeletedDate), this.DeletedDate.ToString());
            }

            return sb.Remove(sb.Length - 1, 1).ToString();
        }

    }
}

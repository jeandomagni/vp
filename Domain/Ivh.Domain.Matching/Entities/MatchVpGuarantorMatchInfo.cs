﻿namespace Ivh.Domain.Matching.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;

    public class MatchVpGuarantorMatchInfo
    {
        private IList<MatchRegistryVpGuarantorMatch> _matches;
        public MatchVpGuarantorMatchInfo()
        {
            this.InsertDate = DateTime.UtcNow;
            this._matches = new List<MatchRegistryVpGuarantorMatch>();
        }

        public virtual int VpGuarantorId { get; set; }
        public virtual string MatchSsn { get; protected set; }
        public virtual DateTime? MatchGuarantorDob { get; protected set; }
        public virtual DateTime InsertDate { get; set; }

        public virtual IList<MatchRegistryVpGuarantorMatch> Matches
        {
            get => this._matches; 
            set => this._matches = value;
        }

        public virtual IList<MatchRegistryVpGuarantorMatch> UniqueMatchedHsGuarantors =>
            this.Matches
                .Where(x => x.HsGuarantorMatchStatusId == HsGuarantorMatchStatusEnum.Matched && x.Registry.DeletedDate == null)
                .GroupBy(x => new { x.Registry.SourceSystemKey, x.Registry.BillingSystemId })
                .Select(x => x.First())
                .ToList();

        public virtual void AddMatches(IEnumerable<MatchRegistry> newMatches)
        {
            foreach (MatchRegistry matchRegistry in newMatches)
            {
                this.AddMatch(matchRegistry);
            }
        }

        public virtual void AddMatch(MatchRegistry newMatch, HsGuarantorMatchStatusEnum? matchStatus = null)
        {
            this.Matches.Add(new MatchRegistryVpGuarantorMatch
            {
                Registry = newMatch,
                VpGuarantor = this,
                HsGuarantorMatchStatusId = matchStatus ?? HsGuarantorMatchStatusEnum.Matched,
            });
        }

        /// <summary>
        /// Sets match ssn.
        /// </summary>
        /// <param name="matchSsn"></param>
        /// <exception cref="ArgumentException">if the match ssn changes</exception>
        public virtual void SetMatchSsn(string matchSsn)
        {
            if (string.IsNullOrEmpty(matchSsn))
            {
                return;
            }

            if (string.IsNullOrEmpty(this.MatchSsn))
            {
                this.MatchSsn = matchSsn;
            }
            else if (this.MatchSsn != matchSsn)
            {
                // initial matches have conflicting SSNs. do.. something?
                throw new ArgumentException("conflicting match SSNs");
            }
        }

        public virtual void SetMatchGuarantorDob(DateTime dob)
        {
            if (!this.MatchGuarantorDob.HasValue)
            {
                this.MatchGuarantorDob = dob;
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendKeyValuePair(nameof(this.VpGuarantorId), this.VpGuarantorId.ToString());
            sb.AppendKeyValuePair(nameof(this.MatchSsn), this.MatchSsn);

            StringBuilder sbInner = new StringBuilder();
            sbInner.Append("{" + Environment.NewLine);
            foreach (MatchRegistryVpGuarantorMatch matchRegistryVpGuarantorMatch in this.Matches)
            {
                sbInner.Append('\t').AppendKeyValuePair(nameof(matchRegistryVpGuarantorMatch.Registry), matchRegistryVpGuarantorMatch.Registry);
                sbInner.Append('\t').AppendKeyValuePair(nameof(matchRegistryVpGuarantorMatch.Registry.DeletedDate), matchRegistryVpGuarantorMatch.Registry.DeletedDate.ToString());
            }
            sbInner.Append("}");

            sb.AppendKeyValuePair(nameof(this.Matches), sbInner.ToString());

            return sb.Remove(sb.Length - 1, 1).ToString();
        }
    }
}

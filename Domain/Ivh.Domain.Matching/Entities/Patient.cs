﻿namespace Ivh.Domain.Matching.Entities
{
    using Ivh.Application.Base.Common.Interfaces.Entities.HsVisit;
    using System;

    public class Patient : IMatchPatient
    {
        public virtual int VisitId { get; set; }
        public virtual int HsGuarantorId { get; set; }
        public virtual DateTime? PatientDOB { get; set; }
        public virtual DateTime? DataChangeDate { get; set; }
        public virtual int ServiceGroupId { get; set; }
        public virtual int StatementIdentifierId { get; set; }

        #region Equality
        public override bool Equals(object obj)
        {
            Patient other = obj as Patient;

            if (ReferenceEquals(null, other))
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return this.PatientDOB == other.PatientDOB &&
                this.HsGuarantorId == other.HsGuarantorId;
        }
        public override int GetHashCode()
        {
            unchecked
            {
                int hash = this.GetType().GetHashCode();
                hash = (hash * 31) ^ this.PatientDOB.GetHashCode();
                hash = (hash * 31) ^ this.HsGuarantorId.GetHashCode();

                return hash;
            }
        }

        #endregion Equality
    }
}

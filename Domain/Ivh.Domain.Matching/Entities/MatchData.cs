﻿namespace Ivh.Domain.Matching.Entities
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Utilities.Extensions;

    public class MatchData
    {
        public Guarantor Guarantor { get; set; }
        public Patient Patient { get; set; }
    }

    public class MatchDataGroup
    {
        public Guarantor Guarantor { get; set; }
        public IList<MatchVisitData> Visits { get; set; }

        public IEnumerable<MatchData> ToMatchDatas()
        {
            return this.Visits.IsNotNullOrEmpty()
                       ? this.Visits.Select(x => new MatchData { Guarantor = this.Guarantor, Patient = x.Patient })
                       : new MatchData { Guarantor = this.Guarantor }.ToListOfOne();
        }
    }

    public class MatchVisitData
    {
        public Patient Patient { get; set; }
    }

}

﻿namespace Ivh.Domain.Matching.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.VisitPay.Enums;

    public class MatchReconciliationMatchInfo
    {
        public virtual int VpGuarantorId { get; set; }
        public virtual string SSN { get; set; }
        public virtual string UnderlyingMatchSSN { get; set; }
        public virtual DateTime DOB { get; set; }

        public virtual string MatchSsn => string.IsNullOrEmpty(this.UnderlyingMatchSSN) 
            ? this.SSN 
            : this.UnderlyingMatchSSN;

        public virtual MatchReconciliationHsGuarantorMatch InitialMatch() 
        {
            // if the authentication matchoption was set, use it
            MatchReconciliationHsGuarantorMatch initialMatch = this.HsGuarantorMatches.FirstOrDefault(x => x.MatchOptionId == MatchOptionEnum.AuthenticationMatch);

            if (initialMatch != null)
            {
                return initialMatch;
            }

            // otherwise, it's probably the first inserted match
            return this.HsGuarantorMatches.OrderByDescending(x => x.MatchedOn).FirstOrDefault();
        }

        public virtual IList<MatchReconciliationHsGuarantorMatch> HsGuarantorMatches { get; set; } = new List<MatchReconciliationHsGuarantorMatch>();
    }
}

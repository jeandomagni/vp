﻿namespace Ivh.Domain.Matching.Entities
{
    using System;
    using Common.VisitPay.Enums;

    public class MatchRegistryVpGuarantorMatch : IEquatable<MatchRegistryVpGuarantorMatch>
    {
        public MatchRegistryVpGuarantorMatch()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        public virtual MatchRegistry Registry { get; set; }
        public virtual MatchVpGuarantorMatchInfo VpGuarantor { get; set; }
        public virtual HsGuarantorMatchStatusEnum HsGuarantorMatchStatusId { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual int HsGuarantorId
        {
            get => this.Registry.HsGuarantorId;
            set { }
        }

        #region equality
        public virtual bool Equals(MatchRegistryVpGuarantorMatch other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return Equals(this.VpGuarantor.VpGuarantorId, other.VpGuarantor.VpGuarantorId) && this.HsGuarantorId == other.HsGuarantorId;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != this.GetType())
            {
                return false;
            }

            return Equals((MatchRegistryVpGuarantorMatch)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (this.HsGuarantorId * 397) ^ (this.VpGuarantor != null ? this.VpGuarantor.VpGuarantorId : 0);
            }
        }

        #endregion equality
    }
}

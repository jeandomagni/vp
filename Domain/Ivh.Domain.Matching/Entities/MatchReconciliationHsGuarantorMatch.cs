﻿namespace Ivh.Domain.Matching.Entities
{
    using System;
    using Common.VisitPay.Enums;

    public class MatchReconciliationHsGuarantorMatch
    {
        public virtual int VpGuarantorHsMatchId { get; set; }

        public virtual int VpGuarantorId { get; set; }

        public virtual int HsGuarantorId { get; set; }

        public virtual MatchOptionEnum? MatchOptionId { get; set; }

        public virtual DateTime MatchedOn { get; set; }

        public virtual HsGuarantorMatchStatusEnum HsGuarantorMatchStatusId { get; set; }
    }
}

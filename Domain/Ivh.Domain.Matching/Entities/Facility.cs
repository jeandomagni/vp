﻿namespace Ivh.Domain.Matching.Entities
{
    using System;

    public class Facility : IEquatable<Facility>
    {
        #region composite key
        public virtual int HsGuarantorId { get; set; }
        public virtual int InsertLoadTrackerId { get; set; }
        public virtual int ServiceGroupId { get; set; }
        #endregion

        public virtual int StatementIdentifierId { get; set; }

        #region equality
        public virtual bool Equals(Facility other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return this.HsGuarantorId == other.HsGuarantorId 
                   && this.InsertLoadTrackerId == other.InsertLoadTrackerId 
                   && this.ServiceGroupId == other.ServiceGroupId;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != this.GetType())
            {
                return false;
            }

            return Equals((Facility)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = this.HsGuarantorId;
                hashCode = (hashCode * 397) ^ this.InsertLoadTrackerId;
                hashCode = (hashCode * 397) ^ this.ServiceGroupId;
                return hashCode;
            }
        }
        #endregion
    }
}

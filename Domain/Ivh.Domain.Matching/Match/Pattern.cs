﻿namespace Ivh.Domain.Matching.Match
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using Application.Matching.Common.Dtos;
    using Common.VisitPay.Enums;
    using Entities;

    public abstract class Pattern
    {
        private readonly IDictionary<string, MatchSetConfigurationRegexEnum> _configuration;

        public decimal Confidence { get; }
        public PatternUseEnum PatternUse { get; }
        public MatchOptionEnum MatchOptionEnum { get; }

        private const string Delimiter = "&";
        public IList<Field> MatchFields { get; }

        protected Pattern(
            MatchOptionEnum patternEnum,
            PatternUseEnum patternUse, 
            decimal confidence, 
            IDictionary<string, MatchSetConfigurationRegexEnum> configuration,
            params Field[] matchFields)
        {
            this.MatchOptionEnum = patternEnum;
            this.PatternUse = patternUse;
            this.Confidence = confidence;
            this._configuration = configuration;
            this.MatchFields = matchFields;
        }

        public MatchRegistryDto GetMatchResult(MatchData matchData)
        {
            return new MatchRegistryDto(this.GetMatchString(matchData), matchData.Guarantor.SourceSystemKey, matchData.Guarantor.HsBillingSystemId)
            {
                MatchOptionId = this.MatchOptionEnum,
                PatternUseId = this.PatternUse,
                PatternConfidence = this.Confidence,
                HsGuarantorId = matchData.Guarantor.HsGuarantorId,
                BaseVisitId = matchData.Patient?.VisitId
            };
        }

        public string GetMatchString(MatchData entity)
        {
            if (!this.IsValid(entity))
            {
                //return default(string);
                return string.Empty;
            }
            return string.Join(Delimiter, this.MatchFields.Select(x => x.GetValue(entity)));
        }

        public string GetMatchHash(string matchString)
        {
            if (string.IsNullOrEmpty(matchString))
            {
                return default(string);
            }

            using (HashAlgorithm hashAlgorithm = new SHA256Managed())
            {
                StringBuilder hash = new System.Text.StringBuilder();
                byte[] crypto = hashAlgorithm.ComputeHash(Encoding.UTF8.GetBytes(matchString));
                foreach (byte theByte in crypto)
                {
                    hash.Append(theByte.ToString("x2"));
                }
                return hash.ToString();
            }
        }

        private bool IsValid(MatchData entity)
        {
            return this.MatchFields.All(x => x.IsValid(entity));
        }
    }

    
}

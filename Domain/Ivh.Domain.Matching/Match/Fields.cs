﻿namespace Ivh.Domain.Matching.Match
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;
    using Normalizers;
    using Validators;

    public static class Fields
    {
        private static readonly INormalizer AddressNormalizer = new AddressNormalizer();

        public static class Guarantor
        {
            private const FieldSourceEnum SourceEnum = FieldSourceEnum.Guarantor;

            public static Field SourceSystemKey(IDictionary<string, MatchSetConfigurationRegexEnum> configuration) => new Field<string>(GuarantorSourceSystemKey, configuration, Guarantor.SourceEnum, FieldPropertyEnum.SourceSystemKey);

            public static Field Ssn(IDictionary<string, MatchSetConfigurationRegexEnum> configuration) => new Field<string>(GuarantorSsn, configuration, Guarantor.SourceEnum, FieldPropertyEnum.Ssn)
                    .AsNumericOnly()
                    .RequireValidSsn();

            public static Field Ssn4(IDictionary<string, MatchSetConfigurationRegexEnum> configuration) => new Field<string>(GuarantorSsn4, configuration, Guarantor.SourceEnum, FieldPropertyEnum.Ssn4)
                    .AsSsn4()
                    .RequireValidSsn4();

            public static Field DOB(IDictionary<string, MatchSetConfigurationRegexEnum> configuration) => new Field<DateTime?>(GuarantorDOB, configuration, Guarantor.SourceEnum, FieldPropertyEnum.DOB)
                    .AsDate();

            public static Field FirstName(IDictionary<string, MatchSetConfigurationRegexEnum> configuration) => new Field<string>(GuarantorFirstName, configuration, Guarantor.SourceEnum, FieldPropertyEnum.FirstName);

            public static Field LastName(IDictionary<string, MatchSetConfigurationRegexEnum> configuration) => new Field<string>(GuarantorLastName, configuration, Guarantor.SourceEnum, FieldPropertyEnum.LastName);

            public static Field Address1(IDictionary<string, MatchSetConfigurationRegexEnum> configuration) => new Field<string>(GuarantorAddress1, configuration, Guarantor.SourceEnum, FieldPropertyEnum.AddressLine1)
                    .AsNormalizedAddress()
                    .RequireValidAddress();

            public static Field Address2(IDictionary<string, MatchSetConfigurationRegexEnum> configuration) => new Field<string>(GuarantorAddress2, configuration, Guarantor.SourceEnum, FieldPropertyEnum.AddressLine2)
                    .AsNormalizedAddress();

            public static Field City(IDictionary<string, MatchSetConfigurationRegexEnum> configuration) => new Field<string>(GuarantorCity, configuration, Guarantor.SourceEnum, FieldPropertyEnum.City)
                    .AsAlphaOnly()
                    .WithMinimumLength(3);

            public static Field State(IDictionary<string, MatchSetConfigurationRegexEnum> configuration) => new Field<string>(GuarantorState, configuration, Guarantor.SourceEnum, FieldPropertyEnum.StateProvince);

            public static Field Zip(IDictionary<string, MatchSetConfigurationRegexEnum> configuration) => new Field<string>(GuarantorZip, configuration, Guarantor.SourceEnum, FieldPropertyEnum.PostalCode)
                    .AsPostalCodeShort();


            #region Getters
            private static string GuarantorSourceSystemKey(MatchData matchData) => matchData.Guarantor.SourceSystemKey;
            private static string GuarantorSsn(MatchData matchData) => matchData.Guarantor.SSN;
            private static string GuarantorSsn4(MatchData matchData) => matchData.Guarantor.SSN4;
            private static DateTime? GuarantorDOB(MatchData matchData) => matchData.Guarantor.DOB;
            private static string GuarantorFirstName(MatchData matchData) => matchData.Guarantor.FirstName;
            private static string GuarantorLastName(MatchData matchData) => matchData.Guarantor.LastName;
            private static string GuarantorAddress1(MatchData matchData) => matchData.Guarantor.AddressLine1;
            private static string GuarantorAddress2(MatchData matchData) => matchData.Guarantor.AddressLine2;
            private static string GuarantorCity(MatchData matchData) => matchData.Guarantor.City;
            private static string GuarantorState(MatchData matchData) => matchData.Guarantor.StateProvince;
            private static string GuarantorZip(MatchData matchData) => matchData.Guarantor.PostalCode;
            #endregion Getters

        }

        public static class Patient
        {
            private const FieldSourceEnum SourceEnum = FieldSourceEnum.Patient;

            public static Field DOB(IDictionary<string, MatchSetConfigurationRegexEnum> configuration) => new Field<DateTime?>(PatientDob, configuration, Patient.SourceEnum, FieldPropertyEnum.DOB).AsDate();

            public static Field StatementIdentifierId(IDictionary<string, MatchSetConfigurationRegexEnum> configuration) => new Field<int>(StatementIdentifierId, configuration, Patient.SourceEnum, FieldPropertyEnum.StatementIdentifierId);

            #region Getters
            private static DateTime? PatientDob(MatchData matchData) => matchData.Patient?.PatientDOB ?? matchData.Guarantor.DOB;
            private static int StatementIdentifierId(MatchData matchData) => matchData.Patient?.StatementIdentifierId ?? 1;
            #endregion Getters
        }

        public static Field<T> Format<T>(this Field<T> matchField, Func<T, string> formatter)
        {
            matchField.Formatter = formatter;
            return matchField;
        }

        public static Field<T> AsNormalizedAddress<T>(this Field<T> matchField)
        {
            matchField.WithNormalizer(AddressNormalizer);
            return matchField;
        }

        public static Field WithNormalizer(this Field matchField, INormalizer normalizer)
        {
            if (normalizer == null)
            {
                throw new ArgumentNullException(nameof(normalizer));
            }
            if (matchField.Normalizers == null)
            {
                matchField.Normalizers = new List<INormalizer>();
            }
            matchField.Normalizers.Add(normalizer);
            return matchField;
        }


        private static readonly Lazy<IDictionary<MatchSetConfigurationRegexEnum, Regex>> MatchRegexes = new Lazy<IDictionary<MatchSetConfigurationRegexEnum, Regex>>(() =>
        new Dictionary<MatchSetConfigurationRegexEnum, Regex>
        {
            [MatchSetConfigurationRegexEnum.RemoveMinistryPrefix] = new Regex("(?<=-)(.*)")
        });

        public static Field WithRegex(this Field matchField, MatchSetConfigurationRegexEnum regexEnum)
        {
            if (matchField.Regexes == null)
            {
                matchField.Regexes = new List<Regex>();
            }

            if (MatchRegexes.Value.ContainsKey(regexEnum))
            {
                matchField.Regexes.Add(MatchRegexes.Value[regexEnum]);
            }
            else
            {
                throw new ArgumentException($"unexpected {nameof(MatchSetConfigurationRegexEnum)} value: {regexEnum.ToString()}", nameof(regexEnum));
            }

            return matchField;
        }

        private static Field<string> WithNoWhitespace(this Field<string> matchField)
        {
            matchField.Format(x => string.Join(string.Empty, x.Where(c => !char.IsWhiteSpace(c))));
            return matchField;
        }

        private static Field<DateTime> AsDate(this Field<DateTime> matchField)
        {
            matchField.Format(x => x.ToString("yyyyMMdd"));
            return matchField;
        }

        private static Field<DateTime?> AsDate(this Field<DateTime?> matchField)
        {
            matchField.Format(x => x.HasValue ? x.Value.ToString("yyyyMMdd") : string.Empty);
            return matchField;
        }

        private static Field<int> AsString(this Field<int> matchField)
        {
            matchField.Format(x => x.ToString());
            return matchField;
        }

        private static Field<string> AsNumericOnly(this Field<string> matchField)
        {
            matchField.Format(x => x.NumericOnly());
            return matchField;
        }

        private static Field<string> AsAlphaOnly(this Field<string> matchField)
        {
            matchField.Format(x => Regex.Replace(x, "[^a-zA-Z -]", string.Empty));
            return matchField;
        }

        private static Field<string> AsAlphaNumericOnly(this Field<string> matchField)
        {
            matchField.Format(x => Regex.Replace(x, "[^a-zA-Z0-9 -]", string.Empty));
            return matchField;
        }

        private static Field<string> RequireValidAddress(this Field<string> matchField)
        {
            matchField.WithMinimumLength(4);
            matchField.OutputValidators.Add(new AddressValidator());

            return matchField;
        }

        private static Field<T> WithMinimumLength<T>(this Field<T> matchField, int length = 1)
        {
            matchField.OutputValidators.Add(new MinimumLengthValidator(length));

            return matchField;
        }

        private static Field<DateTime?> RequireValidDob(this Field<DateTime?> matchField)
        {
            matchField.InputValidators.Add(new DobValidator());

            return matchField;
        }

        private static Field<string> RequireValidSsn(this Field<string> matchField)
        {
            matchField.OutputValidators.Add(new SsnValidator());

            return matchField;
        }

        private static Field<string> RequireValidSsn4(this Field<string> matchField)
        {
            matchField.OutputValidators.Add(new Ssn4Validator());

            return matchField;
        }

        private static Field<string> AsPostalCodeShort(this Field<string> matchField)
        {
            matchField.Format(x => x.Substring(0, 5).NumericOnly());
            return matchField;
        }

        private static Field<string> AsSsn4(this Field<string> matchField)
        {
            matchField.Format(x => x.PadLeft(4,'0').NumericOnly());
            return matchField;
        }

        private static readonly Regex NumericOnlyRegex = new Regex("[^0-9]");
        private static string NumericOnly(this string input)
        {
            return NumericOnlyRegex.Replace(input, string.Empty);
        }
    }
}
﻿namespace Ivh.Domain.Matching.Match
{
    using System.Collections.Generic;
    using System.Linq;
    using Application.Matching.Common.Dtos;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;
    using Entities;
    using Newtonsoft.Json;

    public class Set
    {
        public readonly IDictionary<string, MatchSetConfigurationRegexEnum> Configuration;

        [JsonIgnore]
        public IList<Pattern> Patterns { get; }
        public PatternSetEnum PatternSetEnum { get; }
        public decimal Priority { get; }
        public IList<ApplicationEnum> Applications { get; }

        [JsonConstructor]
        public Set(
            PatternSetEnum patternSetEnum,
            decimal priority,
            IList<ApplicationEnum> applications,
            IDictionary<string, MatchSetConfigurationRegexEnum> configuration,
            params Pattern[] patterns)
        {
            this.Patterns = patterns;
            this.Configuration = configuration;
            this.PatternSetEnum = patternSetEnum;
            this.Priority = priority;
            this.Applications = applications;
        }

        public Set()
        {
        }

        /// <summary>
        /// Gets the required field names by pattern use.
        /// </summary>
        /// <param name="patternUseEnum"></param>
        /// <param name="fieldSource"></param>
        /// <returns>a list of full field names (eg. Guarantor.FirstName), or the property used if a source is defined (eg. FirstName)</returns>
        public IEnumerable<string> GetRequiredFields(PatternUseEnum? patternUseEnum = null, FieldSourceEnum? fieldSource = null)
        {
            IEnumerable<Pattern> patterns = this.Patterns;
            if (patternUseEnum.HasValue)
            {
                patterns = patterns.Where(x => x.PatternUse == patternUseEnum);
            }

            IEnumerable<Field> requiredFields = patterns.SelectMany(p => p.MatchFields);
            
            IEnumerable<string> requiredFieldNames = fieldSource.HasValue
                ? requiredFields.Where(x => x.Source == fieldSource).Select(mf => mf.Property.ToString())
                : requiredFields.Select(mf => mf.FieldName);
            
            return requiredFieldNames.Distinct();
        }

        public bool IsFieldRequired(PatternUseEnum patternUseEnum, FieldSourceEnum fieldSource, FieldPropertyEnum propertyEnum)
        {
            IEnumerable<Pattern> patterns = this.Patterns.Where(x => x.PatternUse == patternUseEnum);

            bool fieldRequired = patterns.All(m =>
                m.MatchFields
                    .Where(x => x.Source == fieldSource)
                    .Where(x => x.Property == propertyEnum)
                    .IsNotNullOrEmpty());

            return fieldRequired;
        }


        private IEnumerable<MatchRegistryDto> GetMatchResults(MatchDataGroup matchDataGroup, IEnumerable<Pattern> patterns)
        {
            IList<MatchData> matchDatas = matchDataGroup.ToMatchDatas().ToList();

            foreach (Pattern pattern in patterns)
            {
                foreach (MatchData matchData in matchDatas)
                {
                    MatchRegistryDto result = pattern.GetMatchResult(matchData);
                    result.PatternSetId = this.PatternSetEnum;
                    yield return result;
                }
            }
        }

        /// <summary>
        /// Generates MatchRegistryDtos for the given match data
        /// </summary>
        /// <param name="matchDataGroup"></param>
        /// <param name="patternUse">optional pattern use filter</param>
        /// <returns></returns>
        public IEnumerable<MatchRegistryDto> GetMatchResults(MatchDataGroup matchDataGroup, PatternUseEnum? patternUse)
        {
            IEnumerable<Pattern> patterns = patternUse.HasValue
                ? this.Patterns.Where(x => x.PatternUse == patternUse.Value)
                : this.Patterns;

            return this.GetMatchResults(matchDataGroup, patterns);
        }
    }
}
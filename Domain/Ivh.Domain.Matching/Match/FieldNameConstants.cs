﻿namespace Ivh.Domain.Matching.Match
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public enum FieldSourceEnum
    {
        Guarantor,
        Patient
    }

    public enum FieldPropertyEnum
    {
        SourceSystemKey,
        Ssn,
        Ssn4,
        DOB,
        FirstName,
        LastName,
        AddressLine1,
        AddressLine2,
        City,
        StateProvince,
        PostalCode,
        StatementIdentifierId
    }

    /// <summary>
    /// a class to get consistant matching field names. changing existing constants will change the match strings generated (bad)
    /// </summary>
    public static class FieldNameConstants
    {
        private static class FieldSourceNames
        {
            public const string GuarantorSource = "Guarantor";
            public const string PatientSource = "Patient";
        }

        private static class FieldPropertyNames
        {
            public const string SourceSystemKey = "SourceSystemKey";
            public const string Ssn = "Ssn";
            public const string Ssn4 = "Ssn4";
            public const string DOB = "DOB";
            public const string FirstName = "FirstName";
            public const string LastName = "LastName";
            public const string AddressLine1 = "AddressLine1";
            public const string AddressLine2 = "AddressLine2";
            public const string City = "City";
            public const string StateProvince = "StateProvince";
            public const string PostalCode = "PostalCode";
            public const string StatementIdentifierId = "StatementIdentifierId";
        }

        private static readonly Dictionary<FieldSourceEnum, string> FieldSources = new Dictionary<FieldSourceEnum, string>()
        {
            [FieldSourceEnum.Guarantor] = FieldSourceNames.GuarantorSource,
            [FieldSourceEnum.Patient] = FieldSourceNames.PatientSource,
        };

        private static readonly Dictionary<FieldPropertyEnum, string> FieldProperties = new Dictionary<FieldPropertyEnum, string>()
        {
            [FieldPropertyEnum.SourceSystemKey] = FieldPropertyNames.SourceSystemKey,
            [FieldPropertyEnum.Ssn] = FieldPropertyNames.Ssn,
            [FieldPropertyEnum.Ssn4] = FieldPropertyNames.Ssn4,
            [FieldPropertyEnum.DOB] = FieldPropertyNames.DOB,
            [FieldPropertyEnum.FirstName] = FieldPropertyNames.FirstName,
            [FieldPropertyEnum.LastName] = FieldPropertyNames.LastName,
            [FieldPropertyEnum.AddressLine1] = FieldPropertyNames.AddressLine1,
            [FieldPropertyEnum.AddressLine2] = FieldPropertyNames.AddressLine2,
            [FieldPropertyEnum.City] = FieldPropertyNames.City,
            [FieldPropertyEnum.StateProvince] = FieldPropertyNames.StateProvince,
            [FieldPropertyEnum.PostalCode] = FieldPropertyNames.PostalCode,
            [FieldPropertyEnum.StatementIdentifierId] = FieldPropertyNames.StatementIdentifierId,
        };

        private static readonly Lazy<Dictionary<FieldSourceEnum, Dictionary<FieldPropertyEnum, string>>> FieldNames = new Lazy<Dictionary<FieldSourceEnum,Dictionary<FieldPropertyEnum, string>>>(() =>
        {
            Dictionary<FieldSourceEnum, Dictionary<FieldPropertyEnum, string>> dictionary = new Dictionary<FieldSourceEnum, Dictionary< FieldPropertyEnum, string>>();

            foreach (KeyValuePair<FieldSourceEnum,string> fieldSource in FieldSources)
            {
                Dictionary<FieldPropertyEnum, string> propertyDictionary = new Dictionary<FieldPropertyEnum, string>();
                dictionary.Add(fieldSource.Key, propertyDictionary);

                foreach (KeyValuePair<FieldPropertyEnum,string> fieldProperty in FieldProperties)
                {
                    propertyDictionary.Add(fieldProperty.Key, GetFullFieldNameInternal(fieldSource.Key, fieldProperty.Key));
                }
            }

            return dictionary;
        });

        public static string GetFullFieldName(FieldSourceEnum fieldSource, FieldPropertyEnum fieldProperty)
        {
            return FieldNames.Value[fieldSource][fieldProperty];
        }

        private static string GetFullFieldNameInternal(FieldSourceEnum fieldSource, FieldPropertyEnum fieldProperty)
        {
            string source = FieldSources[fieldSource];
            string property = FieldProperties[fieldProperty];

            StringBuilder sb = new StringBuilder(source.Length + property.Length + 1);

            sb.Append(source);
            sb.Append('.');
            sb.Append(property);

            return sb.ToString();
        }
    }
}

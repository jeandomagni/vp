﻿namespace Ivh.Domain.Matching.Match
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq.Expressions;
    using System.Runtime.CompilerServices;
    using System.Text.RegularExpressions;
    using Common.Base.Exceptions;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;
    using Validators;

    public abstract class Field
    {
        public string FieldName { get; protected set; }

        public FieldSourceEnum Source { get; protected set; }
        public FieldPropertyEnum Property { get; protected set; }

        internal IList<Regex> Regexes = null;
        internal IList<INormalizer> Normalizers = null;

        protected Field()
        {
        }

        public virtual bool IsValid(MatchData entity)
        {
            string value = this.GetValue(entity);

            if (value == null)
            {
                return false;
            }

            return true;
        }

        public string GetValue(MatchData entity)
        {
            string value = this.GetValueInternal(entity);
            if (value != null)
            {
                //https://docs.microsoft.com/en-us/visualstudio/code-quality/ca1308-normalize-strings-to-uppercase?view=vs-2015
                return $"{this.FieldName}={value}".ToUpper();
            }
            return null;
        }

        protected abstract string GetValueInternal(MatchData entity);
    }

    public class Field<T> : Field
    {
        protected string FieldRegexConfigurationKey => $"Matching.{nameof(Field)}.{this.FieldName}.{nameof(Regex)}";
        internal IList<IInputValidator<T>> InputValidators = null;
        internal IList<IOutputValidator> OutputValidators = null;

        private readonly Func<MatchData, T> _getter = null;
        public Func<T, string> Formatter = null;

        private Field()
        {
            this.OutputValidators = new List<IOutputValidator>();
            this.InputValidators = new List<IInputValidator<T>>();
        }

        /*
        internal Field(Expression<Func<MatchData, T>> expression, IDictionary<string, MatchSetConfigurationRegexEnum> configuration) : this()
        {
            this.FieldName = this.GetName(expression.Body);
            this._getter = expression.Compile();

            if (configuration != null && configuration.ContainsKey(this.FieldRegexConfigurationKey))
            {
                this.WithRegex(configuration[this.FieldRegexConfigurationKey]);
            }
        }
        */

        internal Field(Func<MatchData, T> getter, IDictionary<string, MatchSetConfigurationRegexEnum> configuration, FieldSourceEnum fieldSourceEnum, FieldPropertyEnum fieldPropertyEnum) : this()
        {
            //prefer to use constants
            this.Source = fieldSourceEnum;
            this.Property = fieldPropertyEnum;
            this.FieldName = FieldNameConstants.GetFullFieldName(fieldSourceEnum, fieldPropertyEnum);
            this._getter = getter;

            if (configuration != null && configuration.ContainsKey(this.FieldRegexConfigurationKey))
            {
                this.WithRegex(configuration[this.FieldRegexConfigurationKey]);
            }
        }

        public override bool IsValid(MatchData entity)
        {
            if (this.InputValidators?.Count > 0)
            {
                T inValue = this._getter(entity);
                foreach (IInputValidator<T> inputValidator in this.InputValidators)
                {
                    if (!inputValidator.IsValid(inValue))
                    {
                        return false;
                    }
                }
            }

            string outValue = this.GetValueInternal(entity);

            if (outValue == null)
            {
                return false;
            }

            bool valid = base.IsValid(entity);

            if (this.OutputValidators?.Count > 0)
            {
                foreach (IOutputValidator validator in this.OutputValidators)
                {
                    if (!validator.IsValid(outValue))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        protected override string GetValueInternal(MatchData entity)
        {
            T value = this._getter(entity);
            if (!EqualityComparer<T>.Default.Equals(value, default(T)))
            {
                string returnValue = default(string);
                if (this.Formatter != null)
                {
                    returnValue = this.Formatter(value);
                }
                else
                {
                    returnValue = TypeDescriptor.GetConverter(typeof(T)).ConvertToString(value);
                }

                if (returnValue != null)
                {
                    // apply regexes
                    if (this.Regexes?.Count > 0)
                    {
                        foreach (Regex regex in this.Regexes)
                        {
                            Match match = regex.Match(returnValue);
                            if (match.Success)
                            {
                                returnValue = match.Value;
                            }
                        }
                    }
                }

                if (returnValue != null)
                {
                    // apply Normalizers
                    if (this.Normalizers?.Count > 0)
                    {
                        foreach (INormalizer normalizer in this.Normalizers)
                        {
                            returnValue = normalizer.Normalize(returnValue);
                        }
                    }
                }

                return returnValue;
            }
            
            return null;
        }

        private string GetName(Expression expression)
        {
            if (expression == null)
            {
                throw new ArgumentException();
            }

            if (expression is MemberExpression memberExpression)
            {
                // Reference type property or field
                return $"{memberExpression.Expression.Type.Name}.{memberExpression.Member.Name}";
            }

            if (expression is MethodCallExpression methodCallExpression)
            {
                // Reference type method
                return methodCallExpression.Method.Name;
            }

            if (expression is UnaryExpression unaryExpression)
            {
                // Property, field of method returning value type
                if (unaryExpression.Operand is MethodCallExpression methodExpression)
                {
                    return methodExpression.Method.Name;
                }

                return ((MemberExpression)unaryExpression.Operand).Member.Name;
            }

            throw new ArgumentException();
        }


    }
}
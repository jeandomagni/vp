﻿namespace Ivh.Domain.Matching.Validators
{
    using System;
    using System.Text.RegularExpressions;
    using Common.Base.Utilities.Extensions;
    using Interfaces;

    public class MinimumLengthValidator : IOutputValidator
    {
        private readonly int _minLength = 1;

        public MinimumLengthValidator(int minLength = 1)
        {
            this._minLength = minLength;
        }

        public bool IsValid(string output)
        {
            return output.Length >= _minLength;
        }
    }

    public class AddressValidator : IOutputValidator
    {
        private readonly string[] _invalidAddresses = 
        {
            "UNKNOWN",
            "UNK",
            "HOMELESS",
            "NONE",
            "NONEGIVEN"
        };

        public bool IsValid(string output)
        {
            foreach (string invalidAddress in this._invalidAddresses)
            {
                if(output.Equals(invalidAddress, StringComparison.OrdinalIgnoreCase))
                {
                    return false;
                }
            }
            return true;
        }
    }

    public class DobValidator : IInputValidator<DateTime?>
    {
        public bool IsValid(DateTime? input)
        {
            return input.HasValue && input.Value.Date.IsBetweenInclusive(DateTime.UtcNow.Date.AddYears(-110), DateTime.UtcNow.Date.AddYears(-18));
        }
    }


    public class SsnValidator : IOutputValidator
    {
        private static readonly string[] invalidSsnConstants =
        {
            "002281852",
            "042103580",
            "062360749",
            "078051120",
            "095073645",
            "111111111",
            "123456789",
            "128036045",
            "135016629",
            "141186941",
            "165167999",
            "165187999",
            "165207999",
            "165227999",
            "165247999",
            "189092294",
            "212097694",
            "212099999",
            "222222222",
            "306302348",
            "308125070",
            "333333333",
            "444444444",
            "468288779",
            "549241889",
            "555555555",
            "777777777",
            "888888888",
            "999999999"
        };

        private static readonly Regex[] invalidSsnPatterns =
        {
            new Regex("^[0-9]{5}0000$"),
            new Regex("^[0-9]{3}00[0-9]{4}$"),
            new Regex("^000[0-9]{6}$"),
            new Regex("^666[0-9]{6}$"),
            new Regex("^9[0-9][0123456][0-9]{6}$"),
            
        };

        public bool IsValid(string output)
        {
            if(output == null)
            {
                return false;
            }

            if(output.Length != 9)
            {
                return false;
            }

            foreach (string invalidSsn in invalidSsnConstants)
            {
                if (output.Equals(invalidSsn))
                {
                    return false;
                }
            }

            foreach (Regex regex in invalidSsnPatterns)
            {
                Match match = regex.Match(output);
                if (match.Success)
                {
                    return false;
                }
            }

            return true;
        }

        public bool IsValidNormalized(string val)
        {
            if (val == null)
            {
                return false;
            }

            val = Regex.Replace(val, "[^0-9]", string.Empty);

            return this.IsValid(val);
        }
    }

    public class Ssn4Validator : IOutputValidator
    {
        private static readonly string[] invalidSsn4Patterns =
        {
            "0000"
        };

        public bool IsValid(string output)
        {
            foreach (string regex in invalidSsn4Patterns)
            {
                if (output.Equals(regex))
                {
                    return false;
                }
            }

            return true;
        }
    }
}

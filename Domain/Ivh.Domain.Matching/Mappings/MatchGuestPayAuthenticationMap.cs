﻿namespace Ivh.Domain.Matching.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class MatchGuestPayAuthenticationMap : ClassMap<Facility>
    {
        public MatchGuestPayAuthenticationMap()
        {
            this.ReadOnly();
            this.Schema(Schemas.Cdi.GuestPay);
            this.Table("Guarantor_Authentication");
            this.CompositeId()
                .KeyProperty(x => x.HsGuarantorId)
                .KeyProperty(x => x.ServiceGroupId)
                .KeyProperty(x => x.InsertLoadTrackerId);

            this.Map(x => x.StatementIdentifierId);
        }
    }
}

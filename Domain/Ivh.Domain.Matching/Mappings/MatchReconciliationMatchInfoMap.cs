﻿namespace Ivh.Domain.Matching.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class MatchReconciliationMatchInfoMap : ClassMap<MatchReconciliationMatchInfo>
    {
        public MatchReconciliationMatchInfoMap()
        {
            this.ReadOnly();
            this.Schema("vp");
            this.Table("VpGuarantorMatchInfo");
            this.Id(x => x.VpGuarantorId);

            this.Map(x => x.SSN);
            this.Map(x => x.UnderlyingMatchSSN);
            this.Map(x => x.DOB);

            this.HasMany(x => x.HsGuarantorMatches)
                .KeyColumn("VpGuarantorId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50);
        }
    }
}

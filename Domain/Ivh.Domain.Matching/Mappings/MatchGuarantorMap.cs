﻿namespace Ivh.Domain.Matching.Mappings
{
    using Common.Base.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class MatchGuarantorMap : ClassMap<Guarantor>
    {
        public MatchGuarantorMap()
        {
            this.Schema(NhibernateConstants.SchemaNames.Base);
            this.Table("HsGuarantor");
            this.ReadOnly();
            this.Id(x => x.HsGuarantorId).Column("HsGuarantorID");
            this.Map(x => x.HsBillingSystemId).Column("HsBillingSystemID").Not.Nullable();
            this.Map(x => x.SourceSystemKey).Not.Nullable();
            this.Map(x => x.FirstName);
            this.Map(x => x.LastName);
            this.Map(x => x.DOB);
            this.Map(x => x.SSN);
            this.Map(x => x.SSN4);
            this.Map(x => x.AddressLine1);
            this.Map(x => x.AddressLine2);
            this.Map(x => x.City);
            this.Map(x => x.StateProvince);
            this.Map(x => x.PostalCode);
            this.Map(x => x.DataChangeDate).Nullable();
        }
    }
}

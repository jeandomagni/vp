﻿namespace Ivh.Domain.Matching.Mappings
{
    using Application.Matching.Common.Dtos;
    using AutoMapper;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using Common.VisitPay.Messages.Matching.Dtos;
    using Entities;

    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            // Automapper mapping go here.
            // Use this.CreateMap<,>().
            // DO NOT USE Mapper.CreateMap<,>().
            this.CreateMapBidirectional<Patient, MatchPatientDto>();
            this.CreateMapBidirectional<Guarantor, MatchGuarantorDto>();
            this.CreateMapBidirectional<Facility, MatchGuestPayAuthenticationDto>();
            this.CreateMapBidirectional<MatchRegistry, MatchRegistryDto>();
        }
    }
}

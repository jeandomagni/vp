﻿namespace Ivh.Domain.Matching.Mappings
{
    using Common.Base.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class MatchVpGuarantorMatchInfoMap : ClassMap<MatchVpGuarantorMatchInfo>
    {
        public MatchVpGuarantorMatchInfoMap()
        {
            this.Schema(NhibernateConstants.SchemaNames.Match);
            this.Table("VpGuarantorMatchInfo");

            this.Id(x => x.VpGuarantorId).GeneratedBy.Assigned();

            this.Map(x => x.InsertDate).Not.Nullable();
            this.Map(x => x.MatchSsn).Nullable();
            this.Map(x => x.MatchGuarantorDob).Nullable();

            this.HasMany(x => x.Matches)
                .KeyColumn("VpGuarantorId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();
        }
    }
}

﻿namespace Ivh.Domain.Matching.Mappings
{
    using Common.Base.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class MatchPatientMap : ClassMap<Patient>
    {
        public MatchPatientMap()
        {
            this.Schema(NhibernateConstants.SchemaNames.Base);
            this.Table("Visit");
            this.ReadOnly();
            this.Id(x => x.VisitId);
            this.Map(x => x.PatientDOB);
            this.Map(x => x.HsGuarantorId);
            this.Map(x => x.DataChangeDate);
            this.Map(x => x.ServiceGroupId);
            this.Map(x => x.StatementIdentifierId);
        }
    }
}

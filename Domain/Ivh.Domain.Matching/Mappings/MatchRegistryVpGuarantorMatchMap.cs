﻿namespace Ivh.Domain.Matching.Mappings
{
    using Common.Base.Constants;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class MatchRegistryVpGuarantorMatchMap : ClassMap<MatchRegistryVpGuarantorMatch>
    {
        public MatchRegistryVpGuarantorMatchMap()
        {
            this.Schema(NhibernateConstants.SchemaNames.Match);
            this.Table("RegistryVpGuarantorMatch");

            this.CompositeId()
                .KeyReference(x => x.VpGuarantor, "VpGuarantorId")
                .KeyReference(x => x.Registry, "RegistryId")
                .KeyProperty(x => x.HsGuarantorId);

            this.Map(x => x.HsGuarantorMatchStatusId).CustomType<HsGuarantorMatchStatusEnum>();
            this.Map(x => x.InsertDate).Not.Nullable();
        }
    }
}

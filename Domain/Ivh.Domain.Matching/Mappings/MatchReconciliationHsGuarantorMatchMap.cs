﻿namespace Ivh.Domain.Matching.Mappings
{
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class MatchReconciliationHsGuarantorMatchMap : ClassMap<MatchReconciliationHsGuarantorMatch>
    {
        public MatchReconciliationHsGuarantorMatchMap()
        {
            this.ReadOnly();
            this.Schema("vp");
            this.Table("VpGuarantorHsMatch");
            this.Id(x => x.VpGuarantorHsMatchId);

            this.Map(x => x.HsGuarantorId);
            this.Map(x => x.MatchedOn);
            this.Map(x => x.MatchOptionId).CustomType<MatchOptionEnum>().Nullable();
            this.Map(x => x.VpGuarantorId);
            this.Map(x => x.HsGuarantorMatchStatusId).CustomType<HsGuarantorMatchStatusEnum>();
        }
    }
}

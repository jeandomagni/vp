﻿namespace Ivh.Domain.Matching.Mappings
{
    using Common.Base.Constants;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class MatchRegistryMap : ClassMap<MatchRegistry>
    {
        public MatchRegistryMap()
        {
            this.Schema(NhibernateConstants.SchemaNames.Match);
            this.Table("Registry");
            this.Id(x => x.RegistryId);
            this.Map(x => x.BillingSystemId).Not.Nullable();
            this.Map(x => x.DataChangeDate).Not.Nullable();
            this.Map(x => x.InsertDate).Not.Nullable();
            this.Map(x => x.DeletedDate).Nullable();
            this.Map(x => x.MatchString).Not.Nullable();
            this.Map(x => x.PartitionKey).Not.Nullable();
            this.Map(x => x.MatchOptionId).CustomType<MatchOptionEnum>().Not.Nullable();
            this.Map(x => x.PatternSetId).CustomType<PatternSetEnum>().Not.Nullable();
            this.Map(x => x.PatternUseId).CustomType<PatternUseEnum>().Not.Nullable();
            this.Map(x => x.SourceSystemKey).Not.Nullable();
            this.Map(x => x.HsGuarantorId).Not.Nullable();
            this.Map(x => x.BaseVisitId).Nullable();
        }
    }
}

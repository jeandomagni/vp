﻿namespace Ivh.Domain.Content.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Application.Content.Common.Dtos;
    using AutoMapper;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.VisitPay.Enums;
    using Content.Entities;
    using Interfaces;
    using Settings.Interfaces;

    public class ContentService : IContentService
    {
        private readonly Lazy<ISettingsProvider> _settingsProvider;
        private readonly Lazy<IContentProvider> _contentProvider;

        public ContentService(Lazy<IContentProvider> contentProvider,
            Lazy<ISettingsProvider> settingsProvider)
        {
            this._contentProvider = contentProvider;
            this._settingsProvider = settingsProvider;
        }

        /// <summary>
        /// Should only be called by the ClientDataApi
        /// </summary>
        public CmsVersion ApiGetCurrentVersion(CmsRegionEnum cmsRegionEnum, bool replaceTokens, string locale, IDictionary<string, string> additionalValues = null)
        {
            return this.ApiGetVersion(cmsRegionEnum, replaceTokens, locale, additionalValues);
        }

        public async Task<CmsVersion> GetCurrentVersionAsync(CmsRegionEnum cmsRegionEnum, bool replaceTokens, IDictionary<string, string> additionalValues = null)
        {
            return await this.GetVersionAsync(cmsRegionEnum, replaceTokens, additionalValues);
        }

        /// <summary>
        /// Should only be called by the ClientDataApi
        /// </summary>
        public CmsVersion ApiGetSpecificVersion(CmsRegionEnum cmsRegionEnum, int cmsVersionId, bool replaceTokens, string locale, IDictionary<string, string> additionalValues = null)
        {
            return this.ApiGetVersion(cmsRegionEnum, replaceTokens, locale, additionalValues, cmsVersionId);
        }

        public async Task<CmsVersion> GetSpecificVersionAsync(CmsRegionEnum cmsRegionEnum, int cmsVersionId, bool replaceTokens, IDictionary<string, string> additionalValues = null)
        {
            return await this.GetVersionAsync(cmsRegionEnum, replaceTokens, additionalValues, cmsVersionId);
        }

        public async Task<CmsVersion> GetLabelAsync(string cmsRegionName, string defaultLabel)
        {
            CmsVersion cmsVersion = await this._contentProvider.Value.GetVersionByRegionNameAndTypeAsync(cmsRegionName, CmsTypeEnum.WebsiteLabel, defaultLabel);

            return await this.ReplaceTokensAsync(cmsVersion);
        }

        public async Task<ContentMenu> GetContentMenuAsync(ContentMenuEnum contentMenuEnum)
        {
            return await this._contentProvider.Value.GetContentMenuAsync(contentMenuEnum);
        }
        
        private static KeyValuePair<string, IDictionary<string, string>>? _replacementValuesByHash;

        public IDictionary<string, string> ClientReplacementValues()
        {
            IReadOnlyDictionary<string, string> replacementValues = this._settingsProvider.Value.GetAllSettings();
            string hashCode =this._settingsProvider.Value.SettingsHashCode ?? string.Empty;

            if (_replacementValuesByHash.HasValue && _replacementValuesByHash.Value.Key == hashCode)
            {
                return _replacementValuesByHash.Value.Value;
            }
            
            Dictionary<string, string> newDictionary = replacementValues.ToDictionary(kvp => $"[[{kvp.Key}]]", kvp => kvp.Value);
            _replacementValuesByHash = new KeyValuePair<string,IDictionary<string, string>>(hashCode, newDictionary);

            return newDictionary;
        }

        /// <summary>
        /// Should only be called by the ClientDataApi
        /// </summary>
        private CmsVersion ApiGetVersion(CmsRegionEnum cmsRegionEnum, bool replaceTokens, string locale, IDictionary<string, string> additionalValues = null, int? versionId = null)
        {
            CmsVersion version = this._contentProvider.Value.ApiGetVersion(cmsRegionEnum, locale, versionId);
            return this.ReplaceValuesForCmsVersion(version, replaceTokens, additionalValues, versionId).Result;
        }
        
        private async Task<CmsVersion> GetVersionAsync(CmsRegionEnum cmsRegionEnum, bool replaceTokens, IDictionary<string, string> additionalValues = null, int? versionId = null)
        {
            CmsVersion version = await this._contentProvider.Value.GetVersionAsync(cmsRegionEnum, versionId);
            return await this.ReplaceValuesForCmsVersion(version, replaceTokens, additionalValues, versionId);
        }

        private async Task<CmsVersion> ReplaceValuesForCmsVersion(CmsVersion version, bool replaceTokens, IDictionary<string, string> additionalValues, int? versionId)
        {
            if (version == null || version.CmsVersionId == 0 || version.CmsRegion == null)
            {
                return null;
            }

            if (version.CmsRegion.CmsType.IsInCategory(CmsTypeEnumCategory.ReplacementValues) && replaceTokens)
            {
                return await this.ReplaceTokensAsync(version, additionalValues);
            }

            // this is to break any reference to "cmsVersion" and prevent any replaced values from making their way back to the cache.
            // https://ivinci.atlassian.net/browse/VPNG-11061
            CmsVersionDto dto = Mapper.Map<CmsVersionDto>(version);
            return Mapper.Map<CmsVersion>(dto);
        }

        public async Task<CmsVersion> ReplaceTokensAsync(CmsVersion cmsVersion, IDictionary<string, string> additionalValues = null, bool htmlEncode = true)
        {
            // this mapping is to break any reference to "cmsVersion" and prevent any replaced values from making their way back to the cache
            // https://ivinci.atlassian.net/browse/VPNG-11061
            CmsVersionDto dto = Mapper.Map<CmsVersionDto>(cmsVersion);
            CmsVersion copy = Mapper.Map<CmsVersion>(dto);

            IDictionary<string, string> clientReplacementValues = this.ClientReplacementValues();

            copy.ContentBody = ReplacementUtility.Replace(copy.ContentBody, clientReplacementValues, additionalValues, htmlEncode);

            if (!string.IsNullOrEmpty(copy.ContentTitle))
            {
                copy.ContentTitle = ReplacementUtility.Replace(copy.ContentTitle, clientReplacementValues, additionalValues, htmlEncode);
            }

            await this.ReplaceLinksAsync(copy);

            return copy;
        }

        private static readonly IDictionary<CmsRegionEnum, string> CmsRegionLinkReplacementTags = EnumHelper<CmsRegionEnum>
                .GetValues(CmsRegionEnumCategory.LinkTemplate)
                .ToDictionary(x => x, x => $"[[{x.ToString()}]]");

        private async Task ReplaceLinksAsync(CmsVersion cmsVersion)
        {
            //shouldn't be links inside links, so quit
            if (CmsRegionLinkReplacementTags.ContainsKey((CmsRegionEnum)cmsVersion.CmsRegion.CmsRegionId))
            {
                return;
            }

            //get the cms for the links
            IDictionary<string, string> replacementDictionary = new Dictionary<string, string>();
            foreach (KeyValuePair<CmsRegionEnum, string> cmsRegionLinkReplacementTag in CmsRegionLinkReplacementTags)
            {
                CmsVersion linkCmsVersion = await this.GetCurrentVersionAsync(cmsRegionLinkReplacementTag.Key, true);
                if (linkCmsVersion != null)
                {
                    replacementDictionary.Add(cmsRegionLinkReplacementTag.Value, linkCmsVersion.ContentBody);
                }
            }

            //replace tags
            cmsVersion.ContentBody = ReplacementUtility.Replace(cmsVersion.ContentBody, replacementDictionary, null, false);

            if (!string.IsNullOrEmpty(cmsVersion.ContentTitle))
            {
                cmsVersion.ContentTitle = ReplacementUtility.Replace(cmsVersion.ContentTitle, replacementDictionary, null, false);
            }
        }
        
        public string GetTextRegion(string textRegion, IDictionary<string, string> additionalValues = null)
        {
            return this.GetTextRegionAsync(textRegion, additionalValues).Result;
        }
        
        public async Task<string> GetTextRegionAsync(string textRegion, IDictionary<string, string> additionalValues = null)
        {
            string textRegionString = await this._contentProvider.Value.GetTextRegionAsync(textRegion);

            IDictionary<string, string> clientReplacementValues = this.ClientReplacementValues();
            textRegionString = ReplacementUtility.Replace(textRegionString, clientReplacementValues, additionalValues, null);

            return textRegionString;
        }
        
        public async Task<IDictionary<string, string>> GetTextRegionsAsync(params string[] textRegions)
        {
            IDictionary<string, string> clientReplacementValues = this.ClientReplacementValues();
            IDictionary<string, string> textRegionStrings = new Dictionary<string, string>();

            foreach (string textRegion in textRegions)
            {
                string textRegionString = await this._contentProvider.Value.GetTextRegionAsync(textRegion);
                textRegionString = ReplacementUtility.Replace(textRegionString, clientReplacementValues, null, null);

                textRegionStrings.Add(textRegion, textRegionString);
            }

            return textRegionStrings;
        }

        /// <summary>
        /// Should only be called by the ClientDataApi
        /// </summary>
        public IDictionary<int, int> ApiGetCmsVersionNumbers(CmsRegionEnum cmsRegion, string locale)
        {
            return this._contentProvider.Value.ApiGetCmsVersionNumbers(cmsRegion, locale);
        }

        public async Task<IDictionary<int, int>> GetCmsVersionNumbersAsync(CmsRegionEnum cmsRegion)
        {
            return await this._contentProvider.Value.GetCmsVersionNumbersAsync(cmsRegion);
        }

        /// <summary>
        /// Should only be called by the ClientDataApi
        /// </summary>
        public CmsVersion ApiGetVersionByRegionNameAndType(string cmsRegionName, CmsTypeEnum cmsType, string defaultLabel, string locale)
        {
            return this._contentProvider.Value.ApiGetVersionByRegionNameAndType(cmsRegionName, cmsType, defaultLabel, locale);
        }

        public async Task<CmsVersion> GetVersionByRegionNameAndTypeAsync(string cmsRegionName, CmsTypeEnum cmsType, string defaultLabel)
        {
            return await this._contentProvider.Value.GetVersionByRegionNameAndTypeAsync(cmsRegionName, cmsType, defaultLabel);
        }
        
        public async Task<bool> InvalidateCacheAsync()
        {
            return await this._contentProvider.Value.InvalidateCacheAsync();
        }
    }
}
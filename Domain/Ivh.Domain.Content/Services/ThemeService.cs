﻿namespace Ivh.Domain.Content.Services
{
    using System;
    using Base.Interfaces;
    using Base.Services;
    using Interfaces;

    public class ThemeService :  IThemeService
    {
        private readonly Lazy<IThemeProvider> _themeProvider;

        public ThemeService(Lazy<IThemeProvider> themeProvider)
        {
            this._themeProvider = themeProvider;
        }

        public string GetStylesheet(string requestedFile)
        {
            return this._themeProvider.Value.GetStylesheet(requestedFile);
        }

        public string GetStylesheetHash(string requestedFile)
        {
            return this._themeProvider.Value.GetStylesheetHash(requestedFile);
        }
    }
}

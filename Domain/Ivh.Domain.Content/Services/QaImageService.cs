﻿namespace Ivh.Domain.Content.Services
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using Entities;
    using FileStorage.Entities;
    using FileStorage.Interfaces;
    using Interfaces;
    using MimeTypes;

    public class QaImageService : ImageService
    {
        private readonly Lazy<IFileStorageRepository> _fileStorageRepository;

        public QaImageService(
            Lazy<IImageProvider> imageProvider,
            Lazy<IFileStorageRepository> fileStorageRepository) : base(imageProvider)
        {
            this._fileStorageRepository = fileStorageRepository;
        }

        public override async Task<Image> GetImageAsync(string fileName)
        {
            // default implementation
            Image img = await base.GetImageAsync(fileName);
            if (img != null)
            {
                return img;
            }

            // look for anything in VpStorage.FileStored - assuming the images in question have a GUID for filenames
            string name = Path.GetFileNameWithoutExtension(fileName);
            if (!Guid.TryParse(name, out Guid guid) || guid == default(Guid))
            {
                // QaImages should be named with a Guid
                return null;
            }

            // go find it
            FileStored file = this._fileStorageRepository.Value.GetByKey(guid);
            if (file?.FileContents == null || file.FileContents.Length == 0)
            {
                return null;
            }

            return new Image
            {
                ImageBytes = file.FileContents,
                ContentType = MimeTypeMap.GetMimeType(Path.GetExtension(fileName)),
            };
        }
    }
}
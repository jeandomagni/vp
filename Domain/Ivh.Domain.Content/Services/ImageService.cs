﻿namespace Ivh.Domain.Content.Services
{
    using System;
    using System.Threading.Tasks;
    using Entities;
    using Interfaces;

    public class ImageService :  IImageService
    {
        private const string ExportLogoPath = "Content/Client/logo_for_excel_export.png";
        private readonly Lazy<IImageProvider> _imageProvider;

        public ImageService(Lazy<IImageProvider> imageProvider)
        {
            this._imageProvider = imageProvider;
        }
        
        public async Task<bool> InvalidateCacheAsync(string localCacheDirectory)
        {
            return await this._imageProvider.Value.InvalidateCacheAsync(localCacheDirectory);
        }

        public virtual async Task<Image> GetImageAsync(string fileName)
        {
            return await this._imageProvider.Value.GetImageAsync(fileName);
        }

        public async Task<System.Drawing.Image> GetExportLogoAsync()
        {
            Image imageEntity = await this._imageProvider.Value.GetImageAsync(ExportLogoPath);
            if (imageEntity == null)
            {
                return null;
            }

            return (System.Drawing.Bitmap) new System.Drawing.ImageConverter().ConvertFrom(imageEntity.ImageBytes);
        }
    }
}
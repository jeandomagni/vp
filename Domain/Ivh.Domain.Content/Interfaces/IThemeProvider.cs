﻿namespace Ivh.Domain.Content.Interfaces
{
    public interface IThemeProvider
    {
        string GetStylesheet(string requestedFile);
        string GetStylesheetHash(string requestedFile);
    }
}

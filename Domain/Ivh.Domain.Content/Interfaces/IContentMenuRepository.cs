﻿namespace Ivh.Domain.Content.Interfaces
{
    using Common.Base.Interfaces;
    using Content.Entities;

    public interface IContentMenuRepository : IRepository<ContentMenu>
    {
    }
}
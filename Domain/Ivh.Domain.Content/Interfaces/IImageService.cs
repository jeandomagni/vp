﻿namespace Ivh.Domain.Content.Interfaces
{
    using System.Threading.Tasks;
    using Common.Base.Interfaces;
    using Entities;

    public interface IImageService : IDomainService
    {
        Task<Image> GetImageAsync(string fileName);
        Task<bool> InvalidateCacheAsync(string localCacheDirectory);
        Task<System.Drawing.Image> GetExportLogoAsync();
    }
}

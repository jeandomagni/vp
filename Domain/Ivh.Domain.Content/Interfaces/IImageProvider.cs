﻿namespace Ivh.Domain.Content.Interfaces
{
    using System.Threading.Tasks;
    using Entities;

    public interface IImageProvider
    {
        Task<Image> GetImageAsync(string fileName);
        Task<bool> InvalidateCacheAsync(string localCacheDirectory);
    }
}
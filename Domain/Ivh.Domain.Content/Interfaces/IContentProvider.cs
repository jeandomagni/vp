﻿namespace Ivh.Domain.Content.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IContentProvider
    {
        Task<ContentMenu> GetContentMenuAsync(ContentMenuEnum contentMenuEnum);
        CmsVersion ApiGetVersion(CmsRegionEnum cmsRegionEnum, string locale, int? versionId = null);
        Task<CmsVersion> GetVersionAsync(CmsRegionEnum cmsRegionEnum, int? versionId = null);
        CmsVersion ApiGetVersionByRegionNameAndType(string cmsRegionName, CmsTypeEnum cmsType, string defaultLabel, string locale);
        Task<CmsVersion> GetVersionByRegionNameAndTypeAsync(string cmsRegionName, CmsTypeEnum cmsType, string defaultLabel);
        IDictionary<int, int> ApiGetCmsVersionNumbers(CmsRegionEnum cmsRegionEnum, string locale);
        Task<IDictionary<int, int>> GetCmsVersionNumbersAsync(CmsRegionEnum cmsRegionEnum);
        Task<string> GetTextRegionAsync(string textRegion);
        Task<bool> InvalidateCacheAsync();
    }
}
﻿namespace Ivh.Domain.Content.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface ICmsVersionRepository : IRepository<CmsVersion>
    {
        CmsVersion GetVersion(CmsRegionEnum cmsRegionEnum, string locale, int? versionId = null);
        CmsVersion GetVersionByRegionNameAndType(string regionName, string locale, CmsTypeEnum cmsType);
        IDictionary<int, int> GetCmsVersionNumbers(CmsRegionEnum cmsRegionEnum, string locale);
    }
}
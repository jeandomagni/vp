﻿namespace Ivh.Domain.Content.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IContentService : IDomainService
    {
        CmsVersion ApiGetCurrentVersion(CmsRegionEnum cmsRegionEnum, bool replaceTokens, string locale, IDictionary<string, string> additionalValues = null);
        Task<CmsVersion> GetCurrentVersionAsync(CmsRegionEnum cmsRegionEnum, bool replaceTokens, IDictionary<string, string> additionalValues = null);
        CmsVersion ApiGetSpecificVersion(CmsRegionEnum cmsRegionEnum, int cmsVersionId, bool replaceTokens, string locale, IDictionary<string, string> additionalValues = null);
        Task<CmsVersion> GetSpecificVersionAsync(CmsRegionEnum cmsRegionEnum, int cmsVersionId, bool replaceTokens, IDictionary<string, string> additionalValues = null);
        Task<CmsVersion> GetLabelAsync(string cmsRegionName, string defaultLabel);
        Task<ContentMenu> GetContentMenuAsync(ContentMenuEnum contentMenuEnum);
        IDictionary<int, int> ApiGetCmsVersionNumbers(CmsRegionEnum cmsRegion, string locale);
        Task<IDictionary<int, int>> GetCmsVersionNumbersAsync(CmsRegionEnum cmsRegion);
        //Task<CmsRegion> GetByNameAndTypeAsync(string cmsRegionName, CmsTypeEnum cmsType);
        CmsVersion ApiGetVersionByRegionNameAndType(string cmsRegionName, CmsTypeEnum cmsType, string defaultLabel, string locale);
        Task<CmsVersion> GetVersionByRegionNameAndTypeAsync(string cmsRegionName, CmsTypeEnum cmsType, string defaultLabel);
        IDictionary<string, string> ClientReplacementValues();
        Task<CmsVersion> ReplaceTokensAsync(CmsVersion cmsVersion, IDictionary<string, string> additionalValues = null, bool htmlEncode = true);
        string GetTextRegion(string textRegion, IDictionary<string, string> additionalValues = null);
        Task<string> GetTextRegionAsync(string textRegion, IDictionary<string, string> additionalValues = null);
        Task<IDictionary<string, string>> GetTextRegionsAsync(params string[] textRegions);
        Task<bool> InvalidateCacheAsync();
    }
}

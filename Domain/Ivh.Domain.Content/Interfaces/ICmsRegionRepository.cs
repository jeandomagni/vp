﻿namespace Ivh.Domain.Content.Interfaces
{
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Content.Entities;

    public interface ICmsRegionRepository : IRepository<CmsRegion>
    {
        CmsRegion GetByNameAndType(string cmsRegionName, CmsTypeEnum cmsType);
        int GetNextCmsRegionId();
    }
}
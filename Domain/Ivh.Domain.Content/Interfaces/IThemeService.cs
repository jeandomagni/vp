﻿namespace Ivh.Domain.Content.Interfaces
{
    using Common.Base.Interfaces;

    public interface IThemeService : IDomainService
    {
        string GetStylesheet(string requestedFile);
        string GetStylesheetHash(string requestedFile);
    }
}

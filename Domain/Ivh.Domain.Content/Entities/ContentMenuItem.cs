﻿namespace Ivh.Domain.Content.Entities
{
    using ProtoBuf;

    [ProtoContract]
    public class ContentMenuItem
    {
        [ProtoMember(1)]
        public virtual int ContentMenuItemId { get; set; }

        [ProtoMember(2)]
        public virtual string LinkText { get; set; }

        [ProtoMember(3)]
        public virtual string LinkUrl { get; set; }

        [ProtoMember(4)]
        public virtual int DisplayOrder { get; set; }

        [ProtoMember(5)]
        public virtual bool IsPublic { get; set; }

        [ProtoMember(6)]
        public virtual string IconCssClass { get; set; }

        // Any child that has a ref to its parent needs the AsReference = true parameter
        [ProtoMember(7,AsReference = true)]
        public virtual ContentMenu ContentMenu { get; set; }

        [ProtoMember(8)]
        public virtual bool LinkUrlOpensNewWindow { get; set; }
    }
}
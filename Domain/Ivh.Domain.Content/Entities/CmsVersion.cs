namespace Ivh.Domain.Content.Entities
{
    using System;
    using System.Globalization;
    using ProtoBuf;

    [ProtoContract]
    public class CmsVersion
    {
        [ProtoMember(1, DynamicType = true)]
        public virtual int CmsVersionId { get; set; }

        [ProtoMember(2, DynamicType = true)]
        public virtual int CmsRegionId { get; set; }

        [ProtoMember(3, DynamicType = true)]
        public virtual int VersionNum { get; set; }

        [ProtoMember(4, DynamicType = true)]
        public virtual string ContentTitle { get; set; }
        
        [ProtoMember(5, DynamicType = true)]
        public virtual string ContentBody { get; set; }


        private DateTime _updatedDate = DateTime.UtcNow;

        [ProtoIgnore]
        public virtual DateTime UpdatedDate
        {
            get { return this._updatedDate; }
            set { this._updatedDate = value; }
        }

        /// <summary>
        /// For serialization purposes only because Protobuf will not serialize DateTime
        /// The workaround is to use a string field that converts a backing DateTime member
        /// </summary>
        [ProtoMember(7)]
        public virtual string UpdatedDateString
        {
            get
            {
                string returnValue = this._updatedDate.ToString("o", CultureInfo.InvariantCulture);
                return returnValue;
            }
            set
            {
                DateTime date = DateTime.ParseExact(value, "o", CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal);
                this._updatedDate = date;
            }
        }


        [ProtoMember(8, DynamicType = true)]
        public virtual int UpdatedByUserId { get; set; }


        private DateTime? _expiryDate;


        /// <summary>
        /// For serialization purposes only because Protobuf will not serialize DateTime
        /// The workaround is to use a string field that converts a backing DateTime member
        /// </summary>
        [ProtoMember(9)]
        public virtual string ExpiryDateString
        {
            get { return this._expiryDate?.ToString("o"); }
            set { this._expiryDate = DateTime.Parse(value); }
        }

        [ProtoIgnore]
        public virtual DateTime? ExpiryDate
        {
            get { return this._expiryDate; }
            set { this._expiryDate = value; }
        }


        [ProtoMember(10)]
        public virtual CmsRegion CmsRegion { get; set; }

        [ProtoMember(11)]
        public virtual string Locale { get; set; }
    }
}

﻿namespace Ivh.Domain.Content.Entities
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using ProtoBuf;

    [ProtoContract]
    public class CmsRegion
    {
        [ProtoMember(1)]
        public virtual int CmsRegionId { get; set; }

        [ProtoMember(2)]
        public virtual string CmsRegionName { get; set; }

        [ProtoMember(3)]
        public virtual CmsTypeEnum CmsType { get; set; }
    }
}
﻿namespace Ivh.Domain.Content.Entities
{
    using System;
    public class Image
    {
        public Image()
        {
            this.Expires = DateTime.UtcNow.AddMinutes(60 * 24);
        }

        public byte[] ImageBytes { get; set; }
        public string ContentType { get; set; }
        public DateTime Expires { get; set; }
    }
}

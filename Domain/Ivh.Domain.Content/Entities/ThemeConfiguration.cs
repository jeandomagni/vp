﻿namespace Ivh.Domain.Content.Entities
{
    public class ThemeConfiguration
    {
        public string BaseFile { get; set; }
        public string IncludeDirectory { get; set; }
        public string SettingPrefix { get; set; }
    }
}

﻿namespace Ivh.Domain.Content.Entities
{
    using System.Collections.Generic;
    using ProtoBuf;

    [ProtoContract]
    public class ContentMenu
    {
        [ProtoMember(1)]
        public virtual int ContentMenuId { get; set; }

        [ProtoMember(2)]
        public virtual string ContentMenuName { get; set; }

        [ProtoMember(3)]
        public virtual IList<ContentMenuItem> ContentMenuItems { get; set; }
    }
}
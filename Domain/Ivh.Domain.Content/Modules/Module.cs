﻿namespace Ivh.Domain.Content.Modules
{
    using Autofac;
    using Common.DependencyInjection;
    using Common.VisitPay.Enums;
    using Interfaces;
    using Services;

    public class ContentModule : Autofac.Module
    {
        private readonly RegistrationSettings _registrationSettings;

        public ContentModule(RegistrationSettings registrationSettings) : base()
        {
            this._registrationSettings = registrationSettings;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ContentService>().As<IContentService>();

            IvhEnvironmentTypeEnum environmentType = (this._registrationSettings?.EnvironmentType).GetValueOrDefault(IvhEnvironmentTypeEnum.Unknown);
            bool isDevOrQa = environmentType == IvhEnvironmentTypeEnum.Dev || environmentType == IvhEnvironmentTypeEnum.Qa;

            if (isDevOrQa)
            {
                builder.RegisterType<QaImageService>().As<IImageService>();
            }
            else
            {
                builder.RegisterType<ImageService>().As<IImageService>();
            }

            builder.RegisterType<ThemeService>().As<IThemeService>();
        }
    }
}

﻿namespace Ivh.Domain.Content.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Content.Entities;
    using FluentNHibernate.Mapping;

    public class CmsRegionMapping : ClassMap<CmsRegion>
    {
        public CmsRegionMapping()
        {
            this.Schema("common");
            this.Table("CmsRegion");
            this.Id(x => x.CmsRegionId, "CmsRegionID").Not.Nullable().GeneratedBy.Assigned();
            this.Map(x => x.CmsRegionName).Not.Nullable();
            this.Map(x => x.CmsType, "CmsTypeID").CustomType<CmsTypeEnum>();
        }
    }
}
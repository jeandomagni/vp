﻿namespace Ivh.Domain.Content.Mappings
{
    using Content.Entities;
    using FluentNHibernate.Mapping;

    public class CmsVersionMapping : ClassMap<CmsVersion>
    {
        public CmsVersionMapping()
        {
            this.Schema("common");
            this.Table("CmsVersion");
            this.Id(x => x.CmsVersionId, "CmsVersionID").Not.Nullable();
            this.Map(x => x.CmsRegionId).Not.Nullable();
            this.Map(x => x.VersionNum).Not.Nullable();
            this.Map(x => x.ContentTitle);
            this.Map(x => x.ContentBody).Not.Nullable().Length(10000);
            this.Map(x => x.UpdatedDate).Not.Nullable();
            this.Map(x => x.UpdatedByUserId, "UpdatedBy").Not.Nullable();
            this.Map(x => x.ExpiryDate).Nullable();
            this.Map(x => x.Locale).Not.Nullable();
            this.References(x => x.CmsRegion)
                .Column("CmsRegionID")
                .Not.Insert()
                .Not.Update()
                .Not.Nullable();
        }
    }
}
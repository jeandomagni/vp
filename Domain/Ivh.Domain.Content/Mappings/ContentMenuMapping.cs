﻿namespace Ivh.Domain.Content.Mappings
{
    using Content.Entities;
    using FluentNHibernate.Mapping;

    public class ContentMenuMapping : ClassMap<ContentMenu>
    {
        public ContentMenuMapping()
        {
            this.Schema("common");
            this.Table("ContentMenu");
            this.ReadOnly();
            this.Id(x => x.ContentMenuId).Not.Nullable().GeneratedBy.Assigned();
            this.Map(x => x.ContentMenuName).Not.Nullable();

            this.HasMany(x => x.ContentMenuItems)
                .KeyColumn("ContentMenuId")
                .Not.LazyLoad()
                .BatchSize(50);
        }
    }
}
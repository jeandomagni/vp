﻿namespace Ivh.Domain.Content.Mappings
{
    using Content.Entities;
    using FluentNHibernate.Mapping;

    public class ContentMenuItemMapping : ClassMap<ContentMenuItem>
    {
        public ContentMenuItemMapping()
        {
            this.Schema("common");
            this.Table("ContentMenuItem");
            this.ReadOnly();
            this.Id(x => x.ContentMenuItemId).Not.Nullable();
            this.Map(x => x.LinkText).Not.Nullable();
            this.Map(x => x.LinkUrl).Not.Nullable();
            this.Map(x => x.DisplayOrder).Not.Nullable();
            this.Map(x => x.IsPublic).Not.Nullable();
            this.Map(x => x.IconCssClass).Nullable();
            this.References(x => x.ContentMenu)
                .Column("ContentMenuId")
                .Not.Nullable();
            this.Map(x => x.LinkUrlOpensNewWindow);
        }
    }
}
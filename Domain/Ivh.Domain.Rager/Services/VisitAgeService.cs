﻿namespace Ivh.Domain.Rager.Services
{
    using System;
    using System.Collections.Generic;
    using Base.Interfaces;
    using Entities;
    using Interfaces;
    using IDomainService = Common.Base.Interfaces.IDomainService;

    public class VisitAgeService : IVisitAgeService
    {
        private readonly IVisitRepository _visitRepository;
        private readonly IAgingTierConfigurationRepository _agingTierConfigurationRepository;

        public VisitAgeService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            IVisitRepository visitRepository,
            IAgingTierConfigurationRepository agingTierConfigurationRepository
        )
        {
            this._visitRepository = visitRepository;
            this._agingTierConfigurationRepository = agingTierConfigurationRepository;
        }

        public Visit GetVisitBySourceKeyAndBillingId(string sourceSystemKey, int billingSystemId)
        {
            Visit visit = this._visitRepository.GetVisitBySourceKeyAndBillingId(sourceSystemKey, billingSystemId);
            return visit;
        }

        public Visit GetOrCreateVisitBySourceKeyAndBillingId(string sourceSystemKey, int billingSystemId)
        {
            Visit visit = this._visitRepository.GetVisitBySourceKeyAndBillingId(sourceSystemKey, billingSystemId);

            if (visit == null)
            {
                visit = new Visit { VisitSourceSystemKey = sourceSystemKey, VisitBillingSystemId = billingSystemId };
            }

            return visit;
        }

        public void SaveVisit(Visit visit)
        {
            this._visitRepository.InsertOrUpdate(visit);
        }

        public IList<AgingTierConfiguration> GetActiveAgingTierConfigurations()
        {
            return this._agingTierConfigurationRepository.GetActiveAgingTierConfigurations();
        }

        public IList<int> GetVisitsIds(DateTime dateToProcess)
        {
            return this._visitRepository.GetEligibleToAgeVisitIds(dateToProcess);
        }

        public Visit GetVisitById(int visitId)
        {
            return this._visitRepository.GetById(visitId);
        }
    }
}

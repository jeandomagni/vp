﻿namespace Ivh.Domain.Rager.Services
{
    using System;
    using Base.Interfaces;
    using Base.Services;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;

    public class AgingGuarantorService : DomainService, IAgingGuarantorService
    {
        private readonly Lazy<IAgingGuarantorTypeRepository> _agingGuarantorTypeRepository;
        private readonly Lazy<IAgingGuarantorStateRepository> _agingGuarantorStateRepository;

        public AgingGuarantorService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IAgingGuarantorTypeRepository> agingGuarantorTypeRepository,
            Lazy<IAgingGuarantorStateRepository> agingGuarantorStateRepository
            ) : base(serviceCommonService)
        {
            this._agingGuarantorTypeRepository = agingGuarantorTypeRepository;
            this._agingGuarantorStateRepository = agingGuarantorStateRepository;
        }

        public void SetAgingGuarantorType(GuarantorTypeEnum newGuarantorType, int vpGuarantorId)
        {
            bool newTypeIsCallCenter = newGuarantorType == GuarantorTypeEnum.Offline;
            bool? agingGuarantorIsCallCenter = this._agingGuarantorTypeRepository.Value.AgingGuarantorIsCallCenter(vpGuarantorId);
            bool noAgingGuarantorTypeHistory = !agingGuarantorIsCallCenter.HasValue;

            bool typeChanged = noAgingGuarantorTypeHistory || newTypeIsCallCenter != agingGuarantorIsCallCenter.Value;

            if (typeChanged)
            {
                AgingGuarantorType agingGuarantorType = new AgingGuarantorType
                {
                    VpGuarantorId = vpGuarantorId,
                    IsCallCenter = newTypeIsCallCenter
                };

                this._agingGuarantorTypeRepository.Value.InsertOrUpdate(agingGuarantorType);
            }
        }

        public void SetAgingGuarantorState(VpGuarantorStatusEnum newGuarantorState, int vpGuarantorId)
        {
            bool newStateIsInactive = newGuarantorState == VpGuarantorStatusEnum.Closed;
            bool? agingGuarantorIsInactive = this._agingGuarantorStateRepository.Value.AgingGuarantorIsInactive(vpGuarantorId);
            bool noAgingGuarantorStateHistory = !agingGuarantorIsInactive.HasValue;

            bool stateChanged = noAgingGuarantorStateHistory || newStateIsInactive != agingGuarantorIsInactive.Value;

            if (stateChanged)
            {
                AgingGuarantorState agingGuarantorState = new AgingGuarantorState
                {
                    VpGuarantorId = vpGuarantorId,
                    IsInactive = newStateIsInactive
                };

                this._agingGuarantorStateRepository.Value.InsertOrUpdate(agingGuarantorState);
            }

        }
    }
}

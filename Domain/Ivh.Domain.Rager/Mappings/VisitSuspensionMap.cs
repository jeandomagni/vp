﻿namespace Ivh.Domain.Rager.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class VisitSuspensionMap : ClassMap<VisitSuspension>
    {
        public VisitSuspensionMap()
        {
            this.Schema(Schemas.Cdi.Aging);
            this.Table(nameof(VisitSuspension));
            this.Id(x => x.VisitSuspensionId);
            this.References(x => x.Visit).Column(nameof(Visit.VisitId));
            this.Map(x => x.IsSuspended);
            this.Map(x => x.InsertDate);
        }
    }
}

﻿namespace Ivh.Domain.Rager.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class VisitStateMap : ClassMap<VisitState>
    {
        public VisitStateMap()
        {
            this.Schema(Schemas.Cdi.Aging);
            this.Table(nameof(VisitState));
            this.Id(x => x.VisitStateId);
            this.References(x => x.Visit).Column(nameof(Visit.VisitId));
            this.Map(x => x.IsInactive);
            this.Map(x => x.InsertDate);
        }
    }
}
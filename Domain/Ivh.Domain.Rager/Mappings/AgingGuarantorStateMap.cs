﻿namespace Ivh.Domain.Rager.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class AgingGuarantorStateMap : ClassMap<AgingGuarantorState>
    {
        public AgingGuarantorStateMap()
        {
            this.Schema(Schemas.Cdi.Aging);
            this.Table("GuarantorState");
            this.Id(x => x.AgingGuarantorStateId);

            this.Map(x => x.InsertDate);
            this.Map(x => x.IsInactive);
            this.Map(x => x.VpGuarantorId);
        }
    }
}

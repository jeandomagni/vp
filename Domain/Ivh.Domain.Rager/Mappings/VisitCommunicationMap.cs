﻿namespace Ivh.Domain.Rager.Mappings
{
    using Common.Base.Enums;
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class VisitCommunicationMap : ClassMap<VisitCommunication>
    {
        public VisitCommunicationMap()
        {
            this.Schema(Schemas.Cdi.Aging);
            this.Table(nameof(VisitCommunication));
            this.Id(x => x.VisitCommunicationId);
            this.References(x => x.Visit).Column(nameof(Visit.VisitId));

            this.References(x => x.CommunicationType)
                .Column(nameof(CommunicationType.CommunicationTypeId));
            // TODO?.Not.Nullable();

            this.Map(x => x.DateOfCommunication);
            this.Map(x => x.CommunciationIncrement);
            this.Map(x => x.InsertDate);
            this.Map(x => x.ActionVisitPayUserId);
        }
    }
}

﻿namespace Ivh.Domain.Rager.Mappings
{
    using Common.Base.Enums;
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class AgingTierConfigurationMap : ClassMap<AgingTierConfiguration>
    {
        public AgingTierConfigurationMap()
        {
            this.Schema(Schemas.Cdi.Aging);
            this.Table(nameof(AgingTierConfiguration));
            this.Id(x => x.AgingTierConfigurationId);

            this.Map(x => x.MinAge);
            this.Map(x => x.MaxAge);
            this.Map(x => x.MinimumCommunicationConfiguration);

            this.References(x => x.ResultingAgingTier)
                .Column(nameof(AgingTier.AgingTierId));

            this.Map(x => x.ActiveDate);
        }
    }
}

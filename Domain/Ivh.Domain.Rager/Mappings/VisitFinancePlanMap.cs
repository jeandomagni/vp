﻿namespace Ivh.Domain.Rager.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class VisitFinancePlanMap : ClassMap<VisitFinancePlan>
    {
        public VisitFinancePlanMap()
        {
            this.Schema(Schemas.Cdi.Aging);
            this.Table(nameof(VisitFinancePlan));
            this.Id(x => x.VisitFinancePlanId);
            this.References(x => x.Visit).Column(nameof(Visit.VisitId));
            this.Map(x => x.IsOnFinancePlan);
            this.Map(x => x.InsertDate);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.Rager.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;
    public class AgingTierMap: ClassMap<AgingTier>
    {
        public AgingTierMap()
        {
            this.Schema(Schemas.Cdi.Aging);
            this.Table(nameof(AgingTier));
            this.Id(x => x.AgingTierId).GeneratedBy.Assigned();
            this.Map(x => x.Description);
        }
    }
}

﻿namespace Ivh.Domain.Rager.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class VisitMap : ClassMap<Visit>
    {
        public VisitMap()
        {
            this.Schema(Schemas.Cdi.Aging);
            this.Table(nameof(Visit));
            this.Id(x => x.VisitId);
            this.Map(x => x.VisitSourceSystemKey).Not.Nullable();
            this.Map(x => x.VisitBillingSystemId);

            this.References(x => x.CurrentVisitAgingTier)
                .Column("CurrentVisitAgingTierId");

            this.Map(x => x.InsertDate);
            this.Map(x => x.ProcessedDate);
            this.Map(x => x.VpGuarantorId);

            this.HasMany(x => x.VisitSuspensions)
                .KeyColumn(nameof(Visit.VisitId))
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();

            this.HasMany(x => x.VisitCommunications)
                .KeyColumn(nameof(Visit.VisitId))
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();

            this.HasMany(x => x.VisitFinancePlans)
                .KeyColumn(nameof(Visit.VisitId))
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();

            this.HasMany(x => x.VisitAges)
                .KeyColumn(nameof(Visit.VisitId))
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();

            this.HasMany(x => x.VisitStates)
                .KeyColumn(nameof(Visit.VisitId))
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();

            this.HasMany(x => x.VisitRecalls)
                .KeyColumn(nameof(Visit.VisitId))
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();

            this.HasMany(x => x.AgingGuarantorStates)
                .KeyColumn("VpGuarantorId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.None();

            this.HasMany(x => x.AgingGuarantorTypes)
                .KeyColumn("VpGuarantorId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.None();
        }
    }
}
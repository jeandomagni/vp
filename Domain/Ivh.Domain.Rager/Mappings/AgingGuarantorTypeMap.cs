﻿namespace Ivh.Domain.Rager.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class AgingGuarantorTypeMap : ClassMap<AgingGuarantorType>
    {
        public AgingGuarantorTypeMap()
        {
            this.Schema(Schemas.Cdi.Aging);
            this.Table("GuarantorType");
            this.Id(x => x.AgingGuarantorTypeId);

            this.Map(x => x.InsertDate);
            this.Map(x => x.IsCallCenter);
            this.Map(x => x.VpGuarantorId);
        }
    }
}

﻿namespace Ivh.Domain.Rager.Mappings
{
    using Common.Base.Enums;
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class VisitAgeMap : ClassMap<VisitAge>
    {
        public VisitAgeMap()
        {
            this.Schema(Schemas.Cdi.Aging);
            this.Table(nameof(VisitAge));
            this.Id(x => x.VisitAgeId);
            this.References(x => x.Visit).Column(nameof(Visit.VisitId));

            this.References(x => x.AgingTier)
                .Column(nameof(AgingTier.AgingTierId));

            this.Map(x => x.Age);
            this.Map(x => x.SnapshotAgeSum);
            this.Map(x => x.SnapshotCommunications);
            this.Map(x => x.InsertDate);
            this.Map(x => x.ActionVisitPayUserId);
        }
    }
}
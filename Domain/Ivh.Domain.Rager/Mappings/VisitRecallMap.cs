﻿namespace Ivh.Domain.Rager.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class VisitRecallMap : ClassMap<VisitRecall>
    {
        public VisitRecallMap()
        {
            this.Schema(Schemas.Cdi.Aging);
            this.Table(nameof(VisitRecall));
            this.Id(x => x.VisitRecallId);
            this.References(x => x.Visit).Column(nameof(Visit.VisitId));
            this.Map(x => x.IsRecalled);
            this.Map(x => x.InsertDate);
        }
    }
}
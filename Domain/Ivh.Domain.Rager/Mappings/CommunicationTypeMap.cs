﻿namespace Ivh.Domain.Rager.Mappings
{
    using Common.Base.Enums;
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class CommunicationTypeMap : ClassMap<CommunicationType>
    {
        public CommunicationTypeMap()
        {
            this.Schema(Schemas.Cdi.Aging);
            this.Table(nameof(CommunicationType));
            this.Id(x => x.CommunicationTypeId).GeneratedBy.Assigned(); 
            this.Map(x => x.Description);
        }
    }
}

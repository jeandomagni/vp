﻿namespace Ivh.Domain.Rager.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IAgingGuarantorTypeRepository : IRepository<AgingGuarantorType>
    {
        /// <summary>
        /// queries the GuarantorType history table to determine if a guarantor is a CallCenter type or null if no history exists
        /// </summary>
        /// <param name="vpGuarantorId"></param>
        /// <returns></returns>
        bool? AgingGuarantorIsCallCenter(int vpGuarantorId);
    }
}

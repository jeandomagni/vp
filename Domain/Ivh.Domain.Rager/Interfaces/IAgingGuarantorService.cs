﻿namespace Ivh.Domain.Rager.Interfaces
{
    using Common.VisitPay.Enums;

    public interface IAgingGuarantorService
    {
        void SetAgingGuarantorType(GuarantorTypeEnum newGuarantorType, int vpGuarantorId);
        void SetAgingGuarantorState(VpGuarantorStatusEnum newGuarantorState, int vpGuarantorId);
    }
}

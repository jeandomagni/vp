﻿namespace Ivh.Domain.Rager.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IVisitAgeService : IDomainService
    {
        Visit GetVisitBySourceKeyAndBillingId(string sourceSystemKey, int billingSystemId);
        Visit GetOrCreateVisitBySourceKeyAndBillingId(string sourceSystemKey, int billingSystemId);
        void SaveVisit(Visit visit);
        IList<AgingTierConfiguration> GetActiveAgingTierConfigurations();
        Visit GetVisitById(int visitId);
        IList<int> GetVisitsIds(DateTime dateToProcess);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.Rager.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IAgingTierConfigurationRepository:IRepository<AgingTierConfiguration>
    {
        IList<AgingTierConfiguration> GetActiveAgingTierConfigurations();
    }
}

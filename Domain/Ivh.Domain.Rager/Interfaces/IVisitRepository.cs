﻿namespace Ivh.Domain.Rager.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IVisitRepository : IRepository<Visit>
    {
        Visit GetVisitBySourceKeyAndBillingId(string sourceSystemKey, int billingSystemId);
        IList<int> GetEligibleToAgeVisitIds(DateTime dateToProcess);
    }
}
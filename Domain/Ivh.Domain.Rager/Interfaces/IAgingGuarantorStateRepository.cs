﻿namespace Ivh.Domain.Rager.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IAgingGuarantorStateRepository : IRepository<AgingGuarantorState>
    {
        /// <summary>
        /// queries the GuarantorState history table to determine if a guarantor is inactive or null if no history exists
        /// </summary>
        /// <param name="vpGuarantorId"></param>
        /// <returns></returns>
        bool? AgingGuarantorIsInactive(int vpGuarantorId);
    }
}

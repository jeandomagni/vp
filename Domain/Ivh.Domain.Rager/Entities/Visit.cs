﻿namespace Ivh.Domain.Rager.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Ivh.Common.Base.Utilities.Extensions;
    using Newtonsoft.Json;

    public class Visit
    {
        public Visit()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int VisitId { get; set; }
        public virtual string VisitSourceSystemKey { get; set; }
        public virtual int VisitBillingSystemId { get; set; }
        public virtual int VpGuarantorId { get; set; }
        public virtual AgingTier CurrentVisitAgingTier {
            get
            {
                VisitAge check = this.VisitAges.OrderByDescending(x => x.InsertDate)
                    .ThenByDescending(x => x.VisitAgeId)
                    .FirstOrDefault();

                if (check != null)
                {
                    return check.AgingTier;
                }
                return null;
            }
            set { }
        } 
        public virtual DateTime InsertDate { get; set; }
        public virtual DateTime? ProcessedDate { get; set; }
        public virtual IList<VisitSuspension> VisitSuspensions { get; set; } = new List<VisitSuspension>();
        public virtual bool IsSuspended
        {
            get
            {
                VisitSuspension check = this.VisitSuspensions.OrderByDescending(x => x.InsertDate).ThenByDescending(x => x.VisitSuspensionId).FirstOrDefault();
                if (check != null)
                {
                    return check.IsSuspended;
                }
                return false;
            }
        }
        public virtual IList<VisitCommunication> VisitCommunications { get; set; } = new List<VisitCommunication>();
        public virtual IList<VisitFinancePlan> VisitFinancePlans { get; set; } = new List<VisitFinancePlan>();
        public virtual bool IsOnFinancePlan
        {
            get
            {
                VisitFinancePlan check = this.VisitFinancePlans.OrderByDescending(x => x.InsertDate).ThenByDescending(x => x.VisitFinancePlanId).FirstOrDefault();
                if (check != null)
                {
                    return check.IsOnFinancePlan;
                }
                return false;
            }
        }
        public virtual IList<VisitAge> VisitAges { get; set; } = new List<VisitAge>();
        public virtual string VisitCommunicationsJsonData => this.VisitCommunications.GroupBy(communication => communication.CommunicationType)
                .Select(x => new {
                    CommunicationType = x.Key.CommunicationTypeEnum.ToString(),
                    CommunicationTypeId = x.Key.CommunicationTypeId,
                    SumCommunicationIncrement = x.Sum(y => y.CommunciationIncrement)
                }).ToJSON(new JsonSerializerSettings{ PreserveReferencesHandling = PreserveReferencesHandling.None,NullValueHandling = NullValueHandling.Ignore},true);

        public virtual int AgesCount
        {
            get
            {
                if (this.VisitAges.Any())
                {
                    return this.VisitAges.Sum(x => x.Age);
                }
                return 0;
            }
        }

        public virtual int CommunicationCountForType(AgingCommunicationTypeEnum typeEnum)
        {
            if (this.VisitCommunications.Any())
            {
                return this.VisitCommunications
                    .Where(x => x.CommunicationType.CommunicationTypeEnum == typeEnum)
                    .Sum(x => x.CommunciationIncrement);
            }
            return 0;
        }

        public virtual IList<VisitRecall> VisitRecalls { get; set; } = new List<VisitRecall>();
        public virtual bool IsRecalled
        {
            get
            {
                VisitRecall check = this.VisitRecalls.OrderByDescending(x => x.InsertDate).ThenByDescending(x => x.VisitRecallId).FirstOrDefault();
                if (check != null)
                {
                    return check.IsRecalled;
                }
                return false;
            }
        }
        public virtual void AddVisitRecall(bool isRecalled)
        {
            this.VisitRecalls.Add(new VisitRecall
            {
                IsRecalled = isRecalled,
                InsertDate = DateTime.UtcNow,
                Visit = this
            });
        }
        public virtual IList<VisitState> VisitStates { get; set; } = new List<VisitState>();
        public virtual bool IsInactive
        {
            get
            {
                VisitState check = this.VisitStates.OrderByDescending(x => x.InsertDate).ThenByDescending(x => x.VisitStateId).FirstOrDefault();
                if (check != null)
                {
                    return check.IsInactive;
                }
                return false;
            }
        }

        public virtual void AddVisitState(bool isInactive)
        {
            this.VisitStates.Add(new VisitState
            {
                IsInactive = isInactive,
                InsertDate = DateTime.UtcNow,
                Visit = this
            });
        }

        public virtual void AddVisitCommunications(DateTime dateOfCommunication, CommunicationType communicationType, int communicationIncrement = 1, int? actionVisitPayUserId = null)
        {
            this.VisitCommunications.Add(new VisitCommunication
            {
                Visit = this,
                CommunicationType = communicationType,
                CommunciationIncrement = communicationIncrement,
                DateOfCommunication = dateOfCommunication,
                InsertDate = DateTime.UtcNow,
                ActionVisitPayUserId = actionVisitPayUserId
            });
        }

        public virtual IList<AgingGuarantorState> AgingGuarantorStates { get; set; } = new List<AgingGuarantorState>();
        public virtual AgingGuarantorState CurrentGuarantorState => this.AgingGuarantorStates.OrderByDescending(x => x.InsertDate).ThenByDescending(x => x.AgingGuarantorStateId).FirstOrDefault();
        public virtual bool GuarantorIsInactive
        {
            get
            {
                AgingGuarantorState check = this.CurrentGuarantorState;
                if (check != null)
                {
                    return check.IsInactive;
                }
                return false;
            }
        }

        public virtual IList<AgingGuarantorType> AgingGuarantorTypes { get; set; } = new List<AgingGuarantorType>();
        public virtual AgingGuarantorType CurrentGuarantorType => this.AgingGuarantorTypes.OrderByDescending(x => x.InsertDate).ThenByDescending(x => x.AgingGuarantorTypeId).FirstOrDefault();
        public virtual bool GuarantorIsCallCenter
        {
            get
            {
                AgingGuarantorType check = this.CurrentGuarantorType;
                if (check != null)
                {
                    return check.IsCallCenter;
                }
                return false;
            }
        }

        public virtual bool IsEligibleToAge
        {
            // Warning: This logic has been duplicated in the VisitRepository
            // If this code is updated, be sure to update the repo as well 
            get {
                return !this.IsOnFinancePlan && 
                       !this.IsRecalled && 
                       !this.IsSuspended && 
                       !this.IsInactive && 
                       !this.GuarantorIsInactive && 
                       !this.GuarantorIsCallCenter;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.Rager.Entities
{
    using Common.Base.Enums;
    using Newtonsoft.Json;

    public class AgingTierConfiguration
    {
        public virtual int AgingTierConfigurationId { get; set; }
        public virtual int MinAge { get; set; }
        public virtual int MaxAge { get; set; }
        public virtual string MinimumCommunicationConfiguration { get; set; }
        public virtual AgingTier ResultingAgingTier { get; set; }
        public virtual DateTime ActiveDate { get; set; }

        public virtual MinimumCommunicationConfiguration MinimumCommunicationConfigurationDeserialized => 
            JsonConvert.DeserializeObject<MinimumCommunicationConfiguration>(this.MinimumCommunicationConfiguration);
    }
}

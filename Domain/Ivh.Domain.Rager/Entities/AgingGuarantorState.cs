﻿namespace Ivh.Domain.Rager.Entities
{
    using System;

    public class AgingGuarantorState
    {
        public AgingGuarantorState()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int AgingGuarantorStateId { get; set; }
        public virtual int VpGuarantorId { get; set; }
        public virtual bool IsInactive { get; set; }
        public virtual DateTime InsertDate { get; set; }
    }
}

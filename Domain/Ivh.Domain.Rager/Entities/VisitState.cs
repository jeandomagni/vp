﻿namespace Ivh.Domain.Rager.Entities
{
    using System;

    public class VisitState
    {
        public VisitState()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int VisitStateId { get; set; }
        public virtual Visit Visit { get; set; }
        public virtual bool IsInactive { get; set; }
        public virtual DateTime InsertDate { get; set; }
    }
}

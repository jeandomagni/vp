﻿namespace Ivh.Domain.Rager.Entities
{
    using Ivh.Common.Base.Utilities.Extensions;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    //TODO: This is a POC. 
    public class MinimumCommunicationConfiguration
    {

        [JsonIgnore]
        public string JsonData => this.ToJSON(new JsonSerializerSettings
        {
            PreserveReferencesHandling = PreserveReferencesHandling.None,
            NullValueHandling = NullValueHandling.Ignore
        }, true);


        public Dictionary<AgingCommunicationTypeEnum, int?> MinimumCommunicationTypeCounts;

    }


}

﻿namespace Ivh.Domain.Rager.Entities
{
    using System;

    public class VisitSuspension
    {
        public VisitSuspension()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int VisitSuspensionId { get; set; }
        public virtual Visit Visit { get; set; }
        public virtual bool IsSuspended { get; set; }
        public virtual DateTime InsertDate { get; set; }
    }
}

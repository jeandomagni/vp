﻿namespace Ivh.Domain.Rager.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;

    /// <summary>
    /// This provides a centralized location for the business rules for deciding 
    /// the AgingTier for a given visit
    /// </summary>

    //TODO: This needs unit tests!!
    public class AgingTierGenerator
    {

        private readonly Dictionary<AgingCommunicationTypeEnum, int> _communicationsByTypeFromVisit;

        //The visit age on the visit may not be trusted as we do not know 
        //if the a VisitAge has been added to the visit's VisitCommunications collection yet
        private readonly int _snapShotAgeSum; 
        private readonly IList<AgingTierConfiguration> _configurations;
        public AgingTierGenerator(IList<AgingTierConfiguration> configurations, Visit visit, int snapShotAgeSum)
        {
            this._communicationsByTypeFromVisit = visit.VisitCommunications.GroupBy(x => x.CommunicationType.CommunicationTypeEnum)
                .ToDictionary(x => x.Key, x => x.Sum(y => y.CommunciationIncrement));

            this._snapShotAgeSum = snapShotAgeSum;
            this._configurations = configurations;
        }

        public AgingTierConfiguration GetAgingTierConfigurationForCommunications()
        {
            bool ValidateCommunicationRequirements(bool previousRequirementsSatisfied, KeyValuePair<AgingCommunicationTypeEnum, int?> communicationTypeCountRequirement)
            {
                int visitCommunicationCountsForType = this._communicationsByTypeFromVisit.GetValue(communicationTypeCountRequirement.Key);
                bool currentRequirementSatisfied = visitCommunicationCountsForType >= communicationTypeCountRequirement.Value;

                return previousRequirementsSatisfied && currentRequirementSatisfied;
            }

            return this._configurations
                .Where(x => x.MinimumCommunicationConfigurationDeserialized.MinimumCommunicationTypeCounts.Aggregate(seed: true, func: ValidateCommunicationRequirements))
                .OrderByDescending(x => (int)x.ResultingAgingTier.AgingTierEnum)
                .FirstOrDefault();
        }

        public AgingTierConfiguration GetAgingTierConfigurationByAgeRange()
        {
            AgingTierConfiguration agingTierConfigurationByAge = this._configurations.Where(x => x.MinAge <= this._snapShotAgeSum)
                .OrderByDescending(x => x.ResultingAgingTier.AgingTierId)
                .FirstOrDefault();
            return agingTierConfigurationByAge;
        }

        public AgingTierConfiguration CalculateAgingTier()
        {
            //get the highest eligible tier for each configuration dimension
            IList<AgingTierConfiguration> agingTierConfigurations = new List<AgingTierConfiguration>
            {
                this.GetAgingTierConfigurationForCommunications(),
                this.GetAgingTierConfigurationByAgeRange()
            };

            //of the 2, return the minimum tier
            AgingTierConfiguration calculatedAgingTierConfiguration = agingTierConfigurations.OrderBy(x => x.ResultingAgingTier.AgingTierId).FirstOrDefault();
            return calculatedAgingTierConfiguration;
        }
    }
}
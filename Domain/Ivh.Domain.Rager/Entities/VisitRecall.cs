﻿namespace Ivh.Domain.Rager.Entities
{
    using System;

    public class VisitRecall
    {
        public VisitRecall()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        public virtual int VisitRecallId { get; set; }
        public virtual Visit Visit { get; set; }
        public virtual bool IsRecalled { get; set; }
        public virtual DateTime InsertDate { get; set; }
    }
}
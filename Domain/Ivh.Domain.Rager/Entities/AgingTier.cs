﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.Rager.Entities
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class AgingTier
    {
        public virtual int AgingTierId { get; set; }
        public virtual string Description { get; set; }
        public virtual AgingTierEnum AgingTierEnum => (AgingTierEnum) this.AgingTierId;
    }
}

﻿using System;

namespace Ivh.Domain.Rager.Entities
{
    public class VisitFinancePlan
    {
        public VisitFinancePlan()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int VisitFinancePlanId { get; set; }
        public virtual Visit Visit { get; set; }
        public virtual bool IsOnFinancePlan { get; set; }
        public virtual DateTime InsertDate { get; set; }
    }
}

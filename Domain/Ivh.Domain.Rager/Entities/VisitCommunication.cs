﻿namespace Ivh.Domain.Rager.Entities
{
    using Common.Base.Utilities.Extensions;
    using System;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class VisitCommunication
    {
        public VisitCommunication()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int VisitCommunicationId { get; set; }

        public virtual Visit Visit { get; set; }

        public virtual int VisitId => this.Visit.VisitId;

        public virtual CommunicationType CommunicationType { get; set; }
        public virtual DateTime DateOfCommunication { get; set; }

        public virtual int CommunciationIncrement { get; set; }

        public virtual DateTime InsertDate { get; set; }
        //public virtual string JsonData => this.ToJSON(true, true, true);

        public virtual string JsonData => new
        {
            VisitCommunicationId = this.VisitCommunicationId,
            VisitId = this.Visit.VisitId,
            CommunicationType = this.CommunicationType,
            DateOfCommunication = this.DateOfCommunication,
            CommunciationIncrement = this.CommunciationIncrement,
            InsertDate = this.InsertDate
        }.ToJSON(true, true, true);

        public virtual int? ActionVisitPayUserId { get; set; }
    }


    public class CommunicationType
    {
        public virtual int CommunicationTypeId { get; set; }

        public virtual AgingCommunicationTypeEnum CommunicationTypeEnum => (AgingCommunicationTypeEnum) this.CommunicationTypeId;
        //CommunicationCategoryId
        public virtual string Description { get; set; }
    }


    //TODO: Ask about this. I am proposing using Enum Category
    //public class CommunicationCatetory
    //{
    //    public virtual int CommunicationCatetoryId { get; set; }
    //}

}

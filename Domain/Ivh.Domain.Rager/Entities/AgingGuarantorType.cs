﻿namespace Ivh.Domain.Rager.Entities
{
    using System;

    public class AgingGuarantorType
    {
        public AgingGuarantorType()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int AgingGuarantorTypeId { get; set; }
        public virtual int VpGuarantorId { get; set; }
        public virtual bool IsCallCenter { get; set; }
        public virtual DateTime InsertDate { get; set; }
    }
}

﻿namespace Ivh.Domain.Rager.Entities
{
    using System;

    public class VisitAge
    {
        public VisitAge()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int VisitAgeId { get; set; }
        public virtual Visit Visit { get; set; }
        public virtual AgingTier AgingTier { get; set; }
        public virtual int Age { get; set; }
        public virtual int SnapshotAgeSum { get; set; }
        /// <summary>
        /// This is set by using Visit.VisitCommunicationsJsonData
        /// </summary>
        public virtual string SnapshotCommunications { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual int? ActionVisitPayUserId { get; set; }
    }

}

﻿namespace Ivh.Domain.FinanceManagement.Discount.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;
    using Payment.Entities;

    public interface IDiscountService : IDomainService
    {
        void AssignDiscountsToPayment(Payment payment, IList<DiscountVisitPayment> discountVisitPayments);
    }
}
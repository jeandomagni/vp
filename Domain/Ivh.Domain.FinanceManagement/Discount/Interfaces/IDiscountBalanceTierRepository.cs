﻿namespace Ivh.Domain.FinanceManagement.Discount.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IDiscountBalanceTierRepository : IRepository<DiscountBalanceTier>
    {
    }
}
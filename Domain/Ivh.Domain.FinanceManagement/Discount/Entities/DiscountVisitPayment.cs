﻿namespace Ivh.Domain.FinanceManagement.Discount.Entities
{
    using Payment.Entities;

    public class DiscountVisitPayment
    {
        public DiscountVisitPayment(PaymentVisit paymentVisit, decimal totalBalance)
        {
            this.PaymentVisit = paymentVisit;
            this.TotalBalance = totalBalance;
        }
        
        public decimal TotalBalance { get; }

        public PaymentVisit PaymentVisit { get; }
    }
}
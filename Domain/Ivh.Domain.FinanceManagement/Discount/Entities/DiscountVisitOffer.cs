﻿namespace Ivh.Domain.FinanceManagement.Discount.Entities
{
    using System;
    using Payment.Entities;

    public class DiscountVisitOffer
    {
        public DiscountVisitOffer()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        public virtual int DiscountOfferVisitId { get; set; }

        public virtual DateTime InsertDate { get; set; }

        public virtual PaymentVisit PaymentVisit { get; set; }

        public virtual decimal DiscountPercent { get; set; }

        public virtual DiscountOffer DiscountOffer { get; set; }
        
        public virtual decimal DiscountAmount => Math.Round(this.TotalBalance * this.DiscountPercent, 2, MidpointRounding.AwayFromZero);
        
        public virtual decimal DiscountedCurrentBalance => this.TotalBalance - this.DiscountAmount;
        
        public virtual decimal TotalBalance { get; set; } // not persisted
    }
}
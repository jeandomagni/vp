﻿namespace Ivh.Domain.FinanceManagement.Discount.Entities
{
    public class DiscountBalanceTier
    {
        public virtual int DiscountBalanceTierId { get; set; }
        public virtual decimal MinimumBalance { get; set; }
        public virtual decimal? MaximumBalance { get; set; }
        public virtual decimal DiscountPercent { get; set; }
        public virtual decimal? MinimumPtpScore { get; set; }
        public virtual decimal? MaximumPtpScore { get; set; }

    }
}
namespace Ivh.Domain.FinanceManagement.Discount.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class DiscountOffer
    {
        public DiscountOffer()
        {
            this.VisitOffers = new List<DiscountVisitOffer>();
            this.InsertDate = DateTime.UtcNow;
        }
        
        public virtual int DiscountOfferId { get; set; }
        
        public virtual IList<DiscountVisitOffer> VisitOffers { get; set; }
        
        public virtual decimal DiscountTotalPercent { get; set; }
        
        public virtual decimal DiscountTotal
        {
            get
            {
                return this.VisitOffers.Sum(x => x.DiscountAmount);
            }
        }
        
        public virtual decimal DiscountedTotalCurrentBalance
        {
            get
            {
                return this.VisitOffers.Sum(x => x.DiscountedCurrentBalance);
            }
        }

        public virtual DateTime InsertDate { get; set; }

        public virtual bool IsPromptPayOnly { get; set; } // not persisted
    }
}
namespace Ivh.Domain.FinanceManagement.Discount.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class DiscountOfferMap : ClassMap<DiscountOffer>
    {
        public DiscountOfferMap()
        {
            this.Schema("dbo");
            this.Table("DiscountOffer");
            this.Id(x => x.DiscountOfferId);
            this.Map(x => x.InsertDate);
            this.Map(x => x.DiscountTotalPercent);
            
            this.HasMany(x => x.VisitOffers).KeyColumn("DiscountOfferId")
                .BatchSize(50)
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Cascade.AllDeleteOrphan();
        }
    }
}
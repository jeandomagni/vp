namespace Ivh.Domain.FinanceManagement.Discount.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class DiscountVisitOfferMap : ClassMap<DiscountVisitOffer>
    {
        public DiscountVisitOfferMap()
        {
            this.Schema("dbo");
            this.Table("DiscountOfferVisit");
            this.Id(x => x.DiscountOfferVisitId);
            this.Map(x => x.InsertDate);
            this.Map(x => x.DiscountPercent);
            this.References(x => x.PaymentVisit).Column("VisitId");
            this.References(x => x.DiscountOffer).Column("DiscountOfferId");
        }
    }
}
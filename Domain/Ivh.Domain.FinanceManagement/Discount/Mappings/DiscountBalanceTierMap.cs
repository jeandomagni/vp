﻿namespace Ivh.Domain.FinanceManagement.Discount.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class DiscountBalanceTierMap : ClassMap<DiscountBalanceTier>
    {
        public DiscountBalanceTierMap()
        {
            this.Schema("dbo");
            this.Table("DiscountBalanceTier");
            this.Id(x => x.DiscountBalanceTierId);
            this.Map(x => x.MinimumBalance).Not.Nullable();
            this.Map(x => x.MaximumBalance).Nullable();
            this.Map(x => x.DiscountPercent).Not.Nullable();
            this.Map(x => x.MinimumPtpScore);
            this.Map(x => x.MaximumPtpScore);

            this.Cache.ReadOnly();
        }
    }
}
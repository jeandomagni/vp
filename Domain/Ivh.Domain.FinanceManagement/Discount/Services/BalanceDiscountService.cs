﻿namespace Ivh.Domain.FinanceManagement.Discount.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AppIntelligence.Interfaces;
    using Base.Interfaces;
    using Common.Base.Utilities.Extensions;
    using Entities;
    using Guarantor.Interfaces;
    using Interfaces;

    public class BalanceDiscountService : DiscountBaseService, IDiscountService
    {
        private readonly Lazy<IDiscountBalanceTierRepository> _discountBalanceTierRepository;

        public BalanceDiscountService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IDiscountBalanceTierRepository> discountBalanceTierRepository,
            Lazy<IGuarantorScoreService> scoringService,
            Lazy<IRandomizedTestService> randomizedTestService
        ) : base(serviceCommonService, scoringService, randomizedTestService)
        {
            this._discountBalanceTierRepository = discountBalanceTierRepository;
        }
        
        protected override DiscountOffer GenerateDiscountOffer(IList<DiscountVisitPayment> discountVisitPayments, decimal ptpScore)
        {
            DiscountOffer discount = new DiscountOffer();

            decimal balanceToConsider = discountVisitPayments.Sum(x => x.TotalBalance);

            DiscountBalanceTier discountBalanceTier = this.GetDiscountBalanceTier(balanceToConsider, ptpScore);
            if (discountBalanceTier == null)
            {
                return discount;
            }

            foreach (DiscountVisitPayment visit in discountVisitPayments.Where(this.IsVisitEligible))
            {
                this.AddDiscountVisitOffer(discount, visit, discountBalanceTier.DiscountPercent);
            }

            return discount;
        }

        private DiscountBalanceTier GetDiscountBalanceTier(decimal balanceToConsider, decimal ptpScore)
        {
            IList<DiscountBalanceTier> discountBalanceTiers = this._discountBalanceTierRepository.Value.GetQueryable()
                .Where(x => (!x.MinimumPtpScore.HasValue && !x.MaximumPtpScore.HasValue) || (ptpScore >= x.MinimumPtpScore && ptpScore <= x.MaximumPtpScore))
                .ToList();

            if (discountBalanceTiers.IsNullOrEmpty())
            {
                return null;
            }

            decimal minimumBalance = discountBalanceTiers.Select(x => x.MinimumBalance).Min();
            if (balanceToConsider < minimumBalance)
            {
                return null;
            }

            DiscountBalanceTier discountBalanceTier = discountBalanceTiers
                .Where(tier => balanceToConsider <= tier.MaximumBalance.GetValueOrDefault(decimal.MaxValue))
                .OrderBy(tier => tier.MinimumBalance)
                .FirstOrDefault();

            return discountBalanceTier;
        }
    }
} 
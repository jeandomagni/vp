﻿namespace Ivh.Domain.FinanceManagement.Discount.Services
{
    using System;
    using System.Collections.Generic;
    using Interfaces;
    using System.Linq;
    using AppIntelligence.Interfaces;
    using Base.Interfaces;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Constants;
    using Entities;
    using Guarantor.Interfaces;

    public class InsuranceDiscountService : DiscountBaseService, IDiscountService
    {
        public InsuranceDiscountService(Lazy<IDomainServiceCommonService> domainServiceCommonService,
            Lazy<IGuarantorScoreService> scoringService,
            Lazy<IRandomizedTestService> randomizedTestService) : base(domainServiceCommonService, scoringService, randomizedTestService)
        {
        }

        protected override DiscountOffer GenerateDiscountOffer(IList<DiscountVisitPayment> discountVisitPayments, decimal ptpScore)
        {
            DiscountOffer discount = new DiscountOffer();
            IList<DiscountVisitPayment> eligibleVisits = new List<DiscountVisitPayment>();

            foreach (DiscountVisitPayment discountVisitPayment in discountVisitPayments.Where(this.IsVisitEligible))
            {
                eligibleVisits.Add(discountVisitPayment);
            }

            if (eligibleVisits.IsNullOrEmpty())
            {
                return discount;
            }

            foreach (DiscountVisitPayment visit in eligibleVisits)
            {
                this.AddDiscountVisitOffer(discount, visit, visit.PaymentVisit.DiscountPercentage);
            } 

            return discount;
        }
        
        protected override bool IsVisitEligible(DiscountVisitPayment visit)
        {
            if (!base.IsVisitEligible(visit))
            {
                return false;
            }

            if (visit.PaymentVisit.BillingApplication == BillingApplicationConstants.HB)
            {
                return true;
            }

            return false;
        }
    }
}
﻿namespace Ivh.Domain.FinanceManagement.Discount.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AppIntelligence.Entities;
    using AppIntelligence.Interfaces;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;
    using Entities;
    using Guarantor.Entities;
    using Guarantor.Interfaces;
    using Payment.Entities;
    
    public abstract class DiscountBaseService : DomainService
    {
        private readonly Lazy<IGuarantorScoreService> _scoringService;
        private readonly Lazy<IRandomizedTestService> _randomizedTestService;

        protected DiscountBaseService(Lazy<IDomainServiceCommonService> domainServiceCommonService,
                                      Lazy<IGuarantorScoreService> scoringService,
                                      Lazy<IRandomizedTestService> randomizedTestService) : base(domainServiceCommonService)
        {
            this._scoringService = scoringService;
            this._randomizedTestService = randomizedTestService;
        }
        
        public void AssignDiscountsToPayment(Payment payment, IList<DiscountVisitPayment> discountVisitPayments)
        {
            if (discountVisitPayments.IsNullOrEmpty())
            {
                return;
            }
            
            if (!this.IsPaymentTypeEligible(payment.PaymentType))
            {
                return;
            }

            bool enableBalanceDiscount = this.IsTestGroupEnabledForBalanceDiscount(payment.Guarantor.VpGuarantorId);
            if (!enableBalanceDiscount)
            {
                return;
            }
            VpGuarantorScore ptpScore = this.GetGuarantorPtpScore(payment.Guarantor);
            DiscountOffer discountOffer = this.GenerateDiscountOffer(discountVisitPayments, ptpScore?.Score ?? 0);
            discountOffer.DiscountTotalPercent = discountOffer.DiscountedTotalCurrentBalance + discountOffer.DiscountTotal <= 0 ? 0m : Math.Round(discountOffer.DiscountTotal / (discountOffer.DiscountedTotalCurrentBalance + discountOffer.DiscountTotal), 4);
            discountOffer.IsPromptPayOnly = this.Client.Value.DiscountPromptPayOnly;

            if (discountOffer.DiscountTotalPercent <= 0m)
            {
                return;
            }

            // assign the discount
            payment.DiscountOffer = discountOffer;

            // visit level discount - update scheduled amounts
            if (payment.PaymentType.IsInCategory(PaymentTypeEnumCategory.CurrentNonFinancedBalanceInFull))
            {
                foreach (PaymentScheduledAmount paymentScheduledAmount in payment.PaymentScheduledAmounts)
                {
                    DiscountVisitOffer visitDiscount = discountOffer.VisitOffers?.FirstOrDefault(x => x.PaymentVisit.VisitId == paymentScheduledAmount.VisitId);
                    if (visitDiscount != null)
                    {
                        paymentScheduledAmount.ScheduledAmount -= visitDiscount.DiscountAmount;
                    }
                }
            }
        }

        private VpGuarantorScore GetGuarantorPtpScore(Guarantor guarantor)
        {
            int guarantorToScore = guarantor.VpGuarantorId;
            if (guarantor.IsManaged())
            {
                guarantorToScore = guarantor.ManagingGuarantorId ?? guarantorToScore;
            }
            return this._scoringService.Value.GetScore(guarantorToScore, ScoreTypeEnum.PtpOriginal);
        }

        private bool IsTestGroupEnabledForBalanceDiscount(int vpGuarantorId)
        {
            // If client does not have balance-based discount test group, it will by default enable balance discount
            bool enableBalanceDiscount = true;
            RandomizedTest randomizedTest = this._randomizedTestService.Value.GetRandomizedTest(RandomizedTestEnum.BalanceBasedDiscount);
            if (randomizedTest != null)
            {
                RandomizedTestGroup randomizedTestGroup = this._randomizedTestService.Value.GetRandomizedTestGroupMembership(randomizedTest, vpGuarantorId);
                if (randomizedTestGroup != null)
                {
                    RandomizedTestGroupEnum randomizedTestGroupEnum = (RandomizedTestGroupEnum)randomizedTestGroup.RandomizedTestGroupId;
                    enableBalanceDiscount = false;
                    if (randomizedTestGroupEnum == RandomizedTestGroupEnum.BalanceBasedDiscount)
                    {
                        BalanceBasedDiscountRandomizedTest test = new BalanceBasedDiscountRandomizedTest(randomizedTest);
                        enableBalanceDiscount = test.GetTestGroupConfiguration(RandomizedTestGroupEnum.BalanceBasedDiscount).EnableBalanceBasedDiscount;
                    }
                }
            }

            return enableBalanceDiscount;
        }


        protected virtual bool IsPaymentTypeEligible(PaymentTypeEnum paymentType)
        {
            bool isPaymentTypeEligible = paymentType.IsInCategory(PaymentTypeEnumCategory.DiscountEligible);
            if (!isPaymentTypeEligible)
            {
                return false;
            }
            
            if (this.Client.Value.DiscountPromptPayOnly)
            {
                return paymentType.IsInCategory(PaymentTypeEnumCategory.Prompt);
            }

            return true;
        }

        protected virtual bool IsVisitEligible(DiscountVisitPayment visit)
        {
            if (visit == null)
            {
                return false;
            }
            
            AgingTierEnum discountMaximumAgingTier = this.Client.Value.DiscountMaximumAgingTier;
            if (visit.PaymentVisit.AgingTier > discountMaximumAgingTier)
            {
                return false;
            }

            if (visit.PaymentVisit.CurrentVisitState != VisitStateEnum.Active)
            {
                return false;
            }

            return true;
        }

        protected virtual void AddDiscountVisitOffer(DiscountOffer discount, DiscountVisitPayment visit, decimal discountPercentage)
        {
            DiscountVisitOffer discountVisitOffer = new DiscountVisitOffer
            {
                TotalBalance = visit.TotalBalance, // not persisted
                DiscountOffer = discount,
                DiscountPercent = discountPercentage,
                PaymentVisit = visit.PaymentVisit
            };

            discount.VisitOffers.Add(discountVisitOffer);
        }

        protected abstract DiscountOffer GenerateDiscountOffer(IList<DiscountVisitPayment> discountVisitPayments, decimal ptpScore);
    }
}
﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AppIntelligence.Entities;
    using AppIntelligence.Interfaces;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;
    using Ivh.Domain.Guarantor.Entities;
    using Entities;
    using Interfaces;
    using Statement.Entities;
    using Statement.Interfaces;

    public class FinancePlanOfferService : DomainService, IFinancePlanOfferService
    {
        private readonly Lazy<IFinancePlanMinimumPaymentTierRepository> _minimumPaymentTierRepository;
        private readonly Lazy<IInterestService> _interestService;
        private readonly Lazy<IFinancePlanOfferRepository> _financePlanOfferRepository;
        private readonly Lazy<IFinancePlanOfferSetTypeRepository> _financePlanOfferSetTypeRepository;
        private readonly Lazy<IRandomizedTestService> _randomizedTestService;
        private readonly Lazy<IAmortizationService> _amortizationService;
        private readonly Lazy<IStatementDateService> _statementDateService;

        public FinancePlanOfferService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IFinancePlanMinimumPaymentTierRepository> minimumPaymentTierRepository,
            Lazy<IInterestService> interestService,
            Lazy<IFinancePlanOfferRepository> financePlanOfferRepository,
            Lazy<IFinancePlanOfferSetTypeRepository> financePlanOfferSetTypeRepository,
            Lazy<IRandomizedTestService> randomizedTestService,
            Lazy<IAmortizationService> amortizationService,
            Lazy<IStatementDateService> statementDateService) : base(serviceCommonService)
        {
            this._minimumPaymentTierRepository = minimumPaymentTierRepository;
            this._interestService = interestService;
            this._financePlanOfferRepository = financePlanOfferRepository;
            this._financePlanOfferSetTypeRepository = financePlanOfferSetTypeRepository;
            this._randomizedTestService = randomizedTestService;
            this._amortizationService = amortizationService;
            this._statementDateService = statementDateService;
        }

        public FinancePlanOffer UpdateFinancePlanToFinancialAssistanceOffer(FinancePlan financePlan)
        {
            IList<InterestRate> interestRates = this._interestService.Value.GetInterestRates(
                financePlan.FinancePlanOffer.StatementedCurrentAccountBalance,
                financePlan.ToListOfOne(),
                financePlan.HasFinancialAssistance(),
                financePlan.VpGuarantor.VpGuarantorId,
                financePlan.FinancePlanOfferSetTypeId);

            if (!interestRates.Any(x => x.IsCharity.HasValue && x.IsCharity.Value))
            {
                // no available interest rates w/charity offer
                return null;
            }

            IList<FinancePlanOffer> financePlanOffers = new List<FinancePlanOffer>();

            this.CreateOfferWithInterestRateAndTotalAmount(
                financePlanOffers, 
                financePlan.FinancePlanVisits.Cast<IFinancePlanVisitBalance>().ToList(),
                interestRates.First(x => x.IsCharity.HasValue && x.IsCharity.Value),
                new FinancePlanMinimumPaymentTier {MinimumPayment = 0.01m},
                financePlan.VpGuarantor,
                financePlan.FinancePlanOffer.VpStatement,
                financePlan.FinancePlanOffer.CorrelationGuid,
                financePlan.FirstInterestPeriod(),
                financePlan.IsCombined,
                null,
                financePlan.InterestCalculationMethod);

            if (!financePlanOffers.Any(x => x.IsCharity))
            {
                return null;
            }

            FinancePlanOffer financePlanOffer = financePlanOffers.First(x => x.IsCharity);

            return this._financePlanOfferRepository.Value.ManagedInsert(financePlanOffer);
        }
        
        public IList<FinancePlanOffer> GenerateOffersForUser(FinancePlanCalculationParameters financePlanCalculationParameters,
            IList<FinancePlan> allApplicableFinancePlans,
            bool hasFinancialAssistance,
            bool saveOffer,
            FirstInterestPeriodEnum firstInterestPeriodEnum)
        {
            decimal totalAmount = financePlanCalculationParameters.Visits.Sum(x => x.CurrentBalance);

            IList<InterestRate> interestRates = this._interestService.Value.GetInterestRates(
                totalAmount,
                financePlanCalculationParameters.CombineFinancePlans ? new List<FinancePlan>(): allApplicableFinancePlans,
                hasFinancialAssistance,
                financePlanCalculationParameters.Guarantor.VpGuarantorId,
                financePlanCalculationParameters.FinancePlanOfferSetTypeId,
                financePlanCalculationParameters.VpGuarantorScore?.Score);
            
            if (!interestRates.Any())
            {
                return new List<FinancePlanOffer>();
            }

            if (interestRates.GroupBy(x => x.FinancePlanOfferSetType.FinancePlanOfferSetTypeId).Count() != 1)
            {
                return new List<FinancePlanOffer>();
            }

            FinancePlanOfferSetType financePlanOfferSetType = interestRates.Select(x => x.FinancePlanOfferSetType).First();

            IList<FinancePlanMinimumPaymentTier> allTiers = null;
            if (financePlanOfferSetType.IsPatient)
            {
                // only apply randomized testing to offer sets of patient type
                RandomizedTestGroup randomizedTestGroup = this._randomizedTestService.Value.GetRandomizedTestGroupMembership(RandomizedTestEnum.RandomizedInterestRateOffers, financePlanCalculationParameters.Guarantor.VpGuarantorId);
                if (randomizedTestGroup != null)
                {
                    allTiers = this._minimumPaymentTierRepository.Value.GetQueryable().Where(x => x.RandomizedTestGroupId == randomizedTestGroup.RandomizedTestGroupId).ToList();

                    if (allTiers.IsNullOrEmpty())
                    {
                        this.LoggingService.Value.Warn(() =>  "FinancePlanOfferService::GenerateOffersForUserAsync - No FinancePlanMinimumPaymentTier found in Randomized Test {0} for Randomized Test Group {1}. Selecting non-randomized offers.", randomizedTestGroup.RandomizedTest.RandomizedTestId, randomizedTestGroup.RandomizedTestGroupId);
                    }
                }
            }

            if (allTiers.IsNullOrEmpty())
            {
                allTiers = this._minimumPaymentTierRepository.Value.GetQueryable().ToList();
            }

            allTiers = (allTiers ?? new List<FinancePlanMinimumPaymentTier>()).Where(x => x.FinancePlanOfferSetType.FinancePlanOfferSetTypeId == financePlanOfferSetType.FinancePlanOfferSetTypeId).ToList();
            allTiers = AutoMapper.Mapper.Map<List<FinancePlanMinimumPaymentTier>>(allTiers);

            FinancePlanMinimumPaymentTier selectedTier = FindTierToUse(allTiers, allApplicableFinancePlans);
            if (selectedTier == null)
            {
                throw new Exception("Couldn't Find the minimum payment tier!");
            }

            // VPNG-19279
            // Override the selected minimum if it is a combined finance plan
            bool isCalculateCombinedFinancePlanMinimumEnabled = this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.CalculateCombinedFinancePlanMinimumIsEnabled);
            if (isCalculateCombinedFinancePlanMinimumEnabled && financePlanCalculationParameters.CombineFinancePlans)
            {
                this.CalculateCombinedFinancePlanMinimum(
                    allApplicableFinancePlans, 
                    selectedTier, 
                    firstInterestPeriodEnum, 
                    financePlanCalculationParameters.InterestCalculationMethod,
                    financePlanCalculationParameters.Statement);
            }

            List<FinancePlanOffer> offers = new List<FinancePlanOffer>();
            Guid correlationGuid = Guid.NewGuid();

            foreach (InterestRate interestRate in interestRates)
            {
                this.CreateOfferWithInterestRateAndTotalAmount(
                    offers,
                    financePlanCalculationParameters.Visits,
                    interestRate,
                    selectedTier,
                    financePlanCalculationParameters.Guarantor,
                    financePlanCalculationParameters.Statement,
                    correlationGuid,
                    firstInterestPeriodEnum,
                    financePlanCalculationParameters.CombineFinancePlans,
                    financePlanCalculationParameters.FirstPaymentDueDate,
                    financePlanCalculationParameters.InterestCalculationMethod);
            }
            
            if (!saveOffer)
            {
                return offers;
            }

            List<FinancePlanOffer> returnOffers = new List<FinancePlanOffer>();
            foreach (FinancePlanOffer financePlanOffer in offers)
            {
                FinancePlanOffer offer = this._financePlanOfferRepository.Value.ManagedInsert(financePlanOffer);
                returnOffers.Add(offer);
            }

            return returnOffers;
        }

        public IList<FinancePlanOffer> GetOffersWithCorrelationGuid(int vpGuarantorId, Guid guid)
        {
            return this._financePlanOfferRepository.Value.GetOffersWithCorrelationGuid(vpGuarantorId, guid);
        }

        public FinancePlanOffer GetOfferById(int financePlanOfferId)
        {
            return this._financePlanOfferRepository.Value.GetById(financePlanOfferId);
        }

        private void CreateOfferWithInterestRateAndTotalAmount(
            ICollection<FinancePlanOffer> offers,
            IList<IFinancePlanVisitBalance> visits,
            InterestRate interestRate,
            FinancePlanMinimumPaymentTier selectedTier,
            Guarantor guarantor,
            VpStatement statement,
            Guid correlationGuid,
            FirstInterestPeriodEnum interestFreePeriod,
            bool isCombined,
            DateTime? firstPaymentDueDate,
            InterestCalculationMethodEnum interestCalculationMethod)
        {
            FinancePlanOffer offer = new FinancePlanOffer
            {
                DurationRangeStart = interestRate.DurationRangeStart,
                DurationRangeEnd = interestRate.DurationRangeEnd,
                InsertDate = DateTime.UtcNow,
                InterestRate = interestRate.Rate,
                IsActive = true,
                IsCharity = interestRate.IsCharity.HasValue && interestRate.IsCharity.Value,
                StatementedCurrentAccountBalance = visits.Sum(x => x.CurrentBalance),
                VpGuarantor = guarantor,
                VpStatement = statement,
                CorrelationGuid = correlationGuid,
                FinancePlanOfferSetType = interestRate.FinancePlanOfferSetType,
                IsCombined = isCombined
            };

            DateTime dateToCheck = firstPaymentDueDate.GetValueOrDefault(statement.PaymentDueDate);
            
            // max payment
            offer.MaxPayment = this.GetFactoredPaymentAmount(visits, offer.DurationRangeStart, offer.InterestRate, true, interestFreePeriod);
            offer.MaxPayment = Math.Round(offer.MaxPayment, 2, MidpointRounding.AwayFromZero);
            offer.MaxPayment = this.ValidatePaymentAmount(true, visits, offer.MaxPayment, offer.InterestRate, offer.DurationRangeStart, interestFreePeriod, dateToCheck, interestCalculationMethod);
            if (offer.MaxPayment < selectedTier.MinimumPayment)
            {
                offer.MaxPayment = selectedTier.MinimumPayment;
            }

            // min payment
            offer.MinPayment = this.GetFactoredPaymentAmount(visits, offer.DurationRangeEnd, offer.InterestRate, false, interestFreePeriod);
            offer.MinPayment = offer.MinPayment.RoundUp();
            offer.MinPayment = this.ValidatePaymentAmount(false, visits, offer.MinPayment, offer.InterestRate, offer.DurationRangeEnd, interestFreePeriod, dateToCheck, interestCalculationMethod);
            if (offer.MinPayment < selectedTier.MinimumPayment)
            {
                offer.MinPayment = selectedTier.MinimumPayment;
            }
            
            offers.Add(offer);
        }

        /// <summary>
        /// calculates the monthly payment amount assuming a monthly assessment/payment cycle. this will be close to the amount for a daily assessment/ monthly payment cycle.
        /// </summary>
        /// <returns></returns>
        public decimal GetFactoredPaymentAmount(
            IList<IFinancePlanVisitBalance> visits,
            int durationToCheck,
            decimal interestRate,
            bool checkMaximum,
            FirstInterestPeriodEnum interestFreePeriod)
        {
            if (durationToCheck - (int)interestFreePeriod <= 0)
            {
                //Means doesnt include interest.
                decimal divideBy = (decimal)durationToCheck != 0 ? durationToCheck : 1;
                return visits.Sum(x => x.CurrentBalance) / divideBy;
            }

            if (checkMaximum && durationToCheck > 1)
            {
                durationToCheck--;
            }

            decimal amount = 0;
            
            foreach (IFinancePlanVisitBalance visit in visits)
            {
                int monthsWithOverride = visit.OverridesRemaining;
                int monthsWithoutOverride = durationToCheck - monthsWithOverride;

                decimal factorNonOverride = 0m;

                if (monthsWithoutOverride > 0)
                {
                    factorNonOverride = this._interestService.Value.GetFactorForInterest(monthsWithoutOverride, interestRate, interestFreePeriod);
                    factorNonOverride = CheckFactor(factorNonOverride, monthsWithoutOverride);
                }

                decimal? factorOverride = null;
                if (monthsWithOverride > 0)
                {
                    factorOverride = this._interestService.Value.GetFactorForInterest(monthsWithOverride, visit.OverrideInterestRate.GetValueOrDefault(interestRate), 0);
                    factorOverride = CheckFactor(factorOverride.Value, monthsWithOverride);
                }

                decimal divide = factorOverride.GetValueOrDefault(0) + factorNonOverride;
                if (divide == 0m)
                {
                    divide = 1m;
                }

                decimal paymentAmount = visit.CurrentBalance / factorOverride.GetValueOrDefault(1) / divide;
                amount += paymentAmount;
            }

            return amount;
        }

        public IList<FinancePlanOfferSetType> GetFinancePlanOfferSetTypes()
        {
            List<FinancePlanOfferSetType> types = this._financePlanOfferSetTypeRepository.Value.GetQueryable().ToList();
            if (!this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FpOfferSetTypesIsEnabledClient))
            {
                return types.Where(x => x.IsDefault).ToList();
            }

            bool isClientApplication = this.ApplicationSettingsService.Value.IsClientApplication.Value;

            List<FinancePlanOfferSetType> applicableTypes = types
                .Where(x => x.IsActive)
                .Where(x => (isClientApplication && x.IsClient) || (!isClientApplication && x.IsPatient))
                .OrderBy(x => x.Priority)
                .ThenByDescending(x => x.IsDefault)
                .ThenBy(x => x.Name)
                .ToList();

            return applicableTypes.Any() ? applicableTypes : types.Where(x => x.IsDefault).ToList();
        }

        /// <summary>
        /// smooth over any inconsistencies between amortization and calculated payment amount by setting a new amount
        /// </summary>
        /// <returns></returns>
        public decimal ValidatePaymentAmount(bool validateMax,
            IList<IFinancePlanVisitBalance> visits,
            decimal paymentAmount, 
            decimal interestRate, 
            int duration,
            FirstInterestPeriodEnum interestFreePeriod,
            DateTime firstPaymentDueDate,
            InterestCalculationMethodEnum interestCalculationMethod)
        {
            if (duration == 1)
            {
                return visits.Sum(x => x.CalculateBalance(x.PrincipalBalance, 0, x.InterestDue));
            }

            IList<AmortizationMonth> months = this._amortizationService.Value.GetAmortizationMonthsForAmounts(paymentAmount, visits, interestRate, interestFreePeriod, firstPaymentDueDate, interestCalculationMethod);
            if (months.Count <= 1)
            {
                return paymentAmount;
            }

            //if we are validating the max payment, our target is a penny less than the min payment for a duration that is one month less
            //eg. duration = 5, balance 100: max payment is ($25 * 4 months) - 1c = $24.99 
            // which yields payments of 24.99, 24.99, 24.99, 24.99, 0.04
            if (validateMax)
            {
                duration--;
            }

            decimal GetPaymentAmountAdjustment(IList<AmortizationMonth> amortizationMonths)
            {
                //adjust up
                if (amortizationMonths.Count > duration)
                {
                    int monthsToCheck = months.Count - duration;
                    monthsToCheck = monthsToCheck > 0 ? monthsToCheck : 1;

                    decimal adjustPaymentAmount = months.OrderByDescending(x => x.MonthNumber).Take(monthsToCheck).Sum(x => x.PrincipalAmount) / duration;

                    return adjustPaymentAmount.RoundDown();
                }
                //adjust down
                else
                {
                    decimal adjustment = 0m;

                    int monthsShortOfTarget = duration - months.Count;
                    if (monthsShortOfTarget > 0)
                    {
                        //off by a lot, make a big jump
                        adjustment = -paymentAmount * ((decimal) monthsShortOfTarget / duration);
                    }
                    else
                    {
                        //pretty close, just look at the last two months
                        List<AmortizationMonth> lastTwoMonths = amortizationMonths.OrderByDescending(x => x.MonthNumber).Take(2).ToList();
                        decimal lastMonthTotal = lastTwoMonths[0].Total;
                        decimal secondToLastMonthTotal = lastTwoMonths[1].Total;
                        decimal difference = lastMonthTotal - secondToLastMonthTotal;

                        adjustment = difference / duration;
                    }

                    return adjustment.RoundUp();
                }
            }

            int i = 10;
            while (i-- > 0)
            {
                decimal amountToAdjust = GetPaymentAmountAdjustment(months);
                paymentAmount += amountToAdjust;

                if (amountToAdjust == 0m)
                {
                    if (months.Count == duration)
                    {
                        return validateMax ? paymentAmount - 0.01m : paymentAmount;
                    }
                    else
                    {
                        paymentAmount += months.Count > duration ? 0.01m : -0.01m;
                        return paymentAmount;
                    }
                }

                months = this._amortizationService.Value.GetAmortizationMonthsForAmounts(paymentAmount, visits, interestRate, interestFreePeriod, firstPaymentDueDate, interestCalculationMethod);
            }

            return validateMax ? paymentAmount - 0.01m : paymentAmount;
        }

        private void CalculateCombinedFinancePlanMinimum(
            IList<FinancePlan> allApplicableFinancePlans, 
            FinancePlanMinimumPaymentTier selectedTier, 
            FirstInterestPeriodEnum firstInterestPeriodEnum,
            InterestCalculationMethodEnum interestCalculationMethod,
            VpStatement statement)
        {
            if (allApplicableFinancePlans == null || !allApplicableFinancePlans.Any())
            {
                return;
            }
            
            var balanceWithMonthsRemaining = allApplicableFinancePlans.Select(financePlan => new
            {
                financePlan.CurrentFinancePlanBalance,
                RemainingMonths = this._amortizationService.Value.GetAmortizationDurationForAmounts(
                    financePlan.PaymentAmount,
                    financePlan.ActiveFinancePlanVisitBalances,
                    financePlan.FinancePlanOffer?.InterestRate ?? 0m,
                    firstInterestPeriodEnum,
                    this._statementDateService.Value.GetNextPaymentDate(financePlan.VpGuarantor, statement),
                    interestCalculationMethod)
            }).ToList();

            if (!balanceWithMonthsRemaining.Any())
            {
                return;
            }

            decimal minimumPayment = balanceWithMonthsRemaining.Sum(x => x.CurrentFinancePlanBalance) / Math.Max(balanceWithMonthsRemaining.Max(x => x.RemainingMonths), 1);
            if (selectedTier.MinimumPayment < minimumPayment)
            {
                selectedTier.MinimumPayment = minimumPayment;
            }
        }
        
        private static FinancePlanMinimumPaymentTier FindTierToUse(IEnumerable<FinancePlanMinimumPaymentTier> allTiers, IEnumerable<FinancePlan> allOpenFinancePlans)
        {
            decimal sumOfAllFinancePlans = allOpenFinancePlans.Sum(x => x.PaymentAmount);

            return allTiers.OrderByDescending(x => x.ExisitingRecurringPaymentTotal).FirstOrDefault(financePlanMinimumPaymentTier => sumOfAllFinancePlans >= financePlanMinimumPaymentTier.ExisitingRecurringPaymentTotal);
        }
        
        private static decimal CheckFactor(decimal factor, int duration)
        {
            if (factor == 0 && duration != 0)
            {
                return duration;
            }

            if (factor == 0)
            {
                return 1;
            }

            return factor;
        }
    }
}
﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AppIntelligence.Entities;
    using AppIntelligence.Interfaces;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;

    public class InterestService : DomainService, IInterestService
    {
        private readonly Lazy<IFinancePlanOfferSetTypeRepository> _financePlanOfferSetTypeRepository;
        private readonly Lazy<IInterestRateRepository> _interestRateRepository;
        private readonly Lazy<IInterestRateConsiderationService> _interestRateConsiderationService;
        private readonly Lazy<IRandomizedTestService> _randomizedTestService;

        public InterestService(
            Lazy<IDomainServiceCommonService> domainServiceCommonService,
            Lazy<IFinancePlanOfferSetTypeRepository> financePlanOfferSetTypeRepository,
            Lazy<IInterestRateRepository> interestRateRepository,
            Lazy<IInterestRateConsiderationService> interestRateConsiderationService,
            Lazy<IRandomizedTestService> randomizedTestService) 
            : base(domainServiceCommonService)
        {
            this._financePlanOfferSetTypeRepository = financePlanOfferSetTypeRepository;
            this._interestRateRepository = interestRateRepository;
            this._interestRateConsiderationService = interestRateConsiderationService;
            this._randomizedTestService = randomizedTestService;
        }

        public decimal GetFactorForInterest(int maxDurationInMonths, decimal yearlyInterestRate, FirstInterestPeriodEnum interestFreePeriod)
        {
            if (yearlyInterestRate < 0 || yearlyInterestRate > 1)
            {
                if (yearlyInterestRate < 0)
                {
                    throw new Exception("Cannot have an interest rate that is less than zero");
                }
                throw new Exception("Cannot have an interest rate that is greater than 1, implies greater than 100%");
            }

            if (maxDurationInMonths < 0)
            {
                throw new Exception("Cannot have a negative maxDurationInMonths");
            }
            
            if (interestFreePeriod < 0)
            {
                throw new Exception("Cannot have a negative interestFreePeriodInMonths.");
            }

            if (yearlyInterestRate == 0)
            {
                return 0m;
            }
            
            decimal monthlyInterestRate = yearlyInterestRate / 12m;
            return (1m - (decimal) Math.Pow((double) (1m / (1m + monthlyInterestRate)), (double) (maxDurationInMonths - (decimal) interestFreePeriod))) / (decimal) monthlyInterestRate + (decimal) interestFreePeriod;
        }
        
        /// <summary>
        /// Uses default MidPointRounding.ToEven (aka "Banker's Rounding") to determine monthly interest amount
        /// </summary>
        /// <param name="yearlyInterestRate"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        public decimal GetMonthlyInterestAmountForBalance(decimal yearlyInterestRate, decimal amount)
        {
            decimal monthlyInterestRate = yearlyInterestRate / 12m;
            return Math.Round(monthlyInterestRate * amount, 2, MidpointRounding.ToEven);
        }

        /// <summary>
        /// Uses default MidPointRounding.ToEven (aka "Banker's Rounding") to sum daily interest due over a given period
        /// </summary>
        /// <param name="yearlyInterestRate"></param>
        /// <param name="dateAmounts"></param>
        /// <returns></returns>
        public decimal GetInterestDueForPeriod(decimal yearlyInterestRate, IList<Tuple<DateTime, decimal>> dateAmounts)
        {
            decimal interestDue = 0m;

            foreach (Tuple<DateTime, decimal> dateAmount in dateAmounts)
            {
                //tuple values
                DateTime date = dateAmount.Item1;
                decimal amount = dateAmount.Item2;

                //calculate interest rate for this date
                int daysInYear = date.DaysInYear();
                decimal dailyInterestRate = yearlyInterestRate / daysInYear;

                //calculate interest due for this date
                decimal amountDueForDay = dailyInterestRate * amount;
                interestDue += amountDueForDay;
            }
            
            interestDue = Math.Round(interestDue, 2, MidpointRounding.ToEven);

            return interestDue;
        }

        /// <summary>
        /// Gets list of available interest rates for finance plans.
        /// </summary>
        /// <param name="totalAmountToConsider">used to calculate the total amount to be considered</param>
        /// <param name="allApplicableFinancePlans">used to calculate the total amount to be considered</param>
        /// <param name="hasFinancialAssistance">used to determine HasFinancialAssistance</param>
        /// <param name="vpGuarantorId">used to filter finance plans by a specified guarantor from the parameter allApplicableFinancePlans</param>
        /// <param name="financePlanOfferSetTypeId">finance plan offer set type that is used to get the available interest rates</param>
        /// <param name="ptpScore">Guarantor's propensity to pay score</param>
        /// <returns></returns>
        public IList<InterestRate> GetInterestRates(decimal totalAmountToConsider, IList<FinancePlan> allApplicableFinancePlans, bool hasFinancialAssistance, int vpGuarantorId, int? financePlanOfferSetTypeId = null, decimal? ptpScore = null)
        {
            RandomizedTestGroup randomizedTestGroup = this._randomizedTestService.Value.GetRandomizedTestGroupMembership(RandomizedTestEnum.RandomizedInterestRateOffers, vpGuarantorId);
            if (randomizedTestGroup != null)
            {
                IList<InterestRate> applicableInterestRates = this.GetApplicableInterestRates(totalAmountToConsider, allApplicableFinancePlans, hasFinancialAssistance, randomizedTestGroup.RandomizedTestGroupId, financePlanOfferSetTypeId, ptpScore);
                if (applicableInterestRates.IsNotNullOrEmpty())
                {
                    return applicableInterestRates;
                }
                this.LoggingService.Value.Warn(() =>  "InterestService::GetInterestRates - No interest rate found in Randomized Test {0} for Randomized Test Group {1}. Selecting non-randomized offers.", randomizedTestGroup.RandomizedTest.RandomizedTestId, randomizedTestGroup.RandomizedTestGroupId);
            }

            allApplicableFinancePlans = allApplicableFinancePlans?.Where(x => x.VpGuarantor.VpGuarantorId == vpGuarantorId).ToList();

            return this.GetApplicableInterestRates(totalAmountToConsider, allApplicableFinancePlans, hasFinancialAssistance, randomizedTestGroup?.RandomizedTestGroupId, financePlanOfferSetTypeId, ptpScore);
        }
        
        public int GetMaxDurationRangeEnd(FinancePlanOfferSetTypeEnum financePlanOfferSetType)
        {
            return this._interestRateRepository.Value.GetMaxDurationRangeEnd(financePlanOfferSetType);
        }

        /// <summary>
        /// Gets list of available interest rates for finance plans.
        /// </summary>
        /// <param name="totalAmountToConsider">only used to calculate the total amount to be considered</param>
        /// <param name="allApplicableFinancePlans">only used to calculate the total amount to be considered</param>
        /// <param name="hasFinancialAssistance">only used to determine HasFinancialAssistance</param>
        /// <param name="randomizedTestGroupId">used to filter available interest rates for patient offer set types</param>
        /// <param name="financePlanOfferSetTypeId">finance plan offer set type that is used to get the available interest rates</param>
        /// <param name="ptpScore">Guarantor's propensity to pay score</param>
        /// <returns></returns>
        private IList<InterestRate> GetApplicableInterestRates(decimal totalAmountToConsider, IList<FinancePlan> allApplicableFinancePlans, bool hasFinancialAssistance, int? randomizedTestGroupId, int? financePlanOfferSetTypeId, decimal? ptpScore = null)
        {
            decimal totalConsiderationAmount = this._interestRateConsiderationService.Value.GetTotalConsiderationAmount(totalAmountToConsider, allApplicableFinancePlans);
            if (totalConsiderationAmount <= 0m)
            {
                return new List<InterestRate>();
            }

            List<FinancePlanOfferSetType> financePlanOfferSetTypes = new List<FinancePlanOfferSetType>();
            bool isClient = this.ApplicationSettingsService.Value.IsClientApplication.Value;

            if (financePlanOfferSetTypeId.HasValue)
            {
                FinancePlanOfferSetType financePlanOfferSetType = this._financePlanOfferSetTypeRepository.Value.GetById(financePlanOfferSetTypeId.Value);
                if (financePlanOfferSetType == null)
                {
                    this.LoggingService.Value.Fatal(() => "InterestService::GetApplicableInterestRates - Finance Plan Offer Set not found");
                    throw new Exception("Finance Plan Offer Set not found");
                }
                financePlanOfferSetTypes.Add(financePlanOfferSetType);
            }
            else
            {
                financePlanOfferSetTypes = this._financePlanOfferSetTypeRepository.Value.GetQueryable()
                    .Where(x => x.IsActive && (isClient ? x.IsClient : x.IsPatient))
                    .OrderBy(x => x.Priority).ThenByDescending(x => x.IsDefault).ToList();

                if (financePlanOfferSetTypes.Count <= 0)
                {
                    this.LoggingService.Value.Fatal(() => "InterestService::GetApplicableInterestRates - No default finance plan offer set type");
                    throw new Exception("No default finance plan offer set type");
                }
            }

            IList<InterestRate> allRates = this._interestRateRepository.Value.GetQueryable().ToList();
            IList<InterestRate> rates = new List<InterestRate>();
            foreach (FinancePlanOfferSetType financePlanOfferSetType in financePlanOfferSetTypes)
            {
                if (financePlanOfferSetType.IsPatient)
                {
                    allRates = allRates.Where(x => x.RandomizedTestGroupId == randomizedTestGroupId).ToList();
                }

                if (financePlanOfferSetType.FinancePlanOfferSetTypeId != (int) FinancePlanOfferSetTypeEnum.ScoreBasedOffer)
                {
                    rates = allRates.Where(x => x.FinancePlanOfferSetType.FinancePlanOfferSetTypeId == financePlanOfferSetType.FinancePlanOfferSetTypeId).ToList();
                }
                else
                {
                    rates = allRates.Where(x => x.FinancePlanOfferSetType.FinancePlanOfferSetTypeId == financePlanOfferSetType.FinancePlanOfferSetTypeId && 
                                                ptpScore.HasValue && ptpScore >= 0 && 
                                                ptpScore >= x.MinimumPtpScore && ptpScore <= x.MaximumPtpScore).ToList();
                }
                if (rates.Count > 0)
                {
                    if (this.ApplicationSettingsService.Value.EnableFinancialAssistanceInterestRates.Value && hasFinancialAssistance)
                    {
                        rates = rates.Where(x => x.IsCharity.HasValue && x.IsCharity.Value
                                                                      && totalConsiderationAmount >= x.MinimumBalance.GetValueOrDefault(0)
                                                                      && totalConsiderationAmount <= x.MaximumBalance.GetValueOrDefault(decimal.MaxValue)).ToList();
                    }
                    else
                    {
                        rates = rates.Where(x => (!x.IsCharity.HasValue || !x.IsCharity.Value)
                                                 && (!ptpScore.HasValue || (!x.MinimumPtpScore.HasValue && !x.MaximumPtpScore.HasValue) 
                                                 || (ptpScore >= x.MinimumPtpScore && ptpScore <= x.MaximumPtpScore))
                                                 && totalConsiderationAmount >= x.MinimumBalance.GetValueOrDefault(0)
                                                 && totalConsiderationAmount <= x.MaximumBalance.GetValueOrDefault(decimal.MaxValue)).ToList();
                    }
                    if (rates.Count > 0)
                    {
                        break;
                    }
                }
            }

            return rates;
        }
    }
}
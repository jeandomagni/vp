namespace Ivh.Domain.FinanceManagement.FinancePlan.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Constants;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Constants;
    using Visit.Entities;
    using Entities;
    using Interfaces;

    public class InterestRateConsiderationFinancePlansBalanceService : DomainService, IInterestRateConsiderationService
    {
        /// <summary>
        /// Returns the total of CurrentFinancePlanBalances added to amount passed in.
        /// </summary>
        /// <param name="totalAmountToConsider"></param>
        /// <param name="allApplicableFinancePlans">Used to get the sum from each CurrentFinancePlanBalance</param>
        /// <returns></returns>
        [Obsolete(ObsoleteDescription.VisitTransactions)]
        public decimal GetTotalConsiderationAmount(decimal totalAmountToConsider, IList<FinancePlan> allApplicableFinancePlans)
        {
            return totalAmountToConsider + allApplicableFinancePlans.Sum(x => x.CurrentFinancePlanBalance);
        }

        public InterestRateConsiderationFinancePlansBalanceService(Lazy<IDomainServiceCommonService> serviceCommonService) : base(serviceCommonService)
        {
        }
    }
}
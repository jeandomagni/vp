﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Application.FinanceManagement.Common.Dtos;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.EventJournal;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.FinanceManagement;
    using Common.VisitPay.Messages.HospitalData;
    using Visit.Entities;
    using Visit.Interfaces;
    using Entities;
    using Guarantor.Entities;
    using Interfaces;
    using Logging.Interfaces;
    using NHibernate;
    using Payment.Entities;
    using Statement.Entities;
    using User.Interfaces;
    using Ivh.Common.EventJournal.Templates.Guarantor;
    using Statement.Interfaces;
    using ValueTypes;

    public class FinancePlanService : DomainService, IFinancePlanService
    {
        private const string NewFinancePlanMetricKey = "FinancePlan.New";
        private const string NewCombinedFinancePlanMetricKey = "FinancePlan.New.Combined";

        private readonly Lazy<IAmortizationService> _amortizationService;
        private readonly Lazy<IFinancePlanOfferService> _financePlanOfferService;
        private readonly Lazy<IFinancePlanRepository> _financePlanRepository;
        private readonly Lazy<IFinancePlanReadOnlyRepository> _financePlanReadOnlyRepository;
        private readonly Lazy<IInterestService> _interestService;
        private readonly Lazy<IVisitService> _visitService;
        private readonly Lazy<IVisitPayUserJournalEventService> _userJournalEventService;
        private readonly ISession _session;
        private readonly Lazy<IMetricsProvider> _metricsProvider;
        private readonly Lazy<IVpStatementService> _statementService;
        private readonly Lazy<IStatementDateService> _statementDateService;
        private readonly Lazy<IFacilityService> _facilityService;

        public FinancePlanService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IAmortizationService> amortizationService,
            Lazy<IFinancePlanOfferService> financePlanOfferService,
            Lazy<IFinancePlanRepository> financePlanRepository,
            Lazy<IFinancePlanReadOnlyRepository> financePlanReadOnlyRepository,
            Lazy<IInterestService> interestService,
            Lazy<IVisitService> visitService,
            Lazy<IVisitPayUserJournalEventService> userJournalEventService,
            Lazy<IVpStatementService> statementService,
            Lazy<IStatementDateService> statementDateService,
            ISessionContext<VisitPay> sessionContext,
            Lazy<IMetricsProvider> metricsProvider,
            Lazy<IFacilityService> facilityService) : base(serviceCommonService)
        {
            this._amortizationService = amortizationService;
            this._financePlanOfferService = financePlanOfferService;
            this._financePlanRepository = financePlanRepository;
            this._financePlanReadOnlyRepository = financePlanReadOnlyRepository;
            this._interestService = interestService;
            this._visitService = visitService;
            this._userJournalEventService = userJournalEventService;
            this._statementService = statementService;
            this._statementDateService = statementDateService;
            this._session = sessionContext.Session;
            this._metricsProvider = metricsProvider;
            this._facilityService = facilityService;
        }

        /// <summary>
        ///     If the visit is on an Active originated or Active pending origination financeplan
        /// </summary>
        public bool IsVisitInAFinancePlan(Visit visit)
        {
            return this._financePlanRepository.Value.AreVisitsOnActiveOrPendingFinancePlan(new List<Visit> { visit }).Any();
        }

        public IList<Visit> VisitsOnFinancePlan(IList<Visit> visits, DateTime? asOfDate = null)
        {
            return this._financePlanRepository.Value.AreVisitsOnActiveOrPendingFinancePlan(visits, asOfDate);
        }

        public IList<FinancePlanVisitResult> AreVisitsOnFinancePlan_FinancePlanVisitResult(IList<int> visitIds, DateTime? asOfDate, List<FinancePlanStatusEnum> listOfStatuses = null)
        {
            return this._financePlanRepository.Value.AreVisitsOnFinancePlan_FinancePlanVisitResult(visitIds, asOfDate, listOfStatuses);
        }

        public IList<FinancePlanVisit> AreVisitsOnFinancePlan_FinancePlanVisit(IList<int> visitIds, DateTime? asOfDate, List<FinancePlanStatusEnum> listOfStatuses = null)
        {
            return this._financePlanRepository.Value.AreVisitsOnFinancePlan_FinancePlanVisit(visitIds, asOfDate, listOfStatuses);
        }

        public IList<FinancePlanVisit> GetVisitOnAnyFinancePlan(int visitId)
        {
            IList<FinancePlanVisit> financePlanVisits = this._financePlanRepository.Value.GetVisitOnAnyFinancePlan(visitId);

            return financePlanVisits;
        }

        public IList<int> GetVisitIdsOnActiveFinancePlans(int vpGuarantorId, int? financePlanId = null)
        {
            IList<FinancePlanStatusEnum> checkStatuses;

            if (financePlanId.HasValue)
            {
                checkStatuses = Enum.GetValues(typeof(FinancePlanStatusEnum)).Cast<FinancePlanStatusEnum>().ToList();
            }
            else
            {
                checkStatuses = new List<FinancePlanStatusEnum>
                {
                    FinancePlanStatusEnum.GoodStanding,
                    FinancePlanStatusEnum.PastDue,
                    FinancePlanStatusEnum.UncollectableActive
                };
            }

            IList<int> ids = this._financePlanRepository.Value.GetVisitIdsOnFinancePlans(vpGuarantorId, financePlanId, checkStatuses);

            return ids;
        }

        public IList<int> GetVisitIdsOnPendingFinancePlans(int vpGuarantorId, int? financePlanId = null)
        {
            IList<FinancePlanStatusEnum> checkStatuses;

            if (financePlanId.HasValue)
            {
                checkStatuses = Enum.GetValues(typeof(FinancePlanStatusEnum)).Cast<FinancePlanStatusEnum>().ToList();
            }
            else
            {
                checkStatuses = new List<FinancePlanStatusEnum>
                {
                    FinancePlanStatusEnum.PendingAcceptance
                };
            }

            IList<int> ids = this._financePlanRepository.Value.GetVisitIdsOnFinancePlans(vpGuarantorId, financePlanId, checkStatuses);

            return ids;
        }

        public IList<int> GetVisitIdsOnActiveFinancePlans(IList<int> visitIds)
        {
            return this._financePlanRepository.Value.AreVisitIdsOnActiveFinancePlan(visitIds);
        }

        public FinancePlan OriginateFinancePlan(FinancePlan financePlan, FinancePlanCreditAgreementCollectionDto financePlanCreditAgreementCollectionDto, int termsAcceptedVisitPayUserId, string reason = null)
        {
            if (financePlan == null)
            {
                return null;
            }

            //Make sure that FP still has active visits
            //Make sure that there is still a balance
            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                if (financePlan.CurrentFinancePlanBalance > 0m && financePlan.ActiveFinancePlanVisits.Any(x => x.CurrentVisitState == VisitStateEnum.Active))
                {
                    if (financePlan.OriginationDate != null)
                    {
                        if (financePlan.IsCombined || financePlan.IsReconfiguration())
                        {
                            // carry any outstanding interest over to the new finance plan
                            IList<FinancePlan> financePlans = this._financePlanRepository.Value.GetFinancePlansFromVisits(financePlan.ActiveFinancePlanVisits.Select(x => x.VisitId).ToList());
                            foreach (FinancePlanVisit financePlanVisit in financePlans.Where(x => x.IsActiveOriginated()).SelectMany(x => x.FinancePlanVisits).GroupBy(x => x.VisitId).Select(x => x.First()))
                            {
                                if (financePlanVisit.InterestDue > 0m)
                                {
                                    financePlan.AddExistingInterest(financePlanVisit.VisitId, financePlanVisit.InterestDue);
                                }
                            }
                        }

                        // credit agreement
                        financePlan.SetCreditAgreement(financePlanCreditAgreementCollectionDto, DateTime.UtcNow, termsAcceptedVisitPayUserId);

                        // status
                        financePlan.SetFinancePlanStatus(this.GetStatusWithEnum(FinancePlanStatusEnum.GoodStanding), reason);

                        // first payment due
                        DateTime firstPaymentDueDate =  financePlan.FirstPaymentDueDate ?? this._statementDateService.Value.GetNextPaymentDate(financePlan.VpGuarantor, financePlan.CreatedVpStatement);

                        // duration
                        int duration = this._amortizationService.Value.GetAmortizationDurationForAmounts(
                            financePlan.PaymentAmount,
                            financePlan.ActiveFinancePlanVisitBalances,
                            financePlan.FinancePlanOffer?.InterestRate ?? 0m,
                            financePlan.FirstInterestPeriod(),
                            firstPaymentDueDate,
                            financePlan.InterestCalculationMethod);

                        financePlan.OriginalDuration = duration;
                        financePlan.OriginalPaymentAmount = financePlan.PaymentAmount;
                        financePlan.OriginationDate = DateTime.UtcNow;

                        // remove any carry-over interest from originated balance
                        financePlan.OriginatedFinancePlanBalance = financePlan.CurrentFinancePlanBalance - financePlan.InterestAssessed.GetValueOrDefault(0);

                        foreach (FinancePlanVisit financePlanVisit in financePlan.ActiveFinancePlanVisits)
                        {
                            // remove any carry-over interest from originated balance
                            financePlanVisit.OriginatedSnapshotVisitBalance = financePlanVisit.CurrentBalance - financePlanVisit.InterestAssessed;
                        }

                        financePlan.UnpublishedFinancePlanAuditEvents.Add(AuditEventTypeFinancePlanEnum.Origination);
                    }
                }
                else
                {
                    financePlan.FinancePlanStatus = this.GetStatusWithEnum(FinancePlanStatusEnum.Canceled);
                }
                
                this._financePlanRepository.Value.InsertOrUpdate(financePlan);

                this._session.Transaction.RegisterPostCommitSuccessAction((fp, fpca) =>
                {
                    if (fp.IsActiveOriginated())
                    {
                        this.Bus.Value.PublishMessage(new FinancePlanCreatedMessage
                        {
                            FinancePlanId = fp.FinancePlanId,
                            VpGuarantorId = fp.VpGuarantor.VpGuarantorId
                        }).Wait();

                        if (fp.IsRetailInstallmentContract())
                        {
                            this._metricsProvider.Value.Increment(Metrics.Increment.FinancePlan.OriginationWithRetailInstallmentContract);

                            this.Bus.Value.PublishMessage(new SendFinancePlanConfirmationEmailMessage
                            {
                                FinancePlanPublicIdPhi = fp.FinancePlanPublicIdPhi,
                                VpGuarantorId = fp.VpGuarantor.VpGuarantorId,
                            }).Wait();
                            
                        }
                        else
                        {
                            this._metricsProvider.Value.Increment(Metrics.Increment.FinancePlan.OriginationWithoutRetailInstallmentContract);

                            this.Bus.Value.PublishMessage(new SendFinancePlanConfirmationSimpleTermsEmailMessage
                            {
                                VpGuarantorId = fp.VpGuarantor.VpGuarantorId,
                                NumberMonthlyPayments = fpca.UserLocale?.NumberMonthlyPayments ?? 0
                            }).Wait();
                        }
                    }
                }, financePlan, financePlanCreditAgreementCollectionDto);

                uow.Commit();
            }

            this.SaveFinancePlan(financePlan);

            return financePlan;
        }

        public FinancePlan CreateReconfiguredFinancePlan(FinancePlanCalculationParameters financePlanCalculationParameters, FinancePlan parentFinancePlan, int loggedInVisitPayUserId, JournalEventHttpContextDto httpContext)
        {
            bool hasFinancialAssistance = parentFinancePlan.HasFinancialAssistance();

            IList<FinancePlanOffer> offers = this.GetOffersWithAmount(financePlanCalculationParameters, parentFinancePlan.FinancePlanId.ToListOfOne(), hasFinancialAssistance, true);
            FinancePlanOffer selectedOffer = SelectOfferWithPaymentAmount(financePlanCalculationParameters.MonthlyPaymentAmount, offers);

            FinancePlan financePlan = this.CreateInitialFinancePlan(financePlanCalculationParameters, selectedOffer, FinancePlanStatusEnum.PendingAcceptance, loggedInVisitPayUserId);
            this.AssignUniquePublicIdPhiToFinancePlan(financePlan);

            financePlan.OriginalFinancePlan = parentFinancePlan;
            financePlan.AddFinancePlanVisits(financePlanCalculationParameters, parentFinancePlan);
            financePlan.OriginalFinancePlan = parentFinancePlan;
            parentFinancePlan.ReconfiguredFinancePlan = financePlan;
            financePlan.UnpublishedFinancePlanAuditEvents.Add(AuditEventTypeFinancePlanEnum.Reconfiguration);

            this.SaveFinancePlan(financePlan);
            this.SaveFinancePlan(parentFinancePlan);

            this.LogCustomizationAndAcceptance(financePlanCalculationParameters.Guarantor, null, loggedInVisitPayUserId, false, httpContext, financePlan);

            return financePlan;
        }

        public FinancePlan CreateFinancePlan(FinancePlanCalculationParameters financePlanCalculationParameters, int loggedInVisitPayUserId, FinancePlanCreditAgreementCollectionDto financePlanCreditAgreementCollectionDto, JournalEventHttpContextDto httpContext)
        {
            FinancePlanOffer selectedOffer = this.SelectBestOfferWithPaymentAmount(financePlanCalculationParameters, null);

            FinancePlanStatusEnum initialStatus = financePlanCreditAgreementCollectionDto != null ? FinancePlanStatusEnum.GoodStanding : FinancePlanStatusEnum.PendingAcceptance;
            FinancePlan financePlan = this.CreateInitialFinancePlan(financePlanCalculationParameters, selectedOffer, initialStatus, loggedInVisitPayUserId);
            this.AssignUniquePublicIdPhiToFinancePlan(financePlan);

            financePlan.AddFinancePlanVisits(financePlanCalculationParameters);

            if (financePlan.CreatedVpStatement.IsGracePeriod)
            {
                financePlan.SetTheAmountDueForFinancePlan(financePlan.CreatedVpStatement, "Finance Plan Created");
            }

            this.SaveFinancePlan(financePlan);

            if (initialStatus == FinancePlanStatusEnum.GoodStanding)
            {
                this.OriginateFinancePlan(financePlan, financePlanCreditAgreementCollectionDto, loggedInVisitPayUserId);
            }

            this.LoggingAndCountersForAcceptance(financePlanCalculationParameters.Guarantor, financePlanCalculationParameters.CombineFinancePlans, financePlanCreditAgreementCollectionDto?.UserLocale?.TermsCmsVersionId, loggedInVisitPayUserId, httpContext, financePlan);

            return financePlan;
        }

        private void LoggingAndCountersForAcceptance(Guarantor guarantor, bool isCombineFinancePlan, int? termsCmsVersionId, int loggedInVisitPayUserId, JournalEventHttpContextDto httpContext, FinancePlan financePlan)
        {
            this.LogCustomizationAndAcceptance(guarantor, termsCmsVersionId, loggedInVisitPayUserId, false, httpContext, financePlan);

            this._metricsProvider.Value.Increment(NewFinancePlanMetricKey);

            if (isCombineFinancePlan)
            {
                this._metricsProvider.Value.Increment(NewCombinedFinancePlanMetricKey);
            }
        }

        public bool IsGuarantorEligibleToCombineFinancePlans(Guarantor guarantor, decimal totalBalance, string financePlanStateCode = null)
        {
            if (!this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.CombineFinancePlans))
            {
                return false;
            }

            if (totalBalance <= 0)
            {
                // no balance to finance
                return false;
            }

            List<FinancePlanStatusEnum> eligibleStatuses = new List<FinancePlanStatusEnum>
            {
                FinancePlanStatusEnum.GoodStanding,
                FinancePlanStatusEnum.PastDue,
                FinancePlanStatusEnum.UncollectableActive
            };

            bool hasFinancePlans = this.HasFinancePlans(guarantor, eligibleStatuses);

            if (hasFinancePlans && !string.IsNullOrEmpty(financePlanStateCode))
            {
                //If multi-state, check if there are finance plans for the state in question
                IList<FinancePlan> financePlans = this._financePlanRepository.Value.GetFinancePlans(guarantor.VpGuarantorId, eligibleStatuses);
                IList<FinancePlan> financePlansForState = this.FilterFinancePlansForState(financePlans, financePlanStateCode);
                hasFinancePlans = financePlansForState.Any();
            }

            return hasFinancePlans;
        }

        public IList<FinancePlan> FilterFinancePlansForState(IList<FinancePlan> financePlans, string financePlanStateCode)
        {
            IList<FinancePlan> financePlansForState = new List<FinancePlan>();

            if (!string.IsNullOrEmpty(financePlanStateCode))
            {
                IReadOnlyList<Facility> facilitiesForState = this._facilityService.Value.GetFacilitiesByStateCode(financePlanStateCode);
                List<int> facilityIdsForState = facilitiesForState.Select(f => f.FacilityId).ToList();
                foreach(FinancePlan financePlan in financePlans)
                {
                    //Check if FP belongs to state
                    bool matchesState = financePlan.FinancePlanVisits.Any(v => v.FinancePlanVisitVisit.FacilityId.HasValue && facilityIdsForState.Contains(v.FinancePlanVisitVisit.FacilityId.Value));
                    if (matchesState)
                    {
                        financePlansForState.Add(financePlan);
                    }
                }
            }
            else
            {
                financePlansForState = financePlans;
            }

            return financePlansForState;
        }

        private FinancePlan CreateInitialFinancePlan(
            FinancePlanCalculationParameters financePlanCalculationParameters,
            FinancePlanOffer selectedOffer,
            FinancePlanStatusEnum statusEnum,
            int visitPayUserId)
        {
            DateTime originationDate = this.GetOriginationDate(financePlanCalculationParameters.Guarantor, financePlanCalculationParameters.Statement);
            InterestCalculationMethodEnum interestCalculationMethod = financePlanCalculationParameters.InterestCalculationMethod;

            int amortizationDuration = this._amortizationService.Value.GetAmortizationDurationForAmounts(
                financePlanCalculationParameters.MonthlyPaymentAmount,
                financePlanCalculationParameters.Visits,
                selectedOffer.InterestRate,
                financePlanCalculationParameters.InterestFreePeriod,
                this._statementService.Value.GetNextPaymentDate(financePlanCalculationParameters.Guarantor),
                interestCalculationMethod);

            FinancePlan fp = new FinancePlan
            {
                CreatedVpStatement = financePlanCalculationParameters.Statement,
                CustomizedVisitPayUserId = visitPayUserId,
                FinancePlanOffer = selectedOffer,
                FinancePlanStatus = this.GetStatusWithEnum(statusEnum),
                FirstPaymentDueDate = financePlanCalculationParameters.FirstPaymentDueDate,
                InsertDate = DateTime.UtcNow,
                IsCombined = financePlanCalculationParameters.CombineFinancePlans,
                OfferCalculationStrategy = financePlanCalculationParameters.OfferCalculationStrategy,
                OriginalDuration = amortizationDuration,
                OriginalPaymentAmount = financePlanCalculationParameters.MonthlyPaymentAmount,
                OriginationDate = originationDate,
                PaymentAmount = financePlanCalculationParameters.MonthlyPaymentAmount,
                VpGuarantor = financePlanCalculationParameters.Guarantor,
                InterestCalculationMethod = interestCalculationMethod,
                OriginatedFinancePlanType = financePlanCalculationParameters.FinancePlanType
            };

            // Add uniqueId to the Finance Plan for offline guarantors
            if (this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.OfflineVisitPay) &&
                financePlanCalculationParameters.Guarantor.IsOfflineGuarantor())
            {
                bool isUnique = false;
                while (!isUnique)
                {
                    string uniqueId = RandomSourceSystemKeyGenerator.New("AAANNNNNN");
                    if (!this._financePlanRepository.Value.FinancePlanUniqueIdIsUnique(uniqueId))
                    {
                        continue;
                    }

                    fp.FinancePlanUniqueId = uniqueId;
                    isUnique = true;
                }
            }

            fp.SetInterestRate(selectedOffer.InterestRate, "Finance Plan Created");

            return fp;
        }
        
        public DateTime GetOriginationDate(Guarantor guarantor, VpStatement statement)
        {
            if (statement.IsGracePeriod)
            {
                return statement.PaymentDueDate;
            }

            if (guarantor.NextStatementDate.HasValue)
            {
                return guarantor.NextStatementDate.Value;
            }
            
            return this._statementDateService.Value.GetNextStatementDate(statement.PaymentDueDate, guarantor, statement.StatementDate);
        }

        public void AcceptTermsForReconfiguration(FinancePlan financePlanToAccept, int loggedInVisitPayUserId, FinancePlanCreditAgreementCollectionDto financePlanCreditAgreementCollectionDto, JournalEventHttpContextDto httpContext)
        {
            financePlanToAccept.InsertDate = DateTime.UtcNow;
            financePlanToAccept.OriginationDate = DateTime.UtcNow;
            this._financePlanRepository.Value.InsertOrUpdate(financePlanToAccept);
            this.OriginateFinancePlan(financePlanToAccept, financePlanCreditAgreementCollectionDto, loggedInVisitPayUserId);
            this.UpdateParentFinancePlanBecauseItWasReconfigured(financePlanToAccept);
            this.LoggingAndCountersForAcceptance(financePlanToAccept.VpGuarantor, false, financePlanCreditAgreementCollectionDto.UserLocale.TermsCmsVersionId, loggedInVisitPayUserId, httpContext, financePlanToAccept);
        }

        // todo: this or the one below it should get renamed
        public FinancePlan UpdateFinancePlan(FinancePlan financePlan)
        {
            this._financePlanRepository.Value.Update(financePlan);
            return financePlan;
        }

        public FinancePlan UpdateFinancePlan(FinancePlan financePlan, FinancePlanCalculationParameters financePlanCalculationParameters, bool isUpdateFromBalanceChange, int visitPayUserId, JournalEventHttpContextDto httpContext)
        {
            financePlan.CheckPaymentAmountWhenUpdatingFinancePlan(financePlanCalculationParameters.MonthlyPaymentAmount);

            FinancePlanOffer selectedOffer = this.SelectBestOfferWithPaymentAmount(financePlanCalculationParameters, financePlan.PaymentAmount);

            // if this is an automatic update because of a balance change only let the terms become better.
            if (isUpdateFromBalanceChange)
            {
                // if the new offer is not valid, cancel the pending plan
                if (selectedOffer == null)
                {
                    if (financePlan.IsNotOriginated())
                    {
                        this.LoggingService.Value.Info(() => $"{nameof(FinancePlanService)}::{nameof(this.UpdateFinancePlan)} - Pending Finance Plan ({financePlan.FinancePlanId}) cancelled due to invalid terms");
                        this.CancelFinancePlan(financePlan, "Pending Finance Plan terms invalidated");
                        return financePlan;
                    }
                }

                selectedOffer = MakeSureThatNewOfferIsBetter(financePlan, selectedOffer);
            }

            this.UpdateFinancePlanWithOffer(financePlan, selectedOffer, financePlanCalculationParameters.Statement, visitPayUserId, httpContext);

            return financePlan;
        }

        public FinancePlan UpdateReconfiguredFinancePlan(FinancePlan reconfiguredFinancePlan, FinancePlanCalculationParameters financePlanCalculationParameters, int actionVisitPayUserId, JournalEventHttpContextDto httpContext)
        {
            if (!reconfiguredFinancePlan.IsPendingReconfiguration())
            {
                return reconfiguredFinancePlan;
            }

            reconfiguredFinancePlan.CheckPaymentAmountWhenUpdatingFinancePlan(financePlanCalculationParameters.MonthlyPaymentAmount);

            FinancePlanOffer selectedOffer = this.GetOfferForReconfiguration(financePlanCalculationParameters, reconfiguredFinancePlan.OriginalFinancePlan, true);

            this.UpdateFinancePlanWithOffer(reconfiguredFinancePlan, selectedOffer, financePlanCalculationParameters.Statement, actionVisitPayUserId, httpContext);

            return reconfiguredFinancePlan;
        }

        public FinancePlan UpdateFinancePlanWithOffer(FinancePlan financePlan, FinancePlanOffer financePlanOffer, VpStatement currentStatement, int visitPayUserId, JournalEventHttpContextDto httpContext)
        {
            if (financePlan.IsNotOriginated())
            {
                financePlanOffer = financePlanOffer ?? financePlan.FinancePlanOffer;

                int amortizationDuration = this._amortizationService.Value.GetAmortizationDurationForAmounts(
                    financePlan.PaymentAmount,
                    financePlan.ActiveFinancePlanVisitBalances,
                    financePlanOffer?.InterestRate ?? 0m,
                    financePlan.FirstInterestPeriod(),
                    this._statementService.Value.GetNextPaymentDate(financePlan.VpGuarantor),
                    financePlan.InterestCalculationMethod);
                
                financePlan.OriginalDuration = amortizationDuration;
                financePlan.FinancePlanOffer = financePlanOffer;
                financePlan.CreatedVpStatement = currentStatement;

                if (financePlanOffer != null)
                {
                    if (financePlanOffer.InterestRate != financePlan.CurrentInterestRate)
                    {
                        financePlan.SetInterestRate(financePlanOffer.InterestRate, "Finance Plan Modified");
                    }
                }

                if (financePlan.CreatedVpStatement.IsGracePeriod)
                {
                    financePlan.SetTheAmountDueForFinancePlan(currentStatement);
                }
            }

            this._financePlanRepository.Value.InsertOrUpdate(financePlan);

            this.LogCustomizationAndAcceptance(financePlan.VpGuarantor, financePlan.TermsCmsVersionId, visitPayUserId, true, httpContext, financePlan);

            return financePlan;
        }

        public IList<FinancePlan> UpdateFinancePlansBecauseOfCombination(FinancePlan newCombinedFinancePlan)
        {
            IList<FinancePlan> financePlansToCancel = this._financePlanRepository.Value.GetAllOpenFinancePlans(newCombinedFinancePlan.VpGuarantor.VpGuarantorId)
                .Where(x => x.FinancePlanId != newCombinedFinancePlan.FinancePlanId)
                .ToList();

            foreach (FinancePlan financePlan in financePlansToCancel)
            {
                financePlan.CombinedFinancePlan = newCombinedFinancePlan;
                this._financePlanRepository.Value.InsertOrUpdate(financePlan);

                // set into canceled
                this.CancelFinancePlan(financePlan, "Finance plan has been combined.");

                //As of VP-6186 removed setting each FinancePlanVisit's SoftRemoveDate

                this._financePlanRepository.Value.InsertOrUpdate(financePlan);
            }

            return financePlansToCancel;
        }

        private void UpdateParentFinancePlanBecauseItWasReconfigured(FinancePlan reconfiguredFinancePlan)
        {
            FinancePlan parentFinancePlan = reconfiguredFinancePlan.OriginalFinancePlan;
            this.CancelFinancePlan(parentFinancePlan, "Finance plan has been reconfigured.");
            //As of VP-6186 removed setting each FinancePlanVisit's SoftRemoveDate

            parentFinancePlan.ReconfiguredFinancePlan = reconfiguredFinancePlan;
            this._financePlanRepository.Value.InsertOrUpdate(parentFinancePlan);
        }

        public void UpdatePendingFinancePlanOriginationDate(Guarantor guarantor, VpStatement statement)
        {
            DateTime originationDate = this.GetOriginationDate(guarantor, statement);
            
            IList<FinancePlan> pendingFinancePlans = this.GetAllFinancePlansForGuarantor(guarantor, new[] { FinancePlanStatusEnum.PendingAcceptance, FinancePlanStatusEnum.TermsAccepted });
            foreach (FinancePlan financePlan in pendingFinancePlans)
            {
                // while it's pending, it can originate anytime until the next payment due date
                financePlan.OriginationDate = originationDate;
                financePlan.FirstPaymentDueDate = originationDate;
                this._financePlanRepository.Value.InsertOrUpdate(financePlan);
            }
        }
        
        public void RescheduleRecurringFinancePlanPayments(Guarantor guarantor, DateTime revisedDueDate, int actionVisitPayUserId, int vpStatementId)
        {
            IList<FinancePlan> financePlans = this.GetAllOpenFinancePlansForGuarantor(guarantor, false);
            foreach (FinancePlan financePlan in financePlans)
            {
                if (!financePlan.AddFinancePlanAmountsDueReschedulePayment(vpStatementId, revisedDueDate, actionVisitPayUserId))
                {
                    continue;
                }

                this.CheckIfFinancePlansAreStillPastDueOrUncollectable(financePlan.ToListOfOne());
                this.UpdateFinancePlan(financePlan);
            }
        }

        public void UpdateFinancePlanDueToVisitChange(Visit visit, VisitStateEnum? newState = null)
        {
            if (visit == null)
            {
                return;
            }

            if (!this._financePlanRepository.Value.IsVisitOnAnyStatusFinancePlan(visit.VisitId))
            {
                return;
            }

            IList<FinancePlan> financePlans = this._financePlanRepository.Value.GetFinancePlansIncludingClosedFromVisits(new List<int> { visit.VisitId });
            if (financePlans.IsNullOrEmpty())
            {
                return;
            }

            bool checkState = (newState ?? visit.CurrentVisitState) == VisitStateEnum.Inactive;

            foreach (FinancePlan financePlan in financePlans)
            {
                FinancePlanVisit fpv = financePlan.ActiveFinancePlanVisits.FirstOrDefault(x => x.VisitId == visit.VisitId);
                if (fpv == null)
                {
                    continue;
                }
                
                bool checkBalance = fpv.PrincipalBalance <= 0m && fpv.UnclearedPaymentsSum >= 0m;

                if (checkState || checkBalance)
                {
                    bool isRemoved = false;

                    if (financePlan.IsReconfiguration() && financePlan.IsPendingAcceptance() && fpv.HardRemoveDate == null)
                    {
                        fpv.HardRemoveDate = DateTime.UtcNow;
                        isRemoved = true;
                    }
                    else if (financePlan.IsNotOriginated())
                    {
                        // Remove the visit in this case.
                        financePlan.FinancePlanVisits.Remove(fpv);
                    }
                    // It's an originated fp, and hasnt already been removed.
                    else if ((financePlan.IsActiveOriginated() || financePlan.IsClosed()) && fpv.HardRemoveDate == null)
                    {
                        fpv.HardRemoveDate = DateTime.UtcNow;
                        isRemoved = true;
                    }

                    if (isRemoved)
                    {
                        if (financePlan.IsActiveOriginated())
                        {
                            this._session.Transaction.RegisterPostCommitSuccessAction(v =>
                            {
                                this.Bus.Value.PublishMessage(new OutboundVisitMessage
                                {
                                    ActionDate = DateTime.UtcNow,
                                    BillingSystemId = Math.Abs(v.BillingSystem.BillingSystemId),
                                    SourceSystemKey = v.SourceSystemKey ?? v.MatchedSourceSystemKey,
                                    VpOutboundVisitMessageType = VpOutboundVisitMessageTypeEnum.VisitRemovedFromFinancePlan,
                                    VpVisitId = v.VisitId,
                                    FinancePlanClosedReasonEnum = v.BillingHold ? FinancePlanClosedReasonEnum.Suspended : FinancePlanClosedReasonEnum.FpClosed,
                                    IsAutoPay = visit.VPGuarantor.UseAutoPay,
                                    IsCallCenter = visit.VPGuarantor.IsOfflineGuarantor()
                                }).Wait();
                            }, visit);

                            if (!visit.BillingHold && fpv.PrincipalBalance > 0m)
                            {
                                // send finance plan visit not eligible email
                                // if billing hold, would send a different email from suspension action
                                this._session.Transaction.RegisterPostCommitSuccessAction(v =>
                                {
                                    this.Bus.Value.PublishMessage(new SendFinancePlanVisitNotEligibleEmailMessage
                                    {
                                        VpGuarantorId = visit.VpGuarantorId
                                    }).Wait();
                                }, visit);
                            }
                        }
                        else
                        {
                            this.LoggingService.Value.Warn(() => $"{nameof(this.UpdateFinancePlanDueToVisitChange)} - finance plan visit #{fpv.FinancePlanVisitId} is not on an active originated finance plan");
                        }

                        // VP-1689 - Full interest refund on suspension || recall
                        if (visit.BillingHold || !visit.VpEligible)
                        {
                            // VP-4825...but only when it has a balance
                            // a $0 balance and recall in the same changeset should not trigger a refund
                            if (visit.CurrentBalance > 0m)
                            {
                                this._session.Transaction.RegisterPostCommitSuccessAction(financePlanVisit =>
                                {
                                    this.Bus.Value.PublishMessage(new FinancePlanRefundInterestOnRecall
                                    {
                                        FinancePlanVisitId = financePlanVisit.FinancePlanVisitId,
                                        FinancePlanId = financePlanVisit.FinancePlan.FinancePlanId
                                    }).Wait();
                                }, fpv);
                            }
                        }
                    }
                }

                this.SaveFinancePlan(financePlan);

                if (financePlan.ReconfiguredFinancePlan != null)
                {
                    this.CheckIfReconfigurationIsStillValid(financePlan.ReconfiguredFinancePlan, "Reconfigured Terms Expired");
                }
            }

            this.CheckIfPendingFinancePlanIsValid(financePlans);
        }

        public void UpdateVisitsOnPendingFinancePlan(FinancePlan financePlan, FinancePlanCalculationParameters financePlanCalculationParameters)
        {
            if (!financePlan.IsPending())
            {
                return;
            }
            
            financePlan.FinancePlanVisits.Clear();
            financePlan.AddFinancePlanVisits(financePlanCalculationParameters);
        }

        public FinancePlan GetFinancePlan(Guarantor guarantor, int financePlanId)
        {
            return this._financePlanRepository.Value.GetFinancePlan(guarantor.VpGuarantorId, financePlanId);
        }

        public FinancePlan GetFinancePlan(int vpGuarantorId, int financePlanId)
        {
            return this._financePlanRepository.Value.GetFinancePlan(vpGuarantorId, financePlanId);
        }

        public FinancePlan GetFinancePlanById(int financePlanId)
        {
            return this._financePlanRepository.Value.GetById(financePlanId);
        }

        public FinancePlan GetFinancePlanByContractNumber(string contractNumber)
        {
	        FinancePlanCreditAgreement agreement = this._financePlanRepository.Value.GetFinancePlanCreditAgreementByContractNumber(contractNumber);
	        if (agreement == null || agreement.FinancePlan.FinancePlanOffer == null)
	        {
		        return null;
	        }
	        int financePlanOfferId = agreement.FinancePlan.FinancePlanOffer.FinancePlanOfferId;
			return this._financePlanRepository.Value.GetFinancePlanByOfferId(financePlanOfferId);
        }

		public FinancePlan GetFinancePlanByOfferId(int financePlanOfferId)
        {
            return this._financePlanRepository.Value.GetFinancePlanByOfferId(financePlanOfferId);
        }

        public IList<FinancePlan> GetFinancePlansById(IList<int> financePlanIds)
        {
            return this._financePlanRepository.Value.GetFinancePlansByIds(null, financePlanIds);
        }

        public FinancePlanResults GetFinancePlans(int vpGuarantorId, FinancePlanFilter filter, int batch, int batchSize)
        {
            return this._financePlanRepository.Value.GetFinancePlans(vpGuarantorId, filter, batch, batchSize);
        }

        public IReadOnlyList<FinancePlan> GetFinancePlans(IList<FinancePlanStatusEnum> financePlanStatuses)
        {
            return this._financePlanRepository.Value.GetFinancePlans(financePlanStatuses);
        }

        public IList<FinancePlan> GetFinancePlansAssociatedWithVisits(IList<Visit> visits)
        {
            IList<Visit> visitsOnFPs = this._financePlanRepository.Value.AreVisitsOnActiveOrPendingFinancePlan(visits);
            IList<FinancePlan> financePlanCache = new List<FinancePlan>();

            foreach (Visit visitOnFp in visitsOnFPs)
            {
                FinancePlan foundFinancePlan = null;
                if (financePlanCache.SelectMany(x => x.FinancePlanVisits).Any(x => x.VisitId == visitOnFp.VisitId))
                {
                    //Should be in the cache
                    foundFinancePlan = financePlanCache.FirstOrDefault(x => x.ActiveFinancePlanVisits.Any(y => y.VisitId == visitOnFp.VisitId));
                }
                else
                {
                    foundFinancePlan = this._financePlanRepository.Value.GetFinancePlanWithVisit(visitOnFp);
                    if (foundFinancePlan != null)
                    {
                        financePlanCache.Add(foundFinancePlan);
                    }
                }
            }

            return financePlanCache;
        }

        public FinancePlanCount GetFinancePlanTotals(int vpGuarantorId, FinancePlanFilter filter)
        {
            return this._financePlanRepository.Value.GetFinancePlanTotalsWithoutSum(vpGuarantorId, filter);
        }

        public IList<FinancePlan> GetAllOpenFinancePlansForGuarantor(Guarantor guarantor, bool considerConsolidation = false)
        {
            IList<Guarantor> guarantors = new List<Guarantor> { guarantor };

            if (considerConsolidation && guarantor.IsManaging())
            {
                guarantors.AddRange(guarantor.ActiveManagedConsolidationGuarantors.Select(x => x.ManagedGuarantor));
            }

            return this._financePlanRepository.Value.GetAllOpenFinancePlans(guarantors.Select(x => x.VpGuarantorId).ToList());
        }

        public IDictionary<int, FinancePlan> GetMostRecentFinancePlansForVisitsIds(IList<int> visitIds)
        {
            return this._financePlanRepository.Value.GetMostRecentFinancePlanForVisitIds(visitIds);
        }

        public bool AnyOpenFinancePlansForGuarantor(Guarantor guarantor, bool considerConsolidation = false)
        {
            IList<Guarantor> guarantors = new List<Guarantor> { guarantor };

            if (considerConsolidation && guarantor.IsManaging())
            {
                guarantors.AddRange(guarantor.ActiveManagedConsolidationGuarantors.Select(x => x.ManagedGuarantor));
            }

            return this._financePlanRepository.Value.HaveOpenFinancePlans(guarantors.Select(x => x.VpGuarantorId).ToList());
        }

        private IList<Guarantor> GetGuarantorList(Guarantor guarantor, bool considerConsolidation = false)
        {
            IList<Guarantor> guarantors = new List<Guarantor> { guarantor };
            if (considerConsolidation && guarantor.IsManaging())
            {
                guarantors.AddRange(guarantor.ActiveManagedConsolidationGuarantors.Select(x => x.ManagedGuarantor));
            }
            return guarantors;
        }

        public IList<FinancePlan> GetAllPendingAcceptanceFinancePlansForGuarantor(Guarantor guarantor, bool isReconfiguration = false, bool considerConsolidation = false)
        {
            IList<Guarantor> guarantors = this.GetGuarantorList(guarantor, considerConsolidation);
            return this._financePlanRepository.Value.GetAllPendingAcceptanceFinancePlans(guarantors.Select(x => x.VpGuarantorId).ToList(), isReconfiguration);
        }

        public IList<Guarantor> GetFirstPendingAcceptanceFinancePlanClientOnlyOfferGuarantor(Guarantor guarantor, bool isReconfiguration = false, bool considerConsolidation = false)
        {
            IList<Guarantor> guarantors = this.GetGuarantorList(guarantor, considerConsolidation);
            return this._financePlanRepository.Value.GetFirstPendingAcceptanceFinancePlanClientOnlyOfferGuarantor(guarantors.Select(x => x.VpGuarantorId).ToList(), isReconfiguration);

        }

        public IList<Guarantor> GetFirstPendingAcceptanceFinancePlanPatientOfferGuarantor(Guarantor guarantor, bool isReconfiguration = false, bool considerConsolidation = false)
        {
            IList<Guarantor> guarantors = this.GetGuarantorList(guarantor, considerConsolidation);
            return this._financePlanRepository.Value.GetFirstPendingAcceptanceFinancePlanPatientOfferGuarantor(guarantors.Select(x => x.VpGuarantorId).ToList(), isReconfiguration);
        }

        public IList<FinancePlan> GetAllPendingFinancePlansForGuarantor(Guarantor guarantor)
        {
            return this._financePlanRepository.Value.GetAllPendingFinancePlans(guarantor.VpGuarantorId);
        }

        public IList<FinancePlan> GetAllActiveFinancePlansForGuarantor(Guarantor guarantor)
        {
            return this._financePlanRepository.Value.GetAllActiveFinancePlans(guarantor.VpGuarantorId);
        }

        public IList<int> GetAllActiveFinancePlanIds(int vpGuarantorId)
        {
            return this._financePlanRepository.Value.GetAllActiveFinancePlanIds(vpGuarantorId);
        }

        public IList<FinancePlan> GetAllFinancePlansForGuarantor(Guarantor guarantor, IList<FinancePlanStatusEnum> financePlanStatuses)
        {
            return this._financePlanRepository.Value.GetAllFinancePlans(guarantor.VpGuarantorId, financePlanStatuses);
        }

        public int GetFinancePlanCount(Guarantor guarantor, IList<FinancePlanStatusEnum> financePlanStatuses)
        {
            return this._financePlanRepository.Value.GetFinancePlanCount(guarantor.VpGuarantorId, financePlanStatuses);
        }

        public int GetActiveFinancePlanCount(int vpGuarantorId)
        {
            return this._financePlanRepository.Value.GetActiveFinancePlanCount(vpGuarantorId);
        }

        public decimal GetTotalPaymentAmountForOpenFinancePlans(int vpGuarantorId)
        {
            return this._financePlanRepository.Value.GetTotalPaymentAmountForOpenFinancePlans(vpGuarantorId);
        }

        public bool HasFinancePlans(Guarantor guarantor, IList<FinancePlanStatusEnum> financePlanStatuses)
        {
            return this._financePlanRepository.Value.HasFinancePlans(guarantor.VpGuarantorId, financePlanStatuses);
        }

        public IList<FinancePlanStatus> GetFinancePlanStatuses()
        {
            return this._financePlanRepository.Value.GetFinancePlanStatuses();
        }

        public IList<FinancePlanType> GetFinancePlanTypes()
        {
            return this._financePlanRepository.Value.GetFinancePlanTypes();
        }

        public IReadOnlyList<ScheduledPaymentResult> GetFinancePlanScheduledPayments(Guarantor guarantor)
        {
            if (guarantor.IsOfflineGuarantor() && !guarantor.UseAutoPay)
            {
                // if guarantor is offline type, and does not use autopay, do not display AmountDue as a scheduled payment
                return new List<ScheduledPaymentResult>();
            }

            IList<FinancePlan> financePlans = this.GetAllOpenFinancePlansForGuarantor(guarantor);
            if (!financePlans.Any())
            {
                return new List<ScheduledPaymentResult>();
            }

            DateTime? paymentDate = financePlans
                .Select(x => x.MaxScheduledPaymentDate())
                .OrderByDescending(x => x)
                .FirstOrDefault();

            decimal paymentAmount = financePlans.Sum(x => x.AmountDue);
            if (!paymentDate.HasValue || paymentAmount <= 0m)
            {
                return new List<ScheduledPaymentResult>();
            }

            bool isNotClientApplication = this.ApplicationSettingsService.Value.Application.Value != ApplicationEnum.Client;
            bool scheduledPaymentIsInThePast = (paymentDate.Value.Date < DateTime.UtcNow.Date);
            bool shouldHidePastScheduledPayments = isNotClientApplication && scheduledPaymentIsInThePast;
            // We don't want to show a scheduled payment in the past VP-3376 (for patient)
            // If it is the client application we want to show them to allow for cancelling, which relieves amount due, which reduces the bucket (VP-7078)
            if (shouldHidePastScheduledPayments)
            {
                return new List<ScheduledPaymentResult>();
            }

            IList<ScheduledPaymentResult> list = new ScheduledPaymentResult
            {
                MadeForVpGuarantorId = guarantor.VpGuarantorId,
                MadeByVpGuarantorId = guarantor.IsManaged() ? guarantor.ManagingGuarantorId : guarantor.VpGuarantorId,
                PaymentId = null,
                PaymentType =  PaymentTypeEnum.RecurringPaymentFinancePlan,
                ScheduledPaymentAmount = paymentAmount,
                ScheduledPaymentDate = paymentDate.Value
            }.ToListOfOne();
            
            return (IReadOnlyList<ScheduledPaymentResult>)list;
        }

        public void CheckIfFinancePlansAreStillPastDueOrUncollectable(IList<FinancePlan> financePlans)
        {
            if (financePlans.IsNullOrEmpty())
            {
                return;
            }

            foreach (FinancePlan financePlan in financePlans)
            {
                if (this.ShouldBeInActiveUncollectable(financePlan))
                {
                    this.SetFinancePlanIntoActiveUncollectable(financePlan);
                }
                else if (this.ShouldBeInPastDue(financePlan))
                {
                    this.SetFinancePlanIntoPastDue(financePlan, true);
                }
                else if (financePlan.ShouldBeInCurrent())
                {
                    this.SetFinancePlanIntoCurrent(financePlan);
                }
            }
        }

        public bool CheckIfFinancePlansAreUncollectable(IList<FinancePlan> financePlans)
        {
            if (financePlans.IsNullOrEmpty())
            {
                return false;
            }

            bool wasSetUncollectable = false;
            foreach (FinancePlan financePlan in financePlans)
            {
                if (!this.ShouldBeInActiveUncollectable(financePlan))
                {
                    continue;
                }

                if (this.SetFinancePlanIntoActiveUncollectable(financePlan) || financePlan.IsUncollectable())
                {
                    wasSetUncollectable = true;
                }
            }

            return wasSetUncollectable;
        }

        public void CheckIfFinancePlansShouldBeClosedUncollectable(IList<FinancePlan> financePlans)
        {
            if (!financePlans.IsNotNullOrEmpty())
            {
                return;
            }

            foreach (FinancePlan financePlan in financePlans)
            {
                if (this.CheckIfFinancePlanShouldBeClosedUncollectable(financePlan))
                {
                    financePlan.SetFinancePlanStatus(this.GetStatusWithEnum(FinancePlanStatusEnum.UncollectableClosed), string.Empty);
                    financePlan.OnFinancePlanClose("Finance Plan Closed - Closed Uncollectable");
                    this._financePlanRepository.Value.InsertOrUpdate(financePlan);
                }
            }
        }

        public bool CheckIfFinancePlanShouldBeClosedUncollectable(FinancePlan financePlan, DateTime? activeUncollectableDate = null)
        {
            if (!financePlan.IsActiveUncollectable())
            {
                return false;
            }

            List<FinancePlanVisit> visits = financePlan.ActiveFinancePlanVisits.ToList();
            if (visits.Any() && visits.All(x => x.FinancePlanVisitVisit.VpEligible == false))
            {
                return true;
            }

            return false;
        }

        public bool CheckIfFinancePlanShouldBeActiveUncollectable(FinancePlan financePlan)
        {
            if (financePlan == null)
            {
                return false;
            }

            bool shouldBeInActiveUncollectable = this.ShouldBeInActiveUncollectable(financePlan);
            return shouldBeInActiveUncollectable && this.SetFinancePlanIntoActiveUncollectable(financePlan);
        }

        public void CheckIfFinancePlanIsValid(IList<Guarantor> guarantors)
        {
            DateTime now = DateTime.UtcNow;

            foreach (Guarantor guarantor in guarantors)
            {
                IList<FinancePlan> financePlans = this.GetAllActiveFinancePlansForGuarantor(guarantor);
                if (!financePlans.Any())
                {
                    continue;
                }

                foreach (FinancePlan financePlan in financePlans)
                {
                    this._amortizationService.Value.GetAmortizationMonthsForOriginatedFinancePlan(financePlan, now, this.GetFinancePlanActiveVisitsBalancesDaily(financePlan), logException: true);
                }
            }
        }

        public void CheckIfReconfigurationIsStillValid(IList<Guarantor> guarantors)
        {
            foreach (Guarantor guarantor in guarantors)
            {
                IList<FinancePlan> financePlans = this.GetAllPendingAcceptanceFinancePlansForGuarantor(guarantor, true);
                if (!financePlans.Any())
                {
                    continue;
                }

                foreach (FinancePlan financePlan in financePlans.Where(x => x.OriginalFinancePlan != null))
                {
                    this.CheckIfReconfigurationIsStillValid(financePlan, "Reconfigured Terms Expired");
                }
            }
        }

        private void CheckIfFinancePlanShouldClose(FinancePlan financePlan)
        {
            if (financePlan == null)
            {
                return;
            }

            bool setToClosed = false;

            if (financePlan.IsActiveOriginated() &&
                financePlan.FinancePlanVisits.All(x =>
                        x.PrincipalBalance <= 0m
                        && x.UnclearedPaymentsSum == 0)
                )
            {
                //Close the FP if the balance is 0
                //Closed PIF
                financePlan.FinancePlanStatus = this.GetStatusWithEnum(FinancePlanStatusEnum.PaidInFull);
                financePlan.OnFinancePlanClose("Finance Plan Closed - PIF");
                setToClosed = true;
            }
            else if (financePlan.IsActiveOriginated() && financePlan.ActiveFinancePlanVisits.All(x => x.CurrentVisitState == VisitStateEnum.Inactive))
            {
                //If none of the visits are active but there is a balance left
                //Closed NonPif
                if (this.CheckIfFinancePlanShouldBeClosedUncollectable(financePlan))
                {
                    financePlan.FinancePlanStatus = this.GetStatusWithEnum(FinancePlanStatusEnum.UncollectableClosed);
                    financePlan.OnFinancePlanClose("Finance Plan Closed - Closed Uncollectable");
                }
                else
                {
                    financePlan.FinancePlanStatus = this.GetStatusWithEnum(FinancePlanStatusEnum.NonPaidInFull);
                    financePlan.OnFinancePlanClose("Finance Plan Closed - Non-PIF");
                }
                setToClosed = true;
            }
            else if (financePlan.IsNotOriginated() && !financePlan.HasActiveVisits())
            {
                financePlan.FinancePlanStatus = this.GetStatusWithEnum(FinancePlanStatusEnum.Canceled);
                financePlan.OnFinancePlanClose("Finance Plan Closed - Cancelled");
                setToClosed = true;
            }

            if (financePlan.IsClosed() && setToClosed)
            {
                this._session.Transaction.RegisterPostCommitSuccessAction(fp =>
                {
                    this.Bus.Value.PublishMessage(new FinancePlanClosedMessage
                    {
                        FinancePlanId = fp.FinancePlanId,
                        VpGuarantorId = fp.VpGuarantor.VpGuarantorId
                    }).Wait();
                }, financePlan);
            }
        }

        private void CheckIfReconfigurationIsStillValid(FinancePlan financePlan, string reasonIfNotValid)
        {
            if (!financePlan.IsPendingAcceptance())
            {
                return;
            }

            if (financePlan.OriginalFinancePlan.CurrentFinancePlanBalance < financePlan.PaymentAmount || !financePlan.OriginalFinancePlan.IsActive())
            {
                this.CancelReconfiguredFinancePlan(financePlan, reasonIfNotValid, SystemUsers.SystemUserId, null);
            }
        }

        public void CheckIfPendingFinancePlanHasEligibleVisits(IList<FinancePlan> financePlans)
        {
            foreach (FinancePlan financePlan in financePlans.Where(x => x.IsPendingAcceptance()))
            {
                if (financePlan.ActiveFinancePlanVisits.All(x => x.FinancePlanVisitVisit.VpEligible == false))
                {
                    financePlan.SetFinancePlanStatus(this.GetStatusWithEnum(FinancePlanStatusEnum.Canceled), string.Empty);
                    this._financePlanRepository.Value.InsertOrUpdate(financePlan);
                }
            }
        }

        public void CheckIfPendingFinancePlanIsValid(IList<FinancePlan> financePlans)
        {
            if (financePlans.IsNullOrEmpty())
            {
                return;
            }

            foreach (FinancePlan financePlan in financePlans.GroupBy(x => x.FinancePlanId).Select(x => x.FirstOrDefault()))
            {
                if (financePlan == null)
                {
                    continue;
                }

                if (financePlan.IsNotOriginated())
                {
                    if (financePlan.CurrentFinancePlanBalance < financePlan.PaymentAmount)
                    {
                        this.LoggingService.Value.Info(() => $"{nameof(FinancePlanService)}::{nameof(this.CheckIfPendingFinancePlanIsValid)} - Pending Finance Plan ({financePlan.FinancePlanId}) cancelled with monthly payment amount greater than balance ({financePlan.PaymentAmount}, {financePlan.CurrentFinancePlanBalance})");
                        this.CancelFinancePlan(financePlan, "Pending Finance Plan balance is less than the payment amount");
                    }
                    else if (financePlan.HasPositiveVisitBalanceDifference())
                    {
                        this.LoggingService.Value.Info(() => $"{nameof(FinancePlanService)}::{nameof(this.CheckIfPendingFinancePlanIsValid)} - Pending Finance Plan ({financePlan.FinancePlanId}) cancelled due to PositiveVisitBalanceDifference ({financePlan.PositiveVisitBalanceDifferenceSum})");
                        this.CancelFinancePlan(financePlan, "Pending Finance Plan balance increased");
                    }
                    else
                    {
                        this.UpdateFinancePlan(financePlan, new FinancePlanCalculationParameters(financePlan), true, SystemUsers.SystemUserId, null);
                    }
                }
            }
        }

        public void UpdatePendingFinancePlanPrincipalAmount(IList<PaymentAllocation> paymentAllocations)
        {
            if (paymentAllocations.IsNullOrEmpty())
            {
                return;
            }

            IList<PaymentAllocation> allocationsWithoutFinancePlans = paymentAllocations
                .Where(x => x.PaymentVisit != null)
                .Where(x => x.FinancePlanId == null)
                .ToList();

            IEnumerable<int> visitIds = allocationsWithoutFinancePlans.Select(x => x.PaymentVisit.VisitId);
            IList<FinancePlan> associatedPendingFinancePlans = this._financePlanRepository.Value.GetFinancePlansIncludingClosedFromVisits(visitIds.ToList())
                .Where(x => x.IsNotOriginated())
                .ToList();

            if (associatedPendingFinancePlans.IsNullOrEmpty())
            {
                return;
            }

            IList<FinancePlan> foundPendingFinancePlans = new List<FinancePlan>();
            foreach (PaymentAllocation paymentAllocation in allocationsWithoutFinancePlans)
            {
                FinancePlanVisit financePlanVisit = associatedPendingFinancePlans.SelectMany(x => x.FinancePlanVisits).FirstOrDefault(x => x.VisitId == paymentAllocation.PaymentVisit.VisitId);
                if (financePlanVisit == null)
                {
                    continue;
                }

                financePlanVisit.FinancePlan.AddFinancePlanVisitPrincipalAmountPayment(financePlanVisit, paymentAllocation);
                foundPendingFinancePlans.Add(financePlanVisit.FinancePlan);
            }

            this.CheckIfPendingFinancePlanIsValid(foundPendingFinancePlans);
        }

        public void PopulateFinancePlanIdAndDurationForAllocationMessages(IList<PaymentAllocationMessage> paymentAllocationMessages)
        {
            IList<FinancePlan> allFinancePlans = this._financePlanRepository.Value.GetFinancePlansByIds(null, paymentAllocationMessages
                .Where(x => x.VpFinancePlanId.HasValue)
                .Select(x => x.VpFinancePlanId.Value)
                .Distinct()
                .ToList());

            foreach (FinancePlan financePlan in allFinancePlans.Where(x => x.IsActiveOriginated()))
            {
                int? totalCurrentDuration = this._amortizationService.Value.CalculateNumberOfTotalCurrentDuration(financePlan, this.GetFinancePlanActiveVisitsBalancesDaily(financePlan));

                //Find the associated PaymentAllocationMessages
                //Now that we have the FP and the totalCurrentDuration
                IEnumerable<PaymentAllocationMessage> foundPaymentAllocations = paymentAllocationMessages.Where(x => x.VpFinancePlanId == financePlan.FinancePlanId);
                foreach (PaymentAllocationMessage foundPaymentAllocation in foundPaymentAllocations)
                {
                    foundPaymentAllocation.VpFinancePlanOriginalDuration = financePlan.OriginalDuration;
                    foundPaymentAllocation.VpFinancePlanCurrentTotalDuration = totalCurrentDuration;
                }
            }
        }

        public void SendUncollectableMessage(FinancePlan financePlan)
        {
            OutboundVisitMessageList list = new OutboundVisitMessageList
            {
                Messages = new List<OutboundVisitMessage>()
            };

            bool isCallCenter = financePlan.VpGuarantor.IsOfflineGuarantor();
            bool isAutoPay = financePlan.VpGuarantor.UseAutoPay;

            foreach (FinancePlanVisit financePlanVisit in financePlan.ActiveFinancePlanVisits)
            {
                list.Messages.Add(new OutboundVisitMessage
                {
                    ActionDate = DateTime.UtcNow,
                    BillingSystemId = financePlanVisit.FinancePlanVisitVisit.BillingSystemId,
                    SourceSystemKey = financePlanVisit.FinancePlanVisitVisit.SourceSystemKey,
                    VpOutboundVisitMessageType = VpOutboundVisitMessageTypeEnum.UncollectableNotification,
                    VpVisitId = financePlanVisit.VisitId,
                    IsCallCenter = isCallCenter,
                    IsAutoPay = isAutoPay
                });
            }

            this._session.RegisterPostCommitSuccessAction(p1 => { this.Bus.Value.PublishMessage(p1).Wait(); }, list);
        }

        private void SaveFinancePlan(FinancePlan financePlan)
        {
            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                this.CheckIfFinancePlanShouldClose(financePlan);
                this._financePlanRepository.Value.InsertOrUpdate(financePlan);
                uow.Commit();
            }
        }

        #region statusing

        private FinancePlanStatus GetStatusWithEnum(FinancePlanStatusEnum status)
        {
            return this._financePlanRepository.Value.GetFinancePlanStatuses().FirstOrDefault(x => x.FinancePlanStatusEnum == status);
        }

        private bool ShouldBeInPastDue(FinancePlan financePlan)
        {
            int uncollectableMaxAgingCountFinancePlans = this.Client.Value.UncollectableMaxAgingCountFinancePlans;
            return financePlan.Buckets != null &&
                financePlan.Buckets.ContainsKey(1)
                && !financePlan.Buckets.ContainsKey(uncollectableMaxAgingCountFinancePlans);
        }

        private bool ShouldBeInActiveUncollectable(FinancePlan financePlan)
        {
            int uncollectableMaxAgingCountFinancePlans = this.Client.Value.UncollectableMaxAgingCountFinancePlans;
            return financePlan.Buckets.ContainsKey(uncollectableMaxAgingCountFinancePlans);
        }

        private bool SetFinancePlanIntoActiveUncollectable(FinancePlan financePlan)
        {
            //Only want active originated 
            //Dont want any closed 
            //Uncollectable is greater than pastdue so we dont want to change those back
            //If it's already past due do nothing
            if (financePlan.IsActiveOriginated()
                && !financePlan.IsClosed()
                && !financePlan.IsActiveUncollectable())
            {
                this._session.Transaction.RegisterPostCommitSuccessAction(p1 =>
                {
                    this.Bus.Value.PublishMessage(new SendPendingUncollectableEmailMessage
                    {
                        VpGuarantorId = p1.VpGuarantorId,
                        VisitIds = financePlan.ActiveFinancePlanVisits.Select(c=>c.VisitId).ToList()
                    }).Wait();
                }, financePlan.VpGuarantor);

                financePlan.FinancePlanStatus = this.GetStatusWithEnum(FinancePlanStatusEnum.UncollectableActive);
                this._financePlanRepository.Value.InsertOrUpdate(financePlan);
                return true;
            }
            return false;
        }

        private bool SetFinancePlanIntoPastDue(FinancePlan financePlan, bool overridePrecidenceCheck = false)
        {
            //Only want active originated 
            //Dont want any closed 
            //Uncollectable is greater than pastdue so we dont want to change those back
            //If it's already past due do nothing
            if (financePlan.IsActiveOriginated() && !financePlan.IsClosed() && ((!(financePlan.IsActiveUncollectable() || financePlan.IsClosedUncollectable()) || overridePrecidenceCheck) && !financePlan.IsPastDue()))
            {
                financePlan.FinancePlanStatus = this.GetStatusWithEnum(FinancePlanStatusEnum.PastDue);
                this._financePlanRepository.Value.InsertOrUpdate(financePlan);
                return true;
            }
            return false;
        }

        private bool SetFinancePlanIntoCurrent(FinancePlan financePlan)
        {
            //Only want active originated 
            //Dont want any closed 
            if (financePlan.IsActiveOriginated()
                && !financePlan.IsClosed()
                && !financePlan.FinancePlanStatus.IsActiveGoodStanding())
            {
                financePlan.FinancePlanStatus = this.GetStatusWithEnum(FinancePlanStatusEnum.GoodStanding);
                this._financePlanRepository.Value.InsertOrUpdate(financePlan);
                return true;
            }
            return false;
        }

        public void SetFinancePlanIntoPendingAcceptance(FinancePlan financePlan, string reason)
        {
            if (financePlan.IsNotOriginated() && !financePlan.IsPendingAcceptance())
            {
                financePlan.SetFinancePlanStatus(this.GetStatusWithEnum(FinancePlanStatusEnum.PendingAcceptance), reason);
            }
        }

        #endregion

        private IList<FinancePlan> GetApplicableFinancePlansForOffer(Guarantor guarantor, HashSet<int> excludeList)
        {
            IList<FinancePlan> financePlans = this.GetAllOpenFinancePlansForGuarantor(guarantor);
            return financePlans.Where(x => !excludeList.Contains(x.FinancePlanId)).ToList();
        }

        public IList<int> GetFinancePlanGuarantorIdsByBucketAndStatementDueDate(int bucketNumber, DateTime statementDueDate)
        {
            IList<int> vpGuarantorIds = new List<int>();
            //DB Value of CurrentBucket may not be correct, so we need to compare it to the domain object value (as it is dynamically calculated).
            foreach (int vpGuarantorId in this._financePlanRepository.Value.GetFinancePlanGuarantorIdsByStatementDueDate(statementDueDate))
            {
                IEnumerable<FinancePlan> financePlans = this._financePlanRepository.Value.GetAllActiveFinancePlans(vpGuarantorId);
                vpGuarantorIds.AddRange(financePlans.Where(x => x.CurrentBucket == bucketNumber).Select(x => x.VpGuarantor.VpGuarantorId));
            }

            return vpGuarantorIds.Distinct().ToList();
        }

        public IList<FinancePlan> GetFinancePlansByBucketAndStatementDueDate(int bucketNumber, DateTime statementDueDate)
        {
            IList<FinancePlan> financePlans = new List<FinancePlan>();
            //DB Value of CurrentBucket may not be correct, so we need to compare it to the domain object value (as it is dynamically calculated).
            foreach (int vpGuarantorId in this._financePlanRepository.Value.GetFinancePlanGuarantorIdsByStatementDueDate(statementDueDate))
            {
                IEnumerable<FinancePlan> guarantorFinancePlans = this._financePlanRepository.Value.GetAllActiveFinancePlans(vpGuarantorId);
                financePlans.AddRange(guarantorFinancePlans.Where(x => x.CurrentBucket == bucketNumber));
            }

            return financePlans.Distinct().ToList();
        }

        public void RecalculateFinancePlanDueToFinancialAssistanceFlagChange(int vpGuarantorId, int visitId)
        {
            IList<FinancePlan> financePlans = this._financePlanRepository.Value.GetFinancePlansIncludingClosedFromVisits(new List<int> { visitId })
                .Where(x => x.VpGuarantor.VpGuarantorId == vpGuarantorId)
                .ToList();

            foreach (FinancePlan financePlan in financePlans)
            {
                if (financePlan.FinancePlanOffer.IsCharity)
                {
                    continue;
                }

                FinancePlanVisit financePlanVisit = financePlan.FinancePlanVisits.FirstOrDefault(x => x.VisitId == visitId);
                if (financePlanVisit == null)
                {
                    continue;
                }

                // recaculate the finance plan
                FinancePlanOffer financePlanOffer = this._financePlanOfferService.Value.UpdateFinancePlanToFinancialAssistanceOffer(financePlan);
                if (financePlanOffer == null)
                {
                    continue;
                }

                //assign new offer to finance plan
                financePlan.FinancePlanOffer = financePlanOffer;
                financePlan.SetFinancialAssistanceInterestRate(financePlanOffer.InterestRate);

                this._financePlanRepository.Value.InsertOrUpdate(financePlan);
            }
        }

        #region Buckets / FinancePlanAmountDue

        public void AddTheAmountDueForFinancePlan(FinancePlan financePlan, VpStatement currentStatement, string comment = null)
        {
            financePlan.SetTheAmountDueForFinancePlan(currentStatement, comment);
            this.SaveFinancePlan(financePlan);
        }

        public void RemovePastDueBucket(FinancePlan financePlan, int bucketNumber, string comment, int? visitPayUserId)
        {
            //always remove the most immediate past due bucket
            if (!financePlan.Buckets.ContainsKey(bucketNumber))
            {
                return;
            }

            Tuple<decimal, int?> bucket = financePlan.Buckets[bucketNumber];
            // TODO: need to get verification on this, but setting the VpStatementId to null here
            // in order to satisfy existing Bucket logic 
            financePlan.AddFinancePlanAmountsDueOffset(decimal.Negate(bucket.Item1), DateTime.UtcNow, null, comment, visitPayUserId);
        }

        public IList<FinancePlanAmountDueInfo> GetFinancePlanAmountDueInfoWithRecurringPaymentDueExactDate(DateTime dateToCheck)
        {
            return this._financePlanReadOnlyRepository.Value.GetFinancePlanAmountDueInfoForDateWithoutAPaymentExactDate(dateToCheck);
        }

        public IList<FinancePlanAmountDueInfo> GetFinancePlanAmountDueInfoWithRecurringPaymentDue(DateTime dateToCheck)
        {
            return this._financePlanReadOnlyRepository.Value.GetFinancePlanAmountDueInfoForDateWithoutAPayment(dateToCheck);
        }

        public bool DoesGuarantorHaveRecurringPaymentDue(DateTime dateToCheck, int vpGuarantorId)
        {
            return this._financePlanReadOnlyRepository.Value.GetFinancePlanAmountDueInfoForDateWithoutAPayment(dateToCheck, vpGuarantorId)
                .Any(x=>x.VpGuarantorId == vpGuarantorId);
        }

        public IList<FinancePlan> GetFinancePlansDueForRecurringPaymentByGuarantor(DateTime dateToProcess, int vpGuarantorId)
        {
            IList<FinancePlanAmountDueInfo> financePlanAmountDueInfoList = this._financePlanReadOnlyRepository.Value.GetFinancePlanAmountDueInfoForDateWithoutAPayment(dateToProcess, vpGuarantorId);
            List<int> financePlanIds = financePlanAmountDueInfoList.Select(x => x.FinancePlanId).Distinct().ToList();
            return this.GetListOfFinancePlansById(financePlanIds);
        }

        public IList<FinancePlan> GetFinancePlansWithAmountDueByGuarantor(DateTime dateToProcess, int vpGuarantorId)
        {
            IList<int> financePlanIds = this._financePlanRepository.Value.GetFinancePlanIdsWithAmountDueForDate(dateToProcess, vpGuarantorId);
            return this.GetListOfFinancePlansById(financePlanIds);
        }

        private IList<FinancePlan> GetListOfFinancePlansById(IList<int> financePlanIds)
        {
            IList<FinancePlan> financePlans = new List<FinancePlan>();
            foreach (int financePlanId in financePlanIds)
            {
                financePlans.Add(this._financePlanRepository.Value.GetById(financePlanId));
            }
            return financePlans;
        }
        
        public void AddExternalPrincipalAmountDue(decimal amount, int visitId, int visitTransactionId)
        {
            FinancePlan financePlan = this.GetFinancePlansAssociatedWithVisits(new List<Visit> { new Visit { VisitId = visitId } }).FirstOrDefault();

            FinancePlanVisit financePlanVisit = financePlan?.FinancePlanVisits?.FirstOrDefault(x => x.FinancePlanVisitVisit.VisitId == visitId);
            if (financePlanVisit == null)
            {
                return;
            }

            financePlan.AddFinancePlanVisitPrincipalAmountExternalPayment(financePlanVisit, amount, visitTransactionId);
        }

        #endregion
        
        #region Cancel

        public void CancelReconfiguredFinancePlan(FinancePlan reconfiguredFinancePlan, string reason, int actionVisitPayUserId, JournalEventHttpContextDto context)
        {
            if (!reconfiguredFinancePlan.IsPendingReconfiguration())
            {
                return;
            }

            // update parent plan
            reconfiguredFinancePlan.OriginalFinancePlan.ReconfiguredFinancePlan = null;
            this._financePlanRepository.Value.InsertOrUpdate(reconfiguredFinancePlan.OriginalFinancePlan);

            // update this plan
            reconfiguredFinancePlan.OriginalFinancePlan = null;
            reconfiguredFinancePlan.OriginationDate = null;
            this.CancelFinancePlanInternal(reconfiguredFinancePlan, reason);

            this._financePlanRepository.Value.InsertOrUpdate(reconfiguredFinancePlan);

            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(context, actionVisitPayUserId);
            int vpGuarantorId = reconfiguredFinancePlan.VpGuarantor.VpGuarantorId;
            string username = reconfiguredFinancePlan.VpGuarantor.User.UserName;

            this._userJournalEventService.Value.LogCancelReconfiguredFinancePlan(journalEventUserContext, vpGuarantorId, username, reconfiguredFinancePlan.FinancePlanId, reconfiguredFinancePlan.FinancePlanOfferSetTypeId);
        }

        public void CancelPendingTermsAcceptanceFinancePlansForGuarantor(Guarantor guarantor, DateTime paymentDueDate, DateTime originationDate)
        {
            IList<FinancePlan> financePlansPendingTermsAcceptance = this._financePlanRepository.Value.GetAllPendingAcceptanceFinancePlans(guarantor.VpGuarantorId, originationDate);
            if (financePlansPendingTermsAcceptance == null)
            {
                return;
            }

            foreach (FinancePlan financePlan in financePlansPendingTermsAcceptance)
            {
                bool isCancelled = false;
                DateTime minimumResponseDate = financePlan.InsertDate.Date.AddDays(this.Client.Value.CreditAgreementMinResponsePeriod);
                
                if (financePlan.InsertDate.Date > paymentDueDate.Date || guarantor.IsOfflineGuarantor() && minimumResponseDate.Date >= paymentDueDate.Date)
                {
                    // if created after grace period and pending acceptance, needs to remain open until terms accepted or next statement date.  
                    // if terms not accepted by next statement date, cancel.
                    if (guarantor.NextStatementDate.HasValue && originationDate.Date == guarantor.NextStatementDate.Value.Date)
                    {
                        this.CancelFinancePlan(financePlan, "Terms not Accepted");
                        isCancelled = true;
                    }
                }
                else
                {
                    this.CancelFinancePlan(financePlan, "Terms not Accepted");
                    isCancelled = true;
                }

                if (isCancelled)
                {
                    JournalEventUserContext journalEventUserContext = JournalEventUserContext.BuildSystemUserContext(null);
                    this._userJournalEventService.Value.LogCancelFinancePlanEvent(journalEventUserContext, financePlan.VpGuarantor.VpGuarantorId, financePlan.VpGuarantor.User.UserName, financePlan.FinancePlanId, financePlan.FinancePlanOfferSetTypeId);
                }
            }
        }

        public void CancelFinancePlan(FinancePlan financePlan, string reason)
        {
            if (financePlan.IsPendingReconfiguration())
            {
                this.CancelReconfiguredFinancePlan(financePlan, reason, SystemUsers.SystemUserId, null);
                return;
            }
            foreach (FinancePlanVisit financePlanVisit in financePlan.FinancePlanVisits)
            {
                this.SendOutboundVisitMessageCancelledFinancePlan(financePlanVisit);
            }
            this.CancelFinancePlanInternal(financePlan, reason);

            if (financePlan.HasPendingReconfiguration())
            {
                this.CancelReconfiguredFinancePlan(financePlan.ReconfiguredFinancePlan, reason, SystemUsers.SystemUserId, null);
            }

            this._financePlanRepository.Value.InsertOrUpdate(financePlan);
        }

        private void CancelFinancePlanInternal(FinancePlan financePlan, string reason)
        {
            //As of VP-6186 removed setting each FinancePlanVisit's SoftRemoveDate
            financePlan.SetFinancePlanStatus(this.GetStatusWithEnum(FinancePlanStatusEnum.Canceled), reason);
            financePlan.PaymentAmount = 0m;
            financePlan.WriteOffInterestDue();
            financePlan.OnFinancePlanClose(reason);
        }

        private void SendOutboundVisitMessageCancelledFinancePlan(FinancePlanVisit financePlanVisit)
        {
            if ((financePlanVisit.FinancePlan.CombinedFinancePlan?.HasActiveFinancePlanVisitForVisitId(financePlanVisit.VisitId) ?? false) ||
               (financePlanVisit.FinancePlan.ReconfiguredFinancePlan?.HasActiveFinancePlanVisitForVisitId(financePlanVisit.VisitId) ?? false))
            {
                // don't need to send the removed message, as it's still on a finance plan
                return;
            }

            if (financePlanVisit.FinancePlan.IsActiveOriginated() &&
                financePlanVisit.HardRemoveDate == null)
            {
                this._session.Transaction.RegisterPostCommitSuccessAction(fpv =>
                {
                    this.Bus.Value.PublishMessage(new OutboundVisitMessage
                    {
                        ActionDate = DateTime.UtcNow,
                        BillingSystemId = Math.Abs(fpv.FinancePlanVisitVisit.BillingSystemId),
                        SourceSystemKey = fpv.FinancePlanVisitVisit.SourceSystemKey ?? fpv.FinancePlanVisitVisit.MatchedSourceSystemKey,
                        VpOutboundVisitMessageType = VpOutboundVisitMessageTypeEnum.VisitRemovedFromFinancePlan,
                        VpVisitId = fpv.VisitId,
                        FinancePlanClosedReasonEnum = FinancePlanClosedReasonEnum.FpClosed,
                        IsAutoPay = fpv.FinancePlan.VpGuarantor.UseAutoPay,
                        IsCallCenter = fpv.FinancePlan.VpGuarantor.IsOfflineGuarantor()
                    }).Wait();
                }, financePlanVisit);
            }
        }

        #endregion

        #region Consolidation / Deconsolidation

        public void ConsolidateFinancePlans(FinancePlan managedFinancePlan)
        {
            if (managedFinancePlan.IsClosed())
            {
                return;
            }

            if (managedFinancePlan.IsPendingAcceptance())
            {
                if (managedFinancePlan.IsReconfiguration())
                {
                    this.CancelReconfiguredFinancePlan(managedFinancePlan, "Cancelled on consolidation.", SystemUsers.SystemUserId, null);
                }
                else
                {
                    this.CancelFinancePlan(managedFinancePlan, "Cancelled on consolidation.");
                }
            }
            else if (managedFinancePlan.IsPastDue() || managedFinancePlan.IsActiveUncollectable())
            {
                managedFinancePlan.AddFinancePlanAmountsDueConsolidatation(false);
                this.SetFinancePlanIntoCurrent(managedFinancePlan);
                this._financePlanRepository.Value.InsertOrUpdate(managedFinancePlan);
            }
        }

        public void DeconsolidateFinancePlans(FinancePlan managedFinancePlan)
        {
            if (managedFinancePlan.IsClosed())
            {
                return;
            }

            if (managedFinancePlan.IsPendingAcceptance())
            {
                if (managedFinancePlan.IsReconfiguration())
                {
                    this.CancelReconfiguredFinancePlan(managedFinancePlan, "Cancelled on deconsolidation.", SystemUsers.SystemUserId, null);
                }
                else
                {
                    this.CancelFinancePlan(managedFinancePlan, "Cancelled on deconsolidation.");
                }
            }
            else if (managedFinancePlan.IsPastDue() || managedFinancePlan.IsActiveUncollectable())
            {
                managedFinancePlan.AddFinancePlanAmountsDueConsolidatation(true);
                this.SetFinancePlanIntoCurrent(managedFinancePlan);
                this._financePlanRepository.Value.InsertOrUpdate(managedFinancePlan);
            }
        }

        #endregion

        #region Interest

        private bool IsInterestEnabled()
        {
            return !this.ApplicationSettingsService.Value.PreventInterest.Value;
        }

        /*[Obsolete(ObsoleteDescription.VisitTransactions + " " + ObsoleteDescription.VisitTransactionVisitPayPayment)]
        public async Task<IList<VisitTransaction>> ReAssessCharityInterestAsync(int visitId, Guarantor guarantor, DateTime previousStatementEndDate, DateTime nextPeriodEndDate, IList<StatementPeriodResult> statementPeriods, IList<VisitTransaction> newTransactionsToPublish)
        {
            return await Task.Run(() =>
            {
                // todo: something different - reassessment will need to be redefined in VP3.
                if (!this.IsInterestEnabled() || previousStatementEndDate > nextPeriodEndDate)
                    return null;
    
                IList<FinancePlan> results = this._financePlanRepository.Value.GetFinancePlansIncludingClosedFromVisits(new List<int> { { visitId } });
                if (results != null && results.Any())
                {
                    IList<InterestAssessPeriodVisitNode> visitNodes = new List<InterestAssessPeriodVisitNode>();
    
                    IList<FinancePlan> financePlansThatNeedReAssessment = this._financePlanRepository.Value.GetFinancePlansWithIntList(guarantor.VpGuarantorId, results.Select(x => x.FinancePlanId).Distinct().ToList());
                    this.BuildInterestAssessNodes(nextPeriodEndDate, statementPeriods, financePlansThatNeedReAssessment, visitNodes);
                    foreach (FinancePlan financePlanToReAssess in financePlansThatNeedReAssessment.OrderBy(x => x.OriginationDate))
                    {
                        if (financePlanToReAssess.OriginationDate.HasValue)
                        {
                            List<InterestAssessPeriodVisitNode> nodesToSetNewOffer = visitNodes.Where(x => x.FinancePlanId == financePlanToReAssess.FinancePlanId).ToList();
                            foreach (InterestAssessPeriodVisitNode node in nodesToSetNewOffer)
                            {
                                node.InterestRateChangedForFp = true;
                                node.NewOffer = financePlanToReAssess.FinancePlanOffer;
                            }
                            financePlanToReAssess.UnpublishedFinancePlanAuditEvents.Add(AuditEventTypeFinancePlanEnum.InterestReassessed);
                        }
                    }
                    //return this.ProcessReAssessments(guarantor, visitNodes, newTransactionsToPublish);
                }
                return default(IList<VisitTransaction>);
            });
        }*/

        #region reassessment - commented out

        /*[Obsolete(ObsoleteDescription.VisitTransactions + " " + ObsoleteDescription.VisitTransactionVisitPayPayment)]
        public async Task<IList<VisitTransaction>> ReAssessInterest(Guarantor guarantor, DateTime previousStatementEndDate, DateTime nextPeriodEndDate, IList<StatementPeriodResult> statementPeriods, IList<VisitTransaction> newTransactionsToPublish)
        {
            if (!this.IsInterestEnabled() || previousStatementEndDate > nextPeriodEndDate)
                return null;

            IList<FinancePlanVisitPeriodTotal> results = this._financePlanRepository.Value.GetVisitsOnFinancePlansWithTotalNonPaymentTransactionsForDateRange(guarantor.VpGuarantorId, previousStatementEndDate, nextPeriodEndDate);
            if (results != null)
            {
                IList<FinancePlanVisitPeriodTotal> validResults = results.Where(x => x.TotalTransactionAmount < 0).ToList();
                if (validResults.Any())
                {
                    IList<InterestAssessPeriodVisitNode> visitNodes = new List<InterestAssessPeriodVisitNode>();

                    IList<FinancePlan> financePlansThatNeedReAssessment = this._financePlanRepository.Value.GetFinancePlansWithIntList(guarantor.VpGuarantorId, validResults.Select(x => x.FinancePlanId).Distinct().ToList());

                    //Remove financePlans that have visits with InterestZero flag set (if this has been set and they havent been reassessed then it's supposed to ignore)
                    financePlansThatNeedReAssessment = financePlansThatNeedReAssessment.Where(x => !x.FinancePlanVisits.Any(y => y.InterestZero)).ToList();
                    this.BuildInterestAssessNodes(nextPeriodEndDate, statementPeriods, financePlansThatNeedReAssessment, visitNodes);
                    foreach (FinancePlan financePlanToReAssess in financePlansThatNeedReAssessment.OrderBy(x => x.OriginationDate))
                    {
                        bool financePlanChanged = false;
                        if (financePlanToReAssess.OriginationDate.HasValue)
                        {
                            //Calculate the originationBalance
                            decimal originationBalance = financePlanToReAssess.OriginatedFinancePlanBalance;

                            //Make sure there was a NetNegative for this FP during the new period.
                            if (visitNodes.Where(x => x.Period.VpStatementId == 0 && x.FinancePlanId == financePlanToReAssess.FinancePlanId).Sum(y => y.NetNegativeAdjustmentAmount) <= 0m)
                            {
                                //Total of the NetNegative.
                                decimal totalNetNegative = visitNodes.Where(x => x.FinancePlanId == financePlanToReAssess.FinancePlanId).Sum(y => y.NetNegativeAdjustmentAmount);

                                //Sum all negative transactions after that time
                                //Sum just the negative periods
                                decimal newAdjustedOriginationBalance = originationBalance + totalNetNegative;

                                IList<FinancePlanOffer> allOffers = this._financePlanOfferService.Value.GetOffersWithCorrelationGuid(guarantor, financePlanToReAssess.FinancePlanOffer.CorrelationGuid);
                                IList<FinancePlanOffer> adjustedOffers = await this._financePlanOfferService.Value.GenerateOffersForUserWithHistoricalOffersAsync(
                                    newAdjustedOriginationBalance,
                                    0, // no interest exempt amount when adjusting an existing finance plan
                                    guarantor,
                                    allOffers,
                                    financePlanToReAssess.FirstInterestPeriod()
                                ).ConfigureAwait(false);

                                //Now that their offers are adjusted check and see if they would have the same interest rate.
                                decimal paymentAmount = financePlanToReAssess.PaymentAmount;
                                if (financePlanToReAssess.IsClosed())
                                {
                                    DateTime? time = financePlanToReAssess.LastTimeBeforeClosed();
                                    if (time != null)
                                    {
                                        paymentAmount = financePlanToReAssess.PaymentAmountAsOf(time.Value);
                                    }
                                }

                                FinancePlanOffer newOffer = SelectOfferWithPaymentAmount(paymentAmount, adjustedOffers);

                                if (newOffer != null && (newOffer.InterestRate != financePlanToReAssess.CurrentInterestRate || newOffer.InterestFreePeriod != financePlanToReAssess.InterestFreePeriod))
                                {
                                    financePlanToReAssess.SetInterestRate(newOffer.InterestRate, $"{this.Client.Value.ClientName} interest rate re-assessment");
                                    financePlanToReAssess.InterestFreePeriod = newOffer.InterestFreePeriod;
                                    financePlanToReAssess.FinancePlanOffer = newOffer;
                                    financePlanToReAssess.UnpublishedFinancePlanAuditEvents.Add(AuditEventTypeFinancePlanEnum.InterestReassessed);
                                    financePlanChanged = true;

                                    List<InterestAssessPeriodVisitNode> nodesToSetNewOffer = visitNodes.Where(x => x.FinancePlanId == financePlanToReAssess.FinancePlanId).ToList();
                                    foreach (InterestAssessPeriodVisitNode node in nodesToSetNewOffer)
                                    {
                                        node.InterestRateChangedForFp = true;
                                        node.NewOffer = newOffer;
                                    }
                                }
                            }
                        }
                        if (financePlanChanged)
                        {
                            this._financePlanRepository.Value.InsertOrUpdate(financePlanToReAssess);
                        }
                    }

                    return this.ProcessReAssessments(guarantor, visitNodes, newTransactionsToPublish);
                }
            }
            return null;
        }*/
        /* [Obsolete(ObsoleteDescription.VisitTransactions + " " + ObsoleteDescription.VisitTransactionVisitPayPayment)]
        private IList<VisitTransaction> ProcessReAssessments(Guarantor guarantor, IList<InterestAssessPeriodVisitNode> visitNodes, IList<VisitTransaction> newTransactionsToPublish)
        {
            IList<VisitTransaction> reallocations = new List<VisitTransaction>();
            IList<InterestAssessPeriodVisitNode> orderedVisitNodes = visitNodes.OrderBy(x => x.Visit.InsertDate).ToList();
            foreach (InterestAssessPeriodVisitNode visitNode in orderedVisitNodes.Where(x => x.Period.VpStatementId == 0))
            {
                InterestAssessPeriodVisitNode mostRecentVisitNode = visitNodes.FirstOrDefault(x => x.Period.VpStatementId == 0 && x.VisitId == visitNode.VisitId);
                bool mostRecentHadNetNegative = mostRecentVisitNode != null && mostRecentVisitNode.NetNegativeAdjustmentAmount < 0;

                //Find all finance plans for this visit
                InterestAssessPeriodVisitNode node = visitNode; //Modified Closure
                IOrderedEnumerable<FinancePlan> financePlansForVisit = visitNodes.Where(x => x.VisitId == node.VisitId).Select(x => x.FinancePlan).Distinct().OrderBy(f => f.InsertDate);

                decimal? currentInterestRate = financePlansForVisit.LastOrDefault()?.FinancePlanOffer?.InterestRate;
                foreach (FinancePlan financePlan in financePlansForVisit)
                {
                    //only try to adjust visit if it had a FP that adjusted rate or had a current period net negative adjustment.
                    if (visitNode.InterestRateChangedForFp || mostRecentHadNetNegative)
                    {
                        //Only reassess periods after FP was originated.
                        IList<InterestAssessPeriodVisitNode> allVisitNodesForFp = visitNodes
                            .Where(x => x.FinancePlanId == financePlan.FinancePlanId
                                && x.VisitId == visitNode.VisitId
                                && x.Period.PeriodEndDate > x.FinancePlan.OriginationDateTime())
                            .OrderBy(x => x.Period.PeriodEndDate).ToList();
                        if (allVisitNodesForFp.Any())
                        {
                            IList<InterestAssessPeriodVisitNode> orderedFinancePlanNodes = allVisitNodesForFp.OrderBy(x => x.Period.PeriodEndDate).ToList();
                            IDictionary<InterestAssessPeriodVisitNode, IList<VisitTransaction>> interestDictionary = new Dictionary<InterestAssessPeriodVisitNode, IList<VisitTransaction>>();
                            FinancePlan plan = financePlan;//Modified closure.
                            foreach (InterestAssessPeriodVisitNode nodeToReassess in orderedFinancePlanNodes)
                            {
                                decimal visitNetNegative = allVisitNodesForFp.Where(x => x.Period.PeriodEndDate > nodeToReassess.Period.PeriodEndDate).Sum(x => x.NetNegativeAdjustmentAmount);
                                decimal originationValueForVisit = nodeToReassess.PeriodEndingValue - nodeToReassess.InterestAssessed;
                                decimal newVisitTotal = originationValueForVisit + visitNetNegative;
                                //Dont give them negative interest.
                                if (newVisitTotal < 0m)
                                {
                                    newVisitTotal = 0m;
                                }
                                bool inInterestFreePeriod = this.IsFinancePlanInInterestFreePeriod(nodeToReassess.FinancePlan, nodeToReassess.Period.PeriodEndDate);
                                decimal newInterestAmount = 0m;
                                if (!inInterestFreePeriod)
                                {
                                    // VPNG-22709 Originally, GetMonthlyInterestAmountForBalance was passed financePlan.CurrentInterestRate for the yearlyInterestRate parameter
                                    // But, with older FPs that have been combined the financePlan.CurrentInterestRate may not reflect what the visit's interest should be assesed at
                                    newInterestAmount = this._interestService.Value.GetMonthlyInterestAmountForBalance(currentInterestRate.GetValueOrDefault(financePlan.CurrentInterestRate), newVisitTotal);
                                }

                                if (newInterestAmount < nodeToReassess.InterestAssessed && !nodeToReassess.IsInterestReversedAndReallocated)
                                {
                                    if (!interestDictionary.ContainsKey(nodeToReassess))
                                    {
                                        interestDictionary.Add(nodeToReassess, new List<VisitTransaction>());
                                    }
                                    interestDictionary[nodeToReassess].AddRange(this.AddReversalAndReallocationInterestTransaction(guarantor, nodeToReassess, newInterestAmount, interestDictionary));
                                    nodeToReassess.IsInterestReversedAndReallocated = true;
                                }
                            }

                            foreach (InterestAssessPeriodVisitNode assessPeriodVisitNode in interestDictionary.Keys)
                            {
                                if (!assessPeriodVisitNode.IsReallocatedInterestToPrincipalPayment)
                                {
                                    //Find reallocation
                                    //FinancePlanInterestReallocation foundReallocation = reallocations.FirstOrDefault(x => x.FinancePlanId == financePlan.FinancePlanId && x.StatementId == assessPeriodVisitNode.Period.VpStatementId);
                                    //if (foundReallocation == null)
                                    //{
                                    //    FinancePlanInterestReallocation newRealloc = new FinancePlanInterestReallocation
                                    //    {
                                    //        FinancePlanId = financePlan.FinancePlanId,
                                    //        StatementId = assessPeriodVisitNode.Period.VpStatementId != 0 ? (int?)assessPeriodVisitNode.Period.VpStatementId : null
                                    //    };
                                    //    reallocations.Add(newRealloc);
                                    //    foundReallocation = newRealloc;
                                    //}
                                    IList<VisitTransaction> reallocateInterestToPrincipalPayment = this.ReallocateInterestToPrincipalPayment(assessPeriodVisitNode, interestDictionary[assessPeriodVisitNode], guarantor, interestDictionary);
                                    interestDictionary[assessPeriodVisitNode].AddRange(reallocateInterestToPrincipalPayment);
                                    //this.PopulateReallocationObject(assessPeriodVisitNode, foundReallocation, interestDictionary[assessPeriodVisitNode]);
                                    reallocations.AddRange(reallocateInterestToPrincipalPayment);

                                    this._visitService.Value.SaveVisit(assessPeriodVisitNode.Visit);
                                    assessPeriodVisitNode.IsReallocatedInterestToPrincipalPayment = true;
                                }
                            }

                            if (interestDictionary.Any() && interestDictionary.SelectMany(x => x.Value).Any())
                            {
                                newTransactionsToPublish.AddRange(interestDictionary.SelectMany(x => x.Value).ToList());
                            }
                        }
                    }
                }
            }
            return reallocations;
        }
        */
        /* [Obsolete(ObsoleteDescription.VisitTransactions)]
        private IList<VisitTransaction> AddReversalAndReallocationInterestTransaction(Guarantor guarantor, InterestAssessPeriodVisitNode nodeToReassess, decimal newInterestAmount, IDictionary<InterestAssessPeriodVisitNode, IList<VisitTransaction>> interestDictionary)
        {
            IList<VisitTransaction> newTransactions = new List<VisitTransaction>();
            IList<VisitTransaction> allInterestChargesForVisitAndPeriod = nodeToReassess.Visit.TransactionsForDateRange(nodeToReassess.Period.PeriodStartDate, nodeToReassess.Period.PeriodEndDate)
                .Where(x => x.VpTransactionType.IsInterestAssessed() && !x.VpTransactionType.IsReversal() && !x.VpTransactionType.IsInterestWriteOff() && !x.VpTransactionType.IsHs())
                .ToList();
            this.RemoveNonTerminalTransactions(allInterestChargesForVisitAndPeriod, nodeToReassess, interestDictionary);

            // NOTE: If there are no terminal interest assessments we do not want to reallocate. 
            // This is most likely where interest has been written off. VPNG-19002
            if (allInterestChargesForVisitAndPeriod.Count == 0)
            {
                return newTransactions;
            }

            foreach (VisitTransaction visitTransaction in allInterestChargesForVisitAndPeriod)
            {
                //Remove old incorrect interest.
                VisitTransaction interestReversalTransaction = new VisitTransaction
                {
                    OverrideInsertDate = visitTransaction.OverrideInsertDate ?? visitTransaction.InsertDate, //Make sure it's before the end of that period.
                    Visit = nodeToReassess.Visit,
                    TransactionDate = DateTime.UtcNow,
                    PostDate = DateTime.UtcNow,
                    VpTransactionType = this._visitTransactionService.Value.GetTransactionTypeForInterestAssessmentReversal(),//13
                    VpGuarantorId = guarantor.VpGuarantorId,
                    OpposingVisitTransaction = visitTransaction
                };
                this._visitTransactionService.Value.SetTransactionDescription(interestReversalTransaction);
                interestReversalTransaction.SetTransactionAmount(-1m * visitTransaction.VisitTransactionAmount.TransactionAmount);
                this._visitTransactionRepository.Value.InsertOrUpdate(interestReversalTransaction);
                newTransactions.Add(interestReversalTransaction);
            }

            if (newInterestAmount > 0)
            {
                //Re add new correct interest.
                VisitTransaction interestAssessmentReallocation = new VisitTransaction
                {
                    OverrideInsertDate = nodeToReassess.Period.PeriodEndDate.AddMilliseconds(-10), //Make sure it's before the end of that period.
                    Visit = nodeToReassess.Visit,
                    TransactionDate = DateTime.UtcNow,
                    PostDate = DateTime.UtcNow,
                    VpTransactionType = this._visitTransactionService.Value.GetTransactionTypeForInterestAssessmentReallocation(),//12
                    VpGuarantorId = guarantor.VpGuarantorId,
                    OpposingVisitTransaction = allInterestChargesForVisitAndPeriod.FirstOrDefault()
                };
                this._visitTransactionService.Value.SetTransactionDescription(interestAssessmentReallocation);
                interestAssessmentReallocation.SetTransactionAmount(newInterestAmount);
                this._visitTransactionRepository.Value.InsertOrUpdate(interestAssessmentReallocation);
                newTransactions.Add(interestAssessmentReallocation);
            }

            return newTransactions;
        }*/
        /*[Obsolete(ObsoleteDescription.VisitTransactions)]
        private IList<VisitTransaction> ReallocateInterestToPrincipalPayment(InterestAssessPeriodVisitNode nodeToReassess, IList<VisitTransaction> interestTransactions, Guarantor guarantor, IDictionary<InterestAssessPeriodVisitNode, IList<VisitTransaction>> interestDictionary)
        {
            IList<VisitTransaction> newTransactions = new List<VisitTransaction>();
            //Example: -33.29M
            //Should be positive.
            decimal totalInterestRemoved = interestTransactions.Sum(x => x.VisitTransactionAmount.TransactionAmount);
            decimal totalInterestToMoveToPrincipal = (-1m) * totalInterestRemoved;

            //Find payments in that period
            IList<VisitTransaction> allInterestPaymentsForVisitAndPeriod = nodeToReassess.Visit.TransactionsForDateRange(nodeToReassess.Period.PeriodEndDate, nodeToReassess.Period.NextStatementPeriodResult?.PeriodEndDate ?? DateTime.MaxValue)
                .Where(x => x.VpTransactionType.IsInterestPayment() && !x.VpTransactionType.IsReversal() && !x.VpTransactionType.IsHs())
                .ToList();
            this.RemoveNonTerminalTransactions(allInterestPaymentsForVisitAndPeriod, nodeToReassess, interestDictionary);
            if (allInterestPaymentsForVisitAndPeriod.Count == 0)
            {
                allInterestPaymentsForVisitAndPeriod = nodeToReassess.Visit.TransactionsForDateRange(nodeToReassess.Period.PeriodEndDate, DateTime.MaxValue)
                    .Where(x => x.VpTransactionType.IsInterestPayment() && !x.VpTransactionType.IsReversal() && !x.VpTransactionType.IsHs())
                    .ToList();
                this.RemoveNonTerminalTransactions(allInterestPaymentsForVisitAndPeriod, nodeToReassess, interestDictionary);
            }

            //order the list now that transactions that shouldnt be considered are removed.
            allInterestPaymentsForVisitAndPeriod = allInterestPaymentsForVisitAndPeriod.OrderBy(x => x.OverrideInsertDate ?? x.InsertDate)
                .ThenBy(x => x.VisitTransactionId)
                .ToList();

            VisitTransaction interestPaymentTransactionReversal = null;

            HashSet<VisitTransaction> usedInterestTransactions = new HashSet<VisitTransaction>();

            int totalInterestWhileLoopCount = 0;
            while (totalInterestRemoved < 0 && totalInterestWhileLoopCount < 10)
            {
                //Want the oldest actual payment, not a refund payment.
                //Ordered by the largest to try to minimize this while loop.  VPNG-16503  (The "largest" in this case was the largest negative, so really want the smallest amount...)
                VisitTransaction currentInterest = allInterestPaymentsForVisitAndPeriod.Where(x => !usedInterestTransactions.Contains(x)).FirstOrDefault(x => x.VisitTransactionAmount.TransactionAmount < 0);

                if (currentInterest == null)
                {
                    break;
                }
                usedInterestTransactions.Add(currentInterest);
                //Reverse the transaction completely
                interestPaymentTransactionReversal = new VisitTransaction
                {
                    OverrideInsertDate = currentInterest.OverrideInsertDate ?? currentInterest.InsertDate,
                    Visit = nodeToReassess.Visit,
                    TransactionDate = DateTime.UtcNow,
                    PostDate = DateTime.UtcNow,
                    VpTransactionType = this._visitTransactionService.Value.GetTransactionTypeForInterestPaymentReassessmentReversal(),//203
                    VpGuarantorId = guarantor.VpGuarantorId,
                    OpposingVisitTransaction = currentInterest
                };
                this._visitTransactionService.Value.SetTransactionDescription(interestPaymentTransactionReversal);
                interestPaymentTransactionReversal.SetTransactionAmount(-1m * currentInterest.VisitTransactionAmount.TransactionAmount);
                this._visitTransactionRepository.Value.InsertOrUpdate(interestPaymentTransactionReversal);
                newTransactions.Add(interestPaymentTransactionReversal);

                //If transaction was less or equal than totalInterestRemoved do not reallocate
                //totalInterestRemoved: -33.29M
                //currentInterest.Amount: -15.50M
                if (totalInterestRemoved <= currentInterest.VisitTransactionAmount.TransactionAmount)
                {
                    totalInterestRemoved -= currentInterest.VisitTransactionAmount.TransactionAmount;
                }
                //If transaction was more than totalInterestRemoved reallocate the remainder.
                //totalInterestRemoved: -33.29M
                //currentInterest.Amount: -66.50M
                else
                {
                    VisitTransaction interestPaymentTransactionReallocation = new VisitTransaction
                    {
                        OverrideInsertDate = currentInterest.OverrideInsertDate ?? currentInterest.InsertDate,
                        Visit = nodeToReassess.Visit,
                        TransactionDate = DateTime.UtcNow,
                        PostDate = DateTime.UtcNow,
                        VpTransactionType = this._visitTransactionService.Value.GetTransactionTypeForInterestPaymentReassessmentReallocation(),//204
                        VpGuarantorId = guarantor.VpGuarantorId,
                        OpposingVisitTransaction = currentInterest
                    };
                    this._visitTransactionService.Value.SetTransactionDescription(interestPaymentTransactionReallocation);
                    interestPaymentTransactionReallocation.SetTransactionAmount(currentInterest.VisitTransactionAmount.TransactionAmount - totalInterestRemoved);
                    this._visitTransactionRepository.Value.InsertOrUpdate(interestPaymentTransactionReallocation);
                    newTransactions.Add(interestPaymentTransactionReallocation);
                    totalInterestRemoved = 0m;
                }

                totalInterestWhileLoopCount++;
            }
            if (totalInterestRemoved < 0)
            {
                //Do we care?  //For some reason we couldnt 
                //Probably should remove the remaining totalInterestRemoved from totalInterestToMoveToPrincipal to make sure things add up.
                //totalInterestRemoved: -33.29M
                //totalInterestToMoveToPrincipal: 33.29M

                //This is correct because the rest here is just interest that's going away, it wasnt a payment so we dont want it to go back as principal.  It'll just lower total on the balance.
                totalInterestToMoveToPrincipal += totalInterestRemoved;
                totalInterestRemoved = 0m;
            }

            IList<VisitTransaction> allPrincipalPaymentsForVisitAndPeriod = nodeToReassess.Visit.TransactionsForDateRange(nodeToReassess.Period.PeriodEndDate, nodeToReassess.Period.NextStatementPeriodResult?.PeriodEndDate ?? DateTime.MaxValue)
                .Where(x => x.VpTransactionType.IsPrincipalPayment() && !x.VpTransactionType.IsReversal() && !x.VpTransactionType.IsHs())
                .ToList();

            this.RemoveNonTerminalTransactions(allPrincipalPaymentsForVisitAndPeriod, nodeToReassess, interestDictionary);
            if (allPrincipalPaymentsForVisitAndPeriod.Count == 0)
            {
                allPrincipalPaymentsForVisitAndPeriod = nodeToReassess.Visit.TransactionsForDateRange(nodeToReassess.Period.PeriodEndDate, DateTime.MaxValue)
                    .Where(x => x.VpTransactionType.IsPrincipalPayment() && !x.VpTransactionType.IsReversal() && !x.VpTransactionType.IsHs())
                    .ToList();

                this.RemoveNonTerminalTransactions(allPrincipalPaymentsForVisitAndPeriod, nodeToReassess, interestDictionary);
            }

            //order the list now that transactions that shouldnt be considered are removed.
            allPrincipalPaymentsForVisitAndPeriod = allPrincipalPaymentsForVisitAndPeriod.OrderBy(x => x.OverrideInsertDate ?? x.InsertDate)
                .ThenBy(x => x.VisitTransactionId)
                .ToList();

            int totalPrincipalWhileLoopCount = 0;

            while (totalInterestToMoveToPrincipal > 0 || totalPrincipalWhileLoopCount > 10)
            {
                //Want the oldest actual payment, not a refund payment.
                //This probably doesnt need to be in a while loop with how it's currently implemented, however might have to put upper limits on how much we can add to a single principal payment transaction based on the original payment amount?
                VisitTransaction currentPrincipal = allPrincipalPaymentsForVisitAndPeriod.FirstOrDefault(x => x.VisitTransactionAmount.TransactionAmount < 0);
                if (currentPrincipal == null)
                {
                    if (interestPaymentTransactionReversal == null)
                    {
                        //This is the case when a the interest wasnt reallocated so we dont need to reallocate the payment in any form.
                        break;
                    }
                    //Create new transaction with no opposing 
                    VisitTransaction principalPaymentTransactionReallocation = new VisitTransaction
                    {
                        OverrideInsertDate = interestPaymentTransactionReversal.OverrideInsertDate ?? interestPaymentTransactionReversal.InsertDate,
                        Visit = nodeToReassess.Visit,
                        TransactionDate = DateTime.UtcNow,
                        PostDate = DateTime.UtcNow,
                        VpTransactionType = this._visitTransactionService.Value.GetTransactionTypeForPrincipalPaymentReassessmentReallocation(), //104
                        VpGuarantorId = guarantor.VpGuarantorId,
                        OpposingVisitTransaction = null
                    };
                    this._visitTransactionService.Value.SetTransactionDescription(principalPaymentTransactionReallocation);
                    //Negative amount minus a positive amount to add additional principal to that payment.
                    principalPaymentTransactionReallocation.SetTransactionAmount(-1m * totalInterestToMoveToPrincipal);
                    this._visitTransactionRepository.Value.InsertOrUpdate(principalPaymentTransactionReallocation);
                    newTransactions.Add(principalPaymentTransactionReallocation);
                }
                else
                {
                    //Reverse entire principal payment
                    VisitTransaction principalPaymentTransactionReversal = new VisitTransaction
                    {
                        OverrideInsertDate = currentPrincipal.OverrideInsertDate ?? currentPrincipal.InsertDate,
                        Visit = nodeToReassess.Visit,
                        TransactionDate = DateTime.UtcNow,
                        PostDate = DateTime.UtcNow,
                        VpTransactionType = this._visitTransactionService.Value.GetTransactionTypeForPrincipalPaymentReassessmentReversal(), //103
                        VpGuarantorId = guarantor.VpGuarantorId,
                        OpposingVisitTransaction = currentPrincipal
                    };
                    this._visitTransactionService.Value.SetTransactionDescription(principalPaymentTransactionReversal);
                    //This makes it a positive amount
                    principalPaymentTransactionReversal.SetTransactionAmount(-1m * currentPrincipal.VisitTransactionAmount.TransactionAmount);
                    this._visitTransactionRepository.Value.InsertOrUpdate(principalPaymentTransactionReversal);
                    newTransactions.Add(principalPaymentTransactionReversal);

                    VisitTransaction principalPaymentTransactionReallocation = new VisitTransaction
                    {
                        OverrideInsertDate = currentPrincipal.OverrideInsertDate ?? currentPrincipal.InsertDate,
                        Visit = nodeToReassess.Visit,
                        TransactionDate = DateTime.UtcNow,
                        PostDate = DateTime.UtcNow,
                        VpTransactionType = this._visitTransactionService.Value.GetTransactionTypeForPrincipalPaymentReassessmentReallocation(), //104
                        VpGuarantorId = guarantor.VpGuarantorId,
                        OpposingVisitTransaction = currentPrincipal
                    };
                    this._visitTransactionService.Value.SetTransactionDescription(principalPaymentTransactionReallocation);
                    //Negative amount minus a positive amount to add additional principal to that payment.
                    principalPaymentTransactionReallocation.SetTransactionAmount(currentPrincipal.VisitTransactionAmount.TransactionAmount - totalInterestToMoveToPrincipal);
                    this._visitTransactionRepository.Value.InsertOrUpdate(principalPaymentTransactionReallocation);
                    newTransactions.Add(principalPaymentTransactionReallocation);
                }
                totalInterestToMoveToPrincipal = 0m;
                totalPrincipalWhileLoopCount++;
            }

            return newTransactions;
        }
        */
        /* [Obsolete(ObsoleteDescription.VisitTransactions)]
        private void RemoveNonTerminalTransactions(IList<VisitTransaction> allInterestPaymentsForVisitAndPeriod, InterestAssessPeriodVisitNode nodeToReassess, IDictionary<InterestAssessPeriodVisitNode, IList<VisitTransaction>> interestDictionary)
        {
            List<VisitTransaction> transactionsToRemove = new List<VisitTransaction>();
            List<VisitTransaction> allPendingTransactions = new List<VisitTransaction>();
            foreach (InterestAssessPeriodVisitNode interestAssessPeriodVisitNode in interestDictionary.Keys)
            {
                allPendingTransactions.AddRange(interestDictionary[interestAssessPeriodVisitNode].Where(x => x.VpTransactionType != null && !x.VpTransactionType.IsReversal()));
            }
            foreach (VisitTransaction visitTransaction in allInterestPaymentsForVisitAndPeriod)
            {
                //Yay for n^2...
                if (nodeToReassess.Visit != null &&
                    nodeToReassess.Visit.Transactions.Any(x => x.OpposingVisitTransaction != null && x.OpposingVisitTransaction == visitTransaction
                    || (x.OpposingVisitTransaction != null && x.OpposingVisitTransaction.VisitTransactionId == visitTransaction.VisitTransactionId && visitTransaction.VisitTransactionId != 0)))
                {
                    //Implies that this transaction has already been reversed and shouldnt be reversed again.
                    transactionsToRemove.Add(visitTransaction);
                    continue;
                }
                if (allPendingTransactions.Any(x => x.OpposingVisitTransaction != null && x.OpposingVisitTransaction == visitTransaction))
                {
                    transactionsToRemove.Add(visitTransaction);
                }
            }
            foreach (VisitTransaction visitTransaction in transactionsToRemove)
            {
                //This is incase we have duplicates.
                if (allInterestPaymentsForVisitAndPeriod.Contains(visitTransaction))
                {
                    allInterestPaymentsForVisitAndPeriod.Remove(visitTransaction);
                }
            }
        }*/
        /* [Obsolete(ObsoleteDescription.VisitTransactions + " " + ObsoleteDescription.VisitTransactionVisitPayPayment)]
        private void PopulateReallocationObject(InterestAssessPeriodVisitNode nodeToReassess, FinancePlanInterestReallocation foundReallocation, IList<VisitTransaction> newTransactions)
        {
            FinancePlanInterestReallocationVisit financePlanReallocationVisit = new FinancePlanInterestReallocationVisit
            {
                Visit = nodeToReassess.Visit,
                FinancePlanInterestReallocation = foundReallocation
            };
            foundReallocation.FinancePlanInterestReallocationVisits.Add(financePlanReallocationVisit);

            //Add all of the transactions at the end so it doesnt change the numbers when looking at the visit.
            foreach (VisitTransaction visitTransaction in newTransactions)
            {
                //Find the original Payment for any reassessed payments
                Payment originalPayment = null;
                if (visitTransaction.VpTransactionType.IsVpPayment())
                {
                    VisitTransaction originalTransaction = visitTransaction.OpposingVisitTransaction;
                    int maxIteration = 100;

                    while (originalTransaction?.OpposingVisitTransaction != null)
                    {
                        if (--maxIteration < 1)
                        {
                            this.LoggingService.Value.Warn(() =>  $"FinancePlanService::PopulateReallocationObject - unable to find payment method -- iteration limit ({maxIteration}) exceeded.");
                            originalTransaction = null;
                            break;
                        }

                        originalTransaction = originalTransaction.OpposingVisitTransaction;
                    }

                    if (originalTransaction?.PaymentAllocationId > 0)
                    {
                        originalPayment = this._paymentAllocationService.Value.GetById(originalTransaction.PaymentAllocationId.Value).Payment;
                    }
                    else
                    {
                        this.LoggingService.Value.Warn(() =>  "FinancePlanService::PopulateReallocationObject - unable to find payment method -- original transaction has no payment allocation.");
                    }
                }

                nodeToReassess.Visit.AddVisitTransaction(visitTransaction);
                PaymentAllocation newDist = new PaymentAllocation
                {
                    FinancePlanInterestReallocationVisit = financePlanReallocationVisit,
                    ActualAmount = visitTransaction.VisitTransactionAmount.TransactionAmount,
                    FinancePlanId = financePlanReallocationVisit.FinancePlanInterestReallocation.FinancePlanId,
                    OriginalVisitTransaction = visitTransaction,
                    Payment = originalPayment,
                    //VP-24 - this will probably break this reallocation
                    PaymentVisit = new PaymentVisit() { VisitId = nodeToReassess.VisitId},
                    UnallocatedAmount = 0m,
                    PaymentAllocationStatus = PaymentAllocationStatusEnum.PendingHsAcknowledgement
                };
                newDist.VisitTransactions.Add(visitTransaction);

                if (visitTransaction.VpTransactionType.IsInterest())
                {
                    newDist.PaymentAllocationType = PaymentAllocationTypeEnum.VisitInterest;
                }
                else if (visitTransaction.VpTransactionType.IsPrincipal())
                {
                    newDist.PaymentAllocationType = PaymentAllocationTypeEnum.VisitPrincipal;
                }
                financePlanReallocationVisit.PaymentAllocations.Add(newDist);
            }

            //get the new PaymentAllocationIds and set them for the new VisitTransactions
            this._financePlanInterestReallocationRepository.Value.InsertOrUpdate(foundReallocation);
            foreach (PaymentAllocation paymentAllocation in financePlanReallocationVisit.PaymentAllocations)
            {
                this._paymentAllocationRepository.Value.InsertOrUpdate(paymentAllocation);
                paymentAllocation.OriginalVisitTransaction.PaymentAllocationId = paymentAllocation.PaymentAllocationId;
            }
        } */
        /*[Obsolete(ObsoleteDescription.VisitTransactions)]
        public void BuildInterestAssessNodes(DateTime periodEndDate, IList<StatementPeriodResult> statementPeriods, IList<FinancePlan> financePlansThatNeedReAssessment, IList<InterestAssessPeriodVisitNode> allVisitsInFp)
        {
            if (statementPeriods != null)
            {
                StatementPeriodResult next = null;
                foreach (StatementPeriodResult statementPeriodResult in statementPeriods.OrderByDescending(x => x.PeriodEndDate))
                {
                    statementPeriodResult.NextStatementPeriodResult = next;
                    next = statementPeriodResult;
                }
            }

            if (financePlansThatNeedReAssessment != null)
            {
                foreach (FinancePlan financePlanToReAssess in financePlansThatNeedReAssessment.OrderBy(x => x.OriginationDate))
                {
                    if (financePlanToReAssess.OriginationDate.HasValue)
                    {
                        List<StatementPeriodResult> relevantStatementPeriodsForFinancePlan = statementPeriods.Where(x => x.PeriodEndDate.Date >= financePlanToReAssess.OriginationDate.Value.Date && x.PeriodEndDate < periodEndDate).ToList();
                        //Build object with period and financeplanvisit so that it's easy to consider per period.
                        foreach (FinancePlanVisit financePlanVisit in financePlanToReAssess.FinancePlanVisits)
                        {
                            //The statement that's about to get generated shouldnt be included in here.
                            foreach (StatementPeriodResult statementPeriodResult in relevantStatementPeriodsForFinancePlan)
                            {
                                InterestAssessPeriodVisitNode interestAssessPeriodVisitNode = new InterestAssessPeriodVisitNode
                                {
                                    FinancePlanVisit = financePlanVisit,
                                    Period = statementPeriodResult
                                };
                                allVisitsInFp.Add(interestAssessPeriodVisitNode);

                            }

                            StatementPeriodResult lastStatementPeriod = statementPeriods.OrderByDescending(x => x.StatementDate).FirstOrDefault();
                            //Add Statement that's about to be generated.
                            if (lastStatementPeriod != null)
                            {
                                allVisitsInFp.Add(new InterestAssessPeriodVisitNode
                                {
                                    FinancePlanVisit = financePlanVisit,
                                    Period = new StatementPeriodResult
                                    {
                                        PeriodEndDate = periodEndDate,
                                        PeriodStartDate = lastStatementPeriod.PeriodEndDate.AddMilliseconds(TransactionFilter.NewVisitExclusionWindowMS),
                                        VpStatementId = 0
                                    }
                                });
                            }
                        }
                    }
                }
            }
            //Prune the nodes
            //Remove the finance plans that didnt assess any interest in their period if there is more than 1 per visit.
            List<int> allVisitIds = allVisitsInFp.Select(x => x.VisitId).Distinct().ToList();
            foreach (int visitId in allVisitIds)
            {
                List<InterestAssessPeriodVisitNode> allNodesForVisit = allVisitsInFp.Where(x => x.VisitId == visitId).ToList();
                foreach (StatementPeriodResult statementPeriodResult in allNodesForVisit.Select(x => x.Period).Distinct().ToList())
                {
                    if (allNodesForVisit.Count(x => x.Period == statementPeriodResult) > 1)
                    {
                        //Pick the one to keep and remove the rest.
                        List<InterestAssessPeriodVisitNode> visitAndPeriod = allNodesForVisit.Where(x => x.Period == statementPeriodResult).ToList();
                        //If had interest assessed on FP, keep oldest, this is going off the premise that only one FP can assess interest in a statement period for a visit.
                        InterestAssessPeriodVisitNode found = null;
                        foreach (InterestAssessPeriodVisitNode interestAssessPeriodVisitNode in visitAndPeriod.OrderBy(x => x.FinancePlan.OriginationDate))
                        {
                            DateTime fpOriginationDate = interestAssessPeriodVisitNode.FinancePlan.OriginationDateTime() < interestAssessPeriodVisitNode.Period.PeriodStartDate || !interestAssessPeriodVisitNode.FinancePlan.OriginationDateTime().HasValue ? interestAssessPeriodVisitNode.Period.PeriodStartDate : interestAssessPeriodVisitNode.FinancePlan.OriginationDateTime().Value;
                            DateTime fpClosedDate = interestAssessPeriodVisitNode.FinancePlan.ClosedDate() ?? interestAssessPeriodVisitNode.Period.PeriodEndDate;
                            if (interestAssessPeriodVisitNode.FinancePlan.InterestAssessedForDateRange(fpOriginationDate, fpClosedDate) > 0m)
                            {
                                found = interestAssessPeriodVisitNode;
                                break;
                            }
                        }
                        //If found is null, pick the oldest fp.
                        if (found == null)
                        {
                            found = visitAndPeriod.OrderBy(x => x.FinancePlan.OriginationDateTime()).FirstOrDefault();
                        }
                        //Remove the others, found should have a value now
                        foreach (InterestAssessPeriodVisitNode interestAssessPeriodVisitNode in visitAndPeriod.Where(x => x != found))
                        {
                            allVisitsInFp.Remove(interestAssessPeriodVisitNode);
                        }
                    }
                }
            }
        }
        */

        #endregion

        public Dictionary<FinancePlanVisit, decimal> IfAssessInterestFinancePlanVisits(IList<FinancePlanVisit> financePlanVisits,
            DateTime currentStatementStartDate,
            DateTime currentStatementEndDate)
        {
            Dictionary<FinancePlanVisit, decimal> visitAssessed = new Dictionary<FinancePlanVisit, decimal>();
            InterestCalculationMethodEnum? firstCalculationMethod = financePlanVisits?.FirstOrDefault()?.FinancePlan?.InterestCalculationMethod;

            //ignore finance plan visits removed before the end date
            financePlanVisits = financePlanVisits?.Where(x => (x.HardRemoveDate ?? DateTime.MaxValue) >= currentStatementEndDate).ToList();

            if (!this.IsInterestEnabled()
                || financePlanVisits.IsNullOrEmpty()
                || currentStatementStartDate > currentStatementEndDate
                || firstCalculationMethod == null
            )
            {
                return visitAssessed;
            }

            IDictionary<int, IList<Tuple<DateTime, decimal>>> financePlanActiveVisitBalancesAsOf = this.FinancePlanActiveVisitBalancesAsOf(financePlanVisits, currentStatementStartDate, currentStatementEndDate, firstCalculationMethod);
            //todo: Do I need to worry about previously assessed interest?
            //For old style of interest assessment, i'll have to call this for each previous period...
            foreach (FinancePlanVisit financePlanVisit in financePlanVisits)
            {
                if (!visitAssessed.ContainsKey(financePlanVisit))
                {
                    visitAssessed.Add(financePlanVisit, this.GetInterestAmount(financePlanVisit, financePlanActiveVisitBalancesAsOf, currentStatementStartDate, currentStatementEndDate, ignoreAlreadyAssessedInterest: true) ?? 0);
                }
            }

            return visitAssessed;
        }

        public IDictionary<int, IList<Tuple<DateTime, decimal>>> FinancePlanActiveVisitBalancesAsOf(IList<FinancePlanVisit> financePlanVisits, DateTime currentStatementStartDate, DateTime currentStatementEndDate, InterestCalculationMethodEnum? firstCalculationMethod)
        {
            IDictionary<int, IList<Tuple<DateTime, decimal>>> financePlanActiveVisitBalancesAsOf;
            if (firstCalculationMethod != null && firstCalculationMethod.Value == InterestCalculationMethodEnum.Monthly)
            {
                //ending balance method
                financePlanActiveVisitBalancesAsOf = this.GetFinancePlanActiveVisitsBalancesDailyForPeriod(financePlanVisits, currentStatementEndDate, currentStatementEndDate);
            }
            else
            {
                financePlanActiveVisitBalancesAsOf = this.GetFinancePlanActiveVisitsBalancesDailyForPeriod(financePlanVisits, currentStatementStartDate, currentStatementEndDate);
            }

            return financePlanActiveVisitBalancesAsOf;
        }

        public IList<FinancePlanVisitInterestDue> AssessInterest(FinancePlan financePlan,
            DateTime currentStatementStartDate,
            DateTime currentStatementEndDate,
            DateTime dateToProcess)
        {
            IList<FinancePlanVisitInterestDue> financePlanVisitInterestDues = new List<FinancePlanVisitInterestDue>();

            if (!this.IsInterestEnabled())
            {
                return financePlanVisitInterestDues;
            }

            if (financePlan == null)
            {
                return financePlanVisitInterestDues;
            }

            // check that the the dates to assess interest are valid
            if (currentStatementStartDate > currentStatementEndDate)
            {
                this.LoggingService.Value.Warn(() => $"{nameof(this.AssessInterest)}: current statement start date: {currentStatementStartDate}, current statement end date: {currentStatementEndDate} for financeplan#{financePlan.FinancePlanId}");
                return financePlanVisitInterestDues;
            }

            IDictionary<int, IList<Tuple<DateTime, decimal>>> financePlanActiveVisitBalancesAsOf;
            IList<FinancePlanVisit> activeFinancePlanVisits = financePlan.ActiveFinancePlanVisits;
            if (financePlan.InterestCalculationMethod == InterestCalculationMethodEnum.Monthly)
            {
                //ending balance method
                financePlanActiveVisitBalancesAsOf = this.GetFinancePlanActiveVisitsBalancesDailyForPeriod(activeFinancePlanVisits, currentStatementEndDate, currentStatementEndDate);
            }
            else
            {
                financePlanActiveVisitBalancesAsOf = this.GetFinancePlanActiveVisitsBalancesDailyForPeriod(activeFinancePlanVisits, currentStatementStartDate, currentStatementEndDate);
            }

            foreach (FinancePlanVisit financePlanVisit in financePlan.FinancePlanVisits)
            {
                #region VP3 - right now we don't care about pending visits - treat the same as statemented.
                /*
                bool isVisitOnHardStatement = this._vpStatementService.Value.IsVisitOnHardStatement(visit.VisitId, financePlan.CreatedVpStatement.PeriodStartDate, currentStatementEndDate);
                if (!isVisitOnHardStatement)
                {
                    // defer interest until it statements
                    continue;
                }*/
                #endregion

                decimal? interestAmount = this.GetInterestAmount(financePlanVisit, financePlanActiveVisitBalancesAsOf, currentStatementStartDate, currentStatementEndDate);
                if (!interestAmount.HasValue)
                {
                    continue;
                }

                FinancePlanVisitInterestDue financePlanVisitInterestDue = financePlanVisit.AddFinancePlanVisitInterestAssessment(interestAmount.Value, financePlanVisit.CurrentInterestRate(), currentStatementEndDate);
                financePlanVisitInterestDues.Add(financePlanVisitInterestDue);
            }

            financePlan.UnpublishedFinancePlanAuditEvents.Add(AuditEventTypeFinancePlanEnum.InterestAssessed);

            return financePlanVisitInterestDues;
        }

        private decimal? GetInterestAmount(FinancePlanVisit financePlanVisit, IDictionary<int, IList<Tuple<DateTime, decimal>>> financePlanActiveVisitBalancesAsOf, DateTime currentStatementStartDate, DateTime currentStatementEndDate, bool ignoreAlreadyAssessedInterest = false)
        {
            if (!financePlanActiveVisitBalancesAsOf.ContainsKey(financePlanVisit.VisitId))
            {
                // visit wasn't found, had balance < 0, or was not active
                return null;
            }

            decimal financePlanVisitBalance = financePlanActiveVisitBalancesAsOf[financePlanVisit.VisitId].Last().Item2;
            if (financePlanVisitBalance <= 0m)
            {
                this.LoggingService.Value.Warn(() => $"{nameof(this.GetInterestAmount)}: not assessing interest on {financePlanVisitBalance} for date range {currentStatementStartDate} - {currentStatementEndDate} for financeplanvisit#{financePlanVisit.FinancePlanVisitId}");
                return null;
            }

            if (!ignoreAlreadyAssessedInterest)
            {
                decimal interestForNewStatement = financePlanVisit.PositiveInterestAssessedForDateRange(currentStatementStartDate, currentStatementEndDate);
                if (interestForNewStatement != 0m)
                {
                    this.LoggingService.Value.Warn(() => $"{nameof(this.GetInterestAmount)}: {interestForNewStatement} already assessed for date range {currentStatementStartDate} - {currentStatementEndDate} for financeplanvisit#{financePlanVisit.FinancePlanVisitId}");
                    return null;
                }
            }

            bool hasOverride = financePlanVisit.HasOverride();
            decimal interestRate = financePlanVisit.CurrentInterestRate();
            
            decimal interestAmount;
            if (financePlanVisit.FinancePlan.InterestCalculationMethod == InterestCalculationMethodEnum.Monthly)
            {
                //ending balance method
                interestAmount = this._interestService.Value.GetMonthlyInterestAmountForBalance(interestRate, financePlanVisitBalance);
            }
            else
            {
                interestAmount = this._interestService.Value.GetInterestDueForPeriod(interestRate, financePlanActiveVisitBalancesAsOf[financePlanVisit.VisitId]);
            }

            if (interestAmount <= 0m && !hasOverride)
            {
                return null;
            }

            return interestAmount;
        }

        public bool WriteOffInterestOnlyBalance(int visitId)
        {
            FinancePlanVisit financePlanVisit = this._financePlanRepository.Value.GetVisitOnFinancePlan(visitId);

            return this.WriteOffInterestOnlyBalance(financePlanVisit);
        }

        public bool WriteOffInterestOnlyBalance(FinancePlanVisit financePlanVisit)
        {
            if (financePlanVisit == null)
            {
                return false;
            }

            bool wroteOffInterest = financePlanVisit.WriteOffInterestOnlyBalance();

            this._financePlanRepository.Value.InsertOrUpdate(financePlanVisit.FinancePlan);

            return wroteOffInterest;
        }

        public IList<FinancePlanVisitInterestDueResult> GetInterestForVisits(IList<int> visitIds, DateTime? startDate = null, DateTime? endDate = null)
        {
            if (visitIds.IsNullOrEmpty())
            {
                return new List<FinancePlanVisitInterestDueResult>();
            }

            IList<FinancePlanVisitInterestDueResult> interestDueForVisits = this._financePlanRepository.Value.GetFinancePlanVisitInterestDues(visitIds, null, startDate, endDate);

            return interestDueForVisits;
        }

        public IList<FinancePlanVisitInterestDueResult> GetInterestDueForVisits(IList<int> visitIds)
        {
            if (visitIds.IsNullOrEmpty())
            {
                return new List<FinancePlanVisitInterestDueResult>();
            }

            List<FinancePlanStatusEnum> checkStatuses = EnumExt.GetAllInCategory<FinancePlanStatusEnum>(FinancePlanStatusEnumCategory.Active).ToList();
            IList<FinancePlanVisitInterestDueResult> interestDueForVisits = this._financePlanRepository.Value.GetFinancePlanVisitInterestDues(visitIds, checkStatuses);

            IList<FinancePlanVisitInterestDueResult> interestDuePerVisit = new List<FinancePlanVisitInterestDueResult>();
            foreach (IGrouping<int, FinancePlanVisitInterestDueResult> grouping in interestDueForVisits.GroupBy(x => x.VisitId))
            {
                interestDuePerVisit.Add(new FinancePlanVisitInterestDueResult
                {
                    VisitId = grouping.Key,
                    InterestDue = grouping.Select(x => x.InterestDue).Sum()
                });
            }

            return interestDuePerVisit;
        }

        public IList<FinancePlanVisitInterestDueResult> GetInterestAssessedForVisits(IList<int> visitIds)
        {
            if (visitIds.IsNullOrEmpty())
            {
                return new List<FinancePlanVisitInterestDueResult>();
            }

            IList<FinancePlanVisitInterestDueResult> interestAssessedForVisits = this._financePlanRepository.Value.GetFinancePlanVisitInterestAssessments(visitIds);

            return interestAssessedForVisits;
        }

        #endregion

        #region principal amount

        public IList<FinancePlanVisitPrincipalAmountResult> GetPrincipalAmountsForVisits(IList<int> visitIds)
        {
            if (visitIds.IsNullOrEmpty())
            {
                return new List<FinancePlanVisitPrincipalAmountResult>();
            }

            List<FinancePlanStatusEnum> checkStatuses = EnumExt.GetAllInCategory<FinancePlanStatusEnum>(FinancePlanStatusEnumCategory.Active).ToList();
            IList<FinancePlanVisitPrincipalAmountResult> principalAmountForVisits = this._financePlanRepository.Value.GetFinancePlanVisitPrincipalAmounts(visitIds, checkStatuses);

            IList<FinancePlanVisitPrincipalAmountResult> principalAmountPerVisit = new List<FinancePlanVisitPrincipalAmountResult>();
            foreach (IGrouping<int, FinancePlanVisitPrincipalAmountResult> grouping in principalAmountForVisits.GroupBy(x => x.VisitId))
            {
                principalAmountPerVisit.Add(new FinancePlanVisitPrincipalAmountResult
                {
                    VisitId = grouping.Key,
                    Amount = grouping.Sum(x => x.Amount)
                });
            }

            return principalAmountPerVisit;
        }

        #endregion

        #region balance as of

        public AmortizationVisitDateBalances GetFinancePlanActiveVisitsBalancesDaily(FinancePlan financePlan)
        {
            if (financePlan.IsNotOriginated())
            {
                return null;
            }

            VpStatement statement = this._statementService.Value.GetMostRecentStatement(financePlan.VpGuarantor);

            if (statement == null)
            {
                return new AmortizationVisitDateBalances();
            }

            DateTime periodStartDate;
            DateTime periodEndDate;

            if (statement.IsGracePeriod)
            {
                periodStartDate = statement.PeriodStartDate;
                periodEndDate = statement.PeriodEndDate;
            }
            else
            {
                periodEndDate = this._statementDateService.Value.GetStatementPeriodEndDate(statement.OriginalPaymentDueDate, financePlan.VpGuarantor, DateTime.UtcNow, false, false);
                periodStartDate = this._statementService.Value.GetStatementPeriodStartDate(statement, financePlan.VpGuarantor);
            }

            return this.GetFinancePlanActiveVisitsBalancesDailyForPeriod<AmortizationVisitDateBalances, IFinancePlanVisitBalance>(financePlan.FinancePlanVisits, periodStartDate, periodEndDate, x => x);
        }

        private IDictionary<int, IList<Tuple<DateTime, decimal>>> GetFinancePlanActiveVisitsBalancesDailyForPeriod(IList<FinancePlanVisit> financePlanVisits, DateTime start, DateTime end, bool includeInterest = false)
        {
            return this.GetFinancePlanActiveVisitsBalancesDailyForPeriod<Dictionary<int, IList<Tuple<DateTime, decimal>>>, int>(financePlanVisits, start, end, x => x.VisitId, includeInterest);
        }

        private TDict GetFinancePlanActiveVisitsBalancesDailyForPeriod<TDict, TKey>(IList<FinancePlanVisit> financePlanVisits,
            DateTime start,
            DateTime end,
            Func<FinancePlanVisit, TKey> keySelector,
            bool includeInterest = false) where TDict : Dictionary<TKey, IList<Tuple<DateTime, decimal>>>, new()
        {
            TDict currentBalancesAsOf = new TDict();
            int? vpg = financePlanVisits.FirstOrDefault()?.VpGuarantorId;
            if (vpg == null)
            {
                return currentBalancesAsOf;
            }

            // get visits on finance plan
            List<int> visitIds = financePlanVisits.Select(x => x.VisitId).ToList();
            IReadOnlyList<Visit> visits = this._visitService.Value.GetVisits(vpg.Value, visitIds);

            foreach (DateTime processDate in start.ThroughEachDayUntil(end))
            {
                DateTime date = processDate.ToEndOfDay();

                foreach (FinancePlanVisit financePlanVisit in financePlanVisits)
                {
                    DateTime? removedDate = financePlanVisit.HardRemoveDate;
                    if (removedDate.HasValue && removedDate <= date)
                    {
                        continue;
                    }

                    Visit visit = visits.FirstOrDefault(x => x.VisitId == financePlanVisit.VisitId);
                    if (visit == null)
                    {
                        continue;
                    }

                    if (visit.VisitStateEnumForDate(date) != VisitStateEnum.Active)
                    {
                        continue;
                    }

                    decimal financePlanVisitPrincipalBalanceAsOf = financePlanVisit.PrincipalBalanceAsOf(date);
                    if (financePlanVisitPrincipalBalanceAsOf < 0m)
                    {
                        continue;
                    }

                    decimal currentBalance;
                    if (includeInterest)
                    {
                        if (financePlanVisit.InsertDate > date.Date)
                        {
                            // wasn't financed as of date
                            continue;
                        }

                        // principal balance includes uncleared payments, so passing a 0 below
                        currentBalance = financePlanVisit.CalculateBalance(financePlanVisitPrincipalBalanceAsOf, 0, financePlanVisit.InterestDueForDateRange(null, date));
                    }
                    else
                    {
                        currentBalance = financePlanVisitPrincipalBalanceAsOf;
                    }

                    Tuple<DateTime, decimal> currentBalanceForDate = new Tuple<DateTime, decimal>(date, currentBalance);

                    if (!currentBalancesAsOf.ContainsKey(keySelector(financePlanVisit)))
                    {
                        currentBalancesAsOf.Add(keySelector(financePlanVisit), new List<Tuple<DateTime, decimal>>
                        {
                            currentBalanceForDate
                        });
                    }
                    else
                    {
                        currentBalancesAsOf.GetValue(keySelector(financePlanVisit)).Add(currentBalanceForDate);
                    }
                }

            }

            return currentBalancesAsOf;
        }

        public IDictionary<int, decimal> GetFinancePlanActiveVisitBalancesAsOf(FinancePlan financePlan, DateTime date)
        {
            return this.GetFinancePlanActiveVisitsBalancesDailyForPeriod(financePlan.FinancePlanVisits, date, date, true).ToDictionary(x => x.Key, x => x.Value.FirstOrDefault()?.Item2 ?? default(int));
        }

        #endregion

        #region Offers

        public bool RequireRetailInstallmentContract(FinancePlanOffer financePlanOffer, int numberMonthlyPayments)
        {
            if (!this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureSimpleTermsIsEnabled))
            {
                // require RIC if SimpleTerms is disabled
                return true;
            }

            // 4 months is the legal upper limit for a finance plan without a Retail Installment Contract
            const int legalLimitMonthlyPaymentsForSimpleTerms = 4;

            // the finance plan must be 0% interest to be setup without a Retail Installment Contract
            const decimal legalInterestRateForSimpleTerms = 0m;

            // configured value
            int configValueNumberMonthlyPayments = Math.Max(this.Client.Value.SimpleTermsMaximumMonths, 0);

            // ensure the client setting meets legal requirements
            int maximumNumberOfMonthlyPayments = Math.Min(configValueNumberMonthlyPayments, legalLimitMonthlyPaymentsForSimpleTerms);

            if (financePlanOffer.InterestRate == legalInterestRateForSimpleTerms && numberMonthlyPayments <= maximumNumberOfMonthlyPayments)
            {
                // meets criteria to proceed without a Retail Installment Contract
                return false;
            }

            // RIC required
            return true;
        }

        public decimal GetPaymentAmountForNumberOfPayments(FinancePlanCalculationParameters financePlanCalculationParameters, int numberOfMonthlyPayments)
        {
            IList<FinancePlanOffer> offers = this.GetOffers(financePlanCalculationParameters, true);

            FinancePlanOffer offer = offers.Where(x => x.DurationRangeStart >= numberOfMonthlyPayments || numberOfMonthlyPayments <= x.DurationRangeEnd)
                .OrderBy(x => x.DurationRangeStart)
                .FirstOrDefault();

            if (offer == null)
            {
                return -1;
            }

            decimal paymentAmount = this._financePlanOfferService.Value.GetFactoredPaymentAmount(
                financePlanCalculationParameters.Visits,
                numberOfMonthlyPayments,
                offer.InterestRate,
                false,
                financePlanCalculationParameters.InterestFreePeriod);

            decimal roundedAmount = Math.Ceiling(paymentAmount * 100) / 100;
            
            roundedAmount = this._financePlanOfferService.Value.ValidatePaymentAmount(false,
                financePlanCalculationParameters.Visits,
                roundedAmount,
                offer.InterestRate,
                numberOfMonthlyPayments,
                financePlanCalculationParameters.InterestFreePeriod,
                financePlanCalculationParameters.Statement.PaymentDueDate,
                financePlanCalculationParameters.InterestCalculationMethod);

            return roundedAmount;
        }

        public IList<FinancePlanOffer> GetOffers(FinancePlanCalculationParameters financePlanCalculationParameters, bool saveOffer)
        {
            if (financePlanCalculationParameters.Guarantor == null || financePlanCalculationParameters.Statement == null)
            {
                throw new Exception($"Cannot get offers: Guarantor = {financePlanCalculationParameters.Guarantor != null}, Statement = {financePlanCalculationParameters.Statement != null}");
            }

            bool hasFinancialAssistance = this.HasFinancialAssistance(financePlanCalculationParameters.Visits);

            return this.GetOffersWithAmount(financePlanCalculationParameters, null, hasFinancialAssistance, saveOffer);
        }

        public FinancePlanOffer GetOfferForReconfiguration(FinancePlanCalculationParameters financePlanCalculationParameters, FinancePlan financePlanToReconfigure, bool saveOffer)
        {
            if (financePlanCalculationParameters.Guarantor == null || financePlanCalculationParameters.Statement == null)
            {
                throw new Exception($"Cannot get reconfiguration offers: Guarantor = {financePlanCalculationParameters.Guarantor != null}, Statement = {financePlanCalculationParameters.Statement != null}");
            }

            bool hasFinancialAssistance = financePlanToReconfigure.HasFinancialAssistance();

            IList<FinancePlanOffer> offers = this.GetOffersWithAmount(financePlanCalculationParameters, financePlanToReconfigure.FinancePlanId.ToListOfOne(), hasFinancialAssistance, saveOffer);
            if (financePlanCalculationParameters.MonthlyPaymentAmount == 0m)
            {
                // 0 means checking for minimum amount
                FinancePlanOffer minimumOffer = SelectMinimumOffer(offers);
                return minimumOffer;
            }

            FinancePlanOffer bestOffer = SelectOfferWithPaymentAmount(financePlanCalculationParameters.MonthlyPaymentAmount, offers);
            return bestOffer;
        }

        private IList<FinancePlanOffer> GetOffersWithAmount(FinancePlanCalculationParameters financePlanCalculationParameters, IList<int> financePlansToExclude, bool hasFinancialAssistance, bool saveOffer)
        {
            HashSet<int> excludeList = financePlansToExclude?.ToHashSet() ?? new HashSet<int>();
            Guarantor guarantor = financePlanCalculationParameters.Guarantor;
            IList<FinancePlan> applicableFinancePlans = this.GetApplicableFinancePlansForOffer(guarantor, excludeList);

            if (guarantor.ConsolidationStatus == GuarantorConsolidationStatusEnum.Managed || guarantor.ConsolidationStatus == GuarantorConsolidationStatusEnum.Managing)
            {
                List<Guarantor> guarantors = new List<Guarantor>();

                guarantors.AddRange(guarantor.ConsolidationStatus == GuarantorConsolidationStatusEnum.Managed ? guarantor.ActiveManagingConsolidationGuarantors.Select(x => x.ManagingGuarantor) : guarantor.ActiveManagedConsolidationGuarantors.Select(x => x.ManagedGuarantor));

                foreach (Guarantor consolidatedGuarantor in guarantors)
                {
                    IList<FinancePlan> openConsolidationFinancePlans = this.GetApplicableFinancePlansForOffer(consolidatedGuarantor, excludeList).ToList();
                    if (openConsolidationFinancePlans.IsNotNullOrEmpty())
                    {
                        applicableFinancePlans.AddRange(openConsolidationFinancePlans);
                    }
                }
            }

            IList<FinancePlanOffer> financePlanOffers = this._financePlanOfferService.Value.GenerateOffersForUser(
                financePlanCalculationParameters,
                applicableFinancePlans,
                hasFinancialAssistance,
                saveOffer,
                financePlanCalculationParameters.InterestFreePeriod);

            return financePlanOffers;
        }

        public FinancePlanOffer SelectBestOfferWithPaymentAmount(FinancePlanCalculationParameters financePlanCalculationParameters, decimal? monthlyPaymentAmount)
        {
            decimal amount = monthlyPaymentAmount.GetValueOrDefault(financePlanCalculationParameters.MonthlyPaymentAmount);

            IList<FinancePlanOffer> financePlanOffers = this.GetOffers(financePlanCalculationParameters, true);

            return SelectOfferWithPaymentAmount(amount, financePlanOffers);
        }

        private static FinancePlanOffer MakeSureThatNewOfferIsBetter(FinancePlan financePlan, FinancePlanOffer selectedOffer)
        {
            if (financePlan == null)
            {
                return selectedOffer;
            }

            if (selectedOffer == null)
            {
                return financePlan.FinancePlanOffer;
            }

            if (financePlan.CurrentInterestRate < selectedOffer.InterestRate)
            {
                return financePlan.FinancePlanOffer;
            }

            return selectedOffer;
        }

        private static FinancePlanOffer SelectOfferWithPaymentAmount(decimal monthlyPaymentAmount, IEnumerable<FinancePlanOffer> allOffers)
        {
            return allOffers?.Where(x => x != null &&
                                         x.MaxPayment >= monthlyPaymentAmount &&
                                         x.MinPayment <= monthlyPaymentAmount)
                .OrderBy(x => x.InterestRate)
                .FirstOrDefault();
        }

        private static FinancePlanOffer SelectMinimumOffer(IEnumerable<FinancePlanOffer> financePlanOffers)
        {
            return financePlanOffers?.Where(x => x != null).OrderBy(x => x.MinPayment).FirstOrDefault();
        }

        #endregion

        private void LogCustomizationAndAcceptance(Guarantor guarantor, int? termsCmsVersionId, int loggedInVisitPayUserId, bool isUpdate, JournalEventHttpContextDto httpContext, FinancePlan fp)
        {
            if (loggedInVisitPayUserId == SystemUsers.SystemUserId && httpContext == null)
            {
                return;
            }

            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(httpContext, loggedInVisitPayUserId);

            if (fp.IsReconfiguration())
            {
                this._userJournalEventService.Value.LogReconfiguredFinancePlan(journalEventUserContext, guarantor.VpGuarantorId, guarantor.User.UserName, fp.FinancePlanId, fp.FinancePlanOfferSetTypeId, isUpdate);
            }
            else
            {
                bool isClientUser = journalEventUserContext?.IsClientUser ?? false;
                if (isClientUser)
                {
                    this._userJournalEventService.Value.LogClientCustomizeFinancePlan(journalEventUserContext, guarantor.VpGuarantorId, guarantor.User.UserName, fp.FinancePlanId, fp.FinancePlanOfferSetTypeId, isUpdate);
                }
                else
                {
                    JournalEventParameters parameters = EventGuarantorViewedFinancePlanTerms.GetParameters
                    (
                        annualInterestRate: (fp.CurrentInterestRate * 100).ToString("G"),
                        effectiveDate: fp.OriginationDate?.ToShortDateString() ?? "Unknown",
                        finalPaymentDate: fp.OriginationDate?.AddMonths(fp.OriginalDuration ?? 0).ToShortDateString() ?? "Unknown",
                        financedAmount: fp.CurrentFinancePlanBalance.FormatCurrency(),
                        firstPaymentDueDate: fp.FirstPaymentDueDate?.ToShortDateString() ?? "Unknown",
                        newMonthlyPayment: fp.PaymentAmount.FormatCurrency(),
                        numberOfPayments: fp.OriginalDuration.ToString(),
                        paymentAmount: fp.PaymentAmount.FormatCurrency(),
                        previousMonthlyPayment: fp.OriginalPaymentAmount?.FormatCurrency() ?? "$0.00",
                        totalInterestOwed: fp.InterestDue.FormatCurrency(),
                        vpUserName: guarantor.User.UserName
                    );
                    this._userJournalEventService.Value.LogGuarantorCustomizeFinancePlan(journalEventUserContext, parameters, guarantor.VpGuarantorId, guarantor.User.UserName, fp.FinancePlanId, fp.FinancePlanOfferSetTypeId);
                }

                if (termsCmsVersionId.HasValue && isClientUser)
                {
                    this._userJournalEventService.Value.LogClientAcceptedFinancePlanOnBehalfOfGuarantor(journalEventUserContext, guarantor.VpGuarantorId, guarantor.User.UserName, fp.FinancePlanId, guarantor.UseAutoPay);
                }
                else if(termsCmsVersionId.HasValue)
                {
                    this._userJournalEventService.Value.LogGuarantorAcceptFinancePlan(journalEventUserContext, guarantor.VpGuarantorId, guarantor.User.UserName, fp.FinancePlanId, fp.FinancePlanOfferSetTypeId, termsCmsVersionId.Value);
                }
            }
        }

        public bool HasFinancialAssistance(IList<IFinancePlanVisitBalance> financePlanVisitsToConsider)
        {
            return financePlanVisitsToConsider?.Any(x => x.HasFinancialAssistance) ?? false;
        }
        
        public bool MeetsCriteriaToChangePaymentDueDate(Guarantor guarantor)
        {
            List<FinancePlanStatusEnum> checkStatuses = EnumHelper<FinancePlanStatusEnum>.GetValues(FinancePlanStatusEnumCategory.Pending, FinancePlanStatusEnumCategory.Active).ToList();
            List<int> vpGuarantorIds = new List<int> 
            {
                guarantor.VpGuarantorId
            };
            
            if (guarantor.IsConsolidated())
            {
                vpGuarantorIds.AddRange(guarantor.ActiveManagingConsolidationGuarantors.Select(x => x.ManagingGuarantor.VpGuarantorId));
                vpGuarantorIds.AddRange(guarantor.ActiveManagedConsolidationGuarantors.Select(x => x.ManagedGuarantor.VpGuarantorId));
            }

            foreach (int vpGuarantorId in vpGuarantorIds.Distinct())
            {
                bool hasFinancePlans = this._financePlanRepository.Value.HasFinancePlans(vpGuarantorId, checkStatuses);
                if (hasFinancePlans)
                {
                    // has a finance plan, criteria not met
                    return false;
                }
            }
            
            // doesn't have a finance plan, criteria met
            return true;
        }

        public FinancePlan GetFinancePlanByPublicIdPhi(string financePlanPublicIdPhi)
        {
            FinancePlan financePlan = this._financePlanRepository.Value.GetFinancePlanByPublicIdPhi(financePlanPublicIdPhi);
            return financePlan;
        }

        public IList<FinancePlan> GetFinancePlans(int vpGuarantorId)
        {
            IList<FinancePlan> financePlans = this._financePlanRepository.Value.GetFinancePlans(vpGuarantorId);
            return financePlans;
        }

        private void AssignUniquePublicIdPhiToFinancePlan(FinancePlan financePlan)
        {
            do
            {
                financePlan.GenerateAndSetPublicIdPhi();
            } while (this.FinancePlanPublicIdPhiValueAlreadyExists(financePlan.FinancePlanPublicIdPhi));
        }

        private bool FinancePlanPublicIdPhiValueAlreadyExists(string financePlanPublicIdPhi)
        {
            bool valueAlreadyExists = this._financePlanRepository.Value.PublicIdPhiExists(financePlanPublicIdPhi);
            return valueAlreadyExists;
        }

        /*
        // this wasn't referenced anywhere
        public bool CheckIfFinancePlansArePastDue(IList<FinancePlan> financePlans, bool overridePrecidenceCheck = false)
        {
            if (financePlans.IsNullOrEmpty())
            {
                return false;
            }

            bool wasSetPastDue = false;
            foreach (FinancePlan financePlan in financePlans)
            {
                if (!this.ShouldBeInPastDue(financePlan))
                {
                    continue;
                }

                if (this.SetFinancePlanIntoPastDue(financePlan, overridePrecidenceCheck) || financePlan.IsPastDue())
                {
                    wasSetPastDue = true;
                }
            }
            return wasSetPastDue;
        }*/

        /*
        // this wasn't referenced anywhere
        public bool CheckIfFinancePlanShouldBeClosed(FinancePlan financePlan, DateTime? activeUncollectableDate = null)
        {
            FinancePlanStatus oldStatus = financePlan.FinancePlanStatus;

            if (this.CheckIfFinancePlanShouldBeClosedUncollectable(financePlan, activeUncollectableDate))
            {
                financePlan.FinancePlanStatus = this.GetStatusWithEnum(FinancePlanStatusEnum.UncollectableClosed);
                this._financePlanRepository.Value.InsertOrUpdate(financePlan);
            }
            //TODO: VP-339 - we should probably use the VisitStateEnum.Inactive rather than IsRecalled if we end up refactoring the FP state machine
            else if (!financePlan.IsActiveUncollectable() &&
                     !financePlan.IsClosedUncollectable() &&
                     financePlan.FinancePlanVisits.All(x => !x.FinancePlanVisitVisit.VpEligible))
            {
                financePlan.FinancePlanStatus = this.GetStatusWithEnum(FinancePlanStatusEnum.Canceled);
                this._financePlanRepository.Value.InsertOrUpdate(financePlan);
            }

            if (financePlan.IsClosed() && !oldStatus.IsClosed())
            {
                this._session.Transaction.RegisterPostCommitSuccessAction(fp =>
                {
                    this.Bus.Value.PublishMessage(new FinancePlanClosedMessage
                    {
                        FinancePlanId = fp.FinancePlanId,
                        VpGuarantorId = fp.VpGuarantor.VpGuarantorId
                    }).Wait();
                }, financePlan);
            }

            if (financePlan.ReconfiguredFinancePlan != null)
            {
                this.CheckIfReconfigurationIsStillValid(financePlan.ReconfiguredFinancePlan, "Reconfigured Terms Expired");
            }

            return financePlan.IsClosed() && !oldStatus.IsClosed();
        }*/
    }
}
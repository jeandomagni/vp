namespace Ivh.Domain.FinanceManagement.FinancePlan.Services
{
    using System.Collections.Generic;
    using Entities;
    using Interfaces;

    public class InterestRateConsiderationNoneService : IInterestRateConsiderationService
    {
        public decimal GetTotalConsiderationAmount(decimal totalAmountToConsider, IList<FinancePlan> allApplicableFinancePlans)
        {
            return totalAmountToConsider;
        }
    }
}
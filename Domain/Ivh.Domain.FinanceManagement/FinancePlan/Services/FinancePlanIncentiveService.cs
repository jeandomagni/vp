﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Services
{
    using System;
    using Application.Settings.Common.Dtos;
    using Base.Interfaces;
    using Base.Services;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;

    public class FinancePlanIncentiveService : DomainService, IFinancePlanIncentiveService
    {
        public FinancePlanIncentiveService(Lazy<IDomainServiceCommonService> serviceCommonService) : base(serviceCommonService)
        {
        }

        public FinancePlanInterestRefundIncentiveEligibilityEnum IsFinancePlanEligibleForInterestRefundIncentive(FinancePlan financePlan)
        {
            bool isIncentiveFeatureEnabled = this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FinancePlanRefundInterestIncentive);
            if (!isIncentiveFeatureEnabled)
            {
                return FinancePlanInterestRefundIncentiveEligibilityEnum.FeatureIsNotEnabled;
            }
            
            //Guarantor must be Good Standing to qualify
            bool isFinancePlanInGoodStanding = financePlan.CurrentFinancePlanStatus.IsActiveGoodStanding();
            if (!isFinancePlanInGoodStanding)
            {
                return FinancePlanInterestRefundIncentiveEligibilityEnum.FinancePlanIsNotInGoodStanding;
            }

            //We need to do 2 things: refund interest paid and set interest to 0;
            //If this has already happened or the FP is in the condition of 0 paid interest and 0 interest rate we don't need to perform this action
            decimal interestPaid = financePlan.InterestPaidForDate(DateTime.UtcNow);
            bool hasNoInterestToRefund = interestPaid >= 0m;
            decimal interestRate = financePlan.CurrentInterestRate;
            bool hasInterestRateOfZero = interestRate == 0m;

            if (hasNoInterestToRefund && hasInterestRateOfZero)
            {
                return FinancePlanInterestRefundIncentiveEligibilityEnum.HasNoPaidInterestAndInterestRateIsZero;
            }

            bool meetsRefundTrigger = MeetsFinancePlanInterestRefundTrigger(financePlan);
            if (!meetsRefundTrigger)
            {
                return FinancePlanInterestRefundIncentiveEligibilityEnum.FinancePlanDoesNotMeetRefundTriggerCriteria;
            }

            return FinancePlanInterestRefundIncentiveEligibilityEnum.IsEligible;

        }


        /// <summary>
        /// Some clients may have different rules for refunding interest and the amount it should refund
        /// This method allows for variations of amount to refund in the future.
        /// Currently (VP-3938), all interest paid will be refunded. 
        /// </summary>
        /// <param name="financePlan"></param>
        /// <returns></returns>
        public decimal AmountOfInterestToReallocateToPrincipalForIncentive(FinancePlan financePlan)
        {
            //for now we are going to just return what has been paid
            decimal amountPaidToDate = financePlan.InterestPaidForDate(DateTime.UtcNow);
            return amountPaidToDate;
        }

        private bool MeetsFinancePlanInterestRefundTrigger(FinancePlan financePlan)
        {
            FinancePlanIncentiveConfigurationDto config = Client.Value.FinancePlanIncentiveConfiguration;

            switch (config.FinancePlanIncentiveInterestRefundMethod)
            {
                case FinancePlanIncentiveInterestRefundMethodEnum.NumberOfMonthsLeftInFinancePlan:
                    int numberOfMonthsLeftInFinancePlan = Convert.ToInt32(config.Value);
                    decimal balanceWithRefundedInterestAndClearedInterestDue = financePlan.CurrentFinancePlanBalance
                                                          - financePlan.InterestDue 
                                                          + financePlan.InterestPaidForDate(DateTime.UtcNow);//interest paid is represented as a negative
                    decimal paymentAmount = financePlan.PaymentAmount;
                    int monthsBeforePayoff = numberOfMonthsLeftInFinancePlan;
                    bool meetsRefundTrigger = balanceWithRefundedInterestAndClearedInterestDue <= (paymentAmount * monthsBeforePayoff);
                    return meetsRefundTrigger;
                default:
                    return false;
            }

        }

    }
}
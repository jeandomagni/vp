﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Enums;
    using Common.Base.Exceptions;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;
    using Core.SystemException.Interfaces;
    using Entities;
    using Interfaces;
    using Payment.Entities;
    using Payment.Interfaces;

    public class AmortizationService :  DomainService, IAmortizationService
    {
        private readonly Lazy<IInterestService> _interestService;
        private readonly Lazy<IPaymentAllocationService> _paymentAllocationService;
        private readonly Lazy<ISystemExceptionService> _systemExceptionService;

        public AmortizationService(
            Lazy<IDomainServiceCommonService> domainServiceCommonService,
            Lazy<IInterestService> interestService,
            Lazy<IPaymentAllocationService> paymentAllocationService,
            Lazy<ISystemExceptionService> systemExceptionService)
            : base(domainServiceCommonService)
        {
            this._interestService = interestService;
            this._paymentAllocationService = paymentAllocationService;
            this._systemExceptionService = systemExceptionService;
        }

        public IList<AmortizationMonth> GetAmortizationMonthsForFinancePlan(FinancePlan financePlan,
            DateTime firstPaymentDate,
            AmortizationVisitDateBalances visitDateBalances)
        {
            return financePlan.IsActiveOriginated()
                ? this.GetAmortizationMonthsForOriginatedFinancePlan(financePlan, firstPaymentDate, visitDateBalances)
                : this.GetAmortizationMonthsForPendingFinancePlan(financePlan, financePlan.FirstInterestPeriod(), firstPaymentDate, visitDateBalances);
        }

        public IList<AmortizationMonth> GetAmortizationMonthsForOriginatedFinancePlan(FinancePlan financePlan,
            DateTime firstPaymentDate,
            AmortizationVisitDateBalances visitDateBalances,
            bool logException = false)
        {
            int remainingMonths = (int) financePlan.FirstInterestPeriod();
            IList<IFinancePlanVisitBalance> visits = financePlan.ActiveFinancePlanVisitBalances;
            IList<AmortizationMonth> amortizationMonths = null;

            try
            {
                amortizationMonths = this.Amortize(financePlan.PaymentAmount, visits, financePlan.CurrentInterestRate, remainingMonths, firstPaymentDate, visitDateBalances, financePlan.InterestCalculationMethod);
            }
            catch (NegativeAmortizationException ex)
            {
                if (logException)
                {
                    this._systemExceptionService.Value.LogSystemException(new Core.SystemException.Entities.SystemException
                    {
                        FinancePlanId = financePlan.FinancePlanId,
                        SystemExceptionDescription = $"PrincipalPayment = {ex.PrincipalPaymentAmount:C}, InterestPayment = {ex.InterestPaymentAmount:C}",
                        SystemExceptionType = SystemExceptionTypeEnum.NonAmortizingFinancePlan,
                        VpGuarantorId = financePlan.VpGuarantor.VpGuarantorId
                    });
                }
            }

            amortizationMonths = amortizationMonths ?? new List<AmortizationMonth>();

            foreach (AmortizationMonth amortizationMonth in amortizationMonths)
            {
                SetMonthOnAmortization(amortizationMonth, firstPaymentDate);
                amortizationMonth.FinancePlan = financePlan;
            }

            return amortizationMonths;
        }

        public IList<AmortizationMonth> GetAmortizationMonthsForPendingFinancePlan(FinancePlan financePlan,
            FirstInterestPeriodEnum firstInterestPeriodEnum,
            DateTime firstPaymentDueDate,
            AmortizationVisitDateBalances visitDateBalances = null)
        {
            return this.GetAmortizationMonthsForAmounts(
                financePlan.PaymentAmount,
                financePlan.ActiveFinancePlanVisitBalances,
                financePlan.FinancePlanOffer.InterestRate,
                firstInterestPeriodEnum,
                firstPaymentDueDate,
                financePlan.InterestCalculationMethod,
                visitDateBalances);
        }

        public IList<AmortizationMonth> GetAmortizationMonthsForAmounts(decimal monthlyAmount, 
            IList<IFinancePlanVisitBalance> visits,
            decimal interestRate,
            FirstInterestPeriodEnum interestFreePeriod,
            DateTime firstPaymentDueDate,
            InterestCalculationMethodEnum interestCalculationMethod,
            AmortizationVisitDateBalances visitDateBalances = null)
        {
            IList<AmortizationMonth> amortizationMonths = this.GetAmortizationMonths(monthlyAmount, visits, interestRate, interestFreePeriod, firstPaymentDueDate, interestCalculationMethod, visitDateBalances);

            foreach (AmortizationMonth month in amortizationMonths)
            {
                SetMonthOnAmortization(month, firstPaymentDueDate);
            }

            return amortizationMonths;
        }
        
        public int GetAmortizationDurationForAmounts(decimal monthlyAmount,
            IList<IFinancePlanVisitBalance> visits,
            decimal interestRate,
            FirstInterestPeriodEnum interestFreePeriod,
            DateTime firstPaymentDueDate,
            InterestCalculationMethodEnum interestCalculationMethod)
        {
            return this.GetAmortizationMonths(monthlyAmount, visits, interestRate, interestFreePeriod, firstPaymentDueDate, interestCalculationMethod).Count;
        }

        public int? CalculateNumberOfTotalCurrentDuration(FinancePlan financePlan, AmortizationVisitDateBalances visitDateBalances)
        {
            //Goal here is to figure out how much time has elapsed + the months left in the loan.
            DateTime? originationDate = financePlan.OriginationDate;
            DateTime now = DateTime.UtcNow;
            if (!originationDate.HasValue)
            {
                return null;
            }

            int numberOfMonthsElapsedSinceOrigination = (now.Year - originationDate.Value.Year) * 12 + now.Month - originationDate.Value.Month;

            IList<AmortizationMonth> amortizedMonths = this.GetAmortizationMonthsForOriginatedFinancePlan(financePlan, now, visitDateBalances);

            int totalCurrentDuration = amortizedMonths.Count + numberOfMonthsElapsedSinceOrigination;
            return totalCurrentDuration;
        }
        
        private IList<AmortizationMonth> GetAmortizationMonths(decimal monthlyAmount, 
            IList<IFinancePlanVisitBalance> visits,
            decimal interestRate, 
            FirstInterestPeriodEnum interestFreePeriod,
            DateTime firstPaymentDueDate,
            InterestCalculationMethodEnum interestCalculationMethod,
            AmortizationVisitDateBalances visitDateBalances = null)
        {
            IList<AmortizationMonth> amortizationMonths = null;

            try
            {
                amortizationMonths = this.Amortize(monthlyAmount, visits, interestRate, (int) interestFreePeriod, firstPaymentDueDate, visitDateBalances, interestCalculationMethod);
            }
            catch (NegativeAmortizationException)
            {
            }

            return amortizationMonths ?? new List<AmortizationMonth>();
        }

        /// <summary>
        /// this does the actual amortization
        /// public to unit test directly, not on interface
        /// </summary>
        public IList<AmortizationMonth> Amortize(decimal monthlyPaymentAmount, 
            IList<IFinancePlanVisitBalance> visits, 
            decimal yearlyInterestRate, 
            int interestFreePeriodInMonths,
            DateTime firstPaymentDueDate,
            AmortizationVisitDateBalances visitDateBalances,
            InterestCalculationMethodEnum interestCalculationMethod)
        {
            return interestCalculationMethod == InterestCalculationMethodEnum.Daily  
                ? this.AmortizeDaily(monthlyPaymentAmount, visits, yearlyInterestRate, interestFreePeriodInMonths, firstPaymentDueDate, visitDateBalances)
                : this.AmortizeMonthly(monthlyPaymentAmount, visits, yearlyInterestRate, interestFreePeriodInMonths);
        }

        private IList<AmortizationMonth> AmortizeDaily(decimal monthlyPaymentAmount,
            IList<IFinancePlanVisitBalance> visits,
            decimal yearlyInterestRate,
            int interestFreePeriodInMonths,
            DateTime firstPaymentDueDate,
            IDictionary<IFinancePlanVisitBalance, IList<Tuple<DateTime, decimal>>> visitDateBalances = null)
        {
            IList<AmortizationMonth> retList = new List<AmortizationMonth>();
            decimal totalBalance = visits.Sum(x => x.PrincipalBalance);

            if (totalBalance <= 0 || !ValidateAmount(monthlyPaymentAmount))
            {
                return retList;
            }

            ValidateYearlyInterestRate(yearlyInterestRate);

            IList<AmortizationObject> objs = this.CreateAmortizationObjects(visits, yearlyInterestRate, visitDateBalances);
            IDictionary<AmortizationObject, decimal> monthlyPaymentPerVisit = this.GetPrincipalPerVisit(objs, monthlyPaymentAmount - objs.Sum(x => x.InterestDue));
            int currentMonthCount = 0;
            int remainingInterestFreeMonths = interestFreePeriodInMonths;
            int totalInterestFreeMonths = remainingInterestFreeMonths + (int)(visits.Where(x => x.OverrideInterestRate == 0m).Sum(x => x.OverridesRemaining) / (visits.Count > 0 ? visits.Count: 1));
            int paymentDueDay = firstPaymentDueDate.Day;
            int cycleOffset = 1;
            firstPaymentDueDate = firstPaymentDueDate == DateTime.MinValue ? firstPaymentDueDate.AddMonths(cycleOffset + 1) : firstPaymentDueDate;
            
            if (yearlyInterestRate > 0m && totalInterestFreeMonths == 0 && visitDateBalances == null)
            {
                throw new ArgumentException($"unable to calculate daily interest without historical data for visitids: {string.Join(",", visits.Select(x => x.VisitId)) }");
            }

            void ApplyMonthlyPaymentToAmortizationObject(AmortizationObject fpv)
            {
                if (monthlyPaymentPerVisit?.ContainsKey(fpv) == true)
                {
                    fpv.FinancePlanVisitBalance -= GetMonthlyPrincipalPaymentAmountForAmortizationObject(fpv);
                }
            }

            decimal GetMonthlyPrincipalPaymentAmountForAmortizationObject(AmortizationObject fpv)
            {
                return Math.Round(monthlyPaymentPerVisit[fpv], 2);
            }

            int totalLoops = 200;
            while (totalBalance > 0)
            {
                totalLoops--;
                if (totalLoops <= 0)
                {
                    this.LoggingService.Value.Warn(() =>  $"AmortizationService::Amortize - totalBalance loop iterated 200 times with total balance of {totalBalance}, and a month count of {currentMonthCount}");
                    break;
                }

                DateTime currentPaymentDueDate = firstPaymentDueDate.AddMonths(currentMonthCount - cycleOffset).ToPaymentDueDateForDay(paymentDueDay);
                DateTime currentStatementDate = currentPaymentDueDate.AddDays(-this.Client.Value.GracePeriodLength);

                DateTime nextPaymentDueDate = currentPaymentDueDate.AddMonths(1).ToPaymentDueDateForDay(paymentDueDay);
                DateTime nextStatementDate = nextPaymentDueDate.AddDays(-this.Client.Value.GracePeriodLength);
                
                AmortizationMonth currentMonth = new AmortizationMonth
                {
                    InterestAmount = 0m,
                    MonthNumber = currentMonthCount,
                    PrincipalAmount = 0m,
                    MonthDate = nextPaymentDueDate
                };

                // interest
                if (remainingInterestFreeMonths <= 0)
                {
                    // handle the interest due on first PDD from a reconfig/combined plan scenario
                    IList<AmortizationObject> applicableObjs = remainingInterestFreeMonths > 0 ? objs.Where(x => x.InterestDue > 0).ToList() : objs;
                    
                    foreach (AmortizationObject fpv in applicableObjs)
                    {
                        decimal interestRateToUse = yearlyInterestRate;
                        if (fpv.OverridesRemaining > 0)
                        {
                            interestRateToUse = fpv.OverrideInterestRate.GetValueOrDefault(yearlyInterestRate);
                            fpv.OverridesRemaining--;
                        }

                        if (interestRateToUse == 0m)
                        {
                            //apply principal payment amount from previous statement cycle
                            if (currentMonthCount > 0)
                            {
                                ApplyMonthlyPaymentToAmortizationObject(fpv);
                            }
                            continue;
                        }

                        IList<Tuple<DateTime, decimal>> dateAmounts;
                        //use the given balances if this is the first cycle of an existing FP
                        if (fpv.DateBalances != null && currentMonthCount < 1 && totalInterestFreeMonths < 1)
                        {
                            dateAmounts = fpv.DateBalances;
                        }
                        else
                        {
                            dateAmounts = new List<Tuple<DateTime, decimal>>();

                            //first 21 days - currentBalance
                            foreach (DateTime dateTime in currentStatementDate.ThroughEachDayUntil(currentPaymentDueDate.AddDays(-1)))
                            {
                                dateAmounts.Add(new Tuple<DateTime, decimal>(dateTime, fpv.FinancePlanVisitBalance));
                            }

                            //apply principal payment amount from previous statement cycle
                            if (currentMonthCount > 0)
                            {
                                ApplyMonthlyPaymentToAmortizationObject(fpv);
                            }

                            //pdd on 21st day, so 22nd to endOfCycle is current balance less payment
                            foreach (DateTime dateTime in currentPaymentDueDate.ThroughEachDayUntil(nextStatementDate.AddDays(-1)))
                            {
                                dateAmounts.Add(new Tuple<DateTime, decimal>(dateTime, fpv.FinancePlanVisitBalance));
                            }
                        }
                        
                        decimal interestAmount = this._interestService.Value.GetInterestDueForPeriod(interestRateToUse, dateAmounts);

                        currentMonth.InterestAmount += interestAmount;
                    }
                }
                else if(currentMonthCount > 0)
                {
                    foreach (AmortizationObject fpv in objs)
                    {
                        //just apply principal payment amount from previous statement cycle
                        ApplyMonthlyPaymentToAmortizationObject(fpv);
                    }
                }

                // handle the interest due on first PDD from a reconfig/combined plan scenario
                if (currentMonthCount == 0 && objs.Any(x => x.InterestDue > 0m) && totalInterestFreeMonths > 0)
                {
                    foreach (AmortizationObject fpv in objs.Where(x => x.InterestDue > 0))
                    {
                        currentMonth.InterestAmount += fpv.InterestDue;
                        fpv.InterestDue = 0m;
                    }
                }
                
                // decrease remaining interest free months
                remainingInterestFreeMonths = Math.Max(remainingInterestFreeMonths - 1, 0);

                // remaining amount after interest
                decimal remainingAmount = monthlyPaymentAmount - currentMonth.InterestAmount;
                if (remainingAmount <= 0m)
                {
                    ValidateAmortization(currentMonth.PrincipalAmount, currentMonth.InterestAmount);
                    break;
                }

                // principal
                monthlyPaymentPerVisit = this.GetPrincipalPerVisit(objs, remainingAmount);
                if (monthlyPaymentPerVisit.IsNullOrEmpty())
                {
                    currentMonth.PrincipalAmount = remainingAmount;
                    retList.Add(currentMonth);
                    return retList;
                }

                foreach (AmortizationObject fpv in objs)
                {
                    if (monthlyPaymentPerVisit.ContainsKey(fpv) && monthlyPaymentPerVisit[fpv] <= 0m)
                    {
                        continue;
                    }

                    decimal visitAmount = GetMonthlyPrincipalPaymentAmountForAmortizationObject(fpv);
                    currentMonth.PrincipalAmount += visitAmount;
                }

                currentMonth.PrincipalAmount = Math.Min(currentMonth.PrincipalAmount, totalBalance);
                totalBalance -= currentMonth.PrincipalAmount;

                // look for negative amortization - this throws when found
                if (retList.Count > 0 && currentMonth.InterestAmount > 0 && currentMonth.PrincipalAmount == retList.Last().PrincipalAmount)
                {
                    ValidateAmortization(currentMonth.PrincipalAmount, currentMonth.InterestAmount);
                }

                retList.Add(currentMonth);
                currentMonthCount++;
            }

            return retList;
        }

        private IList<AmortizationMonth> AmortizeMonthly(decimal monthlyPaymentAmount, IList<IFinancePlanVisitBalance> visits, decimal yearlyInterestRate, int interestFreePeriodInMonths)
        {
            IList<AmortizationMonth> retList = new List<AmortizationMonth>();
            decimal totalBalance = visits.Sum(x => x.PrincipalBalance);

            if (totalBalance <= 0 || !ValidateAmount(monthlyPaymentAmount))
            {
                return retList;
            }

            ValidateYearlyInterestRate(yearlyInterestRate);

            IList<AmortizationObject> objs = this.CreateAmortizationObjects(visits, yearlyInterestRate);

            int currentMonthCount = 0;
            int remainingInterestFreeMonths = interestFreePeriodInMonths;
           
            int totalLoops = 200;
            while (totalBalance > 0)
            {
                totalLoops--;
                if (totalLoops <= 0)
                {
                    this.LoggingService.Value.Warn(() =>  $"AmortizationService::Amortize - totalBalance loop iterated 200 times with total balance of {totalBalance}, and a month count of {currentMonthCount}");
                    break;
                }

                AmortizationMonth currentMonth = new AmortizationMonth
                {
                    InterestAmount = 0m,
                    MonthNumber = currentMonthCount,
                    PrincipalAmount = 0m
                };

                // interest
                if (remainingInterestFreeMonths <= 0)
                {
                    // handle the interest due on first PDD from a reconfig/combined plan scenario
                    IList<AmortizationObject> applicableObjs = remainingInterestFreeMonths > 0 ? objs.Where(x => x.InterestDue > 0).ToList() : objs;

                    foreach (AmortizationObject fpv in applicableObjs)
                    {
                        decimal interestRateToUse = yearlyInterestRate;
                        if (fpv.OverridesRemaining > 0)
                        {
                            interestRateToUse = fpv.OverrideInterestRate.GetValueOrDefault(yearlyInterestRate);
                            fpv.OverridesRemaining--;
                        }

                        decimal interestAmount = this._interestService.Value.GetMonthlyInterestAmountForBalance(interestRateToUse, fpv.FinancePlanVisitBalance);
                        currentMonth.InterestAmount += interestAmount;
                    }
                }
                // handle the interest due on first PDD from a reconfig/combined plan scenario
                if (currentMonthCount == 0 && objs.Any(x => x.InterestDue > 0m) && remainingInterestFreeMonths > 0)
                {
                    foreach (AmortizationObject fpv in objs.Where(x => x.InterestDue > 0))
                    {
                        currentMonth.InterestAmount += fpv.InterestDue;
                        fpv.InterestDue = 0m;
                    }
                }
                // decrease remaining interest free months
                remainingInterestFreeMonths = Math.Max(remainingInterestFreeMonths - 1, 0);

                // remaining amount after interest
                decimal remainingAmount = monthlyPaymentAmount - currentMonth.InterestAmount;
                if (remainingAmount <= 0m)
                {
                    break;
                }

                // principal
                IDictionary<AmortizationObject, decimal> monthlyPaymentPerVisit = this.GetPrincipalPerVisit(objs, remainingAmount);
                foreach (AmortizationObject fpv in objs)
                {
                    if (monthlyPaymentPerVisit.ContainsKey(fpv) && monthlyPaymentPerVisit[fpv] <= 0m)
                    {
                        continue;
                    }

                    decimal visitAmount = Math.Round(monthlyPaymentPerVisit[fpv], 2);
                    currentMonth.PrincipalAmount += visitAmount;
                    fpv.FinancePlanVisitBalance -= visitAmount;
                }

                currentMonth.PrincipalAmount = Math.Min(currentMonth.PrincipalAmount, totalBalance);
                totalBalance -= currentMonth.PrincipalAmount;

                // look for negative amortization - this throws when found
                ValidateAmortization(currentMonth.PrincipalAmount, currentMonth.InterestAmount);

                retList.Add(currentMonth);
                currentMonthCount++;
            }

            return retList;
        }

        private IList<AmortizationObject> CreateAmortizationObjects(IList<IFinancePlanVisitBalance> visits, decimal yearlyInterestRate, IDictionary<IFinancePlanVisitBalance, IList<Tuple<DateTime, decimal>>> visitDateBalances = null)
        {
            IList<AmortizationObject> objs = new List<AmortizationObject>();

            IList<BillingApplication> billingApplications = this._paymentAllocationService.Value.BillingApplications();
            if (billingApplications.IsNotNullOrEmpty())
            {
                foreach (BillingApplication billingApplication in billingApplications.OrderBy(x => x.Priority))
                {
                    IEnumerable<IFinancePlanVisitBalance> visitsWithBillingApplication = visits.Where(x => string.Equals(x.BillingApplication.TrimNullSafe(), billingApplication.Name, StringComparison.CurrentCultureIgnoreCase));
                    foreach (IFinancePlanVisitBalance visit in visitsWithBillingApplication)
                    {
                        IList<Tuple<DateTime, decimal>> dateBalances = null;
                        if (visitDateBalances?.ContainsKey(visit) ?? false)
                        {
                            dateBalances = visitDateBalances[visit];
                        }
                        objs.Add(new AmortizationObject(visit, billingApplication, yearlyInterestRate, dateBalances));
                    }
                }
            }

            if (objs.GroupBy(x => x.BillingApplicationName).Count() == 1)
            {
                // all the billing applications are the same, just use pro-rata.
                objs.Clear();
            }

            if (objs.Count == 0)
            {
                // assume pro-rata at this point
                BillingApplication proRata = new BillingApplication("ProRata", 1, 1);

                foreach (IFinancePlanVisitBalance visit in visits)
                {
                    IList<Tuple<DateTime, decimal>> dateBalances = null;
                    if (visitDateBalances?.ContainsKey(visit) ?? false)
                    {
                        dateBalances = visitDateBalances[visit];
                    }
                    objs.Add(new AmortizationObject(visit, proRata, yearlyInterestRate, dateBalances));
                }
            }

            return objs;
        }

        #region principal per visit calculations

        private IDictionary<AmortizationObject, decimal> GetPrincipalPerVisit(IList<AmortizationObject> objs, decimal principalAvailable)
        {
            if (objs.IsNullOrEmpty() || objs.Sum(x => x.FinancePlanVisitBalance) <= 0m || principalAvailable <= 0m)
            {
                return new Dictionary<AmortizationObject, decimal>();
            }

            return this.DistributeVisits(objs, principalAvailable);
        }
        
        /// <summary>
        /// heavily based on DistributeVisits in BillingApplicationPaymentAllocationService,
        /// but with changes to allow for ProRata and broken into some smaller methods
        /// </summary>
        /// <returns></returns>
        private IDictionary<AmortizationObject, decimal> DistributeVisits(IList<AmortizationObject> visits, decimal maxToDistribute)
        {
            // get billing applications used by these visits
            IList<BillingApplication> billingApplications = visits
                .GroupBy(x => x.BillingApplicationName)
                .Select(x => x.First())
                .Select(x => new BillingApplication(x.BillingApplicationName, x.PaymentAllocationPercentage, x.PaymentAllocationPriority))
                .ToList();

            IDictionary<string, decimal> principalPerBillingApplication = billingApplications.ToDictionary(x => x.Name, x => visits.Where(fpv => fpv.BillingApplicationName == x.Name).Sum(fpv => fpv.FinancePlanVisitBalance));

            decimal calculatedMaxToDistributeForAllVisitTypes = Math.Min(principalPerBillingApplication.Values.Sum(), maxToDistribute);
            
            #region max by visit type

            IDictionary<string, decimal> maxByVisitType = billingApplications.Select(x => new
            {
                BillingApplication = x.Name,
                MaxToDistribute = Math.Round(x.Percent * calculatedMaxToDistributeForAllVisitTypes, 2)
            }).ToDictionary(k => k.BillingApplication, v => v.MaxToDistribute);

            if (maxByVisitType.Sum(x => x.Value) > calculatedMaxToDistributeForAllVisitTypes)
            {
                this.FixRoundingErrors(billingApplications, maxByVisitType, calculatedMaxToDistributeForAllVisitTypes, false);
            }

            if (maxByVisitType.Sum(x => x.Value) < calculatedMaxToDistributeForAllVisitTypes)
            {
                this.FixRoundingErrors(billingApplications, maxByVisitType, calculatedMaxToDistributeForAllVisitTypes, true);
            }

            #endregion

            #region adjust for the actual visit sum needed for each type

            decimal carryOver = 0m;

            if (billingApplications.Count > 1)
            {
                // first create custom sort order: lowest priority excess goes to highest priority, then highest priority excess goes to next highest, etc
                // e.g., 10 => 0, 0 => 1, 1 => 2, 2 => 3....9 => 10.
                int lowestPriority = billingApplications.Select(x => x.Priority).Max();

                IList<string> customSort = billingApplications.OrderBy(x => x.Priority == lowestPriority ? x.Priority * -1 : x.Priority).Select(x => x.Name).ToList();

                // add the first one in again at the end.
                customSort.Add(customSort.First());

                foreach (string key in customSort)
                {
                    maxByVisitType[key] += carryOver;
                    carryOver = 0m;

                    decimal amountAvailableToDistribute = maxByVisitType[key];
                    if (!principalPerBillingApplication.ContainsKey(key) || principalPerBillingApplication[key] >= amountAvailableToDistribute)
                    {
                        continue;
                    }

                    // we need less than is available
                    carryOver = maxByVisitType[key] - principalPerBillingApplication[key];
                    maxByVisitType[key] = principalPerBillingApplication[key];
                }
            }

            #endregion

            // Do some post rounding adjustments.
            int difference = Convert.ToInt32((calculatedMaxToDistributeForAllVisitTypes - maxByVisitType.Values.Sum()) * 100);
            while (difference > 0) //not enough
            {
                foreach (string key in billingApplications.OrderBy(x => x.Priority).Select(x => x.Name))
                {
                    maxByVisitType[key] += 0.01m;
                    if (--difference == 0)
                    {
                        break;
                    }
                }
            }

            while (difference < 0) //too much
            {
                foreach (string key in billingApplications.OrderByDescending(x => x.Priority).Select(x => x.Name))
                {
                    maxByVisitType[key] -= 0.01m;
                    if (++difference == 0)
                    {
                        break;
                    }
                }
            }

            IDictionary<AmortizationObject, decimal> principalPerVisit = visits.ToDictionary(x => x, v => 0m);
            IDictionary<string, decimal> totalDistributed = billingApplications.ToDictionary(k => k.Name, v => 0m);
            
            var groupedOrderedVisitsList = visits.Join(billingApplications.Where(x => principalPerBillingApplication[x.Name] > 0m), o => o.BillingApplicationName, i => i.Name, (o, i) => new {Visit = o, BillingApplication = i}).GroupBy(x => x.BillingApplication.Name);
            
            foreach (var billingApplicationVisits in groupedOrderedVisitsList.OrderBy(x => billingApplications.First(y => y.Name == x.Key).Priority))
            {
                string key = billingApplicationVisits.First().BillingApplication.Name;
                maxByVisitType[key] += carryOver;

                foreach (AmortizationObject fpv in billingApplicationVisits.Select(x => x.Visit))
                {
                    decimal unroundedAmount = maxByVisitType[key] * (fpv.FinancePlanVisitBalance / principalPerBillingApplication[key]);

                    principalPerVisit[fpv] += unroundedAmount;
                    totalDistributed[key] += Math.Round(unroundedAmount, 2);
                }
            }

            decimal totalRounded = Math.Round(principalPerVisit.Values.Sum(), 2);
            decimal amountMissing = totalRounded - totalDistributed.Values.Sum();

            List<AmortizationObject> keys = principalPerVisit.Keys.ToList();
            foreach (AmortizationObject key in keys)
            {
                principalPerVisit[key] = Math.Round(principalPerVisit[key], 2);
            }

            this.RoundAllocatedAmount(amountMissing, principalPerVisit);
            
            return principalPerVisit;
        }

        private void FixRoundingErrors(IEnumerable<BillingApplication> billingApplications, IDictionary<string, decimal> maxByVisitType, decimal max, bool addValue)
        {
            IList<BillingApplication> orderedBillingApplications;
            decimal value;

            if (addValue)
            {
                value = 0.01m;
                orderedBillingApplications = billingApplications.OrderBy(x => x.Priority).ToList();
            }
            else
            {
                value = decimal.Negate(0.01m);
                orderedBillingApplications = billingApplications.OrderByDescending(x => x.Priority).ToList();
            }

            int iterationCount = 0;
            while (true)
            {
                foreach (BillingApplication billingApplication in orderedBillingApplications)
                {
                    maxByVisitType[billingApplication.Name] += value;
                    if (maxByVisitType.Sum(x => x.Value) == max)
                    {
                        break;
                    }
                }
                if (maxByVisitType.Sum(x => x.Value) == max)
                {
                    break;
                }

                iterationCount++;
                if (iterationCount > 50)
                {
                    break;
                }
            }
        }

        private void RoundAllocatedAmount(decimal amountToDistribute, IDictionary<AmortizationObject, decimal> allocations)
        {
            decimal tracker = amountToDistribute;
            if (tracker != 0m)
            {
                List<AmortizationObject> keys = allocations.Keys.ToList();

                foreach (AmortizationObject key in keys)
                {
                    decimal amount = allocations[key];

                    if (tracker > 0 && (0.01m <= key.FinancePlanVisitBalance))
                    {
                        allocations[key] += 0.01m;
                        tracker -= 0.01m;
                    }
                    else if (tracker < 0 && amount - 0.01m >= 0)
                    {
                        allocations[key] -= 0.01m;
                        tracker += 0.01m;
                    }
                }

                if (tracker == 0m)
                {
                    return;
                }

                // check a second time
                foreach (AmortizationObject key in keys)
                {
                    decimal amount = allocations[key];

                    if (tracker > 0 && (0.01m <= key.FinancePlanVisitBalance))
                    {
                        allocations[key] += 0.01m;
                        tracker -= 0.01m;
                    }
                    else if (tracker < 0 && amount - 0.01m >= 0)
                    {
                        allocations[key] -= 0.01m;
                        tracker += 0.01m;
                    }
                }
            }
        }

        #endregion

        private static void SetMonthOnAmortization(AmortizationMonth month, DateTime payDate)
        {
            month.MonthDate = payDate.AddMonths(month.MonthNumber);
        }

        private static void ValidateAmortization(decimal principal, decimal interest)
        {
            if (principal <= interest)
            {
                throw new NegativeAmortizationException(principal, interest);
            }
        }

        private static bool ValidateAmount(decimal amount)
        {
            return amount > 0;
        }

        private static void ValidateYearlyInterestRate(decimal yearlyInterestRate)
        {
            if (yearlyInterestRate >= 0 && yearlyInterestRate <= 1)
            {
                return;
            }

            if (yearlyInterestRate < 0)
            {
                throw new Exception("Cannot have an interest rate that is less than zero");
            }

            throw new Exception("Cannot have an interest rate that is greater than 1, implies greater than 100%");
        }

        private class AmortizationObject
        {
            public AmortizationObject(IFinancePlanVisitBalance visit, BillingApplication billingApplication, decimal baseInterestRate, IList<Tuple<DateTime,decimal>> dateBalances = null)
            {
                this.BillingApplicationName = billingApplication.Name;
                this.FinancePlanVisitBalance = visit.PrincipalBalance;
                this.InterestDue = visit.InterestDue;
                this.InterestRate = baseInterestRate;
                this.OverrideInterestRate = visit.OverrideInterestRate;
                this.OverridesRemaining = visit.OverridesRemaining;
                this.PaymentAllocationPercentage = billingApplication.Percent;
                this.PaymentAllocationPriority = billingApplication.Priority;
                this.DateBalances = dateBalances;
            }

            public string BillingApplicationName { get; }

            /// <summary>
            /// principal balance only
            /// </summary>
            public decimal FinancePlanVisitBalance { get; set; }

            public decimal InterestDue { get; set; }

            public decimal InterestRate { get; }

            public decimal? OverrideInterestRate { get; }

            public int OverridesRemaining { get; set; }

            public decimal PaymentAllocationPercentage { get; }

            public int PaymentAllocationPriority { get; }

            public IList<Tuple<DateTime, decimal>> DateBalances { get; set; }
        }
    }
}

﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IFinancePlanAmountDueRepository : IRepository<FinancePlanAmountDue>
    {
    }
}
﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Interfaces
{
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;
    using Guarantor.Entities;
    using Ivh.Application.FinanceManagement.Common.Dtos;
    using System;
    using System.Collections.Generic;
    using Visit.Entities;

    public interface IFinancePlanRepository : IRepository<FinancePlan>
    {
        FinancePlan GetFinancePlan(int vpGuarantorId, int financePlanId);
        IList<FinancePlan> GetFinancePlansByIds(int? vpGuarantorId, IList<int> financePlanIds);
        FinancePlanResults GetFinancePlans(int vpGuarantorId, FinancePlanFilter filter, int batch, int batchSize);
        FinancePlanCount GetFinancePlanTotalsWithoutSum(int vpGuarantorId, FinancePlanFilter filter);
        IList<FinancePlan> GetAllFinancePlans(int vpGuarantorId, IList<FinancePlanStatusEnum> financePlanStatuses);
        IList<FinancePlan> GetAllFinancePlans(IList<int> vpGuarantorIds, IList<FinancePlanStatusEnum> financePlanStatuses);
        IReadOnlyList<FinancePlan> GetFinancePlans(IList<FinancePlanStatusEnum> financePlanStatuses);
        IList<FinancePlan> GetAllOpenFinancePlans(int vpGuarantorId);
        bool HaveOpenFinancePlans(IList<int> vpGuarantorIds);
        IList<FinancePlan> GetAllOpenFinancePlans(IList<int> vpGuarantorIds);
        IList<FinancePlan> GetAllPendingAcceptanceFinancePlans(IList<int> vpGuarantorIds, bool isReconfiguration = false);
        IList<Guarantor> GetFirstPendingAcceptanceFinancePlanClientOnlyOfferGuarantor(IList<int> vpGuarantorIds, bool isReconfiguration = false);
        IList<Guarantor> GetFirstPendingAcceptanceFinancePlanPatientOfferGuarantor(IList<int> vpGuarantorIds, bool isReconfiguration = false);
        IList<FinancePlan> GetAllPendingFinancePlans(int vpGuarantorId);
        IList<FinancePlan> GetAllActiveFinancePlans(int vpGuarantorId);
        IList<int> GetAllActiveFinancePlanIds(int vpGuarantorId);
        decimal GetTotalPaymentAmountForOpenFinancePlans(int vpGuarantorId);
        FinancePlanCreditAgreement GetFinancePlanCreditAgreementByContractNumber(string contactNumber);
        FinancePlan GetFinancePlanByOfferId(int financePlanOfferId);
        bool FinancePlanUniqueIdIsUnique(string financePlanUniqueId);
        int GetActiveFinancePlanCount(int vpGuarantorId);
        int GetFinancePlanCount(int vpGuarantorId, IList<FinancePlanStatusEnum> statuses);
        bool HasFinancePlans(int vpGuarantorId, IList<FinancePlanStatusEnum> statuses);
        IList<FinancePlan> GetFinancePlans(int vpGuarantorId, IList<FinancePlanStatusEnum> financePlanStatuses);
        IList<FinancePlan> GetAllPendingAcceptanceFinancePlans(int vpGuarantorId, DateTime originationDate);
        IList<FinancePlanStatus> GetFinancePlanStatuses();
        IList<FinancePlanType> GetFinancePlanTypes();
        IList<int> AreVisitIdsOnActiveFinancePlan(IList<int> visitIds, DateTime? asOfDate = null);
        IList<Visit> AreVisitsOnActiveFinancePlan(IList<Visit> visits, DateTime? asOfDate = null);
        IList<Visit> AreVisitsOnActiveOrPendingFinancePlan(IList<Visit> visits, DateTime? asOfDate = null);
        IList<FinancePlanVisitResult> AreVisitsOnFinancePlan_FinancePlanVisitResult(IList<int> visitIds, DateTime? asOfDate, List<FinancePlanStatusEnum> listOfStatuses = null);
        IList<FinancePlanVisit> AreVisitsOnFinancePlan_FinancePlanVisit(IList<int> visitIds, DateTime? asOfDate, List<FinancePlanStatusEnum> listOfStatuses = null);
        FinancePlan GetFinancePlanWithVisit(Visit visit);
        FinancePlan GetFinancePlanWithVisitId(int visitId);
        //IList<PaymentBucketResult> GetBucketsAssociatedWithFinancePlan(int financePlanId, IList<PaymentStatusEnum> paymentStatuses = null, bool allowZeroScheduledAmount = false);
        //IList<FinancePlanVisitPeriodTotal> GetVisitsOnFinancePlansWithTotalNonPaymentTransactionsForDateRange(int guarantorId, DateTime startingDate, DateTime endingDate);
        IList<FinancePlan> GetFinancePlansFromVisits(IList<int> visitIds);
        bool IsVisitOnAnyStatusFinancePlan(int visitId);
        IList<FinancePlan> GetFinancePlansIncludingClosedFromVisits(IList<int> visitIds);
        IList<FinancePlan> GetAllOriginatedFinancePlansIncludingClosedFromVisits(IList<int> visitIds);

        /// <summary>
        /// gets the most recent fp for each visitId, including closed fps
        /// </summary>
        /// <param name="visitIds"></param>
        /// <returns></returns>
        IDictionary<int, FinancePlan> GetMostRecentFinancePlanForVisitIds(IList<int> visitIds);

        IList<FinancePlanVisitResult> GetVisitOnFinancePlanResultsBySourceSystemKey(IList<string> sourceSystemKeys, int billingSystemId);
        FinancePlanVisit GetVisitOnFinancePlan(int visitId);
        IList<FinancePlanVisit> GetVisitOnAnyFinancePlan(int visitId);
        IList<int> GetVisitIdsOnFinancePlans(int vpGuarantorId, int? financePlanId, IList<FinancePlanStatusEnum> checkStatuses);
        IList<int> GetFinancePlanGuarantorIdsByStatementDueDate(DateTime statementDueDate);
        IList<int> GetFinancePlanIdsWithAmountDueForDate(DateTime dateToCheck, int? vpGuarantorId = null);
        IList<FinancePlanVisitInterestDueResult> GetFinancePlanVisitInterestDues(IList<int> visitIds, IList<FinancePlanStatusEnum> checkStatuses, DateTime? startDate = null, DateTime? endDate = null);
        IList<FinancePlanVisitInterestDueResult> GetFinancePlanVisitInterestAssessments(IList<int> visitIds);
        IList<FinancePlanVisitPrincipalAmountResult> GetFinancePlanVisitPrincipalAmounts(IList<int> visitIds, IList<FinancePlanStatusEnum> checkStatuses);
        FinancePlan GetFinancePlanByPublicIdPhi(string id);
        IList<FinancePlan> GetFinancePlans(int vpGuarantorId);
        bool PublicIdPhiExists(string financePlanPublicIdPhi);
    }
}

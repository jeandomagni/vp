﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Interfaces
{
    public class FinancePlanCount
    {
        public int Total { get; set; }
        public int PastDue { get; set; }
    }
}
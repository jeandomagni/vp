﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Interfaces
{
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;

    public interface IInterestRateRepository : IRepository<Entities.InterestRate>
    {
        int GetMaxDurationRangeEnd(FinancePlanOfferSetTypeEnum financialOfferSetType);
    }
}

﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Interfaces
{
    using System.Collections.Generic;
    using Entities;

    public class FinancePlanResults
    {
        public IList<FinancePlan> FinancePlans { get; set; }
        public int TotalRecords { get; set; }
        public decimal TotalCurrentBalance { get; set; }
    }
}

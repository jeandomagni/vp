﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IAmortizationService : IDomainService
    {
        IList<AmortizationMonth> GetAmortizationMonthsForFinancePlan(FinancePlan financePlan,
            DateTime firstPaymentDueDate,
            AmortizationVisitDateBalances visitDateBalances);

        IList<AmortizationMonth> GetAmortizationMonthsForOriginatedFinancePlan(FinancePlan financePlan,
            DateTime firstPaymentDueDate,
            AmortizationVisitDateBalances visitDateBalances,
            bool logException = false);

        IList<AmortizationMonth> GetAmortizationMonthsForPendingFinancePlan(FinancePlan financePlan,
            FirstInterestPeriodEnum firstInterestPeriodEnum,
            DateTime firstPaymentDueDate,
            AmortizationVisitDateBalances visitDateBalances = null);

        IList<AmortizationMonth> GetAmortizationMonthsForAmounts(decimal monthlyAmount,
            IList<IFinancePlanVisitBalance> visits,
            decimal interestRate,
            FirstInterestPeriodEnum interestFreePeriod,
            DateTime firstPaymentDueDate,
            InterestCalculationMethodEnum interestCalculationMethod,
            AmortizationVisitDateBalances visitDateBalances = null);

        int GetAmortizationDurationForAmounts(decimal monthlyAmount, IList<IFinancePlanVisitBalance> visits,
            decimal interestRate,
            FirstInterestPeriodEnum interestFreePeriod,
            DateTime firstPaymentDueDate,
            InterestCalculationMethodEnum interestCalculationMethod);

        int? CalculateNumberOfTotalCurrentDuration(FinancePlan financePlan, AmortizationVisitDateBalances visitDateBalanceses);
    }
}
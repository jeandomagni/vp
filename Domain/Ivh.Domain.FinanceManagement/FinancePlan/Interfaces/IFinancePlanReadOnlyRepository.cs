﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.FinanceManagement.FinancePlan.Interfaces
{
    using Entities;

    public interface IFinancePlanReadOnlyRepository
    {
        IList<FinancePlanAmountDueInfo> GetFinancePlanAmountDueInfoForDateWithoutAPaymentExactDate(DateTime dateToCheck, int? vpGuarantorId = null);
        IList<FinancePlanAmountDueInfo> GetFinancePlanAmountDueInfoForDateWithoutAPayment(DateTime dateToCheck,int? vpGuarantorId = null);
    }
}

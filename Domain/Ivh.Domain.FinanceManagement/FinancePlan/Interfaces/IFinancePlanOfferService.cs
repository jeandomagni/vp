﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IFinancePlanOfferService : IDomainService
    {
        FinancePlanOffer UpdateFinancePlanToFinancialAssistanceOffer(FinancePlan financePlan);
        
        IList<FinancePlanOffer> GenerateOffersForUser(FinancePlanCalculationParameters financePlanCalculationParameters,
            IList<FinancePlan> allApplicableFinancePlans,
            bool hasFinancialAssistance,
            bool saveOffer,
            FirstInterestPeriodEnum firstInterestPeriodEnum);

        IList<FinancePlanOffer> GetOffersWithCorrelationGuid(int vpGuarantorId, Guid guid);

        FinancePlanOffer GetOfferById(int financePlanOfferId);

        decimal GetFactoredPaymentAmount(
            IList<IFinancePlanVisitBalance> visits,
            int durationToCheck, 
            decimal interestRate, 
            //int interestFreePeriod, 
            bool checkMaximum, 
            FirstInterestPeriodEnum firstInterestPeriodEnum);

        IList<FinancePlanOfferSetType> GetFinancePlanOfferSetTypes();

        decimal ValidatePaymentAmount(bool validateMax, 
            IList<IFinancePlanVisitBalance> visits, 
            decimal paymentAmount, 
            decimal interestRate,
            int duration, 
            FirstInterestPeriodEnum interestFreePeriod, 
            DateTime firstPaymentDueDate,
            InterestCalculationMethodEnum interestCalculationMethod);
    }
}
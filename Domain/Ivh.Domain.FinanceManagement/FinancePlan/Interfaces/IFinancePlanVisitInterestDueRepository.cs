﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IFinancePlanVisitInterestDueRepository : IRepository<FinancePlanVisitInterestDue>
    {
        decimal GetInterestDueForStatementFinancePlan(int vpStatementId, int financePlanId);
    }
}

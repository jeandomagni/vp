﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IInterestService : IDomainService
    {
        decimal GetFactorForInterest(int maxDurationInMonths, decimal yearlyInterestRate, FirstInterestPeriodEnum interestFreePeriod);
        decimal GetMonthlyInterestAmountForBalance(decimal yearlyInterestRate, decimal amount);
        decimal GetInterestDueForPeriod(decimal yearlyInterestRate, IList<Tuple<DateTime, decimal>> dateAmounts);
        IList<InterestRate> GetInterestRates(decimal totalAmountToConsider, IList<FinancePlan> allApplicableFinancePlans, bool hasFinancialAssistance, int vpGuarantorId, int? financePlanOfferSetTypeId = null, decimal? ptpScore = null);
        int GetMaxDurationRangeEnd(FinancePlanOfferSetTypeEnum financePlanOfferSetType);
    }
}
﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Interfaces
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public interface IFinancePlanVisitBalance
    {
        int VisitId { get; }
        string SourceSystemKey { get; }
        int BillingSystemId { get; }
        decimal CurrentBalance { get; }
        decimal CurrentVisitBalance { get; }
        VisitStateEnum CurrentVisitState { get; }
        decimal PrincipalBalance { get; }
        decimal UnclearedPaymentsSum { get; }
        decimal InterestDue { get; }
        string BillingApplication { get; }
        bool HasFinancialAssistance { get; }
        decimal? OverrideInterestRate { get; }
        int OverridesRemaining { get; }
        decimal CalculateBalance(decimal currentVisitBalance, decimal unclearedPaymentsSum, decimal interestDue);
    }
}
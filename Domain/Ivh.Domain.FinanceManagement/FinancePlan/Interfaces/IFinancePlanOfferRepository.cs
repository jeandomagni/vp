﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Common.Base.Interfaces;
    using Entities;

    public interface IFinancePlanOfferRepository : IRepository<Entities.FinancePlanOffer>
    {
        FinancePlanOffer ManagedInsert(FinancePlanOffer entity);
        IList<FinancePlanOffer> GetOffersWithCorrelationGuid(int vpGuarantorId, Guid guid);
    }
}

﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Interfaces
{
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IFinancePlanIncentiveService : IDomainService
    {
        FinancePlanInterestRefundIncentiveEligibilityEnum IsFinancePlanEligibleForInterestRefundIncentive(FinancePlan financePlan);
        decimal AmountOfInterestToReallocateToPrincipalForIncentive(FinancePlan financePlan);
    }
}

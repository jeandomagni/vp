﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Visit.Entities;
    using Entities;

    public interface IInterestRateConsiderationService : IDomainService
    {
        decimal GetTotalConsiderationAmount(decimal totalAmountToConsider, IList<FinancePlan> allApplicableFinancePlans);
    }
}
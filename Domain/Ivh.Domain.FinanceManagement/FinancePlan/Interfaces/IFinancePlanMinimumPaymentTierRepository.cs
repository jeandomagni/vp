﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Interfaces
{
    using Common.Base.Interfaces;

    public interface IFinancePlanMinimumPaymentTierRepository : IRepository<Entities.FinancePlanMinimumPaymentTier>
    {
    }
}

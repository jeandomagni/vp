﻿using FluentNHibernate.Mapping;
using Ivh.Domain.FinanceManagement.FinancePlan.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.FinanceManagement.FinancePlan.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class FinancePlanVisitPrincipalAmountMap : ClassMap<FinancePlanVisitPrincipalAmount>
    {
        public FinancePlanVisitPrincipalAmountMap()
        {
            this.Schema("dbo");
            this.Table(nameof(FinancePlanVisitPrincipalAmount));
            this.Id(x => x.FinancePlanVisitPrincipalAmountId);
            this.Map(x => x.PaymentAllocationId).Nullable();
            this.Map(x => x.VisitTransactionId).Nullable();
            this.Map(x => x.Amount);
            this.Map(x => x.InsertDate);
            this.Map(x => x.OverrideInsertDate);
            this.Map(x => x.ParentFinancePlanVisitPrincipalAmountId);

            this.References(x => x.FinancePlan)
                .Column("FinancePlanId")
                .Not.LazyLoad()
                .Not.Nullable();
            this.References(x => x.FinancePlanVisit)
                .Column("FinancePlanVisitId")
                .Not.Nullable();

            this.Map(x => x.FinancePlanVisitPrincipalAmountType, "FinancePlanVisitPrincipalAmountTypeId")
                .CustomType<FinancePlanVisitPrincipalAmountTypeEnum>()
                .Not
                .Nullable();
        }
    }
}

﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class FinancePlanMap : ClassMap<FinancePlan>
    {
        public FinancePlanMap()
        {
            this.Schema("dbo");
            this.Table("FinancePlan");
            this.Id(x => x.FinancePlanId);

            this.References(x => x.VpGuarantor)
                .Column("VpGuarantorId")
                .Not.LazyLoad()
                .Not.Nullable();

            this.References(x => x.FinancePlanOffer)
                .Column("FinancePlanOfferId")
                .Not.LazyLoad()
                .Fetch.Join()
                .Nullable();

            this.References(x => x.CurrentFinancePlanStatus)
                .Column("CurrentFinancePlanStatusId")
                .Not.LazyLoad()
                .Not.Nullable();

            this.Map(x => x.CustomizedVisitPayUserId).Nullable();
            this.Map(x => x.AcceptedTermsVisitPayUserId).Nullable();
            this.Map(x => x.InsertDate);
            this.Map(x => x.OriginationDate).Nullable();
            this.Map(x => x.TermsAgreedDate).Nullable();
            this.Map(x => x.TermsCmsVersionId).Nullable();
            this.Map(x => x.OriginatedFinancePlanBalance).Not.Nullable();
            this.Map(x => x.InterestAssessed).Nullable();
            this.Map(x => x.CurrentFinancePlanBalance).Not.Nullable();
            this.Map(x => x.OriginalPaymentAmount).Nullable();
            this.Map(x => x.OriginalDuration).Nullable();
            this.Map(x => x.OfferCalculationStrategy).Not.Nullable().CustomType<OfferCalculationStrategyEnum>();
            this.Map(x => x.CurrentBucket).Nullable();
            this.Map(x => x.CurrentBucketOverride).Nullable();
            this.Map(x => x.IsCombined).Not.Nullable();
            this.Map(x => x.FirstPaymentDueDate).Nullable();
            this.Map(x => x.FinancePlanUniqueId).Nullable();
            this.Map(x => x.FinancePlanPublicIdPhi).Nullable();
            this.Map(x => x.OriginatedFinancePlanType).Not.Nullable().Column("OriginatedFinancePlanTypeId").CustomType<FinancePlanTypeEnum>();
            this.Map(x => x.InterestCalculationMethod)
                .Column($"{nameof(FinancePlan.InterestCalculationMethod)}Id")
                .Not.Nullable()
                .CustomType<InterestCalculationMethodEnum>();

            this.References(x => x.CombinedFinancePlan)
                .Column("CombinedFinancePlanId")
                .Nullable();

            this.References(x => x.OriginalFinancePlan)
                .Column("OriginalFinancePlanId")
                //.Not.LazyLoad()
                //.Fetch.Join()
                .Nullable();
            
            this.References(x => x.ReconfiguredFinancePlan)
                .Column("ReconfiguredFinancePlanId")
                //.Not.LazyLoad()
                //.Fetch.Join()
                .Nullable();

            this.References(x => x.CreatedVpStatement)
                .Column("CreatedVpStatementId")
                .Nullable();

            this.HasMany(x => x.FinancePlanVisits)
                .KeyColumn("FinancePlanId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();

            this.HasMany(x => x.FinancePlanStatusHistory)
                .KeyColumn("FinancePlanId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();

            this.HasMany(x => x.FinancePlanInterestRateHistory)
                .KeyColumn("FinancePlanId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();

            this.HasMany(x => x.FinancePlanPaymentAmountHistories)
                .KeyColumn("FinancePlanId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();

            this.HasMany(x => x.FinancePlanAmountsDue)
                .KeyColumn("FinancePlanId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();

            this.HasMany(x => x.FinancePlanBucketHistories)
                .KeyColumn("FinancePlanId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();

            this.HasMany(x => x.FinancePlanCreditAgreements)
                .KeyColumn("FinancePlanId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .ExtraLazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();
        }
    }
}

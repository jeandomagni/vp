﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class FinancePlanVisitVisitMap : ClassMap<FinancePlanVisitVisit>
    {
        public FinancePlanVisitVisitMap()
        {
            this.Schema("dbo");
            this.Table("Visit");
            this.Id(x => x.VisitId);
            this.Map(x => x.BillingApplication).Not.Nullable().ReadOnly();
            this.Map(x => x.CurrentBalance, "CurrentBalance").Not.Nullable().ReadOnly();
            this.Map(x => x.CurrentVisitState, "CurrentVisitStateId").CustomType<VisitStateEnum>().ReadOnly();
            this.Map(x => x.DischargeDate, "DischargeDate").Nullable().ReadOnly();
            this.Map(x => x.InterestZero, "InterestZero").Not.Nullable().ReadOnly();
            this.Map(x => x.SourceSystemKey).ReadOnly();
            this.Map(x => x.MatchedSourceSystemKey).ReadOnly();
            this.Map(x => x.BillingSystemId).ReadOnly();
            this.Map(x => x.AgingTier)
                .Formula("(select coalesce((SELECT vah.AgingTier FROM dbo.VisitAgingHistory vah WHERE vah.VisitAgingHistoryId = [CurrentVisitAgingHistoryId]), 0))")
                .CustomType<AgingTierEnum>()
                .LazyLoad()
                .ReadOnly();
            this.Map(x => x.VpEligible).ReadOnly();
            this.Map(x => x.BillingHold).ReadOnly();
            this.Map(x => x.Redact).ReadOnly();
            this.Map(x => x.IsUnmatched).ReadOnly();
            this.Map(x => x.FacilityId).ReadOnly();
        }
    }
}
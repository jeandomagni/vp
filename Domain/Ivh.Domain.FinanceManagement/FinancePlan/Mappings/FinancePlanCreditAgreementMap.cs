﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class FinancePlanCreditAgreementMap : ClassMap<FinancePlanCreditAgreement>
    {
        public FinancePlanCreditAgreementMap()
        {
            this.Schema("dbo");
            this.Table("FinancePlanCreditAgreement");
            this.Id(x => x.FinancePlanCreditAgreementId);
            this.Map(x => x.AgreementText).Not.Nullable().Length(10000);
            this.Map(x => x.CmsVersionId);
            this.Map(x => x.InsertDate);
            this.Map(x => x.ContractNumber).Nullable().Length(50);
            this.Map(x => x.Locale).Not.Nullable().Length(25);
            this.Map(x => x.IsUserLocale).Not.Nullable();
            this.Map(x => x.IsRetailInstallmentContract).Not.Nullable();
            this.References(x => x.FinancePlan, "FinancePlanId");
        }
    }
}
﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class FinancePlanVisitMap : ClassMap<FinancePlanVisit>
    {
        public FinancePlanVisitMap()
        {
            this.Schema("dbo");
            this.Table("FinancePlanVisit");
            this.Id(x => x.FinancePlanVisitId);
            this.References(x => x.FinancePlan).Column(nameof(FinancePlan.FinancePlanId)).Not.Nullable();
            this.Map(x => x.VisitId).Not.Nullable();
            this.Map(x => x.VpGuarantorId).Not.Nullable();
            this.Map(x => x.OriginatedSnapshotVisitBalance).Nullable();
            this.Map(x => x.OriginatedSnapshotInterestDeferred).Nullable();
            this.Map(x => x.RemovedSnapshotVisitBalance).Nullable();
            this.Map(x => x.InsertDate);
            this.Map(x => x.HardRemoveDate).Nullable();

            this.HasMany(x => x.FinancePlanVisitPrincipalAmounts)
                .KeyColumn(nameof(FinancePlanVisit.FinancePlanVisitId))
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();

            this.HasMany(x => x.FinancePlanVisitInterestDues)
                .KeyColumn(nameof(FinancePlanVisit.FinancePlanVisitId))
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();

            this.References(x => x.FinancePlanVisitVisit)
                .Column(nameof(FinancePlanVisit.VisitId))
                .Not.Nullable()
                .Not.Insert()
                .Not.Update()
                //.Not.LazyLoad()
                .Cascade.None()
                .ReadOnly();

            this.HasMany(x => x.FinancePlanVisitInterestRateHistories)
                .KeyColumn(nameof(FinancePlanVisit.FinancePlanVisitId))
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();

            this.Map(m => m.UnclearedPaymentsSum)
                .Formula("(SELECT SUM(pa.ActualAmount * -1) FROM dbo.PaymentAllocation pa WHERE pa.VisitId = [VisitId] AND pa.PaymentAllocationTypeId NOT IN (2) AND pa.PaymentBatchStatusId = 1)");
        }
    }
}
﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class FinancePlanOfferSetTypeMap : ClassMap<FinancePlanOfferSetType>
    {
        public FinancePlanOfferSetTypeMap()
        {
            this.Schema("dbo");
            this.Table("FinancePlanOfferSetType");
            this.ReadOnly();
            this.Id(x => x.FinancePlanOfferSetTypeId);
            this.Map(x => x.Name).Not.Nullable();
            this.Map(x => x.Description);
            this.Map(x => x.IsActive).Not.Nullable();
            this.Map(x => x.IsPatient).Not.Nullable();
            this.Map(x => x.IsClient).Not.Nullable();
            this.Map(x => x.IsDefault).Not.Nullable();
            this.Map(x => x.Priority).Nullable();

            this.Cache.ReadOnly();
        }
    }
}
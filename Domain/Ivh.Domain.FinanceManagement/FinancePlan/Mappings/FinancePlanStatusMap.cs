﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class FinancePlanStatusMap : ClassMap<FinancePlanStatus>
    {
        public FinancePlanStatusMap()
        {
            this.Schema("dbo");
            this.Table("FinancePlanStatus");
            this.ReadOnly();
            this.Id(x => x.FinancePlanStatusId);
            this.Map(x => x.FinancePlanStatusDisplayName).Not.Nullable();
            this.Map(x => x.FinancePlanStatusGroupName).Not.Nullable();
            this.Map(x => x.FinancePlanStatusName).Not.Nullable();

            this.Cache.ReadOnly();
        }
    }
}
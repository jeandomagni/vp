﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class FinancePlanVisitInterestDueMap : ClassMap<FinancePlanVisitInterestDue>
    {
        public FinancePlanVisitInterestDueMap()
        {
            this.Schema("dbo");
            this.Table("FinancePlanVisitInterestDue");
            this.Id(x => x.FinancePlanVisitInterestDueId);

            this.Map(x => x.InsertDate);

            this.References(x => x.FinancePlan)
                .Column("FinancePlanId")
                .Not.LazyLoad()
                .Not.Nullable();

            this.Map(x => x.InterestDue)
                .Not.Nullable();

            this.Map(x => x.VpStatementId).Nullable();
            this.Map(x => x.PaymentAllocationId).Nullable();
            this.Map(x => x.CurrentBalanceAtTimeOfAssessment).Nullable();
            this.Map(x => x.InterestRateUsedToAssess).Nullable();
            this.Map(x => x.OverrideInsertDate).Nullable();
            this.Map(x => x.FinancePlanVisitInterestDueType, "FinancePlanVisitInterestDueTypeId").CustomType<FinancePlanVisitInterestDueTypeEnum>().Not.Nullable();

            this.References(x => x.ParentFinancePlanVisitInterestDue).Column("ParentFinancePlanVisitInterestDueId").Nullable();
            this.References(x => x.FinancePlanVisit).Column("FinancePlanVisitId").Nullable();
        }
    }
}
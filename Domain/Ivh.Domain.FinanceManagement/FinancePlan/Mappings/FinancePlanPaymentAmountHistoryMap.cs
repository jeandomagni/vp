namespace Ivh.Domain.FinanceManagement.FinancePlan.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class FinancePlanPaymentAmountHistoryMap : ClassMap<FinancePlanPaymentAmountHistory>
    {
        public FinancePlanPaymentAmountHistoryMap()
        {
            this.Schema("dbo");
            this.Table("FinancePlanPaymentAmountHistory");

            this.Id(x => x.FinancePlanPaymentAmountHistoryId);
            this.Map(x => x.InsertDate);
            this.Map(x => x.ChangeDescription).Length(254);
            this.Map(x => x.PaymentAmount);
            this.References(x => x.FinancePlan)
                .Column("FinancePlanId")
                .Not.Nullable();
        }
    }
}
﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class FinancePlanBucketHistoryMap : ClassMap<FinancePlanBucketHistory>
    {
        public FinancePlanBucketHistoryMap()
        {
            this.Schema("dbo");
            this.Table("FinancePlanBucketHistory");
            this.Id(x => x.FinancePlanBucketHistoryId);
            this.Map(x => x.InsertDate);
            this.Map(x => x.Bucket).Not.Nullable();
            this.Map(x => x.ChangeDescription).Nullable();
            this.References(x => x.FinancePlan).Column("FinancePlanId").Not.Nullable();
            this.References(x => x.VisitPayUser).Column("VisitPayUserId").Nullable();
        }
    }
}
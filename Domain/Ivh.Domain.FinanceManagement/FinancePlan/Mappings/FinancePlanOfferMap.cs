﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Mappings
{
    using FluentNHibernate.Mapping;

    public class FinancePlanOfferMap : ClassMap<Entities.FinancePlanOffer>
	{
        public FinancePlanOfferMap()
        {
            this.Schema("dbo");
            this.Table("FinancePlanOffer");
            this.Id(x => x.FinancePlanOfferId);
            this.References(x => x.VpGuarantor).Column("VpGuarantorId").Not.Nullable();
            this.References(x => x.VpStatement).Column("VpStatementId").Nullable();
            this.Map(x => x.StatementedCurrentAccountBalance).Not.Nullable();
            this.Map(x => x.DurationRangeStart).Not.Nullable();
            this.Map(x => x.DurationRangeEnd).Not.Nullable();
            this.Map(x => x.MinPayment).Not.Nullable();
            this.Map(x => x.MaxPayment).Not.Nullable();
            this.Map(x => x.InterestRate).Not.Nullable();
            this.Map(x => x.IsActive).Not.Nullable();
            this.Map(x => x.InsertDate);
            this.Map(x => x.CorrelationGuid).Not.Nullable();
            this.Map(x => x.IsCombined).Nullable();
            this.References(x => x.FinancePlanOfferSetType).Column("FinancePlanOfferSetTypeId").Not.Nullable();
        }
	}
}

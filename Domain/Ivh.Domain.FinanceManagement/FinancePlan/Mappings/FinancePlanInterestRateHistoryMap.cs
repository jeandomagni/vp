namespace Ivh.Domain.FinanceManagement.FinancePlan.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class FinancePlanInterestRateHistoryMap : ClassMap<FinancePlanInterestRateHistory>
    {
        public FinancePlanInterestRateHistoryMap()
        {
            this.Schema("dbo");
            this.Table("FinancePlanInterestRateHistory");

            this.Id(x => x.FinancePlanInterestRateHistoryId);
            this.Map(x => x.InsertDate);
            this.Map(x => x.ChangeDescription).Length(254);
            this.Map(x => x.InterestRate);
            this.References(x => x.FinancePlan)
                .Column("FinancePlanId")
                .Not.Nullable();
        }
    }
}
﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Mappings
{
    using FluentNHibernate.Mapping;

    public class InterestRateMap : ClassMap<Entities.InterestRate>
	{
        public InterestRateMap()
        {
            this.Schema("dbo");
            this.Table("InterestRate");
            this.Id(x => x.InterestRateId).GeneratedBy.Assigned();
            this.Map(x => x.DurationRangeStart);
            this.Map(x => x.DurationRangeEnd);
            this.Map(x => x.Rate).Column("InterestRate").CustomType<decimal>(); ;
            this.Map(x => x.DisplayText);
            this.Map(x => x.MinimumBalance);
            this.Map(x => x.MaximumBalance);
            this.Map(x => x.IsCharity);
            this.Map(x => x.MinimumPtpScore);
            this.Map(x => x.MaximumPtpScore);
            this.Map(x => x.RandomizedTestGroupId).Nullable().ReadOnly();
            this.References(x => x.FinancePlanOfferSetType).Column("FinancePlanOfferSetTypeId").Not.Nullable();
            this.Cache.ReadOnly();
        }
	}
}

﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class FinancePlanAmountDueMap : ClassMap<FinancePlanAmountDue>
    {
        public FinancePlanAmountDueMap()
        {
            this.Schema("dbo");
            this.Table("FinancePlanAmountDue");
            this.Id(x => x.FinancePlanAmountDueId);

            this.Map(x => x.InsertDate);

            this.References(x => x.FinancePlan)
                .Column("FinancePlanId")
                .Not.LazyLoad()
                .Not.Nullable();

            this.Map(x => x.AmountDue)
                .Not.Nullable();

            this.Map(x => x.DueDate).Nullable();
            this.Map(x => x.OverrideDueDate).Nullable();
            //TODO- remove PaymentId- being replaced by paymentallocation
            this.Map(x => x.PaymentId).Nullable();
            this.Map(x => x.VpStatementId).Nullable();
            this.Map(x => x.PaymentAllocationId).Nullable();

            this.References(x => x.FinancePlanVisit).Column("FinancePlanVisitId").Nullable();
            
            this.Map(x => x.PaymentAllocationType)
                .Formula("(SELECT pa.PaymentAllocationTypeId FROM dbo.PaymentAllocation pa WHERE pa.PaymentAllocationId = PaymentAllocationId)")
                .CustomType<PaymentAllocationTypeEnum>()
                .LazyLoad()
                .ReadOnly();

            this.Map(x => x.PaymentAllocationActualAmount)
                .Formula("(SELECT pa.ActualAmount FROM dbo.PaymentAllocation pa WHERE pa.PaymentAllocationId = PaymentAllocationId)")
                .LazyLoad()
                .ReadOnly();

            this.Map(x => x.PaymentType)
                .Formula("(SELECT p.PaymentTypeId FROM dbo.Payment p WHERE p.PaymentId = PaymentId)")
                .CustomType<PaymentTypeEnum>()
                .LazyLoad()
                .ReadOnly();

        }
    }
}
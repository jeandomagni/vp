namespace Ivh.Domain.FinanceManagement.FinancePlan.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class FinancePlanStatusHistoryMap : ClassMap<FinancePlanStatusHistory>
    {
        public FinancePlanStatusHistoryMap()
        {
            this.Schema("dbo");
            this.Table("FinancePlanStatusHistory");

            this.Id(x => x.FinancePlanStatusHistoryId);
            this.Map(x => x.InsertDate);
            this.Map(x => x.ChangeDescription).Length(254);
            this.References(x => x.FinancePlanStatus)
                .Column("FinancePlanStatusId")
                .Not.Nullable();
            this.References(x => x.FinancePlan)
                .Column("FinancePlanId")
                .Not.Nullable();
        }
    }
}
﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class FinancePlanVisitInterestRateHistoryMap : ClassMap<FinancePlanVisitInterestRateHistory>
    {
        public FinancePlanVisitInterestRateHistoryMap()
        {
            this.Schema("dbo");
            this.Table("FinancePlanVisitInterestRateHistory");
            this.Id(x => x.FinancePlanVisitInterestRateHistoryId);
            this.References(x => x.FinancePlanVisit).Column(nameof(FinancePlanVisit.FinancePlanVisitId)).Not.Nullable();
            this.Map(x => x.InterestRate).Not.Nullable();
            this.Map(x => x.InsertDate);
            this.Map(x => x.OverrideInterestRate).Nullable();
            this.Map(x => x.OverridesRemaining).Not.Nullable();
        }
    }
}
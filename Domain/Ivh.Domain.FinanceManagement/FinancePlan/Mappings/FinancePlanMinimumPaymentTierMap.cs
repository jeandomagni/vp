﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Mappings
{
    using FluentNHibernate.Mapping;

    public class FinancePlanMinimumPaymentTierMap : ClassMap<Entities.FinancePlanMinimumPaymentTier>
    {
        public FinancePlanMinimumPaymentTierMap()
        {
            this.Schema("dbo");
            this.ReadOnly();
            this.Table("FinancePlanMinimumPaymentTier");
            this.Id(x => x.FinancePlanMinimumPaymentTierId).GeneratedBy.Assigned();
            this.Map(x => x.ExisitingRecurringPaymentTotal);
            this.Map(x => x.MinimumPayment);
            this.Map(x => x.RandomizedTestGroupId).Nullable().ReadOnly();
            this.References(x => x.FinancePlanOfferSetType).Column("FinancePlanOfferSetTypeId").Not.Nullable();

            this.Cache.ReadOnly();
        }
    }
}

﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class FinancePlanTypeMap:  ClassMap<FinancePlanType>
    {
        public FinancePlanTypeMap()
        {
            this.Schema(Schemas.VisitPay.Dbo);
            this.Table(nameof(FinancePlanType));
            this.ReadOnly();
            this.Id(x => x.FinancePlanTypeId);
            this.Map(x => x.FinancePlanTypeName);
            this.Map(x => x.FinancePlanTypeDisplayName);
            this.Map(x => x.IsAutoPay);
            this.Map(x => x.IsPaymentMethodEditable);
            this.Map(x => x.RequireCreditAgreement);
            this.Map(x => x.RequireEmailCommunication);
            this.Map(x => x.RequireEsign);
            this.Map(x => x.RequirePaperCommunication);
            this.Map(x => x.RequirePaymentMethod);
        }
    }
}

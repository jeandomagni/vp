﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
    public class FinancePlanOfferSetType
    {
        public virtual int FinancePlanOfferSetTypeId { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual bool IsDefault { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual bool IsPatient { get; set; }
        public virtual bool IsClient { get; set; }
        public virtual bool IsClientOnly => this.IsClient && !this.IsPatient;
        public virtual int Priority { get; set; }
    }
}
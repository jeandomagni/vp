﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
    using System;

    public class AmortizationMonth
    {
        public int MonthNumber { get; set; }
        public DateTime MonthDate { get; set; }

        public FinancePlan FinancePlan { get; set; }

        public decimal InterestAmount { get; set; }
        public decimal PrincipalAmount { get; set; }

        public decimal Total
        {
            get { return this.InterestAmount + this.PrincipalAmount; }
        }

        public override string ToString()
        {
            return $@"Month:{MonthNumber}
                MonthDate:{MonthDate.ToShortDateString()}
                PrincipalAmount:{PrincipalAmount:0.00}
                InterestAmount:{InterestAmount:0.00}
                Total:{Total:0.00}";
        }
    }
}
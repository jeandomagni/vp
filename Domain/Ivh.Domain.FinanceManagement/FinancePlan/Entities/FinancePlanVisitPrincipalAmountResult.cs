﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
    using System;

    public class FinancePlanVisitPrincipalAmountResult
    {
        public int VisitId { get; set; }
        public DateTime InsertDate { get; set; }
        public decimal Amount { get;set; }
    }
}
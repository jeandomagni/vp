﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
    using System;
    using User.Entities;

    public class FinancePlanBucketHistory
    {
        public FinancePlanBucketHistory()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int FinancePlanBucketHistoryId { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual int Bucket { get; set; }
        public virtual string ChangeDescription { get; set; }
        public virtual FinancePlan FinancePlan { get; set; }
        public virtual VisitPayUser VisitPayUser { get; set; }
    }
}
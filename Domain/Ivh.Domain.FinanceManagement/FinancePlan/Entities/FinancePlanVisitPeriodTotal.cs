namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
    public class FinancePlanVisitPeriodTotal
    {
        public int VisitId { get; set; }
        public int FinancePlanId { get; set; }
        public decimal TotalTransactionAmount { get; set; }
    }
}
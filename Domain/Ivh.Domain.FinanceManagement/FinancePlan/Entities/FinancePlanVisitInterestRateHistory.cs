﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
    using System;

    public class FinancePlanVisitInterestRateHistory
    {
        public FinancePlanVisitInterestRateHistory()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        public virtual int FinancePlanVisitInterestRateHistoryId { get; set; }

        public virtual DateTime InsertDate { get; set; }

        public virtual decimal InterestRate { get; set; }

        public virtual decimal? OverrideInterestRate { get; set; }

        public virtual int OverridesRemaining { get; set; }

        public virtual FinancePlanVisit FinancePlanVisit { get; set; }
    }
}
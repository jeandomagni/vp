﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
    using Ivh.Application.Core.Common.Interfaces;
    using System;
    using System.Text.RegularExpressions;

    public class FinancePlanPublicIdPhiValueGenerator : IIdentifierValueSource<long>
    {
        public const string ValidationPatternRegularExpression = "^[1-9]{1}[0-9]{4}-?[0-9]{5}$";
        public const string DisplayFormat = "#####-#####";

        private const int SegmentRangeMax = 99999;
        private const int FirstSegmentRangeMin = 10000;
        private static readonly Random _randomInstance = new Random();

        public Regex ValidationPattern { get; } = new Regex(ValidationPatternRegularExpression);

        public long GenerateIdentifier()
        {
            int segment1 = _randomInstance.Next(FirstSegmentRangeMin, SegmentRangeMax);
            int segment2 = _randomInstance.Next(0, SegmentRangeMax);

            // segment2 could end up with a value equivalent to something 
            // less than the required 5 digits in length,
            // so specify the string format that pads the value to ensure correct length
            // (ex., "99" -> "00099")
            const int minSegmentLength = 5;
            string segmentStringFormat = $"D{minSegmentLength}";
            string concatenated = $"{segment1}{segment2.ToString(segmentStringFormat)}";

            long value = long.Parse(concatenated);
            return value;
        }
    }
}
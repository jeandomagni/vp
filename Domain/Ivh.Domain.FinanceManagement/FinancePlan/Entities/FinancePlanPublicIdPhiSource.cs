﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
    using Ivh.Application.Core.Common.Interfaces;
    using Ivh.Application.Core.Common.Models.Identifiers;
    using Ivh.Common.Base.Utilities.Extensions;
    using System.Text.RegularExpressions;

    public class FinancePlanPublicIdPhiSource : VisitPayPublicIdentifier<long>
    {
        public override long RawValue { get; }
        public override IIdentifierValueSource<long> ValueGenerator { get; }

        public FinancePlanPublicIdPhiSource(
            IIdentifierValueSource<long> valueGenerator)
        {
            this.ValueGenerator = valueGenerator ?? new FinancePlanPublicIdPhiValueGenerator();
            this.RawValue = this.ValueGenerator.GenerateIdentifier();
        }

        public static bool IsValidFormat(string value)
        {
            if (value.IsNullOrEmpty())
            {
                return false;
            }

            Regex validFormat = new FinancePlanPublicIdPhiValueGenerator().ValidationPattern;
            return validFormat.IsMatch(value);
        }
    }
}
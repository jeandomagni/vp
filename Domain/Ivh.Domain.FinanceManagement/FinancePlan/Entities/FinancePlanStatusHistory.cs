namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
    using System;

    public class FinancePlanStatusHistory
    {
        public FinancePlanStatusHistory()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        public virtual int FinancePlanStatusHistoryId { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual FinancePlanStatus FinancePlanStatus { get; set; }
        public virtual string ChangeDescription { get; set; }
        public virtual FinancePlan FinancePlan { get; set; }
    }
}
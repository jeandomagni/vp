﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
    using System;

    public class FinancePlanCreditAgreement
    {
        public FinancePlanCreditAgreement()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        public virtual int FinancePlanCreditAgreementId { get; set; }
        
        public virtual FinancePlan FinancePlan { get; set; }

        public virtual DateTime InsertDate { get; set; }
        
        public virtual string ContractNumber { get; set; }
        
        public virtual int CmsVersionId { get; set; }
        
        public virtual string AgreementText { get; set; }

        public virtual string Locale { get; set; }

        public virtual bool IsUserLocale { get; set; }

        public virtual bool IsRetailInstallmentContract { get; set; }
    }
}
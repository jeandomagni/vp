namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
    using System;

    public class FinancePlanPaymentAmountHistory
    {
        public FinancePlanPaymentAmountHistory()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int FinancePlanPaymentAmountHistoryId { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual decimal PaymentAmount { get; set; }
        public virtual string ChangeDescription { get; set; }
        public virtual FinancePlan FinancePlan { get; set; }
    }
}
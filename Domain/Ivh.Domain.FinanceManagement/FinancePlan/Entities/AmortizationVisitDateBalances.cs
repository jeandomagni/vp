﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
    using System;
    using System.Collections.Generic;
    using Interfaces;

    public class AmortizationVisitDateBalances : Dictionary<IFinancePlanVisitBalance, IList<Tuple<DateTime,decimal>>>
    {
    }
}

﻿using System.Collections.Generic;
using System.Linq;

namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class FinancePlanAmountDueReversalGenerator
    {
        private readonly List<FinancePlanAmountDueReversal> _allFinancePlanAmountDueReversals;
        private readonly List<FinancePlanAmountDueReversal> _amountsDueToBeAdded;

        /// <summary>
        /// allFinancePlanAmountDueReversals is based off a left join between payment allocations (payments and reversals) and FinancePlanAmountDue
        /// </summary>
        public FinancePlanAmountDueReversalGenerator(List<FinancePlanAmountDueReversal> allFinancePlanAmountDueReversals)
        {
            this._allFinancePlanAmountDueReversals = allFinancePlanAmountDueReversals;
            this._amountsDueToBeAdded = this._allFinancePlanAmountDueReversals.Where(x => x.PaymentAllocationId == null).ToList();
        }

        public List<FinancePlanAmountDueReversal> GenerateFinancePlanAmountDueForReversals()
        {
            // Surplus is any amount that was left over after relieving the amount due. 
            // This is the amount that will be reversed first. When the surplus is exhausted the remainder is added back to the amount due
            decimal sumOfSurplusOfPayments = this._allFinancePlanAmountDueReversals.Where(x => x.PaymentAllocationId != null).Sum(x => x.ExcessPayment);
            foreach (FinancePlanAmountDueReversal reversalOfAmountDue in this._amountsDueToBeAdded)
            {
                // it is assumed the reversal will be negative
                if (reversalOfAmountDue.ActualPaymentAmount > 0)
                {
                    continue;
                }

                decimal amountToBeAppliedToBackToAmountDue = sumOfSurplusOfPayments + reversalOfAmountDue.ActualPaymentAmount;
                if (amountToBeAppliedToBackToAmountDue < 0)
                {
                    // Surplus payments have been exhausted and we need to add it back to the amount due
                    amountToBeAppliedToBackToAmountDue = decimal.Negate(amountToBeAppliedToBackToAmountDue);
                    reversalOfAmountDue.AmountDue = amountToBeAppliedToBackToAmountDue;
                }
                else
                {
                    reversalOfAmountDue.AmountDue = 0;
                }

                sumOfSurplusOfPayments = reversalOfAmountDue.SurplusGreaterThanZero(sumOfSurplusOfPayments, reversalOfAmountDue.ActualPaymentAmount);

            }
            return this._amountsDueToBeAdded;
        }

    }

    public class FinancePlanAmountDueReversal
    {
        public decimal AmountDue { get; set; }
        public decimal ActualPaymentAmount { get; set; }
        public decimal ExcessPayment => this.ActualPaymentAmount + this.AmountDue;
        public int FinancePlanVisitId { get; set; }
        public int? PaymentAllocationId { get; set; }
        public int? PaymentId { get; set; }
        public int ReversalPaymentAllocationId { get; set; }
        public PaymentAllocationTypeEnum PaymentAllocationType { get; set; }
        public decimal SurplusGreaterThanZero(decimal sumPaid, decimal reversalAmount)
        {
            decimal amount = sumPaid + reversalAmount;
            return (amount <= 0m) ? 0m : amount;
        }
    }
}

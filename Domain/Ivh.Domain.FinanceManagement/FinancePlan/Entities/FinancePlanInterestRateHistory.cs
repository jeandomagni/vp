namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
    using System;

    public class FinancePlanInterestRateHistory
    {
        public FinancePlanInterestRateHistory()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int FinancePlanInterestRateHistoryId { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual decimal InterestRate { get; set; }
        public virtual string ChangeDescription { get; set; }
        public virtual FinancePlan FinancePlan { get; set; }
    }
}
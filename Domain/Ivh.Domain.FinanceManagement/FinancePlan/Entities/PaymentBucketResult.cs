namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
    public class PaymentBucketResult
    {
        public virtual int PaymentId { get; set; }
        public virtual decimal ScheduledAmount { get; set; }
        public virtual int PaymentBucket { get; set; }
    }
}
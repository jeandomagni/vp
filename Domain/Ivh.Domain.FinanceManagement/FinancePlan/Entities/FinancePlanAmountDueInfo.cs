﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
    using System;

    public class FinancePlanAmountDueInfo
    {
        public int FinancePlanId { get; set; }
        public int VpGuarantorId { get; set; }
        public decimal TotalAmountDue { get; set; }
        public DateTime AsOfDate { get; set; }
        public int? StatementFinancePlanAmountDueId { get; set; }
        public DateTime? StatementFinancePlanAmountDueDueDate { get; set; }
        public int? StatementId { get; set; }
        public int? PaymentFinancePlanAmountDueId { get; set; }
        public DateTime? PaymentFinancePlanAmountDueInsertDate { get; set; }
        public bool HasStatement => this.StatementFinancePlanAmountDueId != null;

        public bool HasPaymentAttemptForStatement =>
            this.StatementFinancePlanAmountDueId != null
            && this.PaymentFinancePlanAmountDueId != null
            && this.PaymentFinancePlanAmountDueId > this.StatementFinancePlanAmountDueId
            && this.PaymentFinancePlanAmountDueInsertDate >= this.StatementFinancePlanAmountDueDueDate;
    }
}
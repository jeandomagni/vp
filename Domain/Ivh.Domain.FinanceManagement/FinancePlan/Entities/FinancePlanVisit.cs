﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.VisitPay.Enums;
    using Interfaces;

    public class FinancePlanVisit : FinancePlanVisitBalanceBase, IFinancePlanVisitBalance
    {
        public FinancePlanVisit()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        private IList<FinancePlanVisitInterestDue> _financePlanInterestDues;
        private IList<FinancePlanVisitInterestRateHistory> _financePlanVisitInterestRateHistories;
        private IList<FinancePlanVisitPrincipalAmount> _financePlanVisitPrincipalAmounts;

        #region finance plan visit

        public virtual int FinancePlanVisitId { get; set; }

        public virtual int VpGuarantorId { get; set; }

        public virtual FinancePlan FinancePlan { get; set; }

        public virtual decimal? OriginatedSnapshotVisitBalance { get; set; }

        public virtual bool? OriginatedSnapshotInterestDeferred { get; set; }

        public virtual decimal? RemovedSnapshotVisitBalance { get; set; }

        public virtual DateTime InsertDate { get; set; }

        /// <summary>
        ///     This cannot be reversed
        /// </summary>
        public virtual DateTime? HardRemoveDate { get; set; }

        public virtual IList<FinancePlanAmountDue> FinancePlanAmountDues
        {
            get { return this.FinancePlan.FinancePlanAmountsDue.Where(x => x.FinancePlanVisit == this).ToList(); }
            set { }
        }

        public virtual IList<FinancePlanVisitInterestDue> FinancePlanVisitInterestDues
        {
            get => this._financePlanInterestDues ?? (this.FinancePlanVisitInterestDues = new List<FinancePlanVisitInterestDue>());
            set => this._financePlanInterestDues = value;
        }

        public virtual IList<FinancePlanVisitInterestRateHistory> FinancePlanVisitInterestRateHistories
        {
            get => this._financePlanVisitInterestRateHistories ?? (this.FinancePlanVisitInterestRateHistories = new List<FinancePlanVisitInterestRateHistory>());
            set => this._financePlanVisitInterestRateHistories = value;
        }
        public virtual IList<FinancePlanVisitPrincipalAmount> FinancePlanVisitPrincipalAmounts
        {
            get => this._financePlanVisitPrincipalAmounts ?? (this.FinancePlanVisitPrincipalAmounts = new List<FinancePlanVisitPrincipalAmount>());
            set => this._financePlanVisitPrincipalAmounts = value;
        }

        #endregion

        #region financial calculations

        public virtual decimal InterestAssessed => this.InterestAssessedForDateRange(null, DateTime.MaxValue);

        /// <summary>
        /// This only returns the values that added to interest in a date range, used to determine if interest was assessed for a period
        /// </summary>
        public virtual decimal PositiveInterestAssessedForDateRange(DateTime? startDate, DateTime endDate)
        {
            List<FinancePlanVisitInterestDue> amounts = this.FinancePlanVisitInterestDues
                .Where(x => x.FinancePlanVisitInterestDueType == FinancePlanVisitInterestDueTypeEnum.InterestAssessment)
                .Where(x => x.InterestDue > 0)
                .Where(x => (x.OverrideInsertDate ?? x.InsertDate) >= startDate.GetValueOrDefault(DateTime.MinValue))
                .Where(x => (x.OverrideInsertDate ?? x.InsertDate) <= endDate)
                .ToList();
            decimal value = amounts.Sum(x => x.InterestDue);

            return value;
        }

        public virtual decimal InterestAssessedForDateRange(DateTime? startDate, DateTime endDate)
        {
            IEnumerable<FinancePlanVisitInterestDue> interestsDues = this.FinancePlanVisitInterestDues
                .Where(x => x.FinancePlanVisitInterestDueType == FinancePlanVisitInterestDueTypeEnum.InterestAssessment)
                .Where(x => (x.OverrideInsertDate ?? x.InsertDate) >= startDate.GetValueOrDefault(DateTime.MinValue))
                .Where(x => (x.OverrideInsertDate ?? x.InsertDate) <= endDate);

            List<FinancePlanVisitInterestDue> amounts = interestsDues.ToList();
            decimal value = amounts.Sum(x => x.InterestDue);

            return value;
        }

        public override decimal InterestDue => this.InterestDueForDateRange(null, DateTime.MaxValue);

        public virtual decimal InterestDueForDateRange(DateTime? startDate, DateTime endDate)
        {
            List<FinancePlanVisitInterestDue> amounts = this.FinancePlanVisitInterestDues
                .Where(x => (x.OverrideInsertDate ?? x.InsertDate) >= startDate.GetValueOrDefault(DateTime.MinValue))
                .Where(x => (x.OverrideInsertDate ?? x.InsertDate) <= endDate)
                .ToList();

            decimal value = amounts.Sum(x => x.InterestDue);

            return value;
        }

        public virtual decimal InterestPaid => this.InterestPaidForDateRange(null, DateTime.MaxValue);

        public virtual decimal InterestPaidForDateRange(DateTime? startingDate, DateTime endingDate)
        {
            return this.AmountPaidSumForDateRange(PaymentAllocationTypeEnum.VisitInterest, startingDate, endingDate);
        }
        
        public virtual decimal PrincipalPaid => this.PrincipalPaidForDateRange(null, DateTime.MaxValue);

        public virtual decimal PrincipalPaidForDateRange(DateTime? startingDate, DateTime endingDate)
        {
            IEnumerable<FinancePlanVisitPrincipalAmount> amounts = this.FinancePlanVisitPrincipalAmounts
                .Where(x => (x.OverrideInsertDate ?? x.InsertDate) >= startingDate.GetValueOrDefault(DateTime.MinValue))
                .Where(x => (x.OverrideInsertDate ?? x.InsertDate) <= endingDate)
                .Where(x => x.FinancePlanVisitPrincipalAmountType == FinancePlanVisitPrincipalAmountTypeEnum.Payment);

            decimal amount = amounts.Sum(x => x.Amount);

            return amount;            
        }

        private decimal AmountDueSumForDateRange(DateTime? startingDate, DateTime endingDate)
        {
            List<FinancePlanAmountDue> amounts = this.FinancePlanAmountDues
                .Where(x => x.InsertDate >= startingDate.GetValueOrDefault(DateTime.MinValue))
                .Where(x => x.InsertDate <= endingDate)
                .ToList();

            decimal value = amounts.Sum(x => x.AmountDue);

            return value;
        }

        private decimal AmountPaidSumForDateRange(PaymentAllocationTypeEnum paymentAllocationType, DateTime? startingDate, DateTime endingDate)
        {
            List<FinancePlanAmountDue> amounts = this.FinancePlanAmountDues
                .Where(x => x.PaymentAllocationType != null)
                .Where(x => x.PaymentAllocationType == paymentAllocationType)
                .GroupBy(x => x.PaymentAllocationId)
                .Select(x => x.First())
                .Where(x => x.InsertDate >= startingDate.GetValueOrDefault(DateTime.MinValue))
                .Where(x => x.InsertDate <= endingDate)
                .ToList();

            decimal value = amounts.Sum(x => decimal.Negate(x.PaymentAllocationActualAmount));

            return value;
        }

        #endregion

        #region interest

        private FinancePlanVisitInterestRateHistory CurrentInterestRateHistory()
        {
            FinancePlanVisitInterestRateHistory currentHistory = this.FinancePlanVisitInterestRateHistories
                .OrderByDescending(x => x.InsertDate)
                .ThenByDescending(x => x.FinancePlanVisitInterestRateHistoryId)
                .FirstOrDefault();

            return currentHistory;
        }

        private void AddPostAssessmentFinancePlanVisitInterestRateHistory()
        {
            FinancePlanVisitInterestRateHistory currentHistory = this.CurrentInterestRateHistory();
            if (currentHistory == null)
            {
                return;
            }

            int overridesRemaining = Math.Max(currentHistory.OverridesRemaining - 1, 0);
            if (overridesRemaining != currentHistory.OverridesRemaining)
            {
                FinancePlanVisitInterestRateHistory newHistory = new FinancePlanVisitInterestRateHistory
                {
                    FinancePlanVisit = this,
                    InsertDate = DateTime.UtcNow,
                    InterestRate = currentHistory.InterestRate,
                    OverrideInterestRate = currentHistory.OverrideInterestRate,
                    OverridesRemaining = overridesRemaining
                };
                this.FinancePlanVisitInterestRateHistories.Add(newHistory);
            }
        }

        public virtual void SetInterestRate(decimal interestRate)
        {
            FinancePlanVisitInterestRateHistory newHistory = new FinancePlanVisitInterestRateHistory
            {
                FinancePlanVisit = this,
                InsertDate = DateTime.UtcNow,
                InterestRate = interestRate,
                OverrideInterestRate = null,
                OverridesRemaining = 0
            };
            this.FinancePlanVisitInterestRateHistories.Add(newHistory);
        }

        public virtual decimal CurrentInterestRate()
        {
            FinancePlanVisitInterestRateHistory currentHistory = this.CurrentInterestRateHistory();
            if (currentHistory == null)
            {
                return 0m;
            }

            if (currentHistory.OverridesRemaining > 0)
            {
                return currentHistory.OverrideInterestRate.GetValueOrDefault(currentHistory.InterestRate);
            }

            return currentHistory.InterestRate;
        }

        public virtual bool HasOverride()
        {
            FinancePlanVisitInterestRateHistory currentHistory = this.CurrentInterestRateHistory();
            if (currentHistory == null)
            {
                return false;
            }

            return currentHistory.OverridesRemaining > 0;
        }

        public virtual bool WriteOffInterestOnlyBalance()
        {
            if (this.InterestDue <= 0 ||
                this.InterestDue < this.MaxCurrentBalance)
            {
                return false;
            }

            return this.WriteOffInterestDue();
        }

        /// <summary>
        /// When newFinancePlanVisit is passed in, try to find the moved interest and link to InterestDue that was moved onto new FP
        /// </summary>
        public virtual bool WriteOffInterestDue(FinancePlanVisit newFinancePlanVisit = null)
        {
            if (this.InterestDue <= 0)
            {
                return false;
            }
            //Wondering if I'm also going to need the child link?  So its both ways?
            FinancePlanVisitInterestDue movedInterestDue = newFinancePlanVisit?.FinancePlanVisitInterestDues.FirstOrDefault(x => x.InterestDue == this.InterestDue);

            FinancePlanVisitInterestDue writtenOffInterest = this.AddFinancePlanVisitInterestWriteOff(decimal.Negate(this.InterestDue));

            if (movedInterestDue != null)
            {
                movedInterestDue.ParentFinancePlanVisitInterestDue = writtenOffInterest;
            }

            return true;
        }

        public virtual bool WriteOffInterest(decimal amountToWriteOff, DateTime? overrideInsertDate = null)
        {
            if (amountToWriteOff > this.InterestAssessed)
            {
                return false;
            }

            this.AddFinancePlanVisitInterestWriteOff(decimal.Negate(amountToWriteOff), overrideInsertDate);

            return true;
        }

        public virtual bool HasFinancialAssistance => this.InterestZero;

        // todo: remove this one
        public virtual FinancePlanVisitInterestDue AddFinancePlanVisitInterestAssessment(decimal interestDue, DateTime currentStatementEndDate)
        {
            return this.AddFinancePlanVisitInterestAssessment(interestDue, this.FinancePlan.CurrentInterestRate, currentStatementEndDate);
        }

        public virtual FinancePlanVisitInterestDue AddFinancePlanVisitInterestAssessment(decimal interestDue, decimal interestRate, DateTime currentStatementEndDate)
        {
            FinancePlanVisitInterestDue financePlanVisitInterestDue = new FinancePlanVisitInterestDue
            {
                FinancePlanVisit = this,
                FinancePlan = this.FinancePlan,
                FinancePlanVisitInterestDueType = FinancePlanVisitInterestDueTypeEnum.InterestAssessment,
                CurrentBalanceAtTimeOfAssessment = this.MaxCurrentBalance,
                InsertDate = DateTime.UtcNow,
                InterestDue = interestDue,
                InterestRateUsedToAssess = interestRate,
                OverrideInsertDate = currentStatementEndDate.AddMilliseconds(-10)
            };
            this._financePlanInterestDues.Add(financePlanVisitInterestDue);

            // decrement any override if assessing interest
            if (interestDue >= 0)
            {
                this.AddPostAssessmentFinancePlanVisitInterestRateHistory();
            }
            
            this.FinancePlan?.FinancePlanVisitInterestDueItems.Add(financePlanVisitInterestDue);

            return financePlanVisitInterestDue;
        }

        public virtual FinancePlanVisitInterestDue AddFinancePlanVisitInterestPayment(decimal interestDue, int paymentAllocationId)
        {
            FinancePlanVisitInterestDue financePlanVisitInterestDue = new FinancePlanVisitInterestDue
            {
                FinancePlanVisit = this,
                FinancePlan = this.FinancePlan,
                FinancePlanVisitInterestDueType = FinancePlanVisitInterestDueTypeEnum.InterestPayment,
                InsertDate = DateTime.UtcNow,
                InterestDue = interestDue,
                PaymentAllocationId = paymentAllocationId,
            };

            this._financePlanInterestDues.Add(financePlanVisitInterestDue);

            return financePlanVisitInterestDue;
        }

        public virtual FinancePlanVisitInterestDue AddFinancePlanVisitInterestWriteOff(decimal interestDue, DateTime? overrideInsertDate = null)
        {
            FinancePlanVisitInterestDue financePlanVisitInterestDue = new FinancePlanVisitInterestDue
            {
                FinancePlanVisit = this,
                FinancePlan = this.FinancePlan,
                FinancePlanVisitInterestDueType = FinancePlanVisitInterestDueTypeEnum.InterestAssessment,
                InsertDate = DateTime.UtcNow,
                InterestDue = interestDue,
                CurrentBalanceAtTimeOfAssessment = this.MaxCurrentBalance,
                OverrideInsertDate = overrideInsertDate
            };

            this._financePlanInterestDues.Add(financePlanVisitInterestDue);
            this.FinancePlan?.FinancePlanVisitInterestDueItems.Add(financePlanVisitInterestDue);

            return financePlanVisitInterestDue;
        }

        public virtual FinancePlanVisitInterestDue AddFinancePlanVisitInterestDue(decimal interestDue, int? paymentAllocationId, FinancePlanVisitInterestDueTypeEnum type)
        {
            FinancePlanVisitInterestDue financePlanVisitInterestDue = new FinancePlanVisitInterestDue
            {
                FinancePlanVisit = this,
                FinancePlan = this.FinancePlan,
                FinancePlanVisitInterestDueType = type,
                InsertDate = DateTime.UtcNow,
                InterestDue = interestDue,
                CurrentBalanceAtTimeOfAssessment = this.MaxCurrentBalance,
                PaymentAllocationId = paymentAllocationId
            };

            this._financePlanInterestDues.Add(financePlanVisitInterestDue);
            
            this.FinancePlan?.FinancePlanVisitInterestDueItems.Add(financePlanVisitInterestDue);

            return financePlanVisitInterestDue;
        }

        public virtual decimal? OverrideInterestRate
        {
            get
            {
                FinancePlanVisitInterestRateHistory currentHistory = this.CurrentInterestRateHistory();
                return currentHistory?.OverrideInterestRate;
            }
        }

        public virtual int OverridesRemaining
        {
            get
            {
                FinancePlanVisitInterestRateHistory currentHistory = this.CurrentInterestRateHistory();
                return currentHistory?.OverridesRemaining ?? 0;
            }
        }

        #endregion

        #region IVisit and IFinancePlanVisitVisit

        public virtual int VisitId { get; set; }

        public override decimal CurrentVisitBalance
        {
            get => this.FinancePlanVisitVisit.CurrentBalance;
            set => this.FinancePlanVisitVisit.CurrentBalance = value;
        }

        public virtual FinancePlanVisitVisit FinancePlanVisitVisit { get; set; } = new FinancePlanVisitVisit();

        public virtual VisitStateEnum CurrentVisitState
        {
            get => this.FinancePlanVisitVisit.CurrentVisitState;
            set => this.FinancePlanVisitVisit.CurrentVisitState = value;
        }

        public virtual string BillingApplication
        {
            get => this.FinancePlanVisitVisit.BillingApplication;
            set => this.FinancePlanVisitVisit.BillingApplication = value;
        }

        public virtual string SourceSystemKey
        {
            get => this.FinancePlanVisitVisit.SourceSystemKey;
            set => this.FinancePlanVisitVisit.SourceSystemKey = value;
        }

        public virtual int BillingSystemId
        {
            get => this.FinancePlanVisitVisit.BillingSystemId;
            set => this.FinancePlanVisitVisit.BillingSystemId = value;
        }

        public virtual bool InterestZero
        {
            get => this.FinancePlanVisitVisit.InterestZero;
            set => this.FinancePlanVisitVisit.InterestZero = value;
        }

        #endregion

        #region PrincipalAmounts

        public virtual decimal MaxCurrentBalance
        {
            get
            {
                //This should be payments (cleared or uncleared) and HSBalance
                decimal calculatedVisitBalance = this.CalculateBalance(this.CurrentVisitBalance, this.UnclearedPaymentsSum, 0);
                decimal maxBalance = Math.Min(this.PrincipalBalance, calculatedVisitBalance);

                return this.CalculateBalance(maxBalance, 0, this.InterestDue);
            }
        }

        public virtual decimal PrincipalBalance => this.PrincipalBalanceAsOf(DateTime.MaxValue);

        public virtual decimal PrincipalBalanceAsOf(DateTime endDate)
        {
            decimal principalBalance = this.FinancePlanVisitPrincipalAmounts
                .Where(x => (x.OverrideInsertDate ?? x.InsertDate) <= endDate)
                .Sum(x => x.Amount);

            return principalBalance;
        }

        /// <summary>
        /// Doesnt include interest, but includes uncleared payments and HS balance
        /// This is how we decide if there's a difference in what was snapshotted(FinancePlanVisitPrincipalAmounts)
        /// with the actions applied (payments, closures, etc) vs the HS balance equivalent (VisitBalance)
        /// </summary>
        public virtual decimal VisitBalanceDifference
        {
            get
            {
                decimal calculatedVisitBalance = this.CalculateBalance(this.CurrentVisitBalance, this.UnclearedPaymentsSum, 0);

                return calculatedVisitBalance - this.PrincipalBalance;
            }
        }

        #endregion
    }
}
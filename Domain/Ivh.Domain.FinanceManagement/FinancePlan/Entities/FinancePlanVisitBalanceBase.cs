﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
    public abstract class FinancePlanVisitBalanceBase
    {
        public virtual decimal CurrentVisitBalance { get; set; }

        public virtual decimal UnclearedPaymentsSum { get; set; }

        public virtual decimal InterestDue { get; set; }
        
        public virtual decimal CurrentBalance
        {
            get
            {
                decimal currentVisitBalance = this.CurrentVisitBalance;
                decimal unclearedPaymentsSum = this.UnclearedPaymentsSum;
                decimal interestDue = this.InterestDue;

                return this.CalculateBalance(currentVisitBalance, unclearedPaymentsSum, interestDue);
            }
        }

        public virtual decimal CalculateBalance(decimal currentVisitBalance, decimal unclearedPaymentsSum, decimal interestDue)
        {
            return currentVisitBalance + unclearedPaymentsSum + interestDue;
        }
    }
}
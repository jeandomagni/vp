﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
    using Common.VisitPay.Enums;

    public class FinancePlanType
    { 
        public virtual int FinancePlanTypeId { get; set; }
        public virtual FinancePlanTypeEnum FinancePlanTypeIdeEnum => (FinancePlanTypeEnum)this.FinancePlanTypeId;
        public virtual string FinancePlanTypeName { get; set; }
        public virtual string FinancePlanTypeDisplayName { get; set; }
        public virtual bool RequireCreditAgreement { get; set; }
        public virtual bool RequireEsign { get; set; }
        public virtual bool RequirePaymentMethod { get; set; }
        public virtual bool RequireEmailCommunication { get; set; }
        public virtual bool RequirePaperCommunication { get; set; }
        public virtual bool IsAutoPay { get; set; }
        public virtual bool IsPaymentMethodEditable { get; set; }
    }
}

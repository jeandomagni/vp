namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
    public class InterestAssessPeriodVisitNode
    {/*
        public bool InterestRateChangedForFp { get; set; }

        public FinancePlanOffer NewOffer { get; set; }

        public bool IsInterestReversedAndReallocated { get; set; }

        public bool IsReallocatedInterestToPrincipalPayment { get; set; }

        public FinancePlanVisit FinancePlanVisit { get; set; }

        public StatementPeriodResult Period { get; set; }

        public int FinancePlanId => this.FinancePlan?.FinancePlanId ?? 0;

        public FinancePlanOffer OfferToUse => this.NewOffer ?? this.FinancePlan.FinancePlanOffer;

        public FinancePlan FinancePlan => this.FinancePlanVisit?.FinancePlan;

        public Visit Visit => this.FinancePlanVisit?.Visit;

        public int VisitId => this.FinancePlanVisit?.VisitId ?? 0;

        [Obsolete(ObsoleteDescription.VisitTransactions)]
        public decimal NetNegativeAdjustmentAmount
        {
            get
            {
                if (this.AdjustmentAmount < 0)
                {
                    return this.AdjustmentAmount;
                }
                return 0m;
            }
        }

        [Obsolete(ObsoleteDescription.VisitTransactions)]
        public decimal AdjustmentAmount
        {
            get
            {
                if (this.FinancePlanVisit != null && this.Period != null)
                {
                    return this.Visit.AdjustmentsForDateRange(this.Period.PeriodStartDate, this.Period.PeriodEndDate);
                }
                return 0m;
            }
        }

        [Obsolete(ObsoleteDescription.VisitTransactions)]
        public decimal InterestAssessed
        {
            get
            {
                if (this.FinancePlanVisit != null && this.Period != null)
                {
                    return this.FinancePlanVisit.InterestAssessedForDateRange(this.Period.PeriodStartDate, this.Period.PeriodEndDate);
                }
                return 0m;
            }
        }

        [Obsolete(ObsoleteDescription.VisitTransactions)]
        public decimal PeriodEndingValue
        {
            get
            {
                if (this.FinancePlanVisit == null || this.Period == null)
                {
                    return 0m;
                }

                decimal currentVisitBalance = this.Visit.CurrentBalanceAsOf(this.Period.PeriodEndDate);
                decimal interestDue = this.FinancePlanVisit.InterestDueForDateRange(null, this.Period.PeriodEndDate);
                // todo: should also have uncleared payments here

                return currentVisitBalance + interestDue; // + UnclearedPayments
            }
        }*/
    }
}
﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
    using System;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class FinancePlanVisitResult
    {
        public virtual int FinancePlanId { get; set; }
        public virtual int VisitId { get; set; }
        public virtual DateTime? HardRemoveDate { get; set; }
        public virtual FinancePlanStatusEnum FinancePlanStatus { get; set; }
        public virtual string VisitSourceSystemKey { get; set; }
        public virtual int? VisitBillingSystemId { get; set; }
    }
}
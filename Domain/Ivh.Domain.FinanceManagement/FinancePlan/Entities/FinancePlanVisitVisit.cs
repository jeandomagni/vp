﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
    using System;
    using Application.Base.Common.Interfaces.Entities.Visit;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class FinancePlanVisitVisit : IFinancePlanVisitVisit
    {
        public virtual int VisitId { get; set; }
        public virtual decimal CurrentBalance { get; set; }
        public virtual VisitStateEnum CurrentVisitState { get; set; }
        public virtual string BillingApplication { get; set; }
        public virtual bool InterestZero { get; set; }
        public virtual DateTime? DischargeDate { get; set; }
        public virtual string SourceSystemKey { get; set; }
        public virtual string MatchedSourceSystemKey { get; set; }
        public virtual int BillingSystemId { get; set; }
        public virtual bool VpEligible { get; set; }
        public virtual bool BillingHold { get; set; }
        public virtual bool Redact { get; set; }
        public virtual bool IsUnmatched { get; set; }
        public virtual int? FacilityId { get; set; }

        public virtual AgingTierEnum AgingTier { get; set; }
        [Obsolete("Use AgingTier instead")]
        public virtual int AgingCount => (int)this.AgingTier;
        public virtual bool IsMaxAge => this.AgingTier == AgingTierEnum.MaxAge;
    }
}
﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
    using System;
    using Guarantor.Entities;
    using Statement.Entities;

    public class FinancePlanOffer
    {
        public FinancePlanOffer()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        public virtual int FinancePlanOfferId { get; set; }

        public virtual Guarantor VpGuarantor { get; set; }

	    public virtual VpStatement VpStatement { get; set; }

	    public virtual decimal StatementedCurrentAccountBalance { get; set; }

	    public virtual int DurationRangeStart { get; set; }

	    public virtual int DurationRangeEnd { get; set; }

	    public virtual decimal MinPayment { get; set; }

	    public virtual decimal MaxPayment { get; set; }

	    public virtual decimal InterestRate { get; set; }

	    public virtual bool IsActive { get; set; }

        public virtual DateTime InsertDate { get; set; }
        public virtual Guid CorrelationGuid { get; set; }
        public virtual bool IsCharity { get; set; }
        public virtual bool IsCombined { get; set; }

        public virtual FinancePlanOfferSetType FinancePlanOfferSetType { get; set; }
    }
}

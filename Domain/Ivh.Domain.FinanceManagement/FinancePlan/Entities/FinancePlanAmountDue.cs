﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
    using System;
    using Common.VisitPay.Enums;

    public class FinancePlanAmountDue
    {
        public FinancePlanAmountDue()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        public virtual int FinancePlanAmountDueId { get; set; }

        public virtual DateTime InsertDate { get; set; }

        public virtual FinancePlan FinancePlan { get; set; }

        public virtual decimal AmountDue { get; set; }

        public virtual DateTime? DueDate { get; set; }

        public virtual DateTime? OverrideDueDate { get; set; }

        /// <summary>
        /// when this exists and the allocation doesnt, it's a failed payment
        /// </summary>
        public virtual int? PaymentId { get; set; }

        /// <summary>
        /// this should exist if it was a successful payment
        /// </summary>
        public virtual int? PaymentAllocationId { get; set; }

        public virtual int? VpStatementId { get; set; }

        public virtual FinancePlanVisit FinancePlanVisit { get; set; }

        #region projections to payment allocation
        
        public virtual PaymentTypeEnum? PaymentType { get; set; }
        public virtual PaymentAllocationTypeEnum? PaymentAllocationType { get; set; }
        public virtual decimal PaymentAllocationActualAmount { get; set; }

        #endregion
    }
}
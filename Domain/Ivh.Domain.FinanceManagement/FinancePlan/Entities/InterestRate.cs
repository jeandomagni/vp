﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
	public class InterestRate
	{
		public virtual int InterestRateId { get; set; }
		public virtual int DurationRangeStart { get; set; }
		public virtual int DurationRangeEnd { get; set; }
		public virtual decimal Rate { get; set; }
		public virtual string DisplayText { get; set; }

        public virtual decimal? MinimumBalance { get; set; }
        public virtual decimal? MaximumBalance { get; set; }
        public virtual bool? IsCharity { get; set; }
        public virtual int? RandomizedTestGroupId { get; set; }

        public virtual decimal? MinimumPtpScore { get; set; }
        public virtual decimal? MaximumPtpScore { get; set; }

        public virtual FinancePlanOfferSetType FinancePlanOfferSetType { get; set; }
    }
}

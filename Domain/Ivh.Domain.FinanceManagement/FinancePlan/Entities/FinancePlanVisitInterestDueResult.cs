﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
    using System;
    using Common.VisitPay.Enums;

    public class FinancePlanVisitInterestDueResult
    {
        public int VisitId { get; set; }
        public decimal InterestDue { get; set; }
        public DateTime InsertDate { get; set; }
        public DateTime? OverrideInsertDate { get; set; }
        public FinancePlanVisitInterestDueTypeEnum FinancePlanVisitInterestDueType { get; set; }
    }
}
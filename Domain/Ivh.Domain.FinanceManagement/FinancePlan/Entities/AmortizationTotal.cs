﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
    public class AmortizationTotal
    {
        public string BillingApplicationName { get; set; }
        public decimal InterestCollectableAmount { get; set; }
        public decimal InterestDeferredAmount { get; set; }
        public decimal PaymentAllocationPercentage { get; set; }
        public decimal InterestCollectableProRataPercentage { get; set; }
        public decimal InterestDeferredProRataPercentage { get; set; }
    }
}

﻿using System;

namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class FinancePlanVisitInterestDue
    {
        public FinancePlanVisitInterestDue()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int FinancePlanVisitInterestDueId { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual FinancePlan FinancePlan { get; set; }
        public virtual decimal InterestDue { get; set; }
        public virtual int? VpStatementId { get; set; } // This should have been the financeplanstatementid
        public virtual int? PaymentAllocationId { get; set; }
        public virtual FinancePlanVisit FinancePlanVisit { get; set; }
        public virtual decimal? CurrentBalanceAtTimeOfAssessment { get; set; }
        public virtual decimal? InterestRateUsedToAssess { get; set; }
        
        //If this is set, this points at the write off interest due from a previous FP
        public virtual FinancePlanVisitInterestDue ParentFinancePlanVisitInterestDue { get; set; }
        public virtual DateTime? OverrideInsertDate { get; set; }
        public virtual FinancePlanVisitInterestDueTypeEnum FinancePlanVisitInterestDueType { get; set; }
    }
}

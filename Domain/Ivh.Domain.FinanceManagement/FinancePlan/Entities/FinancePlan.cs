﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Application.FinanceManagement.Common.Dtos;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.FinanceManagement;
    using Common.VisitPay.Messages.HospitalData;
    using Guarantor.Entities;
    using Interfaces;
    using Ivh.Common.VisitPay.Attributes;
    using Payment.Entities;
    using Statement.Entities;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using User.Entities;

    public class FinancePlan
    {
        public FinancePlan()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        private decimal _currentFinancePlanBalanceOnLoad;

        private string _financePlanPublicIdPhi;

        private IList<FinancePlanAmountDue> _financePlanAmountsDue;
        private IList<FinancePlanBucketHistory> _financePlanBucketHistories;
        private IList<FinancePlanCreditAgreement> _financePlanCreditAgreements;
        private IList<FinancePlanInterestRateHistory> _financePlanInterestRateHistories;
        private IList<FinancePlanPaymentAmountHistory> _financePlanPaymentAmountHistories;
        private IList<FinancePlanStatusHistory> _financePlanStatusHistories;
        private IList<FinancePlanVisit> _financePlanVisits;
        private IList<FinancePlanChangedStatusMessage> _unpublishedFinancePlanChangedStatusMessages;
        private IList<AuditEventTypeFinancePlanEnum> _unpublishedFinancePlanAuditEvents = new List<AuditEventTypeFinancePlanEnum> {AuditEventTypeFinancePlanEnum.Default};
        private IList<OutboundVisitMessage> _outboundVisitMessages;
        private IList<FinancePlanVisitInterestDue> _financePlanVisitInterestDueItems;

        public virtual UpdateReconfiguredFinancePlanInterestMessage UnpublishedUpdateReconfiguredFinancePlanInterestMessage { get; set; }

        #region persisted properties

        public virtual int FinancePlanId { get; set; }

        public virtual Guarantor VpGuarantor { get; set; }

        public virtual FinancePlanOffer FinancePlanOffer { get; set; }

        public virtual DateTime InsertDate { get; set; }

        public virtual DateTime? OriginationDate { get; set; }

        public virtual DateTime? TermsAgreedDate { get; protected set; }

        public virtual int? TermsCmsVersionId { get; protected set; }

        public virtual int? AcceptedTermsVisitPayUserId { get; protected set; }

        public virtual decimal OriginatedFinancePlanBalance { get; set; }
        
        public virtual decimal? InterestAssessed
        {
            get => this.InterestAssessedForDateRange(null, DateTime.MaxValue);
            // ReSharper disable once ValueParameterNotUsed
            set { }
        }

        public virtual decimal CurrentFinancePlanBalance
        {
            get
            {
                //Only want positive values, as the negative values should be closed and then removed from the FP.
                //Closed-end Changed from CurrentBalance to FinancePlanVisitBalance
                decimal result = this.ActiveFinancePlanVisits.Sum(x => x.MaxCurrentBalance);
                return result;
            }
            // ReSharper disable once ValueParameterNotUsed
            set { }
        }

        public virtual VpStatement CreatedVpStatement { get; set; }

        public virtual decimal? OriginalPaymentAmount { get; set; }

        public virtual int? OriginalDuration { get; set; }

        /// <summary>
        /// This is the reference to an old finance plan that was reconfigured.
        /// </summary>
        public virtual FinancePlan OriginalFinancePlan { get; set; }

        /// <summary>
        /// This is the new finance plan that reconfigures this one.
        /// </summary>
        public virtual FinancePlan ReconfiguredFinancePlan { get; set; }
        
        /// <summary>
        /// This is the finance plan that was created as a result of a combination
        /// </summary>
        public virtual FinancePlan CombinedFinancePlan { get; set; }
        
        public virtual int CurrentBucket
        {
            get => this.Buckets.Any() ? this.Buckets.Keys.Max() : 0;
            // ReSharper disable once ValueParameterNotUsed
            set { }
        }

        public virtual int? CurrentBucketOverride { get; set; }

        public virtual OfferCalculationStrategyEnum OfferCalculationStrategy { get; set; }

        public virtual int? CustomizedVisitPayUserId { get; set; }
        
        // VPNG-19800 CurrentFinancePlanStatus not matching with latest status in FinancePlanStatusHistory 
        // This purpose of this property is to persist the value in FinancePlan in order to make querying easier (outside of the application)
        // It should never be set and always be derived to reflect the most recent FinancePlanStatusHistory
        // This follows a similar pattern as Visit.TotalCharges
        public virtual FinancePlanStatus CurrentFinancePlanStatus
        {
            get => this.FinancePlanStatus;
            // ReSharper disable once ValueParameterNotUsed
            set { }
        }

        public virtual bool IsCombined { get; set; }

        public virtual string FinancePlanUniqueId { get; set; }

        public virtual DateTime? FirstPaymentDueDate { get; set; }

        public virtual IList<FinancePlanAmountDue> FinancePlanAmountsDue
        {
            get => this._financePlanAmountsDue ?? (this.FinancePlanAmountsDue = new List<FinancePlanAmountDue>());
            protected set => this._financePlanAmountsDue = value;
        }

        public virtual IList<FinancePlanBucketHistory> FinancePlanBucketHistories
        {
            get => this._financePlanBucketHistories ?? (this.FinancePlanBucketHistories = new List<FinancePlanBucketHistory>());
            set => this._financePlanBucketHistories = value;
        }

        public virtual IList<FinancePlanInterestRateHistory> FinancePlanInterestRateHistory
        {
            get => this._financePlanInterestRateHistories ?? (this.FinancePlanInterestRateHistory = new List<FinancePlanInterestRateHistory>());
            set => this._financePlanInterestRateHistories = value;
        }

        public virtual IList<FinancePlanPaymentAmountHistory> FinancePlanPaymentAmountHistories
        {
            get => this._financePlanPaymentAmountHistories ?? (this.FinancePlanPaymentAmountHistories = new List<FinancePlanPaymentAmountHistory>());
            set => this._financePlanPaymentAmountHistories = value;
        }

        public virtual IList<FinancePlanStatusHistory> FinancePlanStatusHistory
        {
            get => this._financePlanStatusHistories ?? (this.FinancePlanStatusHistory = new List<FinancePlanStatusHistory>());
            set => this._financePlanStatusHistories = value;
        }

        public virtual IList<FinancePlanVisit> FinancePlanVisits
        {
            get => this._financePlanVisits ?? (this.FinancePlanVisits = new List<FinancePlanVisit>());
            set
            {
                this._financePlanVisits = value;
                this._currentFinancePlanBalanceOnLoad = this.CurrentFinancePlanBalance;
            }
        }

        public virtual IList<FinancePlanCreditAgreement> FinancePlanCreditAgreements
        {
            get => this._financePlanCreditAgreements ?? (this.FinancePlanCreditAgreements = new List<FinancePlanCreditAgreement>());
            set => this._financePlanCreditAgreements = value;
        }

        public virtual InterestCalculationMethodEnum InterestCalculationMethod { get; set; }

        /// <summary>
        /// The public identifier for the Finance Plan. This field is PHI. 
        /// Callers should use <see cref="GenerateAndSetPublicIdPhi"/> to assign a value
        /// rather than setting directly, which does not guarantee a valid identifier.
        /// </summary>
        [Phi]
        public virtual string FinancePlanPublicIdPhi
        {
            get => this._financePlanPublicIdPhi;

            protected set
            {
                bool isValid = FinancePlanPublicIdPhiSource.IsValidFormat(value);
                this._financePlanPublicIdPhi = isValid ? value : null;
            }
        }

        public virtual FinancePlanTypeEnum OriginatedFinancePlanType { get; set; }
        #endregion

        #region balance helpers

        public virtual bool HasBalanceChanged => this._currentFinancePlanBalanceOnLoad != this.CurrentFinancePlanBalance;
        
        public virtual decimal InterestAssessedForDateRange(DateTime? startDate, DateTime endDate)
        {
            return this.FinancePlanVisits.Sum(x => x.InterestAssessedForDateRange(startDate, endDate));
        }

        public virtual decimal InterestDue
        {
            get { return this.FinancePlanVisits.Sum(x => x.InterestDue); }
        }
        
        public virtual decimal InterestDueForDate(DateTime endDate)
        {
            if (endDate == DateTime.MaxValue || endDate == DateTime.MinValue)
            {
                return 0m;
            }

            return this.FinancePlanVisits.Sum(x => x.InterestDueForDateRange(null, endDate));
        }
        
        public virtual decimal InterestPaidForDate(DateTime endDate)
        {
            if (this.OriginationDate == null)
            {
                return 0m;
            }

            DateTime startDate = new[] { this.OriginationDate.Value, this.InsertDate }.Min();

            return this.InterestPaidForDateRange(startDate, endDate);
        }
        
        public virtual decimal InterestPaidForDateRange(DateTime startDate, DateTime endDate)
        {
            if (endDate == DateTime.MaxValue ||
                endDate == DateTime.MinValue ||
                startDate == DateTime.MaxValue ||
                startDate == DateTime.MinValue)
            {
                return 0m;
            }

            //Removing this as part of VP-1680 (RoboRefund)
            //If it needed to be there, we're going to have to add an overrideInsertDate to FinancePlanAmountDue,
            //when robo'in make sure it's set to the end of the FP so the balances look correct
            //endDate = new[] { this.ClosedDate().GetValueOrDefault(endDate), endDate }.Min();
            
            DateTime? startOfFp = this.OriginationDateTime();
            if (startOfFp != null && startDate < startOfFp)
            {
                startDate = startOfFp.Value;
            }

            return this.FinancePlanVisits.Sum(x => x.InterestPaidForDateRange(startDate, endDate));
        }
        
        public virtual decimal PrincipalPaidForDate(DateTime endDate)
        {
            if (this.OriginationDate == null)
            {
                return 0m;
            }

            DateTime startDate = new[] { this.OriginationDate.Value, this.InsertDate }.Min();

            return this.PrincipalPaidForDateRange(startDate, endDate);
        }
        
        public virtual decimal PrincipalPaidForDateRange(DateTime startDate, DateTime endDate)
        {
            if (endDate == DateTime.MaxValue ||
                endDate == DateTime.MinValue ||
                startDate == DateTime.MaxValue ||
                startDate == DateTime.MinValue)
            {
                return 0m;
            }

            endDate = new[] {this.ClosedDate().GetValueOrDefault(endDate), endDate}.Min();
            DateTime? startOfFp = this.OriginationDateTime();
            if (startOfFp != null && startDate < startOfFp)
            {
                startDate = startOfFp.Value;
            }

            return this.FinancePlanVisits.Sum(x => x.PrincipalPaidForDateRange(startDate, endDate));
        }
        
        public virtual decimal PositiveVisitBalanceDifferenceSum
        {
            get { return this.ActiveFinancePlanVisits.Select(x => x.VisitBalanceDifference).Where(x => x > 0m).Sum(); }
        }

        public virtual bool HasPositiveVisitBalanceDifference()
        {
            return this.PositiveVisitBalanceDifferenceSum > 0m;
        }
        
        public virtual decimal CurrentFinancePlanBalanceIncludingAllAdjustments
        {
            get { return this.ActiveFinancePlanVisits.Sum(x => x.CurrentBalance); }
        }

        #endregion

        #region date helpers

        public virtual DateTime? OriginationDateTime()
        {
            FinancePlanStatusHistory originatedStatus = this.FinancePlanStatusHistory
                .OrderBy(x => x.InsertDate)
                .ThenBy(x => x.FinancePlanStatusHistoryId)
                .FirstOrDefault(x => x.FinancePlanStatus.IsActiveOriginated());

            return originatedStatus?.InsertDate;
        }
        
        public virtual DateTime? ClosedDate()
        {
            FinancePlanStatusHistory history = this.FinancePlanStatusHistory
                .Where(x => x.FinancePlanStatus.IsClosed() ||
                            x.FinancePlanStatus.IsClosedUncollectable())
                .OrderByDescending(x => x.InsertDate)
                .ThenByDescending(x => x.FinancePlanStatusHistoryId)
                .FirstOrDefault();

            return history?.InsertDate;
        }

        public virtual DateTime? LastTimeBeforeClosed()
        {
            FinancePlanStatusHistory firstClosedStatus = this.FinancePlanStatusHistory
                .OrderBy(x => x.InsertDate)
                .ThenBy(x => x.FinancePlanStatusHistoryId)
                .FirstOrDefault(x => x.FinancePlanStatus.IsClosed());

            return firstClosedStatus?.InsertDate.AddMilliseconds(-10);
        }

        #endregion

        #region interest rate / ifp helpers

        public virtual decimal InterestRateAsOf(DateTime endDate)
        {
            FinancePlanInterestRateHistory lastForThatTime = this.FinancePlanInterestRateHistory
                .OrderByDescending(x => x.InsertDate)
                .ThenByDescending(x => x.FinancePlanInterestRateHistoryId)
                .FirstOrDefault(x => x.InsertDate <= endDate);

            return lastForThatTime?.InterestRate ?? 0m;
        }
        
        public virtual decimal CurrentInterestRate => this.InterestRateAsOf(DateTime.MaxValue);

        public virtual void SetInterestRate(decimal newInterestRate, string reason)
        {
            if (newInterestRate > 1 || newInterestRate < 0)
            {
                return;
            }

            this.FinancePlanInterestRateHistory.Add(new FinancePlanInterestRateHistory()
            {
                ChangeDescription = reason,
                InsertDate = DateTime.UtcNow, //This InsertDate will get overridden when saved to the DB...  But we need this here to show up on for this.CurrentInterestRate
                InterestRate = newInterestRate,
                FinancePlan = this
            });

            if (this.ReconfiguredFinancePlan != null && this.ReconfiguredFinancePlan.IsNotOriginated())
            {
                if (this.UnpublishedUpdateReconfiguredFinancePlanInterestMessage == null)
                {
                    this.UnpublishedUpdateReconfiguredFinancePlanInterestMessage = new UpdateReconfiguredFinancePlanInterestMessage();
                }
                this.UnpublishedUpdateReconfiguredFinancePlanInterestMessage.ParentFinancePlanId = this.FinancePlanId;
                this.UnpublishedUpdateReconfiguredFinancePlanInterestMessage.ReconfiguredFinancePlanId = this.ReconfiguredFinancePlan.FinancePlanId;
                this.UnpublishedUpdateReconfiguredFinancePlanInterestMessage.Reason = reason;
                this.UnpublishedUpdateReconfiguredFinancePlanInterestMessage.ParentFinancePlanInterest = newInterestRate;
            }
        }

        public virtual void SetFinancialAssistanceInterestRate(decimal interestRate)
        {
            this.SetInterestRate(interestRate, "Financial Assistance");
            foreach (FinancePlanVisit financePlanVisit in this.FinancePlanVisits)
            {
                financePlanVisit.SetInterestRate(interestRate);
            }
        }

        public virtual void SetFinancePlanIncentiveInterestRate(decimal interestRate)
        {
            this.SetInterestRate(interestRate, "Finance Plan Incentive");
            foreach (FinancePlanVisit financePlanVisit in this.FinancePlanVisits)
            {
                financePlanVisit.SetInterestRate(interestRate);
            }
        }

        public virtual FirstInterestPeriodEnum FirstInterestPeriod()
        {
            return FirstInterestPeriod(this.CreatedVpStatement?.IsGracePeriod ?? false);
        }

        /// <summary>
        /// there's a built in interest free period when a finance plan is created during grace period (combo plans/reconfigs excluded)
        /// it's not really an interest free period, but the first payment will deduct before the next statement runs, so any interest would be assessed on a lesser amount
        /// </summary>
        /// <returns></returns>
        public static FirstInterestPeriodEnum FirstInterestPeriod(bool isGracePeriodOfCreatedStatement)
        {
            if (!isGracePeriodOfCreatedStatement)
            {
                // awaiting statement - no "free" payment
                return FirstInterestPeriodEnum.NoAction;
            }

            // grace period - get "free" payment before interest assesses
            return FirstInterestPeriodEnum.PushOnePeriod;
        }

        public virtual bool HasFinancialAssistance()
        {
            return this.ActiveFinancePlanVisits?.Any(x => x.HasFinancialAssistance) ?? false;
        }
        
        public virtual void WriteOffInterestDue()
        {
            FinancePlan lookForMovedInterest = null;
            if (this.CombinedFinancePlan != null || this.ReconfiguredFinancePlan != null)
            {
                lookForMovedInterest = this.CombinedFinancePlan ?? this.ReconfiguredFinancePlan;
            }

            foreach (FinancePlanVisit financePlanVisit in this.FinancePlanVisits)
            {
                FinancePlanVisit newFpv = null;
                if (lookForMovedInterest != null)
                {
                    newFpv = lookForMovedInterest.FinancePlanVisits.FirstOrDefault(x => x.VisitId == financePlanVisit.VisitId);
                }

                financePlanVisit.WriteOffInterestDue(newFpv);
            }
        }

        //Should prob unit test this
        public virtual decimal WriteOffInterest(decimal totalAmount)
        {
            return this.WriteOffInterestInternal(totalAmount, this.FinancePlanVisits);
        }

        public virtual decimal WriteOffInterestForFinancePlanVisit(decimal totalAmount, FinancePlanVisit financePlanVisit)
        {
            return this.WriteOffInterestInternal(totalAmount, financePlanVisit.ToListOfOne());
        }

        private decimal WriteOffInterestInternal(decimal totalAmount, IList<FinancePlanVisit> financePlanVisits)
        {
            decimal amountWrittenOff = 0m;
            foreach (FinancePlanVisit financePlanVisit in financePlanVisits)
            {
                decimal amountToWriteOff = (totalAmount - amountWrittenOff);
                if (amountToWriteOff > 0m)
                {
                    decimal actualAmount = amountToWriteOff >= financePlanVisit.InterestAssessed ? financePlanVisit.InterestAssessed : amountToWriteOff;
                    if (financePlanVisit.WriteOffInterest(actualAmount))
                    {
                        amountWrittenOff += actualAmount;
                    }
                }
            }

            return amountWrittenOff;
        }

        #endregion

        #region offer type helpers

        public virtual int FinancePlanOfferSetTypeId => (this.FinancePlanOffer?.FinancePlanOfferSetType?.FinancePlanOfferSetTypeId).GetValueOrDefault(0);

        public virtual bool IsFinancePlanOfferTypePatient => (this.FinancePlanOffer?.FinancePlanOfferSetType?.IsPatient).GetValueOrDefault(true);

        public virtual bool IsFinancePlanOfferTypeClientOnly => (this.FinancePlanOffer?.FinancePlanOfferSetType?.IsClientOnly).GetValueOrDefault(false);

        #endregion

        #region payment amount helpers

        public virtual decimal PaymentAmountAsOf(DateTime endDate)
        {
            FinancePlanPaymentAmountHistory lastForThatTime = this.FinancePlanPaymentAmountHistories
                .OrderByDescending(x => x.InsertDate)
                .ThenByDescending(x => x.FinancePlanPaymentAmountHistoryId)
                .FirstOrDefault(x => x.InsertDate <= endDate);

            return lastForThatTime?.PaymentAmount ?? 0m;
        }

        public virtual decimal PaymentAmount
        {
            get => this.PaymentAmountAsOf(DateTime.MaxValue);
            set => this.SetPaymentAmount(value, "");
        }
        
        public virtual void CheckPaymentAmountWhenUpdatingFinancePlan(decimal monthlyPaymentAmount)
        {
            if (this.PaymentAmount != monthlyPaymentAmount)
            {
                this.OriginalPaymentAmount = monthlyPaymentAmount;
                this.PaymentAmount = monthlyPaymentAmount;
            }

            if (this.PaymentAmount > this.CurrentFinancePlanBalance)
            {
                this.PaymentAmount = this.CurrentFinancePlanBalance;
            }
        }
        
        private void SetPaymentAmount(decimal value, string reason)
        {
            //Last message wins.
            this.UnpublishedFinancePlanPaymentAmountChangedMessage = new FinancePlanPaymentAmountChangedMessage
            {
                FinancePlanId = this.FinancePlanId,
                NewPaymentAmount = value
            };

            this.FinancePlanPaymentAmountHistories.Add(new FinancePlanPaymentAmountHistory
            {
                ChangeDescription = reason,
                FinancePlan = this,
                InsertDate = DateTime.UtcNow,
                PaymentAmount = value
            });
        }

        #endregion

        #region reconfiguration helpers

        public virtual bool IsEligibleForReconfiguration()
        {
            if (!this.IsActiveOriginated())
            {
                return false;
            }

            if (this.ReconfiguredFinancePlan == null || this.ReconfiguredFinancePlan.IsPendingReconfiguration())
            {
                return true;
            }

            return false;
        }

        public virtual bool IsReconfiguration()
        {
            return this.OriginalFinancePlan != null;
        }

        public virtual bool IsPendingReconfiguration()
        {
            return this.IsReconfiguration() &&
                   this.IsNotOriginated();
        }

        public virtual bool HasPendingReconfiguration()
        {
            return this.ReconfiguredFinancePlan != null &&
                   this.ReconfiguredFinancePlan.IsNotOriginated();
        }

        #endregion

        #region status helpers

        public virtual bool IsActive()
        {
            return this.IsActiveOriginated() ||
                   this.IsNotOriginated() ||
                   this.FinancePlanStatus.IsUncollectableActive();
        }

        public virtual bool IsNotOriginated()
        {
            return this.FinancePlanStatus.IsNotOriginated();
        }

        public virtual bool IsActiveOriginated()
        {
            return this.FinancePlanStatus.IsActiveOriginated();
        }

        public virtual bool IsCanceled()
        {
            return this.FinancePlanStatus.FinancePlanStatusEnum == FinancePlanStatusEnum.Canceled;
        }

        public virtual bool IsClosed()
        {
            return this.FinancePlanStatus.IsClosed();
        }

        public virtual bool IsActiveUncollectable()
        {
            return this.FinancePlanStatus.IsActiveUncollectable();
        }

        public virtual bool IsClosedUncollectable()
        {
            return this.FinancePlanStatus.IsClosedUncollectable();
        }

        public virtual bool IsPastDue()
        {
            return this.FinancePlanStatus.IsPastDue();
        }

        public virtual bool IsPendingAcceptance()
        {
            return this.FinancePlanStatus.IsPendingAcceptance();
        }

        public virtual bool IsPending()
        {
            return this.FinancePlanStatus.IsPending();
        }

        public virtual bool IsUncollectable()
        {
            return this.FinancePlanStatus.IsUncollectable();
        }

        public virtual FinancePlanStatus FinancePlanStatusAsOf(DateTime endDate)
        {
            FinancePlanStatusHistory lastForThatTime = this.FinancePlanStatusHistory
                .OrderByDescending(x => x.InsertDate)
                .ThenByDescending(x => x.FinancePlanStatusHistoryId)
                .FirstOrDefault(x => x.InsertDate <= endDate);

            return lastForThatTime != null ? lastForThatTime.FinancePlanStatus : new FinancePlanStatus();
        }

        public virtual bool WasActiveBetween(DateTime periodStartDate, DateTime periodEndDate)
        {
            if (this.FinancePlanStatusHistory == null)
            {
                return false;
            }

            //Need to look at all of the visitStates for that time period.
            List<FinancePlanStatusHistory> eligibleStatusHistories = this.FinancePlanStatusHistory.Where(x => x.InsertDate >= periodStartDate && x.InsertDate <= periodEndDate).ToList();

            //also take the last status from before periodStartDate.
            FinancePlanStatusHistory lastStatusBeforeStatement = this.FinancePlanStatusHistory.OrderByDescending(x => x.InsertDate).ThenByDescending(x => x.FinancePlanStatusHistoryId).FirstOrDefault(x => x.InsertDate < periodStartDate);
            if (lastStatusBeforeStatement != null)
            {
                eligibleStatusHistories.Add(lastStatusBeforeStatement);
            }

            if (eligibleStatusHistories.Count > 0)
            {
                //If there were any statuses in that time period use this logic.
                return eligibleStatusHistories.Any(x => x.FinancePlanStatus.IsActiveOriginated());
            }

            return false;
        }

        // VPNG-19800 CurrentFinancePlanStatus not matching with latest status in FinancePlanStatusHistory 
        // Reverting to original implementation where we rely on the FinancePlanStatusHistory collection
        // to provide the most updated FinancePlanStatusHistory 
        public virtual FinancePlanStatus FinancePlanStatus
        {
            get
            {
                FinancePlanStatusHistory lastForThatTime = this.FinancePlanStatusHistory
                    .OrderByDescending(x => x.InsertDate)
                    .ThenByDescending(x => x.FinancePlanStatusHistoryId)
                    .FirstOrDefault();

                return lastForThatTime != null ? lastForThatTime.FinancePlanStatus : new FinancePlanStatus();
            }
            set => this.SetFinancePlanStatus(value, "");
        }

        public virtual void SetFinancePlanStatus(FinancePlanStatus financePlanStatus, string reason)
        {
            FinancePlanStatus oldStatus = this.FinancePlanStatus;

            if (financePlanStatus.FinancePlanStatusEnum.IsInCategory(FinancePlanStatusEnumCategory.Closed))
            {
                this.CurrentBucketOverride = null;
            }

            this.FinancePlanStatusHistory.Add(new FinancePlanStatusHistory
            {
                ChangeDescription = reason,
                FinancePlanStatus = financePlanStatus,
                InsertDate = DateTime.UtcNow, //This will be overridden, just making sure it orders when not persisted.
                FinancePlan = this
            });

            this.AddFinancePlanStatusChange(oldStatus);

            if (oldStatus.FinancePlanStatusId != financePlanStatus.FinancePlanStatusId)
            {
                this.AddFinancePlanOutboundVisitStatusChange();
            }
        }

        public virtual bool ShouldBeInCurrent()
        {
            //All past due payment buckets are current.
            return this.Buckets != null && !this.Buckets.ContainsKey(1);
        }

        public virtual bool CanGoUncollectable(DateTime checkDate)
        {
            FinancePlanStatusHistory currentStatusHistory = this.FinancePlanStatusHistory
                .OrderByDescending(x => x.InsertDate)
                .ThenByDescending(x => x.FinancePlanStatusHistoryId)
                .FirstOrDefault();

            if (currentStatusHistory == null)
            {
                return false;
            }

            if (currentStatusHistory.FinancePlanStatus.FinancePlanStatusEnum != FinancePlanStatusEnum.UncollectableActive)
            {
                return false;
            }

            return currentStatusHistory.InsertDate <= checkDate;
        }

        private void AddFinancePlanStatusChange(FinancePlanStatus oldStatus)
        {
            this.UnpublishedFinancePlanChangedStatusMessages.Add(new FinancePlanChangedStatusMessage
            {
                FinancePlanId = this.FinancePlanId,
                NewStatus = this.FinancePlanStatus.FinancePlanStatusEnum,
                PreviousStatus = oldStatus.FinancePlanStatusEnum,
                EventDate = DateTime.UtcNow
            });
        }

        private void AddFinancePlanOutboundVisitStatusChange()
        {
            foreach (FinancePlanVisit fpv in this.FinancePlanVisits)
            {
                if (fpv.CurrentVisitState == VisitStateEnum.Inactive)
                {
                    continue;
                }
                this.OutboundVisitMessages.Add(new OutboundVisitMessage
                {
                    VpVisitId = fpv.VisitId,
                    ActionDate = DateTime.UtcNow,
                    BillingSystemId = fpv.BillingSystemId,
                    SourceSystemKey = fpv.SourceSystemKey,
                    VpOutboundVisitMessageType = VpOutboundVisitMessageTypeEnum.FinancePlanVisitStatusChanged,
                    OutboundValue = this.FinancePlanStatus.FinancePlanStatusEnum.ToString(),
                    IsCallCenter = this.VpGuarantor.IsOfflineGuarantor(),
                    IsAutoPay = this.VpGuarantor.UseAutoPay
                });
            }
        }

        #endregion

        #region visit helpers

        public virtual IList<FinancePlanVisit> ActiveFinancePlanVisits
        {
            get { return this.FinancePlanVisits.Where(x => x.HardRemoveDate == null).ToList(); }
        }

        public virtual IList<IFinancePlanVisitBalance> ActiveFinancePlanVisitBalances => this.ActiveFinancePlanVisits.Cast<IFinancePlanVisitBalance>().ToList();

        public virtual void AddFinancePlanVisits(FinancePlanCalculationParameters parameters, FinancePlan parentFinancePlan = null)
        {
            DateTime now = DateTime.UtcNow;

            foreach (IFinancePlanVisitBalance setupVisit in parameters.Visits)
            {
                if (this.FinancePlanVisits.Any(x => x.VisitId == setupVisit.VisitId))
                {
                    //This allows the AddVinancePlanVisits to be called multiple times, but be warned it will not update visits and visits should be generated with an ID
                    continue;
                }

                FinancePlanVisit financePlanVisit = new FinancePlanVisit
                {
                    VpGuarantorId = parameters.Guarantor.VpGuarantorId,
                    FinancePlan = this,
                    OriginatedSnapshotVisitBalance = null,
                    OriginatedSnapshotInterestDeferred = false,
                    InsertDate = now,
                    RemovedSnapshotVisitBalance = null,
                    VisitId = setupVisit.VisitId,
                    SourceSystemKey = setupVisit.SourceSystemKey,
                    BillingSystemId = setupVisit.BillingSystemId,
                    // visit projections
                    CurrentVisitBalance = setupVisit.CurrentVisitBalance,
                    CurrentVisitState = setupVisit.CurrentVisitState,
                    InterestDue = setupVisit.InterestDue,
                    UnclearedPaymentsSum = setupVisit.UnclearedPaymentsSum
                };

                decimal visitBalance = setupVisit.CalculateBalance(setupVisit.CurrentVisitBalance, setupVisit.UnclearedPaymentsSum, 0);
                FinancePlanVisitPrincipalAmount financePlanVisitPrincipalAmount = new FinancePlanVisitPrincipalAmount
                {
                    FinancePlan = this,
                    FinancePlanVisit = financePlanVisit,
                    Amount = visitBalance,
                    FinancePlanVisitPrincipalAmountType = FinancePlanVisitPrincipalAmountTypeEnum.FinancePlanCreated,
                    InsertDate = now
                };

                financePlanVisit.FinancePlanVisitPrincipalAmounts.Add(financePlanVisitPrincipalAmount);

                financePlanVisit.FinancePlanVisitInterestRateHistories.Add(new FinancePlanVisitInterestRateHistory
                {
                    FinancePlanVisit = financePlanVisit,
                    InsertDate = now,
                    InterestRate = this.CurrentInterestRate,
                    OverrideInterestRate = setupVisit.OverrideInterestRate,
                    OverridesRemaining = setupVisit.OverridesRemaining
                });

                this.FinancePlanVisits.Add(financePlanVisit);
            }
        }
        
        public virtual void AddExistingInterest(int visitId, decimal interestDue)
        {
            FinancePlanVisit financePlanVisit = this.FinancePlanVisits.FirstOrDefault(x => x.VisitId == visitId);
            if (financePlanVisit == null)
            {
                return;
            }
            
            DateTime now = DateTime.UtcNow;

            if (interestDue <= 0m)
            {
                return;
            }

            FinancePlanVisitInterestDue financePlanVisitInterestDue = new FinancePlanVisitInterestDue
            {
                FinancePlanVisit = financePlanVisit,
                FinancePlan = this,
                FinancePlanVisitInterestDueType = FinancePlanVisitInterestDueTypeEnum.InterestAssessment,
                InsertDate = now,
                InterestDue = interestDue,
                OverrideInsertDate = this.CreatedVpStatement.PeriodEndDate.AddMilliseconds(-10),
                VpStatementId = this.CreatedVpStatement.VpStatementId
            };

            financePlanVisit.FinancePlanVisitInterestDues.Add(financePlanVisitInterestDue);
            this._financePlanVisitInterestDueItems.Add(financePlanVisitInterestDue);
        }

        public virtual bool HasActiveVisits()
        {
            return this.ActiveFinancePlanVisits
                .Any(x => x.CurrentVisitState == VisitStateEnum.Active);
        }

        public virtual bool HasActiveFinancePlanVisitForVisitId(int id)
        {
            return this.ActiveFinancePlanVisits
                .Where(x => x.VisitId == id)
                .IsNotNullOrEmpty();
        }

        public virtual bool HasFinancePlanVisitForVisitId(int id)
        {
            return this.FinancePlanVisits
                .Where(x => x.HardRemoveDate == null)
                .Where(x => x.VisitId == id)
                .IsNotNullOrEmpty();
        }

        #endregion
        
        #region amounts due / buckets
       
        public virtual void SetTheAmountDueForFinancePlan(VpStatement currentStatement, string comment = null)
        {
            if (!this.HasAmountDueForStatement(currentStatement.VpStatementId))
            {
                this.AddFinancePlanAmountsDueStatement(this.PaymentAmount, currentStatement.PaymentDueDate, currentStatement.VpStatementId, comment);
            }
            else if (this.HasAmountDueForStatement(currentStatement.VpStatementId) && this.IsPending())
            {
                decimal amount = this.FinancePlanAmountDueForStatement(currentStatement.VpStatementId);

                //Reverse
                if (amount != 0m)
                {
                    this.AddFinancePlanAmountsDueStatement(-amount, currentStatement.PaymentDueDate, currentStatement.VpStatementId, comment);
                }

                //ReAdd
                this.AddFinancePlanAmountsDueStatement(this.PaymentAmount, currentStatement.PaymentDueDate, currentStatement.VpStatementId, comment);
            }
        }
        
        public virtual void AddFinancePlanAmountsDueStatement(decimal amountDue, DateTime dueDate, int vpStatementId, string comment = null, int? actionVisitPayUserId = null)
        {
            decimal adjustedAmountDue = this.GetNextFinancePlanAmountDue(amountDue);
            FinancePlanAmountDue financePlanAmountDue = new FinancePlanAmountDue
            {
                FinancePlan = this,
                AmountDue = adjustedAmountDue,
                DueDate = dueDate,
                VpStatementId = vpStatementId
            };
            
            this.FinancePlanAmountsDue.Add(financePlanAmountDue);
            this.AddFinancePlanBucketHistory(comment, actionVisitPayUserId);
        }

        public virtual decimal GetNextFinancePlanAmountDue(decimal amountDue)
        {
            decimal sumAmountsDue = this.FinancePlanAmountsDue.Sum(x => x.AmountDue);
            decimal remainingAmountDue = this.CurrentFinancePlanBalance - sumAmountsDue;
            decimal nextAmountDue = Math.Min(remainingAmountDue, amountDue);
            return nextAmountDue;
        }

        public virtual void AddFinancePlanAmountsDueFailedPayment(DateTime dueDate, int paymentId, string comment = null, int? actionVisitPayUserId = null)
        {
            FinancePlanAmountDue financePlanAmountDue = new FinancePlanAmountDue()
            {
                FinancePlan = this,
                AmountDue = 0m,
                DueDate = dueDate,
                PaymentId = paymentId,
            };

            this.FinancePlanAmountsDue.Add(financePlanAmountDue);
            this.AddFinancePlanBucketHistory(comment, actionVisitPayUserId);
        }

        /// <summary>
        /// Updates any ledgers, such as FinancePlanAmountDue, FinancePlanVisitInterestDue, FinancePlanVisitPrincipalAmount
        /// for a payment which reallocates interest to principal
        /// </summary>
        public virtual void OnInterestReallocationPayment(decimal amountDue, DateTime dueDate, Payment payment, string comment = null, int? actionVisitPayUserId = null)
        {
            foreach (PaymentAllocation paymentAllocation in payment.PaymentAllocations)
            {
                this.OnPaymentAllocation(amountDue, dueDate, paymentAllocation, FinancePlanVisitInterestDueTypeEnum.InterestReallocation, comment, actionVisitPayUserId);
                this.OnInterestReallocationPaymentAllocation(paymentAllocation);
            }
        }

        /// <summary>
        /// Updates any ledgers, such as FinancePlanAmountDue, FinancePlanVisitInterestDue, FinancePlanVisitPrincipalAmount
        /// for a payment 
        /// </summary>
        public virtual void OnPayment(decimal amountDue, DateTime dueDate, Payment payment, string comment = null, int? actionVisitPayUserId = null)
        {
            foreach (PaymentAllocation paymentAllocation in payment.PaymentAllocations)
            {
                this.OnPaymentAllocation(amountDue, dueDate,paymentAllocation,comment,actionVisitPayUserId);
            }
        }

        /// <summary>
        /// Updates any ledgers, such as FinancePlanAmountDue, FinancePlanVisitInterestDue, FinancePlanVisitPrincipalAmount
        /// for a payment allocation
        /// </summary>
        public virtual void OnPaymentAllocation(decimal amountDue, DateTime dueDate, PaymentAllocation paymentAllocation, string comment = null, int? actionVisitPayUserId = null)
        {
            this.OnPaymentAllocation(amountDue, dueDate, paymentAllocation, FinancePlanVisitInterestDueTypeEnum.InterestPayment, comment, actionVisitPayUserId);
        }

        private void OnPaymentAllocation(decimal amountDue, DateTime dueDate, PaymentAllocation paymentAllocation, FinancePlanVisitInterestDueTypeEnum interestDueType, string comment = null, int? actionVisitPayUserId = null)
        {
            Payment payment = paymentAllocation.Payment;
            decimal amountDueRemaining = amountDue;

            FinancePlanVisit financePlanVisit = this.FinancePlanVisits.FirstOrDefault(x => x.VisitId == paymentAllocation.PaymentVisit?.VisitId);
            if (financePlanVisit == null)
            {
                return;
            }

            //Closed-end
            this.AddFinancePlanVisitPrincipalAmountPayment(financePlanVisit, paymentAllocation);

            List<KeyValuePair<int, Tuple<decimal, int?>>> orderedBuckets = this.Buckets.Where(x => x.Value.Item2 != 0).OrderByDescending(x => x.Key).ToList();
            if (orderedBuckets.Count == 0)
            {
                orderedBuckets.Add(new KeyValuePair<int, Tuple<decimal, int?>>(0, new Tuple<decimal, int?>(0, null)));
            }

            List<decimal> amountsForEachBucket = new List<decimal>();
            foreach (KeyValuePair<int, Tuple<decimal, int?>> bucket in orderedBuckets)
            {
                // Safety mechanism to ensure that we do not pay more than the amount due.
                decimal actualAmount = Math.Max(amountDueRemaining, decimal.Negate(bucket.Value.Item1));
                // only a RecurringPayment can pay off Bucket 0
                if (bucket.Key == 0 && !payment.AllowToPayBucketZero())
                {
                    actualAmount = 0m;
                }
                amountsForEachBucket.Add(actualAmount);

                amountDueRemaining -= actualAmount;
                if (amountDueRemaining == 0)
                {
                    break;
                }
            }

            if (amountsForEachBucket.Any())
            {
                // A payment allocation can relieve multiple buckets
                // We want a single record to represent the total amount relieved with the allocation
                decimal sumAmountDueForAllBuckets = amountsForEachBucket.Sum();
                FinancePlanAmountDue financePlanAmountDue = new FinancePlanAmountDue()
                {
                    FinancePlan = this,
                    AmountDue = sumAmountDueForAllBuckets,
                    PaymentAllocationId = paymentAllocation.PaymentAllocationId,
                    PaymentId = payment.PaymentId,
                    InsertDate = DateTime.UtcNow,
                    //OverrideInsertDate = 
                    FinancePlanVisit = financePlanVisit,
                    PaymentAllocationType = paymentAllocation.PaymentAllocationType,
                    PaymentAllocationActualAmount = paymentAllocation.ActualAmount
                };
                this.FinancePlanAmountsDue.Add(financePlanAmountDue);
            }

            if (paymentAllocation.PaymentAllocationType == PaymentAllocationTypeEnum.VisitInterest)
            {
                this.AddFinancePlanVisitInterestDuePaymentAllocation(paymentAllocation, interestDueType);
            }
        }

        private void OnInterestReallocationPaymentAllocation(PaymentAllocation paymentAllocation)
        {
            if (paymentAllocation.PaymentAllocationType == PaymentAllocationTypeEnum.VisitInterest)
            {
                FinancePlanVisit found = this.FinancePlanVisits.FirstOrDefault(x => x.VisitId == paymentAllocation.PaymentVisit?.VisitId);
                found?.AddFinancePlanVisitInterestWriteOff(paymentAllocation.ActualAmount);
            }

            if (paymentAllocation.PaymentAllocationType == PaymentAllocationTypeEnum.VisitPrincipal)
            {
                FinancePlanVisit found = this.FinancePlanVisits.FirstOrDefault(x => x.VisitId == paymentAllocation.PaymentVisit.VisitId);
                found?.AddFinancePlanVisitInterestDue(0m, paymentAllocation.PaymentAllocationId, FinancePlanVisitInterestDueTypeEnum.PrincipalReallocation);
            }
        }

        public virtual void AddFinancePlanVisitInterestDuePaymentAllocation(PaymentAllocation paymentAllocation)
        {
            this.AddFinancePlanVisitInterestDuePaymentAllocation(paymentAllocation, FinancePlanVisitInterestDueTypeEnum.InterestPayment);
        }

        private void AddFinancePlanVisitInterestDuePaymentAllocation(PaymentAllocation paymentAllocation, FinancePlanVisitInterestDueTypeEnum interestDueType)
        {
            if (paymentAllocation.PaymentAllocationType != PaymentAllocationTypeEnum.VisitInterest)
            {
                return;
            }

            FinancePlanVisit financePlanVisit = this.FinancePlanVisits.FirstOrDefault(x => x.VisitId == paymentAllocation.PaymentVisit?.VisitId);

            if (interestDueType == FinancePlanVisitInterestDueTypeEnum.InterestPayment)
            {
                financePlanVisit?.AddFinancePlanVisitInterestPayment(decimal.Negate(paymentAllocation.ActualAmount), paymentAllocation.PaymentAllocationId);
            }
            else
            {
                financePlanVisit?.AddFinancePlanVisitInterestDue(decimal.Negate(paymentAllocation.ActualAmount), paymentAllocation.PaymentAllocationId, interestDueType);
            }
        }

        public virtual void AddFinancePlanVisitInterestDuePaymentAllocation(decimal interestAmount, int paymentAllocationId, int financePlanVisitId)
        {
            FinancePlanVisit financePlanVisit = this.FinancePlanVisits.FirstOrDefault(x => x.FinancePlanVisitId == financePlanVisitId);
            financePlanVisit?.AddFinancePlanVisitInterestPayment(interestAmount, paymentAllocationId);
        }

        public virtual void OnFinancePlanClose(string reason = null, int? actionVisitPayUserId = null)
        {
            this.CurrentBucketOverride = null;
            this.AddFinancePlanAmountsDueCloseFinancePlan(reason, actionVisitPayUserId);
            this.AddFinancePlanVisitPrincipalAmountClosed();
        }

        public virtual void AddFinancePlanAmountsDueCloseFinancePlan(string reason = null, int? actionVisitPayUserId = null)
        {
            this.AddFinancePlanAmountsDueReduceAmount(this.AmountDue, reason, actionVisitPayUserId);
        }

        public virtual void AddFinancePlanAmountsDueConsolidatation(bool isDeconsolidation)
        {
            this.AddFinancePlanAmountsDueReduceAmount(this.PastDueAmount, isDeconsolidation ? "Deconsolidation" : "Consolidation");
        }

        protected virtual void AddFinancePlanAmountsDueReduceAmount(decimal amountToReduce, string reason, int? actionVisitPayUserId = null)
        {
            amountToReduce = decimal.Negate(amountToReduce);
            if (amountToReduce != 0 && this.Buckets.Any())
            {
                decimal amountDueRemaining = amountToReduce;
                foreach (KeyValuePair<int, Tuple<decimal, int?>> bucket in this.Buckets.Where(x => x.Value.Item2 != 0).OrderByDescending(x => x.Key))
                {
                    decimal actualAmount = Math.Max(amountDueRemaining, decimal.Negate(bucket.Value.Item1));// Safety mechanism to ensure that we do not pay more than the amount due.
                    amountDueRemaining -= actualAmount;

                    FinancePlanAmountDue financePlanAmountDue = new FinancePlanAmountDue()
                    {
                        FinancePlan = this,
                        AmountDue = actualAmount,
                        DueDate = DateTime.UtcNow,
                        VpStatementId = bucket.Value.Item2
                    };

                    this.FinancePlanAmountsDue.Add(financePlanAmountDue);

                    if (amountDueRemaining == 0)
                    {
                        break;
                    }
                }
            }

            this.AddFinancePlanBucketHistory(reason, actionVisitPayUserId);
        }

        public virtual void AddFinancePlanVisitPrincipalAmountClosed()
        {
            foreach (FinancePlanVisit financePlanVisit in this.ActiveFinancePlanVisits)
            {
                if (financePlanVisit.PrincipalBalance > 0)
                {
                    decimal closingOffsetAmount = 0 - financePlanVisit.PrincipalBalance;
                    financePlanVisit.FinancePlanVisitPrincipalAmounts.Add(new FinancePlanVisitPrincipalAmount
                    {
                        FinancePlan = this,
                        Amount = closingOffsetAmount,
                        FinancePlanVisit = financePlanVisit,
                        FinancePlanVisitPrincipalAmountType = FinancePlanVisitPrincipalAmountTypeEnum.FinancePlanClosed
                    });
                }
            }

        }

        /// <summary>
        /// When a payment is made outside of visitpay the ledger needs to be adjusted in order to prevent a roborefund
        /// transactionAmount is expected to be a negative amount
        /// </summary>
        public virtual void AddFinancePlanVisitPrincipalAmountExternalPayment(FinancePlanVisit financePlanVisit,decimal transactionAmount, int visitTransactionId  )
        {
            //expecting a debit. Reversals/Voids handled elsewhere
            if (transactionAmount >= 0)
            {
                return;
            }
            
            financePlanVisit.FinancePlanVisitPrincipalAmounts.Add(new FinancePlanVisitPrincipalAmount
            {
                FinancePlan = this,
                Amount = transactionAmount,
                FinancePlanVisit = financePlanVisit,
                VisitTransactionId = visitTransactionId,
                FinancePlanVisitPrincipalAmountType = FinancePlanVisitPrincipalAmountTypeEnum.Payment
            });
            this.OnAddFinancePlanVisitPrincipalAmountPayment(financePlanVisit, transactionAmount, null, visitTransactionId);
        }

        public virtual void AddFinancePlanVisitPrincipalAmountPayment(FinancePlanVisit financePlanVisit, PaymentAllocation paymentAllocation)
        {
            //Closed-end
            if (paymentAllocation.PaymentAllocationType == PaymentAllocationTypeEnum.VisitPrincipal)
            {
                decimal amount = decimal.Negate(paymentAllocation.ActualAmount);
                financePlanVisit.FinancePlanVisitPrincipalAmounts.Add(new FinancePlanVisitPrincipalAmount
                {
                    FinancePlan = this,
                    Amount = amount,
                    FinancePlanVisit = financePlanVisit,
                    PaymentAllocationId = paymentAllocation.PaymentAllocationId,
                    FinancePlanVisitPrincipalAmountType = FinancePlanVisitPrincipalAmountTypeEnum.Payment
                });
                this.OnAddFinancePlanVisitPrincipalAmountPayment(financePlanVisit,amount, paymentAllocation.PaymentAllocationId,null);
            }
        }

        protected virtual void OnAddFinancePlanVisitPrincipalAmountPayment(
            FinancePlanVisit financePlanVisit, 
            decimal amount, 
            int? paymentAllocationId, 
            int? visitTransactionId
        )
        {
            if (financePlanVisit.FinancePlan != null && financePlanVisit.FinancePlan.IsClosed())
            {
                //Dont reverse too much
                decimal amountToReverse = amount;
                decimal sumOfClosed = financePlanVisit.FinancePlanVisitPrincipalAmounts.Where(x => x.FinancePlanVisitPrincipalAmountType == FinancePlanVisitPrincipalAmountTypeEnum.FinancePlanClosed).Sum(y => y.Amount);

                if (sumOfClosed + amountToReverse > 0)
                {
                    amountToReverse = decimal.Negate(sumOfClosed);
                }

                financePlanVisit.FinancePlanVisitPrincipalAmounts.Add(new FinancePlanVisitPrincipalAmount
                {
                    FinancePlan = this,
                    Amount = amountToReverse,
                    FinancePlanVisit = financePlanVisit,
                    PaymentAllocationId = paymentAllocationId,
                    VisitTransactionId = visitTransactionId,
                    FinancePlanVisitPrincipalAmountType = FinancePlanVisitPrincipalAmountTypeEnum.FinancePlanClosed
                });
            }
        }

        public virtual void AddFinancePlanVisitPrincipalAmountAdjustments()
        {
            this.AddFinancePlanVisitPrincipalAmountAdjustments(this.ActiveFinancePlanVisits);
        }

        public virtual void AddFinancePlanVisitPrincipalAmountAdjustments(FinancePlanVisit financePlanVisit)
        {
            if (this.FinancePlanVisits.Any(x => x.FinancePlanVisitId == financePlanVisit.FinancePlanVisitId))
            {
                this.AddFinancePlanVisitPrincipalAmountAdjustments(financePlanVisit.ToListOfOne());
            }
        }

        private void AddFinancePlanVisitPrincipalAmountAdjustments(IList<FinancePlanVisit> financePlanVisits)
        {
            foreach (FinancePlanVisit financePlanVisit in financePlanVisits)
            {
                FinancePlanVisitPrincipalAmount firstPrincipalAmount = financePlanVisit.FinancePlanVisitPrincipalAmounts.FirstOrDefault(x => x.FinancePlanVisitPrincipalAmountType == FinancePlanVisitPrincipalAmountTypeEnum.FinancePlanCreated);
                bool shouldAddAdjustment = financePlanVisit.VisitBalanceDifference < 0;
                if (shouldAddAdjustment)
                {
                    financePlanVisit.FinancePlanVisitPrincipalAmounts.Add(new FinancePlanVisitPrincipalAmount
                    {
                        FinancePlan = financePlanVisit.FinancePlan,
                        Amount = financePlanVisit.VisitBalanceDifference,
                        FinancePlanVisit = financePlanVisit,
                        FinancePlanVisitPrincipalAmountType = FinancePlanVisitPrincipalAmountTypeEnum.Adjustment,
                        OverrideInsertDate = firstPrincipalAmount?.InsertDate ?? this.OriginationDateTime()
                    });
                }
            }
        }

        public virtual void AddFinancePlanAmountsDueOffset(decimal amountDue, DateTime? dueDate, int? vpStatementId, string comment = null, int? actionVisitPayUserId = null)
        {
            FinancePlanAmountDue financePlanAmountDue = new FinancePlanAmountDue()
            {
                FinancePlan = this,
                AmountDue = amountDue,
                DueDate = dueDate,
                VpStatementId = vpStatementId
            };

            this.FinancePlanAmountsDue.Add(financePlanAmountDue);
            this.AddFinancePlanBucketHistory(comment, actionVisitPayUserId);
        }

        public virtual decimal AddFinancePlanAmountsDueCancelPayment(int? actionVisitPayUserId = null)
        {
            if (!this.Buckets.Any())
            {
                return 0m;
            }

            Tuple<decimal, int?> maxBucket = this.Buckets.OrderBy(x => x.Key).Last().Value;
            decimal maxBucketAmountDue = maxBucket.Item1;

            this.AddFinancePlanAmountsDueOffset(decimal.Negate(maxBucketAmountDue), null, maxBucket.Item2, "Payment Cancelled", actionVisitPayUserId);

            return maxBucketAmountDue;
        }

        public virtual bool AddFinancePlanAmountsDueReschedulePayment(int vpStatementId, DateTime rescheduleDate, int? actionVisitPayUserId = null)
        {
            if (!this.Buckets.Any())
            {
                return false;
            }

            List<FinancePlanAmountDue> amountDuesForStatement = this.FinancePlanAmountsDue.Where(x => x.VpStatementId.HasValue && x.VpStatementId == vpStatementId).ToList();
            if (amountDuesForStatement.IsNullOrEmpty())
            {
                return false;
            }

            foreach (FinancePlanAmountDue amountDue in amountDuesForStatement)
            {
                amountDue.OverrideDueDate = rescheduleDate;
            }

            this.AddFinancePlanBucketHistory("Payment Rescheduled", actionVisitPayUserId);

            return true;
        }

        /// <summary>
        /// Updates any ledgers, such as FinancePlanAmountDue, FinancePlanVisitInterestDue, FinancePlanVisitPrincipalAmount
        /// for a payment allocation reversal
        /// </summary>
        public virtual void OnPaymentReversalAllocation(decimal amountDue, decimal actualPaymentAmount, int paymentAlloctionId,int? paymentId, int financePlanVisitId, PaymentAllocationTypeEnum paymentAllocationType, int? actionVisitPayUserId = null)
        {
            FinancePlanVisit financePlanVisit = this.FinancePlanVisits.FirstOrDefault(x => x.FinancePlanVisitId == financePlanVisitId);
            if (financePlanVisit == null)
            {
                return;
            }

            FinancePlanAmountDue financePlanAmountDue = new FinancePlanAmountDue
            {
                FinancePlan = this,
                AmountDue = amountDue,
                PaymentAllocationId = paymentAlloctionId,
                InsertDate = DateTime.UtcNow,
                FinancePlanVisit = financePlanVisit,
                PaymentId = paymentId
            };

            this.FinancePlanAmountsDue.Add(financePlanAmountDue);

            if (paymentAllocationType == PaymentAllocationTypeEnum.VisitPrincipal)
            {
                financePlanVisit.FinancePlanVisitPrincipalAmounts.Add(new FinancePlanVisitPrincipalAmount
                {
                    FinancePlan = this,
                    Amount = decimal.Negate(actualPaymentAmount), // reversal allocation is negative, we want to increase the principal
                    FinancePlanVisit = financePlanVisit,
                    PaymentAllocationId = paymentAlloctionId,
                    FinancePlanVisitPrincipalAmountType = FinancePlanVisitPrincipalAmountTypeEnum.Payment
                });
            }
            
            if (paymentAllocationType == PaymentAllocationTypeEnum.VisitInterest)
            {
                this.AddFinancePlanVisitInterestDuePaymentAllocation(amountDue, paymentAlloctionId, financePlanVisitId);
            }
            this.AddFinancePlanBucketHistory("Payment Reversed", actionVisitPayUserId);
        }

        public virtual bool HasAmountDueForStatement(int currentStatementVpStatementId)
        {
            return this.FinancePlanAmountsDue.Any(x => x.VpStatementId == currentStatementVpStatementId && !x.PaymentId.HasValue);
        }

        public virtual IDictionary<int, Tuple<decimal, int?>> Buckets => this.GetBuckets();
        
        private IDictionary<int, Tuple<decimal, int?>> GetBuckets(DateTime? asOfDate = null)
        {
            IList<FinancePlanAmountDue> localAmountDue = this.FinancePlanAmountsDue;
            if (asOfDate.HasValue)
            {
                localAmountDue = localAmountDue.Where(y => asOfDate.Value >= y.InsertDate).ToList();
            }
            List<Bucket> vpstatements = localAmountDue.Where(x => x.VpStatementId != null)
                .Where(x => x.PaymentAllocationId == null)
                .GroupBy(x => x.VpStatementId)
                .Select(x => new Bucket
                {
                    AmountDue = x.Sum(s => s.AmountDue),
                    VpStatementId = x.Key, // VpStatementId
                    DueDate = x.OrderByDescending(s => s.InsertDate).Last().DueDate
                })
                .ToList();

            decimal allPayments = localAmountDue.Where(x => x.VpStatementId == null).Sum(x => x.AmountDue);
            BucketList bucketList = new BucketList(vpstatements);
            bucketList.RelieveAmountDueForEachBucket(allPayments);

            // convert to dictionary to placate existing code
            Dictionary<int, Tuple<decimal, int?>> buckets = bucketList.ToDictionary(asOfDate);

            this.AddOverrideBucket(buckets);
            this.BucketList = bucketList.Buckets;
            
            return buckets;
        }

        public virtual List<Bucket> BucketList { get; set; }

        public virtual decimal FinancePlanAmountDueForStatement(int vpStatementId)
        {
            return this.FinancePlanAmountsDue.Where(x => x.VpStatementId == vpStatementId && !x.PaymentId.HasValue).Sum(x => x.AmountDue);
        }
        
        public virtual decimal AmountDue
        {
            get { return this.FinancePlanAmountsDue.Sum(x => x.AmountDue); }
        }

        public virtual decimal AmountDueAsOf(DateTime date)
        {
            return Math.Max(this.FinancePlanAmountsDue.Where(x => (x.DueDate.HasValue && x.DueDate <= date) || !x.DueDate.HasValue).Sum(x => x.AmountDue), 0);

        }
        
        public virtual decimal CurrentAmount => this.Buckets.ContainsKey(0) ? this.Buckets[0].Item1 : 0;

        public virtual decimal PastDueAmount
        {
            get
            {
                decimal pastDueAmt = this.Buckets.Any(x => x.Key > 0) ? this.Buckets.Where(x => x.Key > 0).Sum(x => x.Value.Item1) : 0;
                return Math.Min(this.CurrentFinancePlanBalance, pastDueAmt);
            }
        }

        public virtual decimal PastDueAmountAsOf(DateTime date)
        {
            IDictionary<int, Tuple<decimal, int?>> bucketsAsOf = this.GetBuckets(date);
            return bucketsAsOf.Any(x => x.Key > 0) ? bucketsAsOf.Where(x => x.Key > 0).Sum(x => x.Value.Item1) : 0;
        }

        public virtual decimal? AdjustmentsSinceOrigination
        {
            get
            {
                decimal principalAdjustments = this.FinancePlanVisits
                    .Sum(x => x.FinancePlanVisitPrincipalAmounts
                        .Where(y => y.FinancePlanVisitPrincipalAmountType == FinancePlanVisitPrincipalAmountTypeEnum.Adjustment
                                    || y.FinancePlanVisitPrincipalAmountType == FinancePlanVisitPrincipalAmountTypeEnum.FinancePlanClosed
                                    )
                        .Sum(y => y.Amount));
                return principalAdjustments;
            }
        }
        
        public virtual void AddFinancePlanBucketHistory(string comment = null, int? actionVisitPayUserId = null)
        {
            FinancePlanBucketHistory lastHistory = this.FinancePlanBucketHistories.OrderByDescending(x => x.InsertDate).ThenByDescending(x => x.FinancePlanBucketHistoryId).FirstOrDefault();
            int currentBucket = this.CurrentBucket;
            bool bucketValueChanged = lastHistory == null || lastHistory.Bucket != currentBucket;

            if (bucketValueChanged || (lastHistory.Bucket == currentBucket && lastHistory.ChangeDescription != comment))
            {
                this.FinancePlanBucketHistories.Add(new FinancePlanBucketHistory
                {
                    Bucket = currentBucket,
                    ChangeDescription = comment,
                    InsertDate = DateTime.UtcNow, // nhibernate Generated.Always
                    FinancePlan = this,
                    VisitPayUser = new VisitPayUser { VisitPayUserId = actionVisitPayUserId ?? SystemUsers.SystemUserId },
                });

                this.UnpublishedFinancePlanAuditEvents.Add(actionVisitPayUserId == null ? AuditEventTypeFinancePlanEnum.ReAging : AuditEventTypeFinancePlanEnum.AgingChangedByClient);

                if (bucketValueChanged)
                {
                    this.AddOutboundVisitMessageFinancePlanVisitBucketChange();
                }
                
            }
        }

        private void AddOutboundVisitMessageFinancePlanVisitBucketChange()
        {
            string currentBucket = this.CurrentBucket.ToString();
            DateTime actionDate = DateTime.UtcNow;

            foreach (FinancePlanVisit fpv in this.ActiveFinancePlanVisits)
            {
                if (fpv.CurrentVisitState == VisitStateEnum.Inactive)
                {
                    continue;
                }
                this.OutboundVisitMessages.Add(new OutboundVisitMessage
                {
                    VpVisitId = fpv.VisitId,
                    ActionDate = actionDate,
                    BillingSystemId = fpv.BillingSystemId,
                    SourceSystemKey = fpv.SourceSystemKey,
                    VpOutboundVisitMessageType = VpOutboundVisitMessageTypeEnum.FinancePlanVisitBucketChanged,
                    OutboundValue = currentBucket,
                    IsCallCenter = this.VpGuarantor.IsOfflineGuarantor(),
                    IsAutoPay = this.VpGuarantor.UseAutoPay
                });
            }
        }

        private void AddOverrideBucket(IDictionary<int, Tuple<decimal, int?>> buckets)
        {
            if (this.CurrentBucketOverride.HasValue && !buckets.ContainsKey(this.CurrentBucketOverride.Value))
            {
                buckets.Add(this.CurrentBucketOverride.Value, new Tuple<decimal, int?>(0, 0));
            }
        }

        public virtual bool HasPastDueBucketAndRecurringPaymentAttempted()
        {
            //multiple buckets, definitely past due
            if (this.CurrentBucket > 1)
            {
                return true;
            }
            //cant be pastdue
            if (this.CurrentBucket < 1)
            {
                return false;
            }

            FinancePlanAmountDue foundPayment = this.FindLastFailedPaymentAmountDue();
            if (foundPayment == null)
            {
                //Didnt find payment, Seems like the recurring payment hasnt been attempted yet.
                return false;
            }
            return true;
        }

        public virtual FinancePlanAmountDue FindLastFailedPaymentAmountDue()
        {
            //Means it's a 1
            //Need to check if recurrent payment failed
            FinancePlanAmountDue lastAmountDueAdded = this.FinancePlanAmountsDue.OrderByDescending(y => y.InsertDate).ThenByDescending(z => z.FinancePlanAmountDueId).FirstOrDefault(x => x.VpStatementId.HasValue && x.AmountDue > 0 && x.DueDate != null /*&& typeof adding due because of statement*/);
            if (lastAmountDueAdded == null)
            {
                //How can it have a bucket and not have an amountDue added?
                return null;
            }

            FinancePlanAmountDue foundPayment = this.FinancePlanAmountsDue
                //Exclude everything that was before the last amount added
                .Where(d => d.FinancePlanAmountDueId > lastAmountDueAdded.FinancePlanAmountDueId || d.FinancePlanAmountDueId == 0 || d.InsertDate > lastAmountDueAdded.InsertDate)
                //Where it was a failed payment
                .FirstOrDefault(x => x.PaymentId != null && x.AmountDue == 0m /*&& was a failed payment type??  Seems like types would be useful here*/);
            return foundPayment;
        }

        public virtual DateTime? MaxScheduledPaymentDate()
        {
            if (this.AmountDue > 0m)
            {
                return this.FinancePlanAmountsDue
                    .Select(x => x.OverrideDueDate ?? x.DueDate)
                    .Where(x => x.HasValue).Select(x => x.Value)
                    .OrderByDescending(x => x)
                    .FirstOrDefault();
            }

            return null;
        }

        public virtual DateTime MaxScheduledPaymentDateOrNextPaymentDueDate(DateTime nextPaymentDueDate)
        {
            DateTime? date = this.MaxScheduledPaymentDate();
            
            if (date.HasValue && date.Value >= DateTime.UtcNow)
            {
                return date.Value;
            }

            return nextPaymentDueDate;
        }

        public virtual bool HasRescheduledPaymentLaterThan(DateTime checkDate)
        {
            if (this.AmountDue > 0m)
            {
                return this.FinancePlanAmountsDue.Any(x => x.OverrideDueDate.HasValue && DateTime.Compare(x.OverrideDueDate.Value.Date, checkDate.Date) > 0);
            }

            return false;
        }

        #endregion
        
        #region publishing

        public virtual FinancePlanPaymentAmountChangedMessage UnpublishedFinancePlanPaymentAmountChangedMessage { get; set; }

        public virtual IList<FinancePlanChangedStatusMessage> UnpublishedFinancePlanChangedStatusMessages
        {
            get => this._unpublishedFinancePlanChangedStatusMessages ?? (this.UnpublishedFinancePlanChangedStatusMessages = new List<FinancePlanChangedStatusMessage>());
            set => this._unpublishedFinancePlanChangedStatusMessages = value;
        }

        public virtual IList<AuditEventTypeFinancePlanEnum> UnpublishedFinancePlanAuditEvents
        {
            get => this._unpublishedFinancePlanAuditEvents ?? (this.UnpublishedFinancePlanAuditEvents = new List<AuditEventTypeFinancePlanEnum>());
            set => this._unpublishedFinancePlanAuditEvents = value;
        }

        public virtual IList<OutboundVisitMessage> OutboundVisitMessages
        {
            get => this._outboundVisitMessages ?? (this.OutboundVisitMessages = new List<OutboundVisitMessage>());
            set => this._outboundVisitMessages = value;
        }

        public virtual IList<FinancePlanVisitInterestDue> FinancePlanVisitInterestDueItems
        {
            get => this._financePlanVisitInterestDueItems ?? (this.FinancePlanVisitInterestDueItems = new List<FinancePlanVisitInterestDue>());
            set => this._financePlanVisitInterestDueItems = value;
        }

        #endregion

        public virtual bool RecalculateOfferOnUpdate(decimal monthlyPaymentAmount, int? financePlanOfferId)
        {
            return this.FinancePlanOffer == null ||
                   this.FinancePlanOffer.FinancePlanOfferId != financePlanOfferId.GetValueOrDefault(0) ||
                   this.PaymentAmount != monthlyPaymentAmount;
        }

        public virtual void SetCreditAgreement(FinancePlanCreditAgreementCollectionDto financePlanCreditAgreementCollectionDto, DateTime termsAgreedDate, int termsAcceptedVisitPayUserId)
        {
            this.AcceptedTermsVisitPayUserId = termsAcceptedVisitPayUserId;
            this.TermsAgreedDate = termsAgreedDate;
            this.TermsCmsVersionId = financePlanCreditAgreementCollectionDto.UserLocale.TermsCmsVersionId;
            
            DateTime now = DateTime.UtcNow;

            this.FinancePlanCreditAgreements.Add(new FinancePlanCreditAgreement
            {
                FinancePlan = this,
                AgreementText = financePlanCreditAgreementCollectionDto.UserLocale.AgreementText ?? string.Empty,
                CmsVersionId = financePlanCreditAgreementCollectionDto.UserLocale.TermsCmsVersionId,
                ContractNumber = financePlanCreditAgreementCollectionDto.UserLocale.ContractNumber.ToString(),
                InsertDate = now,
                IsRetailInstallmentContract = financePlanCreditAgreementCollectionDto.UserLocale.IsRetailInstallmentContract,
                IsUserLocale = true,
                Locale = financePlanCreditAgreementCollectionDto.UserLocale.Locale
            });

            if (financePlanCreditAgreementCollectionDto.ClientLocale != null)
            {
                this.FinancePlanCreditAgreements.Add(new FinancePlanCreditAgreement
                {
                    FinancePlan = this,
                    AgreementText = financePlanCreditAgreementCollectionDto.ClientLocale.AgreementText ?? string.Empty,
                    CmsVersionId = financePlanCreditAgreementCollectionDto.ClientLocale.TermsCmsVersionId,
                    ContractNumber = financePlanCreditAgreementCollectionDto.ClientLocale.ContractNumber.ToString(),
                    InsertDate = now,
                    IsRetailInstallmentContract = financePlanCreditAgreementCollectionDto.ClientLocale.IsRetailInstallmentContract,
                    IsUserLocale = false,
                    Locale = financePlanCreditAgreementCollectionDto.ClientLocale.Locale
                });
            }
        }

        public virtual void GenerateAndSetPublicIdPhi()
        {
            FinancePlanPublicIdPhiValueGenerator financePlanPublicIdPhiValueGenerator = new FinancePlanPublicIdPhiValueGenerator();
            FinancePlanPublicIdPhiSource id = new FinancePlanPublicIdPhiSource(financePlanPublicIdPhiValueGenerator);
            this.FinancePlanPublicIdPhi = id.RawValue.ToString();
        }

        public virtual string GetDisplayFormattedPublicIdPhi()
        {
            const string displayFormat = FinancePlanPublicIdPhiValueGenerator.DisplayFormat;
            return Convert.ToInt64(this.FinancePlanPublicIdPhi).ToString(displayFormat, CultureInfo.InvariantCulture);
        }

        public virtual bool IsRetailInstallmentContract()
        {
            if (this.FinancePlanCreditAgreements.IsNullOrEmpty())
            {
                return false;
            }

            FinancePlanCreditAgreement agreement = this.FinancePlanCreditAgreements.OrderByDescending(x => x.InsertDate).ThenByDescending(x => x.FinancePlanCreditAgreementId).FirstOrDefault();
            return agreement?.IsRetailInstallmentContract ?? false;
        }
    }
}
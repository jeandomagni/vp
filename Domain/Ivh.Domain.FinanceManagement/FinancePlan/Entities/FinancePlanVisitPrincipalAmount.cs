﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
    using System;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class FinancePlanVisitPrincipalAmount
    {

        public FinancePlanVisitPrincipalAmount()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        public virtual int FinancePlanVisitPrincipalAmountId { get; set; }
        public virtual FinancePlan FinancePlan { get; set; }
        public virtual FinancePlanVisit FinancePlanVisit { get; set; }
        //Amount is an event such as original snapshot balance, payment and possibly an adjustment
        //FinancePlanVisitPrincipalAmountType defines what the amount represents (FinancePlanCreated,Payment,Adjustment)
        public virtual FinancePlanVisitPrincipalAmountTypeEnum FinancePlanVisitPrincipalAmountType { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual int? PaymentAllocationId { get; set; }
        public virtual int? VisitTransactionId { get; set; }

        public virtual DateTime InsertDate { get; set; }
        public virtual DateTime? OverrideInsertDate { get; set; }

        //This points at the naturally created principal amount because of a downward adjustment (this implies this was created for roborefund and is an "NonNaturalQualifyingEvent"
        public virtual int? ParentFinancePlanVisitPrincipalAmountId { get; set; }
    }
}

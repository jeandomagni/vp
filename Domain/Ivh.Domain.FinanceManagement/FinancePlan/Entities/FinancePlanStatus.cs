﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class FinancePlanStatus
    {
        public virtual int FinancePlanStatusId { get; set; }
        public virtual string FinancePlanStatusName { get; set; }
        public virtual string FinancePlanStatusGroupName { get; set; }
        public virtual string FinancePlanStatusDisplayName { get; set; }

        public virtual FinancePlanStatusEnum FinancePlanStatusEnum
        {
            get
            {
                return (FinancePlanStatusEnum)this.FinancePlanStatusId;
            }
        }

        public virtual bool IsUncollectableActive()
        {
            return FinancePlanStatusEnum.UncollectableActive == this.FinancePlanStatusEnum;
        }

        public virtual bool IsUncollectableClosed()
        {
            return FinancePlanStatusEnum.UncollectableClosed == this.FinancePlanStatusEnum;
        }

        public virtual bool IsNotOriginated()
        {
            return this.FinancePlanStatusEnum == FinancePlanStatusEnum.TermsAccepted
                || this.FinancePlanStatusEnum == FinancePlanStatusEnum.PendingAcceptance;
        }
        public virtual bool IsActiveOriginated()
        {
            return this.FinancePlanStatusEnum == FinancePlanStatusEnum.GoodStanding
                   || this.FinancePlanStatusEnum == FinancePlanStatusEnum.PastDue
                   || this.FinancePlanStatusEnum == FinancePlanStatusEnum.UncollectableActive;
        }

        public virtual bool IsActiveGoodStanding()
        {
            return this.FinancePlanStatusEnum == FinancePlanStatusEnum.GoodStanding;
        }
        public virtual bool IsClosed()
        {
            return this.FinancePlanStatusEnum == FinancePlanStatusEnum.NonPaidInFull
                || this.FinancePlanStatusEnum == FinancePlanStatusEnum.Canceled
                || this.FinancePlanStatusEnum == FinancePlanStatusEnum.PaidInFull
                || this.FinancePlanStatusEnum == FinancePlanStatusEnum.UncollectableClosed;
        }
        public virtual bool IsClosedUncollectable()
        {
            return this.FinancePlanStatusEnum == FinancePlanStatusEnum.UncollectableClosed;
        }
        public virtual bool IsActiveUncollectable()
        {
            return this.FinancePlanStatusEnum == FinancePlanStatusEnum.UncollectableActive;
        }
        public virtual bool IsPastDue()
        {
            return this.FinancePlanStatusEnum == FinancePlanStatusEnum.PastDue;
        }

        public virtual bool IsPendingAcceptance()
        {
            return this.FinancePlanStatusEnum == FinancePlanStatusEnum.PendingAcceptance;
        }

        public virtual bool IsTermsAccepted()
        {
            return this.FinancePlanStatusEnum == FinancePlanStatusEnum.TermsAccepted;
        }

        public virtual bool IsPending()
        {
            return this.FinancePlanStatusEnum == FinancePlanStatusEnum.PendingAcceptance || this.FinancePlanStatusEnum == FinancePlanStatusEnum.TermsAccepted;
        }

        public virtual bool IsUncollectable()
        {
            return this.IsActiveUncollectable() || this.IsClosedUncollectable();
        }

        #region equality

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            if (ReferenceEquals(this, obj))
            {
                return true;
            }
            if (obj.GetType() != this.GetType())
            {
                return false;
            }
            return this.Equals((FinancePlanStatus) obj);
        }

        protected bool Equals(FinancePlanStatus other)
        {
            return this.FinancePlanStatusId == other.FinancePlanStatusId;
        }

        public override int GetHashCode()
        {
            return this.FinancePlanStatusId;
        }

        #endregion
    }
}

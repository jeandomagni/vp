﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
    using System;
    using System.Collections.Generic;
    using Common.VisitPay.Enums;
    using Guarantor.Entities;
    using Interfaces;
    using Statement.Entities;

    public class FinancePlanCalculationParameters
    {
        // todo: look closer at these usages
        public FinancePlanCalculationParameters(FinancePlan financePlan) : this(
            financePlan.VpGuarantor,
            financePlan.CreatedVpStatement,
            financePlan.PaymentAmount,
            financePlan.FinancePlanOfferSetTypeId,
            financePlan.OfferCalculationStrategy,
            financePlan.InterestCalculationMethod,
            financePlan.FirstPaymentDueDate.GetValueOrDefault(financePlan.CreatedVpStatement.PaymentDueDate)
            //financePlan.CreatedVpStatement.PaymentDueDate // todo: this is wrong
        )
        {
            this.Visits = financePlan.ActiveFinancePlanVisitBalances;
        }

        public FinancePlanCalculationParameters(
            Guarantor guarantor, 
            VpStatement statement, 
            decimal monthlyPaymentAmount, 
            int? financePlanOfferSetTypeId, 
            OfferCalculationStrategyEnum offerCalculationStrategy,
            InterestCalculationMethodEnum interestCalculationMethod,
            DateTime firstPaymentDueDate)
        {
            this.Guarantor = guarantor;
            this.Statement = statement;
            this.MonthlyPaymentAmount = monthlyPaymentAmount;
            this.FinancePlanOfferSetTypeId = financePlanOfferSetTypeId;
            this.OfferCalculationStrategy = offerCalculationStrategy;
            this.InterestCalculationMethod = interestCalculationMethod;
            this.FirstPaymentDueDate = firstPaymentDueDate;
        }
        
        public Guarantor Guarantor { get; }

        public VpStatement Statement { get; }

        public decimal MonthlyPaymentAmount { get; set; }

        public int? FinancePlanOfferSetTypeId { get; }

        public OfferCalculationStrategyEnum OfferCalculationStrategy { get; }

        public InterestCalculationMethodEnum InterestCalculationMethod { get; }

        public bool CombineFinancePlans { get; set; }
        
        public DateTime FirstPaymentDueDate { get; }

        public IList<IFinancePlanVisitBalance> Visits { get; set; }
        
        public FirstInterestPeriodEnum InterestFreePeriod => FinancePlan.FirstInterestPeriod(this.Statement?.IsGracePeriod ?? false);

        public List<int> SelectedVisits { get; set; }

        public VpGuarantorScore VpGuarantorScore { get; set; }
       
        public int? FinancePlanTypeId { get; set; }


        //if (Enum.IsDefined(typeof(MyEnum), 3)) { ... }
        public FinancePlanTypeEnum FinancePlanType => (this.FinancePlanTypeId.HasValue && Enum.IsDefined(typeof(FinancePlanTypeEnum), this.FinancePlanTypeId.Value)) ? 
            (FinancePlanTypeEnum) this.FinancePlanTypeId.Value 
            : FinancePlanTypeEnum.PaperlessAutoPay;

        public string StateCodeForVisits { get; set; }
    }
}
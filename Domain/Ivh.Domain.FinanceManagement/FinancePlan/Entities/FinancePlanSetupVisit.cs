﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Interfaces;
    
    public class FinancePlanSetupVisit : FinancePlanVisitBalanceBase, IFinancePlanVisitBalance
    {
        private readonly decimal _currentVisitBalance;
        private readonly decimal _unclearedPaymentsSum;
        private readonly decimal _interestDue;

        public FinancePlanSetupVisit(
            int visitId, 
            decimal currentVisitBalance, 
            decimal unclearedPaymentsSum, 
            decimal interestDue,
            VisitStateEnum currentVisitState, 
            string billingApplication,
            bool hasFinancialAssistance)
        {
            this._currentVisitBalance = currentVisitBalance;
            this._unclearedPaymentsSum = unclearedPaymentsSum;
            this._interestDue = interestDue;
            this.VisitId = visitId;
            this.BillingApplication = billingApplication;
            this.CurrentVisitState = currentVisitState;
            this.HasFinancialAssistance = hasFinancialAssistance;
        }

        public FinancePlanSetupVisit(
            int visitId,
            string sourceSystemKey,
            int billingSystemId,
            decimal currentVisitBalance,
            decimal unclearedPaymentsSum,
            decimal interestDue,
            VisitStateEnum currentVisitState,
            string billingApplication,
            bool hasFinancialAssistance, 
            decimal? overrideInterestRate,
            int overridesRemaining)
        : this(visitId, currentVisitBalance, unclearedPaymentsSum, interestDue, currentVisitState, billingApplication, hasFinancialAssistance)
        {
            this.OverrideInterestRate = overrideInterestRate;
            this.OverridesRemaining = overridesRemaining;
            this.SourceSystemKey = sourceSystemKey;
            this.BillingSystemId = billingSystemId;
        }

        public override decimal CurrentVisitBalance => this._currentVisitBalance;

        public decimal PrincipalBalance => this.CalculateBalance(this._currentVisitBalance, this._unclearedPaymentsSum, 0);

        public override decimal UnclearedPaymentsSum => this._unclearedPaymentsSum;

        public override decimal InterestDue => this._interestDue;

        public int VisitId { get; }

        public string SourceSystemKey { get; }
        public int BillingSystemId { get; }

        public VisitStateEnum CurrentVisitState { get; }

        public string BillingApplication { get; }

        public bool HasFinancialAssistance { get; }

        public decimal? OverrideInterestRate { get; }

        public int OverridesRemaining { get; }
    }
}
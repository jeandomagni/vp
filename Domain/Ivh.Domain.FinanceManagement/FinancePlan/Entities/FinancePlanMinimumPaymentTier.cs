﻿namespace Ivh.Domain.FinanceManagement.FinancePlan.Entities
{
    public class FinancePlanMinimumPaymentTier
    {
        public virtual int FinancePlanMinimumPaymentTierId { get; set; }
        public virtual decimal ExisitingRecurringPaymentTotal { get; set; }
        public virtual decimal MinimumPayment { get; set; }
        public virtual int? RandomizedTestGroupId { get; set; }
        public virtual FinancePlanOfferSetType FinancePlanOfferSetType { get; set; }
    }
}

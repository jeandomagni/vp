﻿namespace Ivh.Domain.FinanceManagement.Consolidation.Entities
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Guarantor.Entities;

    public class ConsolidationMatchResponse
    {
        public ConsolidationMatchStatusEnum ConsolidationMatchStatus { get; set; }

        public Guarantor MatchedGuarantor { get; set; }
    }
}
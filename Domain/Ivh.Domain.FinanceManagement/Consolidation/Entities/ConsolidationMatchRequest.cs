﻿namespace Ivh.Domain.FinanceManagement.Consolidation.Entities
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class ConsolidationMatchRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int BirthYear { get; set; }
        public string EmailAddress { get; set; }
        public ConsolidationMatchRequestTypeEnum ConsolidationMatchRequestType { get; set; }

        public bool IsRequestToBeManaged()
        {
            return this.ConsolidationMatchRequestType == ConsolidationMatchRequestTypeEnum.RequestToBeManaged;
        }

        public bool IsRequestToBeManaging()
        {
            return this.ConsolidationMatchRequestType == ConsolidationMatchRequestTypeEnum.RequestToBeManaging;
        }
    }
}
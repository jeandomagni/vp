﻿namespace Ivh.Domain.FinanceManagement.Consolidation.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Utilities.Helpers;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Content.Interfaces;
    using Entities;
    using FinanceManagement.Interfaces;
    using FinancePlan.Entities;
    using FinancePlan.Interfaces;
    using Guarantor.Entities;
    using Guarantor.Interfaces;
    using Interfaces;
    using NHibernate;
    using Statement.Entities;
    using Statement.Interfaces;
    using User.Entities;
    using User.Interfaces;

    public class ConsolidationGuarantorService : DomainService, IConsolidationGuarantorService
    {
        private readonly Lazy<IContentProvider> _contentProvider;
        private readonly Lazy<IConsolidationGuarantorRepository> _consolidationGuarantorRepository;
        private readonly Lazy<IFinancePlanService> _financePlanService;
        private readonly Lazy<IPaymentService> _paymentService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IVpStatementService> _statementService;
        private readonly ISession _session;
        private readonly Lazy<IVisitPayUserRepository> _visitPayUserRepository;

        public ConsolidationGuarantorService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IConsolidationGuarantorRepository> consolidationGuarantorRepository,
            Lazy<IVisitPayUserRepository> visitPayUserRepository,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IContentProvider> contentProvider,
            Lazy<IFinancePlanService> financePlanService,
            Lazy<IVpStatementService> statementService,
            Lazy<IPaymentService> paymentService,
            ISessionContext<VisitPay> sessionContext) : base(serviceCommonService)
        {
            this._consolidationGuarantorRepository = consolidationGuarantorRepository;
            this._visitPayUserRepository = visitPayUserRepository;
            this._guarantorService = guarantorService;
            this._contentProvider = contentProvider;
            this._financePlanService = financePlanService;
            this._statementService = statementService;
            this._paymentService = paymentService;
            this._session = sessionContext.Session;
        }

        public async Task<ConsolidationMatchResponse> MatchGuarantorAsync(ConsolidationMatchRequest consolidationMatchRequest, int visitPayUserId)
        {
            VisitPayUser visitPayUser = await this._visitPayUserRepository.Value.FindByDetailsAsync(consolidationMatchRequest.FirstName, consolidationMatchRequest.LastName, consolidationMatchRequest.BirthYear, consolidationMatchRequest.EmailAddress).ConfigureAwait(false);

            if (visitPayUser != null)
            {
                if (visitPayUser.VisitPayUserId != visitPayUserId) // cannot "consolidate with self"
                {
                    Guarantor matchedGuarantor = this._guarantorService.Value.GetGuarantorEx(visitPayUser.VisitPayUserId);

                    if ((matchedGuarantor == null)
                        || matchedGuarantor.VpGuarantorStatus == VpGuarantorStatusEnum.Closed
                        || matchedGuarantor.IsManagedOrPendingManaged()
                        || (consolidationMatchRequest.IsRequestToBeManaging() && matchedGuarantor.IsManagingOrPendingManaging()))
                        return new ConsolidationMatchResponse { ConsolidationMatchStatus = ConsolidationMatchStatusEnum.NotEligible };

                    using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
                    {
                        Guarantor initiatingGuarantor = this._guarantorService.Value.GetGuarantorEx(visitPayUserId);

                        // ADD the request
                        ConsolidationGuarantor consolidationGuarantor = new ConsolidationGuarantor
                        {
                            InitiatedByUser = initiatingGuarantor.User
                        };

                        switch (consolidationMatchRequest.ConsolidationMatchRequestType)
                        {
                            case ConsolidationMatchRequestTypeEnum.RequestToBeManaged:
                                consolidationGuarantor.DateAcceptedByManagedGuarantor = DateTime.UtcNow;
                                consolidationGuarantor.AcceptedByManagedGuarantorCmsVersion = await this._contentProvider.Value.GetVersionAsync(CmsRegionEnum.ConsolidationManagedAcceptTerms);
                                consolidationGuarantor.AcceptedByManagdGuarantorHipaaCmsVersion = await this._contentProvider.Value.GetVersionAsync(CmsRegionEnum.ConsolidationHipaaReleaseAuthorization);
                                consolidationGuarantor.ManagedGuarantor = initiatingGuarantor;
                                consolidationGuarantor.ManagingGuarantor = matchedGuarantor;
                                consolidationGuarantor.ConsolidationGuarantorStatus = this.GetConsolidationGuarantorStatus(initiatingGuarantor, ConsolidationGuarantorStatusEnum.Pending);
                                break;
                            case ConsolidationMatchRequestTypeEnum.RequestToBeManaging:
                                consolidationGuarantor.DateAcceptedByManagingGuarantor = DateTime.UtcNow;
                                consolidationGuarantor.AcceptedByManagingGuarantorCmsVersion = await this._contentProvider.Value.GetVersionAsync(CmsRegionEnum.ConsolidationManagingAcceptTerms);
                                consolidationGuarantor.ManagedGuarantor = matchedGuarantor;
                                consolidationGuarantor.ManagingGuarantor = initiatingGuarantor;
                                consolidationGuarantor.ConsolidationGuarantorStatus = ConsolidationGuarantorStatusEnum.Pending;
                                break;
                        }

                        this._consolidationGuarantorRepository.Value.Insert(consolidationGuarantor);
                        unitOfWork.Commit();
                    }

                    return new ConsolidationMatchResponse { ConsolidationMatchStatus = ConsolidationMatchStatusEnum.Matched, MatchedGuarantor = matchedGuarantor };
                }
                return new ConsolidationMatchResponse { ConsolidationMatchStatus = ConsolidationMatchStatusEnum.NotEligible };
            }

            return new ConsolidationMatchResponse { ConsolidationMatchStatus = ConsolidationMatchStatusEnum.NoMatchFound };
        }

        public IEnumerable<ConsolidationGuarantor> GetAllManagedGuarantors(Guarantor managingGuarantor)
        {
            return this._consolidationGuarantorRepository.Value.GetAllManagedGuarantors(managingGuarantor);
        }

        public IEnumerable<ConsolidationGuarantor> GetAllManagingGuarantors(Guarantor managedGuarantor)
        {
            return this._consolidationGuarantorRepository.Value.GetAllManagingGuarantors(managedGuarantor);
        }

        public async Task<ConsolidationGuarantor> CancelConsolidationAsync(int consolidationGuarantorId, int visitPayUserId, ConsolidationGuarantorCancellationReasonEnum consolidationGuarantorCancellationReason)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                ConsolidationGuarantor consolidationGuarantor = this._consolidationGuarantorRepository.Value.GetById(consolidationGuarantorId);
                if (consolidationGuarantor.IsPendingOrActive())
                {
                    if (consolidationGuarantor.IsActive())
                    {
                        switch (consolidationGuarantorCancellationReason)
                        {
                            case ConsolidationGuarantorCancellationReasonEnum.CancelledByManaging:
                                consolidationGuarantor.CancelledByManagingGuarantorCmsVersion = await this._contentProvider.Value.GetVersionAsync(CmsRegionEnum.ConsolidationManagingUserCancelConsolidation);
                                break;
                            case ConsolidationGuarantorCancellationReasonEnum.CancelledByManaged:
                                consolidationGuarantor.CancelledByManagedGuarantorCmsVersion = await this._contentProvider.Value.GetVersionAsync(CmsRegionEnum.ConsolidationManagedUserCancelConsolidation);
                                if (this._financePlanService.Value.GetActiveFinancePlanCount(consolidationGuarantor.ManagedGuarantor.VpGuarantorId) > 0)
                                {
                                    consolidationGuarantor.CancelledByManagedGuarantorFpCmsVersion = await this._contentProvider.Value.GetVersionAsync(CmsRegionEnum.ConsolidationManagedUserCancelConsolidationFp);
                                }
                                break;
                            case ConsolidationGuarantorCancellationReasonEnum.ExpiredBySystem:
                            case ConsolidationGuarantorCancellationReasonEnum.GuarantorAccountCancelled:
                            default:
                                //DO NOTHING
                                break;
                        }
                    }

                    consolidationGuarantor.ConsolidationGuarantorStatus = ConsolidationGuarantorStatusEnum.Cancelled;
                    consolidationGuarantor.CancelledByUser = new VisitPayUser { VisitPayUserId = visitPayUserId };
                    consolidationGuarantor.CancelledOn = DateTime.UtcNow;
                    consolidationGuarantor.ConsolidationGuarantorCancellationReason = consolidationGuarantorCancellationReason;

                    this._consolidationGuarantorRepository.Value.InsertOrUpdate(consolidationGuarantor);
                    this.FinalizeDeconsolidation(consolidationGuarantor);
                    unitOfWork.Commit();
                }

                return consolidationGuarantor;
            }
        }

        public async Task<ConsolidationGuarantor> ExpireConsolidationAsync(int consolidationGuarantorId, DateTime expirationDateTime)
        {
            ConsolidationGuarantor consolidationGuarantor = this._consolidationGuarantorRepository.Value.GetById(consolidationGuarantorId);
            if (consolidationGuarantor.IsPending() && consolidationGuarantor.MostRecentActionDate.Date.AddDays(this.Client.Value.ConsolidationExpirationDays) <= expirationDateTime.Date)
            {
                return await this.CancelConsolidationAsync(consolidationGuarantorId, SystemUsers.SystemUserId, ConsolidationGuarantorCancellationReasonEnum.ExpiredBySystem).ConfigureAwait(false);
            }
            return consolidationGuarantor;
        }

        public async Task<ConsolidationGuarantor> RejectConsolidationManagingAsync(int consolidationGuarantorId, int visitPayUserId)
        {
            return await Task.Run(() =>
            {
                using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
                {
                    ConsolidationGuarantor consolidationGuarantor = this._consolidationGuarantorRepository.Value.GetById(consolidationGuarantorId);
                    consolidationGuarantor.ConsolidationGuarantorStatus = ConsolidationGuarantorStatusEnum.Rejected;
                    consolidationGuarantor.CancelledOn = DateTime.UtcNow;
                    consolidationGuarantor.CancelledByUser = new VisitPayUser { VisitPayUserId = visitPayUserId };
                    this._consolidationGuarantorRepository.Value.InsertOrUpdate(consolidationGuarantor);
                    unitOfWork.Commit();

                    return consolidationGuarantor;
                }
            });
        }

        public async Task<ConsolidationGuarantor> RejectConsolidationManagedAsync(int consolidationGuarantorId, int visitPayUserId)
        {
            return await Task.Run(() =>
            {
                using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
                {
                    ConsolidationGuarantor consolidationGuarantor = this._consolidationGuarantorRepository.Value.GetById(consolidationGuarantorId);
                    consolidationGuarantor.ConsolidationGuarantorStatus = ConsolidationGuarantorStatusEnum.Rejected;
                    consolidationGuarantor.CancelledOn = DateTime.UtcNow;
                    consolidationGuarantor.CancelledByUser = new VisitPayUser {VisitPayUserId = visitPayUserId};
                    this._consolidationGuarantorRepository.Value.InsertOrUpdate(consolidationGuarantor);
                    unitOfWork.Commit();

                    return consolidationGuarantor;
                }
            });
        }

        public async Task<ConsolidationGuarantor> AcceptTermsManagingAsync(int consolidationGuarantorId)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                ConsolidationGuarantor consolidationGuarantor = this._consolidationGuarantorRepository.Value.GetById(consolidationGuarantorId);
                if (consolidationGuarantor.IsPending())
                {
                    consolidationGuarantor.ConsolidationGuarantorStatus = this.GetConsolidationGuarantorStatus(consolidationGuarantor.ManagedGuarantor);
                    consolidationGuarantor.DateAcceptedByManagingGuarantor = DateTime.UtcNow;
                    consolidationGuarantor.AcceptedByManagingGuarantorCmsVersion = await this._contentProvider.Value.GetVersionAsync(CmsRegionEnum.ConsolidationManagingAcceptTerms);
                    this._consolidationGuarantorRepository.Value.InsertOrUpdate(consolidationGuarantor);

                    this.FinalizeConsolidation(consolidationGuarantor);

                    unitOfWork.Commit();

                    return consolidationGuarantor;
                }
            }

            return null;
        }

        public async Task<ConsolidationGuarantor> AcceptFinanceFlanTermsManagingAsync(int consolidationGuarantorId)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                DateTime now = DateTime.UtcNow;

                ConsolidationGuarantor consolidationGuarantor = this._consolidationGuarantorRepository.Value.GetById(consolidationGuarantorId);
                if (consolidationGuarantor.IsPending())
                {
                    consolidationGuarantor.ConsolidationGuarantorStatus = ConsolidationGuarantorStatusEnum.Accepted;
                    if (!consolidationGuarantor.DateAcceptedByManagingGuarantor.HasValue)
                    {
                        consolidationGuarantor.DateAcceptedByManagingGuarantor = consolidationGuarantor.DateAcceptedByManagingGuarantor.GetValueOrDefault(now);
                        consolidationGuarantor.AcceptedByManagingGuarantorCmsVersion = await this._contentProvider.Value.GetVersionAsync(CmsRegionEnum.ConsolidationManagingAcceptTerms);
                    }

                    consolidationGuarantor.DateAcceptedFinancePlanTermsByManagingGuarantor = now;
                    // As of VP-701 we are no longer saving the FP cms. 
                    //consolidationGuarantor.AcceptedByManagingGuarantorFpCmsVersion = await this._contentProvider.Value.GetVersionAsync(CmsRegionEnum.ConsolidationManagingAcceptTermsFp);

                    this._consolidationGuarantorRepository.Value.InsertOrUpdate(consolidationGuarantor);

                    this.FinalizeConsolidation(consolidationGuarantor);

                    unitOfWork.Commit();

                    return consolidationGuarantor;
                }
            }

            return null;
        }

        public async Task<ConsolidationGuarantor> AcceptTermsManagedAsync(int consolidationGuarantorId)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                ConsolidationGuarantor consolidationGuarantor = this._consolidationGuarantorRepository.Value.GetById(consolidationGuarantorId);
                if (consolidationGuarantor.IsPending())
                {
                    consolidationGuarantor.ConsolidationGuarantorStatus = this.GetConsolidationGuarantorStatus(consolidationGuarantor.ManagedGuarantor);
                    consolidationGuarantor.DateAcceptedByManagedGuarantor = DateTime.UtcNow;
                    consolidationGuarantor.AcceptedByManagedGuarantorCmsVersion = await this._contentProvider.Value.GetVersionAsync(CmsRegionEnum.ConsolidationManagedAcceptTerms);
                    consolidationGuarantor.AcceptedByManagdGuarantorHipaaCmsVersion = await this._contentProvider.Value.GetVersionAsync(CmsRegionEnum.ConsolidationHipaaReleaseAuthorization);
                    this._consolidationGuarantorRepository.Value.InsertOrUpdate(consolidationGuarantor);

                    this.FinalizeConsolidation(consolidationGuarantor);

                    unitOfWork.Commit();

                    return consolidationGuarantor;
                }
            }

            return null;
        }

        public IEnumerable<ConsolidationGuarantor> GetAllExpiringConsolidations(DateTime insertDate)
        {
            return this._consolidationGuarantorRepository.Value.GetAllExpiringConsolidations(insertDate);
        }

        public IEnumerable<ConsolidationGuarantor> GetPendingRequests()
        {
            return this._consolidationGuarantorRepository.Value.GetPendingRequests();
        }

        public IEnumerable<ConsolidationGuarantor> GetPendingManagingRequests(Guarantor managingGuarantor)
        {
            return this._consolidationGuarantorRepository.Value.GetManagingRequests(managingGuarantor).Where(x => x.IsPending());
        }

        public IEnumerable<ConsolidationGuarantor> GetPendingManagedRequests(Guarantor managedGuarantor)
        {
            return this._consolidationGuarantorRepository.Value.GetManagedRequests(managedGuarantor).Where(x => x.IsPending());
        }

        public IEnumerable<ConsolidationGuarantor> GetManagingRequests(Guarantor managingGuarantor)
        {
            return this._consolidationGuarantorRepository.Value.GetManagingRequests(managingGuarantor);
        }

        public IEnumerable<ConsolidationGuarantor> GetManagedRequests(Guarantor managedGuarantor)
        {
            return this._consolidationGuarantorRepository.Value.GetManagedRequests(managedGuarantor);
        }

        public async Task<ConsolidationGuarantor> GetConsolidationGuarantorAsync(int consolidationGuarantorId, int visitPayUserId)
        {
            return await Task.Run(() => this._consolidationGuarantorRepository.Value.GetById(consolidationGuarantorId));
        }

        private ConsolidationGuarantorStatusEnum GetConsolidationGuarantorStatus(Guarantor guarantor, ConsolidationGuarantorStatusEnum statusIfNoFinancePlans = ConsolidationGuarantorStatusEnum.Accepted)
        {
            int count = this._financePlanService.Value.GetFinancePlanCount(guarantor, new List<FinancePlanStatusEnum>
            {
                FinancePlanStatusEnum.TermsAccepted, FinancePlanStatusEnum.GoodStanding, FinancePlanStatusEnum.PastDue, FinancePlanStatusEnum.UncollectableActive
            });

            return count > 0 ? ConsolidationGuarantorStatusEnum.PendingFinancePlan : statusIfNoFinancePlans;
        }

        private void FinalizeConsolidation(ConsolidationGuarantor consolidationGuarantor)
        {
            if (!consolidationGuarantor.IsActive())
            {
                return;
            }

            Guarantor managedGuarantor = consolidationGuarantor.ManagedGuarantor;

            // finance plans
            IList<FinancePlan> financePlans = this.GetFinancePlans(managedGuarantor.VpGuarantorId);
            if (financePlans.Any())
            {
                foreach (FinancePlan financePlan in financePlans)
                {
                    this._financePlanService.Value.ConsolidateFinancePlans(financePlan);
                }
            }

            // payments
            this._paymentService.Value.CancelPaymentsForGuarantor(managedGuarantor, EnumHelper<PaymentStatusEnum>.GetValues(PaymentStatusEnumCategory.ConsolidationCancellation).ToList());
            this._paymentService.Value.CancelPaymentsForGuarantor(managedGuarantor, EnumHelper<PaymentTypeEnum>.GetValues(PaymentTypeEnumCategory.ConsolidationCancellation).ToList());

            // payment and statement date
            int newDueDay = consolidationGuarantor.ManagingGuarantor.PaymentDueDay;
            this._guarantorService.Value.ChangePaymentDueDay(consolidationGuarantor.ManagedGuarantor, newDueDay, new VisitPayUser { VisitPayUserId = SystemUsers.SystemUserId }, "Consolidation");
            
            VpStatement managingStatement = this._statementService.Value.GetMostRecentStatement(consolidationGuarantor.ManagedGuarantor);
            if (!managingStatement.IsGracePeriod)
            {
                managedGuarantor.NextStatementDate = consolidationGuarantor.ManagingGuarantor.NextStatementDate;
                this._financePlanService.Value.UpdatePendingFinancePlanOriginationDate(managedGuarantor, managingStatement);
            }
            else
            {
                this._statementService.Value.ProcessPaymentDueDayChanged(consolidationGuarantor.ManagedGuarantor, false, SystemUsers.SystemUserId);
            }
        }

        private void FinalizeDeconsolidation(ConsolidationGuarantor consolidationGuarantor)
        {
            Guarantor managedGuarantor = consolidationGuarantor.ManagedGuarantor;

            // finance plans
            IList<FinancePlan> financePlans = this.GetFinancePlans(managedGuarantor.VpGuarantorId);
            if (financePlans.Any())
            {
                foreach (FinancePlan financePlan in financePlans)
                {
                    this._financePlanService.Value.DeconsolidateFinancePlans(financePlan);
                }
            }

            // payments
            this._paymentService.Value.CancelPaymentsForGuarantor(managedGuarantor, EnumHelper<PaymentStatusEnum>.GetValues(PaymentStatusEnumCategory.ConsolidationCancellation).ToList());
            this._paymentService.Value.CancelPaymentsForGuarantor(managedGuarantor, EnumHelper<PaymentTypeEnum>.GetValues(PaymentTypeEnumCategory.ConsolidationCancellation).ToList());
        }

        private IList<FinancePlan> GetFinancePlans(int managedGuarantorId)
        {
            FinancePlanFilter financePlanFilter = new FinancePlanFilter
            {
                FinancePlanStatusIds = Enum.GetValues(typeof(FinancePlanStatusEnum)).Cast<FinancePlanStatusEnum>().ToList(),
                IncludePendingReconfigured = true
            };

            return this._financePlanService.Value.GetFinancePlans(managedGuarantorId, financePlanFilter, 0, int.MaxValue).FinancePlans;
        }
    }
}
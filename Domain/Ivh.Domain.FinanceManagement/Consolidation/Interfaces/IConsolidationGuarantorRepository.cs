﻿namespace Ivh.Domain.FinanceManagement.Consolidation.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Guarantor.Entities;

    public interface IConsolidationGuarantorRepository : IRepository<ConsolidationGuarantor>
    {
        IEnumerable<ConsolidationGuarantor> GetAllExpiringConsolidations(DateTime insertDate);

        IEnumerable<ConsolidationGuarantor> GetPendingRequests();

        IEnumerable<ConsolidationGuarantor> GetManagingRequests(Guarantor managingGuarantor);

        IEnumerable<ConsolidationGuarantor> GetManagedRequests(Guarantor managedGuarantor);

        IEnumerable<ConsolidationGuarantor> GetAllRequests(Guarantor guarantor);

        IEnumerable<ConsolidationGuarantor> GetAllManagedGuarantors(Guarantor managingGuarantor);
        IEnumerable<int> GetAllManagedGuarantorVpGuarantorIds(int vpGuarantorId);

        IEnumerable<ConsolidationGuarantor> GetAllManagingGuarantors(Guarantor managedGuarantor);
    }
}
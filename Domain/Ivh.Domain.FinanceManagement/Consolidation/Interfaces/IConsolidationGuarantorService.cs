﻿namespace Ivh.Domain.FinanceManagement.Consolidation.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;
    using Guarantor.Entities;

    public interface IConsolidationGuarantorService : IDomainService
    {
        Task<ConsolidationMatchResponse> MatchGuarantorAsync(ConsolidationMatchRequest consolidationMatchRequest, int visitPayUserId);

        Task<ConsolidationGuarantor> GetConsolidationGuarantorAsync(int consolidationGuarantorId, int visitPayUserId);

        IEnumerable<ConsolidationGuarantor> GetAllManagedGuarantors(Guarantor managingGuarantor);

        IEnumerable<ConsolidationGuarantor> GetAllManagingGuarantors(Guarantor managedGuarantor);

        Task<ConsolidationGuarantor> CancelConsolidationAsync(int consolidationGuarantorId, int visitPayUserId, ConsolidationGuarantorCancellationReasonEnum consolidationGuarantorCancellationReason);

        Task<ConsolidationGuarantor> ExpireConsolidationAsync(int consolidationGuarantorId, DateTime expirationDateTime);

        Task<ConsolidationGuarantor> RejectConsolidationManagingAsync(int consolidationGuarantorId, int visitPayUserId);

        Task<ConsolidationGuarantor> RejectConsolidationManagedAsync(int consolidationGuarantorId, int visitPayUserId);

        Task<ConsolidationGuarantor> AcceptTermsManagingAsync(int consolidationGuarantorId);

        Task<ConsolidationGuarantor> AcceptFinanceFlanTermsManagingAsync(int consolidationGuarantorId);

        Task<ConsolidationGuarantor> AcceptTermsManagedAsync(int consolidationGuarantorId);

        IEnumerable<ConsolidationGuarantor> GetAllExpiringConsolidations(DateTime expirationDateTime);

        IEnumerable<ConsolidationGuarantor> GetPendingRequests();

        IEnumerable<ConsolidationGuarantor> GetPendingManagingRequests(Guarantor managingGuarantor);

        IEnumerable<ConsolidationGuarantor> GetPendingManagedRequests(Guarantor managedGuarantor);

        IEnumerable<ConsolidationGuarantor> GetManagingRequests(Guarantor managingGuarantor);

        IEnumerable<ConsolidationGuarantor> GetManagedRequests(Guarantor managedGuarantor);
    }
}
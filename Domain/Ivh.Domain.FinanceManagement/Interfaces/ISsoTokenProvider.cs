namespace Ivh.Domain.FinanceManagement.Interfaces
{
    using System.Threading.Tasks;
    using Application.FinanceManagement.Common.Dtos;
    using Core.Sso.Entities;

    public interface ISsoTokenProvider
    {
        Task<HealthEquitySsoTokenResponse> GetSsoToken(string authToken, SsoProvider ssoProvider);
    }
}
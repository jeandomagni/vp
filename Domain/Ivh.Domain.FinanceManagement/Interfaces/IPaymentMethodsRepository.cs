﻿using Ivh.Domain.FinanceManagement.Entities;

namespace Ivh.Domain.FinanceManagement.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;

    public interface IPaymentMethodsRepository : IRepository<PaymentMethod>
    {
        PaymentMethod GetByIdStateless(int paymentMethodId);
        IList<PaymentMethod> GetPaymentMethods(int guarantorId, bool includingInactive = false);
        int GetPaymentMethodsCount(int guarantorId);
        PaymentMethod GetPrimaryPaymentMethodStateless(int vpGuarantorId);
        PaymentMethod GetPrimaryPaymentMethod(int vpGuarantorId);
        PaymentMethod GetByBillingId(string billingId);
        IList<PaymentMethod> GetExpiringPrimaryPaymentMethods(string expDate);
    }
}

﻿namespace Ivh.Domain.FinanceManagement.Interfaces
{
    using Statement.Entities;

    public class GeneratedStatementsResultDto
    {
        public VpStatement VpStatement { get; set; }
        public FinancePlanStatement FinancePlanStatement { get; set; }
    }
}

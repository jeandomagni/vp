﻿namespace Ivh.Domain.FinanceManagement.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Payment.Entities;

    public interface IPaymentReversalProvider : IPaymentProviderBase
    {
        Task<PaymentProcessorResponse> SubmitPaymentsVoidAsync(IList<Payment> originalPayments, IList<Payment> reversalPayments, PaymentProcessor paymentProcessor);
        Task<PaymentProcessorResponse> SubmitPaymentsRefundAsync(IList<Payment> originalPayments, IList<Payment> reversalPayments, PaymentProcessor paymentProcessor, decimal? amount);
    }
}
﻿namespace Ivh.Domain.FinanceManagement.Interfaces
{
    using Common.Base.Interfaces;
    using Visit.Entities;

    public interface IBalanceTransferService : IDomainService
    {
        void SetVisitBalanceTransferStatus(BalanceTransferStatusHistory balanceTransferStatusHistory);
        void SetBalanceTransferStatusActiveForVisit(string visitSourceSystemKey, int billingSystemId);
        void SetBalanceTransferStatusEligible(Visit visit, string notes = null);
    }
}

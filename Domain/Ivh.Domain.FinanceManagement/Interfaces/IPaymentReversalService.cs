namespace Ivh.Domain.FinanceManagement.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Ivh.Domain.User.Entities;
    using Payment.Entities;

    public interface IPaymentReversalService : IDomainService
    {
        /// <summary>
        /// This member is to allow unit tests to specific the value for RIGHT NOW.
        /// If not set, it will default to DateTime.UtcNow;
        /// </summary>
        DateTime Now { get; set; }
        PaymentReversalStatusEnum GetPaymentReversalStatus(IList<Payment> payments);
        [Obsolete(ObsoleteDescription.VisitTransactions + " " + ObsoleteDescription.VisitTransactionVisitPayDiscount)]
        Task<IList<Payment>> VoidPaymentAsync(IList<Payment> originalPayments, VisitPayUser visitPayUser);
        Task<IList<Payment>> RefundPaymentAsync(IList<Payment> originalPayments, VisitPayUser visitPayUser, decimal? amount = null);
        void ReallocateInterestToPrincipal(Payment reallocationPayment, decimal reallocateAmountAfterWriteOff, Dictionary<int, decimal> maxPerVisit, int financePlanId, int? visitPayUserId);
        PaymentRefundableTypeEnum GetPaymentRefundableType(IList<Payment> payments);
    }
}
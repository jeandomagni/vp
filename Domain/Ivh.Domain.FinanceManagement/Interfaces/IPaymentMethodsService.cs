﻿namespace Ivh.Domain.FinanceManagement.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IPaymentMethodsService : IDomainService
    {
        PaymentMethod GetById(int paymentMethodId);
        PaymentMethod GetById(int paymentMethodId, int vpGuarantorId);
        PaymentMethodEvent GetMostRecentPaymentMethodEvent(int vpGuarantorId, PaymentMethodStatusEnum paymentMethodStatus);
        IList<PaymentMethod> GetPaymentMethods(int vpGuarantorId, int actionVisitPayUserId, bool includingInactive = false);
        int GetPaymentMethodsCount(int vpGuarantorId);
        PaymentMethod GetPrimaryPaymentMethod(int vpGuarantorId, bool considerConsolidation);
        IList<PaymentMethod> GetExpiringPrimaryPaymentMethods(string expDate);
        PaymentMethodResponse Delete(PaymentMethod paymentMethod, int actionVisitPayUserId);
        PaymentMethodResponse Save(PaymentMethod paymentMethod, int actionVisitPayUserId, string cardCode, bool suppressNotifications);
        PaymentMethodResponse ValidatePrimaryPaymentMethod(PaymentMethod paymentMethod);
        void AddAchAuthorization(int vpGuarantorId, int paymentMethodId, int cmsVersionId, decimal authorizedAmount, int authorizedDuration, Guid correlationGuid);

        // only used in PaymentProcessorInterceptor
        PaymentMethod GetByBillingId(string billingId);
    }
}
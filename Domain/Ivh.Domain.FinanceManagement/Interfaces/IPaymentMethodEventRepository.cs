﻿namespace Ivh.Domain.FinanceManagement.Interfaces
{
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IPaymentMethodEventRepository : IRepository<PaymentMethodEvent>
    {
        PaymentMethodEvent GetMostRecentPaymentMethodEvent(int vpGuarantorId, PaymentMethodStatusEnum paymentMethodStatus);
    }
}
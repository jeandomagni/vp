﻿namespace Ivh.Domain.FinanceManagement.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Common.Base.Interfaces;
    using Payment.Entities;

    public interface IPaymentBatchOperationsService : IDomainService
    {
        Task<ICollection<QueryPaymentTransaction>> GetPaymentTransactionsAsync(DateTime beginDate, DateTime endDate, string transactionId);
        Task<ICollection<QueryPaymentTransaction>> GetUnsettledAchPaymentTransactionsAsync(DateTime beginDate, DateTime endDate);
    }
}

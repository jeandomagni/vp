namespace Ivh.Domain.FinanceManagement.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Application.FinanceManagement.Common.Dtos;
    using FinancePlan.Entities;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.HospitalData;
    using Entities;
    using Guarantor.Entities;
    using Payment.Entities;
    using Statement.Interfaces;
    using ValueTypes;
    using System.Linq;

    public interface IPaymentService : IDomainService
    {
        PaymentProcessorResponse AddDummyPaymentProcessorResponse(decimal snapshotTotalPaymentAmount, PaymentMethod paymentMethod, PaymentProcessorResponse paymentProcessorResponse, Payment payment, PaymentProcessorResponseStatusEnum status = PaymentProcessorResponseStatusEnum.Unknown);
        PaymentTypeEnum AdjustPaymentType(PaymentTypeEnum paymentType, DateTime scheduledPaymentDate);
        void CancelAllPaymentsForGuarantor(Guarantor vpGuarantor, int? visitPayUserId = null);
        void CancelPayment(int vpGuarantorId, IList<int> paymentIds, int? visitPayUserId = null, string reason = null);
        void CancelPaymentsForGuarantor(Guarantor vpGuarantor, List<PaymentStatusEnum> paymentStatuses, int? visitPayUserId = null);
        void CancelPaymentsForGuarantor(Guarantor vpGuarantor, List<PaymentTypeEnum> paymentTypes, int? visitPayUserId = null);
        void CancelRecurringPayment(Guarantor guarantor, int actionVisitPayUserId, IList<CancelRecurringPaymentDto> cancelRecurringPaymentDtos);
        IList<ChargePaymentResponse> ChargePayment(Payment paymentEntity, int? visitPayUserId = null);
        IList<ChargePaymentResponse> ChargeRecurringPayments(IList<Payment> payments, int? visitPayUserId = null);
        Payment CreateReallocationFinancePlanPayment(int financePlanId, Guarantor vpGuarantor, int? visitPayUserId);
        Payment CreateRecurringPayment(int financePlanId, Guarantor vpGuarantor, decimal amountDue, PaymentMethod paymentMethod = null);
        //VisitTransaction CreateVisitTransactionFromPaymentAllocation(Visit visit, PaymentAllocation paymentAllocation, DateTime paymentDate);
        void FailPayment(Payment payment, string comment = null);
        void FixPaymentAllocations(int paymentId);
        IList<int> GetGuarantorIdsWithPaymentsScheduledFor(DateTime dateToProcess);
        Payment GetMostRecentTextToPayPaymentForGuarantor(int vpGuarantorId);
        Payment GetPayment(int vpGuarantorId, int paymentId);
        IList<PaymentFinancePlan> GetPaymentFinancePlanByFinancePlanIds(IList<int> financePlanIds);
        IReadOnlyList<PaymentProcessorResponse> GetPaymentHistory(PaymentProcessorResponseFilter filter);
        PaymentProcessorResponse GetPaymentProcessorResponse(int paymentProcessorResponseId, int vpGuarantorId);
        IReadOnlyList<Payment> GetPayments(IList<int> paymentIds, int vpGuarantorId);
        IList<Payment> GetPaymentsByTransactionId(string transactionId, IList<PaymentStatusEnum> paymentStatuses = null);
        IReadOnlyList<Payment> GetPaymentsForVisit(int visitId);
        IQueryable<Payment> GetPaymentsQueryable();
        IList<PaymentStatus> GetPaymentStatuses();
        IList<PaymentVisit> GetPaymentVisitsByVisitIds(IList<int> visitIds);
        ScheduledPaymentResult GetScheduledPaymentResult(int vpGuarantorId, int paymentId);
        IReadOnlyList<ScheduledPaymentResult> GetScheduledPaymentResults(int vpGuarantorId);
        IEnumerable<Payment> GetScheduledPayments(int vpGuarantorId);
        int GetScheduledPaymentsCountByGuarantorPaymentMethod(int vpGuarantorId);
        IReadOnlyList<Payment> GetScheduledPaymentsWithExactDate(DateTime scheduledPaymentDate);
        void PopulatePaymentMethodTypeOnPaymentAllocationMessages(IList<PaymentAllocationMessage> paymentAllocationMessages);
        void PostProcessPayment<TStatement>(Guarantor guarantor, TStatement mostRecentStatement, DateTime dateToProcess, IList<Payment> scheduledPayments, IList<Payment> recurringPayments) where TStatement : IVpStatementPostPaymentProcess;
        void ProcessOptimisticAchSettlement(Payment payment);
        IList<Payment> ProcessRecurringPaymentsForGuarantor(IList<Payment> payments, Guarantor vpGuarantor, DateTime dateToProcess, int? visitPayUserId = null);
        IList<Payment> ProcessScheduledPaymentsForGuarantor(Guarantor vpGuarantor, DateTime dateToProcess, int? visitPayUserId = null);
        IList<Payment> ProcessUnsentRecurringPaymentsForGuarantor(Guarantor vpGuarantor, DateTime dateToProcess, int? visitPayUserId = null);
        void PublishPaymentAllocations(IList<PaymentAllocation> paymentAllocations);
        void PublishPaymentFailedEmail(int vpGuarantorId);
        bool RemovePendingPaymentAmountAssociatedWithFinancePlan(FinancePlan financePlan, int? visitPayUserId = null);
        void ReturnAchPayment(Payment payment, string returnReasonCode, string transactionId, bool isReturnedAfterSettlement = false);
        void ReturnAchPaymentAfterSettlement(Payment payment, string transactionId);
        void SavePaymentImport(PaymentImport paymentImport);
        void SavePendingGuarantorResponsePayment(Payment paymentEntity, int? visitPayUserId = null);
        void SchedulePayment(Payment paymentEntity, int? visitPayUserId = null);
        void SendEmailsForPromptPayments(IList<Payment> payments);
        void SetGuarantorCure(int vpGuarantorId, IList<int> paymentIds);
        void SettleAchPayment(Payment payment, AchSettlementTypeEnum achSettlementType = AchSettlementTypeEnum.None);
        Payment UpdatePayment(Guarantor vpGuarantor, Payment payment, int? actionVisitPayUserId = null, string paymentEventComment = null);
        IList<PaymentImport> GetPaymentImports(PaymentImportFilter filter);
        IList<string> GetAllPaymentImportBatchNumbers();
        PaymentImport GetPaymentImport(int paymentImportId);
    }
}
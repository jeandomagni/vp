namespace Ivh.Domain.FinanceManagement.Interfaces
{
    using System.Threading.Tasks;
    using Application.FinanceManagement.Common.Dtos;
    using Core.Sso.Entities;

    public interface IHsaBalanceProvider
    {
        Task<HealthEquityBalanceResponse> GetBalanceForSsnAsync(string ssn, string employerId);
        Task<HealthEquityBalanceResponse> GetBalanceForOauthAsync(string authToken, SsoProvider ssoProvider);
    }
}
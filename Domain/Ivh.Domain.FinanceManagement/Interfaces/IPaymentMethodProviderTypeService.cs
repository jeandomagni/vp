﻿
namespace Ivh.Domain.FinanceManagement.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IPaymentMethodProviderTypeService : IDomainService
    {
        IList<PaymentMethodProviderType> GetPaymentMethodProviderTypes();
        PaymentMethodProviderType GetById(int paymentMethodProviderTypeId);
    }
}

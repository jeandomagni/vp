﻿
namespace Ivh.Domain.FinanceManagement.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IPaymentMethodProviderTypeRepository : IRepository<PaymentMethodProviderType>
    {
    }
}

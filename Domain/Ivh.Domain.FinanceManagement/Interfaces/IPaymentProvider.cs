﻿namespace Ivh.Domain.FinanceManagement.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Entities;
    using Payment.Entities;
    using ValueTypes;

    public interface IPaymentProvider : IPaymentProviderBase
    {
        Task<BillingIdResult> StoreBankAccountBillingIdAsync(PaymentMethod paymentMethod, string accountNumber, string routingNumber);
        Task<BillingIdResult> StoreCreditDebitCardBillingIdAsync(PaymentMethod paymentMethod, string accountNumber, string securityCode);
        Task<bool>UpdateCreditCardExpirationAsync(string billingId, string expiration, string cvv);
        Task<IList<PaymentProcessorResponse>> SubmitPaymentsAsync(IList<Payment> payments, PaymentProcessor paymentProcessor);
        IReadOnlyDictionary<string, string> ParseResponse(string rawResponse);
    }
}
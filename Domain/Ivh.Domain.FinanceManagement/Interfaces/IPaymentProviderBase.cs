﻿namespace Ivh.Domain.FinanceManagement.Interfaces
{
    public interface IPaymentProviderBase
    {
        string PaymentProcessorName { get; }
        //string ProductName { get; }
    }
}
namespace Ivh.Domain.FinanceManagement.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Payment.Entities;

    public interface IPaymentEventService : IDomainService
    {
        void AddPaymentEvent(int? visitPayUserId, Payment payment, string comment = null);
        IReadOnlyList<PaymentEvent> GetPaymentEvents(List<int> paymentIds, int vpGuarantorId);
    }
}
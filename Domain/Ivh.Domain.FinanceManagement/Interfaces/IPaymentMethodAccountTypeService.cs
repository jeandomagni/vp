﻿
namespace Ivh.Domain.FinanceManagement.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IPaymentMethodAccountTypeService : IDomainService
    {
        IList<PaymentMethodAccountType> GetPaymentMethodAccountTypes();
        PaymentMethodAccountType GetById(int paymentMethodAccountTypeId);
    }
}

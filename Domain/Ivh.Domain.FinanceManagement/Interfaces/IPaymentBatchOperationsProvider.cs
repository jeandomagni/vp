﻿namespace Ivh.Domain.FinanceManagement.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Payment.Entities;

    public interface IPaymentBatchOperationsProvider
    {
        Task<ICollection<QueryPaymentTransaction>> GetPaymentTransactionsAsync(DateTime beginDate, DateTime endDate, string transactionId, bool logResponse);
    }
}
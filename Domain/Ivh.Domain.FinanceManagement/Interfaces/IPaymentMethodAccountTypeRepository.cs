﻿namespace Ivh.Domain.FinanceManagement.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IPaymentMethodAccountTypeRepository : IRepository<PaymentMethodAccountType>
    {
    }
}
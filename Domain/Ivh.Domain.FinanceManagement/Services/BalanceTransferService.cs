﻿namespace Ivh.Domain.FinanceManagement.Services
{
    using System;
    using System.Collections.Generic;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Visit.Entities;
    using Visit.Interfaces;
    using FinancePlan.Entities;
    using FinancePlan.Interfaces;
    using Interfaces;
    using User.Entities;

    public class BalanceTransferService : DomainService, IBalanceTransferService
    {
        private readonly Lazy<IFinancePlanRepository> _financePlanRepository;
        private readonly Lazy<IVisitRepository> _visitRepository;

        public BalanceTransferService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IFinancePlanRepository> financePlanRepository,
            Lazy<IVisitRepository> visitRepository) : base(serviceCommonService)
        {
            this._financePlanRepository = financePlanRepository;
            this._visitRepository = visitRepository;
        }

        public void SetVisitBalanceTransferStatus(BalanceTransferStatusHistory balanceTransferStatusHistory)
        {
            if (!this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.BalanceTransfer))
            {
                return;
            }
            if (balanceTransferStatusHistory.Visit == null)
            {
                return;
            }

            balanceTransferStatusHistory.Visit.SetBalanceTransferStatus(balanceTransferStatusHistory);
            this._visitRepository.Value.InsertOrUpdate(balanceTransferStatusHistory.Visit);
        }

        public void SetBalanceTransferStatusActiveForVisit(string visitSourceSystemKey, int billingSystemId)
        {
            if (!this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.BalanceTransfer))
            {
                return;
            }

            Visit visit = this._visitRepository.Value.GetBySourceSystemKey(visitSourceSystemKey, billingSystemId);

            if (visit.BalanceTransferStatus?.BalanceTransferStatusEnum != BalanceTransferStatusEnum.Eligible)
            {
                return;
            }

            this.SetVisitBalanceTransferStatus(new BalanceTransferStatusHistory
            {
                Visit = visit,
                BalanceTransferStatus = new BalanceTransferStatus { BalanceTransferStatusId = (int)BalanceTransferStatusEnum.ActiveBalanceTransfer },
                ChangedBy = new VisitPayUser { VisitPayUserId = SystemUsers.SystemUserId },
                InsertDate = DateTime.UtcNow
            });
        }

        public void SetBalanceTransferStatusEligible(Visit visit, string notes = null)
        {
            if (!this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.BalanceTransfer))
            {
                return;
            }
            if (!this.CanSetBalanceTransferStatus(visit, BalanceTransferStatusEnum.Eligible))
            {
                return;
            }

            this.SetVisitBalanceTransferStatus(new BalanceTransferStatusHistory
            {
                Visit = visit,
                BalanceTransferStatus = new BalanceTransferStatus { BalanceTransferStatusId = (int)BalanceTransferStatusEnum.Eligible },
                ChangedBy = new VisitPayUser { VisitPayUserId = SystemUsers.SystemUserId },
                InsertDate = DateTime.UtcNow,
                Notes = notes
            });
        }
        
        private bool CanSetBalanceTransferStatus(Visit visit, BalanceTransferStatusEnum balanceTransferStatusEnum)
        {
            switch (balanceTransferStatusEnum)
            {
                case BalanceTransferStatusEnum.Eligible:
                    return this.IsEligibleForBalanceTransfer(visit);
                case BalanceTransferStatusEnum.Ineligible:
                    return false;
                //case BalanceTransferStatusEnum.ActiveBalanceTransfer: This is determined in BT
                default:
                    return false;
            }
        }

        private bool IsEligibleForBalanceTransfer(Visit visit)
        {
            if (visit.BalanceTransferStatus?.BalanceTransferStatusEnum == BalanceTransferStatusEnum.Ineligible)
            {
                return false;
            }

            bool onFinancePlan = this._visitRepository.Value.IsOnFinancePlanInStatus(visit, new List<FinancePlanStatusEnum>
            {
                FinancePlanStatusEnum.GoodStanding,
                FinancePlanStatusEnum.PastDue,
                FinancePlanStatusEnum.UncollectableActive,
                FinancePlanStatusEnum.PendingAcceptance,
                FinancePlanStatusEnum.TermsAccepted
            });

            if (!onFinancePlan)
            {
                return false;
            }

            FinancePlan financePlan = this._financePlanRepository.Value.GetFinancePlanWithVisit(visit);
            int minDuration = this.Client.Value.BalanceTransferFinancePlanMinDuration;
            int maxDuration = this.Client.Value.BalanceTransferFinancePlanMaxDuration;

            bool visitBtStatusIsValid = visit.BalanceTransferStatus == null || (!visit.BalanceTransferStatus.IsIneligible() && !visit.BalanceTransferStatus.IsEligible());
            bool fpWithinDuration = financePlan.FinancePlanOffer.DurationRangeStart >= minDuration && financePlan.FinancePlanOffer.DurationRangeEnd <= maxDuration;

            return visitBtStatusIsValid && fpWithinDuration;
        }
    }
}

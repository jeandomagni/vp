﻿namespace Ivh.Domain.FinanceManagement.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.HospitalData;
    using FinancePlan.Interfaces;
    using Interfaces;
    using Ivh.Domain.FinanceManagement.Ach.Entities;
    using Ivh.Domain.FinanceManagement.Ach.Interfaces;
    using Newtonsoft.Json;
    using NHibernate;
    using Payment.Entities;
    using Payment.Interfaces;
    using User.Entities;

    public class PaymentReversalService : DomainService, IPaymentReversalService
    {
        private readonly Lazy<IFinancePlanService> _financePlanService;
        private readonly Lazy<IPaymentAllocationService> _paymentAllocationService;
        private readonly Lazy<IPaymentEventService> _paymentEventService;
        private readonly Lazy<IPaymentMethodsService> _paymentMethodsService;
        private readonly Lazy<IPaymentProcessorRepository> _paymentProcessorRepository;
        private readonly Lazy<IPaymentProcessorResponseRepository> _paymentProcessorResponseRepository;
        private readonly Lazy<IPaymentProvider> _paymentProvider;
        private readonly Lazy<IPaymentRepository> _paymentRepository;
        private readonly Lazy<IPaymentReversalProvider> _paymentReversalProvider;
        private readonly Lazy<IAchService> _achService;
        private readonly ISession _session;

        // this is only to be used for unit testing to set the value of right now.
        private DateTime? _now;

        public PaymentReversalService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IPaymentRepository> paymentRepository,
            Lazy<IPaymentEventService> paymentEventService,
            Lazy<IPaymentReversalProvider> paymentReversalProvider,
            Lazy<IPaymentProcessorRepository> paymentProcessorRepository,
            Lazy<IPaymentProcessorResponseRepository> paymentProcessorResponseRepository,
            Lazy<IPaymentAllocationService> paymentAllocationService,
            Lazy<IPaymentProvider> paymentProvider,
            Lazy<IPaymentMethodsService> paymentMethodsService,
            Lazy<IFinancePlanService> financePlanService,
            Lazy<IAchService> achService,
            ISessionContext<VisitPay> sessionContext) : base(serviceCommonService)
        {
            this._paymentRepository = paymentRepository;
            this._paymentEventService = paymentEventService;
            this._paymentReversalProvider = paymentReversalProvider;
            this._paymentProcessorRepository = paymentProcessorRepository;
            this._paymentProcessorResponseRepository = paymentProcessorResponseRepository;
            this._paymentAllocationService = paymentAllocationService;
            this._paymentProvider = paymentProvider;
            this._paymentMethodsService = paymentMethodsService;
            this._financePlanService = financePlanService;
            this._achService = achService;
            this._session = sessionContext.Session;
        }

        public DateTime Now
        {
            get => this._now ?? DateTime.UtcNow;
            set => this._now = value;
        }

        public async Task<IList<Payment>> VoidPaymentAsync(IList<Payment> originalPayments, VisitPayUser visitPayUser)
        {
            if (this.GetPaymentReversalStatus(originalPayments) != PaymentReversalStatusEnum.Voidable)
            {
                return null;
            }

            PaymentProcessor paymentProcessor = this._paymentProcessorRepository.Value.GetByName(this._paymentProvider.Value.PaymentProcessorName);
            IList<Payment> reversalPayments = this.CreateReversalPayments(originalPayments, PaymentTypeEnum.Void, visitPayUser.VisitPayUserId);
            try
            {
                PaymentProcessorResponse paymentProcessorResponse = await this._paymentReversalProvider.Value.SubmitPaymentsVoidAsync(originalPayments, reversalPayments, paymentProcessor).ConfigureAwait(false);
                this._paymentProcessorResponseRepository.Value.InsertOrUpdate(paymentProcessorResponse);
                if (paymentProcessorResponse.WasSuccessful())
                {
                    this.PostProcessorSuccess(reversalPayments, visitPayUser);
                }
                else
                {
                    this.PostProcessorFailure(reversalPayments, visitPayUser);
                }
            }
            catch (Exception)
            {
                this.PostProcessorFailure(reversalPayments, visitPayUser);
            }

            return reversalPayments;
        }

        public async Task<IList<Payment>> RefundPaymentAsync(IList<Payment> originalPayments, VisitPayUser visitPayUser, decimal? amount = null)
        {
            amount = amount ?? originalPayments.Sum(x => x.NetPaymentAmount);

            if (amount > originalPayments.Sum(x => x.NetPaymentAmount))
            {
                return null;
            }

            if (this.GetPaymentReversalStatus(originalPayments) != PaymentReversalStatusEnum.Refundable)
            {
                return null;
            }

            PaymentProcessor paymentProcessor = this._paymentProcessorRepository.Value.GetByName(this._paymentProvider.Value.PaymentProcessorName);
            PaymentTypeEnum paymentType = amount != originalPayments.Sum(x => x.ActualPaymentAmount) ? PaymentTypeEnum.PartialRefund : PaymentTypeEnum.Refund;
            IList<Payment> reversalPayments = this.CreateReversalPayments(originalPayments, paymentType, visitPayUser.VisitPayUserId, amount);

            try
            {
                PaymentProcessorResponse paymentProcessorResponse = null;
                if (GetPaymentRefundableType(originalPayments) == PaymentRefundableTypeEnum.Default)
                {
                    paymentProcessorResponse = await this._paymentReversalProvider.Value.SubmitPaymentsRefundAsync(originalPayments, reversalPayments, paymentProcessor, amount).ConfigureAwait(false);
                }
                else
                {
                    paymentProcessorResponse = GenerateEmptyPaymentResponse(originalPayments, reversalPayments, paymentProcessor, amount);
                }
                this._paymentProcessorResponseRepository.Value.InsertOrUpdate(paymentProcessorResponse);
                if (paymentProcessorResponse.WasSuccessful())
                {
                    this.PostProcessorSuccess(reversalPayments, visitPayUser);
                }
                else
                {
                    this.PostProcessorFailure(reversalPayments, visitPayUser);
                }
            }
            catch (Exception e)
            {
                this.PostProcessorFailure(reversalPayments, visitPayUser);
            }

            return reversalPayments;
        }

        private PaymentProcessorResponse GenerateEmptyPaymentResponse(IList<Payment> originalPayments, IList<Payment> reversalPayments, PaymentProcessor paymentProcessor, decimal? amount)
        {
            PaymentProcessorResponse paymentProcessorResponse = new PaymentProcessorResponse
            {
                PaymentProcessorSourceKey = string.Empty,
                PaymentProcessor = paymentProcessor,
                PaymentProcessorResponseStatus = PaymentProcessorResponseStatusEnum.Approved,
                RawResponse = "",
                ResponseField1 = string.Empty, //transactionId,
                ResponseField2 = string.Empty, //status,
                ResponseField3 = string.Empty, //authCode,
                ResponseField4 = string.Empty, //avs,
                ResponseField5 = string.Empty, // declineType,
                ResponseField6 = string.Empty, //error,
                ResponseField7 = string.Empty, //errorType,
                ResponseField8 = string.Empty, //offenders,
                ResponseField9 = string.Empty, //responseCode,
                ResponseField10 = string.Empty, //responseCodeDescriptor
                SnapshotTotalPaymentAmount = reversalPayments.Sum(x => x.ActualPaymentAmount),
                PaymentMethod = reversalPayments.First().PaymentMethod,
                PaymentSystemType = PaymentSystemTypeEnum.VisitPay
            };

            paymentProcessorResponse.SnapshotTotalPaymentAmount = reversalPayments.Sum(x => x.ScheduledPaymentAmount);

            foreach (Payment reversalPayment in reversalPayments)
            {
                PaymentPaymentProcessorResponse current = new PaymentPaymentProcessorResponse
                {
                    InsertDate = DateTime.UtcNow,
                    Payment = reversalPayment,
                    PaymentProcessorResponse = paymentProcessorResponse,
                    SnapshotPaymentAmount = reversalPayment.ScheduledPaymentAmount
                };
                reversalPayment.PaymentPaymentProcessorResponses.Add(current);
                paymentProcessorResponse.PaymentPaymentProcessorResponses.Add(current);
            }

            return paymentProcessorResponse;
        }

        public void ReallocateInterestToPrincipal(Payment reallocationPayment, decimal reallocateAmountAfterWriteOff, Dictionary<int, decimal> maxPerVisit, int financePlanId, int? visitPayUserId)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {

                PaymentAllocationResult allocationResult = this._paymentAllocationService.Value.AllocateReallocationPayment(reallocationPayment, reallocateAmountAfterWriteOff, maxPerVisit, financePlanId);
                allocationResult.CommitAllocationsToPayment();

                reallocationPayment.ActualPaymentDate = DateTime.UtcNow;
                reallocationPayment.PaymentStatus = PaymentStatusEnum.ClosedPaid;

                this._paymentRepository.Value.InsertOrUpdate(reallocationPayment);
                this._paymentEventService.Value.AddPaymentEvent(visitPayUserId, reallocationPayment);

                unitOfWork.Commit();

                this._session.Transaction.RegisterPostCommitSuccessAction(p1 =>
                {
                    this.PublishPaymentAllocationMessages(p1);
                    this.PublishPaymentMessage(p1);
                }, reallocationPayment);

            }

        }

        public PaymentReversalStatusEnum GetPaymentReversalStatus(IList<Payment> payments)
        {
            IDictionary<int, PaymentReversalStatusEnum> results = new Dictionary<int, PaymentReversalStatusEnum>(payments.Count);
            foreach (Payment payment in payments)
            {
                if (payment.IsReversalPaymentType())
                {
                    results[payment.PaymentId] = PaymentReversalStatusEnum.NotReversable;
                }
                else if (payment.NetPaymentAmount <= 0m)
                {
                    results[payment.PaymentId] = PaymentReversalStatusEnum.NotReversable;
                }
                else if (payment.PaymentMethod == null)
                {
                    results[payment.PaymentId] = PaymentReversalStatusEnum.NotReversable;
                }
                else if (payment.PaymentMethod.IsCardType && payment.PaymentStatus == PaymentStatusEnum.ClosedPaid)
                {
                    results[payment.PaymentId] = this.GetReversalStatus(this.Client.Value.SettlementWindowBeginCc, this.Client.Value.SettlementWindowEndCc, payment.ActualPaymentDate.Value);
                }
                else if (payment.PaymentMethod.IsAchType && payment.PaymentStatus == PaymentStatusEnum.ClosedPaid)
                {
                    results[payment.PaymentId] = this.GetAchClosedPaidPaymentReversalStatus(payment);
                }
                else if (payment.PaymentMethod.IsAchType && payment.PaymentStatus == PaymentStatusEnum.ActivePendingACHApproval)
                {
                    results[payment.PaymentId] = this.GetAchActivePendingACHApprovalPaymentReversalStatus(payment);
                }
                else if (payment.PaymentMethod.IsLockBoxType && this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureRefundReturnedChecks))
                {
                    results[payment.PaymentId] = PaymentReversalStatusEnum.Refundable;
                }
                else
                {
                    results[payment.PaymentId] = PaymentReversalStatusEnum.NotReversable;
                }
            }

            return results.Any() && results.Values.Distinct().Count() == 1 ? results.Values.First() : PaymentReversalStatusEnum.NotReversable;
        }

        private PaymentReversalStatusEnum GetAchActivePendingACHApprovalPaymentReversalStatus(Payment payment)
        {
            PaymentReversalStatusEnum paymentReversalStatusEnum = this.GetReversalStatus(this.Client.Value.SettlementWindowBeginAch, this.Client.Value.SettlementWindowEndAch, payment.ActualPaymentDate.Value);
            return paymentReversalStatusEnum == PaymentReversalStatusEnum.Refundable ? PaymentReversalStatusEnum.NotReversable : PaymentReversalStatusEnum.Voidable;
        }

        private PaymentReversalStatusEnum GetAchClosedPaidPaymentReversalStatus(Payment payment)
        {
            bool featureOptimisticAchSettlementIsEnabled = this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureOptimisticAchSettlementIsEnabled);
            if (!featureOptimisticAchSettlementIsEnabled)
            {
                return PaymentReversalStatusEnum.Refundable;
            }

            IList<AchSettlementDetail> achSettlementDetails = this._achService.Value.GetAchSettlementDetailsByTransactionId(payment.TransactionId);
            bool hasFileSettlement = achSettlementDetails.Any(x => x.AchFile != null);

            if (hasFileSettlement)
            {
                return PaymentReversalStatusEnum.Refundable;
            }
            else
            {
                return this.GetAchActivePendingACHApprovalPaymentReversalStatus(payment);
            }
        }

        public PaymentRefundableTypeEnum GetPaymentRefundableType(IList<Payment> payments)
        {
            IDictionary<int, PaymentRefundableTypeEnum> results = new Dictionary<int, PaymentRefundableTypeEnum>(payments.Count);
            foreach (Payment payment in payments)
            {
                if (payment.PaymentMethod.IsLockBoxType)
                {
                    results[payment.PaymentId] = PaymentRefundableTypeEnum.ReturnedCheck;
                }
                else
                {
                    results[payment.PaymentId] = PaymentRefundableTypeEnum.Default;
                }
            }

            return results.Any() && results.Values.Distinct().Count() == 1 ? results.Values.First() : PaymentRefundableTypeEnum.Default;
        }

        private PaymentReversalStatusEnum GetReversalStatus(DateTime windowBegin, DateTime windowEnd, DateTime actualPaymentDate)
        {
            // Since we are only dealing in time we need to figure out if the time represents today or tomorrow after converting to UTC
            // converting to UTC could push the time to the next day. Ex. 3:00am could mean 3:00am today or 3:00am tomorrow
            DateTime settlementWindowBeginCcRaw = this.Client.Value.SettlementWindowBeginCcRaw;
            int dayDiff = windowBegin.Day - settlementWindowBeginCcRaw.Day;

            DateTime clientNow = this.TimeZoneHelper.Value.UtcToClient(this.Now);
            bool nowUtcDayEqualToClientUtcDay = this.Now.Day == clientNow.Day;

            //to avoid jumping multiple days, only add a day if:
            //  the settlementWindow jumps a day when converting to UTC
            //  AND
            //  the current client time didn't already jump to the next day when converting to UTC
            int dayAdjust = dayDiff > 0 && nowUtcDayEqualToClientUtcDay ? 1 : 0;

            DateTime currentWindowBegin = this.Now.AddDays(dayAdjust).SetTime(windowBegin);
            DateTime currentWindowEnd = currentWindowBegin.Add(windowEnd - windowBegin);

            // are we in the settlement
            if (this.Now.IsBetween(currentWindowBegin, currentWindowEnd))
            {
                if (actualPaymentDate > currentWindowBegin)
                {
                    return PaymentReversalStatusEnum.Voidable;
                }
                return PaymentReversalStatusEnum.Refundable;
            }
            if (actualPaymentDate > currentWindowEnd)
            {
                return PaymentReversalStatusEnum.Voidable;
            }

            DateTime previousWindowEnd = currentWindowEnd.AddDays(-1);
            if (actualPaymentDate.IsBetween(previousWindowEnd, currentWindowBegin))
            {
                return PaymentReversalStatusEnum.Voidable;
            }
            return PaymentReversalStatusEnum.Refundable;
        }

        private IList<Payment> CreateReversalPayments(ICollection<Payment> originalPayments, PaymentTypeEnum paymentType, int visitPayUserId, decimal? amount = null)
        {
            decimal originalPaymentsSumNetPaymentAmount = originalPayments.Sum(x => x.NetPaymentAmount);
            decimal proRata = 1m;
            if (paymentType == PaymentTypeEnum.PartialRefund)
            {
                amount = amount ?? originalPaymentsSumNetPaymentAmount;
                proRata = amount.Value / originalPaymentsSumNetPaymentAmount;
            }

            IDictionary<Payment, decimal> reversalAmounts = originalPayments.ToDictionary(x => x, x => 0m);

            // distribute
            foreach (Payment originalPayment in originalPayments)
            {
                reversalAmounts[originalPayment] = -Math.Floor(originalPayment.NetPaymentAmount * proRata * 100) / 100; // round down, ex: 01.1362342 becomes 01.13
            }

            // distribute any remaining
            decimal fullAmount = amount.GetValueOrDefault(originalPaymentsSumNetPaymentAmount) * -1;

            for (decimal amountRefunded = reversalAmounts.Sum(x => x.Value); amountRefunded > fullAmount;)
            {
                if (!originalPayments.Any())
                {
                    break;
                }

                foreach (Payment originalPayment in originalPayments.OrderByDescending(x => x.ActualPaymentAmount)) // order by highest original payment amount
                {
                    if (amountRefunded <= fullAmount)
                    {
                        break;
                    }

                    reversalAmounts[originalPayment] -= 0.01m;
                    amountRefunded -= 0.01m;
                }
            }

            // create offsetting payments and apply to visits
            IList<Payment> reversalPayments = new List<Payment>(originalPayments.Count);
            foreach (KeyValuePair<Payment, decimal> reversalAmount in reversalAmounts)
            {
                Payment originalPayment = reversalAmount.Key;

                Payment reversalPayment = new Payment
                {
                    PaymentType = paymentType,
                    ScheduledPaymentDate = this.Now,
                    Guarantor = originalPayment.Guarantor,
                    PaymentAttemptCount = 1,
                    PaymentMethod = originalPayment.PaymentMethod,
                    ParentPayment = originalPayment,
                    PaymentStatus = PaymentStatusEnum.ActivePending,
                    MerchantAccount = originalPayment.MerchantAccount,
                    CreatedVisitPayUserId = visitPayUserId
                };

                decimal reversalScheduledAmount = reversalAmount.Value;

                reversalPayment.PaymentScheduledAmounts.Add(new PaymentScheduledAmount { ScheduledAmount = reversalScheduledAmount, Payment = reversalPayment });
                this._paymentRepository.Value.InsertOrUpdate(reversalPayment);
                reversalPayments.Add(reversalPayment);

                PaymentAllocationResult result = this._paymentAllocationService.Value.AllocatePaymentReverse(originalPayment, reversalPayment);
                result.CommitAllocationsToPayment();

                reversalPayment.PaymentScheduledAmounts[0].ScheduledAmount = reversalPayment.ActualPaymentAmount;
                this._paymentRepository.Value.InsertOrUpdate(originalPayment);
                originalPayment.ReversalPayments.Add(reversalPayment);
            }

            return reversalPayments;
        }

        private void PostProcessorSuccess(IEnumerable<Payment> reversalPayments, VisitPayUser visitPayUser)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                IList<Payment> payments = reversalPayments as IList<Payment> ?? reversalPayments.ToList();
                foreach (Payment reversalPayment in payments)
                {
                    switch (reversalPayment.PaymentType)
                    {
                        case PaymentTypeEnum.Refund:
                        case PaymentTypeEnum.PartialRefund:
                            reversalPayment.PaymentStatus = PaymentStatusEnum.ClosedPaid;
                            break;
                        case PaymentTypeEnum.Void:
                            reversalPayment.PaymentStatus = PaymentStatusEnum.ClosedPaid;
                            reversalPayment.ParentPayment.PaymentStatus = PaymentStatusEnum.ClosedCancelled;
                            break;
                        default:
                            throw new ArgumentException("Invalid PaymentType", nameof(reversalPayments));
                    }

                    reversalPayment.ActualPaymentAmount = reversalPayment.ScheduledPaymentAmount;
                    reversalPayment.ActualPaymentDate = reversalPayment.ScheduledPaymentDate;

                    this._paymentRepository.Value.InsertOrUpdate(reversalPayment);
                    this._paymentEventService.Value.AddPaymentEvent(visitPayUser.VisitPayUserId, reversalPayment);
                    this._paymentEventService.Value.AddPaymentEvent(visitPayUser.VisitPayUserId, reversalPayment.ParentPayment);
                }

                this._session.Transaction.RegisterPostCommitSuccessAction(() =>
                {
                    foreach (Payment reversalPayment in payments)
                    {
                        this.PublishPaymentAllocationMessages(reversalPayment);
                        this.PublishPaymentMessage(reversalPayment);
                    }
                });

                unitOfWork.Commit();

            }
        }

        private void PostProcessorFailure(IEnumerable<Payment> reversalPayments, VisitPayUser visitPayUser)
        {
            foreach (Payment reversalPayment in reversalPayments)
            {
                reversalPayment.PaymentStatus = PaymentStatusEnum.ClosedFailed;
                reversalPayment.ActualPaymentAmount = 0m;
                reversalPayment.ActualPaymentDate = null;
                reversalPayment.PaymentAllocations.Clear();
                this._paymentRepository.Value.InsertOrUpdate(reversalPayment);

                if (reversalPayment.PaymentProcessorResponses.IsNotNullOrEmpty())
                {
                    this._paymentEventService.Value.AddPaymentEvent(visitPayUser.VisitPayUserId, reversalPayment, reversalPayment.PaymentProcessorResponses[0].ErrorMessage);
                    this._paymentEventService.Value.AddPaymentEvent(visitPayUser.VisitPayUserId, reversalPayment.ParentPayment, reversalPayment.PaymentProcessorResponses[0].ErrorMessage);
                }
                else
                {
                    this._paymentEventService.Value.AddPaymentEvent(visitPayUser.VisitPayUserId, reversalPayment);
                    this._paymentEventService.Value.AddPaymentEvent(visitPayUser.VisitPayUserId, reversalPayment.ParentPayment);
                }
            }
        }

        /*
         //VP-24 - Pretty sure this isnt a thing, transactions should come from the HS.
        [Obsolete(ObsoleteDescription.VisitTransactions)]
        private void AddTransactionsFromAllocations(Payment reversalPayment)
        {
            //Create transactions.
            foreach (PaymentAllocation paymentAllocation in reversalPayment.PaymentAllocations)
            {
                if (reversalPayment.ActualPaymentDate == null)
                {
                    continue;
                }

                paymentAllocation.OriginalVisitTransaction = this.CreateVisitTransactionFromPaymentAllocation(paymentAllocation.Visit, paymentAllocation, reversalPayment.ActualPaymentDate.Value);
                paymentAllocation.Visit.AddVisitTransaction(paymentAllocation.OriginalVisitTransaction);
                this._visitService.Value.SaveVisit(paymentAllocation.Visit);
            }
        }
        

        private VisitTransaction CreateVisitTransactionFromPaymentAllocation(Visit visit, PaymentAllocation paymentAllocation, DateTime paymentDate)
        {
            VpTransactionType transactionType = this._visitTransactionService.Value.GetTransactionTypeForPendingPatientCash(paymentAllocation.PaymentAllocationType);
            VisitTransaction visitTransaction = new VisitTransaction
            {
                Visit = visit,
                VpTransactionType = transactionType,
                VpGuarantorId = visit.VPGuarantor.VpGuarantorId,
                TransactionDate = paymentDate,
                PostDate = paymentDate,
                PaymentAllocationId = paymentAllocation.PaymentAllocationId
            };
            this._visitTransactionService.Value.SetTransactionDescription(visitTransaction, paymentAllocation.Payment.IsVoidType(), paymentAllocation.Payment.IsRefundType());
            visitTransaction.SetTransactionAmount(-1 * paymentAllocation.ActualAmount);

            return visitTransaction;
        }
        */

        private void PublishPaymentAllocationMessages(Payment reversalPayment)
        {
            if (reversalPayment.ActualPaymentDate == null || reversalPayment.PaymentStatus != PaymentStatusEnum.ClosedPaid || !reversalPayment.PaymentAllocations.Any())
            {
                return;
            }

            bool hasOptimisticSettlementEnabled = this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureOptimisticAchSettlementIsEnabled);

            // If Optimistic ACH Settlement is enabled then parent payment amount was already sent to outbound.
            // We only want to send the new reversal payment to outbound to avoid double posting the original payment amount

            if (reversalPayment.PaymentType == PaymentTypeEnum.Void
                && reversalPayment.PaymentMethod.IsAchType
                && reversalPayment.ParentPayment.PaymentMethod.IsAchType
                && !hasOptimisticSettlementEnabled)
            {
                this._paymentAllocationService.Value.PublishPaymentAllocationMessages(reversalPayment.ParentPayment);
            }

            this._paymentAllocationService.Value.PublishPaymentAllocationMessages(reversalPayment);
        }

        private void PublishPaymentMessage(Payment payment)
        {
            if (payment.ActualPaymentDate == null)
            {
                return;
            }

            PaymentMessage message = new PaymentMessage
            {
                VpGuarantorId = payment.Guarantor.VpGuarantorId,
                VpPaymentId = payment.PaymentId,
                VpPaymentMethodTypeId = (int)(payment.PaymentMethod?.PaymentMethodType ?? PaymentMethodTypeEnum.Unknown),
                VpPaymentStatusId = (int)payment.PaymentStatus,
                VpPaymentTypeId = (int)payment.PaymentType,
                FullPaymentAmount = payment.ActualPaymentAmount,
                ActualPaymentDate = payment.ActualPaymentDate
            };

            if (payment.ParentPayment != null)
            {
                message.OffsetPaymentId = payment.ParentPayment.PaymentId;
                message.OffsetPaymentAmount = payment.ParentPayment.ActualPaymentAmount;
                message.OffsetPaymentDate = payment.ParentPayment.ActualPaymentDate;
            }

            this._session.Transaction.RegisterPostCommitSuccessAction(p1 =>
            {
                try
                {
                    this.Bus.Value.PublishMessage(p1).Wait();
                }
                catch
                {
                    this.LoggingService.Value.Fatal(() => $"PaymentReversalService.PublishPaymentMessage::Failed to publish {typeof(PaymentMessage).FullName}, Message = {JsonConvert.SerializeObject(message)}");
                }
            }, message);
        }
    }
}
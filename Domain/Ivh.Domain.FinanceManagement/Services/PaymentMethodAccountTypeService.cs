﻿
namespace Ivh.Domain.FinanceManagement.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Entities;
    using Interfaces;

    public class PaymentMethodAccountTypeService : DomainService, IPaymentMethodAccountTypeService
    {
        private readonly Lazy<IPaymentMethodAccountTypeRepository> _paymentMethodAccountTypeRepository;

        public PaymentMethodAccountTypeService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IPaymentMethodAccountTypeRepository> paymentMethodAccountTypeRepository) : base(serviceCommonService)
        {
            this._paymentMethodAccountTypeRepository = paymentMethodAccountTypeRepository;
        }

        public IList<PaymentMethodAccountType> GetPaymentMethodAccountTypes()
        {
            return this._paymentMethodAccountTypeRepository.Value.GetQueryable()
                .Where(x => x.DisplayOrder > 0)
                .OrderBy(x => x.DisplayOrder)
                .ToList();
        }

        public PaymentMethodAccountType GetById(int paymentMethodAccountTypeId)
        {
            return this._paymentMethodAccountTypeRepository.Value.GetById(paymentMethodAccountTypeId);
        }

    }
}

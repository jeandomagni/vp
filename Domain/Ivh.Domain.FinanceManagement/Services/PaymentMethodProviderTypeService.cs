﻿
namespace Ivh.Domain.FinanceManagement.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Entities;
    using Interfaces;

    public class PaymentMethodProviderTypeService : DomainService, IPaymentMethodProviderTypeService
    {
        private readonly Lazy<IPaymentMethodProviderTypeRepository> _paymentMethodProviderTypeRepository;

        public PaymentMethodProviderTypeService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IPaymentMethodProviderTypeRepository> paymentMethodProviderTypeRepository) : base(serviceCommonService)
        {
            this._paymentMethodProviderTypeRepository = paymentMethodProviderTypeRepository;
        }

        public IList<PaymentMethodProviderType> GetPaymentMethodProviderTypes()
        {
            return this._paymentMethodProviderTypeRepository.Value.GetQueryable()
                .Where(x => x.DisplayOrder > 0)
                .OrderBy(x => x.DisplayOrder)
                .ToList();
        }

        public PaymentMethodProviderType GetById(int paymentMethodProviderTypeId)
        {
            return this._paymentMethodProviderTypeRepository.Value.GetById(paymentMethodProviderTypeId);
        }
    }
}


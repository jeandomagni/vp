﻿namespace Ivh.Domain.FinanceManagement.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Application.FinanceManagement.Common.Dtos;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.ServiceBus.Common.Extensions;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.Communication;
    using Common.VisitPay.Messages.Core;
    using Common.VisitPay.Messages.FinanceManagement;
    using Common.VisitPay.Messages.HospitalData;
    using Core.SystemException.Interfaces;
    using Entities;
    using FinancePlan.Entities;
    using FinancePlan.Interfaces;
    using Guarantor.Entities;
    using Interfaces;
    using Ivh.Domain.FinanceManagement.Ach.Entities;
    using Ivh.Domain.FinanceManagement.Ach.Interfaces;
    using Logging.Interfaces;
    using Newtonsoft.Json;
    using NHibernate;
    using Payment.Entities;
    using Payment.Interfaces;
    using Statement.Entities;
    using Statement.Interfaces;
    using ValueTypes;
    using SystemException = Core.SystemException.Entities.SystemException;

    public class PaymentService : DomainService, IPaymentService, ISystemExceptionProvider
    {
        private const string PromptPaymentMetricKey = "Payment.Charged.Prompt";
        private const string SchedulePaymentMetricKey = "Payment.Scheduled";
        private const string PaymentPendingResponseMetricKey = "Payment.PendingGuarantorResponse";

        private readonly Lazy<IFinancePlanService> _financePlanService;
        private readonly Lazy<IPaymentAllocationService> _paymentAllocationService;
        private readonly Lazy<IPaymentEventService> _paymentEventService;
        private readonly Lazy<IPaymentProcessorRepository> _paymentProcessorRepository;
        private readonly Lazy<IPaymentMethodsService> _paymentMethodsService;
        private readonly Lazy<IPaymentProcessorResponseRepository> _paymentProcessorResponseRepository;
        private readonly Lazy<IPaymentProvider> _paymentProvider;
        private readonly Lazy<IPaymentRepository> _paymentRepository;
        private readonly ISession _session;
        private readonly Lazy<IVpStatementService> _statementService;
        private readonly Lazy<ISystemExceptionService> _systemExceptionService;
        private readonly Lazy<IMetricsProvider> _metricsProvider;
        private readonly Lazy<IMerchantAccountAllocationService> _merchantAccountAllocationService;
        private readonly Lazy<IPaymentAllocationRepository> _paymentAllocationRepository;
        private readonly Lazy<IPaymentVisitRepository> _paymentVisitRepository;
        private readonly Lazy<IPaymentFinancePlanRepository> _paymentFinancePlanRepository;
        private readonly Lazy<IPaymentImportRepository> _paymentImportRepository;
        private readonly Lazy<IAchService> _achService;
        private readonly Lazy<TimeZoneHelper> _timeZoneHelper;

        public PaymentService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IPaymentProvider> paymentProvider,
            Lazy<IPaymentRepository> paymentRepository,
            Lazy<IPaymentAllocationService> paymentAllocationService,
            Lazy<IPaymentProcessorRepository> paymentProcessorRepository,
            Lazy<IPaymentMethodsService> paymentMethodsService,
            Lazy<IPaymentProcessorResponseRepository> paymentProcessorResponseRepository,
            Lazy<IVpStatementService> statementService,
            Lazy<IPaymentEventService> paymentEventService,
            Lazy<IFinancePlanService> financePlanService,
            Lazy<ISystemExceptionService> systemExceptionService,
            Lazy<IMetricsProvider> metricsProvider,
            Lazy<IMerchantAccountAllocationService> merchantAccountAllocationService,
            Lazy<IPaymentAllocationRepository> paymentAllocationRepository,
            Lazy<IPaymentVisitRepository> paymentVisitRepository,
            Lazy<IPaymentFinancePlanRepository> paymentFinancePlanRepository,
            Lazy<IPaymentImportRepository> paymentImportRepository,
            Lazy<IAchService> achService,
            Lazy<TimeZoneHelper> timeZoneHelper,
            ISessionContext<VisitPay> sessionContext) : base(serviceCommonService)
        {
            this._paymentProvider = paymentProvider;
            this._paymentRepository = paymentRepository;
            this._paymentAllocationService = paymentAllocationService;
            this._paymentMethodsService = paymentMethodsService;
            this._paymentProcessorRepository = paymentProcessorRepository;
            this._paymentMethodsService = paymentMethodsService;
            this._paymentProcessorResponseRepository = paymentProcessorResponseRepository;
            this._statementService = statementService;
            this._paymentEventService = paymentEventService;
            this._financePlanService = financePlanService;
            this._session = sessionContext.Session;
            this._systemExceptionService = systemExceptionService;
            this._metricsProvider = metricsProvider;
            this._merchantAccountAllocationService = merchantAccountAllocationService;
            this._paymentAllocationRepository = paymentAllocationRepository;
            this._paymentVisitRepository = paymentVisitRepository;
            this._paymentFinancePlanRepository = paymentFinancePlanRepository;
            this._paymentImportRepository = paymentImportRepository;
            this._achService = achService;
            this._timeZoneHelper = timeZoneHelper;
        }

        public IList<PaymentFinancePlan> GetPaymentFinancePlanByFinancePlanIds(IList<int> financePlanIds)
        {
            return this._paymentFinancePlanRepository.Value.GetPaymentFinancePlans(financePlanIds);
        }

        public IList<PaymentVisit> GetPaymentVisitsByVisitIds(IList<int> visitIds)
        {
            return this._paymentVisitRepository.Value.GetPaymentVisits(visitIds);
        }

        public Payment GetPayment(int vpGuarantorId, int paymentId)
        {
            return this._paymentRepository.Value.GetPayment(vpGuarantorId, paymentId);
        }

        public IReadOnlyList<Payment> GetScheduledPaymentsWithExactDate(DateTime scheduledPaymentDate)
        {
            IReadOnlyList<Payment> payments = this._paymentRepository.Value.GetScheduledPaymentsWithExactDate(scheduledPaymentDate);
            return payments;
        }

        public IReadOnlyList<Payment> GetPaymentsForVisit(int visitId)
        {
            return this._paymentRepository.Value.GetPaymentsFromVisit(visitId);
        }

        public IList<int> GetGuarantorIdsWithPaymentsScheduledFor(DateTime dateToProcess)
        {
            return this._paymentRepository.Value.GetScheduledPaymentGuarantorIds(dateToProcess);
        }

        public bool RemovePendingPaymentAmountAssociatedWithFinancePlan(FinancePlan financePlan, int? visitPayUserId = null)
        {
            if (financePlan == null)
            {
                return false;
            }

            bool removedSomething = false;
            IReadOnlyList<Payment> payments = this._paymentRepository.Value.GetScheduledPayments(financePlan.VpGuarantor.VpGuarantorId, null);
            payments = payments.GroupBy(x => x.PaymentId).Select(x => x.First()).ToList();

            IList<PaymentPaymentProcessorResponse> responses = new List<PaymentPaymentProcessorResponse>();
            foreach (Payment payment in payments)
            {
                PaymentScheduledAmount paymentScheduledAmount = payment.PaymentScheduledAmounts.Where(x => x?.PaymentFinancePlan != null).FirstOrDefault(x => x.PaymentFinancePlan.FinancePlanId == financePlan.FinancePlanId);
                if (paymentScheduledAmount == null)
                {
                    continue;
                }
                if (!this.RemovePaymentScheduledAmount(paymentScheduledAmount, payment, visitPayUserId))
                {
                    continue;
                }

                removedSomething = true;
                responses.Add(new PaymentPaymentProcessorResponse
                {
                    InsertDate = payment.ScheduledPaymentDate,
                    Payment = payment,
                    SnapshotPaymentAmount = payment.ScheduledPaymentAmount
                });
            }

            if (!removedSomething)
            {
                return false;
            }

            if (responses.Any())
            {
                decimal total = responses.Sum(z => z.SnapshotPaymentAmount);

                PaymentProcessor paymentProcessor = this._paymentProcessorRepository.Value.GetByName(this._paymentProvider.Value.PaymentProcessorName);
                PaymentProcessorResponse paymentProcessorResponse = new PaymentProcessorResponse
                {
                    PaymentProcessorSourceKey = string.Empty,
                    PaymentProcessor = paymentProcessor,
                    PaymentProcessorResponseStatus = PaymentProcessorResponseStatusEnum.Unknown,
                    SnapshotTotalPaymentAmount = total,
                    PaymentMethod = responses.First().Payment.PaymentMethod,
                    ErrorMessage = string.Empty,
                    PaymentSystemType = PaymentSystemTypeEnum.VisitPay
                };
                this._paymentProcessorResponseRepository.Value.InsertOrUpdate(paymentProcessorResponse);

                foreach (PaymentPaymentProcessorResponse response in responses)
                {
                    response.PaymentProcessorResponse = paymentProcessorResponse;
                    response.Payment.PaymentPaymentProcessorResponses.Add(response);
                    paymentProcessorResponse.PaymentPaymentProcessorResponses.Add(response);
                    this._paymentRepository.Value.InsertOrUpdate(response.Payment);
                }
            }

            return true;
        }

        public IQueryable<Payment> GetPaymentsQueryable()
        {
            return this._paymentRepository.Value.GetQueryable();
        }

        public IReadOnlyList<Payment> GetPayments(IList<int> paymentIds, int vpGuarantorId)
        {
            return this._paymentRepository.Value.GetPayments(vpGuarantorId, paymentIds.ToList());
        }

        public IList<Payment> GetPaymentsByTransactionId(string transactionId, IList<PaymentStatusEnum> paymentStatuses = null)
        {
            return this._paymentRepository.Value.GetPaymentsFromTransactionId(transactionId, paymentStatuses);
        }

        public ScheduledPaymentResult GetScheduledPaymentResult(int vpGuarantorId, int paymentId)
        {
            IReadOnlyList<ScheduledPaymentResult> results = this._paymentRepository.Value.GetScheduledPaymentResults(vpGuarantorId, paymentId);
            return results.FirstOrDefault();
        }

        public IReadOnlyList<ScheduledPaymentResult> GetScheduledPaymentResults(int vpGuarantorId)
        {
            IReadOnlyList<ScheduledPaymentResult> results = this._paymentRepository.Value.GetScheduledPaymentResults(vpGuarantorId);
            return results;
        }

        public int GetScheduledPaymentsCountByGuarantorPaymentMethod(int vpGuarantorId)
        {
            int count = this._paymentRepository.Value.GetScheduledPaymentsCountByGuarantorPaymentMethod(vpGuarantorId);
            return count;
        }

        /// <summary>
        ///     This method is for processing scheduled payments.
        /// </summary>
        public IList<Payment> ProcessScheduledPaymentsForGuarantor(Guarantor vpGuarantor, DateTime dateToProcess, int? visitPayUserId = null)
        {
            List<Payment> paymentsCharged = new List<Payment>();
            IReadOnlyList<Payment> paymentsToCharge = this._paymentRepository.Value.GetScheduledPayments(vpGuarantor.VpGuarantorId, dateToProcess);

            if (paymentsToCharge != null)
            {
                foreach (Payment payment in paymentsToCharge)
                {
                    IEnumerable<Payment> chargedPayments = this.ChargePayment(payment, visitPayUserId).Select(x => x.Payment);
                    paymentsCharged.AddRange(chargedPayments);
                    //must be specific to Scheduled manual payments.
                }
            }

            return paymentsCharged;
        }

        /// <summary>
        ///     This method is for processing unsent recurring payments.
        /// </summary>
        public IList<Payment> ProcessUnsentRecurringPaymentsForGuarantor(Guarantor vpGuarantor, DateTime dateToProcess, int? visitPayUserId = null)
        {
            List<Payment> paymentsCharged = new List<Payment>();
            IReadOnlyList<Payment> paymentsToCharge = this._paymentRepository.Value.GetUnsentRecurringPayments(vpGuarantor.VpGuarantorId, dateToProcess);

            if (paymentsToCharge != null)
            {
                foreach (Payment payment in paymentsToCharge)
                {
                    IEnumerable<Payment> chargedPayments = this.ChargePayment(payment, visitPayUserId).Select(x => x.Payment);
                    paymentsCharged.AddRange(chargedPayments);
                }
            }

            return paymentsCharged;
        }

        /// <summary>
        ///     This method is for processing a recurring payment.
        /// </summary>
        public IList<Payment> ProcessRecurringPaymentsForGuarantor(IList<Payment> payments, Guarantor vpGuarantor, DateTime dateToProcess, int? visitPayUserId = null)
        {
            if (payments == null || payments.Count <= 0)
            {
                return new List<Payment>();
            }

            IList<Payment> processablePayments = payments.Where(x => x.PaymentStatus.IsInCategory(PaymentStatusEnumCategory.ProcessRecurring)).ToList();
            IList<Payment> paymentsCharged = this.ChargeRecurringPayments(processablePayments, visitPayUserId).Where(x => x.Payment != null).Select(x => x.Payment).ToList();

            foreach (Payment payment in processablePayments)
            {
                if (!paymentsCharged.Contains(payment))
                {
                    this.LoggingService.Value.Warn(() => $"FinanceMagement.PaymentService.ProcessRecurringPaymentsForGuarantor - payment.PaymentId == {payment.PaymentId.ToString()},  payment.PaymentStatus == {payment.PaymentStatus}");
                }
            }

            return paymentsCharged;
        }

        public void SchedulePayment(Payment paymentEntity, int? visitPayUserId = null)
        {
            this.SavePayment(paymentEntity, PaymentStatusEnum.ActivePending, visitPayUserId);
            this._metricsProvider.Value.Increment(SchedulePaymentMetricKey);
        }

        public void SavePendingGuarantorResponsePayment(Payment paymentEntity, int? visitPayUserId = null)
        {
            this.SavePayment(paymentEntity, PaymentStatusEnum.PaymentPendingGuarantorResponse, visitPayUserId);
            this._metricsProvider.Value.Increment(PaymentPendingResponseMetricKey);
        }

        private void SavePayment(Payment paymentEntity, PaymentStatusEnum paymentStatus, int? visitPayUserId = null)
        {
            if (paymentEntity.PaymentMethod == null)
            {
                throw new Exception("No payment method was supplied");
            }
            if (paymentEntity.Guarantor == null)
            {
                throw new Exception("No guarantor was supplied.");
            }
            paymentEntity.PaymentStatus = paymentStatus;

            this._paymentRepository.Value.InsertOrUpdate(paymentEntity);
            this._paymentEventService.Value.AddPaymentEvent(visitPayUserId, paymentEntity);
        }

        public IList<ChargePaymentResponse> ChargePayment(Payment paymentEntity, int? visitPayUserId = null)
        {
            return this.ChargeRecurringPayments(paymentEntity.ToListOfOne(), visitPayUserId);
        }

        public IList<ChargePaymentResponse> ChargeRecurringPayments(IList<Payment> payments, int? visitPayUserId = null)
        {
            if (payments.IsNullOrEmpty())
            {
                return new ChargePaymentResponse(false, null).ToListOfOne();
            }

            // need to make sure the payment isnt closed, if we call allocate payments on a closed payment it'll delete all allocations, which is bad
            List<Payment> orderedPayments = payments.Where(x => x.IsRecurringPayment() && x.IsActive)
                .OrderByDescending(x => x.PaymentScheduledAmounts.First()
                    .PaymentFinancePlan.CurrentBucket)
                .ToList();
            orderedPayments.AddRange(payments.Where(x => x.IsNotRecurringPayment() && x.IsActive));

            if (orderedPayments.Count == 0)
            {
                // if all of the payment are not active, don't try to submit again, just return what the result was
                return payments.Select(x => new ChargePaymentResponse(x.WasSuccessful(), x)).ToList();
            }

            foreach (Payment payment in orderedPayments)
            {
                if (payment.Guarantor == null)
                {
                    throw new Exception(TextRegionConstants.PaymentValidationInvalidGuarantor);
                }

                if (payment.PaymentMethod == null)
                {
                    throw new Exception(TextRegionConstants.PaymentValidationInvalidPaymentMethod);
                }
                if (payment.IsClosed)
                {
                    return new ChargePaymentResponse(payment.WasSuccessful(), payment).ToListOfOne();
                }
            }

            if (orderedPayments.Any(x => x.IsPromptPayType()))
            {
                this._metricsProvider.Value.Increment(PromptPaymentMetricKey, orderedPayments.Count(x => x.IsPromptPayType()));
            }

            IList<ChargePaymentResponse> paymentsCharged = new List<ChargePaymentResponse>();
            if (payments.All(x => x.PaymentMethod.IsAchType || x.PaymentMethod.IsCardType || x.PaymentMethod.IsLockBoxType))
            {
                IList<PaymentAllocationResult> results = this._paymentAllocationService.Value.AllocatePayments(orderedPayments);
                foreach (PaymentAllocationResult paymentAllocationResult in results)
                {
                    paymentAllocationResult.CommitAllocationsToPayment();
                }
                foreach (Payment payment in orderedPayments)
                {
                    this._paymentRepository.Value.InsertOrUpdate(payment);
                }

                orderedPayments = this.ProcessAndRecordPayment(orderedPayments, visitPayUserId).ToList();
                bool response = orderedPayments.All(x => x.WasSuccessful());
                foreach (Payment payment in orderedPayments)
                {
                    paymentsCharged.Add(new ChargePaymentResponse(response, payment));
                }
            }
            else
            {
                string paymentIDs = string.Join(",", orderedPayments.Select(p => p.PaymentId.ToString()));
                this.LoggingService.Value.Fatal(() => $"PaymentService::ChargeRecurringPayments Payments not processed -VPUserID: {visitPayUserId} PaymentIds: {paymentIDs}");
            }

            // post payment steps (success, failure)
            if (paymentsCharged.Count > 0)
            {
                this.PostPaymentStep(paymentsCharged.Select(x => x.Payment).ToList(), !paymentsCharged.Last().Success);
            }
            else
            {
                this.LoggingService.Value.Fatal(() => $"PaymentService::ChargeRecurringPayments No payments to process for PostPaymentStep -VPUserID: {visitPayUserId}");
            }

            return paymentsCharged;
        }

        public void ProcessOptimisticAchSettlement(Payment payment)
        {
            bool hasOptimisticSettlementEnabled = this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureOptimisticAchSettlementIsEnabled);

            if (hasOptimisticSettlementEnabled && payment.PaymentStatus == PaymentStatusEnum.ActivePendingACHApproval && payment.PaymentMethod.IsAchType)
            {
                string guarantorName = payment.Guarantor?.User?.FirstNameLastName ?? string.Empty;
                int achSettlementDetailId = this._achService.Value.DoOptimisticSettlement(guarantorName, payment.TransactionId.TrimNullSafe(), payment.ActualPaymentAmount);

                AchSettlementMessage achSettlementMessage = new AchSettlementMessage();

                try
                {
                    achSettlementMessage = new AchSettlementMessage()
                    {
                        TransactionId = payment.TransactionId.TrimNullSafe(),
                        AchSettlementDetailId = achSettlementDetailId
                    };

                    this.Bus.Value.PublishMessage(new AchSettlementMessageGuarantor
                    {
                        VpGuarantorId = payment.Guarantor.VpGuarantorId,
                        Messages = achSettlementMessage.ToListOfOne()
                    }).Wait();
                }
                catch (Exception e)
                {
                    this.LoggingService.Value.Fatal(() => $"PaymentService.ProcessOptimisticAchSettlement::Failed to publish {typeof(AchSettlementMessageGuarantor).FullName}, Message = {JsonConvert.SerializeObject(achSettlementMessage)}, Exception = {e.AggregateExceptionToString()}");
                }
            }
        }

        public void FailPayment(Payment payment, string comment = null)
        {
            payment.PaymentStatus = PaymentStatusEnum.ClosedFailed;
            this._paymentRepository.Value.InsertOrUpdate(payment);
            this._paymentEventService.Value.AddPaymentEvent(null, payment, comment);
        }

        public void CancelPayment(int vpGuarantorId, IList<int> paymentIds, int? visitPayUserId = null, string reason = null)
        {
            List<Payment> payments = this.GetPayments(paymentIds, vpGuarantorId).ToList();
            if (!payments.Any())
            {
                return;
            }

            this.CancelPayment(payments, visitPayUserId, reason);
        }

        public void CancelAllPaymentsForGuarantor(Guarantor vpGuarantor, int? visitPayUserId = null)
        {
            IReadOnlyList<Payment> payments = this._paymentRepository.Value.GetScheduledPayments(vpGuarantor.VpGuarantorId, null);
            foreach (Payment payment in payments)
            {
                this.CancelPayment(vpGuarantor.VpGuarantorId, new List<int> { payment.PaymentId }, visitPayUserId);
            }
        }

        public void CancelPaymentsForGuarantor(Guarantor vpGuarantor, List<PaymentStatusEnum> paymentStatuses, int? visitPayUserId = null)
        {
            IReadOnlyList<Payment> payments = this._paymentRepository.Value.GetPayments(vpGuarantor.VpGuarantorId, paymentStatuses);
            foreach (Payment payment in payments)
            {
                this.CancelPayment(vpGuarantor.VpGuarantorId, new List<int> { payment.PaymentId }, visitPayUserId);
            }
        }

        public void CancelPaymentsForGuarantor(Guarantor vpGuarantor, List<PaymentTypeEnum> paymentTypes, int? visitPayUserId = null)
        {
            IReadOnlyList<Payment> payments = this._paymentRepository.Value.GetPayments(vpGuarantor.VpGuarantorId, paymentTypes).Where(x => x.IsActive).ToList();
            foreach (Payment payment in payments)
            {
                this.CancelPayment(vpGuarantor.VpGuarantorId, new List<int> { payment.PaymentId }, visitPayUserId);
            }
        }

        public void CancelRecurringPayment(Guarantor guarantor, int actionVisitPayUserId, IList<CancelRecurringPaymentDto> cancelRecurringPaymentDtos)
        {
            IList<Payment> payments = new List<Payment>(cancelRecurringPaymentDtos.Count);
            foreach (CancelRecurringPaymentDto cancelRecurringPaymentDto in cancelRecurringPaymentDtos)
            {
                Payment payment = this.CreateRecurringPayment(cancelRecurringPaymentDto.FinancePlanId, guarantor, cancelRecurringPaymentDto.Amount, PaymentStatusEnum.ClosedCancelled, cancelRecurringPaymentDto.DueDate);
                payments.Add(payment);
            }

            this.CancelPayment(payments, actionVisitPayUserId, "Cancelled Recurring Payment");
        }

        public Payment UpdatePayment(Guarantor vpGuarantor, Payment payment, int? actionVisitPayUserId = null, string paymentEventComment = null)
        {
            this._paymentRepository.Value.InsertOrUpdate(payment);
            this._paymentEventService.Value.AddPaymentEvent(actionVisitPayUserId, payment, paymentEventComment);
            return payment;
        }

        public IEnumerable<Payment> GetScheduledPayments(int vpGuarantorId)
        {
            IReadOnlyList<Payment> payments = this._paymentRepository.Value.GetScheduledPayments(vpGuarantorId, null);
            return payments;
        }

        public IReadOnlyList<PaymentProcessorResponse> GetPaymentHistory(PaymentProcessorResponseFilter filter)
        {
            return this._paymentRepository.Value.GetPaymentHistory(filter);
        }

        public PaymentProcessorResponse GetPaymentProcessorResponse(int paymentProcessorResponseId, int vpGuarantorId)
        {
            PaymentProcessorResponse paymentProcessorResponse = this._paymentProcessorResponseRepository.Value.GetById(paymentProcessorResponseId);
            if (paymentProcessorResponse.Payments.All(x => x.Guarantor.VpGuarantorId == vpGuarantorId))
            {
                return paymentProcessorResponse;
            }
            return null;
        }

        public void FixPaymentAllocations(int paymentId)
        {
            using (IUnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                Payment loadPayment = this._paymentRepository.Value.GetById(paymentId);

                //Get all associated payments
                IEnumerable<Payment> paymentsFromResponses = loadPayment.PaymentPaymentProcessorResponses.Select(x => x.Payment);
                IList<Payment> payments = paymentsFromResponses as IList<Payment> ?? paymentsFromResponses.ToList();

                if (payments.Any(x => x.PaymentAllocations.Count > 0))
                {
                    this.LoggingService.Value.Warn(() => $"PaymentService::FixPaymentAllocations - This method shouldnt have been called as some of these payments have allocations - PaymentId = {paymentId}");
                    return;
                }

                IList<PaymentAllocationResult> results = this._paymentAllocationService.Value.AllocatePayments(payments);
                foreach (PaymentAllocationResult paymentAllocationResult in results)
                {
                    paymentAllocationResult.CommitAllocationsToPayment();
                }
                //save them
                foreach (Payment payment in payments)
                {
                    this._paymentRepository.Value.InsertOrUpdate(payment);
                }

                this.PostPaymentSuccessStep(payments);

                foreach (Payment payment in payments)
                {
                    switch (payment.PaymentStatus)
                    {
                        case PaymentStatusEnum.ActiveGatewayError:
                        case PaymentStatusEnum.ActivePending:
                        case PaymentStatusEnum.ActivePendingGuarantorAction:
                        case PaymentStatusEnum.ActivePendingLinkFailure:
                        case PaymentStatusEnum.ClosedCancelled:
                        case PaymentStatusEnum.ClosedFailed:
                            this.PostPaymentFailureStep(payment);
                            break;
                        default:
                            this.LoggingService.Value.Info(() => $"PaymentService::FixPaymentAllocations - Payment wasnt in ClosedPaid or ActivePandingACHApproval so PostPaymentSuccessStep was not called - PaymentId = {paymentId}");
                            break;
                    }

                    this._paymentRepository.Value.InsertOrUpdate(payment);
                    this._paymentEventService.Value.AddPaymentEvent(SystemUsers.SystemUserId, payment, "Fixing payment allocations because of system error");
                }
                unitOfWork.Commit();
            }
        }

        public void SetGuarantorCure(int vpGuarantorId, IList<int> paymentIds)
        {
            if (paymentIds == null || !paymentIds.Any())
            {
                return;
            }

            foreach (int paymentId in paymentIds)
            {
                Payment payment = this._paymentRepository.Value.GetPayment(vpGuarantorId, paymentId);
                payment.IsGuarantorCure = true;

                this._paymentRepository.Value.InsertOrUpdate(payment);
            }
        }

        public IList<PaymentStatus> GetPaymentStatuses()
        {
            return this._paymentRepository.Value.GetPaymentStatuses();
        }

        public PaymentTypeEnum AdjustPaymentType(PaymentTypeEnum paymentType, DateTime scheduledPaymentDate)
        {
            DateTime clientNow = this._timeZoneHelper.Value.UtcToClient(DateTime.UtcNow);

            if (scheduledPaymentDate.Date == clientNow.Date)
            {
                switch (paymentType)
                {
                    case PaymentTypeEnum.ManualScheduledCurrentBalanceInFull:
                        return PaymentTypeEnum.ManualPromptCurrentBalanceInFull;

                    case PaymentTypeEnum.ManualScheduledCurrentNonFinancedBalanceInFull:
                        return PaymentTypeEnum.ManualPromptCurrentNonFinancedBalanceInFull;

                    case PaymentTypeEnum.ManualScheduledSpecificAmount:
                        return PaymentTypeEnum.ManualPromptSpecificAmount;

                    case PaymentTypeEnum.ManualScheduledSpecificFinancePlans:
                        return PaymentTypeEnum.ManualPromptSpecificFinancePlans;

                    case PaymentTypeEnum.ManualScheduledSpecificVisits:
                        return PaymentTypeEnum.ManualPromptSpecificVisits;
                }
            }

            if (scheduledPaymentDate.Date > clientNow.Date)
            {
                switch (paymentType)
                {
                    case PaymentTypeEnum.ManualPromptCurrentBalanceInFull:
                        return PaymentTypeEnum.ManualScheduledCurrentBalanceInFull;

                    case PaymentTypeEnum.ManualPromptCurrentNonFinancedBalanceInFull:
                        return PaymentTypeEnum.ManualScheduledCurrentNonFinancedBalanceInFull;

                    case PaymentTypeEnum.ManualPromptSpecificAmount:
                        return PaymentTypeEnum.ManualScheduledSpecificAmount;

                    case PaymentTypeEnum.ManualPromptSpecificFinancePlans:
                        return PaymentTypeEnum.ManualScheduledSpecificFinancePlans;

                    case PaymentTypeEnum.ManualPromptSpecificVisits:
                        return PaymentTypeEnum.ManualScheduledSpecificVisits;
                }
            }

            return paymentType;
        }

        IEnumerable<SystemException> ISystemExceptionProvider.GetExceptions(DateTime exceptionsAsOfDateTime)
        {
            DateTime threshholdDateTime = exceptionsAsOfDateTime.AddDays(Math.Abs(this.Client.Value.MaxDaysInPendingAchApproval) * -1);

            foreach (Payment payment in this._paymentRepository.Value.GetPaymentsSubmittedOnDateInStatus(threshholdDateTime, new[] { PaymentStatusEnum.ActivePendingACHApproval }))
            {
                yield return new SystemException
                {
                    SystemExceptionType = SystemExceptionTypeEnum.AchConfirmationNotReceived,
                    SystemExceptionDescription = "Payment TransactionId: " + payment.TransactionId,
                    TransactionId = payment.TransactionId,
                    PaymentId = payment.PaymentId,
                    VpGuarantorId = payment.Guarantor.VpGuarantorId
                };
            }
            // new payment related system exceptions can be logged here with additional yield returns
        }

        private void CancelPayment(IList<Payment> payments, int? actionVisitPayUserId = null, string reason = null)
        {
            if (payments.IsNullOrEmpty())
            {
                return;
            }

            PaymentProcessorResponse paymentProcessorResponse = null;

            foreach (Payment payment in payments.Where(x => x.PaymentPaymentProcessorResponses.Where(y => y.PaymentProcessorResponse != null && y.PaymentProcessorResponse.WasSuccessful()).IsNullOrEmpty()))
            {
                paymentProcessorResponse = this.AddDummyPaymentProcessorResponse(payments.Sum(x => x.ScheduledPaymentAmount), payments.First().PaymentMethod, paymentProcessorResponse, payment);

                payment.PaymentStatus = PaymentStatusEnum.ClosedCancelled;
                this._paymentRepository.Value.InsertOrUpdate(payment);
                this._paymentEventService.Value.AddPaymentEvent(actionVisitPayUserId, payment, reason);
            }
        }

        public PaymentProcessorResponse AddDummyPaymentProcessorResponse(decimal snapshotTotalPaymentAmount, PaymentMethod paymentMethod, PaymentProcessorResponse paymentProcessorResponse, Payment payment, PaymentProcessorResponseStatusEnum status = PaymentProcessorResponseStatusEnum.Unknown)
        {
            if (paymentProcessorResponse == null)
            {
                PaymentProcessor paymentProcessor = this._paymentProcessorRepository.Value.GetByName(this._paymentProvider.Value.PaymentProcessorName);

                paymentProcessorResponse = new PaymentProcessorResponse
                {
                    PaymentProcessorSourceKey = string.Empty,
                    PaymentProcessor = paymentProcessor,
                    PaymentProcessorResponseStatus = status,
                    SnapshotTotalPaymentAmount = snapshotTotalPaymentAmount,
                    PaymentMethod = paymentMethod,
                    ErrorMessage = string.Empty,
                    PaymentSystemType = PaymentSystemTypeEnum.VisitPay
                };
                this._paymentProcessorResponseRepository.Value.InsertOrUpdate(paymentProcessorResponse);
            }

            PaymentPaymentProcessorResponse current = new PaymentPaymentProcessorResponse
            {
                InsertDate = payment.ScheduledPaymentDate,
                Payment = payment,
                PaymentProcessorResponse = paymentProcessorResponse,
                SnapshotPaymentAmount = payment.ScheduledPaymentAmount
            };

            payment.PaymentPaymentProcessorResponses.Add(current);
            paymentProcessorResponse.PaymentPaymentProcessorResponses.Add(current);
            this._paymentRepository.Value.InsertOrUpdate(payment);
            return paymentProcessorResponse;
        }

        public Payment GetMostRecentTextToPayPaymentForGuarantor(int vpGuarantorId)
        {
            List<Payment> payments = this._paymentRepository.Value.GetQueryable()
                .Where(x => x.Guarantor.VpGuarantorId == vpGuarantorId)
                .Where(x => x.CurrentPaymentStatus == PaymentStatusEnum.PaymentPendingGuarantorResponse)
                .Where(x => x.ActualPaymentAmount == 0m)//prevent multiple "pay" texts from trying to process a processed payment
                .OrderByDescending(x => x.InsertDate)
                .ToList();

            return payments.FirstOrDefault(x => x.IsActive);
        }

        private IList<Payment> ProcessAndRecordPayment(IList<Payment> payments, int? visitPayUserId)
        {
            if (payments == null)
            {
                throw new ArgumentNullException(nameof(payments));
            }

            // validate that current payment method belongs to guarantor or to consolidationGuarantor ManagingGuarantor
            foreach (Payment payment in payments.Where(x => x.PaymentMethod != null && x.PaymentMethod.VpGuarantorId != x.Guarantor.VpGuarantorId))
            {
                if (payment.Guarantor.IsClosed)
                {
                    throw new InvalidOperationException("Guarantor account is closed. Guarantor ID: " + payment.Guarantor.VpGuarantorId);
                }

                if (!payment.Guarantor.IsManaged()
                    || !payment.Guarantor.ManagingGuarantorId.HasValue
                    || !payment.PaymentMethod.VpGuarantorId.HasValue
                    || payment.PaymentMethod.VpGuarantorId.Value != payment.Guarantor.ManagingGuarantorId.Value)
                {
                    throw new InvalidOperationException("Payment Method does not belong to Guarantor or to Managing Guarantor.");
                }
            }

            if (this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.MerchantAccountSplittingIsEnabled))
            {
                int paymentCount = payments.Count;
                IList<Payment> splitPayments = this._merchantAccountAllocationService.Value.SplitPayments(payments);

                if (splitPayments != null)
                {
                    if (splitPayments.Count > paymentCount)
                    {
                        Guid splitPaymentGroupingGuid = Guid.NewGuid();

                        //update existing payment objects with altered amount and child objects
                        for (int i = payments.Count - 1; i >= 0; i--)
                        {
                            this._merchantAccountAllocationService.Value.UpdatePaymentFromSplitPayment(payments[i], splitPayments[i]);
                            splitPayments.RemoveAt(i);
                        }

                        payments.AddRange(splitPayments);

                        foreach (Payment payment in payments)
                        {
                            payment.SplitPaymentGroupingGuid = splitPaymentGroupingGuid;
                            this._paymentRepository.Value.InsertOrUpdate(payment);
                        }
                    }
                }
                else
                {
                    payments = splitPayments; // todo: expression is always null?
                }
            }

            IDictionary<int, string> errorMessages = new Dictionary<int, string>();
            try
            {
                if (payments.Any(x => x.IsPromptPayType() && x.ActualPaymentAmount != x.ScheduledPaymentAmount))
                {
                    //If the scheduled amount doesnt match the actual amount then we know that a transaction from the hospital was recieved while processing this
                    foreach (Payment x in payments)
                    {
                        x.PaymentStatus = PaymentStatusEnum.ClosedCancelled;
                    }

                    //Since we're giving up before going on we need to remove the payment allocations etc.
                    this.ResetPaymentBeforeAllocated(payments);
                    return payments;
                }

                //Not having any payment allocations is fine if there is no payment amount associated with it.
                if (payments.Any(x => x.PaymentAllocations.Count == 0 && x.ActualPaymentAmount != 0m))
                {
                    this.CancelPayment(payments.Where(payment => payment.PaymentAllocations.Count == 0m && payment.ActualPaymentAmount != 0m).ToList(), null, "One or more of the payments werent allocated.");
                    this.LoggingService.Value.Info(() => $"PaymentService::ProcessAndRecordPayment - There was a payment without payment allocations.  StackTrace = {Environment.StackTrace}");
                    //Since we're giving up before going on we need to remove the payment allocations.
                    this.ResetPaymentBeforeAllocated(payments);
                    return payments;
                }

                //The provider should be adding the response to the paymentEntity.PaymentProcessorResponses and then returning the same value it added as the return value
                PaymentProcessor paymentProcessor = this._paymentProcessorRepository.Value.GetByName(this._paymentProvider.Value.PaymentProcessorName);

                //dont try to charge if there's no thing to charge.
                IList<PaymentProcessorResponse> responses = null;

                if (payments.Sum(x => x.ActualPaymentAmount) > 0m)
                {
                    try
                    {
                        responses = new List<PaymentProcessorResponse>();

                        HandleNullPaymentMethods(payments.Where(x => x.PaymentMethod == null).ToList(), paymentProcessor, responses);

                        List<Payment> paymentsWithProcessorPaymentMethods = payments.Where(x => x.PaymentMethod != null && !x.PaymentMethod.IsLockBoxType).ToList();
                        IList<PaymentProcessorResponse> paymentProcessorResponses = this._paymentProvider.Value.SubmitPaymentsAsync(paymentsWithProcessorPaymentMethods, paymentProcessor).Result;
                        responses.AddRange(paymentProcessorResponses);

                        List<Payment> paymentsWithLockBoxPaymentMethods = payments.Where(x => x.PaymentMethod != null && x.PaymentMethod.IsLockBoxType).ToList();
                        IList<PaymentProcessorResponse> lockBoxPaymentProcessorResponses = new List<PaymentProcessorResponse>();
                        foreach (Payment lockBoxPayment in paymentsWithLockBoxPaymentMethods)
                        {
                            PaymentProcessorResponse paymentProcessorResponse = this.AddDummyPaymentProcessorResponse(
                                snapshotTotalPaymentAmount: lockBoxPayment.ActualPaymentAmount,
                                paymentMethod: lockBoxPayment.PaymentMethod,
                                paymentProcessorResponse: null,
                                payment: lockBoxPayment,
                                status: PaymentProcessorResponseStatusEnum.Approved
                                );
                            lockBoxPaymentProcessorResponses.Add(paymentProcessorResponse);
                        }
                        responses.AddRange(lockBoxPaymentProcessorResponses);

                        if (responses.Any())
                        {
                            foreach (PaymentProcessorResponse response in responses.OrderByDescending(x => x.InsertDate).ThenByDescending(x => x.PaymentProcessorResponseId))
                            {

                                // If there is more than one paymentResponse in paymentResponseList, 
                                // erroneous additional INSERT statements for PaymentPaymentProcessorResponse are created after transaction.Commit() 
                                // when session.Flush() is invoked.
                                // The flush mode is being set to NEVER before InsertOrUpdate(paymentResponse) and restored back to AUTO
                                // to get the desired behavior of "INSERT PaymentProcessorResponse --> INSERT PaymentPaymentProcessorResponse"
                                // per paymentResponse in paymentResponseList.
                                if (responses.Count > 1)
                                {
                                    try
                                    {
                                        this._session.FlushMode = FlushMode.Manual;
                                        this._paymentProcessorResponseRepository.Value.InsertOrUpdate(response);
                                    }
                                    finally
                                    {
                                        this._session.FlushMode = FlushMode.Auto;
                                    }
                                }
                                else
                                {
                                    this._paymentProcessorResponseRepository.Value.InsertOrUpdate(response);
                                }

                                foreach (Payment responsePayment in response.Payments)
                                {
                                    if (!errorMessages.ContainsKey(responsePayment.PaymentId))
                                    {
                                        errorMessages.Add(responsePayment.PaymentId, "");
                                    }

                                    errorMessages[responsePayment.PaymentId] = response.RawResponse;
                                    if (responsePayment.IsPromptPayType())
                                    {
                                        this.SetStatusOfPaymentForPromptPayment(response, responsePayment);
                                    }
                                    else
                                    {
                                        this.SetStatusOfPaymentForScheduledPayment(response, responsePayment);
                                    }
                                }
                            }
                        }
                        else
                        {
                            foreach (Payment payment in payments)
                            {
                                payment.PaymentStatus = payment.IsPromptPayType() ? PaymentStatusEnum.ClosedFailed : PaymentStatusEnum.ActivePendingLinkFailure;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        this.LoggingService.Value.Fatal(() => $"PaymentService::ProcessAndRecordPayment: Exception: {ExceptionHelper.AggregateExceptionToString(ex)}");

                        foreach (Payment payment in payments)
                        {
                            payment.PaymentStatus = payment.IsPromptPayType() ? PaymentStatusEnum.ClosedFailed : PaymentStatusEnum.ActivePendingLinkFailure;

                            this.Bus.Value.PublishMessage(new SystemExceptionMessage
                            {
                                SystemExceptionType = SystemExceptionTypeEnum.PaymentProcessingException,
                                PaymentId = payment.PaymentId,
                                VpGuarantorId = payment.Guarantor.VpGuarantorId,
                                SystemExceptionDescription = "System exception during payment",
                                TransactionId = (payment.TransactionId.IsNotNullOrEmpty() ?
                                    payment.TransactionId :
                                    responses.IsNotNullOrEmpty() ? responses.First(y => y.Payments.Select(z => z.PaymentId).Contains(payment.PaymentId)).PaymentProcessorSourceKey : string.Empty)
                            }).Wait();
                        }
                    }
                }

                if (responses == null)
                {
                    this.CancelPayment(payments.Where(payment => payments.Sum(x => x.ActualPaymentAmount) <= 0m).ToList(), null, "Sum of ActualPaymentAmount <= 0");
                    this.CancelPayment(payments.Where(payment => payments.Sum(x => x.ActualPaymentAmount) > 0m).ToList(), null, "Payment Not Successful");
                }
            }
            catch
            {
                foreach (Payment payment in payments)
                {
                    payment.PaymentStatus = payment.IsPromptPayType() ? PaymentStatusEnum.ClosedFailed : PaymentStatusEnum.ActivePendingLinkFailure;
                }
            }

            // save payments so any status changes save
            foreach (Payment payment in payments)
            {
                this._paymentRepository.Value.InsertOrUpdate(payment);
            }

            // save payment events
            foreach (Payment payment in payments)
            {
                string errorMessage = errorMessages.ContainsKey(payment.PaymentId) ? errorMessages[payment.PaymentId] : null;
                this._paymentEventService.Value.AddPaymentEvent(visitPayUserId, payment, errorMessage);
            }

            return payments;
        }

        private void PostPaymentStep(IList<Payment> payments, bool runFailureStep)
        {
            using (IUnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                this.PostPaymentSuccessStep(payments);

                this._session.Transaction.RegisterPostCommitSuccessAction(p1 =>
                {
                    foreach (Payment payment in p1)
                    {
                        this.ProcessOptimisticAchSettlement(payment);
                    }

                }, payments);

                foreach (Payment payment in payments)
                {
                    payment.PaymentAttemptCount++;
                    switch (payment.PaymentStatus)
                    {
                        case PaymentStatusEnum.ActiveGatewayError:
                        case PaymentStatusEnum.ActivePending:
                        case PaymentStatusEnum.ActivePendingGuarantorAction:
                        case PaymentStatusEnum.ActivePendingLinkFailure:
                        case PaymentStatusEnum.ClosedCancelled:
                        case PaymentStatusEnum.ClosedFailed:
                            //Since the payment failed the current allocation of funds isn't relevant.
                            if (runFailureStep)
                            {
                                this.PostPaymentFailureStep(payment);
                            }
                            this._paymentAllocationService.Value.RemovePaymentAllocation(payment);

                            //We want to keep the Actual payment amount 
                            if (!payment.IsPromptPayType() || payment.IsClosed)
                            {
                                payment.ActualPaymentAmount = 0m;
                                payment.ActualPaymentDate = null;
                            }
                            break;
                    }

                    this._paymentRepository.Value.InsertOrUpdate(payment);

                    if (payment.IsLinkFailure())
                    {
                        this._systemExceptionService.Value.LogSystemException(new SystemException
                        {
                            SystemExceptionType = SystemExceptionTypeEnum.PaymentActivePendingLinkFailure,
                            SystemExceptionDescription = "PaymentActivePendingLinkFailure",
                            PaymentId = payment.PaymentId,
                            VpGuarantorId = payment.Guarantor.VpGuarantorId
                        });
                    }
                }

                unitOfWork.Commit();
            }
        }

        private void PostPaymentFailureStep(Payment payment)
        {
            if (!payment.PaymentAllocations.Any(x => x.FinancePlanId.HasValue))
            {
                return;
            }

            IList<int> financePlanIds = payment.PaymentAllocations.Where(x => x.FinancePlanId.HasValue).Select(x => x.FinancePlanId.Value).Distinct().ToList();
            foreach (FinancePlan financePlan in this._financePlanService.Value.GetFinancePlansById(financePlanIds))
            {
                FinancePlanAmountDue lastStatementAmountDue = financePlan.FinancePlanAmountsDue.OrderByDescending(x => x.DueDate).FirstOrDefault(x => x.DueDate != null && x.VpStatementId.HasValue && x.AmountDue > 0);
                financePlan.AddFinancePlanAmountsDueFailedPayment(lastStatementAmountDue?.DueDate ?? DateTime.UtcNow, payment.PaymentId, "Payment Failed");
                this._financePlanService.Value.UpdateFinancePlan(financePlan);
            }
        }

        private void PostPaymentSuccessStep(IList<Payment> allPayments)
        {
            IList<Payment> successfulPayments = allPayments.Where(x => x.PaymentStatus == PaymentStatusEnum.ClosedPaid || x.PaymentStatus == PaymentStatusEnum.ActivePendingACHApproval).ToList();
            IList<PaymentAllocation> paymentAllocations = successfulPayments.SelectMany(x => x.PaymentAllocations).ToList();

            IList<FinancePlan> financePlans = new List<FinancePlan>();
            if (paymentAllocations.Any(x => x.FinancePlanId.HasValue))
            {
                IList<int> financePlanIds = paymentAllocations.Where(x => x.FinancePlanId.HasValue).Select(x => x.FinancePlanId.Value).Distinct().ToList();
                financePlans = this._financePlanService.Value.GetFinancePlansById(financePlanIds);
            }

            foreach (PaymentAllocation paymentAllocation in paymentAllocations)
            {
                if (paymentAllocation.Payment.ActualPaymentDate == null)
                {
                    continue;
                }

                //todo: Would it make sense to have this live in the consumer: VisitBalanceChangedMessageList, the other types of payments are handled there
                if (!paymentAllocation.FinancePlanId.HasValue)
                {
                    continue;
                }

                FinancePlan financePlan = financePlans.FirstOrDefault(x => x.FinancePlanId == paymentAllocation.FinancePlanId.Value);
                if (financePlan == null)
                {
                    continue;
                }

                DateTime dueDate = (paymentAllocation.Payment.IsManuallyRescheduledToEarlierDate() ? paymentAllocation.Payment.OriginalScheduledPaymentDate : null) ?? DateTime.UtcNow;

                financePlan.OnPaymentAllocation(decimal.Negate(paymentAllocation.ActualAmount), dueDate, paymentAllocation, "Payment Processed");

            }

            if (financePlans.IsNotNullOrEmpty())
            {
                // check if the payment paid the entire finance plan balance
                // if so, remove any remaining balance in it's buckets
                List<IGrouping<int?, PaymentAllocation>> allocationsByFinancePlan = paymentAllocations.Where(x => x.FinancePlanId.HasValue).GroupBy(x => x.FinancePlanId).ToList();
                foreach (IGrouping<int?, PaymentAllocation> grouping in allocationsByFinancePlan)
                {
                    int? financePlanId = grouping.Key;
                    decimal sumOfAllocations = grouping.Sum(x => x.ActualAmount);

                    FinancePlan financePlan = financePlans.FirstOrDefault(x => x.FinancePlanId == financePlanId.GetValueOrDefault(0));
                    if (financePlan == null)
                    {
                        continue;
                    }

                    if (sumOfAllocations > 0m && financePlan.CurrentFinancePlanBalance <= 0m)
                    {
                        // this will remove any buckets - does not set any status
                        financePlan.OnFinancePlanClose("Balance went to zero");
                    }
                }

                // update finance plan status
                this._financePlanService.Value.CheckIfFinancePlansAreStillPastDueOrUncollectable(financePlans);
                foreach (FinancePlan financePlan in financePlans)
                {
                    financePlan.AddFinancePlanBucketHistory("Payment Processed");
                    this._financePlanService.Value.UpdateFinancePlan(financePlan);
                }
            }

            // payments towards visits on pending finance plans won't have FinancePlan allocations
            // need to insert PrincipalAmounts and confirm the pending finance plan is still valid
            this._financePlanService.Value.UpdatePendingFinancePlanPrincipalAmount(paymentAllocations);

            foreach (Payment payment in successfulPayments)
            {
                this.PublishOutboundPaymentAllocationMessages(payment);
                this.PublishOutboundPaymentMessage(payment);
                this.PublishBalanceTransferFinancePlanPaymentMessage(payment);
            }

            List<Guarantor> guarantors = successfulPayments.Select(x => x.Guarantor).GroupBy(x => x.VpGuarantorId).Select(x => x.First()).ToList();
            this._financePlanService.Value.CheckIfReconfigurationIsStillValid(guarantors);
        }

        private void ResetPaymentBeforeAllocated(IList<Payment> payments)
        {
            foreach (Payment payment in payments)
            {
                this._paymentAllocationService.Value.RemovePaymentAllocation(payment);
                payment.ActualPaymentAmount = 0m;
                payment.ActualPaymentDate = null;
                this._paymentRepository.Value.InsertOrUpdate(payment);
            }
        }

        private bool RemovePaymentScheduledAmount(PaymentScheduledAmount paymentScheduledAmount, Payment payment, int? visitPayUserId = null)
        {
            bool removedSomething = false;
            if (paymentScheduledAmount != null)
            {
                payment.PaymentScheduledAmounts.Remove(paymentScheduledAmount);
                if (payment.ScheduledPaymentAmount <= 0)
                {
                    if (!payment.IsClosed)
                    {
                        payment.PaymentStatus = PaymentStatusEnum.ClosedCancelled;
                    }
                }
                this._paymentRepository.Value.InsertOrUpdate(payment);
                this._paymentEventService.Value.AddPaymentEvent(visitPayUserId, payment, "RemovePaymentScheduledAmount");
                removedSomething = true;
            }
            return removedSomething;
        }

        private void PublishOutboundPaymentAllocationMessages(Payment payment, AchSettlementTypeEnum achSettlementType = AchSettlementTypeEnum.None)
        {
            if (payment.ActualPaymentDate != null
                && payment.PaymentStatus == PaymentStatusEnum.ClosedPaid
                && payment.PaymentAllocations.Any())
            {
                this._paymentAllocationService.Value.PublishPaymentAllocationMessages(payment, false, achSettlementType);
            }
        }

        private void PublishOutboundPaymentMessage(Payment payment, AchSettlementTypeEnum achSettlementType = AchSettlementTypeEnum.None)
        {
            bool hasOptimisticSettlementEnabled = this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureOptimisticAchSettlementIsEnabled);
            bool isAchFileReturn = achSettlementType == AchSettlementTypeEnum.FileReturn;
            bool hasActualPaymentDate = payment.ActualPaymentDate != null;

            bool canPublish = hasActualPaymentDate || (hasOptimisticSettlementEnabled && isAchFileReturn);

            if (canPublish)
            {
                this._session.Transaction.RegisterPostCommitSuccessAction(p1 =>
                {
                    PaymentMessage message = null;
                    try
                    {
                        message = new PaymentMessage
                        {
                            VpGuarantorId = p1.Guarantor.VpGuarantorId,
                            VpPaymentId = p1.PaymentId,
                            VpPaymentMethodTypeId = (int)p1.PaymentMethod.PaymentMethodType,
                            VpPaymentStatusId = (int)p1.PaymentStatus,
                            VpPaymentTypeId = (int)p1.PaymentType,
                            FullPaymentAmount = p1.ActualPaymentAmount,
                            ActualPaymentDate = p1.ActualPaymentDate,
                            AchSettlementType = achSettlementType

                        };
                        if (p1.ParentPayment != null)
                        {
                            message.OffsetPaymentId = p1.ParentPayment.PaymentId;
                            message.OffsetPaymentAmount = p1.ParentPayment.ActualPaymentAmount;
                            message.OffsetPaymentDate = p1.ParentPayment.ActualPaymentDate;
                        }

                        this.Bus.Value.PublishMessage(message).Wait();
                    }
                    catch
                    {
                        this.LoggingService.Value.Fatal(() => $"PaymentService.PublishPaymentMessage::Failed to publish {typeof(PaymentMessage).FullName}, Message = {JsonConvert.SerializeObject(message)}");
                    }
                }, payment);
            }
        }

        private static void HandleNullPaymentMethods(IList<Payment> paymentsWithNullPaymentMethod, PaymentProcessor paymentProcessor, ICollection<PaymentProcessorResponse> responses)
        {
            if (!paymentsWithNullPaymentMethod.Any())
            {
                return;
            }

            PaymentProcessorResponse paymentProcessorResponse = new PaymentProcessorResponse
            {
                PaymentProcessorSourceKey = string.Empty,
                PaymentProcessor = paymentProcessor,
                PaymentProcessorResponseStatus = PaymentProcessorResponseStatusEnum.Unknown,
                RawResponse = "NullPaymentMethod",
                ResponseField1 = "",
                ResponseField2 = "",
                ResponseField3 = "",
                ResponseField4 = "",
                ResponseField5 = "",
                ResponseField6 = "",
                ResponseField7 = "",
                ResponseField8 = "",
                ResponseField9 = "",
                ResponseField10 = "",
                SnapshotTotalPaymentAmount = paymentsWithNullPaymentMethod.Sum(x => x.ActualPaymentAmount),
                PaymentMethod = null,
                ErrorMessage = "",
                PaymentSystemType = PaymentSystemTypeEnum.VisitPay
            };

            foreach (Payment payment in paymentsWithNullPaymentMethod)
            {
                PaymentPaymentProcessorResponse current = new PaymentPaymentProcessorResponse
                {
                    InsertDate = DateTime.UtcNow,
                    Payment = payment,
                    PaymentProcessorResponse = paymentProcessorResponse,
                    SnapshotPaymentAmount = payment.ActualPaymentAmount
                };
                payment.PaymentPaymentProcessorResponses.Add(current);
                paymentProcessorResponse.PaymentPaymentProcessorResponses.Add(current);
            }

            responses.Add(paymentProcessorResponse);
        }

        private void SetStatusOfPaymentForPromptPayment(PaymentProcessorResponse latestResponse, Payment responsePayment)
        {
            switch (latestResponse.PaymentProcessorResponseStatus)
            {
                case PaymentProcessorResponseStatusEnum.Accepted:
                    responsePayment.PaymentStatus = PaymentStatusEnum.ActivePendingACHApproval;
                    break;
                case PaymentProcessorResponseStatusEnum.Approved:
                    responsePayment.PaymentStatus = PaymentStatusEnum.ClosedPaid;
                    break;
                case PaymentProcessorResponseStatusEnum.LinkFailure:
                    responsePayment.PaymentStatus = PaymentStatusEnum.ActivePendingLinkFailure;
                    break;
                case PaymentProcessorResponseStatusEnum.CallToProcessorFailed:
                case PaymentProcessorResponseStatusEnum.BadData:
                case PaymentProcessorResponseStatusEnum.Decline:
                case PaymentProcessorResponseStatusEnum.Error:
                case PaymentProcessorResponseStatusEnum.Unknown:
                    responsePayment.PaymentStatus = PaymentStatusEnum.ClosedFailed;
                    break;
            }
        }

        private void SetStatusOfPaymentForScheduledPayment(PaymentProcessorResponse latestResponse, Payment responsePayment)
        {
            switch (latestResponse.PaymentProcessorResponseStatus)
            {
                case PaymentProcessorResponseStatusEnum.CallToProcessorFailed:
                    responsePayment.PaymentStatus = PaymentStatusEnum.ActiveGatewayError;
                    break;
                default:
                    this.SetStatusOfPaymentForPromptPayment(latestResponse, responsePayment);
                    break;
            }
        }

        private void PublishBalanceTransferFinancePlanPaymentMessage(Payment payment)
        {
            if (this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.BalanceTransfer))
            {
                BalanceTransferSendFinancePlanPaymentSettledMessage message = new BalanceTransferSendFinancePlanPaymentSettledMessage
                {
                    VpGuarantorId = payment.Guarantor.VpGuarantorId,
                    PaymentId = payment.PaymentId
                };
                this._session.Transaction.RegisterPostCommitSuccessAction(() => this.Bus.Value.PublishMessage(message).Wait());
            }
        }

        public void PopulatePaymentMethodTypeOnPaymentAllocationMessages(IList<PaymentAllocationMessage> paymentAllocationMessages)
        {
            foreach (PaymentAllocationMessage reassessmentPaymentAllocation in paymentAllocationMessages.Where(x => !x.VpPaymentMethodType.HasValue && x.VpPaymentAllocationId.HasValue))
            {
                bool found = false;
                if (reassessmentPaymentAllocation.VpPaymentAllocationId.HasValue)
                {
                    PaymentAllocation paymentAllocation = this._paymentAllocationRepository.Value.GetById(reassessmentPaymentAllocation.VpPaymentAllocationId.Value);
                    if (paymentAllocation.Payment?.PaymentMethod != null)
                    {
                        reassessmentPaymentAllocation.VpPaymentMethodType = paymentAllocation.Payment.PaymentMethod.PaymentMethodType;
                        found = true;
                    }
                }

                if (!found)
                {
                    PaymentMethodTypeEnum paymentMethodTypeEnum = reassessmentPaymentAllocation.VpGuarantorId.HasValue ?
                        this._paymentMethodsService.Value.GetPrimaryPaymentMethod(reassessmentPaymentAllocation.VpGuarantorId.Value, true)?.PaymentMethodType ?? PaymentMethodTypeEnum.AchChecking
                        : PaymentMethodTypeEnum.AchChecking;

                    reassessmentPaymentAllocation.VpPaymentMethodType = paymentMethodTypeEnum;
                    this.LoggingService.Value.Warn(() => $"PaymentService::PopulatePaymentMethodTypeOnPaymentAllocationMessages - unable to find payment method for VpPaymentAllocationId {reassessmentPaymentAllocation.VpPaymentAllocationId} Using default value: {paymentMethodTypeEnum}");
                }
            }
        }

        #region ACH

        public void SettleAchPayment(Payment payment, AchSettlementTypeEnum achSettlementType = AchSettlementTypeEnum.None)
        {
            bool hasOptimisticSettlementEnabled = this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureOptimisticAchSettlementIsEnabled);

            string logMessage;
            switch (payment.PaymentType)
            {
                case PaymentTypeEnum.Void:
                    logMessage = "ACH Payment Void Settled";
                    break;
                case PaymentTypeEnum.PartialRefund:
                    logMessage = "ACH Payment Partial Refund Settled";
                    break;
                case PaymentTypeEnum.Refund:
                    logMessage = "ACH Payment Refund Settled";
                    break;
                default:
                    string settlementMethod = string.Empty;

                    if (hasOptimisticSettlementEnabled)
                    {
                        if (achSettlementType == AchSettlementTypeEnum.FileSettlement)
                        {
                            settlementMethod = " From File";
                        }
                        else if (achSettlementType == AchSettlementTypeEnum.OptimisticSettlement)
                        {
                            settlementMethod = " Optimistically";
                        }
                    }

                    logMessage = "ACH Payment Settled" + settlementMethod;
                    break;
            }

            payment.PaymentStatus = PaymentStatusEnum.ClosedPaid;
            this.PublishOutboundPaymentAllocationMessages(payment, achSettlementType);
            this.PublishOutboundPaymentMessage(payment, achSettlementType);
            this.UpdatePayment(payment.Guarantor, payment, null, logMessage);

            //VP-57 - Removing setting the visit status here.
            /*
            if (payment.ActualPaymentDate.HasValue)
            {
                foreach (PaymentAllocation paymentAllocation in payment.PaymentAllocations)
                {
                    this._visitStateService.Value.SetVisitState(paymentAllocation.Visit);
                    this._visitService.Value.SaveVisit(paymentAllocation.Visit);
                }
            }
            */
        }

        public void ReturnAchPayment(Payment payment, string returnReasonCode, string transactionId, bool isReturnedAfterSettlement = false)
        {
            IDictionary<FinancePlan, decimal> financePlans = new Dictionary<FinancePlan, decimal>();
            if (payment.PaymentAllocations.Any(x => x.FinancePlanId.HasValue))
            {
                IList<int> financePlanIds = payment.PaymentAllocations.Where(x => x.FinancePlanId.HasValue).Select(x => x.FinancePlanId.Value).Distinct().ToList();
                financePlans = this._financePlanService.Value.GetFinancePlansById(financePlanIds).ToDictionary(k => k, v => 0m);
            }

            foreach (PaymentAllocation paymentAllocation in payment.PaymentAllocations)
            {
                if (paymentAllocation.FinancePlanId.HasValue && financePlans.Keys.Any(x => x.FinancePlanId == paymentAllocation.FinancePlanId.Value))
                {
                    FinancePlan financePlan = financePlans.Keys.First(x => x.FinancePlanId == paymentAllocation.FinancePlanId.Value);
                    if (financePlan != null)
                    {
                        financePlans[financePlan] += paymentAllocation.ActualAmount;
                    }
                }

                //VP-57 - What do we need to do here?
                //this._visitStateService.Value.SetVisitState(paymentAllocation.Visit);
                //this._visitService.Value.SaveVisit(paymentAllocation.Visit);
                //this._balanceTransferStatusService.Value.SetBalanceTransferStatusEligible(paymentAllocation.Visit, $"Returned ACH Payment for transaction: {transactionId}");
            }

            this._paymentAllocationService.Value.OffsetAllAllocationsInPayment(payment);

            string logMessage;
            switch (payment.PaymentType)
            {
                case PaymentTypeEnum.Void:
                    payment.PaymentStatus = PaymentStatusEnum.ClosedFailed;
                    logMessage = "ACH Payment Void Returned ";
                    break;
                case PaymentTypeEnum.PartialRefund:
                    payment.PaymentStatus = PaymentStatusEnum.ClosedFailed;
                    logMessage = "ACH Payment Partial Refund Returned ";
                    break;
                case PaymentTypeEnum.Refund:
                    payment.PaymentStatus = PaymentStatusEnum.ClosedFailed;
                    logMessage = "ACH Payment Refund Returned ";
                    break;
                default:
                    payment.PaymentStatus = PaymentStatusEnum.ClosedFailed;
                    logMessage = "ACH Payment Returned ";
                    break;
            }

            payment.ActualPaymentAmount = 0m;
            payment.ActualPaymentDate = null;

            // VP-56 FinancePlan to PaymentFinancePlan
            List<PaymentScheduledAmount> paymentScheduledAmountToReverse = payment.PaymentScheduledAmounts.Where(x => x.PaymentFinancePlan != null).ToList();
            foreach (PaymentScheduledAmount paymentScheduledAmount in paymentScheduledAmountToReverse)
            {
                decimal amount;
                // VP-56 pull the FP out of the dictionary by FP id 
                FinancePlan financePlanKey = financePlans.Keys.FirstOrDefault(x => x.FinancePlanId == paymentScheduledAmount.PaymentFinancePlan?.FinancePlanId);
                if (financePlans.TryGetValue(financePlanKey, out amount))
                {
                    paymentScheduledAmount.ScheduledAmount = amount;
                }
            }

            PaymentPaymentProcessorResponse paymentProcessorResponse = payment.PaymentPaymentProcessorResponses.FirstOrDefault(x => x.PaymentProcessorResponse.PaymentProcessorSourceKey == transactionId);
            if (paymentProcessorResponse != null)
            {
                paymentProcessorResponse.PaymentProcessorResponse.PaymentProcessorResponseStatus = PaymentProcessorResponseStatusEnum.Decline;
                paymentProcessorResponse.PaymentProcessorResponse.ResponseField2 = "return";
            }

            bool hasOptimisticSettlementEnabled = this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureOptimisticAchSettlementIsEnabled);
            if (hasOptimisticSettlementEnabled && !isReturnedAfterSettlement)
            {
                this.PublishOutboundPaymentMessage(payment, AchSettlementTypeEnum.FileReturn);
                this._paymentAllocationService.Value.PublishPaymentAllocationMessages(payment, true, AchSettlementTypeEnum.FileReturn);
            }

            this.UpdatePayment(payment.Guarantor, payment, null, logMessage + returnReasonCode);
        }

        [Obsolete(ObsoleteDescription.VisitTransactions)]
        public void ReturnAchPaymentAfterSettlement(Payment payment, string transactionId)
        {
            this.ReturnAchPayment(payment, "(Returned After Settlement)", transactionId, true);
            this.PublishPaymentFailedEmail(payment.Guarantor.VpGuarantorId);
            this._paymentAllocationService.Value.PublishPaymentAllocationMessages(payment, true, AchSettlementTypeEnum.FileReturn);

            bool hasOptimisticSettlementEnabled = this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureOptimisticAchSettlementIsEnabled);
            if (hasOptimisticSettlementEnabled)
            {
                this.PublishOutboundPaymentMessage(payment, AchSettlementTypeEnum.FileReturn);
            }
        }

        public void PublishPaymentAllocations(IList<PaymentAllocation> paymentAllocations)
        {
            this._paymentAllocationService.Value.PublishPaymentAllocationMessages(paymentAllocations);
        }

        #endregion

        public Payment CreateReallocationFinancePlanPayment(int financePlanId, Guarantor vpGuarantor, int? visitPayUserId)
        {
            PaymentMethod methodToUse = this._paymentMethodsService.Value.GetPrimaryPaymentMethod(vpGuarantor.VpGuarantorId, true);

            Payment payment = new Payment
            {
                Guarantor = vpGuarantor,
                ActualPaymentAmount = 0m,
                ActualPaymentDate = null,
                InsertDate = DateTime.UtcNow,
                PaymentMethod = methodToUse,
                PaymentStatus = PaymentStatusEnum.ActivePending,
                PaymentType = PaymentTypeEnum.ReallocationFromInterestToPrincipal,
                ScheduledPaymentDate = DateTime.UtcNow,
                ScheduledDiscountPercent = 0m,
                CreatedVisitPayUserId = visitPayUserId ?? SystemUsers.SystemUserId
            };
            payment.AddPaymentScheduledAmount(new PaymentScheduledAmount
            {
                PaymentFinancePlan = new PaymentFinancePlan { FinancePlanId = financePlanId },
                InsertDate = DateTime.UtcNow,
                ScheduledAmount = 0m,
                PaymentVisit = null
            });
            return payment;
        }

        public void SavePaymentImport(PaymentImport paymentImport)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                this._paymentImportRepository.Value.InsertOrUpdate(paymentImport);
                unitOfWork.Commit();
            }
        }

        public PaymentImport GetPaymentImport(int paymentImportId)
        {
            return this._paymentImportRepository.Value.GetById(paymentImportId);
        }

        public IList<PaymentImport> GetPaymentImports(PaymentImportFilter filter)
        {
            return this._paymentImportRepository.Value.GetPaymentImports(filter);
        }

        public IList<string> GetAllPaymentImportBatchNumbers()
        {
            return this._paymentImportRepository.Value.GetAllPaymentImportBatchNumbers();
        }

        #region Buckets

        public Payment CreateRecurringPayment(int financePlanId, Guarantor vpGuarantor, decimal amountDue, PaymentMethod paymentMethod = null)
        {
            return this.CreateRecurringPayment(financePlanId, vpGuarantor, amountDue, PaymentStatusEnum.ActivePending, DateTime.UtcNow, paymentMethod);
        }

        private Payment CreateRecurringPayment(int financePlanId, Guarantor vpGuarantor, decimal amountDue, PaymentStatusEnum paymentStatus, DateTime scheduledPaymentDate, PaymentMethod paymentMethod = null)
        {
            PaymentMethod methodToUse = paymentMethod ?? this._paymentMethodsService.Value.GetPrimaryPaymentMethod(vpGuarantor.VpGuarantorId, true);
            if (methodToUse != null && amountDue > 0m)
            {
                Payment payment = new Payment
                {
                    Guarantor = vpGuarantor,
                    ActualPaymentAmount = 0m,
                    ActualPaymentDate = null,
                    InsertDate = DateTime.UtcNow,
                    PaymentMethod = methodToUse,
                    PaymentStatus = paymentStatus,
                    PaymentType = PaymentTypeEnum.RecurringPaymentFinancePlan,
                    ScheduledPaymentDate = scheduledPaymentDate,
                    ScheduledDiscountPercent = 0m,
                    CreatedVisitPayUserId = -1
                };
                payment.AddPaymentScheduledAmount(new PaymentScheduledAmount
                {
                    PaymentFinancePlan = new PaymentFinancePlan { FinancePlanId = financePlanId },
                    InsertDate = DateTime.UtcNow,
                    ScheduledAmount = amountDue,
                    PaymentVisit = null
                });

                this._paymentRepository.Value.InsertOrUpdate(payment);
                return payment;
            }
            else
            {
                this.LoggingService.Value.Warn(() => $"PaymentService::CreateRecurringPayment - No payment method was found for VpGuarantorId: {vpGuarantor.VpGuarantorId}");
            }
            return null;
        }

        #endregion

        #region PostPaymentProcess

        public void PostProcessPayment<TStatement>(
            Guarantor guarantor,
            TStatement mostRecentStatement,
            DateTime dateToProcess,
            IList<Payment> scheduledPayments,
            IList<Payment> recurringPayments)
            where TStatement : IVpStatementPostPaymentProcess
        {
            string methodName = $"{nameof(PaymentService)}::{nameof(PostProcessPayment)}";

            IList<Payment> allPaymentsList = new List<Payment>();
            if (guarantor == null)
            {
                this.LoggingService.Value.Warn(() => $"{methodName} - guarantor was NULL");
                return;
            }

            if (recurringPayments.IsNotNullOrEmpty())
            {
                allPaymentsList.AddRange(recurringPayments);
            }

            if (scheduledPayments.IsNotNullOrEmpty())
            {
                allPaymentsList.AddRange(scheduledPayments);
            }

            if (allPaymentsList.IsNotNullOrEmpty())
            {
                this.SendEmailsForScheduledPayments(allPaymentsList);
            }

            if (mostRecentStatement == null)
            {
                this.LoggingService.Value.Warn(() => $"{methodName} - mostRecentStatement for VpGuarantor {guarantor.VpGuarantorId} was NULL");
                return;
            }

            bool hasPendingLinkFailure = this._paymentRepository.Value.HasLinkFailurePayments(guarantor.VpGuarantorId);
            bool hasSystemErrors = recurringPayments != null && recurringPayments.Any(a => a.HadSystemError());

            if (hasSystemErrors || hasPendingLinkFailure)
            {
                //Setting ProcessingStatus to Unprocessed will prevent the PastDue logic from happening.
                string logMessage = $"SystemErrors: {(hasSystemErrors ? "Yes" : "No")} PendingLinkFailure: {(hasPendingLinkFailure ? "Yes" : "No")}";
                this._statementService.Value.MarkStatementAsUnprocessed(mostRecentStatement.VpStatementId, mostRecentStatement.VpGuarantorId, logMessage);
                return;
            }

            bool isPaymentDue = dateToProcess >= mostRecentStatement.PaymentDueDate;
            bool isStatementMarkedAsQueued = mostRecentStatement.ProcessingStatus == ChangeEventStatusEnum.Queued;
            bool isStatementMarkedAsProcessed = mostRecentStatement.ProcessingStatus == ChangeEventStatusEnum.Processed;
            bool paymentsContainSystemError = allPaymentsList.Any(x => x.HadSystemError());

            if (isPaymentDue && isStatementMarkedAsQueued && !paymentsContainSystemError)
            {
                // now do the finance plan
                IList<FinancePlan> financePlans = this._financePlanService.Value.GetAllOpenFinancePlansForGuarantor(guarantor);
                if (financePlans.IsNotNullOrEmpty())
                {
                    this._financePlanService.Value.CheckIfFinancePlansAreStillPastDueOrUncollectable(financePlans);
                }

                this._statementService.Value.MarkStatementAsProcessed(mostRecentStatement.VpStatementId, mostRecentStatement.VpGuarantorId);
            }
            else
            {
                string logMessage = $"PaymentDue: {(isPaymentDue ? "Yes" : "No")} StatementMarkedAsQueued: {(isStatementMarkedAsQueued ? "Yes" : "No")} SystemErrors: {(paymentsContainSystemError ? "Yes" : "No")}";
                if (!isStatementMarkedAsProcessed)
                {
                    this._statementService.Value.MarkStatementAsUnprocessed(mostRecentStatement.VpStatementId, mostRecentStatement.VpGuarantorId, logMessage);
                }
                else
                {
                    this.LoggingService.Value.Warn(() => $"{methodName} - Statement {mostRecentStatement.VpStatementId} for VpGuarantor {mostRecentStatement.VpGuarantorId} was already marked as processed \n{logMessage}");
                }
            }
        }

        public void SendEmailsForPromptPayments(IList<Payment> payments)
        {
            // emails
            if (payments.All(x => x.WasSuccessful()))
            {
                // all payment(s) succeeded
                // send confirmation
                this.PublishPaymentConfirmationEmail<SendPaymentConfirmationEmailMessage>(payments.ToList());
            }
        }

        private void SendEmailsForScheduledPayments(IList<Payment> scheduledPaymentsProcessed)
        {
            if (scheduledPaymentsProcessed.IsNullOrEmpty())
            {
                this.LoggingService.Value.Warn(() => "SendEmailForEachScheduledPaymentSuccess:: scheduledPaymentsProcessed is null or empty");
                return;
            }

            IList<string> transactionIds = scheduledPaymentsProcessed.Where(x => x.WasSuccessful()).Select(x => x.TransactionId).Distinct().ToList();

            if (transactionIds.IsNullOrEmpty())
            {
                string message = scheduledPaymentsProcessed.Any(x => x.WasSuccessful()) ? "scheduledPaymentsProcessed has no TransactionIDs." : "scheduledPaymentsProcessed has no successful transactions.";
                this.LoggingService.Value.Warn(() => $"SendEmailForEachScheduledPaymentSuccess:: {message}");
            }

            IList<Payment> successfulPayments = new List<Payment>();

            foreach (string transactionId in transactionIds)
            {
                Payment successPayment = scheduledPaymentsProcessed.FirstOrDefault(x => x.TransactionId != null && x.TransactionId.Equals(transactionId, StringComparison.OrdinalIgnoreCase));
                if (successPayment == null)
                {
                    this.LoggingService.Value.Warn(() => $"SendEmailForEachScheduledPaymentSuccess:: Could not find transactionId {transactionId} in scheduledPaymentsProcessed. successPayment is NULL.");
                    continue;
                }

                if (successPayment.IsReversalPaymentType())
                {
                    this._session.Transaction.RegisterPostCommitSuccessAction((p) =>
                    {
                        SendPaymentReversalConfirmationEmailMessage message = null;
                        try
                        {
                            message = new SendPaymentReversalConfirmationEmailMessage
                            {
                                VpGuarantorId = p.Guarantor.VpGuarantorId,
                                TransactionNumber = p.TransactionId,
                                PaymentType = p.PaymentType
                            };

                            this.Bus.Value.PublishMessage(message).Wait();
                        }
                        catch
                        {
                            this.LoggingService.Value.Fatal(() => $"SendEmailForEachScheduledPaymentSuccess::Failed to publish SendPaymentReversalConfirmationEmailMessage, Message = {JsonConvert.SerializeObject(message)}");
                        }
                    }, successPayment);
                }
                else
                {
                    successfulPayments.Add(successPayment);
                }
            }

            // send an email for successful payments
            this.PublishPaymentConfirmationEmail<ScheduledSendPaymentConfirmationEmailMessage>(successfulPayments.ToList());

            // send an email if any payments failed
            Payment failedPayment = scheduledPaymentsProcessed.FirstOrDefault(x => x.PaymentStatus == PaymentStatusEnum.ClosedFailed);
            if (failedPayment != null)
            {
                this.PublishPaymentFailedEmail(failedPayment.Guarantor.VpGuarantorId);
                this._paymentRepository.Value.InsertOrUpdate(failedPayment);
            }
        }

        public void PublishPaymentFailedEmail(int vpGuarantorId)
        {
            this._session.Transaction.RegisterPostCommitSuccessAction(g =>
            {
                SendPaymentFailureEmailMessage message = null;
                try
                {
                    message = new SendPaymentFailureEmailMessage
                    {
                        VpGuarantorId = g
                    };

                    this.Bus.Value.PublishMessage(message).Wait();
                }
                catch
                {
                    this.LoggingService.Value.Fatal(() => $"PaymentService.PublishPaymentFailedMessage::Failed to publish {typeof(SendPaymentFailureEmailMessage).FullName}, Message = {JsonConvert.SerializeObject(message)}");
                }
            }, vpGuarantorId);
        }

        private void PublishPaymentConfirmationEmail<TMessage>(IList<Payment> payments) where TMessage : BaseSendPaymentConfirmationEmailMessage<TMessage>, new()
        {
            if (payments == null || !payments.Any())
            {
                return;
            }

            this._session.Transaction.RegisterPostCommitSuccessAction(p =>
            {
                TMessage message = null;
                try
                {
                    List<Payment> applicablePayments = p.Where(x => x.PaymentStatus.IsInCategory(PaymentStatusEnumCategory.Paid) && !x.PaymentStatus.IsInCategory(PaymentStatusEnumCategory.Cancelled)).ToList();
                    if (!applicablePayments.Any())
                    {
                        return;
                    }

                    Payment payment = applicablePayments.First();

                    List<PaymentProcessorResponse> paymentProcessorResponses = applicablePayments
                        .Where(py => py.PaymentProcessorResponses != null)
                        .Select(py => py.PaymentProcessorResponses.OrderByDescending(ppr => ppr.InsertDate).FirstOrDefault())
                        .Where(x => x != null)
                        .ToList();

                    message = new TMessage
                    {
                        VpGuarantorId = payment.PaymentMethod.VpGuarantorId.GetValueOrDefault(payment.Guarantor.VpGuarantorId),
                        TransactionNumber = applicablePayments.Select(x => x.TransactionId).Distinct().ToList().ToCommaSeparatedString(true),
                        PaymentProcessorResponseIds = paymentProcessorResponses.Select(x => x.PaymentProcessorResponseId).ToList(),
                        IsAchType = applicablePayments.Any(x => x.PaymentMethod.IsAchType),
                        IsPromptPayment = payment.IsPromptPayType(),
                        IsTextToPayPayment = payment.IsTextToPayType()
                    };

                    this.Bus.Value.PublishMessage(message).Wait();
                }
                catch
                {
                    this.LoggingService.Value.Fatal(() => $"Failed to publish SendPaymentConfirmationEmailMessage, Message = {JsonConvert.SerializeObject(message)}");
                }
            }, payments);
        }

        #endregion
    }
}

﻿namespace Ivh.Domain.FinanceManagement.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Ach.Interfaces;
    using Base.Interfaces;
    using Base.Services;
    using Interfaces;
    using Payment.Entities;

    public class PaymentBatchOperationsService : DomainService, IPaymentBatchOperationsService
    {
        private readonly Lazy<IPaymentBatchOperationsProvider> _paymentBatchOperationsProvider;
        private readonly Lazy<IAchSettlementDetailRepository> _achSettlementDetailRepository;

        public PaymentBatchOperationsService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IPaymentBatchOperationsProvider> paymentBatchOperationsProvider,
            Lazy<IAchSettlementDetailRepository> achSettlementDetailRepository) : base(serviceCommonService)
        {
            this._paymentBatchOperationsProvider = paymentBatchOperationsProvider;
            this._achSettlementDetailRepository = achSettlementDetailRepository;
        }

        public async Task<ICollection<QueryPaymentTransaction>> GetPaymentTransactionsAsync(DateTime beginDate, DateTime endDate, string transactionId)
        {
            return await this._paymentBatchOperationsProvider.Value.GetPaymentTransactionsAsync(beginDate, endDate, transactionId, false);
        }

        public async Task<ICollection<QueryPaymentTransaction>> GetUnsettledAchPaymentTransactionsAsync(DateTime beginDate, DateTime endDate)
        {
            List<QueryPaymentTransaction> unsettledAchPaymentTransactions = new List<QueryPaymentTransaction>();

            IList<string> unsettledAchTransactions = this._achSettlementDetailRepository.Value.GetAchSettlementTransactionIdsWithoutFile();

            foreach (string transactionId in unsettledAchTransactions)
            {
                var transactions = await this._paymentBatchOperationsProvider.Value.GetPaymentTransactionsAsync(beginDate, endDate, transactionId, true);
                if (transactions != null && transactions.Count > 0)
                {
                    unsettledAchPaymentTransactions.AddRange(transactions);
                }

            }

            return unsettledAchPaymentTransactions;
        }

    }
}
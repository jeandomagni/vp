﻿namespace Ivh.Domain.FinanceManagement.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Utilities.Extensions;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.FinanceManagement;
    using Consolidation.Interfaces;
    using Content.Interfaces;
    using Entities;
    using FinancePlan.Interfaces;
    using Guarantor.Entities;
    using Guarantor.Interfaces;
    using Interfaces;
    using Logging.Interfaces;
    using Newtonsoft.Json;
    using NHibernate;
    using Payment.Interfaces;
    using Settings.Interfaces;

    public class PaymentMethodsService : DomainService, IPaymentMethodsService
    {
        private readonly ISession _session;
        private readonly Lazy<IFinancePlanRepository> _financePlanRepository;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IPaymentConfigurationService> _paymentConfigurationService;
        private readonly Lazy<IConsolidationGuarantorRepository> _consolidationGuarantorRepository;
        private readonly Lazy<IPaymentRepository> _paymentRepository;
        private readonly Lazy<IPaymentMethodsRepository> _paymentMethodsRepository;
        private readonly Lazy<IPaymentMethodEventRepository> _paymentMethodEventRepository;
        private readonly Lazy<IPaymentMethodAccountTypeRepository> _paymentMethodAccountTypeRepository;
        private readonly Lazy<IPaymentMethodProviderTypeRepository> _paymentMethodProviderTypeRepository;
        private readonly Lazy<IPaymentProvider> _paymentProvider;
        private readonly Lazy<IContentService> _contentService;
        private readonly Lazy<IMetricsProvider> _metricsProvider;

        public PaymentMethodsService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            ISessionContext<VisitPay> sessionContext,
            Lazy<IPaymentMethodsRepository> paymentMethodsRepository,
            Lazy<IPaymentMethodEventRepository> paymentMethodEventRepository,
            Lazy<IPaymentMethodAccountTypeRepository> paymentMethodAccountTypeRepository,
            Lazy<IPaymentMethodProviderTypeRepository> paymentMethodProviderTypeRepository,
            Lazy<IFinancePlanRepository> financePlanRepository,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IPaymentConfigurationService> paymentConfigurationService,
            Lazy<IConsolidationGuarantorRepository> consolidationGuarantorRepository,
            Lazy<IPaymentRepository> paymentRepository,
            Lazy<IPaymentProvider> paymentProvider,
            Lazy<IContentService> contentService,
            Lazy<IMetricsProvider> metricsProvider) : base(serviceCommonService)
        {
            this._session = sessionContext.Session;
            this._paymentMethodsRepository = paymentMethodsRepository;
            this._paymentMethodEventRepository = paymentMethodEventRepository;
            this._paymentMethodAccountTypeRepository = paymentMethodAccountTypeRepository;
            this._paymentMethodProviderTypeRepository = paymentMethodProviderTypeRepository;
            this._financePlanRepository = financePlanRepository;
            this._guarantorService = guarantorService;
            this._paymentConfigurationService = paymentConfigurationService;
            this._consolidationGuarantorRepository = consolidationGuarantorRepository;
            this._paymentRepository = paymentRepository;
            this._paymentProvider = paymentProvider;
            this._contentService = contentService;
            this._metricsProvider = metricsProvider;
        }

        public PaymentMethod GetById(int id)
        {
            PaymentMethod paymentMethod = this._paymentMethodsRepository.Value.GetById(id);
            if (paymentMethod?.VpGuarantorId == null)
            {
                return null;
            }

            // todo: when we make mobile payment methods UI the same as desktop, this can be removed
            this.SetRemoveable(paymentMethod.ToListOfOne(), paymentMethod.VpGuarantorId);

            return this.FilterPaymentMethods(paymentMethod.ToListOfOne()).FirstOrDefault();
        }

        public PaymentMethod GetById(int paymentMethodId, int vpGuarantorId)
        {
            PaymentMethod paymentMethod = this.GetById(paymentMethodId);
            if (paymentMethod.VpGuarantorId.GetValueOrDefault(0) != vpGuarantorId)
            {
                return null;
            }

            return paymentMethod;
        }

        public IList<PaymentMethod> GetExpiringPrimaryPaymentMethods(string expDate)
        {
            ICollection<PaymentMethod> paymentMethods = this._paymentMethodsRepository.Value.GetExpiringPrimaryPaymentMethods(expDate);

            return paymentMethods.Where(x => this._paymentConfigurationService.Value.IsPaymentMethodTypeAccepted(x.PaymentMethodType)).ToList();
        }

        public IList<PaymentMethod> GetPaymentMethods(int vpGuarantorId, int actionVisitPayUserId, bool includingInactive = false)
        {
            IList<PaymentMethod> paymentMethods = this._paymentMethodsRepository.Value.GetPaymentMethods(vpGuarantorId, includingInactive);
            IList<PaymentMethod> acceptedPaymentMethods = this.FilterPaymentMethods(paymentMethods);

            this.SetRemoveable(acceptedPaymentMethods, vpGuarantorId);
            this.SetPrimaryPermissions(acceptedPaymentMethods, vpGuarantorId, actionVisitPayUserId);

            return acceptedPaymentMethods;
        }

        public int GetPaymentMethodsCount(int vpGuarantorId)
        {
            return this._paymentMethodsRepository.Value.GetPaymentMethodsCount(vpGuarantorId);
        }

        public PaymentMethod GetPrimaryPaymentMethod(int vpGuarantorId, bool considerConsolidation)
        {
            if (considerConsolidation)
            {
                Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
                if (guarantor.ConsolidationStatus == GuarantorConsolidationStatusEnum.Managed)
                {
                    ConsolidationGuarantor consolidationGuarantor = this._consolidationGuarantorRepository.Value.GetManagedRequests(guarantor).FirstOrDefault(x => x.IsActive());
                    if (consolidationGuarantor != null)
                    {
                        vpGuarantorId = consolidationGuarantor.ManagingGuarantor.VpGuarantorId;
                    }
                }
            }

            PaymentMethod primaryPaymentMethod = this._paymentMethodsRepository.Value.GetPrimaryPaymentMethod(vpGuarantorId);
            if (primaryPaymentMethod == null)
            {
                return null;
            }

            return this.FilterPaymentMethods(primaryPaymentMethod.ToListOfOne()).FirstOrDefault();
        }

        public PaymentMethodEvent GetMostRecentPaymentMethodEvent(int vpGuarantorId, PaymentMethodStatusEnum paymentMethodStatus)
        {
            return this._paymentMethodEventRepository.Value.GetMostRecentPaymentMethodEvent(vpGuarantorId, paymentMethodStatus);
        }
        
        public PaymentMethodResponse Delete(PaymentMethod paymentMethod, int actionVisitPayUserId)
        {
            if (paymentMethod.VpGuarantorId.HasValue)
            {
                IList<PaymentMethod> validatedPaymentMethods = paymentMethod.ToListOfOne();
                this.SetRemoveable(validatedPaymentMethods, paymentMethod.VpGuarantorId.GetValueOrDefault(0));

                PaymentMethod validatedPaymentMethod = validatedPaymentMethods.FirstOrDefault();
                if (validatedPaymentMethod != null && !validatedPaymentMethod.IsRemoveable)
                {
                    return new PaymentMethodResponse(false, validatedPaymentMethod.RemoveableMessage);
                }
            }

            bool isActive = paymentMethod.IsActive;

            using (IUnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                paymentMethod.Deactivate();
                this._paymentMethodsRepository.Value.InsertOrUpdate(paymentMethod);

                this.AddPaymentMethodEvent(paymentMethod, PaymentMethodStatusEnum.Deactivated, actionVisitPayUserId);
                
                if (isActive)
                {
                    if (paymentMethod.IsAchType || paymentMethod.IsCardType)
                    {
                        // do not send this message for a deleted payment method that wasn't active
                        // ex: single use payment method
                        this.SendPaymentMethodChangeEmail(paymentMethod.VpGuarantorId);
                    }
                }

                unitOfWork.Commit();
            }

            return new PaymentMethodResponse(true);
        }

        public PaymentMethodResponse Save(PaymentMethod paymentMethod, int actionVisitPayUserId, string cardCode, bool suppressNotifications)
        {
            if (!paymentMethod.VpGuarantorId.HasValue)
            {
                return new PaymentMethodResponse(false, "Invalid Guarantor.");
            }
            
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(paymentMethod.VpGuarantorId.Value);
            int activePaymentMethodCount = this._paymentMethodsRepository.Value.GetPaymentMethodsCount(guarantor.VpGuarantorId);
            PaymentMethod existingPaymentMethod = paymentMethod.PaymentMethodId == default(int) ? null : this._paymentMethodsRepository.Value.GetByIdStateless(paymentMethod.PaymentMethodId);
            
            if (paymentMethod.IsActive && activePaymentMethodCount == 0)
            {
                // set default primary since none exist
                if (paymentMethod.IsAchType || paymentMethod.IsCardType)
                {
                    paymentMethod.IsPrimary = true;
                }
            }
            
            bool wasPrimary = existingPaymentMethod?.IsPrimary ?? false;
            if (!wasPrimary && paymentMethod.IsPrimary && paymentMethod.IsActive)
            {
                // non-autopay should not be able to set primary
                if (!guarantor.UseAutoPay)
                {
                    paymentMethod.IsPrimary = false;
                }
                
                // ensure client cannot set an ACH as primary
                if (paymentMethod.IsAchType && guarantor.User.VisitPayUserId != actionVisitPayUserId)
                {
                    paymentMethod.IsPrimary = false;
                }
            }
            
            PaymentMethod currentPrimaryStateless = this._paymentMethodsRepository.Value.GetPrimaryPaymentMethodStateless(guarantor.VpGuarantorId);
            bool isNew = existingPaymentMethod == null;
            bool primaryChanged = paymentMethod.IsPrimary && (currentPrimaryStateless == null || paymentMethod.PaymentMethodId != currentPrimaryStateless.PaymentMethodId);

            if (paymentMethod.IsCardType && !isNew && !string.IsNullOrWhiteSpace(cardCode))
            {
                // check if the ExpDate changed, and update it with the provider if so
                bool expirationChanged = existingPaymentMethod.ExpDate != paymentMethod.ExpDate;
                if (expirationChanged)
                {
                    bool isSuccess = Task.Run(() => this._paymentProvider.Value.UpdateCreditCardExpirationAsync(paymentMethod.BillingId, paymentMethod.ExpDate, cardCode)).Result;
                    if (!isSuccess)
                    {
                        this.LoggingService.Value.Warn(() => $"PaymentMethodsService::Save - Unable to send expiration date {paymentMethod.ExpDate} to the payment provider");
                        return new PaymentMethodResponse(false, "Unable to save the changes made to the account.");
                    }
                }
            }

            using (IUnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                // if this payment method is primary, and there's already a primary, set existing to not primary.
                if (primaryChanged && currentPrimaryStateless != null)
                {
                    PaymentMethod currentPrimary = this._paymentMethodsRepository.Value.GetById(currentPrimaryStateless.PaymentMethodId);
                    currentPrimary.IsPrimary = false;
                    this._paymentMethodsRepository.Value.InsertOrUpdate(currentPrimary);
                    this.AddPaymentMethodEvent(currentPrimary, PaymentMethodStatusEnum.Modified, actionVisitPayUserId);
                }
                
                paymentMethod.PaymentMethodAccountType = paymentMethod.PaymentMethodAccountType ?? this._paymentMethodAccountTypeRepository.Value.GetById(0);
                paymentMethod.PaymentMethodProviderType = paymentMethod.PaymentMethodProviderType ?? this._paymentMethodProviderTypeRepository.Value.GetById(0);
                paymentMethod.UpdatedDate = DateTime.UtcNow;
                paymentMethod.UpdatedUserId = actionVisitPayUserId;
                
                if (isNew)
                {
                    paymentMethod.CreatedDate = paymentMethod.UpdatedDate;
                    paymentMethod.CreatedUserId = actionVisitPayUserId;

                    if (paymentMethod.IsAchType || paymentMethod.IsCardType)
                    {
                        this._session.Transaction.RegisterPostCommitSuccessAction(() =>
                        {
                            // metrics
                            this._metricsProvider.Value.Increment(Metrics.Increment.PaymentMethods.NewPaymentMethod);
                        });
                    }
                }

                this._paymentMethodsRepository.Value.InsertOrUpdate(paymentMethod);

                this.AddPaymentMethodEvent(paymentMethod, isNew ? PaymentMethodStatusEnum.Activated : PaymentMethodStatusEnum.Modified, actionVisitPayUserId);
                
                if (paymentMethod.IsAchType || paymentMethod.IsCardType)
                {
                    if (primaryChanged)
                    {
                        this.AddPaymentMethodEvent(paymentMethod, PaymentMethodStatusEnum.SetPrimary, actionVisitPayUserId);
                    }

                    // notifications
                    if (suppressNotifications == false)
                    {
                        if (primaryChanged && currentPrimaryStateless != null)
                        {
                            // send this if it's not the first primary change
                            this.SendPrimaryPaymentMethodChangeEmail(paymentMethod.VpGuarantorId.Value);
                        }
                        else if (paymentMethod.IsActive)
                        {
                            this.SendPaymentMethodChangeEmail(paymentMethod.VpGuarantorId.Value);
                        }
                    }
                }

                unitOfWork.Commit();
            }

            return new PaymentMethodResponse(true, null, primaryChanged, paymentMethod);
        }

        public PaymentMethodResponse ValidatePrimaryPaymentMethod(PaymentMethod paymentMethod)
        {
            if (!paymentMethod.IsPrimary)
            {
                return new PaymentMethodResponse(true);
            }

            if (!paymentMethod.IsCardType)
            {
                return new PaymentMethodResponse(true);
            }

            if (paymentMethod.IsExpired)
            {
                string message = this._contentService.Value.GetTextRegion(TextRegionConstants.PaymentValidationPaymentMethodExpired);
                return new PaymentMethodResponse(false, message);
            }

            return new PaymentMethodResponse(true);
        }
        
        public void AddAchAuthorization(int vpGuarantorId, int paymentMethodId, int cmsVersionId, decimal authorizedAmount, int authorizedDuration, Guid correlationGuid)
        {
            PaymentMethod paymentMethod = this.GetById(paymentMethodId, vpGuarantorId);
            if (paymentMethod != null)
            {
                PaymentMethodAuthorization authorization = paymentMethod.AddAchAuthorization(cmsVersionId, authorizedAmount, authorizedDuration);
                authorization.CorrelationGuid = correlationGuid;
                this._paymentMethodsRepository.Value.Update(paymentMethod);
            }
        }

        private void AddPaymentMethodEvent(PaymentMethod paymentMethod, PaymentMethodStatusEnum paymentMethodStatus, int actionVisitPayUserId)
        {
            this._paymentMethodEventRepository.Value.InsertOrUpdate(new PaymentMethodEvent
            {
                PaymentMethod = paymentMethod,
                PaymentMethodStatus = paymentMethodStatus,
                VisitPayUserId = actionVisitPayUserId,
                VpGuarantorId = paymentMethod.VpGuarantorId ?? 0
            });
        }

        private IList<PaymentMethod> FilterPaymentMethods(IList<PaymentMethod> paymentMethods)
        {
            if (paymentMethods == null)
            {
                return new List<PaymentMethod>();
            }
            
            List<PaymentMethod> acceptedPaymentMethods = paymentMethods.Where(x => this._paymentConfigurationService.Value.IsPaymentMethodTypeAccepted(x.PaymentMethodType)).ToList();
            
            return acceptedPaymentMethods;
        }
        
        private void SetPrimaryPermissions(IList<PaymentMethod> paymentMethods, int vpGuarantorId, int actionVisitPayUserId)
        {
            if (paymentMethods == null || !paymentMethods.Any())
            {
                return;
            }

            int paymentMethodVpGuarantorId = paymentMethods.Select(x => x.VpGuarantorId.GetValueOrDefault(-1)).Distinct().FirstOrDefault();
            if (paymentMethodVpGuarantorId != vpGuarantorId)
            {
                return;
            }

            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            foreach (PaymentMethod paymentMethod in paymentMethods)
            {
                if (guarantor.IsOfflineGuarantor() && !guarantor.UseAutoPay)
                {
                    // offline, non-autopay guarantor cannot have a primary payment method
                    paymentMethod.SetPrimaryPermission(false);
                }
                else
                {
                    if (paymentMethod.IsCardType)
                    {
                        paymentMethod.SetPrimaryPermission(true);
                    }
                    else if (paymentMethod.IsAchType)
                    {
                        // a client user cannot set an ACH as primary on behalf of a guarantor
                        paymentMethod.SetPrimaryPermission(guarantor.User.VisitPayUserId == actionVisitPayUserId);
                    }
                }
            }
        }

        private void SetRemoveable(IList<PaymentMethod> paymentMethods, int? vpGuarantorId)
        {
            if (vpGuarantorId == null)
            {
                return;
            }

            // todo: not sure why we can't just look at the list?
            int paymentMethodsCount = this._paymentMethodsRepository.Value.GetPaymentMethodsCount(vpGuarantorId.Value);
            if (paymentMethodsCount == 0)
            {
                return;
            }
            
            IList<int> paymentMethodIdsWithScheduledPayments = this._paymentRepository.Value.GetPaymentMethodIdsAssociatedWithPayments(vpGuarantorId.Value, new[] {PaymentStatusEnum.ActivePending, PaymentStatusEnum.ActiveGatewayError});
            
            bool hasFinancePlans = this._financePlanRepository.Value.GetAllActiveFinancePlanIds(vpGuarantorId.Value).Count > 0;
            if (!hasFinancePlans)
            {
                // check for any managed guarantor finance plans
                IEnumerable<int> managedGuarantors = this._consolidationGuarantorRepository.Value.GetAllManagedGuarantorVpGuarantorIds(vpGuarantorId.Value);
                foreach (int managedGuarantor in managedGuarantors)
                {
                    if (this._financePlanRepository.Value.GetAllActiveFinancePlanIds(managedGuarantor).Count > 0)
                    {
                        hasFinancePlans = true;
                        break;
                    }
                }
            }
            
            foreach (PaymentMethod paymentMethod in paymentMethods)
            {
                // applies to ach, card
                bool hasScheduledPayments = paymentMethodIdsWithScheduledPayments.Contains(paymentMethod.PaymentMethodId);
                if (hasScheduledPayments)
                {
                    // PA-132
                    string message = this._contentService.Value.GetTextRegion(TextRegionConstants.PaymentMethodRemoveScheduledPayment);
                    paymentMethod.SetRemoveable(false, message);
                    continue;
                }

                if (paymentMethod.IsPrimary)
                {
                    // applies to ach, card
                    if (hasFinancePlans)
                    {
                        // FP-126
                        string message = this._contentService.Value.GetTextRegion(TextRegionConstants.PaymentMethodRemoveFinancePlan);
                        paymentMethod.SetRemoveable(false, message);
                        continue;
                    }

                    // applies to ach, card
                    if (paymentMethodsCount > 1)
                    {
                        string message = this._contentService.Value.GetTextRegion(TextRegionConstants.PaymentMethodRemovePrimary);
                        paymentMethod.SetRemoveable(false, message);
                        continue;
                    }
                }

                paymentMethod.SetRemoveable(true);
            }
        }
        
        // only used in PaymentProcessorInterceptor
        public PaymentMethod GetByBillingId(string billingId)
        {
            PaymentMethod paymentMethod = this._paymentMethodsRepository.Value.GetByBillingId(billingId);

            return paymentMethod;
        }
        
        #region messages

        private void SendPrimaryPaymentMethodChangeEmail(int? vpGuarantorId)
        {
            if (vpGuarantorId == null)
            {
                return;
            }

            SendPrimaryPaymentMethodChangeEmailMessage message = null;
            try
            {
                message = new SendPrimaryPaymentMethodChangeEmailMessage { VpGuarantorId = vpGuarantorId.Value };

                this._session.Transaction.RegisterPostCommitSuccessAction(m =>
                {
                    this.Bus.Value.PublishMessage(m).Wait();
                }, message);
            }
            catch
            {
                this.LoggingService.Value.Fatal(() => $"Failed to publish SendPrimaryPaymentMethodChangeEmailMessage, Message = {JsonConvert.SerializeObject(message)}");
            }
        }

        private void SendPaymentMethodChangeEmail(int? vpGuarantorId)
        {
            if (vpGuarantorId == null)
            {
                return;
            }

            SendPaymentMethodChangeEmailMessage message = null;
            try
            {
                message = new SendPaymentMethodChangeEmailMessage { VpGuarantorId = vpGuarantorId.Value };
                this._session.Transaction.RegisterPostCommitSuccessAction(m =>
                {
                    this.Bus.Value.PublishMessage(m).Wait();
                }, message);
            }
            catch
            {
                this.LoggingService.Value.Fatal(() => $"Failed to publish SendPaymentMethodChangeEmailMessage, Message = {JsonConvert.SerializeObject(message)}");
            }
        }

        #endregion
    }
}
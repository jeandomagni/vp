﻿namespace Ivh.Domain.FinanceManagement.Services
{
    using System;
    using System.Collections.Generic;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Constants;
    using Common.VisitPay.Constants;
    using Interfaces;
    using Payment.Entities;
    using Payment.Interfaces;

    public class PaymentEventService : DomainService, IPaymentEventService
    {
        private readonly IPaymentEventRepository _paymentEventRepository;

        public PaymentEventService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            IPaymentEventRepository paymentEventRepository) : base(serviceCommonService)
        {
            this._paymentEventRepository = paymentEventRepository;
        }

        public void AddPaymentEvent(int? visitPayUserId, Payment payment, string comment = null)
        {
            if (payment == null)
            {
                throw new ArgumentNullException("payment");
            }
            this._paymentEventRepository.InsertOrUpdate(new PaymentEvent()
            {
                Payment = payment,
                VpGuarantorId = payment.Guarantor.VpGuarantorId,
                PaymentStatus = payment.PaymentStatus,
                VisitPayUserId = visitPayUserId ?? SystemUsers.SystemUserId,
                Comment = comment != null ? comment.Substring(0, Math.Min(100, comment.Length)) : null
            });
        }

        public IReadOnlyList<PaymentEvent> GetPaymentEvents(List<int> paymentIds, int vpGuarantorId)
        {
            return this._paymentEventRepository.GetPaymentEvents(paymentIds, vpGuarantorId);
        }
    }
}
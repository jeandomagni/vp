﻿namespace Ivh.Domain.FinanceManagement.PaymentSummary.Entities
{
    using System;
    using System.Collections.Generic;

    public class PaymentSummary
    {
        public int VpGuarantorId { get; set; }
        public DateTime? PostDateBegin { get; set; }
        public DateTime? PostDateEnd { get; set; }

        public List<PaymentSummaryTransaction> Payments { get; set; }
    }
}

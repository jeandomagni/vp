﻿namespace Ivh.Domain.FinanceManagement.PaymentSummary.Entities
{
    using System;
    using Common.VisitPay.Enums;

    public class PaymentSummaryTransaction
    {
        public DateTime PostDate { get; set; }
        public decimal TransactionAmount { get; set; }
        public string TransactionDescription { get; set; }
        public string PatientFirstName { get; set; }
        public string PatientLastName { get; set; }
        public int VisitBillingSystemId { get; set; }
        public string VisitSourceSystemKey { get; set; }
        public string VisitSourceSystemKeyDisplay { get; set; }
        public DateTime? VisitDate { get; set; }
        public string VisitDescription { get; set; }
        public bool IsPatientGuarantor { get; set; }
        public bool IsPatientMinor { get; set; }
        public MatchOptionEnum? MatchOption { get; set; }
    }
}

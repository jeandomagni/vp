namespace Ivh.Domain.FinanceManagement.Entities
{
    using System;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class PaymentMethodAuthorization
    {
        public PaymentMethodAuthorization()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        public virtual int PaymentMethodAuthorizationId { get; set; }

        public virtual PaymentMethodAuthorizationTypeEnum PaymentMethodAuthorizationType { get; set; }

        public virtual int CmsVersionId { get; set; }

        public virtual DateTime InsertDate { get; set; }

        public virtual decimal? AuthorizedAmount { get; set; }

        public virtual int? AuthorizedDuration { get; set; }

        public virtual Guid CorrelationGuid { get; set; }

        public virtual PaymentMethod PaymentMethod { get; set; }
    }
}
namespace Ivh.Domain.FinanceManagement.Entities
{
    public class PaymentMethodBillingAddress
    {
        /// <summary>
        /// Gets or sets the PaymentMethodBillingAddressId.
        /// </summary>
        /// <value>
        /// The Payment Method Billing Address id.
        /// </value>
        public virtual int PaymentMethodBillingAddressId { get; set; }

        /// <summary>
        /// Gets or sets the Address1.
        /// </summary>
        /// <value>
        /// The Address1.
        /// </value>
        public virtual string Address1 { get; set; }

        /// <summary>
        /// Gets or sets the Address2.
        /// </summary>
        /// <value>
        /// Address2.
        /// </value>
        public virtual string Address2 { get; set; }

        /// <summary>
        /// Gets or sets the City.
        /// </summary>
        /// <value>
        /// The City.
        /// </value>
        public virtual string City { get; set; }

        /// <summary>
        /// Gets or sets the State.
        /// </summary>
        /// <value>
        /// The State.
        /// </value>
        public virtual string State { get; set; }

        /// <summary>
        /// Gets or sets the Zip.
        /// </summary>
        /// <value>
        /// The Zip.
        /// </value>
        public virtual string Zip { get; set; }
    }
}
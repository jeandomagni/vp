namespace Ivh.Domain.FinanceManagement.Entities
{
	using System;
	using Common.Base.Enums;
	using Common.VisitPay.Enums;

    public class PaymentMethodEvent
	{
	    public PaymentMethodEvent()
	    {
	        this.InsertDate = DateTime.UtcNow;
	    }
		public virtual int PaymentMethodEventId{ get; set; }
		public virtual PaymentMethodStatusEnum PaymentMethodStatus{ get; set; }
		public virtual PaymentMethod PaymentMethod{ get; set; }
		public virtual int VpGuarantorId{ get; set; }
		public virtual int VisitPayUserId { get; set; }
		public virtual DateTime InsertDate { get; set; }
	}
}
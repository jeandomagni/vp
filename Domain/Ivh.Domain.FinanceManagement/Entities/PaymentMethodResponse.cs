﻿namespace Ivh.Domain.FinanceManagement.Entities
{
    public class PaymentMethodResponse
    {
        public PaymentMethodResponse(bool isSuccess, string errorMessage = null, bool primaryChanged = false, PaymentMethod paymentMethod = null)
        {
            this.IsError = !isSuccess;
            this.IsSuccess = isSuccess;
            this.ErrorMessage = errorMessage;
            this.PrimaryChanged = primaryChanged;
            this.PaymentMethod = paymentMethod;
        }

        public bool IsError { get; private set; }
        public bool IsSuccess { get; private set; }
        public string ErrorMessage { get; private set; }
        public bool PrimaryChanged { get; private set; }
        public PaymentMethod PaymentMethod { get; private set; }
    }
}

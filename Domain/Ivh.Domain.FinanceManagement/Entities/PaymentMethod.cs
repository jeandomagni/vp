namespace Ivh.Domain.FinanceManagement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.VisitPay.Enums;

    public class PaymentMethod
    {
        public virtual int PaymentMethodId { get; set; }

        public virtual int? VpGuarantorId { get; set; }

        public virtual bool IsActive { get; set; }

        public virtual bool IsPrimary { get; set; }

        public virtual bool IsRegistrationPaymentMethod { get; set; }

        public virtual string GatewayToken { get; set; }

        public virtual string BillingId { get; set; }

        public virtual PaymentMethodTypeEnum PaymentMethodType { get; set; }

        public virtual PaymentMethodAccountType PaymentMethodAccountType { get; set; }

        public virtual PaymentMethodProviderType PaymentMethodProviderType { get; set; }

        public virtual DateTime CreatedDate { get; set; }

        public virtual DateTime UpdatedDate { get; set; }

        public virtual string LastFour { get; set; }

        public virtual int CreatedUserId { get; set; }

        public virtual int UpdatedUserId { get; set; }

        public virtual string AccountNickName { get; set; }

        public virtual DateTime? ExpiringOn { get; set; }

        public virtual DateTime? ExpiredOn { get; set; }

        #region AchSpecific

        public virtual string FirstName { get; set; }

        public virtual string LastName { get; set; }

        public virtual string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);

        public virtual string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);

        public virtual string BankName { get; set; }

        #endregion AchSpecific

        #region CardSpecific

        public virtual string NameOnCard { get; set; }

        public virtual string ExpDate { get; set; }

        #endregion CardSpecific

        private IList<PaymentMethodBillingAddress> _billingAddresses;

        public virtual IList<PaymentMethodBillingAddress> BillingAddresses
        {
            get => this._billingAddresses ?? (this.BillingAddresses = new List<PaymentMethodBillingAddress>());
            protected set => this._billingAddresses = value;
        }

        #region type helpers

        public virtual bool IsAchType => this.PaymentMethodType.IsInCategory(PaymentMethodTypeEnumCategory.Ach);

        public virtual bool IsCardType => this.PaymentMethodType.IsInCategory(PaymentMethodTypeEnumCategory.Card);

        public virtual bool IsLockBoxType => this.PaymentMethodType.IsInCategory(PaymentMethodTypeEnumCategory.LockBox);

        #endregion

        #region expiration helpers

        public virtual DateTime? ExpDateTime
        {
            get
            {
                DateTime expDateTime;
                if (DateTime.TryParseExact("01" + this.ExpDate, "ddMMyy", null, DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal, out expDateTime))
                {
                    return expDateTime.AddMonths(1);
                }

                return null;
            }
        }

        public virtual string ExpDateForDisplay
        {
            get
            {
                if (this.IsAchType)
                {
                    return string.Empty;
                }

                if (this.IsCardType)
                {
                    return this.ExpDateTime.HasValue ? this.ExpDateTime.Value.AddMonths(-1).ToExpDateDisplay() : "MM/YYYY";
                }
                
                return string.Empty;
            }
        }

        public virtual bool IsExpired => this.CheckExpiration(DateTime.UtcNow.Date);

        public virtual bool IsExpiring => this.CheckExpiration(DateTime.UtcNow.AddMonths(2).Date);

        public virtual bool IsNearExpiry(DateTime checkDate)
        {
            return this.CheckExpiration(checkDate.Date.AddTicks(1));
        }

        private bool CheckExpiration(DateTime checkDate)
        {
            if (!this.IsCardType)
            {
                return false; // ACH doesn't expire
            }

            return !this.ExpDateTime.HasValue || this.ExpDateTime.Value.Date <= checkDate;
        }

        #endregion

        #region display helpers

        public virtual string DisplayName
        {
            get
            {
                if (this.IsAchType)
                {
                    return string.IsNullOrEmpty(this.AccountNickName) ? this.BankName : this.AccountNickName;
                }

                return this.AccountNickName;
            }
        }

        public virtual string ExpirationMessage
        {
            get
            {
                if (this.IsCardType)
                {
                    if (this.IsExpired)
                    {
                        return "This credit card has expired.";
                    }

                    if (this.IsExpiring)
                    {
                        return "This credit card will expire soon.";
                    }
                }

                return string.Empty;
            }
        }

        #endregion

        #region deactivate

        public virtual void Deactivate()
        {
            this.IsActive = false;
            //Need to keep the BillingId in case we create a payment from an existing payment that used this deactivated PaymentMethod
        }

        public virtual bool IsRemoveable { get; protected set; } // not mapped

        public virtual string RemoveableMessage { get; protected set; } // not mapped

        protected internal virtual void SetRemoveable(bool isRemoveable, string message = null)
        {
            this.IsRemoveable = isRemoveable;
            this.RemoveableMessage = message;
        }

        public virtual bool CanSetPrimary { get; protected set; } // not mapped

        protected internal virtual void SetPrimaryPermission(bool canSetPrimary)
        {
            this.CanSetPrimary = canSetPrimary;
        }

        #endregion

        #region authorizations
        
        private IList<PaymentMethodAuthorization> _paymentMethodAuthorizations;

        public virtual IList<PaymentMethodAuthorization> PaymentMethodAuthorizations
        {
            get => this._paymentMethodAuthorizations ?? (this.PaymentMethodAuthorizations = new List<PaymentMethodAuthorization>());
            protected set => this._paymentMethodAuthorizations = value;
        }

        public virtual PaymentMethodAuthorization AddAchAuthorization(int cmsVersionId, decimal authorizedAmount, int authorizedDuration)
        {
            PaymentMethodAuthorization authorization = new PaymentMethodAuthorization
            {
                PaymentMethodAuthorizationType = PaymentMethodAuthorizationTypeEnum.AchAuthorization,
                CmsVersionId = cmsVersionId,
                AuthorizedAmount = authorizedAmount,
                AuthorizedDuration = authorizedDuration,
                PaymentMethod = this,
            };

            this.PaymentMethodAuthorizations.Add(authorization);

            return authorization;
        }

        #endregion
    }
}
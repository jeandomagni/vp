﻿
namespace Ivh.Domain.FinanceManagement.Entities
{
    public class PaymentMethodAccountType
    {
        public virtual int PaymentMethodAccountTypeId { get; set; }

        public virtual string PaymentMethodAccountTypeText { get; set; }

        public virtual string PaymentMethodAccountTypeListDisplayText { get; set; }

        public virtual string PaymentMethodAccountTypeSelectedDisplayText { get; set; }

        public virtual int DisplayOrder { get; set; }
    }
}

﻿
namespace Ivh.Domain.FinanceManagement.Entities
{
    public class PaymentMethodProviderType
    {
        public virtual int PaymentMethodProviderTypeId { get; set; }

        public virtual string PaymentMethodProviderTypeText { get; set; }

        public virtual string PaymentMethodProviderTypeDisplayText { get; set; }

        public virtual string ImageName { get; set; }

        public virtual int DisplayOrder { get; set; }
    }
}

﻿namespace Ivh.Domain.FinanceManagement.ServiceGroup.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class ServiceGroupMerchantAccountMap : ClassMap<ServiceGroupMerchantAccount>
    {
        public ServiceGroupMerchantAccountMap()
        {
            this.Schema("dbo");
            this.Table("ServiceGroupMerchantAccount");
            this.Id(x => x.ServiceGroupMerchantAccountId);
            this.Map(x => x.ServiceGroupId).Nullable();
            this.References(x => x.MerchantAccount).Column("MerchantAccountId");

            this.Cache.ReadOnly();
        }
    }
}
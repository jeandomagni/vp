﻿namespace Ivh.Domain.FinanceManagement.ServiceGroup.Mappings
{
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class ServiceGroupMap : ClassMap<ServiceGroup>
    {
        public ServiceGroupMap()
        {
            this.Schema("dbo");
            this.Table("ServiceGroup");
            this.ReadOnly();
            this.Id(x => x.ServiceGroupId);
            this.Map(x => x.ServiceGroupName).Not.Nullable();
            this.Map(x => x.Logo).Nullable();
            this.Map(x => x.DescriptionCmsRegion).Column("DescriptionCmsRegionId").CustomType<CmsRegionEnum?>().Nullable();
            this.Map(x => x.StatementDisclaimerCmsRegion).Column("StatementDisclaimerCmsRegionId").CustomType<CmsRegionEnum?>().Nullable();

            this.Cache.ReadOnly();
        }
    }
}
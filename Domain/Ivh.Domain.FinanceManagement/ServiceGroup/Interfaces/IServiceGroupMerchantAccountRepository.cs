﻿namespace Ivh.Domain.FinanceManagement.ServiceGroup.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;
    using Payment.Entities;

    public interface IServiceGroupMerchantAccountRepository : IRepository<ServiceGroupMerchantAccount>
    {
        int? GetMerchantAccountId(int? serviceGroupId);
        MerchantAccount GetMerchantAccoutByServiceGroupId(int? serviceGroupId);
    }
}
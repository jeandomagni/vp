﻿namespace Ivh.Domain.FinanceManagement.ServiceGroup.Interfaces
{
    using System.Collections.Generic;
    using Entities;

    public interface IServiceGroupService
    {
        IList<ServiceGroup> GetAllServiceGroups();
    }
}
﻿namespace Ivh.Domain.FinanceManagement.ServiceGroup.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IServiceGroupRepository : IRepository<ServiceGroup>
    {
    }
}
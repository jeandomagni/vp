﻿namespace Ivh.Domain.FinanceManagement.ServiceGroup.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Entities;
    using Interfaces;

    public class ServiceGroupService : IServiceGroupService
    {
        private readonly Lazy<IServiceGroupRepository> _serviceGroupRepository;

        public ServiceGroupService(
            Lazy<IServiceGroupRepository> serviceGroupRepository)
        {
            this._serviceGroupRepository = serviceGroupRepository;
        }

        public IList<ServiceGroup> GetAllServiceGroups()
        {
            List<ServiceGroup> serviceGroups = this._serviceGroupRepository.Value.GetQueryable().ToList();

            return serviceGroups;
        }
    }
}
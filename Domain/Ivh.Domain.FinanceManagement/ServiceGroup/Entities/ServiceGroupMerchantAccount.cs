﻿namespace Ivh.Domain.FinanceManagement.ServiceGroup.Entities
{
    using Payment.Entities;

    public class ServiceGroupMerchantAccount
    {
        public virtual int ServiceGroupMerchantAccountId { get; set; }
        public virtual int? ServiceGroupId { get; set; }
        public virtual MerchantAccount MerchantAccount { get; set; }
    }
}

﻿namespace Ivh.Domain.FinanceManagement.ServiceGroup.Entities
{
    using Common.VisitPay.Enums;

    public class ServiceGroup
    {
        public virtual int ServiceGroupId { get; set; }

        public virtual string ServiceGroupName { get; set; }

        public virtual string Logo { get; set; }

        public virtual CmsRegionEnum? DescriptionCmsRegion { get; set; }

        public virtual CmsRegionEnum? StatementDisclaimerCmsRegion { get; set; }
    }
}
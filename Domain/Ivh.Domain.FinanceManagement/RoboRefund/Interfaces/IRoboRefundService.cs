﻿namespace Ivh.Domain.FinanceManagement.RoboRefund.Interfaces
{
    using System.Collections.Generic;
    using Entities;

    public interface IRoboRefundService
    {
        IList<RoboRefundPayment> RoboRefund(int guarantorId);
        void AddNonNaturalQualifyingEvents(int guarantorId);
        void AssociateReallocationPaymentToRoboPayment(int paymentId, int financePlanId, RoboRefundPayment roboRefundPayment, Dictionary<int, decimal> visitAmounts);
    }
}
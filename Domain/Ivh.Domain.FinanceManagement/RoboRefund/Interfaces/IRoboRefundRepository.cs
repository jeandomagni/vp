﻿namespace Ivh.Domain.FinanceManagement.RoboRefund.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IRoboRefundRepository : IRepository<RoboRefundPayment>
    {
        IList<RoboRefundEventResult> GetUnRefundedEventResults(int guarantorId);
    }
}
﻿namespace Ivh.Domain.FinanceManagement.RoboRefund.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;
    using Entities;
    using FinancePlan.Entities;
    using FinancePlan.Interfaces;
    using Interfaces;
    using Newtonsoft.Json;
    using Statement.Entities;
    using Statement.Interfaces;

    public class RoboRefundService : IRoboRefundService
    {
        private readonly Lazy<IRoboRefundRepository> _roboRefundRepo;
        private readonly Lazy<IFinancePlanRepository> _financePlanRepository;
        private readonly Lazy<IFinancePlanService> _financePlanService;
        private readonly Lazy<IVpStatementRepository> _vpStatementRepository;


        private RoboRefundPayment CreatePayment(int vpGuarantorId, List<RoboRefundEventResult> resultsForVpg)
        {
            RoboRefundPayment currentPayment = new RoboRefundPayment()
            {
                VpGuarantorId = vpGuarantorId
            };
            
            //Group by the VisitId
            foreach (IGrouping<int, RoboRefundEventResult> roboRefundEventResultGrouping in resultsForVpg.GroupBy(x => x.VisitId))
            {
                RoboRefundVisit currentVisit = this.CreateRoboVisit(currentPayment, roboRefundEventResultGrouping);
                currentPayment.Visits.Add(currentVisit);
            }
            
            return currentPayment;
        }

        private RoboRefundVisit CreateRoboVisit(RoboRefundPayment currentPayment, IGrouping<int, RoboRefundEventResult> roboRefundEventResultGrouping)
        {
            RoboRefundVisit currentVisit = new RoboRefundVisit()
            {
                RoboRefundPayment = currentPayment,
                VisitId = roboRefundEventResultGrouping.Key,
            };
            foreach (RoboRefundEventResult roboRefundEventResult in roboRefundEventResultGrouping.ToList())
            {
                currentVisit.QualifyingEvents.Add(new RoboRefundQualifyingEvent()
                {
                    RoboRefundVisit = currentVisit,
                    AdjustmentAmount = roboRefundEventResult.Amount,
                    FinancePlanVisitPrincipalAmountId = roboRefundEventResult.FinancePlanVisitPrincipalAmountId,
                    FinancePlanVisitId = roboRefundEventResult.FinancePlanVisitId,
                    FinancePlanVisitPrincipalAmountOverrideInsertDate = roboRefundEventResult.FinancePlanVisitPrincipalAmountOverrideInsertDate,
                    FinancePlanVisitPrincipalAmountInsertDate = roboRefundEventResult.FinancePlanVisitPrincipalAmountInsertDate
                });
            }
            return currentVisit;
        }


        public RoboRefundService(
            Lazy<IRoboRefundRepository> roboRefundRepo,
            Lazy<IFinancePlanRepository> financePlanRepository,
            Lazy<IFinancePlanService> financePlanService,
            Lazy<IVpStatementRepository> vpStatementRepository
            )
        {
            this._roboRefundRepo = roboRefundRepo;
            this._financePlanRepository = financePlanRepository;
            this._financePlanService = financePlanService;
            this._vpStatementRepository = vpStatementRepository;
        }

        public IList<RoboRefundPayment> RoboRefund(int guarantorId)
        {
            IList<RoboRefundEventResult> results = this._roboRefundRepo.Value.GetUnRefundedEventResults(guarantorId);
           
            IList<RoboRefundPayment> allRefundPayments = new List<RoboRefundPayment>();
            //Initialize the new payment objects
            //Group by VPG
            //Should only ever be 1, Leaving it like this because I could see refactoring it to run in batch every night.
            foreach (IGrouping<int, RoboRefundEventResult> refundEventResults in results.GroupBy(x => x.VpGuarantorId))
            {
                RoboRefundPayment currentPayment = this.CreatePayment(refundEventResults.Key, refundEventResults.ToList());
                
                allRefundPayments.Add(currentPayment);
            }
            //Payments are created, Visits are created, Natural Qualifying(Happened because negative adjustment on active FP) events are created
            
            foreach (RoboRefundPayment roboRefundPayment in allRefundPayments)
            {
                IList<FinancePlan> allFinancePlansWithVisits = this._financePlanRepository.Value.GetAllOriginatedFinancePlansIncludingClosedFromVisits(results.Select(x => x.VisitId).Distinct().ToList());

                //Get all FinancePlanVisits
                this.PopulateFinancePlanVisits(roboRefundPayment, allFinancePlansWithVisits);
            }

            foreach (RoboRefundPayment roboRefundPayment in allRefundPayments)
            {
                this._roboRefundRepo.Value.InsertOrUpdate(roboRefundPayment);
            }

            return allRefundPayments;
        }

        public void AddNonNaturalQualifyingEvents(int guarantorId)
        {
            
            IList<RoboRefundEventResult> results = this._roboRefundRepo.Value.GetUnRefundedEventResults(guarantorId);
            
            //Retro actively add principal amounts to older fps, English: if there was a natural Qualifying event, did that FPV have older closed FPVs that should also get a principal adjustment?
            //Get all financeplans with the visits in question.
            IList<FinancePlan> allFinancePlansWithVisits = this._financePlanRepository.Value.GetAllOriginatedFinancePlansIncludingClosedFromVisits(results.Select(x => x.VisitId).Distinct().ToList());

            //Check if the Qualifying events have older finance plan visits that didnt get events because they were closed.
            
            DateTime now = DateTime.UtcNow;
            //Questions:
            // FP gets event during statement period
            // PrincipalAmount added
            // User combos
            // new event is taken into account for the combo, since it should be reflected in the current hs balance

            foreach (RoboRefundEventResult naturalQualifyingEvent in results)
            {
                //Is this a naturalQualifyingEvent??
                if(allFinancePlansWithVisits
                    .SelectMany(x => x.FinancePlanVisits)
                    .SelectMany(y => y.FinancePlanVisitPrincipalAmounts)
                    .Any(z => z.ParentFinancePlanVisitPrincipalAmountId == naturalQualifyingEvent.FinancePlanVisitPrincipalAmountId))
                {
                    //Not natural event or it's been taken care of
                    continue;
                }
                
                DateTime? naturalQualifyingEventsFinancePlanOriginationDate = allFinancePlansWithVisits
                    .FirstOrDefault(x => x.FinancePlanVisits.Any(y => y.FinancePlanVisitId == naturalQualifyingEvent.FinancePlanVisitId))?.OriginationDate;

                FinancePlanVisitPrincipalAmount principalAmountInQuestion = allFinancePlansWithVisits
                    .SelectMany(x => x.FinancePlanVisits).FirstOrDefault(y => y.FinancePlanVisitId == naturalQualifyingEvent.FinancePlanVisitId)?
                    .FinancePlanVisitPrincipalAmounts.FirstOrDefault(z => z.FinancePlanVisitPrincipalAmountId == naturalQualifyingEvent.FinancePlanVisitPrincipalAmountId 
                                                                          && z.ParentFinancePlanVisitPrincipalAmountId == null);

                List<FinancePlanVisit> financePlanVisitsInPreviousFinancePlansForVisit =
                    //Only finance plans that are older than the origination of event, as newer fps should already take the event into account
                    allFinancePlansWithVisits.Where(z => z.OriginationDate < naturalQualifyingEventsFinancePlanOriginationDate)
                        .SelectMany(x => x.FinancePlanVisits)
                        //All FinancePlanVisits with same visit id
                        //Exclude the FPV that already has the event
                        .Where(y => y.VisitId == naturalQualifyingEvent.VisitId && y.FinancePlanVisitId != naturalQualifyingEvent.FinancePlanVisitId)
                        .GroupBy(p => p.FinancePlanVisitId)
                        .Select(g => g.First())
                        .ToList();

                //create new principal amounts and events for the new amounts
                if (!financePlanVisitsInPreviousFinancePlansForVisit.Any() || principalAmountInQuestion == null)
                {
                    continue;
                }

                foreach (FinancePlanVisit previousFpvVisit in financePlanVisitsInPreviousFinancePlansForVisit)
                {
                    FinancePlanVisitPrincipalAmount firstPrincipalAmount = previousFpvVisit.FinancePlanVisitPrincipalAmounts.OrderBy(y => y.InsertDate).FirstOrDefault(x => x.FinancePlanVisitPrincipalAmountType == FinancePlanVisitPrincipalAmountTypeEnum.FinancePlanCreated);
                    
                    //Calculate if the full amount should be used, this will be a negative or 0 amount
                    decimal sumOfClosed = previousFpvVisit.FinancePlanVisitPrincipalAmounts.Where(y => y.FinancePlanVisitPrincipalAmountType == FinancePlanVisitPrincipalAmountTypeEnum.FinancePlanClosed).Sum(x => x.Amount);
                    //-1500 (sumOfClosed) +  (100(principalAmountInQuestion.Amount)) = -1400
                    decimal amountToUseToAdjustClose = decimal.Negate(principalAmountInQuestion.Amount);
                    if ((sumOfClosed +amountToUseToAdjustClose) > 0)
                    {
                        //Idea here is that we can only remove the closed adjustment, so if the adjustment is more
                        amountToUseToAdjustClose = decimal.Negate(sumOfClosed);
                    }

                    FinancePlanVisitPrincipalAmount newNonNaturalAdjustment = new FinancePlanVisitPrincipalAmount
                    {
                        FinancePlan = previousFpvVisit.FinancePlan,
                        Amount = principalAmountInQuestion.Amount,
                        FinancePlanVisit = previousFpvVisit,
                        FinancePlanVisitPrincipalAmountType = FinancePlanVisitPrincipalAmountTypeEnum.Adjustment,
                        InsertDate = now,
                        OverrideInsertDate = firstPrincipalAmount?.InsertDate ?? previousFpvVisit.FinancePlan.OriginationDateTime(),
                        ParentFinancePlanVisitPrincipalAmountId = principalAmountInQuestion.FinancePlanVisitPrincipalAmountId
                    };
                    previousFpvVisit.FinancePlanVisitPrincipalAmounts.Add(newNonNaturalAdjustment);                    
                    FinancePlanVisitPrincipalAmount firstClosedPrincipalAmount = previousFpvVisit.FinancePlanVisitPrincipalAmounts.OrderBy(y => y.InsertDate).FirstOrDefault(x => x.FinancePlanVisitPrincipalAmountType == FinancePlanVisitPrincipalAmountTypeEnum.FinancePlanClosed);
                    if (firstClosedPrincipalAmount != null)
                    {
                        FinancePlanVisitPrincipalAmount newNonNaturalClosedAdjustment = new FinancePlanVisitPrincipalAmount
                        {
                            FinancePlan = previousFpvVisit.FinancePlan,
                            Amount = amountToUseToAdjustClose,
                            FinancePlanVisit = previousFpvVisit,
                            FinancePlanVisitPrincipalAmountType = FinancePlanVisitPrincipalAmountTypeEnum.FinancePlanClosed,
                            OverrideInsertDate = firstClosedPrincipalAmount?.InsertDate,
                            InsertDate = now,
                            ParentFinancePlanVisitPrincipalAmountId = principalAmountInQuestion.FinancePlanVisitPrincipalAmountId
                        };
                        previousFpvVisit.FinancePlanVisitPrincipalAmounts.Add(newNonNaturalClosedAdjustment);
                    }

                    this._financePlanService.Value.UpdateFinancePlan(previousFpvVisit.FinancePlan);
                }
            }
        }

        public void AssociateReallocationPaymentToRoboPayment(int paymentId, int financePlanId, RoboRefundPayment roboRefundPayment, Dictionary<int, decimal> visitAmounts)
        {
            roboRefundPayment.PaymentRoboRefundPayments.Add(new PaymentRoboRefundPayment()
            {
                FinancePlanId = financePlanId,
                PaymentId = paymentId,
                InsertDate = DateTime.UtcNow,
                RoboRefundPayment = roboRefundPayment,
                JsonData = JsonConvert.SerializeObject(visitAmounts),
                TotalAmount = visitAmounts.Values.Sum()
            });
            this._roboRefundRepo.Value.InsertOrUpdate(roboRefundPayment);
        }

        private void PopulateFinancePlanVisits(RoboRefundPayment roboRefundPayment, IList<FinancePlan> allFinancePlansWithVisits)
        {
            IList<StatementPeriodResult> statementPeriodsForVpg = this._vpStatementRepository.Value.GetAllStatementPeriodsForGuarantor(roboRefundPayment.VpGuarantorId);

            foreach (RoboRefundVisit roboRefundVisit in roboRefundPayment.Visits)
            {
                HashSet<int> newAdjustmentsForVisit = roboRefundVisit.QualifyingEvents
                    .Select(x => x.FinancePlanVisitPrincipalAmountId).ToHashSet();
                IList<FinancePlanVisit> allFinancePlanVisits = allFinancePlansWithVisits.SelectMany(y => y.FinancePlanVisits)
                    .Where(z => z.VisitId == roboRefundVisit.VisitId)
                    .GroupBy(p => p.FinancePlanVisitId)
                    .Select(g => g.First())
                    .OrderBy(a => a.FinancePlan.OriginationDateTime())
                    .ToList();
                foreach (FinancePlanVisit financePlanVisit in allFinancePlanVisits)
                {
                    RoboRefundFinancePlanVisit roboRefundFinancePlanVisit = this.CreateRoboFinancePlanVisit(financePlanVisit, roboRefundVisit, newAdjustmentsForVisit, statementPeriodsForVpg, allFinancePlanVisits);
                    roboRefundVisit.RoboRefundFinancePlanVisits.Add(roboRefundFinancePlanVisit);
                }
            }
        }

        private RoboRefundFinancePlanVisit CreateRoboFinancePlanVisit(FinancePlanVisit financePlanVisit, RoboRefundVisit roboRefundVisit, HashSet<int> newAdjustments, IList<StatementPeriodResult> statementPeriodsForVpg, IList<FinancePlanVisit> allFinancePlanVisits)
        {
            RoboRefundFinancePlanVisit roboRefundFinancePlanVisit = new RoboRefundFinancePlanVisit()
            {
                FinancePlanVisitId = financePlanVisit.FinancePlanVisitId,
                FinancePlanId = financePlanVisit.FinancePlan.FinancePlanId,
                RoboRefundVisit = roboRefundVisit,
                InterestAssessedPreviousWithWrittenOff = financePlanVisit.InterestAssessed,
                //This field is very important
                InterestPaidPrevious = financePlanVisit.InterestPaid,
                //This field is very important
                PrincipalPaidPrevious = financePlanVisit.PrincipalPaid,
                StartingBalanceOriginal = financePlanVisit.FinancePlanVisitPrincipalAmounts
                    .Where(x => x.FinancePlanVisitPrincipalAmountType == FinancePlanVisitPrincipalAmountTypeEnum.FinancePlanCreated)
                    .Sum(y => y.Amount),
                StartingBalancePrevious = financePlanVisit.FinancePlanVisitPrincipalAmounts
                    .Where(x => x.FinancePlanVisitPrincipalAmountType == FinancePlanVisitPrincipalAmountTypeEnum.FinancePlanCreated 
                                || (x.FinancePlanVisitPrincipalAmountType == FinancePlanVisitPrincipalAmountTypeEnum.Adjustment && x.Amount < 0))
                    .Where(y => !newAdjustments.Contains(y.FinancePlanVisitPrincipalAmountId))
                    .Sum(z => z.Amount),
                StartingBalanceNew = financePlanVisit.FinancePlanVisitPrincipalAmounts
                    .Where(x => x.FinancePlanVisitPrincipalAmountType == FinancePlanVisitPrincipalAmountTypeEnum.FinancePlanCreated 
                                || (x.FinancePlanVisitPrincipalAmountType == FinancePlanVisitPrincipalAmountTypeEnum.Adjustment && x.Amount < 0))
                    .Sum(z => z.Amount),
                FinancePlanVisitStartDateTime = financePlanVisit.FinancePlanVisitPrincipalAmounts.FirstOrDefault(x => x.FinancePlanVisitPrincipalAmountType == FinancePlanVisitPrincipalAmountTypeEnum.FinancePlanCreated)?.OverrideInsertDate 
                                                ?? financePlanVisit.FinancePlanVisitPrincipalAmounts.FirstOrDefault(x => x.FinancePlanVisitPrincipalAmountType == FinancePlanVisitPrincipalAmountTypeEnum.FinancePlanCreated)?.InsertDate,
                FinancePlanVisitEndDateTime = financePlanVisit.FinancePlanVisitPrincipalAmounts.FirstOrDefault(x => x.FinancePlanVisitPrincipalAmountType == FinancePlanVisitPrincipalAmountTypeEnum.FinancePlanClosed)?.OverrideInsertDate 
                                              ?? financePlanVisit.FinancePlanVisitPrincipalAmounts.FirstOrDefault(x => x.FinancePlanVisitPrincipalAmountType == FinancePlanVisitPrincipalAmountTypeEnum.FinancePlanClosed)?.InsertDate 
                                              ?? financePlanVisit.HardRemoveDate,
                //This field is very important
                FinancePlanIsClosed = financePlanVisit.FinancePlan.FinancePlanStatus.IsClosed(),
            };
            //Calculate the previous interest, Since the interest gets written off from FP and moved to new, make sure not to include the write off, also do not include the additional interst on new...
            HashSet<int> allParentIds = allFinancePlanVisits.Where(x => x.VisitId == roboRefundVisit.VisitId)
                .SelectMany(y => y.FinancePlanVisitInterestDues)
                .Where(z => z.ParentFinancePlanVisitInterestDue != null)
                .Select(a => a.ParentFinancePlanVisitInterestDue.FinancePlanVisitInterestDueId).ToHashSet();

            //This one is special because dont want to include the written off interest, this is the only way to compare what assessed vs what should have assessed.
            //This field is very important
            roboRefundFinancePlanVisit.InterestAssessedPrevious = financePlanVisit.FinancePlanVisitInterestDues
                .Where(x => x.FinancePlanVisitInterestDueType == FinancePlanVisitInterestDueTypeEnum.InterestAssessment)
                .Where(x => !allParentIds.Contains(x.FinancePlanVisitInterestDueId))//Do not include the written off interst if it was moved to combo loan, we want this interest
                .Where(x => x.ParentFinancePlanVisitInterestDue == null)//Make sure it doesnt include interest from previous FP
                .Sum(x => x.InterestDue);
            
            //This is the amount that was written off for this visit.
            //Could probably derive this from the above InterestAssessedPrevious and InterestAssessedPreviousWithWrittenOff but this is more clear.
            roboRefundFinancePlanVisit.InterestWrittenOffForReconfigCombo =
                financePlanVisit.FinancePlanVisitInterestDues
                    .Where(x => x.FinancePlanVisitInterestDueType == FinancePlanVisitInterestDueTypeEnum.InterestAssessment && allParentIds.Contains(x.FinancePlanVisitInterestDueId))
                    .Sum(x => x.InterestDue);
            
            //This is the new child FP that will have the written off interest from combo'd or reconfiged FP
            //This field is very important
            roboRefundFinancePlanVisit.InterestAddedForReconfigCombo =
                financePlanVisit.FinancePlanVisitInterestDues
                    .Where(x => x.FinancePlanVisitInterestDueType == FinancePlanVisitInterestDueTypeEnum.InterestAssessment && x.ParentFinancePlanVisitInterestDue != null)
                    .Sum(x => x.InterestDue);
            
            //Setting the previous FinancePlanVisitId when there's write off and re-add
            roboRefundFinancePlanVisit.ParentFinancePlanVisitId = financePlanVisit.FinancePlanVisitInterestDues
                .Where(x => x.FinancePlanVisitInterestDueType == FinancePlanVisitInterestDueTypeEnum.InterestAssessment && x.ParentFinancePlanVisitInterestDue != null)
                .Select(y => y.ParentFinancePlanVisitInterestDue?.FinancePlanVisit?.FinancePlanVisitId).FirstOrDefault();
                
            //Get all periods for each visit
            HashSet<int> listOfStatements = financePlanVisit.FinancePlanVisitInterestDues
                .Where(x => x.FinancePlanVisitInterestDueType == FinancePlanVisitInterestDueTypeEnum.InterestAssessment 
                            && x.VpStatementId != null 
                            && x.ParentFinancePlanVisitInterestDue == null)  //Dont want interest that was moved to new FP.
                .Select(y => y.VpStatementId.Value).ToHashSet();
            List<StatementPeriodResult> statementPeriods = statementPeriodsForVpg
                .Where(x => listOfStatements.Contains(x.VpStatementId))
                .OrderBy(y => y.PeriodEndDate).ToList();

            List<RoboRefundFinancePlanVisitRecalculatedInterest> allNewCalculatedInterests = new List<RoboRefundFinancePlanVisitRecalculatedInterest>();
            foreach (StatementPeriodResult statementPeriodResult in statementPeriods)
            {
                RoboRefundFinancePlanVisitRecalculatedInterest interest = new RoboRefundFinancePlanVisitRecalculatedInterest()
                {
                    PeriodStartDate = statementPeriodResult.PeriodStartDate,
                    PeriodEndDate = statementPeriodResult.PeriodEndDate,
                    OldInterest = financePlanVisit.InterestAssessedForDateRange(statementPeriodResult.PeriodStartDate, statementPeriodResult.PeriodEndDate),
                    VpStatementId = statementPeriodResult.VpStatementId,
                    BalancesAsOf = this._financePlanService.Value.FinancePlanActiveVisitBalancesAsOf(financePlanVisit.ToListOfOne(), statementPeriodResult.PeriodStartDate, statementPeriodResult.PeriodEndDate, financePlanVisit.FinancePlan.InterestCalculationMethod)
                };
                //I wonder if I should try to get the daily data from the guts of this call?
                Dictionary<FinancePlanVisit, decimal> result = this._financePlanService.Value.IfAssessInterestFinancePlanVisits(financePlanVisit.ToListOfOne(), statementPeriodResult.PeriodStartDate, statementPeriodResult.PeriodEndDate);
                if (result.ContainsKey(financePlanVisit))
                {
                    interest.NewInterest = result[financePlanVisit];
                }
                allNewCalculatedInterests.Add(interest);
            }
            roboRefundFinancePlanVisit.NewReCalculatedInterests = allNewCalculatedInterests;
            
            return roboRefundFinancePlanVisit;
        }
    }
}
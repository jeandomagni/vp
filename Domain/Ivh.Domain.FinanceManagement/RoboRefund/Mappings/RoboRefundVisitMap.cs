﻿namespace Ivh.Domain.FinanceManagement.RoboRefund.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class RoboRefundVisitMap : ClassMap<RoboRefundVisit>
    {
        public RoboRefundVisitMap()
        {
            this.Schema("dbo");
            this.Table("RoboRefundVisit");
            this.Id(x => x.RoboRefundVisitId);
            
            this.Map(x => x.VisitId);

            //Before
            this.Map(x => x.PrincipalPaidPreviousSum);
            this.Map(x => x.InterestPaidPreviousSum);
            this.Map(x => x.InterestAssessedPreviousSum);

            //Result
            this.Map(x => x.PrincipalPaidAdjustedSum);
            this.Map(x => x.InterestPaidAdjustedSum);
            this.Map(x => x.InterestAssessedAdjustedSum);

            //Difference
            this.Map(x => x.PrincipalPaidDifferentialSum);
            this.Map(x => x.InterestPaidDifferentialSum);
            this.Map(x => x.InterestAssessedDifferentialSum);
            
            this.Map(x => x.AmountToReallocateFromInterestToPrincipalSum);
            this.Map(x => x.AmountOfInterestToWriteOffSum);
            
            this.References(x => x.RoboRefundPayment).Column(nameof(RoboRefundPayment.RoboRefundPaymentId)).Not.Nullable();

            this.HasMany(x => x.RoboRefundFinancePlanVisits)
                .KeyColumn(nameof(RoboRefundFinancePlanVisit.RoboRefundFinancePlanVisitId))
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();

            this.HasMany(x => x.QualifyingEvents)
                .KeyColumn(nameof(RoboRefundQualifyingEvent.RoboRefundQualifyingEventId))
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();
        }
    }
}
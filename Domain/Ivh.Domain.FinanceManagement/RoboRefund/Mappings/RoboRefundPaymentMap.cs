﻿namespace Ivh.Domain.FinanceManagement.RoboRefund.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class RoboRefundPaymentMap : ClassMap<RoboRefundPayment>
    {
        public RoboRefundPaymentMap()
        {
            this.Schema("dbo");
            this.Table("RoboRefundPayment");
            this.Id(x => x.RoboRefundPaymentId);
            this.Map(x => x.InsertDate);
            this.Map(x => x.VpGuarantorId);
            
            //Before
            this.Map(x => x.PrincipalPaidPreviousTotal);
            this.Map(x => x.InterestPaidPreviousTotal);
            this.Map(x => x.InterestAssessedPreviousTotal);

            //Result
            this.Map(x => x.PrincipalPaidAdjustedTotal);
            this.Map(x => x.InterestPaidAdjustedTotal);
            this.Map(x => x.InterestAssessedAdjustedTotal);

            //Difference
            this.Map(x => x.PrincipalPaidDifferentialTotal);
            this.Map(x => x.InterestPaidDifferentialTotal);
            this.Map(x => x.InterestAssessedDifferentialTotal);

            this.Map(x => x.AmountToReallocateFromInterestToPrincipalSum);
            this.Map(x => x.AmountOfInterestToWriteOffSum);
            
            this.HasMany(x => x.Visits)
                .KeyColumn(nameof(RoboRefundVisit.RoboRefundVisitId))
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();
            
            this.HasMany(x => x.PaymentRoboRefundPayments)
                .KeyColumn(nameof(PaymentRoboRefundPayment.PaymentRoboRefundPaymentId))
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();
        }
    }
}
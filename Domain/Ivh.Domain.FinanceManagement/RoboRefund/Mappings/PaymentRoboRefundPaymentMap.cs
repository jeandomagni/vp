﻿namespace Ivh.Domain.FinanceManagement.RoboRefund.Mappings
{
    using Common.Data.Extensions;
    using Entities;
    using FluentNHibernate.Mapping;

    public class PaymentRoboRefundPaymentMap : ClassMap<PaymentRoboRefundPayment>
    {
        public PaymentRoboRefundPaymentMap()
        {
            this.Schema("dbo");
            this.Table("PaymentRoboRefundPayment");
            this.Id(x => x.PaymentRoboRefundPaymentId);
            this.Map(x => x.InsertDate);
            this.Map(x => x.PaymentId);
            this.Map(x => x.FinancePlanId);
            this.Map(x => x.JsonData)
                //Prevents it from truncating
                .AsNVarcharMax();
            this.Map(x => x.TotalAmount);

            this.References(x => x.RoboRefundPayment).Column(nameof(RoboRefundPayment.RoboRefundPaymentId)).Not.Nullable();
        }
    }
}
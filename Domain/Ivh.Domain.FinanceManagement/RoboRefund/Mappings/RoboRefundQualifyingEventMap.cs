﻿namespace Ivh.Domain.FinanceManagement.RoboRefund.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class RoboRefundQualifyingEventMap : ClassMap<RoboRefundQualifyingEvent>
    {
        public RoboRefundQualifyingEventMap()
        {
            this.Schema("dbo");
            this.Table("RoboRefundQualifyingEvent");
            this.Id(x => x.RoboRefundQualifyingEventId);
            this.Map(x => x.InsertDate);
            this.Map(x => x.FinancePlanVisitPrincipalAmountId);
            this.Map(x => x.FinancePlanVisitPrincipalAmountInsertDate);
            this.Map(x => x.FinancePlanVisitPrincipalAmountOverrideInsertDate);
            this.Map(x => x.AdjustmentAmount);

            this.References(x => x.RoboRefundVisit).Column(nameof(RoboRefundVisit.RoboRefundVisitId)).Nullable();
        }
    }
}
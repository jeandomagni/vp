﻿namespace Ivh.Domain.FinanceManagement.RoboRefund.Mappings
{
    using System;
    using Common.Data.Extensions;
    using Entities;
    using FluentNHibernate.Mapping;

    public class RoboRefundFinancePlanVisitMap : ClassMap<RoboRefundFinancePlanVisit>
    {
        public RoboRefundFinancePlanVisitMap()
        {
            this.Schema("dbo");
            this.Table("RoboRefundFinancePlanVisit");
            this.Id(x => x.RoboRefundFinancePlanVisitId);

            this.Map(x => x.InsertDate);
            this.Map(x => x.FinancePlanVisitId)
                .Not.Nullable();
            this.Map(x => x.FinancePlanId)
                .Not.Nullable();

            this.Map(x => x.QualifyingEventTotal);
            this.Map(x => x.FinancePlanVisitStartDateTime);
            this.Map(x => x.FinancePlanVisitEndDateTime);

            this.Map(x => x.JsonData)
                //Prevents it from truncating
                .AsNVarcharMax();

            this.References(x => x.RoboRefundVisit).Column(nameof(RoboRefundVisit.RoboRefundVisitId))
                .Not.Nullable();
            
            //Before
            this.Map(x => x.PrincipalPaidPrevious);
            this.Map(x => x.InterestPaidPrevious);
            this.Map(x => x.InterestAssessedPrevious);
            this.Map(x => x.InterestAssessedPreviousWithParent);

            //Result
            this.Map(x => x.PrincipalPaidAdjusted);
            this.Map(x => x.InterestPaidAdjusted);
            this.Map(x => x.InterestAssessedAdjusted);
            this.Map(x => x.InterestAssessedAdjustedWithParent);
            
            //Difference
            this.Map(x => x.PrincipalPaidDifferential);
            this.Map(x => x.InterestPaidDifferential);
            this.Map(x => x.InterestAssessedDifferential);
            this.Map(x => x.InterestAssessedDifferentialWithParent);

            this.Map(x => x.AmountOfInterestToWriteOff);
            this.Map(x => x.AmountToReallocateFromInterestToPrincipal);
            this.Map(x => x.InterestWrittenOffForReconfigCombo);
            this.Map(x => x.InterestAddedForReconfigCombo);
            
        }
    }
}
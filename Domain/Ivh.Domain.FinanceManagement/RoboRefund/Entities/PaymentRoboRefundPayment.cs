﻿namespace Ivh.Domain.FinanceManagement.RoboRefund.Entities
{
    using System;

    public class PaymentRoboRefundPayment
    {
        public virtual int PaymentRoboRefundPaymentId { get; set; }

        public virtual RoboRefundPayment RoboRefundPayment { get; set; }

        public virtual DateTime InsertDate { get; set; }
        public virtual int PaymentId { get; set; }
        public virtual int FinancePlanId { get; set; }
        public virtual decimal TotalAmount { get; set; }
        public virtual string JsonData { get; set; }
    }
}
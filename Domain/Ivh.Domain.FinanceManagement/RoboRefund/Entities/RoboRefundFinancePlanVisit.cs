﻿namespace Ivh.Domain.FinanceManagement.RoboRefund.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Newtonsoft.Json;

    public class RoboRefundFinancePlanVisit
    {
        public RoboRefundFinancePlanVisit()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int RoboRefundFinancePlanVisitId { get; set; }

        public virtual RoboRefundVisit RoboRefundVisit { get; set; }

        public virtual int FinancePlanVisitId { get; set; }
        public virtual int FinancePlanId { get;set; }
        public virtual DateTime InsertDate { get; set; }

        public virtual string JsonData { get; set; }

        //Dont just add to this list, always set the whole list.
        private IList<RoboRefundFinancePlanVisitRecalculatedInterest> _newReCalculatedInterests;
        public virtual IList<RoboRefundFinancePlanVisitRecalculatedInterest> NewReCalculatedInterests
        {
            get
            {
                if(this._newReCalculatedInterests == null && this.JsonData != null)
                {
                    this._newReCalculatedInterests = JsonConvert.DeserializeObject<List<RoboRefundFinancePlanVisitRecalculatedInterest>>(this.JsonData);
                }
                return this._newReCalculatedInterests ?? new List<RoboRefundFinancePlanVisitRecalculatedInterest>();
            }
            set
            {
                this._newReCalculatedInterests = value;
                this.JsonData = JsonConvert.SerializeObject(value);
            }
        }

        public virtual decimal QualifyingEventTotal
        {
            //get => this.RoboRefundVisit.QualifyingEventTotalForDateRange(this.FinancePlanVisitStartDateTime, this.FinancePlanVisitEndDateTime ?? DateTime.MaxValue);
            get => this.RoboRefundVisit.QualifyingEvents.Where(x => x.FinancePlanVisitId == this.FinancePlanVisitId).Sum(y => y.AdjustmentAmount);
            set {}
        }

        public virtual DateTime? FinancePlanVisitStartDateTime { get; set; }
        public virtual DateTime? FinancePlanVisitEndDateTime { get; set; }

        public virtual decimal StartingBalanceOriginal { get; set; }
        public virtual decimal StartingBalancePrevious { get; set; }
        public virtual decimal StartingBalanceNew { get; set; }

        //Before
        //This should only include paid principal for this finance plan
        public virtual decimal PrincipalPaidPrevious { get;set; }
        //This should only include paid interest for this specific finance plan
        public virtual decimal InterestPaidPrevious { get; set; }
        //This should include the written off interest if it was moved the another FP.  Should not include interest moved from prior fp
        public virtual decimal InterestAssessedPrevious { get; set; }
        //This includes the interest plus the interest that was moved onto this FP
        public virtual decimal InterestAssessedPreviousWithParent
        {
            get { return this.InterestAssessedPrevious + this.InterestAddedForReconfigCombo; }
            set
            {
            }
        }

        //Result
        public virtual decimal PrincipalPaidAdjusted
        {
            get
            {
                return this.PrincipalPaidPrevious + decimal.Negate(this.InterestPaidDifferential);
            }
            set{}
        }
        
        public virtual decimal InterestPaidAdjusted
        {
            get
            {
                decimal calculatedInterestPaidAdjusted = 0m;
                //If the interest paid is greater than assessed, cant have paid more than interest assessed
                if(!this.FinancePlanIsClosed){
                    if (decimal.Negate(this.InterestPaidPrevious) > this.InterestAssessedAdjustedWithParent)
                    {
                        calculatedInterestPaidAdjusted = decimal.Negate(this.InterestAssessedAdjustedWithParent);
                    }
                    else
                    {
                        calculatedInterestPaidAdjusted = this.InterestPaidPrevious;
                    }
                }
                else
                {
                    //If the FP is closed, Cannot write off any interest.
                    //which means I need to reallocate as much as we can here, otherwise if there isnt another FP we're not giving what we can back.
                    if(this.InterestPaidPrevious - this.InterestAssessedDifferentialWithParent > 0)
                    {
                        //More intere
                        calculatedInterestPaidAdjusted = 0m;
                    }
                    else
                    {
                        calculatedInterestPaidAdjusted = this.InterestPaidPrevious - this.InterestAssessedDifferentialWithParent;
                    }
                }

                return calculatedInterestPaidAdjusted;
            }
            set
            {
            }
        }
        
        //This is the result for the visit with this finance plan only, does not include interest from previous FP
        public virtual decimal InterestAssessedAdjusted
        {
            get
            {
                decimal calculatedInterestAssessedAdjusted = 0m;
                //Dont charge more than before...
                if (this.SumOfRecalculatedInterest() > this.InterestAssessedPrevious)
                {
                    calculatedInterestAssessedAdjusted = this.InterestAssessedPrevious;
                }
                else
                {
                    calculatedInterestAssessedAdjusted = this.SumOfRecalculatedInterest();
                }

                return calculatedInterestAssessedAdjusted;
            }
            set{}
        }

        //This is the result for the visit with this finance plan and the previous FP, It does include interest from previous FP.
        //EG.  FP1 Assessed $2, Paid 1$ -> Combo Happens, FP2 (Starts with $1 assessed interest from FP1), then assesses $2 more.
        //This would report 3, where, InterestAssessedAdjusted should only report $2 (This example assumes a 0 dollar example, which isnt realistic, cause this whole process shouldnt trigger)
        public virtual decimal InterestAssessedAdjustedWithParent
        {
            get
            {
                decimal calculatedInterestAssessedAdjusted = 0m;
                //Dont charge more than before...
                if ((this.InterestAddedForReconfigCombo + this.SumOfRecalculatedInterest() + this.InterestFromParent) > (this.InterestAssessedPreviousWithParent))
                {
                    calculatedInterestAssessedAdjusted = this.InterestAssessedPreviousWithParent;
                }
                else
                {
                    calculatedInterestAssessedAdjusted = 0m;
                    //This cant be adjusted to less than 0
                    if(( this.InterestAddedForReconfigCombo +  this.SumOfRecalculatedInterest() + this.InterestFromParent) > 0m)
                    {
                        calculatedInterestAssessedAdjusted =  this.InterestAddedForReconfigCombo + this.SumOfRecalculatedInterest() + this.InterestFromParent;
                    }
                }

                return calculatedInterestAssessedAdjusted;
            }
            set{}
        }

        private decimal SumOfRecalculatedInterest()
        {
            //If the previous interest was 0, but it should have assessed something, dont add interest
            return this.NewReCalculatedInterests.Sum(x => x.Interest);
        }

        //Difference
        public virtual decimal PrincipalPaidDifferential
        {
            get
            {
                return decimal.Negate(this.PrincipalPaidPrevious - this.PrincipalPaidAdjusted);
            }
            set{}
        }

        public virtual decimal InterestPaidDifferential
        {
            get
            {
                return decimal.Negate(this.InterestPaidPrevious - this.InterestPaidAdjusted );
            }
            set{}
        }

        public virtual decimal InterestAssessedDifferential
        {
            get
            {
                return decimal.Negate(this.InterestAssessedPrevious - this.InterestAssessedAdjusted);
            }
            set{}
        }
        public virtual decimal InterestAssessedDifferentialWithParent
        {
            get
            {
                return decimal.Negate(this.InterestAssessedPreviousWithParent - this.InterestAssessedAdjustedWithParent);
            }
            set{}
        }

        public virtual decimal AmountToReallocateFromInterestToPrincipal
        {
            get
            {
                return decimal.Negate(this.InterestPaidDifferential);
            }
            set
            {
            }
        }

        public virtual decimal AmountOfInterestToWriteOff
        {
            get
            {
                //If the FP is closed, Cannot write off any interest.
                //which means I need to reallocate as much as we can here, otherwise if there isnt another FP we're not giving what we can back.
                if(this.FinancePlanIsClosed)
                {
                    return 0m;
                }

                decimal amountOfInterestToWriteOff = decimal.Negate(this.InterestAssessedDifferentialWithParent + this.InterestPaidDifferential);
                
                return amountOfInterestToWriteOff;
            }
            set
            {
            }
        }

        public virtual decimal InterestToMoveToParentFinancePlan
        {
            get
            {
                if (!this.FinancePlanIsClosed)
                {
                    return 0m;
                }
                return this.InterestAssessedDifferential + this.InterestPaidDifferential;
            }
        }

        public virtual decimal InterestAssessedPreviousWithWrittenOff { get; set; }
        public virtual bool FinancePlanIsClosed { get; set; }
        public virtual decimal InterestWrittenOffForReconfigCombo { get; set; }
        public virtual decimal InterestAddedForReconfigCombo { get; set; }
        public virtual int? ParentFinancePlanVisitId { get; set; }

        public virtual decimal InterestFromParent
        {
            get
            {
                RoboRefundFinancePlanVisit found = this.RoboRefundVisit.RoboRefundFinancePlanVisits.FirstOrDefault(x => x.FinancePlanVisitId == this.ParentFinancePlanVisitId);
                if(found == null)
                {
                    return 0m;
                }
                if(found.InterestToMoveToParentFinancePlan >= 0m)
                {
                    return 0m;
                }
                return found.InterestToMoveToParentFinancePlan;
            }
            set{}
        }
    }
}
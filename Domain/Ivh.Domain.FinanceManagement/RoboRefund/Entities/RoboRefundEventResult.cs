﻿namespace Ivh.Domain.FinanceManagement.RoboRefund.Entities
{
    using System;

    public class RoboRefundEventResult
    {
        public int VisitId { get; set; }
        public int VpGuarantorId { get; set; }
        public int FinancePlanVisitPrincipalAmountId { get; set; }
        public DateTime FinancePlanVisitPrincipalAmountInsertDate { get; set; }
        public DateTime? FinancePlanVisitPrincipalAmountOverrideInsertDate { get; set; }
        public int FinancePlanVisitId { get; set; }
        public decimal Amount { get; set; }
    }
}
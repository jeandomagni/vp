﻿namespace Ivh.Domain.FinanceManagement.RoboRefund.Entities
{
    using System;
    using System.Collections.Generic;

    public class RoboRefundFinancePlanVisitRecalculatedInterest
    {
        public int VpStatementId { get; set; }
        public DateTime PeriodStartDate { get;set; }
        public DateTime PeriodEndDate { get;set; }
        //recalculated from historical balances
        public decimal NewInterest { get; set; }
        //sum from before.
        public decimal OldInterest { get; set; }

        public decimal Interest
        {
            get { return Math.Min(this.NewInterest, this.OldInterest); }
        }

        public IDictionary<int, IList<Tuple<DateTime, decimal>>> BalancesAsOf { get; set; }
    }
}
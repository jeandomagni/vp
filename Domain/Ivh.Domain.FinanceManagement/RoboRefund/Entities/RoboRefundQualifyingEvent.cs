﻿namespace Ivh.Domain.FinanceManagement.RoboRefund.Entities
{
    using System;
    using FinancePlan.Entities;

    public class RoboRefundQualifyingEvent
    {
        public RoboRefundQualifyingEvent()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        public virtual int RoboRefundQualifyingEventId { get; set; }

        public virtual DateTime InsertDate { get; set; }

        public virtual RoboRefundVisit RoboRefundVisit { get; set; }

        public virtual int FinancePlanVisitPrincipalAmountId { get; set; }
        public virtual DateTime FinancePlanVisitPrincipalAmountInsertDate { get; set; }
        public virtual DateTime? FinancePlanVisitPrincipalAmountOverrideInsertDate { get; set; }
        public virtual int FinancePlanVisitId { get; set; }

        public virtual decimal AdjustmentAmount { get; set; }
        
    }
}
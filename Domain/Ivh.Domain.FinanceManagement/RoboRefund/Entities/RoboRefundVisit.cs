﻿namespace Ivh.Domain.FinanceManagement.RoboRefund.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class RoboRefundVisit
    {
        public RoboRefundVisit()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual IList<RoboRefundQualifyingEvent> QualifyingEvents { get;set; } = new List<RoboRefundQualifyingEvent>();
        public virtual IList<RoboRefundFinancePlanVisit> RoboRefundFinancePlanVisits { get;set; } = new List<RoboRefundFinancePlanVisit>();
        public virtual RoboRefundPayment RoboRefundPayment { get; set; }

        public virtual int RoboRefundVisitId { get; set; }
        public virtual int VisitId { get;set; }
        public virtual DateTime InsertDate { get; set; }
        
        //Before
        public virtual decimal PrincipalPaidPreviousSum
        {
            get { return this.RoboRefundFinancePlanVisits.Sum(x => x.PrincipalPaidPrevious); }
            set {}
        }

        public virtual decimal InterestPaidPreviousSum
        {
            get { return this.RoboRefundFinancePlanVisits.Sum(x => x.InterestPaidPrevious); }
            set {}
        }
        public virtual decimal InterestAssessedPreviousSum
        {
            get { return this.RoboRefundFinancePlanVisits.Sum(x => x.InterestAssessedPrevious); }
            set {}
        }

        //Result
        public virtual decimal PrincipalPaidAdjustedSum
        {
            get { return this.RoboRefundFinancePlanVisits.Sum(x => x.PrincipalPaidAdjusted); }
            set {}
        }
        public virtual decimal InterestPaidAdjustedSum
        {
            get { return this.RoboRefundFinancePlanVisits.Sum(x => x.InterestPaidAdjusted); }
            set {}
        }
        public virtual decimal InterestAssessedAdjustedSum
        {
            get { return this.RoboRefundFinancePlanVisits.Sum(x => x.InterestAssessedAdjusted); }
            set {}
        }


        //Difference
        public virtual decimal PrincipalPaidDifferentialSum
        {
            get { return this.RoboRefundFinancePlanVisits.Sum(x => x.PrincipalPaidDifferential); }
            set {}
        }
        public virtual decimal InterestPaidDifferentialSum
        {
            get { return this.RoboRefundFinancePlanVisits.Sum(x => x.InterestPaidDifferential); }
            set {}
        }
        public virtual decimal InterestAssessedDifferentialSum
        {
            get { return this.RoboRefundFinancePlanVisits.Sum(x => x.InterestAssessedDifferential); }
            set {}
        }
        
        
        
        public virtual decimal AmountToReallocateFromInterestToPrincipalSum
        {
            get { return this.RoboRefundFinancePlanVisits.Sum(x => x.AmountToReallocateFromInterestToPrincipal); }
            set {}
        }
        public virtual decimal AmountOfInterestToWriteOffSum
        {
            get { return this.RoboRefundFinancePlanVisits.Sum(x => x.AmountOfInterestToWriteOff); }
            set {}
        }
    }
}
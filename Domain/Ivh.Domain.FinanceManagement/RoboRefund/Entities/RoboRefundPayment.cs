﻿namespace Ivh.Domain.FinanceManagement.RoboRefund.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class RoboRefundPayment
    {
        public RoboRefundPayment()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        public virtual int RoboRefundPaymentId { get; set; }

        public virtual DateTime InsertDate { get; set; }
        
        public virtual IList<PaymentRoboRefundPayment> PaymentRoboRefundPayments { get; set; } = new List<PaymentRoboRefundPayment>();
        
        public virtual int VpGuarantorId { get; set; }

        public virtual IList<RoboRefundVisit> Visits { get; set; } = new List<RoboRefundVisit>();
        
        //Before
        public virtual decimal PrincipalPaidPreviousTotal
        {
            get { return this.Visits.Sum(x => x.PrincipalPaidPreviousSum); }
            set {}
        }

        public virtual decimal InterestPaidPreviousTotal
        {
            get { return this.Visits.Sum(x => x.InterestPaidPreviousSum); }
            set {}

        }
        public virtual decimal InterestAssessedPreviousTotal
        {
            get { return this.Visits.Sum(x => x.InterestAssessedPreviousSum); }
            set {}
        }

        //Result
        public virtual decimal PrincipalPaidAdjustedTotal
        {
            get { return this.Visits.Sum(x => x.PrincipalPaidAdjustedSum); }
            set {}
        }
        public virtual decimal InterestPaidAdjustedTotal
        {
            get { return this.Visits.Sum(x => x.InterestPaidAdjustedSum); }
            set {}
        }
        public virtual decimal InterestAssessedAdjustedTotal
        {
            get { return this.Visits.Sum(x => x.InterestAssessedAdjustedSum); }
            set {}
        }


        //Difference
        public virtual decimal PrincipalPaidDifferentialTotal
        {
            get { return this.Visits.Sum(x => x.PrincipalPaidDifferentialSum); }
            set {}
        }
        public virtual decimal InterestPaidDifferentialTotal
        {
            get { return this.Visits.Sum(x => x.InterestPaidDifferentialSum); }
            set {}
        }
        public virtual decimal InterestAssessedDifferentialTotal
        {
            get { return this.Visits.Sum(x => x.InterestAssessedDifferentialSum); }
            set {}
        }
        
        public virtual decimal AmountToReallocateFromInterestToPrincipalSum
        {
            get { return this.Visits.Sum(x => x.AmountToReallocateFromInterestToPrincipalSum); }
            set {}
        }
        public virtual decimal AmountOfInterestToWriteOffSum
        {
            get { return this.Visits.Sum(x => x.AmountOfInterestToWriteOffSum); }
            set {}
        }
    }
}
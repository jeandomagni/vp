﻿
namespace Ivh.Domain.FinanceManagement.Statement.Mapping
{
    using Entities;
    using FluentNHibernate.Mapping;
    using HospitalData.Outbound.Enums;

    public class VpStatementDocumentMap : ClassMap<VpStatementDocument>
    {
        public VpStatementDocumentMap()
        {
            this.BatchSize(50);
            this.Schema("dbo");
            this.Table("VpStatementDocument");

            this.Id(x => x.VpStatementDocumentId);
            this.Map(x => x.InsertDate);

            this.Map(x => x.VpStatementId).Not.Nullable();
            this.Map(x => x.VpOutboundFileType, "VpOutboundFileTypeId").CustomType<VpOutboundFileTypeEnum>().Not.Nullable();
            this.Map(x => x.FileStorageExternalKey).Not.Nullable();
            this.Map(x => x.FileName).Not.Nullable();
            this.Map(x => x.MimeType).Not.Nullable();
        }
    }
}

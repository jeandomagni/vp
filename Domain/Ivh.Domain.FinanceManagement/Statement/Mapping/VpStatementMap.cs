namespace Ivh.Domain.FinanceManagement.Statement.Mapping
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using FluentNHibernate.Mapping;
    using Entities;

    public class VpStatementMap : ClassMap<VpStatement>
    {
        public VpStatementMap()
        {
            this.BatchSize(50);
            this.Schema("dbo");
            this.Table("VpStatement");

            this.Id(x => x.VpStatementId);
            this.Map(x => x.InsertDate);

            this.Map(x => x.VpGuarantorId);
            this.Map(x => x.StatementDate);
            this.Map(x => x.VpStatementType, "VpStatementTypeId").CustomType<VpStatementTypeEnum>().Not.Nullable();
            this.Map(x => x.PeriodStartDate);
            this.Map(x => x.PeriodEndDate);
            this.Map(x => x.PaymentDueDate);
            this.Map(x => x.OriginalPaymentDueDate);

            this.Map(x => x.ProcessingStatus).CustomType<ChangeEventStatusEnum>();
            this.Map(x => x.UncollectableEmailSent);
            this.Map(x => x.PastDueEmailSent);
            this.Map(x => x.CreatePaperStatement);
            this.Map(x => x.PaperStatementSentDate).Nullable();
            this.Map(x => x.BalanceDueReminderMessageSent);
            this.Map(x => x.PriorStatementDate)
                .Nullable();
            this.Map(x => x.IsArchived)
                .Not.Nullable();
            this.Map(x => x.ArchivedFileMimeType);

            this.Map(x => x.StatementVersion)
                .CustomType<VpStatementVersionEnum>()
                .Not.Nullable();
            this.Map(x => x.JsonData)
                //.LazyLoad()
                .Not.Nullable();

            //Todo: Start: This should be deleted after migration.
            this.Map(x => x.LegacyStatemenID)
                .Nullable();
            //Todo: End: This should be deleted after migration.

            this.HasMany(x => x.VpStatementVisits)
                .KeyColumn("VpStatementId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();

            this.HasMany(x => x.VpStatementNotifications)
                .KeyColumn("VpStatementId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();
        }
    }
}
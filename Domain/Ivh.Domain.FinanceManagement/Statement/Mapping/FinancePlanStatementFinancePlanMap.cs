﻿namespace Ivh.Domain.FinanceManagement.Statement.Mapping
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class FinancePlanStatementFinancePlanMap : ClassMap<FinancePlanStatementFinancePlan>
    {
        public FinancePlanStatementFinancePlanMap()
        {
            this.BatchSize(50);
            this.Schema("dbo");
            this.Table("FinancePlanStatementFinancePlan");
            this.Id(x => x.FinancePlanStatementFinancePlanId);
            this.Map(x => x.InsertDate);
            this.References(x => x.FinancePlan)
                .Column("FinancePlanId")
                .Not.Nullable();
            this.Map(x => x.DeletedDate)
                .Nullable();
            this.References(x => x.FinancePlanStatement)
                .Column("FinancePlanStatementId")
                .Not.Nullable();
            this.Map(x => x.JsonData)
                //.LazyLoad()
                .Not.Nullable();
        }
    }
}
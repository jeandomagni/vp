﻿namespace Ivh.Domain.FinanceManagement.Statement.Mapping
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class FinancePlanStatementMap : ClassMap<FinancePlanStatement>
    {
        public FinancePlanStatementMap()
        {
            this.BatchSize(50);
            this.Schema("dbo");
            this.Table("FinancePlanStatement");

            this.Id(x => x.FinancePlanStatementId);
            this.Map(x => x.InsertDate);

            this.Map(x => x.VpStatementId);
            this.Map(x => x.VpGuarantorId);
            this.Map(x => x.StatementDate);
            this.Map(x => x.PeriodStartDate);
            this.Map(x => x.PeriodEndDate);
            this.Map(x => x.PaymentDueDate);
            this.Map(x => x.OriginalPaymentDueDate);

            this.Map(x => x.ProcessingStatus).CustomType<ChangeEventStatusEnum>();
            this.Map(x => x.PriorStatementDate)
                .Nullable();
            this.Map(x => x.StatementVersion)
                .CustomType<VpStatementVersionEnum>()
                .Not.Nullable();
            this.Map(x => x.JsonData)
                //.LazyLoad()
                .Not.Nullable();

            this.HasMany(x => x.FinancePlanStatementFinancePlans)
                .KeyColumn("FinancePlanStatementId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();


            this.HasMany(x => x.FinancePlanStatementVisits)
                .KeyColumn("FinancePlanStatementId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();

            this.HasMany(x => x.FinancePlanStatementNotifications)
                .KeyColumn("FinancePlanStatementId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();
        }
    }
}
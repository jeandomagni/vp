﻿namespace Ivh.Domain.FinanceManagement.Statement.Mapping
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class FinancePlanStatementVisitMap : ClassMap<FinancePlanStatementVisit>
    {
        public FinancePlanStatementVisitMap()
        {
            this.BatchSize(50);
            this.Schema("dbo");
            this.Table("FinancePlanStatementVisit");

            this.CompositeId()
                .KeyReference(sv => sv.FinancePlanStatement,"FinancePlanStatementId")
                .KeyProperty(sv => sv.VisitId, "VisitId");
            this.Map(x => x.InsertDate);
            this.Map(x => x.JsonData)
                //.LazyLoad()
                .Not.Nullable();
            this.Map(x => x.StatementedSnapshotFinancePlanId)
                .Nullable();
            
        }
    }
}
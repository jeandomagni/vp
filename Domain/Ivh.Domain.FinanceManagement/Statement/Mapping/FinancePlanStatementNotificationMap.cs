﻿namespace Ivh.Domain.FinanceManagement.Statement.Mapping
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class FinancePlanStatementNotificationMap : ClassMap<FinancePlanStatementNotification>
    {
        public FinancePlanStatementNotificationMap()
        {
            this.Schema("dbo");
            this.Table("FinancePlanStatementNotification");
            this.Id(x => x.FinancePlanStatementNotificationId);
            this.References(x => x.FinancePlanStatement)
                .Column("FinancePlanStatementId")
                .Not.Nullable();
            this.Map(x => x.NotificationText)
                .Not.Nullable();
            this.Map(x => x.InsertDate);
        }
    }
}

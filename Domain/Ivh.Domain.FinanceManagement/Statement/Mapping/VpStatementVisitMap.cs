namespace Ivh.Domain.FinanceManagement.Statement.Mapping
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class VpStatementVisitMap : ClassMap<VpStatementVisit>
    {
        public VpStatementVisitMap()
        {
            this.Schema("dbo");
            this.Table("VpStatementVisit");
            this.CompositeId()
                .KeyReference(sv => sv.VpStatement,"VpStatementId")
                .KeyReference(sv => sv.Visit, c => c.Not.Lazy(), "VisitId");
            this.Map(x => x.DeletedDate);
            this.Map(x => x.InsertDate);
            this.Map(x => x.StatementedSnapshotFinancePlanId)
                .Nullable();
            this.Map(x => x.StatementedSnapshotVisitState)
                .Column("StatementedSnapshotVisitStatusId")
                .CustomType<VisitStateEnum>()
                .Nullable();
            this.Map(x => x.JsonData)
                //.LazyLoad()
                .Not.Nullable();
        }
    }
}
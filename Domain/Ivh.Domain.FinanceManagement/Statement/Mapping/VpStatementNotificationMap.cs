﻿namespace Ivh.Domain.FinanceManagement.Statement.Mapping
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class VpStatementNotificationMap : ClassMap<VpStatementNotification>
    {
        public VpStatementNotificationMap()
        {
            this.Schema("dbo");
            this.Table("VpStatementNotification");
            this.Id(x => x.VpStatementNotificationId);
            this.References(x => x.VpStatement)
                .Column("VpStatementId")
                .Not.Nullable();
            this.Map(x => x.NotificationText)
                .Not.Nullable();
            this.Map(x => x.InsertDate);
        }
    }
}

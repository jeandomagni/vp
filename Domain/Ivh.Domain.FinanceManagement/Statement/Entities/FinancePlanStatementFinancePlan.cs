﻿namespace Ivh.Domain.FinanceManagement.Statement.Entities
{
    using System;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;
    using FinancePlan.Entities;
    using Interfaces;
    using Newtonsoft.Json;

    public class FinancePlanStatementFinancePlan
    {
        public FinancePlanStatementFinancePlan()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int FinancePlanStatementFinancePlanId { get; set; }
        public virtual FinancePlan FinancePlan { get; set; }
        public virtual DateTime? DeletedDate { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual FinancePlanStatement FinancePlanStatement { get; set; }


        #region Snapshot object
        private FinancePlanStatementFinancePlanSnapshotVp2 _vp2 = null;
        private FinancePlanStatementFinancePlanSnapshotVp3V1 _vp3V1 = null;

        public virtual IFinancePlanStatementFinancePlanSnapshot GetSnapshotObject()
        {
            if (this.GetSnapshotObjectInternal() != null)
            {
                return this.GetSnapshotObjectInternal();
            }

            if (this._jsonData == null)
            {
                this._jsonData = "{}";
            }
            switch (this.FinancePlanStatement.StatementVersion)
            {
                case VpStatementVersionEnum.Unknown:
                    throw new Exception("A statement version needs to be assigned before using this method");
                case VpStatementVersionEnum.Vp2:
                    this._vp2 = JsonConvert.DeserializeObject<FinancePlanStatementFinancePlanSnapshotVp2>(this.JsonData);
                    return this._vp2;
                case VpStatementVersionEnum.Vp3V1:
                    this._vp3V1 = JsonConvert.DeserializeObject<FinancePlanStatementFinancePlanSnapshotVp3V1>(this.JsonData);
                    return this._vp3V1;
            }

            return null;
        }

        protected virtual IFinancePlanStatementFinancePlanSnapshot GetSnapshotObjectInternal()
        {
            return (IFinancePlanStatementFinancePlanSnapshot)this._vp3V1 ?? (IFinancePlanStatementFinancePlanSnapshot)this._vp2 ?? null;
        }
        public virtual void CommitSnapshotObject()
        {
            this._jsonData = this.GetSnapshotObjectInternal().EntityToJsonSafe();
        }
        private string _jsonData;
        protected internal virtual string JsonData
        {
            get => this._jsonData;
            set => this._jsonData = value;
        }
        #endregion
    }
}
namespace Ivh.Domain.FinanceManagement.Statement.Entities
{
    using System;

    public class StatementPeriodResult
    {
        public virtual int VpStatementId { get; set; }
        public virtual int VpGuarantorId { get; set; }
        public virtual DateTime PeriodStartDate { get; set; }
        public virtual DateTime PeriodEndDate { get; set; }
        public virtual DateTime StatementDate { get; set; }
        public virtual DateTime PaymentDueDate { get; set; }
        public virtual StatementPeriodResult NextStatementPeriodResult { get; set; }
    }
}
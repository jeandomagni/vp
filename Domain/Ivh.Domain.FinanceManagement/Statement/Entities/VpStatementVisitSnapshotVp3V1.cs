﻿namespace Ivh.Domain.FinanceManagement.Statement.Entities
{
    using System;
    using Interfaces;

    public class VpStatementVisitSnapshotVp3V1 : IVpStatementVisitSnapshot
    {
        #region SnapShot Values
        public virtual decimal StatementedSnapshotVisitBalance { get; set; }
        public virtual decimal? StatementedSnapshotCreditBalance { get; set; }
        public virtual DateTime? StatementedSnapshotVisitDate { get; set; }
        public virtual string StatementedSnapshotDescription { get; set; }
        public virtual string StatementedSnapshotPatientName { get; set; }
        public virtual int? StatementedSnapshotAgingCount { get; set; }
        #endregion
    }
}
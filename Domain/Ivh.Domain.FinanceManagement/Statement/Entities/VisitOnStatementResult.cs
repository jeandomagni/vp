namespace Ivh.Domain.FinanceManagement.Statement.Entities
{
    using System;

    public class VisitOnStatementResult
    {
        public VisitOnStatementResult()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int VisitId { get; set; }
        public virtual int VpStatementId { get; set; }
        public virtual DateTime InsertDate { get; set; }
    }
}
﻿namespace Ivh.Domain.FinanceManagement.Statement.Entities
{
    public class PendingUncollectableStatementsResult
    {
        public virtual int VpGuarantorId { get; set; }
        public virtual bool? UncollectableEmailSent { get; set; }
        public virtual bool? PastDueEmailSent { get; set; }
        public virtual int? VisitId { get; set; }
        public virtual int? AgingTier { get; set; }
        public virtual int? FinancePlanId { get; set; }
        public virtual int? CurrentBucket { get; set; }
    }
}
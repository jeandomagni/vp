﻿
namespace Ivh.Domain.FinanceManagement.Statement.Entities
{
    using HospitalData.Outbound.Enums;
    using System;

    public class VpStatementDocument
    {
        public VpStatementDocument()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        public virtual int VpStatementDocumentId { get; set; }
        public virtual int VpStatementId { get; set; }
        public virtual VpOutboundFileTypeEnum VpOutboundFileType { get; set; }
        public virtual Guid FileStorageExternalKey { get; set; }
        public virtual string FileName { get; set; }
        public virtual string MimeType { get; set; }
        public virtual DateTime InsertDate { get; set; }
    }
}

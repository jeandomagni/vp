﻿namespace Ivh.Domain.FinanceManagement.Statement.Entities
{
    using Interfaces;

    public class VpStatementSnapshotVp3V1 : IVpStatementSnapshot
    {
        #region SnapShot Values 

        public virtual decimal StatementedSnapshotTotalBalance { get; set; }
        public virtual decimal? StatementedSnapshotVisitCreditBalanceSum { get; set; }
        public virtual decimal? StatementedSnapshotVisitBalanceSum { get; set; }

        #endregion

        #region SnapShot derived values 

        public virtual bool HasInsuranceAdjustmentDetail => false;

        #endregion

        public bool HasStatementBalance => this.StatementedSnapshotTotalBalance > 0;
        public decimal StatementBalance => this.StatementedSnapshotTotalBalance;
    }
}
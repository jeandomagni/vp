﻿namespace Ivh.Domain.FinanceManagement.Statement.Entities
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Interfaces;
    using Newtonsoft.Json;

    public class FinancePlanStatement : BaseStatement
    {
        private IList<FinancePlanStatementNotification> _financePlanStatementNotifications;

        public FinancePlanStatement() : base()
        {
            this.InsertDate = DateTime.UtcNow;
            this._financePlanStatementNotifications = new List<FinancePlanStatementNotification>();
        }

        public virtual IList<FinancePlanStatementNotification> FinancePlanStatementNotifications
        {
            get { return this._financePlanStatementNotifications; }
            set { this._financePlanStatementNotifications = value; }
        }

        public virtual int FinancePlanStatementId { get; set; }
        public virtual int VpStatementId { get; set; }
        public virtual int VpGuarantorId { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual DateTime? PriorStatementDate { get; set; }

        public virtual VpStatementVersionEnum StatementVersion { get; set; }

        public virtual IList<FinancePlanStatementFinancePlan> FinancePlanStatementFinancePlans { get; set; } = new List<FinancePlanStatementFinancePlan>();
        public virtual IList<FinancePlanStatementVisit> FinancePlanStatementVisits { get; set; } = new List<FinancePlanStatementVisit>();

        public virtual decimal DisplayStatementBalance
        {
            get
            {
                if (this.StatementVersion == VpStatementVersionEnum.Vp2)
                {
                    return ((FinancePlanStatementSnapshotVp2)this.GetSnapshotObject()).StatementedSnapshotFinancePlanBalanceSum ?? 0m;
                }
                return ((FinancePlanStatementSnapshotVp3V1)this.GetSnapshotObject()).StatementedSnapshotFinancePlanBalanceSum ?? 0m;
            }
        }

        #region Snapshot object
        private FinancePlanStatementSnapshotVp2 _vp2 = null;
        private FinancePlanStatementSnapshotVp3V1 _vp3V1 = null;

        public virtual IFinancePlanStatementSnapshot GetSnapshotObject()
        {
            if (this.GetSnapshotObjectInternal() != null)
            {
                return this.GetSnapshotObjectInternal();
            }

            if (this._jsonData == null)
            {
                this._jsonData = "{}";
            }
            switch (this.StatementVersion)
            {
                case VpStatementVersionEnum.Unknown:
                    throw new Exception("A statement version needs to be assigned before using this method");
                case VpStatementVersionEnum.Vp2:
                    this._vp2 = JsonConvert.DeserializeObject<FinancePlanStatementSnapshotVp2>(this.JsonData);
                    return this._vp2;
                case VpStatementVersionEnum.Vp3V1:
                    this._vp3V1 = JsonConvert.DeserializeObject<FinancePlanStatementSnapshotVp3V1>(this.JsonData);
                    return this._vp3V1;
            }

            return null;
        }

        protected virtual IFinancePlanStatementSnapshot GetSnapshotObjectInternal()
        {
            return (IFinancePlanStatementSnapshot)this._vp3V1 ?? (IFinancePlanStatementSnapshot)this._vp2 ?? null;
        }
        public virtual void CommitSnapshotObject()
        {
            this._jsonData = JsonConvert.SerializeObject(this.GetSnapshotObjectInternal());
            foreach (FinancePlanStatementVisit visit in this.FinancePlanStatementVisits)
            {
                visit.CommitSnapshotObject();
            }
            foreach (FinancePlanStatementFinancePlan financePlan in this.FinancePlanStatementFinancePlans)
            {
                financePlan.CommitSnapshotObject();
            }
        }
        private string _jsonData;
        protected internal virtual string JsonData
        {
            get => this._jsonData;
            set => this._jsonData = value;
        }
        #endregion
    }
}
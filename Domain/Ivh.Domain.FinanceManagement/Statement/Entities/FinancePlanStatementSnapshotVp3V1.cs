﻿namespace Ivh.Domain.FinanceManagement.Statement.Entities
{
    using Interfaces;

    public class FinancePlanStatementSnapshotVp3V1 : IFinancePlanStatementSnapshot
    {
        #region SnapShot Values
        public virtual decimal? StatementedSnapshotFinancePlanBalanceSum { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanInterestChargedSum { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanInterestChargedForStatementYearSum { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanFeesChargedForStatementYearSum { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanPaymentDueSum { get; set; }
        #endregion
    }
}
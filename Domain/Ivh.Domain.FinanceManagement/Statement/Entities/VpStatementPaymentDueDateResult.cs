namespace Ivh.Domain.FinanceManagement.Statement.Entities
{
    using System;

    public class VpStatementPaymentDueDateResult
    {
        public virtual int VpStatementId { get; set; }
        public virtual DateTime PaymentDueDate { get; set; }
        public virtual int VpGuarantorId { get; set; }
        public virtual bool BalanceDueReminderMessageSent { get; set; }
    }
}
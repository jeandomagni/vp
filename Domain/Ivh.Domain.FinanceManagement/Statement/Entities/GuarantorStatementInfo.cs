﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.FinanceManagement.Statement.Entities
{
    using Common.VisitPay.Enums;

    public class GuarantorStatementInfo
    {
        public int VpGuarantorId { get; set; }
        public int? VpStatementId { get; set; }
        public ChangeEventStatusEnum? VpStatementProcessingStatus { get; set; }
        public DateTime AsOfDate { get; set; }
    }
}

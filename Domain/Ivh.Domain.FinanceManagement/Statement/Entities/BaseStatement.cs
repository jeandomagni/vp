﻿namespace Ivh.Domain.FinanceManagement.Statement.Entities
{
    using System;
    using Common.VisitPay.Enums;
    using System.Collections.Generic;

    public abstract class BaseStatement
    {
        public virtual DateTime StatementDate { get; set; }

        public virtual DateTime PeriodStartDate { get; set; }

        public virtual DateTime PeriodEndDate { get; set; }

        public virtual DateTime PaymentDueDate { get; set; }

        public virtual DateTime OriginalPaymentDueDate { get; set; }

        public virtual ChangeEventStatusEnum ProcessingStatus { get; set; }

        public virtual int DaysInBillingCycle 
        {
            get { return this.PeriodEndDate.Subtract(this.PeriodStartDate).Days; }
        }

        public virtual int StatementYear
        {
            get { return this.StatementDate.Year; }
        }

        public virtual bool IsGracePeriod => this.IsGracePeriodWithDate(DateTime.UtcNow);

        public virtual bool IsGracePeriodWithDate(DateTime dateToProcess)
        {
            return DateTime.Compare(this.PeriodStartDate, dateToProcess) <= 0 &&
                       DateTime.Compare(this.PaymentDueDate, dateToProcess.Date) >= 0;
        }
    }
}
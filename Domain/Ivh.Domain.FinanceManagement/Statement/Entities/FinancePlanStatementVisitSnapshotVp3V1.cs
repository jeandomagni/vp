﻿namespace Ivh.Domain.FinanceManagement.Statement.Entities
{
    using Interfaces;

    public class FinancePlanStatementVisitSnapshotVp3V1 : IFinancePlanStatementVisitSnapshot
    {
        #region SnapShot Values
        public virtual decimal? StatementedSnapshotPreviousBalance { get; set; }
        public virtual decimal? StatementedSnapshotInterestCharged { get; set; }
        public virtual decimal? StatementedSnapshotInterestPaid { get; set; }
        public virtual decimal? StatementedSnapshotPrincipalPaid { get; set; }
        public virtual decimal? StatementedSnapshotOtherBalanceCharges { get; set; }
        public virtual decimal? StatementedSnapshotOriginalBalance { get; set; }
        public virtual decimal? StatementedSnapshotTotalBalance { get; set; }
        #endregion
    }
}
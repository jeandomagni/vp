﻿namespace Ivh.Domain.FinanceManagement.Statement.Entities
{
    using System;
    using Common.VisitPay.Enums;
    using Interfaces;

    public class VpStatementForProcessPaymentResult : IVpStatementPostPaymentProcess
    {
        public virtual int VpStatementId { get; set; }
        public virtual int VpGuarantorId { get; set; }
        public virtual ChangeEventStatusEnum ProcessingStatus { get; set; }
        public virtual DateTime PaymentDueDate { get; set; }
    }
}

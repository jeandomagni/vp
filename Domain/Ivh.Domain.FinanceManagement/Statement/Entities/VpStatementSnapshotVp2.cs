﻿namespace Ivh.Domain.FinanceManagement.Statement.Entities
{
    using Interfaces;

    public class VpStatementSnapshotVp2 : IVpStatementSnapshot
    {
        #region SnapShot Values 
        public virtual bool IsSoftStatement { get; set; }
        public virtual decimal StatementedSnapshotFinancePlansBalance { get; set; }
        public virtual decimal StatementedSnapshotTotalBalance { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanBalanceSum { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanInterestChargedSum { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanInterestDueSum { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanPrincipalDueSum { get; set; }
        public virtual decimal? StatementedSnapshotMinimumAmountDue { get; set; }
        public virtual decimal? StatementedSnapshotNewVisitBalanceSum { get; set; }
        public virtual decimal? StatementedSnapshotPastDueFinancePlanBalanceSum { get; set; }
        public virtual decimal? StatementedSnapshotPastDueVisitBalanceSum { get; set; }
        public virtual decimal? StatementedSnapshotPaymentGatewayErrorBalanceSum { get; set; }
        public virtual decimal? StatementedSnapshotPaymentsAppliedSum { get; set; }
        public virtual decimal? StatementedSnapshotPriorTotalBalance { get; set; }
        public virtual decimal? StatementedSnapshotTotalInterestChargedYTD { get; set; }
        public virtual decimal? StatementedSnapshotVisitBalanceSum { get; set; }
        public virtual decimal? StatementedSnapshotVisitCreditBalanceSum { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanPaymentDueSum { get; set; }
        public virtual decimal? StatementedSnapshotVisitHsTransactionsChargeSum { get; set; }
        public virtual decimal? StatementedSnapshotVisitHsTransactionsAdjustmentSum { get; set; }
        public virtual decimal? StatementedSnapshotVisitHsTransactionsAdjustmentsExcludingInsuranceSum { get; set; }
        public virtual decimal? StatementedSnapshotVisitHsTransactionsAdjustmentsInsuranceSum { get; set; }
        public virtual decimal? StatementedSnapshotVisitHsTransactionsPaymentSum { get; set; }
        public virtual decimal? StatementedSnapshotVisitVpPaymentSum { get; set; }
        public virtual decimal? StatementedSnapshotVisitVpDiscountSum { get; set; }
        public virtual decimal? StatementedSnapshotVisitVpFeeSum { get; set; }
        public virtual decimal? StatementedSnapshotVisitVpInterestAssessedSum { get; set; }
        public virtual decimal? StatementedSnapshotSuspendedVisitBalanceSum { get; set; }
        public virtual decimal? StatementedSnapshotClosedVisitBalanceSum { get; set; }
        public virtual decimal? StatementedSnapshotSuspendedClosedVisitBalanceSum { get; set; }
        public virtual decimal? StatementedSnapshotVpFeesAndInterest { get; set; }
        public virtual decimal? StatementedSnapshotVpPaymentsAndDiscounts { get; set; }
        public virtual decimal? StatementedSnapshotHsAdjustmentsAndHsCharges { get; set; }
        public virtual decimal? StatementedSnapshotPreviousVisitBalanceSum { get; set; }
        public virtual decimal? StatementedSnapshotReassessmentAdjustmentTotalSum { get; set; }

        #endregion

        #region SnapShot derived values 
        public virtual bool HasInsuranceAdjustmentDetail => this.StatementedSnapshotVisitHsTransactionsAdjustmentsExcludingInsuranceSum.HasValue &&
                                                            this.StatementedSnapshotVisitHsTransactionsAdjustmentsInsuranceSum.HasValue;
        #endregion

        public bool HasStatementBalance => this.StatementedSnapshotTotalBalance > 0;
        public decimal StatementBalance => this.StatementedSnapshotTotalBalance;
    }
}
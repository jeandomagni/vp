﻿namespace Ivh.Domain.FinanceManagement.Statement.Entities
{
    using System;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Interfaces;
    using Newtonsoft.Json;

    public class FinancePlanStatementVisit
    {
        public FinancePlanStatementVisit()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        //composite key
        public virtual FinancePlanStatement FinancePlanStatement { get; set; }
        public virtual int VisitId { get; set; }

        public virtual int? StatementedSnapshotFinancePlanId { get; set; }
        public virtual DateTime InsertDate { get; set; }
        
        #region Snapshot object
        private FinancePlanStatementVisitSnapshotVp2 _vp2 = null;
        private FinancePlanStatementVisitSnapshotVp3V1 _vp3V1 = null;

        public virtual IFinancePlanStatementVisitSnapshot GetSnapshotObject()
        {
            if (this.GetSnapshotObjectInternal() != null)
            {
                return this.GetSnapshotObjectInternal();
            }

            if (this._jsonData == null)
            {
                this._jsonData = "{}";
            }
            switch (this.FinancePlanStatement.StatementVersion)
            {
                case VpStatementVersionEnum.Unknown:
                    throw new Exception("A statement version needs to be assigned before using this method");
                case VpStatementVersionEnum.Vp2:
                    this._vp2 = JsonConvert.DeserializeObject<FinancePlanStatementVisitSnapshotVp2>(this.JsonData);
                    return this._vp2;
                case VpStatementVersionEnum.Vp3V1:
                    this._vp3V1 = JsonConvert.DeserializeObject<FinancePlanStatementVisitSnapshotVp3V1>(this.JsonData);
                    return this._vp3V1;
            }

            return null;
        }

        protected virtual IFinancePlanStatementVisitSnapshot GetSnapshotObjectInternal()
        {
            return (IFinancePlanStatementVisitSnapshot)this._vp3V1 ?? (IFinancePlanStatementVisitSnapshot)this._vp2 ?? null;
        }
        
        public virtual void CommitSnapshotObject()
        {
            this._jsonData = JsonConvert.SerializeObject(this.GetSnapshotObjectInternal());
        }
        private string _jsonData;
        protected internal virtual string JsonData
        {
            get => this._jsonData;
            set => this._jsonData = value;
        }
        #endregion

        
        #region Equality Members
        public virtual bool Equals(FinancePlanStatementVisit other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return this.FinancePlanStatement.FinancePlanStatementId == other.FinancePlanStatement.FinancePlanStatementId && this.VisitId == other.VisitId;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (this.FinancePlanStatement.FinancePlanStatementId * 397) ^ this.VisitId;
            }
        }

        public static bool operator ==(FinancePlanStatementVisit left, FinancePlanStatementVisit right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(FinancePlanStatementVisit left, FinancePlanStatementVisit right)
        {
            return !Equals(left, right);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return this.Equals((FinancePlanStatementVisit) obj);
        }
        #endregion Equality Members
    }
}
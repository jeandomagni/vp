﻿namespace Ivh.Domain.FinanceManagement.Statement.Entities
{
    using Interfaces;

    public class FinancePlanStatementSnapshotVp2 : IFinancePlanStatementSnapshot
    {
        #region SnapShot Values
        public virtual decimal StatementedSnapshotFinancePlansBalance { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanBalanceSum { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanInterestChargedSum { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanPaymentDueSum { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanInterestDueSum { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanPrincipalDueSum { get; set; }
        public virtual decimal? StatementedSnapshotMinimumAmountDue { get; set; }
        public virtual decimal? StatementedSnapshotPastDueFinancePlanBalanceSum { get; set; }
        #endregion
    }
}
namespace Ivh.Domain.FinanceManagement.Statement.Entities
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class VpStatementQueueStatusResult
    {
        public virtual int VpStatementId { get; set; }
        public virtual int VpGuarantorId { get; set; }
        public virtual ChangeEventStatusEnum ProcessingStatus { get; set; }
    }
}
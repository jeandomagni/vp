﻿namespace Ivh.Domain.FinanceManagement.Statement.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Interfaces;
    using Newtonsoft.Json;

    public class VpStatement : BaseStatement, IVpStatementPostPaymentProcess
    {
        private IList<VpStatementVisit> _vpStatementVisits;
        private IList<VpStatementNotification> _vpStatementNotifications;

        public VpStatement() : base()
        {
            this.InsertDate = DateTime.UtcNow;
            this._vpStatementVisits = new List<VpStatementVisit>();
            this._vpStatementNotifications = new List<VpStatementNotification>();
        }

        public virtual IList<VpStatementNotification> VpStatementNotifications
        {
            get { return this._vpStatementNotifications; }
            set { this._vpStatementNotifications = value; }
        }

        public virtual int VpStatementId { get; set; }
        public virtual int VpGuarantorId { get; set; }
        public virtual VpStatementTypeEnum VpStatementType { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual bool CreatePaperStatement { get; set; }
        public virtual DateTime? PaperStatementSentDate { get; set; }
        public virtual bool BalanceDueReminderMessageSent { get; set; }

        public virtual decimal DisplayStatementBalance
        {
            get
            {
                if (this.StatementVersion == VpStatementVersionEnum.Vp2)
                {
                    return ((VpStatementSnapshotVp2)this.GetSnapshotObject()).StatementedSnapshotTotalBalance;
                }
                return ((VpStatementSnapshotVp3V1)this.GetSnapshotObject()).StatementedSnapshotVisitBalanceSum ?? 0m;
            }
        }

        public virtual IList<VpStatementVisit> VpStatementVisits
        {
            get { return this._vpStatementVisits; }
            set { this._vpStatementVisits = value; }
        }

        public virtual IList<VpStatementVisit> VpStatementVisitsNotOnFinancePlan
        {
            get
            {
                return this._vpStatementVisits.Where(x => x.StatementedSnapshotFinancePlanId == null || x.StatementedSnapshotFinancePlanId == 0).ToList();
            }
        }

        public virtual IList<VpStatementVisit> ActiveVpStatementVisitsNotOnFinancePlan
        {
            get
            {
                return this.ActiveVpStatementVisits.Where(x => x.StatementedSnapshotFinancePlanId == null || x.StatementedSnapshotFinancePlanId == 0).ToList();
            }
        }

        public virtual IList<VpStatementVisit> ActiveVpStatementVisits
        {
            get { return this._vpStatementVisits.Where(x => x.StatementedSnapshotVisitState == VisitStateEnum.Active && x.DeletedDate == null).ToList(); }
        }
        public virtual bool? UncollectableEmailSent { get; set; }
        public virtual bool? PastDueEmailSent { get; set; }

        public virtual DateTime? PriorStatementDate { get; set; }

        public virtual bool IsArchived { get; set; }
        public virtual string ArchivedFileMimeType { get; set; }

        //Todo: Start: This should be deleted after migration.
        public virtual int? LegacyStatemenID { get; set; }
        //Todo: End: This should be deleted after migration.

        public virtual VpStatementVersionEnum StatementVersion { get; set; }

        public virtual bool HasStatementBalance => this.GetSnapshotObject().HasStatementBalance;

        public virtual decimal StatementBalance => this.GetSnapshotObject().StatementBalance;


        #region Snapshot object
        private VpStatementSnapshotVp2 _vp2 = null;
        private VpStatementSnapshotVp3V1 _vp3V1 = null;

        public virtual IVpStatementSnapshot GetSnapshotObject()
        {
            if (this.GetSnapshotObjectInternal() != null)
            {
                return this.GetSnapshotObjectInternal();
            }

            if (this._jsonData == null)
            {
                this._jsonData = "{}";
            }
            switch (this.StatementVersion)
            {
                case VpStatementVersionEnum.Unknown:
                    throw new Exception("A statement version needs to be assigned before using this method");
                case VpStatementVersionEnum.Vp2:
                    this._vp2 = JsonConvert.DeserializeObject<VpStatementSnapshotVp2>(this.JsonData);
                    return this._vp2;
                case VpStatementVersionEnum.Vp3V1:
                    this._vp3V1 = JsonConvert.DeserializeObject<VpStatementSnapshotVp3V1>(this.JsonData);
                    return this._vp3V1;
            }

            return null;
        }

        public virtual void CommitSnapshotObject()
        {
            this._jsonData = JsonConvert.SerializeObject(this.GetSnapshotObjectInternal());
            foreach (VpStatementVisit vpStatementVisit in this.VpStatementVisits)
            {
                vpStatementVisit.CommitSnapshotObject();
            }
        }

        protected virtual IVpStatementSnapshot GetSnapshotObjectInternal()
        {
            return (IVpStatementSnapshot)this._vp3V1 ?? (IVpStatementSnapshot)this._vp2 ?? null;
        }
        private string _jsonData;
        protected internal virtual string JsonData
        {
            get => this._jsonData;
            set => this._jsonData = value;
        }

        #endregion
    }
}
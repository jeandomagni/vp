﻿namespace Ivh.Domain.FinanceManagement.Statement.Entities
{
    using System;

    public class VpStatementNotification
    {
        public virtual int VpStatementNotificationId { get; set; }
        public virtual VpStatement VpStatement { get; set; }
        public virtual string NotificationText { get; set; }
        public virtual DateTime InsertDate { get; set; }
    }
}

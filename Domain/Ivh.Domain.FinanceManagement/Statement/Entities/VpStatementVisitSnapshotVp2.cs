﻿namespace Ivh.Domain.FinanceManagement.Statement.Entities
{
    using Interfaces;

    public class VpStatementVisitSnapshotVp2 : IVpStatementVisitSnapshot
    {
        #region SnapShot Values

        public virtual decimal StatementedSnapshotVisitBalance { get; set; }
        public virtual int? StatementedSnapshotAgingCount { get; set; }
        public virtual decimal? StatementedSnapshotCreditBalance { get; set; }
        public virtual decimal? StatementedSnapshotHsTransactionsCharges { get; set; }
        public virtual decimal? StatementedSnapshotHsTransactionsAdjustments { get; set; }
        public virtual decimal? StatementedSnapshotHsTransactionsAdjustmentsExcludingInsurance { get; set; }
        public virtual decimal? StatementedSnapshotHsTransactionsAdjustmentsInsurance { get; set; }
        public virtual decimal? StatementedSnapshotHsTransactionsPayments { get; set; }
        public virtual decimal? StatementedSnapshotVpPayments { get; set; }
        public virtual decimal? StatementedSnapshotVpDiscounts { get; set; }
        public virtual decimal? StatementedSnapshotVpFees { get; set; }
        public virtual decimal? StatementedSnapshotVpInterestAssessed { get; set; }
        public virtual decimal? StatementedSnapshotSuspendedBalance { get; set; }
        public virtual decimal? StatementedSnapshotClosedBalance { get; set; }
        public virtual decimal? StatementedSnapshotPreviousVisitBalance { get; set; }
        public virtual decimal? StatementedSnapshotNewVisitBalance { get; set; }
        public virtual string StatementedSnapshotDisplayStatus { get; set; }
        public virtual decimal StatementedSnapshotReassessmentAdjustmentTotal { get; set; }

        #endregion
    }
}
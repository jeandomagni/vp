﻿namespace Ivh.Domain.FinanceManagement.Statement.Entities
{
    using System;

    public class FinancePlanStatementNotification
    {
        public virtual int FinancePlanStatementNotificationId { get; set; }
        public virtual FinancePlanStatement FinancePlanStatement { get; set; }
        public virtual string NotificationText { get; set; }
        public virtual DateTime InsertDate { get; set; }
    }
}

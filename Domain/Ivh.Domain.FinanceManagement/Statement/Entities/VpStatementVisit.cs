﻿namespace Ivh.Domain.FinanceManagement.Statement.Entities
{
    using System;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Interfaces;
    using Newtonsoft.Json;
    using Visit.Entities;

    public class VpStatementVisit : IEquatable<VpStatementVisit>
    {
        public VpStatementVisit()
        {
            this.InsertDate = DateTime.UtcNow;
            this.VpStatement = new VpStatement {VpStatementType = VpStatementTypeEnum.Standard};
            this.Visit = new Visit();
        }

        public virtual Visit Visit { get; set; }

        public virtual VpStatement VpStatement { get; set; }

        public virtual DateTime? DeletedDate { get; set; }

        public virtual DateTime InsertDate { get; set; }

        public virtual decimal StatementedCurrentVisitBalance { get; set; }
        public virtual int? StatementedSnapshotFinancePlanId { get; set; }
        public virtual VisitStateEnum StatementedSnapshotVisitState { get; set; }

        #region Equality Members

        public virtual bool Equals(VpStatementVisit other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return this.VpStatement.VpStatementId == other.VpStatement.VpStatementId && this.Visit != null && other.Visit != null && this.Visit.VisitId == other.Visit.VisitId;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (this.VpStatement.VpStatementId * 397) ^ this.Visit.VisitId;
            }
        }

        public static bool operator ==(VpStatementVisit left, VpStatementVisit right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(VpStatementVisit left, VpStatementVisit right)
        {
            return !Equals(left, right);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((VpStatementVisit) obj);
        }

        #endregion Equality Members
        
        #region Snapshot object
        private VpStatementVisitSnapshotVp2 _vp2 = null;
        private VpStatementVisitSnapshotVp3V1 _vp3V1 = null;

        public virtual IVpStatementVisitSnapshot GetSnapshotObject()
        {
            if (this.GetSnapshotObjectInternal() != null)
            {
                return this.GetSnapshotObjectInternal();
            }

            if (this._jsonData == null)
            {
                this._jsonData = "{}";
            }
            switch (this.VpStatement.StatementVersion)
            {
                case VpStatementVersionEnum.Unknown:
                    throw new Exception("A statement version needs to be assigned before using this method");
                case VpStatementVersionEnum.Vp2:
                    this._vp2 = JsonConvert.DeserializeObject<VpStatementVisitSnapshotVp2>(this.JsonData);
                    return this._vp2;
                case VpStatementVersionEnum.Vp3V1:
                    this._vp3V1 = JsonConvert.DeserializeObject<VpStatementVisitSnapshotVp3V1>(this.JsonData);
                    return this._vp3V1;
            }

            return null;
        }

        protected virtual IVpStatementVisitSnapshot GetSnapshotObjectInternal()
        {
            return (IVpStatementVisitSnapshot)this._vp3V1 ?? (IVpStatementVisitSnapshot)this._vp2 ?? null;
        }

        public virtual void CommitSnapshotObject()
        {
            this._jsonData = JsonConvert.SerializeObject(this.GetSnapshotObjectInternal());
        }

        private string _jsonData;
        protected internal virtual string JsonData
        {
            get => this._jsonData;
            set => this._jsonData = value;
        }

        #endregion

    }
}
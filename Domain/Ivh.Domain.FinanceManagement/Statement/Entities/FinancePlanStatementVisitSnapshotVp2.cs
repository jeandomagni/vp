﻿namespace Ivh.Domain.FinanceManagement.Statement.Entities
{
    using Interfaces;

    public class FinancePlanStatementVisitSnapshotVp2 : IFinancePlanStatementVisitSnapshot
    {
        #region SnapShot Values
        public virtual decimal? StatementedSnapshotVpFees { get; set; }
        public virtual decimal? StatementedSnapshotVpInterestAssessed { get; set; }
        #endregion
    }
}
﻿namespace Ivh.Domain.FinanceManagement.Statement.Entities
{
    using System;
    using FinancePlan.Entities;
    using Interfaces;

    public class FinancePlanStatementFinancePlanSnapshotVp3V1 : IFinancePlanStatementFinancePlanSnapshot
    {
        #region SnapShot Values
        public virtual decimal? StatementedSnapshotFinancePlanOriginalBalance { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanBalance { get; set; }
        public virtual decimal? StatementedSnapshotPriorFinancePlanBalance { get; set; }
        public virtual DateTime? StatementedSnapshotFinancePlanEffectiveDate { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanInterestCharged { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanInterestPaid { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanInterestRate { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanMonthlyPaymentAmount { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanPastDueAmount { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanPaymentDue { get; set; } // AmountDue
        public virtual decimal? StatementedSnapshotFinancePlanPrincipalPaid { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanOtherBalanceCharges { get; set; }
        public virtual FinancePlanStatus StatementedSnapshotFinancePlanStatus { get; set; }

        public virtual bool IsFirstStatement { get; set; }
        #endregion

        #region SnapShot derived values 
        #endregion
    }
}
﻿namespace Ivh.Domain.FinanceManagement.Statement.Entities
{
    using System;
    using FinancePlan.Entities;
    using Interfaces;

    public class FinancePlanStatementFinancePlanSnapshotVp2 : IFinancePlanStatementFinancePlanSnapshot
    {
        
        #region SnapShot Values
        public virtual decimal? SnapShotCurrentBalance { get; set; }
        public virtual decimal? SnapShotDuration { get; set; }
        public virtual decimal? SnapShotPaymentAmount { get; set; }
        public virtual decimal? SnapShotInterestRate { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanBalance { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanAdjustmentsSum { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanAdjustments { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanSuspensions { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanClosures { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanVpPayments { get; set; }
        public virtual DateTime? StatementedSnapshotFinancePlanEffectiveDate { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanInterestCharged { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanInterestDue { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanInterestPaid { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanInterestRate { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanMonthlyPaymentAmount { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanPastDueAmount { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanPaymentDue { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanPrincipalDue { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanPrincipalPaid { get; set; }
        public virtual int? StatementedSnapshotFinancePlanStatusId { get; set; }
        public virtual string StatementedSnapshotFinancePlanStatusDisplayName { get; set; }
        public virtual decimal? StatementedSnapshotPriorFinancePlanBalance { get; set; }
        public virtual decimal? StatementedSnapshotReassessmentFinancePlanAdjustmentTotal { get; set; }
        #endregion
        #region SnapShot derived values 
        public virtual decimal FinancePlanPayments {
            get { return this.StatementedSnapshotFinancePlanVpPayments ?? 0m; }
        }
        #endregion
    }
}
﻿namespace Ivh.Domain.FinanceManagement.Statement.Interfaces
{
    public interface IVpStatementVisitSnapshot
    {
        int? StatementedSnapshotAgingCount { get; set; }
        decimal StatementedSnapshotVisitBalance { get; set; }
        decimal? StatementedSnapshotCreditBalance { get; set; }
    }
}
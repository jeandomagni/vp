﻿namespace Ivh.Domain.FinanceManagement.Statement.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Common.VisitPay.Enums;
    using Entities;
    using Guarantor.Entities;

    public interface IFinancePlanStatementService
    {
        FinancePlanStatement GetMostRecentStatement(Guarantor guarantor);
        FinancePlanStatement CreateFinancePlanStatement(Guarantor guarantor, DateTime dateToProcess, FinancePlanStatement previousFinancePlanStatement, bool isImmediateStatement, bool isFirstStatement, VpStatementVersionEnum version);
        void InsertOrUpdate(FinancePlanStatement newFpStatement);
        FinancePlanStatement GetFinancePlanStatement(int guarantorId, int statementId);
        IReadOnlyList<FinancePlanStatement> GetFinancePlanStatements(int guarantorId);
        FinancePlanStatement GetFinancePlanStatementByVpStatementId(int guarantorId, int vpStatementId);
        FinancePlanStatement GetFinancePlanStatementByPeriodStartDate(int guarantorId, DateTime periodStartDate);
        bool IsFirstStatementForFinancePlan(int guarantorId,int financePlanId);
        IList<FinancePlanStatement> GetFinancePlanStatementByVpStatementIds(IList<int> vpStatementIds);
        decimal GetInterestDueForFinancePlanStatement(int vpStatementId, int financePlanId);
        [Obsolete("Temporary code only needed until 7/14/2019 for VP-6889")]
        void FixMissingInterest(int vpStatementId);
    }
}
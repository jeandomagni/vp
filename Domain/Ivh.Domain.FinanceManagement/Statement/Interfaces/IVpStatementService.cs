namespace Ivh.Domain.FinanceManagement.Statement.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;
    using Guarantor.Entities;
    using ValueTypes;

    public interface IVpStatementService : IDomainService
    {
        VpStatement GetStatement(int vpStatementId);
        VpStatement GetStatement(int vpGuarantorId, int vpStatementId);
        IReadOnlyList<VpStatement> GetStatements(int vpGuarantorId);
        IList<VpStatement> GetStatementsForCreatingPaperStatements();
        void InsertOrUpdate(VpStatement statement);
        int GetStatementTotals(int guarantorId);
        VpStatement GetMostRecentStatement(Guarantor guarantor);
        VpStatementForProcessPaymentResult GetMostRecentStatementForProcessPayment(Guarantor guarantor);
        VpStatementForProcessPaymentResult GetVpStatementForProcessPaymentResult(int vpGuarantorId, int vpStatementId);
        IList<VpStatementQueueStatusResult> GetMostRecentStatementQueueStatus(IList<int> vpGuarantorIds);
        bool DoesGuarantorHaveStatement(int vpGuarantorId);
        IList<GuarantorStatementInfo> GetGuarantorStatementInfoToBeChargedWithDate(DateTime dateTime);
        void MarkStatementAsProcessed(int statementId, int vpGuarantorId, string logMessage = null);
        void MarkStatementAsUnprocessed(int statementId, int vpGuarantorId, string logMessage = null);
        void MarkStatementAsQueued(int statementId, int vpGuarantorId, string logMessage = null);
        bool IsAwaitingStatement(VpStatement lastStatement);
        /// <summary>
        /// returns the next payment due date, never a date in the past.
        /// if grace period, statement.PaymentDueDate
        /// if not, calculated next PaymentDueDate 
        /// </summary>
        DateTime GetNextPaymentDate(Guarantor guarantor);
        /// <summary>
        /// returns the payment due date for the next statement.
        /// </summary>
        DateTime GetPaymentDueDateForNextStatement(Guarantor guarantor);
        DateTime GetStatementPeriodStartDate(VpStatement previousStatement, Guarantor guarantor);
        IList<PendingUncollectableStatementsResult> GetAllPendingUncollectableStatements(int maxAgingCountFinancePlans, DateTime statementDueDate);
        IList<int> GuarantorsIdsWhoHaventHadUncollectableEmailForCurrentStatement(IList<int> guarantors);
        IList<int> GuarantorsIdsWhoHaventHadPastDueEmailForCurrentStatement(IList<int> guarantors);
        void MarkPastDueEmailAsSent(VpStatement mostRecentStatement);
        void MarkUncollectableEmailAsSent(VpStatement mostRecentStatement);
        ProcessPaymentDueDayChangedResult ProcessPaymentDueDayChanged(Guarantor guarantor, bool isFinancePlanCreatedChange, int actionVisitPayUserId);
        IList<VpStatementPaymentDueDateResult> GetStatementsWithPaymentDueDateResults(DateTime dueDate);
        byte[] GetStatementContent(int vpGuarantorId, int vpStatementId);
        bool HasStatementBeenCreatedAlready(Guarantor guarantor, DateTime dateToProcess);
        VpStatement CreateStatement(Guarantor guarantor, DateTime dateToProcess, VpStatement previousStatement, bool isImmediateStatement, bool isFirstStatement, VpStatementVersionEnum version);
        void IncrementTheNextStatementDate(Guarantor guarantor, DateTime dateToProcess, VpStatement newStatement, bool isFirstStatement);
    }
}
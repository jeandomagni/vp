﻿namespace Ivh.Domain.FinanceManagement.Statement.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;
    using Guarantor.Entities;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;

    public interface IFinancePlanStatementRepository : IRepository<FinancePlanStatement>
    {
        FinancePlanStatement GetMostRecentStatement(Guarantor guarantor);
        FinancePlanStatement GetFinancePlanStatementByVpStatementId(int guarantorId, int vpStatementId);
        FinancePlanStatement GetFinancePlanStatementByPeriodStartDate(int guarantorId, DateTime periodStartDate);
        IReadOnlyList<FinancePlanStatement> GetFinancePlanStatements(int guarantorId);
        IList<FinancePlanStatement> GetFinancePlanStatementByVpStatementIds(IList<int> vpStatementIds);
        [Obsolete("Temporary code only needed until 7/14/2019 for VP-6889")]
        InterestEventMessageList FixMissingInterest(int vpStatementId);
    }
}
﻿namespace Ivh.Domain.FinanceManagement.Statement.Interfaces
{
    public interface IVpStatementSnapshot
    {
        bool HasStatementBalance { get; }
        decimal StatementBalance { get; }
    }
}
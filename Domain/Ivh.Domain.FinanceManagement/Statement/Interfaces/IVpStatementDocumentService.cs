﻿
namespace Ivh.Domain.FinanceManagement.Statement.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IVpStatementDocumentService : IDomainService
    {
        void InsertOrUpdate(VpStatementDocument statementDocument);
    }
}

﻿namespace Ivh.Domain.FinanceManagement.Statement.Interfaces
{
    using System;
    using Common.VisitPay.Enums;

    public interface IVpStatementPostPaymentProcess
    {
        int VpStatementId { get; set; }
        int VpGuarantorId { get; set; }
        ChangeEventStatusEnum ProcessingStatus { get; set; }
        DateTime PaymentDueDate { get; set; }
    }
}

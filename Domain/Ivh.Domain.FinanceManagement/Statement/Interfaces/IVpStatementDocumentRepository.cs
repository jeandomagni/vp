﻿
namespace Ivh.Domain.FinanceManagement.Statement.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IVpStatementDocumentRepository : IRepository<VpStatementDocument>
    {
    }
}

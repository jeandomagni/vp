﻿namespace Ivh.Domain.FinanceManagement.Statement.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Visit.Entities;
    using Entities;
    using Guarantor.Entities;

    public interface IVpStatementRepository : IRepository<VpStatement>
    {
        IList<int> GetAllPendingStatementGuarantors();
        VpStatement GetVpStatement(int vpGuarantorId, int vpStatementId);
        IList<VpStatement> GetStatementsForCreatingPaperStatements();
        IReadOnlyList<VpStatement> GetVpStatements(int vpGuarantorId);
        int GetVpStatementTotals(int vpGuarantorId);
        IList<VpStatement> GetStatementsAroundDateForGuarantor(int vpGuarantorId, DateTime dateToProcess, int dayBracket);
        VpStatement GetMostRecentStatement(Guarantor guarantor);
        VpStatementForProcessPaymentResult GetMostRecentStatementForProcessPayment(Guarantor guarantor);
        VpStatementForProcessPaymentResult GetVpStatementForProcessPaymentResult(int vpGuarantorId, int vpStatementId);
        bool HasMostRecentStatement(IList<int> vpGuarantorIds);
        IList<VpStatementQueueStatusResult> GetMostRecentStatementQueueStatus(IList<int> vpGuarantorIds);
        IList<VpStatement> GetTopVpStatementByGuarantor(int vpGuarantorId, int topCount);
        IList<GuarantorStatementInfo> GetGuarantorStatementInfoToBeChargedWithDate(DateTime dateTime);
        VpStatement GetVpStatementWithDate(int vpGuarantorId, DateTime date);
        IList<PendingUncollectableStatementsResult> GetAllPendingUncollectableStatements(int maxAgingCountFinancePlans, DateTime statementDueDate);
        IList<int> GuarantorsIdsWhoHaventHadUncollectableEmailForCurrentStatement(IList<int> guarantors);
        IList<int> GuarantorsIdsWhoHaventHadPastDueEmailForCurrentStatement(IList<int> guarantors);
        IList<VisitOnStatementResult> GetVisitOnStatementResultsForVisits(IList<Visit> visitsForNewStatement);
        IList<StatementPeriodResult> GetAllStatementPeriodsForGuarantor(int vpGuarantorId);
        void MarkStatementAsQueued(int statementId);
        void MarkStatementAsProcessed(int statementId);
        void MarkStatementAsUnprocessed(int statementId);
        IList<VpStatementPaymentDueDateResult> GetStatementsWithPaymentDueDateResults(DateTime dueDate);
        byte[] GetStatementContent(int vpGuarantorId, int vpStatementId);
        int GetCurrentStatementIdForDate(DateTime paymentActualPaymentDate, int vpGuarantorId);
        bool IsVisitOnHardStatement(int visitId, DateTime startDate, DateTime endDate);
        bool DoesGuarantorHaveStatement(int vpGuarantorId);
    }
}
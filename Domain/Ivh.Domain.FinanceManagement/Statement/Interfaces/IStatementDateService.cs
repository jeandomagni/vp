﻿namespace Ivh.Domain.FinanceManagement.Statement.Interfaces
{
    using System;
    using Entities;
    using Guarantor.Entities;

    public interface IStatementDateService
    {
        DateTime GetNextPaymentDate(DateTime? originalPaymentDueDate, Guarantor guarantor, DateTime dateToProcess);

        DateTime GetNextPaymentDate(DateTime? originalPaymentDueDate, Guarantor guarantor, DateTime dateToProcess, bool isImmediateStatement, bool isFirstStatement);
        
        /// <summary>
        /// find the nearest payment due date in the future
        /// </summary>
        DateTime GetNextPaymentDate(int overridePaymentDueDay);

        DateTime GetStatementPeriodEndDate(DateTime? originalPaymentDueDate, Guarantor guarantor, DateTime dateToProcess, bool isImmediateStatement = false, bool isFirstStatement = false);

        DateTime GetNextStatementDate(DateTime? originalPaymentDueDate, Guarantor guarantor, DateTime dateToProcess);

        DateTime GetNextStatementDate(DateTime? originalPaymentDueDate, Guarantor guarantor, DateTime dateToProcess, bool isImmediateStatement, bool isFirstStatement);
        
        DateTime GeneratePaymentDueDayUnadjustedForDateToProcess(DateTime? originalPaymentDueDate, Guarantor guarantor, DateTime dateToProcess, bool isImmediateStatement = false, bool isFirstStatement = false, int? overridePaymentDueDay = null);
        
        DateTime GenerateUnadjustedPaymentDateWithForDate(DateTime startingDate, int day);
        
        /// <summary>
        /// returns the next payment due date, never a date in the past.
        /// if grace period, statement.PaymentDueDate
        /// if not, calculated next PaymentDueDate 
        /// </summary>
        DateTime GetNextPaymentDate(Guarantor guarantor, VpStatement lastStatement);
        
        /// <summary>
        /// returns the payment due date for the next statement.
        /// </summary>
        DateTime GetPaymentDueDateForNextStatement(Guarantor guarantor, VpStatement lastStatement);
        
        /// <summary>
        /// this method assumes Guarantor.PaymentDueDay has already changed
        /// </summary>
        DateTime GetNewPaymentDateForPaymentDueDayChange(Guarantor guarantor, VpStatement lastStatement);

         bool UpdateStatementPaymentDueDate(BaseStatement statement, DateTime newPaymentDueDate, bool updateOriginalPaymentDueDate);
    }
}
﻿namespace Ivh.Domain.FinanceManagement.Statement.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Entities;
    using FinancePlan.Interfaces;
    using Guarantor.Entities;
    using Interfaces;

    public class FinancePlanStatementService : DomainService, IFinancePlanStatementService
    {
        private readonly Lazy<IFinancePlanStatementRepository> _financePlanStatementRepository;
        private readonly Lazy<IStatementDateService> _statementDateService;
        private readonly Lazy<IFinancePlanVisitInterestDueRepository> _financePlanVisitInterestDueRepository;

        public FinancePlanStatementService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IFinancePlanStatementRepository> financePlanStatementRepository,
            Lazy<IStatementDateService> statementDateService,
            Lazy<IFinancePlanVisitInterestDueRepository> financePlanVisitInterestDueRepository
        ) : base(serviceCommonService)
        {
            this._financePlanStatementRepository = financePlanStatementRepository;
            this._statementDateService = statementDateService;
            this._financePlanVisitInterestDueRepository = financePlanVisitInterestDueRepository;
        }

        public FinancePlanStatement GetMostRecentStatement(Guarantor guarantor)
        {
            return this._financePlanStatementRepository.Value.GetMostRecentStatement(guarantor);
        }

        public FinancePlanStatement CreateFinancePlanStatement(Guarantor guarantor, DateTime dateToProcess, FinancePlanStatement previousFinancePlanStatement, bool isImmediateStatement, bool isFirstStatement, VpStatementVersionEnum version)
        {
            if (guarantor.NextStatementDate == null)
            {
                throw new NullReferenceException("guarantor.NextStatementDate is null");
            }

            DateTime? originalPaymentDueDate = previousFinancePlanStatement?.OriginalPaymentDueDate;

            FinancePlanStatement statement = new FinancePlanStatement
            {
                VpGuarantorId = guarantor.VpGuarantorId,
                StatementVersion = version,
                StatementDate = this._statementDateService.Value.GetNextStatementDate(originalPaymentDueDate, guarantor, dateToProcess, isImmediateStatement, isFirstStatement),
                PaymentDueDate = this._statementDateService.Value.GetNextPaymentDate(originalPaymentDueDate, guarantor, dateToProcess, isImmediateStatement, isFirstStatement),
                PeriodStartDate = this.GetStatementPeriodStartDate(previousFinancePlanStatement, guarantor),
                PeriodEndDate = this._statementDateService.Value.GetStatementPeriodEndDate(previousFinancePlanStatement?.OriginalPaymentDueDate, guarantor, dateToProcess, isImmediateStatement, isFirstStatement),
                OriginalPaymentDueDate = this._statementDateService.Value.GeneratePaymentDueDayUnadjustedForDateToProcess(previousFinancePlanStatement?.OriginalPaymentDueDate, guarantor, dateToProcess, isImmediateStatement, isFirstStatement)
            };

            //We want the original paymentduedate to be within a month of the last statement.
            while (statement.OriginalPaymentDueDate.AddMonths(1) < statement.PaymentDueDate)
            {
                statement.OriginalPaymentDueDate = statement.OriginalPaymentDueDate.AddMonths(1);
            }

            return statement;
        }

        public DateTime GetStatementPeriodStartDate(FinancePlanStatement previousStatement, Guarantor guarantor)
        {
            if (previousStatement != null)
            {
                return previousStatement.PeriodEndDate.AddMilliseconds(TransactionFilter.NewVisitExclusionWindowMS);
            }

            return guarantor.RegistrationDate.Date;
        }

        public void InsertOrUpdate(FinancePlanStatement newFpStatement)
        {
            this._financePlanStatementRepository.Value.InsertOrUpdate(newFpStatement);
        }

        public FinancePlanStatement GetFinancePlanStatement(int guarantorId, int statementId)
        {
            FinancePlanStatement financePlanStatement = this._financePlanStatementRepository.Value.GetById(statementId);
            if (financePlanStatement.VpGuarantorId == guarantorId)
            {
                return financePlanStatement;
            }

            return null;
        }

        public FinancePlanStatement GetFinancePlanStatementByPeriodStartDate(int guarantorId, DateTime periodStartDate)
        {
            return this._financePlanStatementRepository.Value.GetFinancePlanStatementByPeriodStartDate(guarantorId, periodStartDate);
        }

        public FinancePlanStatement GetFinancePlanStatementByVpStatementId(int guarantorId, int vpStatementId)
        {
            return this._financePlanStatementRepository.Value.GetFinancePlanStatementByVpStatementId(guarantorId, vpStatementId);
        }

        public bool IsFirstStatementForFinancePlan(int vpGuarantorId, int financePlanId)
        {
            bool financePlanStatementFinancePlansExist = this._financePlanStatementRepository.Value.GetFinancePlanStatements(vpGuarantorId)
                .SelectMany(x => x.FinancePlanStatementFinancePlans)
                .Any(x => x.FinancePlan.FinancePlanId == financePlanId);

            return !financePlanStatementFinancePlansExist;
        }

        public IReadOnlyList<FinancePlanStatement> GetFinancePlanStatements(int guarantorId)
        {
            return this._financePlanStatementRepository.Value.GetFinancePlanStatements(guarantorId);
        }

        public IList<FinancePlanStatement> GetFinancePlanStatementByVpStatementIds(IList<int> vpStatementIds)
        {
            return this._financePlanStatementRepository.Value.GetFinancePlanStatementByVpStatementIds(vpStatementIds);
        }

        public decimal GetInterestDueForFinancePlanStatement(int vpStatementId, int financePlanId)
        {
            return this._financePlanVisitInterestDueRepository.Value.GetInterestDueForStatementFinancePlan(vpStatementId, financePlanId);
        }

        [Obsolete("Temporary code only needed until 7/14/2019 for VP-6889")]
        public void FixMissingInterest(int vpStatementId)
        {
            Common.VisitPay.Messages.FinanceManagement.InterestEventMessageList interestEventMessageList = this._financePlanStatementRepository.Value.FixMissingInterest(vpStatementId);
            if (interestEventMessageList != null && interestEventMessageList.Messages != null && interestEventMessageList.Messages.Count > 0)
            {
                this.Bus.Value.PublishMessage(interestEventMessageList).Wait();
            }
        }
    }
}
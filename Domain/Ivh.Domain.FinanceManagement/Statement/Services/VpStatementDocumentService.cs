﻿
namespace Ivh.Domain.FinanceManagement.Statement.Services
{
    using Entities;
    using Interfaces;
    using System;

    public class VpStatementDocumentService : IVpStatementDocumentService
    {
        private readonly Lazy<IVpStatementDocumentRepository> _vpStatementDocumentRepository;

        public VpStatementDocumentService(Lazy<IVpStatementDocumentRepository> vpStatementDocumentRepository)
        {
            this._vpStatementDocumentRepository = vpStatementDocumentRepository;
        }

        public void InsertOrUpdate(VpStatementDocument statementDocument)
        {
            this._vpStatementDocumentRepository.Value.InsertOrUpdate(statementDocument);
        }
    }
}

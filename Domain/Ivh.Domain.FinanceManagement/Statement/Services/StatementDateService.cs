﻿namespace Ivh.Domain.FinanceManagement.Statement.Services
{
    using System;
    using Base.Interfaces;
    using Base.Services;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Entities;
    using Guarantor.Entities;
    using Interfaces;

    public class StatementDateService : DomainService, IStatementDateService
    {
        private int _statementToPaymentDatePeriodBufferDays = -1;
        private int _statementPostingPeriodBuffer = -1;
        
        public StatementDateService(Lazy<IDomainServiceCommonService> serviceCommonService) : base(serviceCommonService)
        {
        }

        private int StatementToPaymentDatePeriodBufferDays
        {
            get
            {
                if (this._statementToPaymentDatePeriodBufferDays == -1)
                {
                    this._statementToPaymentDatePeriodBufferDays = this.Client.Value.GracePeriodLength;
                }
                return this._statementToPaymentDatePeriodBufferDays;
            }
        }

        private int StatementPostingPeriodBuffer
        {
            get
            {
                if (this._statementPostingPeriodBuffer == -1)
                {
                    this._statementPostingPeriodBuffer = this.Client.Value.StatementGenerationDays;
                    if (this._statementPostingPeriodBuffer < 1)
                    {
                        this._statementPostingPeriodBuffer = 1;
                    }
                }
                return this._statementPostingPeriodBuffer;
            }
        }
        
        private DateTime SetDateToBeginningOfDay(DateTime paymentDate)
        {
            return paymentDate.Date;
        }

        private DateTime SetDateToEndOfDay(DateTime paymentDate)
        {
            paymentDate = this.SetDateToBeginningOfDay(paymentDate);
            //Only add 990 ms because of datetimeoffset(2)
            return paymentDate.AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(990);
        }

        public DateTime GeneratePaymentDueDayUnadjustedForDateToProcess(DateTime? originalPaymentDueDate, Guarantor guarantor, DateTime dateToProcess, bool isImmediateStatement = false, bool isFirstStatement = false, int? overridePaymentDueDay = null)
        {
            int paymentDueDay = overridePaymentDueDay ?? guarantor.PaymentDueDay;

            if (isImmediateStatement)
            {
                return this.GetNextPaymentDateForImmediateStatement(paymentDueDay, dateToProcess);
            }

            DateTime guessPaymentDate;

            if (originalPaymentDueDate == null)
            {
                //We have to use dateToProcess when this is running for the first time!
                //This is their first PaymentDate
                DateTime guessFirstPaymentDate = this.GenerateUnadjustedPaymentDateWithForDate(guarantor.RegistrationDate.Date, paymentDueDay);

                DateTime nonImmediateStatementGuess = guessFirstPaymentDate;

                //Make sure we have the buffer time when creating it for the first time.
                //If the regular statement would push to the next payment due date then the ImmediateStatement should stay the same
                if (dateToProcess.Date.AddDays(this.StatementToPaymentDatePeriodBufferDays) >= nonImmediateStatementGuess)
                {
                    //Means that guessFirstPaymentDate needs to be pushed out a month
                    nonImmediateStatementGuess = this.GenerateUnadjustedPaymentDateWithForDate(guarantor.RegistrationDate.Date.AddMonths(1), paymentDueDay);
                    //Which means that the ImmediateStatement guess is probably good as is
                    if (isFirstStatement && dateToProcess.Date.AddDays(this.StatementToPaymentDatePeriodBufferDays) > nonImmediateStatementGuess)
                    {
                        nonImmediateStatementGuess = this.GenerateUnadjustedPaymentDateWithForDate(guarantor.RegistrationDate.Date.AddMonths(2), paymentDueDay);
                    }
                }

                guessPaymentDate = nonImmediateStatementGuess;
            }
            else
            {
                if (dateToProcess > originalPaymentDueDate
                    && dateToProcess.Month == originalPaymentDueDate.Value.Month
                    && dateToProcess.AddDays(this.StatementToPaymentDatePeriodBufferDays).Month == dateToProcess.Month 
                    && dateToProcess.AddDays(this.StatementToPaymentDatePeriodBufferDays).Day <= paymentDueDay)
                {
                    guessPaymentDate = this.GenerateUnadjustedPaymentDateWithForDate(originalPaymentDueDate.Value.Date, paymentDueDay);

                }
                else
                {
                    guessPaymentDate = this.GenerateUnadjustedPaymentDateWithForDate(originalPaymentDueDate.Value.Date.AddMonths(1), paymentDueDay);

                }
            }
            return guessPaymentDate;
        }

        public DateTime GenerateUnadjustedPaymentDateWithForDate(DateTime startingDate, int day)
        {
            if (day <= 0)
            {
                throw new ArgumentOutOfRangeException("day");
            }

            int paymentDay = day;
            DateTime reasonableValue = DateTime.MinValue;
            while (reasonableValue == DateTime.MinValue)
            {
                try
                {
                    reasonableValue = new DateTime(startingDate.Year, startingDate.Month, paymentDay);
                }
                catch (ArgumentOutOfRangeException)
                {
                    if (paymentDay - 1 > 1)
                    {
                        paymentDay--;
                    }
                    else
                    {
                        throw new Exception("Couldnt generate PaymentDate because Guarantor.PaymentDueDay wasnt valid.");
                    }
                }
            }
            return reasonableValue;
        }
        
        public DateTime GetNextStatementDate(DateTime? originalPaymentDueDate, Guarantor guarantor, DateTime dateToProcess)
        {
            return this.GetNextStatementDate(originalPaymentDueDate, guarantor, dateToProcess, false, false);
        }

        public DateTime GetNextStatementDate(DateTime? originalPaymentDueDate, Guarantor guarantor, DateTime dateToProcess, bool isImmediateStatement, bool isFirstStatement)
        {
            // we're bypassing this functionality because we're not honoring the 21 days for ImmediateStatements.
            if (isImmediateStatement)
            {
                return this.GetStatementPeriodEndDate(originalPaymentDueDate, guarantor, dateToProcess, true, isFirstStatement);
            }

            // statement date is {21: StatementToPaymentDatePeriodBuffer} days before the payment date
            DateTime paymentDate = this.GetNextPaymentDate(originalPaymentDueDate, guarantor, dateToProcess, false, isFirstStatement);
            paymentDate = this.SetDateToBeginningOfDay(paymentDate);

            int numberOfDaysToSubtractFromPaymentDate = -1 * this.StatementToPaymentDatePeriodBufferDays;
            DateTime nextStatementDate = paymentDate.Date.AddDays(numberOfDaysToSubtractFromPaymentDate);

            return nextStatementDate.Date;
        }
        
        public DateTime GetNextPaymentDate(DateTime? originalPaymentDueDate, Guarantor guarantor, DateTime dateToProcess)
        {
            return this.GetNextPaymentDate(originalPaymentDueDate, guarantor, dateToProcess, false, false);
        }
        
        public DateTime GetNextPaymentDate(DateTime? originalPaymentDueDate, Guarantor guarantor, DateTime dateToProcess, bool isImmediateStatement, bool isFirstStatement)
        {
            DateTime guessPaymentDate = this.GeneratePaymentDueDayUnadjustedForDateToProcess(originalPaymentDueDate, guarantor, dateToProcess, isImmediateStatement, isFirstStatement, null);
            
            if (isImmediateStatement)
            {
                return guessPaymentDate;
            }

            // check to make sure there's enough time between statement 
            while (dateToProcess.Date.AddDays(this.StatementToPaymentDatePeriodBufferDays) > guessPaymentDate)
            {
                guessPaymentDate = guessPaymentDate.AddDays(1);
            }

            return guessPaymentDate;
        }
        
        /// <summary>
        /// find the nearest payment due date in the future
        /// </summary>
        public DateTime GetNextPaymentDate(int overridePaymentDueDay)
        {
            int year = DateTime.UtcNow.Year;
            int month = DateTime.UtcNow.Month;
            
            DateTime guessPaymentDate = this.GetValidPaymentDueDate(year, month, overridePaymentDueDay);
            if (guessPaymentDate >= DateTime.UtcNow.Date)
            {
                // it's not in the past
                return guessPaymentDate;
            }

            // add one month
            DateTime newDate = new DateTime(year, month, 1).AddMonths(1);
            guessPaymentDate = this.GetValidPaymentDueDate(newDate.Year, newDate.Month, overridePaymentDueDay);

            return guessPaymentDate;
        }

        private DateTime GetValidPaymentDueDate(int year, int month, int day)
        {
            if (day > DateConstants.LatestValidPaymentDueDay) // 27
            {
                day = DateTime.DaysInMonth(year, month);
            }

            return new DateTime(year, month, day).Date;
        }
        
        private DateTime GetNextPaymentDateForImmediateStatement(int paymentDueDay, DateTime dateToProcess)
        {
            DateTime targetDate = dateToProcess.AddDays(this.StatementToPaymentDatePeriodBufferDays);
            if (paymentDueDay > 27)
            {
                paymentDueDay = DateTime.DaysInMonth(targetDate.Year, targetDate.Month);
            }
            return new DateTime(targetDate.Year, targetDate.Month, paymentDueDay);
        }
        
        public DateTime GetStatementPeriodEndDate(DateTime? originalPaymentDueDate, Guarantor guarantor, DateTime dateToProcess, bool isImmediateStatement = false, bool isFirstStatement = false)
        {
            if (isImmediateStatement)
            {
                return dateToProcess;
            }

            DateTime nextEndDate = this.GetNextStatementDate(originalPaymentDueDate, guarantor, dateToProcess, false, isFirstStatement).Date.AddDays(-1 * this.StatementPostingPeriodBuffer);
            return this.SetDateToEndOfDay(nextEndDate);
        }
        
        /// <summary>
        /// returns the next payment due date, never a date in the past.
        /// if grace period, statement.PaymentDueDate
        /// if not, calculated next PaymentDueDate 
        /// </summary>
        public DateTime GetNextPaymentDate(Guarantor guarantor, VpStatement lastStatement)
        {
            if (lastStatement?.IsGracePeriod ?? false)
            {
                return lastStatement.PaymentDueDate;
            }
            
            return this.GetPaymentDueDateForNextStatement(guarantor, lastStatement);
        }
        
        /// <summary>
        /// returns the payment due date for the next statement.
        /// </summary>
        public DateTime GetPaymentDueDateForNextStatement(Guarantor guarantor, VpStatement lastStatement)
        {
            DateTime nextStatementDate;

            if (lastStatement == null)
            {
                // VPNG-20399 In order to give accurate next payment date for Immediate Statement we need to treat it like no statement has been run
                nextStatementDate = this.GetNextStatementDate(null, guarantor, DateTime.UtcNow, false, true);
            }
            else
            {
                // awaiting statement
                nextStatementDate = guarantor.NextStatementDate ?? this.GetNextStatementDate(lastStatement.PaymentDueDate, guarantor, lastStatement.PaymentDueDate);
            }
            
            return this.GetNextPaymentDate(lastStatement?.PaymentDueDate, guarantor, nextStatementDate);

        }

        /// <summary>
        /// this method assumes Guarantor.PaymentDueDay has already changed
        /// </summary>
        public DateTime GetNewPaymentDateForPaymentDueDayChange(Guarantor guarantor, VpStatement lastStatement)
        {
            if (lastStatement == null)
            {
                return this.GetPaymentDueDateForNextStatement(guarantor, null);
            }

            DateTime newPaymentDueDate;

            if (lastStatement.PaymentDueDate.Day == guarantor.PaymentDueDay)
            {
                // it's the same
                return lastStatement.PaymentDueDate.Date;
            }
            
            if (guarantor.PaymentDueDay > lastStatement.PaymentDueDate.Day)
            {
                newPaymentDueDate = this.GetNextPaymentDate(lastStatement.PaymentDueDate.AddMonths(-1), guarantor, lastStatement.StatementDate);
                return newPaymentDueDate;
            }

            newPaymentDueDate = this.GetNextPaymentDate(lastStatement.PaymentDueDate, guarantor, lastStatement.StatementDate);
            return newPaymentDueDate;
        }

        /// <summary>
        /// does not save to database - only updates the entity
        /// </summary>
        public bool UpdateStatementPaymentDueDate(BaseStatement statement, DateTime newPaymentDueDate, bool updateOriginalPaymentDueDate)
        {
            if (statement.PaymentDueDate.Date == newPaymentDueDate.Date)
            {
                return false;
            }
            
            DateTime previousPaymentDueDate = statement.PaymentDueDate;

            statement.PaymentDueDate = newPaymentDueDate;

            if (updateOriginalPaymentDueDate)
            {
                // if we're changing the payment due date because of finance plan origination, update this also
                // future calculations are often based on OriginalPaymentDueDate
                statement.OriginalPaymentDueDate = newPaymentDueDate;
            }

            if (statement.PaymentDueDate.Date > previousPaymentDueDate.Date)
            {
                // make sure we'd process payments again if moving payment due date forward
                if (statement.ProcessingStatus == ChangeEventStatusEnum.Processed)
                {
                    statement.ProcessingStatus = ChangeEventStatusEnum.Unprocessed;
                }
            }

            return true;
        }
    }
}
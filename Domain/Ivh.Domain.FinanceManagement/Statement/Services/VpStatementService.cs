﻿namespace Ivh.Domain.FinanceManagement.Statement.Services
{
    using System;
    using System.Collections.Generic;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Entities;
    using FinancePlan.Interfaces;
    using FinancePlan.Entities;
    using Guarantor.Entities;
    using Guarantor.Interfaces;
    using Interfaces;
    using ValueTypes;

    public class VpStatementService : DomainService, IVpStatementService
    {
        private readonly Lazy<IFinancePlanStatementRepository> _financePlanStatementRepository;
        private readonly Lazy<IVpStatementRepository> _vpStatementRepository;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IFinancePlanService> _financePlanService;
        private readonly Lazy<IStatementDateService> _statementDateService;

        public VpStatementService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IFinancePlanStatementRepository> financePlanStatementRepository,
            Lazy<IVpStatementRepository> vpStatementRepository,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IFinancePlanService> financePlanService,
            Lazy<IStatementDateService> statementDateService
            ) : base(serviceCommonService)
        {
            this._financePlanStatementRepository = financePlanStatementRepository;
            this._vpStatementRepository = vpStatementRepository;
            this._guarantorService = guarantorService;
            this._financePlanService = financePlanService;
            this._statementDateService = statementDateService;
        }

        public VpStatement GetStatement(int vpStatementId)
        {
            return this._vpStatementRepository.Value.GetById(vpStatementId);
        }

        public VpStatement GetStatement(int vpGuarantorId, int vpStatementId)
        {
            return this._vpStatementRepository.Value.GetVpStatement(vpGuarantorId, vpStatementId);
        }

        public IReadOnlyList<VpStatement> GetStatements(int vpGuarantorId)
        {
            return this._vpStatementRepository.Value.GetVpStatements(vpGuarantorId);
        }

        public IList<VpStatement> GetStatementsForCreatingPaperStatements()
        {
            return this._vpStatementRepository.Value.GetStatementsForCreatingPaperStatements();
        }

        public void InsertOrUpdate(VpStatement statement)
        {
            this._vpStatementRepository.Value.InsertOrUpdate(statement);
        }

        public int GetStatementTotals(int guarantorId)
        {
            return this._vpStatementRepository.Value.GetVpStatementTotals(guarantorId);
        }

        public DateTime GetStatementPeriodStartDate(VpStatement previousStatement, Guarantor guarantor)
        {
            if (previousStatement != null)
            {
                return previousStatement.PeriodEndDate.AddMilliseconds(TransactionFilter.NewVisitExclusionWindowMS);
            }

            return guarantor.RegistrationDate.Date;
        }

        public IList<VpStatementPaymentDueDateResult> GetStatementsWithPaymentDueDateResults(DateTime dueDate)
        {
            return this._vpStatementRepository.Value.GetStatementsWithPaymentDueDateResults(dueDate);
        }

        public IList<PendingUncollectableStatementsResult> GetAllPendingUncollectableStatements(int maxAgingCountFinancePlans, DateTime statementDueDate)
        {
            IList<PendingUncollectableStatementsResult> pendingUncollectableStatementsResults = new List<PendingUncollectableStatementsResult>();
            foreach (PendingUncollectableStatementsResult pendingUncollectableStatementsResult in this._vpStatementRepository.Value.GetAllPendingUncollectableStatements(maxAgingCountFinancePlans, statementDueDate))
            {
                if (pendingUncollectableStatementsResult.FinancePlanId.HasValue)
                {
                    //DB Value of CurrentBucket may not be correct, so we need to compare it to the domain object value (as it is dynamically calculated).
                    FinancePlan financePlan = this._financePlanService.Value.GetFinancePlanById(pendingUncollectableStatementsResult.FinancePlanId.Value);
                    if (financePlan != null && financePlan.CurrentBucket >= maxAgingCountFinancePlans)
                    {
                        pendingUncollectableStatementsResult.CurrentBucket = financePlan.CurrentBucket;
                        pendingUncollectableStatementsResults.Add(pendingUncollectableStatementsResult);
                    }
                }
                else if (pendingUncollectableStatementsResult.VisitId.HasValue)
                {
                    pendingUncollectableStatementsResults.Add(pendingUncollectableStatementsResult);
                }

            }

            return pendingUncollectableStatementsResults;
        }

        public IList<int> GuarantorsIdsWhoHaventHadUncollectableEmailForCurrentStatement(IList<int> guarantors)
        {
            return this._vpStatementRepository.Value.GuarantorsIdsWhoHaventHadUncollectableEmailForCurrentStatement(guarantors);
        }

        public IList<int> GuarantorsIdsWhoHaventHadPastDueEmailForCurrentStatement(IList<int> guarantors)
        {
            return this._vpStatementRepository.Value.GuarantorsIdsWhoHaventHadPastDueEmailForCurrentStatement(guarantors);
        }

        public void MarkPastDueEmailAsSent(VpStatement mostRecentStatement)
        {
            if (mostRecentStatement == null)
            {
                return;
            }

            if (!mostRecentStatement.PastDueEmailSent.HasValue || !mostRecentStatement.PastDueEmailSent.Value)
            {
                mostRecentStatement.PastDueEmailSent = true;
                this._vpStatementRepository.Value.InsertOrUpdate(mostRecentStatement);
            }
        }

        public void MarkUncollectableEmailAsSent(VpStatement mostRecentStatement)
        {
            if (mostRecentStatement == null)
            {
                return;
            }

            if (!mostRecentStatement.UncollectableEmailSent.HasValue || !mostRecentStatement.UncollectableEmailSent.Value)
            {
                mostRecentStatement.UncollectableEmailSent = true;
                this._vpStatementRepository.Value.InsertOrUpdate(mostRecentStatement);
            }
        }

        #region payment due date

        /// <summary>
        /// returns the next payment due date, never a date in the past.
        /// if grace period, statement.PaymentDueDate
        /// if not, calculated next PaymentDueDate 
        /// </summary>
        public DateTime GetNextPaymentDate(Guarantor guarantor)
        {
            VpStatement lastStatement = this.GetMostRecentStatement(guarantor);
            DateTime nextPaymentDate = this._statementDateService.Value.GetNextPaymentDate(guarantor, lastStatement);

            return nextPaymentDate;
        }

        /// <summary>
        /// returns the payment due date for the next statement.
        /// </summary>
        public DateTime GetPaymentDueDateForNextStatement(Guarantor guarantor)
        {
            VpStatement lastStatement = this.GetMostRecentStatement(guarantor);
            DateTime nextPaymentDate = this._statementDateService.Value.GetPaymentDueDateForNextStatement(guarantor, lastStatement);

            return nextPaymentDate;
        }

        public ProcessPaymentDueDayChangedResult ProcessPaymentDueDayChanged(Guarantor guarantor, bool isFinancePlanCreatedChange, int actionVisitPayUserId)
        {
            VpStatement currentStatement = this.GetMostRecentStatement(guarantor);
            bool isGracePeriod = currentStatement?.IsGracePeriod ?? false;
            DateTime newPaymentDueDate;

            bool meetsCriteriaToChange = this._financePlanService.Value.MeetsCriteriaToChangePaymentDueDate(guarantor);
            bool useFinancePlanRules = isFinancePlanCreatedChange && meetsCriteriaToChange;
            if (useFinancePlanRules)
            {
                newPaymentDueDate = this._statementDateService.Value.GetNextPaymentDate(guarantor.PaymentDueDay);
            }
            else
            {
                newPaymentDueDate = this._statementDateService.Value.GetNewPaymentDateForPaymentDueDayChange(guarantor, currentStatement);
            }

            bool newPaymentDueDateIsDifferent = currentStatement?.PaymentDueDate.Date != newPaymentDueDate.Date;
            if (!newPaymentDueDateIsDifferent)
            {
                return new ProcessPaymentDueDayChangedResult(false, newPaymentDueDate);
            }
            
            // is finance plan change, do not need to honor grace period
            // or, in grace period,
            // move things around
            if (useFinancePlanRules || isGracePeriod)
            {
                // update the statement
                this.UpdateStatementPaymentDueDate(currentStatement, newPaymentDueDate, isFinancePlanCreatedChange);

                // update the guarantor
                this.RecalculateGuarantorNextStatementDate(currentStatement, guarantor, DateTime.UtcNow);

                // update finance plans
                if (!isFinancePlanCreatedChange)
                {
                    // we don't need to do this if it's a finance plan origination, as there wouldn't be any existing plans
                    // we don't need to do this without a statement, as there wouldn't be any existing plans

                    // update origination dates for pending plans
                    this._financePlanService.Value.UpdatePendingFinancePlanOriginationDate(guarantor, currentStatement);
                    
                    this._financePlanService.Value.RescheduleRecurringFinancePlanPayments(guarantor, newPaymentDueDate, actionVisitPayUserId, currentStatement.VpStatementId);
                }

                return new ProcessPaymentDueDayChangedResult(true, newPaymentDueDate);
            }

            // in awaiting statement, move the next statement date to accommodate the new payment due day
            // the current statement does not change
            this.RecalculateGuarantorNextStatementDate(currentStatement, guarantor, currentStatement?.StatementDate ?? DateTime.UtcNow, newPaymentDueDate);
            return new ProcessPaymentDueDayChangedResult(false, newPaymentDueDate);
        }

        // not on the interface, public for unit testing
        public void UpdateStatementPaymentDueDate(VpStatement statement, DateTime newPaymentDueDate, bool updateOriginalPaymentDueDate)
        {
            if (statement == null)
            {
                return;
            }

            // update vpstatement
            if (this._statementDateService.Value.UpdateStatementPaymentDueDate(statement, newPaymentDueDate, updateOriginalPaymentDueDate))
            {
                this._vpStatementRepository.Value.InsertOrUpdate(statement);
            }

            // update fpstatement
            FinancePlanStatement financePlanStatement = this._financePlanStatementRepository.Value.GetFinancePlanStatementByVpStatementId(statement.VpGuarantorId, statement.VpStatementId);
            if (financePlanStatement != null)
            {
                if (this._statementDateService.Value.UpdateStatementPaymentDueDate(financePlanStatement, newPaymentDueDate, updateOriginalPaymentDueDate))
                {
                    this._financePlanStatementRepository.Value.InsertOrUpdate(financePlanStatement);
                }
            }
        }

        // not on the interface, public for unit testing
        public void RecalculateGuarantorNextStatementDate(VpStatement currentStatement, Guarantor guarantor, DateTime dateToProcess, DateTime? overridePaymentDueDay = null)
        {
            DateTime nextStatementDate = this._statementDateService.Value.GetNextStatementDate(overridePaymentDueDay ?? currentStatement.PaymentDueDate, guarantor, dateToProcess, false, currentStatement == null);
            guarantor.NextStatementDate = nextStatementDate;

            this._guarantorService.Value.InsertOrUpdateGuarantor(guarantor);
        }

        #endregion

        public void IncrementTheNextStatementDate(Guarantor guarantor, DateTime dateToProcess, VpStatement newStatement, bool isFirstStatement)
        {
            guarantor.NextStatementDate = this._statementDateService.Value.GetNextStatementDate(newStatement?.PaymentDueDate, guarantor, dateToProcess, false, isFirstStatement);
            this._guarantorService.Value.InsertOrUpdateGuarantor(guarantor);
        }

        public VpStatement CreateStatement(Guarantor guarantor, DateTime dateToProcess, VpStatement previousStatement, bool isImmediateStatement, bool isFirstStatement, VpStatementVersionEnum version)
        {
            if (guarantor.NextStatementDate == null)
            {
                throw new NullReferenceException("guarantor.NextStatementDate is null");
            }

            DateTime? originalPaymentDueDate = previousStatement?.OriginalPaymentDueDate;

            VpStatement statement = new VpStatement
            {
                VpGuarantorId = guarantor.VpGuarantorId,
                VpStatementType = VpStatementTypeEnum.Standard,
                StatementVersion = version,
                StatementDate = this._statementDateService.Value.GetNextStatementDate(originalPaymentDueDate, guarantor, dateToProcess, isImmediateStatement, isFirstStatement),
                PaymentDueDate = this._statementDateService.Value.GetNextPaymentDate(originalPaymentDueDate, guarantor, dateToProcess, isImmediateStatement, isFirstStatement),
                PeriodStartDate = this.GetStatementPeriodStartDate(previousStatement, guarantor),
                PeriodEndDate = this._statementDateService.Value.GetStatementPeriodEndDate(previousStatement?.OriginalPaymentDueDate, guarantor, dateToProcess, isImmediateStatement, isFirstStatement),
                OriginalPaymentDueDate = this._statementDateService.Value.GeneratePaymentDueDayUnadjustedForDateToProcess(previousStatement?.OriginalPaymentDueDate, guarantor, dateToProcess, isImmediateStatement, isFirstStatement)
            };

            //We want the original paymentduedate to be within a month of the last statement.
            while (statement.OriginalPaymentDueDate.AddMonths(1) < statement.PaymentDueDate)
            {
                statement.OriginalPaymentDueDate = statement.OriginalPaymentDueDate.AddMonths(1);
            }

            return statement;
        }

        public bool HasStatementBeenCreatedAlready(Guarantor guarantor, DateTime dateToProcess)
        {
            const int dayBracket = 1;
            IList<VpStatement> statements = this._vpStatementRepository.Value.GetStatementsAroundDateForGuarantor(guarantor.VpGuarantorId, dateToProcess, dayBracket);
            return statements.IsNotNullOrEmpty();
        }

        public VpStatement GetMostRecentStatement(Guarantor guarantor)
        {
            return this._vpStatementRepository.Value.GetMostRecentStatement(guarantor);
        }

        public VpStatementForProcessPaymentResult GetMostRecentStatementForProcessPayment(Guarantor guarantor)
        {
            return this._vpStatementRepository.Value.GetMostRecentStatementForProcessPayment(guarantor);
        }

        public VpStatementForProcessPaymentResult GetVpStatementForProcessPaymentResult(int vpGuarantorId, int vpStatementId)
        {
            return this._vpStatementRepository.Value.GetVpStatementForProcessPaymentResult(vpGuarantorId, vpStatementId);
        }

        public IList<VpStatementQueueStatusResult> GetMostRecentStatementQueueStatus(IList<int> vpGuarantorIds)
        {
            return this._vpStatementRepository.Value.GetMostRecentStatementQueueStatus(vpGuarantorIds);
        }

        public bool DoesGuarantorHaveStatement(int vpGuarantorId)
        {
            return this._vpStatementRepository.Value.DoesGuarantorHaveStatement(vpGuarantorId);
        }

        public IList<GuarantorStatementInfo> GetGuarantorStatementInfoToBeChargedWithDate(DateTime dateTime)
        {
            return this._vpStatementRepository.Value.GetGuarantorStatementInfoToBeChargedWithDate(dateTime);
        }

        public void MarkStatementAsProcessed(int statementId, int vpGuarantorId, string logMessage = null)
        {
            this._vpStatementRepository.Value.MarkStatementAsProcessed(statementId);
            if (!string.IsNullOrWhiteSpace(logMessage))
            {
                this.LoggingService.Value.Info(() => $"VpStatementService::MarkStatementAsProcessed - Marked statement {statementId} for VpGuarantor {vpGuarantorId} as processed. {logMessage}");
            }
        }

        public void MarkStatementAsUnprocessed(int statementId, int vpGuarantorId, string logMessage = null)
        {
            this._vpStatementRepository.Value.MarkStatementAsUnprocessed(statementId);
            if (!string.IsNullOrWhiteSpace(logMessage))
            {
                this.LoggingService.Value.Info(() => $"VpStatementService::MarkStatementAsUnprocessed - Marked statement {statementId} for VpGuarantor {vpGuarantorId} as unprocessed. {logMessage}");
            }
        }

        public void MarkStatementAsQueued(int statementId, int vpGuarantorId, string logMessage = null)
        {
            this._vpStatementRepository.Value.MarkStatementAsQueued(statementId);
            if (!string.IsNullOrWhiteSpace(logMessage))
            {
                this.LoggingService.Value.Info(() => $"VpStatementService::MarkStatementAsQueued - Marked statement {statementId} for VpGuarantor {vpGuarantorId} as queued. {logMessage}");
            }
        }

        public bool IsAwaitingStatement(VpStatement lastStatement)
        {
            return lastStatement == null || DateTime.Compare(lastStatement.PaymentDueDate.Date, DateTime.UtcNow.Date) < 0;
        }

        public byte[] GetStatementContent(int vpGuarantorId, int vpStatementId)
        {
            return this._vpStatementRepository.Value.GetStatementContent(vpGuarantorId, vpStatementId);
        }
    }
}

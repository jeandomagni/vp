﻿namespace Ivh.Domain.FinanceManagement.ValueTypes
{
    using System;
    using Common.VisitPay.Enums;

    public class ScheduledPaymentResult
    {
        public int? MadeByVpGuarantorId { get; set; }
        public int MadeForVpGuarantorId { get; set; }

        /// <summary>
        /// a recurring payment won't have a payment id
        /// </summary>
        public int? PaymentId { get; set; }
        public int? PaymentMethodId { get; set; }
        public PaymentTypeEnum PaymentType { get; set; }
        public decimal ScheduledPaymentAmount { get; set; }
        public DateTime ScheduledPaymentDate { get; set; }
    }
}
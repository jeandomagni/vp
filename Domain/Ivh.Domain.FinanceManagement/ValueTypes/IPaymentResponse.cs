﻿namespace Ivh.Domain.FinanceManagement.ValueTypes
{
    public interface IPaymentResponse
    {
        string GatewayToken { get; }
        bool Succeeded { get; }       
    }
}
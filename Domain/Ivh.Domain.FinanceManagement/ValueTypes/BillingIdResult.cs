﻿namespace Ivh.Domain.FinanceManagement.ValueTypes
{
    using System.Collections.Generic;

    public struct BillingIdResult
    {
        public string BillingID { get; set; }
        public string GatewayToken { get; set; }
        public bool Succeeded { get; set; }
        public IReadOnlyDictionary<string, string> Response { get; set; }
    }
}

﻿namespace Ivh.Domain.FinanceManagement.ValueTypes
{
    using System;

    public class ProcessPaymentDueDayChangedResult
    {
        public ProcessPaymentDueDayChangedResult(bool isChanged, DateTime newPaymentDueDate)
        {
            this.IsChanged = isChanged;
            this.NewPaymentDueDate = newPaymentDueDate;
        }

        public bool IsChanged { get; }

        public DateTime NewPaymentDueDate { get; }
    }
}
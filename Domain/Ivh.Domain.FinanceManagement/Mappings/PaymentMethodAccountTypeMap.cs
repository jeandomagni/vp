﻿using FluentNHibernate.Mapping;

namespace Ivh.Domain.FinanceManagement.Mappings
{
    public class PaymentMethodAccountTypeMap : ClassMap<Entities.PaymentMethodAccountType>
    {
        public PaymentMethodAccountTypeMap()
        {
            this.Schema("dbo");
            this.Table("PaymentMethodAccountType");
            this.Id(x => x.PaymentMethodAccountTypeId);
            this.Map(x => x.PaymentMethodAccountTypeText).Not.Nullable();
            this.Map(x => x.PaymentMethodAccountTypeListDisplayText).Not.Nullable();
            this.Map(x => x.PaymentMethodAccountTypeSelectedDisplayText).Not.Nullable();
            this.Map(x => x.DisplayOrder).Not.Nullable();
        }
    }
}

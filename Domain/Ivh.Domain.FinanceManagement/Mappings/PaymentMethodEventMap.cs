﻿using FluentNHibernate.Mapping;

namespace Ivh.Domain.FinanceManagement.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class PaymentMethodEventMap : ClassMap<Entities.PaymentMethodEvent>
    {
        public PaymentMethodEventMap()
        {
            this.Schema("dbo");
            this.Table("PaymentMethodEvent");
            this.Id(x => x.PaymentMethodEventId);
            this.Map(x => x.PaymentMethodStatus).Column("PaymentMethodStatusId").CustomType<PaymentMethodStatusEnum>();
            this.Map(x => x.InsertDate);
            this.References(x => x.PaymentMethod).Column("PaymentMethodId");
            this.Map(x => x.VisitPayUserId);
            this.Map(x => x.VpGuarantorId);
        }
    }
}
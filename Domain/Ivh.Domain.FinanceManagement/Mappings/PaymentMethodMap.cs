﻿namespace Ivh.Domain.FinanceManagement.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class PaymentMethodMap : ClassMap<PaymentMethod>
    {
        public PaymentMethodMap()
        {
            this.Schema("dbo");
            this.Table("PaymentMethod");
            this.Id(x => x.PaymentMethodId);
            this.Map(x => x.VpGuarantorId);
            this.Map(x => x.IsActive);
            this.Map(x => x.IsPrimary);
            this.Map(x => x.IsRegistrationPaymentMethod);
            this.Map(x => x.GatewayToken);
            this.Map(x => x.BillingId);
            this.Map(x => x.PaymentMethodType).Column("PaymentMethodTypeId").CustomType<PaymentMethodTypeEnum>();
            this.Map(x => x.CreatedDate);
            this.Map(x => x.UpdatedDate);
            this.Map(x => x.LastFour);
            this.Map(x => x.CreatedUserId);
            this.Map(x => x.UpdatedUserId);
            this.Map(x => x.AccountNickName);

            this.Map(x => x.BankName);
            this.Map(x => x.FirstName);
            this.Map(x => x.LastName);

            this.Map(x => x.ExpDate);
            this.Map(x => x.NameOnCard);
            this.Map(x => x.ExpiringOn);
            this.Map(x => x.ExpiredOn);

            this.References(x => x.PaymentMethodAccountType).Column("PaymentMethodAccountTypeId")
                .Not.LazyLoad()
                .Fetch.Join();
            this.References(x => x.PaymentMethodProviderType).Column("PaymentMethodProviderTypeId")
                .Not.LazyLoad()
                .Fetch.Join();

            this.HasMany(x => x.BillingAddresses)
                .BatchSize(50)
                .KeyColumn("PaymentMethodId")
                .Not.Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Cascade.AllDeleteOrphan();
            
            this.HasMany(x => x.PaymentMethodAuthorizations)
                .KeyColumn("PaymentMethodAuthorizationId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .ExtraLazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();
        }
    }
}
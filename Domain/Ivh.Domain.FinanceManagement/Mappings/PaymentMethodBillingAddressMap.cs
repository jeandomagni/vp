﻿using FluentNHibernate.Mapping;

namespace Ivh.Domain.FinanceManagement.Mappings
{
    public class PaymentMethodBillingAddressMap : ClassMap<Entities.PaymentMethodBillingAddress>
    {
        public PaymentMethodBillingAddressMap()
        {
            this.Schema("dbo");
            this.Table("PaymentMethodBillingAddress");
            this.Id(x => x.PaymentMethodBillingAddressId);
            this.Map(x => x.Address1);
            this.Map(x => x.Address2);
            this.Map(x => x.City);
            this.Map(x => x.State);
            this.Map(x => x.Zip);
        }
    }
}

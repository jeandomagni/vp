﻿using FluentNHibernate.Mapping;

namespace Ivh.Domain.FinanceManagement.Mappings
{
    public class PaymentMethodProviderTypeMap : ClassMap<Entities.PaymentMethodProviderType>
    {
        public PaymentMethodProviderTypeMap()
        {
            this.Schema("dbo");
            this.Table("PaymentMethodProviderType");
            this.Id(x => x.PaymentMethodProviderTypeId);
            this.Map(x => x.PaymentMethodProviderTypeText).Not.Nullable();
            this.Map(x => x.PaymentMethodProviderTypeDisplayText).Not.Nullable();
            this.Map(x => x.ImageName).Not.Nullable();
            this.Map(x => x.DisplayOrder).Not.Nullable();
        }
    }
}

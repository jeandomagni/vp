﻿namespace Ivh.Domain.FinanceManagement.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class PaymentMethodAuthorizationMap : ClassMap<PaymentMethodAuthorization>
    {
        public PaymentMethodAuthorizationMap()
        {
            this.Schema("dbo");
            this.Table("PaymentMethodAuthorization");
            this.Id(x => x.PaymentMethodAuthorizationId);
            this.Map(x => x.PaymentMethodAuthorizationType).Column("PaymentMethodAuthorizationTypeId").CustomType<PaymentMethodAuthorizationTypeEnum>().Not.Nullable();
            this.Map(x => x.CmsVersionId).Not.Nullable();
            this.Map(x => x.InsertDate).Not.Nullable();
            this.Map(x => x.AuthorizedAmount).Nullable();
            this.Map(x => x.AuthorizedDuration).Nullable();
            this.Map(x => x.CorrelationGuid).Not.Nullable();
            this.References(x => x.PaymentMethod).Column("PaymentMethodId");
        }
    }
}
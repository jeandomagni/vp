﻿namespace Ivh.Domain.FinanceManagement.Mappings
{
    using System.Collections.Generic;
    using AutoMapper;
    using Core.Audit.Entities;
    using FinancePlan.Entities;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;
    using Payment.Entities;

    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            // Automapper mapping go here.
            // Use this.CreateMap<,>().
            // DO NOT USE Mapper.CreateMap<,>().
            this.CreateMap<FinancePlan, AuditEventFinancePlan>()
                .ForMember(dest => dest.VpGuarantorId, opts => opts.MapFrom(src => src.VpGuarantor.VpGuarantorId))
                .ForMember(dest => dest.CreatedVpStatementId, opts => opts.MapFrom(src => src.CreatedVpStatement.VpStatementId))
                .ForMember(dest => dest.OfferCalculationStrategy, opts => opts.MapFrom(src => src.OfferCalculationStrategy.ToString()))
                .ForMember(dest => dest.OriginalFinancePlanId, opts => opts.MapFrom(src => src.OriginalFinancePlan.FinancePlanId))
                .ForMember(dest => dest.ReconfiguredFinancePlanId, opts => opts.MapFrom(src => src.ReconfiguredFinancePlan.FinancePlanId))
                .ForMember(dest => dest.CurrentFinancePlanStatusId, opts => opts.MapFrom(src => src.CurrentFinancePlanStatus.FinancePlanStatusId));

            this.CreateMap<Payment, Payment>()
                .ForMember(dest => dest.PaymentId, opts => opts.UseValue(0))
                .ForMember(dest => dest.PaymentAllocations, opts => opts.MapFrom(src => new List<PaymentAllocation>()));

            this.CreateMap<PaymentScheduledAmount, PaymentScheduledAmount>()
                .ForMember(dest => dest.PaymentScheduledAmountId, opts => opts.UseValue(0))
                .ForMember(dest => dest.Payment, opts => opts.UseValue<Payment>(null));

            this.CreateMap<PaymentAllocation, PaymentAllocation>()
                .ForMember(dest => dest.PaymentAllocationId, opts => opts.UseValue(0))
                .ForMember(dest => dest.Payment, opts => opts.UseValue<Payment>(null));

            this.CreateMap<PaymentStatusHistory, PaymentStatusHistory>()
                .ForMember(dest => dest.PaymentStatusHistoryId, opts => opts.UseValue(0))
                .ForMember(dest => dest.Payment, opts => opts.UseValue<Payment>(null));

            this.CreateMap<PaymentImportMessage, PaymentImport>();
        }
    }
}
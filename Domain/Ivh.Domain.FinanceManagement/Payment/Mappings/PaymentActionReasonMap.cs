namespace Ivh.Domain.FinanceManagement.Payment.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class PaymentActionReasonMap : ClassMap<PaymentActionReason>
    {
        public PaymentActionReasonMap()
        {
            this.Schema("dbo");
            this.Table("PaymentActionReason");
            this.ReadOnly();

            this.Id(x => x.PaymentActionReasonId);

            this.Map(x => x.Description).Not.Nullable().Length(2000);
            this.Map(x => x.IsActive).Not.Nullable();
            this.Map(x => x.IsCancel).Not.Nullable();
            this.Map(x => x.IsReschedule).Not.Nullable();
            this.Map(x => x.IsManual).Not.Nullable();
            this.Map(x => x.IsRecurring).Not.Nullable();
            this.Map(x => x.RequireInput).Not.Nullable();
            this.Map(x => x.DisplayOrder).Not.Nullable();
        }
    }
}
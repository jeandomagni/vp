﻿namespace Ivh.Domain.FinanceManagement.Payment.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class PaymentMenuMap : ClassMap<PaymentMenu>
    {
        public PaymentMenuMap()
        {
            this.Schema("dbo");
            this.Table("PaymentMenu");
            this.ReadOnly();
            this.Id(x => x.PaymentMenuId);
            this.Map(x => x.PaymentMenuName);
            this.HasMany(x => x.PaymentMenuItems)
                .KeyColumn("PaymentMenuId")
                .Not.KeyNullable()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cache.ReadOnly();

            this.Cache.ReadOnly();
        }
    }
}
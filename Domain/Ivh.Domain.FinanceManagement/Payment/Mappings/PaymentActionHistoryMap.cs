namespace Ivh.Domain.FinanceManagement.Payment.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class PaymentActionHistoryMap : ClassMap<PaymentActionHistory>
    {
        public PaymentActionHistoryMap()
        {
            this.Schema("dbo");
            this.Table("PaymentActionHistory");

            this.Id(x => x.PaymentActionHistoryId);

            this.Map(x => x.PaymentActionType).Column("PaymentActionTypeId").Not.Nullable().CustomType<PaymentActionTypeEnum>();
            this.Map(x => x.CorrelationGuid).Not.Nullable();
            this.Map(x => x.InsertDate);
            this.Map(x => x.PaymentRescheduledDate).Nullable();
            this.Map(x => x.Description).Nullable();
            this.Map(x => x.FinancePlanId).Nullable();
            this.Map(x => x.VpGuarantorId).Not.Nullable();

            this.References(x => x.Payment, "PaymentId").Nullable();
            this.References(x => x.ActionVisitPayUser, "ActionVisitPayUserId");
            this.References(x => x.PaymentActionReason, "PaymentActionReasonId");
        }
    }
}
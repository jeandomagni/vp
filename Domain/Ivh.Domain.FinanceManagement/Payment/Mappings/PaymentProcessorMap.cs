namespace Ivh.Domain.FinanceManagement.Payment.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class PaymentProcessorMap : ClassMap<PaymentProcessor>
    {
        public PaymentProcessorMap()
        {
            this.Schema("dbo");
            this.Table("PaymentProcessor");
            this.Id(x => x.PaymentProcessorId);
            this.Map(x => x.PaymentProcessorName);
            this.Map(x => x.ResponseField1Name);
            this.Map(x => x.ResponseField2Name);
            this.Map(x => x.ResponseField3Name);
            this.Map(x => x.ResponseField4Name);
            this.Map(x => x.ResponseField5Name);
            this.Map(x => x.ResponseField6Name);
            this.Map(x => x.ResponseField7Name);
            this.Map(x => x.ResponseField8Name);
            this.Map(x => x.ResponseField9Name);
            this.Map(x => x.ResponseField10Name);
        }
    }
}
﻿namespace Ivh.Domain.FinanceManagement.Payment.Mappings
{
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;
    
    public class PaymentMap : ClassMap<Payment>
    {
        public PaymentMap()
        {
            this.Schema("dbo");
            this.Table("Payment");
            this.Id(x => x.PaymentId);
            this.Cache.ReadWrite();
            this.Map(x => x.InsertDate);
            this.Map(x => x.PaymentType).Column("PaymentTypeId").CustomType<PaymentTypeEnum>();
            this.Map(x => x.ActualPaymentAmount);
            this.Map(x => x.OriginalScheduledPaymentDate);
            this.Map(x => x.ScheduledPaymentDate);
            this.Map(x => x.ActualPaymentDate);
            this.Map(x => x.ScheduledDiscountPercent);
            this.Map(x => x.PaymentAttemptCount);
            this.Map(x => x.IsGuarantorCure);
            this.Map(x => x.IsHouseholdBalance);
            this.Map(x => x.CreatedVisitPayUserId).Nullable();
            this.Map(x => x.SplitPaymentGroupingGuid).Nullable();
            this.Map(x => x.CurrentPaymentStatus).Column("CurrentPaymentStatusId").CustomType<PaymentStatusEnum>();
            this.References(x => x.ParentPayment).Column("ParentPaymentId");
            this.References(x => x.PaymentMethod).Column("PaymentMethodId").Nullable();
            this.References(x => x.Guarantor).Column("VpGuarantorId");
            this.References(x => x.MerchantAccount).Column("MerchantAccountId").Nullable();

            this.HasMany(x => x.PaymentAllocations)
                .KeyColumn("PaymentId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();

            this.HasMany(x => x.PaymentPaymentProcessorResponses)
                .KeyColumn("PaymentId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();

            this.HasMany(x => x.PaymentScheduledAmounts)
                .Not.LazyLoad()
                .KeyColumn("PaymentId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();

            this.HasMany(x => x.PaymentStatusHistories)
                .KeyColumn("PaymentId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();
            
            this.HasMany(x => x.ReversalPayments)
                .KeyColumn("ParentPaymentId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Not.Cascade.All();

            this.HasMany(x => x.PaymentActionHistories)
                .KeyColumn("PaymentId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();

            this.References(x => x.DiscountOffer)
                .Column("DiscountOfferId")
                .Cascade.All();
        }
    }
}
﻿namespace Ivh.Domain.FinanceManagement.Payment.Mappings
{
    using FluentNHibernate.Mapping;
    using Ivh.Domain.FinanceManagement.Payment.Entities;

    public class MerchantAccountMap : ClassMap<MerchantAccount>
    {
        public MerchantAccountMap()
        {
            this.Schema("dbo");
            this.Table("MerchantAccount");
            this.Id(x => x.MerchantAccountId);
            this.Map(x => x.Description).Not.Nullable();
            this.Map(x => x.Account).Not.Nullable();
            this.Map(x => x.CustomerIdSettingsKey).Not.Nullable();
            this.Map(x => x.PasswordSettingsKey).Not.Nullable();
            this.Map(x => x.PasswordQueryApiSettingsKey).Not.Nullable();
            this.Map(x => x.AchCustomerIdSettingsKey).Not.Nullable();
            this.Map(x => x.AchPasswordSettingsKey).Not.Nullable();
            this.References(x => x.MasterMerchantAccount).Column("MasterMerchantAccountId").Nullable().Not.LazyLoad();
            this.Cache.ReadOnly();
        }
    }
}

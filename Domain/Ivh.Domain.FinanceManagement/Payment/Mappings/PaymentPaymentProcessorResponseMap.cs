﻿namespace Ivh.Domain.FinanceManagement.Payment.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class PaymentPaymentProcessorResponseMap : ClassMap<PaymentPaymentProcessorResponse>
    {
        public PaymentPaymentProcessorResponseMap()
        {
            this.Not.LazyLoad();
            this.Schema("dbo");
            this.Table("PaymentPaymentProcessorResponse");
            this.Id(x => x.PaymentPaymentProcessorResponseId);

            this.Map(x => x.InsertDate);
            this.Map(x => x.SnapshotPaymentAmount);

            this.References(x => x.Payment)
                .Column("PaymentId")
                .Fetch.Join()
                .Not.LazyLoad();

            this.References(x => x.PaymentProcessorResponse)
                .Column("PaymentProcessorResponseId")
                .Fetch.Join()
                .Not.LazyLoad();
        }
    }
}
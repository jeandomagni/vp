﻿namespace Ivh.Domain.FinanceManagement.Payment.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class PaymentMenuItemMap : ClassMap<PaymentMenuItem>
    {
        public PaymentMenuItemMap()
        {
            this.Schema("dbo");
            this.Table("PaymentMenuItem");
            this.ReadOnly();
            this.Id(x => x.PaymentMenuItemId);
            this.Map(x => x.CmsRegionId).Not.Nullable();
            this.Map(x => x.DisplayOrder).Not.Nullable();
            this.Map(x => x.IconCssClass).Nullable();

            this.HasMany(x => x.PaymentMenuItems)
                .KeyColumn("ParentPaymentMenuItemId")
                .Not.LazyLoad()
                .BatchSize(50)
                .Cache.ReadOnly();

            this.References(x => x.ParentPaymentMenuItem)
                .Column("ParentPaymentMenuItemId")
                .Nullable();

            this.References(x => x.PaymentOption)
                .Column("PaymentOptionId")
                .Nullable();

            this.Cache.ReadOnly();
        }
    }
}
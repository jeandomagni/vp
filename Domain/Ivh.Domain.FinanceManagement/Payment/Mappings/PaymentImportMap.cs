﻿namespace Ivh.Domain.FinanceManagement.Payment.Mappings
{
    using FluentNHibernate.Mapping;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Domain.FinanceManagement.Payment.Entities;

    public class PaymentImportMap : ClassMap<PaymentImport>
    {
        public PaymentImportMap()
        {
            this.Schema("dbo");
            this.Table("PaymentImport");
            this.Id(x => x.PaymentImportId);

            this.Map(x => x.LockBoxNumber).Length(20);
            this.Map(x => x.BatchCreditDate).Not.Nullable();
            this.Map(x => x.GuarantorAccountNumber).Length(16);
            this.Map(x => x.GuarantorFirstName).Length(25);
            this.Map(x => x.GuarantorLastName).Length(36);
            this.Map(x => x.InvoiceAmount);
            this.Map(x => x.BatchNumber).Length(10);
            this.Map(x => x.PaymentSequenceNumber).Length(10);
            this.Map(x => x.CheckPaymentNumber).Length(17);
            this.Map(x => x.CheckAccountNumber).Length(4);
            this.Map(x => x.CheckRouting).Length(9);
            this.Map(x => x.CheckRemitter).Length(61);
            this.Map(x => x.PsrId).Length(16);
            this.Map(x => x.PaymentComment).Length(80);
            this.Map(x => x.EmailAddress).Length(80);
            this.Map(x => x.DraftReturnCode).Length(3);
            this.Map(x => x.DraftReturnInfo).Length(35);
            this.Map(x => x.RdfiBankName).Length(45);

            this.Map(x => x.PaymentImportStateEnum).Column("PaymentImportStateId").CustomType<PaymentImportStateEnum>();
            this.References(x => x.VpGuarantor).Column("VpGuarantorId").Nullable();
            this.References(x => x.Payment).Column("PaymentId").Nullable();

            this.Map(x => x.PaymentImportStateReason).Nullable();
            this.Map(x => x.ClientUserNotes).Nullable();
        }
    }
}

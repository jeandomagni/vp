﻿namespace Ivh.Domain.FinanceManagement.Payment.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class PaymentFinancePlanMap : ClassMap<PaymentFinancePlan>
    {
        public PaymentFinancePlanMap()
        {
            this.Schema("dbo");
            this.Table("FinancePlan");
            this.ReadOnly();
            this.Id(x => x.FinancePlanId);
            this.Map(x => x.CurrentBucket);

            // Note: VPNG-19800 suggests that this field may not be reliable. FinancePlan entity relies on FinancePlanStatusHistory
            this.Map(x => x.FinancePlanStatus, "CurrentFinancePlanStatusId").CustomType<FinancePlanStatusEnum>();

            // Note the FinancePlan entity is relying on ActiveFinancePlanVisits sum of visit current balance
            this.Map(x => x.CurrentFinancePlanBalance).Not.Nullable();
            this.Map(x => x.OriginationDate).Nullable();
        }
    }
}
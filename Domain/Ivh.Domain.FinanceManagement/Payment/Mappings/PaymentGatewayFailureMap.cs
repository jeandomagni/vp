﻿namespace Ivh.Domain.FinanceManagement.Payment.Mappings
{
    using Common.Base.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class PaymentGatewayFailureMap : ClassMap<PaymentGatewayFailure>
    {
        public PaymentGatewayFailureMap()
        {
            this.Schema("dbo");
            this.Table("Payment");
            this.Id(x => x.PaymentId);
            this.Map(x => x.VpGuarantorId);
            this.Map(x => x.ScheduledPaymentDate);
            this.Map(x => x.PaymentTypeName);
            this.Map(x => x.ScheduledAmount);
            this.Map(x => x.FinancePlanId);
            this.Map(x => x.VisitId);
            this.Map(x => x.LastAttempt);
            this.Map(x => x.PaymentStatusName);
            this.Map(x => x.Comment);
        }
    }
}
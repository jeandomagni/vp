﻿namespace Ivh.Domain.FinanceManagement.Payment.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class PaymentScheduledAmountMap : ClassMap<PaymentScheduledAmount>
    {
        public PaymentScheduledAmountMap()
        {
            this.Schema("dbo");
            this.Table("PaymentScheduledAmount");
            this.Id(x => x.PaymentScheduledAmountId);
            this.Map(x => x.InsertDate);
            this.Map(x => x.ScheduledAmount).Not.Nullable();
            this.Map(x => x.ScheduledInterest).Nullable();
            this.Map(x => x.ScheduledPrincipal).Nullable();
            this.Map(x => x.FinancePlanId).Column("FinancePlanId").Nullable();
            this.Map(x => x.VisitId).Column("VisitId").Nullable();

            this.References(x => x.PaymentFinancePlan)
                .Column("FinancePlanId")
                .Nullable()
                .Not.Insert()
                .Not.Update();
            this.References(x => x.PaymentVisit)
                .Column("VisitId")
                .Nullable()
                .Not.Insert()
                .Not.Update();
            this.References(x => x.Payment)
                .Column("PaymentId")
                .Nullable();
        }
    }
}
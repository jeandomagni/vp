﻿using FluentNHibernate.Mapping;
using Ivh.Domain.FinanceManagement.Payment.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.FinanceManagement.Payment.Mappings
{
    public class BalanceTransferStatusMerchantAccountMap : ClassMap<BalanceTransferStatusMerchantAccount>
    {
        public BalanceTransferStatusMerchantAccountMap()
        {
            this.Schema("dbo");
            this.Table("BalanceTransferStatusMerchantAccount");
            this.Id(x => x.BalanceTransferStatusMerchantAccountId);
            this.References(x => x.BalanceTransferStatus).Column("BalanceTransferStatusId");
            this.References(x => x.MerchantAccount).Column("MerchantAccountId");
        }
    }
}

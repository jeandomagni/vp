﻿namespace Ivh.Domain.FinanceManagement.Payment.Mappings
{
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class PaymentAllocationMap : ClassMap<PaymentAllocation>
    {
        public PaymentAllocationMap()
        {
            this.Schema("dbo");
            this.Table("PaymentAllocation");
            this.Id(x => x.PaymentAllocationId);

            this.Map(x => x.FinancePlanId);
            this.Map(x => x.InsertDate);
                //.Generated.Always();
            this.Map(x => x.PaymentAllocationType).Column("PaymentAllocationTypeId").CustomType<PaymentAllocationTypeEnum>();
            this.Map(x => x.PaymentAllocationStatus).Column("PaymentAllocationStatusId").CustomType<PaymentAllocationStatusEnum>();
            this.Map(x => x.PaymentBatchStatus).Column("PaymentBatchStatusId").CustomType<PaymentBatchStatusEnum>();
            this.Map(x => x.ActualAmount);
            this.Map(x => x.OriginalAmount);
            this.Map(x => x.UnallocatedAmount);
            this.Map(x => x.DeleteDate)
                .Nullable();
            this.Map(x => x.ClearedDate)
                .Nullable();

            this.References(x => x.OriginalPaymentAllocation).Column("OriginalPaymentAllocationId");

            this.References(x => x.Payment).Column("PaymentId");
            
            this.References(x => x.PaymentVisit)
                .Column("VisitId")
                .Nullable();
        }
    }
}
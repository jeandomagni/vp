namespace Ivh.Domain.FinanceManagement.Payment.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class PaymentStatusHistoryMap : ClassMap<PaymentStatusHistory>
    {
        public PaymentStatusHistoryMap()
        {
            this.Schema("dbo");
            this.Table("PaymentStatusHistory");

            this.Id(x => x.PaymentStatusHistoryId);
            this.Map(x => x.InsertDate);
            this.Map(x => x.ChangeDescription).Length(254);
            this.Map(x => x.PaymentStatus)
                .Column("PaymentStatusId").CustomType<PaymentStatusEnum>();
            this.References(x => x.Payment)
                .Column("PaymentId")
                .Not.Nullable();
        }
    }
}
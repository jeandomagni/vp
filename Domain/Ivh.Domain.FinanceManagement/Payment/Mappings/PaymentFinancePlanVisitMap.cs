﻿namespace Ivh.Domain.FinanceManagement.Payment.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class PaymentFinancePlanVisitMap : ClassMap<PaymentFinancePlanVisit>
    {
        public PaymentFinancePlanVisitMap()
        {
            this.Schema("dbo");
            this.Table("FinancePlanVisit");
            this.ReadOnly();
            this.Id(x => x.FinancePlanVisitId);
            this.Map(x => x.HardRemoveDate).Nullable();
            this.Map(x => x.VisitId);
            this.Map(x => x.FinancePlanId).Nullable();

            this.Map(x => x.InterestDue)
                .Formula("(select sum(i.InterestDue) from FinancePlanVisitInterestDue i where i.FinancePlanVisitId = [FinancePlanVisitId])")
                .ReadOnly();
        }
    }
}
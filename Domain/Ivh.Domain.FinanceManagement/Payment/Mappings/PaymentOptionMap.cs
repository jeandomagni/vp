﻿namespace Ivh.Domain.FinanceManagement.Payment.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class PaymentOptionMap : ClassMap<PaymentOption>
    {
        public PaymentOptionMap()
        {
            this.Schema("dbo");
            this.Table("PaymentOption");
            this.ReadOnly();
            this.Id(x => x.PaymentOptionId);
            this.Map(x => x.PaymentOptionName);

            this.Cache.ReadOnly();
        }
    }
}
﻿namespace Ivh.Domain.FinanceManagement.Payment.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class PaymentVisitMap : ClassMap<PaymentVisit>
    {
        public PaymentVisitMap()
        {
            this.Schema("dbo");
            this.Table("Visit");
            this.ReadOnly();
            this.Id(x => x.VisitId);
            this.Map(x => x.VpGuarantorId);
            this.Map(x => x.CurrentBalance);
            this.Map(x => x.CurrentVisitState, "CurrentVisitStateId").CustomType<VisitStateEnum>();
            this.Map(x => x.InsertDate);
            this.Map(x => x.DischargeDate)
                .Nullable();
            this.Map(x => x.BillingApplication)
                .Not.Nullable();
            this.Map(x => x.PaymentPriority)
                .Nullable();
            
            this.Map(x => x.SourceSystemKey)
                .Nullable();
            this.Map(x => x.SourceSystemKeyDisplay)
                .Nullable();
            this.Map(x => x.BillingSystemId)
                .Nullable();
            this.Map(x => x.MatchedSourceSystemKey)
                .Nullable();
            this.Map(x => x.MatchedBillingSystemId)
                .Nullable();
            
            this.Map(x => x.AgingTier)
                //.Formula("(SELECT TOP 1 vah.AgingTier FROM dbo.VisitAgingHistory vah WHERE vah.VisitID = [ID] ORDER BY vah.InsertDate desc, vah.VisitAgingHistoryId desc)")
                .Formula("(select coalesce((SELECT vah.AgingTier FROM dbo.VisitAgingHistory vah WHERE vah.VisitAgingHistoryId = [CurrentVisitAgingHistoryId]), 0))")
                .CustomType<AgingTierEnum>()
                .LazyLoad()
                .ReadOnly();
            this.Map(x => x.BalanceTransferStatusEnum)
                .Formula("(SELECT TOP 1 btsh.BalanceTransferStatusId FROM dbo.BalanceTransferStatusHistory btsh WHERE btsh.VisitID = [VisitId] ORDER BY btsh.InsertDate desc, btsh.BalanceTransferStatusHistoryId desc)")
                .CustomType<BalanceTransferStatusEnum?>()//Not sure this will work as a nullable type.  Willing to try
                .LazyLoad()
                .ReadOnly();

            this.Map(x => x.DiscountPercentage)
                .Formula(@"(SELECT TOP 1 ISNULL(ip.DiscountPercentage, 0) FROM  VisitInsurancePlan vp
                            INNER JOIN InsurancePlan ip ON vp.InsurancePlanId = ip.InsurancePlanId
                            WHERE vp.VisitId = [VisitId]
                            ORDER BY ip.DiscountPercentage DESC)")
                .LazyLoad()
                .ReadOnly();

            this.Map(x => x.EarliestStatementDate)
                .Formula(@"(SELECT TOP 1 s.StatementDate
                            FROM VpStatementVisit sv
                            INNER JOIN VpStatement s on s.VpStatementId = sv.VpStatementId
                            WHERE sv.VisitId = [VisitId]
                            ORDER BY s.StatementDate ASC)")
                .Nullable()
                .LazyLoad()
                .ReadOnly();

            this.Map(x => x.EarliestFinancePlanStatementDate)
                .Formula(@"(SELECT TOP 1 fs.StatementDate
                            FROM FinancePlanStatementVisit fsv
                            INNER JOIN FinancePlanStatement fs on fs.FinancePlanStatementId = fsv.FinancePlanStatementId
                            WHERE fsv.VisitId = [VisitId]
                            ORDER BY fs.StatementDate ASC)")
                .Nullable()
                .LazyLoad()
                .ReadOnly();

            this.Map(x => x.ServiceGroupId).Nullable();
            this.Map(x => x.VpEligible).ReadOnly();
            this.Map(x => x.BillingHold).ReadOnly();
            this.Map(x => x.Redact).ReadOnly();
            this.Map(x => x.IsUnmatched).ReadOnly();
        }
    }
}

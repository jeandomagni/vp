﻿namespace Ivh.Domain.FinanceManagement.Payment.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class PaymentProcessorResponseMap : ClassMap<PaymentProcessorResponse>
    {
        public PaymentProcessorResponseMap()
        {
            this.Schema("dbo");
            this.Table("PaymentProcessorResponse");
            this.Id(x => x.PaymentProcessorResponseId);
            this.Cache.ReadWrite();
            this.Map(x => x.InsertDate);
            this.Map(x => x.PaymentProcessorSourceKey).Length(200);
            this.Map(x => x.PaymentProcessorResponseStatus).Column("PaymentProcessorResponseStatusId").CustomType<PaymentProcessorResponseStatusEnum>();
            this.Map(x => x.PaymentSystemType).Column("PaymentSystemTypeId").CustomType<PaymentSystemTypeEnum>().Not.Nullable();
            this.Map(x => x.RawResponse);
            this.Map(x => x.SnapshotTotalPaymentAmount);

            this.Map(x => x.ResponseField1).Length(250);
            this.Map(x => x.ResponseField2).Length(250);
            this.Map(x => x.ResponseField3).Length(250);
            this.Map(x => x.ResponseField4).Length(250);
            this.Map(x => x.ResponseField5).Length(250);
            this.Map(x => x.ResponseField6).Length(250);
            this.Map(x => x.ResponseField7).Length(250);
            this.Map(x => x.ResponseField8).Length(250);
            this.Map(x => x.ResponseField9).Length(250);
            this.Map(x => x.ResponseField10).Length(250);
            this.Map(x => x.ErrorMessage).Length(250);

            this.References(x => x.PaymentProcessor)
                .Column("PaymentProcessorId");
            this.References(x => x.PaymentMethod)
                .Column("PaymentMethodId");
            this.HasMany(x => x.PaymentPaymentProcessorResponses)
                .BatchSize(50)
                .KeyColumn("PaymentProcessorResponseId")
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Inverse()
                .Cascade.All();
        }
    }
}
﻿namespace Ivh.Domain.FinanceManagement.Payment.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class PaymentStatusMap : ClassMap<PaymentStatus>
    {
        public PaymentStatusMap()
        {
            this.Schema("dbo");
            this.Table("PaymentStatus");
            this.ReadOnly();
            this.Id(x => x.PaymentStatusId);
            this.Map(x => x.PaymentStatusName);
            this.Map(x => x.PaymentStatusDisplayName);
            this.Map(x => x.DisplayOrder);

            this.Cache.ReadOnly();
        }
    }
}
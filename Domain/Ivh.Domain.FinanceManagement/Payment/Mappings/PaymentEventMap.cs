﻿namespace Ivh.Domain.FinanceManagement.Payment.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class PaymentEventMap : ClassMap<PaymentEvent>
    {
        public PaymentEventMap()
        {
            this.Schema("dbo");
            this.Table("PaymentEvent");
            this.Id(x => x.PaymentEventId);
            this.Map(x => x.InsertDate);
            this.Map(x => x.PaymentStatus).Column("PaymentStatusId").CustomType<PaymentStatusEnum>();
            this.References(x => x.Payment).Column("PaymentId");
            this.Map(x => x.VisitPayUserId);
            this.Map(x => x.VpGuarantorId);
            this.Map(x => x.Comment).Nullable();
        }
    }
}
﻿namespace Ivh.Domain.FinanceManagement.Payment.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;
    using Discount.Entities;
    using FinanceManagement.Entities;
    using Guarantor.Entities;

    public class Payment
    {
        public Payment()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        private IList<PaymentActionHistory> _paymentActionHistories;
        private IList<PaymentAllocation> _paymentAllocations;
        private IList<PaymentPaymentProcessorResponse> _paymentPaymentProcessorResponses;
        private IList<PaymentScheduledAmount> _paymentScheduledAmounts;
        private IList<PaymentStatusHistory> _paymentStatusHistories;
        private IList<Payment> _reversalPayments;
        private DateTime? _originalPaymentDueDate;
        
        public virtual int PaymentId { get; set; }

        public virtual Guarantor Guarantor { get; set; }

        public virtual DateTime InsertDate { get; set; }

        public virtual PaymentTypeEnum PaymentType { get; set; }

        public virtual PaymentStatusEnum CurrentPaymentStatus { get; set; } = PaymentStatusEnum.ActivePending;

        public virtual PaymentStatusEnum PaymentStatus
        {
            get => this.CurrentPaymentStatus;
            set => this.SetPaymentStatus(value, "");
        }

        public virtual IList<PaymentStatusHistory> PaymentStatusHistories
        {
            get => this._paymentStatusHistories ?? (this.PaymentStatusHistories = new List<PaymentStatusHistory>());
            set => this._paymentStatusHistories = value;
        }

        public virtual bool IsActive => this.PaymentStatus == PaymentStatusEnum.ActiveGatewayError ||
                                        this.PaymentStatus == PaymentStatusEnum.ActivePending ||
                                        this.PaymentStatus == PaymentStatusEnum.ActivePendingACHApproval ||
                                        this.PaymentStatus == PaymentStatusEnum.ActivePendingGuarantorAction ||
                                        this.PaymentStatus == PaymentStatusEnum.PaymentPendingGuarantorResponse ||
                                        this.PaymentStatus == PaymentStatusEnum.ActivePendingLinkFailure;

        public virtual bool IsClosed => this.PaymentStatus == PaymentStatusEnum.ClosedCancelled || 
                                        this.PaymentStatus == PaymentStatusEnum.ClosedFailed || 
                                        this.PaymentStatus == PaymentStatusEnum.ClosedPaid;

        public virtual PaymentMethod PaymentMethod { get; set; }

        public virtual int PaymentAttemptCount { get; set; }

        public virtual decimal ScheduledPaymentAmount
        {
            get { return this.PaymentScheduledAmounts.Sum(x => x.ScheduledAmount); }
        }

        public virtual IList<PaymentScheduledAmount> PaymentScheduledAmounts
        {
            get => this._paymentScheduledAmounts ?? (this.PaymentScheduledAmounts = new List<PaymentScheduledAmount>());
            set => this._paymentScheduledAmounts = value;
        }

        public virtual decimal ActualPaymentAmount { get; set; }

        public virtual DateTime? OriginalScheduledPaymentDate
        {
            get => this._originalPaymentDueDate ?? this.ScheduledPaymentDate;
            set => this._originalPaymentDueDate = value;
        }

        public virtual DateTime ScheduledPaymentDate { get; set; }

        public virtual DateTime? ActualPaymentDate { get; set; }

        public virtual Payment ParentPayment { get; set; }

        public virtual MerchantAccount MerchantAccount { get; set; }

        public virtual int MerchantAccountId => this.MerchantAccount?.MerchantAccountId ?? 0;

        public virtual Guid? SplitPaymentGroupingGuid { get; set; }
        
        public virtual decimal ScheduledDiscountPercent
        {
            get => this.DiscountOffer?.DiscountTotalPercent ?? 0m;
            set { }
        }

        public virtual IList<PaymentAllocation> PaymentAllocations
        {
            get => this._paymentAllocations ?? (this.PaymentAllocations = new List<PaymentAllocation>());
            set => this._paymentAllocations = value;
        }

        public virtual IList<PaymentAllocation> PaymentAllocationsAll
        {
            get
            {
                List<PaymentAllocation> allocations = new List<PaymentAllocation>();
                allocations.AddRange(this.PaymentAllocations);
                foreach (Payment reversalPayment in this.ReversalPayments)
                {
                    allocations.AddRange(reversalPayment.PaymentAllocations);
                }
                return allocations;
            }
        }

        public virtual IList<PaymentProcessorResponse> PaymentProcessorResponses
        {
            get { return this.PaymentPaymentProcessorResponses.Select(x => x.PaymentProcessorResponse).ToList(); }
        }

        public virtual DateTime? LastPaymentFailureDate
        {
            get
            {
                PaymentProcessorResponse lastFailed = this.PaymentProcessorResponses.OrderByDescending(x => x.InsertDate).FirstOrDefault(x => !x.WasSuccessful());
                return lastFailed?.InsertDate;
            }
        }

        public virtual IList<PaymentPaymentProcessorResponse> PaymentPaymentProcessorResponses
        {
            get => this._paymentPaymentProcessorResponses ?? (this.PaymentPaymentProcessorResponses = new List<PaymentPaymentProcessorResponse>());
            protected set => this._paymentPaymentProcessorResponses = value;
        }

        public virtual string TransactionId
        {
            get { return this.PaymentProcessorResponses.OrderByDescending(x => x.InsertDate).Select(x => x.PaymentProcessorSourceKey).FirstOrDefault(); }
        }

        public virtual DiscountOffer DiscountOffer { get; set; }
        
        public virtual decimal PaymentAllocationsUnallocatedAmountSum
        {
            get { return this.PaymentAllocations.Sum(x => x.UnallocatedAmount); }
        }

        public virtual IList<Payment> ReversalPayments
        {
            get => this._reversalPayments ?? (this.ReversalPayments = new List<Payment>());
            set => this._reversalPayments = value;
        }

        public virtual decimal NetPaymentAmount
        {
            get { return this.ActualPaymentAmount + this.ReversalPayments.Sum(x => x.ActualPaymentAmount); } // these should be negative numbers
        }

        public virtual bool? IsGuarantorCure { get; set; }

        public virtual bool? IsHouseholdBalance { get; set; }

        private void SetPaymentStatus(PaymentStatusEnum value, string reason)
        {
            if (this.PaymentStatusHistories.IsNotNullOrEmpty() && this.CurrentPaymentStatus == value)
            {
                return;
            }

            this.PaymentStatusHistories.Add(new PaymentStatusHistory
            {
                ChangeDescription = reason,
                InsertDate = DateTime.UtcNow,
                Payment = this,
                PaymentStatus = value
            });
            this.CurrentPaymentStatus = value;
        }

        public virtual void AddPaymentScheduledAmount(PaymentScheduledAmount psa)
        {
            if (psa.ScheduledAmount != Math.Round(psa.ScheduledAmount, 2))
            {
                psa.ScheduledAmount = Math.Round(psa.ScheduledAmount, 2);
            }

            psa.Payment = this;
            this.PaymentScheduledAmounts.Add(psa);
        }

        public virtual IList<PaymentActionHistory> PaymentActionHistories
        {
            get => this._paymentActionHistories ?? (this.PaymentActionHistories = new List<PaymentActionHistory>());
            set => this._paymentActionHistories = value;
        }

        public virtual bool IsManuallyRescheduled()
        {
            if (this.PaymentActionHistories == null || !this.PaymentActionHistories.Any())
            {
                return false;
            }

            return this.PaymentActionHistories.OrderByDescending(x => x.InsertDate).ThenByDescending(x => x.PaymentActionHistoryId).First().PaymentActionType == PaymentActionTypeEnum.Reschedule;
        }
        
        public virtual bool IsManuallyRescheduledToEarlierDate()
        {
            return this.IsManuallyRescheduled() &&
                   DateTime.Compare(this.ScheduledPaymentDate, this.OriginalScheduledPaymentDate.GetValueOrDefault(this.ScheduledPaymentDate)) < 0;
        }
        
        public virtual int? CreatedVisitPayUserId { get; set; }

        #region PaymentType Helpers

        public virtual bool IsCurrentBalanceInFullType()
        {
            return this.PaymentType == PaymentTypeEnum.ManualPromptCurrentBalanceInFull ||
                   this.PaymentType == PaymentTypeEnum.ManualScheduledCurrentBalanceInFull;
        }

        public virtual bool IsCurrentNonFinancedBalanceInFullType()
        {
            return this.PaymentType.IsInCategory(PaymentTypeEnumCategory.CurrentNonFinancedBalanceInFull);
        }

        public virtual bool IsSpecificAmountType()
        {
            return this.PaymentType == PaymentTypeEnum.ManualPromptSpecificAmount || 
                   this.PaymentType == PaymentTypeEnum.ManualScheduledSpecificAmount;
        }

        public virtual bool IsSpecificFinancePlansType()
        {
            return this.PaymentType == PaymentTypeEnum.ManualPromptSpecificFinancePlans || 
                   this.PaymentType == PaymentTypeEnum.ManualScheduledSpecificFinancePlans || 
                   this.PaymentType == PaymentTypeEnum.ManualPromptSpecificFinancePlansBucketZero || 
                   this.PaymentType == PaymentTypeEnum.LockBoxManualPromptSpecificFinancePlansBucketZero || 
                   this.PaymentType == PaymentTypeEnum.RecurringPaymentFinancePlan;
        }

        public virtual bool IsSpecificVisitsType()
        {
            return this.PaymentType == PaymentTypeEnum.ManualPromptSpecificVisits || 
                   this.PaymentType == PaymentTypeEnum.ManualScheduledSpecificVisits;
        }

        public virtual bool IsPromptPayType()
        {
            return this.PaymentType == PaymentTypeEnum.ManualPromptCurrentBalanceInFull || 
                   this.PaymentType == PaymentTypeEnum.ManualPromptSpecificAmount || 
                   this.PaymentType == PaymentTypeEnum.ManualPromptSpecificFinancePlans || 
                   this.PaymentType == PaymentTypeEnum.ManualPromptSpecificVisits || 
                   this.PaymentType == PaymentTypeEnum.ManualPromptSpecificFinancePlansBucketZero || 
                   this.PaymentType == PaymentTypeEnum.LockBoxManualPromptSpecificFinancePlansBucketZero ||
                   this.PaymentType == PaymentTypeEnum.ManualPromptCurrentNonFinancedBalanceInFull;
        }

        public virtual bool IsTextToPayType()
        {
            bool isTextToPayType = this.PaymentStatusHistories.Any(x => x.PaymentStatus == PaymentStatusEnum.PaymentPendingGuarantorResponse);
            return isTextToPayType || this.CurrentPaymentStatus == PaymentStatusEnum.PaymentPendingGuarantorResponse;
        }

        public virtual bool IsRecurringPayment()
        {
            return this.PaymentType == PaymentTypeEnum.RecurringPaymentFinancePlan;
        }

        public virtual bool IsNotRecurringPayment()
        {
            return !this.IsRecurringPayment();
        }

        public virtual bool AllowToPayBucketZero()
        {
            return this.IsRecurringPayment() ||
                   this.PaymentType == PaymentTypeEnum.ManualPromptSpecificFinancePlansBucketZero ||
                   this.PaymentType == PaymentTypeEnum.LockBoxManualPromptSpecificFinancePlansBucketZero;
        }

        public virtual bool IsReversalPaymentType()
        {
            return this.PaymentType == PaymentTypeEnum.Refund || 
                   this.PaymentType == PaymentTypeEnum.PartialRefund || 
                   this.PaymentType == PaymentTypeEnum.Void;
        }

        public virtual bool IsRefundType()
        {
            return this.PaymentType == PaymentTypeEnum.Refund ||
                   this.PaymentType == PaymentTypeEnum.PartialRefund;
        }

        public virtual bool IsVoidType()
        {
            return this.PaymentType == PaymentTypeEnum.Void;
        }

        public virtual bool WasSuccessful()
        {
            PaymentStatusEnum paymentStatus = this.PaymentStatus;

            return paymentStatus == PaymentStatusEnum.ClosedPaid || 
                   paymentStatus == PaymentStatusEnum.ClosedCancelled || 
                   paymentStatus == PaymentStatusEnum.ActivePendingACHApproval;
        }

        public virtual bool IsPendingGuarantorAction()
        {
            return this.PaymentStatus == PaymentStatusEnum.ActivePendingGuarantorAction;
        }

        public virtual bool IsPendingAchApproval()
        {
            return this.PaymentStatus == PaymentStatusEnum.ActivePendingACHApproval;
        }

        public virtual bool HadSystemError()
        {
            return this.PaymentStatus == PaymentStatusEnum.ActiveGatewayError;
        }

        public virtual bool IsScheduledPayType()
        {
            return !this.IsPromptPayType();
        }

        public virtual bool IsLinkFailure()
        {
            return this.PaymentStatus == PaymentStatusEnum.ActivePendingLinkFailure;
        }

        #endregion
    }
}
 
﻿using System;
using System.Collections.Generic;
using Ivh.Common.Base.Enums;

namespace Ivh.Domain.FinanceManagement.Payment.Entities
{
    using Common.VisitPay.Enums;

    public class PaymentFilter
    {
        public PaymentFilter()
        {
            this.VpGuarantorId = new List<int>();
        }

        /// <summary>
        /// Payment Made For (Payment.VpGuarantorId)
        /// </summary>
        public IList<int> VpGuarantorId { get; set;}
        public DateTime? PaymentDateRangeFrom { get; set; }
        public DateTime? PaymentDateRangeTo { get; set; }
        public DateTime? ScheduledPaymentDateRangeFrom { get; set; }
        public DateTime? ScheduledPaymentDateRangeTo { get; set; }
        public int? PaymentMethodId { get; set; }
        public IList<PaymentStatusEnum> PaymentStatus { get; set; }
        public int? PaymentTypeId { get; set; }
        public string TransactionId { get; set; }
        public int? TransactionRange { get; set; }
        public string PaymentId { get; set; }
        public string SortField { get; set; }
        public string SortOrder { get; set; }
        public int? MadeByVpGuarantorId { get; set; }
    }
}

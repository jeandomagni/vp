﻿namespace Ivh.Domain.FinanceManagement.Payment.Entities
{
    public class PaymentActionReason
    {
        public virtual int PaymentActionReasonId { get; set; }

        public virtual string Description { get; set; }

        public virtual bool IsActive { get; set; }

        public virtual bool IsCancel { get; set; }

        public virtual bool IsReschedule { get; set; }

        public virtual bool IsManual { get; set; }

        public virtual bool IsRecurring { get; set; }

        public virtual bool RequireInput { get; set; }

        public virtual int DisplayOrder { get; set; }
    }
}
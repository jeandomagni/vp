namespace Ivh.Domain.FinanceManagement.Payment.Entities
{
    using System;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class PaymentStatusHistory
    {
        public PaymentStatusHistory()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int PaymentStatusHistoryId { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual PaymentStatusEnum PaymentStatus { get; set; }
        public virtual string ChangeDescription { get; set; }
        public virtual Payment Payment { get; set; }
    }
}
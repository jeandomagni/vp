﻿namespace Ivh.Domain.FinanceManagement.Payment.Entities
{
    using System;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class PaymentEvent
    {
        public PaymentEvent()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int PaymentEventId { get; set; }
        public virtual PaymentStatusEnum PaymentStatus { get; set; }
        public virtual Payment Payment { get; set; }
        public virtual int VpGuarantorId { get; set; }
        public virtual int VisitPayUserId { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual string Comment { get; set; }
    }
}
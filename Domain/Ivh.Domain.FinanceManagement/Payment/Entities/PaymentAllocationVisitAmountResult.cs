﻿namespace Ivh.Domain.FinanceManagement.Payment.Entities
{
    using System;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class PaymentAllocationVisitAmountResult
    {
        public PaymentAllocationVisitAmountResult()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        public int VisitId { get; set; }
        public decimal ActualAmount { get; set; }
        public DateTime InsertDate { get; set; }
        public PaymentAllocationTypeEnum PaymentAllocationType { get; set; }
        public int PaymentAllocationId { get; set; }

    }
}
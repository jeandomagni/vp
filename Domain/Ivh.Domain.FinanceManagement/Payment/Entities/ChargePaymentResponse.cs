﻿namespace Ivh.Domain.FinanceManagement.Payment.Entities
{
    public class ChargePaymentResponse 
    {
        public ChargePaymentResponse(bool success, Payment payment)
        {
            this.Success = success;
            this.Payment = payment;
        }

        public bool Success { get; private set; }
        public Payment Payment { get; private set; }
    }
}
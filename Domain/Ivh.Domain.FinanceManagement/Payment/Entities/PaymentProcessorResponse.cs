﻿namespace Ivh.Domain.FinanceManagement.Payment.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using FinanceManagement.Entities;

    public class PaymentProcessorResponse
    {
        public PaymentProcessorResponse()
        {
            this.InsertDate = DateTime.UtcNow;
            this.PaymentPaymentProcessorResponses = new List<PaymentPaymentProcessorResponse>();
        }

        public virtual int PaymentProcessorResponseId { get; set; }
        public virtual IList<PaymentPaymentProcessorResponse> PaymentPaymentProcessorResponses { get; protected set; }

        public virtual IList<Payment> Payments
        {
            get { return this.PaymentPaymentProcessorResponses.Select(x => x.Payment).ToList(); }
        }

        public virtual DateTime InsertDate { get; set; }
        public virtual string PaymentProcessorSourceKey { get; set; }
        public virtual PaymentProcessor PaymentProcessor { get; set; }
        public virtual PaymentProcessorResponseStatusEnum PaymentProcessorResponseStatus { get; set; }
        public virtual PaymentMethod PaymentMethod { get; set; }
        public virtual PaymentSystemTypeEnum PaymentSystemType { get; set; }
        public virtual decimal SnapshotTotalPaymentAmount { get; set; }
        public virtual string RawResponse { get; set; }
        public virtual string ResponseField1 { get; set; }
        public virtual string ResponseField2 { get; set; }
        public virtual string ResponseField3 { get; set; }
        public virtual string ResponseField4 { get; set; }
        public virtual string ResponseField5 { get; set; }
        public virtual string ResponseField6 { get; set; }
        public virtual string ResponseField7 { get; set; }
        public virtual string ResponseField8 { get; set; }
        public virtual string ResponseField9 { get; set; }
        public virtual string ResponseField10 { get; set; }
        public virtual string ErrorMessage { get; set; }

        public virtual bool WasSuccessful()
        {
            return (this.PaymentProcessorResponseStatus == PaymentProcessorResponseStatusEnum.Accepted ||
                    this.PaymentProcessorResponseStatus == PaymentProcessorResponseStatusEnum.Approved);
        }

        public virtual bool HadSystemError()
        {
            return (this.PaymentProcessorResponseStatus == PaymentProcessorResponseStatusEnum.CallToProcessorFailed);
        }
        
    }
}
﻿namespace Ivh.Domain.FinanceManagement.Payment.Entities
{
    using System.Collections.Generic;

    public class PaymentMenuItem
    {
        public virtual int PaymentMenuItemId { get; set; }

        public virtual PaymentOption PaymentOption { get; set; }

        public virtual int CmsRegionId { get; set; }

        public virtual int DisplayOrder { get; set; }

        public virtual string IconCssClass { get; set; }

        public virtual PaymentMenuItem ParentPaymentMenuItem { get; set; }

        public virtual IList<PaymentMenuItem> PaymentMenuItems { get; set; }
    }
}
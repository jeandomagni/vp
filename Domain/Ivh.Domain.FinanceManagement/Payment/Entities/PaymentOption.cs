﻿namespace Ivh.Domain.FinanceManagement.Payment.Entities
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class PaymentOption
    {
        public virtual PaymentOptionEnum PaymentOptionId { get; set; }

        public virtual string PaymentOptionName { get; set; }

        public virtual CmsRegionEnum? CmsRegionId 
        {
            get
            {
                switch (this.PaymentOptionId)
                {
                    case PaymentOptionEnum.AccountBalance:
                        return CmsRegionEnum.PaymentOptionAccountBalanceDescription;

                    case PaymentOptionEnum.CurrentNonFinancedBalance:
                        return null;

                    case PaymentOptionEnum.FinancePlan:
                        return null;

                    case PaymentOptionEnum.HouseholdCurrentNonFinancedBalance:
                        return CmsRegionEnum.PaymentOptionHouseholdCurrentNonFinancedBalanceDescription;

                    case PaymentOptionEnum.Resubmit:
                        return CmsRegionEnum.PaymentOptionResubmitDescription;

                    case PaymentOptionEnum.SpecificAmount:
                        return CmsRegionEnum.PaymentOptionSpecificAmountDescription;

                    case PaymentOptionEnum.SpecificFinancePlans:
                        return CmsRegionEnum.PaymentOptionSpecificFinancePlansDescription;

                    case PaymentOptionEnum.SpecificVisits:
                        return CmsRegionEnum.PaymentOptionSpecificVisitsDescription;
                }

                return null;
            }
        }

        public virtual PaymentDateTypeEnum? DateBoundaryType
        {
            get
            {
                switch (this.PaymentOptionId)
                {
                    case PaymentOptionEnum.FinancePlan:
                        return null;
                        
                    case PaymentOptionEnum.AccountBalance:
                    case PaymentOptionEnum.CurrentNonFinancedBalance:
                    case PaymentOptionEnum.HouseholdCurrentNonFinancedBalance:
                    case PaymentOptionEnum.SpecificAmount:
                    case PaymentOptionEnum.SpecificFinancePlans:
                    case PaymentOptionEnum.SpecificVisits:
                        return PaymentDateTypeEnum.ClientSetting;

                    case PaymentOptionEnum.Resubmit:
                    default:
                        return PaymentDateTypeEnum.Today;
                }
            }
        }

        public virtual PaymentDateTypeEnum DefaultPaymentDateType => PaymentDateTypeEnum.Today;

        public virtual bool IsAmountReadonly
        {
            get
            {
                switch (this.PaymentOptionId)
                {
                    case PaymentOptionEnum.FinancePlan:
                    case PaymentOptionEnum.SpecificAmount:
                        return false;
                    default:
                        return true;
                }
            }
        }

        public virtual bool IsDateReadonly
        {
            get
            {
                switch (this.PaymentOptionId)
                {
                    case PaymentOptionEnum.Resubmit:
                        return true;
                    default:
                        return false;
                }
            }
        }

        public virtual bool IsExclusive => false;

        public virtual bool IsVisibleWhenNotEnabled
        {
            get
            {
                switch (this.PaymentOptionId)
                {
                    case PaymentOptionEnum.FinancePlan:
                        return true;
                    default:
                        return false;
                }
            }
        }
    }
}

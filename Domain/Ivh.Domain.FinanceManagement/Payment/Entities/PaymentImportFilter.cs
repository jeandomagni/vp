﻿
namespace Ivh.Domain.FinanceManagement.Payment.Entities
{
    using Common.VisitPay.Enums;
    using System;

    public class PaymentImportFilter
    {
        public DateTime? DateRangeFrom { get; set; }
        public DateTime? DateRangeTo { get; set; }
        public PaymentImportStateEnum? PaymentImportState { get; set; }
        public string BatchNumber { get; set; }
    }
}

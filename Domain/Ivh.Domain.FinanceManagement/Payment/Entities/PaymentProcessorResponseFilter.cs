﻿namespace Ivh.Domain.FinanceManagement.Payment.Entities
{
    using System;
    using System.Collections.Generic;
    using Common.VisitPay.Enums;

    public class PaymentProcessorResponseFilter
    {
        public int? InsertDateRange { get; set; }
        public DateTime? InsertDateRangeFrom { get; set; }
        public DateTime? InsertDateRangeTo { get; set; }
        public int? PaymentMethodId { get; set; }
        public string PaymentProcessorResponseId { get; set; }
        public string PaymentProcessorSourceKey { get; set; }
        public IList<PaymentStatusEnum> PaymentStatus { get; set; }
        public IList<PaymentTypeEnum> PaymentType { get; set; }
        public IList<int> VpGuarantorId { get; set; }
        public int? MadeByVpGuarantorId { get; set; }

        public string SortField { get; set; }
        public string SortOrder { get; set; }
    }
}

namespace Ivh.Domain.FinanceManagement.Payment.Entities
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Utilities.Extensions;

    public class PaymentAllocationResult
    {
        public List<PaymentAllocation> Allocations { get; set; } = new List<PaymentAllocation>();
        public DateTime? ActualPaymentDate = null;
        public decimal ActualPaymentAmount;
        public Payment Payment;

        public void CommitAllocationsToPayment()
        {
            if (this.Payment != null)
            {
                if (this.Payment.PaymentAllocations != null)
                {
                    this.Payment.PaymentAllocations.Clear();
                    this.Payment.PaymentAllocations.AddRange(this.Allocations);
                }
                this.Payment.ActualPaymentAmount = this.ActualPaymentAmount;
                this.Payment.ActualPaymentDate = this.ActualPaymentDate;
            }

        }
    }
}
﻿namespace Ivh.Domain.FinanceManagement.Payment.Entities
{
    using System;

    public class PaymentPaymentProcessorResponse
    {
        public PaymentPaymentProcessorResponse()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int PaymentPaymentProcessorResponseId { get; set; }
        public virtual PaymentProcessorResponse PaymentProcessorResponse { get; set; }
        public virtual Payment Payment { get; set; }
        public virtual decimal SnapshotPaymentAmount { get; set; }
        public virtual DateTime InsertDate { get; set; }
    }
}
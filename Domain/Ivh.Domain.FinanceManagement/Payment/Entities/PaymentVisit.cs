﻿namespace Ivh.Domain.FinanceManagement.Payment.Entities
{
    using Common.Base.Enums;
    using System;
    using Application.Base.Common.Interfaces.Entities.Visit;
    using Common.VisitPay.Enums;

    public class PaymentVisit : IBalanceTransferVisit, IPaymentAllocationVisit, IPaymentVisit
    {
        public PaymentVisit()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        public virtual int VisitId { get; set; }

        public virtual int VpGuarantorId { get; set; }

        public virtual decimal CurrentBalance { get; protected set; }

        public virtual VisitStateEnum CurrentVisitState { get; set; }

        public virtual DateTime InsertDate { get; set; }

        [Obsolete("Use AgingTier instead")]
        public virtual int AgingCount => (int) this.AgingTier;

        public virtual AgingTierEnum AgingTier { get; set; }

        public virtual string BillingApplication { get; set; }

        public virtual decimal DiscountPercentage { get; set; }

        public virtual BalanceTransferStatusEnum? BalanceTransferStatusEnum { get; set; }

        public virtual int? ServiceGroupId { get; set; }

        public virtual string SourceSystemKey { get; set; }

        public virtual string SourceSystemKeyDisplay { get; set; }

        public virtual string MatchedSourceSystemKey { get; set; }

        public virtual int? BillingSystemId { get; set; }

        public virtual int? MatchedBillingSystemId { get; set; }

        public virtual bool IsUnmatched { get; set; }

        public virtual bool IsMaxAge
        {
            get => this.AgingTier == AgingTierEnum.MaxAge;
            set { }
        }

        public virtual bool VpEligible { get; set; }

        public virtual bool BillingHold { get; set; }

        public virtual bool Redact { get; set; }

        public virtual DateTime? DischargeDate { get; set; }

        public virtual DateTime? EarliestStatementDate { get; set; }

        public virtual DateTime? EarliestFinancePlanStatementDate { get; set; }

        public virtual int? PaymentPriority { get; set; }
    }
}

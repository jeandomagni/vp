﻿namespace Ivh.Domain.FinanceManagement.Payment.Entities
{
    using System;
    using Common.VisitPay.Enums;

    public class PaymentAllocation
    {
        private PaymentBatchStatusEnum? _paymentBatchStatus = PaymentBatchStatusEnum.Pending;

        public PaymentAllocation()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        public virtual int PaymentAllocationId { get; set; }

        public virtual PaymentAllocationTypeEnum PaymentAllocationType { get; set; }

        public virtual PaymentVisit PaymentVisit { get; set; }

        public virtual int? FinancePlanId { get; set; }

        public virtual DateTime InsertDate { get; set; }

        public virtual decimal ActualAmount { get; set; }

        public virtual decimal OriginalAmount { get; set; }

        public virtual decimal UnallocatedAmount { get; set; }

        public virtual PaymentAllocation OriginalPaymentAllocation { get; set; }

        public virtual DateTime? DeleteDate { get; set; }

        public virtual PaymentAllocationStatusEnum PaymentAllocationStatus { get; set; }

        public virtual PaymentBatchStatusEnum PaymentBatchStatus
        {
            get => this._paymentBatchStatus ?? (this.PaymentBatchStatus = PaymentBatchStatusEnum.Pending);
            protected set => this._paymentBatchStatus = value;
        }

        public virtual DateTime? ClearedDate { get; set; }

        public virtual Payment Payment { get; set; }

        public virtual decimal MaxAllocationAmount { get; set; }

        public virtual void SetCleared()
        {
            if (this.PaymentBatchStatus != PaymentBatchStatusEnum.Cleared)
            {
                this.PaymentBatchStatus = PaymentBatchStatusEnum.Cleared;
                this.ClearedDate = DateTime.UtcNow;
            }
        }

        public virtual decimal UnclearedActualAmountAsOf(DateTime date)
        {
            DateTime clearedDate = this.ClearedDate.GetValueOrDefault(DateTime.MaxValue);

            if (this.InsertDate != clearedDate &&
                this.InsertDate <= date && clearedDate >= date)
            {
                return this.ActualAmount;
            }

            return 0;
        }
    }
}
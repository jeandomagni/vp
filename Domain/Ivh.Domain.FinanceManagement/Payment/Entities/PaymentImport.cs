﻿namespace Ivh.Domain.FinanceManagement.Payment.Entities
{
    using Guarantor.Entities;
    using System;
    using Ivh.Common.VisitPay.Enums;

    public partial class PaymentImport
    {
        public PaymentImport()
        {
            this.PaymentImportStateEnum = PaymentImportStateEnum.Pending;
        }

        public virtual int PaymentImportId { get; set; }

        public virtual Guarantor VpGuarantor { get; set; }
        public virtual Payment Payment { get; set; }
        public virtual PaymentImportStateEnum PaymentImportStateEnum { get; set; }
        public virtual string PaymentImportStateReason { get; set; }

        public virtual string LockBoxNumber { get; set; }
        public virtual DateTime BatchCreditDate { get; set; }
        public virtual string GuarantorAccountNumber { get; set; }
        public virtual string GuarantorFirstName { get; set; }
        public virtual string GuarantorLastName { get; set; }
        public virtual decimal? InvoiceAmount { get; set; }
        public virtual string BatchNumber { get; set; }
        public virtual string PaymentSequenceNumber { get; set; }
        public virtual string CheckPaymentNumber { get; set; }
        public virtual string CheckAccountNumber { get; set; }
        public virtual string CheckRouting { get; set; }
        public virtual string CheckRemitter { get; set; }
        public virtual string PsrId { get; set; }
        public virtual string PaymentComment { get; set; }
        public virtual string EmailAddress { get; set; }
        public virtual string DraftReturnCode { get; set; }
        public virtual string DraftReturnInfo { get; set; }
        public virtual string RdfiBankName { get; set; }
        public virtual string ClientUserNotes { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.FinanceManagement.Payment.Entities
{
    using Settings.Entities;
    using Settings.Interfaces;

    public class MerchantAccount
    {
        public virtual int MerchantAccountId { get; set; }
        public virtual string Description { get; set; }
        public virtual string Account { get; set; }
        public virtual string CustomerIdSettingsKey { get; set; }
        public virtual string PasswordSettingsKey { get; set; }
        public virtual string AchCustomerIdSettingsKey { get; set; }
        public virtual string AchPasswordSettingsKey { get; set; }
        public virtual string PasswordQueryApiSettingsKey { get; set; }
        public virtual MerchantAccount MasterMerchantAccount { get; set; }
    }
}

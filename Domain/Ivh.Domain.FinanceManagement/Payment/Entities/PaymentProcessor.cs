﻿namespace Ivh.Domain.FinanceManagement.Payment.Entities
{
    public class PaymentProcessor
    {
        public virtual int PaymentProcessorId { get; set; }
        public virtual string PaymentProcessorName { get; set; }
        public virtual string ResponseField1Name { get; set; }
        public virtual string ResponseField2Name { get; set; }
        public virtual string ResponseField3Name { get; set; }
        public virtual string ResponseField4Name { get; set; }
        public virtual string ResponseField5Name { get; set; }
        public virtual string ResponseField6Name { get; set; }
        public virtual string ResponseField7Name { get; set; }
        public virtual string ResponseField8Name { get; set; }
        public virtual string ResponseField9Name { get; set; }
        public virtual string ResponseField10Name { get; set; }
    }
}
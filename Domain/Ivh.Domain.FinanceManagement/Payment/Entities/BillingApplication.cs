﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.FinanceManagement.Payment.Entities
{
    public class BillingApplication
    {
        public BillingApplication(string name, decimal percent, int priority)
        {
            this.Name = name;
            this.Percent = percent;
            this.Priority = priority;
        }

        public string Name { get; private set; }
        public decimal Percent { get; private set; }
        public int Priority { get; private set; }
    }
}

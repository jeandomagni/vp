﻿namespace Ivh.Domain.FinanceManagement.Payment.Entities
{
    using System;

    public class PaymentScheduledAmount
    {
        public PaymentScheduledAmount()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int PaymentScheduledAmountId { get; set; }
        public virtual DateTime InsertDate { get; set; }       
        public virtual decimal ScheduledAmount { get; set; }
        public virtual decimal ScheduledPrincipal { get; set; }
        public virtual decimal ScheduledInterest { get; set; }
        public virtual Payment Payment { get; set; }

        public virtual int? VisitId { get; set; }
        private PaymentVisit _paymentVisit;
        /// <summary>
        /// Read only abstraction from the database, will also set VisitID to persist related Visit in database
        /// </summary>
        public virtual PaymentVisit PaymentVisit
        {
            get
            {
                return _paymentVisit;
            }
            set
            {
                _paymentVisit = value;
                this.VisitId = value?.VisitId;
            }
        }

        public virtual int? FinancePlanId { get; set; }
        private PaymentFinancePlan _paymentFinancePlan;
        /// <summary>
        /// Read only abstraction from the database, will also set FinancePlanId to persist related FinancePlan in database
        /// </summary>
        public virtual PaymentFinancePlan PaymentFinancePlan
        {
            get
            {
                return _paymentFinancePlan;
            }
            set
            {
                _paymentFinancePlan = value;
                this.FinancePlanId = value?.FinancePlanId;
            }
        }
    }
}
﻿namespace Ivh.Domain.FinanceManagement.Payment.Entities
{
    using System;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class PaymentFinancePlan
    {
        public virtual int FinancePlanId { get; set; }
        public virtual int CurrentBucket { get; set; }
        public virtual FinancePlanStatusEnum FinancePlanStatus { get; set; }
        public virtual decimal CurrentFinancePlanBalance { get; set; }
        public virtual DateTime? OriginationDate { get; set; }
    }
}
﻿namespace Ivh.Domain.FinanceManagement.Payment.Entities
{
    using System.Collections.Generic;

    public class PaymentMenu
    {
        public virtual int PaymentMenuId { get; set; }
        public virtual string PaymentMenuName { get; set; }
        public virtual IList<PaymentMenuItem> PaymentMenuItems { get; set; }
    }
}
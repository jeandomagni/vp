﻿namespace Ivh.Domain.FinanceManagement.Payment.Entities
{
    using System;

    public class PaymentFinancePlanVisit
    {
        public virtual int FinancePlanId { get; set; }
        public virtual int FinancePlanVisitId { get; set; }
        public virtual int VisitId { get; set; }
        public virtual DateTime? HardRemoveDate { get; set; }
        public virtual decimal InterestDue { get; set; }
    }
}
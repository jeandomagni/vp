﻿namespace Ivh.Domain.FinanceManagement.Payment.Entities
{
    using Ivh.Domain.Visit.Entities;
    public class BalanceTransferStatusMerchantAccount
    {
        public virtual int BalanceTransferStatusMerchantAccountId { get; set; }
        public virtual BalanceTransferStatus BalanceTransferStatus { get; set; }
        public virtual MerchantAccount MerchantAccount { get; set; }
    }
}

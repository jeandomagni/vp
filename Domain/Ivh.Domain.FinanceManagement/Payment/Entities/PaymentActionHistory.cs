namespace Ivh.Domain.FinanceManagement.Payment.Entities
{
    using System;
    using Common.VisitPay.Enums;
    using Ivh.Domain.User.Entities;

    public class PaymentActionHistory
    {
        public PaymentActionHistory()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        public virtual int PaymentActionHistoryId { get; set; }

        public virtual PaymentActionTypeEnum PaymentActionType { get; set; }

        public virtual Guid CorrelationGuid { get; set; }

        public virtual DateTime InsertDate { get; set; }

        public virtual DateTime? PaymentRescheduledDate { get; set; }

        public virtual string Description { get; set; }
        
        public virtual Payment Payment { get; set; }

        public virtual int? FinancePlanId { get; set; }

        public virtual int VpGuarantorId { get; set; }

        public virtual PaymentActionReason PaymentActionReason { get; set; }

        public virtual VisitPayUser ActionVisitPayUser { get; set; }
    }
}
﻿namespace Ivh.Domain.FinanceManagement.Payment.Entities
{
    using Ivh.Common.VisitPay.Enums;

    public partial class PaymentImport
    {
        public virtual bool CanPost => this.PaymentImportStateEnum != PaymentImportStateEnum.Success
                    && this.PaymentImportStateEnum != PaymentImportStateEnum.Cancelled;

        public virtual bool IsEditable => this.PaymentImportStateEnum != PaymentImportStateEnum.Success
            && this.PaymentImportStateEnum != PaymentImportStateEnum.Pending;
    }
}

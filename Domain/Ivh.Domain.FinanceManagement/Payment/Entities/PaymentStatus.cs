﻿namespace Ivh.Domain.FinanceManagement.Payment.Entities
{
    using Common.VisitPay.Enums;

    public class PaymentStatus
    {
        public virtual int PaymentStatusId { get; set; }
        public virtual string PaymentStatusName { get; set; }
        public virtual string PaymentStatusDisplayName { get; set; }
        public virtual int DisplayOrder { get; set; }

        public virtual PaymentStatusEnum PaymentStatusEnum => (PaymentStatusEnum) this.PaymentStatusId;
    }
}
﻿namespace Ivh.Domain.FinanceManagement.Payment.Entities
{
    using System;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class QueryPaymentTransaction
    {
        public virtual string TransactionId { get; set; }
        public virtual string RefTransid { get; set; }
        public virtual string CustId { get; set; }
        public virtual DateTime TransactionDateTime { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual PaymentTypeEnum PaymentType { get; set; }
        public virtual PaymentProcessorResponseStatusEnum PaymentProcessorResponseStatus { get; set; }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = 13;
                hashCode = (hashCode * 397) ^ (this.TransactionId.GetHashCode());
                hashCode = (hashCode * 397) ^ (this.RefTransid.GetHashCode());
                hashCode = (hashCode * 397) ^ (this.CustId.GetHashCode());
                hashCode = (hashCode * 397) ^ (this.TransactionDateTime.GetHashCode());
                hashCode = (hashCode * 397) ^ (this.Amount.GetHashCode());
                hashCode = (hashCode * 397) ^ (this.PaymentType.GetHashCode());
                hashCode = (hashCode * 397) ^ (this.PaymentProcessorResponseStatus.GetHashCode());
                return hashCode;
            }
        }
    }
}

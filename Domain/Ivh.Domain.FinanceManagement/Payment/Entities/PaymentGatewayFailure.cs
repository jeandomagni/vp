﻿namespace Ivh.Domain.FinanceManagement.Payment.Entities
{
    using System;
    
    public class PaymentGatewayFailure
    {
        public virtual int PaymentId { get; set; }
        public virtual int VpGuarantorId { get; set; }

        public virtual DateTime ScheduledPaymentDate { get; set; }
        public virtual string PaymentTypeName { get; set; }
        public virtual decimal ScheduledAmount { get; set; }
        public virtual int? FinancePlanId { get; set; }
        public virtual int? VisitId { get; set; }
        public virtual DateTime LastAttempt { get; set; }
        public virtual string PaymentStatusName { get; set; }
        public virtual string Comment { get; set; }
    }
}
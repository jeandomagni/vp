﻿namespace Ivh.Domain.FinanceManagement.Payment.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;
    using ValueTypes;

    public interface IPaymentRepository : IRepository<Payment>
    {
        #region payment statuses
        
        IList<PaymentStatus> GetPaymentStatuses();

        #endregion

        #region generic payment queries
        
        Payment GetPayment(int vpGuarantorId, int paymentId);
        IReadOnlyList<Payment> GetPayments(int vpGuarantorId, IEnumerable<int> paymentIds);
        IReadOnlyList<Payment> GetPayments(int vpGuarantorId, IEnumerable<PaymentStatusEnum> paymentStatuses);
        IReadOnlyList<Payment> GetPayments(int vpGuarantorId, IEnumerable<PaymentTypeEnum> paymentTypes);
        
        #endregion

        #region scheduled payment queries
        
        IReadOnlyList<ScheduledPaymentResult> GetScheduledPaymentResults(int vpGuarantorId, int? paymentId = null);
        IReadOnlyList<Payment> GetScheduledPayments(int? vpGuarantorId, DateTime? scheduledPaymentDate);
        IReadOnlyList<Payment> GetScheduledPaymentsWithExactDate(DateTime scheduledPaymentDate);
        IList<int> GetScheduledPaymentGuarantorIds(DateTime scheduledPaymentDate);
        int GetScheduledPaymentsCountByGuarantorPaymentMethod(int vpGuarantorId);
        
        #endregion
        
        bool HasLinkFailurePayments(int vpGuarantorId);
        IReadOnlyList<Payment> GetUnsentRecurringPayments(int? vpGuarantorId, DateTime? scheduledPaymentDate);
        IList<Payment> GetPaymentsFromPaymentAllocationIds(IList<int> allocationIds);
        IList<Payment> GetPaymentsFromTransactionId(string transactionId, IList<PaymentStatusEnum> paymentStatuses = null);
        IReadOnlyList<Payment> GetPaymentsFromVisit(int visitId);
        IList<Payment> GetPaymentsSubmittedOnDateInStatus(DateTime dateTime, IList<PaymentStatusEnum> statuses);
        IList<int> GetPaymentMethodIdsAssociatedWithPayments(int vpGuarantorId, IList<PaymentStatusEnum> paymentStatuses);
        IReadOnlyList<PaymentProcessorResponse> GetPaymentHistory(PaymentProcessorResponseFilter filter);
    }
}
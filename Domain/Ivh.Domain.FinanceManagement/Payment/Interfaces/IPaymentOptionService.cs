﻿namespace Ivh.Domain.FinanceManagement.Payment.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IPaymentOptionService : IDomainService
    {
        IList<PaymentMenuItem> GetPaymentMenu(int paymentMenuId);
        IList<PaymentOption> GetPaymentOptions();
    }
}
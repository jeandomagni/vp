namespace Ivh.Domain.FinanceManagement.Payment.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IPaymentProcessorRepository : IRepository<PaymentProcessor>
    {
        PaymentProcessor GetByName(string name);
    }
}
﻿using System.Collections.Generic;
using Ivh.Domain.FinanceManagement.Payment.Entities;

namespace Ivh.Domain.FinanceManagement.Payment.Interfaces
{
    using System;
    using Common.Base.Interfaces;
    using Payment = Entities.Payment;

    public interface IPaymentEventRepository : IRepository<Entities.PaymentEvent>
    {
        IReadOnlyList<PaymentEvent> GetPaymentEvents(List<int> paymentIds, int VpGuarantorId);
    }
}
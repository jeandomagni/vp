﻿namespace Ivh.Domain.FinanceManagement.Payment.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IPaymentVisitRepository : IRepository<PaymentVisit>
    {
        IList<PaymentVisit> GetPaymentVisits(IList<int> visitIds);
        IList<PaymentVisit> GetPaymentVisitsByVpGuarantorId(int vpGuarantorId);
        IList<PaymentVisit> GetCurrentStatementPaymentVisits(int vpGuarantorId, bool excludeFinancePlanVisits);

    }
}
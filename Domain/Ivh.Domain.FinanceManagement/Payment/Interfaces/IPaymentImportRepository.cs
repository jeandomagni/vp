﻿namespace Ivh.Domain.FinanceManagement.Payment.Interfaces
{
    using System.Collections.Generic;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Domain.FinanceManagement.Payment.Entities;

    public interface IPaymentImportRepository : IRepository<PaymentImport>
    {
        IList<string> GetAllPaymentImportBatchNumbers();
        IList<PaymentImport> GetPaymentImports(PaymentImportFilter filter);
    }
}

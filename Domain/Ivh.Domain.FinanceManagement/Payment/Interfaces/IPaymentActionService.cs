﻿namespace Ivh.Domain.FinanceManagement.Payment.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IPaymentActionService : IDomainService
    {
        void AddCancelAction(IList<Payment> payments, int paymentActionReasonId, string paymentActionDescription, int actionVisitPayUserId);
        void AddCancelAction(IList<int> financePlanIds, int vpGuarantorId, int paymentActionReasonId, string paymentActionDescription, int actionVisitPayUserId);
        void AddRescheduleAction(Payment payment, int paymentActionReasonId, string paymentActionDescription, int actionVisitPayUserId);
        void AddRescheduleAction(IList<Payment> payments, int paymentActionReasonId, string paymentActionDescription, int actionVisitPayUserId);
        void AddRescheduleAction(IList<int> financePlanIds, int vpGuarantorId, int paymentActionReasonId, string paymentActionDescription, int actionVisitPayUserId);
        IList<PaymentActionReason> GetPaymentCancelReasons(PaymentTypeEnum paymentType);
        IList<PaymentActionReason> GetPaymentRescheduleReasons(PaymentTypeEnum paymentType);
        bool IsEligibleToCancel(int vpGuarantorId, int visitPayUserId, PaymentTypeEnum paymentType);
        bool IsEligibleToReschedule(int vpGuarantorId, int visitPayUserId, PaymentTypeEnum paymentType);
    }
}
﻿namespace Ivh.Domain.FinanceManagement.Payment.Interfaces
{
    using Ivh.Common.Base.Interfaces;
    using Ivh.Domain.FinanceManagement.Payment.Entities;
    using System.Linq;

    public interface IMerchantAccountRepository : IRepository<MerchantAccount>
    {
        IQueryable<MerchantAccount> GetAll(bool includeMasterAccount = false);
    }
}

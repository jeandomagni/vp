﻿namespace Ivh.Domain.FinanceManagement.Payment.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IPaymentActionReasonRepository : IRepository<PaymentActionReason>
    {
    }
}
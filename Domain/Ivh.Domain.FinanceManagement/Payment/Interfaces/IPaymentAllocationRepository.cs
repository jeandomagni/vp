﻿namespace Ivh.Domain.FinanceManagement.Payment.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IPaymentAllocationRepository : IRepository<PaymentAllocation>
    {
        IList<PaymentAllocation> GetAllPaymentAllocationsForVisits(IList<int> visitIds, PaymentBatchStatusEnum? paymentBatchStatus);
        IList<PaymentAllocation> GetNonInterestPaymentAllocationsForVisits(IList<int> visitIds, PaymentBatchStatusEnum? paymentBatchStatus);
        IList<PaymentAllocationVisitAmountResult> GetAllPaymentAllocationAmountsForVisits(IList<int> visitIds, PaymentBatchStatusEnum paymentBatchStatus);
        IList<PaymentAllocationVisitAmountResult> GetNonInterestPaymentAllocationAmountsForVisits(IList<int> visitIds, PaymentBatchStatusEnum paymentBatchStatus, DateTime? startDate = null, DateTime? endDate = null);
        void RemovePaymentAllocations(Payment payment);
    }
}

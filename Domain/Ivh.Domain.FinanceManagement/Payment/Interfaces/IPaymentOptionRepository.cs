﻿namespace Ivh.Domain.FinanceManagement.Payment.Interfaces
{
    using Common.Base.Interfaces;
    using Payment.Entities;

    public interface IPaymentOptionRepository : IRepository<PaymentOption>
    {
    }
}
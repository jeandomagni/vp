﻿namespace Ivh.Domain.FinanceManagement.Payment.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IPaymentAllocationService : IDomainService
    {
        //Dead
        PaymentAllocationResult AllocatePayment(Payment paymentEntity);
        IList<PaymentAllocationResult> AllocatePayments(IList<Payment> paymentEntities);
        PaymentAllocationResult AllocatePaymentReverse(Payment originalPaymentEntity, Payment reversalPaymentEntity);
        void RemovePaymentAllocation(Payment paymentEntity);
        PaymentAllocation GetById(int paymentAllocationId);
        void PublishPaymentAllocationMessages(Payment payment, bool onlyPublishAllocationsWithoutOriginalPaymentAllocation = false, AchSettlementTypeEnum achSettlementType = AchSettlementTypeEnum.None);
        void PublishPaymentAllocationMessages(IList<PaymentAllocation> paymentAllocations);
        IList<BillingApplication> BillingApplications();
        void ClearPaymentAllocations(IList<int> paymentAllocationIds);
        IList<PaymentAllocationVisitAmountResult> GetNonInterestUnclearedPaymentAmountsForVisits(IList<int> visitIds, DateTime? startDate = null, DateTime? endDate = null);
        void OffsetAllAllocationsInPayment(Payment payment);
        PaymentAllocationResult AllocateReallocationPayment(Payment reallocationPayment, decimal reallocateAmountAfterWriteOff, Dictionary<int, decimal> maxPerVisit, int financePlanId);
    }
}
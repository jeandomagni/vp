﻿namespace Ivh.Domain.FinanceManagement.Payment.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IPaymentProcessorResponseRepository : IRepository<PaymentProcessorResponse>
    {
        PaymentProcessorResponse GetPaymentProcessorResponse(int paymentProcessorResponseId);
        IList<PaymentProcessorResponse> GetPaymentProcessorResponses(IList<int> paymentProcessorResponseIds);
        PaymentProcessorResponse GetPaymentProcessorResponse(string paymentProcessorSourceKey);
    }
}
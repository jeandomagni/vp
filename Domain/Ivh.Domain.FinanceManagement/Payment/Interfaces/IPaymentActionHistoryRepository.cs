﻿namespace Ivh.Domain.FinanceManagement.Payment.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IPaymentActionHistoryRepository : IRepository<PaymentActionHistory>
    {
        IList<PaymentActionHistory> GetPaymentActionsByType(int vpGuarantorId, int actionVisitPayUserId, PaymentActionTypeEnum paymentActionType);
    }
}
﻿namespace Ivh.Domain.FinanceManagement.Payment.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IPaymentFinancePlanRepository : IRepository<PaymentFinancePlan>
    {
        IList<PaymentFinancePlan> GetPaymentFinancePlans(IList<int> financePlanIds);
        IList<PaymentFinancePlan> GetActivePaymentFinancePlansFromVisits(IList<PaymentVisit> visits);
        IList<PaymentFinancePlanVisit> GetFinancePlanVisitsOnActiveFiancePlans(IList<PaymentVisit> visits);

    }
}
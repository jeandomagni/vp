﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.FinanceManagement.Payment.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IMerchantAccountAllocationService : IDomainService
    {
        IList<Payment> SplitPayments(IList<Payment> paymentEntities);
        void UpdatePaymentFromSplitPayment(Payment payment, Payment splitPayment);
        IList<MerchantAccount> GetAllMerchantAccounts(bool includeMasterAccount = false);
        MerchantAccount GetMerchantAccountByServiceGroupId(int? serviceGroupId);
    }
}

﻿using Ivh.Common.Base.Interfaces;
using Ivh.Domain.FinanceManagement.Payment.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.FinanceManagement.Payment.Interfaces
{
    public interface IBalanceTransferStatusMerchantAccountRepository : IRepository<BalanceTransferStatusMerchantAccount>
    {
        int GetBalanceTransferMerchantAccountId();
    }
}

﻿namespace Ivh.Domain.FinanceManagement.Payment.Utilities
{
    using System;
    using System.Collections.Generic;
    using Autofac;
    using Common.Base.Enums;
    using Common.DependencyInjection;
    using Common.VisitPay.Enums;
    using Entities;
    using Settings.Interfaces;

    public static class Utility
    {
        private static readonly IList<string> AcceptedAvsCodes = IvinciContainer.Instance.Container().Resolve<IApplicationSettingsService>().AcceptedAvsCodes.Value;

        public static string GetStatusMessage(PaymentProcessorResponse paymentProcessorResponse, Payment payment)
        {
            return GetStatusMessage(paymentProcessorResponse, payment.PaymentStatus);
        }

        public static string GetStatusMessage(PaymentProcessorResponse paymentProcessorResponse, PaymentStatusEnum paymentStatus)
        {
            if (paymentProcessorResponse == null)
            {
                return string.Empty;
            }

            return GetStatusMessage(paymentProcessorResponse.ResponseField2, paymentProcessorResponse.ResponseField5, paymentProcessorResponse.ResponseField4, paymentStatus);
        }

        public static string GetRealTimeStatusMessage(PaymentProcessorResponse paymentProcessorResponse)
        {
            if (paymentProcessorResponse == null)
            {
                return string.Empty;
            }

            return GetRealTimeStatusMessage(paymentProcessorResponse.ResponseField2, paymentProcessorResponse.ResponseField5, paymentProcessorResponse.ResponseField4);
        }

        public static string GetRealTimeStatusMessage(IReadOnlyDictionary<string, string> responseValues)
        {
            if (responseValues == null)
            {
                return string.Empty;
            }

            string avs = responseValues.ContainsKey("avs") ? responseValues["avs"] : null;
            string declineType = responseValues.ContainsKey("declinetype") ? responseValues["declinetype"] : null;
            string status = responseValues.ContainsKey("status") ? responseValues["status"] : null;

            return GetRealTimeStatusMessage(status, declineType, avs);
        }

        private static string GetRealTimeStatusMessage(string transactionStatus, string declineType, string avsCode)
        {
            if (string.IsNullOrEmpty(transactionStatus))
            {
                return string.Empty;
            }

            if (transactionStatus.ToLowerInvariant() == "approved" && !string.IsNullOrEmpty(avsCode))
            {
                return "You have entered an incorrect address. Please try again.";
            }

            if (transactionStatus.ToLowerInvariant() == "declined" || transactionStatus.ToLowerInvariant() == "decline")
            {
                switch ((declineType ?? string.Empty).ToLowerInvariant())
                {
                    case "avs":
                        return "You have entered an incorrect address. Please try again.";

                    case "cvv":
                        return "You entered an incorrect CVV code. Please try again.";

                    case "expiredcard":
                        return "This card is expired. Please udpate the expiration date or select a different payment method.";

                    case "carderror":
                        return "You have entered an incorrect credit card number. Please try again.";

                    case "decline":
                    case "call":
                    default:
                        return "This card was declined. Please try again with a different payment method.";
                }
            }

            return string.Empty;
        }

        private static string GetStatusMessage(string transactionStatus, string declineType, string avsCode, PaymentStatusEnum paymentStatus)
        {
            IList<PaymentStatusEnum> applicableStatuses = new List<PaymentStatusEnum>
            {
                PaymentStatusEnum.ClosedFailed,
                PaymentStatusEnum.ActivePendingGuarantorAction
            };

            if (!applicableStatuses.Contains(paymentStatus) || string.IsNullOrEmpty(transactionStatus))
            {
                return string.Empty;
            }
            
            string statusText = paymentStatus == PaymentStatusEnum.ActivePendingGuarantorAction ? "Pending Resubmit" : "Failed";
            string errorText = string.Empty;

            if (transactionStatus.ToLowerInvariant() == "approved" && !string.IsNullOrEmpty(avsCode) && !AcceptedAvsCodes.Contains(avsCode))
            {
                errorText = "Incorrect Address";
            }
            else if (transactionStatus.ToLowerInvariant() == "declined")
            {
                IDictionary<string, string> declineMessages = new Dictionary<string, string>
                {
                    { "avs", "Incorrect Address" },
                    { "cvv", "Incorrect CVV Code" },
                    { "expiredcard", "Card Expired" },
                    { "carderror", "Incorrect Card Number" },
                    { "decline", "Card Declined" },
                    { "call", "Card Declined" },
                };

                string key = declineType?.ToLowerInvariant();
                if (!string.IsNullOrEmpty(key) && declineMessages.ContainsKey(key))
                {
                    errorText = declineMessages[key];
                }
            }

            return string.IsNullOrEmpty(errorText) ? string.Empty : $"{statusText} - {errorText}";
        }
    }
}

﻿namespace Ivh.Domain.FinanceManagement.Payment.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;
    using User.Entities;

    public class PaymentActionService : DomainService, IPaymentActionService
    {
        private readonly Lazy<IPaymentActionHistoryRepository> _paymentActionHistoryRepository;
        private readonly Lazy<IPaymentActionReasonRepository> _paymentActionReasonRepository;

        public PaymentActionService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IPaymentActionHistoryRepository> paymentActionHistoryRepository,
            Lazy<IPaymentActionReasonRepository> paymentActionReasonRepository) : base(serviceCommonService)
        {
            this._paymentActionHistoryRepository = paymentActionHistoryRepository;
            this._paymentActionReasonRepository = paymentActionReasonRepository;
        }
        
        public void AddCancelAction(IList<Payment> payments, int paymentActionReasonId, string paymentActionDescription, int actionVisitPayUserId)
        {
            Guid correlationGuid = Guid.NewGuid();

            foreach (Payment payment in payments)
            {
                PaymentActionHistory paymentActionHistory = new PaymentActionHistory
                {
                    ActionVisitPayUser = new VisitPayUser {VisitPayUserId = actionVisitPayUserId},
                    CorrelationGuid = correlationGuid,
                    Description = string.IsNullOrEmpty(paymentActionDescription) ? null : paymentActionDescription,
                    InsertDate = DateTime.UtcNow,
                    Payment = new Payment {PaymentId = payment.PaymentId},
                    PaymentActionReason = new PaymentActionReason {PaymentActionReasonId = paymentActionReasonId},
                    PaymentActionType = PaymentActionTypeEnum.Cancel,
                    VpGuarantorId = payment.Guarantor.VpGuarantorId
                };

                this._paymentActionHistoryRepository.Value.Insert(paymentActionHistory);
            }
        }

        public void AddCancelAction(IList<int> financePlanIds, int vpGuarantorId, int paymentActionReasonId, string paymentActionDescription, int actionVisitPayUserId)
        {
            Guid correlationGuid = Guid.NewGuid();

            foreach (int financePlanId in financePlanIds)
            {
                PaymentActionHistory paymentActionHistory = new PaymentActionHistory
                {
                    ActionVisitPayUser = new VisitPayUser {VisitPayUserId = actionVisitPayUserId},
                    CorrelationGuid = correlationGuid,
                    Description = string.IsNullOrEmpty(paymentActionDescription) ? null : paymentActionDescription,
                    FinancePlanId = financePlanId,
                    InsertDate = DateTime.UtcNow,
                    PaymentActionReason = new PaymentActionReason {PaymentActionReasonId = paymentActionReasonId},
                    PaymentActionType = PaymentActionTypeEnum.Cancel,
                    VpGuarantorId = vpGuarantorId
                };

                this._paymentActionHistoryRepository.Value.Insert(paymentActionHistory);
            }
        }

        public void AddRescheduleAction(Payment payment, int paymentActionReasonId, string paymentActionDescription, int actionVisitPayUserId)
        {
            this.AddRescheduleAction(payment.ToListOfOne(), paymentActionReasonId, paymentActionDescription, actionVisitPayUserId);
        }

        public void AddRescheduleAction(IList<Payment> payments, int paymentActionReasonId, string paymentActionDescription, int actionVisitPayUserId)
        {
            Guid correlationGuid = Guid.NewGuid();

            foreach (Payment payment in payments)
            {
                PaymentActionHistory paymentActionHistory = new PaymentActionHistory
                {
                    ActionVisitPayUser = new VisitPayUser {VisitPayUserId = actionVisitPayUserId},
                    CorrelationGuid = correlationGuid,
                    Description = string.IsNullOrEmpty(paymentActionDescription) ? null : paymentActionDescription,
                    InsertDate = DateTime.UtcNow,
                    Payment = new Payment {PaymentId = payment.PaymentId},
                    PaymentActionReason = new PaymentActionReason {PaymentActionReasonId = paymentActionReasonId},
                    PaymentActionType = PaymentActionTypeEnum.Reschedule,
                    PaymentRescheduledDate = payment.ScheduledPaymentDate,
                    VpGuarantorId = payment.Guarantor.VpGuarantorId
                };

                this._paymentActionHistoryRepository.Value.Insert(paymentActionHistory);
            }
        }

        public void AddRescheduleAction(IList<int> financePlanIds, int vpGuarantorId, int paymentActionReasonId, string paymentActionDescription, int actionVisitPayUserId)
        {
            Guid correlationGuid = Guid.NewGuid();

            foreach (int financePlanId in financePlanIds)
            {
                PaymentActionHistory paymentActionHistory = new PaymentActionHistory
                {
                    ActionVisitPayUser = new VisitPayUser {VisitPayUserId = actionVisitPayUserId},
                    CorrelationGuid = correlationGuid,
                    Description = string.IsNullOrEmpty(paymentActionDescription) ? null : paymentActionDescription,
                    FinancePlanId = financePlanId,
                    InsertDate = DateTime.UtcNow,
                    PaymentActionReason = new PaymentActionReason {PaymentActionReasonId = paymentActionReasonId},
                    PaymentActionType = PaymentActionTypeEnum.Reschedule,
                    VpGuarantorId = vpGuarantorId
                };

                this._paymentActionHistoryRepository.Value.Insert(paymentActionHistory);
            }
        }

        public IList<PaymentActionReason> GetPaymentCancelReasons(PaymentTypeEnum paymentType)
        {
            return this.GetReasons(paymentType, true);
        }

        public IList<PaymentActionReason> GetPaymentRescheduleReasons(PaymentTypeEnum paymentType)
        {
            return this.GetReasons(paymentType, false);
        }

        public bool IsEligibleToCancel(int vpGuarantorId, int visitPayUserId, PaymentTypeEnum paymentType)
        {
            // manual payments are always eligible for cancel
            if (!paymentType.IsInCategory(PaymentTypeEnumCategory.Recurring))
            {
                return true;
            }
            
            IList<PaymentActionHistory> paymentActionHistories = this._paymentActionHistoryRepository.Value.GetPaymentActionsByType(vpGuarantorId, visitPayUserId, PaymentActionTypeEnum.Cancel);
            
            // filter by date range
            // filter by finance plan (above indicates manual payments are always eligible)
            DateTime dateToCheck = DateTime.UtcNow.Date.AddDays(this.Client.Value.CancelRecurringPaymentWindow * -1).Date;
            IEnumerable<PaymentActionHistory> filtered = paymentActionHistories
                .Where(x => x.InsertDate >= dateToCheck)
                .Where(x => x.FinancePlanId.HasValue);
            
            int numberOfCancels = filtered.GroupBy(x => x.CorrelationGuid).Count();

            return numberOfCancels < this.Client.Value.CancelRecurringPaymentMaximum;
        }
        
        public bool IsEligibleToReschedule(int vpGuarantorId, int visitPayUserId, PaymentTypeEnum paymentType)
        {
            // manual payment is always eligible for reschedule
            if (!paymentType.IsInCategory(PaymentTypeEnumCategory.Recurring))
            {
                return true;
            }

            IList<PaymentActionHistory> paymentActionHistories = this._paymentActionHistoryRepository.Value.GetPaymentActionsByType(vpGuarantorId, visitPayUserId, PaymentActionTypeEnum.Reschedule);
            
            DateTime dateToCheck = DateTime.UtcNow.Date.AddDays(this.Client.Value.RescheduleRecurringPaymentWindow * -1).Date;

            // filter by finance plan (above indicates manual payments are always eligible)
            // filter by date range
            IEnumerable<PaymentActionHistory> filtered = paymentActionHistories
                .Where(x => x.InsertDate >= dateToCheck)
                .Where(x => x.FinancePlanId.HasValue);
            
            int numberOfReschedules = filtered.GroupBy(x => x.CorrelationGuid).Count();

            return numberOfReschedules < this.Client.Value.RescheduleRecurringPaymentMaximum;
        }

        private IList<PaymentActionReason> GetReasons(PaymentTypeEnum paymentType, bool isCancel)
        {
            bool isManual = paymentType.IsInCategory(PaymentTypeEnumCategory.Manual);
            bool isRecurring = paymentType.IsInCategory(PaymentTypeEnumCategory.Recurring);

            if (!isRecurring && !isManual && !isCancel)
            {
                return new List<PaymentActionReason>();
            }

            List<PaymentActionReason> list = this._paymentActionReasonRepository.Value.GetQueryable().Where(x => x.IsActive).ToList();

            list = isCancel ? list.Where(x => x.IsCancel).ToList() : list.Where(x => x.IsReschedule).ToList();

            if (isManual)
            {
                list = list.Where(x => x.IsManual).ToList();
            }

            if (isRecurring)
            {
                list = list.Where(x => x.IsRecurring).ToList();
            }

            return list.OrderBy(x => x.DisplayOrder).ToList();
        }
    }
}
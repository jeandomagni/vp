﻿namespace Ivh.Domain.FinanceManagement.Payment.Services.PaymentAllocation
{
    using AutoMapper;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Utilities.Extensions;
    using Entities;
    using Ivh.Common.Base.Utilities.Helpers;
    using Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.VisitPay.Enums;
    using ServiceGroup.Entities;
    using ServiceGroup.Interfaces;

    public class MerchantAccountAllocationService : DomainService, IMerchantAccountAllocationService
    {
        private readonly Lazy<IMerchantAccountRepository> _merchantAccountRepository;
        private readonly Lazy<IBalanceTransferStatusMerchantAccountRepository> _balanceTransferStatusMerchantAccountRepository;
        private readonly Lazy<IServiceGroupMerchantAccountRepository> _serviceGroupMerchantAccountRepository;
        private readonly Lazy<IPaymentAllocationRepository> _paymentAllocationRepository;

        public MerchantAccountAllocationService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IMerchantAccountRepository> merchantAccountRepository,
            Lazy<IBalanceTransferStatusMerchantAccountRepository> balanceTransferStatusMerchantAccountRepository,
            Lazy<IServiceGroupMerchantAccountRepository> serviceGroupMerchantAccountRepository,
            Lazy<IPaymentAllocationRepository> paymentAllocationRepository) : base(serviceCommonService)
        {
            this._merchantAccountRepository = merchantAccountRepository;
            this._balanceTransferStatusMerchantAccountRepository = balanceTransferStatusMerchantAccountRepository;
            this._serviceGroupMerchantAccountRepository = serviceGroupMerchantAccountRepository;
            this._paymentAllocationRepository = paymentAllocationRepository;
        }

        public IList<Payment> SplitPayments(IList<Payment> paymentEntities)
        {
            IList<MerchantAccount> merchantAccounts = this._merchantAccountRepository.Value.GetAll().ToList();
            if (merchantAccounts == null || !merchantAccounts.Any())
            {
                return paymentEntities;
            }

            bool isBalanceTransferEnabled = this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.BalanceTransfer);
            List<Payment> payments = new List<Payment>();
            try
            {
                int btMerchantAccountId = 0;
                if (isBalanceTransferEnabled)
                {
                    btMerchantAccountId = this._balanceTransferStatusMerchantAccountRepository.Value.GetBalanceTransferMerchantAccountId();
                }

                foreach (Payment paymentEntity in paymentEntities)
                {
                    IDictionary<int, IList<PaymentAllocation>> merchantAccountPaymentAllocations = new Dictionary<int, IList<PaymentAllocation>>();

                    List<Payment> splitPayments = new List<Payment>();
                    bool hasMerchantAccount = paymentEntity.MerchantAccount != null;
                    bool hasPaymentAllocations = paymentEntity.PaymentAllocations != null && paymentEntity.PaymentAllocations.Count > 0;                    

                    if (hasPaymentAllocations && !hasMerchantAccount)
                    {

                        IList<PaymentAllocation> newBalanceTransferPaymentAllocations = paymentEntity.PaymentAllocations
                            .Where(x => isBalanceTransferEnabled 
                                        && x.PaymentVisit.BalanceTransferStatusEnum.IsInCategory(BalanceTransferStatusEnumCategory.Active)
                                        && x.PaymentAllocationType != PaymentAllocationTypeEnum.VisitInterest
                            ).ToList();

                        if (newBalanceTransferPaymentAllocations.Any())
                        {
                            if (btMerchantAccountId > 0)
                            {
                                merchantAccountPaymentAllocations.Add(btMerchantAccountId, newBalanceTransferPaymentAllocations.ToList());
                            }
                            else
                            {
                                throw new Exception("No Balance Transfer Merchant Account was found");
                            }
                        }

                        IList<PaymentAllocation> newPaymentAllocations = paymentEntity.PaymentAllocations
                            .Where(x => !isBalanceTransferEnabled
                                        || !x.PaymentVisit.BalanceTransferStatusEnum.IsInCategory(BalanceTransferStatusEnumCategory.Active)
                                        || x.PaymentAllocationType == PaymentAllocationTypeEnum.VisitInterest
                            ).ToList();

                        if (newPaymentAllocations.Any())
                        {
                            IList<int> hospitalMechantAccountIds = merchantAccounts.Where(x => x.MerchantAccountId != btMerchantAccountId).Select(x => x.MerchantAccountId).ToList();

                            foreach (int merchantAccountId in hospitalMechantAccountIds)
                            {
                                IList<PaymentAllocation> merchantPaymentAllocations = newPaymentAllocations.Where(x => this.GetServiceGroupMerchantAccountId(x.PaymentVisit.ServiceGroupId) == merchantAccountId).ToList();
                                if (merchantPaymentAllocations.Any())
                                {
                                    merchantAccountPaymentAllocations.Add(merchantAccountId, merchantPaymentAllocations.ToList());
                                }
                            }
                        }

                        if (merchantAccountPaymentAllocations.Count == 1)
                        {
                            MerchantAccount merchantAccount = merchantAccounts.Single(x => x.MerchantAccountId == merchantAccountPaymentAllocations.First().Key);
                            paymentEntity.MerchantAccount = merchantAccount;
                            splitPayments.Add(paymentEntity);
                        }
                        else
                        {
                            foreach (KeyValuePair<int, IList<PaymentAllocation>> merchantAllocation in merchantAccountPaymentAllocations)
                            {
                                Payment paymentClone = this.GetPaymentClone(paymentEntity, merchantAllocation, merchantAccounts);
                                splitPayments.Add(paymentClone);
                            }
                        }

                    }
                    else
                    {
                        splitPayments.Add(paymentEntity);
                    }

                    payments.AddRange(splitPayments);
                }

                return payments;
            }
            catch (Exception ex)
            {
                this.LoggingService.Value.Fatal(() => $"MerchantAccountAllocationService::SplitPayments(IList<Payment>): Exception: {ExceptionHelper.AggregateExceptionToString(ex)}");
                throw;
            }
        }

        public Payment GetPaymentClone(Payment paymentEntity, KeyValuePair<int, IList<PaymentAllocation>> merchantAllocation, IList<MerchantAccount> merchantAccounts)
        {
            Payment paymentClone = Mapper.Map<Payment>(paymentEntity);
            MerchantAccount merchantAccount = merchantAccounts.Single(x => x.MerchantAccountId == merchantAllocation.Key);
            foreach (PaymentAllocation pa in merchantAllocation.Value)
            {
                PaymentAllocation paymentAllocationClone = Mapper.Map<PaymentAllocation>(pa);
                paymentClone.PaymentAllocations.Add(paymentAllocationClone);
                paymentAllocationClone.Payment = paymentClone;

            }
            paymentClone.MerchantAccount = merchantAccount;
            paymentClone.ActualPaymentAmount = paymentClone.PaymentAllocations.Sum(x => x.ActualAmount);

            IEnumerable<PaymentScheduledAmount> newScheduledAmounts = paymentClone.PaymentScheduledAmounts.Where(x => x.PaymentScheduledAmountId == 0);

            // Assign ScheduledAmount per visit for cloned PaymentScheduledAmount.
            // There could be a ScheduledAmount for a visit that belongs to a different merchant account
            bool actualPaymentAmountUsed = false;
            foreach (PaymentScheduledAmount newScheduledAmount in newScheduledAmounts)
            {
                int? visitId = newScheduledAmount?.PaymentVisit?.VisitId;
                if (visitId != null)
                {
                    decimal allocatedAmountForVisit = paymentClone.PaymentAllocations.Where(x => x.PaymentVisit.VisitId == visitId).Select(x => x.ActualAmount).Sum();
                    newScheduledAmount.ScheduledAmount = allocatedAmountForVisit;
                }
                else
                {
                    // Some payments made from payment groups have more than one PaymentScheduledAmount that does not belong to a visit.
                    // If the ActualPaymentAmount has already been assigned to the ScheduledAmount of a PaymentScheduledAmount, 
                    // then any remaining non-visit PaymentScheduledAmount must have ScheduledAmount set to zero.
                    //Code below this section will remove PaymentScheduledAmount objects that are set to zero.
                    if (actualPaymentAmountUsed)
                    {
                        newScheduledAmount.ScheduledAmount = 0;
                    }
                    else
                    {
                        newScheduledAmount.ScheduledAmount = paymentClone.ActualPaymentAmount;
                        actualPaymentAmountUsed = true;
                    }
                }

                if (newScheduledAmount.ScheduledAmount > 0)
                {
                    newScheduledAmount.Payment = paymentClone;
                }
            }

            // Remove cloned PaymentScheduledAmount objects that have no ScheduledAmount.
            // There could be a ScheduledAmount for a visit that belongs to a different merchant account
            // or a redundant ScheduledAmount for a non-visit.
            for (int i = paymentClone.PaymentScheduledAmounts.Count() - 1; i >= 0; i--)
            {
                PaymentScheduledAmount paymentScheduledAmount = paymentClone.PaymentScheduledAmounts[i];
                bool isNew = paymentScheduledAmount.PaymentScheduledAmountId == 0;
                bool isZeroPayment = paymentScheduledAmount.ScheduledAmount == 0;
                if (isNew && isZeroPayment)
                {
                    paymentClone.PaymentScheduledAmounts.RemoveAt(i);
                }
            }

            IEnumerable<PaymentStatusHistory> newPaymentStatusHistories = paymentClone.PaymentStatusHistories.Where(x => x.PaymentStatusHistoryId == 0);
            foreach (PaymentStatusHistory newPaymentStatusHistory in newPaymentStatusHistories)
            {
                newPaymentStatusHistory.Payment = paymentClone;
            }
            return paymentClone;
        }

        //TODO: might want to create a repo mock to unit test this, or leave for integration testing
        public void UpdatePaymentFromSplitPayment(Payment payment, Payment splitPayment)
        {
            payment.MerchantAccount = splitPayment.MerchantAccount;
            payment.ActualPaymentAmount = splitPayment.ActualPaymentAmount;
            
            this._paymentAllocationRepository.Value.RemovePaymentAllocations(payment);

            foreach (PaymentAllocation paymentAllocation in splitPayment.PaymentAllocations)
            {
                paymentAllocation.PaymentAllocationId = default(int);
                paymentAllocation.Payment = payment;
                payment.PaymentAllocations.Add(paymentAllocation);
            }

            payment.PaymentScheduledAmounts.Clear();
            foreach (PaymentScheduledAmount paymentScheduledAmount in splitPayment.PaymentScheduledAmounts)
            {
                paymentScheduledAmount.Payment = payment;
                payment.PaymentScheduledAmounts.Add(paymentScheduledAmount);
            }
            
            payment.PaymentStatusHistories.Clear();
            foreach (PaymentStatusHistory paymentStatusHistory in splitPayment.PaymentStatusHistories)
            {
                paymentStatusHistory.Payment = payment;
                payment.PaymentStatusHistories.Add(paymentStatusHistory);
            }
        }

        private int? GetServiceGroupMerchantAccountId(int? serviceGroupId)
        {
            int? merchantAccount = this._serviceGroupMerchantAccountRepository.Value.GetMerchantAccountId(serviceGroupId);

            return merchantAccount;
        }

        public IList<MerchantAccount> GetAllMerchantAccounts(bool includeMasterAccount = false)
        {
            return this._merchantAccountRepository.Value.GetAll(includeMasterAccount).ToList();
        }

        public IList<ServiceGroupMerchantAccount> GetAllServiceGroupMerchantAccounts()
        {
            return this._serviceGroupMerchantAccountRepository.Value.GetQueryable().ToList();
        }

        public MerchantAccount GetMerchantAccountByServiceGroupId(int? serviceGroupId)
        {
            return this._serviceGroupMerchantAccountRepository.Value.GetMerchantAccoutByServiceGroupId(serviceGroupId);
        }
    }
}

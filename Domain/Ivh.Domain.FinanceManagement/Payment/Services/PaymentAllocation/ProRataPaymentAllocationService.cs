namespace Ivh.Domain.FinanceManagement.Payment.Services.PaymentAllocation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Visit.Interfaces;
    using Entities;
    using FinancePlan.Entities;
    using FinancePlan.Interfaces;
    using Interfaces;
    using NHibernate;
    using Statement.Interfaces;

    public class ProRataPaymentAllocationService : PaymentAllocationServiceBase
    {
        /// <summary>
        ///     Distributes payments
        ///     Note: this class is not thread safe.  Use different instances per thread or refactor.
        /// </summary>
        public ProRataPaymentAllocationService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IVisitService> visitService,
            Lazy<IVpStatementService> vpStatementService,
            Lazy<IFinancePlanService> financePlanService,
            Lazy<IPaymentRepository> paymentRepository,
            Lazy<IVisitTransactionRepository> visitTransactionRepository,
            Lazy<IVisitTransactionService> visitTransactionService,
            Lazy<IMerchantAccountAllocationService> merchantAccountAllocationService,
            ISessionContext<VisitPay> sessionContext,
            Lazy<IPaymentAllocationRepository> paymentAllocationRepository,
            Lazy<IPaymentVisitRepository> paymentVisitRepository,
            Lazy<IPaymentFinancePlanRepository> paymentFinancePlanRepository
            )
            : base(serviceCommonService, visitService, vpStatementService, financePlanService, paymentRepository, visitTransactionRepository, visitTransactionService, merchantAccountAllocationService, sessionContext, paymentAllocationRepository, paymentVisitRepository, paymentFinancePlanRepository)
        {
        }

        private const string BalanceKey = "All";

        protected override decimal DistributeVisits(Payment paymentEntity, IList<PaymentVisit> orderedVisitsList, decimal maxToDistribute, AllocationType allocationType, IDictionary<PaymentVisit, decimal> maxPerVisit = null, FinancePlan financePlan = null)
        {
            if (maxToDistribute <= 0m)
            {
                return 0;
            }

            IDictionary<int, decimal> unclearedAllocations = new Dictionary<int, decimal>();
            bool isPrincipal = allocationType == AllocationType.Principal;
            bool isInterest = !isPrincipal;

            //Sum all of the current balances
            //Remove the currently distributed values. 
            IDictionary<string, decimal> balances = this.GetTotalForAllCurrentBalancesWithoutNewlyDistributed(
                paymentEntity,
                orderedVisitsList,
                unclearedAllocations,
                maxPerVisit,
                isInterest
            );

            decimal totalForAllCurrentBalancesWithoutNewlyDistributed = balances[BalanceKey];

            if (totalForAllCurrentBalancesWithoutNewlyDistributed <= 0m)
            {
                return 0m;
            }

            decimal calculatedMaxToDistribute = 0m;

            if (maxToDistribute > totalForAllCurrentBalancesWithoutNewlyDistributed)
            {
                calculatedMaxToDistribute = totalForAllCurrentBalancesWithoutNewlyDistributed;
            }
            else
            {
                calculatedMaxToDistribute = maxToDistribute;
            }

            //Add Allocations
            List<PaymentAllocation> paymentAllocations = new List<PaymentAllocation>();

            decimal totalDistributed = 0m;
            decimal totalDistributedUnrounded = 0m;

            List<List<PaymentVisit>> visitGroupsList = this.GroupVisitsByPriority(orderedVisitsList.ToList());

            bool hasPaymentAllocationPriorities = this.Client.Value.PaymentAllocationOrderedPriorityGrouping.Any();
            bool payInFull = isPrincipal && hasPaymentAllocationPriorities;

            decimal amountLeftToDistribute = calculatedMaxToDistribute;

            foreach (List<PaymentVisit> paymentVisitListGroup in visitGroupsList)
            {
                VisitGroupPaymentAllocationResult visitGroupPaymentAllocationResult = this.AllocateDistributionsToVisits(
                    paymentEntity,
                    paymentVisitListGroup,
                    unclearedAllocations,
                    maxPerVisit,
                    amountLeftToDistribute,
                    totalForAllCurrentBalancesWithoutNewlyDistributed,
                    payInFull,
                    isInterest
                );

                totalDistributed += visitGroupPaymentAllocationResult.TotalDistributed;
                totalDistributedUnrounded += visitGroupPaymentAllocationResult.TotalDistributedUnrounded;

                paymentAllocations.AddRange(visitGroupPaymentAllocationResult.PaymentAllocations);
                this.AdjustRoundedAllocationAmounts(paymentAllocations, totalDistributedUnrounded, ref totalDistributed);

                if (isInterest)
                {
                    // Interest allocation uses this value to calculate prorata amount
                    // This value must represent "the whole" (regardless of how visits are grouped) and not be decreased as money is allocated   
                    amountLeftToDistribute = calculatedMaxToDistribute;
                }
                else
                {
                    // Principal allocation uses this value to calculate prorata amount
                    // This value must be decreased as money is allocated to limit the next visit group to only use money that is still available 
                    amountLeftToDistribute = calculatedMaxToDistribute - totalDistributed;
                }

                bool outOfMoney = totalDistributed >= calculatedMaxToDistribute;
                if (outOfMoney)
                {
                    break;
                }
            }
           
            this.AddGeneratedPaymentAllocations(paymentEntity, paymentAllocations);

            return totalDistributed;
        }

        /// <summary>
        /// This will distribute blindly using the maxPerVisit dictionary, doesn't take current balance, uncleared payments, interest or anything into account.  YOU HAVE BEEN WARNED.
        /// </summary>
        protected override decimal DistributeVisitsUsingMaxPerVisit(Payment paymentEntity, IList<PaymentVisit> orderedVisitsList, decimal maxToDistribute, AllocationType allocationType, IDictionary<PaymentVisit, decimal> maxPerVisit = null, int? financePlanId = null)
        {
            bool hasMaxPerVisit = maxPerVisit != null;
            if (!hasMaxPerVisit)
            {
                return 0m;
            }
            List<PaymentAllocation> paymentAllocations = new List<PaymentAllocation>();
            decimal totalDistributed = 0m;
            decimal totalDistributedUnrounded = 0m;
            decimal maxPerVisitSum = maxPerVisit?.Sum(x => x.Value) ?? 0m;
            bool isPrincipal = allocationType == AllocationType.Principal;
            foreach (PaymentVisit paymentVisit in orderedVisitsList)
            {
                bool hasMaxPerVisitAmount = maxPerVisit.ContainsKey(paymentVisit);
                decimal maxPerVisitAmount = hasMaxPerVisitAmount ? maxPerVisit[paymentVisit] : 0m;
                decimal maxPerVisitPercentage = maxPerVisitAmount / maxPerVisitSum;
                decimal exactAmountToDistributeForVisit = maxToDistribute * maxPerVisitPercentage;

                //Create Allocations
                PaymentAllocationTypeEnum paymentAllocationType = isPrincipal ? PaymentAllocationTypeEnum.VisitPrincipal : PaymentAllocationTypeEnum.VisitInterest;
                PaymentAllocation visitDistribution = this.CreateOrFindPaymentAllocationForVisit(paymentVisit, paymentEntity, paymentAllocationType, financePlanId);

                //Add allocations
                visitDistribution.ActualAmount += exactAmountToDistributeForVisit;
                paymentAllocations.Add(visitDistribution);

                //Add to totals
                totalDistributedUnrounded += exactAmountToDistributeForVisit;
                // Purposely adding rounded amounts inside the loop
                // to accurately represent the payment allocation amounts.
                // The amount difference that this creates is expected to be fixed when the method AdjustRoundedAllocationAmounts() is called
                totalDistributed += Math.Round(exactAmountToDistributeForVisit, 2);

                //This is the max you can round onto this ScheduledAmount
                visitDistribution.MaxAllocationAmount = maxPerVisitAmount;
            }

            this.AdjustRoundedAllocationAmounts(paymentAllocations, totalDistributedUnrounded, ref totalDistributed);
            this.AddGeneratedPaymentAllocations(paymentEntity, paymentAllocations);

            return totalDistributed;
        }

        protected override IDictionary<string, decimal> GetTotalForAllCurrentBalancesWithoutNewlyDistributed(
            Payment paymentEntity,
            IList<PaymentVisit> visitsList,
            IDictionary<int, decimal> unclearedAllocations,
            IDictionary<PaymentVisit, decimal> maxPerVisit,
            bool isInterest)
        {
            decimal totalForAllCurrentBalancesWithoutNewlyDistributed = 0m;
            decimal amountDue = 0m;
            bool hasMaxPerVisits = maxPerVisit != null;
            HashSet<int> visitIdHashSet = visitsList.Select(y => y.VisitId).ToHashSet();

            if (isInterest)
            {
                amountDue = this.PaymentFinancePlanVisits.Values.Where(x => visitIdHashSet.Contains(x.VisitId)).Sum(x => x.InterestDue);
                if (hasMaxPerVisits)
                {
                    amountDue = this.GetInterestDue(visitsList, maxPerVisit);
                }
            }
            else
            {
                unclearedAllocations = this.UnclearedAllocationsByType(visitIdHashSet, PaymentAllocationTypeEnum.VisitPrincipal);
                decimal unclearedAllocationsSum = visitsList.Sum(x => unclearedAllocations.GetValue(x.VisitId));
                amountDue = visitsList.Sum(x => x.CurrentBalance);
                amountDue += unclearedAllocationsSum;
                if (hasMaxPerVisits)
                {
                    amountDue = this.GetPrincipalDue(visitsList, maxPerVisit, unclearedAllocations);
                }
            }

            decimal paymentDistributionAmountTotal = this.GetPaymentDistributionAmount(paymentEntity, visitIdHashSet, isInterest);

            decimal alreadyAllocatedDistributionAmount = this.GetAlreadyAllocatedDistributionAmount(paymentEntity, visitIdHashSet, isInterest);

            totalForAllCurrentBalancesWithoutNewlyDistributed = amountDue - paymentDistributionAmountTotal - alreadyAllocatedDistributionAmount;

            IDictionary<string, decimal> balances = new Dictionary<string, decimal>();
            balances.Add(BalanceKey, totalForAllCurrentBalancesWithoutNewlyDistributed);
            return balances;
        }

        protected VisitGroupPaymentAllocationResult AllocateDistributionsToVisits(
            Payment paymentEntity,
            List<PaymentVisit> paymentVisits,
            IDictionary<int, decimal> unclearedAllocations,
            IDictionary<PaymentVisit, decimal> maxPerVisit,
            decimal calculatedMaxToDistribute,
            decimal totalForAllCurrentBalancesWithoutNewlyDistributed,
            bool payInFull,
            bool isInterest)
        {

            // Need to manipulate the amounts for MaxToDistribute and TotalBalances
            // so that they will allow maximum payment (via pro rata) for the group of PaymentVisits 
            if (payInFull)
            {
                IDictionary<string, decimal> totalForGroupedCurrentBalancesWithoutNewlyDistributed = this.GetTotalForAllCurrentBalancesWithoutNewlyDistributed(paymentEntity, paymentVisits, unclearedAllocations, maxPerVisit, isInterest);
                totalForAllCurrentBalancesWithoutNewlyDistributed = totalForGroupedCurrentBalancesWithoutNewlyDistributed.Sum(x => x.Value);
                calculatedMaxToDistribute = Math.Min(calculatedMaxToDistribute, totalForAllCurrentBalancesWithoutNewlyDistributed);
            }

            VisitGroupPaymentAllocationResult visitGroupPaymentAllocationResult = new VisitGroupPaymentAllocationResult();

            if (calculatedMaxToDistribute <= 0m)
            {
                return visitGroupPaymentAllocationResult;
            }

            foreach (PaymentVisit paymentVisit in paymentVisits)
            {
                PaymentVisitDistribution paymentVisitDistribution = this.GetPaymentVisitDistribution(
                    paymentEntity,
                    paymentVisit,
                    unclearedAllocations,
                    maxPerVisit,
                    calculatedMaxToDistribute,
                    totalForAllCurrentBalancesWithoutNewlyDistributed,
                    isInterest
                );

                //Create Allocations
                PaymentAllocationTypeEnum paymentAllocationType = isInterest ? PaymentAllocationTypeEnum.VisitInterest : PaymentAllocationTypeEnum.VisitPrincipal;
                int? financePlanId = this.PaymentFinancePlanVisits.ContainsKey(paymentVisit.VisitId) ? (int?)this.PaymentFinancePlanVisits[paymentVisit.VisitId].FinancePlanId : null;
                PaymentAllocation visitDistribution = this.CreateOrFindPaymentAllocationForVisit(paymentVisit, paymentEntity, paymentAllocationType, financePlanId);

                //Add allocations
                visitDistribution.ActualAmount += paymentVisitDistribution.ExactAmountToDistributeForVisit;
                visitGroupPaymentAllocationResult.PaymentAllocations.Add(visitDistribution);

                //Add to totals
                visitGroupPaymentAllocationResult.TotalDistributedUnrounded += paymentVisitDistribution.ExactAmountToDistributeForVisit;
                // Purposely adding rounded amounts inside the loop
                // to accurately represent the payment allocation amounts.
                // The amount difference that this creates is expected to be fixed when the method AdjustRoundedAllocationAmounts() is called
                visitGroupPaymentAllocationResult.TotalDistributed += Math.Round(paymentVisitDistribution.ExactAmountToDistributeForVisit, 2);

                //This is the max you can round onto this ScheduledAmount
                decimal amountDue = isInterest ? this.GetInterestDue(paymentVisit.VisitId) : paymentVisit.CurrentBalance;
                visitDistribution.MaxAllocationAmount = amountDue - paymentVisitDistribution.FoundDistributionAmountTotal;
            }

            return visitGroupPaymentAllocationResult;
        }


    }
}
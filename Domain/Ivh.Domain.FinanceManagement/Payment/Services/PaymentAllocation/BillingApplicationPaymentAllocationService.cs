namespace Ivh.Domain.FinanceManagement.Payment.Services.PaymentAllocation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Utilities.Helpers;
    using Visit.Interfaces;
    using Entities;
    using FinancePlan.Entities;
    using FinancePlan.Interfaces;
    using Interfaces;
    using NHibernate;
    using Statement.Interfaces;

    public class BillingApplicationPaymentAllocationService : PaymentAllocationServiceBase
    {

        public override IList<BillingApplication> BillingApplications()
        {
            int hbPriority = this.Client.Value.BillingApplicationHBAllocationPriority;
            int pbPriority = this.Client.Value.BillingApplicationPBAllocationPriority;

            decimal hbPercentage = (decimal)(this.Client.Value.BillingApplicationHBAllocationPercentage * 0.01);
            decimal pbPercentage = (decimal)(this.Client.Value.BillingApplicationPBAllocationPercentage * 0.01);

            return new List<BillingApplication>
            {
                new BillingApplication(BillingApplicationConstants.HB, hbPercentage, hbPriority),
                new BillingApplication(BillingApplicationConstants.PB, pbPercentage, pbPriority)
            };
        }

        /// <inheritdoc />
        /// <summary>
        ///     Distributes payments
        ///     Note: this class is not thread safe.  Use different instances per thread or refactor.
        /// </summary>
        public BillingApplicationPaymentAllocationService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IVisitService> visitService,
            Lazy<IVpStatementService> vpStatementService,
            Lazy<IFinancePlanService> financePlanService,
            Lazy<IPaymentRepository> paymentRepository,
            Lazy<IVisitTransactionRepository> visitTransactionRepository,
            Lazy<IVisitTransactionService> visitTransactionService,
            Lazy<IMerchantAccountAllocationService> merchantAccountAllocationService,
            ISessionContext<VisitPay> sessionContext,
            Lazy<IPaymentAllocationRepository> paymentAllocationRepository,
            Lazy<IPaymentVisitRepository> paymentVisitRepository,
            Lazy<IPaymentFinancePlanRepository> paymentFinancePlanRepository
            )
            : base(serviceCommonService, visitService, vpStatementService, financePlanService, paymentRepository, visitTransactionRepository, visitTransactionService, merchantAccountAllocationService, sessionContext, paymentAllocationRepository, paymentVisitRepository, paymentFinancePlanRepository)
        {
        }

        protected override decimal DistributeVisits(Payment paymentEntity, IList<PaymentVisit> orderedVisitsList, decimal maxToDistribute, AllocationType allocationType, IDictionary<PaymentVisit, decimal> maxPerVisit = null, FinancePlan financePlan = null)
        {
            if (maxToDistribute <= 0m)
            {
                return 0;
            }

            IDictionary<int, decimal> unclearedAllocations = new Dictionary<int, decimal>();
            bool isPrincipal = allocationType == AllocationType.Principal;
            bool isInterest = !isPrincipal;

            //Sum of all the current balances with the currently distributed amounts removed
            IDictionary<string, decimal> totalForAllCurrentBalancesWithoutNewlyDistributed = this.GetTotalForAllCurrentBalancesWithoutNewlyDistributed(paymentEntity, orderedVisitsList, unclearedAllocations, maxPerVisit, isInterest);

            if (totalForAllCurrentBalancesWithoutNewlyDistributed.Values.Sum() <= 0m)
            {
                return 0m;
            }

            DistributionInfo distributionInfo = this.CalculateDistributionInfo(maxToDistribute, totalForAllCurrentBalancesWithoutNewlyDistributed);

            List<BillingApplicationPaymentVisit> joinedVisitList = orderedVisitsList
                .Join(
                    this.BillingApplications()
                        .Where(x => totalForAllCurrentBalancesWithoutNewlyDistributed[x.Name] > 0m)
                    , o => o.BillingApplication
                    , i => i.Name
                    , (o, i) => new BillingApplicationPaymentVisit { PaymentVisit = o, BillingApplication = i }
                ).ToList();

            List<IGrouping<string, BillingApplicationPaymentVisit>> prioritizedVisitGroups = joinedVisitList
                 .GroupBy(x => x.BillingApplication.Name)
                 .OrderBy(x => this.BillingApplications().First(y => y.Name == x.Key).Priority)
                 .ToList();

            IList<PaymentAllocation> paymentAllocations = new List<PaymentAllocation>();
            IDictionary<string, decimal> totalDistributed = this.BillingApplications().ToDictionary(k => k.Name, v => 0m);
            IDictionary<string, decimal> totalDistributedUnrounded = this.BillingApplications().ToDictionary(k => k.Name, v => 0m);

            //Grouped by BillingApplication.Priority and BillingApplication.Name
            foreach (IGrouping<string, BillingApplicationPaymentVisit> visitsGroup in prioritizedVisitGroups)
            {
                string key = visitsGroup.Key;
                distributionInfo.CalculatedMaxToDistributeByVisitType[key] += distributionInfo.CarryOver;

                List<PaymentVisit> billingApplicationPrioritizedPaymentVisits = visitsGroup.Select(x => x.PaymentVisit).ToList();
                List<List<PaymentVisit>> visitGroupsList = this.GroupVisitsByPriority(billingApplicationPrioritizedPaymentVisits);

                bool hasPaymentAllocationPriorities = this.Client.Value.PaymentAllocationOrderedPriorityGrouping.Any();
                bool payInFull = isPrincipal && hasPaymentAllocationPriorities;

                foreach (List<PaymentVisit> paymentVisitListGroup in visitGroupsList)
                {
                    VisitGroupPaymentAllocationResult visitGroupPaymentAllocationResult = this.AllocateDistributionsToVisits(
                        paymentEntity,
                        paymentVisitListGroup,
                        unclearedAllocations,
                        maxPerVisit,
                        distributionInfo,
                        key,
                        totalForAllCurrentBalancesWithoutNewlyDistributed[key],
                        payInFull,
                        isInterest
                    );

                    totalDistributedUnrounded[key] += visitGroupPaymentAllocationResult.TotalDistributedUnrounded;
                    totalDistributed[key] += visitGroupPaymentAllocationResult.TotalDistributed;

                    paymentAllocations.AddRange(visitGroupPaymentAllocationResult.PaymentAllocations);

                    decimal totalAmountDistributed = totalDistributed[key];
                    decimal maxAllowedToDistribute = distributionInfo.CalculatedMaxToDistributeByVisitType[key];
                    bool outOfMoney = totalAmountDistributed >= maxAllowedToDistribute;
                    if (outOfMoney)
                    {
                        break;
                    }
                }
            }

            decimal totalDistributedUnroundedTotal = totalDistributedUnrounded.Values.Sum();
            decimal totalDistributedTotal = totalDistributed.Values.Sum();

            // This will fix the allocation amounts to add up to the payment amount
            // in case we are under/over due to rounded values being assigned to each payment allocation amount
            this.AdjustRoundedAllocationAmounts(paymentAllocations, totalDistributedUnroundedTotal, ref totalDistributedTotal);

            //Add all distributions that were generated to the payment.
            this.AddGeneratedPaymentAllocations(paymentEntity, paymentAllocations);

            return totalDistributedTotal;
        }

        /// <summary>
        /// This will distribute blindly using the maxPerVisit dictionary, doesn't take current balance, uncleared payments, interest or anything into account. YOU HAVE BEEN WARNED.
        /// </summary>
        protected override decimal DistributeVisitsUsingMaxPerVisit(Payment paymentEntity, IList<PaymentVisit> orderedVisitsList, decimal maxToDistribute, AllocationType allocationType, IDictionary<PaymentVisit, decimal> maxPerVisit, int? financePlanId = null)
        {
            bool hasMaxPerVisit = maxPerVisit != null;
            if (!hasMaxPerVisit)
            {
                return 0m;
            }
            IDictionary<string, decimal> totalForAllCurrentBalancesWithoutNewlyDistributed = null;
            bool isInterest = allocationType != AllocationType.Principal;

            //Sum all of the current balances

            IDictionary<string, decimal> orderedPrincipalSum = new Dictionary<string, decimal>();
            foreach (BillingApplication billingApplication in this.BillingApplications())
            {
                if (!orderedPrincipalSum.ContainsKey(billingApplication.Name))
                {
                    orderedPrincipalSum.Add(billingApplication.Name, 0m);
                }

                foreach (PaymentVisit paymentVisit in orderedVisitsList.Where(x => x.BillingApplication == billingApplication.Name))
                {
                    if (maxPerVisit.ContainsKey(paymentVisit))
                    {
                        orderedPrincipalSum[billingApplication.Name] += maxPerVisit[paymentVisit];
                    }
                }
            }
            totalForAllCurrentBalancesWithoutNewlyDistributed = orderedPrincipalSum;

            DistributionInfo distributionInfo = this.CalculateDistributionInfo(maxToDistribute, totalForAllCurrentBalancesWithoutNewlyDistributed);

            IList<PaymentAllocation> paymentAllocations = new List<PaymentAllocation>();
            var groupedOrderedVisitsList = orderedVisitsList.Join(this.BillingApplications(), o => o.BillingApplication, i => i.Name, (o, i) => new { Visit = o, BillingApplication = i }).GroupBy(x => x.BillingApplication.Name);

            IDictionary<string, decimal> totalDistributed = this.BillingApplications().ToDictionary(k => k.Name, v => 0m);
            IDictionary<string, decimal> totalDistributedUnrounded = this.BillingApplications().ToDictionary(k => k.Name, v => 0m);

            foreach (var billingApplicationVisits in groupedOrderedVisitsList.OrderBy(x => this.BillingApplications().First(y => y.Name == x.Key).Priority))
            {
                string key = billingApplicationVisits.First().BillingApplication.Name;
                distributionInfo.CalculatedMaxToDistributeByVisitType[key] += distributionInfo.CarryOver;

                foreach (PaymentVisit paymentVisit in billingApplicationVisits.Select(x => x.Visit))
                {
                    decimal maxPerVisitTotal = totalForAllCurrentBalancesWithoutNewlyDistributed[key];
                    bool hasMaxPerVisitAmount = maxPerVisit.ContainsKey(paymentVisit);
                    decimal maxPerVisitAmount = hasMaxPerVisitAmount ? maxPerVisit[paymentVisit] : 0m;
                    decimal maxPerVisitPercentage = maxPerVisitAmount / maxPerVisitTotal;
                    decimal exactAmountToDistributeForVisit = distributionInfo.CalculatedMaxToDistributeByVisitType[key] * maxPerVisitPercentage;

                    distributionInfo.AmountDistributedByVisitType[key] += exactAmountToDistributeForVisit;

                    //Create Allocations
                    PaymentAllocationTypeEnum paymentAllocationType = isInterest ? PaymentAllocationTypeEnum.VisitInterest : PaymentAllocationTypeEnum.VisitPrincipal;
                    PaymentAllocation visitDistribution = this.CreateOrFindPaymentAllocationForVisit(paymentVisit, paymentEntity, paymentAllocationType, financePlanId);

                    //Add allocations
                    visitDistribution.ActualAmount += exactAmountToDistributeForVisit;
                    paymentAllocations.Add(visitDistribution);

                    //Add to totals
                    totalDistributedUnrounded[key] += exactAmountToDistributeForVisit;
                    // Purposely adding rounded amounts inside the loop
                    // to accurately represent the payment allocation amounts.
                    // The amount difference that this creates is expected to be fixed when the method AdjustRoundedAllocationAmounts() is called
                    totalDistributed[key] += Math.Round(exactAmountToDistributeForVisit, 2);

                    //This is the max you can round onto this ScheduledAmount
                    visitDistribution.MaxAllocationAmount = maxPerVisit[paymentVisit];
                }
            }

            decimal totalDistributedUnroundedTotal = totalDistributedUnrounded.Values.Sum();
            decimal totalDistributedTotal = totalDistributed.Values.Sum();

            this.AdjustRoundedAllocationAmounts(paymentAllocations, totalDistributedUnroundedTotal, ref totalDistributedTotal);

            //Add all distributions that were generated to the payment.
            this.AddGeneratedPaymentAllocations(paymentEntity, paymentAllocations);

            //Apply the pennies that were rounded onto the items into the total.
            return totalDistributedTotal;
        }

        private VisitGroupPaymentAllocationResult AllocateDistributionsToVisits(
            Payment paymentEntity,
            List<PaymentVisit> paymentVisits,
            IDictionary<int, decimal> unclearedAllocations,
            IDictionary<PaymentVisit, decimal> maxPerVisit,
            DistributionInfo distributionInfo,
            string key,
            decimal totalForAllCurrentBalancesWithoutNewlyDistributed,
            bool payInFull,
            bool isInterest)
        {

            decimal calculatedMaxToDistributeByVisitType = distributionInfo.AvailableAmountToDistributeByVisitType(key);

            // Need to manipulate the amounts for MaxToDistribute and TotalBalances
            // so that they will allow maximum payment (via pro rata) for the group of PaymentVisits 
            if (payInFull)
            {
                IDictionary<string, decimal> totalForGroupedCurrentBalancesWithoutNewlyDistributed = this.GetTotalForAllCurrentBalancesWithoutNewlyDistributed(paymentEntity, paymentVisits, unclearedAllocations, maxPerVisit, isInterest);
                totalForAllCurrentBalancesWithoutNewlyDistributed = totalForGroupedCurrentBalancesWithoutNewlyDistributed.Sum(x => x.Value);
                calculatedMaxToDistributeByVisitType = Math.Min(calculatedMaxToDistributeByVisitType, totalForAllCurrentBalancesWithoutNewlyDistributed);
            }

            VisitGroupPaymentAllocationResult visitGroupPaymentAllocationResult = new VisitGroupPaymentAllocationResult();

            if (calculatedMaxToDistributeByVisitType <= 0m)
            {
                return visitGroupPaymentAllocationResult;
            }

            foreach (PaymentVisit paymentVisit in paymentVisits)
            {
                PaymentVisitDistribution paymentVisitDistribution = this.GetPaymentVisitDistribution(
                    paymentEntity,
                    paymentVisit,
                    unclearedAllocations,
                    maxPerVisit,
                    calculatedMaxToDistributeByVisitType,
                    totalForAllCurrentBalancesWithoutNewlyDistributed,
                    isInterest
                );

                distributionInfo.AmountDistributedByVisitType[key] += paymentVisitDistribution.ExactAmountToDistributeForVisit;

                //Create Allocations
                PaymentAllocationTypeEnum paymentAllocationType = isInterest ? PaymentAllocationTypeEnum.VisitInterest : PaymentAllocationTypeEnum.VisitPrincipal;
                int? financePlanId = this.PaymentFinancePlanVisits.ContainsKey(paymentVisit.VisitId) ? (int?)this.PaymentFinancePlanVisits[paymentVisit.VisitId].FinancePlanId : null;
                PaymentAllocation visitDistribution = this.CreateOrFindPaymentAllocationForVisit(paymentVisit, paymentEntity, paymentAllocationType, financePlanId);

                //Add allocations
                visitDistribution.ActualAmount += paymentVisitDistribution.ExactAmountToDistributeForVisit;
                visitGroupPaymentAllocationResult.PaymentAllocations.Add(visitDistribution);

                //Add to totals
                visitGroupPaymentAllocationResult.TotalDistributedUnrounded += paymentVisitDistribution.ExactAmountToDistributeForVisit;
                // Purposely adding rounded amounts inside the loop
                // to accurately represent the payment allocation amounts.
                // The amount difference that this creates is expected to be fixed when the method AdjustRoundedAllocationAmounts() is called
                visitGroupPaymentAllocationResult.TotalDistributed += Math.Round(paymentVisitDistribution.ExactAmountToDistributeForVisit, 2);

                //This is the max you can round onto this ScheduledAmount
                decimal amountDue = isInterest ? this.GetInterestDue(paymentVisit.VisitId) : paymentVisit.CurrentBalance;
                visitDistribution.MaxAllocationAmount = amountDue - paymentVisitDistribution.FoundDistributionAmountTotal;
            }

            return visitGroupPaymentAllocationResult;
        }

        protected override IDictionary<string, decimal> GetTotalForAllCurrentBalancesWithoutNewlyDistributed(
            Payment paymentEntity,
            IList<PaymentVisit> visitsList,
            IDictionary<int, decimal> unclearedAllocations,
            IDictionary<PaymentVisit, decimal> maxPerVisit,
            bool isInterest
            )
        {
            IDictionary<string, decimal> balances = new Dictionary<string, decimal>();
            HashSet<int> visitIdHashSet = visitsList.Select(y => y.VisitId).ToHashSet();

            this.InitBalances(maxPerVisit, ref balances);
            this.GetCurrentBalances(visitsList, unclearedAllocations, maxPerVisit, balances, isInterest);
            this.RemoveCurrentlyDistributedValues(paymentEntity, visitIdHashSet, balances, isInterest);

            return balances;
        }

        private void InitBalances(IDictionary<PaymentVisit, decimal> maxPerVisit, ref IDictionary<string, decimal> balances)
        {
            foreach (BillingApplication billingApplication in this.BillingApplications())
            {
                if (!balances.ContainsKey(billingApplication.Name))
                {
                    balances.Add(billingApplication.Name, 0m);
                }
            }

            bool hasMaxPerVisits = maxPerVisit != null;
            if (hasMaxPerVisits)
            {
                balances = balances.ToDictionary(k => k.Key, v => 0m);
            }
        }

        private void GetCurrentBalances(
            IList<PaymentVisit> visitsList,
            IDictionary<int, decimal> unclearedAllocations,
            IDictionary<PaymentVisit, decimal> maxPerVisit,
            IDictionary<string, decimal> balances,
            bool isInterest)
        {
            bool hasMaxPerVisits = maxPerVisit != null;
            foreach (PaymentVisit paymentVisit in visitsList)
            {
                bool hasMaxPerVisit = hasMaxPerVisits && maxPerVisit.ContainsKey(paymentVisit);
                decimal maxPerVisitAmount = hasMaxPerVisit ? maxPerVisit[paymentVisit] : 0m;
                decimal amountDue = 0m;

                if (isInterest)
                {
                    amountDue = this.GetInterestDue(paymentVisit.VisitId);
                }
                else
                {
                    decimal unclearedPaymentAmount = unclearedAllocations.GetValue(paymentVisit.VisitId);
                    amountDue = paymentVisit.CurrentBalance;
                    amountDue += unclearedPaymentAmount;
                }

                if (hasMaxPerVisit && maxPerVisitAmount < amountDue)
                {
                    balances[paymentVisit.BillingApplication] += maxPerVisitAmount;
                }
                else
                {
                    balances[paymentVisit.BillingApplication] += amountDue;
                }
            }
        }

        private void RemoveCurrentlyDistributedValues(Payment paymentEntity, HashSet<int> visitIdHashSet, IDictionary<string, decimal> balances, bool isInterest)
        {
            IEnumerable<IGrouping<string, PaymentAllocation>> paymentBillingApplicationGroups = this.GetPaymentDistributionBillingApplicationGroups(paymentEntity, visitIdHashSet, isInterest);
            foreach (IGrouping<string, PaymentAllocation> paymentAllocationGroup in paymentBillingApplicationGroups)
            {
                balances[paymentAllocationGroup.Key] -= paymentAllocationGroup.Sum(x => x.ActualAmount);
            }

            IEnumerable<IGrouping<string, PaymentAllocation>> alreadyAllocatedBillingApplicationGroups = this.GetAlreadyAllocatedDistributionBillingApplicationGroups(paymentEntity, visitIdHashSet, isInterest);
            if (alreadyAllocatedBillingApplicationGroups != null)
            {
                foreach (IGrouping<string, PaymentAllocation> paymentAllocationGroup in alreadyAllocatedBillingApplicationGroups)
                {
                    balances[paymentAllocationGroup.Key] -= paymentAllocationGroup.Sum(x => x.ActualAmount);
                }
            }
        }

        private DistributionInfo CalculateDistributionInfo(decimal maxToDistribute, IDictionary<string, decimal> balances)
        {
            decimal calculatedMaxToDistributeForAllVisitTypes = 0m;

            if (maxToDistribute > balances.Values.Sum())
            {
                calculatedMaxToDistributeForAllVisitTypes = balances.Values.Sum();
            }
            else
            {
                calculatedMaxToDistributeForAllVisitTypes = maxToDistribute;
            }

            IDictionary<string, decimal> calculatedMaxToDistributeByVisitType = this.GetMaxToDistributeByVisitType(calculatedMaxToDistributeForAllVisitTypes);
            this.FixRoundingErrors(calculatedMaxToDistributeByVisitType, calculatedMaxToDistributeForAllVisitTypes);

            IDictionary<string, decimal> amountDistributedByVisitType = this.BillingApplications().ToDictionary(k => k.Name, v => 0m);

            decimal carryOver = this.AdjustActualVisitSum(calculatedMaxToDistributeByVisitType, balances, amountDistributedByVisitType);
            this.FixRoundingErrors(calculatedMaxToDistributeByVisitType, calculatedMaxToDistributeForAllVisitTypes);

            DistributionInfo distributionInfo = new DistributionInfo()
            {
                CalculatedMaxToDistributeByVisitType = calculatedMaxToDistributeByVisitType,
                AmountDistributedByVisitType = amountDistributedByVisitType,
                CarryOver = carryOver
            };
            return distributionInfo;
        }

        /// <summary>
        /// Gets the base visit type distribution based on weightings
        /// </summary>
        /// <param name="maxToDistribute"></param>
        /// <returns></returns>
        private IDictionary<string, decimal> GetMaxToDistributeByVisitType(decimal maxToDistribute)
        {
            IDictionary<string, decimal> calculatedMaxToDistributeByVisitType = this.BillingApplications()
                .Select(x => new
                {
                    BillingApplication = x.Name,
                    MaxToDistribute = Math.Round(x.Percent * maxToDistribute, 2)
                })
                .ToDictionary(k => k.BillingApplication, v => v.MaxToDistribute);
            return calculatedMaxToDistributeByVisitType;
        }

        /// <summary>
        /// Adjust for the actual visit sum needed for each type
        /// </summary>
        /// <param name="calculatedMaxToDistributeByVisitType"></param>
        /// <param name="totalForAllCurrentBalancesWithoutNewlyDistributed"></param>
        /// <param name="amountDistributedByVisitType"></param>
        /// <returns></returns>
        private decimal AdjustActualVisitSum(
            IDictionary<string, decimal> calculatedMaxToDistributeByVisitType,
            IDictionary<string, decimal> totalForAllCurrentBalancesWithoutNewlyDistributed,
            IDictionary<string, decimal> amountDistributedByVisitType)
        {
            decimal carryOver = 0m;
            decimal AvailableToDistribute(string k) => calculatedMaxToDistributeByVisitType[k] - amountDistributedByVisitType[k];

            if (this.BillingApplications().Count > 1)
            {
                //first create custom sort order: lowest priority excess goes to highest priority, then highest priority excess goes to next highest, etc
                // e.g., 10 => 0, 0 => 1, 1 => 2, 2 => 3....9 => 10.
                int lowestPriority = this.BillingApplications().Select(x => x.Priority).Max();
                IList<string> customSort = this.BillingApplications().OrderBy(x => x.Priority == lowestPriority ? x.Priority * -1 : x.Priority).Select(x => x.Name).ToList();
                customSort.Add(customSort.First());//add the first one in again at the end.

                foreach (string key in customSort)
                {
                    calculatedMaxToDistributeByVisitType[key] += carryOver;
                    carryOver = 0m;
                    if (totalForAllCurrentBalancesWithoutNewlyDistributed.ContainsKey(key)
                        && totalForAllCurrentBalancesWithoutNewlyDistributed[key] < AvailableToDistribute(key))
                    {
                        //we need less than is available
                        carryOver = calculatedMaxToDistributeByVisitType[key] - totalForAllCurrentBalancesWithoutNewlyDistributed[key];
                        calculatedMaxToDistributeByVisitType[key] = totalForAllCurrentBalancesWithoutNewlyDistributed[key];
                    }
                }
            }

            return carryOver;
        }

        private void FixRoundingErrors(IDictionary<string, decimal> visitTypeAmounts, decimal total)
        {
            bool subtractionNeeded = visitTypeAmounts.Sum(x => x.Value) > total;
            bool additionNeeded = visitTypeAmounts.Sum(x => x.Value) < total;

            if (subtractionNeeded || additionNeeded)
            {
                decimal appliedAmount = subtractionNeeded ? -0.01m : 0.01m;
                List<BillingApplication> orderedBillingApplications = subtractionNeeded
                    ? this.BillingApplications().OrderByDescending(x => x.Priority).ToList()
                    : this.BillingApplications().OrderBy(x => x.Priority).ToList();

                int iterationCount = 0;
                while (true)
                {
                    foreach (BillingApplication billingApplication in orderedBillingApplications)
                    {
                        visitTypeAmounts[billingApplication.Name] += appliedAmount;

                        if (visitTypeAmounts.Sum(x => x.Value) == total)
                        {
                            break;
                        }
                    }
                    if (visitTypeAmounts.Sum(x => x.Value) == total)
                    {
                        break;
                    }

                    iterationCount++;
                    if (iterationCount > 50)
                    {
                        break;
                    }
                }
            }
        }

        internal class BillingApplicationPaymentVisit
        {
            public PaymentVisit PaymentVisit { get; set; }
            public BillingApplication BillingApplication { get; set; }
        }

        internal class DistributionInfo
        {
            public IDictionary<string, decimal> CalculatedMaxToDistributeByVisitType { get; set; }
            public IDictionary<string, decimal> AmountDistributedByVisitType { get; set; }
            public decimal CarryOver { get; set; }

            public decimal AvailableAmountToDistributeByVisitType(string key)
            {
                return this.CalculatedMaxToDistributeByVisitType[key] - this.AmountDistributedByVisitType[key];
            }
        }

    }
}
namespace Ivh.Domain.FinanceManagement.Payment.Services.PaymentAllocation
{
    using Common.Base.Utilities.Extensions;
    using Visit.Interfaces;
    using Entities;
    using FinancePlan.Entities;
    using FinancePlan.Interfaces;
    using Interfaces;
    using NHibernate;
    using Statement.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Base.Interfaces;
    using Base.Services;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.HospitalData;
    using Discount.Entities;
    using Newtonsoft.Json;

    public abstract class PaymentAllocationServiceBase : DomainService, IPaymentAllocationService
    {
        protected const string ActualAmountPropertyName = "ActualAmount";
        protected const string MaxAllocationAmountPropertyName = "MaxAllocationAmount";

        //Cached for the lifetime of this service.
        protected readonly IDictionary<int, HashSet<int>> FinancePlanBuckets = new Dictionary<int, HashSet<int>>();
        protected readonly IDictionary<int, IDictionary<int, decimal>> FinancePlanBucketWithPaymentAmounts = new Dictionary<int, IDictionary<int, decimal>>();
        protected readonly IList<PaymentAllocationVisitAmountResult> UnclearedPaymentAllocations = new List<PaymentAllocationVisitAmountResult>();
        protected readonly IDictionary<int, PaymentFinancePlanVisit> PaymentFinancePlanVisits = new Dictionary<int, PaymentFinancePlanVisit>();

        protected readonly Lazy<IFinancePlanService> FinancePlanService;
        protected readonly Lazy<IPaymentRepository> PaymentRepository;
        protected readonly Lazy<IPaymentVisitRepository> PaymentVisitRepository;
        protected readonly Lazy<IPaymentFinancePlanRepository> PaymentFinancePlanRepository;

        protected readonly Lazy<IVisitService> VisitService;
        protected readonly Lazy<IVisitTransactionRepository> VisitTransactionRepository;
        protected readonly Lazy<IVpStatementService> VpStatementService;
        protected readonly Lazy<IVisitTransactionService> VisitTransactionService;
        protected readonly Lazy<IMerchantAccountAllocationService> MerchantAccountAllocationService;

        private readonly ISession _session;
        private readonly Lazy<IPaymentAllocationRepository> _paymentAllocationRepository;

        /// <summary>
        ///     Distributes payments
        ///     Note: this class is not thread safe.  Use different instances per thread or refactor.
        /// </summary>
        protected PaymentAllocationServiceBase(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IVisitService> visitService,
            Lazy<IVpStatementService> vpStatementService,
            Lazy<IFinancePlanService> financePlanService,
            Lazy<IPaymentRepository> paymentRepository,
            Lazy<IVisitTransactionRepository> visitTransactionRepository,
            Lazy<IVisitTransactionService> visitTransactionService,
            Lazy<IMerchantAccountAllocationService> merchantAccountAllocationService,
            ISessionContext<VisitPay> sessionContext,
            Lazy<IPaymentAllocationRepository> paymentAllocationRepository,
            Lazy<IPaymentVisitRepository> paymentVisitRepository,
            Lazy<IPaymentFinancePlanRepository> paymentFinancePlanRepository
            ) : base(serviceCommonService)
        {
            this.VisitService = visitService;
            this.VpStatementService = vpStatementService;
            this.FinancePlanService = financePlanService;
            this.PaymentRepository = paymentRepository;
            this.VisitTransactionRepository = visitTransactionRepository;
            this.VisitTransactionService = visitTransactionService;
            this.MerchantAccountAllocationService = merchantAccountAllocationService;
            this._session = sessionContext.Session;
            this._paymentAllocationRepository = paymentAllocationRepository;
            this.PaymentVisitRepository = paymentVisitRepository;
            this.PaymentFinancePlanRepository = paymentFinancePlanRepository;
        }

        protected Dictionary<Payment, PaymentAllocationResult> PaymentAllocationResults = new Dictionary<Payment, PaymentAllocationResult>();

        public void RemovePaymentAllocation(Payment paymentEntity)
        {
            this._paymentAllocationRepository.Value.RemovePaymentAllocations(paymentEntity);
        }

        public IList<PaymentAllocationResult> AllocatePayments(IList<Payment> paymentEntities)
        {
            this.PaymentAllocationResults = new Dictionary<Payment, PaymentAllocationResult>();
            foreach (Payment paymentEntity in paymentEntities)
            {
                this.AllocatePayment(paymentEntity);
            }
            return this.PaymentAllocationResults.Values.ToList();
        }

        public PaymentAllocationResult AllocatePayment(Payment paymentEntity)
        {
            this.PaymentAllocationResults[paymentEntity] = new PaymentAllocationResult() { Payment = paymentEntity };

            try
            {
                //Shouldn't have any previous distributions.
                this.CreateDistributionsForVisitDiscounts(paymentEntity);

                //Past due interest (FinancePlans)
                //Past due Principal (FinancePlans)
                //past due visits
                //current interest (FinancePlans) 
                //current principal (FinancePlans)
                //remainder of visits, pro-rata
                decimal totalDistributed = 0m;
                if (paymentEntity.IsSpecificVisitsType() || paymentEntity.IsCurrentNonFinancedBalanceInFullType())
                {
                    totalDistributed = this.AllocateSpecificVisitTypePayment(paymentEntity);
                }
                else if (paymentEntity.IsCurrentBalanceInFullType())
                {
                    totalDistributed = this.AllocateCurrentBalanceInFullTypeAllocationPayment(paymentEntity);
                }
                else if (paymentEntity.IsSpecificAmountType())
                {
                    totalDistributed = this.AllocateSpecificAmountTypeAllocationPayment(paymentEntity);
                }
                else if (paymentEntity.IsSpecificFinancePlansType()) // this also handles RecurringFinancePlan
                {
                    totalDistributed = this.AllocateSpecificFinancePlansTypeAllocationPayment(paymentEntity);
                }
                this.RemoveAllZeroDistributions(paymentEntity);
                this.SumAllDistributionsAndFinishSetupOnPayment(paymentEntity);
                this.EnsureFinancePlanIds(paymentEntity);
                this.SetOriginalAmountOnAllocations(this.PaymentAllocationResults[paymentEntity].Allocations);

                return this.PaymentAllocationResults[paymentEntity];
            }
            finally
            {
                this.FinancePlanBuckets.Clear();
                this.FinancePlanBucketWithPaymentAmounts.Clear();
                this.UnclearedPaymentAllocations.Clear();
                this.PaymentFinancePlanVisits.Clear();
            }
        }

        private void SetOriginalAmountOnAllocations(IList<PaymentAllocation> paymentEntityPaymentAllocations)
        {
            foreach (PaymentAllocation paymentEntityPaymentAllocation in paymentEntityPaymentAllocations)
            {
                paymentEntityPaymentAllocation.OriginalAmount = paymentEntityPaymentAllocation.ActualAmount;
            }
        }

        public PaymentAllocationResult AllocatePaymentReverse(Payment originalPaymentEntity, Payment reversalPaymentEntity)
        {
            this.PaymentAllocationResults[reversalPaymentEntity] = new PaymentAllocationResult() { Payment = reversalPaymentEntity };

            //Find all original allocations.
            IList<PaymentReallocationNode> allocationNodes = this.CreateReallocationNodes(reversalPaymentEntity, originalPaymentEntity, true, true);
            IList<PaymentReallocationNode> discountNodes = this.CreateReallocationNodes(reversalPaymentEntity, originalPaymentEntity, false, true);
            IList<PaymentReallocationNode> interestNodes = this.CreateReallocationNodes(reversalPaymentEntity, originalPaymentEntity, true, false);

            //assumptions
            //Original payment has many reversal payments
            //all reversal payments can only negate the amount in the original payment
            decimal maxAmountToDistribute = allocationNodes.Sum(x => x.AmountAvailable) + interestNodes.Sum(x => x.AmountAvailable);
            bool isRefundAmountHigherThanPaymentAmount = Math.Abs(reversalPaymentEntity.ScheduledPaymentAmount) > originalPaymentEntity.ActualPaymentAmount;

            if (Math.Abs(reversalPaymentEntity.ScheduledPaymentAmount) > Math.Abs(maxAmountToDistribute))
            {
                if (isRefundAmountHigherThanPaymentAmount)
                {
                    throw new Exception("Too much is getting reversed!");
                }
                else
                {
                    this.LoggingService.Value.Warn(() => "PaymentAllocationServiceBase::AllocatePaymentReverse() PaymentId {0} reversal amount is higher than maxAmountToDistribute. reversalPaymentEntity.ScheduledPaymentAmount = {1:0.00}, maxAmountToDistribute = {2:0.00}"
                        , originalPaymentEntity.PaymentId, reversalPaymentEntity.ScheduledPaymentAmount, maxAmountToDistribute);
                }
            }

            decimal amountToDistributeToPrincipal = reversalPaymentEntity.ScheduledPaymentAmount;
            if (allocationNodes.Sum(x => x.AmountAvailable) > amountToDistributeToPrincipal)
            {
                amountToDistributeToPrincipal = allocationNodes.Sum(x => x.AmountAvailable);
            }

            //Reverse distribute principal first
            this.ReverseDistributeNodes(reversalPaymentEntity, allocationNodes, amountToDistributeToPrincipal);

            decimal ratio = reversalPaymentEntity.ScheduledPaymentAmount / maxAmountToDistribute;
            this.ReverseDistributeNodes(reversalPaymentEntity, discountNodes, Math.Round(ratio * discountNodes.Sum(x => x.AmountAvailable), 2));

            //distribute the remainder to interest if possible
            decimal amountRemaining = reversalPaymentEntity.ScheduledPaymentAmount - allocationNodes.Sum(x => x.Allocation.ActualAmount);
            this.ReverseDistributeNodes(reversalPaymentEntity, interestNodes, amountRemaining);

            this.PaymentAllocationResults[reversalPaymentEntity].ActualPaymentAmount = this.PaymentAllocationResults[reversalPaymentEntity].Allocations
                .Where(x => x.PaymentAllocationType != PaymentAllocationTypeEnum.VisitDiscount)
                .Sum(x => x.ActualAmount);
            this.RemoveAllZeroDistributions(reversalPaymentEntity);

            return this.PaymentAllocationResults[reversalPaymentEntity];
        }

        public PaymentAllocation GetById(int paymentAllocationId)
        {
            return this._paymentAllocationRepository.Value.GetById(paymentAllocationId);
        }

        protected decimal DistributeFinancePlans_BucketFifo(Payment paymentEntity, IList<FinancePlan> financePlans, decimal maxToDistribute, AllocationType allocationType, bool pastdueOnly, IList<PaymentVisit> paymentVisits = null)
        {
            //If there's nothing to distribute don't do the work.
            if (maxToDistribute <= 0)
            {
                return 0;
            }

            decimal totalDistributed = 0m;
            IList<FinancePlan> plans = new List<FinancePlan>();
            if (financePlans == null)
            {
                IList<FinancePlan> financePlanFromService = this.GetFinancePlansFromService(paymentEntity);
                plans.AddRange(financePlanFromService);
            }
            else
            {
                plans = financePlans;
            }

            paymentVisits = paymentVisits ?? this.PaymentVisitRepository.Value.GetPaymentVisits(plans.SelectMany(x => x.ActiveFinancePlanVisits).Select(y => y.VisitId).ToList());
            this.SetupAllocationCache(paymentVisits);
            this.SetupFinancePlanVisitCache(paymentVisits);

            if (pastdueOnly)
            {
                plans = plans.Where(x => x.IsPastDue()).ToList();
            }

            //Basically: financePlan.PastAmountDue by bucket
            //Then if it's not past due, just pay the visits pro rata

            foreach (FinancePlan financePlan in plans)
            {
                if (!this.FinancePlanBuckets.ContainsKey(financePlan.FinancePlanId))
                {
                    this.FinancePlanBuckets.Add(financePlan.FinancePlanId, financePlan.Buckets.Keys.ToHashSet());
                    this.FinancePlanBucketWithPaymentAmounts.Add(financePlan.FinancePlanId, financePlan.Buckets.ToDictionary(k => k.Key, v => v.Value.Item1));

                    //Adjust the FinancePlanBucketWithPaymentAmounts with the actaul payment amounts so we dont allocate more than user specified.
                    if (paymentEntity.PaymentScheduledAmounts.Any(x => x.PaymentFinancePlan != null)) //Only decrement the bucket amount if there is a finance plan assigned to the PSA
                    {
                        decimal maxAmountPerPaymentScheduledAmounts = paymentEntity.PaymentScheduledAmounts.Where(psa => psa?.PaymentFinancePlan?.FinancePlanId == financePlan.FinancePlanId).Sum(x => x.ScheduledAmount);

                        //In the case where there isnt any buckets and someone is trying to pay on a specific finance plan, add that amount to bucket 0
                        if (this.FinancePlanBucketWithPaymentAmounts[financePlan.FinancePlanId].Count == 0)
                        {
                            this.FinancePlanBucketWithPaymentAmounts[financePlan.FinancePlanId].Add(0, maxAmountPerPaymentScheduledAmounts);
                        }

                        if (this.FinancePlanBucketWithPaymentAmounts[financePlan.FinancePlanId].Sum(x => x.Value) > maxAmountPerPaymentScheduledAmounts)
                        {
                            foreach (KeyValuePair<int, decimal> keyValuePair in this.FinancePlanBucketWithPaymentAmounts[financePlan.FinancePlanId].OrderByDescending(x => x.Key))
                            {
                                if (maxAmountPerPaymentScheduledAmounts > keyValuePair.Value)
                                {
                                    //leave it alone and decrement
                                    maxAmountPerPaymentScheduledAmounts -= keyValuePair.Value;
                                }
                                else
                                {
                                    this.FinancePlanBucketWithPaymentAmounts[financePlan.FinancePlanId][keyValuePair.Key] = maxAmountPerPaymentScheduledAmounts;
                                    maxAmountPerPaymentScheduledAmounts = 0m;
                                }
                            }
                        }
                        else if (maxAmountPerPaymentScheduledAmounts > this.FinancePlanBucketWithPaymentAmounts[financePlan.FinancePlanId].Sum(x => x.Value))
                        {
                            //When there's more in the scheduledAmounts add it to the first bucket
                            if (this.FinancePlanBucketWithPaymentAmounts[financePlan.FinancePlanId].Keys.Any())
                            {
                                int key = this.FinancePlanBucketWithPaymentAmounts[financePlan.FinancePlanId].Keys.First();
                                this.FinancePlanBucketWithPaymentAmounts[financePlan.FinancePlanId][key] = maxAmountPerPaymentScheduledAmounts;
                            }
                        }
                    }
                    else
                    {
                        //Otherwise just put the max into the first bucket.
                        if (this.FinancePlanBucketWithPaymentAmounts[financePlan.FinancePlanId].Keys.Any())
                        {
                            int key = this.FinancePlanBucketWithPaymentAmounts[financePlan.FinancePlanId].Keys.First();
                            this.FinancePlanBucketWithPaymentAmounts[financePlan.FinancePlanId][key] = paymentEntity.PaymentScheduledAmounts.Sum(x => x.ScheduledAmount);
                        }
                    }
                }
            }

            IList<int> distinctBuckets = plans.SelectMany(x => x.Buckets).Select(x => x.Key).Distinct().OrderByDescending(x => x).ToList();
            if (!distinctBuckets.Any())
            {
                distinctBuckets.Add(0);
            }

            foreach (int bucket in distinctBuckets)
            {
                //When it's past due only process pastdue buckets
                if (pastdueOnly && bucket <= 0)
                {
                    continue;
                }

                IList<FinancePlan> currentPlans = plans.Where(x => x.IsActive() && (x.Buckets.ContainsKey(bucket) || (bucket == 0 && !x.Buckets.Any())))
                    .OrderByDescending(x => x.IsPastDue())
                    .ThenBy(x => x.InsertDate)
                    .ThenBy(x => x.FinancePlanId)
                    .ToList();

                foreach (FinancePlan financePlan in currentPlans)
                {
                    decimal? bucketResult = null;
                    decimal total = maxToDistribute - totalDistributed;

                    //When we're paying off past due buckets only pay past due amount.
                    //When it's not past due we're just paying off the visits on that fp pro rata.
                    if (this.FinancePlanBucketWithPaymentAmounts.ContainsKey(financePlan.FinancePlanId) &&
                        this.FinancePlanBucketWithPaymentAmounts[financePlan.FinancePlanId].ContainsKey(bucket))
                    {
                        bucketResult = this.FinancePlanBucketWithPaymentAmounts[financePlan.FinancePlanId][bucket];

                        //When the bucket is less than the total we can distribute, use the lesser value which would be the scheduled amount.
                        if (maxToDistribute - totalDistributed > bucketResult.Value)
                        {
                            total = bucketResult.Value;
                        }
                    }

                    if (total <= 0)
                    {
                        continue;
                    }

                    //distribute that total to the visits on this fp prorata
                    //Since it's by prorata this ordering probably doesn't matter.
                    IList<FinancePlanVisit> activeFinancePlanVisits = financePlan.ActiveFinancePlanVisits
                        .Where(x => x.CurrentVisitState == VisitStateEnum.Active && x.CurrentBalance > 0)
                        .ToList();

                    IList<int> orderedVisitFromFpVisitIds = activeFinancePlanVisits.Select(x => x.VisitId)
                        .ToList();

                    IList<PaymentVisit> orderedVisitFromFp = paymentVisits
                        .Where(x => orderedVisitFromFpVisitIds.Contains(x.VisitId))
                        .Where(x => x.CurrentBalance > 0)
                        .Where(x => x.CurrentVisitState == VisitStateEnum.Active)
                        .OrderBy(x => x.CurrentBalance)
                        .ThenBy(x => x.InsertDate)
                        .ThenBy(x => x.VisitId)
                        .ToList();

                    //Since the financePlan has it's own max in the PrincipalAmounts, sending in max for each visit when allocating financePlanPayments
                    IDictionary<PaymentVisit, decimal> maxPerVisit = null;
                    if (allocationType == AllocationType.Principal)
                    {
                        maxPerVisit = activeFinancePlanVisits
                            .ToDictionary(x => orderedVisitFromFp.FirstOrDefault(paymentVisit => paymentVisit.VisitId == x.VisitId), y => y.PrincipalBalance);
                    }
                    decimal amountDistributed = this.DistributeVisits(paymentEntity, orderedVisitFromFp, total, allocationType, maxPerVisit, financePlan);
                    totalDistributed += amountDistributed;
                    if (!bucketResult.HasValue)
                    {
                        continue;
                    }

                    bucketResult -= amountDistributed;
                    this.FinancePlanBucketWithPaymentAmounts[financePlan.FinancePlanId][bucket] = bucketResult.Value;
                }
            }

            return totalDistributed;
        }

        protected decimal DistributeVisitsFifo(Payment paymentEntity, IList<PaymentVisit> orderedPastDueVisitsList, decimal? maxTotalToDistribute, IDictionary<int, decimal> maxValuePerVisitDictionary, AllocationType allocationType, FinancePlan financePlan = null)
        {
            decimal totalDistributed = 0m;

            IList<PaymentAllocation> paymentAllocations = new List<PaymentAllocation>();
            foreach (PaymentVisit visit in orderedPastDueVisitsList)
            {
                if (visit.CurrentBalance <= 0) continue;
                PaymentAllocation visitDistribution = null;
                if (allocationType == AllocationType.Principal)
                {
                    visitDistribution = this.CreateOrFindPaymentAllocationForVisit(visit, paymentEntity, PaymentAllocationTypeEnum.VisitPrincipal, financePlan?.FinancePlanId);
                }
                else
                {
                    visitDistribution = this.CreateOrFindPaymentAllocationForVisit(visit, paymentEntity, PaymentAllocationTypeEnum.VisitInterest, financePlan?.FinancePlanId);
                }
                decimal maxAmount = 0m;

                if (maxTotalToDistribute != null)
                {
                    //Use the total rather than a per visit max
                    maxAmount = maxTotalToDistribute.Value - totalDistributed;
                }
                else if (maxValuePerVisitDictionary != null)
                {
                    maxAmount = maxValuePerVisitDictionary.ContainsKey(visit.VisitId) ? maxValuePerVisitDictionary[visit.VisitId] : 0m;
                }

                //Make sure that adding the max amount isn't going to be greater than the current balance.
                if (allocationType == AllocationType.Principal)
                {
                    decimal unclearedPrincipal = this.UnclearedPaymentAllocations.Where(x => x.VisitId == visit.VisitId && x.PaymentAllocationType == PaymentAllocationTypeEnum.VisitPrincipal).Sum(x => x.ActualAmount);
                    PaymentAllocation discountForVisit = this.PaymentAllocationResults[paymentEntity].Allocations.FirstOrDefault(x => x.PaymentAllocationType == PaymentAllocationTypeEnum.VisitDiscount && x.PaymentVisit.VisitId == visit.VisitId);
                    decimal currentBalanceMinusDiscount = visit.CurrentBalance - (discountForVisit != null ? discountForVisit.ActualAmount : 0m) - unclearedPrincipal;
                    if (maxAmount + visitDistribution.ActualAmount > currentBalanceMinusDiscount)
                    {
                        visitDistribution.ActualAmount = currentBalanceMinusDiscount - visitDistribution.ActualAmount;
                    }
                    else
                    {
                        visitDistribution.ActualAmount += maxAmount;
                    }
                }
                else if (allocationType == AllocationType.Interest)
                {
                    decimal interestAlreadyAllocated = this.PaymentAllocationResults[paymentEntity].Allocations != null ? this.PaymentAllocationResults[paymentEntity].Allocations.Where(x => x.PaymentVisit != null && x.PaymentVisit.VisitId == visit.VisitId && x.PaymentAllocationType == PaymentAllocationTypeEnum.VisitInterest).Sum(x => x.ActualAmount) : 0m;
                    decimal unclearedInterest = this.UnclearedPaymentAllocations.Where(x => x.VisitId == visit.VisitId && x.PaymentAllocationType == PaymentAllocationTypeEnum.VisitInterest).Sum(x => x.ActualAmount);
                    decimal interestDue = 0;
                    if (this.PaymentFinancePlanVisits.ContainsKey(visit.VisitId)
                        && this.PaymentFinancePlanVisits[visit.VisitId] != null)
                    {
                        interestDue = this.PaymentFinancePlanVisits[visit.VisitId].InterestDue;
                    }
                    if (maxAmount + visitDistribution.ActualAmount > (interestDue - interestAlreadyAllocated - unclearedInterest))
                    {
                        visitDistribution.ActualAmount = (interestDue - interestAlreadyAllocated - unclearedInterest) - visitDistribution.ActualAmount;
                    }
                    else
                    {
                        visitDistribution.ActualAmount += maxAmount;
                    }
                }
                paymentAllocations.Add(visitDistribution);

                //Remove the amount distributed from the dictionary
                if (maxValuePerVisitDictionary != null && maxValuePerVisitDictionary.ContainsKey(visit.VisitId))
                {
                    maxValuePerVisitDictionary[visit.VisitId] -= visitDistribution.ActualAmount;
                }
                totalDistributed += visitDistribution.ActualAmount;
            }

            //Add all distributions that were generated to the payment.
            this.AddGeneratedPaymentAllocations(paymentEntity, paymentAllocations, setMaxAllocationAmountToZero: false);
            return totalDistributed;
        }

        protected decimal AllocateSpecificVisitTypePayment(Payment paymentEntity)
        {
            decimal totalDistributed = 0m;
            if (paymentEntity.PaymentScheduledAmounts == null) throw new Exception("Cannot distribute for SpecificVisits without PaymentScheduledAmounts");
            this.SetupAllocationCache(paymentEntity.PaymentScheduledAmounts.Select(vp => vp.PaymentVisit).ToList());
            this.SetupFinancePlanVisitCache(paymentEntity.PaymentScheduledAmounts.Select(vp => vp.PaymentVisit).ToList());

            //We dont distribute onto FP's in this case.
            IList<PaymentVisit> orderedPastDueVisitsList = paymentEntity.PaymentScheduledAmounts.Select(vp => vp.PaymentVisit).Where(x => x != null && x.CurrentVisitState == VisitStateEnum.Active && x.AgingCount > this.ApplicationSettingsService.Value.PastDueAgingCount.Value && x.CurrentBalance > 0)
                .OrderByDescending(x => x.AgingCount)
                .ThenBy(x => x.CurrentBalance)
                .ThenBy(x => x.InsertDate)
                .ThenBy(x => x.VisitId).ToList();
            IDictionary<int, decimal> maxValuePerVisitDictionary = paymentEntity.PaymentScheduledAmounts
                .Where(x => x.PaymentVisit != null)
                .GroupBy(x => x.PaymentVisit.VisitId)
                .ToDictionary(visitPayment => visitPayment.Key, visitPayment => visitPayment.Sum(x => x.ScheduledAmount));

            totalDistributed += this.DistributeVisitsFifo(paymentEntity, orderedPastDueVisitsList, null, maxValuePerVisitDictionary, AllocationType.Interest);
            totalDistributed += this.DistributeVisitsFifo(paymentEntity, orderedPastDueVisitsList, null, maxValuePerVisitDictionary, AllocationType.Principal);

            IList<PaymentVisit> theRestOfTheVisits = paymentEntity.PaymentScheduledAmounts.Select(vp => vp.PaymentVisit).Where(x => x != null && x.CurrentVisitState == VisitStateEnum.Active && !(x.AgingCount > this.ApplicationSettingsService.Value.PastDueAgingCount.Value) && x.CurrentBalance > 0)
                .OrderByDescending(x => x.AgingCount)
                .ThenBy(x => x.CurrentBalance)
                .ThenBy(x => x.InsertDate)
                .ThenBy(x => x.VisitId).ToList();
            totalDistributed += this.DistributeVisitsFifo(paymentEntity, theRestOfTheVisits, null, maxValuePerVisitDictionary, AllocationType.Interest);
            totalDistributed += this.DistributeVisitsFifo(paymentEntity, theRestOfTheVisits, null, maxValuePerVisitDictionary, AllocationType.Principal);

            return totalDistributed;
        }

        private void SetupAllocationCache(IList<PaymentVisit> paymentVisits)
        {
            HashSet<int> availabilityHashSet = this.UnclearedPaymentAllocations.Select(x => x.VisitId).ToHashSet();
            IList<int> newVisitIds = paymentVisits
                .Where(y => !availabilityHashSet.Contains(y.VisitId))
                .Select(vp => vp.VisitId)
                .ToList();

            if (newVisitIds.IsNotNullOrEmpty())
            {
                IList<PaymentAllocationVisitAmountResult> unclearedPaymentAmounts = this.GetAllUnclearedPaymentAmountsForVisits(newVisitIds);
                this.UnclearedPaymentAllocations.AddRange(unclearedPaymentAmounts);
            }
        }

        private void SetupFinancePlanVisitCache(IList<PaymentVisit> paymentVisits)
        {
            HashSet<int> availabilityHashSet = this.PaymentFinancePlanVisits.Keys.ToHashSet();
            IList<PaymentFinancePlanVisit> paymentFinancePlanVisits = this.PaymentFinancePlanRepository.Value.GetFinancePlanVisitsOnActiveFiancePlans(paymentVisits.Where(y => !availabilityHashSet.Contains(y.VisitId)).ToList());

            IDictionary<int, PaymentFinancePlanVisit> local = paymentFinancePlanVisits.ToDictionary(k => k.VisitId, v => v);
            foreach (int localKey in local.Keys)
            {
                if (!this.PaymentFinancePlanVisits.ContainsKey(localKey))
                {
                    this.PaymentFinancePlanVisits.Add(localKey, local[localKey]);
                }
            }
        }

        protected Dictionary<int, decimal> UnclearedAllocationsByType(HashSet<int> visitHashSet, PaymentAllocationTypeEnum type)
        {
            Dictionary<int, decimal> returnDecimals = new Dictionary<int, decimal>();

            foreach (PaymentAllocationVisitAmountResult paymentAllocationVisitAmountResult in this.UnclearedPaymentAllocations.Where(x => visitHashSet.Contains(x.VisitId) && x.PaymentAllocationType == type).ToList())
            {
                if (!returnDecimals.ContainsKey(paymentAllocationVisitAmountResult.VisitId))
                {
                    returnDecimals.Add(paymentAllocationVisitAmountResult.VisitId, 0m);
                }

                returnDecimals[paymentAllocationVisitAmountResult.VisitId] += paymentAllocationVisitAmountResult.ActualAmount * -1m;
            }

            return returnDecimals;
        }

        protected decimal AllocateCurrentBalanceInFullTypeAllocationPayment(Payment paymentEntity)
        {
            decimal totalDistributed = 0m;

            IList<PaymentVisit> allVisits = null;
            IList<PaymentVisit> statementVisits = null;
            IList<FinancePlan> allFinancePlans = null;

            statementVisits = this.PaymentVisitRepository.Value.GetCurrentStatementPaymentVisits(paymentEntity.Guarantor.VpGuarantorId, true);

            if (paymentEntity.IsCurrentBalanceInFullType() || paymentEntity.IsSpecificAmountType())
            {
                allVisits = this.PaymentVisitRepository.Value.GetPaymentVisitsByVpGuarantorId(paymentEntity.Guarantor.VpGuarantorId).Where(x => x.CurrentVisitState == VisitStateEnum.Active).ToList();//this.VisitService.Value.GetAllActiveVisits(paymentEntity.Guarantor);
                allFinancePlans = this.FinancePlanService.Value.GetAllOpenFinancePlansForGuarantor(paymentEntity.Guarantor);
            }

            this.SetupAllocationCache(allVisits);
            this.SetupFinancePlanVisitCache(allVisits);
            if (allVisits == null)
            {
                throw new Exception("Didn't find visits!");
            }

            decimal maxToDistribute = paymentEntity.ScheduledPaymentAmount - totalDistributed;
            //Past due interest (FinancePlans)
            if (allFinancePlans != null)
            {
                IList<FinancePlan> pastDueFinancePlans = allFinancePlans.Where(x => x.IsActiveOriginated() && x.IsPastDue()).OrderBy(x => x.InsertDate).ThenBy(x => x.FinancePlanId).ToList();

                totalDistributed += this.DistributeFinancePlans_BucketFifo(paymentEntity, pastDueFinancePlans, maxToDistribute, AllocationType.Interest, true);
                maxToDistribute = paymentEntity.ScheduledPaymentAmount - totalDistributed;
                //Past due Principal (FinancePlans)
                totalDistributed += this.DistributeFinancePlans_BucketFifo(paymentEntity, pastDueFinancePlans, maxToDistribute, AllocationType.Principal, true);
                maxToDistribute = paymentEntity.ScheduledPaymentAmount - totalDistributed;
            }

            //past due nonfinanced visits
            IList<int> agingCounts = this.PastDueNonFinancedPaymentVisits(allVisits)
                .Select(x => x.AgingCount).Distinct().OrderByDescending(x => x).ToList();

            foreach (int agingCount in agingCounts)
            {
                //Modified closure
                int count = agingCount;
                IList<PaymentVisit> orderedPastDueVisitsList = this.PastDueNonFinancedPaymentVisits(allVisits)
                    .Where(x => x.AgingCount == count)
                    .OrderBy(x => x.CurrentBalance)
                    .ThenBy(x => x.InsertDate)
                    .ThenBy(x => x.VisitId).ToList();
                totalDistributed += this.DistributeVisits(paymentEntity, orderedPastDueVisitsList, maxToDistribute, AllocationType.Interest);
                maxToDistribute = paymentEntity.ScheduledPaymentAmount - totalDistributed;
                totalDistributed += this.DistributeVisits(paymentEntity, orderedPastDueVisitsList, maxToDistribute, AllocationType.Principal);
                maxToDistribute = paymentEntity.ScheduledPaymentAmount - totalDistributed;
            }
            //Distribute Against the Active statemented

            if (statementVisits.IsNotNullOrEmpty())
            {
                agingCounts = allVisits.Where(x => x.CurrentVisitState == VisitStateEnum.Active && x.CurrentBalance > 0)
                    .Select(x => x.AgingCount).Distinct().OrderByDescending(x => x).ToList();
                foreach (int agingCount in agingCounts)
                {
                    //Modified closure
                    int count = agingCount;
                    //VP-158 - ActiveVpStatmentVisits is (x.Visit.CurrentVisitStateEnum == VisitStateEnum.Active && x.DeletedDate == null)
                    //HashSet<int> currentStatementedVisitsHashSet = currentStatement.ActiveVpStatementVisits.Where(x => x.Visit != null && x.Visit.CurrentVisitStateEnum == VisitStateEnum.Active && x.Visit.AgingCount == count).Select(x => x.Visit.VisitId).ToHashSet();
                    IList<PaymentVisit> orderedStatementedVisitsList = statementVisits.Where(x => x.CurrentVisitState == VisitStateEnum.Active && x.CurrentBalance > 0 && x.AgingTier == (AgingTierEnum)count)
                        .OrderByDescending(x => x.AgingCount)
                        .ThenBy(x => x.CurrentBalance)
                        .ThenBy(x => x.InsertDate)
                        .ThenBy(x => x.VisitId).ToList();
                    totalDistributed += this.DistributeVisits(paymentEntity, orderedStatementedVisitsList, maxToDistribute, AllocationType.Interest);
                    maxToDistribute = paymentEntity.ScheduledPaymentAmount - totalDistributed;
                    totalDistributed += this.DistributeVisits(paymentEntity, orderedStatementedVisitsList, maxToDistribute, AllocationType.Principal);
                    maxToDistribute = paymentEntity.ScheduledPaymentAmount - totalDistributed;
                }
            }

            if (allFinancePlans != null)
            {
                IList<FinancePlan> currentFinancePlans = allFinancePlans.Where(x => x.IsActiveOriginated()).OrderBy(x => x.InsertDate).ToList();
                //Current interest (FinancePlans)
                totalDistributed += this.DistributeFinancePlans_BucketFifo(paymentEntity, currentFinancePlans, maxToDistribute, AllocationType.Interest, false);
                maxToDistribute = paymentEntity.ScheduledPaymentAmount - totalDistributed;
                //Current Principal (FinancePlans)
                totalDistributed += this.DistributeFinancePlans_BucketFifo(paymentEntity, currentFinancePlans, maxToDistribute, AllocationType.Principal, false);
                maxToDistribute = paymentEntity.ScheduledPaymentAmount - totalDistributed;
            }

            //Remaining visits
            IList<PaymentVisit> orderedVisitsList = allVisits.Where(x => x.CurrentVisitState == VisitStateEnum.Active && x.CurrentBalance > 0)
                .OrderByDescending(x => x.AgingCount)
                .ThenBy(x => x.CurrentBalance)
                .ThenBy(x => x.InsertDate)
                .ThenBy(x => x.VisitId).ToList();
            totalDistributed += this.DistributeVisits(paymentEntity, orderedVisitsList, maxToDistribute, AllocationType.Interest);
            maxToDistribute = paymentEntity.ScheduledPaymentAmount - totalDistributed;
            totalDistributed += this.DistributeVisits(paymentEntity, orderedVisitsList, maxToDistribute, AllocationType.Principal);
            maxToDistribute = paymentEntity.ScheduledPaymentAmount - totalDistributed;

            return totalDistributed;
        }

        private IEnumerable<PaymentVisit> PastDueNonFinancedPaymentVisits(IList<PaymentVisit> visits)
        {
            return visits.Where(x => x.AgingCount > this.ApplicationSettingsService.Value.PastDueAgingCount.Value
                                     && x.CurrentVisitState == VisitStateEnum.Active
                                     && x.CurrentBalance > 0
                                     && !this.PaymentFinancePlanVisits.ContainsKey(x.VisitId));
        }

        //same as AllocateCurrentBalanceInFullTypeAllocationPayment
        protected decimal AllocateSpecificAmountTypeAllocationPayment(Payment paymentEntity)
        {
            return this.AllocateCurrentBalanceInFullTypeAllocationPayment(paymentEntity);
        }

        protected decimal AllocateSpecificFinancePlansTypeAllocationPayment(Payment paymentEntity)
        {
            decimal totalDistributed = 0m;

            decimal maxToDistribute = paymentEntity.ScheduledPaymentAmount - totalDistributed;

            //finance plans to use
            IList<FinancePlan> financePlanFromService = this.GetFinancePlansFromService(paymentEntity);
            IList<PaymentVisit> paymentVisits = this.PaymentVisitRepository.Value.GetPaymentVisits(financePlanFromService.SelectMany(x => x.ActiveFinancePlanVisits).Select(y => y.VisitId).ToList());

            //Past due interest (FinancePlans)
            totalDistributed += this.DistributeFinancePlans_BucketFifo(paymentEntity, financePlanFromService, maxToDistribute, AllocationType.Interest, true, paymentVisits);
            maxToDistribute = paymentEntity.ScheduledPaymentAmount - totalDistributed;
            //Past due Principal (FinancePlans)
            totalDistributed += this.DistributeFinancePlans_BucketFifo(paymentEntity, financePlanFromService, maxToDistribute, AllocationType.Principal, true, paymentVisits);
            maxToDistribute = paymentEntity.ScheduledPaymentAmount - totalDistributed;

            //Current interest (FinancePlans)
            totalDistributed += this.DistributeFinancePlans_BucketFifo(paymentEntity, financePlanFromService, maxToDistribute, AllocationType.Interest, false, paymentVisits);
            maxToDistribute = paymentEntity.ScheduledPaymentAmount - totalDistributed;
            //Current Principal (FinancePlans)
            totalDistributed += this.DistributeFinancePlans_BucketFifo(paymentEntity, financePlanFromService, maxToDistribute, AllocationType.Principal, false, paymentVisits);
            maxToDistribute = paymentEntity.ScheduledPaymentAmount - totalDistributed;

            return totalDistributed;
        }

        protected abstract IDictionary<string, decimal> GetTotalForAllCurrentBalancesWithoutNewlyDistributed(Payment paymentEntity, IList<PaymentVisit> visitsList, IDictionary<int, decimal> unclearedAllocations, IDictionary<PaymentVisit, decimal> maxPerVisit, bool isInterest);
        protected abstract decimal DistributeVisits(Payment paymentEntity, IList<PaymentVisit> orderedVisitsList, decimal maxToDistribute, AllocationType allocationType, IDictionary<PaymentVisit, decimal> maxPerVisit = null, FinancePlan financePlan = null);
        protected abstract decimal DistributeVisitsUsingMaxPerVisit(Payment paymentEntity, IList<PaymentVisit> orderedVisitsList, decimal maxToDistribute, AllocationType allocationType, IDictionary<PaymentVisit, decimal> maxPerVisit, int? financePlanId = null);

        protected enum AllocationType
        {
            Interest,
            Principal
        }

        protected class PaymentReallocationNode
        {
            public PaymentAllocation Allocation { get; set; }
            public PaymentAllocation OriginalAllocation { get; set; }
            public IList<PaymentAllocation> OtherReversalAllocations { get; set; } = new List<PaymentAllocation>();

            public decimal OriginalAmount => this.OriginalAllocation.OriginalAmount;

            public decimal AmountAvailable => decimal.Negate(this.OriginalAmountAvailable + this.Allocation.ActualAmount);

            public decimal OriginalAmountAvailable
            {
                get
                {
                    decimal originalAmountAvailable = this.OriginalAllocation.OriginalAmount
                                                      + this.OtherReversalAllocations.Sum(x => x.ActualAmount);
                    return originalAmountAvailable;
                }
            }

            public PaymentVisit PaymentVisit => this.OriginalAllocation.PaymentVisit;
        }

        #region BaseEntity helpers

        protected decimal CreateDistributionsForVisitDiscounts(Payment paymentEntity)
        {
            decimal amountDistributed = 0m;
            if (paymentEntity?.DiscountOffer?.VisitOffers == null)
            {
                return amountDistributed;
            }

            foreach (DiscountVisitOffer discountVisitOffer in paymentEntity.DiscountOffer.VisitOffers)
            {
                if (discountVisitOffer.PaymentVisit == null)
                {
                    continue;
                }

                decimal unclearedPayments = this.GetNonInterestUnclearedPaymentAmountsForVisits(discountVisitOffer.PaymentVisit.VisitId.ToListOfOne()).Sum(x => x.ActualAmount);
                decimal unclearedBalance = discountVisitOffer.PaymentVisit.CurrentBalance - unclearedPayments;
                decimal discountAmount = Math.Round(unclearedBalance * discountVisitOffer.DiscountPercent, 2, MidpointRounding.AwayFromZero);

                if (discountAmount <= 0)
                {
                    continue;
                }

                PaymentAllocation distribution = this.CreateOrFindPaymentAllocationForVisit(discountVisitOffer.PaymentVisit, paymentEntity, PaymentAllocationTypeEnum.VisitDiscount, null);

                distribution.ActualAmount = discountAmount;
                amountDistributed += distribution.ActualAmount;
                this.PaymentAllocationResults[paymentEntity].Allocations.Add(distribution);
            }

            return amountDistributed;
        }

        protected PaymentAllocation CreateOrFindPaymentAllocationForVisit(PaymentVisit visit, Payment paymentEntity, PaymentAllocationTypeEnum typeEnum, int? financePlanId)
        {
            //Don't include the previous distributions, in case there is previous distributions.
            //Not sure this makes sense...
            //Seems like a payment shouldn't have distributions if it is failed and we should be processing successful payments...
            //Where it's the correct distribution type and visit
            //Make sure if it's a specific visit for a finance plan treat it as a different distribution
            PaymentAllocation found = this.PaymentAllocationResults[paymentEntity].Allocations.FirstOrDefault(x => //x.PaymentAllocationId != default(int) && 
                x.PaymentAllocationType == typeEnum && x.PaymentVisit.VisitId == visit.VisitId
                && ((x.FinancePlanId == null) || (x.FinancePlanId == financePlanId)));
            if (found != null) return found;

            return this.CreatePaymentAllocationForVisit(visit, paymentEntity, typeEnum, financePlanId);
        }

        private PaymentAllocation CreatePaymentAllocationForVisit(PaymentVisit visit, Payment paymentEntity, PaymentAllocationTypeEnum typeEnum, int? financePlanId)
        {
            return new PaymentAllocation
            {
                ActualAmount = 0m,
                FinancePlanId = financePlanId,
                PaymentVisit = visit,
                PaymentAllocationType = typeEnum,
                PaymentAllocationStatus = PaymentAllocationStatusEnum.PendingHsAcknowledgement,
                UnallocatedAmount = 0m,
                Payment = paymentEntity,
            };
        }

        protected void RemoveAllZeroDistributions(Payment paymentEntity)
        {
            //Remove all Interest distributions with 0 actual
            List<PaymentAllocation> removeList = new List<PaymentAllocation>();
            foreach (PaymentAllocation paymentAllocation in this.PaymentAllocationResults[paymentEntity].Allocations)
            {
                if (
                    //Seems like I should just remove all zero distributions.
                    //(paymentAllocation.PaymentAllocationType == PaymentAllocationTypeEnum.VisitInterest || paymentAllocation.PaymentAllocationType == PaymentAllocationTypeEnum.VisitPrincipal) &&
                    paymentAllocation.ActualAmount == 0m)
                {
                    removeList.Add(paymentAllocation);
                }
            }
            foreach (PaymentAllocation paymentAllocation in removeList)
            {
                this.PaymentAllocationResults[paymentEntity].Allocations.Remove(paymentAllocation);
            }
        }

        protected void RoundAllocatedAmount(decimal amountToDistribute, IEnumerable<object> objects, string propertyToDistributeTo, string propertyCannotBeGreater)
        {
            decimal tracker = amountToDistribute;
            IDictionary<int, decimal> maxForObject = new Dictionary<int, decimal>();
            bool wrappedAround = false;
            if (tracker != 0)
            {
                List<object> listOfObjects = objects.ToList();
                for (int i = 0; i < listOfObjects.Count; i++)
                {
                    decimal prop = (decimal)this.GetPropValue(listOfObjects[i], propertyToDistributeTo);
                    //Multiply by -1 as we don't want to offset too much money
                    if (!maxForObject.ContainsKey(i))
                    {
                        decimal cannotAddMoreThan = (decimal)this.GetPropValue(listOfObjects[i], propertyCannotBeGreater);
                        maxForObject.Add(i, cannotAddMoreThan);
                    }
                    if (tracker > 0 && (0.01m <= maxForObject[i]))
                    {
                        this.SetPropValue(listOfObjects[i], propertyToDistributeTo, (prop + 0.01m));
                        maxForObject[i] -= 0.01m;
                        tracker -= 0.01m;
                    }
                    else if (tracker < 0 && ((prop - 0.01m) >= 0))
                    {
                        this.SetPropValue(listOfObjects[i], propertyToDistributeTo, (prop - 0.01m));
                        maxForObject[i] += 0.01m;
                        tracker += 0.01m;
                    }
                    if (tracker != 0m && ((i + 1) >= listOfObjects.Count))
                    {
                        //Haven't rounded all pennies and came to the end ot the list
                        if (wrappedAround)
                        {
                            //Only allow it to wrap around once
                            break;
                        }
                        wrappedAround = true;
                        i = 0;
                    }
                    if (tracker == 0m)
                    {
                        //When rounded pennies are gone quit rounded onto visits.
                        break;
                    }
                }
            }
        }

        protected void RoundAllocatedAmountMin(decimal amountToDistribute, IEnumerable<object> objects, string propertyToDistributeTo, string propertyCannotBeLessThan)
        {
            decimal tracker = amountToDistribute;
            IDictionary<int, decimal> minForObject = new Dictionary<int, decimal>();
            bool wrappedAround = false;
            if (tracker != 0)
            {
                List<object> listOfObjects = objects.ToList();
                for (int i = 0; i < listOfObjects.Count; i++)
                {
                    decimal prop = (decimal)this.GetPropValue(listOfObjects[i], propertyToDistributeTo);
                    //Multiply by -1 as we don't want to offset too much money
                    if (!minForObject.ContainsKey(i))
                    {
                        decimal cannotBeLessThan = (decimal)this.GetPropValue(listOfObjects[i], propertyCannotBeLessThan);
                        minForObject.Add(i, cannotBeLessThan);
                    }
                    if (tracker > 0 && ((prop - 0.01m) >= minForObject[i]))
                    {
                        this.SetPropValue(listOfObjects[i], propertyToDistributeTo, (prop + 0.01m));
                        minForObject[i] -= 0.01m;
                        tracker -= 0.01m;
                    }
                    else if (tracker < 0 && ((prop + 0.01m) <= 0))
                    {
                        this.SetPropValue(listOfObjects[i], propertyToDistributeTo, (prop - 0.01m));
                        minForObject[i] += 0.01m;
                        tracker += 0.01m;
                    }
                    if (tracker != 0m && ((i + 1) >= listOfObjects.Count))
                    {
                        //Haven't rounded all pennies and came to the end ot the list
                        if (wrappedAround)
                        {
                            //Only allow it to wrap around once
                            break;
                        }
                        wrappedAround = true;
                        i = 0;
                    }
                    if (tracker == 0m)
                    {
                        //When rounded pennies are gone quit rounded onto visits.
                        break;
                    }
                }
            }
        }

        protected object GetPropValue(object src, string propName)
        {
            return src.GetType().GetProperty(propName).GetValue(src, null);
        }

        protected void SetPropValue(object src, string propName, object toSet)
        {
            PropertyInfo prop = src.GetType().GetProperty(propName, BindingFlags.Public | BindingFlags.Instance);
            if (null != prop && prop.CanWrite)
            {
                prop.SetValue(src, toSet, null);
            }
        }

        protected void SumAllDistributionsAndFinishSetupOnPayment(Payment paymentEntity)
        {
            this.PaymentAllocationResults[paymentEntity].ActualPaymentDate = DateTime.UtcNow;
            this.PaymentAllocationResults[paymentEntity].ActualPaymentAmount = this.PaymentAllocationResults[paymentEntity].Allocations.Where(x => x.PaymentAllocationType != PaymentAllocationTypeEnum.VisitDiscount).Sum(x => x.ActualAmount);
        }

        protected IList<PaymentReallocationNode> CreateReallocationNodes(Payment reversalPaymentEntity, Payment originalPayment, bool excludeDiscounts = true, bool excludeInterest = true)
        {
            int? financePlanId = null;
            PaymentScheduledAmount foundFinancePlanScheduledPayment = originalPayment.PaymentScheduledAmounts.FirstOrDefault(x => x.PaymentFinancePlan != null);
            if (foundFinancePlanScheduledPayment != null)
            {
                financePlanId = foundFinancePlanScheduledPayment.PaymentFinancePlan.FinancePlanId;
            }

            List<PaymentReallocationNode> allAllocations = new List<PaymentReallocationNode>();
            IList<PaymentAllocation> originalPaymentAllocations = this.AllocationsWithOutstandingNetDue(originalPayment.PaymentAllocations.ToList());

            foreach (PaymentAllocation originalAllocation in originalPaymentAllocations)
            {
                bool isNotInterest = excludeInterest && originalAllocation.PaymentAllocationType != PaymentAllocationTypeEnum.VisitInterest;
                bool isNotDiscount = excludeDiscounts && originalAllocation.PaymentAllocationType != PaymentAllocationTypeEnum.VisitDiscount;
                bool isIncludedInterest = !excludeInterest && originalAllocation.PaymentAllocationType == PaymentAllocationTypeEnum.VisitInterest;
                bool isIncludedDiscount = !excludeDiscounts && originalAllocation.PaymentAllocationType == PaymentAllocationTypeEnum.VisitDiscount;
                if ((isNotInterest && isNotDiscount) || isIncludedInterest || isIncludedDiscount)
                {
                    this.CreateReallocationNode(reversalPaymentEntity, originalAllocation, financePlanId, allAllocations);
                }
            }
            foreach (Payment reversalPayment in originalPayment.ReversalPayments)
            {
                if (reversalPayment != reversalPaymentEntity && reversalPayment.PaymentId != reversalPaymentEntity.PaymentId)
                {
                    List<PaymentAllocation> reversalAllocations = reversalPayment.PaymentAllocations.ToList();
                    foreach (PaymentAllocation reversalAllocation in reversalAllocations)
                    {
                        PaymentReallocationNode foundAllocation = allAllocations.FirstOrDefault(x => x.Allocation.PaymentAllocationType == reversalAllocation.PaymentAllocationType && x.PaymentVisit.VisitId == reversalAllocation.PaymentVisit.VisitId);
                        if (foundAllocation != null)
                        {
                            foundAllocation.OtherReversalAllocations.Add(reversalAllocation);
                        }
                    }
                }
            }
            return allAllocations;
        }

        protected IList<PaymentAllocation> AllocationsWithOutstandingNetDue(IList<PaymentAllocation> paymentAllocationList)
        {
            List<int> listOfVisitIds = paymentAllocationList.Where(x => x.PaymentVisit != null).Select(x => x.PaymentVisit.VisitId).Distinct().ToList();
            HashSet<int> visitIdsWithNetZero = new HashSet<int>();
            foreach (int visitId in listOfVisitIds)
            {
                //This might need to be the ActualAmount -> Seems like this would never remove anything if we use OriginalAmount.
                //But, we probably dont care how it was reallocated?  So maybe this whole method isnt needed.
                decimal totalForVisit = paymentAllocationList.Where(x => x.PaymentVisit.VisitId == visitId).Sum(x => x.OriginalAmount);
                if (totalForVisit == 0)
                {
                    visitIdsWithNetZero.Add(visitId);
                }
            }

            IEnumerable<PaymentAllocation> allocationsWithOutstandingNetDue = paymentAllocationList.Where(x => x.PaymentVisit != null && !visitIdsWithNetZero.Contains(x.PaymentVisit.VisitId));
            return allocationsWithOutstandingNetDue.ToList();
        }

        protected void EnsureFinancePlanIds(Payment payment)
        {
            List<PaymentAllocation> generatedPaymentAllocations = this.PaymentAllocationResults[payment].Allocations;
            bool hasAllocationWithEmptyFinancePlanId = generatedPaymentAllocations.Any(x => !x.FinancePlanId.HasValue);

            if (hasAllocationWithEmptyFinancePlanId)
            {
                IList<PaymentVisit> paymentVisits = generatedPaymentAllocations.Where(x => !x.FinancePlanId.HasValue).Select(x => x.PaymentVisit).Distinct().ToList();
                this.SetupFinancePlanVisitCache(paymentVisits);
                IEnumerable<PaymentAllocation> paymentAllocationsWithoutFinancePlan = generatedPaymentAllocations.Where(x => !x.FinancePlanId.HasValue);
                foreach (PaymentAllocation paymentAllocation in paymentAllocationsWithoutFinancePlan)
                {
                    int visitId = paymentAllocation.PaymentVisit.VisitId;
                    bool foundKey = this.PaymentFinancePlanVisits.ContainsKey(visitId);
                    if (foundKey)
                    {
                        paymentAllocation.FinancePlanId = this.PaymentFinancePlanVisits[visitId].FinancePlanId;
                    }
                }
            }
        }

        public PaymentAllocationResult AllocateReallocationPayment(Payment reallocationPayment, decimal reallocateAmountAfterWriteOff, Dictionary<int, decimal> maxPerVisit, int financePlanId)
        {
            this.PaymentAllocationResults[reallocationPayment] = new PaymentAllocationResult() { Payment = reallocationPayment };

            try
            {
                IList<PaymentVisit> paymentVisits = this.PaymentVisitRepository.Value.GetPaymentVisits(maxPerVisit.Where(x => x.Value > 0m).Select(x => x.Key).ToList());
                Dictionary<PaymentVisit, decimal> maxVisit = new Dictionary<PaymentVisit, decimal>();
                foreach (PaymentVisit paymentVisit in paymentVisits)
                {
                    if (maxPerVisit.ContainsKey(paymentVisit.VisitId))
                    {
                        maxVisit.Add(paymentVisit, maxPerVisit[paymentVisit.VisitId]);
                    }
                }
                this.DistributeVisitsUsingMaxPerVisit(reallocationPayment, paymentVisits, reallocateAmountAfterWriteOff, AllocationType.Principal, maxVisit, financePlanId);
                this.CreateOpposingDistributionsForInterest(reallocationPayment);
                if (this.PaymentAllocationResults[reallocationPayment].Allocations.Sum(x => x.ActualAmount) != 0m)
                {
                    throw new Exception("Reallocation didn't sum to 0.00!\nShould always sum to 0.00.");
                }

                this.RemoveAllZeroDistributions(reallocationPayment);
                this.SumAllDistributionsAndFinishSetupOnPayment(reallocationPayment);
                this.EnsureFinancePlanIds(reallocationPayment);
                this.SetOriginalAmountOnAllocations(this.PaymentAllocationResults[reallocationPayment].Allocations);

                return this.PaymentAllocationResults[reallocationPayment];
            }
            finally
            {
                this.FinancePlanBuckets.Clear();
                this.FinancePlanBucketWithPaymentAmounts.Clear();
                this.UnclearedPaymentAllocations.Clear();
                this.PaymentFinancePlanVisits.Clear();
            }
        }

        private void CreateOpposingDistributionsForInterest(Payment reallocationPayment)
        {
            IList<PaymentAllocation> newInterestAllocations = new List<PaymentAllocation>();
            foreach (PaymentAllocation paymentAllocation in this.PaymentAllocationResults[reallocationPayment].Allocations)
            {
                PaymentAllocation interestDistribution = this.CreateOrFindPaymentAllocationForVisit(paymentAllocation.PaymentVisit, reallocationPayment, PaymentAllocationTypeEnum.VisitInterest, paymentAllocation.FinancePlanId);
                interestDistribution.ActualAmount += decimal.Negate(paymentAllocation.ActualAmount);
                newInterestAllocations.Add(interestDistribution);
            }
            this.PaymentAllocationResults[reallocationPayment].Allocations.AddRange(newInterestAllocations);
        }

        protected void CreateReallocationNode(Payment reversalPaymentEntity, PaymentAllocation originalAllocation, int? foundFinancePlanId, List<PaymentReallocationNode> allAllocations)
        {
            PaymentReallocationNode node = new PaymentReallocationNode
            {
                OriginalAllocation = originalAllocation
            };
            node.Allocation = this.CreateOrFindPaymentAllocationForVisit(originalAllocation.PaymentVisit, reversalPaymentEntity, originalAllocation.PaymentAllocationType, foundFinancePlanId);
            allAllocations.Add(node);
        }

        protected void ReverseDistributeNodes(Payment reversalPaymentEntity, IList<PaymentReallocationNode> allocationNodes, decimal totalAmountToDistribute)
        {
            //Apply the amounts for each node prorata
            //VPNG-22127 negative and positive amounts might be in allocationNodes so ABS all amounts
            decimal absTotalOriginalAmount = allocationNodes.Sum(x => Math.Abs(x.OriginalAmount));
            foreach (PaymentReallocationNode paymentReallocationNode in allocationNodes)
            {
                decimal proRataRatio = Math.Abs(paymentReallocationNode.OriginalAmount) / absTotalOriginalAmount;
                paymentReallocationNode.Allocation.ActualAmount = totalAmountToDistribute * proRataRatio;
            }

            decimal totalUnrounded = allocationNodes.Select(x => x.Allocation).Sum(y => y.ActualAmount);
            decimal totalRounded = Math.Round(totalUnrounded, 2);
            foreach (PaymentReallocationNode paymentReallocationNode in allocationNodes)
            {
                paymentReallocationNode.Allocation.ActualAmount = Math.Round(paymentReallocationNode.Allocation.ActualAmount, 2);
                paymentReallocationNode.Allocation.MaxAllocationAmount = paymentReallocationNode.AmountAvailable;
            }

            decimal sumRounded = allocationNodes.Select(x => x.Allocation).Sum(y => y.ActualAmount);
            decimal amountMissing = totalRounded - sumRounded;
            List<PaymentAllocation> paymentAllocations = allocationNodes.Select(x => x.Allocation).ToList();
            this.RoundAllocatedAmountMin(amountMissing, paymentAllocations, "ActualAmount", "MaxAllocationAmount");

            this.AddGeneratedPaymentAllocations(reversalPaymentEntity, paymentAllocations);
        }

        #endregion

        public virtual IList<BillingApplication> BillingApplications()
        {
            return new List<BillingApplication>();
        }

        public void OffsetAllAllocationsInPayment(Payment payment)
        {
            DateTime now = DateTime.UtcNow;
            HashSet<int> allocationIdsThatHaveBeenReversed = payment.PaymentAllocations.Where(x => x.OriginalPaymentAllocation != null).Select(y => y.OriginalPaymentAllocation.PaymentAllocationId).ToHashSet();
            List<PaymentAllocation> paymentAllocationsNotReversed = payment.PaymentAllocations.Where(x => x.OriginalPaymentAllocation == null
                                                                                                          && !allocationIdsThatHaveBeenReversed.Contains(x.PaymentAllocationId)).ToList();
            foreach (PaymentAllocation paymentPaymentAllocation in paymentAllocationsNotReversed)
            {
                PaymentAllocation allocation = this.CreatePaymentAllocationForVisit(paymentPaymentAllocation.PaymentVisit, payment, paymentPaymentAllocation.PaymentAllocationType, paymentPaymentAllocation.FinancePlanId);
                allocation.ActualAmount = decimal.Negate(paymentPaymentAllocation.ActualAmount);
                allocation.OriginalPaymentAllocation = paymentPaymentAllocation;
                allocation.DeleteDate = now;
                paymentPaymentAllocation.DeleteDate = now;
                payment.PaymentAllocations.Add(allocation);
            }

            payment.ActualPaymentDate = now;
            payment.ActualPaymentAmount = payment.PaymentAllocations.Sum(x => x.ActualAmount);
        }

        protected IList<FinancePlan> GetFinancePlansFromService(Payment paymentEntity)
        {
            List<int> financePlanIds = paymentEntity.PaymentScheduledAmounts.Where(x => x.PaymentFinancePlan != null).Select(x => x.PaymentFinancePlan.FinancePlanId).ToList();
            IList<FinancePlan> financePlans = this.FinancePlanService.Value.GetFinancePlansById(financePlanIds);
            return financePlans;
        }

        #region Message Publishing

        private PaymentAllocationMessage CreatePaymentAllocationMessage(PaymentAllocation x, Payment p1, AchSettlementTypeEnum achSettlementType = AchSettlementTypeEnum.None)
        {
            decimal allocationAmount = x.ActualAmount;

            bool hasOptimisticSettlementEnabled = this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureOptimisticAchSettlementIsEnabled);
            bool isFileReturn = (achSettlementType == AchSettlementTypeEnum.FileReturn);
            if (hasOptimisticSettlementEnabled && isFileReturn)
            {
                allocationAmount = allocationAmount * -1m;
            }

            return new PaymentAllocationMessage
            {
                VpPaymentAllocationId = x.PaymentAllocationId,
                VpPaymentAllocationTypeId = (int)x.PaymentAllocationType,
                VpPaymentAllocationOpposingPaymentAllocationId = x.OriginalPaymentAllocation?.PaymentAllocationId,
                AllocationAmount = allocationAmount,
                VisitBillingSystemId = x.PaymentVisit.MatchedBillingSystemId ?? x.PaymentVisit.BillingSystemId ?? 0,
                VisitSourceSystemKey = x.PaymentVisit.MatchedSourceSystemKey ?? x.PaymentVisit.SourceSystemKey ?? $"VisitId-{x.PaymentVisit.VisitId}",
                VpPaymentAllocationInsertDate = x.InsertDate,
                VpPaymentId = p1?.PaymentId,
                VpPaymentType = p1?.PaymentType ?? PaymentTypeEnum.Unknown,
                VpPaymentActualPaymentDate = p1?.ActualPaymentDate,
                VpPaymentAmount = p1?.ActualPaymentAmount ?? 0m,
                VpParentPaymentId = p1?.ParentPayment?.PaymentId,
                VpGuarantorId = p1?.Guarantor?.VpGuarantorId,
                VpFinancePlanId = x.FinancePlanId,
                BalanceTransferStatusId = (int?)x.PaymentVisit.BalanceTransferStatusEnum,
                MerchantAccountId = p1?.MerchantAccount?.MerchantAccountId,
                VpPaymentMethodType = p1?.PaymentMethod?.PaymentMethodType,
                AchSettlementType = achSettlementType
            };
        }

        public void PublishPaymentAllocationMessages(IList<PaymentAllocation> paymentAllocations)
        {
            if (paymentAllocations.IsNullOrEmpty())
            {
                return;
            }
            this._session.Transaction.RegisterPostCommitSuccessAction((allocations) =>
            {
                List<PaymentAllocationMessage> paymentAllocationMessages = allocations.Select(x => { return this.CreatePaymentAllocationMessage(x, null); }).ToList();

                paymentAllocationMessages = this.FilterPaymentAllocationMessages(paymentAllocationMessages);

                this.SendPaymentAllocationMessagesToBus(paymentAllocationMessages);
            }, paymentAllocations);
        }

        public void PublishPaymentAllocationMessages(Payment payment, bool onlyPublishAllocationsWithoutOriginalPaymentAllocation = false, AchSettlementTypeEnum achSettlementType = AchSettlementTypeEnum.None)
        {
            if (payment == null || payment.PaymentAllocations.IsNullOrEmpty())
            {
                return;
            }

            this._session.Transaction.RegisterPostCommitSuccessAction((p1, p2) =>
            {
                List<PaymentAllocationMessage> paymentAllocationMessages = p1.PaymentAllocations.Select(x => { return this.CreatePaymentAllocationMessage(x, p1, achSettlementType); }).ToList();

                paymentAllocationMessages = this.FilterPaymentAllocationMessages(paymentAllocationMessages, onlyPublishAllocationsWithoutOriginalPaymentAllocation);
                this.SendPaymentAllocationMessagesToBus(paymentAllocationMessages);
            }, payment, achSettlementType);
        }

        private void SendPaymentAllocationMessagesToBus(List<PaymentAllocationMessage> paymentAllocationMessages)
        {
            if (paymentAllocationMessages.Any())
            {
                PaymentAllocationMessageList message = new PaymentAllocationMessageList { Messages = paymentAllocationMessages };
                try
                {
                    this.Bus.Value.PublishMessage(message).Wait();
                }
                catch
                {
                    this.LoggingService.Value.Fatal(() => string.Format("PaymentAllocationServiceBase.PublishPaymentAllocationMessages::Failed to publish {0}, Message = {1}", typeof(PaymentAllocationMessage).FullName, JsonConvert.SerializeObject(message)));
                }
            }
        }

        private List<PaymentAllocationMessage> FilterPaymentAllocationMessages(List<PaymentAllocationMessage> paymentAllocationMessages, bool onlyPublishAllocationsWithoutOriginalPaymentAllocation = false)
        {
            // PaymentAllocationTypeEnum.VisitInterest is not an assessment - we wouldn't have a payment allocation for an assessment?
            //if (!this.ApplicationSettingsService.Value.PublishInterestAssessment.Value)
            //{
            //paymentAllocationMessages = paymentAllocationMessages.Where(x => ((PaymentAllocationTypeEnum)x.VpPaymentAllocationTypeId) != PaymentAllocationTypeEnum.VisitInterest).ToList();
            //}

            if (onlyPublishAllocationsWithoutOriginalPaymentAllocation)
            {
                paymentAllocationMessages = paymentAllocationMessages.Where(x => !x.VpPaymentAllocationOpposingPaymentAllocationId.HasValue).ToList();
            }

            return paymentAllocationMessages;
        }

        #endregion

        #region cleared / uncleared

        public void ClearPaymentAllocations(IList<int> paymentAllocationIds)
        {
            if (paymentAllocationIds.IsNullOrEmpty())
            {
                return;
            }

            const int batchSize = 1000;

            using (IUnitOfWork uow = new UnitOfWork(this._session))
            {
                List<PaymentAllocation> list = new List<PaymentAllocation>();

                int batches = (int)Math.Ceiling((double)paymentAllocationIds.Count / batchSize);
                for (int i = 0; i < batches; i++)
                {
                    List<int> batchedIds = paymentAllocationIds.Skip(batchSize * i).Take(batchSize).ToList();
                    List<PaymentAllocation> batchedResults = this._paymentAllocationRepository.Value.GetQueryable().Where(x => batchedIds.Contains(x.PaymentAllocationId)).ToList();
                    if (batchedResults.IsNullOrEmpty())
                    {
                        continue;
                    }

                    list.AddRange(batchedResults);
                }

                foreach (PaymentAllocation paymentAllocation in list)
                {
                    paymentAllocation.SetCleared();
                    this._paymentAllocationRepository.Value.Update(paymentAllocation);
                }

                uow.Commit();
            }
        }

        public IList<PaymentAllocationVisitAmountResult> GetNonInterestUnclearedPaymentAmountsForVisits(IList<int> visitIds, DateTime? startDate = null, DateTime? endDate = null)
        {
            IList<PaymentAllocationVisitAmountResult> paymentAllocations = this._paymentAllocationRepository.Value.GetNonInterestPaymentAllocationAmountsForVisits(visitIds, PaymentBatchStatusEnum.Pending, startDate, endDate);

            return paymentAllocations;
        }

        private IList<PaymentAllocationVisitAmountResult> GetAllUnclearedPaymentAmountsForVisits(IList<int> visitIds)
        {
            IList<PaymentAllocationVisitAmountResult> paymentAllocations = this._paymentAllocationRepository.Value.GetAllPaymentAllocationAmountsForVisits(visitIds, PaymentBatchStatusEnum.Pending);

            return paymentAllocations;
        }

        #endregion

        #region Existing Distributions

        protected decimal GetPaymentDistributionAmount(Payment paymentEntity, int visitId, bool isInterest)
        {
            return this.GetPaymentDistributionAmount(paymentEntity, new HashSet<int> { visitId }, isInterest);
        }

        protected decimal GetPaymentDistributionAmount(Payment paymentEntity, HashSet<int> visitIdHashSet, bool isInterest)
        {
            Func<PaymentAllocation, bool> filterPaymentAllocationType = this.GetPaymentAllocationTypeFilter(isInterest);

            decimal paymentDistributionAmountTotal = paymentEntity.PaymentAllocations
                .Where(x => x.PaymentVisit != null
                            && visitIdHashSet.Contains(x.PaymentVisit.VisitId)
                            && filterPaymentAllocationType(x))
                .Sum(x => x.ActualAmount);

            return paymentDistributionAmountTotal;
        }

        protected IEnumerable<IGrouping<string, PaymentAllocation>> GetPaymentDistributionBillingApplicationGroups(Payment paymentEntity, HashSet<int> visitIdHashSet, bool isInterest)
        {
            Func<PaymentAllocation, bool> filterPaymentAllocationType = this.GetPaymentAllocationTypeFilter(isInterest);

            IEnumerable<IGrouping<string, PaymentAllocation>> billingApplicationGroups = paymentEntity.PaymentAllocations
                .Where(x => x.PaymentVisit != null
                            && visitIdHashSet.Contains(x.PaymentVisit.VisitId)
                            && filterPaymentAllocationType(x))
                .GroupBy(x => x.PaymentVisit.BillingApplication);

            return billingApplicationGroups;
        }

        protected decimal GetAlreadyAllocatedDistributionAmount(Payment paymentEntity, int visitId, bool isInterest)
        {
            return this.GetAlreadyAllocatedDistributionAmount(paymentEntity, new HashSet<int> { visitId }, isInterest);
        }

        protected decimal GetAlreadyAllocatedDistributionAmount(Payment paymentEntity, HashSet<int> visitIdHashSet, bool isInterest)
        {
            decimal alreadyAllocatedDistributionAmountTotal = 0;

            Func<PaymentAllocation, bool> filterPaymentAllocationType = this.GetPaymentAllocationTypeFilter(isInterest);

            if (this.PaymentAllocationResults[paymentEntity].Allocations != null)
            {
                alreadyAllocatedDistributionAmountTotal = this.PaymentAllocationResults[paymentEntity].Allocations
                    .Where(x => x.PaymentVisit != null
                                && visitIdHashSet.Contains(x.PaymentVisit.VisitId)
                                && filterPaymentAllocationType(x))
                    .Sum(x => x.ActualAmount);
            }

            return alreadyAllocatedDistributionAmountTotal;
        }

        protected IEnumerable<IGrouping<string, PaymentAllocation>> GetAlreadyAllocatedDistributionBillingApplicationGroups(Payment paymentEntity, HashSet<int> visitIdHashSet, bool isInterest)
        {
            Func<PaymentAllocation, bool> filterPaymentAllocationType = this.GetPaymentAllocationTypeFilter(isInterest);
            IEnumerable<IGrouping<string, PaymentAllocation>> billingApplicationGroups = null;

            if (this.PaymentAllocationResults[paymentEntity].Allocations != null)
            {
                billingApplicationGroups = this.PaymentAllocationResults[paymentEntity].Allocations
                    .Where(x => visitIdHashSet.Contains(x.PaymentVisit.VisitId)
                                && filterPaymentAllocationType(x))
                    .GroupBy(x => x.PaymentVisit.BillingApplication);
            }

            return billingApplicationGroups;
        }

        protected Func<PaymentAllocation, bool> GetPaymentAllocationTypeFilter(bool isInterest)
        {
            Func<PaymentAllocation, bool> filterPaymentAllocationType;
            if (isInterest)
            {
                filterPaymentAllocationType = x => x.PaymentAllocationType == PaymentAllocationTypeEnum.VisitInterest;
            }
            else
            {
                filterPaymentAllocationType = x => x.PaymentAllocationType != PaymentAllocationTypeEnum.VisitInterest;
            }

            return filterPaymentAllocationType;
        }

        #endregion

        #region Amount Due

        protected decimal GetInterestDue(int visitId)
        {
            bool isPaymentFinancePlanVisit = this.PaymentFinancePlanVisits.ContainsKey(visitId)
                                             && this.PaymentFinancePlanVisits[visitId] != null;
            decimal interestDue = isPaymentFinancePlanVisit ? this.PaymentFinancePlanVisits[visitId].InterestDue : 0m;
            return interestDue;
        }

        protected decimal GetInterestDue(PaymentVisit paymentVisit, IDictionary<PaymentVisit, decimal> maxPerVisit)
        {
            return this.GetInterestDue(new List<PaymentVisit> { paymentVisit }, maxPerVisit);
        }

        protected decimal GetInterestDue(IList<PaymentVisit> visitsList, IDictionary<PaymentVisit, decimal> maxPerVisit)
        {
            decimal interestSum = 0m;
            if (maxPerVisit == null)
            {
                maxPerVisit = new Dictionary<PaymentVisit, decimal>();
            }
            foreach (PaymentVisit visit in visitsList)
            {
                decimal interestDue = this.GetInterestDue(visit.VisitId);
                bool hasMaxPerVisit = maxPerVisit.ContainsKey(visit);
                decimal maxPerVisitAmount = hasMaxPerVisit ? maxPerVisit[visit] : 0m;

                bool isPaymentFinancePlanVisit = this.PaymentFinancePlanVisits.ContainsKey(visit.VisitId)
                                                 && this.PaymentFinancePlanVisits[visit.VisitId] != null;

                if (isPaymentFinancePlanVisit)
                {
                    if (hasMaxPerVisit && maxPerVisitAmount < interestDue)
                    {
                        interestSum += maxPerVisitAmount;
                    }
                    else
                    {
                        interestSum += interestDue;
                    }
                }
            }

            return interestSum;
        }

        protected decimal GetPrincipalDue(IList<PaymentVisit> visitsList, IDictionary<PaymentVisit, decimal> maxPerVisit, IDictionary<int, decimal> unclearedAllocations)
        {
            decimal principalSum = 0m;
            if (maxPerVisit == null)
            {
                maxPerVisit = new Dictionary<PaymentVisit, decimal>();
            }
            foreach (PaymentVisit visit in visitsList)
            {
                decimal visitUnclearedPaymentAmount = unclearedAllocations.GetValue(visit.VisitId);
                decimal visitBalance = visit.CurrentBalance + visitUnclearedPaymentAmount;
                if (maxPerVisit.ContainsKey(visit) && maxPerVisit[visit] < visitBalance)
                {
                    principalSum += maxPerVisit[visit];
                }
                else
                {
                    principalSum += visitBalance;
                }
            }

            return principalSum;
        }

        #endregion

        #region Add PaymentAllocationResults

        protected void AddGeneratedPaymentAllocations(Payment paymentEntity, IList<PaymentAllocation> paymentAllocations, bool setMaxAllocationAmountToZero = true)
        {
            //Add all distributions that were generated to the payment.
            foreach (PaymentAllocation paymentAllocation in paymentAllocations)
            {
                if (setMaxAllocationAmountToZero)
                {
                    paymentAllocation.MaxAllocationAmount = 0m;
                }
                bool isAllocationAlreadyGenerated = this.PaymentAllocationResults[paymentEntity].Allocations.Contains(paymentAllocation);
                if (!isAllocationAlreadyGenerated)
                {
                    this.PaymentAllocationResults[paymentEntity].Allocations.Add(paymentAllocation);
                }
            }
        }

        #endregion

        #region Rounding

        protected void AdjustRoundedAllocationAmounts(IList<PaymentAllocation> paymentAllocations, decimal totalDistributedUnrounded, ref decimal totalDistributed)
        {
            //Round the allocation totals
            decimal totalRounded = Math.Round(totalDistributedUnrounded, 2);
            decimal amountMissing = totalRounded - totalDistributed;

            //Round the allocation amounts
            foreach (PaymentAllocation paymentAllocation in paymentAllocations)
            {
                paymentAllocation.ActualAmount = Math.Round(paymentAllocation.ActualAmount, 2);
            }
            this.RoundAllocatedAmount(amountMissing, paymentAllocations, ActualAmountPropertyName, MaxAllocationAmountPropertyName);

            //Apply the pennies that were rounded onto the items into the total.
            totalDistributed += amountMissing;
        }

        #endregion

        #region Visit Grouping

        protected List<List<PaymentVisit>> GroupVisitsByPriority(List<PaymentVisit> paymentVisitList)
        {
            List<List<PaymentVisit>> visitGroupsList = new List<List<PaymentVisit>>
            {
                paymentVisitList
            };

            IList<PaymentAllocationPriorityGroupingEnum> allocationGroupPriorities = this.Client.Value.PaymentAllocationOrderedPriorityGrouping;

            foreach (PaymentAllocationPriorityGroupingEnum allocationGroupPriority in allocationGroupPriorities)
            {
                List<List<PaymentVisit>> innerVisitGroupsList = new List<List<PaymentVisit>>();
                foreach (List<PaymentVisit> visitList in visitGroupsList)
                {
                    List<List<PaymentVisit>> dateGroups = this.GetGroupedVisits(visitList, allocationGroupPriority);
                    innerVisitGroupsList.AddRange(dateGroups);
                }
                visitGroupsList.Clear();
                visitGroupsList.AddRange(innerVisitGroupsList);
            }

            return visitGroupsList;
        }

        protected Func<PaymentVisit, string> GetGroupByFilter(PaymentAllocationPriorityGroupingEnum groupByEnum)
        {
            Func<PaymentVisit, string> filterPaymentAllocationType;
            if (groupByEnum == PaymentAllocationPriorityGroupingEnum.PaymentPriority)
            {
                filterPaymentAllocationType = x => (x.PaymentPriority ?? int.MaxValue).ToString().PadLeft(11, '0'); //hack to force a numeric sort when using a number as a string
            }
            else if (groupByEnum == PaymentAllocationPriorityGroupingEnum.StatementDate)
            {
                filterPaymentAllocationType = x => (x.EarliestFinancePlanStatementDate ?? x.EarliestStatementDate ?? DateTime.MaxValue).ToString("yyyyMMdd");
            }
            else if (groupByEnum == PaymentAllocationPriorityGroupingEnum.DischargeDate)
            {
                filterPaymentAllocationType = x => (x.DischargeDate ?? DateTime.MaxValue).ToString("yyyyMMdd");
            }
            else //should not be here
            {
                filterPaymentAllocationType = x => x.BillingApplication;
            }

            return filterPaymentAllocationType;
        }

        private List<List<PaymentVisit>> GetGroupedVisits(List<PaymentVisit> visitList, PaymentAllocationPriorityGroupingEnum groupByEnum)
        {
            List<List<PaymentVisit>> visitListGroups = new List<List<PaymentVisit>>();
            Func<PaymentVisit, string> groupByFilter = this.GetGroupByFilter(groupByEnum);

            List<IGrouping<string, PaymentVisit>> groupedVisitListGroups = visitList
                .GroupBy(x => groupByFilter(x))
                .OrderBy(x => x.Key)
                .ToList();

            foreach (IGrouping<string, PaymentVisit> visitListGroup in groupedVisitListGroups)
            {
                List<PaymentVisit> groupedVisits = visitListGroup.ToList();
                visitListGroups.Add(groupedVisits);
            }

            return visitListGroups;
        }

        #endregion

        #region Visit Distribution

        protected PaymentVisitDistribution GetPaymentVisitDistribution(
            Payment paymentEntity,
            PaymentVisit paymentVisit,
            IDictionary<int, decimal> unclearedAllocations,
            IDictionary<PaymentVisit, decimal> maxPerVisit,
            decimal calculatedMaxToDistribute,
            decimal totalForAllCurrentBalancesWithoutNewlyDistributed,
            bool isInterest)
        {
            decimal foundDistributionAmountTotal = 0m;
            decimal exactAmountToDistributeForVisit = 0m;

            decimal alreadyAllocated = this.GetAlreadyAllocatedDistributionAmount(paymentEntity, paymentVisit.VisitId, isInterest: isInterest);
            foundDistributionAmountTotal = this.GetPaymentDistributionAmount(paymentEntity, paymentVisit.VisitId, isInterest: isInterest);
            foundDistributionAmountTotal += alreadyAllocated;

            decimal amountDue = 0m;
            if (isInterest)
            {
                amountDue = this.GetInterestDue(paymentVisit.VisitId);
            }
            else
            {
                decimal unclearedAllocationAmount = unclearedAllocations.GetValue(paymentVisit.VisitId);
                amountDue = paymentVisit.CurrentBalance;
                amountDue += unclearedAllocationAmount;
            }

            bool hasMaxPerVisit = maxPerVisit != null && maxPerVisit.ContainsKey(paymentVisit);
            decimal maxPerVisitAmount = hasMaxPerVisit ? maxPerVisit[paymentVisit] : 0m;
            if (hasMaxPerVisit && maxPerVisitAmount < amountDue)
            {
                amountDue = maxPerVisitAmount;
            }

            decimal amountAvailableForAllocation = amountDue - foundDistributionAmountTotal;
            decimal allocationPercentage = amountAvailableForAllocation / totalForAllCurrentBalancesWithoutNewlyDistributed;
            exactAmountToDistributeForVisit = calculatedMaxToDistribute * allocationPercentage;

            return new PaymentVisitDistribution()
            {
                ExactAmountToDistributeForVisit = exactAmountToDistributeForVisit,
                FoundDistributionAmountTotal = foundDistributionAmountTotal
            };
        }

        #endregion
    }

    public class PaymentVisitDistribution
    {
        public decimal FoundDistributionAmountTotal { get; set; }
        public decimal ExactAmountToDistributeForVisit { get; set; }
    }

    public class VisitGroupPaymentAllocationResult
    {
        public VisitGroupPaymentAllocationResult()
        {
            this.TotalDistributedUnrounded = 0m;
            this.TotalDistributed = 0m;
            this.PaymentAllocations = new List<PaymentAllocation>();
        }

        public decimal TotalDistributedUnrounded { get; set; }
        public decimal TotalDistributed { get; set; }

        public IList<PaymentAllocation> PaymentAllocations { get; set; }
    }
}
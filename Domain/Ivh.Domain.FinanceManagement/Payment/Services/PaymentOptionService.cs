﻿namespace Ivh.Domain.FinanceManagement.Payment.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Entities;
    using Interfaces;

    public class PaymentOptionService : DomainService, IPaymentOptionService
    {
        private readonly Lazy<IPaymentMenuRepository> _paymentMenuRepository;
        private readonly Lazy<IPaymentOptionRepository> _paymentOptionRepository;

        public PaymentOptionService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IPaymentMenuRepository> paymentMenuRepository,
            Lazy<IPaymentOptionRepository> paymentOptionRepository) : base(serviceCommonService)
        {
            this._paymentMenuRepository = paymentMenuRepository;
            this._paymentOptionRepository = paymentOptionRepository;
        }

        public IList<PaymentMenuItem> GetPaymentMenu(int paymentMenuId)
        {
            PaymentMenu paymentMenu = this._paymentMenuRepository.Value.GetQueryable().ToList().FirstOrDefault(x => x.PaymentMenuId == paymentMenuId);
            return paymentMenu?.PaymentMenuItems ?? new List<PaymentMenuItem>();
        }

        public IList<PaymentOption> GetPaymentOptions()
        {
            return this._paymentOptionRepository.Value.GetQueryable().ToList();
        }
    }
}
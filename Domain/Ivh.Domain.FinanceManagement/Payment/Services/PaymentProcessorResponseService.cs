namespace Ivh.Domain.FinanceManagement.Payment.Services
{
    using System;
    using System.Collections.Generic;
    using Base.Interfaces;
    using Base.Services;
    using Entities;
    using Interfaces;

    public class PaymentProcessorResponseService : DomainService, IPaymentProcessorResponseService
    {
        private readonly IPaymentProcessorResponseRepository _paymentProcessorResponseRepository;

        public PaymentProcessorResponseService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            IPaymentProcessorResponseRepository paymentProcessorResponseRepository) : base(serviceCommonService)
        {
            this._paymentProcessorResponseRepository = paymentProcessorResponseRepository;
        }

        public PaymentProcessorResponse GetPaymentProcessorResponse(int paymentProcessorResponseId)
        {
            return this._paymentProcessorResponseRepository.GetPaymentProcessorResponse(paymentProcessorResponseId);
        }

        public IList<PaymentProcessorResponse> GetPaymentProcessorResponses(IList<int> paymentProcessorResponseIds)
        {
            return this._paymentProcessorResponseRepository.GetPaymentProcessorResponses(paymentProcessorResponseIds);
        }

        public PaymentProcessorResponse GetPaymentProcessorResponse(string paymentProcessorSourceKey)
        {
            return this._paymentProcessorResponseRepository.GetPaymentProcessorResponse(paymentProcessorSourceKey);
        }
    }
}
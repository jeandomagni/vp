﻿namespace Ivh.Domain.AppIntelligence.Interfaces
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using Ivh.Domain.Base.Interfaces;

    public interface IRandomizedTestService : IDomainService
    {
        RandomizedTest GetRandomizedTest(RandomizedTestEnum randomizedTest);
        RandomizedTestGroup GetRandomizedTestGroupMembership(RandomizedTest randomizedTest, int vpGuarantorId);
        RandomizedTestGroup GetRandomizedTestGroupMembership(RandomizedTestEnum randomizedTest, int vpGuarantorId, bool assignIfNotAssigned = true, RandomizedTestGroupEnum? randomizedTestGroup = null);
    }
}
﻿namespace Ivh.Domain.AppIntelligence.Interfaces
{
    using Entities;

    public interface IFinancePlanIntelligenceService
    {
        void LogTermsDisplayed(FinancePlanTermsDisplayed financePlanTermsDisplayed);
    }
}
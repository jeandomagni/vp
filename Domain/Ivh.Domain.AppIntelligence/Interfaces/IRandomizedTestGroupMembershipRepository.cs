﻿namespace Ivh.Domain.AppIntelligence.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IRandomizedTestGroupMembershipRepository : IRepository<RandomizedTestGroupMembership>
    {
        RandomizedTestGroupMembership GetRandomizedTestGroupMembership(RandomizedTest randomizedTest, int vpGuarantorId);
        IList<int> GetManagedGuarantorIds(int managingGuarantorId);
        IList<int> GetManagingGuarantorIds(int managedGuarantorId);
    }
}
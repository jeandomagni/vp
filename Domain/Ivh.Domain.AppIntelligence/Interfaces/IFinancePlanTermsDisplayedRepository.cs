﻿namespace Ivh.Domain.AppIntelligence.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IFinancePlanTermsDisplayedRepository : IRepository<FinancePlanTermsDisplayed>
    {
    }
}
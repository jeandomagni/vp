﻿namespace Ivh.Domain.AppIntelligence.Interfaces
{
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IRandomizedTestRepository : IRepository<RandomizedTest>
    {
        RandomizedTest GetActiveRandomizedTest(RandomizedTestEnum randomizedTestEnum);
    }
}
﻿namespace Ivh.Domain.AppIntelligence.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;
    using Logging.Interfaces;

    public class RandomizedTestService : DomainService, IRandomizedTestService
    {
        private readonly Lazy<IRandomizedTestRepository> _randomizedTestRepository;
        private readonly Lazy<IRandomizedTestGroupMembershipRepository> _randomizedTestGroupMembershipRepository;
        private readonly Lazy<IMetricsProvider> _metricsProvider;

        public RandomizedTestService(
            Lazy<IDomainServiceCommonService> domainServiceCommonService,
            Lazy<IRandomizedTestRepository> randomizedTestRepository,
            Lazy<IRandomizedTestGroupMembershipRepository> randomizedTestGroupMembershipRepository,
            Lazy<IMetricsProvider> metricsProvider) : base(domainServiceCommonService)
        {
            this._randomizedTestRepository = randomizedTestRepository;
            this._randomizedTestGroupMembershipRepository = randomizedTestGroupMembershipRepository;
            this._metricsProvider = metricsProvider;
        }

        public RandomizedTestGroup GetWeightedRandomizedTestGroup(RandomizedTest randomizedTest)
        {
            decimal totalWeight = randomizedTest.RandomizedTestGroups.Sum(x => x.Weighting);
            long ceiling = 0;
            Dictionary<RandomizedTestGroup, long> localWeighting = randomizedTest.RandomizedTestGroups.ToDictionary(k => k, v =>
            {
                ceiling += (long)(v.Weighting / totalWeight * long.MaxValue);
                return ceiling;
            });

            byte[] bytes = new byte[8];
            new RNGCryptoServiceProvider().GetBytes(bytes);
            long randomInteger = Math.Abs(BitConverter.ToInt64(bytes, 0));
            RandomizedTestGroup abTestGroup = localWeighting.First(x => randomInteger <= x.Value).Key;
            return abTestGroup;
        }

        public RandomizedTest GetRandomizedTest(RandomizedTestEnum randomizedTest)
        {
            return this._randomizedTestRepository.Value.GetActiveRandomizedTest(randomizedTest);
        }

        public RandomizedTestGroup GetRandomizedTestGroupMembership(RandomizedTest randomizedTest, int vpGuarantorId)
        {
            return this.GetRandomizedTestGroupMembershipInternal(randomizedTest, vpGuarantorId);
        }

        private RandomizedTestGroup GetRandomizedTestGroupMembershipInternal(RandomizedTest randomizedTest, int vpGuarantorId, RandomizedTestGroup randomizedTestGroup = null)
        {
            if (!randomizedTest.IsActive())
            {
                return null;
            }

            // check to see if a group has already been assigned
            RandomizedTestGroupMembership randomizedTestGroupMembership = this._randomizedTestGroupMembershipRepository.Value.GetRandomizedTestGroupMembership(randomizedTest, vpGuarantorId);

            if (randomizedTestGroupMembership == null)
            {
                // if not, check to see if any of the consolidated relationships have had a group assigned
                IList<int> consolidatedGuarantorIds = new List<int>();
                consolidatedGuarantorIds.AddRange(this._randomizedTestGroupMembershipRepository.Value.GetManagingGuarantorIds(vpGuarantorId));
                consolidatedGuarantorIds.AddRange(this._randomizedTestGroupMembershipRepository.Value.GetManagedGuarantorIds(vpGuarantorId));

                if (consolidatedGuarantorIds.Any() && randomizedTestGroup == null)
                {
                    foreach (int consolidatedGuarantorId in consolidatedGuarantorIds)
                    {
                        randomizedTestGroupMembership = this._randomizedTestGroupMembershipRepository.Value.GetRandomizedTestGroupMembership(randomizedTest, consolidatedGuarantorId);
                        if (randomizedTestGroupMembership != null)
                        {
                            randomizedTestGroup = randomizedTestGroupMembership.RandomizedTestGroup;
                            break;
                        }
                    }
                }

                // create a new assignment with either the existing the optional value passed in, the consolidated relationship value, or a new randomized group
                randomizedTestGroupMembership = new RandomizedTestGroupMembership()
                {
                    RandomizedTest = randomizedTest,
                    VpGuarantorId = vpGuarantorId,
                    RandomizedTestGroup = randomizedTestGroup ?? this.GetWeightedRandomizedTestGroup(randomizedTest)
                };

                //save it
                this._randomizedTestGroupMembershipRepository.Value.Insert(randomizedTestGroupMembership);

                // increment metrics for the selected test group
                this._metricsProvider.Value.Increment($"RandomizedTest:{randomizedTestGroupMembership.RandomizedTest.Name}::{randomizedTestGroupMembership.RandomizedTestGroup.Name}");

                // do for consolidated relationships
                foreach (int consolidatedGuarantorId in consolidatedGuarantorIds)
                {
                    this.GetRandomizedTestGroupMembershipInternal(randomizedTest, consolidatedGuarantorId, randomizedTestGroupMembership.RandomizedTestGroup);
                }
            }

            return randomizedTestGroupMembership.RandomizedTestGroup;
        }

        public RandomizedTestGroup GetRandomizedTestGroupMembership(RandomizedTestEnum randomizedTest, int vpGuarantorId, bool assignIfNotAssigned = true, RandomizedTestGroupEnum? randomizedTestGroup = null)
        {
            RandomizedTest randomizedTest1 = this.GetRandomizedTest(randomizedTest);
            if (randomizedTest1 == null)
            {
                return null;
            }

            if (assignIfNotAssigned)
            {
                RandomizedTestGroup assignedGroup = null;
                if (randomizedTestGroup != null)
                {
                    assignedGroup = randomizedTest1.RandomizedTestGroups.FirstOrDefault(x => x.RandomizedTestGroupId == (int)randomizedTestGroup);
                }

                return this.GetRandomizedTestGroupMembershipInternal(randomizedTest1, vpGuarantorId, assignedGroup);
            }

            return this._randomizedTestGroupMembershipRepository.Value.GetRandomizedTestGroupMembership(randomizedTest1, vpGuarantorId)?.RandomizedTestGroup;
        }
    }
}
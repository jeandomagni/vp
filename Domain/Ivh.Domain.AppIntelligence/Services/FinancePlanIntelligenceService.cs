﻿namespace Ivh.Domain.AppIntelligence.Services
{
    using System;
    using Base.Interfaces;
    using Base.Services;
    using Entities;
    using Interfaces;

    public class FinancePlanIntelligenceService : DomainService, IFinancePlanIntelligenceService
    {
        private readonly Lazy<IFinancePlanTermsDisplayedRepository> _financePlanTermsDisplayedRepository;

        public FinancePlanIntelligenceService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IFinancePlanTermsDisplayedRepository> financePlanTermsDisplayedRepository) : base(serviceCommonService)
        {
            this._financePlanTermsDisplayedRepository = financePlanTermsDisplayedRepository;
        }

        public void LogTermsDisplayed(FinancePlanTermsDisplayed financePlanTermsDisplayed)
        {
            this._financePlanTermsDisplayedRepository.Value.Insert(financePlanTermsDisplayed);
        }
    }
}
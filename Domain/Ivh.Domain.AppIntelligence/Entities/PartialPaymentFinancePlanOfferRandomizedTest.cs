﻿using System.Linq;
using Ivh.Common.Base.Enums;

namespace Ivh.Domain.AppIntelligence.Entities
{
    using Common.VisitPay.Enums;

    public class PartialPaymentFinancePlanOfferRandomizedTest
    {
        private readonly RandomizedTest _randomizedTest;

        public PartialPaymentFinancePlanOfferRandomizedTest(RandomizedTest randomizedTest)
        {
            this._randomizedTest = randomizedTest;
        }

        public PartialPaymentFinancePlanOfferRandomizedTestGroupConfiguration GetTestGroupConfiguration(RandomizedTestGroupEnum randomizedTestGroupEnum)
        {
            RandomizedTestGroup group = this._randomizedTest.RandomizedTestGroups.FirstOrDefault(x => x.RandomizedTestGroupId == (int)randomizedTestGroupEnum);
            return group == null ? null : new PartialPaymentFinancePlanOfferRandomizedTestGroupConfiguration(group.ConfigurationJson);
        }
    }
}

﻿using Newtonsoft.Json;

namespace Ivh.Domain.AppIntelligence.Entities
{
    public class BalanceBasedDiscountConfiguration
    {
        public BalanceBasedDiscountConfiguration()
        {
        }

        public BalanceBasedDiscountConfiguration(string configurationJson)
        {
            if (string.IsNullOrWhiteSpace(configurationJson))
            {
                return;
            }

            try
            {
                BalanceBasedDiscountConfiguration deserialized = JsonConvert.DeserializeObject<BalanceBasedDiscountConfiguration>(configurationJson);
                this.EnableBalanceBasedDiscount = deserialized.EnableBalanceBasedDiscount;
            }
            catch
            {
                // invalid json
            }
        }

        public bool EnableBalanceBasedDiscount { get; set; }
    }
}

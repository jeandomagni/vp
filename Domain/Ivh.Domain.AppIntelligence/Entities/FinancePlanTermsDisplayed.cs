﻿namespace Ivh.Domain.AppIntelligence.Entities
{
    using System;
    using Common.VisitPay.Enums;

    public class FinancePlanTermsDisplayed
    {
        public FinancePlanTermsDisplayed()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        public virtual int FinancePlanTermsDisplayedId { get; set; }

        public virtual int VpGuarantorId { get; set; }

        public virtual DateTime EffectiveDate { get; set; }

        public virtual DateTime FinalPaymentDate { get; set; }

        public virtual decimal? FinancePlanBalance { get; set; }

        public virtual FinancePlanOfferSetTypeEnum FinancePlanOfferSetType { get; set; }

        public virtual DateTime InsertDate { get; set; }

        public virtual decimal InterestRate { get; set; }

        public virtual bool IsCombined { get; set; }

        public virtual int? MaximumNumberOfPayments { get; set; }

        public virtual decimal? MinimumMonthlyPaymentAmount { get; set; }

        public virtual decimal MonthlyPaymentAmount { get; set; }

        public virtual decimal MonthlyPaymentAmountOtherPlans { get; set; }

        public virtual decimal MonthlyPaymentAmountPrevious { get; set; }

        public virtual decimal MonthlyPaymentAmountTotal { get; set; }

        public virtual int NumberMonthlyPayments { get; set; }

        public virtual decimal TotalInterest { get; set; }

        public virtual bool IsClient { get; set; }
    }
}
﻿namespace Ivh.Domain.AppIntelligence.Entities
{
    using System.Linq;
    using Common.VisitPay.Enums;

    public class FinancePlanSuggestedDurationRandomizedTest
    {
        private readonly RandomizedTest _randomizedTest;

        public FinancePlanSuggestedDurationRandomizedTest(RandomizedTest randomizedTest)
        {
            this._randomizedTest = randomizedTest;
        }

        public FinancePlanSuggestedDurationRandomizedTestGroupConfiguration GetTestGroupConfiguration(RandomizedTestGroupEnum randomizedTestGroupEnum)
        {
            RandomizedTestGroup group = this._randomizedTest.RandomizedTestGroups.FirstOrDefault(x => x.RandomizedTestGroupId == (int) randomizedTestGroupEnum);
            return group == null ? null : new FinancePlanSuggestedDurationRandomizedTestGroupConfiguration(group.ConfigurationJson);
        }
    }
}
﻿namespace Ivh.Domain.AppIntelligence.Entities
{
    using Newtonsoft.Json;

    public class FinancePlanSuggestedAmountRandomizedTestGroupConfiguration
    {
        public FinancePlanSuggestedAmountRandomizedTestGroupConfiguration()
        {
        }

        public FinancePlanSuggestedAmountRandomizedTestGroupConfiguration(string configurationJson)
        {
            if (string.IsNullOrWhiteSpace(configurationJson))
            {
                return;
            }

            try
            {
                FinancePlanSuggestedAmountRandomizedTestGroupConfiguration deserialized = JsonConvert.DeserializeObject<FinancePlanSuggestedAmountRandomizedTestGroupConfiguration>(configurationJson);
                this.Amount = deserialized.Amount;
                this.MaxAmount = deserialized.MaxAmount;
            }
            catch
            {
                // invalid json
            }
        }

        public decimal Amount { get; set; }
        public decimal? MaxAmount { get; set; }
    }
}

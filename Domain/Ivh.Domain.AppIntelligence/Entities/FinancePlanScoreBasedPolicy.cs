﻿namespace Ivh.Domain.AppIntelligence.Entities
{
    public class FinancePlanScoreBasedPolicy
    {
        public decimal MinAmount { get; set; }

        public decimal MaxAmount { get; set; }

        public int MinPtp { get; set; }

        public int MaxPtp { get; set; }

        public decimal SuggestedAmount;
    }
}

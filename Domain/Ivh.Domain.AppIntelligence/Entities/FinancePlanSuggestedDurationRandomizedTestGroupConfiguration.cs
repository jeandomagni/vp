﻿namespace Ivh.Domain.AppIntelligence.Entities
{
    using Newtonsoft.Json;

    public class FinancePlanSuggestedDurationRandomizedTestGroupConfiguration
    {
        public FinancePlanSuggestedDurationRandomizedTestGroupConfiguration()
        {
        }

        public FinancePlanSuggestedDurationRandomizedTestGroupConfiguration(string configurationJson)
        {
            if (string.IsNullOrWhiteSpace(configurationJson))
            {
                return;
            }

            try
            {
                FinancePlanSuggestedDurationRandomizedTestGroupConfiguration deserialized = JsonConvert.DeserializeObject<FinancePlanSuggestedDurationRandomizedTestGroupConfiguration>(configurationJson);
                this.Duration = deserialized.Duration;
                this.MaximumMonthlyPaymentAmount = deserialized.MaximumMonthlyPaymentAmount;
            }
            catch
            {
                // invalid json
            }
        }

        public int? Duration { get; set; }
        public decimal? MaximumMonthlyPaymentAmount { get; set; }
    }
}
﻿using System.Linq;

namespace Ivh.Domain.AppIntelligence.Entities
{
    using Common.VisitPay.Enums;

    public class BalanceBasedDiscountRandomizedTest
    {
        private readonly RandomizedTest _randomizedTest;

        public BalanceBasedDiscountRandomizedTest(RandomizedTest randomizedTest)
        {
            this._randomizedTest = randomizedTest;
        }

        public BalanceBasedDiscountConfiguration GetTestGroupConfiguration(RandomizedTestGroupEnum randomizedTestGroupEnum)
        {
            RandomizedTestGroup group = this._randomizedTest.RandomizedTestGroups.FirstOrDefault(x => x.RandomizedTestGroupId == (int)randomizedTestGroupEnum);
            return group == null ? null : new BalanceBasedDiscountConfiguration(group.ConfigurationJson);
        }
    }
}

﻿namespace Ivh.Domain.AppIntelligence.Entities
{
    using System;

    public class RandomizedTestGroup
    {
        public RandomizedTestGroup()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        public virtual int RandomizedTestGroupId { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual RandomizedTest RandomizedTest { get; set; }
        public virtual string Name { get; set; }
        public virtual decimal Weighting { get; set; }
        public virtual string Details { get; set; }
        public virtual string ConfigurationJson { get; set; }
    }
}
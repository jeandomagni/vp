﻿using Newtonsoft.Json;

namespace Ivh.Domain.AppIntelligence.Entities
{
    public class PartialPaymentFinancePlanOfferRandomizedTestGroupConfiguration
    {
        public PartialPaymentFinancePlanOfferRandomizedTestGroupConfiguration()
        {
        }

        public PartialPaymentFinancePlanOfferRandomizedTestGroupConfiguration(string configurationJson)
        {
            if (string.IsNullOrWhiteSpace(configurationJson))
            {
                return;
            }

            try
            {
                PartialPaymentFinancePlanOfferRandomizedTestGroupConfiguration deserialized = JsonConvert.DeserializeObject<PartialPaymentFinancePlanOfferRandomizedTestGroupConfiguration>(configurationJson);
                this.MinimumRequiredMonthlyPayments = deserialized.MinimumRequiredMonthlyPayments;
                this.NudgePercentage = deserialized.NudgePercentage;
            }
            catch
            {
                // invalid json
            }
        }

        public decimal? MinimumRequiredMonthlyPayments { get; set; }
        public decimal? NudgePercentage { get; set; }
    }
}

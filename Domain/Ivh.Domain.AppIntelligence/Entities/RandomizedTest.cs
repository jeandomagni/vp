﻿namespace Ivh.Domain.AppIntelligence.Entities
{
    using System;
    using System.Collections.Generic;

    public class RandomizedTest
    {
        public RandomizedTest()
        {
            this.InsertDate = DateTime.UtcNow;
            this.RandomizedTestGroups = new List<RandomizedTestGroup>();
        }

        public virtual int RandomizedTestId { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual string Name { get; set; }
        public virtual DateTime BeginDateTime { get; set; }
        public virtual DateTime EndDateTime { get; set; }
        public virtual IList<RandomizedTestGroup> RandomizedTestGroups { get; set; }
        public virtual string Details { get; set; }

        public virtual bool IsActive()
        {
            return this.BeginDateTime <= DateTime.UtcNow && this.EndDateTime >= DateTime.UtcNow;
        }
    }
}
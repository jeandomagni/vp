﻿namespace Ivh.Domain.AppIntelligence.Entities
{
    using System.Linq;
    using Common.VisitPay.Enums;

    public class FinancePlanSuggestedAmountRandomizedTest
    {
        private readonly RandomizedTest _randomizedTest;

        public FinancePlanSuggestedAmountRandomizedTest(RandomizedTest randomizedTest)
        {
            this._randomizedTest = randomizedTest;
        }

        public FinancePlanSuggestedAmountRandomizedTestGroupConfiguration GetTestGroupConfiguration(RandomizedTestGroupEnum randomizedTestGroupEnum)
        {
            RandomizedTestGroup group = this._randomizedTest.RandomizedTestGroups.FirstOrDefault(x => x.RandomizedTestGroupId == (int)randomizedTestGroupEnum);
            return group == null ? null : new FinancePlanSuggestedAmountRandomizedTestGroupConfiguration(group.ConfigurationJson);
        }
    }
}

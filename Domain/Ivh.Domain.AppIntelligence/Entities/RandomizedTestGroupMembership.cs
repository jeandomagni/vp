﻿namespace Ivh.Domain.AppIntelligence.Entities
{
    using System;

    public class RandomizedTestGroupMembership
    {
        public RandomizedTestGroupMembership()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int RandomizedTestGroupMembershipId { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual RandomizedTest RandomizedTest { get; set; }
        public virtual RandomizedTestGroup RandomizedTestGroup { get; set; }
        public virtual int VpGuarantorId { get; set; }
    }
}
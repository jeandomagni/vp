﻿namespace Ivh.Domain.AppIntelligence.Entities
{
    using System.Linq;
    using Common.VisitPay.Enums;

    public class FinancePlanScoreBasedSuggestedAmount
    {
        private readonly RandomizedTest _randomizedTest;

        public FinancePlanScoreBasedSuggestedAmount(RandomizedTest randomizedTest)
        {
            this._randomizedTest = randomizedTest;
        }

        public FinancePlanScoreBasedSuggestedAmountGroupConfiguration GetTestGroupConfiguration(RandomizedTestGroupEnum randomizedTestGroupEnum)
        {
            RandomizedTestGroup group = this._randomizedTest.RandomizedTestGroups.FirstOrDefault(x => x.RandomizedTestGroupId == (int)randomizedTestGroupEnum);
            return group == null ? null : new FinancePlanScoreBasedSuggestedAmountGroupConfiguration(group.ConfigurationJson);
        }
    }
}

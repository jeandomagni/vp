﻿namespace Ivh.Domain.AppIntelligence.Entities
{
    using Newtonsoft.Json;
    using System.Collections.Generic;

    public class FinancePlanScoreBasedSuggestedAmountGroupConfiguration
    {
        public FinancePlanScoreBasedSuggestedAmountGroupConfiguration()
        {
        }

        public FinancePlanScoreBasedSuggestedAmountGroupConfiguration(string configurationJson)
        {
            if (string.IsNullOrWhiteSpace(configurationJson))
            {
                return;
            }

            try
            {
                this.FinancePlanScoreBasedPolicies = JsonConvert.DeserializeObject<List<FinancePlanScoreBasedPolicy>>(configurationJson);
            }
            catch
            {
                // invalid json
            }
        }

        public List<FinancePlanScoreBasedPolicy> FinancePlanScoreBasedPolicies { get; set; }

    }
}

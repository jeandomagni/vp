﻿namespace Ivh.Domain.AppIntelligence.Survey.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface ISurveyService : IDomainService
    {
        List<SurveyResult> GetSurveyResultsForVisitPayUser(int visitpayUser, string surveyName);
        List<SurveyResult> GetAllSurveyResultsForVisitPayUser(int visitpayUser);
        Survey GetSurveyBySurveyName(string surveyName);
        List<Survey> GetActiveSurveysByGroup(SurveyGroupEnum surveyGroup);
        void ProcessSurveyResults(SurveyResult surveyResult);
    }
}

﻿namespace Ivh.Domain.AppIntelligence.Survey.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface ISurveyRepository: IRepository<Survey>
    {
        Survey GetSurveyBySurveyName(string surveyName);
    }
}

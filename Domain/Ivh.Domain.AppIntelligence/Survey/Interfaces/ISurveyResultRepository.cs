﻿namespace Ivh.Domain.AppIntelligence.Survey.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface ISurveyResultRepository : IRepository<SurveyResult>
    {
        IList<SurveyResult> GetSurveyResultsForVisitPayUser(int visitpayUser, int surveyId);
        IList<SurveyResult> GetAllSurveyResultsForVisitPayUser(int visitpayUser);
    }
}
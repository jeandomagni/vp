﻿namespace Ivh.Domain.AppIntelligence.Survey.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;

    public class SurveyService :  ISurveyService
    {
        private readonly Lazy<ISurveyRepository> _surveyRepository;
        private readonly Lazy<ISurveyResultRepository> _surveyResultRepository;

        public SurveyService(Lazy<ISurveyRepository> surveyRepository,
            Lazy<ISurveyResultRepository> surveyResultRepository)
        {
            this._surveyRepository = surveyRepository;
            this._surveyResultRepository = surveyResultRepository;
        }

        public List<SurveyResult> GetSurveyResultsForVisitPayUser(int visitpayUser, string surveyName)
        {
            Survey survey = this.GetSurveyBySurveyName(surveyName);
            IList<SurveyResult> surveyResults = this._surveyResultRepository.Value.GetSurveyResultsForVisitPayUser(visitpayUser, survey.SurveyId);
            return surveyResults.ToList();
        }

        public List<SurveyResult> GetAllSurveyResultsForVisitPayUser(int visitpayUser)
        {
            return this._surveyResultRepository.Value.GetAllSurveyResultsForVisitPayUser(visitpayUser).ToList();
        }

        public Survey GetSurveyBySurveyName(string surveyName)
        {
            return this._surveyRepository.Value.GetSurveyBySurveyName(surveyName);
        }

        public List<Survey> GetActiveSurveysByGroup(SurveyGroupEnum surveyGroup)
        {
            List<Survey> surveysInGroup = this._surveyRepository.Value
                .GetQueryable()
                .Where(x => x.SurveyGroup == surveyGroup)
                .ToList()//nHibernate complained about IsActive property 
                .Where(x => x.IsActive)
                .ToList();

            return surveysInGroup;
        }

        public void ProcessSurveyResults(SurveyResult surveyResult)
        {
            this._surveyResultRepository.Value.InsertOrUpdate(surveyResult);
        }
    }
}
﻿namespace Ivh.Domain.AppIntelligence.Survey.Entities
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class SurveyRating
    {
        public virtual int SurveyRatingId { get; set; }
        public virtual SurveyRatingGroup SurveyRatingGroup { get; set; }
        public virtual CmsRegionEnum DescriptionCmsRegion { get; set; }
        public virtual int SurveyRatingScore { get; set; }
    }
}
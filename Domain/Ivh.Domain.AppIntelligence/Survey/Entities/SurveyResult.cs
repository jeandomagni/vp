﻿namespace Ivh.Domain.AppIntelligence.Survey.Entities
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class SurveyResult
    {
        public virtual int SurveyResultId { get; set; }
        public virtual SurveyResultTypeEnum SurveyResultType { get; set; }
        public virtual int? SurveyId { get; set; }
        public virtual DateTime SurveyDate { get; set; }
        public virtual int? VisitPayUserId { get; set; }
        public virtual int? FinancePlanId { get; set; }
        public virtual int? PaymentId { get; set; }
        public virtual int? VpStatementId { get; set; }
        public virtual string SurveyComment { get; set; }
        public virtual IList<SurveyResultAnswer> SurveyResultAnswers { get; set; }
        public virtual string DeviceType { get; set; }

    }
}
﻿namespace Ivh.Domain.AppIntelligence.Survey.Entities
{
    public class SurveyResultAnswer
    {
        public virtual int SurveyResultAnswerId { get; set; }
        public virtual SurveyResult SurveyResult { get; set; }
        public virtual string QuestionText { get; set; }
        public virtual int SurveyQuestionRating { get; set; }
        public virtual int  SurveyQuestionId { get; set; }
        public virtual string SurveyRatingDescriptionText { get; set; }
    }
}
﻿namespace Ivh.Domain.AppIntelligence.Survey.Entities
{
    public class SurveyResultType
    {
        public virtual int SurveyResultTypeId { get; set; }
        public virtual string SurveyResultTypeName { get; set; }
    }
}
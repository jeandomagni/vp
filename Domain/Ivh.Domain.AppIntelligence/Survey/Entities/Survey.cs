﻿namespace Ivh.Domain.AppIntelligence.Survey.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class Survey
    {
        public virtual int SurveyId { get; set; }
        public virtual string SurveyName { get; set; } 
        public virtual DateTime? DateActive { get; set; }
        public virtual DateTime? DateExpires { get; set; }
        
        //This will be the tile that shows at the top of survey        
        public virtual CmsRegionEnum TitleCmsRegion { get; set; }
        
        //The user will be presented the survey at different intervals based upon whether the user ignored or submitted a survey
        public virtual int FrequencyInDaysWhenIgnored { get; set; }
        public virtual int FrequencyInDays { get; set; }
        public virtual bool UseNavigationCount { get; set; }

        public virtual SurveyGroupEnum SurveyGroup { get; set; }

        public virtual IList<SurveyQuestion> SurveyQuestionsAll { get; set; } = new List<SurveyQuestion>();
        public virtual IList<SurveyQuestion> SurveyQuestions => this.SurveyQuestionsAll.Where(x => x.IsActive).ToList();

        public virtual bool IsActive => (this.DateExpires > DateTime.UtcNow || this.DateExpires == null)
                                        && (this.DateActive < DateTime.UtcNow || this.DateActive == null);


    }
}
﻿namespace Ivh.Domain.AppIntelligence.Survey.Entities
{
    using System.Collections.Generic;

    public class SurveyRatingGroup
    {
        public virtual int SurveyRatingGroupId { get; set; }
        public virtual string SurveyRatingGroupName { get; set; }
        public virtual IList<SurveyRating> SurveyRatings { get; set; }
    }
}
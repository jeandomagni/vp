﻿namespace Ivh.Domain.AppIntelligence.Survey.Entities
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class SurveyQuestion
    {
        public virtual int SurveyQuestionId { get; set; }
        public virtual Survey Survey { get; set; }
        public virtual CmsRegionEnum QuestionCmsRegion { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual SurveyRatingGroup SurveyRatingGroup { get; set; }
    }
}
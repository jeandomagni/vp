﻿namespace Ivh.Domain.AppIntelligence.Survey.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class SurveyMap : ClassMap<Survey>
    {
        public SurveyMap()
        {
            //TODO: consider nullable versus not nullable
            this.Table("Survey"); //todo: use nameof after TC upgrade
            this.Schema("intelligence");
            this.Id(x => x.SurveyId).GeneratedBy.Assigned(); ;
            this.Map(x => x.SurveyName);
            this.Map(x => x.TitleCmsRegion, "TitleCmsRegionId").CustomType<CmsRegionEnum>().Not.Nullable();
            this.Map(x => x.DateActive);
            this.Map(x => x.DateExpires);
            this.Map(x => x.FrequencyInDays);
            this.Map(x => x.FrequencyInDaysWhenIgnored);
            this.Map(x => x.UseNavigationCount);

            this.Map(x => x.SurveyGroup, "SurveyGroupId").CustomType<SurveyGroupEnum>();

            this.HasMany(x => x.SurveyQuestionsAll)
           .KeyColumn("SurveyId")
           .Inverse()
           .Not.KeyNullable()
           .Not.KeyUpdate()
           .Not.LazyLoad()
           .BatchSize(50)
           .Cascade.AllDeleteOrphan();
        }
    }
}
﻿namespace Ivh.Domain.AppIntelligence.Survey.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class SurveyRatingGroupMap : ClassMap<SurveyRatingGroup>
    {
        public SurveyRatingGroupMap()
        {
            this.Table("SurveyRatingGroup");
            this.Schema("intelligence");
            this.Id(x => x.SurveyRatingGroupId).GeneratedBy.Assigned();
            this.Map(x => x.SurveyRatingGroupName);

            this.HasMany(x => x.SurveyRatings)
                .KeyColumn("SurveyRatingGroupId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();
        }
    }
}
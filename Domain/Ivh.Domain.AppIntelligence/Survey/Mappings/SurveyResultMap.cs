﻿namespace Ivh.Domain.AppIntelligence.Survey.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class SurveyResultMap : ClassMap<SurveyResult>
    {
        public SurveyResultMap()
        {
            this.Table("SurveyResult"); 
            this.Schema("intelligence");
            this.Id(x => x.SurveyResultId);
            this.Map(x => x.SurveyId);
            this.Map(x => x.VisitPayUserId).Not.Nullable();
            this.Map(x => x.SurveyDate).Not.Nullable();
            this.Map(x => x.FinancePlanId);
            this.Map(x => x.PaymentId);
            this.Map(x => x.VpStatementId);
            this.Map(x => x.SurveyComment);
            this.Map(x => x.DeviceType);

            this.Map(x => x.SurveyResultType, "SurveyResultTypeId").CustomType<SurveyResultTypeEnum>().Not.Nullable();

            this.HasMany(x => x.SurveyResultAnswers)
              .KeyColumn("SurveyResultId")
              .Inverse()
              .Not.KeyNullable()
              .Not.KeyUpdate()
              .Not.LazyLoad()
              .BatchSize(50)
              .Cascade.AllDeleteOrphan();

        }
    }
}
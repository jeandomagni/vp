﻿namespace Ivh.Domain.AppIntelligence.Survey.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class SurveyResultAnswerMap : ClassMap<SurveyResultAnswer>
    {
        public SurveyResultAnswerMap()
        {
            this.Table("SurveyResultAnswer");
            this.Schema("intelligence");
            this.Id(x => x.SurveyResultAnswerId);
            this.References(x => x.SurveyResult).Column("SurveyResultId");
            this.Map(x => x.SurveyQuestionId);
            this.Map(x => x.QuestionText);
            this.Map(x => x.SurveyQuestionRating);
            this.Map(x => x.SurveyRatingDescriptionText);
        }
    }
}
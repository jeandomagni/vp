﻿namespace Ivh.Domain.AppIntelligence.Survey.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class SurveyQuestionMap : ClassMap<SurveyQuestion>
    {
        public SurveyQuestionMap()
        {
            this.Table("SurveyQuestion");
            this.Schema("intelligence");
            this.Id(x => x.SurveyQuestionId).GeneratedBy.Assigned(); 
            this.References(x => x.Survey).Column("SurveyId");
            this.Map(x => x.QuestionCmsRegion, "QuestionCmsRegionId").CustomType<CmsRegionEnum>().Not.Nullable();
            this.Map(x => x.IsActive).Not.Nullable();
            this.References(x => x.SurveyRatingGroup).Column("SurveyRatingGroupId");
        }
    }
}
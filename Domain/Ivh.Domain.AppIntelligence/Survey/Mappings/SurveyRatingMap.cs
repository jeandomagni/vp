﻿namespace Ivh.Domain.AppIntelligence.Survey.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class SurveyRatingMap : ClassMap<SurveyRating>
    {
        public SurveyRatingMap()
        {
            this.Table("SurveyRating");
            this.Schema("intelligence");
            this.Id(x => x.SurveyRatingId).GeneratedBy.Identity();
            this.References(x => x.SurveyRatingGroup).Column("SurveyRatingGroupId");
            this.Map(x => x.DescriptionCmsRegion, "DescriptionCmsRegionId").CustomType<CmsRegionEnum>().Not.Nullable();
            this.Map(x => x.SurveyRatingScore);
        }
    }
}

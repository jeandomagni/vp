﻿namespace Ivh.Domain.AppIntelligence.Survey.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class SurveyResultTypeMap : ClassMap<SurveyResultType>
    {
        public SurveyResultTypeMap()
        {
            this.Schema("intelligence");
            this.Table("SurveyResultType");
            this.Id(x => x.SurveyResultTypeId).GeneratedBy.Assigned(); 
            this.Map(x => x.SurveyResultTypeName).Not.Nullable();
        }
    }
}
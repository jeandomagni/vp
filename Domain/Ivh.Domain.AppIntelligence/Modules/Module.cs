﻿namespace Ivh.Domain.AppIntelligence.Modules
{
    using Autofac;
    using Interfaces;
    using Services;
    using Survey.Interfaces;
    using Survey.Services;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<FinancePlanIntelligenceService>().As<IFinancePlanIntelligenceService>();
            builder.RegisterType<RandomizedTestService>().As<IRandomizedTestService>();
            builder.RegisterType<SurveyService>().As<ISurveyService>();
        }
    }
}

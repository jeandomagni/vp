﻿namespace Ivh.Domain.AppIntelligence.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class RandomizedTestMap : ClassMap<RandomizedTest>
    {
        public RandomizedTestMap()
        {
            this.Schema("intelligence");
            this.Table("RandomizedTest");
            this.ReadOnly();
            this.Id(x => x.RandomizedTestId);
            this.Map(x => x.InsertDate);
            this.HasMany(x => x.RandomizedTestGroups).KeyColumn("RandomizedTestId")
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.All()
                .Cache.ReadOnly();

            this.Map(x => x.Name).Not.Nullable();
            this.Map(x => x.BeginDateTime).Not.Nullable();
            this.Map(x => x.EndDateTime).Not.Nullable();
            this.Map(x => x.Details).Nullable();
            this.Cache.ReadOnly();
        }
    }
}
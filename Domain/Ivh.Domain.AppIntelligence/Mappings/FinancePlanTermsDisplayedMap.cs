﻿namespace Ivh.Domain.AppIntelligence.Mappings
{
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class FinancePlanTermsDisplayedMap : ClassMap<FinancePlanTermsDisplayed>
    {
        public FinancePlanTermsDisplayedMap()
        {
            this.Schema("intelligence");
            this.Table("FinancePlanTermsDisplayed");
            this.Id(x => x.FinancePlanTermsDisplayedId);
            this.Map(x => x.VpGuarantorId);
            this.Map(x => x.EffectiveDate);
            this.Map(x => x.FinalPaymentDate);
            this.Map(x => x.FinancePlanBalance).Nullable();
            this.Map(x => x.FinancePlanOfferSetType).Column("FinancePlanOfferSetTypeId").CustomType<FinancePlanOfferSetTypeEnum>().Not.Nullable();
            this.Map(x => x.InsertDate);
            this.Map(x => x.InterestRate);
            this.Map(x => x.IsCombined);
            this.Map(x => x.MaximumNumberOfPayments).Nullable();
            this.Map(x => x.MinimumMonthlyPaymentAmount).Nullable();
            this.Map(x => x.MonthlyPaymentAmount);
            this.Map(x => x.MonthlyPaymentAmountOtherPlans);
            this.Map(x => x.MonthlyPaymentAmountPrevious);
            this.Map(x => x.MonthlyPaymentAmountTotal);
            this.Map(x => x.NumberMonthlyPayments);
            this.Map(x => x.TotalInterest);
            this.Map(x => x.IsClient);
        }
    }
}
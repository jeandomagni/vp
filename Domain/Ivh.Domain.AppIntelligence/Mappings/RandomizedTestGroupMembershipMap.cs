﻿namespace Ivh.Domain.AppIntelligence.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class RandomizedTestGroupMembershipMap : ClassMap<RandomizedTestGroupMembership>
    {
        public RandomizedTestGroupMembershipMap()
        {
            this.Schema("intelligence");
            this.Table("RandomizedTestGroupMembership");
            this.Id(x => x.RandomizedTestGroupMembershipId);
            this.Map(x => x.InsertDate);
            this.References(x => x.RandomizedTest).Column("RandomizedTestId").Not.Nullable();
            this.References(x => x.RandomizedTestGroup).Column("RandomizedTestGroupId").Not.Nullable();
            this.Map(x => x.VpGuarantorId).Not.Nullable();
            
        }
    }
}
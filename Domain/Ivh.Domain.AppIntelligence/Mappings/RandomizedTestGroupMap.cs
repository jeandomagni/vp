﻿namespace Ivh.Domain.AppIntelligence.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class RandomizedTestGroupMap : ClassMap<RandomizedTestGroup>
    {
        public RandomizedTestGroupMap()
        {
            this.Schema("intelligence");
            this.Table("RandomizedTestGroup");
            this.ReadOnly();
            this.Id(x => x.RandomizedTestGroupId);
            this.Map(x => x.InsertDate);
            this.References(x => x.RandomizedTest).Column("RandomizedTestId").Not.Nullable();
            this.Map(x => x.Name).Not.Nullable();
            this.Map(x => x.Weighting).Not.Nullable();
            this.Map(x => x.Details).Nullable();
            this.Map(x => x.ConfigurationJson).Nullable();
            this.Cache.ReadOnly();
        }
    }
}
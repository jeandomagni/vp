﻿
namespace Ivh.Domain.Email.Entities
{
    using Ivh.Domain.HospitalData.Outbound.Enums;
    using System;

    public class CommunicationDocument
    {
        public CommunicationDocument()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        public virtual int CommunicationDocumentId { get; set; }
        public virtual int CommunicationId { get; set; }
        public virtual VpOutboundFileTypeEnum VpOutboundFileType { get; set; }
        public virtual Guid FileStorageExternalKey { get; set; }
        public virtual string FileName { get; set; }
        public virtual string MimeType { get; set; }
        public virtual DateTime InsertDate { get; set; }
    }
}

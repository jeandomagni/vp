namespace Ivh.Domain.Email.Entities
{
    using System;

    public class CommunicationEvent
    {
        public virtual int CommunicationEventId { get; protected set; }
        public virtual Guid UniqueId { get; protected set; }
        public virtual string ToAddress { get; protected set; }
        public virtual string EventName { get; protected set; }
        public virtual string Category { get; protected set; }
        public virtual string Response { get; protected set; }
        public virtual string Attempt { get; protected set; }
        public virtual string EventStatus { get; protected set; }
        public virtual string Reason { get; protected set; }
        public virtual string EventType { get; protected set; }
        public virtual string SourceApp { get; protected set; }
        public virtual string EventId { get; protected set; }
        public virtual string GroupId { get; protected set; }
        public virtual string IP { get; protected set; }
        public virtual string SmtpId { get; protected set; }
        public virtual string Timestamp { get; protected set; }
        public virtual string Url { get; protected set; }
        public virtual string UserAgent { get; protected set; }
        public virtual string RawData { get; protected set; }
        public virtual DateTime? EventDateTime { get; protected set; }
        public virtual DateTime InsertedDate { get; protected set; }
        public virtual Communication Communication { get; set; }
    }
}
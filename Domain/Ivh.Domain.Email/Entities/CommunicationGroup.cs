﻿namespace Ivh.Domain.Email.Entities
{
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class CommunicationGroup
    {
        public virtual int CommunicationGroupId { get; set; }

        public virtual string CommunicationGroupName { get; set; }

        public virtual string IconType { get; set; }

        public virtual bool IsActive { get; set; }

        public virtual CmsRegionEnum? DescriptionCmsRegion { get; set; }

        public virtual ICollection<CommunicationType> CommunicationTypes { get; set; }
    }
}
﻿namespace Ivh.Domain.Email.Entities
{
    using System;

    public class CommunicationTracker
    {
        public CommunicationTracker()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int CommunicationTrackerId { get; set; }
        public virtual int? FinancePlanId { get; set; }
        public virtual int? VpStatementId { get; set; }
        public virtual int? PaymentProcessorResponseId { get; set; }
        public virtual CommunicationType CommunicationType { get; set; }
        public virtual DateTime InsertDate { get; set; }
    }
}

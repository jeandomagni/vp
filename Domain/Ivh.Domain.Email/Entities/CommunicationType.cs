﻿namespace Ivh.Domain.Email.Entities
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Content.Entities;

    public class CommunicationType
    {
        public virtual CommunicationTypeEnum CommunicationTypeId { get; set; }
        public virtual string CommunicationTypeName { get; set; }
        public virtual CmsRegion CmsRegion { get; set; }
        public virtual CommunicationTypeStatusEnum CommunicationTypeStatus { get; set; }
        public virtual int? CommunicationGroupId { get; set; }
        public virtual CmsRegion TemplateCmsRegion { get; set; }
        public virtual CommunicationMethod CommunicationMethod { get; set; }

        public override bool Equals(object obj)
        {
            CommunicationType t = obj as CommunicationType;
            if (t == null)
            {
                return false;
            }

            if (this.CommunicationTypeId == t.CommunicationTypeId
                && this.CommunicationTypeName == t.CommunicationTypeName
                && (this.CmsRegion == null && t.CmsRegion == null || this.CmsRegion != null && t.CmsRegion != null && this.CmsRegion.CmsRegionId == t.CmsRegion.CmsRegionId)
                && (this.TemplateCmsRegion == null && t.TemplateCmsRegion == null || this.TemplateCmsRegion != null && t.TemplateCmsRegion != null && this.TemplateCmsRegion.CmsRegionId == t.TemplateCmsRegion.CmsRegionId)
                && this.CommunicationTypeStatus == t.CommunicationTypeStatus
                && this.CommunicationMethod.CommunicationMethodId == t.CommunicationMethod.CommunicationMethodId
                && this.CommunicationGroupId == t.CommunicationGroupId
            )
            {
                return true;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return (
                this.CommunicationTypeId + "|"
                                         + this.CommunicationTypeName + "|"
                                         + (this.CmsRegion != null ? this.CmsRegion.CmsRegionId.ToString() : "") + "|"
                                         + (this.TemplateCmsRegion != null ? this.TemplateCmsRegion.CmsRegionId.ToString() : "") + "|"
                                         + this.CommunicationTypeStatus + "|"
                                         + (this.CommunicationMethod != null ? this.CommunicationMethod.CommunicationMethodId.ToString() : "") + "|"
                                         + this.CommunicationGroupId
            ).GetHashCode();
        }

        public override string ToString()
        {
            return this.CommunicationTypeId + "|"
                                            + this.CommunicationTypeName + "|"
                                            + (this.CmsRegion != null ? this.CmsRegion.CmsRegionId.ToString() : "") + "|"
                                            + (this.TemplateCmsRegion != null ? this.TemplateCmsRegion.CmsRegionId.ToString() : "") + "|"
                                            + this.CommunicationTypeStatus + "|"
                                            + this.CommunicationMethod.CommunicationMethodId + "|"
                                            + this.CommunicationGroupId;
        }
    }
}
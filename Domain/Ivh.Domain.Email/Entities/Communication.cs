﻿using System;
using System.Collections.Generic;

namespace Ivh.Domain.Email.Entities
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class Communication
    {
        public virtual int CommunicationId { get; set; }
        public virtual Guid UniqueId { get; set; }
        public virtual string FromName { get; set; }
        public virtual string FromAddress { get; set; }
        public virtual string ToAddress { get; set; }
        public virtual string Cc { get; set; }
        public virtual string Bcc { get; set; }
        public virtual string Subject { get; set; }
        public virtual string Body { get; set; }
        public virtual int? SentToUserId { get; set; }
        public virtual int? SentToPaymentUnitId { get; set; }
        public virtual CommunicationStatusEnum CommunicationStatus { get; set; }
        public virtual int? SendFailureCnt { get; set; }
        public virtual Communication ResentCommunication { get; set; }
        public virtual DateTime CreatedDate { get; set; }
        public virtual DateTime? OutboundServerAccepted { get; set; }
        public virtual DateTime? ProcessedDate { get; set; }
        public virtual DateTime? DeliveredDate { get; set; }
        public virtual DateTime? FirstOpenDate { get; set; }
        public virtual DateTime? FailedDate { get; set; }
        public virtual IList<CommunicationEvent> CommunicationEvents { get; set; }
        public virtual CommunicationType CommunicationType { get; set; }
        public virtual string SerializedMessage { get; set; }
        public virtual string Locale { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.Email.Entities
{
    public class CommunicationMethod
    {
        public virtual int CommunicationMethodId { get; set; }
        public virtual string CommunicationMethodName { get; set; }
    }
}

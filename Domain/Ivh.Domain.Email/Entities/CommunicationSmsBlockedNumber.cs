﻿namespace Ivh.Domain.Email.Entities
{
    public class CommunicationSmsBlockedNumber
    {
        public virtual int CommunicationSmsBlockedNumberId { get; set; }
        public virtual string BlockedPhoneNumber { get; set; }
        public virtual string SmsPhoneNumber { get; set; }
    }
}
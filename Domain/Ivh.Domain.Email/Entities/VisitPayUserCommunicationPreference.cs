﻿namespace Ivh.Domain.Email.Entities
{
    using System;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class VisitPayUserCommunicationPreference
    {
        public virtual int VisitPayUserCommunicationPreferenceId { get; set; }
        public virtual int VisitPayUserId { get; set; }
        public virtual int CommunicationGroupId { get; set; }
        public virtual DateTime? InsertDate { get; protected set; }
        public virtual CommunicationMethodEnum CommunicationMethodId { get; set; }

        public virtual CommunicationGroup CommunicationGroup { get; set; }
    }
}
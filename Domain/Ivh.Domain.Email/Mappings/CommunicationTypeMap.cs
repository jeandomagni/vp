﻿namespace Ivh.Domain.Email.Mappings
{
    using Common.Base.Enums;
    using Common.Data.Constants;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class CommunicationTypeMap : ClassMap<CommunicationType>
    {
        public CommunicationTypeMap()
        {
            this.Schema(Schemas.VisitPay.Dbo);
            this.Table(nameof(CommunicationType));
            this.ReadOnly();
            this.CompositeId()
                .KeyProperty(x => x.CommunicationTypeId).CustomType<CommunicationTypeEnum>()
                .KeyReference(x => x.CommunicationMethod, "CommunicationMethodId");

            this.Map(x => x.CommunicationTypeName);
            this.Map(x => x.CommunicationTypeStatus, "CommunicationTypeStatusId").CustomType<CommunicationTypeStatusEnum>();
            this.References(x => x.CmsRegion, "CmsRegionId");
            this.References(x => x.TemplateCmsRegion, "TemplateCmsRegionId");
            this.Map(x => x.CommunicationGroupId);

            this.Cache.ReadOnly();
        }
    }
}
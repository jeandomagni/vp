namespace Ivh.Domain.Email.Mappings
{
    using Common.Base.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class CommunicationEventMap : ClassMap<CommunicationEvent>
    {
        public CommunicationEventMap()
        {
            this.Schema("dbo");
            this.Table("CommunicationEvent");
            this.Id(x => x.CommunicationEventId);
            this.Map(x => x.UniqueId).Not.Nullable();
            this.Map(x => x.ToAddress).Column("Email");
            this.Map(x => x.EventName);
            this.Map(x => x.Category).Length(NhibernateConstants.SqlNvarcharMax);
            this.Map(x => x.Response).Length(NhibernateConstants.SqlNvarcharMax);
            this.Map(x => x.Attempt).Length(NhibernateConstants.SqlNvarcharMax);
            this.Map(x => x.EventId).Length(NhibernateConstants.SqlNvarcharMax);
            this.Map(x => x.EventStatus).Length(NhibernateConstants.SqlNvarcharMax);
            this.Map(x => x.GroupId).Length(NhibernateConstants.SqlNvarcharMax);
            this.Map(x => x.IP);
            this.Map(x => x.Reason).Length(NhibernateConstants.SqlNvarcharMax);
            this.Map(x => x.EventType).Length(NhibernateConstants.SqlNvarcharMax);
            this.Map(x => x.SmtpId);
            this.Map(x => x.SourceApp);
            this.Map(x => x.Timestamp);
            this.Map(x => x.Url).Length(NhibernateConstants.SqlNvarcharMax);
            this.Map(x => x.UserAgent).Length(NhibernateConstants.SqlNvarcharMax);
            this.Map(x => x.EventDateTime);
            this.Map(x => x.InsertedDate).Not.Nullable();
            this.Map(x => x.RawData).Length(4001).Not.Nullable();
            this.References(x => x.Communication).Column("CommunicationId");
        }
    }
}
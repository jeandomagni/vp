﻿namespace Ivh.Domain.Email.Mappings
{
    using Common.Base.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class CommunicationTrackerMap : ClassMap<CommunicationTracker>
    {
        public CommunicationTrackerMap()
        {
            this.Schema("dbo");
            this.Table("CommunicationTracker");
            this.Id(x => x.CommunicationTrackerId);
            this.Map(x => x.FinancePlanId).Nullable();
            this.Map(x => x.VpStatementId).Nullable();
            this.Map(x => x.PaymentProcessorResponseId).Nullable();
            this.Map(x => x.InsertDate);

            this.References(x => x.CommunicationType).Columns("CommunicationTypeId", "CommunicationMethodId").Not.LazyLoad();
        }
    }
}
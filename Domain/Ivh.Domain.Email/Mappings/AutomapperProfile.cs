﻿namespace Ivh.Domain.Email.Mappings
{
    using AutoMapper;

    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            // Automapper mapping go here.
            // Use this.CreateMap<,>().
            // DO NOT USE Mapper.CreateMap<,>().
        }
    }
}
﻿
namespace Ivh.Domain.Email.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;
    using HospitalData.Outbound.Enums;

    public class CommunicationDocumentMap : ClassMap<CommunicationDocument>
    {
        public CommunicationDocumentMap()
        {
            this.Schema("dbo");
            this.Table("CommunicationDocument");

            this.Id(x => x.CommunicationDocumentId);
            this.Map(x => x.InsertDate);

            this.Map(x => x.CommunicationId).Not.Nullable();
            this.Map(x => x.VpOutboundFileType, "VpOutboundFileTypeId").CustomType<VpOutboundFileTypeEnum>().Not.Nullable();
            this.Map(x => x.FileStorageExternalKey).Not.Nullable();
            this.Map(x => x.FileName).Not.Nullable();
            this.Map(x => x.MimeType).Not.Nullable();
        }
    }
}

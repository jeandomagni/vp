﻿namespace Ivh.Domain.Email.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class CommunicationMap : ClassMap<Communication>
    {
        public CommunicationMap()
        {
            this.Schema("dbo");
            this.Table("Communication");
            this.Id(x => x.CommunicationId);
            this.Map(x => x.UniqueId).Not.Nullable();
            this.Map(x => x.FromName);
            this.Map(x => x.FromAddress).Nullable();
            this.Map(x => x.ToAddress).Not.Nullable();
            this.Map(x => x.Cc);
            this.Map(x => x.Bcc);
            this.Map(x => x.Subject, "EmailSubject").Nullable();
            this.Map(x => x.Body, "EmailBody").Not.Nullable().Length(10000);
            this.Map(x => x.SentToUserId);
            this.Map(x => x.SentToPaymentUnitId);
            this.Map(x => x.CommunicationStatus, "CommunicationStatusId").CustomType<CommunicationStatusEnum>();
            this.Map(x => x.SendFailureCnt).Not.Nullable();
            this.Map(x => x.CreatedDate).Not.Nullable();
            this.Map(x => x.OutboundServerAccepted);
            this.Map(x => x.ProcessedDate);
            this.Map(x => x.DeliveredDate);
            this.Map(x => x.FirstOpenDate);
            this.Map(x => x.FailedDate);
            this.Map(x => x.SerializedMessage);
            this.Map(x => x.Locale);

            this.References(x => x.ResentCommunication).Column("ResentCommunicationId").Not.LazyLoad();
            this.References(x => x.CommunicationType).Columns("CommunicationTypeId", "CommunicationMethodId").Not.LazyLoad();
            this.HasMany(x => x.CommunicationEvents).KeyColumn("CommunicationId").LazyLoad();
        }
    }
}
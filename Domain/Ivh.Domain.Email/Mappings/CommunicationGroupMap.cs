﻿namespace Ivh.Domain.Email.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class CommunicationGroupMap : ClassMap<CommunicationGroup>
    {
        public CommunicationGroupMap()
        {
            this.Schema("dbo");
            this.Table("CommunicationGroup");
            this.Id(x => x.CommunicationGroupId);
            this.Map(x => x.CommunicationGroupName);
            this.Map(x => x.DescriptionCmsRegion)
                .Column("DescriptionCmsRegionId")
                .CustomType<CmsRegionEnum>()
                .Nullable();
            this.Map(x => x.IconType);
            this.Map(x => x.IsActive);

            this.HasMany(x => x.CommunicationTypes)
                .BatchSize(50)
                .KeyColumn("CommunicationGroupId")
                .Not.LazyLoad()
                .Cascade.All();
        }
    }
}
﻿using FluentNHibernate.Mapping;
using Ivh.Domain.Email.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.Email.Mappings
{
    public class CommunicationMethodMap : ClassMap<CommunicationMethod>
    {
        public CommunicationMethodMap()
        {
            this.Schema("dbo");
            this.Table("CommunicationMethod");
            this.Id(x => x.CommunicationMethodId);
            this.Map(x => x.CommunicationMethodName);
        }
    }
}

﻿namespace Ivh.Domain.Email.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class CommunicationSmsBlockedNumberMap : ClassMap<CommunicationSmsBlockedNumber>
    {
        public CommunicationSmsBlockedNumberMap()
        {
            this.Schema("dbo");
            this.Table("CommunicationSmsBlockedNumber");
            this.Id(x => x.CommunicationSmsBlockedNumberId);
            this.Map(x => x.BlockedPhoneNumber);
            this.Map(x => x.SmsPhoneNumber);
        }
    }
}
﻿namespace Ivh.Domain.Email.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class VisitPayUserCommunicationPreferenceMap : ClassMap<VisitPayUserCommunicationPreference>
    {
        public VisitPayUserCommunicationPreferenceMap()
        {
            this.Table("VisitPayUserCommunicationPreference");
            this.Schema("dbo");
            this.Id(x => x.VisitPayUserCommunicationPreferenceId).GeneratedBy.Identity();

            this.Map(x => x.VisitPayUserId);
            this.Map(x => x.CommunicationMethodId).CustomType<CommunicationMethodEnum>();
            this.Map(x => x.CommunicationGroupId);
            this.Map(x => x.InsertDate).ReadOnly();
            this.References(x => x.CommunicationGroup, "CommunicationGroupId").Not.Nullable().ReadOnly();
        }
    }
}

﻿namespace Ivh.Domain.Email.Interfaces
{
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface ICommunicationPreferenceRepository : IRepository<VisitPayUserCommunicationPreference>
    {
        bool Exists(int visitPayUserId, CommunicationMethodEnum communicationMethod, CommunicationTypeEnum communicationTypeId);
    }
}
﻿namespace Ivh.Domain.Email.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Entities;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;

    public interface ICommunicationTrackerRepository : IRepository<CommunicationTracker>
    {
        bool IsCommunicationForPaymentSent(IList<int> paymentProcessorResponseIds, IList<CommunicationTypeEnum> communicationTypes, CommunicationMethodEnum? communicationMethod);
        bool IsCommunicationForFinancePlanSent(int financePlanId, CommunicationTypeEnum communicationType, CommunicationMethodEnum? communicationMethod);
        bool IsCommunicationForVpStatementSent(int vpStatementId, CommunicationTypeEnum communicationType, CommunicationMethodEnum? communicationMethod);
    }
}
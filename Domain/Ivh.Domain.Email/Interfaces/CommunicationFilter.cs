﻿namespace Ivh.Domain.Email.Interfaces
{
    using System;

    public class CommunicationFilter
    {
        public DateTime? CommunicationDateRangeFrom { get; set; }
        public DateTime? CommunicationDateRangeTo { get; set; }
        public DateTime? DateRangeFrom { get; set; }
        public int? CommunicationTypeId { get; set; }
        public int? CommunicationMethodId { get; set; }
        public string SortField { get; set; }
        public string SortOrder { get; set; }
    }
}
﻿namespace Ivh.Domain.Email.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface ICommunicationService : IDomainService
    {
        bool HasCommunicationPreference(int visitPayUserId, CommunicationMethodEnum communicationMethod, CommunicationTypeEnum communicationTypeId);
        CommunicationType GetCommunicationType(CommunicationTypeEnum communicationTypeId, CommunicationMethodEnum communicationMethod);
        IList<CommunicationGroup> CommunicationGroups { get; }
        CommunicationGroup GetCommunicationGroup(int communicationGroupId);
        IList<VisitPayUserCommunicationPreference> CommunicationPreferences(int visitPayUserId);
        void SetPreferences(int visitPayUserId, CommunicationMethodEnum communicationMethod, IEnumerable<CommunicationGroup> communicationGroups);
        void AddPreference(int visitPayUserId, CommunicationMethodEnum communicationMethod, int communicationGroupId);
        void RemovePreference(int visitPayUserId, CommunicationMethodEnum communicationMethod, int communicationGroupId);
        IList<CommunicationSmsBlockedNumber> FindBlockedNumbers(string phoneNumber);
        void AddBlockedNumber(string phoneNumber, string smsNumber);
        void RemoveBlockedNumber(string phoneNumber, string smsNumber);
        int? GetVisitPayUserIdFromLatestTextToPayMessageByPhoneNumber(string visitPayFormattedPhoneNumber);
   
    }
}
﻿namespace Ivh.Domain.Email.Interfaces
{
    using System.Collections.Generic;
    using Application.Email.Common.Dtos;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;

    public interface ICommunicationTrackerService : IDomainService
    {
        bool IsCommunicationForPaymentSent(IList<int> paymentProcessorResponseIds, IList<CommunicationTypeEnum> communicationTypes, CommunicationMethodEnum? communicationMethod);
        bool IsCommunicationForFinancePlanSent(int financePlanId, CommunicationTypeEnum communicationType, CommunicationMethodEnum? communicationMethod);
        bool IsCommunicationForVpStatementSent(int vpStatementId, CommunicationTypeEnum communicationType, CommunicationMethodEnum? communicationMethod);
        void TrackPaymentEmail(IList<int> paymentProcessorResponseIds, CommunicationDto communicationDto);
        void TrackVpStatementEmail(int vpStatementId, CommunicationDto communicationDto);
        void TrackFinancePlanEmail(int financePlanId, CommunicationDto communicationDto);
    }
}
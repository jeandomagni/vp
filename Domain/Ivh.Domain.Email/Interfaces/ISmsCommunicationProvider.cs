﻿namespace Ivh.Domain.Email.Interfaces
{
    using System.Threading.Tasks;
    using Entities;

    public interface ISmsCommunicationProvider
    {
        Task<bool> SendCommunication(Communication communication);
    }
}
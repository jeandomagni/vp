﻿
namespace Ivh.Domain.Email.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface ICommunicationDocumentRepository : IRepository<CommunicationDocument>
    {
    }
}

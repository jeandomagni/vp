﻿using System.Net.Mail;

namespace Ivh.Domain.Email.Interfaces
{
    using System;
    using System.Threading.Tasks;

    public interface IEmailServiceProvider
    {
        /// <summary>
        /// Sends this instance.
        /// </summary>
        /// <returns></returns>
        Task<bool> SendAsync(MailMessage mailMessage, Guid uniqueId);
    }
}

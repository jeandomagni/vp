﻿namespace Ivh.Domain.Email.Interfaces
{
    using System.Collections.Generic;
    using Entities;

    public class CommunicationEventResults
    {
        public IList<CommunicationEvent> Events { get; set; }
        public int TotalRecords { get; set; }
    }
}
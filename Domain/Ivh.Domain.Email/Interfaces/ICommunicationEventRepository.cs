﻿using Ivh.Common.Base.Interfaces;

namespace Ivh.Domain.Email.Interfaces
{
    public interface ICommunicationEventRepository : IRepository<Entities.CommunicationEvent>
    {
        CommunicationEventResults ForCommunicationId(int communicationId, int batch, int batchSize, string sortField, string sortOrder);
    }
}

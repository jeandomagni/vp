﻿namespace Ivh.Domain.Email.Interfaces
{
    using System.Collections.Generic;
    using Entities;

    public class EmailResults
    {
        public IList<Communication> Emails { get; set; }
        public int TotalRecords { get; set; }
    }
}
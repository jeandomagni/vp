﻿namespace Ivh.Domain.Email.Interfaces
{
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface ICommunicationTypeRepository : IRepository<CommunicationType>
    {
        CommunicationType GetByCommunicationTypeId(CommunicationTypeEnum communicationTypeEnum, CommunicationMethodEnum communicationMethodEnum);
    }
}
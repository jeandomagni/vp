﻿
namespace Ivh.Domain.Email.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface ICommunicationDocumentService : IDomainService
    {
        void InsertOrUpdate(CommunicationDocument communicationDocument);
    }
}

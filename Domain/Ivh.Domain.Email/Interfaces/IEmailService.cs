﻿namespace Ivh.Domain.Email.Interfaces
{
    using System;
    using Common.Base.Enums;
    using Application.Email.Common.Dtos;
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IEmailService : IDomainService
    {
        void Update(CommunicationDto communicationDto);
        void Resend(int emailId, string toAddress = null, int? vpGuarantorId = null, int? sentByVisitPayUserId = null);
        void Event(CommunicationEventDto communicationEventDto);
        CommunicationDto GetById(int emailId);
        EmailResults GetEmailsForUserId(int visitPayUserId, CommunicationFilter filter, int batch, int batchSize);
        int GetEmailsForUserIdTotals(int visitPayUserId, CommunicationFilter filter);
        CommunicationEventResults GetEventsForEmailId(int emailId, int batch, int batchSize, string sortField, string sortOrder);
        int GetUserIdForEmailId(int emailId);
        IList<int> GetHeldEmailIds(IList<CommunicationTypeEnum> emailTypes = null);
        IList<Communication> GetLastSentEmails(int limit);
        IList<Communication> GetUnsentCommunications(CommunicationMethodEnum communicationMethod);
        int? GetCmsVersionIdForEmailType(CommunicationTypeEnum communicationTypeEnum, CommunicationMethodEnum communicationMethodEnum);
        CommunicationDto FindCommunicationWithUniqueId(Guid uniqueId);
    }
}

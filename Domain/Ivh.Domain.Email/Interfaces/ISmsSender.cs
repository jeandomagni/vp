﻿namespace Ivh.Domain.Email.Interfaces
{
    using System.Threading.Tasks;
    using Application.Email.Common.Dtos;

    public interface ISmsSender
    {
        Task<bool> Send(CommunicationDto communicationDto);

    }
}
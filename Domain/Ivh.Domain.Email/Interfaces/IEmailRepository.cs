﻿namespace Ivh.Domain.Email.Interfaces
{
    using System;
    using Common.Base.Enums;
    using System.Collections.Generic;
    using Common.VisitPay.Enums;
    using Ivh.Common.Base.Interfaces;

    public interface ICommunicationRepository : IRepository<Entities.Communication>
    {
        Entities.Communication GetCommunicationByUniqueId(Guid uniqueId);
        int GetCommunicationIdByUniqueId(Guid uniqueId);
        EmailResults ForUserId(int visitPayUserId, CommunicationFilter filter, int batch, int batchSize);
        int ForUserIdTotals(int visitPayUserId, CommunicationFilter filter);
        int GetUserIdForCommunicationId(int communicationId);
        IList<Entities.Communication> GetLastSentEmails(int limit);
        IList<Entities.Communication> GetUnsentCommunications(CommunicationMethodEnum communicationMethod);
        IList<int> GetHeldCommunicationIds(IList<CommunicationTypeEnum> emailTypes = null);
        Entities.Communication GetLatestTextToPayMessageByPhone(string visitPayFormattedPhoneNumber);
    }
}
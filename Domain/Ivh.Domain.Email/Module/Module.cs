﻿using Autofac;
using Ivh.Domain.Email.Interfaces;
using Ivh.Domain.Email.Services;

namespace Ivh.Domain.Email.Module
{
    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<EmailService>().As<IEmailService>();
            builder.RegisterType<CommunicationService>().As<ICommunicationService>();
            builder.RegisterType<CommunicationTrackerService>().As<ICommunicationTrackerService>();
            builder.RegisterType<CommunicationDocumentService>().As<ICommunicationDocumentService>();
        }
    }
}

﻿
namespace Ivh.Domain.Email.Services
{
    using Entities;
    using Interfaces;
    using System;

    public class CommunicationDocumentService : ICommunicationDocumentService
    {
        private readonly Lazy<ICommunicationDocumentRepository> _communicationDocumentRepository;

        public CommunicationDocumentService(Lazy<ICommunicationDocumentRepository> communicationDocumentRepository)
        {
            this._communicationDocumentRepository = communicationDocumentRepository;
        }

        public void InsertOrUpdate(CommunicationDocument communicationDocument)
        {
            this._communicationDocumentRepository.Value.InsertOrUpdate(communicationDocument);
        }
    }
}

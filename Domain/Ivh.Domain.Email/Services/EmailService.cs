﻿namespace Ivh.Domain.Email.Services
{
    using System;
    using System.Collections.Generic;
    using Application.Email.Common.Constants;
    using Application.Email.Common.Dtos;
    using AutoMapper;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.Core;
    using Content.Entities;
    using Content.Interfaces;
    using Entities;
    using Interfaces;

    public class EmailService : DomainService, IEmailService
    {
        private readonly Lazy<ICommunicationRepository> _communicationRepository;
        private readonly Lazy<ICommunicationTypeRepository> _communicationTypeRepository;
        private readonly Lazy<ICommunicationEventRepository> _communicationEventRepository;
        private readonly Lazy<IContentService> _contentService;

        public EmailService(
            Lazy<IDomainServiceCommonService> domainServiceCommonService,
            Lazy<ICommunicationRepository> communicationRepository,
            Lazy<ICommunicationTypeRepository> communicationTypeRepository,
            Lazy<ICommunicationEventRepository> eventRepository,
            Lazy<IContentService> contentService) : base(domainServiceCommonService)
        {
            this._communicationRepository = communicationRepository;
            this._communicationTypeRepository = communicationTypeRepository;
            this._communicationEventRepository = eventRepository;
            this._contentService = contentService;
        }

        public void Resend(int emailId, string toAddress = null, int? vpGuarantorId = null, int? sentByVisitPayUserId = null)
        {
            this.Bus.Value.PublishMessage(new ResendEmailMessage
            {
                EmailId = emailId,
                ToAddress = toAddress,
                VpGuarantorId = vpGuarantorId,
                SentByVisitPayUserId = sentByVisitPayUserId
            }).Wait();
        }

        public void Update(CommunicationDto communicationDto)
        {
            Communication communication = this._communicationRepository.Value.GetById(communicationDto.CommunicationId) ?? Mapper.Map<Communication>(communicationDto);
            Mapper.Map(communicationDto, communication);
            this._communicationRepository.Value.InsertOrUpdate(communication);
            communicationDto.CommunicationId = communication.CommunicationId; // update dto so if it is used during the same session the id is set
        }

        public void Event(CommunicationEventDto communicationEventDto)
        {
            CommunicationEvent communicationEvent = Mapper.Map<CommunicationEvent>(communicationEventDto);
            if (communicationEvent.UniqueId != default(Guid))
            {
                Communication communication = this._communicationRepository.Value.GetCommunicationByUniqueId(communicationEvent.UniqueId);
                if (communication != null)
                {
                    communication.CommunicationEvents.Add(communicationEvent);
                    switch (communicationEvent.EventName)
                    {
                        case CommunicationEventType.Open:
                            communication.FirstOpenDate = communicationEvent.EventDateTime;
                            break;
                        case CommunicationEventType.Delivered:
                        case CommunicationEventType.Sent:
                            communication.DeliveredDate = communicationEvent.EventDateTime;
                            break;
                        case CommunicationEventType.Processed:
                        case CommunicationEventType.Queued:
                            communication.ProcessedDate = communicationEvent.EventDateTime;
                            break;
                        case CommunicationEventType.Bounce:
                        case CommunicationEventType.Dropped:
                        case CommunicationEventType.Undelivered:
                        case CommunicationEventType.Failed:
                            communication.FailedDate = communicationEvent.EventDateTime;
                            break;
                    }
                }
                else
                {
                    this.LoggingService.Value.Warn(() =>  $"EmailService::Event - Failed to find communication by UniqueId {communicationEvent.UniqueId.ToString()}\nMessage: {communicationEventDto.ToJSON()}");
                }
            }
            else
            {
                this.LoggingService.Value.Warn(() =>  $"EmailService::Event - Communication UniqueId is empty\nMessage: {communicationEventDto.ToJSON()}");
            }

            this._communicationEventRepository.Value.InsertOrUpdate(communicationEvent);
        }

        public CommunicationDto GetById(int emailId)
        {
            return Mapper.Map<CommunicationDto>(this._communicationRepository.Value.GetById(emailId));
        }

        public EmailResults GetEmailsForUserId(int visitPayUserId, CommunicationFilter filter, int batch, int batchSize)
        {
            return this._communicationRepository.Value.ForUserId(visitPayUserId, filter, batch, batchSize);
        }

        public int GetEmailsForUserIdTotals(int visitPayUserId, CommunicationFilter filter)
        {
            return this._communicationRepository.Value.ForUserIdTotals(visitPayUserId, filter);
        }

        public int GetUserIdForEmailId(int emailId)
        {
            return this._communicationRepository.Value.GetUserIdForCommunicationId(emailId);
        }

        public CommunicationEventResults GetEventsForEmailId(int emailId, int batch, int batchSize, string sortField, string sortOrder)
        {
            return this._communicationEventRepository.Value.ForCommunicationId(emailId, batch, batchSize, sortField, sortOrder);
        }

        public IList<int> GetHeldEmailIds(IList<CommunicationTypeEnum> emailTypes = null)
        {
            return this._communicationRepository.Value.GetHeldCommunicationIds(emailTypes);
        }

        public IList<Communication> GetLastSentEmails(int limit)
        {
            return this._communicationRepository.Value.GetLastSentEmails(limit);
        }

        public IList<Communication> GetUnsentCommunications(CommunicationMethodEnum communicationMethod)
        {
            return this._communicationRepository.Value.GetUnsentCommunications(communicationMethod);
        }

        public int? GetCmsVersionIdForEmailType(CommunicationTypeEnum communicationTypeEnum, CommunicationMethodEnum communicationMethodEnum)
        {
            CommunicationType communicationType = this._communicationTypeRepository.Value.GetByCommunicationTypeId(communicationTypeEnum, communicationMethodEnum);
            if (communicationType == null)
            {
                return null;
            }

            CmsVersion cmsVersion = this._contentService.Value.GetCurrentVersionAsync((CmsRegionEnum)communicationType.CmsRegion.CmsRegionId, false).Result;
            return cmsVersion?.CmsVersionId;
        }

        public CommunicationDto FindCommunicationWithUniqueId(Guid uniqueId)
        {
            Communication communication = this._communicationRepository.Value.GetCommunicationByUniqueId(uniqueId);
            return Mapper.Map<CommunicationDto>(communication);
        }
    }
}
﻿namespace Ivh.Domain.Email.Services
{
    using System;
    using System.Collections.Generic;
    using Application.Email.Common.Dtos;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;

    public class CommunicationTrackerService : DomainService, ICommunicationTrackerService
    {
        private readonly Lazy<ICommunicationTrackerRepository> _communicationTrackerRepository;
        private readonly Lazy<ICommunicationTypeRepository> _communicationTypeRepository;

        public CommunicationTrackerService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<ICommunicationTrackerRepository> communicationTrackerRepository,
            Lazy<ICommunicationTypeRepository> communicationTypeRepository) : base(serviceCommonService)
        {
            this._communicationTrackerRepository = communicationTrackerRepository;
            this._communicationTypeRepository = communicationTypeRepository;
        }

        public bool IsCommunicationForPaymentSent(IList<int> paymentProcessorResponseIds, IList<CommunicationTypeEnum> communicationTypes, CommunicationMethodEnum? communicationMethod)
        {
            return this._communicationTrackerRepository.Value.IsCommunicationForPaymentSent(paymentProcessorResponseIds, communicationTypes, communicationMethod);
        }

        public bool IsCommunicationForFinancePlanSent(int financePlanId, CommunicationTypeEnum communicationType, CommunicationMethodEnum? communicationMethod)
        {
            return this._communicationTrackerRepository.Value.IsCommunicationForFinancePlanSent(financePlanId, communicationType, communicationMethod);
        }

        public bool IsCommunicationForVpStatementSent(int vpStatementId, CommunicationTypeEnum communicationType, CommunicationMethodEnum? communicationMethod)
        {
            return this._communicationTrackerRepository.Value.IsCommunicationForVpStatementSent(vpStatementId, communicationType, communicationMethod);
        }

        public void TrackPaymentEmail(IList<int> paymentProcessorResponseIds, CommunicationDto communicationDto)
        {
            if (communicationDto == null || paymentProcessorResponseIds.IsNullOrEmpty())
            {
                return;
            }
            CommunicationType type = this._communicationTypeRepository.Value.GetByCommunicationTypeId(communicationDto.CommunicationType.CommunicationTypeId, communicationDto.CommunicationType.CommunicationMethodId);
            if (type == null)
            {
                return;
            }

            foreach (int paymentProcessorResponseId in paymentProcessorResponseIds)
            {
                this._communicationTrackerRepository.Value.Insert(new CommunicationTracker() { CommunicationType = type, PaymentProcessorResponseId = paymentProcessorResponseId });
            }
        }

        public void TrackVpStatementEmail(int vpStatementId, CommunicationDto communicationDto)
        {
            if (communicationDto == null)
            {
                return;
            }
            var type = this._communicationTypeRepository.Value.GetByCommunicationTypeId(communicationDto.CommunicationType.CommunicationTypeId, communicationDto.CommunicationType.CommunicationMethodId);
            if (type == null)
            {
                return;
            }
            this._communicationTrackerRepository.Value.Insert(new CommunicationTracker() { CommunicationType = type, VpStatementId = vpStatementId });
        }

        public void TrackFinancePlanEmail(int financePlanId, CommunicationDto communicationDto)
        {
            if (communicationDto == null)
            {
                return;
            }
            CommunicationType type = this._communicationTypeRepository.Value.GetByCommunicationTypeId(communicationDto.CommunicationType.CommunicationTypeId, communicationDto.CommunicationType.CommunicationMethodId);
            if (type == null)
            {
                return;
            }
            this._communicationTrackerRepository.Value.Insert(new CommunicationTracker() { CommunicationType = type, FinancePlanId = financePlanId });
        }
    }
}

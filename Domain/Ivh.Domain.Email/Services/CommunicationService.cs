﻿namespace Ivh.Domain.Email.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;

    public class CommunicationService : DomainService, ICommunicationService
    {
        private readonly Lazy<ICommunicationPreferenceRepository> _communicationPreferenceRepository;
        private readonly Lazy<ICommunicationGroupRepository> _communicationGroupRepository;
        private readonly Lazy<ICommunicationTypeRepository> _communicationTypeRepository;
        private readonly Lazy<ICommunicationSmsBlockedNumberRepository> _communicationSmsBlockedNumberRepository;
        private readonly Lazy<ICommunicationRepository> _communicationRepository;

        public CommunicationService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<ICommunicationPreferenceRepository> communicationPreferenceRepository,
            Lazy<ICommunicationGroupRepository> communicationGroupRepository,
            Lazy<ICommunicationTypeRepository> communicationTypeRepository,
            Lazy<ICommunicationSmsBlockedNumberRepository> communicationSmsBlockedNumberRepository,
            Lazy<ICommunicationRepository> communicationRepository) : base(serviceCommonService)
        {
            this._communicationPreferenceRepository = communicationPreferenceRepository;
            this._communicationGroupRepository = communicationGroupRepository;
            this._communicationTypeRepository = communicationTypeRepository;
            this._communicationSmsBlockedNumberRepository = communicationSmsBlockedNumberRepository;
            this._communicationRepository = communicationRepository;
        }

        public bool HasCommunicationPreference(int visitPayUserId, CommunicationMethodEnum communicationMethod, CommunicationTypeEnum communicationTypeId)
        {
            bool hasCommunicationPreference = this._communicationPreferenceRepository.Value.Exists(visitPayUserId, communicationMethod, communicationTypeId);
            return hasCommunicationPreference;
        }

        public CommunicationType GetCommunicationType(CommunicationTypeEnum communicationTypeId, CommunicationMethodEnum communicationMethod)
        {
            return this._communicationTypeRepository.Value.GetByCommunicationTypeId(communicationTypeId, communicationMethod);
        }

        /// <summary>
        /// Return all active communication groups that are greater than 0 
        /// NOTE: 0 is the communication group for messages that cannot be turned off like activate and deactivate messages
        /// </summary>
        public IList<CommunicationGroup> CommunicationGroups
        {
            get { return this._communicationGroupRepository.Value.GetQueryable().Where(cg => cg.IsActive && cg.CommunicationGroupId > 0).ToList(); }
        }

        public CommunicationGroup GetCommunicationGroup(int communicationGroupId)
        {
            return this._communicationGroupRepository.Value.GetById(communicationGroupId);
        }

        public IList<VisitPayUserCommunicationPreference> CommunicationPreferences(int visitPayUserId)
        {
            return this._communicationPreferenceRepository.Value.GetQueryable().Where(p => p.VisitPayUserId == visitPayUserId).ToList();
        }

        public void SetPreferences(int visitPayUserId, CommunicationMethodEnum communicationMethod, IEnumerable<CommunicationGroup> communicationGroups)
        {
            foreach (CommunicationGroup communicationGroup in communicationGroups)
            {
                this._communicationPreferenceRepository.Value.Insert(new VisitPayUserCommunicationPreference
                {
                    CommunicationGroupId = communicationGroup.CommunicationGroupId,
                    CommunicationGroup = communicationGroup,
                    CommunicationMethodId = communicationMethod,
                    VisitPayUserId = visitPayUserId
                });
            }
        }

        public void AddPreference(int visitPayUserId, CommunicationMethodEnum communicationMethod, int communicationGroupId)
        {
            CommunicationGroup communicationGroup = this._communicationGroupRepository.Value.GetById(communicationGroupId);
            VisitPayUserCommunicationPreference communicationPreference = this._communicationPreferenceRepository.Value.GetQueryable()
                .Where(cp => cp.VisitPayUserId == visitPayUserId)
                .Where(cp => cp.CommunicationGroupId == communicationGroupId)
                .FirstOrDefault(cp => cp.CommunicationMethodId == communicationMethod);

            if (communicationPreference == null)
            {
                this._communicationPreferenceRepository.Value.Insert(new VisitPayUserCommunicationPreference
                {
                    CommunicationGroupId = communicationGroup.CommunicationGroupId,
                    CommunicationGroup = communicationGroup,
                    CommunicationMethodId = communicationMethod,
                    VisitPayUserId = visitPayUserId
                });
            }

        }

        public void RemovePreference(int visitPayUserId, CommunicationMethodEnum communicationMethod, int communicationGroupId)
        {
            VisitPayUserCommunicationPreference communicationPreference = this._communicationPreferenceRepository.Value.GetQueryable().FirstOrDefault(cp => cp.VisitPayUserId == visitPayUserId && cp.CommunicationGroupId == communicationGroupId && cp.CommunicationMethodId == communicationMethod);
            if (communicationPreference != null)
            {
                this._communicationPreferenceRepository.Value.Delete(communicationPreference);
            }
        }

        public IList<CommunicationSmsBlockedNumber> FindBlockedNumbers(string phoneNumber)
        {
            phoneNumber = phoneNumber.RemoveNonNumericCharacters();
            if (phoneNumber.Length != 11)
            {
                phoneNumber = $"1{phoneNumber}";
            }
            return this._communicationSmsBlockedNumberRepository.Value.GetQueryable().Where(bn => bn.BlockedPhoneNumber == phoneNumber).ToList();
        }

        public void AddBlockedNumber(string phoneNumber, string smsNumber)
        {
            phoneNumber = phoneNumber.RemoveNonNumericCharacters();
            smsNumber = smsNumber.RemoveNonNumericCharacters();
            if (!this._communicationSmsBlockedNumberRepository.Value.GetQueryable().Any(bn => bn.BlockedPhoneNumber == phoneNumber && bn.SmsPhoneNumber == smsNumber))
            {
                this._communicationSmsBlockedNumberRepository.Value.Insert(new CommunicationSmsBlockedNumber { BlockedPhoneNumber = phoneNumber, SmsPhoneNumber = smsNumber });
            }
        }

        public void RemoveBlockedNumber(string phoneNumber, string smsNumber)
        {
            phoneNumber = phoneNumber.RemoveNonNumericCharacters();
            smsNumber = smsNumber.RemoveNonNumericCharacters();
            CommunicationSmsBlockedNumber communicationSmsBlockedNumber = this._communicationSmsBlockedNumberRepository.Value.GetQueryable().FirstOrDefault(bn => bn.BlockedPhoneNumber == phoneNumber && bn.SmsPhoneNumber == smsNumber);
            if (communicationSmsBlockedNumber != null)
            {
                this._communicationSmsBlockedNumberRepository.Value.Delete(communicationSmsBlockedNumber);
            }
        }

        public int? GetVisitPayUserIdFromLatestTextToPayMessageByPhoneNumber(string visitPayFormattedPhoneNumber)
        {
            Communication communication = this._communicationRepository.Value.GetLatestTextToPayMessageByPhone(visitPayFormattedPhoneNumber);
            return communication.SentToUserId;
        }
    }
}
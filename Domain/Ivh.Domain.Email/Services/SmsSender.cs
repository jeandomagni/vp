﻿namespace Ivh.Domain.Email.Services
{
    using System;
    using System.Threading.Tasks;
    using Application.Email.Common.Dtos;
    using AutoMapper;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;
    using Settings.Interfaces;

    public class SmsSender :   ISmsSender
    {
        private readonly Lazy<ISmsCommunicationProvider> _smsProvider;
        private readonly Lazy<ICommunicationRepository> _communicationRepo;
        private readonly Lazy<IFeatureService> _featureService;

        public SmsSender(
            Lazy<ISmsCommunicationProvider> smsProvider,
            Lazy<ICommunicationRepository> communicationRepo,
            Lazy<IFeatureService> featureService
            )

        {
            this._smsProvider = smsProvider;
            this._communicationRepo = communicationRepo;
            this._featureService = featureService;
        }
        
        /// <summary>
        /// This will save and send the communication.
        /// The communication object should have the message and someone to send to before before calling this.
        /// </summary>
        /// <param name="communicationDto"></param>
        public async Task<bool> Send(CommunicationDto communicationDto)
        {
            //Dont let anything send it if's disabled.
            if (!this._featureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.Sms))
            {
                return false;
            }

            //Cannot send a communication with a type that's not SMS
            if (communicationDto?.CommunicationType == null || communicationDto.CommunicationType.CommunicationMethodId != CommunicationMethodEnum.Sms)
            {
                return false;
            }
            if (communicationDto.Body == null || communicationDto.ToAddress.IsNullOrEmpty())
            {
                return false;
            }

            Communication communication = this._communicationRepo.Value.GetById(communicationDto.CommunicationId) ?? Mapper.Map<Communication>(communicationDto);
            Mapper.Map(communicationDto, communication);

            bool result = await this._smsProvider.Value.SendCommunication(communication);
            //The provider will assign it a unique id that the service assigns and serialize the response, we need to make sure to capture this.
            this._communicationRepo.Value.InsertOrUpdate(communication);
            communicationDto.CommunicationId = communication.CommunicationId;
            communicationDto.UniqueId = communication.UniqueId;

            return result;
        }

    }
}
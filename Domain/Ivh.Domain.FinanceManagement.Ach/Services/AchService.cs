﻿namespace Ivh.Domain.FinanceManagement.Ach.Services
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using CsvHelper;
    using Entities;
    using Interfaces;
    using NHibernate;
    using MappingsCsv;

    public class AchService : DomainService, IAchService
    {
        private readonly Lazy<IAchFileRepository> _achFileRepository;
        private readonly Lazy<IAchProvider> _achProvider;
        private readonly Lazy<IAchReturnDetailRepository> _achReturnDetailRepository;
        private readonly Lazy<IAchSettlementDetailRepository> _achSettlementDetailRepository;
        private readonly ISession _session;

        public AchService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            ISessionContext<VisitPay> sessionContext,
            Lazy<IAchFileRepository> achFileRepository,
            Lazy<IAchReturnDetailRepository> achReturnDetailRepository,
            Lazy<IAchSettlementDetailRepository> achSettlementDetailRepository,
            Lazy<IAchProvider> achProvider) : base(serviceCommonService)
        {
            this._session = sessionContext.Session;
            this._achFileRepository = achFileRepository;
            this._achReturnDetailRepository = achReturnDetailRepository;
            this._achSettlementDetailRepository = achSettlementDetailRepository;
            this._achProvider = achProvider;
        }

        private const string Paid = "Paid";
        private const string Web = "WEB";

        public bool DownloadReturns(DateTime startDate, DateTime endDate)
        {
            IList<bool> allSucceeded = new List<bool>();
            foreach (Response response in this._achProvider.Value.DownloadReturnsFiles(startDate, endDate))
            {
                DateTime downloaDateTime = DateTime.UtcNow;

                AchFile achFile = new AchFile
                {
                    AchFileType = AchFileTypeEnum.Return,
                    DownloadedDate = downloaDateTime,
                    FileDateFrom = startDate,
                    FileDateTo = endDate,
                    FileReferenceToken = StoreFile(response.Value),
                    ReturnDetails = new List<AchReturnDetail>()
                };

                this._achFileRepository.Value.InsertOrUpdate(achFile);

                if (response.Code != "000")
                {
                    throw new InvalidOperationException(response.Message);
                }

                if (!string.IsNullOrEmpty(response.Value)
                    && !response.Value.Equals("There were no returns found for this company.", StringComparison.OrdinalIgnoreCase))
                {
                    using (TextReader textReader = new StringReader(response.Value))
                    {
                        CsvReader csvReader = new CsvReader(textReader);
                        csvReader.Configuration.RegisterClassMap<AchReturnDetailMap>();
                        csvReader.Configuration.HasHeaderRecord = false;
                        IList<AchReturnDetail> achReturnDetails = csvReader.GetRecords<AchReturnDetail>().ToList();
                        foreach (AchReturnDetail achReturnDetail in achReturnDetails)
                        {
                            achReturnDetail.AchFile = achFile;
                            achReturnDetail.DownloadedDate = downloaDateTime;
                            achReturnDetail.IndividualId = achReturnDetail.IndividualId.TrimNullSafe();
                            achReturnDetail.ProcessedDate = null;
                            achFile.ReturnDetails.Add(achReturnDetail);
                        }
                    }

                    this.AddAchFile(achFile);
                    allSucceeded.Add(true);
                }
                else
                {
                    allSucceeded.Add(false);
                }
            }
            return allSucceeded.All(x => x);
        }

        public bool DownloadSettlements(DateTime startDate, DateTime endDate)
        {
            IList<bool> allSucceeded = new List<bool>();
            foreach (Response response in this._achProvider.Value.DownloadSettlementsFiles(startDate, endDate))
            {
                DateTime downloaDateTime = DateTime.UtcNow;

                AchFile achFile = new AchFile
                {
                    AchFileType = AchFileTypeEnum.Settlement,
                    DownloadedDate = downloaDateTime,
                    FileDateFrom = startDate,
                    FileDateTo = endDate,
                    FileReferenceToken = StoreFile(response.Value),
                    SettlementDetails = new List<AchSettlementDetail>()
                };

                this._achFileRepository.Value.InsertOrUpdate(achFile);

                if (response.Code != "000")
                {
                    throw new InvalidOperationException(response.Message);
                }

                if (!string.IsNullOrEmpty(response.Value))
                {
                    using (TextReader textReader = new StringReader(response.Value))
                    {
                        CsvReader csvReader = new CsvReader(textReader);
                        csvReader.Configuration.RegisterClassMap<AchSettlementDetailMap>();
                        csvReader.Configuration.HasHeaderRecord = true;
                        csvReader.Configuration.WillThrowOnMissingField = false;
                        IList<AchSettlementDetail> achSettlementDetails = csvReader.GetRecords<AchSettlementDetail>().ToList();
                        foreach (AchSettlementDetail achSettlementDetail in achSettlementDetails.Where(x => x.IsValid()))
                        {
                            achSettlementDetail.AchFile = achFile;
                            achSettlementDetail.DownloadedDate = downloaDateTime;
                            achSettlementDetail.IndividualId = achSettlementDetail.IndividualId.TrimNullSafe();
                            achSettlementDetail.ProcessedDate = null;
                            achFile.SettlementDetails.Add(achSettlementDetail);
                        }
                    }

                    this.AddAchFile(achFile);
                    allSucceeded.Add(true);
                }
                else
                {
                    allSucceeded.Add(false);
                }
            }
            return allSucceeded.All(x => x);
        }

        public int DoOptimisticSettlement(string guarantorName, string individualId, decimal amount)
        {
            AchSettlementDetail achSettlementDetail = new AchSettlementDetail()
            {
                AchFile = null,
                CreditAmount = "0.00",
                CreditAmount1 = "0.00",
                DebitAmount = amount.ToString("0.00"),
                DebitAmount1 = amount.ToString("0.00"),
                DishonorReason = string.Empty,
                DownloadedDate = DateTime.UtcNow,
                FileName = string.Empty,
                IndividualId = individualId,
                PaidGroup = string.Empty,
                ProcessedDate = null,
                ProcessingDate = DateTime.UtcNow.ToShortDateString(),
                ReturnAddenda = string.Empty,
                ReturnCode = string.Empty,
                ReturnDate = string.Empty,
                SecCode = Web,
                Status = Paid,
                Textbox21 = guarantorName,
                Textbox4 = null,
                Textbox5 = null,
                Textbox6 = null,
                Textbox7 = null,              
            };

            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                this._achSettlementDetailRepository.Value.Insert(achSettlementDetail);
                unitOfWork.Commit();
            }
         
            return achSettlementDetail.AchSettlementDetailId;
        }

        private static Guid StoreFile(string file)
        {
            //TODO: StoreFile these are just stubs for the blob store;
            return Guid.NewGuid();
        }

        private static string GetFile(Guid fileReferenceToken)
        {
            //TODO: GetFile these are just stubs for the blob store;
            return string.Empty;
        }

        public IReadOnlyList<AchReturnDetail> GetReturnDetails(DateTime startDate, DateTime endDate, bool unprocessedOnly = false)
        {
            return this._achReturnDetailRepository.Value.GetReturnDetails(startDate, endDate, unprocessedOnly);
        }

        public AchReturnDetail GetReturnDetail(int id)
        {
            return this._achReturnDetailRepository.Value.GetById(id);
        }

        public IReadOnlyList<AchSettlementDetail> GetSettlementDetails(DateTime startDate, DateTime endDate, bool unprocessedOnly = false)
        {
            return this._achSettlementDetailRepository.Value.GetSettlementDetails(startDate, endDate, unprocessedOnly);
        }

        public AchSettlementDetail GetSettlementDetail(int id)
        {
            return this._achSettlementDetailRepository.Value.GetById(id);
        }

        public IList<AchSettlementDetail> GetAchSettlementDetailsByTransactionId(string transactionId)
        {
            return this._achSettlementDetailRepository.Value.GetAchSettlementDetailsByTransactionId(transactionId);
        }

        public void MarkReturnDetailAsProcessed(int achReturnDetailId, DateTime processedDate)
        {
            AchReturnDetail achReturnDetail = this._achReturnDetailRepository.Value.GetById(achReturnDetailId);
            achReturnDetail.ProcessedDate = processedDate;
            this._achReturnDetailRepository.Value.InsertOrUpdate(achReturnDetail);

            if (achReturnDetail.AchFile != null && this._achReturnDetailRepository.Value.GetReturnDetailsCount(achReturnDetail.AchFile, true) == 0)
            {
                achReturnDetail.AchFile.ProcessedDate = processedDate;
                this._achFileRepository.Value.InsertOrUpdate(achReturnDetail.AchFile);
            }
        }

        public void MarkSettlementDetailAsProcessed(int achSettlementDetailId, DateTime processedDate)
        {
            AchSettlementDetail achSettlementDetail = this._achSettlementDetailRepository.Value.GetById(achSettlementDetailId);
            achSettlementDetail.ProcessedDate = processedDate;
            this._achSettlementDetailRepository.Value.InsertOrUpdate(achSettlementDetail);

            if (achSettlementDetail.AchFile != null && this._achSettlementDetailRepository.Value.GetSettlementDetailsCount(achSettlementDetail.AchFile, true) == 0)
            {
                achSettlementDetail.AchFile.ProcessedDate = processedDate;
                this._achFileRepository.Value.InsertOrUpdate(achSettlementDetail.AchFile);
            }
        }

        public void AddAchFile(AchFile achFile)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                // TODO: Probably need to better handle retry logic. i.e., if we download the file twice in one night, what do we do? 
                // 1) what if first batch has ALREADY been processed.
                // 2) what if the first batch has NOT been processed.
                // this might be the right place to handle that logic.

                this._achFileRepository.Value.InsertOrUpdate(achFile);

                if (achFile.ReturnDetails.IsNotNullOrEmpty())
                {
                    foreach (AchReturnDetail returnDetail in achFile.ReturnDetails)
                    {
                        returnDetail.AchFile = achFile;
                        this._achReturnDetailRepository.Value.InsertOrUpdate(returnDetail);
                    }
                }

                if (achFile.SettlementDetails.IsNotNullOrEmpty())
                {
                    foreach (AchSettlementDetail settlementDetail in achFile.SettlementDetails)
                    {
                        settlementDetail.AchFile = achFile;
                        this._achSettlementDetailRepository.Value.InsertOrUpdate(settlementDetail);
                    }
                }

                unitOfWork.Commit();
            }
        }

        private static readonly IDictionary<string, AchReturnCodeEnum> ForwardMap = new Dictionary<string, AchReturnCodeEnum>(StringComparer.OrdinalIgnoreCase)
        {
            {string.Empty, AchReturnCodeEnum.None},
            {"R", AchReturnCodeEnum.R00_Unknown},
            {"R01", AchReturnCodeEnum.R01_InsufficientFunds},
            {"R02", AchReturnCodeEnum.R02_AccountClosed},
            {"R03", AchReturnCodeEnum.R03_NoAccount},
            {"R04", AchReturnCodeEnum.R04_InvalidAccountNumber},
            {"R05", AchReturnCodeEnum.R05_UnauthorizedDebitEntry},
            {"R06", AchReturnCodeEnum.R06_ReturnedPerODFIsRequest},
            {"R07", AchReturnCodeEnum.R07_AuthorizationRevoked},
            {"R08", AchReturnCodeEnum.R08_PaymentStopped},
            {"R09", AchReturnCodeEnum.R09_UncollectedFunds},
            {"R10", AchReturnCodeEnum.R10_CustomerAdvisesNotAuthorized},
            {"R11", AchReturnCodeEnum.R11_CheckSafekeepingEntryReturn},
            {"R12", AchReturnCodeEnum.R12_BranchSoldToAnotherDFI},
            {"R13", AchReturnCodeEnum.R13_RDFINotQualifiedToParticipate},
            {"R14", AchReturnCodeEnum.R14_AccountHolderDeceased},
            {"R16", AchReturnCodeEnum.R16_AccountFrozen},
            {"R17", AchReturnCodeEnum.R17_FileRecordEditCriteria},
            {"R18", AchReturnCodeEnum.R18_ImproperEffectiveEntryDate},
            {"R19", AchReturnCodeEnum.R19_AmountFieldError},
            {"R20", AchReturnCodeEnum.R20_NonTransactionAccount},
            {"R21", AchReturnCodeEnum.R21_InvalidCompanyIdentification},
            {"R22", AchReturnCodeEnum.R22_InvalidIndividualIDNumber},
            {"R23", AchReturnCodeEnum.R23_CreditRefusedByReceiver},
            {"R24", AchReturnCodeEnum.R24_DuplicateEntry},
            {"R26", AchReturnCodeEnum.R26_MandatoryFieldError},
            {"R28", AchReturnCodeEnum.R28_RoutingNumberCheckDigitError},
            {"R29", AchReturnCodeEnum.R29_CorporateCustomerAdvisesNotAuthorized},
            {"R31", AchReturnCodeEnum.R31_PermissibleReturnEntry},
            {"R32", AchReturnCodeEnum.R32_RDFINonSettlement},
            {"R33", AchReturnCodeEnum.R33_ReturnOfXCKEntry},
            {"R34", AchReturnCodeEnum.R34_LimitedParticipationDFI},
            {"R37", AchReturnCodeEnum.R37_SourceDocumentPresentedForPayment},
            {"R38", AchReturnCodeEnum.R38_StopPaymentOnSourceDocument},
            {"R39", AchReturnCodeEnum.R39_ImproperSourceDocumentSourceDocument},
            {"R50", AchReturnCodeEnum.R50_StateLawAffectingRCKAcceptance},
            {"R51", AchReturnCodeEnum.R51_ItemRelatedToRCKEntryIsIneligible},
            {"R52", AchReturnCodeEnum.R52_StopPaymentOnItemRelatedToRCKEntry},
            {"R53", AchReturnCodeEnum.R53_ItemAndRCKEntryPresentedForPayment},
            {"R80", AchReturnCodeEnum.R80_IATEntryCodingError},
            {"R81", AchReturnCodeEnum.R81_NonParticipantInIATProgram},
            {"R82", AchReturnCodeEnum.R82_InvalidForeignReceivingDFIIdentification},
            {"R83", AchReturnCodeEnum.R83_ForeignReceivingDFIUnableToSettle},
            {"R84", AchReturnCodeEnum.R84_EntryNotProcessedByGateway},
            {"R85", AchReturnCodeEnum.R85_IncorrectlyCodedOutboundInternationalPayment},
            {"R61", AchReturnCodeEnum.R61_MisroutedReturn},
            {"R67", AchReturnCodeEnum.R67_DuplicateReturn},
            {"R68", AchReturnCodeEnum.R68_UntimelyReturn},
            {"R69", AchReturnCodeEnum.R69_FieldError},
            {"R70", AchReturnCodeEnum.R70_PermissibleReturnEntryNotAcceptedReturnNotRequestedByODFI},
            {"C", AchReturnCodeEnum.C00_Unknown},
            {"C01", AchReturnCodeEnum.C01_IncorrectDFIAccount},
            {"C02", AchReturnCodeEnum.C02_IncorrectRoutingNumber},
            {"C03", AchReturnCodeEnum.C03_IncorrectRoutingNumberAndDFIAccountNumber},
            {"C04", AchReturnCodeEnum.C04_IncorrectIndividualCompanyName},
            {"C05", AchReturnCodeEnum.C05_IncorrectTransactionCode},
            {"C06", AchReturnCodeEnum.C06_IncorrectAccountNumberAndTransactionCode},
            {"C07", AchReturnCodeEnum.C07_IncorrectRoutingNumberIncorrectDFIAccountNumberAndIncorrectTransactionCode},
            {"C08", AchReturnCodeEnum.C08_IncorrectReceivingDFIdentificationIATOnly},
            {"C13", AchReturnCodeEnum.C13_AddendaFormatError},
            {"C14", AchReturnCodeEnum.C14_IncorrectSECCodeForOutboundInternationalPayment},
        };

        private static readonly IDictionary<AchReturnCodeEnum, string> ReverseMap = ForwardMap.ToDictionary(k => k.Value, v => v.Key);

        public AchReturnCodeEnum GetReturnCode(string code)
        {
            if (!string.IsNullOrEmpty(code) && ForwardMap.ContainsKey(code))
            {
                return ForwardMap[code];
            }

            return AchReturnCodeEnum.None;
        }

        public string GetReturnCode(AchReturnCodeEnum code)
        {
            if (ReverseMap.ContainsKey(code))
            {
                return ReverseMap[code];
            }

            return string.Empty;
        }
        
        public bool IsNocReturn(AchReturnDetail achReturnDetail)
        {
            return EnumHelper<AchReturnCodeEnum>.Contains(this.GetReturnCode(achReturnDetail.ReturnReasonCode), AchReturnCodeEnumGroup.NocCode);
        }

        public bool IsNocReturn(AchSettlementDetail achSettlementDetail)
        {
            return EnumHelper<AchReturnCodeEnum>.Contains(this.GetReturnCode(achSettlementDetail.ReturnCode), AchReturnCodeEnumGroup.NocCode);
        }

        public bool IsReturn(AchSettlementDetail achSettlementDetail)
        {
            return
                EnumHelper<AchReturnCodeEnum>.Contains(this.GetReturnCode(achSettlementDetail.ReturnCode), AchReturnCodeEnumGroup.ReturnCode)
                || (achSettlementDetail.Status.IsNotNullOrEmpty() && achSettlementDetail.Status.Equals("Return", StringComparison.OrdinalIgnoreCase));
        }
    }
}
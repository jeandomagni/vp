﻿namespace Ivh.Domain.FinanceManagement.Ach.MappingsCsv
{
    using CsvHelper.Configuration;
    using Entities;

    public sealed class AchSettlementDetailMap : CsvClassMap<AchSettlementDetail>
    {
        public AchSettlementDetailMap()
        {
            this.Map(x => x.AchSettlementDetailId).Ignore();
            this.Map(x => x.AchFile).Ignore();
            this.Map(x => x.DownloadedDate).Ignore();
            this.Map(x => x.ProcessedDate).Ignore();

            //FileName,ProcessingDate,textbox21,IndividualID,DebitAmount,CreditAmount,SECCode,Status,ReturnCode,ReturnDate,ReturnAddenda,DishonorReason,PaidGroup,DebitAmount_1,CreditAmount_1
            int i = 0;
            this.Map(x => x.FileName).Index(i++);
            this.Map(x => x.ProcessingDate).Index(i++);
            this.Map(x => x.Textbox21).Index(i++);
            this.Map(x => x.IndividualId).Index(i++);
            this.Map(x => x.DebitAmount).Index(i++);
            this.Map(x => x.CreditAmount).Index(i++);
            this.Map(x => x.SecCode).Index(i++);
            this.Map(x => x.Status).Index(i++);
            this.Map(x => x.ReturnCode).Index(i++);
            this.Map(x => x.ReturnDate).Index(i++);
            this.Map(x => x.ReturnAddenda).Index(i++);
            this.Map(x => x.DishonorReason).Index(i++);
            this.Map(x => x.PaidGroup).Index(i++);
            this.Map(x => x.DebitAmount1).Index(i++);
            this.Map(x => x.CreditAmount1).Index(i++);
        }
    }
}
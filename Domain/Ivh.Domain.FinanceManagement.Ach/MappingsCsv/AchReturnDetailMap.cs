﻿namespace Ivh.Domain.FinanceManagement.Ach.MappingsCsv
{
    using CsvHelper.Configuration;
    using Entities;

    public sealed class AchReturnDetailMap : CsvClassMap<AchReturnDetail>
    {
        public AchReturnDetailMap()
        {
            this.Map(x => x.AchReturnDetailId).Ignore();
            this.Map(x => x.AchFile).Ignore();
            this.Map(x => x.DownloadedDate).Ignore();
            this.Map(x => x.ProcessedDate).Ignore();
            this.Map(x => x.AmountNumeric).Ignore();
            this.Map(x => x.AmountNumeric).Ignore();

            int i = 0;
            this.Map(x => x.CompanyId).Index(i++);
            this.Map(x => x.CompanyName).Index(i++);
            this.Map(x => x.SecCode).Index(i++);
            this.Map(x => x.Description).Index(i++);
            this.Map(x => x.EffectiveDate).Index(i++);
            this.Map(x => x.IndividualId).Index(i++);
            this.Map(x => x.IndividualName).Index(i++);
            this.Map(x => x.RdfiRoutingNumber).Index(i++);
            this.Map(x => x.RdfiAccountNumber).Index(i++);
            this.Map(x => x.TransactionCode).Index(i++);
            this.Map(x => x.Amount).Index(i++);
            this.Map(x => x.CustomerSpecifiedTraceNumber).Index(i++);
            this.Map(x => x.WebTransactionSingleOrRecurringIdentifier).Index(i++);
            this.Map(x => x.ReturnReasonCode).Index(i++);
            this.Map(x => x.ReturnCorrectionOrAddendaInfo).Index(i++);
            this.Map(x => x.ReturnDate).Index(i++);
            this.Map(x => x.ReturnTraceNumber).Index(i++);
        }
    }
}
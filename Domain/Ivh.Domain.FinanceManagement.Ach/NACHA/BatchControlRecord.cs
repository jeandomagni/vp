﻿namespace Ivh.Domain.FinanceManagement.Ach.Entities
{
    using CsvHelper.Configuration;
    using TypeConverters;

    [Record(RecordTypeCode = 8)]
    public class BatchControlRecord : RecordBase
    {
        [Field(DataElementName = "Service Class Code", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "Numeric", Length = 3, Index = 1)]
        public int ServiceClassCode { get; set; }

        [Field(DataElementName = "Entry Addenda Count", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "Numeric", Length = 6, Index = 4)]
        public int EntryAddendaCount { get; set; }

        [Field(DataElementName = "Entry Hash", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "Numeric", Length = 10, Index = 10)]
        public int EntryHash { get; set; }

        [Field(DataElementName = "Total Debit Entry Dollar Amount", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "$$$$$$$$$$cc", Length = 12, Index = 20)]
        public decimal TotalDebitEntryDollarAmount { get; set; }

        [Field(DataElementName = "Total Credit Entry Dollar Amount", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "$$$$$$$$$$cc", Length = 12, Index = 32)]
        public decimal TotalCreditEntryDollarAmount { get; set; }

        [Field(DataElementName = "Company Identification", FieldInclusionRequirement = FieldInclusionRequirementEnum.Required, Contents = "NNNNNNNNNN", Length = 12, Index = 44)]
        public string CompanyIdentification { get; set; }

        [Field(DataElementName = "Method Authentication Code", FieldInclusionRequirement = FieldInclusionRequirementEnum.Optional, Contents = "BLANK", Length = 19, Index = 54)]
        public string MethodAuthenticationCode { get; set; }

        [Field(DataElementName = "Reserved", FieldInclusionRequirement = FieldInclusionRequirementEnum.Optional, Contents = "BLANK", Length = 6, Index = 73)]
        public string Reserved { get; set; }

        [Field(DataElementName = "Originating DFI Identification", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "TTTTAAAA", Length = 6, Index = 79)]
        public string OriginatingDfiIdentification { get; set; }

        [Field(DataElementName = "Batch Number", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "Numeric", Length = 7, Index = 87)]
        public int BatchNumber { get; set; }
    }

    public sealed class BatchControlRecordMap : CsvClassMap<BatchControlRecord>
    {
        public BatchControlRecordMap()
        {
            this.Map(x => x.ServiceClassCode).Index(1).TypeConverter<AchIntegerConverter>();
            this.Map(x => x.EntryAddendaCount).Index(2).TypeConverter<AchIntegerConverter>();
            this.Map(x => x.EntryHash).Index(3).TypeConverter<AchIntegerConverter>();
            this.Map(x => x.TotalDebitEntryDollarAmount).Index(4).TypeConverter<AchDollarAmountConverter>();
            this.Map(x => x.TotalCreditEntryDollarAmount).Index(5).TypeConverter<AchDollarAmountConverter>();
            this.Map(x => x.CompanyIdentification).Index(6);
            this.Map(x => x.MethodAuthenticationCode).Index(7);
            this.Map(x => x.Reserved).Index(8);
            this.Map(x => x.OriginatingDfiIdentification).Index(9);
            this.Map(x => x.EntryHash).Index(10).TypeConverter<AchIntegerConverter>();
        }
    }
}
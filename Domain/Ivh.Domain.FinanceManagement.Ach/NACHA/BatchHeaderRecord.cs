﻿namespace Ivh.Domain.FinanceManagement.Ach.Entities
{
    using System;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using CsvHelper.Configuration;
    using TypeConverters;

    [Record(RecordTypeCode = 5)]
    public class BatchHeaderRecord : RecordBase
    {
        [Field(DataElementName = "Service Class Code", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "NNN", Length = 3, Index = 1)]
        public int ServiceClassCode { get; set; }

        [Field(DataElementName = "Company Name", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "Alpha-Numeric", Length = 16, Index = 4)]
        public string CompanyName { get; set; }

        [Field(DataElementName = "Company Discretionary Data", FieldInclusionRequirement = FieldInclusionRequirementEnum.Optional, Contents = "Alpha-Numeric", Length = 20, Index = 20)]
        public string CompanyDiscretionaryData { get; set; }

        [Field(DataElementName = "Company Identification", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "NNNNNNNNNN", Length = 10, Index = 40)]
        public string CompanyIdentification { get; set; }

        [Field(DataElementName = "Standard Entry Class Code", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "PPD,CCD,CTX,TEL,WEB", Length = 3, Index = 50)]
        public StandardEntryClassEnum StandardEntryClassCode { get; set; }

        [Field(DataElementName = "Company Entry Description", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "Alpha-Numeric", Length = 10, Index = 53)]
        public string CompanyEntryDescription { get; set; }

        [Field(DataElementName = "Company Descriptive Date", FieldInclusionRequirement = FieldInclusionRequirementEnum.Optional, Contents = "Alpha-Numeric", Length = 6, Index = 63)]
        public string CompanyDescriptiveDate { get; set; }

        [Field(DataElementName = "Effective Entry Date", FieldInclusionRequirement = FieldInclusionRequirementEnum.Required, Contents = "YYMMDD", Length = 6, Index = 69, FormatPattern = "YYMMDD")]
        public DateTime EffectiveEntryDate { get; set; }

        [Field(DataElementName = "Settlement Date", FieldInclusionRequirement = FieldInclusionRequirementEnum.Optional, Contents = "Blank/Reserved", Length = 3, Index = 75)]
        public string SettlementDate { get; set; }

        [Field(DataElementName = "Originator Status Code", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "1", Length = 1, Index = 78)]
        public int OriginatorStatusCode { get; set; }

        [Field(DataElementName = "Originating DFI Identification", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "NNNNNNNN", Length = 8, Index = 79)]
        public string OriginatingDfiIdentification { get; set; }

        [Field(DataElementName = "Batch Number", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "Numeric", Length = 7, Index = 87)]
        public int BatchNumber { get; set; }
    }

    public sealed class BatchHeaderRecordMap : CsvClassMap<BatchHeaderRecord>
    {
        public BatchHeaderRecordMap()
        {
            this.Map(x => x.ServiceClassCode).Index(1).TypeConverter<AchIntegerConverter>();
            this.Map(x => x.CompanyName).Index(2);
            this.Map(x => x.CompanyDiscretionaryData).Index(3);
            this.Map(x => x.CompanyIdentification).Index(4);
            this.Map(x => x.StandardEntryClassCode).Index(5).TypeConverter<AchStandardClassEntryCodeConverter>();
            this.Map(x => x.CompanyEntryDescription).Index(6);
            this.Map(x => x.CompanyDescriptiveDate).Index(7);
            this.Map(x => x.EffectiveEntryDate).Index(8).TypeConverter<AchDateConverter>();
            this.Map(x => x.SettlementDate).Index(9);
            this.Map(x => x.OriginatorStatusCode).Index(10).TypeConverter<AchIntegerConverter>();
            this.Map(x => x.OriginatingDfiIdentification).Index(11);
            this.Map(x => x.BatchNumber).Index(12).TypeConverter<AchIntegerConverter>();
        }
    }
}
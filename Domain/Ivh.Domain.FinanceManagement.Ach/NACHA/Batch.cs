﻿namespace Ivh.Domain.FinanceManagement.Ach.Utility
{
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using Entities.EntryDetailRecord;

    public class Batch : IBatch
    {
        public BatchHeaderRecord BatchHeader { get; set; }
        public ICollection<IEntryDetailRecord> EntryDetails { get; set; }
        public BatchControlRecord BatchControl { get; set; }

        public StandardEntryClassEnum StandardEntryClass
        {
            get { return this.BatchHeader.StandardEntryClassCode; }
        }

        public Batch()
        {
            this.EntryDetails = new List<IEntryDetailRecord>();
        }
    }
}
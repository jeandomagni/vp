﻿namespace Ivh.Domain.FinanceManagement.Ach.Entities.EntryDetailRecord
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using EntryDetailAddendaRecord;

    [Record(RecordTypeCode = 6)]
    public class CtxEntryDetailRecord : EntryDetailRecordBase, IEntryDetailRecord
    {
        public override StandardEntryClassEnum StandardEntryClass
        {
            get { return StandardEntryClassEnum.CTX; }
        }

        public ICollection<CtxEntryDetailRecord> EntryDetailAddenda { get; set; }

        public CtxEntryDetailRecord()
        {
            throw new NotImplementedException();
            //this.EntryDetailAddenda = new List<CtxEntryDetailRecord>();
        }
    }
}
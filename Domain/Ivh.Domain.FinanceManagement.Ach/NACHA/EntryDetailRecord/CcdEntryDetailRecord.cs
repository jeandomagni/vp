﻿namespace Ivh.Domain.FinanceManagement.Ach.Entities.EntryDetailRecord
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using EntryDetailAddendaRecord;

    [Record(RecordTypeCode = 6)]
    public class CcdEntryDetailRecord : EntryDetailRecordBase, IEntryDetailRecord
    {
        public override StandardEntryClassEnum StandardEntryClass
        {
            get { return StandardEntryClassEnum.CCD; }
        }

        public ICollection<CcdEntryDetailRecord> EntryDetailAddenda { get; set; }

        public CcdEntryDetailRecord()
        {
            throw new NotImplementedException();
            //this.EntryDetailAddenda = new List<CcdEntryDetailRecord>();
        }
    }
}
﻿namespace Ivh.Domain.FinanceManagement.Ach.Entities.EntryDetailRecord
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using CsvHelper.Configuration;
    using EntryDetailAddendaRecord;
    using TypeConverters;

    [Record(RecordTypeCode = 6)]
    public class WebEntryDetailRecord : EntryDetailRecordBase, IEntryDetailRecord
    {
        public override StandardEntryClassEnum StandardEntryClass
        {
            get { return StandardEntryClassEnum.WEB; }
        }

        [Field(DataElementName = "Transaction Code", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "Numeric", Length = 2, Index = 1)]
        public int TransactionCode { get; set; }
        
        [Field(DataElementName = "Receiving DFI Identification", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "TTTTAAAA", Length = 8, Index = 3)]
        public string ReceivingDfiIdentification { get; set; }

        [Field(DataElementName = "Check Digit", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "Numeric", Length = 1, Index = 11)]
        public int CheckDigit { get; set; }

        [Field(DataElementName = "DFI Account Number", FieldInclusionRequirement = FieldInclusionRequirementEnum.Required, Contents = "Alpha-Numeric", Length = 17, Index = 12)]
        public string DfiAccountNumber { get; set; }

        [Field(DataElementName = "Amount", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "$$$$$$$$cc", Length = 10 , Index = 29)]
        public decimal Amount { get; set; }

        [Field(DataElementName = "Individual Identification Number", FieldInclusionRequirement = FieldInclusionRequirementEnum.Optional, Contents = "Alpha-Numeric", Length = 15, Index = 39)]
        public string IndividualIdentificationNumber { get; set; }

        [Field(DataElementName = "Individual Name", FieldInclusionRequirement = FieldInclusionRequirementEnum.Required, Contents = "Alpha-Numeric", Length = 22, Index = 54)]
        public string IndividualName { get; set; }

        [Field(DataElementName = "Payment Type Code", FieldInclusionRequirement = FieldInclusionRequirementEnum.Required, Contents = "Alpha-Numeric", Length = 2, Index = 76)]
        public string PaymentTypeCode { get; set; }

        [Field(DataElementName = "Addenda Record Indicator", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "Numeric", Length = 1, Index = 78)]
        public bool AddendaRecordIndicator { get; set; }

        [Field(DataElementName = "Trace Number", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "Numeric", Length = 15, Index = 79)]
        public long TraceNumber { get; set; }
    }

    public sealed class WebEntryDetailRecordMap : CsvClassMap<WebEntryDetailRecord>
    {
        public WebEntryDetailRecordMap()
        {
            Map(x => x.TransactionCode).Index(1).TypeConverter<AchIntegerConverter>();
            Map(x => x.ReceivingDfiIdentification).Index(2);
            Map(x => x.CheckDigit).Index(3).TypeConverter<AchIntegerConverter>();
            Map(x => x.DfiAccountNumber).Index(4);
            Map(x => x.Amount).Index(5).TypeConverter<AchDollarAmountConverter>();
            Map(x => x.IndividualIdentificationNumber).Index(6);
            Map(x => x.IndividualName).Index(7);
            Map(x => x.PaymentTypeCode).Index(8);
            Map(x => x.AddendaRecordIndicator).Index(9).TypeConverter<AchBooleanConverter>();
            Map(x => x.TraceNumber).Index(10).TypeConverter<AchBigIntegerConverter>();
        }
    }
}
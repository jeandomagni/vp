﻿namespace Ivh.Domain.FinanceManagement.Ach.Entities.EntryDetailRecord
{
    using System;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using EntryDetailAddendaRecord;

    [Record(RecordTypeCode = 6)]
    public class PpdEntryDetailRecord : EntryDetailRecordBase, IEntryDetailRecord
    {
        public override StandardEntryClassEnum StandardEntryClass
        {
            get { return StandardEntryClassEnum.PPD; }
        }

        public PpdEntryDetailRecord()
        {
            throw new NotImplementedException();
        }
    }
}
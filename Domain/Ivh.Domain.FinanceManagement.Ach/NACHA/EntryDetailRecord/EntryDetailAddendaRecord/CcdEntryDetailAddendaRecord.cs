﻿namespace Ivh.Domain.FinanceManagement.Ach.Entities.EntryDetailRecord.EntryDetailAddendaRecord
{
    using System;

    [Record(RecordTypeCode = 7)]
    public class CcdEntryDetailAddendaRecord : EntryDetailAddendaRecordBase, IEntryDetailAddendaRecord
    {
        public CcdEntryDetailAddendaRecord()
        {
            throw new NotImplementedException();
        }
    }
}
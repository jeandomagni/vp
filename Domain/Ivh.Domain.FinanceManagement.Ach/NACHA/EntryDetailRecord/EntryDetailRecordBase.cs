﻿namespace Ivh.Domain.FinanceManagement.Ach.Entities.EntryDetailRecord
{
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using EntryDetailAddendaRecord;

    [Record(RecordTypeCode = 6)]
    public abstract class EntryDetailRecordBase
        : RecordBase, IEntryDetailRecord
    {
        public abstract StandardEntryClassEnum StandardEntryClass { get; }

        protected EntryDetailRecordBase()
        {
        }
    }
}
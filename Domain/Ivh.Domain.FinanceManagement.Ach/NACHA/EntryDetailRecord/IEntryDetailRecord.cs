﻿namespace Ivh.Domain.FinanceManagement.Ach.Entities.EntryDetailRecord
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public interface IEntryDetailRecord
    {
        StandardEntryClassEnum StandardEntryClass { get; }
    }
}
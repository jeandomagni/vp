﻿namespace Ivh.Domain.FinanceManagement.Ach.Entities
{
    using CsvHelper.Configuration;
    using TypeConverters;

    [Record(RecordTypeCode = 9)]
    public class FileControlRecord : RecordBase
    {
        [Field(DataElementName = "Batch Count", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "Numeric", Length = 6, Index = 1)]
        public int BatchCount { get; set; }

        [Field(DataElementName = "Block Count", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "Numeric", Length = 6, Index = 7)]
        public int BlockCount { get; set; }

        [Field(DataElementName = "Entry Addenda Count", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "Numeric", Length = 8, Index = 13)]
        public int EntryAddendaCount { get; set; }

        [Field(DataElementName = "Entry Hash", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "Numeric", Length = 10, Index = 21)]
        public int EntryHash { get; set; }

        [Field(DataElementName = "Total Debit Entry Dollar Amount", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "$$$$$$$$$$cc", Length = 12, Index = 31)]
        public decimal TotalDebitEntryDollarAmount { get; set; }

        [Field(DataElementName = "Total Credit Entry Dollar Amount", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "$$$$$$$$$$cc", Length = 12, Index = 43)]
        public decimal TotalCreditEntryDollarAmount { get; set; }

        [Field(DataElementName = "Reserved", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "Blank", Length = 39, Index = 55)]
        public string Reserved { get; set; }
    }

    public sealed class FileControlRecordMap : CsvClassMap<FileControlRecord>
    {
        public FileControlRecordMap()
        {
            this.Map(x => x.BatchCount).Index(1).TypeConverter<AchIntegerConverter>();
            this.Map(x => x.BlockCount).Index(2).TypeConverter<AchIntegerConverter>();
            this.Map(x => x.EntryAddendaCount).Index(3).TypeConverter<AchIntegerConverter>();
            this.Map(x => x.EntryHash).Index(4).TypeConverter<AchIntegerConverter>();
            this.Map(x => x.TotalDebitEntryDollarAmount).Index(5).TypeConverter<AchDollarAmountConverter>();
            this.Map(x => x.TotalCreditEntryDollarAmount).Index(6).TypeConverter<AchDollarAmountConverter>();
            this.Map(x => x.Reserved).Index(7).TypeConverter<AchIntegerConverter>();
        }
    }
}
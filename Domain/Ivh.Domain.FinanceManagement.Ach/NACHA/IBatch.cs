﻿namespace Ivh.Domain.FinanceManagement.Ach.Entities
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public interface IBatch
    {
        StandardEntryClassEnum StandardEntryClass { get; }
    }
}

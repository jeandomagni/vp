﻿namespace Ivh.Domain.FinanceManagement.Ach.Entities
{
    using System;

    [AttributeUsage(AttributeTargets.Class)]
    public sealed class RecordAttribute : Attribute
    {
        public int RecordTypeCode { get; set; }
    }
}
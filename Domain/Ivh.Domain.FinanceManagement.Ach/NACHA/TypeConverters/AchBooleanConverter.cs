﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.FinanceManagement.Ach.TypeConverters
{
    using System.Globalization;
    using CsvHelper.TypeConversion;

    public class AchBooleanConverter : ITypeConverter
    {
        public bool CanConvertFrom(Type type)
        {
            return type == typeof(string);
        }

        public bool CanConvertTo(Type type)
        {
            return type == typeof(bool);
        }

        public object ConvertFromString(TypeConverterOptions options, string text)
        {
            return text[0] == '1';
        }

        public string ConvertToString(TypeConverterOptions options, object value)
        {
            bool boolean = (bool)value;

            return boolean ? "1" : "0";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.FinanceManagement.Ach.TypeConverters
{
    using System.Globalization;
    using CsvHelper.TypeConversion;

    public class AchDollarAmountConverter : ITypeConverter
    {
        public bool CanConvertFrom(Type type)
        {
            return type == typeof(string);
        }

        public bool CanConvertTo(Type type)
        {
            return type == typeof(decimal);
        }

        public object ConvertFromString(TypeConverterOptions options, string text)
        {
            return decimal.Parse(text) / 100;
        }

        public string ConvertToString(TypeConverterOptions options, object value)
        {
            decimal amount = (decimal)value;

            return (amount * 100).ToString(CultureInfo.InvariantCulture);
        }
    }
}

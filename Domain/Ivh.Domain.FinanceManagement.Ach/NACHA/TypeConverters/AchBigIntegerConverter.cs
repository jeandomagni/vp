﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.FinanceManagement.Ach.TypeConverters
{
    using System.Globalization;
    using CsvHelper.TypeConversion;

    public class AchBigIntegerConverter : ITypeConverter
    {
        public bool CanConvertFrom(Type type)
        {
            return type == typeof(string);
        }

        public bool CanConvertTo(Type type)
        {
            return type == typeof(long);
        }

        public object ConvertFromString(TypeConverterOptions options, string text)
        {
            return int.Parse(text);
        }

        public string ConvertToString(TypeConverterOptions options, object value)
        {
            int integer = (int)value;

            return (integer).ToString(CultureInfo.InvariantCulture);
        }
    }
}

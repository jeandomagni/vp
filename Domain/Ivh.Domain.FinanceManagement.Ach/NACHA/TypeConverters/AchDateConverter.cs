﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.FinanceManagement.Ach.TypeConverters
{
    using CsvHelper.TypeConversion;

    public class AchDateConverter : ITypeConverter
    {
        public bool CanConvertFrom(Type type)
        {
            return type == typeof(string);
        }

        public bool CanConvertTo(Type type)
        {
            return type == typeof(DateTime);
        }

        public object ConvertFromString(TypeConverterOptions options, string text)
        {
            return new DateTime(2000 + int.Parse(text.Substring(0,2)),int.Parse(text.Substring(2,2)),int.Parse(text.Substring(4,2)));
        }

        public string ConvertToString(TypeConverterOptions options, object value)
        {
            DateTime dateTime = (DateTime) value;

            return dateTime.ToString("YYMMdd");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.FinanceManagement.Ach.TypeConverters
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using CsvHelper.TypeConversion;

    public class AchStandardClassEntryCodeConverter : ITypeConverter
    {
        public bool CanConvertFrom(Type type)
        {
            return type == typeof(string);
        }

        public bool CanConvertTo(Type type)
        {
            return type == typeof(StandardEntryClassEnum);
        }

        public object ConvertFromString(TypeConverterOptions options, string text)
        {
            return Enum.Parse(typeof (StandardEntryClassEnum), text, true);
        }

        public string ConvertToString(TypeConverterOptions options, object value)
        {
            StandardEntryClassEnum code = (StandardEntryClassEnum)value;

            return code.ToString();
        }
    }
}

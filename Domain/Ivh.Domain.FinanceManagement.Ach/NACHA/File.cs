﻿namespace Ivh.Domain.FinanceManagement.Ach.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using EntryDetailRecord;
    using Utility;

    public class File
    {
        public FileHeaderRecord FilehHeader { get; set; }
        public ICollection<Batch> Batches { get; set; }
        public FileControlRecord FileControl { get; set; }

        private File()
        {
            this.Batches = new List<Batch>();
        }

        public static File Load(IEnumerable<string> records)
        {
            File file = new File();

            IEnumerator<string> recordsEnumerator = records.GetEnumerator();
            Batch currentBatch = null;
            IEntryDetailRecord currentRecord = null;
            while (recordsEnumerator.MoveNext())
            {
                switch (recordsEnumerator.Current[0])
                {
                    case '1':
                        file.FilehHeader = new FileHeaderRecord();
                        break;
                    case '5':
                        currentBatch = new Batch { BatchHeader = new BatchHeaderRecord() };
                        file.Batches.Add(currentBatch);
                        break;
                    case '6':
                        switch (currentBatch.StandardEntryClass)
                        {
                            case StandardEntryClassEnum.WEB:
                                currentRecord = new WebEntryDetailRecord();
                                break;
                            default:
                                throw new NotImplementedException();
                        }
                        currentBatch.EntryDetails.Add(currentRecord);
                        break;
                    case '7':
                        throw new NotImplementedException();// 7 is for detail addenda. addenda are only for CCD and CTX types. We should only be doing WEB.
                    case '8':
                        currentBatch.BatchControl = new BatchControlRecord();
                        break;
                    case '9':
                        file.FileControl = new FileControlRecord();
                        break;

                }
            }

            return file;
        }
    }
}
﻿namespace Ivh.Domain.FinanceManagement.Ach.Entities
{
    using System;
    using CsvHelper;
    using CsvHelper.Configuration;
    using TypeConverters;

    [Record(RecordTypeCode = 1)]
    public class FileHeaderRecord : RecordBase
    {
        [Field(DataElementName = "Priority Code", FieldInclusionRequirement = FieldInclusionRequirementEnum.Required, Contents = "01", Length = 2, Index = 1)]
        public int PriorityCode { get; set; }

        [Field(DataElementName = "Immediate Destination", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "bNNNNNNNNN", Length = 10, Index = 3)]
        public string ImmediateDestination { get; set; }

        [Field(DataElementName = "Immediate Origin", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "NNNNNNNNNN", Length = 10, Index = 13)]
        public string ImmediateOrigin { get; set; }

        [Field(DataElementName = "File Creation Date", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "YYMMDD", Length = 6, Index = 23, FormatPattern = "YYMMDD")]
        public DateTime FileCreationDate { get; set; }

        [Field(DataElementName = "File Creation Time", FieldInclusionRequirement = FieldInclusionRequirementEnum.Optional, Contents = "HHMM", Length = 4, Index = 29, FormatPattern = "HHMM")]
        public DateTime FileCreationTime { get; set; }

        [Field(DataElementName = "File ID Modifier", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "Alpha Numeric", Length = 1, Index = 33)]
        public string FileIdModifier { get; set; }

        [Field(DataElementName = "Record Size", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "094", Length = 3, Index = 34)]
        public int RecordSize { get; set; }

        [Field(DataElementName = "Blocking Factor", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "10", Length = 2, Index = 37)]
        public int BlockingFactor { get; set; }

        [Field(DataElementName = "Format Code", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Contents = "1", Length = 1, Index = 39)]
        public int FormatCode { get; set; }

        [Field(DataElementName = "Immediate Destination Name", FieldInclusionRequirement = FieldInclusionRequirementEnum.Optional, Contents = "Alpha Numeric", Length = 23, Index = 40)]
        public string ImmediateDestinationName { get; set; }

        [Field(DataElementName = "Immediate Origin Name", FieldInclusionRequirement = FieldInclusionRequirementEnum.Optional, Contents = "Alpha Numeric", Length = 23, Index = 63)]
        public string ImmediateOriginName { get; set; }

        [Field(DataElementName = "Reference Code", FieldInclusionRequirement = FieldInclusionRequirementEnum.Optional, Contents = "Alpha Numeric", Length = 23, Index = 63)]
        public string ReferenceCode { get; set; }
    }

    public class FileHeaderRecordMap : CsvClassMap<FileHeaderRecord>
    {
        public FileHeaderRecordMap()
        {
            this.Map(x => x.PriorityCode).Index(1).TypeConverter<AchIntegerConverter>();
            this.Map(x => x.ImmediateDestination).Index(2);
            this.Map(x => x.ImmediateOrigin).Index(3);
            this.Map(x => x.FileCreationDate).Index(4).TypeConverter<AchDateConverter>();
            this.Map(x => x.FileCreationTime).Index(5).TypeConverter<AchTimeConverter>();
            this.Map(x => x.FileIdModifier).Index(6);
            this.Map(x => x.RecordSize).Index(7).TypeConverter<AchIntegerConverter>();
            this.Map(x => x.BlockingFactor).Index(8).TypeConverter<AchIntegerConverter>();
            this.Map(x => x.FormatCode).Index(9).TypeConverter<AchIntegerConverter>();
            this.Map(x => x.ImmediateDestinationName).Index(10);
            this.Map(x => x.ImmediateOriginName).Index(11);
            this.Map(x => x.ReferenceCode).Index(12);
        }
    }
}
﻿namespace Ivh.Domain.FinanceManagement.Ach.Entities
{
    using System;

    [AttributeUsage(AttributeTargets.Property)]
    public sealed class FieldAttribute : Attribute
    {
        public string DataElementName { get; set; }
        public FieldInclusionRequirementEnum FieldInclusionRequirement { get; set; }
        public string Contents { get; set; }
        public int Length { get; set; }
        public int Index { get; set; }
        public string FormatPattern { get; set; }
    }

    public enum FieldInclusionRequirementEnum
    {
        Mandatory,
        Required,
        Optional
    }
}
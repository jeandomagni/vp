﻿namespace Ivh.Domain.FinanceManagement.Ach.Entities
{
    using System;
    using System.Linq;
    using System.Reflection;

    public abstract class RecordBase
    {
        private int? _recordTypeCode;

        [Field(DataElementName = "Record Type Code", FieldInclusionRequirement = FieldInclusionRequirementEnum.Mandatory, Length = 1, Index = 0)]
        public int RecordTypeCode
        {
            get
            {
                if (!this._recordTypeCode.HasValue)
                {
                    PropertyInfo prop = this.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance)
                        .FirstOrDefault(p => p.GetCustomAttributes(typeof (RecordAttribute), false).Count() == 1);
                    if (prop == null) throw new ArgumentNullException("RecordAttribute.RecordTypeCode not set on class");
                    RecordAttribute ret = (RecordAttribute) prop.GetValue(this, null);
                    this._recordTypeCode = ret.RecordTypeCode;
                }
                return this._recordTypeCode.Value;
            }
        }
    }
}
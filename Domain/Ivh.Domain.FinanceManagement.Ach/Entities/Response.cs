﻿namespace Ivh.Domain.FinanceManagement.Ach.Entities
{
    using System;
    using System.IO;
    using System.Xml.Serialization;
    using Common.Base.Utilities.Extensions;

    public class Response
    {
        public string Code { get; set; }
        public string Message { get; set; }
        [XmlElement("Value")]
        public string ValueRaw { get; set; }
        
        [XmlIgnore]
        public string Value
        {
            get
            {
                if (this.ValueRaw == null)
                    return null;
              
                if (!string.IsNullOrEmpty(this.ValueRaw) && this.ValueRaw.IsBase64Encoded())
                {
                    byte[] decodedBytes = Convert.FromBase64String(this.ValueRaw);

                    using (MemoryStream stream = new MemoryStream(decodedBytes, 0, decodedBytes.Length))
                    {
                        stream.Position = 0;
                        StreamReader sr = new StreamReader(stream);
                        return sr.ReadToEnd();
                    }
                }

                return this.ValueRaw;
            }
        }
    }
}

﻿namespace Ivh.Domain.FinanceManagement.Ach.Entities
{
    using System;
    using Common.Base.Utilities.Extensions;
    
    public class AchSettlementDetail
    {
        public virtual int AchSettlementDetailId { get; set; }
        public virtual AchFile AchFile { get; set; }
        public virtual DateTime DownloadedDate { get; set; }
        public virtual DateTime? ProcessedDate { get; set; }

        public virtual string PaidGroup { get; set; }
        public virtual string DebitAmount1 { get; set; }
        public virtual string CreditAmount1 { get; set; }
        public virtual string FileName { get; set; }
        public virtual string ProcessingDate { get; set; }
        public virtual string Textbox21 { get; set; }
        public virtual string IndividualId { get; set; }
        public virtual string DebitAmount { get; set; }
        public virtual string CreditAmount { get; set; }
        public virtual string SecCode { get; set; }
        public virtual string Status { get; set; }
        public virtual string ReturnCode { get; set; }
        public virtual string ReturnDate { get; set; }
        public virtual string ReturnAddenda { get; set; }
        public virtual string DishonorReason { get; set; }
        public virtual string Textbox4 { get; set; }
        public virtual string Textbox5 { get; set; }
        public virtual string Textbox6 { get; set; }
        public virtual string Textbox7 { get; set; }

        public virtual bool IsValid()
        {
            return this.IndividualId.IsNotNullOrEmpty()
                   && this.FileName.IsNotNullOrEmpty()
                   && this.DebitAmount.IsNotNullOrEmpty()
                   && this.CreditAmount.IsNotNullOrEmpty()
                   && this.SecCode.IsNotNullOrEmpty()
                   && this.Status.IsNotNullOrEmpty();
        }
    }
}
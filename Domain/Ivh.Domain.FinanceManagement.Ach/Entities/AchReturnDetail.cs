﻿namespace Ivh.Domain.FinanceManagement.Ach.Entities
{
    using System;
    
    public class AchReturnDetail
    {
        public virtual int AchReturnDetailId { get; set; }
        public virtual AchFile AchFile { get; set; }
        public virtual DateTime DownloadedDate { get; set; }
        public virtual DateTime? ProcessedDate { get; set; }

        public virtual string CompanyId { get; set; }
        public virtual string CompanyName { get; set; }
        public virtual string SecCode { get; set; }
        public virtual string Description { get; set; }
        public virtual string EffectiveDate { get; set; }
        public virtual string IndividualId { get; set; }
        public virtual string IndividualName { get; set; }
        public virtual string RdfiRoutingNumber { get; set; }
        public virtual string RdfiAccountNumber { get; set; }
        public virtual string TransactionCode { get; set; }
        public virtual string Amount { get; set; }

        public virtual decimal AmountNumeric
        {
            get
            {
                decimal amount = 0m;
                return decimal.TryParse(this.Amount, out amount) ? amount / (this.Amount.Contains(".") ? 1 : 100) : amount;
            }
        }
        public virtual string CustomerSpecifiedTraceNumber { get; set; }
        public virtual string WebTransactionSingleOrRecurringIdentifier { get; set; }
        public virtual string ReturnReasonCode { get; set; }
        public virtual string ReturnCorrectionOrAddendaInfo { get; set; }
        public virtual string ReturnDate { get; set; }
        public virtual string ReturnTraceNumber { get; set; }
    }
}
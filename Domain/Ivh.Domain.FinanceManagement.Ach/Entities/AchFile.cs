﻿namespace Ivh.Domain.FinanceManagement.Ach.Entities
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class AchFile
    {
        public virtual int AchFileId { get; set; }
        public virtual DateTime FileDateFrom { get; set; }
        public virtual DateTime FileDateTo { get; set; }
        public virtual DateTime DownloadedDate { get; set; }
        public virtual DateTime? ProcessedDate { get; set; }
        public virtual AchFileTypeEnum AchFileType { get; set; }
        public virtual Guid FileReferenceToken { get; set; }

        public virtual IList<AchReturnDetail> ReturnDetails { get; set; }
        public virtual IList<AchSettlementDetail> SettlementDetails { get; set; }
    }
}
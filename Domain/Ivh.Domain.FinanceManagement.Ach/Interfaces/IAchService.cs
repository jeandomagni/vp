﻿namespace Ivh.Domain.FinanceManagement.Ach.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IAchService : IDomainService
    {
        bool DownloadReturns(DateTime startDate, DateTime endDate);
        IReadOnlyList<AchReturnDetail> GetReturnDetails(DateTime startDate, DateTime endDate, bool unprocessedOnly = false);
        AchReturnDetail GetReturnDetail(int id);
        void MarkReturnDetailAsProcessed(int achReturnDetailId, DateTime processedDate);

        bool DownloadSettlements(DateTime startDate, DateTime endDate);
        IReadOnlyList<AchSettlementDetail> GetSettlementDetails(DateTime startDate, DateTime endDate, bool unprocessedOnly = false);
        AchSettlementDetail GetSettlementDetail(int id);
        IList<AchSettlementDetail> GetAchSettlementDetailsByTransactionId(string transactionId);
        void MarkSettlementDetailAsProcessed(int achSettlementDetailId, DateTime processedDate);
        void AddAchFile(AchFile achFile);
        AchReturnCodeEnum GetReturnCode(string code);
        string GetReturnCode(AchReturnCodeEnum code);

        bool IsNocReturn(AchReturnDetail achReturnDetail);
        bool IsNocReturn(AchSettlementDetail achSettlementDetail);
        bool IsReturn(AchSettlementDetail achSettlementDetail);

        int DoOptimisticSettlement(string guarantorName, string individualId, decimal amount);
    }
}

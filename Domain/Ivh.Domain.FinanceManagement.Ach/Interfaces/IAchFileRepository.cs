﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.FinanceManagement.Ach.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IAchFileRepository : IRepository<AchFile>
    {
    }
}

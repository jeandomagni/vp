﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.FinanceManagement.Ach.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IAchReturnDetailRepository : IRepository<AchReturnDetail>
    {
        IReadOnlyList<AchReturnDetail> GetReturnDetails(DateTime startDate, DateTime endDate, bool unprocessedOnly = false);
        int GetReturnDetailsCount(AchFile achFile, bool unprocessedOnly = false);
        IReadOnlyList<AchReturnDetail> GetDetailsForFile(int achFileId);
    }
}

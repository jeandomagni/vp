﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.FinanceManagement.Ach.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IAchSettlementDetailRepository : IRepository<AchSettlementDetail>
    {
        IReadOnlyList<AchSettlementDetail> GetSettlementDetails(DateTime startDate, DateTime endDate, bool unprocessedOnly = false);
        int GetSettlementDetailsCount(AchFile achFile, bool unprocessedOnly = false);
        IReadOnlyList<AchSettlementDetail> GetDetailsForFile(int achFileId);
        IList<AchSettlementDetail> GetAchSettlementDetailsByTransactionId(string transactionId);
        IList<string> GetAchSettlementTransactionIdsWithoutFile();
    }
}

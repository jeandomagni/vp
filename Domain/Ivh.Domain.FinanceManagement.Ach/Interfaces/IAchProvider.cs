﻿namespace Ivh.Domain.FinanceManagement.Ach.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Entities;

    public interface IAchProvider
    {
        IList<Response> DownloadReturnsFiles(DateTime startDate, DateTime endDate);
        IList<Response> DownloadSettlementsFiles(DateTime startDate, DateTime endDate);
    }
}
﻿namespace Ivh.Domain.FinanceManagement.Ach.Interfaces
{
    public interface INachaGateway
    {
        string GetReturnFile(string nachaId, string nachaSecurityToken, string beginDate, string endDate, string fileFormat);
        string GetReport(string nachaId, string nachaSecurityToken, string reporttypeid, string odfiid, string resellerid, string startdate, string enddate, string datetypeid, string originationfileid, string salespersonid, string exportformat);
    }
}
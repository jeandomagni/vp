﻿namespace Ivh.Domain.FinanceManagement.Ach.Mappings
{
    using Common.Base.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class AchReturnDetailMap : ClassMap<AchReturnDetail>
    {
        public AchReturnDetailMap()
        {
            this.Schema("Ach");
            this.Table("AchReturnDetail");
            this.Id(x => x.AchReturnDetailId);
            this.References(x => x.AchFile, "AchFileId");
            this.Map(x => x.DownloadedDate).Not.Nullable();
            this.Map(x => x.ProcessedDate).Nullable();

            this.Map(x => x.CompanyId).Nullable();
            this.Map(x => x.CompanyName).Nullable();
            this.Map(x => x.SecCode).Nullable();
            this.Map(x => x.Description).Nullable();
            this.Map(x => x.EffectiveDate).Nullable();
            this.Map(x => x.IndividualId).Nullable();
            this.Map(x => x.IndividualName).Nullable();
            this.Map(x => x.RdfiRoutingNumber).Nullable();
            this.Map(x => x.RdfiAccountNumber).Nullable();
            this.Map(x => x.TransactionCode).Nullable();
            this.Map(x => x.Amount).Nullable();
            this.Map(x => x.CustomerSpecifiedTraceNumber).Nullable();
            this.Map(x => x.WebTransactionSingleOrRecurringIdentifier).Nullable();
            this.Map(x => x.ReturnReasonCode).Nullable();
            this.Map(x => x.ReturnCorrectionOrAddendaInfo).Nullable();
            this.Map(x => x.ReturnDate).Nullable();
            this.Map(x => x.ReturnTraceNumber).Nullable();
        }
    }
}
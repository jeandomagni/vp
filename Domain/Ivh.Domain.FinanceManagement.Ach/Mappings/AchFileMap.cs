﻿namespace Ivh.Domain.FinanceManagement.Ach.Mappings
{
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class AchFileMap : ClassMap<AchFile>
    {
        public AchFileMap()
        {
            this.Schema("Ach");
            this.Table("AchFile");
            this.Id(x => x.AchFileId);
            this.Map(x => x.FileDateFrom).Not.Nullable();
            this.Map(x => x.FileDateTo).Not.Nullable();
            this.Map(x => x.DownloadedDate).Not.Nullable().Generated.Always();
            this.Map(x => x.ProcessedDate).Nullable();
            this.Map(x => x.AchFileType).Column("AchFileTypeId").CustomType<AchFileTypeEnum>();
            this.Map(x => x.FileReferenceToken).Not.Nullable();
        }
    }
}
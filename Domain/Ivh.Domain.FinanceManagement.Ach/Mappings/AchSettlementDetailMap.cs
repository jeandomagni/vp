﻿namespace Ivh.Domain.FinanceManagement.Ach.Mappings
{
    using Common.Base.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class AchSettlementDetailMap : ClassMap<AchSettlementDetail>
    {
        public AchSettlementDetailMap()
        {
            this.Schema("Ach");
            this.Table("AchSettlementDetail");
            this.Id(x => x.AchSettlementDetailId);
            this.References(x => x.AchFile, "AchFileId").Nullable();
            this.Map(x => x.DownloadedDate).Not.Nullable();
            this.Map(x => x.ProcessedDate).Nullable();

            this.Map(x => x.PaidGroup).Nullable();
            this.Map(x => x.DebitAmount1).Nullable();
            this.Map(x => x.CreditAmount1).Nullable();
            this.Map(x => x.FileName).Nullable();
            this.Map(x => x.ProcessingDate).Nullable();
            this.Map(x => x.Textbox21).Nullable();
            this.Map(x => x.IndividualId).Nullable();
            this.Map(x => x.DebitAmount).Nullable();
            this.Map(x => x.CreditAmount).Nullable();
            this.Map(x => x.SecCode).Nullable();
            this.Map(x => x.Status).Nullable();
            this.Map(x => x.ReturnCode).Nullable();
            this.Map(x => x.ReturnDate).Nullable();
            this.Map(x => x.ReturnAddenda).Nullable();
            this.Map(x => x.DishonorReason).Nullable();
            this.Map(x => x.Textbox4).Nullable();
            this.Map(x => x.Textbox5).Nullable();
            this.Map(x => x.Textbox6).Nullable();
            this.Map(x => x.Textbox7).Nullable();
        }
    }
}
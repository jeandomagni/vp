﻿namespace Ivh.Domain.Guarantor.Services
{
    using System;
    using Base.Interfaces;
    using Base.Services;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Messages.Core;
    using Entities;
    using Interfaces;
    using NHibernate;

    public class GuarantorInvitationService : DomainService, IGuarantorInvitationService
    {
        private readonly Lazy<IGuarantorInvitationRepository> _guarantorInvitationRepository;
        private readonly ISession _session;

        public GuarantorInvitationService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IGuarantorInvitationRepository> guarantorInvitationRepository,
            ISessionContext<VisitPay> sessionContext) : base(serviceCommonService)
        {
            this._guarantorInvitationRepository = guarantorInvitationRepository;
            this._session = sessionContext.Session;
        }

        public void SendGuarantorInvitation(GuarantorInvitation guarantorInvitation)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                guarantorInvitation.InvitationToken = Guid.NewGuid();
                this._guarantorInvitationRepository.Value.InsertOrUpdate(guarantorInvitation);

                //queue the email.
                this._session.Transaction.RegisterPostCommitSuccessAction((gi) =>
                {
                    this.Bus.Value.PublishMessage(new SendNewUserInvitationEmailMessage()
                    {
                        FirstName = gi.FirstName,
                        Email = gi.Email,
                        Token = gi.InvitationToken
                    }).Wait();
                }, guarantorInvitation);
                unitOfWork.Commit();
            }
        }

        public GuarantorInvitation GetGuarantorInvitation(Guid invitationToken)
        {
            return this._guarantorInvitationRepository.Value.GetGuarantorInvitation(invitationToken);
        }
    }
}

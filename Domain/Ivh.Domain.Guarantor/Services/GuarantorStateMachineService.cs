﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.Guarantor.Services
{
    using Common.Base.Enums;
    using Common.EventJournal.Interfaces;
    using Common.State;
    using Common.State.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;
    using Rules;

    public class GuarantorStateMachineService : Machine<Guarantor, VpGuarantorStatusEnum>, IGuarantorStateMachineService
    {
        public GuarantorStateMachineService(
            IContextProvider<Guarantor, VpGuarantorStatusEnum> contextProvider,
            VpGuarantorStateRules rules,
            Lazy<IEventJournalService> eventJournalService)
            : base(contextProvider, rules, eventJournalService, VpGuarantorStatusEnum.NotSet)
        {
        }
    }
}

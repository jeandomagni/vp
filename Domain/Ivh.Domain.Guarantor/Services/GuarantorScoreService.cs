﻿namespace Ivh.Domain.Guarantor.Services
{
    using System;
    using Base.Interfaces;
    using Base.Services;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;

    public class GuarantorScoreService : DomainService, IGuarantorScoreService
    {
        private readonly Lazy<IVpGuarantorScoreRepository> _scoreRepository;

        public GuarantorScoreService(Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IVpGuarantorScoreRepository> scoreRepository) : base(serviceCommonService)
        {
            this._scoreRepository = scoreRepository;
        }

        public VpGuarantorScore GetScore(int vpGuarantorId, ScoreTypeEnum scoreType)
        {
            return this._scoreRepository.Value.GetScore(vpGuarantorId, scoreType);
        }

    }
}
﻿namespace Ivh.Domain.Guarantor.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using Common.VisitPay.Messages.Matching.Dtos;
    using Entities;
    using Interfaces;
    using User.Entities;

    public class GuarantorService : DomainService, IGuarantorService
    {
        private readonly Lazy<IBillingSystemRepository> _billingSystemRepository;
        private readonly Lazy<IGuarantorRepository> _guarantorRepository;
        private readonly Lazy<IHsGuarantorMapRepository> _hsGuarantorMapRepository;
        private readonly Lazy<IGuarantorStateMachineService> _guarantorStateMachineService;

        public GuarantorService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IGuarantorRepository> guarantorRepository,
            Lazy<IGuarantorStateMachineService> guarantorStateMachineService,
            Lazy<IBillingSystemRepository> billingSystemRepository,
            Lazy<IHsGuarantorMapRepository> hsGuarantorMapRepository) : base(serviceCommonService)
        {
            this._guarantorRepository = guarantorRepository;
            this._billingSystemRepository = billingSystemRepository;
            this._hsGuarantorMapRepository = hsGuarantorMapRepository;
            this._guarantorStateMachineService = guarantorStateMachineService;
        }

        public Guarantor GetGuarantor(int vpGuarantorId)
        {
            return this._guarantorRepository.Value.GetById(vpGuarantorId);
        }

        public Guarantor GetGuarantor(string visitPayUserName)
        {
            int vpGuarantorId = this._guarantorRepository.Value.GetGuarantorId(visitPayUserName);
            return this._guarantorRepository.Value.GetById(vpGuarantorId);
        }

        public Guarantor GetGuarantorEx(int visitPayUserId, int? vpGuarantorId = null)
        {
            return this._guarantorRepository.Value.GetById(vpGuarantorId ?? this.GetGuarantorId(visitPayUserId));
        }

        public Guarantor GetGuarantorBySsk(string sourceSystemKey)
        {
            Guarantor vpGuarantor = null;
            IList<HsGuarantorMap> hsGuarantorMaps = this.GetHsGuarantors(sourceSystemKey);
            if (hsGuarantorMaps != null && hsGuarantorMaps.Count == 1)
            {
                vpGuarantor = hsGuarantorMaps.First().VpGuarantor;
            }
            return vpGuarantor;
        }

        public int GetGuarantorId(int visitPayUserId)
        {
            return this._guarantorRepository.Value.GetGuarantorId(visitPayUserId);
        }

        public int GetGuarantorId(Guid visitPayUserGuid)
        {
            return this._guarantorRepository.Value.GetGuarantorId(visitPayUserGuid);
        }

        public int GetGuarantorId(string visitPayUserName)
        {
            return this._guarantorRepository.Value.GetGuarantorId(visitPayUserName);
        }

        public int GetVisitPayUserId(int vpGuarantorId)
        {
            return this._guarantorRepository.Value.GetVisitPayUserId(vpGuarantorId);
        }

        public void SetToActiveHsAcknowledgedPendingVisits(Guarantor guarantor)
        {           
            guarantor.ChangeStateToActive = guarantor.ShouldBeActive;
            this._guarantorStateMachineService.Value.Evaluate(guarantor);

            this._guarantorRepository.Value.InsertOrUpdate(guarantor);
        }

        public void SetToActiveVisitsLoaded(Guarantor guarantor)
        {
            guarantor.ChangeStateToActive = guarantor.ShouldBeActive;
            this._guarantorStateMachineService.Value.Evaluate(guarantor);

            this._guarantorRepository.Value.InsertOrUpdate(guarantor);
        }

        public void InsertMatchingHsGuarantor(HsGuarantorAcknowledgementDto acknowledgement, Guarantor guarantor)
        {
            if (guarantor.MatchedHsGuarantorMaps.Count(x => x.HsGuarantorId == acknowledgement.HsGuarantorId
                                                  && x.BillingSystem.BillingSystemId == acknowledgement.HsBillingSystemId
                                                  && x.SourceSystemKey == acknowledgement.SourceSystemKey)
                <= 0)
            {
                BillingSystem system = this._billingSystemRepository.Value.GetById(acknowledgement.HsBillingSystemId);
                if (system != null)
                {
                    guarantor.HsGuarantorMaps.Add(new HsGuarantorMap
                    {
                        HsGuarantorId = acknowledgement.HsGuarantorId,
                        VpGuarantor = guarantor,
                        BillingSystem = system,
                        SourceSystemKey = acknowledgement.SourceSystemKey
                    });
                    this._guarantorRepository.Value.InsertOrUpdate(guarantor);
                }
            }
        }
        
        public void ChangePaymentDueDay(Guarantor guarantor, int dueDay, VisitPayUser actionVisitPayUser, string changeReason)
        {
            guarantor.UpdatePaymentDueDay(dueDay, actionVisitPayUser, changeReason);
            this._guarantorRepository.Value.InsertOrUpdate(guarantor);
        }

        public void ChangeStatementDate(Guarantor guarantor, DateTime? nextStatementDate)
        {
            guarantor.NextStatementDate = nextStatementDate;
            this._guarantorRepository.Value.InsertOrUpdate(guarantor);
        }

        public void InsertOrUpdateGuarantor(Guarantor guarantor)
        {
            this._guarantorRepository.Value.InsertOrUpdate(guarantor);
        }

        public void UpdateGuarantorType(Guarantor guarantor, GuarantorTypeEnum guarantorTypeEnum)
        {
            guarantor.SetGuarantorType(guarantorTypeEnum);
            this._guarantorRepository.Value.Update(guarantor);
        }

        public IList<Guarantor> GetGuarantorsFromIntList(IList<int> vpGuarantorIds)
        {
            List<Guarantor> guarantors = new List<Guarantor>();
            if (vpGuarantorIds != null && vpGuarantorIds.Count > 0)
            {
                foreach (int vpGuarantorId in vpGuarantorIds.Distinct())
                {
                    Guarantor check = this._guarantorRepository.Value.GetById(vpGuarantorId);
                    if (check != null)
                    {
                        guarantors.Add(check);
                    }
                }
            }
            return guarantors;
        }

        public IList<Guarantor> GetGuarantors(GuarantorFilter filter)
        {
            return this._guarantorRepository.Value.GetGuarantors(filter);
        }

        public IList<Guarantor> GetGuarantors(GuarantorFilter filter, int page, int rows)
        {
            return this._guarantorRepository.Value.GetGuarantors(filter, page, rows);
        }

        public int GetGuarantorCount(GuarantorFilter filter)
        {
            return this._guarantorRepository.Value.GetGuarantorCount(filter);
        }

        public IList<int> GetActiveGuarantorsWithPendingStatement(DateTime date)
        {
            return this._guarantorRepository.Value.GetActiveGuarantorsWithPendingStatement(date).Select(x => x.VpGuarantorId).ToList();
        }
        
        public IList<HsGuarantorMap> GetHsGuarantors(int hsGuarantorId, HsGuarantorMatchStatusEnum? hsGuarantorMatchStatusEnum = null)
        {
            return this._hsGuarantorMapRepository.Value.GetGuarantors(hsGuarantorId, hsGuarantorMatchStatusEnum);
        }

        public HsGuarantorMap GetHsGuarantor(string sourceSystemKey, int billingSystemId)
        {
            return this._hsGuarantorMapRepository.Value.GetHsGuarantor(sourceSystemKey, billingSystemId);
        }

        public IList<HsGuarantorMap> GetHsGuarantors(string sourceSystemKey)
        {
            return this._hsGuarantorMapRepository.Value.GetHsGuarantors(sourceSystemKey);
        }

        public string GetGuarantorMatchSsn(int vpGuarantorId)
        {
            return this._guarantorRepository.Value.GetGuarantorMatchSsn(vpGuarantorId);
        }

        public IList<VpGuarantorCancellationReason> GetAllCancellationReasons()
        {
            return this._guarantorRepository.Value.GetAllCancellationReasons();
        }

        public bool IsGuarantorAccountClosed(int vpGuarantorId)
        {
            return this._guarantorRepository.Value.IsGuarantorAccountClosed(vpGuarantorId);
        }

        public GuarantorMatchResult IsGuarantorMatch(MatchDataDto matchData)
        {
            GuarantorMatchResult result = this._guarantorRepository.Value.IsGuarantorMatch(matchData, PatternUseEnum.Initial);

            if (result == null)
            {
                return GuarantorMatchResult.NoMatchFound;
            }

            return result;
        }

        public IList<GuarantorMatchResult> AddInitialMatches(int vpGuarantorId, MatchGuarantorDto guarantor, MatchPatientDto patient)
        {
            IList<GuarantorMatchResult> matchesAdded = this._guarantorRepository.Value.AddInitialMatches(vpGuarantorId, guarantor, patient);

            return matchesAdded;
        }
    }
}
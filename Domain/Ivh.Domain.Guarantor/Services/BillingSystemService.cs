﻿namespace Ivh.Domain.Guarantor.Services
{
    using System;
    using System.Collections.Generic;
    using Base.Interfaces;
    using Base.Services;
    using Entities;
    using Interfaces;

    public class BillingSystemService : DomainService, IBillingSystemService
    {
        private readonly IBillingSystemRepository _repository;

        public BillingSystemService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            IBillingSystemRepository repository) : base(serviceCommonService)
        {
            this._repository = repository;
        }

        public IList<BillingSystem> GetAllBillingSystemTypes(bool includeUnmatched = false)
        {
            return this._repository.GetAllBillingSystemTypes(includeUnmatched);
        }

    }
}

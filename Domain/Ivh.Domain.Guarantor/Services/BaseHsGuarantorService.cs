﻿namespace Ivh.Domain.Guarantor.Services
{
    using System;
    using System.Collections.Generic;
    using Application.Core.Common.Dtos;
    using Base.Interfaces;
    using Base.Services;
    using Entities;
    using Interfaces;

    public class BaseHsGuarantorService : DomainService, IBaseHsGuarantorService
    {
        private readonly Lazy<IBaseHsGuarantorRepository> _baseHsGuarantorRepository;

        public BaseHsGuarantorService(
            Lazy<IBaseHsGuarantorRepository> baseHsGuarantorRepository,
            Lazy<IDomainServiceCommonService> serviceCommonService
            ) : base(serviceCommonService)
        {
            this._baseHsGuarantorRepository = baseHsGuarantorRepository;
        }

        public IList<BaseHsGuarantor> GetBaseHsGuarantors(string lastName, string hsGuarantorSourceSystemKey)
        {
            IList<BaseHsGuarantor> guarantors = this._baseHsGuarantorRepository.Value.GetBaseHsGuarantors(lastName, hsGuarantorSourceSystemKey);

            return guarantors;
        }
    }
}

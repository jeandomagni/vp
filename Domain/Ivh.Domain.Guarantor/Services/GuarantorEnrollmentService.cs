﻿namespace Ivh.Domain.Guarantor.Services
{
    using System;
    using System.Threading.Tasks;
    using Base.Interfaces;
    using Base.Services;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using Interfaces;

    public class GuarantorEnrollmentService : DomainService, IGuarantorEnrollmentService
    {
        private readonly IGuarantorEnrollmentProvider _enrollmentProvider;

        public GuarantorEnrollmentService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            IGuarantorEnrollmentProvider enrollmentProvider) : base(serviceCommonService)
        {
            this._enrollmentProvider = enrollmentProvider;
        }

        public async Task<EnrollmentResponseDto> EnrollGuarantorAsync(string guarantorIdToEnroll, int billingSystemId, int vpGuarantorId)
        {
            EnrollmentResponseDto enrollmentResponseDto = await this._enrollmentProvider.EnrollGuarantorAsync(guarantorIdToEnroll, billingSystemId, vpGuarantorId);
            if (enrollmentResponseDto.EnrollmentResult == EnrollmentResultEnum.Error || enrollmentResponseDto.EnrollmentResult == EnrollmentResultEnum.Invalid)
            {
                throw new Exception($"An error occurred while enrolling guarantor. Response:{enrollmentResponseDto.RawResponse}");
            }
            return enrollmentResponseDto;
        }

        public async Task<bool> UnEnrollGuarantorAsync(string guarantorIdToEnroll)
        {
            EnrollmentResponseDto enrollmentResponseDto = await this._enrollmentProvider.UnEnrollGuarantorAsync(guarantorIdToEnroll);
            if (enrollmentResponseDto.EnrollmentResult == EnrollmentResultEnum.Error || enrollmentResponseDto.EnrollmentResult == EnrollmentResultEnum.Invalid)
            {
                throw new Exception($"An error occurred while unenrolling guarantor. Response:{enrollmentResponseDto.RawResponse}");
            }
            return true;
        }

        public async Task PublishEnrollGuarantorMessageAsync(int vpGuarantorId)
        {
            await this._enrollmentProvider.PublishEnrollGuarantorMessageAsync(vpGuarantorId);
        }

        public async Task PublishUnEnrollGuarantorMessageAsync(int vpGuarantorId)
        {
            await this._enrollmentProvider.PublishUnEnrollGuarantorMessageAsync(vpGuarantorId);
        }
    }
}
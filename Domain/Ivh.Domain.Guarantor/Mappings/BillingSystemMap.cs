namespace Ivh.Domain.Guarantor.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class BillingSystemMap : ClassMap<BillingSystem>
    {
        public BillingSystemMap()
        {
            this.Schema("dbo");
            this.Table("BillingSystem");
            this.ReadOnly();
            this.Id(x => x.BillingSystemId);
            this.Map(x => x.BillingSystemName).Not.Nullable();
            this.Cache.ReadOnly();
        }
    }
}
﻿namespace Ivh.Domain.Guarantor.Mappings
{
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class HsGuarantorMapMapping : ClassMap<HsGuarantorMap>
    {
        public HsGuarantorMapMapping()
        {
            this.Table("HsGuarantorMap");
            this.Schema("dbo");
            this.Id(x => x.HsGuarantorMapId);
            this.Map(x => x.InsertDate);
            this.Map(x => x.HsGuarantorId);
            this.References(x => x.VpGuarantor).Column("VpGuarantorId");
            this.Map(x => x.SourceSystemKey);
            this.References(x => x.BillingSystem).Column("BillingSystemId").Not.LazyLoad();
            this.Map(x => x.SSN4);
            this.Map(x => x.HsGuarantorMatchStatus, "HsGuarantorMatchStatusId")
                .CustomType<HsGuarantorMatchStatusEnum>()
                .Not.Nullable();
            this.Map(x => x.VpEligible).Nullable();
            this.Map(x => x.GenderId).Nullable();
            this.Map(x => x.MatchOption).CustomType<MatchOptionEnum?>().Column("MatchOptionId").Nullable();
        }
    }
}
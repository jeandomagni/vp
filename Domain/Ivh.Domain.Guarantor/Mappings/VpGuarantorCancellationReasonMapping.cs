﻿namespace Ivh.Domain.Guarantor.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class VpGuarantorCancellationReasonMapping : ClassMap<VpGuarantorCancellationReason>
    {
        public VpGuarantorCancellationReasonMapping()
        {
            this.Table("VpGuarantorCancellationReason");
            this.Schema("dbo");
            this.Id(x => x.VpGuarantorCancellationReasonId).Column("VpGuarantorCancellationReasonId");
            this.Map(x => x.Name).Column("Name").Not.Nullable();
            this.Map(x => x.DisplayName).Column("DisplayName").Not.Nullable();
            this.Map(x => x.DisplaySortOrder).Column("DisplaySortOrder").Not.Nullable();
            this.Map(x => x.Selectable).Column("Selectable").Not.Nullable();
        }
    }
}

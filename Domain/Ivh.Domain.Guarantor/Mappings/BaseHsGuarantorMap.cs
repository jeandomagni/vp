﻿namespace Ivh.Domain.Guarantor.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class BaseHsGuarantorMap : ClassMap<BaseHsGuarantor>
    {
        public BaseHsGuarantorMap()
        {
            this.Polymorphism.Explicit();
            this.ReadOnly();

            this.Schema(Schemas.VisitPay.Base);
            this.Table("HsGuarantor");

            this.Id(x => x.HsGuarantorId);
            this.Map(x => x.HsBillingSystemId);
            this.Map(x => x.SourceSystemKey);
            this.Map(x => x.FirstName);
            this.Map(x => x.LastName);
            this.Map(x => x.DOB);
            this.Map(x => x.SSN);
            this.Map(x => x.SSN4);
            this.Map(x => x.AddressLine1);
            this.Map(x => x.AddressLine2);
            this.Map(x => x.City);
            this.Map(x => x.StateProvince);
            this.Map(x => x.PostalCode);
            this.Map(x => x.Country);
            this.Map(x => x.DataChangeId);
        }
    }
}

namespace Ivh.Domain.Guarantor.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class VpGuarantorScoreMap : ClassMap<VpGuarantorScore>
    {
        public VpGuarantorScoreMap()
        {
            this.Schema("scoring");
            this.Table("VpGuarantorScore");
            this.Id(x => x.VpGuarantorScoreId);
            this.Map(x => x.VpGuarantorId).Not.Nullable();
            this.Map(x => x.Score).Not.Nullable();
            this.Map(x => x.ScoreTypeId).Not.Nullable();
            this.Map(x => x.ScoreCreationDate).Not.Nullable();
        }
    }
}
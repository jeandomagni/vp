﻿namespace Ivh.Domain.Guarantor.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class ConsolidationGuarantorMapping : ClassMap<ConsolidationGuarantor>
    {
        public ConsolidationGuarantorMapping()
        {
            this.Table("ConsolidationGuarantor");
            this.Schema("dbo");
            this.Id(x => x.ConsolidationGuarantorId).Column("ConsolidationGuarantorId");
            this.References(x => x.InitiatedByUser).Column("InitiatedByUserId").Nullable();
            this.References(x => x.ManagedGuarantor).Column("ManagedGuarantorId").Not.Nullable();
            this.References(x => x.ManagingGuarantor).Column("ManagingGuarantorId").Not.Nullable();
            this.Map(x => x.ConsolidationGuarantorStatus).Column("ConsolidationGuarantorStatusId").CustomType<ConsolidationGuarantorStatusEnum>().Not.Nullable();
            this.Map(x => x.InsertDate);
            this.References(x => x.AcceptedByManagedGuarantorCmsVersion).Column("AcceptedByManagedGuarantorCmsVersionId").Nullable();
            this.References(x => x.AcceptedByManagingGuarantorCmsVersion).Column("AcceptedByManagingGuarantorCmsVersionId").Nullable();
            this.References(x => x.AcceptedByManagdGuarantorHipaaCmsVersion).Column("AcceptedByManagedGuarantorHipaaCmsVersionId").Nullable();
            this.References(x => x.AcceptedByManagingGuarantorFpCmsVersion).Column("AcceptedByManagingGuarantorFpCmsVersionId").Nullable();
            this.Map(x => x.DateAcceptedByManagedGuarantor).Nullable();
            this.Map(x => x.DateAcceptedByManagingGuarantor).Nullable();
            this.Map(x => x.DateAcceptedFinancePlanTermsByManagingGuarantor).Nullable();
            this.References(x => x.CancelledByUser).Column("CancelledByUserId").Nullable();
            this.Map(x => x.ConsolidationGuarantorCancellationReason).Column("ConsolidationGuarantorCancellationReasonId").CustomType<ConsolidationGuarantorCancellationReasonEnum>().Nullable();
            this.Map(x => x.CancelledOn).Nullable();
            this.References(x => x.CancelledByManagedGuarantorCmsVersion).Column("CancelledByManagedGuarantorCmsVersionId").Nullable();
            this.References(x => x.CancelledByManagingGuarantorCmsVersion).Column("CancelledByManagingGuarantorCmsVersionId").Nullable();
            this.References(x => x.CancelledByManagedGuarantorFpCmsVersion).Column("CancelledByManagedGuarantorFpCmsVersionId").Nullable();
        }
    }
}
﻿namespace Ivh.Domain.Guarantor.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class VpGuarantorCancellationMapping : ClassMap<VpGuarantorCancellation>
    {
        public VpGuarantorCancellationMapping()
        {
            this.Table("VpGuarantorCancellation");
            this.Id(x => x.VpGuarantorCancellationId).Column("VpGuarantorCancellationId").GeneratedBy.Identity();
            this.References(x => x.VpGuarantor).Column("VpGuarantorId")
                .Not.Nullable()
                .Cascade.None();
            this.References(x => x.VisitPayUser).Column("InsertVisitPayUserId")
                .Not.Nullable()
                .Cascade.None();
            this.References(x => x.VpGuarantorCancellationReason).Column("VpGuarantorCancellationReasonId")
                .Not.Nullable()
                .Cascade.None();
            this.Map(x => x.InsertDate).Column("InsertDate");
            this.Map(x => x.Notes).Column("Notes");
        }
    }
}

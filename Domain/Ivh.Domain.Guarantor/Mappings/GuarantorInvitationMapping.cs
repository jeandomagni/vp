﻿namespace Ivh.Domain.Guarantor.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class GuarantorInvitationMapping : ClassMap<GuarantorInvitation>
    {
        public GuarantorInvitationMapping()
        {
            this.Table("VpGuarantorInvitation");
            this.Schema("dbo");
            this.Id(x => x.VisitPayGuarantorInvitationId).Column("VpGuarantorInvitationId");
            this.Map(x => x.Email).Not.Nullable();
            this.Map(x => x.FirstName).Not.Nullable();
            this.Map(x => x.MiddleName).Nullable();
            this.Map(x => x.LastName).Not.Nullable();
            this.Map(x => x.VisitPayUserId).Not.Nullable();
            this.Map(x => x.InvitationToken).Not.Nullable();
            this.Map(x => x.InsertDate); 
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.Guarantor.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class VpGuarantorStateHistoryMap : ClassMap<VpGuarantorStateHistory>
    {
        public VpGuarantorStateHistoryMap()
        {
            this.Schema("dbo");
            this.Table("VpGuarantorStateHistory");
            this.Id(x => x.HistoryId).Column("VpGuarantorStateHistoryId");
            this.References<Guarantor>(x => x.Entity).Column("VpGuarantorId").Not.Nullable();
            this.Map(x => x.InitialState).Column("InitialStateId").CustomType<VpGuarantorStatusEnum>().Not.Nullable();
            this.Map(x => x.EvaluatedState).Column("EvaluatedStateId").CustomType<VpGuarantorStatusEnum>().Not.Nullable();
            this.Map(x => x.DateTime).Generated.Always();
            this.Map(x => x.RuleName).Nullable();
        }
    }
}
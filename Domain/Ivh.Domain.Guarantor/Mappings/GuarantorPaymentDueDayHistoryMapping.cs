﻿namespace Ivh.Domain.Guarantor.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class GuarantorPaymentDueDayHistoryMapping : ClassMap<GuarantorPaymentDueDayHistory>
    {
        public GuarantorPaymentDueDayHistoryMapping()
        {
            this.Table("VpGuarantorPaymentDueDayHistory");
            this.Schema("dbo");
            this.Id(x => x.VpGuarantorPaymentDueDayHistoryId);
            this.Map(x => x.PaymentDueDay).Not.Nullable();
            this.Map(x => x.OldPaymentDueDay).Not.Nullable();
            this.Map(x => x.InsertDate);
            this.References(x => x.Guarantor).Column("VpGuarantorId").Not.Nullable();
            this.References(x => x.InsertUser).Column("InsertVisitPayUserId").Not.Nullable();
            this.Map(x => x.ChangeReason).Nullable();
        }
    }
}
﻿namespace Ivh.Domain.Guarantor.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class GuarantorMapping : ClassMap<Guarantor>
    {
        public GuarantorMapping()
        {
            this.Table("VpGuarantor");
            this.Schema("dbo");
            this.Id(x => x.VpGuarantorId);
            //Todo: Need distribute cache
            //this.Cache.ReadWrite();

            this.Map(x => x.TermsOfUseCmsVersionId).Not.Nullable();
            this.Map(x => x.PaymentDueDay).Not.Nullable();
            this.Map(x => x.NextStatementDate);
            this.Map(x => x.RegistrationDate).Not.Nullable();
            this.Map(x => x.AccountClosedDate);
            this.Map(x => x.AccountCancellationDate);
            this.Map(x => x.AccountReactivationDate);
            this.Map(x => x.MostRecentVpStatementId).Nullable();
            this.Map(x => x.UseAutoPay);
            this.Map(x => x.VpGuarantorStatus)
                .Column("VpGuarantorStatusId")
                .CustomType<VpGuarantorStatusEnum>()
                .Not.Nullable();

            this.HasMany(x => x.VpGuarantorStateHistories)
                .KeyColumn("VpGuarantorId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();

            this.Map(x => x.VpGuarantorTypeEnum)
                .Column("VpGuarantorTypeId").CustomType<GuarantorTypeEnum>()
                .Not.Nullable();

            this.References(x => x.User).Column("VisitPayUserId")
                .Not.LazyLoad()
                .Fetch.Join();

            this.HasMany(x => x.HsGuarantorMaps).KeyColumn("VpGuarantorId")
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.All();

            this.HasMany(x => x.PaymentDueDayHistories)
                .BatchSize(50)
                .KeyColumn("VpGuarantorId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .Cascade.AllDeleteOrphan();

            this.HasMany(x => x.ManagedConsolidationGuarantors)
                .ReadOnly()
                .KeyColumn("ManagingGuarantorId");

            this.HasMany(x => x.ManagingConsolidationGuarantors)
                .ReadOnly()
                .KeyColumn("ManagedGuarantorId");

            this.HasMany(x => x.Cancellations)
                .KeyColumn("VpGuarantorId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                //.Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();
        }
    }
}
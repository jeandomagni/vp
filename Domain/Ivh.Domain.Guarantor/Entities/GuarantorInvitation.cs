﻿    namespace Ivh.Domain.Guarantor.Entities
{
    using System;
    using Common.Base.Utilities.Helpers;

    public class GuarantorInvitation
    {
        public GuarantorInvitation()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        
        public virtual int VisitPayGuarantorInvitationId { get; set; }
        public virtual string Email{ get; set; }
        public virtual string FirstName { get; set; }
        public virtual string MiddleName { get; set; }
        public virtual string LastName { get; set; }
        public virtual int VisitPayUserId { get; set; }
        public virtual Guid InvitationToken{ get; set; }
        public virtual Guarantor VisitPayGuarantor { get; set; }
        public virtual DateTime InsertDate{ get; set; }
        public virtual string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);
        public virtual string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);
    }
}
﻿namespace Ivh.Domain.Guarantor.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Utilities.Extensions;
    using Common.State.Interfaces;
    using Common.VisitPay.Enums;
    using User.Entities;

    public class Guarantor : IEntity<VpGuarantorStatusEnum>
    {
        public Guarantor()
        {
            this.HsGuarantorMaps = new List<HsGuarantorMap>();
            this.PaymentDueDayHistories = new List<GuarantorPaymentDueDayHistory>();
            this.ManagingConsolidationGuarantors = new List<ConsolidationGuarantor>();
            this.ManagedConsolidationGuarantors = new List<ConsolidationGuarantor>();
            this.Cancellations = new List<VpGuarantorCancellation>();
            this.VpGuarantorStateHistories = new List<VpGuarantorStateHistory>();
        }

        public virtual int VpGuarantorId { get; set; }
        public virtual VisitPayUser User { get; set; }
        public virtual int TermsOfUseCmsVersionId { get; set; }
        public virtual int PaymentDueDay { get; set; }
        public virtual DateTime? NextStatementDate { get; set; }
        public virtual DateTime RegistrationDate { get; set; }

        public virtual GuarantorTypeEnum VpGuarantorTypeEnum { get; protected set; }
        public virtual void SetGuarantorType(GuarantorTypeEnum guarantorTypeEnum)
        {
            if (this.VpGuarantorTypeEnum != guarantorTypeEnum)
            {
                this.UnpublishedGuarantorChangedEvent = new GuarantorChangedEvent
                {
                    Guarantor = this,
                    EventDate = DateTime.UtcNow
                };

                this.VpGuarantorTypeEnum = guarantorTypeEnum;
            }
        }

        public virtual IList<HsGuarantorMap> HsGuarantorMaps { get; set; }
        public virtual IList<VpGuarantorCancellation> Cancellations { get; set; }
        public virtual GuarantorChangedEvent UnpublishedGuarantorChangedEvent { get; set; }


        public virtual int? MostRecentVpStatementId { get; set; }

        public virtual IList<HsGuarantorMap> MatchedHsGuarantorMaps
        {
            get { return (this.HsGuarantorMaps ?? new List<HsGuarantorMap>()).Where(x => x.HsGuarantorMatchStatus.IsInCategory(HsGuarantorMatchStatusEnumCategory.Matched)).ToList(); }
        }

        public virtual string StatementAccountNumber
        {
            get
            {
                return this.MatchedHsGuarantorMaps.OrderBy(m => m.InsertDate).FirstOrDefault()?.SourceSystemKey;
            }
        }

        public virtual IList<ConsolidationGuarantor> ManagingConsolidationGuarantors { get; set; }
        
        public virtual IList<ConsolidationGuarantor> ActiveManagingConsolidationGuarantors
        {
            get { return (this.ManagingConsolidationGuarantors ?? new List<ConsolidationGuarantor>()).Where(x => x.IsActive()).ToList(); }
        }

        public virtual int? ManagingGuarantorId
        {
            get
            {
                ConsolidationGuarantor guarantor = this.ManagingConsolidationGuarantors.FirstOrDefault(x => x.IsPendingOrActive());
                if (guarantor != null)
                {
                    return guarantor.ManagingGuarantor.VpGuarantorId;
                }
                return null;
            }
        }

        public virtual GuarantorConsolidationStatusEnum ConsolidationStatus
        {
            get
            {
                if (this.ManagingConsolidationGuarantors.Any(x => x.IsActive()))
                {
                    return GuarantorConsolidationStatusEnum.Managed;
                }
                if (this.ManagedConsolidationGuarantors.Any(x => x.IsActive()))
                {
                    return GuarantorConsolidationStatusEnum.Managing;
                }
                if (this.ManagingConsolidationGuarantors.Any(x => x.IsPending()))
                {
                    return GuarantorConsolidationStatusEnum.PendingManaged;
                }
                if (this.ManagedConsolidationGuarantors.Any(x => x.IsPending()))
                {
                    return GuarantorConsolidationStatusEnum.PendingManaging;
                }

                return GuarantorConsolidationStatusEnum.NotConsolidated;
            }
        }


        // UseAutoPay is assumed true unless set to non-autopay during finance plan creation
        public virtual bool UseAutoPay { get; set; } = true;

        public virtual bool IsConsolidated()
        {
            return this.ConsolidationStatus == GuarantorConsolidationStatusEnum.Managing || this.ConsolidationStatus == GuarantorConsolidationStatusEnum.Managed;
        }

        public virtual bool IsManaged()
        {
            return this.ConsolidationStatus == GuarantorConsolidationStatusEnum.Managed;
        }

        public virtual bool IsPendingManaged()
        {
            return this.ConsolidationStatus == GuarantorConsolidationStatusEnum.PendingManaged;
        }

        public virtual bool IsManagedOrPendingManaged()
        {
            return this.IsManaged() || this.IsPendingManaged();
        }

        public virtual bool IsManaging()
        {
            return this.ConsolidationStatus == GuarantorConsolidationStatusEnum.Managing;
        }

        public virtual bool IsPendingManaging()
        {
            return this.ConsolidationStatus == GuarantorConsolidationStatusEnum.PendingManaging;
        }

        public virtual bool IsManagingOrPendingManaging()
        {
            return this.IsManaging() || this.IsPendingManaging();
        }

        public virtual bool IsOfflineGuarantor()
        {
            return this.VpGuarantorTypeEnum == GuarantorTypeEnum.Offline;
        }

        public virtual bool WasConsolidationAcceptedBetween(DateTime periodStart, DateTime periodEnd)
        {
            List<DateTime> accepts = this.ManagingConsolidationGuarantors.Where(x => x.DateAcceptedByManagedGuarantor.HasValue &&
                                                                                               x.DateAcceptedByManagingGuarantor.HasValue &&
                                                                                               x.ConsolidationGuarantorStatus == ConsolidationGuarantorStatusEnum.Accepted)
                                                                                               .Select(x => new[] { x.DateAcceptedByManagedGuarantor.Value, x.DateAcceptedByManagingGuarantor.Value }.Max())
                                                                                               .ToList();

            return accepts.Any(initiationDate => periodStart <= initiationDate && periodEnd >= initiationDate);
        }

        public virtual bool WasConsolidationCancelledBetween(DateTime periodStart, DateTime periodEnd)
        {
            List<DateTime> cancellations = this.ManagingConsolidationGuarantors.Where(x => x.DateAcceptedByManagedGuarantor.HasValue &&
                                                                                                 x.DateAcceptedByManagingGuarantor.HasValue &&
                                                                                                 x.IsInactive() &&
                                                                                                 x.CancelledOn.HasValue).Select(x => x.CancelledOn.Value).ToList();

            return cancellations.Any(cancellationDate => periodStart <= cancellationDate && periodEnd >= cancellationDate);
        }

        public virtual IList<ConsolidationGuarantor> ManagedConsolidationGuarantors { get; set; }

        public virtual IList<ConsolidationGuarantor> ActiveManagedConsolidationGuarantors
        {
            get { return (this.ManagedConsolidationGuarantors ?? new List<ConsolidationGuarantor>()).Where(x => x.IsActive()).ToList(); }
        }

        public virtual IList<int> ManagedGuarantorIds
        {
            get { return this.ManagedConsolidationGuarantors.Where(x => x.IsPendingOrActive()).Select(x => x.ManagedGuarantor.VpGuarantorId).ToList(); }
        }

        public virtual DateTime? AccountClosedDate { get; set; }
        public virtual DateTime? AccountCancellationDate { get; set; }

        public virtual DateTime? AccountReactivationDate { get; set; }
        public virtual IList<GuarantorPaymentDueDayHistory> PaymentDueDayHistories { get; set; }

        public virtual bool IsClosed => this.AccountClosedDate.HasValue;

        public virtual int PaymentDueDayChanges
        {
            get { return this.PaymentDueDayHistories.Count; }
        }

        public virtual bool CanClose()
        {
            return this.VpGuarantorStatus == VpGuarantorStatusEnum.Active;
        }
        
        public virtual void CloseAccount()
        {
            DateTime now = DateTime.UtcNow;

            this.AccountReactivationDate = null;
            this.AccountCancellationDate = now;
            this.AccountClosedDate = now;
            this.User.LockoutReasonEnum = LockoutReasonEnum.AccountCancellation;
        }
        
        public virtual void ReactivateAccountOn(DateTime reactivateAccountOn)
        {
            this.AccountReactivationDate = reactivateAccountOn;
            this.AccountClosedDate = null;
            this.AccountCancellationDate = null;
        }

        public virtual void UpdatePaymentDueDay(int paymentDueDay, VisitPayUser actionVisitPayUser, string changeReason = null)
        {
            this.PaymentDueDayHistories.Add(new GuarantorPaymentDueDayHistory
            {
                Guarantor = this,
                InsertUser = actionVisitPayUser,
                PaymentDueDay = paymentDueDay,
                OldPaymentDueDay = this.PaymentDueDay,
                ChangeReason = changeReason
            });
            this.PaymentDueDay = paymentDueDay;
        }

        #region State Machine

        private VpGuarantorStatusEnum _vpGuarantorStatus = VpGuarantorStatusEnum.NotSet;

        public virtual VpGuarantorStatusEnum VpGuarantorStatus
        {
            get => this._vpGuarantorStatus;
            set { this._vpGuarantorStatus = value; }
        }

        public virtual bool ShouldBeActive => !this.IsClosed /*&& this.VpGuarantorTypeEnum != GuarantorTypeEnum.Offline*/;
        public virtual bool ChangeStateToActive { get; set; }
        public virtual bool ChangeStateToClosed { get; set; }

        public virtual IList<VpGuarantorStateHistory> VpGuarantorStateHistories { get; set; }

        private VpGuarantorStateHistory CurrentVpGuarantorStateHistory
        {
            get { return this?.VpGuarantorStateHistories.OrderByDescending(x => x.DateTime).FirstOrDefault(); }
        }

        VpGuarantorStatusEnum IEntity<VpGuarantorStatusEnum>.GetState()
        {
            return this.VpGuarantorStatus;
        }

        void IEntity<VpGuarantorStatusEnum>.SetState(VpGuarantorStatusEnum resultingState, string ruleName)
        {
            bool hasNewHistory = this.CurrentVpGuarantorStateHistory?.HistoryId == default(int);

            if (hasNewHistory)
            {
                VpGuarantorStateHistory newHistory = this.CurrentVpGuarantorStateHistory;
                newHistory.InitialState = this.VpGuarantorStatus;
                newHistory.EvaluatedState = resultingState;
                newHistory.RuleName = ruleName;
                newHistory.DateTime = DateTime.UtcNow;
            }
            else
            {
                VpGuarantorStateHistory vpGuarantorStateHistory = new VpGuarantorStateHistory
                {
                    Entity = this,
                    InitialState = this.VpGuarantorStatus,
                    EvaluatedState = resultingState,
                    RuleName = ruleName,
                    DateTime = DateTime.UtcNow
                };

                this.VpGuarantorStateHistories.Add(vpGuarantorStateHistory);
            }

            if (this.VpGuarantorStatus != resultingState)
            {
                this.UnpublishedGuarantorChangedEvent = new GuarantorChangedEvent
                {
                    Guarantor = this,
                    EventDate = DateTime.UtcNow
                };
            }

            this._vpGuarantorStatus = resultingState;
        }

        IHistory<IEntity<VpGuarantorStatusEnum>, VpGuarantorStatusEnum> IEntity<VpGuarantorStatusEnum>.GetCurrentStateHistory()
        {
            return this.CurrentVpGuarantorStateHistory;
        }

        #endregion

    }
}
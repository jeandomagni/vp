﻿namespace Ivh.Domain.Guarantor.Entities
{
    public class VpGuarantorCancellationReason
    {
        public virtual int VpGuarantorCancellationReasonId { get; set; }
        public virtual string Name { get; set; }
        public virtual string DisplayName { get; set; }
        public virtual int DisplaySortOrder { get; set; }
        public virtual bool Selectable { get; set; }
    }
}

﻿namespace Ivh.Domain.Guarantor.Entities
{
    using System;
    using Common.Base.Enums;
    using Common.Base.Utilities.Helpers;
    using Common.VisitPay.Enums;
    using Ivh.Domain.Content.Entities;
    using User.Entities;

    public class ConsolidationGuarantor
    {
        public ConsolidationGuarantor()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int ConsolidationGuarantorId { get; set; }

        public virtual VisitPayUser InitiatedByUser { get; set; }

        public virtual Guarantor ManagedGuarantor { get; set; }

        public virtual Guarantor ManagingGuarantor { get; set; }

        public virtual ConsolidationGuarantorStatusEnum ConsolidationGuarantorStatus { get; set; }

        public virtual DateTime InsertDate { get; set; }

        public virtual CmsVersion AcceptedByManagedGuarantorCmsVersion { get; set; }

        public virtual CmsVersion AcceptedByManagingGuarantorCmsVersion { get; set; }

        public virtual CmsVersion AcceptedByManagdGuarantorHipaaCmsVersion { get; set; }

        public virtual CmsVersion AcceptedByManagingGuarantorFpCmsVersion { get; set; }

        public virtual DateTime? DateAcceptedByManagedGuarantor { get; set; }

        public virtual DateTime? DateAcceptedByManagingGuarantor { get; set; }

        public virtual DateTime? DateAcceptedFinancePlanTermsByManagingGuarantor { get; set; }

        public virtual VisitPayUser CancelledByUser { get; set; }

        public virtual DateTime? CancelledOn { get; set; }

        public virtual ConsolidationGuarantorCancellationReasonEnum? ConsolidationGuarantorCancellationReason { get; set; }

        public virtual CmsVersion CancelledByManagedGuarantorCmsVersion { get; set; }

        public virtual CmsVersion CancelledByManagingGuarantorCmsVersion { get; set; }

        public virtual CmsVersion CancelledByManagedGuarantorFpCmsVersion { get; set; }

        public virtual bool IsPendingOrActive()
        {
            return this.IsActive() || this.IsPending();
        }

        public virtual bool IsActive()
        {
            return this.ConsolidationGuarantorStatus == ConsolidationGuarantorStatusEnum.Accepted;
        }

        public virtual bool IsPending()
        {
            return this.ConsolidationGuarantorStatus == ConsolidationGuarantorStatusEnum.Pending
                   || this.ConsolidationGuarantorStatus == ConsolidationGuarantorStatusEnum.PendingFinancePlan;
        }

        public virtual bool IsInactive()
        {
            return this.ConsolidationGuarantorStatus == ConsolidationGuarantorStatusEnum.Cancelled
                   || this.ConsolidationGuarantorStatus == ConsolidationGuarantorStatusEnum.Rejected;
        }

        public virtual DateTime MostRecentActionDate
        {
            get
            {
                return DateTimeHelper.Max(
                    this.InsertDate,
                    this.DateAcceptedByManagedGuarantor,
                    this.DateAcceptedByManagingGuarantor).Value;
            }
        }
    }
}
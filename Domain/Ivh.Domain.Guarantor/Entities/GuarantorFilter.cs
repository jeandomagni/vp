﻿namespace Ivh.Domain.Guarantor.Entities
{
    using System;

    public class GuarantorFilter
    {
        public string HsGuarantorId { get; set; }
        public int? VpGuarantorId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Ssn4 { get; set; }
        public DateTime? DOB { get; set; }
        public string UserNameOrEmailAddress { get; set; }
        public string SortField { get; set; }
        public string SortOrder { get; set; }

        public string PhoneNumber { get; set; }
        public string AddressStreet1 { get; set; }
        public string AddressStreet2 { get; set; }
        public string City { get; set; }
        public string StateProvince { get; set; }
        public string PostalCode { get; set; }
        public decimal? AmountDue { get; set; }

        public bool HasAddressFilters => !string.IsNullOrEmpty(this.AddressStreet1)
                                            || !string.IsNullOrEmpty(this.AddressStreet2)
                                            || !string.IsNullOrEmpty(this.City)
                                            || !string.IsNullOrEmpty(this.StateProvince)
                                            || !string.IsNullOrEmpty(this.PostalCode);
    }
}

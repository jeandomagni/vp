﻿namespace Ivh.Domain.Guarantor.Entities
{
    using System;
    using User.Entities;

    public class VpGuarantorCancellation
    {
        public VpGuarantorCancellation()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int VpGuarantorCancellationId { get; set; }
        public virtual Guarantor VpGuarantor { get; set; }
        public virtual VisitPayUser VisitPayUser { get; set; }
        public virtual VpGuarantorCancellationReason VpGuarantorCancellationReason { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual string Notes { get; set; }
    }
}

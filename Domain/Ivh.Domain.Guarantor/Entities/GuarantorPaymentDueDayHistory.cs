﻿namespace Ivh.Domain.Guarantor.Entities
{
    using System;
    using User.Entities;

    public class GuarantorPaymentDueDayHistory
    {
        public GuarantorPaymentDueDayHistory()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int VpGuarantorPaymentDueDayHistoryId { get; set; }
        public virtual Guarantor Guarantor { get; set; }
        public virtual int PaymentDueDay { get; set; }
        public virtual int OldPaymentDueDay { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual VisitPayUser InsertUser { get; set; }

        public virtual string ChangeReason { get; set; }
    }
}
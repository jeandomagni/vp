﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.Guarantor.Entities
{
    using Common.Base.Enums;
    using Common.State.Interfaces;
    using Common.VisitPay.Enums;

    public class VpGuarantorStateHistory : IHistory<IEntity<VpGuarantorStatusEnum>, VpGuarantorStatusEnum>
    {
        public virtual int HistoryId { get; set; }
        public virtual IEntity<VpGuarantorStatusEnum> Entity { get; set; }
        public virtual VpGuarantorStatusEnum InitialState { get; set; }
        public virtual VpGuarantorStatusEnum EvaluatedState { get; set; }
        public virtual DateTime DateTime { get; set; }
        public virtual string RuleName { get; set; }
    }
}

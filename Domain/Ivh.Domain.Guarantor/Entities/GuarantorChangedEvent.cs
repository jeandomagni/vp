﻿namespace Ivh.Domain.Guarantor.Entities
{
    using System;

    public class GuarantorChangedEvent 
    {
        public Guarantor Guarantor { get; set; }
        public DateTime EventDate { get; set; }
    }
}

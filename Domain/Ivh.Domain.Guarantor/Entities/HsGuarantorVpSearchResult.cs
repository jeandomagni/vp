﻿namespace Ivh.Domain.Guarantor.Entities
{
    using System;
    using Common.Base.Utilities.Helpers;

    public class HsGuarantorVpSearchResult
    {
        public virtual string AddressLine1 { get; set; }
        public virtual string AddressLine2 { get; set; }
        public virtual int HsGuarantorId { get; set; }
        public virtual int HsBillingSystemId { get; set; }
        public virtual string SourceSystemKey { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual DateTime? DOB { get; set; }
        public virtual string Ssn4 { get; set; }
        public virtual string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);
        public virtual string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);
    }
}

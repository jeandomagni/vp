﻿namespace Ivh.Domain.Guarantor.Entities
{
    public class IssueResult
    {
        // public virtual string Id { get; set; }
        public virtual string IssueLevel { get; set; }
        public virtual string IssueMessage { get; set; }
    }

}

﻿namespace Ivh.Domain.Guarantor.Entities
{
    using System;
    using Common.VisitPay.Enums;

    public class HsGuarantorMap
    {
        public HsGuarantorMap()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        public virtual int HsGuarantorMapId { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual int HsGuarantorId { get; set; }
        public virtual Guarantor VpGuarantor { get; set; }
        public virtual string SourceSystemKey { get; set; }
        public virtual BillingSystem BillingSystem { get; set; }
        public virtual int BillingSystemId { get; set; }
        public virtual string SSN4 { get; set; }
        public virtual HsGuarantorMatchStatusEnum HsGuarantorMatchStatus { get; set; }
        public virtual bool VpEligible { get; set; }
        public virtual int? GenderId { get; set; }
        public virtual MatchOptionEnum? MatchOption { get; set; }
    }
}
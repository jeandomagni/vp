﻿namespace Ivh.Domain.Guarantor.Entities
{
    using System;
    using Application.Base.Common.Interfaces.Entities.HsGuarantor;
    using Common.VisitPay.Attributes;

    /// <summary>
    /// readonly copy of HS Guarantor data
    /// </summary>
    public class BaseHsGuarantor : IHsGuarantor
    {
        public virtual int HsGuarantorId { get; set; }
        public virtual int HsBillingSystemId { get; set; }
        public virtual string SourceSystemKey { get; set; }
        [GuarantorMatchField("FirstName")]
        public virtual string FirstName { get; set; }
        [GuarantorMatchField("LastName")]
        public virtual string LastName { get; set; }
        [GuarantorMatchField("DOB")]
        public virtual DateTime? DOB { get; set; }
        [GuarantorMatchField("SSN")]
        public virtual string SSN { get; set; }
        [GuarantorMatchField("SSN4")]
        public virtual string SSN4 { get; set; }
        [GuarantorMatchField("AddressLine1")]
        public virtual string AddressLine1 { get; set; }
        [GuarantorMatchField("AddressLine2")]
        public virtual string AddressLine2 { get; set; }
        [GuarantorMatchField("City")]
        public virtual string City { get; set; }
        [GuarantorMatchField("StateProvince")]
        public virtual string StateProvince { get; set; }
        [GuarantorMatchField("PostalCode")]
        public virtual string PostalCode { get; set; }
        [GuarantorMatchField("Country")]
        public virtual string Country { get; set; }
        public virtual DateTime? DataChangeDate { get; set; }
        public virtual int? DataChangeId { get; set; }
    }
}

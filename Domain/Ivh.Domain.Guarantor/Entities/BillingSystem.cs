﻿namespace Ivh.Domain.Guarantor.Entities
{
    public class BillingSystem
    {
        public virtual int BillingSystemId { get; set; }
        public virtual string BillingSystemName { get; set; }
    }
}
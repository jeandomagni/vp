﻿namespace Ivh.Domain.Guarantor.Entities
{
    using System;
    using Common.Base.Utilities.Helpers;

    public class HsGuarantorDetails
    {
        public int HsGuarantorId { get; set; }
        public int HsBillingSystemId { get; set; }
        public string SourceSystemKey { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DOB { get; set; }
        public string SSN4 { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string StateProvince { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public virtual string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);
        public virtual string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);
    }
}
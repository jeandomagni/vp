﻿namespace Ivh.Domain.Guarantor.Entities
{
    using System;

    public class VpGuarantorScore
    {
        public virtual int VpGuarantorScoreId { get; set; }
        public virtual int VpGuarantorId { get; set; }
        public virtual decimal Score { get; set; }
        public virtual int ScoreTypeId { get; set; }
        public virtual DateTime ScoreCreationDate { get; set; }
    }
}
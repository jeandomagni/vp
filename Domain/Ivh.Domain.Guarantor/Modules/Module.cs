﻿namespace Ivh.Domain.Guarantor.Modules
{
    using Autofac;
    using Interfaces;
    using Rules;
    using Services;
    
    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<VpGuarantorStateRules>().AsSelf().SingleInstance();
            builder.RegisterType<GuarantorStateMachineService>().As<IGuarantorStateMachineService>();
            builder.RegisterType<GuarantorInvitationService>().As<IGuarantorInvitationService>();
            builder.RegisterType<GuarantorService>().As<IGuarantorService>();
        }
    }
}

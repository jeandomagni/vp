﻿namespace Ivh.Domain.Guarantor.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Application.Matching.Common.Dtos;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.Matching.Dtos;
    using Entities;

    public interface IGuarantorRepository : IRepository<Guarantor>
    {
        int GetVisitPayUserId(int vpGuarantorId);
        int GetGuarantorId(int visitPayUserId);
        int GetGuarantorId(Guid visitPayUserGuid);
        int GetGuarantorId(string visitPayUserName);
        IList<Guarantor> GetActiveGuarantorsWithPendingStatement(DateTime date);
        IList<int> GetGuarantorIds(ICollection<VpGuarantorStatusEnum> guarantorStatuses);
        IList<Guarantor> GetGuarantors(GuarantorFilter filter);
        void InsertHsGuarantorMap(HsGuarantorMap appHsGuarantor);
        IEnumerable<Guarantor> GetActiveGuarantorsWithPaymentDueDate(DateTime dueDate);
        IList<VpGuarantorCancellationReason> GetAllCancellationReasons();
        bool IsGuarantorAccountClosed(int vpGuarantorId);
        DateTime? GetGuarantorAccountClosedDate(int vpGuarantorId);
        IList<Guarantor> GetGuarantors(GuarantorFilter filter, int page, int rows);
        int GetGuarantorCount(GuarantorFilter filter);
        GuarantorMatchResult IsGuarantorMatch(MatchDataDto matchData, PatternUseEnum patternUseEnum);
        IList<GuarantorMatchResult> AddInitialMatches(int vpGuarantorId, MatchGuarantorDto guarantor, MatchPatientDto patient);
        string GetGuarantorMatchSsn(int vpGuarantorId);
    }
}
﻿namespace Ivh.Domain.Guarantor.Interfaces
{
    using System;
    using Common.Base.Interfaces;
    using Entities;

    public interface IGuarantorInvitationService : IDomainService
    {
        void SendGuarantorInvitation(GuarantorInvitation guarantorInvitation);
        GuarantorInvitation GetGuarantorInvitation(Guid invitationToken);
    }
}
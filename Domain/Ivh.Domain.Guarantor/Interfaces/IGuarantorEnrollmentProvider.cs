﻿namespace Ivh.Domain.Guarantor.Interfaces
{
    using System.Threading.Tasks;
    using Common.VisitPay.Messages.HospitalData.Dtos;

    public interface IGuarantorEnrollmentProvider
    {
        Task<EnrollmentResponseDto> UnEnrollGuarantorAsync(string guarantorIdToEnroll);
        Task<EnrollmentResponseDto> EnrollGuarantorAsync(string guarantorIdToEnroll, int billingSystemId, int vpGuarantorId);
        Task PublishEnrollGuarantorMessageAsync(int vpGuarantorId);
        Task PublishUnEnrollGuarantorMessageAsync(int vpGuarantorId);
    }
}
﻿namespace Ivh.Domain.Guarantor.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IHsGuarantorMapRepository : IRepository<HsGuarantorMap>
    {
        IList<HsGuarantorMap> GetGuarantors(int hsGuarantorId, HsGuarantorMatchStatusEnum? hsGuarantorMatchStatusEnum = null);
        HsGuarantorMap GetHsGuarantor(string sourceSystemKey, int billingSystemId);
        IList<HsGuarantorMap> GetHsGuarantors(string sourceSystemKey);
    }
}
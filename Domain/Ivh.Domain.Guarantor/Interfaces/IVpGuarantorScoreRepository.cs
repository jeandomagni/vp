﻿namespace Ivh.Domain.Guarantor.Interfaces
{
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IVpGuarantorScoreRepository : IRepository<VpGuarantorScore>
    {
        VpGuarantorScore GetScore(int vpGuarantorId, ScoreTypeEnum scoreType);
    }
}
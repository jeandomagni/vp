﻿namespace Ivh.Domain.Guarantor.Interfaces
{
    using Common.VisitPay.Enums;
    using Entities;

    public interface IGuarantorScoreService
    {
        VpGuarantorScore GetScore(int vpGuarantorId, ScoreTypeEnum scoreType);
    }
}
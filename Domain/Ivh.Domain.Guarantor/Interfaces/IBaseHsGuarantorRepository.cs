﻿namespace Ivh.Domain.Guarantor.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IBaseHsGuarantorRepository : IReadOnlyRepository<BaseHsGuarantor>
    {
        BaseHsGuarantor GetBaseHsGuarantor(int billingSystemId, string hsGuarantorSourceSystemKey);
        IList<BaseHsGuarantor> GetBaseHsGuarantors(string lastName, string hsGuarantorSourceSystemKey);
    }
}

﻿namespace Ivh.Domain.Guarantor.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IBaseHsGuarantorService : IDomainService
    {
        IList<BaseHsGuarantor> GetBaseHsGuarantors(string lastName, string hsGuarantorSourceSystemKey);
    }
}

﻿namespace Ivh.Domain.Guarantor.Interfaces
{
    using System;
    using Common.Base.Interfaces;
    using Entities;

    public interface IGuarantorInvitationRepository : IRepository<GuarantorInvitation>
    {
        GuarantorInvitation GetGuarantorInvitation(Guid invitationToken);
    }
}
﻿namespace Ivh.Domain.Guarantor.Interfaces
{
    using System.Threading.Tasks;
    using Common.Base.Interfaces;
    using Common.VisitPay.Messages.HospitalData.Dtos;

    public interface IGuarantorEnrollmentService : IDomainService
    {
        Task<bool> UnEnrollGuarantorAsync(string guarantorIdToEnroll);
        Task<EnrollmentResponseDto> EnrollGuarantorAsync(string guarantorIdToEnroll, int billingSystemId, int vpGuarantorId);
        Task PublishEnrollGuarantorMessageAsync(int vpGuarantorId);
        Task PublishUnEnrollGuarantorMessageAsync(int vpGuarantorId);
    }
}
﻿namespace Ivh.Domain.Guarantor.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IBillingSystemService : IDomainService
    {
        IList<BillingSystem> GetAllBillingSystemTypes(bool includeUnmatched = false);
    }
}
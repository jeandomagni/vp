﻿namespace Ivh.Domain.Guarantor.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IBillingSystemRepository : IRepository<BillingSystem>
    {
        IList<BillingSystem> GetAllBillingSystemTypes(bool includeUnmatched = false);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.Guarantor.Interfaces
{
    using Common.Base.Enums;
    using Common.State.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IGuarantorStateMachineService : IMachine<Guarantor, VpGuarantorStatusEnum>
    {
    }
}

﻿namespace Ivh.Domain.Guarantor.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using Common.VisitPay.Messages.Matching.Dtos;
    using Entities;
    using User.Entities;

    public interface IGuarantorService : IDomainService
    {
        Guarantor GetGuarantor(int vpGuarantorId);
        Guarantor GetGuarantor(string visitPayUserName);
        Guarantor GetGuarantorEx(int visitPayUserId, int? vpGuarantorId = null);
        Guarantor GetGuarantorBySsk(string sourceSystemKey);
        int GetGuarantorId(int visitPayUserId);
        int GetGuarantorId(Guid visitPayUserGuid);
        int GetVisitPayUserId(int vpGuarantorId);
        void InsertOrUpdateGuarantor(Guarantor guarantor);
        void UpdateGuarantorType(Guarantor guarantor, GuarantorTypeEnum guarantorTypeEnum);
        int GetGuarantorId(string visitPayUserName);
        IList<Guarantor> GetGuarantorsFromIntList(IList<int> vpGuarantorIds);
        IList<Guarantor> GetGuarantors(GuarantorFilter filter);
        IList<int> GetActiveGuarantorsWithPendingStatement(DateTime date);
        void SetToActiveHsAcknowledgedPendingVisits(Guarantor guarantor);
        void SetToActiveVisitsLoaded(Guarantor guarantor);
        void InsertMatchingHsGuarantor(HsGuarantorAcknowledgementDto acknowledgement, Guarantor guarantor);
        void ChangePaymentDueDay(Guarantor guarantor, int dueDay, VisitPayUser actionVisitPayUser, string changeReason);
        void ChangeStatementDate(Guarantor guarantor, DateTime? nextStatementDate);
        IList<HsGuarantorMap> GetHsGuarantors(int hsGuarantorId, HsGuarantorMatchStatusEnum? hsGuarantorMatchStatusEnum = null);
        HsGuarantorMap GetHsGuarantor(string sourceSystemKey, int billingSystemId);
        IList<HsGuarantorMap> GetHsGuarantors(string sourceSystemKey);
        string GetGuarantorMatchSsn(int vpGuarantorId);
        IList<VpGuarantorCancellationReason> GetAllCancellationReasons();
        bool IsGuarantorAccountClosed(int vpGuarantorId);
        IList<Guarantor> GetGuarantors(GuarantorFilter filter, int page, int rows);
        int GetGuarantorCount(GuarantorFilter filter);
        GuarantorMatchResult IsGuarantorMatch(MatchDataDto matchData);

        /// <summary>
        /// For licensing purposes this should only be invoked from service bus message consumption.
        /// Adds registration match, and any valid ongoing matches.
        /// </summary>
        /// <param name="vpGuarantorId"></param>
        /// <param name="guarantor"></param>
        /// <param name="patient"></param>
        /// <returns></returns>
        IList<GuarantorMatchResult> AddInitialMatches(int vpGuarantorId, MatchGuarantorDto guarantor, MatchPatientDto patient);
    }
}
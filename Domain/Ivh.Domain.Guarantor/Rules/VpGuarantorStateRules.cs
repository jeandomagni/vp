﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.Guarantor.Rules
{
    using Common.Base.Enums;
    using Entities;
    using Common.State;
    using Common.VisitPay.Enums;

    public class VpGuarantorStateRules : Rules<Guarantor, VpGuarantorStatusEnum>
    {
        //Business rules document
        //https://ivincehealth.sharepoint.com/:x:/r/Products/vpp/_layouts/15/doc.aspx?sourcedoc=%7B67842CDD-DBD3-460F-BB10-90E8A7EB231B%7D&file=VP3%20States.xlsx&action=default

        public const string GS_101 = "GS-101";
        public const string GS_102 = "GS-102";
        public const string GS_103 = "GS-103";
        public const string CLOSED = "CLOSED";
        public const string ACTIVE = "ACTIVE";
        public const string NOT_SET = "NOT_SET";

        public VpGuarantorStateRules()
        {
            this.AddRule(GS_101)
                .When(VpGuarantorStatusEnum.NotSet)
                .And((ec) => ec.Entity.ChangeStateToActive)
                .Then(VpGuarantorStatusEnum.Active);

            this.AddRule(GS_102)
                .When(VpGuarantorStatusEnum.Active)
                .And((ec) => ec.Entity.ChangeStateToClosed)
                .Then(VpGuarantorStatusEnum.Closed);

            this.AddRule(GS_103)
                .When(VpGuarantorStatusEnum.Closed)
                .And((ec) => ec.Entity.ChangeStateToActive)
                .Then(VpGuarantorStatusEnum.Active);

            //Set same value if the rules above did not evaluate to TRUE

            this.AddRule(NOT_SET)
                .When(VpGuarantorStatusEnum.NotSet)
                .And((ec) => true)
                .Then(VpGuarantorStatusEnum.NotSet);

            this.AddRule(ACTIVE)
                .When(VpGuarantorStatusEnum.Active)
                .And((ec) => true)
                .Then(VpGuarantorStatusEnum.Active);

            this.AddRule(CLOSED)
                .When(VpGuarantorStatusEnum.Closed)
                .And((ec) => true)
                .Then(VpGuarantorStatusEnum.Closed);
        }
    }
}

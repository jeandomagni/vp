﻿namespace Ivh.Domain.User.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class VisitPayUserAcknowledgementMap : ClassMap<VisitPayUserAcknowledgement>
    {
        public VisitPayUserAcknowledgementMap()
        {
            this.Schema("dbo");
            this.Table("VisitPayUserAcknowledgement");
            this.Id(x => x.VisitPayUserAcknowledgementId);
            this.References(x => x.VisitPayUser)
                .Column("VisitPayUserId");
            this.Map(x => x.CmsVersionId).Not.Nullable();
            this.Map(x => x.ClientCmsVersionId).Nullable();
            this.Map(x => x.AcknowledgementDate).Not.Nullable();
            this.Map(x => x.VisitPayUserAcknowledgementType).Not.Nullable().Column("VisitPayUserAcknowledgementTypeId").CustomType<VisitPayUserAcknowledgementTypeEnum>();
            this.Map(x => x.SmsPhoneType).Nullable().Column("SmsPhoneType").CustomType<SmsPhoneTypeEnum>();
        }
    }
}
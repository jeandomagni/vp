namespace Ivh.Domain.User.Mappings
{
    using Common.Data.Constants;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class VisitPayUserMap : ClassMap<VisitPayUser>
    {
        public VisitPayUserMap()
        {
            this.Table(nameof(VisitPayUser));
            this.Schema(Schemas.VisitPay.Dbo);
            this.Id(x => x.VisitPayUserId);

            this.Map(x => x.AccessFailedCount);
            this.Map(x => x.Email);
            this.Map(x => x.EmailConfirmed);
            this.Map(x => x.LockoutEnabled);
            this.Map(x => x.LockoutEndDateUtc);
            this.Map(x => x.LockoutReasonEnum).Column("LockoutReasonId").Nullable().CustomType<LockoutReasonEnum>();
            this.Map(x => x.PasswordHash);
            this.Map(x => x.PhoneNumber);
            this.Map(x => x.PhoneNumberType);
            this.Map(x => x.PhoneNumberConfirmed);
            this.Map(x => x.TwoFactorEnabled);
            this.Map(x => x.UserName).Length(255).Not.Nullable().Unique();
            this.Map(x => x.SecurityStamp);
            this.Map(x => x.TempPassword);
            this.Map(x => x.TempPasswordExpDate);
            this.Map(x => x.FirstName);
            this.Map(x => x.MiddleName);
            this.Map(x => x.LastName);
            this.Map(x => x.PhoneNumberSecondary);
            this.Map(x => x.PhoneNumberSecondaryType);
            this.Map(x => x.Gender);
            this.Map(x => x.SSN4);
            this.Map(x => x.DateOfBirth);
            this.Map(x => x.EmulateToken);
            this.Map(x => x.EmulateExpDate);
            this.Map(x => x.EmulateUserId);
            this.Map(x => x.PasswordExpiresUtc).Nullable();
            this.Map(x => x.SecurityValidationValue).Nullable();
            this.Map(x => x.InsertDate);
            this.Map(x => x.PasswordHashConfigurationVersionId).Nullable();
            this.Map(x => x.Locale);
            this.Map(x => x.VisitPayUserGuid);
            this.Map(x => x.AccessTokenPhi).Length(8).Nullable();

            this.HasMany(x => x.Claims).KeyColumn("VisitPayUserId")
                .BatchSize(50)
                //.Not.LazyLoad()
                .Cascade.All();
            this.HasMany(x => x.Logins).KeyColumn("VisitPayUserId")
                .BatchSize(50)
                //.Not.LazyLoad()
                .Cascade.All();
            this.HasMany(x => x.Acknowledgements).KeyColumn("VisitPayUserId")
                .BatchSize(50)
                //.Not.LazyLoad()
                .Inverse()
                .Cascade.AllDeleteOrphan();

            this.HasManyToMany(x => x.Roles)
                .BatchSize(50)
                .Table("VisitPayUserRole")
                .ParentKeyColumn("VisitPayUserId")
                .ChildKeyColumn("VisitPayRoleId")
                //.Not.LazyLoad()
                .Cascade.All();

            this.HasMany(x => x.VpUserLoginSummary)
                .BatchSize(50)
                .KeyColumn("VpUserId")
                //.Not.LazyLoad()
                .Cascade.All();

            this.HasMany(x => x.SecurityQuestionAnswers).KeyColumn("VisitPayUserId")
                .BatchSize(50)
                .Not.Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                //.Not.LazyLoad()
                .Cascade.AllDeleteOrphan();

            this.HasMany(x => x.VisitPayUserAddresses)
                .KeyColumn("VisitPayUserId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();

            this.References(x => x.CurrentPhysicalAddress)
                .Fetch.Join()
                .Column("CurrentAddressId");

            this.References(x => x.CurrentMailingAddress)
                .Fetch.Join()
                .Nullable()
                .Column("CurrentMailingAddressId");
        }
    }
}
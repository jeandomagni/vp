namespace Ivh.Domain.User.Mappings
{
    using FluentNHibernate.Mapping;

    public class SecurityQuestionMap : ClassMap<Entities.SecurityQuestion>
    {
        public SecurityQuestionMap()
        {
            this.Table("SecurityQuestion");
            this.Schema("dbo");
            this.Id(x => x.SecurityQuestionId);
            this.Map(x => x.Question).Column("SecurityQuestion");
            this.Map(x => x.IsActive);
        }
    }
}
namespace Ivh.Domain.User.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class VisitPayUserLoginMap : ClassMap<VisitPayUserLogin>
    {
        public VisitPayUserLoginMap()
        {
            this.Table("VisitPayUserLogin");
            this.Schema("dbo");
            this.Id(x => x.VisitPayUserLoginId);
            //CompositeId()
            //    .KeyReference(x => x.VisitPayUserId)
            //    .KeyReference(x => x.LoginProvider);
            this.Map(x => x.LoginProvider);
            this.Map(x => x.ProviderKey);
            this.Map(x => x.VisitPayUserId);
        }
    }
}
namespace Ivh.Domain.User.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class VisitPayRoleMap : ClassMap<VisitPayRole>
    {
        public VisitPayRoleMap()
        {
            this.Table("VisitPayRole");
            this.Schema("dbo");
            this.HasManyToMany(x => x.Users)
                .BatchSize(50)
                .Table("VisitPayUserRole")
                .ParentKeyColumn("VisitPayRoleId")
                .ChildKeyColumn("VisitPayUserId")
                .Cascade.All();
            this.Id(x => x.VisitPayRoleId);
            this.Map(x => x.Name);
            this.Map(x => x.VisitPayRoleDescription);
        }
    }
}
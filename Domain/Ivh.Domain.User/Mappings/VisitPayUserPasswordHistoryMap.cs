﻿namespace Ivh.Domain.User.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class VisitPayUserPasswordHistoryMap : ClassMap<VisitPayUserPasswordHistory>
    {
        public VisitPayUserPasswordHistoryMap()
        {
            this.Table("VisitPayUserPasswordHistory");
            this.Schema("dbo");
            this.Id(x => x.VisitPayUserPasswordHistoryId).GeneratedBy.Identity();

            this.Map(x => x.PasswordExpiresUtc);
            this.Map(x => x.PasswordHash);
            this.Map(x => x.VisitPayUserId);
            this.Map(x => x.PasswordHashConfigurationVersionId).Nullable();
        }
    }
}

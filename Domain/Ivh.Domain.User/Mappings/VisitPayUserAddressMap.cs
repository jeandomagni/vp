﻿namespace Ivh.Domain.User.Mappings
{
    using Common.Base.Constants;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class VisitPayUserAddressMap : ClassMap<VisitPayUserAddress>
    {
        public VisitPayUserAddressMap()
        {
            this.Table(nameof(VisitPayUserAddress));
            this.Schema(NhibernateConstants.SchemaNames.Dbo);
            this.Id(x => x.VisitPayUserAddressId);

            this.References(x => x.VisitPayUser)
                .Column("VisitPayUserId");
            this.Map(x => x.AddressStreet1);
            this.Map(x => x.AddressStreet2);
            this.Map(x => x.City);
            this.Map(x => x.StateProvince);
            this.Map(x => x.PostalCode);
            this.Map(x => x.VisitPayUserAddressType).Nullable().Column("VisitPayUserAddressTypeId").CustomType<VisitPayUserAddressTypeEnum>();
            this.Map(x => x.IsPrimary);
            this.Map(x => x.InsertDate);
            this.Map(x => x.AddressHash).Not.Insert().Not.Update();
            this.Map(x => x.AddressStatus).Column("AddressStatusId").CustomType<AddressStatusEnum>();
            this.Map(x => x.VerificationCheckDate).Nullable();
            this.Map(x => x.IsUspsVerifiedFormat).Not.Nullable();
        }
    }
}
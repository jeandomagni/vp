﻿namespace Ivh.Domain.User.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class VpUserLoginSummaryMap : ClassMap<VpUserLoginSummary>
    {
        public VpUserLoginSummaryMap()
        {
            this.Table("VpUserLoginSummary");
            this.Schema("dbo");
            this.Id(x => x.VpUserId).GeneratedBy.Assigned();
            this.Map(x => x.LastLoginDate);
        }
    }
}

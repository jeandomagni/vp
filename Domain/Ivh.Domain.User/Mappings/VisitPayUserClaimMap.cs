namespace Ivh.Domain.User.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class VisitPayUserClaimMap : ClassMap<VisitPayUserClaim>
    {
        public VisitPayUserClaimMap()
        {
            this.Table("VisitPayUserClaim");
            this.Schema("dbo");
            this.Id(x => x.VisitPayUserClaimId);
            this.Map(x => x.ClaimType);
            this.Map(x => x.ClaimValue);
            this.References(x => x.User)
                .Column("VisitPayUserId");
        }
    }
}
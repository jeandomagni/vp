namespace Ivh.Domain.User.Mappings
{
    using Common.Data;
    using Entities;
    using FluentNHibernate.Mapping;

    public class SecurityQuestionAnswerMap : ClassMap<SecurityQuestionAnswer>
    {
        public SecurityQuestionAnswerMap()
        {
            this.Table("SecurityQuestionAnswer");
            this.Schema("dbo");
            this.Id(x => x.SecurityQuestionAnswerId);
            this.Map(x => x.Answer)
                .Column("SecurityQuestionAnswer")
                .CustomType<EncryptedString>();

            this.References(x => x.SecurityQuestion)
                .Column("SecurityQuestionId")
                .Fetch.Join()
                .Not.Nullable();
        }
    }
}

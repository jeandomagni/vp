﻿namespace Ivh.Domain.User.Modules
{
    using Autofac;
    using Interfaces;
    using Services;
    using Settings.Interfaces;
    using Settings.Services;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<VisitPayRoleService>().As<IVisitPayRoleService>();

            builder.RegisterType<ClientService>().As<IClientService>();
            builder.RegisterType<VisitPayRoleService>().As<IVisitPayRoleService>();
            builder.RegisterType<VisitPayUserJournalEventService>().As<IVisitPayUserJournalEventService>();
            builder.RegisterType<VpUserLoginSummaryService>().As<IVpUserLoginSummaryService>();
            builder.RegisterType<SecurityQuestionService>().As<ISecurityQuestionService>();
            builder.RegisterType<VisitPayUserAddressVerificationService>().As<IVisitPayUserAddressVerificationService>();
        }
    }
}

namespace Ivh.Domain.User.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using Application.Base.Common.Dtos;
    using Application.User.Common.Dtos;
    using Base.Interfaces;
    using Common.Base.Utilities.Extensions;
    using Common.Encryption.Security.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;
    using Microsoft.AspNet.Identity;
    using Settings.Entities;

    public class VisitPayUserService : UserManager<VisitPayUser>, IVisitPayUserService
    {
        private const int TempPasswordPasswordHashConfiguration = 0;

        private readonly Lazy<IIdentityEmailService> _identityEmailService;
        private readonly Lazy<IIdentitySmsService> _identitySmsService;
        private readonly Lazy<IVisitPayUserRepository> _visitPayUserRepository;
        private readonly Lazy<IVisitPayUserPasswordHistoryRepository> _visitPayUserPasswordHistoryRepository;
        private readonly Lazy<IPasswordHasherService> _passwordHasherService;
        private readonly Lazy<IUserLockoutStore<VisitPayUser, string>> _userLockoutStore;
        private readonly Lazy<IDomainServiceCommonService> _commonServices;

        public VisitPayUserService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            IUserStore<VisitPayUser> store,
            Lazy<IIdentityEmailService> identityEmailService,
            Lazy<IIdentitySmsService> identitySmsService,
            Lazy<IVisitPayUserRepository> visitPayUserRepository,
            Lazy<IVisitPayUserPasswordHistoryRepository> visitPayUserPasswordHistoryRepository,
            Lazy<IPasswordHasherService> passwordHasherService,
            IUserTokenProvider<VisitPayUser, string> userTokenProvider) : base(store)
        {
            this._commonServices = serviceCommonService;
            this._identityEmailService = identityEmailService;
            this._identitySmsService = identitySmsService;
            this._visitPayUserRepository = visitPayUserRepository;
            this._visitPayUserPasswordHistoryRepository = visitPayUserPasswordHistoryRepository;
            this._passwordHasherService = passwordHasherService;
            this.UserTokenProvider = userTokenProvider;
            this._userLockoutStore = new Lazy<IUserLockoutStore<VisitPayUser, string>>(() => (IUserLockoutStore<VisitPayUser, string>)this._visitPayUserRepository.Value);
            this.Setup();
        }

        public VisitPayUser FindUserById(int visitPayUserId)
        {
            return this.FindById(visitPayUserId.ToString());
        }

        public IdentityResult UpdateUser(VisitPayUser user)
        {
            IdentityResult result = this.Update(user);

            return result;
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(VisitPayUser user)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            ClaimsIdentity userIdentity = await this.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie).ConfigureAwait(false);
            // Add custom user claims here
            return userIdentity;
        }

        private void Setup()
        {
            // Configure validation logic for usernames
            this.UserValidator = new UserValidator<VisitPayUser>(this)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = false
            };


            Client client = this._commonServices.Value.Client.Value;
            bool isStrongPassword = this._commonServices.Value.ApplicationSettingsService.Value.IsClientApplication.Value ? client.ClientIsStrongPassword : client.GuarantorIsStrongPassword;
            int passwordLength = this._commonServices.Value.ApplicationSettingsService.Value.IsClientApplication.Value ? client.ClientPasswordMinLength : client.GuarantorPasswordMinLength;

            this.PasswordValidator = new PasswordValidator
            {
                RequiredLength = passwordLength,
                RequireNonLetterOrDigit = isStrongPassword,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true
            };

            // Configure validation logic for passwords
            // Configure user lockout defaults
            this.UserLockoutEnabledByDefault = true;
            this.DefaultAccountLockoutTimeSpan = this._commonServices.Value.ApplicationSettingsService.Value.IsClientApplication.Value ? TimeSpan.FromMinutes(client.ClientAutoUnlockTime) : TimeSpan.FromMinutes(client.GuarantorAutoUnlockTime);
            this.MaxFailedAccessAttemptsBeforeLockout = this._commonServices.Value.ApplicationSettingsService.Value.IsClientApplication.Value ? client.ClientMaxFailedAttemptCount : client.GuarantorMaxFailedAttemptCount;

            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug it in here.
            this.RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<VisitPayUser>
            {
                MessageFormat = "Your security code is {0}"
            });
            this.RegisterTwoFactorProvider("Email Code", new EmailTokenProvider<VisitPayUser>
            {
                Subject = "Security Code",
                BodyFormat = "Your security code is {0}"
            });
            this.EmailService = this._identityEmailService.Value;
            this.SmsService = this._identitySmsService.Value;
        }

        public async Task<IdentityResult> IncrementAccessFailedAttemptsAfterSecureCodeFailureAsync(VisitPayUser user)
        {
            IdentityResult result = IdentityResult.Success;

            bool isLockoutEnabled = await this.GetLockoutEnabledAsync(user.VisitPayUserId.ToString());
            if (!isLockoutEnabled)
            {
                return result;
            }

            int failedAttemptCount = await this._userLockoutStore.Value.IncrementAccessFailedCountAsync(user);
            this.Update(user);

            if (failedAttemptCount < this.MaxFailedAccessAttemptsBeforeLockout)
            {
                return result;
            }

            // lock the account
            user.LockoutEndDateUtc = DateTime.UtcNow.Add(this.DefaultAccountLockoutTimeSpan);
            user.LockoutReasonEnum = LockoutReasonEnum.LoginFailed;
            user.AccessFailedCount = 0; // start with clean slate when lock expires

            this.Update(user);

            result = IdentityResult.Failed("Too many failed login attempts. Your account has been temporarily locked.");

            return result;
        }

        public Task<IdentityResult> IsPasswordStrongEnough(string password)
        {
            return this.PasswordValidator.ValidateAsync(password);
        }

        public async Task<DataResult<VisitPayUser, FindUserEnum>> FindByDetailsAsync(VisitPayUserFindByDetailsParametersDto visitPayUserFindByDetailsParameters)
        {
            try
            {
                VisitPayUser user = await this._visitPayUserRepository.Value.FindByDetailsAsync(visitPayUserFindByDetailsParameters);
                return this.FindUserCommonValidation(user);
            }
            catch (Exception ex)
            {
                this._commonServices.Value.LoggingService.Value.Warn(() => ex.Message);
                return new DataResult<VisitPayUser, FindUserEnum> { Result = FindUserEnum.MoreThanOneFound };
            }
        }

        private DataResult<VisitPayUser, FindUserEnum> FindUserCommonValidation(VisitPayUser user)
        {
            if (user == null)
            {
                return new DataResult<VisitPayUser, FindUserEnum> { Result = FindUserEnum.NotFound };
            }

            if (user.IsLocked || user.IsDisabled)
            {
                return new DataResult<VisitPayUser, FindUserEnum> { Data = user, Result = FindUserEnum.AccountLocked };
            }

            return new DataResult<VisitPayUser, FindUserEnum> { Data = user, Result = FindUserEnum.Found };
        }

        public VisitPayUser FindByEmulateToken(string token)
        {
            return this._visitPayUserRepository.Value.FindByEmulateTokenAsync(token).Result;
        }

        public VisitPayUser FindByDetailsAndToken(string lastName, DateTime dateOfBirth, string token)
        {
            IReadOnlyList<VisitPayUser> matchingUsers = this._visitPayUserRepository.Value.FindByNameAndDob(lastName, dateOfBirth);
            if (matchingUsers.Count == 0)
            {
                return null;
            }

            IList<VisitPayUser> usersMatchedByToken = new List<VisitPayUser>();
            foreach (VisitPayUser matchingUser in matchingUsers)
            {
                if (this.VerifyTempPassword(matchingUser, token))
                {
                    usersMatchedByToken.Add(matchingUser);
                    continue;
                }

                IList<VisitPayUserPasswordHistory> passwordHistories = this._visitPayUserPasswordHistoryRepository.Value.GetUserPasswordHistory(matchingUser.VisitPayUserId, int.MaxValue);
                foreach (VisitPayUserPasswordHistory passwordHistory in passwordHistories)
                {
                    if (this.VerifyTempPassword(passwordHistory.PasswordHash, token))
                    {
                        usersMatchedByToken.Add(matchingUser);
                        break;
                    }
                }
            }

            if (usersMatchedByToken.Count <= 1)
            {
                return usersMatchedByToken.FirstOrDefault();
            }

            string str = $"{lastName},{dateOfBirth},{token}";
            this._commonServices.Value.LoggingService.Value.Fatal(() => $"{nameof(VisitPayUserService)}:{nameof(this.FindByDetailsAndToken)} - found more than one match for {str}");
            return null;
        }

        public VisitPayUser FindByDetailsAndAccessTokenPhi(string lastName, DateTime dob, string accessTokenPhi)
        {
            Regex nonDigitCharacters = new Regex(@"\D");
            string accessTokenPhiDigitsOnly = nonDigitCharacters.Replace(accessTokenPhi, string.Empty);

            IReadOnlyList<VisitPayUser> matchingUsers =
                this._visitPayUserRepository.Value
                    .FindByDetailsAndAccessTokenPhi(lastName, dob, accessTokenPhiDigitsOnly);

            if (!matchingUsers.Any())
            {
                return null;
            }

            if (matchingUsers.Count.Equals(1))
            {
                return matchingUsers.FirstOrDefault();
            }

            string str = $"{lastName},{dob},{accessTokenPhi}";
            this._commonServices.Value.LoggingService.Value.Fatal(() => $"{nameof(VisitPayUserService)}:{nameof(this.FindByDetailsAndToken)} - found more than one match for {str}");
            return null;
        }

        public Task<bool> VerifyPasswordAsync(VisitPayUser user, string password)
        {
            return this.VerifyPasswordAsync(this.GetPasswordStore(), user, password);
        }

        protected override Task<bool> VerifyPasswordAsync(IUserPasswordStore<VisitPayUser, string> store, VisitPayUser user, string password)
        {
            if (!string.IsNullOrWhiteSpace(user.TempPassword) && user.TempPasswordExpDate.HasValue && DateTime.UtcNow < user.TempPasswordExpDate.Value && user.TempPassword.Equals(password))
            {
                return Task.FromResult(true);
            }

            PasswordHashConfiguration hashConfig = this.GetPasswordHashConfiguration(user.PasswordHashConfigurationVersionId);
            Task<bool> isValid = this._passwordHasherService.Value.VerifyHashedPasswordAsync(user.PasswordHash, password, hashConfig.SaltLength, hashConfig.Iterations, hashConfig.HashEnum);
            if (isValid.Result)
            {
                this.UpdatePasswordHashToLatestConfiguration(user, password);
            }

            return isValid;
        }

        private void UpdatePasswordHashToLatestConfiguration(VisitPayUser user, string password)
        {
            int latestPasswordHashConfigurationKey = this.GetLatestPasswordHashConfigurationKey();
            if (latestPasswordHashConfigurationKey != user.PasswordHashConfigurationVersionId)
            {
                PasswordHashConfiguration hashConfig = this.GetLatestPasswordHashConfiguration();
                user.PasswordHashConfigurationVersionId = latestPasswordHashConfigurationKey;
                user.PasswordHash = this._passwordHasherService.Value.HashPassword(password, hashConfig.SaltLength, hashConfig.Iterations, hashConfig.HashEnum);
                this.Update(user);
            }
        }

        // the GeneratePasswordResetTokenAsync method used to generate the temp password has not been overwritten.
        // therefore the temp password is generated with the default hash, so use the default hash to verify it
        // todo if the hash used to generate the temp password changes, this should change as well.
        public bool VerifyTempPassword(VisitPayUser user, string password)
        {
            return this.VerifyTempPassword(user.TempPassword, password);
        }

        public bool VerifyTempPassword(string tempPassword, string checkPassword)
        {
            PasswordHashConfiguration hashConfig = this.GetPasswordHashConfiguration(TempPasswordPasswordHashConfiguration);
            return this._passwordHasherService.Value.VerifyHashedPassword(tempPassword, checkPassword, hashConfig.SaltLength, hashConfig.Iterations, hashConfig.HashEnum);
        }

        private PasswordHashConfiguration GetPasswordHashConfiguration(int passwordHashConfigurationVersionId)
        {
            return this._commonServices.Value.ApplicationSettingsService.Value.PasswordHashConfigurations.Value[passwordHashConfigurationVersionId];
        }

        private PasswordHashConfiguration GetLatestPasswordHashConfiguration()
        {
            return this._commonServices.Value.ApplicationSettingsService.Value.PasswordHashConfigurations.Value[this.GetLatestPasswordHashConfigurationKey()];
        }

        private int GetLatestPasswordHashConfigurationKey()
        {
            return this._commonServices.Value.ApplicationSettingsService.Value.PasswordHashConfigurations.Value.Keys.Max();
        }

        public string HashPasswordWithCurrentConfiguration(string password)
        {
            PasswordHashConfiguration hashConfig = this.GetLatestPasswordHashConfiguration();
            return this._passwordHasherService.Value.HashPassword(password, hashConfig.SaltLength, hashConfig.Iterations, hashConfig.HashEnum);
        }

        /// <summary>
        ///     Generate a password reset token for the user using the UserTokenProvider
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="emulateUserId"></param>
        /// <returns></returns>
        public virtual Task<string> GenerateEmulateTokenAsync(string userId, int emulateUserId)
        {
            return this.GenerateUserTokenAsync(TokenPurposeEnum.EmulateUser.ToString(), userId)
                .ContinueWith(task =>
                {
                    if (task.IsCompleted)
                    {
                        VisitPayUser user = this.FindByIdAsync(userId).Result;
                        user.EmulateToken = task.Result;
                        user.EmulateExpDate = DateTime.UtcNow.AddSeconds(this._commonServices.Value.Client.Value.EmulateTokenExpLimitInSeconds);
                        user.EmulateUserId = emulateUserId;
                    }
                    return task.Result;
                });
        }

        /// <summary>
        ///     Generate a temp login token for the vpcc user using the UserTokenProvider
        /// </summary>
        /// <returns></returns>
        public virtual string GenerateOfflineLoginToken(VisitPayUser visitPayUser)
        {
            if (!string.IsNullOrWhiteSpace(visitPayUser.TempPassword))
            {
                // save previous token
                this.SavePasswordHistory(visitPayUser.VisitPayUserId, visitPayUser.TempPassword, visitPayUser.TempPasswordExpDate, TempPasswordPasswordHashConfiguration);
            }

            string token = this.GenerateUserToken(TokenPurposeEnum.VpccLogin.ToString(), visitPayUser.VisitPayUserId.ToString());

            visitPayUser.TempPasswordExpDate = DateTime.UtcNow.AddHours(this._commonServices.Value.Client.Value.ClientTempPasswordExpLimitInHours);
            visitPayUser.LockoutEndDateUtc = DateTime.UtcNow;
            visitPayUser.LockoutReasonEnum = null;

            this.Update(visitPayUser);
            return token;
        }

        public VisitPayUserResults GetVisitPayClientUsers(VisitPayUserFilter filter, int batch, int batchSize)
        {
            if (filter.RolesInclude == null)
            {
                filter.RolesInclude = new List<int>();
            }

            if (filter.RolesExclude == null)
            {
                filter.RolesExclude = new List<int>();
            }

            return this._visitPayUserRepository.Value.GetVisitPayUsers(filter, 3, batch, batchSize);
        }

        public void DeactivateInactiveVisitPayClientUsers()
        {
            IReadOnlyList<VisitPayUser> visitPayUsers = this._visitPayUserRepository.Value.GetInactiveVisitPayClientUsers(DateTime.UtcNow.AddDays(this._commonServices.Value.Client.Value.ClientDeactivateDays * -1));
            foreach (VisitPayUser clientUser in visitPayUsers)
            {
                clientUser.LockoutEnabled = true;
                clientUser.LockoutEndDateUtc = DateTime.MaxValue.Date;
                clientUser.LockoutReasonEnum = LockoutReasonEnum.AccountDisabled;
                this.Update(clientUser);
            }
        }

        public bool VerifyPasswordHasNotBeenUsed(int userId, string password, int passwordReuseLimit)
        {
            IList<VisitPayUserPasswordHistory> passwordHistories = this._visitPayUserPasswordHistoryRepository.Value.GetUserPasswordHistory(userId, passwordReuseLimit);

            VisitPayUser user = this._visitPayUserRepository.Value.FindById(userId.ToString());
            if (user != null && !string.IsNullOrEmpty(user.PasswordHash))
            {
                // ensure current password is checked.
                if (!passwordHistories.Select(x => x.PasswordHash.Equals(user.PasswordHash)).Any())
                {
                    passwordHistories.Add(new VisitPayUserPasswordHistory
                    {
                        PasswordHash = user.PasswordHash,
                        PasswordHashConfigurationVersionId = user.PasswordHashConfigurationVersionId
                    });
                }
            }

            foreach (VisitPayUserPasswordHistory historyPassword in passwordHistories)
            {
                PasswordHashConfiguration hashConfig = this.GetPasswordHashConfiguration(historyPassword.PasswordHashConfigurationVersionId);
                if (this._passwordHasherService.Value.VerifyHashedPassword(historyPassword.PasswordHash, password, hashConfig.SaltLength, hashConfig.Iterations, hashConfig.HashEnum))
                {
                    return false;
                }
            }
            return true;
        }

        public void SavePasswordHistory(int userId, string passwordHash, DateTime? passwordExpiresUtc, int? passwordHashConfiguration = null)
        {
            int passwordHashConfigurationToUse = passwordHashConfiguration ?? this.GetLatestPasswordHashConfigurationKey();
            this._visitPayUserPasswordHistoryRepository.Value.SavePasswordHistory(userId, passwordHash, passwordExpiresUtc.GetValueOrDefault(DateTime.UtcNow), passwordHashConfigurationToUse);
        }

        public void SetPasswordExpiration(VisitPayUser visitPayUser, int passwordExpLimitInDays)
        {
            visitPayUser.PasswordExpiresUtc = DateTime.UtcNow.AddDays(passwordExpLimitInDays);
            this.Update(visitPayUser);
        }

        public override async Task<IdentityResult> ChangePasswordAsync(string userId, string currentPassword, string newPassword)
        {
            IUserPasswordStore<VisitPayUser, string> store = this.GetPasswordStore();
            VisitPayUser user = this._visitPayUserRepository.Value.FindById(userId);
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            if (await this.VerifyPasswordAsync(store, user, currentPassword).ConfigureAwait(false))
            {
                return await this.HashAndSaveNewPassword(newPassword, user);
            }
            return IdentityResult.Failed("Invalid Password");
        }

        public override async Task<IdentityResult> ResetPasswordAsync(string userId, string tempPassword, string newPassword)
        {
            VisitPayUser user = this._visitPayUserRepository.Value.FindById(userId);
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            if (this.VerifyTempPassword(user, tempPassword))
            {
                user.LockoutEndDateUtc = null;
                user.TempPassword = null;
                user.TempPasswordExpDate = null;
                return await this.HashAndSaveNewPassword(newPassword, user);
            }
            return IdentityResult.Failed("Invalid Password");
        }

        /// <summary>
        /// Reset a user's password without a reset token
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        public IdentityResult ResetPassword(string userId, string newPassword)
        {
            VisitPayUser user = this._visitPayUserRepository.Value.FindById(userId);
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            user.LockoutEndDateUtc = null;
            user.TempPassword = null;
            user.TempPasswordExpDate = null;
            return this.HashAndSaveNewPassword(newPassword, user).Result;
        }

        private async Task<IdentityResult> HashAndSaveNewPassword(string newPassword, VisitPayUser user)
        {
            IdentityResult isPasswordVaild = await base.PasswordValidator.ValidateAsync(newPassword).ConfigureAwait(false);
            if (!isPasswordVaild.Succeeded)
            {
                return isPasswordVaild;
            }

            user.PasswordHash = this.HashPasswordWithCurrentConfiguration(newPassword);
            user.PasswordHashConfigurationVersionId = this.GetLatestPasswordHashConfigurationKey();
            return await base.UpdateAsync(user).ConfigureAwait(false);
        }

        private IUserPasswordStore<VisitPayUser, string> GetPasswordStore()
        {
            IUserPasswordStore<VisitPayUser, string> store = this.Store as IUserPasswordStore<VisitPayUser, string>;
            if (store == null)
            {
                throw new NotSupportedException("StoreNotIUserSecurityStampStore");
            }
            return store;
        }

        public bool IsAccountEnabled(int visitPayUserId)
        {
            return this._visitPayUserRepository.Value.IsAccountEnabled(visitPayUserId);
        }

        public IList<VisitPayUser> AllWithSmsNumber(string phoneNumber)
        {
            return this._visitPayUserRepository.Value.GetVisitPayUsersWithSmsNumber(phoneNumber);
        }

        public void SetUserLocale(string userId, string locale)
        {
            this._visitPayUserRepository.Value.SetUserLocale(userId, locale);
        }

        public string GetUserLocale(string userId)
        {
            return this._visitPayUserRepository.Value.GetUserLocale(userId);
        }

        public void GenerateAccessTokenPhiForUser(int vpUserId)
        {
            VisitPayUser vpUser = this._visitPayUserRepository.Value.FindById(vpUserId.ToString());

            if (vpUser == null)
            {
                return;
            }

            if (vpUser.AccessTokenPhi.IsNotNullOrEmpty() && VisitPayUserAccessTokenPhi.IsValid(vpUser.AccessTokenPhi))
            {
                return;
            }

            this.AssignAccessTokenPhiToVpUser(vpUser);
            this.Update(vpUser);
        }

        private void AssignAccessTokenPhiToVpUser(VisitPayUser vpUser)
        {
            int timesAttempted = 0;
            do
            {
                timesAttempted += 1;
                vpUser.GenerateAndSetAccessTokenPhi();
            } while (this._visitPayUserRepository.Value.AccessTokenPhiValueExists(vpUser.AccessTokenPhi, vpUser.VisitPayUserId) && timesAttempted < 250);

            if(timesAttempted > 249)
            {
                this._commonServices.Value.LoggingService.Value.Warn(() => String.Format("Could not assign AccessTokenPhi to VpUser {0}", vpUser.VisitPayUserId));
            }
        }

        #region unreferenced code

        /*public string HashTempPassword(string password, PasswordHashConfiguration hashConfig)
        {
            return this._passwordHasherService.Value.HashPassword(password, hashConfig.SaltLength, hashConfig.Iterations, hashConfig.HmacHashEnum);
        }*/

        /*
        public async Task<DataResult<VisitPayUser, FindUserEnum>> FindByDetailsAsync(string lastName, DateTime dateOfBirth, int ssn4)
        {
            try
            {
                VisitPayUser user = await this._visitPayUserRepository.Value.FindByDetailsAsync(lastName, dateOfBirth, ssn4);
                return this.FindUserCommonValidation(user);
            }
            catch (Exception ex)
            {
                this._commonServices.Value.LoggingService.Value.Warn(() => ex.Message);
                return new DataResult<VisitPayUser, FindUserEnum> { Result = FindUserEnum.MoreThanOneFound };
            }
        }
        */

        #endregion
    }
}
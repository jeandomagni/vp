﻿namespace Ivh.Domain.User.Services
{
    using System;
    using Entities;
    using Interfaces;

    public class VpUserLoginSummaryService :  IVpUserLoginSummaryService
    {
        private readonly IVpUserLoginSummaryRepository _vpUserLoginSummaryRepository;

        public VpUserLoginSummaryService(
            IVpUserLoginSummaryRepository vpUserLoginSummaryRepository)
        {
            this._vpUserLoginSummaryRepository = vpUserLoginSummaryRepository;
        }

        public VpUserLoginSummary GetUserLoginSummary(int userId)
        {
            return this._vpUserLoginSummaryRepository.GetById(userId);
        }

        public void SetUserLastLogin(int userId)
        {
            var vpUserLoginSummary = this._vpUserLoginSummaryRepository.GetById(userId) ?? new VpUserLoginSummary();
            vpUserLoginSummary.VpUserId = userId;
            vpUserLoginSummary.LastLoginDate = DateTime.UtcNow;

            this._vpUserLoginSummaryRepository.InsertOrUpdate(vpUserLoginSummary);
        }
    }
}

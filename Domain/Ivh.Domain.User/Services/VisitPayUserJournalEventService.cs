﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.User.Services
{
    using System.Globalization;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.EventJournal;
    using Common.EventJournal.Templates.Admin;
    using Common.EventJournal.Templates.Client;
    using Common.EventJournal.Templates.Guarantor;
    using Common.EventJournal.Templates.SystemAction;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Entities;
    using EventJournal.Interfaces;
    using Interfaces;
    using Remotion.Linq.Parsing;

    public class VisitPayUserJournalEventService : DomainService, IVisitPayUserJournalEventService
    {

        private readonly Lazy<IVisitPayUserRepository> _visitPayUserRepository;
        private readonly Lazy<IEventJournalService> _eventJournalService;

        public VisitPayUserJournalEventService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IEventJournalService> eventJournalService,
            Lazy<IVisitPayUserRepository> visitPayUserRepository) : base(serviceCommonService)
        {
            this._visitPayUserRepository = visitPayUserRepository;
            this._eventJournalService = eventJournalService;
        }

        #region JournalEventUserContext

        public JournalEventUserContext GetJournalEventUserContext(JournalEventHttpContextDto journalEventHttpContextDto, VisitPayUser visitPayUser)
        {
            VisitPayUser visitPayUser1 = visitPayUser != null ? this._visitPayUserRepository.Value.FindById(visitPayUser.Id) : null;

            // If visitPayUser1 is null, ie, visitPayUser does not exists, use the original requested visitPayUser object to create the userEventHistoryContext
            // This prevents it defaulting to sysuser. Still defaults to sysuser if visitPayUser is null
            return this.CreateJournalEventUserContext(journalEventHttpContextDto, visitPayUser1 ?? visitPayUser);
        }
        public JournalEventUserContext GetJournalEventUserContext(JournalEventHttpContextDto journalEventHttpContextDto, int visitPayUserId)
        {
            VisitPayUser visitPayUser1 = this._visitPayUserRepository.Value.FindById(visitPayUserId.ToString());

            return this.CreateJournalEventUserContext(journalEventHttpContextDto, visitPayUser1);
        }

        private JournalEventUserContext CreateJournalEventUserContext(JournalEventHttpContextDto journalEventHttpContextDto, VisitPayUser visitPayUser)
        {
            bool isIvinciUser = (visitPayUser?.Roles?.Any(r => r.VisitPayRoleId == (int)VisitPayRoleEnum.IvhEmployee)).GetValueOrDefault(false);

            JournalEventUserContext journalEventUserContext = new JournalEventUserContext
            {
                UserName = (visitPayUser == null) ? SystemUsers.SystemUserName : visitPayUser.UserName,
                EventVisitPayUserId = visitPayUser?.VisitPayUserId ?? SystemUsers.SystemUserId,
                IsIvinciUser = isIvinciUser,
                IsClientUser = (visitPayUser?.IsInClientRole).GetValueOrDefault(false),
                IsSystemUser = (visitPayUser?.VisitPayUserId ?? SystemUsers.SystemUserId) == SystemUsers.SystemUserId,
                JournalEventHttpContextDto = journalEventHttpContextDto,
                Email = visitPayUser?.Email,
                FirstName = visitPayUser?.FirstName,
                LastName = visitPayUser?.LastName
            };

            return journalEventUserContext;
        }

        public JournalEventUserContext GetJournalContext(VisitPayUser visitPayUser)
        {
            return this.GetJournalEventUserContext(null, visitPayUser);
        }

        public JournalEventUserContext GetContext(JournalEventHttpContextDto journalEventHttpContextDto)
        {
            return this.GetJournalEventUserContext(journalEventHttpContextDto, null);
        }

        public JournalEventUserContext GetSystemJournalContext()
        {
            return new JournalEventUserContext() { UserName = SystemUsers.SystemUserName, EventVisitPayUserId = SystemUsers.SystemUserId, IsSystemUser = true };
        }

        #endregion

        #region JournalEventContext

        public JournalEventContext GetJournalEventContextForGuarantor(int visitPayUserId, int vpGuarantorId)
        {
            return new JournalEventContext() { VpGuarantorId = vpGuarantorId, VisitPayUserId = visitPayUserId };
        }

        public JournalEventContext GetJournalEventContextForClient(int visitPayUserId, int? vpGuarantorId)
        {
            return new JournalEventContext() { VisitPayUserId = visitPayUserId, VpGuarantorId = vpGuarantorId };
        }

        public JournalEventContext GetJournalEventContextForSystem(int visitPayUserId, int? vpGuarantorId)
        {
            return new JournalEventContext() { VisitPayUserId = visitPayUserId, VpGuarantorId = vpGuarantorId };
        }

        public JournalEventContext GetJournalEventContextForAdmin(int visitPayUserId)
        {
            return new JournalEventContext() { VisitPayUserId = visitPayUserId };
        }

        #endregion

        #region Journal Events

        //Called from domain service
        public void LogCancelReconfiguredFinancePlan(JournalEventUserContext journalEventUserContext, int vpGuarantorId, string vpGuarantorUserName, int financePlanId, int financePlanOfferSetTypeId)
        {
            if (journalEventUserContext.IsSystemUser)
            {
                JournalEventContext journalEventContext = this.GetJournalEventContextForSystem(journalEventUserContext.EventVisitPayUserId, vpGuarantorId);
                journalEventContext.FinancePlanId = financePlanId;

                this._eventJournalService.Value.AddEvent<EventSystemActionCanceledAReconfiguredFinancePlan, JournalEventParameters>(
                    EventSystemActionCanceledAReconfiguredFinancePlan.GetParameters
                    (
                        financePlanId: financePlanId.ToString(),
                        canceledTerms: financePlanOfferSetTypeId.ToString(),
                        vpUserName: vpGuarantorUserName
                    ),
                    journalEventUserContext,
                    journalEventContext);
            }
            else if (journalEventUserContext.IsClientUser)
            {
                VisitPayUser eventUser = this._visitPayUserRepository.Value.FindById(journalEventUserContext.EventVisitPayUserId.ToString());
                JournalEventContext journalEventContext = this.GetJournalEventContextForClient(journalEventUserContext.EventVisitPayUserId, vpGuarantorId);
                journalEventContext.FinancePlanId = financePlanId;

                this._eventJournalService.Value.AddEvent<EventClientCanceledAReconfiguredFinancePlan, JournalEventParameters>(
                    EventClientCanceledAReconfiguredFinancePlan.GetParameters
                    (
                        eventUserName: eventUser.UserName,
                        canceledTerms: financePlanOfferSetTypeId.ToString(),
                        vpUserName: vpGuarantorUserName
                    ),
                    journalEventUserContext,
                    journalEventContext);
            }
            else // Guarantor
            {
                JournalEventContext journalEventContext = this.GetJournalEventContextForGuarantor(journalEventUserContext.EventVisitPayUserId, vpGuarantorId);
                journalEventContext.FinancePlanId = financePlanId;

                this._eventJournalService.Value.AddEvent<EventGuarantorDeclinedReconfiguredTerms, JournalEventParameters>(
                    EventGuarantorDeclinedReconfiguredTerms.GetParameters
                    (
                        financePlanId: financePlanId.ToString(),
                        declinedTerms: financePlanOfferSetTypeId.ToString(),
                        vpUserName: vpGuarantorUserName
                    ),
                    journalEventUserContext,
                    journalEventContext);
            }
        }

        public void LogGuarantorAcceptReconfiguredFinancePlan(JournalEventUserContext journalEventUserContext, int vpGuarantorId, string vpGuarantorUserName, int financePlanId, int financePlanOfferSetTypeId)
        {
            JournalEventContext journalEventContext = this.GetJournalEventContextForGuarantor(journalEventUserContext.EventVisitPayUserId, vpGuarantorId);
            journalEventContext.FinancePlanId = financePlanId;

            this._eventJournalService.Value.AddEvent<EventGuarantorAcceptedReconfiguredTerms, JournalEventParameters>(
                EventGuarantorAcceptedReconfiguredTerms.GetParameters
                (
                    financePlanId: financePlanId.ToString(),
                    newTerms: financePlanOfferSetTypeId.ToString(),
                    vpUserName: vpGuarantorUserName
                ),
                journalEventUserContext,
                journalEventContext
            );
        }

        public void LogCancelFinancePlanEvent(JournalEventUserContext journalEventUserContext, int vpGuarantorId, string vpGuarantorUserName, int financePlanId, int financePlanOfferSetTypeId)
        {
            if (journalEventUserContext.IsSystemUser)
            {
                JournalEventContext journalEventContext = this.GetJournalEventContextForSystem(journalEventUserContext.EventVisitPayUserId, vpGuarantorId);
                journalEventContext.FinancePlanId = financePlanId;

                this._eventJournalService.Value.AddEvent<EventSystemActionCanceledAFinancePlan, JournalEventParameters>(
                    EventSystemActionCanceledAFinancePlan.GetParameters
                    (
                        financePlanId: financePlanId.ToString(),
                        canceledTerms: financePlanOfferSetTypeId.ToString(),
                        vpUserName: vpGuarantorUserName
                    ),
                    journalEventUserContext,
                    journalEventContext);
            }
            else if (journalEventUserContext.IsClientUser)
            {
                JournalEventContext journalEventContext = this.GetJournalEventContextForClient(journalEventUserContext.EventVisitPayUserId, vpGuarantorId);
                journalEventContext.FinancePlanId = financePlanId;

                VisitPayUser eventUser = this._visitPayUserRepository.Value.FindById(journalEventUserContext.EventVisitPayUserId.ToString());
                this._eventJournalService.Value.AddEvent<EventClientCanceledAFinancePlan, JournalEventParameters>(
                    EventClientCanceledAFinancePlan.GetParameters
                    (
                        eventUserName: eventUser.UserName,
                        financePlanId: financePlanId.ToString(),
                        vpUserName: vpGuarantorUserName
                    ),
                    journalEventUserContext,
                    journalEventContext);
            }
            else
            {
                JournalEventContext journalEventContext = this.GetJournalEventContextForGuarantor(journalEventUserContext.EventVisitPayUserId, vpGuarantorId);
                journalEventContext.FinancePlanId = financePlanId;

                this._eventJournalService.Value.AddEvent<EventGuarantorCanceledAFinancePlan, JournalEventParameters>(
                    EventGuarantorCanceledAFinancePlan.GetParameters
                    (
                        financePlanId: financePlanId.ToString(),
                        canceledTerms: financePlanOfferSetTypeId.ToString(),
                        vpUserName: vpGuarantorUserName
                    ),
                    journalEventUserContext,
                    journalEventContext);
            }
        }

        public void LogReconfiguredFinancePlan(JournalEventUserContext journalEventUserContext, int vpGuarantorId, string vpGuarantorUserName, int financePlanId, int financePlanOfferSetTypeId, bool isUpdate)
        {
            JournalEventContext journalEventContext = this.GetJournalEventContextForClient(journalEventUserContext.EventVisitPayUserId, vpGuarantorId);
            journalEventContext.FinancePlanId = financePlanId;

            if (journalEventUserContext.IsClientUser)
            {
                VisitPayUser eventUser = this._visitPayUserRepository.Value.FindById(journalEventUserContext.EventVisitPayUserId.ToString());
                if (isUpdate)
                {
                    this._eventJournalService.Value.AddEvent<EventClientModifiedAReconfiguredFinancePlan, JournalEventParameters>(
                        EventClientModifiedAReconfiguredFinancePlan.GetParameters
                        (
                            eventUserName: eventUser.UserName,
                            newTerms: financePlanOfferSetTypeId.ToString(),
                            vpUserName: vpGuarantorUserName
                        ),
                        journalEventUserContext,
                        journalEventContext);
                }
                else
                {
                    this._eventJournalService.Value.AddEvent<EventClientCreatedAReconfiguredFinancePlan, JournalEventParameters>(
                        EventClientCreatedAReconfiguredFinancePlan.GetParameters
                        (
                            eventUserName: eventUser.UserName,
                            newTerms: financePlanOfferSetTypeId.ToString(),
                            vpUserName: vpGuarantorUserName
                        ),
                        journalEventUserContext,
                        journalEventContext);
                }

            }

        }

        public void LogClientCustomizeFinancePlan(JournalEventUserContext journalEventUserContext, int vpGuarantorId, string vpGuarantorUserName, int financePlanId, int financePlanOfferSetTypeId, bool isUpdate)
        {
            JournalEventContext journalEventContext = this.GetJournalEventContextForClient(journalEventUserContext.EventVisitPayUserId, vpGuarantorId);
            journalEventContext.FinancePlanId = financePlanId;

            if (journalEventUserContext.IsClientUser)
            {
                VisitPayUser eventUser = this._visitPayUserRepository.Value.FindById(journalEventUserContext.EventVisitPayUserId.ToString());
                if (isUpdate)
                {
                    this._eventJournalService.Value.AddEvent<EventClientModifiedANonOriginatedFinancePlan, JournalEventParameters>(
                        EventClientModifiedANonOriginatedFinancePlan.GetParameters
                        (
                            eventUserName: eventUser.UserName,
                            newTerms: financePlanOfferSetTypeId.ToString(),
                            vpUserName: vpGuarantorUserName
                        ),
                        journalEventUserContext,
                        journalEventContext);
                }
                else
                {
                    this._eventJournalService.Value.AddEvent<EventClientCustomizedFinancePlanForAVpGuarantor, JournalEventParameters>(
                        EventClientCustomizedFinancePlanForAVpGuarantor.GetParameters
                        (
                            eventUserName: eventUser.UserName,
                            newTerms: financePlanOfferSetTypeId.ToString(),
                            vpUserName: vpGuarantorUserName
                        ),
                        journalEventUserContext,
                        journalEventContext);
                }
            }

        }

        // Rare case where the JournalEventParameters need to be passed in
        // We should not or may not know what a finance plan is in the namespace of this method
        public void LogGuarantorCustomizeFinancePlan(JournalEventUserContext journalEventUserContext, JournalEventParameters parameters, int vpGuarantorId, string vpGuarantorUserName, int financePlanId, int financePlanOfferSetTypeId)
        {
            JournalEventContext journalEventContext = this.GetJournalEventContextForGuarantor(journalEventUserContext.EventVisitPayUserId, vpGuarantorId);
            journalEventContext.FinancePlanId = financePlanId;

            this._eventJournalService.Value.AddEvent<EventGuarantorViewedFinancePlanTerms, JournalEventParameters>(
                parameters,
                journalEventUserContext,
                journalEventContext);
        }

        public void LogClientAcceptedFinancePlanOnBehalfOfGuarantor(JournalEventUserContext journalEventUserContext, int vpGuarantorId, string vpGuarantorUserName, int financePlanId, bool isAutoPay)
        {
            JournalEventContext journalEventContext = this.GetJournalEventContextForClient(journalEventUserContext.EventVisitPayUserId, vpGuarantorId);
            journalEventContext.FinancePlanId = financePlanId;


            this._eventJournalService.Value.AddEvent<EventClientAcceptedTermsOfFinancePlanForVpGuarantor, JournalEventParameters>(
                EventClientAcceptedTermsOfFinancePlanForVpGuarantor.GetParameters
                (
                    vpUserName: vpGuarantorUserName,
                    financePlanId: financePlanId.ToString(),
                    isAutoPay: isAutoPay.ToString(),
                    eventUserName: journalEventUserContext.UserName
                ),
                journalEventUserContext,
                journalEventContext);
        }

        public void LogGuarantorAcceptFinancePlan(JournalEventUserContext journalEventUserContext, int vpGuarantorId, string vpGuarantorUserName, int financePlanId, int financePlanOfferSetTypeId, int termsCmsVersionId)
        {
            JournalEventContext journalEventContext = this.GetJournalEventContextForGuarantor(journalEventUserContext.EventVisitPayUserId, vpGuarantorId);
            journalEventContext.FinancePlanId = financePlanId;

            this._eventJournalService.Value.AddEvent<EventGuarantorAcceptedFinancePlanTerms, JournalEventParameters>(
                EventGuarantorAcceptedFinancePlanTerms.GetParameters
                (
                    acceptedTerms: financePlanOfferSetTypeId.ToString(),
                    financePlanId: financePlanId.ToString(),
                    vpUserName: vpGuarantorUserName
                ),
                journalEventUserContext,
                journalEventContext);
        }

        public void LogTransactionUpdate(JournalEventUserContext journalEventUserContext, int vpGuarantorId, int visitId, int hsGuarantorId, decimal transactionAmount, VpTransactionTypeEnum vpTransactionType, string message)
        {
            JournalEventContext journalEventContext = this.GetJournalEventContextForAdmin(journalEventUserContext.EventVisitPayUserId);
            journalEventContext.HsGuarantorId = hsGuarantorId;
            journalEventContext.VisitId = visitId;

            this._eventJournalService.Value.AddEvent<EventAdminVpTransactionUpdate, JournalEventParameters>(
                EventAdminVpTransactionUpdate.GetParameters
                (
                    amountOfAdjustment: transactionAmount.ToString("C"),
                    eventUserName: journalEventUserContext.UserName,
                    notes: message
                ),
                journalEventUserContext,
                journalEventContext);
        }

        /// <summary>
        /// </summary>
        /// <param name="journalEventUserContext"></param>
        /// <param name="hsGuarantorId"></param>
        /// <param name="vpGuarantorId"></param>
        /// <param name="whenMatched">
        ///     If match is made during registration = "Registration", If match is made during routine
        ///     recurring process = "Recurring"
        /// </param>
        public void LogGuarantorMatch(JournalEventUserContext journalEventUserContext, int hsGuarantorId, int vpGuarantorId, string whenMatched)
        {
            JournalEventContext journalEventContext = this.GetJournalEventContextForSystem(journalEventUserContext.EventVisitPayUserId, vpGuarantorId);
            journalEventContext.HsGuarantorId = hsGuarantorId;

            this._eventJournalService.Value.AddEvent<EventSystemActionHsGuarantorIdVpGuarantorIdGuarantorMatched, JournalEventParameters>(
                EventSystemActionHsGuarantorIdVpGuarantorIdGuarantorMatched.GetParameters
                (
                    hsGuarantorId: hsGuarantorId.ToString(),
                    matchEvent: whenMatched,
                    vpGuarantorId: vpGuarantorId.ToString()
                ),
                journalEventUserContext,
                journalEventContext);
        }

        public void LogGuarantorUnmatch(JournalEventUserContext journalEventUserContext, int hsGuarantorId, int vpGuarantorId)
        {
            JournalEventContext journalEventContext = this.GetJournalEventContextForSystem(journalEventUserContext.EventVisitPayUserId, vpGuarantorId);
            journalEventContext.HsGuarantorId = hsGuarantorId;

            this._eventJournalService.Value.AddEvent<EventSystemActionHsGuarantorIdVpGuarantorIdGuarantorUnmatched, JournalEventParameters>(
                EventSystemActionHsGuarantorIdVpGuarantorIdGuarantorUnmatched.GetParameters
                (
                    hsGuarantorId: hsGuarantorId.ToString(),
                    vpGuarantorId: vpGuarantorId.ToString()
                ),
                journalEventUserContext,
                journalEventContext);
        }

        public void LogVisitUnmatch(JournalEventUserContext journalEventUserContext, int hsGuarantorId, int vpGuarantorId, string visitSourceSystemKey, int visitId)
        {
            JournalEventContext journalEventContext = this.GetJournalEventContextForSystem(journalEventUserContext.EventVisitPayUserId, vpGuarantorId);
            journalEventContext.HsGuarantorId = hsGuarantorId;
            journalEventContext.VisitId = visitId;

            this._eventJournalService.Value.AddEvent<EventSystemActionVisitHsGuarantorIdUnmatch, JournalEventParameters>(
                EventSystemActionVisitHsGuarantorIdUnmatch.GetParameters
                (
                    hsGuarantorId: hsGuarantorId.ToString(),
                    visitSourceSystemKey: visitSourceSystemKey,
                    vpVisitId: visitId.ToString()
                ),
                journalEventUserContext,
                journalEventContext);
        }

        public void LogVisitRedacted(JournalEventUserContext journalEventUserContext, int hsGuarantorId, int vpGuarantorId, string visitSourceSystemKey, int visitId)
        {
            JournalEventContext journalEventContext = this.GetJournalEventContextForSystem(journalEventUserContext.EventVisitPayUserId, vpGuarantorId);
            journalEventContext.HsGuarantorId = hsGuarantorId;
            journalEventContext.VisitId = visitId;

            this._eventJournalService.Value.AddEvent<EventSystemActionVisitRedacted, JournalEventParameters>(
                EventSystemActionVisitRedacted.GetParameters
                (
                    visitSourceSystemKey: visitSourceSystemKey,
                    vpVisitId: visitId.ToString()
                ),
                journalEventUserContext,
                journalEventContext);
        }

        public void LogVisitMatch(JournalEventUserContext journalEventUserContext, int hsGuarantorId, int vpGuarantorId, string visitSourceSystemKey, int visitId)
        {
            JournalEventContext journalEventContext = this.GetJournalEventContextForSystem(journalEventUserContext.EventVisitPayUserId, vpGuarantorId);
            journalEventContext.HsGuarantorId = hsGuarantorId;
            journalEventContext.VisitId = visitId;

            this._eventJournalService.Value.AddEvent<EventSystemActionVisitRematch, JournalEventParameters>(
                EventSystemActionVisitRematch.GetParameters
                (
                    hsGuarantorId: hsGuarantorId.ToString(),
                    visitSourceSystemKey: visitSourceSystemKey,
                    vpVisitId: visitId.ToString()
                ),
                journalEventUserContext,
                journalEventContext);
        }

        public void LogBillingIdFailure(JournalEventUserContext journalEventUserContext, int vpGuarantorId, string response)
        {
            JournalEventContext journalEventContext = this.GetJournalEventContextForSystem(journalEventUserContext.EventVisitPayUserId, vpGuarantorId);

            this._eventJournalService.Value.AddEvent<EventSystemActionBillingIdFailure, JournalEventParameters>(
                EventSystemActionBillingIdFailure.GetParameters
                (
                    accountNumber: "",
                    responseInfo: "",
                    tCResponse: response,
                    vpUserName: journalEventUserContext.UserName
                ),
                journalEventUserContext,
                journalEventContext);
        }

        public void LogResendCommunication(JournalEventUserContext journalEventUserContext, int vpGuarantorId, int originalCommunicationId, int resentCommunicationId, string communicationType, string vpGuarantorUserName)
        {
            VisitPayUser user = this._visitPayUserRepository.Value.FindById(journalEventUserContext.EventVisitPayUserId.ToString());
            if (user != null)
            {
                JournalEventContext journalEventContext = this.GetJournalEventContextForSystem(journalEventUserContext.EventVisitPayUserId, vpGuarantorId);

                this._eventJournalService.Value.AddEvent<EventClientResentAnEmail, JournalEventParameters>(
                    EventClientResentAnEmail.GetParameters
                    (
                        emailContentTitle: communicationType,
                        eventUserName: journalEventUserContext.UserName,
                        newEmailId: resentCommunicationId.ToString(),
                        previousEmailId: originalCommunicationId.ToString(),
                        vpUserName: vpGuarantorUserName
                    ),
                    journalEventUserContext,
                    journalEventContext);
            }
        }

        public void LogClientUserConnectedToChat(JournalEventUserContext journalEventUserContext, int vpGuarantorId, string vpGuarantorUserName, string chatThreadId)
        {
            if (journalEventUserContext.IsClientUser)
            {
                JournalEventContext journalEventContext = this.GetJournalEventContextForClient(journalEventUserContext.EventVisitPayUserId, vpGuarantorId);
                VisitPayUser eventUser = this._visitPayUserRepository.Value.FindById(journalEventUserContext.EventVisitPayUserId.ToString());
                this._eventJournalService.Value.AddEvent<EventClientUserConnectedToChatThread, JournalEventParameters>(
                    EventClientUserConnectedToChatThread.GetParameters
                    (
                        eventUserName: eventUser.UserName,
                        chatThreadId: chatThreadId,
                        vpUserName: vpGuarantorUserName
                    ),
                    journalEventUserContext,
                    journalEventContext);
            }
        }

        public void LogClientUserDisconnectedFromChat(JournalEventUserContext journalEventUserContext, int vpGuarantorId, string vpGuarantorUserName, string chatThreadId)
        {
            if (journalEventUserContext.IsClientUser)
            {
                JournalEventContext journalEventContext = this.GetJournalEventContextForClient(journalEventUserContext.EventVisitPayUserId, vpGuarantorId);
                VisitPayUser eventUser = this._visitPayUserRepository.Value.FindById(journalEventUserContext.EventVisitPayUserId.ToString());
                this._eventJournalService.Value.AddEvent<EventClientUserDisconnectedFromChatThread, JournalEventParameters>(
                    EventClientUserDisconnectedFromChatThread.GetParameters
                    (
                        eventUserName: eventUser.UserName,
                        chatThreadId: chatThreadId,
                        vpUserName: vpGuarantorUserName
                    ),
                    journalEventUserContext,
                    journalEventContext);
            }
        }

        public void LogClientUserEndedChat(JournalEventUserContext journalEventUserContext, int vpGuarantorId, string vpGuarantorUserName, string chatThreadId)
        {
            if (journalEventUserContext.IsClientUser)
            {
                JournalEventContext journalEventContext = this.GetJournalEventContextForClient(journalEventUserContext.EventVisitPayUserId, vpGuarantorId);
                VisitPayUser eventUser = this._visitPayUserRepository.Value.FindById(journalEventUserContext.EventVisitPayUserId.ToString());
                this._eventJournalService.Value.AddEvent<EventClientUserEndedChatThread, JournalEventParameters>(
                    EventClientUserEndedChatThread.GetParameters
                    (
                        eventUserName: eventUser.UserName,
                        chatThreadId: chatThreadId,
                        vpUserName: vpGuarantorUserName
                    ),
                    journalEventUserContext,
                    journalEventContext);
            }
        }

        public void LogClientUserResetAccount(JournalEventUserContext journalEventUserContext, int targetUserId)
        {
            JournalEventContext journalEventContext = this.GetJournalEventContextForClient(journalEventUserContext.EventVisitPayUserId, null);
            VisitPayUser targetUser = this._visitPayUserRepository.Value.FindById(targetUserId.ToString());

            if (journalEventUserContext.IsIvinciUser)
            {
                this._eventJournalService.Value.AddEvent<EventClientResetAClientUserAcct, JournalEventParameters>(
                    EventClientResetAClientUserAcct.GetParameters
                    (
                        clientUserName: targetUser.UserName,
                        eventUserName: journalEventUserContext.UserName
                    ),
                    journalEventUserContext,
                    journalEventContext);
            }
            else
            {
                this._eventJournalService.Value.AddEvent<EventClientManagerResetAClientUserAccount, JournalEventParameters>(
                    EventClientManagerResetAClientUserAccount.GetParameters
                    (
                        userName: targetUser.UserName,
                        eventUserName: journalEventUserContext.UserName
                    ),
                    journalEventUserContext,
                    journalEventContext);
            }
        }

        public void LogGuarantorMismatchIgnored(JournalEventUserContext journalEventUserContext, int hsGuarantorId, int vpGuarantorId)
        {
            JournalEventContext journalEventContext = this.GetJournalEventContextForClient(journalEventUserContext.EventVisitPayUserId, vpGuarantorId);
            journalEventContext.HsGuarantorId = hsGuarantorId;

            this._eventJournalService.Value.AddEvent<EventClientGuarantorMismatchIgnored, JournalEventParameters>(
                EventClientGuarantorMismatchIgnored.GetParameters
                (
                    eventUserName: journalEventUserContext.UserName
                ),
                journalEventUserContext,
                journalEventContext);
        }

        public void LogReactivateAccountEvent(JournalEventUserContext journalEventUserContext, int targetUserId, int vpGuarantorId)
        {
            JournalEventContext journalEventContext = this.GetJournalEventContextForClient(journalEventUserContext.EventVisitPayUserId, vpGuarantorId);
            VisitPayUser targetUser = this._visitPayUserRepository.Value.FindById(targetUserId.ToString());

            this._eventJournalService.Value.AddEvent<EventClientReactivatedVpGuarantorAccount, JournalEventParameters>(
                EventClientReactivatedVpGuarantorAccount.GetParameters
                (
                    vpUserName: targetUser.UserName,
                    eventUserName: journalEventUserContext.UserName
                ),
                journalEventUserContext,
                journalEventContext);
        }

        public void LogCancelAccountEvent(JournalEventUserContext journalEventUserContext, int targetUserId, int vpGuarantorId, string reason = "", string note = "")
        {
            JournalEventContext journalEventContext = this.GetJournalEventContextForClient(journalEventUserContext.EventVisitPayUserId, vpGuarantorId);
            VisitPayUser targetUser = this._visitPayUserRepository.Value.FindById(targetUserId.ToString());

            this._eventJournalService.Value.AddEvent<EventClientClosedVpGuarantorAccount, JournalEventParameters>(
                EventClientClosedVpGuarantorAccount.GetParameters
                (
                    cancelReason: reason,
                    eventUserName: journalEventUserContext.UserName,
                    vpUserName: targetUser.UserName
                ),
                journalEventUserContext,
                journalEventContext);
        }

        public void LogSystemActionEmailSent(string emailId, string notificationEmail, string typeName, int? sentToUserId, int vpGuarantorId)
        {
            JournalEventUserContext journalEventUserContext = this.GetJournalEventUserContext(null, SystemUsers.SystemUserId);
            JournalEventContext journalEventContext = this.GetJournalEventContextForSystem(journalEventUserContext.EventVisitPayUserId, vpGuarantorId == 0 ? null : (int?)vpGuarantorId);

            VisitPayUser targetUser = null;
            if (sentToUserId != null)
            {
                targetUser = this._visitPayUserRepository.Value.FindById(sentToUserId.ToString());
            }

            this._eventJournalService.Value.AddEvent<EventSystemActionEmailSent, JournalEventParameters>(
                EventSystemActionEmailSent.GetParameters
                (
                    emailContentTitle: typeName,
                    emailId: emailId,
                    notificationEmail: notificationEmail,
                    typeName: typeName,
                    vpUserName: targetUser?.UserName ?? ""
                ),
                journalEventUserContext,
                journalEventContext);
        }

        public void LogLoginOffline(JournalEventUserContext journalEventUserContext, int? vpGuarantorId = null)
        {
            JournalEventContext journalEventContext = this.GetJournalEventContextForClient(journalEventUserContext.EventVisitPayUserId, vpGuarantorId);
            VisitPayUser targetUser = this._visitPayUserRepository.Value.FindById(journalEventUserContext.EventVisitPayUserId.ToString());

            this._eventJournalService.Value.AddEvent<EventGuarantorVpccLogin, JournalEventParameters>(
                EventGuarantorVpccLogin.GetParameters
                (
                    vpUserName: targetUser.UserName,
                    applicationId: this.ApplicationSettingsService.Value.Application.Value.ToString(),
                    deviceType: journalEventUserContext.DeviceType.ToString(),
                    url: journalEventUserContext.Url
                ),
                journalEventUserContext,
                journalEventContext);
        }

        public void LogGuarantorSelfVerification(JournalEventUserContext journalEventUserContext, int? vpGuarantorId = null)
        {
            JournalEventContext journalEventContext = this.GetJournalEventContextForClient(journalEventUserContext.EventVisitPayUserId, vpGuarantorId);
            VisitPayUser targetUser = this._visitPayUserRepository.Value.FindById(journalEventUserContext.EventVisitPayUserId.ToString());

            this._eventJournalService.Value.AddEvent<EventGuarantorVpccSelfVerified, JournalEventParameters>(
                EventGuarantorVpccSelfVerified.GetParameters(targetUser.UserName),
                journalEventUserContext,
                journalEventContext);
        }

        public void LogClientOptedInCommunicationTypeForVpGuarantor(JournalEventUserContext journalEventUserContext, int visitPayUserId, CommunicationMethodEnum communicationMethod)
        {
            JournalEventContext journalEventContext = this.GetJournalEventContextForClient(journalEventUserContext.EventVisitPayUserId, null);
            VisitPayUser targetUser = this._visitPayUserRepository.Value.FindById(visitPayUserId.ToString());

            if (communicationMethod == CommunicationMethodEnum.Email)
            {
                this._eventJournalService.Value.AddEvent<EventClientEnabledElectronicCommunications, JournalEventParameters>(
                    EventClientEnabledElectronicCommunications.GetParameters(journalEventUserContext.UserName, targetUser.UserName),
                    journalEventUserContext,
                    journalEventContext);
            }
            else if(communicationMethod == CommunicationMethodEnum.Mail)
            {
                this._eventJournalService.Value.AddEvent<EventClientEnabledPaperCommunications, JournalEventParameters>(
                    EventClientEnabledPaperCommunications.GetParameters(journalEventUserContext.UserName, targetUser.UserName),
                    journalEventUserContext,
                    journalEventContext);
            }

        }

        public void LogClientOptedOutCommunicationTypeForVpGuarantor(JournalEventUserContext journalEventUserContext, int visitPayUserId, CommunicationMethodEnum communicationMethod)
        {
            JournalEventContext journalEventContext = this.GetJournalEventContextForClient(journalEventUserContext.EventVisitPayUserId, null);
            VisitPayUser targetUser = this._visitPayUserRepository.Value.FindById(visitPayUserId.ToString());

            if (communicationMethod == CommunicationMethodEnum.Email)
            {
                this._eventJournalService.Value.AddEvent<EventClientOptedVpGuarantorOutOfElectronicCommunications, JournalEventParameters>(
                    EventClientOptedVpGuarantorOutOfElectronicCommunications.GetParameters(journalEventUserContext.UserName, targetUser.UserName),
                    journalEventUserContext,
                    journalEventContext);
            }
            else if (communicationMethod == CommunicationMethodEnum.Mail)
            {
                this._eventJournalService.Value.AddEvent<EventClientOptedVpGuarantorOutOfPaperCommunications, JournalEventParameters>(
                    EventClientOptedVpGuarantorOutOfPaperCommunications.GetParameters(journalEventUserContext.UserName, targetUser.UserName),
                    journalEventUserContext,
                    journalEventContext);
            }

        }
        #endregion

    }
}

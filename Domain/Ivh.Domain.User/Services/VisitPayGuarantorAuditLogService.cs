namespace Ivh.Domain.User.Services
{
    using System.Collections.Generic;
    using Entities;
    using Interfaces;

    public class VisitPayGuarantorAuditLogService :  IVisitPayGuarantorAuditLogService
    {
        private readonly IVisitPayGuarantorAuditLogRepository _visitPayGuarantorAuditLogRepository;

        public VisitPayGuarantorAuditLogService(IVisitPayGuarantorAuditLogRepository visitPayGuarantorAuditLogRepository)
           
        {
            this._visitPayGuarantorAuditLogRepository = visitPayGuarantorAuditLogRepository;
        }

        public IReadOnlyList<VisitPayGuarantorAuditLog> GetVisitPayGuarantorAuditLog(int vpGuarantorId)
        {
            return this._visitPayGuarantorAuditLogRepository.GetVisitPayGuarantorAuditLog(vpGuarantorId);
        }
    }
}
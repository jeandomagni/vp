﻿namespace Ivh.Domain.User.Services
{
    using System;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Entities;
    using Interfaces;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.Owin;
    using Microsoft.Owin.Security;

    public class VisitPaySignInService : SignInManager<VisitPayUser, string>
    {
        private const bool IsPersistent = false;
        private const bool RememberBrowser = false;

        private readonly VisitPayUserService _userManager;
        private readonly Lazy<IVisitPayUserService> _visitPayUserService;

        public VisitPaySignInService(
            VisitPayUserService userManager,
            Lazy<IVisitPayUserService> visitPayUserService,
            IAuthenticationManager authenticationManager = null // optional to allow jobrunner and console apps to use this service
        )
            : base(userManager, authenticationManager)
        {
            this._userManager = userManager;
            this._visitPayUserService = visitPayUserService;
        }

        public override async Task<ClaimsIdentity> CreateUserIdentityAsync(VisitPayUser user)
        {
            return await this._userManager.GenerateUserIdentityAsync(user).ConfigureAwait(false);
        }

        public void SignIn(ClaimsIdentity identity)
        {
            this.AuthenticationManager.SignIn(new AuthenticationProperties() {IsPersistent = false}, identity);
        }

        public override async Task<SignInStatus> PasswordSignInAsync(string userName, string password, bool isPersistent, bool shouldLockout)
        {
            if (this.UserManager == null)
            {
                return SignInStatus.Failure;
            }

            VisitPayUser user = await this.UserManager.FindByNameAsync(userName);
            if (user == null)
            {
                return SignInStatus.Failure;
            }

            if (await this.UserManager.IsLockedOutAsync(user.Id))
            {
                return SignInStatus.LockedOut;
            }

            if (await this._visitPayUserService.Value.VerifyPasswordAsync(user, password).ConfigureAwait(true))
            {
                return await this.SignInOrTwoFactor(user, isPersistent, VisitPaySignInService.RememberBrowser);
            }

            if (shouldLockout)
            {
                // If lockout is requested, increment access failed count which might lock out the user
                await this.UserManager.AccessFailedAsync(user.Id);
                if (await this.UserManager.IsLockedOutAsync(user.Id))
                {
                    return SignInStatus.LockedOut;
                }
            }

            return SignInStatus.Failure;
        }

        private async Task<SignInStatus> SignInOrTwoFactor(VisitPayUser user, bool isPersistent, bool rememberBrowser)
        {
            string id = Convert.ToString(user.Id);
            if (await this.UserManager.GetTwoFactorEnabledAsync(user.Id)
                && (await this.UserManager.GetValidTwoFactorProvidersAsync(user.Id)).Count > 0
                && !await this.AuthenticationManager.TwoFactorBrowserRememberedAsync(id))
            {
                ClaimsIdentity identity = new ClaimsIdentity(DefaultAuthenticationTypes.TwoFactorCookie);
                identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, id));
                this.AuthenticationManager.SignIn(identity);
                return SignInStatus.RequiresVerification;
            }

            await this.SignInAsync(user, isPersistent, rememberBrowser);
            return SignInStatus.Success;
        }

        public SignInStatus SignInSimple(VisitPayUser user, bool isPersistent, bool rememberBrowser)
        {
            this.SignIn(user, isPersistent, rememberBrowser);
            return SignInStatus.Success;
        }
    }
}
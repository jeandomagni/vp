﻿namespace Ivh.Domain.User.Services
{
    using Application.Base.Common.Dtos;
    using Entities;
    using Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class VisitPayUserAddressVerificationService : IVisitPayUserAddressVerificationService
    {
        private readonly Lazy<IVisitPayUserAddressVerificationProvider> _visitPayUserAddressVerificationProvider;

        public VisitPayUserAddressVerificationService(
            Lazy<IVisitPayUserAddressVerificationProvider> visitPayUserAddressVerificationProvider)
        {
            this._visitPayUserAddressVerificationProvider = visitPayUserAddressVerificationProvider;
        }

        public async Task<DataResult<IEnumerable<string>, bool>> GetStandardizedMailingAddressAsync(
            int vpUserId,
            VisitPayUserAddress vpUserAddress,
            bool tryGetFromCache)
        {
            return
                await this._visitPayUserAddressVerificationProvider.Value
                    .GetStandardizedMailingAddressAsync(vpUserId, vpUserAddress, tryGetFromCache);
        }
    }
}

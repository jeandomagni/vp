﻿namespace Ivh.Domain.User.Services
{
    using System.Threading.Tasks;
    using Interfaces;
    using Microsoft.AspNet.Identity;

    public class IdentityEmailService :  IIdentityEmailService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your email service here to send an email.
            return Task.FromResult(0);
        }
    }
}

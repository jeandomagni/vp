﻿namespace Ivh.Domain.User.Services
{
    using System;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.Core;
    using Interfaces;

    public class VisitPayUserDetailChangedService : DomainService, IVisitPayUserDetailChangedService
    {
        private readonly Lazy<IVisitPayUserDetailChangedRepository> _visitPayUserDetailChangedRepository;

        public VisitPayUserDetailChangedService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IVisitPayUserDetailChangedRepository> visitPayUserDetailChangedRepository) : base(serviceCommonService)
        {
            this._visitPayUserDetailChangedRepository = visitPayUserDetailChangedRepository;
        }

        public void VisitPayUserAddressChanged(int visitPayUserId)
        {
            if (this._visitPayUserDetailChangedRepository.Value.DuplicateVisitPayerUserAddressExists(visitPayUserId))
            {
                this.Bus.Value.PublishMessage(new AddSystemMessageVisitPayUserMessage { VisitPayUserId = visitPayUserId, SystemMessageEnum = SystemMessageEnum.Consolidate }).Wait();
            }
        }
    }
}
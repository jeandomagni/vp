﻿namespace Ivh.Domain.User.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.VisitPay.Strings;
    using Entities;
    using Interfaces;

    public class VisitPayRoleService :  IVisitPayRoleService
    {
        private readonly IVisitPayRoleRepository _visitPayRoleRepository;

        private static readonly List<string> Roles = new List<string>
        {
            VisitPayRoleStrings.Csr.ArrangePayment,
            VisitPayRoleStrings.Csr.ChangePaymentDueDay,
            VisitPayRoleStrings.Csr.CancelRecurringPayment,
            VisitPayRoleStrings.Csr.CancelFP,
            VisitPayRoleStrings.Csr.CSR,
            VisitPayRoleStrings.Csr.ExtendedFinancePlanOfferSets,
            VisitPayRoleStrings.Csr.ManagePaymentMethods,
            VisitPayRoleStrings.Csr.ReconfigureFP,
            VisitPayRoleStrings.Csr.RescheduleRecurringPayment,
            VisitPayRoleStrings.Csr.ResetVisitAging,
            VisitPayRoleStrings.Csr.SupportAdmin,
            VisitPayRoleStrings.Csr.SupportCreate,
            VisitPayRoleStrings.Csr.SupportTemplateAdmin,
            VisitPayRoleStrings.Csr.SupportView,
            VisitPayRoleStrings.Csr.UserAdmin,
            VisitPayRoleStrings.Csr.CallCenter,
            VisitPayRoleStrings.Csr.ChatOperator,
            VisitPayRoleStrings.Csr.ChatOperatorAdmin,
            VisitPayRoleStrings.Csr.PaymentQueue,
            VisitPayRoleStrings.Payment.PaymentRefund,
            VisitPayRoleStrings.Payment.PaymentRefundReturnedCheck,
            VisitPayRoleStrings.Payment.PaymentVoid,
            VisitPayRoleStrings.Payment.CardReader,
            VisitPayRoleStrings.Miscellaneous.ClientUserManagement,
            VisitPayRoleStrings.Miscellaneous.IvhAdmin,
            VisitPayRoleStrings.Miscellaneous.IvhEmployee,
            VisitPayRoleStrings.Miscellaneous.Reports,
            VisitPayRoleStrings.Miscellaneous.Unmatch,
            VisitPayRoleStrings.Miscellaneous.VpSupportTickets,
            VisitPayRoleStrings.Miscellaneous.VpSupportTicketsManage,
            VisitPayRoleStrings.Miscellaneous.VpSupportTicketAdmin,
            VisitPayRoleStrings.Miscellaneous.VisitPayDocument,
            VisitPayRoleStrings.Restricted.AuditReviewer,
            VisitPayRoleStrings.Restricted.ClientUserAuditor,
            VisitPayRoleStrings.Restricted.ClientPciAuditor,
            VisitPayRoleStrings.Restricted.IvhUserAuditor,
            VisitPayRoleStrings.Restricted.IvhPciAuditor,
            VisitPayRoleStrings.Restricted.AuditEventLogViewer,
            VisitPayRoleStrings.Restricted.VisitPayReports
        };

        private static readonly List<string> UnRestrictedClientRoles = new List<string>
        {
            VisitPayRoleStrings.Csr.ArrangePayment,
            VisitPayRoleStrings.Csr.CancelRecurringPayment,
            VisitPayRoleStrings.Csr.CancelFP,
            VisitPayRoleStrings.Csr.ChangePaymentDueDay,
            VisitPayRoleStrings.Csr.CSR,
            VisitPayRoleStrings.Csr.ExtendedFinancePlanOfferSets,
            VisitPayRoleStrings.Csr.ManagePaymentMethods,
            VisitPayRoleStrings.Csr.ReconfigureFP,
            VisitPayRoleStrings.Csr.RescheduleRecurringPayment,
            VisitPayRoleStrings.Csr.ResetVisitAging,
            VisitPayRoleStrings.Csr.SupportAdmin,
            VisitPayRoleStrings.Csr.SupportCreate,
            VisitPayRoleStrings.Csr.SupportView,
            VisitPayRoleStrings.Csr.UserAdmin,
            VisitPayRoleStrings.Payment.PaymentRefund,
            VisitPayRoleStrings.Payment.PaymentVoid,
            VisitPayRoleStrings.Payment.CardReader,
            VisitPayRoleStrings.Miscellaneous.ClientUserManagement,
            VisitPayRoleStrings.Miscellaneous.Reports,
            VisitPayRoleStrings.Miscellaneous.Unmatch,
            VisitPayRoleStrings.Miscellaneous.VpSupportTickets,
            VisitPayRoleStrings.Miscellaneous.VpSupportTicketsManage
        };

        public VisitPayRoleService(IVisitPayRoleRepository visitPayRoleRepository)
        {
            this._visitPayRoleRepository = visitPayRoleRepository;
        }

        public VisitPayRole GetRoleByName(string value)
        {
            VisitPayRole role = this._visitPayRoleRepository.GetRoleByName(value);
            if (role == null)
            {
                VisitPayRole visitPayRole = new VisitPayRole(){Name = value};
                this._visitPayRoleRepository.InsertOrUpdate(visitPayRole);
            }
            return role;
        }

        public IList<VisitPayRole> GetClientUserRoles(bool isIvhAdmin)
        {
            return isIvhAdmin ? this._visitPayRoleRepository.GetQueryable().Where(x => Roles.Contains(x.Name)).ToList() : this._visitPayRoleRepository.GetQueryable().Where(x => UnRestrictedClientRoles.Contains(x.Name)).ToList();
        }

    }
}
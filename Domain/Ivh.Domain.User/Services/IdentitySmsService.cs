namespace Ivh.Domain.User.Services
{
    using System.Threading.Tasks;
    using Interfaces;
    using Microsoft.AspNet.Identity;

    public class IdentitySmsService :  IIdentitySmsService
    {
        public IdentitySmsService()
        {
            
        }

        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your SMS service here to send a text message.
            return Task.FromResult(0);
        }
    }
}
﻿namespace Ivh.Domain.User.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using Entities;
    using Interfaces;

    public class SecurityQuestionService :  ISecurityQuestionService
    {
        private readonly ISecurityQuestionRepository _securityQuestionRepository;

        public SecurityQuestionService(
            ISecurityQuestionRepository securityQuestionRepository)
        {
            this._securityQuestionRepository = securityQuestionRepository;
        }

        public IList<SecurityQuestion> GetAllSecurityQuestions()
        {
            return this._securityQuestionRepository.GetQueryable().ToList();
        }
    }
}

﻿namespace Ivh.Domain.User.Entities
{
    using Ivh.Application.Core.Common.Interfaces;
    using System;
    using System.Text.RegularExpressions;

    public class AccessTokenPhiValueGenerator : IIdentifierValueSource<int>
    {
        public const string ValidationPatternRegularExpression = "^[1-9]{1}[0-9]{3}-?[0-9]{4}$";
        public const string PrintFormat = "####-####";

        private const int RangeMax = 99999999;
        private const int RangeMin = 10000000;
        private static readonly Random _randomInstance = new Random();

        public Regex ValidationPattern { get; } = new Regex(ValidationPatternRegularExpression);

        public int GenerateIdentifier() => _randomInstance.Next(RangeMin, RangeMax);
    }
}
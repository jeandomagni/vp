namespace Ivh.Domain.User.Entities
{
    public class SecurityQuestion
    {
        public virtual int SecurityQuestionId { get; set; }
        public virtual string Question { get; set; }
        public virtual bool IsActive { get; set; }
    }
}
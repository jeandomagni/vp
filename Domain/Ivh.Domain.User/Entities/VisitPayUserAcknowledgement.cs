﻿namespace Ivh.Domain.User.Entities
{
    using System;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class VisitPayUserAcknowledgement
    {
        public virtual int VisitPayUserAcknowledgementId { get; set; }
        public virtual VisitPayUser VisitPayUser { get; set; }
        public virtual int CmsVersionId { get; set; }
        public virtual int? ClientCmsVersionId { get; set; }
        public virtual DateTime AcknowledgementDate { get; set; }
        public virtual VisitPayUserAcknowledgementTypeEnum VisitPayUserAcknowledgementType { get; set; }
        public virtual SmsPhoneTypeEnum? SmsPhoneType { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as VisitPayUserAcknowledgement;

            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return this.VisitPayUser == other.VisitPayUser &&
                this.CmsVersionId == other.CmsVersionId;
        }

        protected bool Equals(VisitPayUserAcknowledgement other)
        {
            return Equals(this.VisitPayUser, other.VisitPayUser) && Equals(this.CmsVersionId, other.CmsVersionId) && Equals(this.ClientCmsVersionId, other.ClientCmsVersionId) && this.AcknowledgementDate.Equals(other.AcknowledgementDate);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = this.VisitPayUser?.GetHashCode() ?? 0;
                hashCode = (hashCode*397) ^ (this.CmsVersionId.GetHashCode());
                hashCode = (hashCode*397) ^ (this.ClientCmsVersionId.GetHashCode());
                hashCode = (hashCode*397) ^ this.AcknowledgementDate.GetHashCode();
                return hashCode;
            }
        }
    }
}
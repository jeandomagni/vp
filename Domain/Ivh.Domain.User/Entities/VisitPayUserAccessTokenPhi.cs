﻿namespace Ivh.Domain.User.Entities
{
    using Ivh.Application.Core.Common.Interfaces;
    using Ivh.Application.Core.Common.Models.Identifiers;
    using Ivh.Common.Base.Utilities.Extensions;
    using System.Text.RegularExpressions;

    public class VisitPayUserAccessTokenPhi : VisitPayPublicIdentifier<int>
    {
        public override int RawValue { get; }
        public override IIdentifierValueSource<int> ValueGenerator { get; }

        public VisitPayUserAccessTokenPhi(
            IIdentifierValueSource<int> valueGenerator)
        {
            this.ValueGenerator = valueGenerator;
            this.RawValue = this.ValueGenerator.GenerateIdentifier();
        }

        public static bool IsValid(string tokenValue)
        {
            if (tokenValue.IsNullOrEmpty())
            {
                return false;
            }

            Regex validFormat = new AccessTokenPhiValueGenerator().ValidationPattern;
            return validFormat.IsMatch(tokenValue);
        }
    }
}
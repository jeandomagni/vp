﻿namespace Ivh.Domain.User.Entities
{
    public class VisitPayUserLogin
    {
        public virtual int VisitPayUserId { get; set; }
        public virtual string LoginProvider { get; set; }

        public virtual string ProviderKey { get; set; }
        public virtual int VisitPayUserLoginId { get; set; }
    }
}
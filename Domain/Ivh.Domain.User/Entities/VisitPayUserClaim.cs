﻿namespace Ivh.Domain.User.Entities
{
    public class VisitPayUserClaim
    {
        public virtual int VisitPayUserClaimId { get; set; }
        public virtual string ClaimType { get; set; }

        public virtual string ClaimValue { get; set; }

        public virtual VisitPayUser User { get; set; }
    }
}
﻿namespace Ivh.Domain.User.Entities
{
    using System;

    public class VisitPayGuarantorAuditLog
    {
        public int UserEventHistoryId { get; set; }
        public DateTime EventDate { get; set; }
        public string UserName { get; set; }
        public int UserId { get; set; }
        public string IpAddress { get; set; }
        public string UserAgent { get; set; }
        public int EmulateUserId { get; set; }
        public int VpGuarantorId { get; set; }
        public string EventType { get; set; }

    }
}
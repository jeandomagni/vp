﻿namespace Ivh.Domain.User.Entities
{
    using System;

    public class VpUserLoginSummary
    {
        public virtual int VpUserId { get; set; }
        public virtual DateTime LastLoginDate { get; set; }
    }
}

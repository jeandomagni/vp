﻿namespace Ivh.Domain.User.Entities
{
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Microsoft.AspNet.Identity;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    public class VisitPayUser : IUser<string>
    {
        public VisitPayUser()
        {
            this.InsertDate = DateTime.UtcNow;
            this.VisitPayUserGuid = Guid.NewGuid();
        }

        private ICollection<VisitPayUserAcknowledgement> _acknowledgements = new List<VisitPayUserAcknowledgement>();
        private ICollection<VisitPayUserClaim> _claims = new List<VisitPayUserClaim>();
        private ICollection<VisitPayUserLogin> _logins = new List<VisitPayUserLogin>();
        private ICollection<VisitPayRole> _roles = new List<VisitPayRole>();
        private ICollection<SecurityQuestionAnswer> _securityQuestionAnswers = new List<SecurityQuestionAnswer>();
        private readonly ICollection<VisitPayUserAddress> _addresses = new List<VisitPayUserAddress>();
        private string _accessTokenPhi;

        #region Root

        public virtual int VisitPayUserId { get; set; }

        public virtual Guid VisitPayUserGuid { get; set; }

        public virtual string TempPassword { get; set; }
        public virtual DateTime? TempPasswordExpDate { get; set; }

        public virtual string EmulateToken { get; set; }
        public virtual DateTime? EmulateExpDate { get; set; }
        public virtual int? EmulateUserId { get; set; }

        [GuarantorMatchField("FirstName")]
        public virtual string FirstName { get; set; }
        [GuarantorMatchField("MiddleName")]
        public virtual string MiddleName { get; set; }
        [GuarantorMatchField("LastName", uiDisplayName: "Last Name", uiDisplayOrder: 2)]
        public virtual string LastName { get; set; }

        [GuarantorMatchField("Gender")]
        public virtual string Gender { get; set; }
        [GuarantorMatchField("SSN4", uiDisplayOrder: 0)]
        public virtual int? SSN4 { get; set; }
        [GuarantorMatchField("DOB", uiDisplayOrder: 1)]
        public virtual DateTime? DateOfBirth { get; set; }

        public virtual string Locale { get; set; }

        public virtual string SecurityValidationValue { get; set; }

        public virtual DateTime? LastLoginDate
        {
            get { return this.VpUserLoginSummary != null ? this.VpUserLoginSummary.OrderByDescending(x => x.LastLoginDate).Select(x => (DateTime?)x.LastLoginDate).FirstOrDefault() : (DateTime?)null; }
        }

        public virtual ICollection<VpUserLoginSummary> VpUserLoginSummary { get; set; }

        public virtual DateTime InsertDate { get; set; }

        public virtual bool PhoneNumberTypeIsMobile => IsMobileType(this.PhoneNumberType);

        public virtual bool PhoneNumberSecondaryTypeIsMobile => IsMobileType(this.PhoneNumberSecondaryType);

        private static bool IsMobileType(string type)
        {
            return (type ?? "").ToUpper() == "MOBILE";
        }

        public virtual string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);

        public virtual string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);

        #endregion

        #region Identity user

        public virtual int AccessFailedCount { get; set; }

        public virtual string Email { get; set; }

        public virtual bool EmailConfirmed { get; set; }

        public virtual bool LockoutEnabled { get; set; }

        public virtual DateTime? LockoutEndDateUtc { get; set; }

        public virtual LockoutReasonEnum? LockoutReasonEnum { get; set; }

        public virtual string PasswordHash { get; set; }

        public virtual string PhoneNumber { get; set; }
        public virtual string PhoneNumberType { get; set; }

        public virtual string SmsPhoneNumber
        {
            get
            {
                VisitPayUserAcknowledgement ack = this.Acknowledgements.FirstOrDefault(x => x.VisitPayUserAcknowledgementType == VisitPayUserAcknowledgementTypeEnum.SmsAccept);
                if (ack == null)
                {
                    return string.Empty;
                }

                string smsPhoneNumber = ack.SmsPhoneType == SmsPhoneTypeEnum.Primary ? this.PhoneNumber : this.PhoneNumberSecondary;
                return smsPhoneNumber;
            }
            set { }
        }

        public virtual bool PhoneNumberConfirmed { get; set; }
        public virtual string PhoneNumberSecondary { get; set; }
        public virtual string PhoneNumberSecondaryType { get; set; }

        public virtual bool TwoFactorEnabled { get; set; }

        public virtual string SecurityStamp { get; set; }

        public virtual ICollection<VisitPayRole> Roles
        {
            get => this._roles;
            protected set
            {
                this._roles = value;
                this.EnsureThatPatientUsersDontHaveOtherRoles();
            }
        }

        public virtual ICollection<VisitPayUserAcknowledgement> Acknowledgements
        {
            get { return this._acknowledgements; }
            protected set { this._acknowledgements = value; }
        }

        public virtual ICollection<VisitPayUserClaim> Claims
        {
            get { return this._claims; }
            protected set { this._claims = value; }
        }

        public virtual ICollection<VisitPayUserLogin> Logins
        {
            get { return this._logins; }
            protected set { this._logins = value; }
        }

        public virtual ICollection<SecurityQuestionAnswer> SecurityQuestionAnswers
        {
            get { return this._securityQuestionAnswers; }
            set { this._securityQuestionAnswers = value; }
        }

        public virtual IList<VisitPayUserAddress> VisitPayUserAddresses { get; set; } = new List<VisitPayUserAddress>();


        //As of VP-3853 Physical address is the default, migrated address type
        //and will be the default pointer for matching address fields  (until future alternatives are considered)
        //The current address compare for updating the address depends on this field (PatientAddressCompareString points to this field) 
        //Mailing type will be considered with paper statement implementation
        public virtual VisitPayUserAddress CurrentPhysicalAddress { get; set; }

        public virtual VisitPayUserAddress CurrentMailingAddress { get; set; }

        /// <summary>
        /// This will set the current address belonging to the VisitPayUserAddressType of the VisitPayUserAddress.
        /// VisitPay user can have a current address per VisitPayUserAddressType.
        /// </summary>
        /// <param name="visitPayUserAddress"></param>
        public virtual void SetCurrentAddress(VisitPayUserAddress visitPayUserAddress)
        {
            visitPayUserAddress.VisitPayUser = this;
            this.VisitPayUserAddresses.Add(visitPayUserAddress);

            switch (visitPayUserAddress.VisitPayUserAddressType)
            {
                case VisitPayUserAddressTypeEnum.Physical:
                    this.CurrentPhysicalAddress = visitPayUserAddress;
                    break;
                case VisitPayUserAddressTypeEnum.Mailing:
                    this.CurrentMailingAddress = visitPayUserAddress;
                    break;
            }
        }

        public virtual bool IsLocked
        {
            get
            {
                return this.LockoutEnabled &&
                       this.LockoutEndDateUtc.HasValue &&
                       !this.IsDisabled &&
                       DateTime.Compare(this.LockoutEndDateUtc.Value, DateTime.UtcNow) > 0;
            }
        }

        public virtual bool IsDisabled
        {
            get
            {
                return this.LockoutEnabled &&
                       this.LockoutEndDateUtc.HasValue &&
                       this.LockoutEndDateUtc.Value.Date == DateTime.MaxValue.Date;
            }
        }

        public virtual string Id
        {
            get { return this.VisitPayUserId.ToString(); }
        }

        public virtual string UserName { get; set; }

        private void EnsureThatPatientUsersDontHaveOtherRoles()
        {
            if (this._roles != null)
            {
                if (this._roles.Any(x => x.Name == VisitPayRoleStrings.System.Patient))
                {
                    if (this._roles.Count > 1)
                    {
                        VisitPayRole patientRole = this._roles.FirstOrDefault(x => x.Name == VisitPayRoleStrings.System.Patient);
                        this._roles.Clear();
                        this._roles.Add(patientRole);
                    }
                }
            }
        }

        #endregion

        public virtual DateTime? PasswordExpiresUtc { get; set; }

        public virtual int PasswordHashConfigurationVersionId { get; set; }

        public virtual void DisableLogin()
        {
            this.LockoutEnabled = true;
            this.LockoutEndDateUtc = DateTime.MaxValue;
            this.TempPassword = null;
            this.PasswordHash = null;
        }

        public virtual void EnableLogin()
        {
            this.LockoutEnabled = true;
            this.LockoutEndDateUtc = null;
            this.LockoutReasonEnum = null;
        }

        public virtual bool Verify(string lastName, DateTime dateOfBirth, int ssn4)
        {
            return this.LastName.Equals(lastName.TrimNullSafe(), StringComparison.CurrentCultureIgnoreCase)
                   && this.DateOfBirth.HasValue
                   && this.DateOfBirth.Value.Date == dateOfBirth.Date
                   && this.SSN4 == ssn4;
        }

        // Introduced with VP-2572
        public virtual bool VerifyWithoutSsn4(string lastName, string firstName, DateTime dateOfBirth)
        {
            return this.LastName.Equals(lastName.TrimNullSafe(), StringComparison.CurrentCultureIgnoreCase)
                   && this.DateOfBirth.HasValue
                   && this.DateOfBirth.Value.Date == dateOfBirth.Date
                   && this.FirstName.Equals(firstName.TrimNullSafe(), StringComparison.CurrentCultureIgnoreCase);
        }

        public virtual string PatientProfileCompareString()
        {
            VisitPayUserAddress currentAddress = this.CurrentPhysicalAddress;
            VisitPayUserAddress currentMailingAddress = this.CurrentMailingAddress;

            string[] fields =
            {
                this.FirstName,
                this.LastName,
                this.MiddleName,
                currentAddress?.AddressStreet1,
                currentAddress?.AddressStreet2,
                currentAddress?.City,
                currentAddress?.StateProvince,
                currentAddress?.PostalCode,
                currentMailingAddress?.AddressStreet1,
                currentMailingAddress?.AddressStreet2,
                currentMailingAddress?.City,
                currentMailingAddress?.StateProvince,
                currentMailingAddress?.PostalCode,
                this.PhoneNumber,
                this.PhoneNumberType,
                this.PhoneNumberSecondary,
                this.PhoneNumberSecondaryType,
                string.Join(",", this.SecurityQuestionAnswers.Select(x => string.Format("{0}:{1}", x.SecurityQuestion.SecurityQuestionId, x.Answer)))
            };

            return string.Join("", fields.Where(x => !string.IsNullOrEmpty(x)).Select(x => string.Concat("{", x.Trim(), "}")));
        }

        public virtual string PhysicalAddressCompareString()
        {
            return this.CurrentPhysicalAddress?.AddressCompareString() ?? string.Empty;
        }

        public virtual string MailingAddressCompareString()
        {
            return this.CurrentMailingAddress?.AddressCompareString() ?? string.Empty;
        }

        public virtual bool IsInClientRole
        {
            get { return this.Roles.Any(x => x.VisitPayRoleId == 3); }
        }

        /// <summary>
        ///     A unique value used in conjunction with other PHI to validate a user's identity.
        ///     This is used in paper communications.
        /// </summary>
        /// <remarks>
        ///     Though preferable for setter to be private
        ///     so that callers would be required to use GenerateAndSetAccessTokenPhi method,
        ///     nHibernate requires at least protected internal access for property setters.
        ///     Therefore, only assign the incoming value if it's in the valid format.
        /// </remarks>
        [Phi]
        public virtual string AccessTokenPhi
        {
            get => this._accessTokenPhi;

            set
            {
                if (value.IsNullOrEmpty())
                {
                    this._accessTokenPhi = null;
                    return;
                }

                bool isValid = VisitPayUserAccessTokenPhi.IsValid(value);
                this._accessTokenPhi = isValid ? value : null;
            }
        }

        /// <summary>
        ///     Sets the value of the <see cref="AccessTokenPhi"/> as the string representation
        ///     of a random, 8-digit integer with a minimum value of 10,000,000
        ///     (to avoid having to manage leading zeros).
        /// </summary>
        /// <remarks>
        ///     This is the preferred approach for assigning a value to <see cref="AccessTokenPhi"/>,
        ///     rather than setting the property directly,
        ///     but the ORM prohibits marking the setter as private.
        /// </remarks>
        public virtual void GenerateAndSetAccessTokenPhi()
        {
            this.AccessTokenPhi = this.GenerateValidAccessTokenPhiValue();
        }

        public virtual string GenerateValidAccessTokenPhiValue()
        {
            AccessTokenPhiValueGenerator valueGenerator = new AccessTokenPhiValueGenerator();
            VisitPayUserAccessTokenPhi token = new VisitPayUserAccessTokenPhi(valueGenerator);
            return token.RawValue.ToString();
        }

        public virtual string GetPrintFormattedAccessTokenPhi()
        {
            const string printFormat = AccessTokenPhiValueGenerator.PrintFormat;
            return Convert.ToInt32(this.AccessTokenPhi).ToString(printFormat, CultureInfo.InvariantCulture);
        }

        public virtual void AddAcknowledgement(VisitPayUserAcknowledgementTypeEnum acknowledgementType, int cmsVersionId)
        {
            this.Acknowledgements.Add(new VisitPayUserAcknowledgement
            {
                AcknowledgementDate = DateTime.UtcNow,
                VisitPayUserAcknowledgementType = acknowledgementType,
                CmsVersionId = cmsVersionId,
                VisitPayUser = this
            });
        }
    }
}
﻿namespace Ivh.Domain.User.Entities
{
    using System.Collections.Generic;
    using Microsoft.AspNet.Identity;

    public class VisitPayRole :  IRole
    {
        public virtual string Id
        {
            get { return this.VisitPayRoleId.ToString(); }
        }

        public virtual string Name { get; set; }

        public virtual int VisitPayRoleId { get; set; }
        public virtual IList<VisitPayUser> Users { get; protected set; }

        public VisitPayRole()
        {
            this.Users = new List<VisitPayUser>();
        }

        public virtual string VisitPayRoleDescription { get; set; }
    }
}
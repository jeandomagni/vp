﻿namespace Ivh.Domain.User.Entities
{
    using System;

    public class VisitPayUserPasswordHistory
    {
        public virtual int VisitPayUserPasswordHistoryId { get; set; }
        public virtual int VisitPayUserId { get; set; }
        public virtual string PasswordHash { get; set; }
        public virtual DateTime PasswordExpiresUtc { get; set; }
        public virtual int PasswordHashConfigurationVersionId { get; set; }
    }
}

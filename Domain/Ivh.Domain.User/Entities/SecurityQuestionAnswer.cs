namespace Ivh.Domain.User.Entities
{
    public class SecurityQuestionAnswer
    {
        public virtual int SecurityQuestionAnswerId { get; set; }
        public virtual SecurityQuestion SecurityQuestion { get; set; }
        public virtual string Answer { get; set; }
    }
}
﻿namespace Ivh.Domain.User.Entities
{
    using System;
    using System.Linq;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    public class VisitPayUserAddress
    {
        public virtual int VisitPayUserAddressId { get; set; }
        public virtual VisitPayUser VisitPayUser { get; set; }
        [GuarantorMatchField("AddressLine1")]
        public virtual string AddressStreet1 { get; set; }
        [GuarantorMatchField("AddressLine2")]
        public virtual string AddressStreet2 { get; set; }
        [GuarantorMatchField("City")]
        public virtual string City { get; set; }
        [GuarantorMatchField("StateProvince")]
        public virtual string StateProvince { get; set; }
        [GuarantorMatchField("PostalCode")]
        public virtual string PostalCode { get; set; }
        public virtual VisitPayUserAddressTypeEnum VisitPayUserAddressType { get; set; } = VisitPayUserAddressTypeEnum.Physical;
        public virtual bool? IsPrimary { get; set; }
        public virtual DateTime InsertDate { get; set; } = DateTime.UtcNow;
        public virtual AddressStatusEnum AddressStatus { get; set; } = AddressStatusEnum.Unverified;
        public virtual DateTime? VerificationCheckDate { get; set; }
        public virtual int AddressHash { get; set; }
        public virtual bool IsUspsVerifiedFormat { get; set; }
        public virtual string AddressCompareString()
        {
            string[] fields =
            {
                this.AddressStreet1,
                this.AddressStreet2,
                this.City,
                this.StateProvince,
                this.PostalCode,
            };

            return string.Join("", fields.Where(x => !string.IsNullOrWhiteSpace(x)).Select(x => string.Concat("{", x.Trim(), "}")));
        }

    }
}
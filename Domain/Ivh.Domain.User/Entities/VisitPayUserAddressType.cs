﻿namespace Ivh.Domain.User.Entities
{
    public class VisitPayUserAddressType
    {
        public virtual int VisitPayUserAddressTypeId { get; set; }
        public virtual string VisitPayUserAddressTypeName { get; set; }
    }
}
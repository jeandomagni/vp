﻿namespace Ivh.Domain.User.Interfaces
{
    using Application.Base.Common.Dtos;
    using Entities;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IVisitPayUserAddressVerificationProvider
    {
        Task<DataResult<IEnumerable<string>, bool>> GetStandardizedMailingAddressAsync(
            int vpUserId,
            VisitPayUserAddress vpUserAddress,
            bool tryGetFromCache);
    }
}

﻿namespace Ivh.Domain.User.Interfaces
{
    using Common.Base.Interfaces;

    public interface IVisitPayUserAddressRepository : IRepository<Entities.VisitPayUserAddress>
    {

    }
}

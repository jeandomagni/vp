namespace Ivh.Domain.User.Interfaces
{
    using Common.Base.Interfaces;
    using Microsoft.AspNet.Identity;

    public interface IIdentitySmsService : IDomainService, IIdentityMessageService
    {
    }
}
namespace Ivh.Domain.User.Interfaces
{
    using Application.User.Common.Dtos;
    using Entities;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IVisitPayUserRepository
    {
        VisitPayUser FindById(string userId);
        Task<VisitPayUser> FindByDetailsAsync(VisitPayUserFindByDetailsParametersDto visitPayUserFindByDetailsParameters);
        Task<VisitPayUser> FindByDetailsAsync(string firstName, string lastName, int yearOfBirth, string emailAddress);
        Task<VisitPayUser> FindByEmulateTokenAsync(string token);
        IReadOnlyList<VisitPayUser> FindByNameAndDob(string lastName, DateTime dateOfBirth);
        IReadOnlyList<VisitPayUser> FindByDetailsAndAccessTokenPhi(string lastName, DateTime dob, string accessTokenPhi);
        VisitPayUserResults GetVisitPayUsers(VisitPayUserFilter filter, int baseRoleId, int batch, int batchSize);
        IReadOnlyList<VisitPayUser> GetInactiveVisitPayClientUsers(DateTime lastLoginDate);
        bool IsAccountEnabled(int visitPayUserId);
        IList<VisitPayUser> GetVisitPayUsersWithSmsNumber(string phoneNumber);
        string GetUserLocale(string userId);
        void SetUserLocale(string userId, string locale);
        bool AccessTokenPhiValueExists(string accessTokenPhi, int visitPayUserId);

        #region unreferenced code

        //Task<VisitPayUser> FindByDetailsAsync(string lastName, DateTime dateOfBirth, int ssn4);

        #endregion
    }
}
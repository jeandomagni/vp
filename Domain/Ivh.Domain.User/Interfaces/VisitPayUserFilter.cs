﻿namespace Ivh.Domain.User.Interfaces
{
    using System;
    using System.Collections.Generic;

    public class VisitPayUserFilter
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string UserName { get; set; }
        public IList<int> RolesInclude { get; set; }
        public IList<int> RolesExclude { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public string SortField { get; set; }
        public string SortOrder { get; set; }
        public IList<int> VisitPayUserIds { get; set; }
        public bool? IsDisabled { get; set; }
        public bool IsNotIvhAdminOrIvhUserAuditor { get; set; }
        public bool IsNotClientUserManagementOrClientUserAuditor { get; set; }
    }
}

﻿namespace Ivh.Domain.User.Interfaces
{
    using System.Collections.Generic;
    using Entities;

    public class VisitPayUserResults
    {
        public IReadOnlyList<VisitPayUser> VisitPayUsers { get; set; }
        public int TotalRecords { get; set; }
    }
}

﻿namespace Ivh.Domain.User.Interfaces
{
    using System;
    using System.Threading.Tasks;
    using Common.Base.Interfaces;
    using Entities;
    using Microsoft.AspNet.Identity;

    public interface IVisitPayUserService : IDomainService
    {
        VisitPayUser FindUserById(int visitPayUserId);
        IdentityResult UpdateUser(VisitPayUser user);
        VisitPayUserResults GetVisitPayClientUsers(VisitPayUserFilter filter, int batch, int batchSize);
        void DeactivateInactiveVisitPayClientUsers();
        void SavePasswordHistory(int userId, string passwordHash, DateTime? passwordExpiresUtc, int? passwordHashConfiguration = null);
        void SetPasswordExpiration(VisitPayUser visitPayUser, int passwordExpLimitInDays);
        Task<bool> VerifyPasswordAsync(VisitPayUser user, string password);
        VisitPayUser FindByDetailsAndToken(string lastName, DateTime dateOfBirth, string token);
        VisitPayUser FindByDetailsAndAccessTokenPhi(string lastName, DateTime dob, string accessTokenPhi);
        string HashPasswordWithCurrentConfiguration(string password);
        void SetUserLocale(string userId, string locale);
        string GetUserLocale(string userId);
        void GenerateAccessTokenPhiForUser(int visitPayUserId);
        string GenerateOfflineLoginToken(VisitPayUser visitPayUser);
    }
}

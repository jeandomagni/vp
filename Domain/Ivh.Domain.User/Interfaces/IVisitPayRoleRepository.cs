﻿namespace Ivh.Domain.User.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IVisitPayRoleRepository : IRepository<VisitPayRole>
    {
        VisitPayRole GetRoleByName(string value);
    }
}
﻿namespace Ivh.Domain.User.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IVisitPayRoleService : IDomainService
    {
        VisitPayRole GetRoleByName(string value);
        IList<VisitPayRole> GetClientUserRoles(bool isIvhAdmin);
    }
}
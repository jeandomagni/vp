namespace Ivh.Domain.User.Interfaces
{
    using Common.Base.Interfaces;

    public interface ISecurityQuestionRepository : IRepository<Entities.SecurityQuestion>
    {
        
    }
}
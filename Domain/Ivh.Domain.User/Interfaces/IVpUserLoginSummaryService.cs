﻿namespace Ivh.Domain.User.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IVpUserLoginSummaryService : IDomainService
    {
        VpUserLoginSummary GetUserLoginSummary(int userId);
        void SetUserLastLogin(int userId);
    }
}

﻿namespace Ivh.Domain.User.Interfaces
{
    using Common.Base.Interfaces;
    using Common.EventJournal;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IVisitPayUserJournalEventService : IDomainService
    {
        JournalEventUserContext GetJournalEventUserContext(JournalEventHttpContextDto journalEventHttpContextDto, VisitPayUser visitPayUser);
        JournalEventUserContext GetJournalEventUserContext(JournalEventHttpContextDto journalEventHttpContextDto, int visitPayUserId);
        JournalEventContext GetJournalEventContextForGuarantor(int visitPayUserId, int vpGuarantorId);
        JournalEventContext GetJournalEventContextForClient(int visitPayUserId, int? vpGuarantorId);
        JournalEventContext GetJournalEventContextForSystem(int visitPayUserId, int? vpGuarantorId);
        JournalEventContext GetJournalEventContextForAdmin(int visitPayUserId);

        void LogClientUserConnectedToChat(JournalEventUserContext journalEventUserContext, int vpGuarantorId, string vpGuarantorUserName, string chatThreadId);
        void LogClientUserDisconnectedFromChat(JournalEventUserContext journalEventUserContext, int vpGuarantorId, string vpGuarantorUserName, string chatThreadId);
        void LogClientUserEndedChat(JournalEventUserContext journalEventUserContext, int vpGuarantorId, string vpGuarantorUserName, string chatThreadId);
        void LogClientUserResetAccount(JournalEventUserContext journalEventUserContext, int targetUserId);
        void LogClientCustomizeFinancePlan(JournalEventUserContext journalEventUserContext, int vpGuarantorId, string vpGuarantorUserName, int financePlanId, int financePlanOfferSetTypeId, bool isUpdate);
        void LogGuarantorCustomizeFinancePlan(JournalEventUserContext journalEventUserContext, JournalEventParameters parameters, int vpGuarantorId, string vpGuarantorUserName, int financePlanId, int financePlanOfferSetTypeId);
        void LogGuarantorAcceptFinancePlan(JournalEventUserContext journalEventUserContext, int vpGuarantorId, string vpGuarantorUserName, int financePlanId, int financePlanOfferSetTypeId, int termsCmsVersionId);
        void LogCancelFinancePlanEvent(JournalEventUserContext journalEventUserContext, int vpGuarantorId, string vpGuarantorUserName, int financePlanId, int financePlanOfferSetTypeId);
        void LogTransactionUpdate(JournalEventUserContext journalEventUserContext, int vpGuarantorId, int visitId, int hsGuarantorId, decimal transactionAmount, VpTransactionTypeEnum vpTransactionType, string message);
        void LogGuarantorMatch(JournalEventUserContext journalEventUserContext, int hsGuarantorId, int vpGuarantorId, string whenMatched);
        void LogGuarantorUnmatch(JournalEventUserContext journalEventUserContext, int hsGuarantorId, int vpGuarantorId);
        void LogGuarantorMismatchIgnored(JournalEventUserContext journalEventUserContext, int hsGuarantorId, int vpGuarantorId);
        void LogVisitUnmatch(JournalEventUserContext journalEventUserContext, int hsGuarantorId, int vpGuarantorId, string visitSourceSystemKey, int visitId);
        void LogVisitRedacted(JournalEventUserContext journalEventUserContext, int hsGuarantorId, int vpGuarantorId, string visitSourceSystemKey, int visitId);
        void LogVisitMatch(JournalEventUserContext journalEventUserContext, int hsGuarantorId, int vpGuarantorId, string visitSourceSystemKey, int visitId);
        void LogBillingIdFailure(JournalEventUserContext journalEventUserContext, int vpGuarantorId, string response);
        void LogResendCommunication(JournalEventUserContext journalEventUserContext, int vpGuarantorId, int originalCommunicationId, int resentCommunicationId, string communicationType, string vpGuarantorUserName);
        void LogReactivateAccountEvent(JournalEventUserContext journalEventUserContext, int targetUserId, int vpGuarantorId);
        void LogCancelReconfiguredFinancePlan(JournalEventUserContext journalEventUserContext, int vpGuarantorId, string vpGuarantorUserName, int financePlanId, int financePlanOfferSetTypeId);
        void LogReconfiguredFinancePlan(JournalEventUserContext journalEventUserContext, int vpGuarantorId, string vpGuarantorUserName, int financePlanId, int financePlanOfferSetTypeId, bool isUpdate);
        void LogGuarantorAcceptReconfiguredFinancePlan(JournalEventUserContext journalEventUserContext, int vpGuarantorId, string vpGuarantorUserName, int financePlanId, int financePlanOfferSetTypeId);
        void LogCancelAccountEvent(JournalEventUserContext journalEventUserContext, int targetUserId, int vpGuarantorId, string reason = "", string note = "");
        void LogLoginOffline(JournalEventUserContext journalEventUserContext, int? vpGuarantorId = null);
        void LogGuarantorSelfVerification(JournalEventUserContext journalEventUserContext, int? vpGuarantorId = null);
        void LogSystemActionEmailSent(string emailId, string notificationEmail, string typeName, int? sentToUserId, int vpGuarantorId);
        void LogClientAcceptedFinancePlanOnBehalfOfGuarantor(JournalEventUserContext journalEventUserContext, int vpGuarantorId, string vpGuarantorUserName, int financePlanId, bool isAutoPay);
        void LogClientOptedInCommunicationTypeForVpGuarantor(JournalEventUserContext journalEventUserContext, int visitPayUserId, CommunicationMethodEnum communicationType);
        void LogClientOptedOutCommunicationTypeForVpGuarantor(JournalEventUserContext journalEventUserContext, int visitPayUserId, CommunicationMethodEnum communicationType);

    }
}

﻿namespace Ivh.Domain.User.Interfaces
{
    using Application.Base.Common.Dtos;
    using Entities;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IVisitPayUserAddressVerificationService
    {
        /// <summary>
        /// Provides the given mailing address in standard format, or an error response if the internal call fails.
        /// </summary>
        Task<DataResult<IEnumerable<string>, bool>> GetStandardizedMailingAddressAsync(
            int vpUserId,
            VisitPayUserAddress visitPayUserAddress,
            bool tryGetFromCache);
    }
}

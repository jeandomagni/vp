﻿namespace Ivh.Domain.User.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Entities;

    public interface IVisitPayUserPasswordHistoryRepository
    {
        IList<VisitPayUserPasswordHistory> GetUserPasswordHistory(int visitPayUserId, int passwordReuseLimit);
        void SavePasswordHistory(int userId, string passwordHash, DateTime passwordExpiresUtc, int passwordHashConfigurationVersionId);
    }
}

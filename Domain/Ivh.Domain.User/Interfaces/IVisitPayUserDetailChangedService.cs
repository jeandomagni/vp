﻿namespace Ivh.Domain.User.Interfaces
{
    using Common.Base.Interfaces;

    public interface IVisitPayUserDetailChangedService : IDomainService
    {
        void VisitPayUserAddressChanged(int visitPayUserId);
    }
}
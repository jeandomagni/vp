﻿namespace Ivh.Domain.User.Interfaces
{
    using System.Collections.Generic;
    using Entities;

    public interface IVisitPayGuarantorAuditLogRepository
    {
        IReadOnlyList<VisitPayGuarantorAuditLog> GetVisitPayGuarantorAuditLog(int vpGuarantorId);
    }
}
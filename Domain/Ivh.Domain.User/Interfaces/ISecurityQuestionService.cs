﻿namespace Ivh.Domain.User.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface ISecurityQuestionService : IDomainService
    {
        IList<SecurityQuestion> GetAllSecurityQuestions();
    }
}
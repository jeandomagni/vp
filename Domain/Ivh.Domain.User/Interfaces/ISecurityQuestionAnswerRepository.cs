namespace Ivh.Domain.User.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface ISecurityQuestionAnswerRepository : IRepository<SecurityQuestionAnswer>
    {
    }
}
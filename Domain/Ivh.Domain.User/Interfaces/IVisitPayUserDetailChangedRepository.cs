﻿namespace Ivh.Domain.User.Interfaces
{
    public interface IVisitPayUserDetailChangedRepository
    {
        bool DuplicateVisitPayerUserAddressExists(int visitPayUserId);
    }
}
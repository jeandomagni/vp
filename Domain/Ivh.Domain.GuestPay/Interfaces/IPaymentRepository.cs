﻿namespace Ivh.Domain.GuestPay.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IPaymentRepository : IRepository<Payment>
    {
        Payment GetPayment(int paymentId, string enteredAuthenticationKey);
        Payment GetPayment(string paymentProcessorSourceKey);
        IList<Payment> GetPaymentByTransactionId(string transactionId, IList<PaymentStatusEnum> paymentStatuses = null);
    }
}
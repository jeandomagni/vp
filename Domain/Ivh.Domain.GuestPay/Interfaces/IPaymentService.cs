﻿namespace Ivh.Domain.GuestPay.Interfaces
{
    using System.Threading.Tasks;
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.EventJournal;
    using Common.VisitPay.Enums;
    using Entities;
    using System.Linq;

    public interface IPaymentService : IDomainService
    {
        Payment GetPayment(int paymentId, string enteredAuthenticationKey);
        Payment GetPayment(string paymentProcessorSourceKey);
        IQueryable<Payment> GetPaymentsQueryable();
        IList<Payment> GetPaymentByTransactionId(string transactionId, IList<PaymentStatusEnum> paymentStatuses = null);
        void InsertOrUpdate(Payment payment);
        void ProcessOptimisticAchSettlement(Payment gpPayment);
        Task<bool> ProcessPaymentAsync(Entities.Payment payment, JournalEventHttpContextDto context);      
        void PublishGuestPayPayment(Payment payment, AchSettlementTypeEnum achSettlementType = AchSettlementTypeEnum.None);
    }
}
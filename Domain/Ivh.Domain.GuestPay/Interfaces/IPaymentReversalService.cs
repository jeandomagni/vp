namespace Ivh.Domain.GuestPay.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IPaymentReversalService : IDomainService
    {
        void VoidPayment(Payment payment);
        void RefundPayment(Payment payment, decimal? amount = null);
    }
}
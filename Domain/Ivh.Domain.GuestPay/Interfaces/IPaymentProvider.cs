﻿namespace Ivh.Domain.GuestPay.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using FinanceManagement.Payment.Entities;
    using FinanceManagement.ValueTypes;
    using Payment = Entities.Payment;

    public interface IPaymentProvider : IPaymentProviderBase
    {
        Task<PaymentProcessorResponse> SubmitPaymentAsync(Entities.Payment payment, PaymentProcessor paymentProcessor);
        Task<BillingIdResult> VerifyPaymentMethodAsync(Entities.Payment payment, PaymentProcessor paymentProcessor);
        IReadOnlyDictionary<string, string> ParseResponse(string rawResponse);
    }
}
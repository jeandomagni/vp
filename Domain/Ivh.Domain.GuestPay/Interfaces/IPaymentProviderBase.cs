﻿namespace Ivh.Domain.GuestPay.Interfaces
{
    public interface IPaymentProviderBase
    {
        string PaymentProcessorName { get; }
        //string ProductName { get; }
    }
}
﻿namespace Ivh.Domain.GuestPay.Interfaces
{
    using System.Threading.Tasks;
    using FinanceManagement.Payment.Entities;

    public interface IPaymentReversalProvider : IPaymentProviderBase
    {
        Task<PaymentProcessorResponse> SubmitPaymentVoidAsync(Entities.Payment originalPayment, Entities.Payment reversalPayment, PaymentProcessor paymentProcessor);
        Task<PaymentProcessorResponse> SubmitPaymentRefundAsync(Entities.Payment originalPayment, Entities.Payment reversalPayment, PaymentProcessor paymentProcessor, decimal? amount = null);
    }
}
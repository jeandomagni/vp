﻿namespace Ivh.Domain.GuestPay.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IPaymentUnitAuthenticationRepository : IRepository<PaymentUnitAuthentication>
    {
        IList<PaymentUnitAuthentication> AuthenticatePaymentUnit(PaymentUnitAuthentication paymentUnitAuthentication, IList<string> authenticationFields);
        PaymentUnitAuthentication GetPaymentUnitAuthentication(int paymentUnitAuthenticationId, string paymentUnitSourceSystemKey);
        IList<PaymentUnitAuthentication> GetPaymentUnitAuthentications(string paymentUnitSourceSystemKey);
    }
}
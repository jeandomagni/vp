﻿namespace Ivh.Domain.GuestPay.Interfaces
{
    using System.Collections.Generic;
    using Application.GuestPay.Common.Dtos;
    using Common.Base.Interfaces;

    public interface IGuarantorAuthenticationService : IDomainService
    {
        IList<AuthenticationMatchFieldDto> GetGuarantorAuthenticationFields();
    }
}

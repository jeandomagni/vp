﻿namespace Ivh.Domain.GuestPay.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Ivh.Common.EventJournal;

    public interface IGuestPayJournalEventService : IDomainService
    {
        void LogPaymentMethodFailure(int paymentUnitAuthenticationId, string paymentUnitSourceSystemKey, string guarantorLastName, JournalEventHttpContextDto context, IReadOnlyDictionary<string, string> processorResponse);
    }
}
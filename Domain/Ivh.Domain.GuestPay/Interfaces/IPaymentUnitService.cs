﻿namespace Ivh.Domain.GuestPay.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IPaymentUnitService : IDomainService
    {
        PaymentUnitAuthentication AuthenticatePaymentUnit(PaymentUnitAuthentication paymentUnitAuthentication);
        PaymentUnitAuthentication GetPaymentUnitAuthentication(int paymentUnitAuthenicationId, string paymentUnitSourceSystemKey);
        IList<PaymentUnitAuthentication>  GetPaymentUnitAuthentications(string paymentUnitSourceSystemKey);
    }
}
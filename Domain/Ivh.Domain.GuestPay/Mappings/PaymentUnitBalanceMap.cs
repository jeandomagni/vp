﻿namespace Ivh.Domain.GuestPay.Mappings
{
    using FluentNHibernate.Mapping;
    using GuestPay.Entities;

    public class PaymentUniBalancetMap : ClassMap<PaymentUnitBalance>
    {
        public PaymentUniBalancetMap()
        {
            this.Schema("guestpay");
            this.Table("PaymentUnitBalance");
            this.Id(x => x.PaymentUnitBalanceId);
            this.Map(x => x.UpdateDate)
                .Generated.Always();
           
            this.Map(x => x.Balance).Not.Nullable();
            this.Map(x => x.PaymentUnitSourceSystemKey).Not.Nullable();
            
        }
    }
}
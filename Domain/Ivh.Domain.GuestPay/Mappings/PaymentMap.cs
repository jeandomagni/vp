﻿namespace Ivh.Domain.GuestPay.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using FluentNHibernate.Mapping;
    using Entities;

    public class PaymentMap : ClassMap<Payment>
    {
        public PaymentMap()
        {
            this.Schema("guestpay");
            this.Table("Payment");
            this.Id(x => x.PaymentId);
            this.Map(x => x.InsertDate);
            this.Map(x => x.PaymentStatus).Column("PaymentStatusId").CustomType<PaymentStatusEnum>();
            this.Map(x => x.PaymentSystemType).Column("PaymentSystemTypeId").CustomType<PaymentSystemTypeEnum>();
            this.Map(x => x.PaymentMethodType).Column("PaymentMethodTypeId").CustomType<PaymentMethodTypeEnum>();
            this.Map(x => x.PaymentAmount).Not.Nullable();
            this.Map(x => x.PaymentDate).Nullable();
            this.Map(x => x.PaymentAttemptCount).Nullable(); // seems useless on guest pay
            this.Map(x => x.EmailAddress).Not.Nullable();
            this.Map(x => x.GuarantorNumber).Not.Nullable();
            this.Map(x => x.GuarantorLastName).Not.Nullable();
            this.Map(x => x.SourceSystemKey).Not.Nullable();
            this.Map(x => x.BankName).Nullable();
            this.Map(x => x.PaymentAccountHolderName).Column("NameOnCard").Nullable();
            //this.Map(x => x.PaymentAccountHolderAddress).Nullable(); // need to add
            this.Map(x => x.ExpDate).Nullable();
            this.Map(x => x.LastFour).Nullable();
            this.Map(x => x.EnteredAuthenticationKey).Not.Nullable();
            this.Map(x => x.IpAddress).Column("IPAddress").Nullable();
            this.Map(x => x.StatementIdentifierId).Column("StatementIdentifierId");
            this.Map(x => x.IsCountryUsa);

            this.References(x => x.ParentPayment).Column("ParentPaymentId");
            this.References(x => x.PaymentUnitAuthentication, "PaymentUnitId").Nullable();
            this.References(x => x.PaymentProcessorResponse, "PaymentProcessorResponseId").Nullable();
            this.References(x => x.MerchantAccount).Column("MerchantAccountId").Nullable();
        }
    }
}
﻿namespace Ivh.Domain.GuestPay.Mappings
{
    using Common.Base.Enums;
    using FluentNHibernate.Mapping;
    using GuestPay.Entities;

    public class PaymentUnitAuthenticationMap : ClassMap<PaymentUnitAuthentication>
    {
        public PaymentUnitAuthenticationMap()
        {
            this.Schema("guestpay");
            this.Table("PaymentUnitAuthentication");
            this.Id(x => x.PaymentUnitAuthenticationId);
            this.Map(x => x.InsertDate);
           
            this.References(x => x.PaymentUnitBalance)
                .Column("PaymentUnitBalanceId");

            this.Map(x => x.GuarantorFirstName).Not.Nullable();
            this.Map(x => x.GuarantorLastName).Not.Nullable();
            this.Map(x => x.PaymentUnitSourceSystemKey).Not.Nullable();
            this.Map(x => x.ServiceGroupId).Nullable();
            this.Map(x => x.StatementIdentifierId);
            this.Map(x => x.BillingSystemId).Nullable();

        }
    }
}
﻿namespace Ivh.Domain.GuestPay.Services
{
    using System;
    using Common.Base.Enums;
    using Common.Data;
    using Interfaces;
    using Entities;
    using Newtonsoft.Json;
    using NHibernate;
    using AutoMapper;
    using Base.Interfaces;
    using Base.Services;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.Communication;
    using Common.VisitPay.Messages.HospitalData;

    public class PaymentReversalService : DomainService, IPaymentReversalService
    {
        private readonly Lazy<IPaymentRepository> _paymentRepository;
        private readonly ISession _session;

        public PaymentReversalService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IPaymentRepository> paymentRepository,
            ISessionContext<GuestPay> session) : base(serviceCommonService)
        {
            this._paymentRepository = paymentRepository;
            this._session = session.Session;
        }

        public void VoidPayment(Payment payment)
        {
            this._session.Transaction.RegisterPostCommitSuccessAction((p1) =>
            {
                GuestPayPaymentVoidMessage guestPayPaymentVoidMessage = null;
                try
                {
                    guestPayPaymentVoidMessage = new GuestPayPaymentVoidMessage()
                    {
                        VpPaymentUnitId = payment.PaymentUnitAuthentication.PaymentUnitAuthenticationId,
                        VpPaymentId = payment.PaymentId
                    };

                    this.Bus.Value.PublishMessage(guestPayPaymentVoidMessage).Wait();
                }
                catch
                {
                    this.LoggingService.Value.Fatal(() => $"Ivh.Domain.GuestPay.Services.PaymentReversalService.RefundPayment::Failed to publish {typeof(GuestPayPaymentVoidMessage).FullName}, Message = {JsonConvert.SerializeObject(guestPayPaymentVoidMessage)}");
                }

                SendGuestPayPaymentReversalConfirmationEmailMessage sendGuestPayPaymentReversalConfirmationEmailMessage = null;
                try
                {
                    sendGuestPayPaymentReversalConfirmationEmailMessage = new SendGuestPayPaymentReversalConfirmationEmailMessage
                    {
                        EmailAddress = payment.EmailAddress,
                        GuarantorFirstName = payment.GuarantorFirstName,
                        PaymentUnitId = payment.PaymentUnitAuthentication.PaymentUnitAuthenticationId,
                        TransactionId = payment.TransactionId,
                        PaymentType = PaymentTypeEnum.Void
                    };

                    this.Bus.Value.PublishMessage(sendGuestPayPaymentReversalConfirmationEmailMessage).Wait();
                }
                catch
                {
                    this.LoggingService.Value.Fatal(() => $"Ivh.Domain.GuestPay.Services.PaymentReversalService.RefundPayment::Failed to publish {typeof(SendGuestPayPaymentReversalConfirmationEmailMessage).FullName}, Message = {JsonConvert.SerializeObject(sendGuestPayPaymentReversalConfirmationEmailMessage)}");
                }
            }, payment);

            payment.PaymentStatus = PaymentStatusEnum.ClosedCancelled;
            this._paymentRepository.Value.InsertOrUpdate(payment);
        }

        public void RefundPayment(Payment payment, decimal? amount = null)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                Payment voidPayment = Mapper.Map<Payment>(payment);
                voidPayment.InsertDate = DateTime.UtcNow;
                voidPayment.ParentPayment = payment;
                voidPayment.PaymentAmount = amount ?? decimal.Negate(Math.Abs(payment.PaymentAmount));
                voidPayment.PaymentProcessorResponse = null;
                this._paymentRepository.Value.InsertOrUpdate(voidPayment);

                this._session.Transaction.RegisterPostCommitSuccessAction((p1) =>
                {
                    GuestPayPaymentMessage guestPayPaymentMessage = null;
                    try
                    {
                        guestPayPaymentMessage = new GuestPayPaymentMessage
                        {
                            VpGuarantorEmailAddress = p1.EmailAddress,
                            VpGuarantorNumber = p1.GuarantorNumber,
                            VpGuarantorLastName = p1.GuarantorLastName,
                            VpGuarantorSourceSystemKey = p1.PaymentUnitAuthentication.PaymentUnitSourceSystemKey,
                            VpPaymentUnitSourceSystemKey = p1.PaymentUnitAuthentication.PaymentUnitSourceSystemKey,
                            VpPaymentUnitBillingSystemId = p1.PaymentUnitAuthentication.BillingSystemId ?? default(int),
                            VpPaymentUnitId = p1.PaymentUnitAuthentication.PaymentUnitAuthenticationId,
                            VpPaymentId = p1.PaymentId,
                            VpPaymentAmount = p1.PaymentAmount,
                            VpPaymentDate = p1.PaymentDate,
                            VpPaymentMethodTypeId = (int) p1.PaymentMethodType,
                            MerchantAccountId = p1.MerchantAccountId,
                            StatementIdentifierId = p1.StatementIdentifierId,
                            PaymentSystemType = p1.PaymentSystemType
                        };

                        this.Bus.Value.PublishMessage(guestPayPaymentMessage).Wait();
                    }
                    catch
                    {
                        this.LoggingService.Value.Fatal(() => $"Ivh.Domain.GuestPay.Services.PaymentReversalService.RefundPayment::Failed to publish {typeof(GuestPayPaymentMessage).FullName}, Message = {JsonConvert.SerializeObject(guestPayPaymentMessage)}");
                    }

                    SendGuestPayPaymentReversalConfirmationEmailMessage sendGuestPayPaymentReversalConfirmationEmailMessage = null;
                    try
                    {
                        sendGuestPayPaymentReversalConfirmationEmailMessage = new SendGuestPayPaymentReversalConfirmationEmailMessage
                        {
                            EmailAddress = p1.EmailAddress,
                            GuarantorFirstName = p1.GuarantorFirstName,
                            PaymentUnitId = p1.PaymentUnitAuthentication.PaymentUnitAuthenticationId,
                            TransactionId = p1.ParentPayment.TransactionId,
                            PaymentType = PaymentTypeEnum.Refund
                        };

                        this.Bus.Value.PublishMessage(sendGuestPayPaymentReversalConfirmationEmailMessage).Wait();
                    }
                    catch
                    {
                        this.LoggingService.Value.Fatal(() => $"Ivh.Domain.GuestPay.Services.PaymentReversalService.RefundPayment::Failed to publish {typeof(SendGuestPayPaymentReversalConfirmationEmailMessage).FullName}, Message = {JsonConvert.SerializeObject(sendGuestPayPaymentReversalConfirmationEmailMessage)}");
                    }
                }, voidPayment);

                unitOfWork.Commit();
            }
        }
    }
}

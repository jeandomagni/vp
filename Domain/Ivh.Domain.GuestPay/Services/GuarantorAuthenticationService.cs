﻿namespace Ivh.Domain.GuestPay.Services
{
    using System;
    using System.Collections.Generic;
    using Application.GuestPay.Common.Dtos;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Utilities.Extensions;
    using Interfaces;
    using Newtonsoft.Json;

    public class GuarantorAuthenticationService : DomainService, IGuarantorAuthenticationService
    {
        public GuarantorAuthenticationService(
            Lazy<IDomainServiceCommonService> serviceCommonService) : base(serviceCommonService)
        {
        }

        public IList<AuthenticationMatchFieldDto> GetGuarantorAuthenticationFields()
        {
            string guestPayGuarantorAuthenticationFieldsJson = this.Client.Value.GuestPayAuthenticationMatchFields;

            if (guestPayGuarantorAuthenticationFieldsJson.IsNotNullOrEmpty())
            {
                try
                {
                    return JsonConvert.DeserializeObject<List<AuthenticationMatchFieldDto>>(guestPayGuarantorAuthenticationFieldsJson);
                }
                catch (Exception)
                {

                }
            }

            return new List<AuthenticationMatchFieldDto>();
        }
    }
}

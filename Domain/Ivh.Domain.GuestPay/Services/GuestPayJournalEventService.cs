﻿namespace Ivh.Domain.GuestPay.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.EventJournal;
    using EventJournal.Interfaces;
    using Common.EventJournal.Templates.Guarantor;
    using Common.VisitPay.Constants;
    using Interfaces;
    using Ivh.Domain.User.Interfaces;
    using User.Entities;

    public class GuestPayJournalEventService : DomainService, IGuestPayJournalEventService
    {
        private readonly Lazy<IEventJournalService> _eventJournalService;

        public GuestPayJournalEventService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IEventJournalService> eventJournalService) : base(serviceCommonService)
        {
            this._eventJournalService = eventJournalService;
        }

        public void LogPaymentMethodFailure(int paymentUnitAuthenticationId, string paymentUnitSourceSystemKey, string guarantorLastName, JournalEventHttpContextDto context, IReadOnlyDictionary<string, string> processorResponse)
        {
            string userName = string.Concat(paymentUnitSourceSystemKey ?? "", "|", guarantorLastName ?? "");

            JournalEventUserContext journalEventUserContext = JournalEventUserContext.BuildSystemUserContext(context);
            JournalEventContext journalEventContext = new JournalEventContext() {VisitPayUserId = SystemUsers.SystemUserId};

            this._eventJournalService.Value.AddEvent<EventGuarantorGuestPayPaymentMethodVerifyFailure, JournalEventParameters>(
                EventGuarantorGuestPayPaymentMethodVerifyFailure.GetParameters
                (
                    hsGuarantorId: userName,
                    paymentAuthenticationId: paymentUnitAuthenticationId.ToString()
                ),
                journalEventUserContext,
                journalEventContext
            );

        }

    }
}
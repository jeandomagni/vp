﻿namespace Ivh.Domain.GuestPay.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Application.GuestPay.Common.Dtos;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;

    public class PaymentUnitService : DomainService, IPaymentUnitService
    {
        private readonly Lazy<IGuarantorAuthenticationService> _guarantorAuthenticationService;
        private readonly Lazy<IPaymentUnitAuthenticationRepository> _paymentUnitAuthenticationRepository;

        public PaymentUnitService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IGuarantorAuthenticationService> guarantorAuthenticationService,
            Lazy<IPaymentUnitAuthenticationRepository> paymentUnitAuthenticationRepository) : base(serviceCommonService)
        {
            this._guarantorAuthenticationService = guarantorAuthenticationService;
            this._paymentUnitAuthenticationRepository = paymentUnitAuthenticationRepository;
        }

        public PaymentUnitAuthentication GetPaymentUnitAuthentication(int paymentUnitAuthenticationId, string paymentUnitSourceSystemKey)
        {
            return this._paymentUnitAuthenticationRepository.Value.GetPaymentUnitAuthentication(paymentUnitAuthenticationId, paymentUnitSourceSystemKey);
        }

        public IList<PaymentUnitAuthentication>  GetPaymentUnitAuthentications(string paymentUnitSourceSystemKey)
        {
            return this._paymentUnitAuthenticationRepository.Value.GetPaymentUnitAuthentications(paymentUnitSourceSystemKey);
        }

        public PaymentUnitAuthentication AuthenticatePaymentUnit(PaymentUnitAuthentication paymentUnitAuthentication)
        {
            IList<AuthenticationMatchFieldDto> authenticationFields = this._guarantorAuthenticationService.Value.GetGuarantorAuthenticationFields();

            IList<PaymentUnitAuthentication> foundPaymentUnitAuthentications = this._paymentUnitAuthenticationRepository.Value.AuthenticatePaymentUnit(
                paymentUnitAuthentication, 
                authenticationFields
                    .Where(x => x.Type.IsNotInCategory(AuthenticationMatchFieldTypeEnumCategory.DisplayOnly))
                    .Select(x => x.Key)
                    .ToList());

            if (foundPaymentUnitAuthentications.Count > 1)
            {
                Func<string> ambiguousIds = () => string.Join(",", foundPaymentUnitAuthentications.Select(x => x.PaymentUnitAuthenticationId));
                this.LoggingService.Value.Warn(() => $"{nameof(PaymentUnitService)}::{nameof(this.AuthenticatePaymentUnit)} - ambiguous payment unit results: {ambiguousIds.Invoke()}");

                return null;
            }

            return foundPaymentUnitAuthentications.FirstOrDefault();
        }
    }
}
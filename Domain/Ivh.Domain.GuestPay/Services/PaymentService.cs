﻿namespace Ivh.Domain.GuestPay.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Utilities.Helpers;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.EventJournal;
    using Common.ServiceBus.Common.Extensions;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.HospitalData;
    using FinanceManagement.Payment.Entities;
    using FinanceManagement.Payment.Interfaces;
    using FinanceManagement.ValueTypes;
    using Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;
    using Ivh.Domain.FinanceManagement.Ach.Interfaces;
    using Ivh.Domain.GuestPay.Entities;
    using Newtonsoft.Json;
    using NHibernate;
    using Payment = Entities.Payment;

    public class PaymentService : DomainService, IPaymentService
    {
        private readonly Lazy<IGuestPayJournalEventService> _guestPayUserEventHistoryService;
        private readonly ISession _session;
        private readonly Lazy<IPaymentProcessorRepository> _paymentProcessorRepository;
        private readonly Lazy<IPaymentProcessorResponseRepository> _paymentProcessorResponseRepository;
        private readonly Lazy<IPaymentProvider> _paymentProvider;
        private readonly Lazy<Interfaces.IPaymentRepository> _paymentRepository;
        private readonly Lazy<IAchService> _achService;

        public PaymentService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<Interfaces.IPaymentRepository> paymentRepository,
            Lazy<IPaymentProvider> paymentProvider,
            Lazy<IPaymentProcessorRepository> paymentProcessorRepository,
            Lazy<IPaymentProcessorResponseRepository> paymentProcessorResponseRepository,
            Lazy<IGuestPayJournalEventService> guestPayUserEventHistoryService,
            Lazy<IAchService> achService,
            ISessionContext<GuestPay> session) : base(serviceCommonService)
        {
            this._paymentRepository = paymentRepository;
            this._paymentProvider = paymentProvider;
            this._paymentProcessorRepository = paymentProcessorRepository;
            this._paymentProcessorResponseRepository = paymentProcessorResponseRepository;
            this._guestPayUserEventHistoryService = guestPayUserEventHistoryService;
            this._achService = achService;
            this._session = session.Session;
        }

        public Payment GetPayment(int paymentId, string enteredAuthenticationKey)
        {
            return this._paymentRepository.Value.GetPayment(paymentId, enteredAuthenticationKey);
        }

        public async Task<bool> ProcessPaymentAsync(Payment payment, JournalEventHttpContextDto context)
        {
            if (payment == null)
            {
                throw new ArgumentNullException(nameof(payment));
            }

            if (payment.PaymentAmount <= 0m)
            {
                this.LoggingService.Value.Warn(() => "GuestPay.PaymentService.ProcessPaymentAsync - payment.PaymentAmount <= 0");
                return false;
            }

            // save payment before processing
            payment.PaymentAttemptCount++;
            payment.PaymentStatus = PaymentStatusEnum.ActivePending;
            this._paymentRepository.Value.InsertOrUpdate(payment);

            // process
            PaymentProcessor paymentProcessor = this._paymentProcessorRepository.Value.GetByName(this._paymentProvider.Value.PaymentProcessorName);
            PaymentProcessorResponse paymentProcessorResponse = null;

            BillingIdResult verification;
            if (payment.IsCountryUsa && string.IsNullOrWhiteSpace(payment.BillingId))
            {
                verification = await this._paymentProvider.Value.VerifyPaymentMethodAsync(payment, paymentProcessor);
            }
            else
            {
                verification = new BillingIdResult() { Succeeded = true };
            }

            if (verification.Succeeded)
            {
                try
                {
                    paymentProcessorResponse = await this._paymentProvider.Value.SubmitPaymentAsync(payment, paymentProcessor);
                    this._paymentProcessorResponseRepository.Value.InsertOrUpdate(paymentProcessorResponse);
                }
                catch (Exception ex)
                {
                    this.LoggingService.Value.Fatal(() => "GuestPay.PaymentService.ProcessPaymentAsync - PaymentId = {0}, {1}", payment.PaymentId, ExceptionHelper.AggregateExceptionToString(ex));
                }
            }
            else
            {
                this._guestPayUserEventHistoryService.Value.LogPaymentMethodFailure(payment.PaymentUnitAuthentication.PaymentUnitAuthenticationId, payment.GuarantorNumber, payment.GuarantorLastName, context, verification.Response);
            }
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                if (paymentProcessorResponse != null)
                {
                    payment.PaymentDate = DateTime.UtcNow;
                    payment.PaymentStatus = SetStatusOfPayment(paymentProcessorResponse.PaymentProcessorResponseStatus, payment);
                    switch (payment.PaymentStatus)
                    {
                        case PaymentStatusEnum.ActivePendingACHApproval:
                            this._session.Transaction.RegisterPostCommitSuccessAction(p1 =>
                            {
                                this.ProcessOptimisticAchSettlement(p1);

                            }, payment);
                            break;
                        case PaymentStatusEnum.ClosedPaid:
                            this.PublishGuestPayPayment(payment);
                            break;
                        case PaymentStatusEnum.ClosedFailed:
                            {
                                if (paymentProcessorResponse.PaymentProcessorResponseStatus == PaymentProcessorResponseStatusEnum.Decline)
                                {
                                    IReadOnlyDictionary<string, string> responseValues = this._paymentProvider.Value.ParseResponse(paymentProcessorResponse.RawResponse);

                                    this._guestPayUserEventHistoryService.Value.LogPaymentMethodFailure(
                                        payment.PaymentUnitAuthentication.PaymentUnitAuthenticationId,
                                        payment.GuarantorNumber,
                                        payment.GuarantorLastName,
                                        context,
                                        responseValues);
                                }
                                break;
                            }
                    }
                }
                else
                {
                    payment.PaymentStatus = PaymentStatusEnum.ClosedFailed;
                }

                this._paymentRepository.Value.InsertOrUpdate(payment);
                unitOfWork.Commit();
            }

            return paymentProcessorResponse != null && paymentProcessorResponse.WasSuccessful();
        }

        public void ProcessOptimisticAchSettlement(Payment gpPayment)
        {
            bool hasOptimisticSettlementEnabled = this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureOptimisticAchSettlementIsEnabled);
            
            if (hasOptimisticSettlementEnabled && gpPayment.PaymentStatus == PaymentStatusEnum.ActivePendingACHApproval && gpPayment.IsAchType)
            {
                int achSettlementDetailId = this._achService.Value.DoOptimisticSettlement(gpPayment.GuarantorFirstNameLastName, gpPayment.TransactionId.TrimNullSafe(), gpPayment.PaymentAmount);
                AchSettlementMessage achSettlementMessage = new AchSettlementMessage();

                try
                {
                    int vpGuarantorId = int.Parse(gpPayment.GuarantorNumber);
                    achSettlementMessage = new AchSettlementMessage
                    {
                        TransactionId = gpPayment.TransactionId.TrimNullSafe(),
                        AchSettlementDetailId = achSettlementDetailId
                    };

                    this.Bus.Value.PublishMessage(new AchSettlementMessageGuarantor
                    {
                        VpGuarantorId = vpGuarantorId,
                        Messages = achSettlementMessage.ToListOfOne()
                    }).Wait();
                }
                catch (Exception e)
                {
                    this.LoggingService.Value.Fatal(() => $"PaymentService.ProcessOptimisticAchSettlement::Failed to publish {typeof(AchSettlementMessageGuarantor).FullName}, Message = {JsonConvert.SerializeObject(achSettlementMessage)}, Exception = {e.AggregateExceptionToString()}");
                }
            }
        }

        private static PaymentStatusEnum SetStatusOfPayment(PaymentProcessorResponseStatusEnum responseStatus, Payment payment)
        {
            switch (responseStatus)
            {
                case PaymentProcessorResponseStatusEnum.Accepted:
                case PaymentProcessorResponseStatusEnum.Approved:
                    payment.PaymentStatus = payment.IsAchType ? PaymentStatusEnum.ActivePendingACHApproval : PaymentStatusEnum.ClosedPaid;
                    break;
                case PaymentProcessorResponseStatusEnum.LinkFailure:
                    payment.PaymentStatus = PaymentStatusEnum.ActivePendingLinkFailure;
                    break;
                case PaymentProcessorResponseStatusEnum.CallToProcessorFailed:
                case PaymentProcessorResponseStatusEnum.BadData:
                case PaymentProcessorResponseStatusEnum.Decline:
                case PaymentProcessorResponseStatusEnum.Error:
                case PaymentProcessorResponseStatusEnum.Unknown:
                    payment.PaymentStatus = PaymentStatusEnum.ClosedFailed;
                    break;
            }
            return payment.PaymentStatus;
        }

        public void PublishGuestPayPayment(Payment payment, AchSettlementTypeEnum achSettlementType = AchSettlementTypeEnum.None)
        {
            this._session.Transaction.RegisterPostCommitSuccessAction((p1) =>
            {
                GuestPayPaymentMessage guestPayPaymentMessage = null;
                try
                {
                    guestPayPaymentMessage = new GuestPayPaymentMessage
                    {
                        VpGuarantorEmailAddress = payment.EmailAddress,
                        VpGuarantorNumber = payment.GuarantorNumber,
                        VpGuarantorLastName = payment.GuarantorLastName,
                        VpGuarantorSourceSystemKey = payment.PaymentUnitAuthentication.PaymentUnitSourceSystemKey,
                        VpPaymentUnitSourceSystemKey = payment.PaymentUnitAuthentication.PaymentUnitSourceSystemKey,
                        VpPaymentUnitBillingSystemId = payment.PaymentUnitAuthentication.BillingSystemId ?? default(int),
                        VpPaymentUnitId = payment.PaymentUnitAuthentication.PaymentUnitAuthenticationId,
                        VpPaymentId = payment.PaymentId,
                        VpPaymentAmount = payment.PaymentAmount,
                        VpPaymentDate = payment.PaymentDate,
                        VpPaymentMethodTypeId = (int)payment.PaymentMethodType,
                        MerchantAccountId = payment.MerchantAccountId,
                        StatementIdentifierId = payment.StatementIdentifierId,
                        PaymentSystemType = payment.PaymentSystemType,
                        AchSettlementType = achSettlementType
                    };

                    this.Bus.Value.PublishMessage(guestPayPaymentMessage).Wait();
                }
                catch
                {
                    this.LoggingService.Value.Fatal(() => $"Ivh.Domain.GuestPay.Services.PaymentService.PublishGuestPayPaymentMessage::Failed to publish {typeof(GuestPayPaymentMessage).FullName}, Message = {JsonConvert.SerializeObject(guestPayPaymentMessage)}");
                }

            }, payment);
        }

        public Payment GetPayment(string paymentProcessorSourceKey)
        {
            return this._paymentRepository.Value.GetPayment(paymentProcessorSourceKey);
        }

        public IList<Payment> GetPaymentByTransactionId(string transactionId, IList<PaymentStatusEnum> paymentStatuses = null)
        {
            return this._paymentRepository.Value.GetPaymentByTransactionId(transactionId, paymentStatuses);
        }

        public void InsertOrUpdate(Payment payment)
        {
            this._paymentRepository.Value.InsertOrUpdate(payment);
        }

        public IQueryable<Payment> GetPaymentsQueryable()
        {
            return this._paymentRepository.Value.GetQueryable();
        }

    }
}
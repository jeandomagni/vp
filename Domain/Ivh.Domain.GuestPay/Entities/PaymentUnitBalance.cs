﻿namespace Ivh.Domain.GuestPay.Entities
{
    using System;

    public class PaymentUnitBalance
    {
        public virtual int PaymentUnitBalanceId { get; set; }
        public virtual string PaymentUnitSourceSystemKey { get; set; }
        public virtual decimal Balance { get; set; }
        public virtual DateTime UpdateDate { get; set; }
    }
}
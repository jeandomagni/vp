﻿namespace Ivh.Domain.GuestPay.Entities
{
    using System;
    using Ivh.Common.Base.Utilities.Helpers;


    public class PaymentUnitAuthentication
    {
        public PaymentUnitAuthentication()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int PaymentUnitAuthenticationId { get; set; }
        
        public virtual DateTime InsertDate { get; set; }
        
        public virtual PaymentUnitBalance PaymentUnitBalance { get; set; }

        public virtual string PaymentUnitSourceSystemKey { get; set; }

        public virtual string GuarantorFirstName { get; set; }

        public virtual string GuarantorLastName { get; set; }

        public virtual int? ServiceGroupId { get; set; }

        public virtual int StatementIdentifierId { get; set; }

        /// <summary>
        /// only required for clients with ambiguous statementIdentifier -> billingSystem maps
        /// </summary>
        public virtual int? BillingSystemId { get; set; }

        public virtual string GuarantorLastNameFirstName => FormatHelper.LastNameFirstName(this.GuarantorLastName,this.GuarantorFirstName);

        public virtual string GuarantorFirstNameLastName => FormatHelper.FirstNameLastName(this.GuarantorFirstName,this.GuarantorLastName);

        public virtual object GetValueByName(string field)
        {
            switch (field)
            {
                case "PaymentUnitSourceSystemKey":
                    return this.PaymentUnitSourceSystemKey;
                case "GuarantorFirstName":
                    return this.GuarantorFirstName;
                case "GuarantorLastName":
                    return this.GuarantorLastName;
                case "StatementIdentifierId":
                    return this.StatementIdentifierId;
                default:
                    return string.Empty;
            }
        }
    }
}

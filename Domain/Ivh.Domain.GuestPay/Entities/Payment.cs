﻿namespace Ivh.Domain.GuestPay.Entities
{
    using System;
    using System.Globalization;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.VisitPay.Enums;
    using FinanceManagement.Payment.Entities;
    using Ivh.Common.Base.Constants;

    public class Payment
    {
        public Payment()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        public virtual int PaymentId { get; set; }

        public virtual PaymentSystemTypeEnum PaymentSystemType { get; set; } = PaymentSystemTypeEnum.GuestPay;

        public virtual DateTime InsertDate { get; set; }

        public virtual PaymentStatusEnum PaymentStatus { get; set; }

        public virtual PaymentMethodTypeEnum PaymentMethodType { get; set; }

        public virtual decimal PaymentAmount { get; set; }

        public virtual DateTime? PaymentDate { get; set; }

        public virtual int PaymentAttemptCount { get; set; }

        public virtual string EmailAddress { get; set; }

        public virtual string GuarantorNumber { get; set; }

        public virtual string GuarantorFirstName { get; set; }//NOT MAPPED. DOES NOT NEED TO BE PERSISTED.
        public virtual string GuarantorLastName { get; set; }

        public virtual string GuarantorLastNameFirstName => FormatHelper.LastNameFirstName(this.GuarantorLastName, this.GuarantorFirstName);
        public virtual string GuarantorFirstNameLastName => FormatHelper.FirstNameLastName(this.GuarantorFirstName, this.GuarantorLastName);

        public virtual string SourceSystemKey { get; set; }

        public virtual string BankName { get; set; }

        public virtual string PaymentAccountHolderName { get; protected internal set; }

        public virtual string ExpDate { get; protected internal set; }

        public virtual string LastFour { get; set; }
        public virtual string BillingId { get; set; }
        public virtual string GatewayToken { get; set; }

        public virtual string EnteredAuthenticationKey { get; set; }

        public virtual string IpAddress { get; set; }

        public virtual PaymentUnitAuthentication PaymentUnitAuthentication { get; set; }

        public virtual PaymentProcessorResponse PaymentProcessorResponse { get; set; }

        public virtual Payment ParentPayment { get; set; }
        public virtual MerchantAccount MerchantAccount { get; set; }
        public virtual int MerchantAccountId => this.MerchantAccount?.MerchantAccountId ?? 0;

        // NOT PERSISTED
        // CC Fields
        private string _cardAccountHolderName;
        public virtual string CardAccountHolderName
        {
            set
            {
                this._cardAccountHolderName = value;
                this.SetPaymentAccountHolderName();
            }
        }

        public virtual string SecurityCode { get; set; }

        public virtual string AddressStreet1 { get; set; }

        public virtual string AddressStreet2 { get; set; }

        public virtual string City { get; set; }

        public virtual string State { get; set; }

        public virtual string Zip { get; set; }

        public virtual bool IsCountryUsa { get; set; }

        public virtual int StatementIdentifierId { get; set; }

        private int? _expDateM;
        public virtual int? ExpDateM
        {
            protected internal get { return this._expDateM; }
            set
            {
                this._expDateM = value;
                this.SetExpDate();
            }
        }

        private int? _expDateY;
        public virtual int? ExpDateY
        {
            protected internal get { return this._expDateY; }
            set
            {
                this._expDateY = value;
                this.SetExpDate();
            }
        }

        private void SetExpDate()
        {
            string expDateM = this._expDateM.HasValue ? this._expDateM.Value.ToString().PadLeft(2, '0').Last(2) : string.Empty;
            string expDateY = this._expDateY.HasValue ? this._expDateY.Value.ToString().PadLeft(2, '0').Last(2) : string.Empty;
            string expDate = string.Concat(expDateM, expDateY);

            this.ExpDate = string.IsNullOrEmpty(expDate) ? null : expDate;
        }

        // ACH Fields
        public virtual string BankRoutingNumber { get; set; }

        private string _bankAccountNumber;
        public virtual string BankAccountNumber
        {
            get
            {
                return this._bankAccountNumber;
            }
            set
            {
                this._bankAccountNumber = value;
                if (this._bankAccountNumber != null)
                {
                    this.LastFour = this._bankAccountNumber.Last(4);
                }
            }
        }

        private string _bankAccountHolderFirstName;
        public virtual string BankAccountHolderFirstName
        {
            set
            {
                this._bankAccountHolderFirstName = value;
                this.SetPaymentAccountHolderName();
            }
        }

        private string _bankAccountHolderLastName;
        public virtual string BankAccountHolderLastName
        {
            set
            {
                this._bankAccountHolderLastName = value;
                this.SetPaymentAccountHolderName();
            }
        }

        // Other
        public virtual string UserAgent { protected internal get; set; }

        //
        private void SetPaymentAccountHolderName()
        {
            string bankAccountHolderName = string.Concat(this._bankAccountHolderFirstName ?? string.Empty, " ", this._bankAccountHolderLastName ?? string.Empty).Trim();
            if (!string.IsNullOrEmpty(bankAccountHolderName))
            {
                this.PaymentAccountHolderName = bankAccountHolderName;
            }

            string cardAccountHolderName = this._cardAccountHolderName ?? string.Empty;
            if (!string.IsNullOrEmpty(cardAccountHolderName))
            {
                this.PaymentAccountHolderName = cardAccountHolderName;
            }

            if (string.IsNullOrEmpty(this.PaymentAccountHolderName))
            {
                this.PaymentAccountHolderName = null;
            }
        }

        //
        public virtual bool IsAchType
        {
            get
            {
                return this.PaymentMethodType.IsInCategory(PaymentMethodTypeEnumCategory.Ach);
            }
        }

        public virtual bool IsCardType
        {
            get
            {
                return this.PaymentMethodType.IsInCategory(PaymentMethodTypeEnumCategory.Card);
            }
        }

        public virtual bool IsCardExpired()
        {
            if (!this.IsCardType)
            {
                return false;
            }

            DateTime expirationDate;
            if (DateTime.TryParseExact($"01{this.ExpDate}", "ddMMyy", null, DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal, out expirationDate))
            {
                if (expirationDate.AddMonths(1) < DateTime.UtcNow)
                {
                    return true;
                }
            }

            return false;
        }

        public virtual string TransactionId
        {
            get
            {
                return this.PaymentProcessorResponse != null && this.PaymentProcessorResponse.PaymentProcessorSourceKey.IsNotNullOrEmpty()
                    ? this.PaymentProcessorResponse.PaymentProcessorSourceKey
                    : string.Empty;
            }
        }

        public virtual string AuthCode
        {
            get
            {
                return this.PaymentProcessorResponse != null && this.PaymentProcessorResponse.ResponseField3.IsNotNullOrEmpty()
                    ? this.PaymentProcessorResponse.ResponseField3
                    : string.Empty;
            }
        }
    }
}
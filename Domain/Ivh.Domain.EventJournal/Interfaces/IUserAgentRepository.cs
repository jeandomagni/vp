﻿namespace Ivh.Domain.EventJournal.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IUserAgentRepository : IRepository<UserAgent>
    {
        bool Exists(int userAgentId);
    }
}

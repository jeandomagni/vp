﻿namespace Ivh.Domain.EventJournal.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IEventJournalQueryService : IDomainService
    {
        JournalEventResults GetJournalEvents(JournalEventFilter filter);
        int JournalEventCount(JournalEventFilter filter);
        JournalEvent GetJournalEvent(int journalEventId);
        JournalEvent GetLastEventOfType(JournalEventTypeEnum type);
    }
}

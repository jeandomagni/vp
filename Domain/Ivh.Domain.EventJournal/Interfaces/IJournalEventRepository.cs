﻿namespace Ivh.Domain.EventJournal.Interfaces
{
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IJournalEventRepository : IRepository<JournalEvent>
    {
        JournalEventResults GetJournalEvents(JournalEventFilter filter);
        int JournalEventCount(JournalEventFilter filter);
        JournalEvent GetLastEventOfType(JournalEventTypeEnum journalEventType);
    }
}

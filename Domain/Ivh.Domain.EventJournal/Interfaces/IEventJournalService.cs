﻿namespace Ivh.Domain.EventJournal.Interfaces
{
    using Common.EventJournal;

    public interface IEventJournalService : Ivh.Common.EventJournal.Interfaces.IEventJournalService
    {
        void SaveEvent(Entities.JournalEvent journalEvent);
    }
}

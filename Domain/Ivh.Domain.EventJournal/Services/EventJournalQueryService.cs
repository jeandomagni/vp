﻿namespace Ivh.Domain.EventJournal.Services
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Interfaces;
    using Entities;

    public class EventJournalQueryService :  IEventJournalQueryService
    {
        private readonly Lazy<IJournalEventRepository> _journalEventRepository;

        public EventJournalQueryService(Lazy<IJournalEventRepository> journalEventRepository)
        {
            this._journalEventRepository = journalEventRepository;
        }

        public JournalEventResults GetJournalEvents(JournalEventFilter filter)
        {
            return this._journalEventRepository.Value.GetJournalEvents(filter);
        }

        public int JournalEventCount(JournalEventFilter filter)
        {
            return this._journalEventRepository.Value.JournalEventCount(filter);
        }

        public JournalEvent GetJournalEvent(int journalEventId)
        {
            return this._journalEventRepository.Value.GetById(journalEventId);
        }

        public JournalEvent GetLastEventOfType(JournalEventTypeEnum type)
        {
            return this._journalEventRepository.Value.GetLastEventOfType(type);
        }

    }
}

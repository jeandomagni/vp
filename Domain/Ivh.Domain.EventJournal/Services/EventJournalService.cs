﻿namespace Ivh.Domain.EventJournal.Services
{
    using System;
    using AutoMapper;
    using Common.Base.Utilities.Extensions;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.EventJournal;
    using Common.EventJournal.Interfaces;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.EventJournal;
    using Entities;
    using Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using NHibernate;
    using IEventJournalService = Interfaces.IEventJournalService;
    using JournalEvent = Common.EventJournal.JournalEvent;

    public class EventJournalService : IEventJournalService
    {
        private readonly ApplicationEnum _applicationEnum;
        private readonly Lazy<IBus> _bus;
        private readonly Lazy<IUserAgentRepository> _userAgentRepository;
        private readonly Lazy<IJournalEventRepository> _journalEventRepository;
        private readonly ISession _session;

        public EventJournalService(
            ApplicationEnum applicationEnum,
            Lazy<IBus> bus,
            ISessionContext<Logging> sessionContext,
            Lazy<IJournalEventRepository> journalEventRepository,
            Lazy<IUserAgentRepository> userAgentRepository)
        {
            this._applicationEnum = applicationEnum;
            this._bus = bus;
            this._journalEventRepository = journalEventRepository;
            this._userAgentRepository = userAgentRepository;
            this._session = sessionContext.Session;
        }

        public JournalEvent AddEvent<TTemplate>(
            JournalEventUserContext journalEventUserContext = null,
            JournalEventContext journalEventContext = null)
            where TTemplate : JournalEventTemplate, new()
        {
            TTemplate template = new TTemplate() { JournalEventContext = journalEventContext, JournalEventUserContext = journalEventUserContext };
            JournalEvent journalEvent = template.BuildJournalEvent();
            return this.PublishEvent(journalEvent);
        }

        public JournalEvent AddEvent<TTemplate, T1>(
            T1 entity,
            JournalEventUserContext journalEventUserContext = null,
            JournalEventContext journalEventContext = null)
            where TTemplate : JournalEventTemplate<T1>, new()
            where T1 : IJournalEventParameters
        {
            TTemplate template = new TTemplate() { JournalEventContext = journalEventContext, JournalEventUserContext = journalEventUserContext };
            JournalEvent journalEvent = template.BuildJournalEvent(entity);
            return this.PublishEvent(journalEvent);
        }

        public JournalEvent AddEvent<TTemplate>(
            JournalEventContext journalEventContext = null)
            where TTemplate : SystemJournalTemplate, new()
        {
            TTemplate template = new TTemplate() { JournalEventContext = journalEventContext };
            JournalEvent journalEvent = template.BuildJournalEvent();
            return this.PublishEvent(journalEvent);
        }

        public JournalEvent AddEvent<TTemplate, T1>(T1 entity, JournalEventContext journalEventContext = null)
            where TTemplate : SystemJournalTemplate<T1>, new()
            where T1 : IJournalEventParameters
        {
            TTemplate template = new TTemplate() { JournalEventContext = journalEventContext };
            JournalEvent journalEvent = template.BuildJournalEvent(entity);
            return this.PublishEvent(journalEvent);
        }

        public void SaveEvent(Entities.JournalEvent journalEvent)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                string userAgentString = journalEvent.UserAgentString ?? "";
                journalEvent.UserAgentId = userAgentString.GetConsistentHashCode();
                if (!this._userAgentRepository.Value.Exists(journalEvent.UserAgentId))
                {
                    this._userAgentRepository.Value.InsertOrUpdate(new UserAgent()
                    {
                        UserAgentId = journalEvent.UserAgentId,
                        UserAgentString = userAgentString
                    });
                    this._session.Flush();
                }
                this._journalEventRepository.Value.InsertOrUpdate(journalEvent);
                unitOfWork.Commit();
            }
        }

        private JournalEvent PublishEvent(JournalEvent journalEvent)
        {
            this._session.Transaction.RegisterPostCommitSuccessAction(x =>
            {
                x.Application = this._applicationEnum;
                this._bus.Value.PublishMessage(Mapper.Map<JournalEventMessage>(x)).ConfigureAwait(false);
            }, journalEvent);
            return journalEvent;
        }
    }
}

﻿namespace Ivh.Domain.EventJournal.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class JournalEventVisitPayUserMap : ClassMap<JournalEventVisitPayUser>
    {
        public JournalEventVisitPayUserMap()
        {
            this.Table("VisitPayUser");
            this.Schema("dbo");
            this.Id(x => x.VisitPayUserId);
            this.Map(x => x.UserName).Length(255).Not.Nullable().Unique().ReadOnly();
        }
    }
}

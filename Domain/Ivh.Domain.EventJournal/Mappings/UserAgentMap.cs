﻿namespace Ivh.Domain.EventJournal.Mappings
{
    using System;
    using Common.Base.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class UserAgentMap : ClassMap<UserAgent>
    {
        public UserAgentMap()
        {
            this.Schema("log");
            this.Table("UserAgent");
            this.Id(x => x.UserAgentId).GeneratedBy.Assigned();
            this.Map(x => x.UserAgentString,"UserAgent").Not.Nullable();
            this.Cache.ReadWrite();
        }
    }
}

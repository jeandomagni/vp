﻿namespace Ivh.Domain.EventJournal.Mappings
{
    using System;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class JournalEventMap : ClassMap<JournalEvent>
    {
        public JournalEventMap()
        {
            this.Schema("log");
            this.Table("JournalEvent");
            this.Id(x => x.JournalEventId).Not.Nullable().GeneratedBy.Identity();
            this.Map(x => x.EventDateTime).Not.Nullable();
            this.Map(x => x.CorrelationId).Not.Nullable();
            this.Map(x => x.JournalEventType, "JournalEventTypeId").Not.Nullable().CustomType<JournalEventTypeEnum>();
            this.Map(x => x.JournalEventCategory, "JournalEventCategoryId").Not.Nullable().CustomType<JournalEventCategoryEnum>();
            this.Map(x => x.EventTags).Nullable();
            this.Map(x => x.Application, "ApplicationId").Not.Nullable().CustomType<ApplicationEnum>();
            this.Map(x => x.EventVisitPayUserId).Not.Nullable();
            this.Map(x => x.DeviceType, "DeviceTypeId").Nullable().CustomType<DeviceTypeEnum>();
            this.Map(x => x.UserAgentId).Nullable();
            this.Map(x => x.UserHostAddress).Nullable();
            
            this.Map(x => x.VisitPayUserId).Nullable();
            this.Map(x => x.VpGuarantorId).Nullable();
            this.Map(x => x.HsGuarantorId).Nullable();
            this.Map(x => x.VisitId).Nullable();
            this.Map(x => x.VisitTransactionId).Nullable();
            this.Map(x => x.PaymentId).Nullable();
            this.Map(x => x.FinancePlanId).Nullable();
            
            this.Map(x => x.Message).Not.Nullable();
            this.Map(x => x.AdditionalData).Nullable();

            this.References(x => x.EventVisitPayUser).Column("EventVisitPayUserId")
                .Not.LazyLoad()
                .ReadOnly()
                .Fetch.Join(); // create outer join so it is part of a single SELECT statement

            this.References(x => x.VisitPayUser).Column("VisitPayUserId")
                .Not.LazyLoad()
                .ReadOnly()
                .Fetch.Join(); // create outer join so it is part of a single SELECT statement

            this.References(x => x.UserAgent).Column("UserAgentId")
                .Not.LazyLoad()
                .ReadOnly()
                .Fetch.Join(); // create outer join so it is part of a single SELECT statement

            //May choose to persist these at a later time
            //this.Map(x => x.JournalUserEventContext).Nullable();
            //this.Map(x => x.JournalEventContext).Nullable();
        }
    }
}

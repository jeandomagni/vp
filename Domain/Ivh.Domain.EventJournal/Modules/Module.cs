﻿namespace Ivh.Domain.EventJournal.Modules
{
    using Autofac;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Services;

    public class Module : Autofac.Module
    {
        private readonly ApplicationEnum _applicationEnum;

        public Module(ApplicationEnum applicationEnum)
        {
            this._applicationEnum = applicationEnum;
        }
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<EventJournalService>()
                .As<Common.EventJournal.Interfaces.IEventJournalService>()
                .WithParameters(new[]
                {
                    new NamedParameter("applicationEnum", this._applicationEnum)
                });

            builder.RegisterType<EventJournalService>()
                .As<Interfaces.IEventJournalService>()
                .WithParameters(new[]
            {
                new NamedParameter("applicationEnum", this._applicationEnum)
            });
        }
    }
}
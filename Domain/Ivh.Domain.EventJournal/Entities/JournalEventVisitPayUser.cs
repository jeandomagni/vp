﻿namespace Ivh.Domain.EventJournal.Entities
{
    public class JournalEventVisitPayUser
    {
        public virtual int VisitPayUserId { get; set; }
        public virtual string UserName { get; set; }
    }
}

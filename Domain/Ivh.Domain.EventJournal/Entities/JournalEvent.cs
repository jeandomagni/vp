﻿namespace Ivh.Domain.EventJournal.Entities
{
    public class JournalEvent : Common.EventJournal.JournalEvent
    {
        public virtual JournalEventVisitPayUser EventVisitPayUser { get; set; }
        public virtual JournalEventVisitPayUser VisitPayUser { get; set; }
        public virtual UserAgent UserAgent { get; set; }
    }
}

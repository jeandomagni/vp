﻿namespace Ivh.Domain.EventJournal.Entities
{
    using System.Collections.Generic;
    using Entities;

    public class JournalEventResults
    {
        public IReadOnlyList<JournalEvent> JournalEvents { get; set; }
        public int TotalRecords { get; set; }
    }
}

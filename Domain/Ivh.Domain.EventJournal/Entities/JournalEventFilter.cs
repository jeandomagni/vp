﻿namespace Ivh.Domain.EventJournal.Entities
{
    using System;
    using System.Collections.Generic;
    using Common.VisitPay.Enums;

    public class JournalEventFilter
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string SortField { get; set; }
        public string SortOrder { get; set; }
        public int? Page { get; set; }
        public int? Rows { get; set; }
        public string VisitPayUserId { get; set; }
        public int? VpGuarantorId { get; set; }
        public string UserName { get; set; }
        public IList<JournalEventTypeEnum> JournalEventTypes { get; set; }
        public IList<JournalEventCategoryEnum> JournalEventCategories { get; set; }
    }
}

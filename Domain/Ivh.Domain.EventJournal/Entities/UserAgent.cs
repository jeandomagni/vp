﻿namespace Ivh.Domain.EventJournal.Entities
{
    public class UserAgent
    {
        public virtual int UserAgentId { get; set; }
        public virtual string UserAgentString { get; set; }
    }
}

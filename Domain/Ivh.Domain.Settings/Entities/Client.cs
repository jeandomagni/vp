﻿namespace Ivh.Domain.Settings.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Application.Settings.Common.Dtos;
    using Attributes;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.VisitPay.Enums;
    using Newtonsoft.Json;
    using TB.ComponentModel;

    public class Client
    {
        private static readonly char[] ListDelimiters = ";,./".ToCharArray();

        private readonly IDictionary<string, string> _clientSettings;
        public readonly Lazy<TimeZoneHelper> TimeZoneHelper;

        protected Client()
        {
        }

        public Client(
            IDictionary<string, string> clientSettings,
            Lazy<TimeZoneHelper> timeZoneHelper)
        {
            this._clientSettings = clientSettings;
            this.TimeZoneHelper = timeZoneHelper;
        }

        /// <summary>
        /// Configuration for field validation when a user requests a Username or Password. 
        /// If this setting is left blank, the forms will use default behavior, using Ssn4 for additional verification. 
        /// The Json properties are Field (name of model property), Required (boolean) and Visible (bool).
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string AccountRecoveryConfiguration => this._clientSettings[nameof(this.AccountRecoveryConfiguration)].ConvertTo<string>();

        public virtual string ActionableBalanceTextShort => this._clientSettings[nameof(this.ActionableBalanceTextShort)].ConvertTo<string>().ToLower();

        public virtual string ActionableBalanceTextShortTitle => this.ActionableBalanceTextShort.ToTitleCase();

        public virtual string ActionableBalanceTextLong => this._clientSettings[nameof(this.ActionableBalanceTextLong)].ConvertTo<string>().ToLower();

        public virtual string ActionableBalanceTextLongTitle => this.ActionableBalanceTextLong.ToTitleCase();

        public virtual int ActiveUncollectableDays => this._clientSettings[nameof(this.ActiveUncollectableDays)].ConvertTo<int>();

        /// <summary>
        /// AgingTier to set when moving a visit from inactive to active and previous AgingTier is MaxAge.
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual AgingTierEnum AgingTierWhenReactivatingMaxAgeVisit => this._clientSettings[nameof(this.AgingTierWhenReactivatingMaxAgeVisit)].ConvertTo<AgingTierEnum>();

        public virtual string AppClientUrlHost => this._clientSettings[nameof(this.AppClientUrlHost)].ConvertTo<string>();

        /// <summary>
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string AppGuarantorExpressUrlHost => this._clientSettings[nameof(this.AppGuarantorExpressUrlHost)].ConvertTo<string>();

        /// <summary>
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string AppGuarantorRegistrationUrl => this._clientSettings[nameof(this.AppGuarantorRegistrationUrl)].ConvertTo<string>();

        /// <summary>
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string AppGuarantorUrlHost
        {
            get => this._clientSettings[nameof(this.AppGuarantorUrlHost)].ConvertTo<string>();
            set => this._clientSettings[nameof(this.AppGuarantorUrlHost)] = value.ConvertTo<string>();
        }

        /// <summary>
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string AppGuarantorUrlHostShortened => this._clientSettings[nameof(this.AppGuarantorUrlHostShortened)].ConvertTo<string>();

        /// <summary>
        /// Client support business hours
        /// </summary>
        public virtual string AppSupportBusinessHours => this._clientSettings[nameof(this.AppSupportBusinessHours)].ConvertTo<string>();

        /// <summary>
        /// Client support notification email
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string AppSupportNotificationEmail => this._clientSettings[nameof(this.AppSupportNotificationEmail)].ConvertTo<string>();

        /// <summary>
        /// Client support phone
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string AppSupportPhone => this._clientSettings[nameof(this.AppSupportPhone)].ConvertTo<string>();

        /// <summary>
        /// Prefix for Client Support Request Case
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string AppSupportRequestCasePrefix => this._clientSettings[nameof(this.AppSupportRequestCasePrefix)].ConvertTo<string>();

        /// <summary>
        /// Prefix for Client Support Request Name
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string AppSupportRequestNamePrefix => this._clientSettings[nameof(this.AppSupportRequestNamePrefix)].ConvertTo<string>();

        /// <summary>
        /// Link to site for servicing bad debt visits not in Visitpay
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string BadDebtVisitsUrl => this._clientSettings[nameof(this.BadDebtVisitsUrl)].ConvertTo<string>();

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int BalanceDueNotificationInDays => this._clientSettings[nameof(this.BalanceDueNotificationInDays)].ConvertTo<int>();

        /// <summary>
        /// Feature: Does the Client have BalanceTransfer turned on
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool BalanceTransferIsEnabled => this._clientSettings[nameof(this.BalanceTransferIsEnabled)].ConvertTo<bool>();

        /// <summary>
        /// Minimum number of months the finance plan is amortized for it to be eligible for balance transfer.
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int BalanceTransferFinancePlanMinDuration => this._clientSettings[nameof(this.BalanceTransferFinancePlanMinDuration)].ConvertTo<int>();

        /// <summary>
        /// Maximum number of months the finance plan is amortized for it to be eligible for balance transfer.
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int BalanceTransferFinancePlanMaxDuration => this._clientSettings[nameof(this.BalanceTransferFinancePlanMaxDuration)].ConvertTo<int>();

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string BenefitsPhone => this._clientSettings[nameof(this.BenefitsPhone)];

        /// <summary>
        /// Business hours - Warning: Contains English text that is not localized
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string BusinessHours => this._clientSettings[nameof(this.BusinessHours)].ConvertTo<string>();

        /// <summary>
        /// Maximum number of times a guarantor can cancel a recurring payment
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int CancelRecurringPaymentMaximum => this._clientSettings[nameof(this.CancelRecurringPaymentMaximum)].ConvertTo<int>();

        /// <summary>
        /// Number of days for which the CancelRecurringPaymentMaximum applies
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int CancelRecurringPaymentWindow => this._clientSettings[nameof(this.CancelRecurringPaymentWindow)].ConvertTo<int>();

        /// <summary>
        /// Unlock user account if user logging in with correct username and password after n minutes from account lockout.
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int ClientAutoUnlockTime => this._clientSettings[nameof(this.ClientAutoUnlockTime)].ConvertTo<int>();

        /// <summary>
        ///      <see cref="Client"/> Preferred Brand Name
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string ClientBrandName
        {
            get => this._clientSettings[nameof(this.ClientBrandName)].ConvertTo<string>();
            set => this._clientSettings[nameof(this.ClientBrandName)] = value.ConvertTo<string>();
        }

        /// <summary>
        /// The client office referenced in VPCC paper mail communications
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string ClientCentralBusinessOffice => this._clientSettings[nameof(this.ClientCentralBusinessOffice)].ConvertTo<string>();

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int ClientDeactivateDays => this._clientSettings[nameof(this.ClientDeactivateDays)].ConvertTo<int>();

        /// <summary>
        ///     Longer version of the clients name.
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string ClientFullName => this._clientSettings[nameof(this.ClientFullName)].ConvertTo<string>();

        /// <summary>
        ///     Longer version of the clients name (upper case).
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string ClientFullNameUpperCase => this._clientSettings[nameof(this.ClientFullNameUpperCase)].ConvertTo<string>();

        /// <summary>
        ///     Longer version of the clients name (alternate).
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string ClientFullNameAlt => this._clientSettings[nameof(this.ClientFullNameAlt)].ConvertTo<string>();

        /// <summary>
        ///     Longer version of the clients name (alternate - upper case).
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string ClientFullNameAltUpperCase => this._clientSettings[nameof(this.ClientFullNameAltUpperCase)].ConvertTo<string>();

        /// <summary>
        /// Url for logo used in communications
        /// </summary>
        public virtual string ClientLogoRelativePath => this._clientSettings[nameof(this.ClientLogoRelativePath)].ConvertTo<string>();

        /// <summary>
        /// Width of logo used in communications
        /// </summary>
        public virtual string ClientLogoWidth => this._clientSettings[nameof(this.ClientLogoWidth)].ConvertTo<string>();

        /// <summary>
        ///     Number of failed login attempts for lockout
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int ClientMaxFailedAttemptCount => this._clientSettings[nameof(this.ClientMaxFailedAttemptCount)].ConvertTo<int>();

        /// <summary>
        ///      <see cref="Client"/> Name (shorter version)
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string ClientName => this._clientSettings[nameof(this.ClientName)].ConvertTo<string>();

        /// <summary>
        ///     <see cref="Client"/> Name (shorter version - upper case)
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string ClientNameUpperCase => this._clientSettings[nameof(this.ClientNameUpperCase)].ConvertTo<string>();

        /// <summary>
        ///     Password expiry limit in days
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int ClientPasswordExpLimitInDays => this._clientSettings[nameof(this.ClientPasswordExpLimitInDays)].ConvertTo<int>();

        /// <summary>
        ///     Password should be atleast x characters long
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int ClientPasswordMinLength => this._clientSettings[nameof(this.ClientPasswordMinLength)].ConvertTo<int>();

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int ClientPasswordReuseLimit => this._clientSettings[nameof(this.ClientPasswordReuseLimit)].ConvertTo<int>();

        /// <summary>
        ///     Session out time in minutes for client
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int ClientSessionTimeOutInMinutes => this._clientSettings[nameof(this.ClientSessionTimeOutInMinutes)].ConvertTo<int>();

        /// <summary>
        ///     Client Short Name
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string ClientShortName => this._clientSettings[nameof(this.ClientShortName)].ConvertTo<string>();

        /// <summary>
        ///     Whether client choose Strong or Weak Password
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool ClientIsStrongPassword => this._clientSettings[nameof(this.ClientIsStrongPassword)].ConvertTo<bool>();

        /// <summary>
        ///     The client survey url
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string ClientSurveyURL => this._clientSettings[nameof(this.ClientSurveyURL)].ConvertTo<string>();

        /// <summary>
        ///     Whether the client survey is turned on/off
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool ClientIsSurveyOn => this._clientSettings[nameof(this.ClientIsSurveyOn)].ConvertTo<bool>();

        /// <summary>
        ///     Number of required client user security questions
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int ClientSecurityQuestionsCount => this._clientSettings[nameof(this.ClientSecurityQuestionsCount)].ConvertTo<int>();

        /// <summary>
        ///     Temporary password expiry limit in hours
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int ClientTempPasswordExpLimitInHours => this._clientSettings[nameof(this.ClientTempPasswordExpLimitInHours)].ConvertTo<int>();

        /// <summary>
        ///     Client Time Zone
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string ClientTimeZone => this._clientSettings[nameof(this.ClientTimeZone)].ConvertTo<string>();

        /// <summary>
        ///     Client Time Zone Code
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string ClientTimeZoneCode => this._clientSettings[nameof(this.ClientTimeZoneCode)].ConvertTo<string>();

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int ClosedUncollectableDays => this._clientSettings[nameof(this.ClosedUncollectableDays)].ConvertTo<int>();

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int ConsolidationExpirationDays => this._clientSettings[nameof(this.ConsolidationExpirationDays)].ConvertTo<int>();

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int ConsolidationReminderDays => this._clientSettings[nameof(this.ConsolidationReminderDays)].ConvertTo<int>();

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string CreditAgreementName => this._clientSettings[nameof(this.CreditAgreementName)].ConvertTo<string>();

        /// <summary>
        /// Maximum Aging Tier (less than or equal) for a visit to be considered discount eligible
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual AgingTierEnum DiscountMaximumAgingTier => this._clientSettings[nameof(this.DiscountMaximumAgingTier)].ConvertTo<AgingTierEnum>();

        /// <summary>
        /// Specify that only prompt payments can receive discounts
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool DiscountPromptPayOnly => this._clientSettings[nameof(this.DiscountPromptPayOnly)].ConvertTo<bool>();

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string AppCreditAgreementUrl => this._clientSettings[nameof(this.AppCreditAgreementUrl)].ConvertTo<string>();

        /// <summary>
        ///     Emulate token expiry limit in minutes
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int EmulateTokenExpLimitInSeconds => this._clientSettings[nameof(this.EmulateTokenExpLimitInSeconds)].ConvertTo<int>();

        /// <summary>
        ///     Emulate token expiry limit in minutes
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int ExpiringCreditCardReminderIntervalInDays => this._clientSettings[nameof(this.ExpiringCreditCardReminderIntervalInDays)].ConvertTo<int>();

        /// <summary>
        /// Are the alternate public pages enabled?
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureAlternatePublicPages => this._clientSettings[nameof(this.FeatureAlternatePublicPages)].ConvertTo<bool>();

        /// <summary>
        /// Is Cancel Recurring Payment Enabled (Client)
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureCancelRecurringIsEnabledClient => this._clientSettings[nameof(this.FeatureCancelRecurringIsEnabledClient)].ConvertTo<bool>();

        /// <summary>
        /// Is Cancel Recurring Payment Enabled (Guarantor)
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureCancelRecurringIsEnabledGuarantor => this._clientSettings[nameof(this.FeatureCancelRecurringIsEnabledGuarantor)].ConvertTo<bool>();

        /// <summary>
        /// Is Cancel Recurring Payment Enabled (Guarantor) when Past Due
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureCancelRecurringPdIsEnabledGuarantor => this._clientSettings[nameof(this.FeatureCancelRecurringPdIsEnabledGuarantor)].ConvertTo<bool>();

        /// <summary>
        /// External card reading device integration is enabled
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureCardReaderIsEnabled => this._clientSettings[nameof(this.FeatureCardReaderIsEnabled)].ConvertTo<bool>();

        /// <summary>
        /// Is Chat Enabled
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureChatIsEnabled => this._clientSettings[nameof(this.FeatureChatIsEnabled)].ConvertTo<bool>();

        /// <summary>
        /// Is Combining Finance Plans Enabled
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureCombineFinancePlansEnabled => this._clientSettings[nameof(this.FeatureCombineFinancePlansEnabled)].ConvertTo<bool>();

        /// <summary>
        /// Feature: Is Consolidation Enabled
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureConsolidationIsEnabled => this._clientSettings[nameof(this.FeatureConsolidationIsEnabled)].ConvertTo<bool>();

        /// <summary>
        /// Feature: Is Consolidation Visit Masking Enabled
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureConsolidationMaskingIsEnabled => this._clientSettings[nameof(this.FeatureConsolidationMaskingIsEnabled)].ConvertTo<bool>();

        /// <summary>
        /// Feature: Is Debranded UI Is Enabled
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureDebrandedUiIsEnabled => this._clientSettings[nameof(this.FeatureDebrandedUiIsEnabled)].ConvertTo<bool>();

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureDemoIsAlertEnabled => this._clientSettings[nameof(this.FeatureDemoIsAlertEnabled)].ConvertTo<bool>();

        /// <summary>
        /// Feature: Is the ClientReportDashboard UI demo functionality enabled
        /// </summary>
        public virtual bool FeatureDemoClientReportDashboardIsEnabled => this._clientSettings[nameof(this.FeatureDemoClientReportDashboardIsEnabled)].ConvertTo<bool>();

        /// <summary>
        /// Feature: Is the PreService UI demo functionality enabled
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureDemoPreServiceUiIsEnabled => this._clientSettings[nameof(this.FeatureDemoPreServiceUiIsEnabled)].ConvertTo<bool>();

        /// <summary>
        /// Feature: Is the MyChart SSO UI demo functionality enabled
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureDemoMyChartSsoUiIsEnabled => this._clientSettings[nameof(this.FeatureDemoMyChartSsoUiIsEnabled)].ConvertTo<bool>();

        /// <summary>
        /// Feature: Is the HealthEquity demo functionality enabled
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureDemoHealthEquityIsEnabled => this._clientSettings[nameof(this.FeatureDemoHealthEquityIsEnabled)].ConvertTo<bool>();

        /// <summary>
        /// Feature: Is the Insurance Accrual UI demo functionality enabled
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureDemoInsuranceAccrualUiIsEnabled => this._clientSettings[nameof(this.FeatureDemoInsuranceAccrualUiIsEnabled)].ConvertTo<bool>();

        /// <summary>
        /// Is the extended finance plan offer set featured enabled in the client portal
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureFpOfferSetTypesIsEnabledClient => this._clientSettings[nameof(this.FeatureFpOfferSetTypesIsEnabledClient)].ConvertTo<bool>();

        /// <summary>
        /// Feature: Hide Redacted Visits Enabled
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureHideRedactedVisitsIsEnabled => this._clientSettings[nameof(this.FeatureHideRedactedVisitsIsEnabled)].ConvertTo<bool>();

        /// <summary>
        /// Feature: Is Household Balance Enabled
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureHouseholdBalanceIsEnabled => this._clientSettings[nameof(this.FeatureHouseholdBalanceIsEnabled)].ConvertTo<bool>();

        /// <summary>
        /// When enabled, VpTransactionType will be evaluated upon a downward financed visit 
        /// balance change to determine if the change should be applied as a payment to prinicipal. 
        /// Prevents Roborefunds when user makes a payment outside of visitpay. 
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureExternalFinancePlanPaymentEnabled => this._clientSettings[nameof(this.FeatureExternalFinancePlanPaymentEnabled)].ConvertTo<bool>();

        /// <summary>
        /// Feature: Is Itemization Enabled
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureItemizationIsEnabled => this._clientSettings[nameof(this.FeatureItemizationIsEnabled)].ConvertTo<bool>();

        /// <summary>
        /// Allows paper mail delivery of communications
        /// </summary>
        public virtual bool FeatureMailIsEnabled => this._clientSettings[nameof(this.FeatureMailIsEnabled)].ConvertTo<bool>();

        /// <summary>
        /// ACH payments will get settled immediately after a successful payment processor response is received
        /// </summary>
        public virtual bool FeatureOptimisticAchSettlementIsEnabled => this._clientSettings[nameof(this.FeatureOptimisticAchSettlementIsEnabled)].ConvertTo<bool>();

        /// <summary>
        /// Allow the AgingTier of a visit to be reset when moving from inactive to active and previously max age.
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureResetAgingTierForReactivatedMaxAgeVisits => this._clientSettings[nameof(this.FeatureResetAgingTierForReactivatedMaxAgeVisits)].ConvertTo<bool>();

        /// <summary>
        /// Is Page Help Enabled
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeaturePageHelpIsEnabled => this._clientSettings[nameof(this.FeaturePageHelpIsEnabled)].ConvertTo<bool>();

        /// <summary>
        /// Feature: Is Payment Api Enabled
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeaturePaymentApiIsEnabled => this._clientSettings[nameof(this.FeaturePaymentApiIsEnabled)].ConvertTo<bool>();

        /// <summary>
        /// Feature: Is Payment Summary Enabled
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeaturePaymentSummaryIsEnabled => this._clientSettings[nameof(this.FeaturePaymentSummaryIsEnabled)].ConvertTo<bool>();

        /// <summary>
        /// If Enabled, the registration page shows a redirect to GuestPay for international users. Coupled with AppGuarantorExpressUrlHost (must have a value) and GuestPayAllowInternationalPayments.
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureRedirectInternationalUserRegistration => this._clientSettings[nameof(this.FeatureRedirectInternationalUserRegistration)].ConvertTo<bool>();

        /// <summary>
        /// Feature: Is Reschedule Recurring Payment Enabled (Client)
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureRescheduleRecurringIsEnabledClient => this._clientSettings[nameof(this.FeatureRescheduleRecurringIsEnabledClient)].ConvertTo<bool>();

        /// <summary>
        /// Feature: Is Reschedule Recurring Payment Enabled (Guarantor)
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureRescheduleRecurringIsEnabledGuarantor => this._clientSettings[nameof(this.FeatureRescheduleRecurringIsEnabledGuarantor)].ConvertTo<bool>();

        /// <summary>
        /// Feature: Upon registration a user will receive a notification if any existing visits are in bad debt
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureShowBadDebtNotificationIsEnabled => this._clientSettings[nameof(this.FeatureShowBadDebtNotificationIsEnabled)].ConvertTo<bool>();

        /// <summary>
        /// Feature: Upon registration a user will receive a notification if any existing visits are in bad debt
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureShowPrePaymentPlanNotificationIsEnabled => this._clientSettings[nameof(this.FeatureShowPrePaymentPlanNotificationIsEnabled)].ConvertTo<bool>();
        
        /// <summary>
        /// When enabled, allows a finance plan meeting specific criteria to be setup without agreeing to the full RIC
        /// </summary>
        public virtual bool FeatureSimpleTermsIsEnabled => this._clientSettings[nameof(this.FeatureSimpleTermsIsEnabled)].ConvertTo<bool>();

        /// <summary>
        /// Feature: Are Surveys Enabled
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureSurveyIsEnabled => this._clientSettings[nameof(this.FeatureSurveyIsEnabled)].ConvertTo<bool>();

        /// <summary>
        /// A user will receive a text with payment information and the user can reply to pay. Coupled with Application setting FeatureSmsEnabled.
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureTextToPayIsEnabled => this._clientSettings[nameof(this.FeatureTextToPayIsEnabled)].ConvertTo<bool>();

        /// <summary>
        /// Feature: Is VisitPay Reports page is enabled for SSRS reports
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureVisitPayReportsIsEnabled => this._clientSettings[nameof(this.FeatureVisitPayReportsIsEnabled)].ConvertTo<bool>();

        /// <summary>
        /// Feature: Is VisitPay managing aging for visits
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureVisitPayAgingIsEnabled => this._clientSettings[nameof(this.FeatureVisitPayAgingIsEnabled)].ConvertTo<bool>();

        /// <summary>
        /// VisitPay Client Services can upload documents and clients can view them.
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureVisitPayDocumentIsEnabled => this._clientSettings[nameof(this.FeatureVisitPayDocumentIsEnabled)].ConvertTo<bool>();

        /// <summary>
        /// When enabled, this will show the list of facilities utilized in related emails.
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureFacilityListIsEnabled => this._clientSettings[nameof(this.FeatureFacilityListIsEnabled)].ConvertTo<bool>();

        /// <summary>
        /// When enabled, this will show the list of facilities in a grid on the Contact Us page.
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureFacilityContactUsGridIsEnabled => this._clientSettings[nameof(this.FeatureFacilityContactUsGridIsEnabled)].ConvertTo<bool>();

        /// <summary>
        /// When enabled, this will give ability to refund returned mail-in checks.
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureRefundReturnedChecks => this._clientSettings[nameof(this.FeatureRefundReturnedChecks)].ConvertTo<bool>();

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int FinalPastDueNotificationDaysBeforeDueDate => this._clientSettings[nameof(this.FinalPastDueNotificationDaysBeforeDueDate)].ConvertTo<int>();

        /// <summary>
        /// When enabled, this will limit offline guarantors finance plan to combo only.
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureOfflineComboOnlyIsEnabled => this._clientSettings[nameof(this.FeatureOfflineComboOnlyIsEnabled)].ConvertTo<bool>();

        /// <summary>
        /// Avoid showing transactions that have been acknowledged by hospital but have not been cleared.
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureSuppressDuplicatePrincipalPaymentsInUi => this._clientSettings[nameof(this.FeatureSuppressDuplicatePrincipalPaymentsInUi)].ConvertTo<bool>();

        /// <summary>
        /// Financial Assistance Support Phone Number
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string FinancialAssistanceSupportPhone => this._clientSettings[nameof(this.FinancialAssistanceSupportPhone)].ConvertTo<string>();

        /// <summary>
        /// Financial Assistance Url
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string FinancialAssistanceUrl => this._clientSettings[nameof(this.FinancialAssistanceUrl)].ConvertTo<string>();

        /// <summary>
        /// show the payment logo in the footer instead of the client''s logo
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FooterShowPaymentLogo => this._clientSettings[nameof(this.FooterShowPaymentLogo)].ConvertTo<bool>();

        #region HealthEquity

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool HealthEquityEnabled => this._clientSettings["HealthEquity.Enabled"].ConvertTo<bool>();

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool HealthEquityShowBalance => this._clientSettings["HealthEquity.ShowBalance"].ConvertTo<bool>();

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool HealthEquityShowOutboundSso => this._clientSettings["HealthEquity.ShowOutboundSso"].ConvertTo<bool>();

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual IDictionary<string, string> HealthEquitySsoEmployerIdMap => JsonConvert.DeserializeObject<Dictionary<string, string>>(this._clientSettings["HealthEquity.HealthEquitySsoEmployerIdMap"].ConvertTo<string>());

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool HealthEquityDisplayIfSsoEmployerIdMapIsEmpty => this._clientSettings["HealthEquity.DisplayIfSsoEmployerIdMapIsEmpty"].ConvertTo<bool>();

        #endregion

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int GracePeriodLength => this._clientSettings[nameof(this.GracePeriodLength)].ConvertTo<int>();

        /// <summary>
        ///     Unlock user account if user logging in with correct username and password after n minutes from account lockout.
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int GuarantorAutoUnlockTime => this._clientSettings[nameof(this.GuarantorAutoUnlockTime)].ConvertTo<int>();

        /// <summary>
        /// Number of seconds to delay between registration and homepage.
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int GuarantorEnrollmentProviderDelaySeconds => this._clientSettings[nameof(this.GuarantorEnrollmentProviderDelaySeconds)].ConvertTo<int>();

        /// <summary>
        /// Is the landing page enabled
        /// </summary>
        public virtual bool GuarantorIsLandingPageEnabled => this._clientSettings[nameof(this.GuarantorIsLandingPageEnabled)].ConvertTo<bool>();

        /// <summary>
        ///     Whether client choose Strong or Weak Password
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool GuarantorIsStrongPassword => this._clientSettings[nameof(this.GuarantorIsStrongPassword)].ConvertTo<bool>();

        /// <summary>
        ///     Number of failed login attempts for lockout
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int GuarantorMaxFailedAttemptCount => this._clientSettings[nameof(this.GuarantorMaxFailedAttemptCount)].ConvertTo<int>();

        /// <summary>
        ///     Password expiry limit in days
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int GuarantorPasswordExpLimitInDays => this._clientSettings[nameof(this.GuarantorPasswordExpLimitInDays)].ConvertTo<int>();

        /// <summary>
        ///     Password shouldn't exceed at least x characters in length
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int GuarantorPasswordMaxLength => this._clientSettings[nameof(this.GuarantorPasswordMaxLength)].ConvertTo<int>();

        /// <summary>
        ///     Password should be at least x characters long
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int GuarantorPasswordMinLength => this._clientSettings[nameof(this.GuarantorPasswordMinLength)].ConvertTo<int>();

        /// <summary>
        ///     The System Matches HsGuarantors on these fields
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual IList<string> GuarantorMatchingFields => this._clientSettings[nameof(this.GuarantorMatchingFields)].ConvertTo<string>().Split(ListDelimiters).ToList();

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool GuarantorRequireSecurityQuestionsForRecovery => this._clientSettings[nameof(this.GuarantorRequireSecurityQuestionsForRecovery)].ConvertTo<bool>();

        /// <summary>
        ///     Number of Security Questions
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int GuarantorSecurityQuestionsCount => this._clientSettings[nameof(this.GuarantorSecurityQuestionsCount)].ConvertTo<int>();

        /// <summary>
        ///     Session out time in minutes
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int GuarantorSessionTimeOutInMinutes => this._clientSettings[nameof(this.GuarantorSessionTimeOutInMinutes)].ConvertTo<int>();

        /// <summary>
        ///     Temporary password expiry limit in hours
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int GuarantorTempPasswordExpLimitInHours => this._clientSettings[nameof(this.GuarantorTempPasswordExpLimitInHours)].ConvertTo<int>();

        /// <summary>
        ///     Unrepeatable password count
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int GuarantorUnrepeatablePasswordCount => this._clientSettings[nameof(this.GuarantorUnrepeatablePasswordCount)].ConvertTo<int>();

        /// <summary>
        /// GuestPay Allow International Payments
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool GuestPayAllowInternationalPayments => this._clientSettings[nameof(this.GuestPayAllowInternationalPayments)].ConvertTo<bool>();

        /// <summary>
        /// Duration in minutes of temporary authentication lockout
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int GuestPayAuthenticationLockoutTimeInMinutes => this._clientSettings[nameof(this.GuestPayAuthenticationLockoutTimeInMinutes)].ConvertTo<int>();

        /// <summary>
        ///     GuestPay Client Brand Name
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string GuestPayClientBrandName => this._clientSettings[nameof(this.GuestPayClientBrandName)].ConvertTo<string>();

        /// <summary>
        /// GuestPay Display Current Balance
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool GuestPayDisplayCurrentBalance => this._clientSettings[nameof(this.GuestPayDisplayCurrentBalance)].ConvertTo<bool>();

        /// <summary>
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string GuestPayLegacyUrl => this._clientSettings[nameof(this.GuestPayLegacyUrl)].ConvertTo<string>();

        /// <summary>
        /// Maximum number of authentication attempts before temporary lockout
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int GuestPayMaxFailedAttemptCount => this._clientSettings[nameof(this.GuestPayMaxFailedAttemptCount)].ConvertTo<int>();

        /// <summary>
        ///     GuestPay session timeout in minutes
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int GuestPaySessionTimeoutInMinutes => this._clientSettings[nameof(this.GuestPaySessionTimeoutInMinutes)].ConvertTo<int>();

        /// <summary>
        /// GuestPay support from email
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string GuestPaySupportFromEmail => this._clientSettings[nameof(this.GuestPaySupportFromEmail)].ConvertTo<string>();

        /// <summary>
        /// GuestPay support from name
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string GuestPaySupportFromName => this._clientSettings[nameof(this.GuestPaySupportFromName)].ConvertTo<string>();

        /// <summary>
        /// GuestPay support phone
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string GuestPaySupportPhone => this._clientSettings[nameof(this.GuestPaySupportPhone)].ConvertTo<string>();

        /// <summary>
        /// GuestPay Billing System Name
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string GuestPayBillingSystem => this._clientSettings[nameof(this.GuestPayBillingSystem)].ConvertTo<string>();

        /// <summary>
        /// HsGuarantor Patient Identifier
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string HsGuarantorPatientIdentifier => this._clientSettings[nameof(this.HsGuarantorPatientIdentifier)].ConvertTo<string>();

        /// <summary>
        /// HsGuarantor Patient Text
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string HsGuarantorPatientText => this._clientSettings[nameof(this.HsGuarantorPatientText)].ConvertTo<string>();

        /// <summary>
        /// Interest Calculation Method - needs to match a value in InterestCalculationMethodEnum.  Monthly will be used as a fallback.
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual InterestCalculationMethodEnum InterestCalculationMethod
        {
            get
            {
                string value = this._clientSettings[nameof(this.InterestCalculationMethod)].ConvertTo<string>();
                if (int.TryParse(value, out int interestCalculationMethodId))
                {
                    if (Enum.GetValues(typeof(InterestCalculationMethodEnum)).Cast<int>().Any(x => x == interestCalculationMethodId))
                    {
                        // return client specified
                        return (InterestCalculationMethodEnum)interestCalculationMethodId;
                    }
                }

                // return a default
                return InterestCalculationMethodEnum.Monthly;
            }
        }

        /// <summary>
        /// Is Visit Pay Enabled
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool IsVisitPayEnabled => this._clientSettings[nameof(this.IsVisitPayEnabled)].ConvertTo<bool>();

        /// <summary>
        /// IvinciHealth Support From Email
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string IvinciSupportFromEmail => this._clientSettings[nameof(this.IvinciSupportFromEmail)].ConvertTo<string>();

        /// <summary>
        /// Default language name
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string LanguageAppDefaultName => this._clientSettings[nameof(this.LanguageAppDefaultName)].ConvertTo<string>();

        /// <summary>
        /// Supported languages
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual List<ClientCultureInfoDto> LanguageAppSupported => JsonConvert.DeserializeObject<List<ClientCultureInfoDto>>(this._clientSettings[nameof(this.LanguageAppSupported)].ConvertTo<string>());
        /// <summary>
        /// Default language name
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string LanguageGuestPayDefaultName => this._clientSettings[nameof(this.LanguageGuestPayDefaultName)].ConvertTo<string>();

        /// <summary>
        /// Supported languages
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual List<ClientCultureInfoDto> LanguageGuestPaySupported => JsonConvert.DeserializeObject<List<ClientCultureInfoDto>>(this._clientSettings[nameof(this.LanguageGuestPaySupported)].ConvertTo<string>());

        /// <summary>
        /// Account number displayed on paper statement coupons for lock box deposits
        /// </summary>
        public virtual string LockBoxAccountNumber => this._clientSettings[nameof(this.LockBoxAccountNumber)].ConvertTo<string>();

        /// <summary>
        /// Display text for any masked values
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string MaskedReplacementValue => this._clientSettings[nameof(this.MaskedReplacementValue)].ConvertTo<string>();

        /// <summary>
        /// MatchOptionIds to mask
        /// </summary>
        public virtual IList<int> MatchOptionMaskingConfiguration => this._clientSettings[nameof(this.MatchOptionMaskingConfiguration)].ConvertTo<string>().Split(ListDelimiters).Select(x => Convert.ToInt32(x)).ToList();

        /// <summary>
        /// 
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int MaxDaysInPendingAchApproval => this._clientSettings[nameof(this.MaxDaysInPendingAchApproval)].ConvertTo<int>();

        /// <summary>
        /// 
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int MaxFuturePaymentIntervalInDays => this._clientSettings[nameof(this.MaxFuturePaymentIntervalInDays)].ConvertTo<int>();

        /// <summary>
        /// Feature: Is Single Sign-On Enabled (MyChart)
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool MerchantAccountSplittingIsEnabled => this._clientSettings[nameof(this.MerchantAccountSplittingIsEnabled)].ConvertTo<bool>();

        /// <summary>
        /// Feature: Is Single Sign-On Enabled (MyChart)
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool MyChartSsoIsEnabled => this._clientSettings["MyChart.FeatureSsoIsEnabled"].ConvertTo<bool>();

        /// <summary>
        /// Display text for redacted visit data
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string RedactedVisitReplacementValue => this._clientSettings[nameof(this.RedactedVisitReplacementValue)].ConvertTo<string>();

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int PaymentMaxAttemptCount => this._clientSettings[nameof(this.PaymentMaxAttemptCount)].ConvertTo<int>();

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual DateTime PaymentReconciliationCutoffTime
        {
            get
            {
                DateTime paymentReconciliationCutoffTime = this._clientSettings[nameof(this.PaymentReconciliationCutoffTime)].ConvertTo<DateTime>();
                return this.TimeZoneHelper.Value.Client.ToUniversalDateTime(paymentReconciliationCutoffTime);
            }
        }

        /// <summary>
        ///     Number of days prior to Scheduled Payment Date to send reminder. ("Payment is Scheduled for 'n' days from today.")
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int PendingPaymentReminderDays => this._clientSettings[nameof(this.PendingPaymentReminderDays)].ConvertTo<int>();

        public virtual string ProcessPaymentStartTime => this._clientSettings[nameof(this.ProcessPaymentStartTime)].ConvertTo<string>();

        /// <summary>
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string PrivacyPolicyLink => this._clientSettings[nameof(this.PrivacyPolicyLink)].ConvertTo<string>();

        /// <summary>
        /// Registration guarantor identifier text
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string RegistrationGuarantorIdentifierText => this._clientSettings[nameof(this.RegistrationGuarantorIdentifierText)].ConvertTo<string>();

        /// <summary>
        /// Registration guarantor identifier text for description (not abbreviated)
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string RegistrationGuarantorIdentifierTextLong => this._clientSettings[nameof(this.RegistrationGuarantorIdentifierTextLong)].ConvertTo<string>();

        /// <summary>
        /// 
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int RegistrationPendingDays => this._clientSettings[nameof(this.RegistrationPendingDays)].ConvertTo<int>();

        /// <summary>
        /// 
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string RegistrationIdConfiguration => this._clientSettings[nameof(this.RegistrationIdConfiguration)].ConvertTo<string>();

        /// <summary>
        /// Address used for accepting payments via mail
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual AddressDto RemitMailingAddress => JsonConvert.DeserializeObject<AddressDto>(this._clientSettings[nameof(this.RemitMailingAddress)].ConvertTo<string>());

        /// <summary>
        /// Maximum number of times a guarantor can reschedule a recurring payment
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int RescheduleRecurringPaymentMaximum => this._clientSettings[nameof(this.RescheduleRecurringPaymentMaximum)].ConvertTo<int>();

        /// <summary>
        /// Number of days for which the RescheduleRecurringPaymentMaximum applies
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int RescheduleRecurringPaymentWindow => this._clientSettings[nameof(this.RescheduleRecurringPaymentWindow)].ConvertTo<int>();

        /// <summary>
        /// Address used for accepting returned mail
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual AddressDto ReturnMailingAddress => JsonConvert.DeserializeObject<AddressDto>(this._clientSettings[nameof(this.ReturnMailingAddress)].ConvertTo<string>());

        /// <summary>
        /// NSF/Returned checks fee amount displayed on paper statement coupons for lock box deposits
        /// </summary>
        public virtual decimal ReturnedPaymentPenaltyFeeAmount => this._clientSettings[nameof(this.ReturnedPaymentPenaltyFeeAmount)].ConvertTo<decimal>();

        /// <summary>
        /// Flag to specify if scoring is enabled on this client.
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool ScoringIsEnabled => this._clientSettings["Scoring.ScoringIsEnabled"].ConvertTo<bool>();

        /// <summary>
        /// Boundary: Minimum amount a visit''s HsCurrentBalance can be and still be scored.
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual decimal ScoringMinimumHsCurrentBalanceToScore => this._clientSettings["Scoring.MinimumHsCurrentBalanceToScore"].ConvertTo<decimal>();

        /// <summary>
        /// Boundary: Minimum amount the total visit balance for all guarantor''s visits in the batch can be and still be scored.
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual decimal ScoringMinimumGuarantorBatchBalanceToScore => this._clientSettings["Scoring.MinimumGuarantorBatchBalanceToScore"].ConvertTo<decimal>();

        /// <summary>
        /// Boundary: Minimum amount the guarantor''s total visit balance can be and still be scored.
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual decimal ScoringMinimumGuarantorBalanceToScore => this._clientSettings["Scoring.MinimumGuarantorBalanceToScore"].ConvertTo<decimal>();

        /// <summary>
        ///     Phase 1 Proto Score R script
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string Phase1ProtoScoreRScript => this._clientSettings["Scoring.Phase1ProtoScoreRScript"].ConvertTo<string>();

        /// <summary>
        ///     Phase 1 Proto Score R data model
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string Phase1ProtoScoreRData => this._clientSettings["Scoring.Phase1ProtoScoreRData"].ConvertTo<string>();

        /// <summary>
        ///     Phase 2 Client PTP Score R script
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string Phase2ClientPtpScoreRScript => this._clientSettings["Scoring.Phase2ClientPtpScoreRScript"].ConvertTo<string>();

        /// <summary>
        ///     Phase 2 Client PTP Score R data model
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string Phase2ClientPtpScoreRData => this._clientSettings["Scoring.Phase2ClientPtpScoreRData"].ConvertTo<string>();


        /// <summary>
        /// Use the guarantor''s SourceSystemKey for matching. (for patient-centric systems)
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool ScoringUseSskForGuarantorMatching => this._clientSettings["Scoring.UseSskForGuarantorMatching"].ConvertTo<bool>();

        /// <summary>
        /// Enables the use of real live third party API needed for scoring.
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool ScoringLogInfoEnable => this._clientSettings["Scoring.LogInfo.Enable"].ConvertTo<bool>();

        /// <summary>
        /// Enables the use of real live third party API needed for scoring.
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool ScoringUseThirdPartyApi => this._clientSettings["Scoring.UseThirdPartyApi"].ConvertTo<bool>();

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool ScoringFeatureIncludeVisitTransactionsInRModel => this._clientSettings["Scoring.FeatureIncludeVisitTransactionsInRModel"].ConvertTo<bool>();

        /// <summary>
        /// The number of days an existing third party data can be reused.
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int ThirdPartyRefreshAge => this._clientSettings["Scoring.ThirdPartyRefreshAge"].ConvertTo<int>();

        /// <summary>
        /// The third party data set to extract when generating ptp scores.
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int PtpScoreThirdPartyDataSet => this._clientSettings["Scoring.PtpScoreThirdPartyDataSet"].ConvertTo<int>();

        /// <summary>
        /// The third party data set to extract when evaluating presumptive charity.
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int PcEvaluationThirdPartyDataSet => this._clientSettings["Scoring.PCEvaluationThirdPartyDataSet"].ConvertTo<int>();

        /// <summary>
        /// Days to wait after visit's FirstSelfPayDate before running guarantor visits for Presumptive Charity
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int SegmentationPresumptiveCharityDaysSinceFirstSelfPayDate => this._clientSettings["Segmentation.PCDaysSinceFirstSelfPayDate"].ConvertTo<int>();

        /// <summary>
        /// The minimum FirstSelfPayDate for a visit to be evaluated for Presumptive Charity
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual DateTime SegmentationPresumptiveCharityMinSelfPayDate => this._clientSettings["Segmentation.PCMinSelfPayDate"].ConvertTo<DateTime>();

        /// <summary>
        /// Enable/Disables bad debt segmentation
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool SegmentationEnableBadDebt => this._clientSettings["Segmentation.EnableBadDebt"].ConvertTo<bool>();

        /// <summary>
        /// Enable/Disables accounts receivables segmentation
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool SegmentationEnableAccountsReceivables => this._clientSettings["Segmentation.EnableAccountsReceivables"].ConvertTo<bool>();

        /// <summary>
        /// Enable/Disables active passive segmentation
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool SegmentationEnableActivePassive => this._clientSettings["Segmentation.EnableActivePassive"].ConvertTo<bool>();

        /// <summary>
        /// Enable/Disables presumptive charity segmentation
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool SegmentationEnablePresumptiveCharity => this._clientSettings["Segmentation.EnablePresumptiveCharity"].ConvertTo<bool>();

        /// <summary>
        /// Evaluate and send to client visits that are eligible to be evaluated for PC but do not have a PTP score/HH Income/HH Size
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool SegmentationPCEvaluateNoPtpScoreVisits => this._clientSettings["Segmentation.PCEvaluateNoPtpScoreVisits"].ConvertTo<bool>();

        /// <summary>
        ///     A comma separated list of SelfPayClassIds to use when PresumptiveCharityEvaluationProvider is set to SelfPayClassPresumptiveCharityEvaluationProvider.
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string SegmentationPcSelfPayClassIds => this._clientSettings["Segmentation.PCSelfPayClassIds"].ConvertTo<string>();

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int SelfPayPrimaryInsuranceTypeId => this._clientSettings[nameof(this.SelfPayPrimaryInsuranceTypeId)].ConvertTo<int>();

        /// <summary>
        ///     Send grid password
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string SendGridPassword => this._clientSettings[nameof(this.SendGridPassword)].ConvertTo<string>();

        /// <summary>
        ///     Send grid user name
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string SendGridUserName => this._clientSettings[nameof(this.SendGridUserName)].ConvertTo<string>();

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual DateTime SettlementWindowBeginAch
        {
            get
            {
                DateTime settlementWindowBeginAch = this._clientSettings[nameof(this.SettlementWindowBeginAch)].ConvertTo<DateTime>();
                return this.TimeZoneHelper.Value.Client.ToUniversalDateTime(settlementWindowBeginAch);
            }
        }

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual DateTime SettlementWindowBeginCc
        {
            get
            {
                DateTime settlementWindowBeginCc = this._clientSettings[nameof(this.SettlementWindowBeginCc)].ConvertTo<DateTime>();
                return this.TimeZoneHelper.Value.Client.ToUniversalDateTime(settlementWindowBeginCc);
            }
        }

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual DateTime SettlementWindowBeginCcRaw => this._clientSettings[nameof(this.SettlementWindowBeginCc)].ConvertTo<DateTime>();

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual DateTime SettlementWindowEndAch
        {
            get
            {
                DateTime settlementWindowEndAch = this._clientSettings[nameof(this.SettlementWindowEndAch)].ConvertTo<DateTime>();
                return this.TimeZoneHelper.Value.Client.ToUniversalDateTime(settlementWindowEndAch);
            }
        }

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual DateTime SettlementWindowEndCc
        {
            get
            {
                DateTime settlementWindowEndCc = this._clientSettings[nameof(this.SettlementWindowEndCc)].ConvertTo<DateTime>();
                return this.TimeZoneHelper.Value.Client.ToUniversalDateTime(settlementWindowEndCc);
            }
        }

        /// <summary>
        /// Display name for Material Payment Plan terms
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string SimpleTermsAgreementName  => this._clientSettings[nameof(this.SimpleTermsAgreementName)].ConvertTo<string>();
        
        /// <summary>
        /// Maximum months to allow a simple terms agreement
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int SimpleTermsMaximumMonths  => this._clientSettings[nameof(this.SimpleTermsMaximumMonths)].ConvertTo<int>();

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int StatementGenerationDays => this._clientSettings[nameof(this.StatementGenerationDays)].ConvertTo<int>();

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string SupportFromEmail => this._clientSettings[nameof(this.SupportFromEmail)].ConvertTo<string>();

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string SupportFromName
        {
            get => this._clientSettings[nameof(this.SupportFromName)].ConvertTo<string>();
            set => this._clientSettings[nameof(this.SupportFromName)] = value.ConvertTo<string>();
        }

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string SupportPhone => this._clientSettings[nameof(this.SupportPhone)].ConvertTo<string>();

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string SupportRequestCasePrefix => this._clientSettings[nameof(this.SupportRequestCasePrefix)].ConvertTo<string>();

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int SupportRequestReviewDays => this._clientSettings[nameof(this.SupportRequestReviewDays)].ConvertTo<int>();

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int UncollectableMaxAgingCountFinancePlans => this._clientSettings[nameof(this.UncollectableMaxAgingCountFinancePlans)].ConvertTo<int>();

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int UnmatchedItemizationRetentionHours => this._clientSettings[nameof(this.UnmatchedItemizationRetentionHours)].ConvertTo<int>();

        /// <summary>
        /// VpGuarantor Patient Identifier
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string VpGuarantorPatientIdentifier => this._clientSettings[nameof(this.VpGuarantorPatientIdentifier)].ConvertTo<string>();

        /// <summary>
        /// VpGuarantor Patient Text
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string VpGuarantorPatientText => this._clientSettings[nameof(this.VpGuarantorPatientText)].ConvertTo<string>();

        /// <summary>
        /// Imbalance days
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int VisitImbalanceDays => this._clientSettings[nameof(this.VisitImbalanceDays)].ConvertTo<int>();

        /// <summary>
        /// Wait Period To Reactive Account
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int WaitPeriodToReactivateAccount => this._clientSettings[nameof(this.WaitPeriodToReactivateAccount)].ConvertTo<int>();

        #region EOB

        /// <summary>
        /// Feature: Is the client sending us EOB files to import and do we want to import them (Client)
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureEobImportIsEnabled => this._clientSettings[nameof(this.FeatureEobImportIsEnabled)].ConvertTo<bool>();

        /// <summary>
        /// Feature: Inbound EOB data will get checked for length so it doesnt exceed the database column lengths
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureEobImportCheckStringLengths => this._clientSettings[nameof(this.FeatureEobImportCheckStringLengths)].ConvertTo<bool>();

        /// <summary>
        /// Location for the client's archived 835 files
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string EobArchive835Location => this._clientSettings[nameof(this.EobArchive835Location)].ConvertTo<string>();

        /// <summary>
        /// EOB visit details will be displayed in the UI
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureEobUserInterfaceIsEnabled => this._clientSettings[nameof(this.FeatureEobUserInterfaceIsEnabled)].ConvertTo<bool>();

        #endregion

        /// <summary>
        /// Feature: Does the Client have OfflineVisitPay turned on
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool OfflineVisitPayIsEnabled => this._clientSettings[nameof(this.OfflineVisitPayIsEnabled)].ConvertTo<bool>();

        /// <summary>
        /// When enabled, This will allow a refund of interest for paying off a finance plan in good standing. Dependent setting: FinancePlanIncentiveConfiguration. VP-3938
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FinancePlanRefundInterestIncentiveEnabled => this._clientSettings[nameof(this.FinancePlanRefundInterestIncentiveEnabled)].ConvertTo<bool>();

        /// <summary>
        /// Json representation of FinancePlanIncentiveConfigurationDto that triggers behavior for the feature "Finance Plan Refund InterestIncentive" (FinancePlanRefundInterestIncentiveEnabled). Requires C# FinancePlanIncentiveInterestRefundMethodEnum to exist for the json FinancePlanIncentiveInterestRefundMethod field. VP-3938
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual FinancePlanIncentiveConfigurationDto FinancePlanIncentiveConfiguration => this._clientSettings[nameof(this.FinancePlanIncentiveConfiguration)].ConvertTo<string>().TryParseJson<FinancePlanIncentiveConfigurationDto>();

        /// <summary>
        /// Feature: Does the Client have FinancePlanVisitSelectionIsEnabled turned on
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FinancePlanVisitSelectionIsEnabled => this._clientSettings[nameof(this.FinancePlanVisitSelectionIsEnabled)].ConvertTo<bool>();

        /// <summary>
        /// number of days to extend a FP offer for an Offline VisitPay user
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int CreditAgreementMinResponsePeriod => this._clientSettings[nameof(this.CreditAgreementMinResponsePeriod)].ConvertTo<int>();

        /// <summary>
        /// Imbalance days
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string GuestPayAuthenticationMatchFields => this._clientSettings[nameof(this.GuestPayAuthenticationMatchFields)].ConvertTo<string>();

        /// <summary>
        /// Feature: When enabled the minimum payment is calculated differently for combined finance plans than stacked finance plans
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool CalculateCombinedFinancePlanMinimumIsEnabled => this._clientSettings[nameof(this.CalculateCombinedFinancePlanMinimumIsEnabled)].ConvertTo<bool>();

        /// <summary>
        /// Accepted time range for communication delivery (hh:mm-hh:mm)
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string FriendlyCommunicationDeliveryRange => this._clientSettings[nameof(this.FriendlyCommunicationDeliveryRange)].ConvertTo<string>();

        /// <summary>
        /// Milliseconds to wait before drawing attention to the help center widget
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int HelpCenterWidgetAlertDelay => this._clientSettings[nameof(this.HelpCenterWidgetAlertDelay)].ConvertTo<int>();

        /// <summary>
        /// Percentage of payment to be allocated to HB visits if PaymentAllocationModelType is BillingApplication.
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int BillingApplicationHBAllocationPercentage => this._clientSettings[nameof(this.BillingApplicationHBAllocationPercentage)].ConvertTo<int>();

        /// <summary>
        /// Percentage of payment to be allocated to PB visits if PaymentAllocationModelType is BillingApplication.
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int BillingApplicationPBAllocationPercentage => this._clientSettings[nameof(this.BillingApplicationPBAllocationPercentage)].ConvertTo<int>();

        /// <summary>
        /// Priority of payment allocation for HB visits if PaymentAllocationModelType is BillingApplication.
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int BillingApplicationHBAllocationPriority => this._clientSettings[nameof(this.BillingApplicationHBAllocationPriority)].ConvertTo<int>();

        /// <summary>
        /// Priority of payment allocation for PB visits if PaymentAllocationModelType is BillingApplication.
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int BillingApplicationPBAllocationPriority => this._clientSettings[nameof(this.BillingApplicationPBAllocationPriority)].ConvertTo<int>();

        /// <summary>
        /// Inner priority ordered grouping of visits that will take all available money before passing on to the next group.
        /// </summary>
        public virtual IList<PaymentAllocationPriorityGroupingEnum> PaymentAllocationOrderedPriorityGrouping => JsonConvert.DeserializeObject<IList<PaymentAllocationPriorityGroupingEnum>>(this._clientSettings[nameof(this.PaymentAllocationOrderedPriorityGrouping)].ConvertTo<string>());

        /// <summary>
        /// This is the feature that enables ip filtering by country
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool? FeatureRestrictIpAddressByCountry => this._clientSettings.ContainsKey(nameof(this.FeatureRestrictIpAddressByCountry)) ? this._clientSettings[nameof(this.FeatureRestrictIpAddressByCountry)].ConvertTo<bool?>() : null;

        /// <summary>
        /// This is the account number for the ip geolocation service. FeatureRestrictIpAddressByCountry http://maxmind.github.io https://www.maxmind.com
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int? MaxmindAccountNumber => this._clientSettings.ContainsKey(nameof(this.MaxmindAccountNumber)) ? this._clientSettings[nameof(this.MaxmindAccountNumber)].ConvertTo<int?>() : null;

        /// <summary>
        /// This is the license key for the ip geolocation service. FeatureRestrictIpAddressByCountry  http://maxmind.github.io https://www.maxmind.com
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string MaxmindLicenseKey => this._clientSettings.ContainsKey(nameof(this.MaxmindLicenseKey)) ? this._clientSettings[nameof(this.MaxmindLicenseKey)].ConvertTo<string>() : null;

        /// <summary>
        /// personal information fields to hide or ignore for vpg-hsg matching on registration
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string RegistrationMatchConfiguration => this._clientSettings[nameof(this.RegistrationMatchConfiguration)].ConvertTo<string>();

        /// <summary>
        /// Feature: Does the Client have FeatureVisitUnmatchingMethodSelection turned on
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureVisitUnmatchingMethodSelection => this._clientSettings[nameof(this.FeatureVisitUnmatchingMethodSelection)].ConvertTo<bool>();

        /// <summary>
        /// MatchConfiguration
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string MatchConfiguration => this._clientSettings[nameof(this.MatchConfiguration)].ConvertTo<string>();

        /// <summary>
        /// the app is using the matching api for matching
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureMatchingApiIsEnabled => this._clientSettings[nameof(this.FeatureMatchingApiIsEnabled)].ConvertTo<bool>();

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual bool FeatureAnalyticReportsIsEnabled => this._clientSettings[nameof(this.FeatureAnalyticReportsIsEnabled)].ConvertTo<bool>();

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string PowerBiClientId => this._clientSettings[nameof(this.PowerBiClientId)].ConvertTo<string>();

        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string PowerBiClientReportGroupId => this._clientSettings[nameof(this.PowerBiClientReportGroupId)].ConvertTo<string>();

        /// <summary>
        /// Base URL for ChatGateway API
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string ChatGatewayApiUrl => this._clientSettings[nameof(this.ChatGatewayApiUrl)].ConvertTo<string>();

        /// <summary>
        /// Base URL for ChatGateway sockets
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string ChatGatewaySocketUrl => this._clientSettings[nameof(this.ChatGatewaySocketUrl)].ConvertTo<string>();

        /// <summary>
        /// Base URL for relay-web-app embed iframe
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string ChatIframeUrl => this._clientSettings[nameof(this.ChatIframeUrl)].ConvertTo<string>();

        /// <summary>
        /// JWT Signing key for ChatGateway API
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string ChatGatewayJwtSigningKey => this._clientSettings[nameof(this.ChatGatewayJwtSigningKey)].ConvertTo<string>();

        /// <summary>
        /// Number of days from today to push out the default payment due date suggested to a user
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual int PaymentDueDateDefaultDaysInFuture => this._clientSettings[nameof(this.PaymentDueDateDefaultDaysInFuture)].ConvertTo<int>();

        /// <summary>
        /// Client Name for Trace Api requests
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string TraceApiClientName => this._clientSettings[nameof(this.TraceApiClientName)].ConvertTo<string>();

        /// <summary>
        /// Theme client accent color
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string ThemeClientAccentColor => this._clientSettings["/theme.client-accent"].ConvertTo<string>();

        /// <summary>
        /// Endpoint in ClamAV Proxy API to use for malware scanning
        /// </summary>
        [ClientSetting(CanEditInUI = false, IsCmsVariable = false)]
        public virtual string ClamAvScanApiEndpoint => this._clientSettings[nameof(this.ClamAvScanApiEndpoint)].ConvertTo<string>();
    }
}
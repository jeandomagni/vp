﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.Settings.Entities
{
    public class PaymentProviderCredentials
    {
        public string TrustCommerceCustomerId { get; internal set; }
        public string TrustCommercePassword { get; internal set; }
        public string TrustCommercePasswordQueryApi { get; internal set; }
        public string NachaId { get; internal set; }
        public string NachaSecurityToken { get; internal set; }
    }
}

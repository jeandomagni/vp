﻿namespace Ivh.Domain.Settings.Entities
{
    using System;
    using Common.Base.Enums;
    using Common.Encryption.Enums;

    public class PasswordHashConfiguration
    {
        public int SaltLength { get; set; }
        public int Iterations { get; set; }
        public string HashFunction { get; set; }
        public HashEnum HashEnum => (HashEnum)Enum.Parse(typeof(HashEnum), this.HashFunction);
    }
}
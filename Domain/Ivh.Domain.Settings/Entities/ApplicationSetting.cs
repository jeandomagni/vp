﻿namespace Ivh.Domain.Settings.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using Common.Base.Utilities.Extensions;

    public class ApplicationSetting<T> : IApplicationSetting<T>
    {
        private readonly Lazy<T> _value;
        private readonly IReadOnlyDictionary<string, string> _values;

        public ApplicationSetting(string key, IReadOnlyDictionary<string, string> values)
        {
            this._values = values;
            this._value = new Lazy<T>(() => GetValue(key, values));
        }

        public ApplicationSetting(string key, IReadOnlyDictionary<string, string> values, Func<T> defaultValue = null, UriKind uriKind = UriKind.Absolute, bool? trailingSlash = null)
        {
            this._values = values;
            this._value = new Lazy<T>(() => GetValue(key, values, defaultValue, uriKind, trailingSlash));
        }

        public ApplicationSetting(Func<Lazy<T>> lazy)
        {
            this._value = lazy();
        }

        public ApplicationSetting(Func<T> init)
        {
            this._value = new Lazy<T>(init);
        }

        public ApplicationSetting(T init)
        {
            this._value = new Lazy<T>(() => init);
        }

        public T Value => this._value.Value;

        private static T GetValue(string key, IReadOnlyDictionary<string, string> values, Func<T> defaultValue = null, UriKind uriKind = UriKind.Absolute, bool? trailingSlash = null)
        {
            string value = null;
            if (values.ContainsKey(key))
            {
                value = values[key];
            }

            if (value == null)
            {
                value = Ivh.Common.Properties.Settings.Default.GetSettingValue(key);
            }
            
            if (value == null)
            {
                if (defaultValue != null)
                {
                    return defaultValue();
                }
                throw new ConfigurationErrorsException("AppSettings Value Missing: " + key);
            }
            Type t = typeof(T);
            if (t == typeof(string))
            {
                return (T)(object)value;
            }
            if (t.IsEnum)
            {
                try
                {
                    T returnValue = (T)Enum.Parse(t, value, true);
                    return returnValue;
                }
                catch
                {
                    throw new InvalidCastException("Unable to convert AppSetting: " + key + " to " + t.Name);
                }
            }
            if (t == typeof(bool))
            {
                bool returnValue;
                if (bool.TryParse(value, out returnValue))
                {
                    return (T)(object)returnValue;
                }
                if (defaultValue != null)
                {
                    return defaultValue();
                }
                throw new InvalidCastException("Unable to convert AppSetting: " + key + " to " + t.Name);
            }
            if (t == typeof(int))
            {
                int returnValue;
                if (int.TryParse(value, out returnValue))
                {
                    return (T)(object)returnValue;
                }
                if (defaultValue != null)
                {
                    return defaultValue();
                }
                throw new InvalidCastException("Unable to convert AppSetting: " + key + " to " + t.Name);
            }
            if (t == typeof(Guid))
            {
                Guid returnValue;
                if (Guid.TryParse(value, out returnValue))
                {
                    return (T)(object)returnValue;
                }
                if (defaultValue != null)
                {
                    return defaultValue();
                }
                throw new InvalidCastException("Unable to convert AppSetting: " + key + " to " + t.Name);
            }
            if (t == typeof(Uri))
            {
                return (T)(object)value.ToUri(uriKind, trailingSlash);
            }

            throw new InvalidCastException("Unable to convert AppSetting: " + key + " to " + t.Name);
        }
    }

    public interface IApplicationSetting<T>
    {
        T Value { get; }
    }
}
﻿using Ivh.Common.Base.Utilities.Extensions;

namespace Ivh.Domain.Settings.Entities
{
    using System;

    public class AddressVerificationProviderCredentials
    {
        public string UserId { get; }
        public string Password { get; }

        public AddressVerificationProviderCredentials(
            string userId,
            string password)
        {
            if (userId.IsNullOrEmpty())
            {
                throw new ArgumentException(nameof(userId));
            }

            if (password.IsNullOrEmpty())
            {
                throw new ArgumentException(nameof(password));
            }

            this.UserId = userId;
            this.Password = password;
        }
    }
}
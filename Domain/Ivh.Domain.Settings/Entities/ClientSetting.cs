﻿namespace Ivh.Domain.Settings.Entities
{
    public class ClientSetting
    {
        public virtual string ClientSettingKey { get; set; }
        public virtual string ClientSettingValue { get; set; }
        public virtual string ClientSettingValueOverride { get; set; }
        public virtual string DataType { get; set; }
        public virtual string ClientSettingName { get; set; }
        public virtual string ClientSettingDescription { get; set; }
        public virtual bool EditInUI { get; set; }
        public virtual bool IsCmsVariable { get; set; }
        public virtual bool IsEncrypted { get; set; }
    }
}

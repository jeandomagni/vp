﻿namespace Ivh.Domain.Settings.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Security.Cryptography.X509Certificates;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;
    using Enums;

    public interface IApplicationSettingsService : IDomainService
    {
        void Validate();
        Uri GetUrl(UrlEnum urlEnum);

        IApplicationSetting<Uri> PathToPowershellScripts { get; }
        IApplicationSetting<Uri> PathToPowershellScripts2 { get; }
        IApplicationSetting<string> DistributedCacheAuthKeyString { get; }
        IApplicationSetting<string> ServiceBusName { get; }
        IApplicationSetting<string> ScheduleServiceBusName { get; }
        IApplicationSetting<string> ServiceBusVirtualHost { get; }
        IApplicationSetting<string> RabbitMqUserName { get; }
        IApplicationSetting<string> RabbitMqPassword { get; }
        IApplicationSetting<string> RabbitMqQueueNamePrefix { get; }
        IApplicationSetting<string> RabbitMqScheduleQueue { get; }
        IApplicationSetting<bool> RabbitMqEnableSsl { get; }
        IApplicationSetting<string> RabbitMqSslClientCertPath { get; }
        IApplicationSetting<string> RabbitMqSslClientCertPassword { get; }
        IApplicationSetting<string> Environment { get; }
        IApplicationSetting<IvhEnvironmentTypeEnum> EnvironmentType { get; }
        IApplicationSetting<string> RedisEncryptionMaster { get; }
        IApplicationSetting<string> RedisEncryptionAuth { get; }
        IApplicationSetting<int> RedisMainCacheInstances { get; }
        IApplicationSetting<string> DataDogServerName { get; }
        IApplicationSetting<string> DataDogPort { get; }
        IApplicationSetting<string> ApplicationName { get; }
        IApplicationSetting<ApplicationEnum> Application { get; }
        IApplicationSetting<Uri> TrustCommerceApi { get; }
        IApplicationSetting<string> TrustCommerceCustomerId { get; }
        IApplicationSetting<string> TrustCommercePassword { get; }
        IApplicationSetting<string> TrustCommercePasswordQueryApi { get; }
        IApplicationSetting<bool> TrustCommerceIsLiveMode { get; }
        IApplicationSetting<Uri> NachaGateway { get; }
        IApplicationSetting<string> NachaId { get; }
        IApplicationSetting<string> NachaSecurityToken { get; }
        IApplicationSetting<string> MonitoringAuthenticationToken { get; }
        IApplicationSetting<bool> IsClientApplication { get; } //IsClient
        IApplicationSetting<bool> IsSystemApplication { get; } //IsSystemApplication
        IApplicationSetting<bool> IsWebApplication { get; }
        IApplicationSetting<string> SmtpUsername { get; }
        IApplicationSetting<string> SmtpPassword { get; }
        IApplicationSetting<bool> PreventEmailsFromSending { get; }
        IApplicationSetting<bool> RerouteEmailForTesting { get; }
        IApplicationSetting<string> RerouteEmailTo { get; }
        IApplicationSetting<bool> PreventInterest { get; }
        IApplicationSetting<bool> EnableFinancialAssistanceInterestRates { get; }
        IApplicationSetting<Guid> ProgramUniqueGuid { get; }
        IApplicationSetting<string> RedisEncryptionMasterOld { get; }
        IApplicationSetting<string> RedisEncryptionAuthOld { get; }
        IApplicationSetting<string> Client { get; }
        IApplicationSetting<string> EnrollmentClientId { get; }
        IApplicationSetting<string> EnrollmentPassword { get; }
        IApplicationSetting<string> EnrollmentUsername { get; }
        IApplicationSetting<Uri> Enrollment { get; }
        IApplicationSetting<Uri> EnrollmentJwtAuthentication { get; }
        IApplicationSetting<string> FileStorageArchiveFolder { get; }
        IApplicationSetting<string> FileStorageIncomingFolder { get; }
        IApplicationSetting<string> FileStoragePackagingFolder { get; }
        IApplicationSetting<string> FileStorageTempFolder { get; }
        IApplicationSetting<string> FileStorageSharedKey { get; }
        IApplicationSetting<int> FileStorageArchiveDays { get; }
        IApplicationSetting<PaymentAllocationModelTypeEnum> PaymentAllocationModelType { get; }
        IApplicationSetting<DiscountModelTypeEnum> DiscountModelType { get; }
        IApplicationSetting<string> JamsEtlFromEpicFolder { get; }
        IApplicationSetting<string> EXE_Directory_StageTableLoader { get; }
        IApplicationSetting<IDictionary<string, string>> Redirect { get; }
        IApplicationSetting<bool> PublishInterestAssessment { get; }
        IApplicationSetting<bool> ShowBuildBanner { get; }
        IApplicationSetting<IList<string>> AcceptedAvsCodes { get; }
        IApplicationSetting<bool> IsAchEnabled { get; }
        IApplicationSetting<bool> IsCreditCardEnabled { get; }
        IApplicationSetting<bool> IsLockBoxEnabled { get; }
        IApplicationSetting<bool> FeatureDemoIsAlertEnabled { get; }
        IApplicationSetting<string> SsoEncryptionKey { get; }
        IApplicationSetting<int> SsoTimeoutTokensAfterMinutes { get; }
        IApplicationSetting<int> AnalyticsId { get; }
        IApplicationSetting<Uri> AnalyticsBase { get; }
        IApplicationSetting<Uri> SecurePan { get; }
        IApplicationSetting<string> SecurePanAuthKey { get; }
        IApplicationSetting<string> SecurePanEncryptionKey { get; }
        IApplicationSetting<ApiEnum> Api { get; }
        IApplicationSetting<string> ClientDataApiAppId { get; }
        IApplicationSetting<string> ClientDataApiAppKey { get; }
        IApplicationSetting<Uri> ClientDataApi { get; }
        IApplicationSetting<int> ClientDataApiTimeToLive { get; }
        IApplicationSetting<int> PastDueAgingCount { get; }
        IApplicationSetting<CmsRegionEnum> ThemeCssRegion { get; }
        IApplicationSetting<bool> RerouteSmsForTesting { get; }
        IApplicationSetting<string> RerouteSmsTo { get; }
        IApplicationSetting<Uri> TwilioCallbackBase { get; }
        IApplicationSetting<string> TwilioCallbackPassword { get; }
        IApplicationSetting<string> TwilioCallbackUsername { get; }
        IApplicationSetting<string> TwilioMessagingServiceSid { get; }
        IApplicationSetting<string> TwilioAuthToken { get; }
        IApplicationSetting<string> TwilioAccountSid { get; }
        IApplicationSetting<bool> FeatureSmsEnabled { get; }
        IApplicationSetting<Uri> TwilioApi { get; }

        IApplicationSetting<GuarantorEnrollmentProviderEnum> GuarantorEnrollmentProvider { get; }
        IApplicationSetting<InterestRateConsiderationServiceEnum> InterestRateConsiderationService { get; }
        IApplicationSetting<HsFacilityResolutionServiceEnum> HsFacilityResolutionService { get; }
        IApplicationSetting<HsBillingSystemResolutionProviderEnum> HsBillingSystemResolutionProvider { get; }
        IApplicationSetting<RegistrationMatchStringApplicationServiceEnum> RegistrationMatchStringApplicationService { get; }

        IApplicationSetting<Uri> HealthEquityApi { get; }
        IApplicationSetting<IDictionary<string, string>> HealthEquityApiUserNames { get; }
        IApplicationSetting<IDictionary<string, string>> HealthEquityApiPasswords { get; }
        IApplicationSetting<Uri> HealthEquitySso { get; }
        IApplicationSetting<string> HealthEquitySsoBase64EncodedCertificate { get; }
        IApplicationSetting<string> HealthEquitySsoBase64EncodedCertificatePassword { get; }
        IApplicationSetting<bool> HealthEquitySsoSamlEndpointValidate { get; }
        IApplicationSetting<bool> HealthEquitySsoSamlRequireHsa { get; }
        IApplicationSetting<X509Certificate2> HealthEquitySsoCertificate { get; }
        IApplicationSetting<string> HealthEquitySsoSamlResponseIssuer { get; }
        IApplicationSetting<int> HealthEquitySsoSamlResponseExpiration { get; }

        IApplicationSetting<string> HealthEquityOAuthCryptKey { get; }
        IApplicationSetting<string> HealthEquityOAuthAuthKey { get; }
        IApplicationSetting<string> HealthEquityOAuthResourceServerBaseAddress { get; }
        IApplicationSetting<string> HealthEquityOAuthSsoPath { get; }
        IApplicationSetting<string> HealthEquityOAuthSsoTokenPath { get; }
        IApplicationSetting<string> HealthEquityOAuthMemberBalancePath { get; }

        IApplicationSetting<IList<VpTransactionTypeEnum>> HsCurrentBalanceSumExcludedTransactionTypes { get; }

        IApplicationSetting<IDictionary<int, PasswordHashConfiguration>> PasswordHashConfigurations { get; }
        IApplicationSetting<int> Load835FileMaxDegreeOfParallelism { get; }
        IApplicationSetting<Uri> SsrsReportServerUri { get; }
        IApplicationSetting<string> SsrsReportPath { get; }
        IApplicationSetting<string> SsrsReportServerUserName { get; }
        IApplicationSetting<string> SsrsReportServerPassword { get; }
        IApplicationSetting<string> SsrsReportServerDomain { get; }

        IApplicationSetting<int> ScoringBatchLoadBatchSize { get; }
        IApplicationSetting<string> ScoringRscriptEndPointUrl { get; }
        IApplicationSetting<string> ScoringThirdPartyUrl { get; }
        IApplicationSetting<string> ScoringThirdPartySecurityToken { get; }
        IApplicationSetting<string> ScoringThirdPartyXmlNamespace { get; }
        IApplicationSetting<bool> ScoringLogInfoEnable { get; }

        IApplicationSetting<int> SegmentationBatchLoadBatchSize { get; }

        IApplicationSetting<string> ClientTimeZone { get; }

        IApplicationSetting<string> GuestPayRemotePayApiKey { get; }

        IApplicationSetting<string> GetString(string key);
        PaymentProviderCredentials GetPaymentProviderCredentials(string trustCommerceCustomerIdKey, string trustCommercePasswordKey, string trustCommercePasswordQueryApiKey, string nachaIdKey, string nachaSecurityTokenKey);

        AddressVerificationProviderCredentials GetAddressVerificationProviderCredentials();

        IApplicationSetting<string> UspsApiUrlHostname { get; }
        IApplicationSetting<string> UspsApiUrlPath { get; }

        IApplicationSetting<Guid> PingAuthorizationKey { get; }

        IApplicationSetting<string> PowerBiUsername { get; }
        IApplicationSetting<string> PowerBiPassword { get; }
        IApplicationSetting<string> PowerBiApiUrl { get; }
        IApplicationSetting<string> PowerBiAuthorityUrl { get; }
        IApplicationSetting<string> PowerBiResourceUrl { get; }
        IApplicationSetting<string> PowerBiApplicationId { get; }

        IApplicationSetting<string> TraceApiBlackoutStartUrl { get; }
        IApplicationSetting<string> TraceApiBlackoutStopUrl { get; }
        IApplicationSetting<string> TraceApiGetDeviceStateUrl { get; }

        IApplicationSetting<string> HealthEquityAuthorizeGrantSsoProviderSecret { get; }

    }
}


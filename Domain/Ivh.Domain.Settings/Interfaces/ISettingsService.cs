﻿namespace Ivh.Domain.Settings.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Common.Base.Interfaces;

    public interface ISettingsService : IDomainService
    {
        Task<IReadOnlyDictionary<string, string>> GetAllSettingsAsync();
        Task<IReadOnlyDictionary<string, string>> GetAllManagedSettingsAsync();
        Task<IReadOnlyDictionary<string, string>> GetAllEditableManagedSettingsAsync();
        Task<bool> InvalidateCacheAsync();
        Task<bool> OverrideManagedSettingAsync(string key, string overrideValue);
        Task<bool> ClearManagedSettingOverridesAsync();
        Task<bool> ClearManagedSettingOverrideAsync(string key);
        string GetSettingsHash();
        string GetManagedSettingsHash();
        string GetEditableManagedSettingsHash();
    }
}

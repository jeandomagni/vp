﻿namespace Ivh.Domain.Settings.Interfaces
{
    using System;
    using Common.Base.Interfaces;
    using Enums;

    public interface IUrlInterceptService : IUrlService
    {
        string GetCacheKey(UrlEnum urlEnum);
        Uri GetUriFromCache(UrlEnum urlEnum);
        void SetUriIntoCache(UrlEnum urlEnum, Uri uri);
        void RemoveUriFromCache(UrlEnum urlEnum);
    }
}
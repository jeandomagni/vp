﻿namespace Ivh.Domain.Settings.Interfaces
{
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;

    public interface IFeatureService : IDomainService
    {
        bool IsFeatureEnabled(VisitPayFeatureEnum visitPayFeature);
    }
}

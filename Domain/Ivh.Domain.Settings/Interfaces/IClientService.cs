﻿namespace Ivh.Domain.Settings.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IClientService : IDomainService
    {
        Client GetClient();
    }
}
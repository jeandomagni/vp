﻿namespace Ivh.Domain.Settings.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IClientSettingRepository : IRepository<Entities.ClientSetting>
    {
        IList<ClientSetting> GetAllSettings();
    }
}

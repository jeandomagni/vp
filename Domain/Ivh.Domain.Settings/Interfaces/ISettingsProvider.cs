﻿namespace Ivh.Domain.Settings.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface ISettingsProvider
    {
        string SettingsHashCode { get; }
        string GetSettingsHash();
        string GetManagedSettingsHash();
        string GetEditableManagedSettingsHash();
        IReadOnlyDictionary<string, string> GetAllSettings();
        IReadOnlyDictionary<string, string> GetAllManagedSettings();
        IReadOnlyDictionary<string, string> GetAllEditableManagedSettings();
        Task<bool> InvalidateCacheAsync();
        Task<bool> OverrideManagedSettingAsync(string key, string overrideValue);
        Task<bool> ClearManagedSettingOverridesAsync();
        Task<bool> ClearManagedSettingOverrideAsync(string key);
    }
}

﻿namespace Ivh.Domain.Settings.Interfaces
{
    using System;
    using Common.Base.Interfaces;
    using Enums;

    public interface IUrlService : IDomainService
    {
        Uri GetUrl(UrlEnum urlEnum);
    }
}
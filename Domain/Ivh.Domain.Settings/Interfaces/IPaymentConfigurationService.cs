﻿namespace Ivh.Domain.Settings.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IPaymentConfigurationService : IDomainService
    {
        bool IsAchEnabled { get; }
        bool IsCreditCardEnabled { get; }
        bool IsLockBoxEnabled { get; }
        bool IsAvsCodeAccepted(string code);
        bool IsPaymentMethodTypeAccepted(PaymentMethodTypeEnum paymentMethodType);
        IList<string> AcceptedAvsCodes { get; }
        IList<PaymentMethodTypeEnum> AcceptedPaymentMethodTypes { get; }
        IList<PaymentMethodParentTypeEnum> AcceptedPaymentMethodParentTypes { get; }
        PaymentProviderCredentials PaymentProviderCredentials { get; }
        bool TrustCommerceIsLiveMode { get; }
        PaymentProviderCredentials GetPaymentProviderCredentials(string trustCommerceCustomerIdKey, string trustCommercePasswordKey, string trustCommercePasswordQueryApiKey, string nachaIdKey, string nachaSecurityTokenKey);
    }
}
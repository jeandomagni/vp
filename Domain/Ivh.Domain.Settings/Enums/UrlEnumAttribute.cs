﻿namespace Ivh.Domain.Settings.Enums
{
    using System;

    [AttributeUsage(AttributeTargets.Field)]
    public class UrlEnumAttribute : Attribute
    {
        public string SettingKey { get; }
        public bool CanIntercept { get; }
        public string CustomInterceptRoute { get; }
        public UriKind UriKind { get; }
        public bool TrailingSlash { get; }

        public UrlEnumAttribute(string settingKey, UriKind uriKind, bool trailingSlash, bool canIntercept, string customInterceptRoute = null)
        {
            this.SettingKey = settingKey;
            this.UriKind = uriKind;
            this.TrailingSlash = trailingSlash;
            this.CanIntercept = canIntercept;
            this.CustomInterceptRoute = customInterceptRoute;
        }
    }
}

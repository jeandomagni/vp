﻿namespace Ivh.Domain.Settings.Enums
{
    using System;

    public enum UrlEnum
    {
        [UrlEnum("TrustCommerceApiUri", UriKind.Absolute, true, false)]
        TrustCommerceApi,

        [UrlEnum("NachaGatewayUri", UriKind.Absolute, false, false)]
        NachaGateway,

        [UrlEnum("ClientDataApi.Url", UriKind.Absolute, true, false)]
        ClientDataApi,

        [UrlEnum("Twilio.ApiUri", UriKind.Absolute, false, true, "2010-04-01/Accounts/{accountId}/Messages.json")]
        TwilioApi,

        [UrlEnum("Twilio.CallbackUrlBase", UriKind.Absolute, true, false)]
        TwilioCallbackBase,

        [UrlEnum("HealthEquity.ApiUri", UriKind.Absolute, false, false)]
        HealthEquityApi,

        [UrlEnum("HealthEquity.SsoUri", UriKind.Absolute, false, false)]
        HealthEquitySso,

        [UrlEnum("PathToPowershellScripts", UriKind.Absolute, true, false)]
        PathToPowershellScripts,

        [UrlEnum("PathToPowershellScripts2", UriKind.Absolute, true, false)]
        PathToPowershellScripts2,

        [UrlEnum("Enrollment.Url", UriKind.Absolute, false, false)]
        Enrollment,

        [UrlEnum("Enrollment.Jwt.AuthenticationUrl", UriKind.Absolute, false, false)]
        EnrollmentJwtAuthentication,

        [UrlEnum("Analytics.BaseUrl", UriKind.Absolute, false, true, "piwik.js")]
        AnalyticsBase,

        [UrlEnum("SecurePanUrl", UriKind.Absolute, false, true)]
        SecurePan,

        [UrlEnum("GuestPay.Url", UriKind.Absolute, false, true)]
        GuestPayUrl,

        [UrlEnum("PaymentApi.Url", UriKind.Absolute, false, true)]
        PaymentApiUrl,

        [UrlEnum("SecurePanApi.Url", UriKind.Absolute, false, true)]
        SecurePanApiUrl,

        [UrlEnum("TraceApiUri", UriKind.Absolute, false, false)]
        TraceApi
    }
}
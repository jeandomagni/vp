﻿namespace Ivh.Domain.Settings.Attributes
{
    using System;

    [AttributeUsage(AttributeTargets.Property)]
    public sealed class ClientSettingAttribute : Attribute
    {
        public bool CanEditInUI { get; set; }
        public bool IsCmsVariable { get; set; }
    }
}

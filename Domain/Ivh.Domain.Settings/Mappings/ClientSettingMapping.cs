﻿namespace Ivh.Domain.Settings.Mappings
{
    using Common.Base.Constants;
    using FluentNHibernate.Mapping;
    using Entities;

    public class ClientSettingMapping : ClassMap<ClientSetting>
    {
        public ClientSettingMapping()
        {
            this.Schema("common");
            this.Table("ClientSetting");
            this.Id(x => x.ClientSettingKey).GeneratedBy.Assigned();
            this.Map(x => x.ClientSettingValue).Not.Nullable().Length(NhibernateConstants.SqlNvarcharMax);
            this.Map(x => x.ClientSettingValueOverride).Nullable().Length(NhibernateConstants.SqlNvarcharMax);
            this.Map(x => x.ClientSettingDescription);
            this.Map(x => x.ClientSettingName).Not.Nullable();
            this.Map(x => x.DataType).Not.Nullable();
            this.Map(x => x.EditInUI).Not.Nullable();
            this.Map(x => x.IsCmsVariable).Not.Nullable();
            this.Map(x => x.IsEncrypted).Not.Nullable();
            this.Cache.ReadWrite();
        }
    }
}

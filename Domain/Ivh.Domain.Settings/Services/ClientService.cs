﻿namespace Ivh.Domain.Settings.Services
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using Common.Base.Utilities.Helpers;
    using Entities;
    using Interfaces;
    using Logging.Interfaces;

    public class ClientService : IClientService
    {
        private readonly Lazy<ISettingsProvider> _settingsProvider;
        private readonly Lazy<TimeZoneHelper> _timeZoneHelper;

        public ClientService(
            Lazy<ISettingsProvider> settingsProvider,
            Lazy<ILoggingProvider> logger,
            Lazy<TimeZoneHelper> timeZoneHelper)
        {
            this.Logger = logger;
            this._settingsProvider = settingsProvider;
            this._timeZoneHelper = timeZoneHelper;
        }

        public Lazy<ILoggingProvider> Logger { get; }

        private static KeyValuePair<string, ConcurrentDictionary<string, string>>? _settingsByHash;

        public Client GetClient()
        {
            try
            {
                IReadOnlyDictionary<string, string> settings = this._settingsProvider.Value.GetAllSettings();
                string hashCode = this._settingsProvider.Value.SettingsHashCode;

                if (_settingsByHash.HasValue && _settingsByHash.Value.Key == hashCode)
                {
                    return new Client(_settingsByHash.Value.Value, this._timeZoneHelper);
                }

                ConcurrentDictionary<string, string> clientSettings = new ConcurrentDictionary<string, string>(settings, StringComparer.OrdinalIgnoreCase);
                _settingsByHash = new KeyValuePair<string, ConcurrentDictionary<string, string>>(hashCode, clientSettings);

                return new Client(clientSettings, this._timeZoneHelper);
            }
            catch (Exception e)
            {
                this.Logger.Value.Fatal(() => "Unable to Get Settings from Settings Service. {0}", ExceptionHelper.AggregateExceptionToString(e));
                throw;
            }
        }
    }
}
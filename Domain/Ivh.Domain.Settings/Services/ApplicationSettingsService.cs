﻿namespace Ivh.Domain.Settings.Services
{
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.VisitPay.Enums;
    using Entities;
    using Enums;
    using Interfaces;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Reflection;
    using System.Security.Cryptography.X509Certificates;

    public class ApplicationSettingsService : IApplicationSettingsService
    {
        private readonly IReadOnlyDictionary<string, string> _settingsValues;
        private readonly IWebHelper _webHelper = new WebHelper();
        private readonly Lazy<IUrlService> _urlService = null;

        public ApplicationSettingsService(
            Lazy<ISettingsService> settingsService,
            Lazy<IUrlService> urlService)
        {
            this._urlService = urlService;

            IReadOnlyDictionary<string, string> settings = settingsService.Value.GetAllSettingsAsync().Result;
            if (settings.IsNotNullOrEmpty())
            {
                this._settingsValues = settings;
            }
            else
            {
                throw new ConfigurationErrorsException("Could not retrieve application settings.");
            }
        }

        public void Validate()
        {
            Type type = this.GetType();
            this.Load<string>(type);
            this.Load<bool>(type);
            this.Load<int>(type);
            this.Load<Guid>(type);
            this.Load<Uri>(type);
        }

        public Uri GetUrl(UrlEnum urlEnum)
        {
            return this._urlService.Value.GetUrl(urlEnum);
        }

        private void Load<T>(Type type)
        {
            foreach (PropertyInfo property in type.GetProperties().Where(p => p.PropertyType == typeof(Entities.ApplicationSetting<T>)))
            {
                T value = ((Entities.ApplicationSetting<T>)property.GetValue(this)).Value;
            }
        }

        public IApplicationSetting<bool> IsSystemApplication => new ApplicationSetting<bool>(() => new Lazy<bool>(() => this.Application.Value.IsInCategory(ApplicationEnumCategory.SystemApplication)));
        public IApplicationSetting<bool> IsClientApplication => new ApplicationSetting<bool>("IsClientApplication", this._settingsValues, () => false); //different spelling
        public IApplicationSetting<bool> IsWebApplication => new ApplicationSetting<bool>(() => new Lazy<bool>(() => this._webHelper.IsWebApplication));
        public IApplicationSetting<Guid> ProgramUniqueGuid => new ApplicationSetting<Guid>("ProgramUniqueGuid", this._settingsValues);
        public IApplicationSetting<Uri> PathToPowershellScripts => new ApplicationSetting<Uri>("PathToPowershellScripts", this._settingsValues, null, UriKind.Absolute, true);
        public IApplicationSetting<Uri> PathToPowershellScripts2 => new ApplicationSetting<Uri>("PathToPowershellScripts2", this._settingsValues, null, UriKind.Absolute, true);
        public IApplicationSetting<string> DistributedCacheAuthKeyString => new ApplicationSetting<string>("DistributedCacheAuthKeyString", this._settingsValues);
        public IApplicationSetting<string> RabbitMqUserName => new ApplicationSetting<string>("RabbitMqUserName", this._settingsValues);
        public IApplicationSetting<string> RabbitMqPassword => new ApplicationSetting<string>("RabbitMqPassword", this._settingsValues);
        public IApplicationSetting<string> RabbitMqQueueNamePrefix => new ApplicationSetting<string>("RabbitMqQueueNamePrefix", this._settingsValues);
        public IApplicationSetting<bool> RabbitMqEnableSsl => new ApplicationSetting<bool>("RabbitMqEnableSsl", this._settingsValues);
        public IApplicationSetting<string> RabbitMqSslClientCertPath => new ApplicationSetting<string>("RabbitMqSslClientCertPath", this._settingsValues);
        public IApplicationSetting<string> RabbitMqSslClientCertPassword => new ApplicationSetting<string>("RabbitMqSslClientCertPassword", this._settingsValues);
        public IApplicationSetting<string> RabbitMqScheduleQueue => new ApplicationSetting<string>("RabbitMqScheduleQueue", this._settingsValues);
        public IApplicationSetting<string> Environment => new ApplicationSetting<string>("Environment", this._settingsValues, () => "Unknown");
        public IApplicationSetting<IvhEnvironmentTypeEnum> EnvironmentType => new ApplicationSetting<IvhEnvironmentTypeEnum>("EnvironmentType", this._settingsValues, () => IvhEnvironmentTypeEnum.Unknown);
        public IApplicationSetting<string> RedisEncryptionMaster => new ApplicationSetting<string>("RedisEncryptionMaster", this._settingsValues);
        public IApplicationSetting<string> RedisEncryptionAuth => new ApplicationSetting<string>("RedisEncryptionAuth", this._settingsValues);
        public IApplicationSetting<string> RedisEncryptionMasterOld => new ApplicationSetting<string>("RedisEncryptionMasterOld", this._settingsValues);
        public IApplicationSetting<string> RedisEncryptionAuthOld => new ApplicationSetting<string>("RedisEncryptionAuthOld", this._settingsValues);
        public IApplicationSetting<int> RedisMainCacheInstances => new ApplicationSetting<int>("RedisMainCacheInstances", this._settingsValues);
        public IApplicationSetting<string> DataDogServerName => new ApplicationSetting<string>("DataDogServerName", this._settingsValues);
        public IApplicationSetting<string> DataDogPort => new ApplicationSetting<string>("DataDogPort", this._settingsValues);
        public IApplicationSetting<Uri> TrustCommerceApi => new ApplicationSetting<Uri>("TrustCommerceApiUri", this._settingsValues, null, UriKind.Absolute, true);
        public IApplicationSetting<string> TrustCommerceCustomerId => new ApplicationSetting<string>("TrustCommerceCustomerId", this._settingsValues);
        public IApplicationSetting<string> TrustCommercePassword => new ApplicationSetting<string>("TrustCommercePassword", this._settingsValues);
        public IApplicationSetting<string> TrustCommercePasswordQueryApi => new ApplicationSetting<string>("TrustCommercePasswordQueryApi", this._settingsValues);
        public IApplicationSetting<bool> TrustCommerceIsLiveMode => new ApplicationSetting<bool>("TrustCommerceIsLiveMode", this._settingsValues);
        public IApplicationSetting<Uri> NachaGateway => new ApplicationSetting<Uri>("NachaGatewayUri", this._settingsValues, null, UriKind.Absolute, false);
        public IApplicationSetting<string> NachaId => new ApplicationSetting<string>("NachaId", this._settingsValues);
        public IApplicationSetting<string> NachaSecurityToken => new ApplicationSetting<string>("NachaSecurityToken", this._settingsValues);
        public IApplicationSetting<string> MonitoringAuthenticationToken => new ApplicationSetting<string>("monitoringAuthenticationToken", this._settingsValues); //different spelling
        public IApplicationSetting<string> SmtpUsername => new ApplicationSetting<string>("SmtpUsername", this._settingsValues);
        public IApplicationSetting<string> SmtpPassword => new ApplicationSetting<string>("SmtpPassword", this._settingsValues);
        public IApplicationSetting<bool> PreventEmailsFromSending => new ApplicationSetting<bool>("PreventEmailsFromSending", this._settingsValues);
        public IApplicationSetting<bool> RerouteEmailForTesting => new ApplicationSetting<bool>("RerouteEmailForTesting", this._settingsValues);
        public IApplicationSetting<string> RerouteEmailTo => new ApplicationSetting<string>("RerouteEmailTo", this._settingsValues);
        public IApplicationSetting<bool> PreventInterest => new ApplicationSetting<bool>("PreventInterest", this._settingsValues, () => false);
        public IApplicationSetting<bool> EnableFinancialAssistanceInterestRates => new ApplicationSetting<bool>("EnableFinancialAssistanceInterestRates", this._settingsValues, () => false);
        public IApplicationSetting<PaymentAllocationModelTypeEnum> PaymentAllocationModelType => new ApplicationSetting<PaymentAllocationModelTypeEnum>("PaymentAllocationModelType", this._settingsValues, () => PaymentAllocationModelTypeEnum.Unknown);
        public IApplicationSetting<DiscountModelTypeEnum> DiscountModelType => new ApplicationSetting<DiscountModelTypeEnum>("DiscountModelType", this._settingsValues, () => DiscountModelTypeEnum.Unknown);

        public IApplicationSetting<GuarantorEnrollmentProviderEnum> GuarantorEnrollmentProvider => new ApplicationSetting<GuarantorEnrollmentProviderEnum>("GuarantorEnrollmentProvider", this._settingsValues);
        public IApplicationSetting<InterestRateConsiderationServiceEnum> InterestRateConsiderationService => new ApplicationSetting<InterestRateConsiderationServiceEnum>("InterestRateConsiderationService", this._settingsValues);
        public IApplicationSetting<HsFacilityResolutionServiceEnum> HsFacilityResolutionService => new ApplicationSetting<HsFacilityResolutionServiceEnum>("HsFacilityResolutionService", this._settingsValues);
        public IApplicationSetting<HsBillingSystemResolutionProviderEnum> HsBillingSystemResolutionProvider => new ApplicationSetting<HsBillingSystemResolutionProviderEnum>("HsBillingSystemResolutionProvider", this._settingsValues);
        public IApplicationSetting<RegistrationMatchStringApplicationServiceEnum> RegistrationMatchStringApplicationService => new ApplicationSetting<RegistrationMatchStringApplicationServiceEnum>("RegistrationMatchStringApplicationService", this._settingsValues);

        public IApplicationSetting<string> Client => new ApplicationSetting<string>("Client", this._settingsValues, () => "Unknown");
        public IApplicationSetting<string> EnrollmentUsername => new ApplicationSetting<string>("Enrollment.Username", this._settingsValues);
        public IApplicationSetting<string> EnrollmentPassword => new ApplicationSetting<string>("Enrollment.Password", this._settingsValues);
        public IApplicationSetting<string> EnrollmentClientId => new ApplicationSetting<string>("Enrollment.ClientId", this._settingsValues);
        public IApplicationSetting<Uri> EnrollmentJwtAuthentication => new ApplicationSetting<Uri>("Enrollment.Jwt.AuthenticationUrl", this._settingsValues, null, UriKind.Absolute, false);
        public IApplicationSetting<Uri> Enrollment => new ApplicationSetting<Uri>("Enrollment.Url", this._settingsValues, null, UriKind.Absolute, false);

        // FileStorageApplicationService settings
        public IApplicationSetting<string> FileStorageArchiveFolder => new ApplicationSetting<string>("fileStorage:ArchiveFolder", this._settingsValues);
        public IApplicationSetting<string> FileStorageIncomingFolder => new ApplicationSetting<string>("fileStorage:IncomingFolder", this._settingsValues);
        public IApplicationSetting<string> FileStoragePackagingFolder => new ApplicationSetting<string>("fileStorage:PackagingFolder", this._settingsValues);
        public IApplicationSetting<string> FileStorageTempFolder => new ApplicationSetting<string>("fileStorage:TempFolder", this._settingsValues);
        public IApplicationSetting<string> FileStorageSharedKey => new ApplicationSetting<string>("filestorage:SharedKey", this._settingsValues);
        public IApplicationSetting<int> FileStorageArchiveDays => new ApplicationSetting<int>("fileStorage:ArchiveDays", this._settingsValues);


        public IApplicationSetting<string> JamsEtlFromEpicFolder => new ApplicationSetting<string>("JamsEtlFromEpicFolder", this._settingsValues);
        public IApplicationSetting<string> EXE_Directory_StageTableLoader => new ApplicationSetting<string>("EXE_Directory_StageTableLoader", this._settingsValues);

        public IApplicationSetting<string> ServiceBusNameInternal => new ApplicationSetting<string>("ServiceBusName", this._settingsValues, () =>
        {
            Assembly assembly = Assembly.GetEntryAssembly();
            if (assembly != null)
            {
                return assembly.FullName.Split(','.ToArrayOfOne(), StringSplitOptions.RemoveEmptyEntries).First().Trim();
            }

            throw new ConfigurationErrorsException("AppSettings Value Missing: ServiceBusName");
        });

        public IApplicationSetting<string> ApplicationName => new ApplicationSetting<string>("ApplicationName", this._settingsValues, () =>
        {
            string applicationName = this.ServiceBusName.Value;
            applicationName = applicationName.Split('.'.ToArrayOfOne(), StringSplitOptions.None).Last();
            return applicationName;
        });

        public IApplicationSetting<ApplicationEnum> Application => new ApplicationSetting<ApplicationEnum>("Application", this._settingsValues, () => ApplicationEnum.Unknown);

        public IApplicationSetting<IList<string>> ProcessorTypeNames => new ApplicationSetting<IList<string>>(() => new Lazy<IList<string>>(() =>
        {
            string setting = new ApplicationSetting<string>("ProcessorTypeNames", this._settingsValues).Value;
            return setting.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).ToList();
        }));

        public IApplicationSetting<string> ServiceBusName => new ApplicationSetting<string>(() => new Lazy<string>(() => $"{this.RabbitMqQueueNamePrefix.Value}::{this.ServiceBusNameInternal.Value}"));

        public IApplicationSetting<string> ScheduleServiceBusName => new ApplicationSetting<string>(() => new Lazy<string>(() => $"{this.RabbitMqQueueNamePrefix.Value}::{this.RabbitMqScheduleQueue.Value}"));

        public IApplicationSetting<string> ServiceBusVirtualHost => new ApplicationSetting<string>(() => new Lazy<string>(() => this.Client.Value.ToString().ToLower()));

        public IApplicationSetting<IDictionary<string, string>> Redirect => new ApplicationSetting<IDictionary<string, string>>(() => new Lazy<IDictionary<string, string>>(() =>
        {
            try
            {
                return JsonConvert.DeserializeObject<Dictionary<string, string>>((new ApplicationSetting<string>("Redirect", this._settingsValues)).Value);
            }
            catch (Exception)
            {
                return new Dictionary<string, string>();
            }
        }));

        public IApplicationSetting<bool> PublishInterestAssessment => new ApplicationSetting<bool>("PublishInterestAssessment", this._settingsValues, () => false);
        public IApplicationSetting<bool> ShowBuildBanner => new ApplicationSetting<bool>("ShowBuildBanner", this._settingsValues, () => false);

        public IApplicationSetting<IList<string>> AcceptedAvsCodes => new ApplicationSetting<IList<string>>(() => new Lazy<IList<string>>(() =>
        {
            string setting = new ApplicationSetting<string>("AcceptedAvsCodes", this._settingsValues).Value;
            return setting.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).ToList();
        }));

        public IApplicationSetting<bool> IsAchEnabled => new ApplicationSetting<bool>("PaymentType.IsAchEnabled", this._settingsValues, () => false);
        public IApplicationSetting<bool> IsCreditCardEnabled => new ApplicationSetting<bool>("PaymentType.IsCreditCardEnabled", this._settingsValues, () => false);
        public IApplicationSetting<bool> IsLockBoxEnabled => new ApplicationSetting<bool>("PaymentType.IsLockBoxEnabled", this._settingsValues, () => false);

        public IApplicationSetting<bool> FeatureDemoIsAlertEnabled => new ApplicationSetting<bool>("Feature.Demo.IsAlertEnabled", this._settingsValues, () => false);

        public IApplicationSetting<string> SsoEncryptionKey => new ApplicationSetting<string>("Sso.EncryptionKey", this._settingsValues, () => "");
        public IApplicationSetting<int> SsoTimeoutTokensAfterMinutes => new ApplicationSetting<int>("Sso.EncryptionKey", this._settingsValues, () => 5);

        public IApplicationSetting<int> AnalyticsId => new ApplicationSetting<int>("Analytics.Id", this._settingsValues);
        public IApplicationSetting<Uri> AnalyticsBase => new ApplicationSetting<Uri>("Analytics.BaseUrl", this._settingsValues, null, UriKind.Absolute, false);
        public IApplicationSetting<Uri> SecurePan => new ApplicationSetting<Uri>("SecurePanUrl", this._settingsValues, null, UriKind.Absolute, true);
        public IApplicationSetting<string> SecurePanAuthKey => new ApplicationSetting<string>("SecurePanAuthKey", this._settingsValues);
        public IApplicationSetting<string> SecurePanEncryptionKey => new ApplicationSetting<string>("SecurePanEncryptionKey", this._settingsValues);

        public IApplicationSetting<ApiEnum> Api => new ApplicationSetting<ApiEnum>("Api", this._settingsValues, () => ApiEnum.Unknown); //this needs to be set on the Api service side

        public IApplicationSetting<string> ClientDataApiAppId => new ApplicationSetting<string>("ClientDataApi.AppId", this._settingsValues, () => null); // this is the client-side AppId for VisitPay
        public IApplicationSetting<string> ClientDataApiAppKey => new ApplicationSetting<string>("ClientDataApi.AppKey", this._settingsValues, () => null); // this is the client-side AppKey for VisitPay
        public IApplicationSetting<Uri> ClientDataApi => new ApplicationSetting<Uri>("ClientDataApi.Url", this._settingsValues, null, UriKind.Absolute, true);
        public IApplicationSetting<int> ClientDataApiTimeToLive => new ApplicationSetting<int>("ClientDataApi.TimeToLive", this._settingsValues, () => 15);

        public IApplicationSetting<int> PastDueAgingCount => new ApplicationSetting<int>("PastDueAgingCount", this._settingsValues);
        public IApplicationSetting<CmsRegionEnum> ThemeCssRegion => new ApplicationSetting<CmsRegionEnum>("ThemeCssRegion", this._settingsValues, () => CmsRegionEnum.Unknown);

        public IApplicationSetting<bool> RerouteSmsForTesting => new ApplicationSetting<bool>("RerouteSmsForTesting", this._settingsValues);
        public IApplicationSetting<string> RerouteSmsTo => new ApplicationSetting<string>("RerouteSmsTo", this._settingsValues);
        public IApplicationSetting<bool> FeatureSmsEnabled => new ApplicationSetting<bool>("Feature.Sms.Enabled", this._settingsValues, () => false);
        public IApplicationSetting<string> TwilioAccountSid => new ApplicationSetting<string>("Twilio.AccountSid", this._settingsValues, () => null);
        public IApplicationSetting<string> TwilioAuthToken => new ApplicationSetting<string>("Twilio.AuthToken", this._settingsValues, () => null);
        public IApplicationSetting<string> TwilioMessagingServiceSid => new ApplicationSetting<string>("Twilio.MessagingServiceSid", this._settingsValues, () => null);
        public IApplicationSetting<string> TwilioCallbackUsername => new ApplicationSetting<string>("Twilio.CallbackUsername", this._settingsValues, () => null);
        public IApplicationSetting<string> TwilioCallbackPassword => new ApplicationSetting<string>("Twilio.CallbackPassword", this._settingsValues, () => null);
        public IApplicationSetting<Uri> TwilioCallbackBase => new ApplicationSetting<Uri>("Twilio.CallbackUrlBase", this._settingsValues, null, UriKind.Absolute, true);
        public IApplicationSetting<Uri> TwilioApi => new ApplicationSetting<Uri>("Twilio.ApiUri", this._settingsValues, null, UriKind.Absolute, false);

        public IApplicationSetting<Uri> HealthEquityApi => new ApplicationSetting<Uri>("HealthEquity.ApiUri", this._settingsValues, null, UriKind.Absolute, false);

        public IApplicationSetting<IDictionary<string, string>> HealthEquityApiUserNames => new ApplicationSetting<IDictionary<string, string>>(() => new Lazy<IDictionary<string, string>>(() =>
        {
            if (!this._settingsValues.ContainsKey("HealthEquity.ApiUserNames"))
            {
                throw new ConfigurationErrorsException("HealthEquity.ApiUserNames is missing.");
            }

            string json = this._settingsValues["HealthEquity.ApiUserNames"];
            return JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
        }));

        public IApplicationSetting<IDictionary<string, string>> HealthEquityApiPasswords => new ApplicationSetting<IDictionary<string, string>>(() => new Lazy<IDictionary<string, string>>(() =>
        {
            if (!this._settingsValues.ContainsKey("HealthEquity.ApiPasswords"))
            {
                throw new ConfigurationErrorsException("HealthEquity.ApiPasswords is missing.");
            }

            string json = this._settingsValues["HealthEquity.ApiPasswords"];
            return JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
        }));

        public IApplicationSetting<Uri> HealthEquitySso => new ApplicationSetting<Uri>("HealthEquity.SsoUri", this._settingsValues, null, UriKind.Absolute, false);
        public IApplicationSetting<string> HealthEquitySsoBase64EncodedCertificate => new ApplicationSetting<string>("HealthEquity.SsoBase64EncodedCertificate", this._settingsValues, () => null);
        public IApplicationSetting<string> HealthEquitySsoBase64EncodedCertificatePassword => new ApplicationSetting<string>("HealthEquity.SsoBase64EncodedCertificatePassword", this._settingsValues, () => null);
        public IApplicationSetting<X509Certificate2> HealthEquitySsoCertificate => new ApplicationSetting<X509Certificate2>(() => new Lazy<X509Certificate2>(() => new X509Certificate2(Convert.FromBase64String(this.HealthEquitySsoBase64EncodedCertificate.Value), this.HealthEquitySsoBase64EncodedCertificatePassword.Value)));
        public IApplicationSetting<bool> HealthEquitySsoSamlEndpointValidate => new ApplicationSetting<bool>("HealthEquity.SsoSamlEndpointValidate", this._settingsValues, () => false);
        public IApplicationSetting<bool> HealthEquitySsoSamlRequireHsa => new ApplicationSetting<bool>("HealthEquity.SsoSamlRequireHsa", this._settingsValues, () => false);
        public IApplicationSetting<string> HealthEquitySsoSamlResponseIssuer => new ApplicationSetting<string>("HealthEquity.SsoSamlResponseIssuer", this._settingsValues, () => null);
        public IApplicationSetting<int> HealthEquitySsoSamlResponseExpiration => new ApplicationSetting<int>("HealthEquity.SsoSamlResponseExpiration", this._settingsValues, () => 5);

        public IApplicationSetting<string> HealthEquityOAuthResourceServerBaseAddress => new ApplicationSetting<string>("HealthEquity.OAuth.ResourceServerBaseAddress", this._settingsValues, () => null);
        public IApplicationSetting<string> HealthEquityOAuthMemberBalancePath => new ApplicationSetting<string>("HealthEquity.OAuth.MemberBalancePath", this._settingsValues, () => null);
        public IApplicationSetting<string> HealthEquityOAuthSsoPath => new ApplicationSetting<string>("HealthEquity.OAuth.SsoPath", this._settingsValues, () => null);
        public IApplicationSetting<string> HealthEquityOAuthSsoTokenPath => new ApplicationSetting<string>("HealthEquity.OAuth.SsoTokenPath", this._settingsValues, () => null);
        public IApplicationSetting<string> HealthEquityOAuthCryptKey => new ApplicationSetting<string>("HealthEquity.OAuth.CryptKey", this._settingsValues, () => null);
        public IApplicationSetting<string> HealthEquityOAuthAuthKey => new ApplicationSetting<string>("HealthEquity.OAuth.AuthKey", this._settingsValues, () => null);

        public IApplicationSetting<IList<VpTransactionTypeEnum>> HsCurrentBalanceSumExcludedTransactionTypes => new ApplicationSetting<IList<VpTransactionTypeEnum>>(() => new Lazy<IList<VpTransactionTypeEnum>>(() =>
        {
            if (!this._settingsValues.ContainsKey("HsCurrentBalanceSumExcludedTransactionTypes"))
            {
                return null;
            }

            string json = this._settingsValues["HsCurrentBalanceSumExcludedTransactionTypes"];
            return JsonConvert.DeserializeObject<List<VpTransactionTypeEnum>>(json);
        }));

        public IApplicationSetting<IDictionary<int, PasswordHashConfiguration>> PasswordHashConfigurations => new ApplicationSetting<IDictionary<int, PasswordHashConfiguration>>(() => new Lazy<IDictionary<int, PasswordHashConfiguration>>(() =>
        {
            if (!this._settingsValues.ContainsKey("PasswordHashConfigurations"))
            {
                throw new ConfigurationErrorsException("PasswordHashConfigurations is missing.");
            }

            string json = this._settingsValues["PasswordHashConfigurations"];
            return JsonConvert.DeserializeObject<Dictionary<int, PasswordHashConfiguration>>(json);
        }));

        public IApplicationSetting<int> Load835FileMaxDegreeOfParallelism => new ApplicationSetting<int>("Load835FileMaxDegreeOfParallelism", this._settingsValues, () => 4);

        public IApplicationSetting<Uri> SsrsReportServerUri => new ApplicationSetting<Uri>("SsrsReportServerUri", this._settingsValues, null, UriKind.Absolute, false);
        public IApplicationSetting<string> SsrsReportPath => new ApplicationSetting<string>("SsrsReportPath", this._settingsValues, () => null);
        public IApplicationSetting<string> SsrsReportServerUserName => new ApplicationSetting<string>("SsrsReportServerUserName", this._settingsValues, () => null);
        public IApplicationSetting<string> SsrsReportServerPassword => new ApplicationSetting<string>("SsrsReportServerPassword", this._settingsValues, () => null);
        public IApplicationSetting<string> SsrsReportServerDomain => new ApplicationSetting<string>("SsrsReportServerDomain", this._settingsValues, () => null);

        public IApplicationSetting<int> ScoringBatchLoadBatchSize => new ApplicationSetting<int>("Scoring.BatchLoad.BatchSize", this._settingsValues, () => 100);
        public IApplicationSetting<string> ScoringRscriptEndPointUrl => new ApplicationSetting<string>("Scoring.RscriptEndPoint.Url", this._settingsValues, () => null);
        public IApplicationSetting<string> ScoringThirdPartyUrl => new ApplicationSetting<string>("Scoring.ThirdParty.Url", this._settingsValues, () => null);
        public IApplicationSetting<string> ScoringThirdPartySecurityToken => new ApplicationSetting<string>("Scoring.ThirdParty.SecurityToken", this._settingsValues, () => null);
        public IApplicationSetting<string> ScoringThirdPartyXmlNamespace => new ApplicationSetting<string>("Scoring.ThirdParty.XmlNamespace", this._settingsValues, () => null);
        public IApplicationSetting<bool> ScoringLogInfoEnable => new ApplicationSetting<bool>("Scoring.LogInfo.Enable", this._settingsValues, () => true);

        public IApplicationSetting<int> SegmentationBatchLoadBatchSize => new ApplicationSetting<int>("Segmentation.BatchLoad.BatchSize", this._settingsValues, () => 1000);

        public IApplicationSetting<string> ClientTimeZone => new ApplicationSetting<string>("ClientTimeZone", this._settingsValues, () => "Mountain Standard Time");

        public IApplicationSetting<string> GuestPayRemotePayApiKey => new ApplicationSetting<string>("GuestPay.RemotePay.ApiKey", this._settingsValues, () => null);

        public IApplicationSetting<string> PowerBiUsername => new ApplicationSetting<string>("PowerBI.Username", this._settingsValues, () => null);
        public IApplicationSetting<string> PowerBiPassword => new ApplicationSetting<string>("PowerBI.Password", this._settingsValues, () => null);
        public IApplicationSetting<string> PowerBiApiUrl => new ApplicationSetting<string>("PowerBI.ApiUrl", this._settingsValues, () => null);
        public IApplicationSetting<string> PowerBiAuthorityUrl => new ApplicationSetting<string>("PowerBI.AuthorityUrl", this._settingsValues, () => null);
        public IApplicationSetting<string> PowerBiResourceUrl => new ApplicationSetting<string>("PowerBI.ResourceUrl", this._settingsValues, () => null);
        public IApplicationSetting<string> PowerBiApplicationId => new ApplicationSetting<string>("PowerBI.ApplicationId", this._settingsValues, () => null);

        public IApplicationSetting<string> TraceApiBlackoutStartUrl => new ApplicationSetting<string>("TraceApi.BlackoutStartUrl", this._settingsValues, () => null);
        public IApplicationSetting<string> TraceApiBlackoutStopUrl => new ApplicationSetting<string>("TraceApi.BlackoutStopUrl", this._settingsValues, () => null);
        public IApplicationSetting<string> TraceApiGetDeviceStateUrl => new ApplicationSetting<string>("TraceApi.GetDeviceStateUrl", this._settingsValues, () => null);

        public IApplicationSetting<string> HealthEquityAuthorizeGrantSsoProviderSecret => new ApplicationSetting<string>("HealthEquity_AuthorizeGrant_SsoProviderSecret", this._settingsValues, () => null);

        public IApplicationSetting<string> GetString(string key)
        {
            return new Entities.ApplicationSetting<string>(key, this._settingsValues, () => "");
        }

        public PaymentProviderCredentials GetPaymentProviderCredentials(
            string trustCommerceCustomerIdKey,
            string trustCommercePasswordKey,
            string trustCommercePasswordQueryApiKey,
            string nachaIdKey,
            string nachaSecurityTokenKey)
        {
            return new PaymentProviderCredentials()
            {
                TrustCommerceCustomerId = this.GetString(trustCommerceCustomerIdKey).Value,
                TrustCommercePassword = this.GetString(trustCommercePasswordKey).Value,
                TrustCommercePasswordQueryApi = this.GetString(trustCommercePasswordQueryApiKey).Value,
                NachaId = this.GetString(nachaIdKey).Value,
                NachaSecurityToken = this.GetString(nachaSecurityTokenKey).Value
            };
        }

        public AddressVerificationProviderCredentials GetAddressVerificationProviderCredentials()
        {
            const string uspsApiUserIdKey = "UspsApi.WebToolsUserId";
            const string uspsApiPasswordKey = "UspsApi.Password";

            string password = this.GetString(uspsApiPasswordKey).Value;
            string userId = this.GetString(uspsApiUserIdKey).Value;

            return new AddressVerificationProviderCredentials(userId, password);
        }

        public IApplicationSetting<string> UspsApiUrlHostname => new ApplicationSetting<string>("UspsApi.Url.Hostname", this._settingsValues, () => null);
        public IApplicationSetting<string> UspsApiUrlPath => new ApplicationSetting<string>("UspsApi.Url.Path", this._settingsValues, () => null);

        public IApplicationSetting<Guid> PingAuthorizationKey => new ApplicationSetting<Guid>("Ping.AuthorizationKey", this._settingsValues);
    }
}


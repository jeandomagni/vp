﻿namespace Ivh.Domain.Settings.Services
{
    using System;
    using Interfaces;

    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class SettingsService : ISettingsService
    {
        private readonly Lazy<ISettingsProvider> _settingsProvider;

        public SettingsService(
            Lazy<ISettingsProvider> settingsProvider)
        {
            this._settingsProvider = settingsProvider;
        }

        /// <summary>
        /// This is the client-side method call
        /// </summary>
        /// <returns></returns>
        public Task<IReadOnlyDictionary<string, string>> GetAllSettingsAsync()
        {
            return Task.FromResult(this._settingsProvider.Value.GetAllSettings());
        }

        public Task<IReadOnlyDictionary<string, string>> GetAllManagedSettingsAsync()
        {
            return Task.FromResult(this._settingsProvider.Value.GetAllManagedSettings());
        }

        public Task<IReadOnlyDictionary<string, string>> GetAllEditableManagedSettingsAsync()
        {
            return Task.FromResult(this._settingsProvider.Value.GetAllEditableManagedSettings());
        }

        public async Task<bool> InvalidateCacheAsync()
        {
            return await this._settingsProvider.Value.InvalidateCacheAsync();
        }

        public async Task<bool> OverrideManagedSettingAsync(string key, string overrideValue)
        {
            return await this._settingsProvider.Value.OverrideManagedSettingAsync(key, overrideValue);
        }

        public async Task<bool> ClearManagedSettingOverridesAsync()
        {
            return await this._settingsProvider.Value.ClearManagedSettingOverridesAsync();
        }

        public async Task<bool> ClearManagedSettingOverrideAsync(string key)
        {
            return await this._settingsProvider.Value.ClearManagedSettingOverrideAsync(key);
        }

        public string GetSettingsHash()
        {
            return this._settingsProvider.Value.SettingsHashCode;
        }

        public string GetManagedSettingsHash()
        {
            return this._settingsProvider.Value.GetManagedSettingsHash();
        }

        public string GetEditableManagedSettingsHash()
        {
            return this._settingsProvider.Value.GetEditableManagedSettingsHash();
        }
    }
}

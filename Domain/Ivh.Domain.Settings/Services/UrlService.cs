﻿namespace Ivh.Domain.Settings.Services
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Reflection;
    using Common.Base.Utilities.Extensions;
    using Enums;
    using Interfaces;

    public class UrlService : IUrlService
    {
        protected readonly Lazy<ISettingsService> SettingService;

        public UrlService(
            Lazy<ISettingsService> settingService
            )
        {
            this.SettingService = settingService;
        }
        public virtual Uri GetUrl(UrlEnum urlEnum)
        {
            IReadOnlyDictionary<string, string> settingsValues = this.SettingService.Value.GetAllSettingsAsync().Result;
            UrlEnumAttribute urlEnumAttribute = urlEnum.GetAttribute<UrlEnumAttribute>();
            if(urlEnumAttribute != null)
            {
                if (settingsValues.ContainsKey(urlEnumAttribute.SettingKey))
                {
                    return settingsValues[urlEnumAttribute.SettingKey].ToUri(urlEnumAttribute.UriKind, urlEnumAttribute.TrailingSlash);
                }
                throw new ConfigurationErrorsException($"{urlEnum}.{urlEnumAttribute.SettingKey} does not exisit in settings.");
            }
            throw new CustomAttributeFormatException($"{urlEnum} does not have UrlEnumAttribute");
        }
    }
}
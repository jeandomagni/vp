﻿namespace Ivh.Domain.Settings.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Enums;
    using Common.Base.Utilities.Helpers;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;

    public class PaymentConfigurationService : IPaymentConfigurationService
    {
        private readonly Lazy<IApplicationSettingsService> _applicationSettings;

        private PaymentProviderCredentials _paymentProviderCredentials;

        private IList<string> _acceptedAvsCodes;

        private IList<PaymentMethodTypeEnum> _acceptedPaymentMethodTypes;

        private IList<PaymentMethodParentTypeEnum> _acceptedPaymentMethodParentTypes;

        public PaymentConfigurationService(Lazy<IApplicationSettingsService> applicationSettings)
        {
            this._applicationSettings = applicationSettings;
        }

        public bool IsAchEnabled => this._applicationSettings.Value.IsAchEnabled.Value;

        public bool IsCreditCardEnabled => this._applicationSettings.Value.IsCreditCardEnabled.Value;

        public bool IsLockBoxEnabled => this._applicationSettings.Value.IsLockBoxEnabled.Value;

        public bool IsAvsCodeAccepted(string code)
        {
            if (this.AcceptedAvsCodes == null || !this.AcceptedAvsCodes.Any())
            {
                return true;
            }

            if (this.AcceptedAvsCodes.Any(x => string.Equals(x, code, StringComparison.CurrentCultureIgnoreCase)))
            {
                return true;
            }

            return false;
        }

        public bool IsPaymentMethodTypeAccepted(PaymentMethodTypeEnum paymentMethodType)
        {
            return this.AcceptedPaymentMethodTypes.Any(x => x == paymentMethodType);
        }

        public IList<string> AcceptedAvsCodes => this._acceptedAvsCodes ?? (this._acceptedAvsCodes = this._applicationSettings.Value.AcceptedAvsCodes.Value);

        public IList<PaymentMethodTypeEnum> AcceptedPaymentMethodTypes => this._acceptedPaymentMethodTypes ?? (this._acceptedPaymentMethodTypes = this.GetAcceptedPaymentMethodTypes());

        public IList<PaymentMethodParentTypeEnum> AcceptedPaymentMethodParentTypes => this._acceptedPaymentMethodParentTypes ?? (this._acceptedPaymentMethodParentTypes = this.GetAcceptedPaymentMethodParentTypes());

        public PaymentProviderCredentials PaymentProviderCredentials => this._paymentProviderCredentials ?? (this._paymentProviderCredentials = this.GetPaymentProviderCredentials("TrustCommerceCustomerId", "TrustCommercePassword", "TrustCommercePasswordQueryApi", "NachaId", "NachaSecurityToken"));

        public bool TrustCommerceIsLiveMode => this._applicationSettings.Value.TrustCommerceIsLiveMode.Value;

        private IList<PaymentMethodTypeEnum> GetAcceptedPaymentMethodTypes()
        {
            List<PaymentMethodTypeEnum> enums = new List<PaymentMethodTypeEnum>();

            if (this._applicationSettings.Value.IsAchEnabled.Value)
            {
                enums.AddRange(EnumHelper<PaymentMethodTypeEnum>.GetValues(PaymentMethodTypeEnumCategory.Ach));
            }

            if (this._applicationSettings.Value.IsCreditCardEnabled.Value)
            {
                enums.AddRange(EnumHelper<PaymentMethodTypeEnum>.GetValues(PaymentMethodTypeEnumCategory.Card));
            }

            if (this._applicationSettings.Value.IsLockBoxEnabled.Value)
            {
                enums.AddRange(EnumHelper<PaymentMethodTypeEnum>.GetValues(PaymentMethodTypeEnumCategory.LockBox));
            }

            return enums;
        }

        private IList<PaymentMethodParentTypeEnum> GetAcceptedPaymentMethodParentTypes()
        {
            List<PaymentMethodParentTypeEnum> enums = new List<PaymentMethodParentTypeEnum>();

            if (this._applicationSettings.Value.IsAchEnabled.Value)
            {
                enums.Add(PaymentMethodParentTypeEnum.Ach);
            }

            if (this._applicationSettings.Value.IsCreditCardEnabled.Value)
            {
                enums.Add(PaymentMethodParentTypeEnum.CreditDebitCard);
            }

            return enums;
        }

        public PaymentProviderCredentials GetPaymentProviderCredentials(
            string trustCommerceCustomerIdKey,
            string trustCommercePasswordKey,
            string trustCommercePasswordQueryApiKey,
            string nachaIdKey,
            string nachaSecurityTokenKey)
        {
            return this._applicationSettings.Value.GetPaymentProviderCredentials(trustCommerceCustomerIdKey, trustCommercePasswordKey, trustCommercePasswordQueryApiKey, nachaIdKey, nachaSecurityTokenKey);
        }

        /** avs codes for reference **/
        /*
            "A", "Street address matches, but five-digit and nine-digit postal code do not match."
            "D",  "Street address and postal code match. Code \"M\" is equivalent."
            "G", "Non-U.S. issuing bank does not support AVS."
            "J", "Card member's name, billing address, and postal code match."
            "M", "Street address and postal code match. Code \"D\" is equivalent."
            "N", "Street address and postal code do not match."
            "Q", "Card member's name, billing address, and postal code match."
            "S", "Bank does not support AVS."
            "U", "Address information unavailable."
            "V", "Card member's name, billing address, and billing postal code match."
            "X", "Street address and nine-digit postal code match."
            "Y", "Street address and five-digit postal code match."
            "Z", "Street address does not match, but five-digit postal code matches."

            "B", "Street address matches, but postal code not verified."
            "C", "Street address and postal code do not match."
            "E", "AVS data is invalid or AVS is not allowed for this card type."
            "F", "Card member's name does not match, but billing postal code matches."
            "H", "Card member's name does not match. Street address and postal code match."
            "I", "Address not verified."
            "K", "Card member's name matches but billing address and billing postal code do not match."
            "L", "Card member's name and billing postal code match, but billing address does not match."
            "O", "Card member's name and billing address match, but billing postal code does not match."
            "P", "Postal code matches, but street address not verified."
            "R", "System unavailable."
            "T", "Card member's name does not match, but street address matches."
            "W", "Street address does not match, but nine-digit postal code matches."
        */
    }
}
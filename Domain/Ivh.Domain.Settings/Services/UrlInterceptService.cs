﻿namespace Ivh.Domain.Settings.Services
{
    using System;
    using System.Threading.Tasks;
    using Common.Base.Utilities.Extensions;
    using Common.Cache;
    using Enums;
    using Interfaces;

    public class UrlInterceptService : UrlService, IUrlInterceptService, IUrlService
    {
        private readonly Lazy<IDistributedCache> _cache;
        private static string _cacheKeyTemplate;
        private static readonly object Lock = new object();

        public UrlInterceptService(
            Lazy<ISettingsService> settingService,
             Lazy<IApplicationSettingsService> applicationSettingsService,
             Lazy<IDistributedCache> distributedCache
             ) : base(settingService)
        {
            this._cache = distributedCache;

            if (_cacheKeyTemplate == null)
            {
                lock (Lock)
                {
                    string client = applicationSettingsService.Value.Client.Value.ToString();
                    string environment = applicationSettingsService.Value.Environment.Value.ToString();
                    _cacheKeyTemplate = this.GetType().Namespace + "." + client + "." + environment + ".";
                }
            }
        }

        public override Uri GetUrl(UrlEnum urlEnum)
        {
            Uri uri = base.GetUrl(urlEnum);
            UrlEnumAttribute urlEnumAttribute = urlEnum.GetAttribute<UrlEnumAttribute>();
            if (urlEnumAttribute != null && urlEnumAttribute.CanIntercept)
            {
                Uri uriFromCache = this.GetUriFromCache(urlEnum);
                if (uri != null && uriFromCache != null)
                {
                    /*UriBuilder uriBuilder = new UriBuilder(uri)
                    {
                        Scheme = uriFromCache.Scheme,
                        Host = uriFromCache.Host,
                        Port = uriFromCache.Port
                    };

                    return uriBuilder.Uri;*/
                    return uriFromCache;
                }
            }

            return uri;
        }

        public string GetCacheKey(UrlEnum urlEnum)
        {
            return _cacheKeyTemplate + urlEnum;
        }

        public Uri GetUriFromCache(UrlEnum urlEnum)
        {
            string key = this.GetCacheKey(urlEnum);

            string uriStringFromCache = Task.Run(async () => await this._cache.Value.GetStringAsync(key)).Result;
            if (uriStringFromCache.IsNotNullOrEmpty())
            {
                return new Uri(uriStringFromCache);
            }
            return null;
        }

        public void SetUriIntoCache(UrlEnum urlEnum, Uri uri)
        {
            string key = this.GetCacheKey(urlEnum);
            try
            {
                if (this._cache.Value.GetLock(key))
                {
                    Task.Run(async () => await this._cache.Value.SetStringAsync(this.GetCacheKey(urlEnum), uri.AbsoluteUri)).Wait();
                }
            }
            finally
            {
                this._cache.Value.ReleaseLock(key);
            }
        }

        public void RemoveUriFromCache(UrlEnum urlEnum)
        {
            string key = this.GetCacheKey(urlEnum);
            try
            {
                if (this._cache.Value.GetLock(key))
                {
                    Task.Run(async () => await this._cache.Value.RemoveAsync(this.GetCacheKey(urlEnum))).Wait();
                }
            }
            finally
            {
                this._cache.Value.ReleaseLock(key);
            }
        }
    }
}
﻿namespace Ivh.Domain.Settings.Services
{
    using System;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;
    using Logging.Interfaces;

    public class FeatureService : IFeatureService
    {
        private readonly Lazy<IApplicationSettingsService> _applicationSettingService;
        private readonly Lazy<IClientService> _clientService;
        private readonly Lazy<IPaymentConfigurationService> _paymentConfigurationService;
        private readonly Lazy<ILoggingProvider> _logger;
        
        public FeatureService(
            Lazy<IApplicationSettingsService> applicationSettingService,
            Lazy<IClientService> clientService,
            Lazy<IPaymentConfigurationService> paymentConfigurationService,
            Lazy<ILoggingProvider> logger
            )
        {
            this._applicationSettingService = applicationSettingService;
            this._clientService = clientService;
            this._paymentConfigurationService = paymentConfigurationService;
            this._logger = logger;
        }

        public bool IsFeatureEnabled(VisitPayFeatureEnum visitPayFeature)
        {
            if (this.IsFeatureCategoryValid(visitPayFeature))
            {
                return this.CheckFeatureEnabled(visitPayFeature);
            }

            return false;
        }

        private bool CheckFeatureEnabled(VisitPayFeatureEnum visitPayFeature)
        {
            try
            {
                Client client = this._clientService.Value.GetClient();

                switch (visitPayFeature)
                {
                    case VisitPayFeatureEnum.AlternatePublicPages:
                        return client.FeatureAlternatePublicPages;

                    case VisitPayFeatureEnum.CancelRecurringPaymentClient:
                        return client.FeatureCancelRecurringIsEnabledClient;

                    case VisitPayFeatureEnum.CancelRecurringPaymentGuarantor:
                        return client.FeatureCancelRecurringIsEnabledGuarantor &&
                               client.CancelRecurringPaymentMaximum > 0;

                    case VisitPayFeatureEnum.CancelRecurringPaymentPdGuarantor:
                        return client.FeatureCancelRecurringIsEnabledGuarantor &&
                               client.FeatureCancelRecurringPdIsEnabledGuarantor;

                    case VisitPayFeatureEnum.CombineFinancePlans:
                        return client.FeatureCombineFinancePlansEnabled;

                    case VisitPayFeatureEnum.Consolidation:
                        return client.FeatureConsolidationIsEnabled;

                    case VisitPayFeatureEnum.ConsolidationMasking:
                        return client.FeatureConsolidationMaskingIsEnabled;

                    case VisitPayFeatureEnum.DemoIsAlert:
                        return this._applicationSettingService.Value.FeatureDemoIsAlertEnabled.Value;

                    case VisitPayFeatureEnum.FpOfferSetTypesIsEnabledClient:
                        return client.FeatureFpOfferSetTypesIsEnabledClient;

                    case VisitPayFeatureEnum.HealthEquityFeature:
                        return client.HealthEquityEnabled;

                    case VisitPayFeatureEnum.HealthEquityShowOutboundSso:
                        return client.HealthEquityShowOutboundSso;

                    case VisitPayFeatureEnum.HealthEquityShowBalance:
                        return client.HealthEquityShowBalance;

                    case VisitPayFeatureEnum.HouseholdBalance:
                        return client.FeatureHouseholdBalanceIsEnabled;

                    case VisitPayFeatureEnum.Itemization:
                        return client.FeatureItemizationIsEnabled;

                    case VisitPayFeatureEnum.PaymentSummary:
                        return client.FeaturePaymentSummaryIsEnabled;

                    case VisitPayFeatureEnum.PaymentsAch:
                        return this._paymentConfigurationService.Value.IsAchEnabled;

                    case VisitPayFeatureEnum.PaymentsCreditCard:
                        return this._paymentConfigurationService.Value.IsCreditCardEnabled;

                    case VisitPayFeatureEnum.RescheduleRecurringPaymentClient:
                        return client.FeatureRescheduleRecurringIsEnabledClient;

                    case VisitPayFeatureEnum.RescheduleRecurringPaymentGuarantor:
                        return client.FeatureRescheduleRecurringIsEnabledGuarantor &&
                               client.RescheduleRecurringPaymentMaximum > 0;

                    case VisitPayFeatureEnum.MyChartSso:
                        return client.MyChartSsoIsEnabled;

                    case VisitPayFeatureEnum.Sms:
                        return this._applicationSettingService.Value.FeatureSmsEnabled.Value;

                    case VisitPayFeatureEnum.Surveys:
                        return client.FeatureSurveyIsEnabled;

                    case VisitPayFeatureEnum.ExplanationOfBenefitsImport:
                        return client.FeatureEobImportIsEnabled;

                    case VisitPayFeatureEnum.ExplanationOfBenefitsUserInterface:
                        return client.FeatureEobUserInterfaceIsEnabled;

                    case VisitPayFeatureEnum.OfflineVisitPay:
                        return client.OfflineVisitPayIsEnabled;

                    case VisitPayFeatureEnum.BalanceTransfer:
                        return client.BalanceTransferIsEnabled;

                    case VisitPayFeatureEnum.CalculateCombinedFinancePlanMinimumIsEnabled:
                        return client.CalculateCombinedFinancePlanMinimumIsEnabled;

                    case VisitPayFeatureEnum.MerchantAccountSplittingIsEnabled:
                        return client.MerchantAccountSplittingIsEnabled;

                    case VisitPayFeatureEnum.GuarantorSecurityQuestionsIsEnabled:
                        return client.GuarantorRequireSecurityQuestionsForRecovery && client.GuarantorSecurityQuestionsCount > 0;

                    case VisitPayFeatureEnum.VisitPayReportsIsEnabled:
                        return client.FeatureVisitPayReportsIsEnabled;

                    case VisitPayFeatureEnum.FeatureShowBadDebtNotificationIsEnabled:
                        return client.FeatureShowBadDebtNotificationIsEnabled;

                    case VisitPayFeatureEnum.FeatureRestrictIpAddressByCountry:
                        return client.FeatureRestrictIpAddressByCountry ?? false;

                    case VisitPayFeatureEnum.FeatureVisitPayAgingIsEnabled:
                        return client.FeatureVisitPayAgingIsEnabled;

                    case VisitPayFeatureEnum.FeatureResetAgingTierForReactivatedMaxAgeVisits:
                        return client.FeatureVisitPayAgingIsEnabled && client.FeatureResetAgingTierForReactivatedMaxAgeVisits;

                    case VisitPayFeatureEnum.FeatureHideRedactedVisitsIsEnabled:
                        return client.FeatureHideRedactedVisitsIsEnabled;

                    case VisitPayFeatureEnum.FeaturePaymentApiIsEnabled:
                        return client.FeaturePaymentApiIsEnabled;

                    case VisitPayFeatureEnum.FeatureExternalFinancePlanPaymentEnabled:
                        return client.FeatureExternalFinancePlanPaymentEnabled;

                    case VisitPayFeatureEnum.FinancePlanVisitSelectionIsEnabled:
                        return client.FinancePlanVisitSelectionIsEnabled;

                    case VisitPayFeatureEnum.FeatureRedirectInternationalUserRegistration:
                        return client.FeatureRedirectInternationalUserRegistration
                               && client.GuestPayAllowInternationalPayments
                               && client.AppGuarantorExpressUrlHost.TrimNullSafe()?.Length > 0;

                    case VisitPayFeatureEnum.FeatureVisitUnmatchingMethodSelection:
                        return client.FeatureVisitUnmatchingMethodSelection;

                    case VisitPayFeatureEnum.FeaturePageHelpIsEnabled:
                        return client.FeaturePageHelpIsEnabled;

                    case VisitPayFeatureEnum.FeatureTextToPayIsEnabled:
                        return this._clientService.Value.GetClient().FeatureTextToPayIsEnabled
                               && this._applicationSettingService.Value.FeatureSmsEnabled.Value;

                    case VisitPayFeatureEnum.FeatureVisitPayDocumentIsEnabled:
                        return client.FeatureVisitPayDocumentIsEnabled;

                    case VisitPayFeatureEnum.FeatureMatchingApiIsEnabled:
                        return client.FeatureMatchingApiIsEnabled;

                    case VisitPayFeatureEnum.FeatureAnalyticReportsIsEnabled:
                        return client.FeatureAnalyticReportsIsEnabled;

                    case VisitPayFeatureEnum.FeatureCardReaderIsEnabled:
                        return client.FeatureCardReaderIsEnabled;

                    case VisitPayFeatureEnum.FeatureIsLockBoxEnabled:
                        return this._applicationSettingService.Value.IsLockBoxEnabled.Value;

                    case VisitPayFeatureEnum.FeatureChatIsEnabled:
                        return client.FeatureChatIsEnabled;

                    case VisitPayFeatureEnum.FeatureMailIsEnabled:
                        return client.FeatureMailIsEnabled;

					case VisitPayFeatureEnum.FeatureFacilityListIsEnabled:
						return client.FeatureFacilityListIsEnabled;

                    case VisitPayFeatureEnum.FeatureFacilityContactUsGridIsEnabled:
                        return client.FeatureFacilityContactUsGridIsEnabled;

                    case VisitPayFeatureEnum.FeatureOfflineComboOnly:
                        return client.FeatureOfflineComboOnlyIsEnabled;

                    case VisitPayFeatureEnum.FeatureOptimisticAchSettlementIsEnabled:
                        return client.FeatureOptimisticAchSettlementIsEnabled;

                    case VisitPayFeatureEnum.FeatureSimpleTermsIsEnabled:
                        return client.FeatureSimpleTermsIsEnabled;

                    case VisitPayFeatureEnum.FeatureRefundReturnedChecks:
                        return client.FeatureRefundReturnedChecks;

                    case VisitPayFeatureEnum.FeatureSuppressDuplicatePrincipalPaymentsInUi:
                        return client.FeatureSuppressDuplicatePrincipalPaymentsInUi;

                    case VisitPayFeatureEnum.FinancePlanRefundInterestIncentive:
                        return client.FinancePlanRefundInterestIncentiveEnabled && client.FinancePlanIncentiveConfiguration != null;

                    #region demo features

                    case VisitPayFeatureEnum.DemoClientReportDashboardIsEnabled:
                        return client.FeatureDemoClientReportDashboardIsEnabled;
                        
                    case VisitPayFeatureEnum.DemoHealthEquityIsEnabled:
                        return client.FeatureDemoHealthEquityIsEnabled;

                    case VisitPayFeatureEnum.DemoInsuranceAccrualUiIsEnabled:
                        return client.FeatureDemoInsuranceAccrualUiIsEnabled;

                    case VisitPayFeatureEnum.DemoMyChartSSOUiIsEnabled:
                        return client.FeatureDemoMyChartSsoUiIsEnabled;

                    case VisitPayFeatureEnum.DemoPreServiceUiIsEnabled:
                        return client.FeatureDemoPreServiceUiIsEnabled;

                    #endregion

                    #region usability

                    case VisitPayFeatureEnum.FeatureDebrandedUiIsEnabled:
                        return client.FeatureDebrandedUiIsEnabled;

                    #endregion

                    default:
                        this._logger.Value.Warn(() =>  $"FeatureService.IsFeatureEnabled no case for {visitPayFeature}");
                        break;
                }
            }
            catch (Exception ex)
            {
                this._logger.Value.Fatal(() => $"FeatureService.IsFeatureEnabled failed with message {ExceptionHelper.AggregateExceptionToString(ex)}");
            }

            return default(bool);
        }

        private bool IsFeatureCategoryValid(VisitPayFeatureEnum visitPayFeatureEnum)
        {
            // demo features - show in Dev, Demo, Test
            if (EnumHelper<VisitPayFeatureEnum>.Contains(visitPayFeatureEnum, VisitPayFeatureEnumCategory.DemoFeature))
            {
                string environment = this._applicationSettingService.Value.Environment.Value;
                if (string.IsNullOrEmpty(environment))
                {
                    return false;
                }

                return environment.StartsWith("dev", StringComparison.InvariantCultureIgnoreCase) ||
                       environment.StartsWith("demo", StringComparison.InvariantCultureIgnoreCase) ||
                       environment.StartsWith("test", StringComparison.InvariantCultureIgnoreCase);
            }

            // usability features - show in Dev, Usability, Test
            if (EnumHelper<VisitPayFeatureEnum>.Contains(visitPayFeatureEnum, VisitPayFeatureEnumCategory.UsabilityFeature))
            {
                string environment = this._applicationSettingService.Value.Environment.Value;
                if (string.IsNullOrEmpty(environment))
                {
                    return false;
                }

                return environment.StartsWith("dev", StringComparison.InvariantCultureIgnoreCase) ||
                       environment.StartsWith("usabil", StringComparison.InvariantCultureIgnoreCase) ||
                       environment.StartsWith("test", StringComparison.InvariantCultureIgnoreCase);
            }

            // other features - no conditions
            return true;
        }
    }
}

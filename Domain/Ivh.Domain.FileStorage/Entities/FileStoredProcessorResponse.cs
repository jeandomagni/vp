﻿namespace Ivh.Domain.FileStorage.Entities
{
    using System;
    using Common.Base.Enums;

    public class FileStoredProcessorResponse
    {
        public virtual int FileStorageProcessorResponseId { get; set; }
    }
}
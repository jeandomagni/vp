﻿namespace Ivh.Domain.FileStorage.Entities
{
    using System;
    using Common.VisitPay.Attributes;

    public class FileStored
    {
        public FileStored()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        public virtual int Id { get; set; }
        public virtual Guid ExternalKey { get; set; }
        [RedactField(255)]
        public virtual string Filename { get; set; }
        [RedactField(255)]
        public virtual string FileMimeType { get; set; }
        public virtual DateTime FileDateTime { get; set; }
        public virtual DateTime? ExternalReferenceDate { get; set; }
        public virtual byte[] FileContents { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual DateTime? RemoveDate { get; set; }
    }
}
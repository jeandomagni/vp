﻿namespace Ivh.Domain.FileStorage.Services
{
    using System;
    using System.Threading.Tasks;
    using Common.Data;
    using Entities;
    using Interfaces;
    using NHibernate;
    using System.Collections.Generic;
    using System.Threading;
    using Base.Interfaces;
    using Base.Services;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;

    public class FileStorageService : DomainService, IFileStorageService
    {
        private readonly IFileStorageRepository _fileStorageRepository;
        private readonly ISession _session;

        public FileStorageService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            IFileStorageRepository fileStorageRepository,
            ISessionContext<Storage> sessionContext) : base(serviceCommonService)
        {
            this._fileStorageRepository = fileStorageRepository;
            this._session = sessionContext.Session;
        }

        public async Task<bool> SaveFileAsync(FileStored fileStored)
        {
            if (fileStored == null) throw new ArgumentNullException(nameof(fileStored));

            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                try
                {
                    // TODO: the interface for this I/O intensive operation 
                    // is Synchronous and should likely be refactored. In the 
                    // interum we should be able to cheat by wrapping it in a Task.Run()
                    await Task.Run(() => this._fileStorageRepository.InsertOrUpdate(fileStored)).ConfigureAwait(false);
                    unitOfWork.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    unitOfWork.Rollback();
                    throw; // no swallowing - let upstream error handler catch
                }
                finally
                {
                }
            }
        }

        public async Task<FileStored> GetFileAsync(Guid key)
        {
            if (key == null) throw new ArgumentNullException("key");

            try
            {
                var fileStored = await Task.Run(() =>
                    {
                        return this._fileStorageRepository.GetByKey(key);
                    }
                ).ConfigureAwait(true);

                return fileStored;

            }
            catch (Exception)
            {
                throw; // no swallowing - let upstream error handler catch
            }
            finally
            {
            }
        }

        public async Task<IList<FileStored>> GetUnreferencedAsync()
        {
            try
            {
                var list = await Task.Run(() =>
                {
                    return this._fileStorageRepository.GetUnreferenced();
                }
                ).ConfigureAwait(true);

                return list;

            }
            catch (Exception)
            {
                throw; // no swallowing - let upstream error handler catch
            }
            finally
            {
            }
        }

        /// <summary>
        /// Soft delete of row, hard delete of the FileContents column
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task RemoveUnreferencedAsync(int id)
        {
            try
            {
                await Task.Run(() =>
                {
                    FileStored entity = this._fileStorageRepository.GetById(id);
                    entity.RemoveDate = DateTime.UtcNow;
                    entity.FileContents = new byte[0];
                    this._fileStorageRepository.InsertOrUpdate(entity);
                }
                ).ConfigureAwait(true);
            }
            catch (Exception)
            {
                throw; // no swallowing - let upstream error handler catch
            }
            finally
            {
            }
        }
    }
}
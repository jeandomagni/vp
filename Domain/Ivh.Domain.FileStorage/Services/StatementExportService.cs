﻿namespace Ivh.Domain.FileStorage.Services
{
    using Base.Interfaces;
    using Base.Services;
    using Entities;
    using Interfaces;
    using System;
    using System.Text;
    using Common.Base.Utilities.Helpers;

    public class StatementExportService : DomainService, IStatementExportService
    {
        private readonly Lazy<IFileStorageService> _fileStorageService;
        private readonly Lazy<TimeZoneHelper> _timeZoneHelper;

        public StatementExportService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IFileStorageService> fileStorageService,
            Lazy<TimeZoneHelper> timeZoneHelper) : base(serviceCommonService)
        {
            this._fileStorageService = fileStorageService;
            this._timeZoneHelper = timeZoneHelper;
        }

        public FileStored ExportOfflineStatements(string jsonContents)
        {
            this.LoggingService.Value.Info(() => $"{nameof(this.ExportOfflineStatements)} length: {jsonContents.Length}");
            DateTime dateNow = DateTime.UtcNow;
            DateTime clientDateNow = this._timeZoneHelper.Value.UtcToClient(dateNow);
            Guid externalKey = Guid.NewGuid();
            string fileName = $"VPSTMT_PRD_{clientDateNow:yyyyMMdd_HHmmss}.txt";
            string fileMimeType = "text/plain";
            byte[] fileContents = Encoding.UTF8.GetBytes(jsonContents);

            FileStored fileStored = new FileStored()
            {
                ExternalKey = externalKey,
                FileContents = fileContents,
                FileDateTime = dateNow,
                FileMimeType = fileMimeType,
                Filename = fileName,
                InsertDate = dateNow
            };

            bool success = this._fileStorageService.Value.SaveFileAsync(fileStored).Result;
            this.LoggingService.Value.Info(() => $"{nameof(this.ExportOfflineStatements)} - File storage success: {success} | FileName: {fileName} | ExternalKey: {externalKey}");
            if (success)
            {
                return fileStored;
            }

            this.LoggingService.Value.Warn(() => $"{nameof(this.ExportOfflineStatements)} - Failed to store file! >> FileName: {fileName} | ExternalKey: {externalKey}");
            return null;
        }
    }
}
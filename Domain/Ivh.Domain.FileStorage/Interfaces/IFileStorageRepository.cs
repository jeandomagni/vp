﻿namespace Ivh.Domain.FileStorage.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;
    using System;
using System.Collections.Generic;

    public interface IFileStorageRepository : IRepository<FileStored>
    {
        FileStored GetByKey(Guid externalKey);
        IList<FileStored> GetUnreferenced();
        void RemoveUnreferenced(FileStored fs);
    }
}
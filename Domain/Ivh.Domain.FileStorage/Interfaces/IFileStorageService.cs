﻿namespace Ivh.Domain.FileStorage.Interfaces
{
    using System.Threading.Tasks;
    using Entities;
    using System;
    using System.Collections.Generic;
    using Common.Base.Interfaces;

    public interface IFileStorageService : IDomainService
    {
        Task<bool> SaveFileAsync(FileStored fileStorage);
        Task<FileStored> GetFileAsync(Guid key);
        Task<IList<FileStored>> GetUnreferencedAsync();
        Task RemoveUnreferencedAsync(int id);
    }
}
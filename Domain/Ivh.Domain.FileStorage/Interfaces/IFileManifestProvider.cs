﻿
namespace Ivh.Domain.FileStorage.Interfaces
{
    using Entities;
    using System.Collections.Generic;

    public interface IFileManifestProvider
    {
        string CreateFileManifestStringForFiles(IList<FileStored> files);
        byte[] CreateFileManifestBytesForFiles(IList<FileStored> files);
        void WriteFileManifestForFiles(IList<FileStored> files, string fileName, string directory);
    }
}

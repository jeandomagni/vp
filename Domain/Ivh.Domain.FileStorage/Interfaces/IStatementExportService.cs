﻿namespace Ivh.Domain.FileStorage.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IStatementExportService : IDomainService
    {
        FileStored ExportOfflineStatements(string jsonContents);
    }
}
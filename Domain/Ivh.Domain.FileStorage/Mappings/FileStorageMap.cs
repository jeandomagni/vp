﻿namespace Ivh.Domain.FileStorage.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class FileStorageMap : ClassMap<FileStored>
    {
        public FileStorageMap()
        {
            this.Schema("dbo");
            this.Table("FileStored");
            this.Id(x => x.Id);               
            this.Map(x => x.ExternalKey).Not.Nullable();
            this.Map(x => x.FileContents)
                .Length(4194304); // 4 meg
            this.Map(x => x.FileDateTime).Not.Nullable();
            this.Map(x => x.FileMimeType).Not.Nullable();
            this.Map(x => x.Filename).Not.Nullable();
            this.Map(x => x.ExternalReferenceDate).Nullable();
            this.Map(x => x.InsertDate);
            this.Map(x => x.RemoveDate).Nullable();
        }
    }
}
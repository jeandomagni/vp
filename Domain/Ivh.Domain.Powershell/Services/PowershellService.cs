﻿namespace Ivh.Domain.Powershell.Services
{
    using System;
    using Base.Interfaces;
    using Interfaces;
    using Ivh.Domain.Base.Services;

    public class PowershellService : DomainService, IPowershellService
    {
        private readonly IPowershellProvider _powershellProvider;

        public PowershellService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            IPowershellProvider powershellProvider) : base(serviceCommonService)
        {
            this._powershellProvider = powershellProvider;
        }

        public string ExportCdiDataToFile()
        {
            return this._powershellProvider.ExportCdiDataToFile();
        }

        public string HsGuarantorMatchDataMove()
        {
            return this._powershellProvider.HsGuarantorMatchDataMove();
        }

        public string InsertFileNametoFileTracker()
        {
            return this._powershellProvider.InsertFileNametoFileTracker();
        }

        public string MoveVpDataToCdi()
        {
            return this._powershellProvider.MoveVpDataToCdi();
        }

        public string PublishCdiEtlToVpEtl()
        {
            return this._powershellProvider.PublishCdiEtlToVpEtl();
        }

        public string ReRunnableOutboundSQL()
        {
            return this._powershellProvider.ReRunnableOutboundSQL();
        }

        public string Inbound_LoadFiles()
        {
            return this._powershellProvider.Inbound_LoadFiles();
        }

        public string Inbound_LoadHistory()
        {
            return this._powershellProvider.Inbound_LoadHistory();
        }

        public string Inbound_LoadSnapshotAndDelta()
        {
            return this._powershellProvider.Inbound_LoadSnapshotAndDelta();
        }

        public string Inbound_LoadBaseStage()
        {
            return this._powershellProvider.Inbound_LoadBaseStage();
        }

        public string Inbound_LoadChangeEvents_Part1()
        {
            return this._powershellProvider.Inbound_LoadChangeEvents_Part1();
        }

        public string Inbound_LoadBase()
        {
            return this._powershellProvider.Inbound_LoadBase();
        }

        public string Inbound_LoadChangeEvents_Part2()
        {
            return this._powershellProvider.Inbound_LoadChangeEvents_Part2();
        }

        public string Inbound_GuestPay()
        {
            return this._powershellProvider.Inbound_GuestPay();
        }

        public string CloneCdiAppGuarantor(int hsGuarantorId, int vpGuarantorId, string appendValue, string userName, bool consolidate)
        {
            return this._powershellProvider.CloneCdiAppGuarantor(hsGuarantorId, vpGuarantorId, appendValue, userName, consolidate);
        }
    }
}

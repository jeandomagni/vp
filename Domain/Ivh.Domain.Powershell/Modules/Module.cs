﻿namespace Ivh.Domain.Powershell.Modules
{
    using Autofac;
    using Interfaces;
    using Services;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<PowershellService>().As<IPowershellService>();
        }
    }
}

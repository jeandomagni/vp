﻿namespace Ivh.Domain.SecureCommunication.GuarantorSupportRequests.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface ISupportTemplateService : IDomainService
    {
        SupportTemplate GetSupportTemplate(int supportTemplateId);
        IList<SupportTemplate> GetSupportTemplates(int vpUserId);
        SupportTemplate SaveSupportTemplate(SupportTemplate supportTemplate);
        void DeleteSupportTemplate(SupportTemplate supportTemplate);
    }
}

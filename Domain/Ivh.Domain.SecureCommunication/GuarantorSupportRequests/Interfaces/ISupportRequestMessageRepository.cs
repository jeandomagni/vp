﻿namespace Ivh.Domain.SecureCommunication.GuarantorSupportRequests.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface ISupportRequestMessageRepository : IRepository<SupportRequestMessage>
    {
        SupportRequestMessage GetSupportRequestMessage(int supportRequestId, int supportRequestMessageId);
        IList<SupportRequestMessage> GetUnreadMessages(int vpGuarantorId);
        IList<SupportRequestMessageCountResult> GetUnreadMessagesToGuarantorCount(int vpGuarantorId);
        int GetAllUnreadMessagesFromGuarantorsCount();
        SupportRequestMessage GetSupportRequestDraftMessage(int supportRequestId, int createdByVpUserId);
    }
}

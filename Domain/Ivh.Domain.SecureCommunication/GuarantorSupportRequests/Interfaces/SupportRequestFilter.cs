﻿namespace Ivh.Domain.SecureCommunication.GuarantorSupportRequests.Interfaces
{
    using System;
    using Common.VisitPay.Enums;

    public class SupportRequestFilter
    {
        public SupportRequestStatusEnum? SupportRequestStatus { get; set; }
        public DateTime? DateRangeFrom { get; set; }
        public string SupportRequestDisplayId { get; set; }
        public int? AssignedToVisitPayUserId { get; set; }
        public bool? ReadMessages { get; set; }
        public bool? UnreadMessages { get; set; }
        public string SortField { get; set; }
        public string SortOrder { get; set; }
        public string FacilitySourceSystemKey { get; set; }
    }
}

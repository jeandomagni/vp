﻿namespace Ivh.Domain.SecureCommunication.GuarantorSupportRequests.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface ISupportTemplateRepository : IRepository<SupportTemplate>
    {
        IList<SupportTemplate> GetSupportTemplatesForUser(int vpUserId);
    }
}

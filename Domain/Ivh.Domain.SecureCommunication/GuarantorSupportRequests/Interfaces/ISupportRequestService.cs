﻿namespace Ivh.Domain.SecureCommunication.GuarantorSupportRequests.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Ivh.Domain.Guarantor.Entities;
    using Entities;
    using User.Entities;

    public interface ISupportRequestService : IDomainService
    {
        SupportRequest GetSupportRequest(int supportRequestId);
        SupportRequest GetSupportRequest(int supportRequestId, Guarantor guarantor);
        SupportRequestResults GetSupportRequests(Guarantor guarantor, SupportRequestFilter filter, int batch, int batchSize);
        int GetSupportRequestTotals(Guarantor guarantor, SupportRequestFilter filter);
        SupportRequest SaveSupportRequest(SupportRequest supportRequest);
        SupportRequestMessage GetSupportRequestMessage(SupportRequest supportRequest, int supportRequestMessageId);
        SupportRequestMessage SaveSupportRequestMessage(SupportRequestMessage supportRequestMessage);
        SupportRequestMessage SaveInternalNote(int supportRequestId, Guarantor guarantor, string messageBody, int createdByVpUserId);
        SupportRequestMessage SaveClientMessage(int supportRequestId, Guarantor guarantor, string messageBody, int createdByVpUserId);
        SupportRequestMessage SaveGuarantorMessage(int supportRequestId, Guarantor guarantor, string messageBody, int createdByVpUserId);
        void SaveDetails(int supportRequestId, Guarantor guarantor, int? assignedToVisitPayUserId, int actionVisitPayUserId);
        void DeleteAttachment(int supportRequestId, int supportRequestMessageAttachmentId);
        void SetSupportTopic(Guarantor guarantor, int supportRequestId, int? supportTopicId);
        SupportRequest SetSupportRequestClosedOrOpen(SupportRequestStatusEnum supportRequestStatus, int supportRequestId, Guarantor guarantor, string messageBody, int createdByVpUserId);
        void SetRead(int supportRequestId, Guarantor guarantor, SupportRequestMessageTypeEnum supportRequestType);
        int GetUnreadMessagesToGuarantorCount(Guarantor guarantor);
        int GetSupportRequestsWithUnreadMessagesToGuarantorCount(int vpGuarantorId);
        int GetAllUnreadMessagesFromGuarantorsCount();
        void RedactSupportRequestsForVisit(int vpGuarantorId, int visitId);
        IReadOnlyList<VisitPayUser> GetAllAssignedUsers(IList<int> excludeUserIds);
        void SaveDraft(int supportRequestId, Guarantor guarantor, string messageBody, int createdByVpUserId);
        SupportRequestMessage GetSupportDraftMessage(int supportRequestId, int createdByVpUserId);
        void FlagForFollowUp(int supportRequestId, Guarantor guarantor, bool flagForFollowUp, int createdByVpUserId);

    }
}

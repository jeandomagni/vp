﻿namespace Ivh.Domain.SecureCommunication.GuarantorSupportRequests.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface ISupportTopicRepository : IRepository<SupportTopic>
    {
    }
}

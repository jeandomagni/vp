﻿namespace Ivh.Domain.SecureCommunication.GuarantorSupportRequests.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface ISupportRequestRepository : IRepository<SupportRequest>
    {
        SupportRequest GetSupportRequest(int vpGuarantorId, int supportRequestId);
        IList<SupportRequest> GetSupportRequestForVisit(int vpGuarantorId, int visitId);
        IList<int> GetAllAssignedUsers();
        SupportRequestResults GetSupportRequests(int? vpGuarantorId, SupportRequestFilter filter, int batch, int batchSize);
        SupportRequestResults GetSupportRequestTotals(int? vpGuarantorId, SupportRequestFilter filter);
    }
}

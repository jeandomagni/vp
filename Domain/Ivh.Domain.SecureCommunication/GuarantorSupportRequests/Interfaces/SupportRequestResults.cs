﻿namespace Ivh.Domain.SecureCommunication.GuarantorSupportRequests.Interfaces
{
    using System.Collections.Generic;
    using Entities;

    public class SupportRequestResults
    {
        public IReadOnlyList<SupportRequest> SupportRequests { get; set; }
        public int TotalRecords { get; set; }
    }
}

﻿namespace Ivh.Domain.SecureCommunication.GuarantorSupportRequests.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface ISupportTopicService : IDomainService
    {
        SupportTopic GetSupportTopic(int supportTopicId);
        IList<SupportTopic> GetSupportTopics();
    }
}

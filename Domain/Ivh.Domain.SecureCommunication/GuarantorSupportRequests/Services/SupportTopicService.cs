﻿namespace Ivh.Domain.SecureCommunication.GuarantorSupportRequests.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Entities;
    using Interfaces;

    public class SupportTopicService : DomainService, ISupportTopicService
    {
        private readonly ISupportTopicRepository _supportTopicRepository;

        public SupportTopicService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            ISupportTopicRepository supportTopicRepository) : base(serviceCommonService)
        {
            this._supportTopicRepository = supportTopicRepository;
        }

        public SupportTopic GetSupportTopic(int supportTopicId)
        {
            return this._supportTopicRepository.GetById(supportTopicId);
        }

        public IList<SupportTopic> GetSupportTopics()
        {
            return this._supportTopicRepository.GetQueryable().Where(x => x.IsActive).ToList();
        }
    }
}

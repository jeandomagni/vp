﻿namespace Ivh.Domain.SecureCommunication.GuarantorSupportRequests.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Entities;
    using Interfaces;

    public class SupportTemplateService : DomainService, ISupportTemplateService
    {
        private readonly Lazy<ISupportTemplateRepository> _supportTemplateRepository;

        public SupportTemplateService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<ISupportTemplateRepository> supportTemplateRepository) : base(serviceCommonService)
        {
            this._supportTemplateRepository = supportTemplateRepository;
        }

        public SupportTemplate GetSupportTemplate(int supportTemplateId)
        {
            return this._supportTemplateRepository.Value.GetById(supportTemplateId);
        }

        public void DeleteSupportTemplate(SupportTemplate supportTemplate)
        {
            this._supportTemplateRepository.Value.Delete(supportTemplate);
        }

        public IList<SupportTemplate> GetSupportTemplates(int vpUserId)
        {
            return this._supportTemplateRepository.Value
                       .GetSupportTemplatesForUser(vpUserId)
                       .OrderBy(x => x.Title)
                       .ToList();
        }

        public SupportTemplate SaveSupportTemplate(SupportTemplate supportTemplate)
        {
            this._supportTemplateRepository.Value.InsertOrUpdate(supportTemplate);
            return supportTemplate;
        }
    }
}

﻿namespace Ivh.Domain.SecureCommunication.GuarantorSupportRequests.Services
{
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;
    using Ivh.Domain.Guarantor.Entities;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using User.Entities;
    using User.Interfaces;
    using Visit.Entities;
    using Visit.Interfaces;

    public class SupportRequestService : DomainService, ISupportRequestService
    {
        private readonly Lazy<ISupportRequestRepository> _supportRequestRepository;
        private readonly Lazy<ISupportRequestMessageRepository> _supportRequestMessageRepository;
        private readonly Lazy<IVisitPayUserRepository> _visitPayUserRepository;
        private readonly Lazy<IVisitRepository> _visitRepository;

        public SupportRequestService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<ISupportRequestRepository> supportRequestRepository,
            Lazy<ISupportRequestMessageRepository> supportRequestMessageRepository,
            Lazy<IVisitPayUserRepository> visitPayUserRepository,
            Lazy<IVisitRepository> visitRepository)
                : base(serviceCommonService)
        {
            this._supportRequestRepository = supportRequestRepository;
            this._supportRequestMessageRepository = supportRequestMessageRepository;
            this._visitPayUserRepository = visitPayUserRepository;
            this._visitRepository = visitRepository;
        }

        public SupportRequest GetSupportRequest(int supportRequestId)
        {
            return this._supportRequestRepository.Value.GetById(supportRequestId);
        }

        public SupportRequest GetSupportRequest(int supportRequestId, Guarantor guarantor)
        {
            return this._supportRequestRepository.Value.GetSupportRequest(guarantor.VpGuarantorId, supportRequestId);
        }

        public SupportRequestResults GetSupportRequests(Guarantor guarantor, SupportRequestFilter filter, int batch, int batchSize)
        {
            // Allow the following entry patterns for SupportRequestDisplayId:
            // Full Case ID (SLPB-179)
            // Full Case ID – without the dash (SLPB179)
            // Case Number only (179)
            if (!string.IsNullOrEmpty(filter.SupportRequestDisplayId))
            {
                string prefix = string.Concat(this.Client.Value.SupportRequestCasePrefix, "-");
                string alphaCharacters = Regex.Replace(filter.SupportRequestDisplayId, "[0-9]", "");
                if (alphaCharacters.Length > 0 && !alphaCharacters.EndsWith("-"))
                {
                    alphaCharacters = string.Concat(alphaCharacters, "-");
                }

                if (alphaCharacters.Length > 0 && !string.Equals(alphaCharacters, prefix, StringComparison.CurrentCultureIgnoreCase))
                {
                    return new SupportRequestResults
                    {
                        SupportRequests = new List<SupportRequest>(),
                        TotalRecords = 0
                    };
                }
            }

            
            SupportRequestResults supportRequestResults;

            bool pageInRepository = string.IsNullOrWhiteSpace(filter.FacilitySourceSystemKey);
            if (pageInRepository)
            {
                supportRequestResults = this._supportRequestRepository.Value.GetSupportRequests(guarantor?.VpGuarantorId, filter, batch, batchSize);
            }
            else
            {
                supportRequestResults = this._supportRequestRepository.Value.GetSupportRequests(guarantor?.VpGuarantorId, filter, 0, 1000);
            }

            SupportRequestResults supportRequestResultsWithFacilityData = this.GetSupportRequestResultsWithFacilityData(supportRequestResults);

            if (string.IsNullOrEmpty(filter.FacilitySourceSystemKey))
            {
                return supportRequestResultsWithFacilityData;
            }

            return GetFacilityFilteredResults(filter, supportRequestResultsWithFacilityData, batch, batchSize);
        }

        public int GetSupportRequestTotals(Guarantor guarantor, SupportRequestFilter filter)
        {
            return this._supportRequestRepository.Value.GetSupportRequestTotals(guarantor.VpGuarantorId, filter).TotalRecords;
        }

        public SupportRequest SaveSupportRequest(SupportRequest supportRequest)
        {
            if (string.IsNullOrEmpty(supportRequest.SupportRequestDisplayId))
                supportRequest.SupportRequestDisplayId = "";

            this._supportRequestRepository.Value.InsertOrUpdate(supportRequest);

            if (!string.IsNullOrEmpty(supportRequest.SupportRequestDisplayId))
                return supportRequest;

            supportRequest.SupportRequestDisplayId = $"{this.Client.Value.SupportRequestCasePrefix}-{supportRequest.SupportRequestId}";
            this._supportRequestRepository.Value.InsertOrUpdate(supportRequest);

            return supportRequest;
        }

        public SupportRequestMessage GetSupportRequestMessage(SupportRequest supportRequest, int supportRequestMessageId)
        {
            return this._supportRequestMessageRepository.Value.GetSupportRequestMessage(supportRequest.SupportRequestId, supportRequestMessageId);
        }

        public SupportRequestMessage SaveSupportRequestMessage(SupportRequestMessage supportRequestMessage)
        {
            this._supportRequestMessageRepository.Value.InsertOrUpdate(supportRequestMessage);

            return supportRequestMessage;
        }

        public SupportRequestMessage SaveInternalNote(int supportRequestId, Guarantor guarantor, string messageBody, int createdByVpUserId)
        {
            SupportRequest supportRequest = this.GetSupportRequest(supportRequestId, guarantor);

            SupportRequestMessage supportRequestMessage = new SupportRequestMessage
            {
                CreatedByVpUserId = createdByVpUserId,
                InsertDate = DateTime.UtcNow,
                IsRead = false,
                MessageBody = messageBody,
                SupportRequest = supportRequest,
                SupportRequestMessageType = SupportRequestMessageTypeEnum.InternalNote,
            };

            supportRequest.SupportRequestMessages.Add(supportRequestMessage);

            this.SaveSupportRequest(supportRequest);

            return supportRequestMessage;
        }

        public SupportRequestMessage SaveClientMessage(int supportRequestId, Guarantor guarantor, string messageBody, int createdByVpUserId)
        {
            SupportRequest supportRequest = this.GetSupportRequest(supportRequestId, guarantor);

            SupportRequestMessage supportRequestMessage = new SupportRequestMessage
            {
                CreatedByVpUserId = createdByVpUserId,
                InsertDate = DateTime.UtcNow,
                IsRead = false,
                MessageBody = messageBody,
                SupportRequest = supportRequest,
                SupportRequestMessageType = SupportRequestMessageTypeEnum.ToGuarantor
            };

            supportRequest.SupportRequestMessages.Add(supportRequestMessage);

            this.SaveSupportRequest(supportRequest);

            this.ClearSupportRequestDraftMessage(supportRequestId, guarantor, createdByVpUserId);

            return supportRequestMessage;
        }

        public SupportRequestMessage SaveGuarantorMessage(int supportRequestId, Guarantor guarantor, string messageBody, int createdByVpUserId)
        {
            SupportRequest supportRequest = this.GetSupportRequest(supportRequestId, guarantor);

            SupportRequestMessage supportRequestMessage = new SupportRequestMessage
            {
                CreatedByVpUserId = createdByVpUserId,
                InsertDate = DateTime.UtcNow,
                IsRead = false,
                MessageBody = messageBody,
                SupportRequest = supportRequest,
                SupportRequestMessageType = SupportRequestMessageTypeEnum.FromGuarantor,
            };

            supportRequest.SupportRequestMessages.Add(supportRequestMessage);

            this.SaveSupportRequest(supportRequest);

            return supportRequestMessage;
        }

        public void SaveDetails(int supportRequestId, Guarantor guarantor, int? assignedToVisitPayUserId, int actionVisitPayUserId)
        {
            SupportRequest supportRequest = this.GetSupportRequest(supportRequestId, guarantor);

            bool hasAssignedChanged = (supportRequest.AssignedVisitPayUser == null && assignedToVisitPayUserId != null) || (supportRequest.AssignedVisitPayUser != null && supportRequest.AssignedVisitPayUser.VisitPayUserId != assignedToVisitPayUserId);

            supportRequest.AssignedVisitPayUser = assignedToVisitPayUserId == null ? null : new VisitPayUser { VisitPayUserId = assignedToVisitPayUserId.Value };

            //
            IList<string> changesToLog = new List<string>();
            if (hasAssignedChanged)
            {
                VisitPayUser assignedVisitPayUser = this._visitPayUserRepository.Value.FindById(assignedToVisitPayUserId.GetValueOrDefault(0).ToString());
                changesToLog.Add(string.Format("Assigned to: {0}", assignedToVisitPayUserId.HasValue ? assignedVisitPayUser.FirstNameLastName : "Unassigned"));
            }

            if (changesToLog.Any())
            {
                supportRequest.SupportRequestMessages.Add(new SupportRequestMessage
                {
                    CreatedByVpUserId = actionVisitPayUserId,
                    InsertDate = DateTime.UtcNow,
                    IsRead = false,
                    MessageBody = string.Format("Action: {0}{1}{2}", "Support Request Updated", Environment.NewLine, string.Join(", ", changesToLog)),
                    SupportRequest = supportRequest,
                    SupportRequestMessageType = SupportRequestMessageTypeEnum.InternalNote
                });
            }

            //
            this.SaveSupportRequest(supportRequest);
        }

        public void DeleteAttachment(int supportRequestId, int supportRequestMessageAttachmentId)
        {
            SupportRequest supportRequest = this.GetSupportRequest(supportRequestId);

            SupportRequestMessageAttachment attachment = supportRequest.SupportRequestMessages
                .SelectMany(x => x.SupportRequestMessageAttachments)
                .FirstOrDefault(x => x.SupportRequestMessageAttachmentId == supportRequestMessageAttachmentId);

            if (attachment != null)
            {
                attachment.FileContent = new byte[] { };
                attachment.FileSize = 0;
                attachment.AttachmentFileName = "";
            }

            this.SaveSupportRequest(supportRequest);
        }

        public void SetSupportTopic(Guarantor guarantor, int supportRequestId, int? supportTopicId)
        {
            SupportRequest supportRequest = this.GetSupportRequest(supportRequestId, guarantor);

            if (supportTopicId.HasValue)
            {
                supportRequest.SupportTopic = new SupportTopic { SupportTopicId = supportTopicId.Value };
            }
            else
            {
                supportRequest.SupportTopic = null;
            }

            this.SaveSupportRequest(supportRequest);
        }

        public SupportRequest SetSupportRequestClosedOrOpen(SupportRequestStatusEnum supportRequestStatus, int supportRequestId, Guarantor guarantor, string messageBody, int createdByVpUserId)
        {
            bool isClosed = supportRequestStatus == SupportRequestStatusEnum.Closed;

            string messageBodyPrefix = isClosed ? "Request Marked Complete" : "Request Re-Opened";

            SupportRequest supportRequest = this.GetSupportRequest(supportRequestId, guarantor);

            supportRequest.SupportRequestMessages.Add(new SupportRequestMessage
            {
                CreatedByVpUserId = createdByVpUserId,
                InsertDate = DateTime.UtcNow,
                IsRead = false,
                MessageBody = string.Format("Action: {0}{1}{1}{2}", messageBodyPrefix, Environment.NewLine, messageBody),
                SupportRequest = supportRequest,
                SupportRequestMessageType = isClosed ? SupportRequestMessageTypeEnum.CloseMessage : SupportRequestMessageTypeEnum.OpenMessage
            });

            supportRequest.SupportRequestStatus = supportRequestStatus;
            supportRequest.ClosedDate = isClosed ? DateTime.UtcNow : (DateTime?)null;

            this.SaveSupportRequest(supportRequest);

            return supportRequest;
        }

        public void SetRead(int supportRequestId, Guarantor guarantor, SupportRequestMessageTypeEnum supportRequestType)
        {
            SupportRequest supportRequest = this.GetSupportRequest(supportRequestId, guarantor);

            supportRequest.SupportRequestMessages.Where(x => x.SupportRequestMessageType == supportRequestType && !x.IsRead).ToList().ForEach(x => x.IsRead = true);

            this.SaveSupportRequest(supportRequest);
        }

        public int GetUnreadMessagesToGuarantorCount(Guarantor guarantor)
        {
            IList<SupportRequestMessageCountResult> results = this._supportRequestMessageRepository.Value.GetUnreadMessagesToGuarantorCount(guarantor.VpGuarantorId);
            return results.Sum(x => x.Count);
        }

        public int GetSupportRequestsWithUnreadMessagesToGuarantorCount(int vpGuarantorId)
        {
            IList<SupportRequestMessageCountResult> results = this._supportRequestMessageRepository.Value.GetUnreadMessagesToGuarantorCount(vpGuarantorId);
            return results.Count;
        }

        public int GetAllUnreadMessagesFromGuarantorsCount()
        {
            return this._supportRequestMessageRepository.Value.GetAllUnreadMessagesFromGuarantorsCount();
        }

        public void RedactSupportRequestsForVisit(int vpGuarantorId, int visitId)
        {
            IList<SupportRequest> supportRequests = this._supportRequestRepository.Value.GetSupportRequestForVisit(vpGuarantorId, visitId);
            if (supportRequests == null || !supportRequests.Any())
                return;

            string replacementValue = this.Client.Value.RedactedVisitReplacementValue;

            foreach (SupportRequest supportRequest in supportRequests)
            {
                RedactFieldAttribute.RedactFields(supportRequest, replacementValue);
                this._supportRequestRepository.Value.Update(supportRequest);
            }
        }

        public IReadOnlyList<VisitPayUser> GetAllAssignedUsers(IList<int> excludeUserIds)
        {
            excludeUserIds = excludeUserIds ?? new List<int>();

            IList<int> allAssignedUserIds = this._supportRequestRepository.Value.GetAllAssignedUsers();
            List<int> searchFor = allAssignedUserIds.Where(x => !excludeUserIds.Contains(x)).ToList();
            if (searchFor.Any())
            {
                return this._visitPayUserRepository.Value.GetVisitPayUsers(new VisitPayUserFilter
                {
                    VisitPayUserIds = searchFor
                }, 3, 0, int.MaxValue).VisitPayUsers;
            }

            return new List<VisitPayUser>();
        }

        public void SaveDraft(int supportRequestId, Guarantor guarantor, string messageBody, int createdByVpUserId)
        {
            SupportRequest supportRequest = this._supportRequestRepository.Value.GetSupportRequest(guarantor.VpGuarantorId, supportRequestId);
            SupportRequestMessage supportRequestMessage = this._supportRequestMessageRepository.Value.GetSupportRequestDraftMessage(supportRequest.SupportRequestId, createdByVpUserId);
            if (supportRequestMessage != null)
            {
                supportRequestMessage.MessageBody = messageBody;
            }
            else
            {
                supportRequestMessage = new SupportRequestMessage
                {
                    CreatedByVpUserId = createdByVpUserId,
                    InsertDate = DateTime.UtcNow,
                    IsRead = false,
                    MessageBody = messageBody,
                    SupportRequest = supportRequest,
                    SupportRequestMessageType = SupportRequestMessageTypeEnum.DraftMessage
                };
            }

            supportRequest.SupportRequestMessages.Add(supportRequestMessage);

            this.SaveSupportRequest(supportRequest);
        }

        public SupportRequestMessage GetSupportDraftMessage(int supportRequestId, int createdByVpUserId)
        {
            return this._supportRequestMessageRepository.Value.GetSupportRequestDraftMessage(supportRequestId, createdByVpUserId);
        }

        public void FlagForFollowUp(int supportRequestId, Guarantor guarantor, bool flagForFollowUp, int createdByVpUserId)
        {
            SupportRequest supportRequest = this._supportRequestRepository.Value.GetSupportRequest(guarantor.VpGuarantorId, supportRequestId);
            supportRequest.IsFlaggedForFollowUp = flagForFollowUp;
            this.SaveSupportRequest(supportRequest);
        }

        public void ClearSupportRequestDraftMessage(int supportRequestId, Guarantor guarantor, int createdByVpUserId)
        {
            SupportRequest supportRequest = this._supportRequestRepository.Value.GetSupportRequest(guarantor.VpGuarantorId, supportRequestId);
            List<SupportRequestMessage> draftMessagesToRemove = supportRequest.SupportRequestMessages
                .Where(x => x.SupportRequestMessageType == SupportRequestMessageTypeEnum.DraftMessage
                        && x.CreatedByVpUserId == createdByVpUserId)
                .ToList();
            if (draftMessagesToRemove.Count > 0)
            {
                draftMessagesToRemove.ForEach(x => supportRequest.SupportRequestMessages.Remove(x));
                this._supportRequestRepository.Value.InsertOrUpdate(supportRequest);
            }
        }

        private bool AllSupportRequestsHaveFacilityData(IEnumerable<SupportRequest> supportRequests)
        {
            return !supportRequests.Any(r => r.FacilitySourceSystemKeys.IsNullOrEmpty());
        }

        private IEnumerable<SupportRequest> GenerateNewSupportRequestsCollectionWithFacilityData(IEnumerable<SupportRequest> supportRequests)
        {
            IList<SupportRequest> supportRequestsWithFacilities = new List<SupportRequest>();

            foreach (SupportRequest supportRequest in supportRequests)
            {
                if (supportRequest.FacilitySourceSystemKeys.IsNotNullOrEmpty())
                {
                    supportRequestsWithFacilities.Add(supportRequest);
                    continue;
                }

                IReadOnlyList<Visit> activeVisitsForVpGuarantor = 
                    this._visitRepository.Value.GetAllActiveVisitsForVpGuarantor(supportRequest.VpGuarantor.VpGuarantorId);

                if (!activeVisitsForVpGuarantor.Any())
                {
                    supportRequestsWithFacilities.Add(supportRequest);
                    continue;
                }

                List<string> facilitiesForAllActiveVisitsForVpGuarantor = activeVisitsForVpGuarantor.Select(v => v.Facility?.SourceSystemKey).Distinct().ToList();
                facilitiesForAllActiveVisitsForVpGuarantor.RemoveAll(string.IsNullOrWhiteSpace);
                facilitiesForAllActiveVisitsForVpGuarantor.Sort();

                if (!facilitiesForAllActiveVisitsForVpGuarantor.Any())
                {
                    supportRequestsWithFacilities.Add(supportRequest);
                    continue;
                }

                supportRequest.FacilitySourceSystemKeys = facilitiesForAllActiveVisitsForVpGuarantor;
                supportRequestsWithFacilities.Add(supportRequest);
            }

            return supportRequestsWithFacilities;
        }

        private SupportRequestResults GetSupportRequestResultsWithFacilityData(SupportRequestResults results)
        {
            IReadOnlyList<SupportRequest> supportRequestsCollection = results.SupportRequests;

            if (supportRequestsCollection.IsNullOrEmpty() || this.AllSupportRequestsHaveFacilityData(supportRequestsCollection))
            {
                return results;
            }

            // 1+ support request(s) do not have facility data, so assign the list of facilities for all guarantor's active visits
            List<SupportRequest> supportRequestsWithFacilitiesIndicated = 
                this.GenerateNewSupportRequestsCollectionWithFacilityData(supportRequestsCollection).ToList();

            SupportRequestResults resultsWithFacilityData = new SupportRequestResults
            {
                TotalRecords = results.TotalRecords,
                SupportRequests = supportRequestsWithFacilitiesIndicated
            };

            return resultsWithFacilityData;
        }

        private static SupportRequestResults GetFacilityFilteredResults(
            SupportRequestFilter filter,
            SupportRequestResults supportRequestResultsWithFacilityData,
            int batch,
            int batchSize)
        {
            List<SupportRequest> requestsWithFacilityMatch =
                supportRequestResultsWithFacilityData.SupportRequests
                    .Where(r => r.FacilitySourceSystemKeys.Contains(filter.FacilitySourceSystemKey)).ToList();

            if (filter.SortField.Equals(nameof(SupportRequest.FacilitySourceSystemKeys)))
            {
                bool sortAscending = string.Equals("asc", filter.SortOrder, StringComparison.InvariantCultureIgnoreCase);

                requestsWithFacilityMatch =
                    requestsWithFacilityMatch
                        .OrderBy(r => string.Join(",", r.FacilitySourceSystemKeys.OrderBy(k => k).ToList()), sortAscending)
                        .ToList();
            }

            SupportRequestResults facilityFilteredResults = new SupportRequestResults
            {
                TotalRecords = requestsWithFacilityMatch.Count,
                SupportRequests = requestsWithFacilityMatch.Skip(batch * batchSize).Take(batchSize).ToList()
            };

            return facilityFilteredResults;
        }
    }
}

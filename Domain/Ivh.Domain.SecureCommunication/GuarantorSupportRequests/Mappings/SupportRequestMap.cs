﻿namespace Ivh.Domain.SecureCommunication.GuarantorSupportRequests.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class SupportRequestMap : ClassMap<SupportRequest>
    {
        public SupportRequestMap()
        {
            this.Schema("dbo");
            this.Table("SupportRequest");
            this.Id(x => x.SupportRequestId);
            this.Map(x => x.SupportRequestDisplayId).Not.Nullable();
            this.Map(x => x.SupportRequestStatus).Column("SupportRequestStatusId").CustomType<SupportRequestStatusEnum>().Not.Nullable();
            this.Map(x => x.InsertDate);
            this.Map(x => x.PatientFirstName).Nullable();
            this.Map(x => x.PatientLastName).Nullable();
            this.Map(x => x.TreatmentDate).Nullable();
            this.Map(x => x.ClosedDate).Nullable();
            this.Map(x => x.IsFlaggedForFollowUp).Nullable();

            this.References(x => x.AssignedVisitPayUser).Column("AssignedVisitPayUserId").Nullable();
            this.References(x => x.SubmittedForVpGuarantor).Column("SubmittedForVpGuarantorId").Nullable();
            this.References(x => x.SupportRequestStatusSource).Column("SupportRequestStatusId").Not.Nullable().Not.Insert().Not.Update();
            this.References(x => x.SupportTopic).Column("SupportTopicId").Nullable();
            this.References(x => x.TreatmentLocation).Column("TreatmentLocationId").Nullable();
            this.References(x => x.Visit).Column("VisitId").Nullable();
            this.References(x => x.VpGuarantor).Column("VpGuarantorId").Not.Nullable();

            this.HasMany(x => x.SupportRequestMessages)
                .BatchSize(50)
                .KeyColumn("SupportRequestId")
                .Not.LazyLoad()
                .Fetch.Subselect()
                .Cascade.AllDeleteOrphan()
                .Inverse();
        }
    }
}

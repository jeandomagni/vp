﻿namespace Ivh.Domain.SecureCommunication.GuarantorSupportRequests.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class SupportRequestMessageTypeMap : ClassMap<SupportRequestMessageType>
    {
        public SupportRequestMessageTypeMap()
        {
            this.Schema(Schemas.VisitPay.Dbo);
            this.Table(nameof(SupportRequestMessageType));
            this.ReadOnly();
            this.Id(x => x.SupportRequestMessageTypeId);
            this.Map(x => x.SupportRequestMessageTypeName).Not.Nullable();
        }
    }
}
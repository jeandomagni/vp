﻿namespace Ivh.Domain.SecureCommunication.GuarantorSupportRequests.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class SupportRequestStatusMap : ClassMap<SupportRequestStatus>
    {
        public SupportRequestStatusMap()
        {
            this.Schema(Schemas.VisitPay.Dbo);
            this.Table(nameof(SupportRequestStatus));
            this.ReadOnly();
            this.Id(x => x.SupportRequestStatusId);
            this.Map(x => x.SupportRequestStatusName).Not.Nullable();
        }
    }
}
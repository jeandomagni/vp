﻿namespace Ivh.Domain.SecureCommunication.GuarantorSupportRequests.Mappings
{
    using Common.Base.Enums;
    using Common.Data.Extensions;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class SupportRequestMessageMap : ClassMap<SupportRequestMessage>
    {
        public SupportRequestMessageMap()
        {
            this.Schema("dbo");
            this.Table("SupportRequestMessage");
            this.Id(x => x.SupportRequestMessageId);
            this.References(x => x.SupportRequest).Column("SupportRequestId").Not.Nullable();
            this.Map(x => x.SupportRequestMessageType).Column("SupportRequestMessageTypeId").CustomType<SupportRequestMessageTypeEnum>().Not.Nullable();
            this.Map(x => x.MessageBody).Not.Nullable().AsNVarcharMax();
            this.Map(x => x.IsRead).Not.Nullable();
            this.Map(x => x.CreatedByVpUserId).Not.Nullable();
            this.References(x => x.VisitPayUser).Column("CreatedByVpUserId").Nullable().Not.Insert().Not.Update();
            this.Map(x => x.InsertDate);

            this.HasMany(x => x.SupportRequestMessageAttachments)
                .BatchSize(50)
                .KeyColumn("SupportRequestMessageId")
                .Not.LazyLoad()
                .Fetch.Subselect()
                .Cascade.All();
        }
    }
}

﻿namespace Ivh.Domain.SecureCommunication.GuarantorSupportRequests.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class SupportTemplateMap : ClassMap<SupportTemplate>
    {
        public SupportTemplateMap()
        {
            this.Schema("dbo");
            this.Table("SupportTemplate");
            this.Id(x => x.SupportTemplateId);
            this.Map(x => x.VisitPayUserId).Nullable();
            this.Map(x => x.Title).Not.Nullable();
            this.Map(x => x.Content).Not.Nullable();
        }
    }
}

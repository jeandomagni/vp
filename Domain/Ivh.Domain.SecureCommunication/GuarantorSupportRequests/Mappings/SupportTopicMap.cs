﻿namespace Ivh.Domain.SecureCommunication.GuarantorSupportRequests.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class SupportTopicMap : ClassMap<SupportTopic>
    {
        public SupportTopicMap()
        {
            this.Schema("dbo");
            this.Table("SupportTopic");
            this.Id(x => x.SupportTopicId);
            this.Map(x => x.SupportTopicName).Not.Nullable();
            this.Map(x => x.IsActive).Not.Nullable();
            this.Map(x => x.SortOrder).Not.Nullable();
            this.Map(x => x.SupportTopicLevel).Not.Nullable();
            this.Map(x => x.ParentSupportTopicId).Nullable();
            this.References(x => x.ParentSupportTopic).Column("ParentSupportTopicId").Nullable().Not.Update().Not.Insert().Not.LazyLoad();

            this.HasManyToMany(x => x.Templates)
                .BatchSize(50)
                .Table("SupportTopicCmsRegion")
                .ParentKeyColumn("SupportTopicId")
                .ChildKeyColumn("CmsRegionID");
        }
    }
}

﻿namespace Ivh.Domain.SecureCommunication.GuarantorSupportRequests.Mappings
{
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class SupportRequestMessageAttachmentMap : ClassMap<SupportRequestMessageAttachment>
    {
        public SupportRequestMessageAttachmentMap()
        {
            this.Schema("dbo");
            this.Table("SupportRequestMessageAttachment");
            this.Id(x => x.SupportRequestMessageAttachmentId);
            this.References(x => x.SupportRequestMessage).Column("SupportRequestMessageId").Not.Nullable();
            this.Map(x => x.AttachmentFileName).Not.Nullable();
            this.Map(x => x.MimeType).Not.Nullable();
            this.Map(x => x.FileSize).Not.Nullable();
            this.Map(x => x.FileContent).CustomSqlType("VARBINARY (MAX)").Length(2147483647).Nullable();
            this.Map(x => x.QuarantinedBytes).CustomSqlType("VARBINARY (MAX)").Length(2147483647).Nullable();
            this.Map(x => x.MalwareScanStatus, "MalwareScanStatusId").CustomType<MalwareScanStatusEnum>().Not.Nullable();
        }
    }
}

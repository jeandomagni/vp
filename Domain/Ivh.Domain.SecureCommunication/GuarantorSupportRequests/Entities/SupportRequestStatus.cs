﻿namespace Ivh.Domain.SecureCommunication.GuarantorSupportRequests.Entities
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class SupportRequestStatus
    {
        public virtual int SupportRequestStatusId { get; set; }
        public virtual string SupportRequestStatusName { get; set; }

        public virtual SupportRequestStatusEnum SupportRequestStatusEnum
        {
            get { return (SupportRequestStatusEnum)this.SupportRequestStatusId; }
        }
    }
}

﻿namespace Ivh.Domain.SecureCommunication.GuarantorSupportRequests.Entities
{
    public class SupportTemplate
    {
        public virtual int SupportTemplateId { get; set; }

        public virtual int? VisitPayUserId { get; set; }

        public virtual string Title { get; set; }

        public virtual string Content { get; set; }
    }
}

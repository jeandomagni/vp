﻿namespace Ivh.Domain.SecureCommunication.GuarantorSupportRequests.Entities
{
    using System.Collections.Generic;
    using Content.Entities;

    public class SupportTopic
    {
        private IList<CmsRegion> _templates;
        
        public virtual int SupportTopicId { get; set; }
        public virtual string SupportTopicName { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual int SortOrder { get; set; }
        public virtual int SupportTopicLevel { get; set; }
        public virtual int? ParentSupportTopicId { get; set; }
        public virtual SupportTopic ParentSupportTopic { get; set; }

        public virtual IList<CmsRegion> Templates 
        {
            get { return this._templates ?? (this.Templates = new List<CmsRegion>()); }
            set { this._templates = value; }
        }
    }
}

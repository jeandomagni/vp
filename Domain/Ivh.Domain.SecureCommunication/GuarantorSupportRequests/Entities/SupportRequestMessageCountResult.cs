﻿namespace Ivh.Domain.SecureCommunication.GuarantorSupportRequests.Entities
{
    public class SupportRequestMessageCountResult
    {
        public int SupportRequestId { get; set; }
        public int Count { get; set; }
    }
}
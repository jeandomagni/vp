﻿namespace Ivh.Domain.SecureCommunication.GuarantorSupportRequests.Entities
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class SupportRequestMessageType
    {
        public virtual int SupportRequestMessageTypeId { get; set; }
        public virtual string SupportRequestMessageTypeName { get; set; }

        public virtual SupportRequestMessageTypeEnum SupportRequestMessageTypeEnum
        {
            get { return (SupportRequestMessageTypeEnum)this.SupportRequestMessageTypeId; }
        }
    }
}

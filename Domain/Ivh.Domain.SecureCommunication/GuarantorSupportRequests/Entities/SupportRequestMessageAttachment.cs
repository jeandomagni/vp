﻿namespace Ivh.Domain.SecureCommunication.GuarantorSupportRequests.Entities
{
    using Common.VisitPay.Enums;

    public class SupportRequestMessageAttachment
    {
        public virtual int SupportRequestMessageAttachmentId { get; set; }
        public virtual SupportRequestMessage SupportRequestMessage { get; set; }
        public virtual string AttachmentFileName { get; set; }
        public virtual string MimeType { get; set; }
        public virtual int FileSize { get; set; }
        public virtual byte[] FileContent { get; set; }
        public virtual byte[] QuarantinedBytes { get; set; }
        public virtual MalwareScanStatusEnum MalwareScanStatus { get; set; }
    }
}

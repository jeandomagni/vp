﻿namespace Ivh.Domain.SecureCommunication.GuarantorSupportRequests.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using User.Entities;

    public class SupportRequestMessage
    {
        public SupportRequestMessage()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        private IList<SupportRequestMessageAttachment> _supportRequestMessageAttachments;

        public virtual int SupportRequestMessageId { get; set; }
        public virtual SupportRequest SupportRequest { get; set; }
        public virtual SupportRequestMessageTypeEnum SupportRequestMessageType { get; set; }
        public virtual string MessageBody { get; set; }
        public virtual bool IsRead { get; set; }
        public virtual int CreatedByVpUserId { get; set; }
        public virtual VisitPayUser VisitPayUser { get; set; }
        public virtual DateTime InsertDate { get; set; }

        public virtual IList<SupportRequestMessageAttachment> SupportRequestMessageAttachments 
        {
            get { return this._supportRequestMessageAttachments ?? (this.SupportRequestMessageAttachments = new List<SupportRequestMessageAttachment>()); }
            set { this._supportRequestMessageAttachments = value; }
        }

        public virtual IList<SupportRequestMessageAttachment> ActiveSupportRequestMessageAttachments => this.SupportRequestMessageAttachments.Where(x => x.FileSize > 0).ToList();
    }
}

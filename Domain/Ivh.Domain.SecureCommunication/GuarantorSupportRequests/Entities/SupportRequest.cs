﻿namespace Ivh.Domain.SecureCommunication.GuarantorSupportRequests.Entities
{
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;
    using Core.TreatmentLocation.Entities;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Domain.Guarantor.Entities;
    using Ivh.Domain.User.Entities;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Visit.Entities;

    public class SupportRequest
    {
        public SupportRequest()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        private IList<SupportRequestMessage> _supportRequestMessages;
        private IList<string> _facilitySourceSystemKeys;

        public virtual int SupportRequestId { get; set; }
        public virtual string SupportRequestDisplayId { get; set; }
        public virtual SupportTopic SupportTopic { get; set; }
        public virtual SupportRequestStatusEnum SupportRequestStatus { get; set; }
        public virtual SupportRequestStatus SupportRequestStatusSource { get; set; }
        public virtual Guarantor VpGuarantor { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual Visit Visit { get; set; }
        [RedactField]
        public virtual string PatientLastName { get; set; }
        [RedactField]
        public virtual string PatientFirstName { get; set; }
        public virtual bool IsFlaggedForFollowUp { get; set; }
        public virtual TreatmentLocation TreatmentLocation { get; set; }
        public virtual DateTime? TreatmentDate { get; set; }
        public virtual DateTime? ClosedDate { get; set; }
        public virtual Guarantor SubmittedForVpGuarantor { get; set; }
        public virtual VisitPayUser AssignedVisitPayUser { get; set; }

        public virtual IList<string> FacilitySourceSystemKeys
        {
            get
            {
                if (this.Visit?.Facility == null || this.Visit.Facility.SourceSystemKey.IsNullOrEmpty())
                {
                    return this._facilitySourceSystemKeys.IsNullOrEmpty() ? new List<string>() : this._facilitySourceSystemKeys;
                }

                return this.Visit.Facility.SourceSystemKey.ToListOfOne();
            }

            set => this._facilitySourceSystemKeys = value;
        }

        public virtual IList<SupportRequestMessage> SupportRequestMessages
        {
            get => this._supportRequestMessages ?? (this.SupportRequestMessages = new List<SupportRequestMessage>());
            set => this._supportRequestMessages = value;
        }

        public virtual string SupportTopicDisplayString => string.Join(" - ", new[] { this.SupportTopic?.ParentSupportTopic?.SupportTopicName, this.SupportTopic?.SupportTopicName }.Where(x => !string.IsNullOrEmpty(x)));

        public virtual string PatientLastNameFirstName => FormatHelper.LastNameFirstName(this.PatientLastName, this.PatientFirstName);
        public virtual string PatientFirstNameLastName => FormatHelper.FirstNameLastName(this.PatientFirstName, this.PatientLastName);
    }
}

﻿namespace Ivh.Domain.SecureCommunication.ClientSupportRequests.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IClientSupportRequestMessageRepository : IRepository<ClientSupportRequestMessage>
    {
        int GetUnreadMessagesCountFromClient(int? assignedVisitPayUserId);
        int GetUnreadMessagesCountToClient(int? visitPayUserId);
    }
}
﻿namespace Ivh.Domain.SecureCommunication.ClientSupportRequests.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IClientSupportTopicRepository : IRepository<ClientSupportTopic>
    {
    }
}
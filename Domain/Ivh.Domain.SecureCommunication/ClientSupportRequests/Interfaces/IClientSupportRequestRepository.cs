﻿namespace Ivh.Domain.SecureCommunication.ClientSupportRequests.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IClientSupportRequestRepository : IRepository<ClientSupportRequest>
    {
        ClientSupportRequestResults GetClientSupportRequests(ClientSupportRequestFilter filter);
        IReadOnlyList<int> GetAllClientUsersWithSupportRequest();
        IList<int> GetAllAssignedUsers();
        byte[] GetAttachmentContent(int clientSupportRequestMessageAttachmentId);
    }
}
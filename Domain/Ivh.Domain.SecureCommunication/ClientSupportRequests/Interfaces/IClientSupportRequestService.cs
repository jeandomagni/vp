﻿namespace Ivh.Domain.SecureCommunication.ClientSupportRequests.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;
    using GuarantorSupportRequests.Entities;
    using User.Entities;

    public interface IClientSupportRequestService : IDomainService
    {
        ClientSupportRequest GetClientSupportRequest(int clientSupportRequestId);
        ClientSupportRequestResults GetClientSupportRequests(ClientSupportRequestFilter filter);
        int GetUnreadMessagesCountFromClient(int? assignedVisitPayUserId);
        int GetUnreadMessagesCountToClient(int? visitPayUserId);
        IList<ClientSupportTopic> GetClientSupportTopics();
        byte[] GetAttachmentContent(int clientSupportRequestMessageAttachmentId);
        void CreateClientSupportRequest(ClientSupportRequest clientSupportRequest, string messageBody, SupportRequest referenceGuarantorSupportRequest);
        void SaveAttachment(int clientSupportRequestId, int clientSupportRequestMessageId, ClientSupportRequestMessageAttachment attachment);
        void DeleteAttachment(int clientSupportRequestId, int clientSupportRequestMessageAttachmentId);
        void SaveDetails(int clientSupportRequestId, int? assignedToVisitPayUserId, int? targetVpGuarantorId, string jiraId, int visitPayUserId);
        void SaveMessage(int clientSupportRequestId, ClientSupportRequestMessage clientSupportRequestMessage);
        void SetMessageRead(int clientSupportRequestId, ClientSupportRequestMessageTypeEnum clientSupportRequestMessageType);
        ClientSupportRequest SetSupportRequestStatus(int supportRequestId, int visitPayUserId, ClientSupportRequestStatusEnum clientSupportRequestStatus, string message);
        IDictionary<int, string> GetAllClientUsersWithSupportRequest();
        IReadOnlyList<VisitPayUser> GetAllAssignedUsers();
        void CreateClientSupportRequestOnBehalfOfClient(ClientSupportRequest clientSupportRequest, string messageBody, int iVinciAdminId);
        void UpdateClientSupportRequest(ClientSupportRequest clientSupportRequest);
    }
}
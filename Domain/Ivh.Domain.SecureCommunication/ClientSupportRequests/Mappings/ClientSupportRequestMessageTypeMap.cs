﻿namespace Ivh.Domain.SecureCommunication.ClientSupportRequests.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class ClientSupportRequestMessageTypeMap : ClassMap<ClientSupportRequestMessageType>
    {
        public ClientSupportRequestMessageTypeMap()
        {
            this.Schema(Schemas.VisitPay.Dbo);
            this.Table(nameof(ClientSupportRequestMessageType));
            this.ReadOnly();
            this.Id(x => x.ClientSupportRequestMessageTypeId);
            this.Map(x => x.ClientSupportRequestMessageTypeName).Not.Nullable();
        }
    }
}
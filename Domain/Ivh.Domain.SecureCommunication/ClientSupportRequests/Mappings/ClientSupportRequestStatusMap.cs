﻿namespace Ivh.Domain.SecureCommunication.ClientSupportRequests.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class ClientSupportRequestStatusMap : ClassMap<ClientSupportRequestStatus>
    {
        public ClientSupportRequestStatusMap()
        {
            this.Schema(Schemas.VisitPay.Dbo);
            this.Table(nameof(ClientSupportRequestStatus));
            this.ReadOnly();
            this.Id(x => x.ClientSupportRequestStatusId);
            this.Map(x => x.ClientSupportRequestStatusName).Not.Nullable();
        }
    }
}
﻿namespace Ivh.Domain.SecureCommunication.ClientSupportRequests.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class ClientSupportTopicMap : ClassMap<ClientSupportTopic>
    {
        public ClientSupportTopicMap()
        {
            this.Schema("dbo");
            this.ReadOnly();
            this.Table("ClientSupportTopic");
            this.Id(x => x.ClientSupportTopicId);
            this.Map(x => x.ClientSupportTopicName).Not.Nullable();
            this.Map(x => x.IsActive).Not.Nullable();
            this.Map(x => x.SortOrder).Not.Nullable();
            this.References(x => x.ParentClientSupportTopic).Column("ParentClientSupportTopicId").Nullable().Not.Update().Not.Insert().Not.LazyLoad();

            this.HasManyToMany(x => x.Templates)
                .LazyLoad()
                .BatchSize(50)
                .Table("ClientSupportTopicCmsRegion")
                .ParentKeyColumn("ClientSupportTopicId")
                .ChildKeyColumn("CmsRegionID");
        }
    }
}
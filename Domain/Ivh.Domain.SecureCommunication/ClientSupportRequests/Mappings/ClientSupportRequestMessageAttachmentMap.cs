﻿namespace Ivh.Domain.SecureCommunication.ClientSupportRequests.Mappings
{
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class ClientSupportRequestMessageAttachmentMap : ClassMap<ClientSupportRequestMessageAttachment>
    {
        public ClientSupportRequestMessageAttachmentMap()
        {
            this.Schema("dbo");
            this.Table("ClientSupportRequestMessageAttachment");
            this.Id(x => x.ClientSupportRequestMessageAttachmentId);
            this.Map(x => x.AttachmentFileName).Not.Nullable();
            this.Map(x => x.MimeType).Not.Nullable();
            this.Map(x => x.FileSize).Not.Nullable();
            this.Map(x => x.FileContent).CustomSqlType("VARBINARY (MAX)").Length(2147483647).Nullable().LazyLoad();
            this.Map(x => x.QuarantinedBytes).CustomSqlType("VARBINARY (MAX)").Length(2147483647).Nullable().LazyLoad();
            this.Map(x => x.MalwareScanStatus, "MalwareScanStatusId").CustomType<MalwareScanStatusEnum>().Not.Nullable();
            this.References(x => x.ClientSupportRequestMessage).Column("ClientSupportRequestMessageId").Not.Nullable();
        }
    }
}
﻿namespace Ivh.Domain.SecureCommunication.ClientSupportRequests.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class ClientSupportRequestMap : ClassMap<ClientSupportRequest>
    {
        public ClientSupportRequestMap()
        {
            this.Schema("dbo");
            this.Table("ClientSupportRequest");
            this.Id(x => x.ClientSupportRequestId);
            this.Map(x => x.ClientSupportRequestDisplayId).Not.Nullable();
            this.Map(x => x.ClientSupportRequestStatus).Column("ClientSupportRequestStatusId").CustomType<ClientSupportRequestStatusEnum>().Not.Nullable();
            this.Map(x => x.ContactPhone).Nullable();
            this.Map(x => x.InsertDate);
            this.Map(x => x.ClosedDate).Nullable();
            this.Map(x => x.JiraId).Nullable();
            this.References(x => x.ClientSupportRequestStatusSource).Column("ClientSupportRequestStatusId").Not.Nullable().Not.Insert().Not.Update();
            this.References(x => x.ClientSupportTopic).Column("ClientSupportTopicId").Not.Nullable();
            this.References(x => x.VisitPayUser).Column("VisitPayUserId").Not.Nullable();
            this.References(x => x.AssignedVisitPayUser).Column("AssignedVisitPayUserId").Nullable();
            this.References(x => x.TargetVpGuarantor).Column("TargetVpGuarantorId").Nullable();

            this.HasMany(x => x.ClientSupportRequestMessages)
                .BatchSize(50)
                .KeyColumn("ClientSupportRequestId")
                .Not.LazyLoad()
                .Fetch.Subselect()
                .Cascade.All();
        }
    }
}
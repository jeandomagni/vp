﻿namespace Ivh.Domain.SecureCommunication.ClientSupportRequests.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class ClientSupportRequestMessageMap : ClassMap<ClientSupportRequestMessage>
    {
        public ClientSupportRequestMessageMap()
        {
            this.Schema("dbo");
            this.Table("ClientSupportRequestMessage");
            this.Id(x => x.ClientSupportRequestMessageId);
            this.Map(x => x.ClientSupportRequestMessageType).Column("ClientSupportRequestMessageTypeId").CustomType<ClientSupportRequestMessageTypeEnum>().Not.Nullable();
            this.Map(x => x.MessageBody).Not.Nullable();
            this.Map(x => x.IsRead).Not.Nullable();
            this.Map(x => x.InsertDate);
            this.References(x => x.ClientSupportRequest).Column("ClientSupportRequestId").Not.Nullable();
            this.References(x => x.CreatedByVisitPayUser).Column("CreatedByVisitPayUserId").Nullable();

            this.HasMany(x => x.ClientSupportRequestMessageAttachments)
                .BatchSize(50)
                .KeyColumn("ClientSupportRequestMessageId")
                .Not.LazyLoad()
                .Fetch.Subselect()
                .Cascade.All();
        }
    }
}
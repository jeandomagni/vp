﻿namespace Ivh.Domain.SecureCommunication.ClientSupportRequests.Entities
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class ClientSupportRequestFilter
    {
        public string ClientSupportRequestDisplayId { get; set; }

        public ClientSupportRequestStatusEnum? ClientSupportRequestStatus { get; set; }

        public int? AssignedToVisitPayUserId { get; set; }

        public int? CreatedByVisitPayUserId { get; set; }

        public int? TargetVpGuarantorId { get; set; }

        public int? DaysOffset { get; set; }

        public string JiraId { get; set; }

        public int Page { get; set; }

        public int Rows { get; set; }

        public string SortField { get; set; }

        public string SortOrder { get; set; }
    }
}
﻿namespace Ivh.Domain.SecureCommunication.ClientSupportRequests.Entities
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class ClientSupportRequestMessageType
    {
        public virtual int ClientSupportRequestMessageTypeId { get; set; }
        public virtual string ClientSupportRequestMessageTypeName { get; set; }

        public virtual ClientSupportRequestMessageTypeEnum ClientSupportRequestMessageTypeEnum
        {
            get { return (ClientSupportRequestMessageTypeEnum) this.ClientSupportRequestMessageTypeId; }
        }
    }
}
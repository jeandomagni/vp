﻿namespace Ivh.Domain.SecureCommunication.ClientSupportRequests.Entities
{
    using System.Collections.Generic;

    public class ClientSupportRequestResults
    {
        public IReadOnlyList<ClientSupportRequest> ClientSupportRequests { get; set; }

        public int TotalRecords { get; set; }
    }
}
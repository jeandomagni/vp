﻿namespace Ivh.Domain.SecureCommunication.ClientSupportRequests.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Ivh.Domain.Guarantor.Entities;
    using User.Entities;

    public class ClientSupportRequest
    {
        public ClientSupportRequest()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        private IList<ClientSupportRequestMessage> _clientSupportRequestMessages;

        public virtual int ClientSupportRequestId { get; set; }
        public virtual string ClientSupportRequestDisplayId { get; set; }
        public virtual ClientSupportTopic ClientSupportTopic { get; set; }
        public virtual ClientSupportRequestStatusEnum ClientSupportRequestStatus { get; set; }
        public virtual ClientSupportRequestStatus ClientSupportRequestStatusSource { get; set; }
        public virtual string ContactPhone { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual string JiraId { get; set; }
        public virtual DateTime? ClosedDate { get; set; }
        public virtual VisitPayUser VisitPayUser { get; set; }
        public virtual VisitPayUser AssignedVisitPayUser { get; set; }
        public virtual Guarantor TargetVpGuarantor { get; set; }

        public virtual IList<ClientSupportRequestMessage> ClientSupportRequestMessages
        {
            get { return this._clientSupportRequestMessages ?? (this.ClientSupportRequestMessages = new List<ClientSupportRequestMessage>()); }
            set { this._clientSupportRequestMessages = value; }
        }

        //

        public virtual ClientSupportRequestMessage FirstMessageFromClient
        {
            get { return this.ClientSupportRequestMessages.Where(x => x.ClientSupportRequestMessageType == ClientSupportRequestMessageTypeEnum.FromClient).OrderBy(x => x.InsertDate).FirstOrDefault(); }
        }

        public virtual IList<ClientSupportRequestMessage> MessagesClient
        {
            get
            {
                return this.ClientSupportRequestMessages.Where(x => x.ClientSupportRequestMessageType == ClientSupportRequestMessageTypeEnum.FromClient ||
                                                                    x.ClientSupportRequestMessageType == ClientSupportRequestMessageTypeEnum.ToClient).ToList();
            }
        }

        public virtual IList<ClientSupportRequestMessage> MessagesInternal
        {
            get
            {
                return this.ClientSupportRequestMessages.Where(x => x.ClientSupportRequestMessageType == ClientSupportRequestMessageTypeEnum.InternalNote ||
                                                                    x.ClientSupportRequestMessageType == ClientSupportRequestMessageTypeEnum.CloseMessage ||
                                                                    x.ClientSupportRequestMessageType == ClientSupportRequestMessageTypeEnum.OpenMessage).ToList();
            }            
        }

        public virtual string MessagePreview
        {
            get { return this.FirstMessageFromClient == null ? string.Empty : this.FirstMessageFromClient.MessageBody; }
        }

        public virtual ClientSupportRequestMessage MostRecentMessage
        {
            get
            {
                return this.ClientSupportRequestMessages.OrderByDescending(x => x.InsertDate).FirstOrDefault(); 
            }
        }

        public virtual DateTime? LastModifiedDate
        {
            get { return this.MostRecentMessage == null ? (DateTime?)null : this.MostRecentMessage.InsertDate; }
        }

        public virtual VisitPayUser LastModifiedByVisitPayUser
        {
            get { return this.MostRecentMessage == null ? null : this.MostRecentMessage.CreatedByVisitPayUser; }
        }

        public virtual string TopicDisplay
        {
            get
            {
                string topicName = this.ClientSupportTopic.ClientSupportTopicName;
                return this.ClientSupportTopic.ParentClientSupportTopic != null ? string.Format("{0} - {1}", this.ClientSupportTopic.ParentClientSupportTopic.ClientSupportTopicName, topicName) : topicName;
            }
        }

        public virtual int UnreadMessageCountFromClient
        {
            get { return this.ClientSupportRequestMessages.Count(x => x.IsRead == false && x.ClientSupportRequestMessageType == ClientSupportRequestMessageTypeEnum.FromClient); }
        }

        public virtual int UnreadMessageCountToClient
        {
            get { return this.ClientSupportRequestMessages.Count(x => x.IsRead == false && x.ClientSupportRequestMessageType == ClientSupportRequestMessageTypeEnum.ToClient); }
        }

        public virtual int AttachmentCountClient
        {
            get { return this.MessagesClient.Sum(x => x.AttachmentCount); }
        }

        public virtual int AttachmentCountInternal
        {
            get { return this.MessagesInternal.Sum(x => x.AttachmentCount); }
        }
    }
}
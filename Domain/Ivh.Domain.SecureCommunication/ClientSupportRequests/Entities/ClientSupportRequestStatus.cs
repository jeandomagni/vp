﻿namespace Ivh.Domain.SecureCommunication.ClientSupportRequests.Entities
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class ClientSupportRequestStatus
    {
        public virtual int ClientSupportRequestStatusId { get; set; }
        public virtual string ClientSupportRequestStatusName { get; set; }

        public virtual ClientSupportRequestStatusEnum ClientSupportRequestStatusEnum
        {
            get { return (ClientSupportRequestStatusEnum) this.ClientSupportRequestStatusId; }
        }
    }
}
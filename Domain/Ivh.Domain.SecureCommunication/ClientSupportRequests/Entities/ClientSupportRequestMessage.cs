﻿namespace Ivh.Domain.SecureCommunication.ClientSupportRequests.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using User.Entities;

    public class ClientSupportRequestMessage
    {
        public ClientSupportRequestMessage()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        private IList<ClientSupportRequestMessageAttachment> _clientSupportRequestMessageAttachments;

        public virtual int ClientSupportRequestMessageId { get; set; }
        public virtual ClientSupportRequest ClientSupportRequest { get; set; }
        public virtual ClientSupportRequestMessageTypeEnum ClientSupportRequestMessageType { get; set; }
        public virtual string MessageBody { get; set; }
        public virtual bool IsRead { get; set; }
        public virtual VisitPayUser CreatedByVisitPayUser { get; set; }
        public virtual DateTime InsertDate { get; set; }

        public virtual IList<ClientSupportRequestMessageAttachment> ClientSupportRequestMessageAttachments
        {
            get { return this._clientSupportRequestMessageAttachments ?? (this.ClientSupportRequestMessageAttachments = new List<ClientSupportRequestMessageAttachment>()); }
            set { this._clientSupportRequestMessageAttachments = value; }
        }

        public virtual IList<ClientSupportRequestMessageAttachment> ActiveClientSupportRequestMessageAttachments => this.ClientSupportRequestMessageAttachments.Where(x => x.FileSize > 0).ToList();

        //

        public virtual int AttachmentCount
        {
            get { return this.ActiveClientSupportRequestMessageAttachments.Count; }
        }
    }
}
﻿namespace Ivh.Domain.SecureCommunication.ClientSupportRequests.Entities
{
    using Common.VisitPay.Enums;

    public class ClientSupportRequestMessageAttachment
    {
        public virtual int ClientSupportRequestMessageAttachmentId { get; set; }
        public virtual ClientSupportRequestMessage ClientSupportRequestMessage { get; set; }
        public virtual string AttachmentFileName { get; set; }
        public virtual string MimeType { get; set; }
        public virtual int FileSize { get; set; }
        public virtual byte[] FileContent { get; set; }
        public virtual byte[] QuarantinedBytes { get; set; }
        public virtual MalwareScanStatusEnum MalwareScanStatus { get; set; }
    }
}
﻿namespace Ivh.Domain.SecureCommunication.ClientSupportRequests.Entities
{
    using System.Collections.Generic;
    using Content.Entities;

    public class ClientSupportTopic
    {
        private IList<CmsRegion> _templates;

        public virtual int ClientSupportTopicId { get; set; }
        public virtual string ClientSupportTopicName { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual int SortOrder { get; set; }
        public virtual ClientSupportTopic ParentClientSupportTopic { get; set; }

        public virtual IList<CmsRegion> Templates
        {
            get { return this._templates ?? (this.Templates = new List<CmsRegion>()); }
            set { this._templates = value; }
        }
    }
}
﻿namespace Ivh.Domain.SecureCommunication.ClientSupportRequests.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.Data;
    using Guarantor.Entities;
    using Entities;
    using GuarantorSupportRequests.Entities;
    using Interfaces;
    using NHibernate;
    using Common.Base.Utilities.Helpers;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.ServiceBus.Common;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.Communication;
    using User.Entities;
    using User.Interfaces;

    public class ClientSupportRequestService : DomainService, IClientSupportRequestService
    {
        private readonly ISession _session;
        private readonly Lazy<IClientSupportRequestRepository> _clientSupportRequestRepository;
        private readonly Lazy<IClientSupportRequestMessageRepository> _clientSupportRequestMessageRepository;
        private readonly Lazy<IClientSupportTopicRepository> _clientSupportTopicRepository;
        private readonly Lazy<IVisitPayUserRepository> _visitPayUserRepository;

        public ClientSupportRequestService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            ISessionContext<VisitPay> sessionContext,
            Lazy<IClientSupportRequestRepository> clientSupportRequestRepository,
            Lazy<IClientSupportRequestMessageRepository> clientSupportRequestMessageRepository,
            Lazy<IClientSupportTopicRepository> clientSupportTopicRepository,
            Lazy<IVisitPayUserRepository> visitPayUserRepository) : base(serviceCommonService)
        {
            this._session = sessionContext.Session;
            this._clientSupportRequestRepository = clientSupportRequestRepository;
            this._clientSupportRequestMessageRepository = clientSupportRequestMessageRepository;
            this._clientSupportTopicRepository = clientSupportTopicRepository;
            this._visitPayUserRepository = visitPayUserRepository;
        }

        public ClientSupportRequest GetClientSupportRequest(int clientSupportRequestId)
        {
            return this._clientSupportRequestRepository.Value.GetById(clientSupportRequestId);
        }

        public ClientSupportRequestResults GetClientSupportRequests(ClientSupportRequestFilter filter)
        {
            return this._clientSupportRequestRepository.Value.GetClientSupportRequests(filter);
        }

        public IList<ClientSupportTopic> GetClientSupportTopics()
        {
            return this._clientSupportTopicRepository.Value.GetQueryable().Where(x => x.IsActive).OrderBy(x => x.SortOrder).ToList();
        }

        public int GetUnreadMessagesCountFromClient(int? assignedVisitPayUserId)
        {
            return this._clientSupportRequestMessageRepository.Value.GetUnreadMessagesCountFromClient(assignedVisitPayUserId);
        }

        public int GetUnreadMessagesCountToClient(int? visitPayUserId)
        {
            return this._clientSupportRequestMessageRepository.Value.GetUnreadMessagesCountToClient(visitPayUserId);
        }

        public byte[] GetAttachmentContent(int clientSupportRequestMessageAttachmentId)
        {
            return this._clientSupportRequestRepository.Value.GetAttachmentContent(clientSupportRequestMessageAttachmentId);
        }

        public void CreateClientSupportRequest(ClientSupportRequest clientSupportRequest, string messageBody, SupportRequest referenceGuarantorSupportRequest)
        {
            this.CreateClientSupportRequestForUser(clientSupportRequest, messageBody, false, -1, referenceGuarantorSupportRequest);
        }

        public void CreateClientSupportRequestOnBehalfOfClient(ClientSupportRequest clientSupportRequest, string messageBody, int iVinciAdminId)
        {
            this.CreateClientSupportRequestForUser(clientSupportRequest, messageBody, true, iVinciAdminId, null);
        }

        private void CreateClientSupportRequestForUser(ClientSupportRequest clientSupportRequest, string messageBody, bool isOnBehalfOfClient, int iVinciAdminId, SupportRequest referenceGuarantorSupportRequest)
        {
            using (IUnitOfWork uow = new UnitOfWork(this._session))
            {
                DateTime now = DateTime.UtcNow;

                clientSupportRequest.InsertDate = now;
                clientSupportRequest.ClientSupportRequestStatus = ClientSupportRequestStatusEnum.Open;

                ClientSupportRequestMessageTypeEnum clientSupportRequestMessageTypeEnum = (isOnBehalfOfClient)
                    ? ClientSupportRequestMessageTypeEnum.ToClient
                    : ClientSupportRequestMessageTypeEnum.FromClient;

                VisitPayUser visitPayUser = (isOnBehalfOfClient) ? new VisitPayUser { VisitPayUserId = iVinciAdminId } : clientSupportRequest.VisitPayUser;

                clientSupportRequest.ClientSupportRequestMessages.Add(new ClientSupportRequestMessage
                {
                    ClientSupportRequest = clientSupportRequest,
                    ClientSupportRequestMessageType = clientSupportRequestMessageTypeEnum,
                    CreatedByVisitPayUser = visitPayUser,
                    MessageBody = messageBody,
                    InsertDate = now,
                    IsRead = false
                });

                if (referenceGuarantorSupportRequest != null)
                {
                    clientSupportRequest.ClientSupportRequestMessages.Add(new ClientSupportRequestMessage
                    {
                        ClientSupportRequest = clientSupportRequest,
                        ClientSupportRequestMessageType = ClientSupportRequestMessageTypeEnum.InternalNote,
                        CreatedByVisitPayUser = clientSupportRequest.VisitPayUser,
                        MessageBody = $"Reference Support Request {referenceGuarantorSupportRequest.SupportRequestDisplayId}",
                        InsertDate = now
                    });
                }

                this.SaveClientSupportRequest(clientSupportRequest);

                this._session.Transaction.RegisterPostCommitSuccessAction(() =>
                {
                    this.Bus.Value.PublishMessage(new SendClientSupportRequestConfirmationEmailMessage
                    {
                        ClientSupportRequestDisplayId = clientSupportRequest.ClientSupportRequestDisplayId,
                        VisitPayUserId = clientSupportRequest.VisitPayUser.VisitPayUserId
                    }).Wait();

                    this.Bus.Value.PublishMessage(new SendClientSupportRequestAdminNotificationEmailMessage
                    {
                        ClientSupportRequestDisplayId = clientSupportRequest.ClientSupportRequestDisplayId
                    }).Wait();
                });

                uow.Commit();
            }
        }
        public void SaveAttachment(int clientSupportRequestId, int clientSupportRequestMessageId, ClientSupportRequestMessageAttachment attachment)
        {
            ClientSupportRequest clientSupportRequest = this.GetClientSupportRequest(clientSupportRequestId);

            ClientSupportRequestMessage clientSupportRequestMessage = clientSupportRequest.ClientSupportRequestMessages.FirstOrDefault(x => x.ClientSupportRequestMessageId == clientSupportRequestMessageId);
            if (clientSupportRequestMessage == null)
            {
                return;
            }

            attachment.ClientSupportRequestMessage = clientSupportRequestMessage;
            clientSupportRequestMessage.ClientSupportRequestMessageAttachments.Add(attachment);

            this.SaveClientSupportRequest(clientSupportRequest);
        }

        public void DeleteAttachment(int clientSupportRequestId, int clientSupportRequestMessageAttachmentId)
        {
            ClientSupportRequest clientSupportRequest = this.GetClientSupportRequest(clientSupportRequestId);

            ClientSupportRequestMessageAttachment attachment = clientSupportRequest.ClientSupportRequestMessages
                .SelectMany(x => x.ActiveClientSupportRequestMessageAttachments)
                .FirstOrDefault(x => x.ClientSupportRequestMessageAttachmentId == clientSupportRequestMessageAttachmentId);

            if (attachment != null)
            {
                attachment.FileContent = new byte[] { };
                attachment.FileSize = 0;
                attachment.AttachmentFileName = "";
            }

            this.SaveClientSupportRequest(clientSupportRequest);
        }

        public void SaveDetails(int clientSupportRequestId, int? assignedToVisitPayUserId, int? targetVpGuarantorId, string jiraId, int visitPayUserId)
        {
            ClientSupportRequest clientSupportRequest = this.GetClientSupportRequest(clientSupportRequestId);

            bool hasJiraIdChanged = clientSupportRequest.JiraId != jiraId;
            bool hasAssignedChanged = (clientSupportRequest.AssignedVisitPayUser == null && assignedToVisitPayUserId != null) || (clientSupportRequest.AssignedVisitPayUser != null && clientSupportRequest.AssignedVisitPayUser.VisitPayUserId != assignedToVisitPayUserId);
            bool hasTargetVpGuarantorIdChanged = (clientSupportRequest.TargetVpGuarantor == null && targetVpGuarantorId != null) || (clientSupportRequest.TargetVpGuarantor != null && clientSupportRequest.TargetVpGuarantor.VpGuarantorId != targetVpGuarantorId);

            clientSupportRequest.AssignedVisitPayUser = assignedToVisitPayUserId == null ? null : new VisitPayUser { VisitPayUserId = assignedToVisitPayUserId.Value };
            clientSupportRequest.JiraId = jiraId;
            clientSupportRequest.TargetVpGuarantor = targetVpGuarantorId == null ? null : new Guarantor { VpGuarantorId = targetVpGuarantorId.Value };

            //
            IList<string> changesToLog = new List<string>();
            if (hasAssignedChanged)
            {
                VisitPayUser assignedVisitPayUser = this._visitPayUserRepository.Value.FindById(assignedToVisitPayUserId.GetValueOrDefault(0).ToString());
                changesToLog.Add($"Assigned to: {(assignedToVisitPayUserId.HasValue ? assignedVisitPayUser.FirstNameLastName : "Unassigned")}");
            }
            if (hasJiraIdChanged)
            {
                changesToLog.Add($"Jira ID: {(!string.IsNullOrEmpty(jiraId) ? jiraId : "No Value")}");
            }
            if (hasTargetVpGuarantorIdChanged)
            {
                changesToLog.Add($"{this.Client.Value.AppSupportRequestNamePrefix} ID: {(targetVpGuarantorId.HasValue ? targetVpGuarantorId.Value.ToString() : "Not Set")}");
            }

            if (changesToLog.Any())
            {
                clientSupportRequest.ClientSupportRequestMessages.Add(new ClientSupportRequestMessage
                {
                    CreatedByVisitPayUser = new VisitPayUser { VisitPayUserId = visitPayUserId },
                    InsertDate = DateTime.UtcNow,
                    IsRead = false,
                    MessageBody = $"Action: {"Ticket Updated"}{Environment.NewLine}{string.Join(", ", changesToLog)}",
                    ClientSupportRequest = clientSupportRequest,
                    ClientSupportRequestMessageType = ClientSupportRequestMessageTypeEnum.InternalNote
                });
            }

            //
            this.SaveClientSupportRequest(clientSupportRequest);
        }

        public void SaveMessage(int clientSupportRequestId, ClientSupportRequestMessage clientSupportRequestMessage)
        {
            using (IUnitOfWork uow = new UnitOfWork(this._session))
            {
                ClientSupportRequest clientSupportRequest = this.GetClientSupportRequest(clientSupportRequestId);

                clientSupportRequest.ClientSupportRequestMessages.Add(clientSupportRequestMessage);
                clientSupportRequestMessage.ClientSupportRequest = clientSupportRequest;

                this.SaveClientSupportRequest(clientSupportRequest);

                if (clientSupportRequestMessage.ClientSupportRequestMessageType == ClientSupportRequestMessageTypeEnum.ToClient)
                {
                    this._session.Transaction.RegisterPostCommitSuccessAction(() =>
                    {
                        this.Bus.Value.PublishMessage(new SendClientSupportRequestMessageNotificationEmailMessage
                        {
                            ClientSupportRequestDisplayId = clientSupportRequest.ClientSupportRequestDisplayId,
                            InsertDate = clientSupportRequest.InsertDate,
                            VisitPayUserId = clientSupportRequest.VisitPayUser.VisitPayUserId
                        }).Wait();
                    });
                }
                else if (clientSupportRequestMessage.ClientSupportRequestMessageType == ClientSupportRequestMessageTypeEnum.FromClient)
                {
                    this._session.Transaction.RegisterPostCommitSuccessAction(() =>
                    {
                        this.Bus.Value.PublishMessage(new SendClientSupportRequestMessageAdminNotificationEmailMessage
                        {
                            ClientSupportRequestDisplayId = clientSupportRequest.ClientSupportRequestDisplayId,
                            InsertDate = clientSupportRequest.InsertDate,
                        }).Wait();
                    });
                }

                uow.Commit();
            }
        }

        public void SetMessageRead(int clientSupportRequestId, ClientSupportRequestMessageTypeEnum clientSupportRequestMessageType)
        {
            ClientSupportRequest clientSupportRequest = this.GetClientSupportRequest(clientSupportRequestId);
            if (clientSupportRequest == null)
            {
                return;
            }

            clientSupportRequest.ClientSupportRequestMessages.Where(x => x.ClientSupportRequestMessageType == clientSupportRequestMessageType).ForEach(x =>
            {
                x.IsRead = true;
            });

            this.SaveClientSupportRequest(clientSupportRequest);
        }

        public ClientSupportRequest SetSupportRequestStatus(int supportRequestId, int visitPayUserId, ClientSupportRequestStatusEnum clientSupportRequestStatus, string message)
        {
            string messageBodyPrefix = clientSupportRequestStatus == ClientSupportRequestStatusEnum.Open ? "Ticket Re-Opened" : "Ticket Marked Complete";

            using (IUnitOfWork uow = new UnitOfWork(this._session))
            {
                ClientSupportRequest clientSupportRequest = this.GetClientSupportRequest(supportRequestId);

                clientSupportRequest.ClientSupportRequestMessages.Add(new ClientSupportRequestMessage
                {
                    CreatedByVisitPayUser = new VisitPayUser { VisitPayUserId = visitPayUserId },
                    InsertDate = DateTime.UtcNow,
                    IsRead = false,
                    MessageBody = $"Action: {messageBodyPrefix}{Environment.NewLine}{message}",
                    ClientSupportRequest = clientSupportRequest,
                    ClientSupportRequestMessageType = clientSupportRequestStatus == ClientSupportRequestStatusEnum.Open ? ClientSupportRequestMessageTypeEnum.OpenMessage : ClientSupportRequestMessageTypeEnum.CloseMessage
                });

                clientSupportRequest.ClientSupportRequestStatus = clientSupportRequestStatus;
                clientSupportRequest.ClosedDate = clientSupportRequestStatus == ClientSupportRequestStatusEnum.Open ? (DateTime?)null : DateTime.UtcNow;

                this._session.Transaction.RegisterPostCommitSuccessAction(() =>
                {
                    this.Bus.Value.PublishMessage(new SendClientSupportRequestStatusNotificationEmailMessage
                    {
                        ClientSupportRequestDisplayId = clientSupportRequest.ClientSupportRequestDisplayId,
                        InsertDate = clientSupportRequest.InsertDate,
                        Status = clientSupportRequestStatus,
                        VisitPayUserId = clientSupportRequest.VisitPayUser.VisitPayUserId
                    }).Wait();
                });

                this.SaveClientSupportRequest(clientSupportRequest);

                uow.Commit();

                return clientSupportRequest;
            }
        }

        public IDictionary<int, string> GetAllClientUsersWithSupportRequest()
        {
            IReadOnlyList<int> visitPayUserIds = this._clientSupportRequestRepository.Value.GetAllClientUsersWithSupportRequest();

            VisitPayUserResults visitPayUsers = this._visitPayUserRepository.Value.GetVisitPayUsers(new VisitPayUserFilter { VisitPayUserIds = visitPayUserIds.ToList() }, 3, 0, int.MaxValue);

            return visitPayUsers.VisitPayUsers.OrderBy(x => x.LastName).ThenBy(x => x.FirstName).ToDictionary(k => k.VisitPayUserId, v => FormatHelper.FirstNameLastName(v.FirstName, v.LastName));
        }

        public IReadOnlyList<VisitPayUser> GetAllAssignedUsers()
        {
            IList<int> assignedUsers = this._clientSupportRequestRepository.Value.GetAllAssignedUsers();
            if (assignedUsers.Any())
            {
                return this._visitPayUserRepository.Value.GetVisitPayUsers(new VisitPayUserFilter
                {
                    VisitPayUserIds = assignedUsers
                }, 3, 0, int.MaxValue).VisitPayUsers;
            }

            return new List<VisitPayUser>();
        }

        public void UpdateClientSupportRequest(ClientSupportRequest clientSupportRequest)
        {
            this.SaveClientSupportRequest(clientSupportRequest);
        }

        private void SaveClientSupportRequest(ClientSupportRequest clientSupportRequest)
        {
            if (string.IsNullOrEmpty(clientSupportRequest.ClientSupportRequestDisplayId))
            {
                clientSupportRequest.ClientSupportRequestDisplayId = "";
            }

            this._clientSupportRequestRepository.Value.InsertOrUpdate(clientSupportRequest);

            if (!string.IsNullOrEmpty(clientSupportRequest.ClientSupportRequestDisplayId))
            {
                return;
            }

            clientSupportRequest.ClientSupportRequestDisplayId = $"{this.Client.Value.AppSupportRequestCasePrefix}-{clientSupportRequest.ClientSupportRequestId}";
            this._clientSupportRequestRepository.Value.InsertOrUpdate(clientSupportRequest);
        }
    }
}
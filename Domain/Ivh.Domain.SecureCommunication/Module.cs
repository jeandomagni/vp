﻿namespace Ivh.Domain.SecureCommunication
{
    using Autofac;
    using ClientSupportRequests.Interfaces;
    using ClientSupportRequests.Services;
    using GuarantorSupportRequests.Interfaces;
    using GuarantorSupportRequests.Services;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ClientSupportRequestService>().As<IClientSupportRequestService>();

            builder.RegisterType<SupportRequestService>().As<ISupportRequestService>();
            builder.RegisterType<SupportTopicService>().As<ISupportTopicService>();
            builder.RegisterType<SupportTemplateService>().As<ISupportTemplateService>();
        }
    }
}
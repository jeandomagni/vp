﻿namespace Ivh.Domain.Qat.Interfaces
{
    using System.Collections.Generic;
    using FinanceManagement.Payment.Entities;

    public interface IQaPaymentService
    {
        List<Payment> GetTextToPayPayments(int guarantorId);
        IList<PaymentAllocation> GetUnclearedPaymentAllocationsForVisits(IList<int> visitIds);
    }
}
﻿namespace Ivh.Domain.Qat.Interfaces
{
    using Common.Base.Interfaces;
    using User.Entities;

    public interface IQaVisitPayUserRepository: IRepository<VisitPayUser>
    {
    }
}

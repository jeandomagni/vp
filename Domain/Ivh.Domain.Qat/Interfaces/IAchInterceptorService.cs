﻿namespace Ivh.Domain.Qat.Interfaces
{
    using System;
    using Common.Base.Interfaces;

    public interface IAchInterceptorService : IDomainService
    {
        void Activate(string uri);
        void Deactivate();

        string GetReturnFile(DateTime startDate, DateTime endDate, string fileFormat);
        string GetReport(string reportTypeId, DateTime startDate, DateTime endDate, string dateTypeId, string exportFormat);
    }
}

﻿namespace Ivh.Domain.Qat.Interfaces
{
    using System.Collections.Generic;
    using Entities;

    public interface IIhcEnrollmentService
    {
        IhcEnrollmentResponse GetByGuarantorId(string guarantorId);
        void AddVisits(string guarantorSourceSystemKey, IEnumerable<string> visitSourceSystemKeys);
    }
}
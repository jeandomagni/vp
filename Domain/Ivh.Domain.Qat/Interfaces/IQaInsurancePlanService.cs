﻿namespace Ivh.Domain.Qat.Interfaces
{
    using Common.Base.Interfaces;
    using Common.VisitPay.Messages.HospitalData.Dtos;

    public interface IQAInsurancePlanService : IDomainService
    {
        void AddInsurancePlan(InsurancePlanDto insurancePlanDto);
    }
}
﻿namespace Ivh.Domain.Qat.Interfaces
{
    using Common.Base.Interfaces;
    using HospitalData.Visit.Entities;

    public interface IQaHsVisitRepository : IRepository<Visit>
    {

        void UpdateQaVisitInsurancePlans(InsurancePlan insurancePlan);
        void UpdateQaEobPayor(int externalLinkId);

    }
}

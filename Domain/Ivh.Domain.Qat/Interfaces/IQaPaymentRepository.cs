﻿namespace Ivh.Domain.Qat.Interfaces
{
    using Common.Base.Interfaces;
    using FinanceManagement.Payment.Entities;

    public interface IQaPaymentRepository : IRepository<Payment>
    {
    }
}
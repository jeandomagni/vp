﻿namespace Ivh.Domain.Qat.Interfaces
{
    public interface IQaSettingsService
    {
        bool ChangeClientLogo(string filename, byte[] logo);
    }
}
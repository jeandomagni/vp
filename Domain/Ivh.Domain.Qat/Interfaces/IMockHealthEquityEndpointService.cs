﻿namespace Ivh.Domain.Qat.Interfaces
{
    using System.Collections.Generic;
    using Application.Qat.Common.Dtos;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public interface IMockHealthEquityEndpointService
    {
        IList<SavingsAccountInfoDto> GetSavingsAccountInfos(int vpguarantorId, IList<AccountStatusApiModelEnum> statuses, IList<AccountTypeFlagApiModelEnum> types);
    }
}

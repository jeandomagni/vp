﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.Qat.Interfaces
{
    using Common.Base.Interfaces;
    public interface IQaCommunicationRepository : IRepository<Ivh.Domain.Email.Entities.Communication>
    {
    }
}

﻿namespace Ivh.Domain.Qat.Interfaces
{
    using System.Collections.Generic;
    using Eob.Entities;

    public interface IQaVisitService
    {
        IList<EobExternalLink> GetEobExternalLinks();
        bool UpdateEobPayerFilterExternalLink(int eobPayerFilterId, int eobExternalLinkId);
    }
}

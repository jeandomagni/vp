﻿namespace Ivh.Domain.Qat.Interfaces
{
    using Common.VisitPay.Enums;

    public interface IAppBaseDataService
    {
        void AddHsGuarantor(string hsGuarantorSourceSystemKey, int billingSystemId);
        void AddVisit(string visitSourceSystemKey, int billingSystemId);
        void AddGuarantorScoreToVpApp(int vpGuarantorId, int score, ScoreTypeEnum scoreType);
    }
}

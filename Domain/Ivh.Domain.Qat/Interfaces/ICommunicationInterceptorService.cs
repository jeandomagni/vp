﻿namespace Ivh.Domain.Qat.Interfaces
{
    using Common.Base.Interfaces;

    public interface ICommunicationInterceptorService : IDomainService
    {
        bool IsActive();
        void Activate(string uri);
        void Deactivate();
    }
}
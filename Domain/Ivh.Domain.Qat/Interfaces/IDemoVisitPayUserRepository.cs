﻿namespace Ivh.Domain.Qat.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IDemoVisitPayUserRepository : IRepository<DemoVisitPayUserClone>
    {
    }
}
﻿namespace Ivh.Domain.Qat.Interfaces
{
    using Common.Base.Interfaces;

    public interface IQaVisitTransactionService : IDomainService
    {
        void AdjustVisitBalanceDates(string systemSourceKey, int billingSystemId, int numberOfDays);
    }
}

﻿namespace Ivh.Domain.Qat.Interfaces
{
    using Entities;

    public interface IHsGuarantorKeepCurrentRepository
    {
        void Insert(HsGuarantorKeepCurrent entity);
    }
}
﻿namespace Ivh.Domain.Qat.Interfaces
{
    using System.Collections.Generic;
    using Entities;
    using Guarantor.Entities;
    using User.Entities;

    public interface IDemoVisitPayUserService
    {
        Guarantor GetGuarantorFromUser(string username);
        void ClearAndRename(string username);
        void PostClone(string sourceUserName, string newUserName, string suffix);

        DemoVisitPayUserClone GetDemoVisitPayUserClone(string visitPayUserName);
        void RenameVisitPayUser(string visitPayUserName, string newUserName);
        VisitPayUser GetVisitPayUserFromUserName(string visitPayUserName);
        List<DemoVisitPayUserClone> GetAllDemoVisitPayUserClones();
    }
}
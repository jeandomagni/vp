﻿namespace Ivh.Domain.Qat.Interfaces
{
    using Common.Base.Interfaces;
    using FinanceManagement.Entities;

    public interface ITestPaymentMethodsService : IDomainService
    {
        void SaveOrUpdate(PaymentMethod paymentMethod, int visitPayUserId);
    }
}
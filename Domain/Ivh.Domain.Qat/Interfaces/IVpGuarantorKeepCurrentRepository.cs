﻿namespace Ivh.Domain.Qat.Interfaces
{
    using Entities;

    public interface IVpGuarantorKeepCurrentRepository
    {
        VpGuarantorKeepCurrent GetById(int id);
        void Insert(VpGuarantorKeepCurrent entity);
    }
}
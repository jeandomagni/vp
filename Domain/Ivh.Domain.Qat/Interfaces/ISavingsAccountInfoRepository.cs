﻿namespace Ivh.Domain.Qat.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;
    public interface ISavingsAccountInfoRepository : IRepository<SavingsAccountInfo>
    {
        IList<SavingsAccountInfo> GetSavingsAccountInfos(int vpguarantorId, IList<AccountStatusApiModelEnum> statuses, IList<AccountTypeFlagApiModelEnum> types);
    }
}

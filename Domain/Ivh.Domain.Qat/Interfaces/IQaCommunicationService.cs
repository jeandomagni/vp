﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.Qat.Interfaces
{
    using Email.Entities;

    public interface IQaCommunicationService
    {
        IList<Communication> GetTextToPayPaymentOptionCommunications();
        Communication GetTextToPayPaymentOptionCommunication(int vpUserId);
    }
}

namespace Ivh.Domain.Qat.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Application.Qat.Common.Dtos;
    using Application.Qat.Common.Enums;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IPaymentProcessorInterceptorService : IDomainService
    {
        bool DefaultSuccessful { get; set; }
        int DelayMs { get; set; }
        bool IsActive();
        void Activate(string uri);
        void Deactivate();
        PpiRequest GetPaymentRequest(Guid requestGuid);
        PpiRequest CompletePaymentRequest(Guid requestGuid, PpiResponseTypeEnum responseType, char? responseAvsCode);
        Task<PpiResponse> QueuePaymentRequestAsync(Guid requestGuid, IDictionary<string, string> request);
        PpiRequest SetAchAction(Guid requestGuid, AchFileTypeEnum fileType, DateTime processDate, AchReturnCodeEnum? returnCode, decimal? returnAmount);
        IList<PpiRequest> QueryRequestQueue(PpiRequestFilterDto ppiRequestFilter);
    }
}
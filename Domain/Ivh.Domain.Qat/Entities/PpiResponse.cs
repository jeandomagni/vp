﻿namespace Ivh.Domain.Qat.Entities
{
    using System;
    using System.Text;

    [Serializable]
    public class PpiResponse
    {
        public string Avs { get; set; }
        public string AuthCode { get; set; }
        public string DeclineType { get; set; }
        public string Error { get; set; }
        public string ErrorType { get; set; }
        public string Offenders { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseCodeDescriptor { get; set; }
        public string Status { get; set; }
        public string TransactionId { get; set; }

        public string BillingId { get; set; }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("avs=" + this.Avs ?? string.Empty);
            stringBuilder.AppendLine("authcode=" + this.AuthCode ?? string.Empty);
            stringBuilder.AppendLine("declinetype=" + this.DeclineType ?? string.Empty);
            stringBuilder.AppendLine("error=" + this.Error ?? string.Empty);
            stringBuilder.AppendLine("errortype=" + this.ErrorType ?? string.Empty);
            stringBuilder.AppendLine("offenders=" + this.Offenders ?? string.Empty);
            stringBuilder.AppendLine("responsecode=" + this.ResponseCode ?? string.Empty);
            stringBuilder.AppendLine("responsecodedescriptor=" + this.ResponseCodeDescriptor ?? string.Empty);
            stringBuilder.AppendLine("status=" + this.Status ?? string.Empty);
            stringBuilder.AppendLine("transid=" + this.TransactionId ?? string.Empty);

            stringBuilder.AppendLine("billingid=" + this.BillingId);

            return stringBuilder.ToString();
        }
    }
}
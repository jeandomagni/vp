﻿namespace Ivh.Domain.Qat.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Application.Qat.Common.Enums;
    using Common.Base.Enums;
    using Common.Base.Utilities.Helpers;
    using Common.VisitPay.Enums;
    using Ivh.Common.Base.Constants;
    using Ivh.Common.Base.Utilities.Extensions;

    public class PpiRequest
    {
        public PpiRequest()
        {
            this.AcceptedAvsCodes = new List<string>();
            this.InsertDate = DateTime.UtcNow;
        }

        public Guid RequestGuid { get; set; }
        public DateTime InsertDate { get; set; }

        // 
        public string Action { get; set; }

        // sale
        public string BillingId { get; set; }
        public string Amount { get; set; }
        public string CustomField2 { get; set; }
        public string CustomField3 { get; set; }
        public string CustomField4 { get; set; }
        public string CustomField5 { get; set; }
        public string CustomField6 { get; set; }
        public string CustomField9 { get; set; }
        public string CustomField12 { get; set; }

        // store, verify: all
        public string Name { get; set; }

        // store, verify: cc
        public string Cc { get; set; }
        public string Exp { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Cvv { get; set; }

        // store, verify: ach
        public string Media { get; set; }
        public string Account { get; set; }
        public string Routing { get; set; }

        // response
        public char? ResponseAvsCode { get; private set; }
        public PpiResponseTypeEnum? ResponseType { get; private set; }
        public void SetResponseType(PpiResponseTypeEnum responseType, char? responseAvsCode)
        {
            this.ResponseType = responseType;
            this.ResponseAvsCode = responseAvsCode;
        }
        public async Task GetResponseTypeAsync()
        {
            while (!this.ResponseType.HasValue)
            {
                await Task.Delay(500).ConfigureAwait(false);
            }
        }

        // visitpay
        public PaymentMethodTypeEnum? VpPaymentMethodType { get; set; }
        public string VpAccountName { get; set; }
        public string VpLastFour { get; set; }

        //
        public IList<PpiResponseTypeEnum> ResponseTypes
        {
            get
            {
                List<PpiResponseTypeEnum> items = new List<PpiResponseTypeEnum>();

                if (!this.IsPending || !this.VpPaymentMethodType.HasValue)
                {
                    return items;
                }

                if (this.Action.ToUpper() == "SALE")
                {
                    items.AddRange(EnumHelper<PaymentMethodTypeEnum>.Contains(this.VpPaymentMethodType.Value, PaymentMethodTypeEnumCategory.Ach) ? EnumHelper<PpiResponseTypeEnum>.GetValuesIntersect(PpiResponseTypeEnumCategory.Sale, PpiResponseTypeEnumCategory.Ach) : EnumHelper<PpiResponseTypeEnum>.GetValuesIntersect(PpiResponseTypeEnumCategory.Sale, PpiResponseTypeEnumCategory.Card));
                }
                else if (new[] {"STORE", "VERIFY"}.Contains(this.Action.ToUpper()))
                {
                    items.AddRange(EnumHelper<PaymentMethodTypeEnum>.Contains(this.VpPaymentMethodType.Value, PaymentMethodTypeEnumCategory.Ach) ? EnumHelper<PpiResponseTypeEnum>.GetValuesIntersect(PpiResponseTypeEnumCategory.Store, PpiResponseTypeEnumCategory.Ach) : EnumHelper<PpiResponseTypeEnum>.GetValuesIntersect(PpiResponseTypeEnumCategory.Store, PpiResponseTypeEnumCategory.Card));
                }

                return items;
            }
        }
        public bool IsPending
        {
            get { return this.ResponseType == null; }
        }
        public bool IsSuccess
        {
            get
            {
                if (this.ResponseType == null)
                {
                    return false;
                }

                return this.ResponseType.Value.IsInCategory(PpiResponseTypeEnumCategory.Success);
            }
        }
        public bool IsFail
        {
            get
            {
                return !this.IsPending && !this.IsSuccess;
            }
        }

        public IList<string> AcceptedAvsCodes { get; private set; }
        public void SetAcceptedAvsCodes(IList<string> avsCodes)
        {
            this.AcceptedAvsCodes = avsCodes;
        }

        //
        public AchFileTypeEnum? AchFileType { get; private set; }
        public DateTime? AchProcessDate { get; private set; }
        public AchReturnCodeEnum? AchReturnCode { get; private set; }
        public decimal? AchReturnAmount { get; private set; }
        public bool AchProcessed { get; set; }

        public PpiResponse Response { get; set; }

        public void SetAchAction(AchFileTypeEnum fileType, DateTime processDate, AchReturnCodeEnum? returnCode, decimal? returnAmount)
        {
            this.AchFileType = fileType;
            this.AchProcessDate = processDate;
            this.AchReturnCode = returnCode;
            this.AchReturnAmount = returnAmount;
        }
    }
}
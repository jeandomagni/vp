﻿namespace Ivh.Domain.Qat.Entities
{
    public class VpGuarantorKeepCurrent
    {
        public int VpGuarantorId { get; set; }
        public string KeepCurrentAnchor { get; set; }
        public int KeepCurrentOffset { get; set; }
    }
}
﻿namespace Ivh.Domain.Qat.Entities
{
    public class HsGuarantorKeepCurrent
    {
        public int HsGuarantorId { get; set; }
        public string KeepCurrentAnchor { get; set; }
        public int KeepCurrentOffset { get; set; }
    }
}
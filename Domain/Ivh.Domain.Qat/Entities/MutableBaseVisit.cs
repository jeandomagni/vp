﻿namespace Ivh.Domain.Qat.Entities
{
    using System.Collections.Generic;
    using Visit.Entities;

    public class MutableBaseVisit : BaseVisit
    {
        private IList<MutableBaseVisitTransaction> _visitTransactions;

        public MutableBaseVisit()
        {
            this._visitTransactions = new List<MutableBaseVisitTransaction>();
        }

        public new virtual IList<MutableBaseVisitTransaction> VisitTransactions
        {
            get => this._visitTransactions;
            set => this._visitTransactions = value;
        }
    }
}

﻿namespace Ivh.Domain.Qat.Entities
{
    public class IhcEnrollmentResponse
    {
        public virtual int IhcEnrollmentResponseID { get; set; }
        public virtual string GuarantorId { get; set; }
        public virtual string ExpectedEnrollmentStatus { get; set; }
        public virtual string Response { get; set; }
        public virtual int? ReturnErrorCode { get; set; }
    }
}
﻿namespace Ivh.Domain.Qat.Entities
{
    public class DemoVisitPayUserClone
    {
        public virtual int DemoVisitPayUserCloneId { get; set; }
        public virtual DemoVisitPayUserSource DemoVisitPayUserSource { get; set; }
        public virtual string CloneVisitPayUserName { get; set; }
        public virtual bool IsHidden { get; set; }
    }
}
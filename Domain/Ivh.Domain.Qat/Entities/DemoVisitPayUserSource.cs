﻿
namespace Ivh.Domain.Qat.Entities
{
    using System.Collections.Generic;

    public class DemoVisitPayUserSource
    {
        public virtual int DemoVisitPayUserSourceId { get; set; }
        public virtual int SourceVpGuarantorId { get; set; }
        public virtual int SourceHsGuarantorId { get; set; }
        public virtual string SourceDescription { get; set; }

        public virtual IList<DemoVisitPayUserClone> DemoVisitPayUserClones { get;set;}
    }
}

﻿namespace Ivh.Domain.Qat.Entities
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class SavingsAccountInfo
    {
        public virtual int SavingsAccountInfoId { get; set; }
        public virtual AccountTypeFlagApiModelEnum SavingsAccountTypeId { get; set; }
        public virtual AccountStatusApiModelEnum SavingsAccountStatusId { get; set; }
        public virtual int VpGuarantorId { get; set; }
        public virtual string Description { get; set; }
        public virtual decimal AvailableBalance { get; set; }
        public virtual decimal Investments { get; set; }
        public virtual decimal Contributions { get; set; }
        public virtual decimal Distributions { get; set; }

    }
}

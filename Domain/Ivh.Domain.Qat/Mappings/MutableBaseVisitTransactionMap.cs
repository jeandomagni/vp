﻿namespace Ivh.Domain.Qat.Mappings
{
    using Common.Data.Constants;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;
    using Visit.Mappings;

    public class MutableBaseVisitTransactionMap : ClassMap<MutableBaseVisitTransaction>
    {
        public MutableBaseVisitTransactionMap()
        {
            this.Polymorphism.Explicit();

            this.Schema(Schemas.VisitPay.Base);
            this.Table("VisitTransaction");

            this.Id(x => x.VisitTransactionId).GeneratedBy.Assigned();

            this.Map(x => x.TransactionDescription);
            this.Map(x => x.TransactionAmount);
            this.Map(x => x.PostDate);
            this.Map(x => x.VisitId);
            this.Map(x => x.VpTransactionTypeId).CustomType<VpTransactionTypeEnum>();
        }
    }
}

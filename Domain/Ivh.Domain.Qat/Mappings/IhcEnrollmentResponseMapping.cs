﻿namespace Ivh.Domain.Qat.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class IhcEnrollmentResponseMapping : ClassMap<IhcEnrollmentResponse>
    {
        public IhcEnrollmentResponseMapping()
        {
            this.Table("IhcEnrollmentResponse");
            this.Schema("qa");
            this.Id(x => x.IhcEnrollmentResponseID);
            this.Map(x => x.GuarantorId).Not.Nullable();
            this.Map(x => x.ExpectedEnrollmentStatus).Not.Nullable();
            this.Map(x => x.Response).Not.Nullable();
            this.Map(x => x.ReturnErrorCode).Nullable();
        }
    }
}
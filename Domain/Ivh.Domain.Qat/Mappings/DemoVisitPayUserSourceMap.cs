﻿
namespace Ivh.Domain.Qat.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class DemoVisitPayUserSourceMap: ClassMap<DemoVisitPayUserSource>
    {
        public DemoVisitPayUserSourceMap()
        {
            this.Table(nameof(DemoVisitPayUserSource));
            this.Schema("qa");
            this.Id(x => x.DemoVisitPayUserSourceId);
            this.Map(x => x.SourceHsGuarantorId);
            this.Map(x => x.SourceVpGuarantorId);
            this.Map(x => x.SourceDescription);

            this.HasMany(x => x.DemoVisitPayUserClones)
                .KeyColumn(nameof(DemoVisitPayUserSource.DemoVisitPayUserSourceId))
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();
        }

    }
}

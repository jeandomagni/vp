﻿namespace Ivh.Domain.Qat.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;
    using Visit.Mappings;

    public class MutableBaseVisitMap : ClassMap<MutableBaseVisit>
    {
        public MutableBaseVisitMap()
        {
            this.Polymorphism.Explicit();

            this.Schema(Schemas.VisitPay.Base);
            this.Table("Visit");

            this.Id(x => x.VisitId).GeneratedBy.Assigned();

            this.Map(x => x.HsGuarantorId);
            this.Map(x => x.PatientFirstName);
            this.Map(x => x.PatientLastName);
            this.Map(x => x.BillingSystemId);
            this.Map(x => x.SourceSystemKey);
            this.Map(x => x.SourceSystemKeyDisplay);
            this.Map(x => x.AdmitDate);
            this.Map(x => x.DischargeDate);
            this.Map(x => x.VisitDescription);
            this.Map(x => x.IsPatientGuarantor);
            this.Map(x => x.IsPatientMinor);

            this.HasMany(x => x.VisitTransactions)
                .KeyColumn("VisitId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();
        }
    }
}

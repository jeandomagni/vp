﻿namespace Ivh.Domain.Qat.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class SavingsAccountInfoMap : ClassMap<SavingsAccountInfo>
    {
        public SavingsAccountInfoMap()
        {
            this.Table(nameof(SavingsAccountInfo));
            this.Schema("qa");
            this.Id(x => x.SavingsAccountInfoId);
            this.Map(x => x.SavingsAccountStatusId).CustomType<AccountStatusApiModelEnum>();
            this.Map(x => x.SavingsAccountTypeId).CustomType<AccountTypeFlagApiModelEnum>();
            this.Map(x => x.VpGuarantorId);
            this.Map(x => x.AvailableBalance);
            this.Map(x => x.Contributions);
            this.Map(x => x.Distributions);
            this.Map(x => x.Investments);
            this.Map(x => x.Description);

        }
    }
}

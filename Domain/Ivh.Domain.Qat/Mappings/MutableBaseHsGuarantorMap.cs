﻿namespace Ivh.Domain.Qat.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;
    using Guarantor.Mappings;

    public class MutableBaseHsGuarantorMap : ClassMap<MutableBaseHsGuarantor>
    {
        public MutableBaseHsGuarantorMap()
        {
            this.Polymorphism.Explicit();

            this.Schema(Schemas.VisitPay.Base);
            this.Table("HsGuarantor");

            this.Id(x => x.HsGuarantorId).GeneratedBy.Assigned();
            this.Map(x => x.HsBillingSystemId);
            this.Map(x => x.SourceSystemKey);
            this.Map(x => x.FirstName);
            this.Map(x => x.LastName);
            this.Map(x => x.DOB);
            this.Map(x => x.SSN);
            this.Map(x => x.SSN4);
            this.Map(x => x.AddressLine1);
            this.Map(x => x.AddressLine2);
            this.Map(x => x.City);
            this.Map(x => x.StateProvince);
            this.Map(x => x.PostalCode);
            this.Map(x => x.Country);
            this.Map(x => x.DataChangeId);
        }
    }
}

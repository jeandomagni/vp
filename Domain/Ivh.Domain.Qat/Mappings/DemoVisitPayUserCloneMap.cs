﻿namespace Ivh.Domain.Qat.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class DemoVisitPayUserCloneMap:ClassMap<DemoVisitPayUserClone>
    {
        public DemoVisitPayUserCloneMap()
        {
            this.Table(nameof(DemoVisitPayUserClone));
            this.Schema("qa");
            this.Id(x => x.DemoVisitPayUserCloneId);
            this.Map(x => x.CloneVisitPayUserName);
            this.Map(x => x.IsHidden);
            this.References(x => x.DemoVisitPayUserSource).Column(nameof(DemoVisitPayUserSource.DemoVisitPayUserSourceId));
        }
    }
}
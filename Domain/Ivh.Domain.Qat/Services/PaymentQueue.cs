﻿namespace Ivh.Domain.Qat.Services
{
    using System;
    using System.Collections.Concurrent;
    using Entities;

    internal class PaymentQueue : ConcurrentDictionary<Guid, PpiRequest>
    {
        public static readonly PaymentQueue Instance = new PaymentQueue();

        private PaymentQueue()
        {
        }
    }
}
﻿namespace Ivh.Domain.Qat.Services
{
    using System;
    using HospitalData.Visit.Entities;
    using HospitalData.Visit.Interfaces;
    using Interfaces;
    using AutoMapper;
    using Common.VisitPay.Messages.HospitalData.Dtos;

    public class QAInsurancePlanService : IQAInsurancePlanService
    {
        private readonly Lazy<IInsurancePlanRepository> _insurancePlanRepository;

        public QAInsurancePlanService(Lazy<IInsurancePlanRepository> insurancePlanRepository)
        {
            this._insurancePlanRepository = insurancePlanRepository;
        }
        public void AddInsurancePlan(InsurancePlanDto insurancePlanDto)
        {
            InsurancePlan insurancePlan = Mapper.Map<InsurancePlan>(insurancePlanDto);
            this._insurancePlanRepository.Value.Insert(insurancePlan);
        }
    }
}
﻿namespace Ivh.Domain.Qat.Services
{
    using System;
    using System.IO;
    using System.Linq;
    using Interfaces;
    using Settings.Entities;
    using Settings.Interfaces;

    public class QaSettingsService : IQaSettingsService
    {
        private readonly Lazy<IClientSettingRepository> _clientSettingRepository;

        public QaSettingsService(Lazy<IClientSettingRepository> clientSettingRepository)
        {
            this._clientSettingRepository = clientSettingRepository;
        }
        
        public bool ChangeClientLogo(string filename, byte[] logo)
        {
            ClientSetting clientSetting = this._clientSettingRepository.Value.GetQueryable().FirstOrDefault(x => x.ClientSettingKey == "/theme.client-logo-path");
            if (clientSetting != null && clientSetting.EditInUI)
            {
                string extension = Path.GetExtension(filename)?.Replace(".", string.Empty);
                if (string.IsNullOrWhiteSpace(extension))
                {
                    return false;
                }
                
                clientSetting.ClientSettingValueOverride = $"url(data:image/{extension};base64,{Convert.ToBase64String(logo)})";
                this._clientSettingRepository.Value.InsertOrUpdate(clientSetting);

                return true;
            }

            return false;
        }
    }
}
﻿using System.Collections.Generic;
using System.Text;

namespace Ivh.Domain.Qat.Services
{
    using Settings.Interfaces;
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.Cache;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Interfaces;
    using Logging.Interfaces;

    public class CommunicationInterceptorService : DomainService, ICommunicationInterceptorService
    {
        private const int CacheDuration = 60 * 60 * 24;
        private static readonly SemaphoreSlim Semaphore = new SemaphoreSlim(1, 1);

        private bool IsValidEnvironment
        {
            get { return new[] { IvhEnvironmentTypeEnum.Qa, IvhEnvironmentTypeEnum.Dev }.Contains(this.ApplicationSettingsService.Value.EnvironmentType.Value); }
        }

        public CommunicationInterceptorService(Lazy<IDomainServiceCommonService> serviceCommonService) : base(serviceCommonService)
        {
        }

        public bool IsActive()
        {
            if (!this.IsValidEnvironment)
            {
                return false;
            }

            string uriString = null;

            Task.Run(async () =>
            {
                await Semaphore.WaitAsync();

                uriString = await this.Cache.Value.GetStringAsync(CommunicationInterceptorCacheKeys.SmsOverrideUrl);

                Semaphore.Release();
            }).Wait();

            Uri uri;

            bool parsed = uriString.TryToUri(UriKind.Absolute, true, out uri);

            this.LoggingService.Value.Info(() => $"MockCommunicationProcessor:IsActive:{uriString},{parsed},{(uri == null ? "null" : uri.AbsoluteUri)}");

            return parsed;
        }

        public void Activate(string uri)
        {
            if (!this.IsValidEnvironment)
            {
                return;
            }

            bool isActivated = false;

            Task.Run(async () =>
            {
                await Semaphore.WaitAsync();

                if (Uri.IsWellFormedUriString(uri, UriKind.Absolute))
                {
                    isActivated = await this.Cache.Value.SetStringAsync(CommunicationInterceptorCacheKeys.SmsOverrideUrl, new Uri(uri).AbsoluteUri, CacheDuration);
                }

                Semaphore.Release();
            }).Wait();

            this.LoggingService.Value.Info(() => $"MockCommunicationProcessor:Activate:{isActivated},{uri},{new Uri(uri).AbsoluteUri}");

            this.PublishIsActivated(isActivated);
        }

        public void Deactivate()
        {
            if (!this.IsValidEnvironment)
            {
                return;
            }

            bool isDeactivated = false;

            Task.Run(async () =>
            {
                await Semaphore.WaitAsync();

                isDeactivated = await this.Cache.Value.RemoveAsync(CommunicationInterceptorCacheKeys.SmsOverrideUrl);

                Semaphore.Release();
            }).Wait();

            this.LoggingService.Value.Info(() => $"MockCommunicatioProcessor:Deactivate:{isDeactivated}");

            this.PublishIsActivated(!isDeactivated);
        }

        private void PublishIsActivated(bool isActivated)
        {
            this.Cache.Value.PublishMessage(CommunicationInterceptorCacheKeys.Messaging.IsActivatedChannel, isActivated.ToString());
        }
    }
}

﻿namespace Ivh.Domain.Qat.Services
{
    using System;
    using Base.Interfaces;
    using Base.Services;
    using Common.VisitPay.Enums;
    using Entities;
    using Guarantor.Entities;
    using Guarantor.Interfaces;
    using HospitalData.HsGuarantor.Entities;
    using HospitalData.HsGuarantor.Interfaces;
    using HospitalData.Visit.Entities;
    using HospitalData.Visit.Interfaces;
    using Interfaces;

    public class AppBaseDataService : DomainService, IAppBaseDataService
    {
        private readonly Lazy<IMutableBaseVisitRepository> _visitRepository;
        private readonly Lazy<IMutableBaseHsGuarantorRepository> _hsGuarantorRepository;
        private readonly Lazy<IHsGuarantorRepository> _cdiHsGuarantorRepository;
        private readonly Lazy<IVisitRepository> _cdiVisitRepository;
        private readonly Lazy<IVpGuarantorScoreRepository> _vpGuarantorScoreRepository;

        public AppBaseDataService(
            Lazy<IMutableBaseVisitRepository> visitRepository,
            Lazy<IMutableBaseHsGuarantorRepository> hsGuarantorRepository,
            Lazy<IHsGuarantorRepository> cdiHsGuarantorRepository,
            Lazy<IVisitRepository> cdiVisitRepository,
            Lazy<IVpGuarantorScoreRepository> vpGuarantorScoreRepository,
            Lazy<IDomainServiceCommonService> serviceCommonService
        ) : base(serviceCommonService)
        {
            this._visitRepository = visitRepository;
            this._hsGuarantorRepository = hsGuarantorRepository;
            this._cdiHsGuarantorRepository = cdiHsGuarantorRepository;
            this._cdiVisitRepository = cdiVisitRepository;
            this._vpGuarantorScoreRepository = vpGuarantorScoreRepository;
        }

        public void AddHsGuarantor(string hsGuarantorSourceSystemKey, int billingSystemId)
        {
            HsGuarantor cdiGuarantor = this._cdiHsGuarantorRepository.Value.GetHsGuarantor(billingSystemId, hsGuarantorSourceSystemKey);
            if(cdiGuarantor == null)
            {
                return;
            }

            MutableBaseHsGuarantor guarantor = AutoMapper.Mapper.Map<MutableBaseHsGuarantor>(cdiGuarantor);

            this._hsGuarantorRepository.Value.InsertOrUpdate(guarantor);
        }

        public void AddVisit(string visitSourceSystemKey, int billingSystemId)
        {
            Visit cdiVisit = this._cdiVisitRepository.Value.GetVisit(billingSystemId, visitSourceSystemKey);
            if (cdiVisit == null)
            {
                return;
            }

            MutableBaseVisit visit = AutoMapper.Mapper.Map<MutableBaseVisit>(cdiVisit);

            this._visitRepository.Value.InsertOrUpdate(visit);
        }

        // for demo accounts
        public void AddGuarantorScoreToVpApp(int vpGuarantorId, int score, ScoreTypeEnum scoreType)
        {
            VpGuarantorScore vpGuarantorScore = new VpGuarantorScore
            {
                VpGuarantorId = vpGuarantorId,
                Score = score,
                ScoreTypeId = (int) scoreType,
                ScoreCreationDate = DateTime.UtcNow.AddYears(1) // this is to ensure it's the most recent score since this is just for demo accounts
            };
            this._vpGuarantorScoreRepository.Value.InsertOrUpdate(vpGuarantorScore);
        }
    }
}

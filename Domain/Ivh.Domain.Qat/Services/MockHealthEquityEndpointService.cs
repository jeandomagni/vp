﻿namespace Ivh.Domain.Qat.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Application.Qat.Common.Dtos;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;
    public class MockHealthEquityEndpointService : IMockHealthEquityEndpointService
    {
        private readonly Lazy<ISavingsAccountInfoRepository> _savingsAccountInfoRepository;

        public MockHealthEquityEndpointService(Lazy<ISavingsAccountInfoRepository> savingsAccountInfoRepository)
        {
            this._savingsAccountInfoRepository = savingsAccountInfoRepository;
        }

        public IList<SavingsAccountInfoDto> GetSavingsAccountInfos(int vpguarantorId, IList<AccountStatusApiModelEnum> statuses, IList<AccountTypeFlagApiModelEnum> types)
        {
            IList<SavingsAccountInfo> savingsAccountInfos = this._savingsAccountInfoRepository.Value.GetSavingsAccountInfos(vpguarantorId, statuses, types);

            return savingsAccountInfos.Select(x => new SavingsAccountInfoDto
            {
                AccountId = x.VpGuarantorId.ToString(),
                AvailableBalance = x.AvailableBalance,
                Contributions = x.Contributions,
                Description = x.Description,
                Distributions = x.Distributions,
                Investments = x.Investments,
                Status = x.SavingsAccountStatusId,
                Type = x.SavingsAccountTypeId
            }).ToList();

        }
    }
}

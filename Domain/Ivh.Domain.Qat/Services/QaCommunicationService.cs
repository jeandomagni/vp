﻿namespace Ivh.Domain.Qat.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Email.Entities;
    using Interfaces;

    public class QaCommunicationService : DomainService, IQaCommunicationService
    {
        private readonly Lazy<IQaCommunicationRepository> _communicationRepository;

        public QaCommunicationService(Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IQaCommunicationRepository>communicationRepository
        ) : base(serviceCommonService)
        {
            this._communicationRepository = communicationRepository;
        }

        public IList<Communication> GetTextToPayPaymentOptionCommunications()
        {
            List<Communication> communitcations = this._communicationRepository.Value.GetQueryable()
                .Where(x => x.CommunicationType.CommunicationTypeId == CommunicationTypeEnum.TextToPayPaymentOption)
                .ToList();

            //return the latest TextToPayPaymentOption communication for each user
            IEnumerable<Communication> result = communitcations.GroupBy(x => x.SentToUserId, (key, g) =>
                    g.OrderByDescending(e => e.CreatedDate)
                    .First()
            );

            return result.ToList();

        }

        public Communication GetTextToPayPaymentOptionCommunication(int vpUserId)
        {
            Communication communitcation = this._communicationRepository.Value.GetQueryable()
                .Where(x => x.SentToUserId == vpUserId)
                .Where(x => x.CommunicationType.CommunicationTypeId == CommunicationTypeEnum.TextToPayPaymentOption)
                .OrderByDescending(e => e.CreatedDate)
                .FirstOrDefault();

            return communitcation;

        }
    }
}
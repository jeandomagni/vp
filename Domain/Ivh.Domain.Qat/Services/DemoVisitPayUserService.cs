﻿namespace Ivh.Domain.Qat.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Application.Qat.Common;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Utilities.Helpers;
    using Entities;
    using Guarantor.Entities;
    using Guarantor.Interfaces;
    using Interfaces;
    using User.Entities;
    using User.Services;
    using Visit.Interfaces;

    public class DemoVisitPayUserService : DomainService, IDemoVisitPayUserService
    {
        private readonly Lazy<IDemoVisitPayUserRepository> _demoVisitPayUserRepository;
        private readonly Lazy<IQaVisitPayUserRepository> _qaVisitPayUserRepository;
        private readonly Lazy<IGuarantorRepository> _guarantorRepository;
        private readonly Lazy<VisitPayUserService> _visitPayUserService;
        private readonly Lazy<IVisitPayUserIssueResultRepository> _visitPayUserIssueResultRepository;

        public DemoVisitPayUserService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IQaVisitPayUserRepository> qaVisitPayUserRepository,
            Lazy<IGuarantorRepository> guarantorRepository,
            Lazy<VisitPayUserService> visitPayUserService,
            Lazy<IVisitPayUserIssueResultRepository> visitPayUserIssueResultRepository,
            Lazy<IDemoVisitPayUserRepository> demoVisitPayUserRepository) : base(serviceCommonService)
        {
            this._qaVisitPayUserRepository = qaVisitPayUserRepository;
            this._guarantorRepository = guarantorRepository;
            this._visitPayUserService = visitPayUserService;
            this._visitPayUserIssueResultRepository = visitPayUserIssueResultRepository;
            this._demoVisitPayUserRepository = demoVisitPayUserRepository;
        }
        
        public Guarantor GetGuarantorFromUser(string username)
        {
            List<int> visitPayUserIds = this._qaVisitPayUserRepository.Value.GetQueryable().Where(x => x.UserName == username).Select(x => x.VisitPayUserId).ToList();
            if (visitPayUserIds.Count != 1)
            {
                return null;
            }

            int visitPayUserId = visitPayUserIds.First();
            
            List<Guarantor> guarantors = this._guarantorRepository.Value.GetQueryable().Where(x => x.User.VisitPayUserId == visitPayUserId).ToList();
            if (guarantors.Count != 1)
            {
                return null;
            }

            return guarantors.First();
        }
        
        public void ClearAndRename(string username)
        {
            Guarantor guarantor = this.GetGuarantorFromUser(username);
            if (guarantor == null)
            {
                // don't need to clear rename if not found
                return;
            }

            // clear
            this._visitPayUserIssueResultRepository.Value.ClearGuarantorData(guarantor.VpGuarantorId);

            // rename
            VisitPayUser user = guarantor.User;
            user.UserName = Guid.NewGuid().ToString();
            user.FirstName = RandomStringGeneratorUtility.GetRandomString(10);
            user.LastName = RandomStringGeneratorUtility.GetRandomString(10);
            user.Email = $"{RandomStringGeneratorUtility.GetRandomString(10)}@{RandomStringGeneratorUtility.GetRandomString(10)}.com";
            this._qaVisitPayUserRepository.Value.Update(user);
        }

        public void PostClone(string sourceUserName, string newUserName, string suffix)
        {
            VisitPayUser visitPayUser = this._qaVisitPayUserRepository.Value.GetQueryable().FirstOrDefault(x => x.UserName == newUserName);
            if (visitPayUser == null)
            {
                return;
            }
            
            IPersonaCredential credential = null;
            if (PersonaConstants.TrainingAccountUsernames.Any(newUserName.StartsWith))
            {
                credential = new TrainingPersonaCredential(sourceUserName, visitPayUser.FirstName, visitPayUser.LastName, suffix);

                // update last name for training accounts
                visitPayUser.LastName = $"{credential.LastName}{suffix}";
            }
            else if (PersonaConstants.DemoAccountUsernames.Any(newUserName.StartsWith))
            {
                credential = new DemoPersonaCredential(sourceUserName, visitPayUser.FirstName, visitPayUser.LastName, suffix);
            }

            if (credential == null)
            {
                return;
            }

            // always update email
            visitPayUser.Email = credential.Email;

            // save user
            this._qaVisitPayUserRepository.Value.InsertOrUpdate(visitPayUser);

            // set password
            this._visitPayUserService.Value.ResetPassword(visitPayUser.VisitPayUserId.ToString(), credential.Password);
        }

        public DemoVisitPayUserClone GetDemoVisitPayUserClone(string visitPayUserName)
        {
            return this._demoVisitPayUserRepository.Value.GetQueryable().FirstOrDefault(x => x.CloneVisitPayUserName == visitPayUserName);
        }
        
        public void RenameVisitPayUser(string visitPayUserName, string newUserName)
        {
            VisitPayUser visitPayUser = this._qaVisitPayUserRepository.Value.GetQueryable().FirstOrDefault(x => x.UserName == visitPayUserName);
            if (visitPayUser == null)
            {
                //no need to update
                return;
            }

            visitPayUser.UserName = newUserName;
            this._qaVisitPayUserRepository.Value.InsertOrUpdate(visitPayUser);
        }
        
        public VisitPayUser GetVisitPayUserFromUserName(string visitPayUserName)
        {
            VisitPayUser visitPayUser = this._qaVisitPayUserRepository.Value.GetQueryable().SingleOrDefault(x => x.UserName == visitPayUserName);
            return visitPayUser;
        }
        
        public List<DemoVisitPayUserClone> GetAllDemoVisitPayUserClones()
        {
            return this._demoVisitPayUserRepository.Value.GetQueryable().Where(x => !x.IsHidden).ToList();
        }
    }

    public abstract class PersonaCredential
    {
        private readonly string _username;
        protected readonly string Suffix;

        protected PersonaCredential(string username, string firstName, string lastName, string suffix)
        {
            this._username = username;
            this.Suffix = (suffix ?? string.Empty).Trim();

            this.FirstName = firstName;
            this.LastName = lastName;
        }
        
        public string FirstName { get; }

        public string LastName { get; }
        
        public string Email => $"{this.Username}@{this.EmailDomain}".ToLower();
        
        public string Username => $"{this._username}{this.Suffix}".ToLower();

        protected virtual string EmailDomain => "visitpay.com";
    }

    public class TrainingPersonaCredential : PersonaCredential, IPersonaCredential
    {
        private const string TrainingPassword = "Training.password1";

        public TrainingPersonaCredential(string username, string firstName, string lastName, string suffix) : base(username, firstName, lastName, suffix)
        {
        }

        public string Password => TrainingPassword;
    }

    public class DemoPersonaCredential : PersonaCredential, IPersonaCredential
    {
        public DemoPersonaCredential(string username, string firstName, string lastName, string suffix) : base(username, firstName, lastName, suffix)
        {
            this.Password = $"{this.FirstName}.{this.LastName}{this.Suffix}0!";
        }

        public string Password { get; private set; }

        public void OverridePassword(string password)
        {
            this.Password = password;
        }
    }

    public interface IPersonaCredential
    { 
        string FirstName { get; }
        string LastName { get; }
        string Email { get; }
        string Username { get; }
        string Password { get; }
    }
}
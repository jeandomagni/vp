﻿namespace Ivh.Domain.Qat.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Common.VisitPay.Enums;
    using FinanceManagement.Payment.Entities;
    using FinanceManagement.Payment.Interfaces;
    using Interfaces;

    public class QaPaymentService : DomainService, IQaPaymentService
    {
        private readonly Lazy<IQaPaymentRepository> _paymentRepository;
        private readonly Lazy<IPaymentAllocationRepository> _paymentAllocationRepository;

        public QaPaymentService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IQaPaymentRepository> paymentRepository,
            Lazy<IPaymentAllocationRepository> paymentAllocationRepository
                ) : base(serviceCommonService)
        {
            this._paymentRepository = paymentRepository;
            this._paymentAllocationRepository = paymentAllocationRepository;
        }

        public List<Payment> GetTextToPayPayments(int guarantorId)
        {

            List<Payment> payments = this._paymentRepository.Value.GetQueryable()
                .Where(x => x.Guarantor.VpGuarantorId == guarantorId).ToList();
            
            //Get any payment that is pending response or began as pending response
            payments = payments.Where(x => x.IsTextToPayType())
                                        .ToList();

            return payments; 
        }

        public IList<PaymentAllocation> GetUnclearedPaymentAllocationsForVisits(IList<int> visitIds)
        {
            IList<PaymentAllocation> paymentAllocations = this._paymentAllocationRepository.Value.GetAllPaymentAllocationsForVisits(visitIds, PaymentBatchStatusEnum.Pending);

            return paymentAllocations;
        }
    }
}
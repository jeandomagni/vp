﻿namespace Ivh.Domain.Qat.Services
{
    using System;
    using System.Collections.Generic;
    using Base.Interfaces;
    using Base.Services;
    using Common.VisitPay.Constants;
    using Interfaces;
    using Visit.Entities;
    using Visit.Interfaces;

    public class QaVisitTransactionService : DomainService, IQaVisitTransactionService
    {
        private readonly Lazy<IVisitService> _visitService;
        private readonly Lazy<IVisitTransactionWriteService> _visitTransactionService;

        public QaVisitTransactionService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IVisitTransactionWriteService> visitTransactionService,
            Lazy<IVisitService> visitService) : base(serviceCommonService)
        {
            this._visitService = visitService;
            this._visitTransactionService = visitTransactionService;
        }

        [Obsolete(ObsoleteDescription.VisitTransactions)]
        public void AdjustVisitBalanceDates(string systemSourceKey, int billingSystemId, int numberOfDays)
        {
            Visit visit = this._visitService.Value.GetVisitBySystemSourceKey(systemSourceKey, billingSystemId);
            IEnumerable<VisitTransaction> visitTransactions = this._visitTransactionService.Value.GetVisitTransactions(visit.VpGuarantorId, visit.VisitId);
            visit.HsCurrentBalanceChangedDate = visit.HsCurrentBalanceChangedDate.Value.AddDays(numberOfDays);
            foreach (VisitTransaction transaction in visitTransactions)
            {
                if (transaction.SourceSystemKeySetDate.HasValue)
                {
                    transaction.SourceSystemKeySetDate = transaction.SourceSystemKeySetDate.Value.AddDays(numberOfDays);
                    this._visitTransactionService.Value.SaveVisitTransaction(visit, transaction);
                }
            }
            this._visitService.Value.SaveVisit(visit);
        }
    }
}

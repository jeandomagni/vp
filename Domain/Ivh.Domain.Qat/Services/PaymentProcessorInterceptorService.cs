﻿namespace Ivh.Domain.Qat.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Application.Qat.Common.Dtos;
    using Application.Qat.Common.Enums;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Entities;
    using FinanceManagement.Entities;
    using FinanceManagement.Interfaces;
    using Interfaces;
    using Logging.Interfaces;
    using Settings.Interfaces;

    public class PaymentProcessorInterceptorService : DomainService, IPaymentProcessorInterceptorService
    {
        private const int CacheDuration = 60 * 60 * 24;
        private static readonly SemaphoreSlim Semaphore = new SemaphoreSlim(1, 1);
        private readonly Lazy<IPaymentMethodsService> _paymentMethodsService;
        private readonly Lazy<IPaymentConfigurationService> _paymentConfigurationService;

        private bool IsValidEnvironment => new[] { IvhEnvironmentTypeEnum.Qa, IvhEnvironmentTypeEnum.Dev }.Contains(this.ApplicationSettingsService.Value.EnvironmentType.Value);

        public PaymentProcessorInterceptorService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IPaymentMethodsService> paymentMethodsService,
            Lazy<IPaymentConfigurationService> paymentConfigurationService) : base(serviceCommonService)
        {
            this._paymentMethodsService = paymentMethodsService;
            this._paymentConfigurationService = paymentConfigurationService;
        }

        #region activated

        public bool IsActive()
        {
            if (!this.IsValidEnvironment)
            {
                return false;
            }

            string uriString = null;

            Task.Run(async () =>
            {
                await Semaphore.WaitAsync();

                uriString = await this.Cache.Value.GetStringAsync(PaymentInterceptorCacheKeys.OverrideUrl);

                Semaphore.Release();
            }).Wait();

            Uri uri;

            bool parsed = uriString.TryToUri(UriKind.Absolute, true, out uri);

            this.LoggingService.Value.Info(() => $"MockPaymentProcessor:IsActive:{uriString},{parsed},{(uri == null ? "null" : uri.AbsoluteUri)}");

            return parsed;
        }

        public void Activate(string uriString)
        {
            if (!this.IsValidEnvironment)
            {
                return;
            }

            PaymentQueue.Instance.Clear();
            bool isActivated = false;

            Task.Run(async () =>
            {
                await Semaphore.WaitAsync();

                if (Uri.IsWellFormedUriString(uriString, UriKind.Absolute))
                {
                    isActivated = await this.Cache.Value.SetStringAsync(PaymentInterceptorCacheKeys.OverrideUrl, new Uri(uriString).AbsoluteUri, CacheDuration);
                }

                Semaphore.Release();
            }).Wait();

            this.LoggingService.Value.Info(() => $"MockPaymentProcessor:Activate:{isActivated},{uriString},{new Uri(uriString).AbsoluteUri}");

            this.PublishIsActivated(isActivated);
        }

        public void Deactivate()
        {
            if (!this.IsValidEnvironment)
            {
                return;
            }

            PaymentQueue.Instance.Clear();
            bool isDeactivated = false;

            Task.Run(async () =>
            {
                await Semaphore.WaitAsync();

                isDeactivated = await this.Cache.Value.RemoveAsync(PaymentInterceptorCacheKeys.OverrideUrl);

                Semaphore.Release();
            }).Wait();

            this.LoggingService.Value.Info(() => $"MockPaymentProcessor:Deactivate:{isDeactivated}");

            this.PublishIsActivated(!isDeactivated);
        }

        #endregion

        #region default action

        public bool DefaultSuccessful
        {
            get => this.GetDefaultSuccessful() ?? (this.DefaultSuccessful = true);
            set => this.Cache.Value.SetStringAsync(PaymentInterceptorCacheKeys.DefaultSuccessful, value.ToString(), CacheDuration).Wait();
        }

        private bool? GetDefaultSuccessful()
        {
            string str = this.Cache.Value.GetStringAsync(PaymentInterceptorCacheKeys.DefaultSuccessful).Result;
            if (string.IsNullOrEmpty(str))
            {
                return null;
            }

            bool b;
            if (!bool.TryParse(str, out b))
            {
                return null;
            }

            return b;
        }

        #endregion

        #region delay

        public int DelayMs
        {
            get => this.GetDelayMs() ?? (this.DelayMs = 30*1000);
            set => this.Cache.Value.SetStringAsync(PaymentInterceptorCacheKeys.DelayMs, value.ToString(), CacheDuration).Wait();
        }

        private int? GetDelayMs()
        {
            string str = this.Cache.Value.GetStringAsync(PaymentInterceptorCacheKeys.DelayMs).Result;
            int delayMs;
            int.TryParse(str, out delayMs);

            return delayMs > 0 ? delayMs : (int?)null;
        }

        #endregion

        public async Task<PpiResponse> QueuePaymentRequestAsync(Guid requestGuid, IDictionary<string, string> request)
        {
            PpiRequest ppiRequest = ParseRequest(request, requestGuid);

            switch (ppiRequest.Action.ToUpper())
            {
                case "SALE":
                    this.AddPaymentMethod(ppiRequest);
                    break;
                case "STORE":
                case "VERIFY":
                    this.SetAccountDetails(ppiRequest);
                    this.SetAvsCodes(ppiRequest);
                    break;
            }

            if (!PaymentQueue.Instance.TryAdd(requestGuid, ppiRequest))
            {
                return null;
            }

            this.PublishPaymentQueuedMessage(requestGuid);

            // wait
            await Task.WhenAny(ppiRequest.GetResponseTypeAsync(), Task.Delay(this.DelayMs)).ConfigureAwait(false);

            if (!ppiRequest.ResponseType.HasValue)
            {
                // time elapsed
                PpiResponseTypeEnum defaultType = PpiResponseTypeEnum.Approved;
                if (!this.DefaultSuccessful)
                {
                    defaultType = PpiResponseTypeEnum.Decline;
                }

                char? defaultAvsCode = null;

                // default to an accepted AVS code
                if (!string.IsNullOrEmpty(ppiRequest.Action) && new[] {"STORE", "VERIFY"}.Contains(ppiRequest.Action.ToUpper()))
                {
                    defaultAvsCode = 'Y';
                }
            
                ppiRequest.SetResponseType(defaultType, defaultAvsCode);
            }

            this.PublishPaymentCompleteMessage(requestGuid);

            return CreateResponse(ppiRequest);
        }

        public PpiRequest GetPaymentRequest(Guid requestGuid)
        {
            PpiRequest request;

            return PaymentQueue.Instance.TryGetValue(requestGuid, out request) ? request : null;
        }

        public PpiRequest CompletePaymentRequest(Guid requestGuid, PpiResponseTypeEnum responseType, char? responseAvsCode)
        {
            PpiRequest request = this.GetPaymentRequest(requestGuid);
            if (request != null)
            {
                request.SetResponseType(responseType, responseAvsCode);
            }

            return request;
        }

        public PpiRequest SetAchAction(Guid requestGuid, AchFileTypeEnum fileType, DateTime processDate, AchReturnCodeEnum? returnCode, decimal? returnAmount)
        {
            PpiRequest request = this.GetPaymentRequest(requestGuid);
            if (request != null)
            {
                request.SetAchAction(fileType, processDate, returnCode, returnAmount);
            }

            return request;
        }

        public IList<PpiRequest> QueryRequestQueue(PpiRequestFilterDto ppiRequestFilter)
        {
            List<PpiRequest> queue = PaymentQueue.Instance.Select(x => x.Value).ToList();

            if (ppiRequestFilter.AchProcessed.HasValue)
            {
                queue = queue.Where(x => x.AchProcessed == ppiRequestFilter.AchProcessed.Value).ToList();
            }

            if (ppiRequestFilter.AchProcessDate.HasValue)
            {
                queue = queue.Where(x => x.AchProcessDate.HasValue && x.AchProcessDate.Value.Date == ppiRequestFilter.AchProcessDate).ToList();
            }

            if (ppiRequestFilter.RequestGuids != null && ppiRequestFilter.RequestGuids.Any())
            {
                queue = queue.Where(x => ppiRequestFilter.RequestGuids.Contains(x.RequestGuid)).ToList();
            }

            if (ppiRequestFilter.IsSuccess.HasValue)
            {
                queue = queue.Where(x => x.IsSuccess == ppiRequestFilter.IsSuccess.Value).ToList();
            }

            if (ppiRequestFilter.IsAch.HasValue)
            {
                queue = queue.Where(x => x.VpPaymentMethodType.HasValue && EnumHelper<PaymentMethodTypeEnum>.GetValues(PaymentMethodTypeEnumCategory.Ach).ToList().Contains(x.VpPaymentMethodType.Value)).ToList();
            }

            return queue.OrderByDescending(x => x.InsertDate).ToList();
        }

        private static PpiRequest ParseRequest(IDictionary<string, string> request, Guid requestGuid)
        {
            Func<string, string> getValue = k =>
            {
                string v;
                request.TryGetValue(k, out v);
                return v ?? string.Empty;
            };

            return new PpiRequest
            {
                RequestGuid = requestGuid,
                InsertDate = DateTime.UtcNow,
                Action = getValue("action"),

                // payment request
                BillingId = getValue("billingid"),
                Amount = getValue("amount"),
                CustomField2 = getValue("customfield2"),
                CustomField3 = getValue("customfield3"),
                CustomField4 = getValue("customfield4"),
                CustomField5 = getValue("customfield5"),
                CustomField6 = getValue("customfield6"),
                CustomField9 = getValue("customfield7"),
                CustomField12 = getValue("customfield12"),

                // store, verify
                Name = getValue("name"),
                // ach
                Media = getValue("media"),
                Account = getValue("account"),
                Routing = getValue("routing"),
                // cc
                Cc = getValue("cc"),
                Exp = getValue("exp"),
                Address1 = getValue("address1"),
                City = getValue("city"),
                State = getValue("state"),
                Zip = getValue("zip"),
                Cvv = getValue("cvv")
            };
        }

        private static PpiResponse CreateResponse(PpiRequest request)
        {
            PpiResponse response;

            switch (request.ResponseType.GetValueOrDefault(PpiResponseTypeEnum.Approved))
            {
                case PpiResponseTypeEnum.Decline:
                    response = Decline(request);
                    break;
                case PpiResponseTypeEnum.DeclineCall:
                    response = DeclineCall(request);
                    break;
                case PpiResponseTypeEnum.DeclineCardError:
                    response = DeclineCardError(request);
                    break;
                case PpiResponseTypeEnum.ProcessorUnreacheable404:
                    response = ProcessorUnreachable(request, 404);
                    break;
                case PpiResponseTypeEnum.ProcessorUnreacheable500:
                    response = ProcessorUnreachable(request, 500);
                    break;
                case PpiResponseTypeEnum.NoSuchBillingId:
                    response = NoSuchBillingId(request);
                    break;
                case PpiResponseTypeEnum.MissingData:
                    response = MissingData(request);
                    break;
                case PpiResponseTypeEnum.BadFormat:
                    response = BadFormat(request);
                    break;
                case PpiResponseTypeEnum.LinkFailure:
                    response = LinkFailure(request);
                    break;
                case PpiResponseTypeEnum.CallToProcessorFailed:
                    response = CallToProcessorFailed(request);
                    break;
                case PpiResponseTypeEnum.Accepted:
                    response = Accepted(request);
                    break;
                case PpiResponseTypeEnum.Rejected102:
                    response = Rejected102(request);
                    break;
                case PpiResponseTypeEnum.Rejected203:
                    response = Rejected203(request);
                    break;
                case PpiResponseTypeEnum.FailToProcess:
                    response = FailToProcess(request);
                    break;
                case PpiResponseTypeEnum.Approved:
                default:
                    response = Approved(request);
                    break;
            }

            if (new[] { "STORE", "VERIFY" }.Contains(request.Action.ToUpper()))
            {
                if (EnumHelper<PpiResponseTypeEnum>.ContainsIntersect(request.ResponseType.GetValueOrDefault(PpiResponseTypeEnum.Approved), PpiResponseTypeEnumCategory.Success, PpiResponseTypeEnumCategory.Store))
                {
                    response.BillingId = RandomSourceSystemKeyGenerator.New("QAT-NNNNNN");
                }
            }

            request.Response = response;

            return response;
        }

        private void AddPaymentMethod(PpiRequest request)
        {
            if (string.IsNullOrEmpty(request.BillingId))
            {
                return;
            }

            PaymentMethod paymentMethod = this._paymentMethodsService.Value.GetByBillingId(request.BillingId);
            if (paymentMethod == null)
            {
                return;
            }

            request.VpPaymentMethodType = paymentMethod.PaymentMethodType;
            request.VpAccountName = paymentMethod.NameOnCard ?? paymentMethod.FirstNameLastName;
            request.VpLastFour = paymentMethod.LastFour;
        }

        private void SetAccountDetails(PpiRequest request)
        {
            request.VpAccountName = request.Name;

            if (!string.IsNullOrEmpty(request.Cc))
            {
                request.VpPaymentMethodType = this.GetCardType(request.Cc);
                request.VpLastFour = request.Cc.Last(4);
            }
            else if (!string.IsNullOrEmpty(request.Account))
            {
                request.VpPaymentMethodType = PaymentMethodTypeEnum.AchChecking; // could also be savings but for our purposes just needs to be an ACH type for now.
                request.VpLastFour = request.Account.Last(4);
            }
        }

        private void SetAvsCodes(PpiRequest request)
        {
            if (!request.VpPaymentMethodType.HasValue)
            {
                return;
            }

            if (EnumHelper<PaymentMethodTypeEnum>.Contains(request.VpPaymentMethodType.Value, PaymentMethodTypeEnumCategory.Card))
            {
                request.SetAcceptedAvsCodes(this._paymentConfigurationService.Value.AcceptedAvsCodes);
            }
        }

        private PaymentMethodTypeEnum? GetCardType(string cardNumber)
        {
            if (string.IsNullOrEmpty(cardNumber))
            {
                return null;
            }

            if (cardNumber.StartsWith("34") || cardNumber.StartsWith("37"))
            {
                return PaymentMethodTypeEnum.AmericanExpress;
            }

            if (cardNumber.StartsWith("6"))
            {
                return PaymentMethodTypeEnum.Discover;
            }

            if (cardNumber.StartsWith("4"))
            {
                return PaymentMethodTypeEnum.Visa;
            }

            if (cardNumber.Length >= 2)
            {
                int first2;
                if (int.TryParse(cardNumber.Substring(0, 2), out first2))
                {
                    if (first2 >= 51 && first2 <= 57)
                    {
                        return PaymentMethodTypeEnum.Mastercard;
                    }
                }
            }

            return null;
        }

        private void PublishIsActivated(bool isActivated)
        {
            this.Cache.Value.PublishMessage(PaymentInterceptorCacheKeys.Messaging.IsActivatedChannel, isActivated.ToString());
        }

        private void PublishPaymentCompleteMessage(Guid requestGuid)
        {
            this.Cache.Value.PublishMessage(PaymentInterceptorCacheKeys.Messaging.PaymentComplete, requestGuid.ToString());
        }

        private void PublishPaymentQueuedMessage(Guid requestGuid)
        {
            this.Cache.Value.PublishMessage(PaymentInterceptorCacheKeys.Messaging.PaymentQueued, requestGuid.ToString());
        }

        #region response types

        private static PpiResponse Approved(PpiRequest request)
        {
            return new PpiResponse
            {
                AuthCode = RandomSourceSystemKeyGenerator.New("NNNNNN"),
                TransactionId = RandomSourceSystemKeyGenerator.New("999-NNNNNNNNNN"),
                ResponseCode = "00",
                ResponseCodeDescriptor = "Approved and completed",
                Status = "approved",
                Avs = request.ResponseAvsCode.HasValue ? request.ResponseAvsCode.Value.ToString() : string.Empty
            };
        }

        private static PpiResponse Accepted(PpiRequest request)
        {
            return new PpiResponse
            {
                TransactionId = RandomSourceSystemKeyGenerator.New("999-NNNNNNNNNN"),
                ResponseCode = "000",
                Status = "accepted"
            };
        }

        private static PpiResponse DeclineBase(PpiRequest request)
        {
            return new PpiResponse
            {
                TransactionId = RandomSourceSystemKeyGenerator.New("999-NNNNNNNNNN"),
                Status = "decline",
                Avs = request.ResponseAvsCode.HasValue ? request.ResponseAvsCode.Value.ToString() : string.Empty
            };
        }

        private static PpiResponse Decline(PpiRequest request)
        {
            PpiResponse response = DeclineBase(request);
            response.DeclineType = "decline";
            return response;
        }

        private static PpiResponse DeclineCall(PpiRequest request)
        {
            PpiResponse response = DeclineBase(request);
            response.DeclineType = "call";
            return response;
        }

        private static PpiResponse DeclineCardError(PpiRequest request)
        {
            PpiResponse response = DeclineBase(request);
            response.DeclineType = "carderror";
            return response;
        }

        private static PpiResponse MissingData(PpiRequest request)
        {
            return new PpiResponse
            {
                Error = "missingfields",
                Status = "baddata",
                Offenders = "cc,exp"
            };
        }

        private static PpiResponse BadFormat(PpiRequest request)
        {
            return new PpiResponse
            {
                Error = "badformat",
                Status = "baddata",
                Offenders = "platform"
            };
        }

        private static PpiResponse NoSuchBillingId(PpiRequest request)
        {
            return new PpiResponse
            {
                ErrorType = "nonosuchbillingid",
                Status = "error"
            };
        }

        private static PpiResponse CallToProcessorFailed(PpiRequest request)
        {
            return new PpiResponse
            {
                ErrorType = "nonosuchbillingid",
                Status = "error"
            };
        }

        private static PpiResponse LinkFailure(PpiRequest request)
        {
            return new PpiResponse
            {
                ErrorType = "linkfailure",
                Status = "error"
            };
        }

        private static PpiResponse Rejected102(PpiRequest request)
        {
            return new PpiResponse
            {
                TransactionId = RandomSourceSystemKeyGenerator.New("999-NNNNNNNNNN"),
                Status = "rejected",
                ResponseCode = "102"
            };
        }

        private static PpiResponse Rejected203(PpiRequest request)
        {
            return new PpiResponse
            {
                TransactionId = RandomSourceSystemKeyGenerator.New("999-NNNNNNNNNN"),
                Status = "rejected",
                ResponseCode = "203"
            };
        }

        private static PpiResponse FailToProcess(PpiRequest request)
        {
            return new PpiResponse
            {
                TransactionId = RandomSourceSystemKeyGenerator.New("999-NNNNNNNNNN"),
                Status = "rejected",
                ErrorType = "failtoprocess"
            };
        }

        private static PpiResponse ProcessorUnreachable(PpiRequest request, int statusCode)
        {
            return new PpiResponse
            {
                ResponseCode = statusCode.ToString()
            };
        }

        #endregion
    }
}
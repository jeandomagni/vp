﻿namespace Ivh.Domain.Qat.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Entities;
    using Interfaces;
    using Newtonsoft.Json;
    using Provider.Enrollment.Intermountain.Models;

    public class IhcEnrollmentService : IIhcEnrollmentService
    {
        private readonly Lazy<IIhcEnrollmentRepository> _ihcEnrollmentRepository;

        public IhcEnrollmentService(Lazy<IIhcEnrollmentRepository> ihcEnrollmentRepository)
        {
            this._ihcEnrollmentRepository = ihcEnrollmentRepository;
        }

        public IhcEnrollmentResponse GetByGuarantorId(string guarantorId)
        {
            IhcEnrollmentResponse response = this._ihcEnrollmentRepository.Value.GetQueryable().FirstOrDefault(x => x.GuarantorId == guarantorId);

            return response;
        }

        public void AddVisits(string guarantorSourceSystemKey, IEnumerable<string> visitSourceSystemKeys)
        {
            IhcEnrollmentResponse enrollment = this.GetByGuarantorId(guarantorSourceSystemKey);
            if (enrollment == null)
            {
                enrollment = new IhcEnrollmentResponse();
            }

            EnrollGuarantorResponseModel response = null;
            if (!string.IsNullOrWhiteSpace(enrollment.Response))
            {
                response = JsonConvert.DeserializeObject<EnrollGuarantorResponseModel>(enrollment.Response);
            }

            if (response == null)
            {
                response = new EnrollGuarantorResponseModel
                {
                    guarantorId = guarantorSourceSystemKey,
                    enrollmentStatus = "T",
                    responseCode = "0",
                    responseMessage = "SUCCESS",
                    accounts = new List<EnrollGuarantorAccountResponseModel>()
                };
            }

            foreach (string visitSourceSystemKey in visitSourceSystemKeys)
            {
                string[] parts = visitSourceSystemKey.Split('_');
                if (parts.Length < 3)
                {
                    continue;
                }

                string originatingSystem = parts[0];
                string facilityId = parts[1];
                string accountNumber = parts[2];

                if (string.IsNullOrWhiteSpace(accountNumber))
                {
                    continue;
                }

                if (response.accounts.Any(x => x.accountNumber == accountNumber))
                {
                    continue;
                }

                response.accounts.Add(new EnrollGuarantorAccountResponseModel
                {
                    eligiblitStatus = "A",
                    accountNumber = accountNumber,
                    facilityId = facilityId,
                    originatingSystem = originatingSystem
                });
            }

            enrollment.GuarantorId = response.guarantorId;
            enrollment.ExpectedEnrollmentStatus = response.enrollmentStatus;
            enrollment.ReturnErrorCode = null;
            enrollment.Response = JsonConvert.SerializeObject(response);

            this._ihcEnrollmentRepository.Value.InsertOrUpdate(enrollment);
        }
    }
}
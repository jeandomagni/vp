﻿namespace Ivh.Domain.Qat.Services
{
    using System;
    using System.Collections.Generic;
    using Base.Interfaces;
    using Base.Services;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Eob.Entities;
    using Interfaces;
    using NHibernate;

    public class QaVisitService : DomainService, IQaVisitService
    {
        private readonly ISession _session;

        public QaVisitService(
            Lazy<IDomainServiceCommonService> domainServiceCommonService,
            ISessionContext<VisitPay> sessionContext) 
            : base(domainServiceCommonService)
        {
            this._session = sessionContext.Session;
        }
        
        public IList<EobExternalLink> GetEobExternalLinks()
        {
            return this._session.QueryOver<EobExternalLink>().List();
        }

        public bool UpdateEobPayerFilterExternalLink(int eobPayerFilterId, int eobExternalLinkId)
        {
            // don't want to change the mapping, so sql query instead
            string eobExternalLinkIdName = nameof(EobPayerFilterEobExternalLink.EobExternalLinkId);
            string eobPayerFilterIdName = nameof(EobPayerFilterEobExternalLink.EobPayerFilterId);

            ISQLQuery query = this._session.CreateSQLQuery($"UPDATE [eob].[EobPayerFilterEobExternalLink] SET {eobExternalLinkIdName} = {eobExternalLinkId} WHERE {eobPayerFilterIdName} = {eobPayerFilterId}");
            query.ExecuteUpdate();

            return true;
        }
    }
}

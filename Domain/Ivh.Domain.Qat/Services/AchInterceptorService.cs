﻿namespace Ivh.Domain.Qat.Services
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.Base.Utilities.Extensions;
    using Common.Cache;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;
    using Settings.Interfaces;

    public class AchInterceptorService : DomainService, IAchInterceptorService
    {
        private const int CacheDuration = 60 * 60 * 24;
        private static readonly object Lock = new object();

        private bool IsValidEnvironment
        {
            get
            {
                return new[] { IvhEnvironmentTypeEnum.Qa, IvhEnvironmentTypeEnum.Dev }.Contains(this.ApplicationSettingsService.Value.EnvironmentType.Value);
            }
        }

        public AchInterceptorService(
            Lazy<IDomainServiceCommonService> serviceCommonService) : base(serviceCommonService)
        {
        }

        public void Activate(string uriString)
        {
            if (!this.IsValidEnvironment)
                return;

            lock (Lock)
            {
                try
                {
                    if (Uri.IsWellFormedUriString(uriString, UriKind.Absolute))
                    {
                        this.Cache.Value.SetStringAsync(AchInterceptorCacheKeys.NachaGatewayOverrideUri, new Uri(uriString).AbsoluteUri, 60 * 60 * 24).Wait();
                    }
                }
                finally
                {
                    //for future clean up if using distributed cache
                }
            }
        }

        public void Deactivate()
        {
            if (!this.IsValidEnvironment)
                return;

            lock (Lock)
            {
                try
                {
                    this.Cache.Value.RemoveAsync(AchInterceptorCacheKeys.NachaGatewayOverrideUri).Wait();
                }
                finally
                {
                    //for future clean up if using distributed cache
                }
            }
        }

        public string GetReturnFile(DateTime startDate, DateTime endDate, string fileFormat)
        {
            if (!this.IsActive())
                return string.Empty;

            List<AchReturnFormat> achReturnPayments = this.GetPaymentsForReturnFile(startDate, endDate);
            return this.CreateReturnResponse(achReturnPayments);
        }

        public string GetReport(string reportTypeId, DateTime startDate, DateTime endDate, string dateTypeId, string exportFormat)
        {
            if (!this.IsActive())
                return string.Empty;

            List<AchReportFormat> achReportPayments = this.GetPaymentsForReportFile(startDate, endDate);
            return this.CreateReportResponse(achReportPayments);
        }

        private bool IsActive()
        {
            if (!this.IsValidEnvironment)
                return false;

            lock (Lock)
            {
                try
                {
                    string uriString = this.Cache.Value.GetStringAsync(AchInterceptorCacheKeys.NachaGatewayOverrideUri).Result;
                    Uri uri;
                    return uriString.TryToUri(UriKind.Absolute, true, out uri);
                }
                finally
                {
                    //for future clean up if using distributed cache
                }
            }
        }

        private List<AchReturnFormat> GetPaymentsForReturnFile(DateTime startDate, DateTime endDate)
        {
            List<AchReturnFormat> achReturnPayments = new List<AchReturnFormat>();
            List<PpiRequest> ppiRequests = PaymentQueue.Instance.Values.ToList().FindAll(f => !f.AchProcessed &&
                                                                                f.AchProcessDate != null &&
                                                                                f.AchFileType != null &&
                                                                                f.AchFileType == AchFileTypeEnum.Return &&
                                                                                f.AchProcessDate >= startDate &&
                                                                                f.AchProcessDate <= endDate);

            foreach (PpiRequest ppiRequest in ppiRequests)
            {
                AchReturnFormat achReturnPayment = new AchReturnFormat()
                {
                    CompanyId = "1234567890",
                    CompanyName = "Sample Client Name",
                    SecCode = "WEB",
                    Description = "Sample Description",
                    EffectiveDate = ppiRequest.AchProcessDate.GetValueOrDefault(DateTime.UtcNow).ToString("yyMMdd"),
                    IndividualId = ppiRequest.Response.TransactionId,
                    IndividualName = ppiRequest.VpAccountName,
                    RdfiRoutingNumber = ppiRequest.Routing,
                    RdfiAccountNumber = ppiRequest.VpLastFour,
                    TransactionCode = "26",
                    Amount = ppiRequest.AchReturnAmount.HasValue ? ppiRequest.AchReturnAmount.Value.ToString() : ppiRequest.Amount,
                    CustomerSpecifiedTraceNumber = "32 1234128380",
                    WebTransactionSingleOrRecurringIdentifier = "",
                    ReturnReasonCode = ppiRequest.AchReturnCode.GetValueOrDefault(AchReturnCodeEnum.None).ToString(),
                    ReturnCorrectionOrAddendaInfo = "",
                    ReturnDate = ppiRequest.AchProcessDate.GetValueOrDefault(DateTime.UtcNow).ToString("yyMMdd"),
                    ReturnTraceNumber = "123456789013456000"
                };


                achReturnPayments.Add(achReturnPayment);
            }

            return achReturnPayments;
        }

        private List<AchReportFormat> GetPaymentsForReportFile(DateTime startDate, DateTime endDate)
        {
            List<AchReportFormat> achReportPayments = new List<AchReportFormat>();
            List<PpiRequest> ppiRequests = PaymentQueue.Instance.Values.ToList().FindAll(f => !f.AchProcessed &&
                                                                                f.AchProcessDate != null &&
                                                                                f.AchFileType != null &&
                                                                                f.AchProcessDate >= startDate.AddDays(-1) &&
                                                                                f.AchProcessDate <= endDate.AddDays(-1));

            foreach (PpiRequest ppiRequest in ppiRequests)
            {
                ppiRequest.AchProcessed = true;

                AchReportFormat achReportPayment = new AchReportFormat()
                {
                    FileName = "1234567890_16102410_24_2016_83150_PM_1.str",
                    ProcessingDate = ppiRequest.AchProcessDate.GetValueOrDefault(DateTime.UtcNow).ToString("yyMMdd"),
                    Textbox21 = ppiRequest.VpAccountName,
                    IndividualId = ppiRequest.Response.TransactionId,
                    DebitAmount = ppiRequest.Amount,
                    CreditAmount = "0",
                    SecCode = "WEB",
                    Status = ppiRequest.AchFileType == AchFileTypeEnum.Return ? "Return" : "Paid",
                    ReturnCode = ppiRequest.AchReturnCode.GetValueOrDefault(AchReturnCodeEnum.None).ToString(),
                    ReturnDate = ppiRequest.AchProcessDate.GetValueOrDefault(DateTime.UtcNow).ToString("yyMMdd"),
                    ReturnAddenda = "",
                    DishonorReason = "",
                    PaidGroup = "",
                    DebitAmount1 = "",
                    CreditAmount1 = "0"
                };

                achReportPayments.Add(achReportPayment);
            }

            return achReportPayments;
        }

        private string CreateReturnResponse(List<AchReturnFormat> achReturnPayments)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<Response>");
            sb.AppendLine("<Code>000</Code>");
            sb.AppendLine("<Message>Successful</Message>");

            StringBuilder value = new StringBuilder();
            foreach (AchReturnFormat achReturnPayment in achReturnPayments)
                value.AppendLine(achReturnPayment.ToString());

            sb.AppendLine("<Value>" + value.ToString() + "</Value>");
            sb.AppendLine("</Response>");

            return sb.ToString();
        }

        private string CreateReportResponse(List<AchReportFormat> achReportPayments)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("FileName,ProcessingDate,textbox21,IndividualID,DebitAmount,CreditAmount,SECCode,Status,ReturnCode,ReturnDate,ReturnAddenda,DishonorReason,PaidGroup,DebitAmount_1,CreditAmount_1");

            foreach (AchReportFormat achReportPayment in achReportPayments)
                sb.AppendLine(achReportPayment.ToString());

            sb.AppendLine("");
            sb.AppendLine("");
            sb.AppendLine("textbox4,textbox5,textbox6,textbox7");
            sb.AppendLine("Origination Total,'$40,115.83',$0.00,393");
            sb.AppendLine("Return Total,'$1,236.00',$0.00,8");
            sb.AppendLine("");
            sb.AppendLine("Textbox11");
            sb.AppendLine("'$40,115.83'");

            return Convert.ToBase64String(Encoding.ASCII.GetBytes(sb.ToString()));
        }
    }

    public class AchReturnFormat
    {
        public string CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string SecCode { get; set; }
        public string Description { get; set; }
        public string EffectiveDate { get; set; }
        public string IndividualId { get; set; }
        public string IndividualName { get; set; }
        public string RdfiRoutingNumber { get; set; }
        public string RdfiAccountNumber { get; set; }
        public string TransactionCode { get; set; }
        public string Amount { get; set; }
        public string CustomerSpecifiedTraceNumber { get; set; }
        public string WebTransactionSingleOrRecurringIdentifier { get; set; }
        public string ReturnReasonCode { get; set; }
        public string ReturnCorrectionOrAddendaInfo { get; set; }
        public string ReturnDate { get; set; }
        public string ReturnTraceNumber { get; set; }

        public override string ToString()
        {
            return string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16}", this.CompanyId, this.CompanyName, this.SecCode, this.Description, this.EffectiveDate, this.IndividualId, this.IndividualName, this.RdfiRoutingNumber, this.RdfiAccountNumber, this.TransactionCode, this.Amount, this.CustomerSpecifiedTraceNumber, this.WebTransactionSingleOrRecurringIdentifier, this.ReturnReasonCode, this.ReturnCorrectionOrAddendaInfo, this.ReturnDate, this.ReturnTraceNumber);
        }
    }

    public class AchReportFormat
    {
        public string FileName { get; set; }
        public string ProcessingDate { get; set; }
        public string Textbox21 { get; set; }
        public string IndividualId { get; set; }
        public string DebitAmount { get; set; }
        public string CreditAmount { get; set; }
        public string SecCode { get; set; }
        public string Status { get; set; }
        public string ReturnCode { get; set; }
        public string ReturnDate { get; set; }
        public string ReturnAddenda { get; set; }
        public string DishonorReason { get; set; }
        public string PaidGroup { get; set; }
        public string DebitAmount1 { get; set; }
        public string CreditAmount1 { get; set; }

        public override string ToString()
        {
            return string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14}", this.FileName, this.ProcessingDate, this.Textbox21, this.IndividualId, this.DebitAmount, this.CreditAmount, this.SecCode, this.Status, this.ReturnCode, this.ReturnDate, this.ReturnAddenda, this.DishonorReason, this.PaidGroup, this.DebitAmount1, this.CreditAmount1);
        }
    }
}

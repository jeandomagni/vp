﻿namespace Ivh.Domain.Qat.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Enums;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using FinanceManagement.Entities;
    using FinanceManagement.Interfaces;
    using Interfaces;
    using NHibernate;

    public class TestPaymentMethodsService : DomainService, ITestPaymentMethodsService
    {
        private readonly Lazy<IPaymentMethodEventRepository> _paymentMethodEventRepository;
        private readonly Lazy<IPaymentMethodAccountTypeRepository> _paymentMethodAccountTypeRepository;
        private readonly Lazy<IPaymentMethodProviderTypeRepository> _paymentMethodProviderTypeRepository;
        private readonly Lazy<IPaymentMethodsRepository> _paymentMethodsRepository;
        private readonly ISession _session;

        public TestPaymentMethodsService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            ISessionContext<VisitPay> sessionContext,
            Lazy<IPaymentMethodsRepository> paymentMethodsRepository,
            Lazy<IPaymentMethodEventRepository> paymentMethodEventRepository,
            Lazy<IPaymentMethodAccountTypeRepository> paymentMethodAccountTypeRepository,
            Lazy<IPaymentMethodProviderTypeRepository> paymentMethodProviderTypeRepository) : base(serviceCommonService)
        {
            this._session = sessionContext.Session;
            this._paymentMethodsRepository = paymentMethodsRepository;
            this._paymentMethodEventRepository = paymentMethodEventRepository;
            this._paymentMethodAccountTypeRepository = paymentMethodAccountTypeRepository;
            this._paymentMethodProviderTypeRepository = paymentMethodProviderTypeRepository;
        }

        public void SaveOrUpdate(PaymentMethod paymentMethod, int visitPayUserId)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                if (paymentMethod.IsPrimary)
                {
                    PaymentMethod currentPrimaryStateless = this._paymentMethodsRepository.Value.GetPrimaryPaymentMethodStateless(paymentMethod.VpGuarantorId.GetValueOrDefault(0));
                    if (currentPrimaryStateless != null && currentPrimaryStateless.PaymentMethodId != paymentMethod.PaymentMethodId)
                    {
                        PaymentMethod currentPrimary = this._paymentMethodsRepository.Value.GetById(currentPrimaryStateless.PaymentMethodId);
                        currentPrimary.IsPrimary = false;
                        this._paymentMethodsRepository.Value.InsertOrUpdate(currentPrimary);
                        this._paymentMethodEventRepository.Value.InsertOrUpdate(new PaymentMethodEvent
                        {
                            PaymentMethod = currentPrimary,
                            PaymentMethodStatus = PaymentMethodStatusEnum.Modified,
                            VisitPayUserId = visitPayUserId,
                            VpGuarantorId = currentPrimary.VpGuarantorId ?? 0
                        });
                    }
                }

                paymentMethod.PaymentMethodAccountType = paymentMethod.PaymentMethodAccountType ?? this._paymentMethodAccountTypeRepository.Value.GetById(0);
                paymentMethod.PaymentMethodProviderType = paymentMethod.PaymentMethodProviderType ?? this._paymentMethodProviderTypeRepository.Value.GetById(0);
                
                this._paymentMethodsRepository.Value.InsertOrUpdate(paymentMethod);
                
                this._paymentMethodEventRepository.Value.InsertOrUpdate(new PaymentMethodEvent
                {
                    PaymentMethod = paymentMethod,
                    PaymentMethodStatus = paymentMethod.PaymentMethodId == 0 ? PaymentMethodStatusEnum.Activated : PaymentMethodStatusEnum.Modified,
                    VisitPayUserId = visitPayUserId,
                    VpGuarantorId = paymentMethod.VpGuarantorId ?? 0
                });
                
                if (paymentMethod.IsPrimary)
                {
                    this._paymentMethodEventRepository.Value.InsertOrUpdate(new PaymentMethodEvent
                    {
                        PaymentMethod = paymentMethod,
                        PaymentMethodStatus = PaymentMethodStatusEnum.SetPrimary,
                        VisitPayUserId = visitPayUserId,
                        VpGuarantorId = paymentMethod.VpGuarantorId ?? 0
                    });
                }

                unitOfWork.Commit();
            }
        }
    }
}
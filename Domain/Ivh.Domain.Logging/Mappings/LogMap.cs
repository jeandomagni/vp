﻿namespace Ivh.Domain.Logging.Mappings
{
    using Common.Base.Constants;
    using Common.Data.Extensions;
    using Entities;
    using FluentNHibernate.Mapping;

    public class LogMap : ClassMap<Log>
    {
        public LogMap()
        {
            this.Schema(NhibernateConstants.SchemaNames.Log);
            this.Table("Log");
            this.Id(x => x.Id).GeneratedBy.Identity();
            this.Map(x => x.Date).Not.Nullable();
            this.Map(x => x.Thread).Not.Nullable();
            this.Map(x => x.Level).Not.Nullable();
            this.Map(x => x.Logger).Not.Nullable();
            this.Map(x => x.Message).AsVarcharMax().Not.Nullable();
            this.Map(x => x.Exception).Nullable();
            this.Map(x => x.Version).Nullable();
            this.Map(x => x.StackTraceHashCode).Nullable();
            this.Map(x => x.CorrelationId).Nullable();
        }
    }
}

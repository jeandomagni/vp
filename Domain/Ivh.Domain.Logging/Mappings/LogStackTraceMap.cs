﻿namespace Ivh.Domain.Logging.Mappings
{
    using Common.Base.Constants;
    using Common.Data.Extensions;
    using Entities;
    using FluentNHibernate.Mapping;

    public class LogStackTraceMap : ClassMap<LogStackTrace>
    {
        public LogStackTraceMap()
        {
            this.Schema(NhibernateConstants.SchemaNames.Log);
            this.Table("LogStackTrace");
            this.Id(x => x.StackTraceHashCode).GeneratedBy.Assigned();
            this.Map(x => x.StackTrace).AsVarcharMax().Not.Nullable();
            this.Map(x => x.FirstOccurrence).Not.Nullable();
            this.Map(x => x.MostRecentOccurrence).Not.Nullable();
            this.Map(x => x.NumberOfOccurrences).Not.Nullable();
        }
    }
}

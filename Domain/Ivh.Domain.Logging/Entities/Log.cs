﻿namespace Ivh.Domain.Logging.Entities
{
    using System;
    public class Log
    {
        public virtual int Id { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual string Thread { get; set; }
        public virtual string Level { get; set; }
        public virtual string Logger { get; set; }
        public virtual string Message { get; set; }
        public virtual string Exception { get; set; }
        public virtual string Version { get; set; }
        public virtual int? StackTraceHashCode { get; set; }
        public virtual Guid? CorrelationId { get; set; }
    }
}

﻿namespace Ivh.Domain.Logging.Entities
{
    using System;

    public class LogStackTrace
    {
        public virtual int StackTraceHashCode { get; set; }
        public virtual string StackTrace { get; set; }
        public virtual DateTime FirstOccurrence { get; set; }
        public virtual DateTime MostRecentOccurrence { get; set; }
        public virtual int NumberOfOccurrences { get; set; }
    }
}

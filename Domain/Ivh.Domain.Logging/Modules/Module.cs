﻿namespace Ivh.Domain.Logging.Modules
{
    using Autofac;
    using Interfaces;
    using Services;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<LoggingService>().As<ILoggingService>();
        }
    }
}
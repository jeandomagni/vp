﻿namespace Ivh.Domain.Logging.Enums
{
    public enum LoggerTypeEnum
    {
        ServiceBus,
        Database
    }
}

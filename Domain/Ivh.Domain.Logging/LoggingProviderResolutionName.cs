﻿namespace Ivh.Domain.Logging
{
    public static class LoggingProviderResolutionName
    {
        public const string DatabaseLogging = nameof(DatabaseLogging);
        public const string ServiceBusLogging = nameof(ServiceBusLogging);
    }
}

﻿namespace Ivh.Domain.Logging.Services
{
    using System;
    using Interfaces;
    public class LoggingService : ILoggingService
    {
        private readonly ILoggingProvider _loggingProvider;

        public LoggingService(ILoggingProvider loggingProvider)
        {
            this._loggingProvider = loggingProvider;
        }

        public void Debug(Func<string> message, params object[] parameters)
        {
            this._loggingProvider.Debug(message, parameters);
        }

        public void Fatal(Func<string> message, params object[] parameters)
        {
            this._loggingProvider.Fatal(message, parameters);
        }

        public void Fatal(Func<string> message, string stackTrace, params object[] parameters)
        {
            this._loggingProvider.Fatal(message, stackTrace, parameters);
        }

        public void Info(Func<string> message, params object[] parameters)
        {
            this._loggingProvider.Info(message, parameters);
        }

        public void Warn(Func<string> message, params object[] parameters)
        {
            this._loggingProvider.Warn(message, parameters);
        }

        public void SetApplicationNameAndVersion(string applicationName, string applicationVersion, DateTime date, int threadId, Guid correlationId)
        {
            this._loggingProvider.SetApplicationNameAndVersion(applicationName, applicationVersion, date, threadId, correlationId);
        }

        public void SetApplicationNameAndVersion(string applicationName, string applicationVersion)
        {
            this._loggingProvider.SetApplicationNameAndVersion(applicationName, applicationVersion);
        }
    }
}

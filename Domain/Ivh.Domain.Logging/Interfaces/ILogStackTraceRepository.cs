﻿namespace Ivh.Domain.Logging.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface ILogStackTraceRepository : IRepository<LogStackTrace>
    {
        void Increment(int stackTraceHashCode);
    }
}

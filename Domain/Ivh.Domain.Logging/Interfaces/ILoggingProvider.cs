﻿namespace Ivh.Domain.Logging.Interfaces
{
    using System;
    using System.Threading;

    public interface ILoggingProvider
    {
        void Debug(Func<string> message, params object[] parameters);
        void Fatal(Func<string> message, params object[] parameters);
        void Fatal(Func<string> message, string stackTrace, params object[] parameters);
        void Info(Func<string> message, params object[] parameters);
        void Warn(Func<string> message, params object[] parameters);
        void SetApplicationNameAndVersion(string applicationName, string applicationVersion, DateTime date, int threadId, Guid correlationId);
        void SetApplicationNameAndVersion(string applicationName, string applicationVersion);
    }
}


﻿namespace Ivh.Domain.Logging.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface ILogRepository : IRepository<Log>
    {
        
    }
}

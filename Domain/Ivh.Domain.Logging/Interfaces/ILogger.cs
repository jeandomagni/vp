﻿namespace Ivh.Domain.Logging.Interfaces
{
    using System;

    [Obsolete("Refactor to use ILoggingApplicationService, ILoggingService or ILoggingProvider")]
    public interface ILogger
    {
        void Debug(string message, params object[] parameters);
        void Fatal(string message, params object[] parameters);
        void Fatal(string message, string stackTrace, params object[] parameters);
        void Info(string message, params object[] parameters);
        void Warn(string message, params object[] parameters);
        void SetApplicationNameAndVersion(string applicationName, string applicationVersion);
    }
}

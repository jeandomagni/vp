﻿namespace Ivh.Domain.Logging.Interfaces
{
    public interface IMetricsProvider
    {
        void Increment(string statName, int value = 1, double sampleRate = 1.0, params string[] tags);
        void Decrement(string statName, int value = 1, double sampleRate = 1.0, params string[] tags);
        void Gauge<T>(string statName, T value, double sampleRate = 1, params string[] tags);
    }
}
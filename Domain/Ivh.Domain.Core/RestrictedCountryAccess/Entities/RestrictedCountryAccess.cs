﻿namespace Ivh.Domain.Core.RestrictedCountryAccess.Entities
{
    public class RestrictedCountryAccess
    {
        public virtual string CountryCodeIso2 { get; set; }
        public virtual string CountryName { get; set; }
    }
}
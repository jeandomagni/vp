﻿namespace Ivh.Domain.Core.RestrictedCountryAccess.Interfaces
{
    using Common.Base.Interfaces;

    public interface IRestrictedCountryAccessRepository : IRepository<Entities.RestrictedCountryAccess>
    {
    }
}
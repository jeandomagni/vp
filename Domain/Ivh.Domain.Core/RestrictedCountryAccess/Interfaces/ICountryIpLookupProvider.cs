﻿namespace Ivh.Domain.Core.RestrictedCountryAccess.Interfaces
{
    public interface ICountryIpLookupProvider
    {
        string GetCountryForIp(string ip);
    }
}

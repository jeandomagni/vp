﻿namespace Ivh.Domain.Core.RestrictedCountryAccess.Interfaces
{
    public interface IRestrictedCountryAccessService
    {
        bool IsIpRestrictedByCountry(string ipAddress);
    }
}
﻿namespace Ivh.Domain.Core.RestrictedCountryAccess.Mappings
{
    using FluentNHibernate.Mapping;

    public class RestrictedCountryAccessMap : ClassMap<Entities.RestrictedCountryAccess>
    {
        public RestrictedCountryAccessMap()
        {
            this.Schema("dbo");
            this.Table("RestrictedCountryAccess");
            this.Id(x => x.CountryCodeIso2).GeneratedBy.Assigned();
            this.Map(x => x.CountryName)
                .Nullable();
        }
    }
}
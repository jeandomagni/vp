﻿namespace Ivh.Domain.Core.RestrictedCountryAccess.Services
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Enums;
    using Common.Cache;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;
    using Logging.Interfaces;
    using Settings.Interfaces;

    public class RestrictedCountryAccessService : IRestrictedCountryAccessService
    {
        private const int MinutesForCountryCache = 10;
        private const int SecondsForIpCacheRedis = 604800; // Should be 1 week, 7 * 24 * 60 * 60
        private static ConcurrentDictionary<string, byte> _countryData = null;
        private static DateTime _countryDataExpTime = DateTime.UtcNow;

        private readonly Lazy<ICountryIpLookupProvider> _countryLookUpProvider;
        private readonly Lazy<IDistributedCache> _cache;
        private readonly Lazy<IRestrictedCountryAccessRepository> _restrictedCountryAccessRepo;
        private readonly Lazy<IFeatureService> _featureService;
        private readonly Lazy<ILoggingService> _loggingService;

        private bool IsCountryInList(string countryCodeIso2)
        {
            if (string.IsNullOrWhiteSpace(countryCodeIso2))
            {
                return false;
            }

            if (_countryData == null || _countryDataExpTime <= DateTime.UtcNow)
            {
                //Reload the data
                _countryDataExpTime = DateTime.UtcNow.AddMinutes(MinutesForCountryCache);
                List<RestrictedCountryAccess> allCountries = this._restrictedCountryAccessRepo.Value.GetQueryable().ToList();
                _countryData = new ConcurrentDictionary<string, byte>(allCountries.ToDictionary(x => x.CountryCodeIso2.ToLowerInvariant(), y => (byte) 0));
            }

            return _countryData.ContainsKey(countryCodeIso2.ToLowerInvariant());
        }

        private bool IsPrivate(string ipAddress)
        {
            if (ipAddress.Contains("::"))
            {
                //IPV6
                return false;
            }

            int[] ipParts = ipAddress.Split(new String[] { "." }, StringSplitOptions.RemoveEmptyEntries)
                .Select(s => int.Parse(s)).ToArray();

            //Make sure it parsed correctly. Error on the side of caution, just let them in if it seems invalid.
            if (ipParts.Length < 2)
            {
                return true;
            }

            // in private ip range
            if (ipParts[0] == 10 ||
                (ipParts[0] == 192 && ipParts[1] == 168) ||
                (ipParts[0] == 172 && (ipParts[1] >= 16 && ipParts[1] <= 31)) ||
                (ipParts[0] == 169 && ipParts[1] == 254)
                ) {
                return true;
            }

            // IP Address is probably public.
            // This doesn't catch some VPN ranges like OpenVPN and Hamachi.
            return false;
        }

        public RestrictedCountryAccessService(
            Lazy<ICountryIpLookupProvider> countryLookUpProvider,
            Lazy<IDistributedCache> cache,
            Lazy<IRestrictedCountryAccessRepository> restrictedCountryAccessRepo,
            Lazy<IFeatureService> featureService,
            Lazy<ILoggingService> loggingService
            )
        {
            this._countryLookUpProvider = countryLookUpProvider;
            this._cache = cache;
            this._restrictedCountryAccessRepo = restrictedCountryAccessRepo;
            this._featureService = featureService;
            this._loggingService = loggingService;
        }

        public bool IsIpRestrictedByCountry(string ipAddress)
        {
            if (string.IsNullOrWhiteSpace(ipAddress))
            {
                return false;
            }

            if (!this._featureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureRestrictIpAddressByCountry))
            {
                return false;
            }

            if (this.IsPrivate(ipAddress))
            {
                //who knows with private IPs
                return false;
            }

            string cacheKey = $"IpAddress-{ipAddress}";
            string countryCode = this._cache.Value.GetString(cacheKey);
            if (countryCode == null)
            {
                countryCode = this._countryLookUpProvider.Value.GetCountryForIp(ipAddress);
                this._cache.Value.SetString(cacheKey, countryCode, SecondsForIpCacheRedis);
            }

            bool isIpRestrictedByCountry = this.IsCountryInList(countryCode);

            this._loggingService.Value.Info(() => $"RestrictedCountryAccessService::IsIpRestrictedByCountry IP:{ipAddress}, CountryCode: {countryCode}, IsRestricted: {isIpRestrictedByCountry}");
            return isIpRestrictedByCountry;
        }
    }
}
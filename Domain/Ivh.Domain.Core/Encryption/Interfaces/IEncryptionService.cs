﻿namespace Ivh.Domain.Core.Encryption.Interfaces
{
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;

    public interface IEncryptionService : IDomainService
    {
        SsoProviderEnum ProviderEnum { get; }

        string Decrypt(string valueToDecrypt, string iV = null);
        string Encrypt(string valueToEncrypt, string iV = null);

    }
}
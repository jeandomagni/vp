﻿namespace Ivh.Domain.Core.Encryption.Services
{
    using System;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Enums;
    using Common.Encryption;
    using Common.VisitPay.Enums;
    using Interfaces;
    using Settings.Interfaces;

    public class StLukesEncryptionService : DomainService,  IEncryptionService
    {
        private readonly string _encryptionKey;

        public StLukesEncryptionService(Lazy<IDomainServiceCommonService> serviceCommonService) : base(serviceCommonService)
        {
            this._encryptionKey = this.ApplicationSettingsService.Value.SsoEncryptionKey.Value;
            //for this implementation the IV is always 0, that's just how open epic's sso works.
        }

        public SsoProviderEnum ProviderEnum { get { return SsoProviderEnum.OpenEpic; } }

        public string Decrypt(string valueToDecrypt, string iV = null)
        {
            byte[] aesKey = AesStLukes.AesCryptDeriveKey(this._encryptionKey);
            byte[] bytesToDecrypt = Convert.FromBase64String(valueToDecrypt);

            byte[] decryptedBytes = AesStLukes.AesDecryptBytes(bytesToDecrypt, aesKey);
            return System.Text.Encoding.UTF8.GetString(decryptedBytes);
        }
        public string Encrypt(string valueToEncrypt, string iV = null)
        {
            byte[] aesKey = AesStLukes.AesCryptDeriveKey(this._encryptionKey);
            byte[] bytesToEncrypt = System.Text.Encoding.UTF8.GetBytes(valueToEncrypt);

            byte[] encryptedBytes = AesStLukes.AesEncryptBytes(bytesToEncrypt, aesKey);
            return Convert.ToBase64String(encryptedBytes);
        }
    }
}
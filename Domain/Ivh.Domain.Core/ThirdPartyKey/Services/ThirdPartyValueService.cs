﻿namespace Ivh.Domain.Core.ThirdPartyKey.Services
{
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Domain.Base.Interfaces;
    using Ivh.Domain.Base.Services;
    using Ivh.Domain.Core.ThirdPartyKey.Entities;
    using Ivh.Domain.Core.ThirdPartyKey.Interfaces;
    using System;
    using System.Linq;

    public class ThirdPartyValueService : DomainService, IThirdPartyValueService
    {
        private readonly IThirdPartyValueRepository _thirdPartyValueRepository;

        public ThirdPartyValueService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            IThirdPartyValueRepository thirdPartyValueRepository) : base(serviceCommonService)
        {
            this._thirdPartyValueRepository = thirdPartyValueRepository;
        }

        public void SetCardReaderDeviceKey(string value, int visitPayUserId)
        {
            SetKey(value, visitPayUserId, ThirdPartyKeyTypeEnum.CardReaderDeviceKey);
        }

        public void SetCardReaderUserAccount(string value, int visitPayUserId)
        {
            SetKey(value, visitPayUserId, ThirdPartyKeyTypeEnum.UserAccount);
        }

        private void SetKey(string value, int visitPayUserId, ThirdPartyKeyTypeEnum type)
        {
            ThirdPartyValue thirdPartyValue = _thirdPartyValueRepository.GetQueryable().FirstOrDefault(x => x.VisitPayUserId == visitPayUserId && x.ThirdPartyKeyType == type);
            if(thirdPartyValue == null)
            {
                thirdPartyValue = new ThirdPartyValue
                {
                    ThirdPartyKeyType = type,
                    VisitPayUserId = visitPayUserId
                };
            }
            thirdPartyValue.Value = value;
            _thirdPartyValueRepository.InsertOrUpdate(thirdPartyValue);
        }

        public string GetCardReaderDeviceValue(int visitPayUserId)
        {
            return GetThirdPartyValue(visitPayUserId, ThirdPartyKeyTypeEnum.CardReaderDeviceKey);
        }

        public string GetCardReaderUserAccountValue(int visitPayUserId)
        {
            return GetThirdPartyValue(visitPayUserId, ThirdPartyKeyTypeEnum.UserAccount);
        }

        private string GetThirdPartyValue(int visitPayUserId, ThirdPartyKeyTypeEnum type)
        {
            return this._thirdPartyValueRepository.GetQueryable().FirstOrDefault(x => x.ThirdPartyKeyType == type && x.VisitPayUserId == visitPayUserId)?.Value;
        }

        public bool CardDeviceKeyNeedsToBeSet(int visitPayUserId, bool isFeatureEnabled, bool userHasRole)
        {
            string deviceKey = this.GetCardReaderDeviceValue(visitPayUserId);
            string userAccount = this.GetCardReaderUserAccountValue(visitPayUserId);
            bool cookieNeedsToSet = isFeatureEnabled
                                    && userHasRole
                                    && (string.IsNullOrEmpty(deviceKey) || string.IsNullOrEmpty(userAccount));
            return cookieNeedsToSet;
        }
    }
}

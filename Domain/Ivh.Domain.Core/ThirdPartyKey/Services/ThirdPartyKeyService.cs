﻿namespace Ivh.Domain.Core.ThirdPartyKey.Services
{
    using System;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;

    public class ThirdPartyKeyService : DomainService, IThirdPartyKeyService
    {
        private readonly IThirdPartyKeyRepository _thirdPartyKeyRepository;

        public ThirdPartyKeyService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            IThirdPartyKeyRepository thirdPartyKeyRepository) : base(serviceCommonService)
        {
            this._thirdPartyKeyRepository = thirdPartyKeyRepository;
        }

        public bool IsCardReaderDeviceKeyAllowed(string key)
        {
            return IsKeyAllowed(key, ThirdPartyKeyTypeEnum.CardReaderDeviceKey);
        }
        
        public bool IsCardReaderUserAccountAllowed(string userAccount)
        {
            return IsKeyAllowed(userAccount, ThirdPartyKeyTypeEnum.UserAccount);
        }

        private bool IsKeyAllowed(string key, ThirdPartyKeyTypeEnum type)
        {
            IQueryable<ThirdPartyKey> query = this._thirdPartyKeyRepository.GetAllStateless().Where(x => x.ThirdPartyKeyType == type);

            if (query.Any() == false)
            {
                return true;
            }

            return query.Any(x => x.Value == key);
        }
    }
}
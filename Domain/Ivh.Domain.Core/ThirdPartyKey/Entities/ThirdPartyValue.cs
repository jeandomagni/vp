﻿namespace Ivh.Domain.Core.ThirdPartyKey.Entities
{
    using Ivh.Common.VisitPay.Enums;

    public class ThirdPartyValue
    {
        public virtual int ThirdPartyValueId { get; set; }
        public virtual string Value { get; set; }
        public virtual int? VisitPayUserId { get; set; }
        public virtual ThirdPartyKeyTypeEnum ThirdPartyKeyType { get; set; }
    }
}

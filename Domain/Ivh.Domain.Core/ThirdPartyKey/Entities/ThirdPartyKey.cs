﻿namespace Ivh.Domain.Core.ThirdPartyKey.Entities
{
    using Ivh.Common.VisitPay.Enums;

    public class ThirdPartyKey
    {
        public virtual int ThirdPartyKeyId { get; set; }
        public virtual string Value { get; set; }
        public virtual ThirdPartyKeyTypeEnum ThirdPartyKeyType { get; set; }

    }
}

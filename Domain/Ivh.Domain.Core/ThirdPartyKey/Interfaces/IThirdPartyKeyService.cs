﻿namespace Ivh.Domain.Core.ThirdPartyKey.Interfaces
{
    using Common.Base.Interfaces;

    public interface IThirdPartyKeyService : IDomainService
    {
        bool IsCardReaderDeviceKeyAllowed(string key);
        bool IsCardReaderUserAccountAllowed(string userAccount);
    }
}

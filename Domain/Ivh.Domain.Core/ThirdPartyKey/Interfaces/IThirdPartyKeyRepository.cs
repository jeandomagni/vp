﻿namespace Ivh.Domain.Core.ThirdPartyKey.Interfaces
{
    using Ivh.Common.Base.Interfaces;
    using System.Linq;

    public interface IThirdPartyKeyRepository : IRepository<Entities.ThirdPartyKey>
    {
        IQueryable<Entities.ThirdPartyKey> GetAllStateless();
    }
}

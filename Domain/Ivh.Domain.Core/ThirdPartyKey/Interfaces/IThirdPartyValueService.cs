﻿namespace Ivh.Domain.Core.ThirdPartyKey.Interfaces
{
    using Common.Base.Interfaces;

    public interface IThirdPartyValueService : IDomainService
    {
        string GetCardReaderDeviceValue(int visitPayUserId);
        string GetCardReaderUserAccountValue(int visitPayUserId);
        void SetCardReaderDeviceKey(string value, int visitPayUserId);
        void SetCardReaderUserAccount(string value, int visitPayUserId);
        bool CardDeviceKeyNeedsToBeSet(int visitPayUserId, bool isFeatureEnabled, bool userHasRole);
    }
}

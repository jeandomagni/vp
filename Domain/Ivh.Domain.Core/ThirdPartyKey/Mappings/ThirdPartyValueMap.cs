﻿namespace Ivh.Domain.Core.ThirdPartyKey.Mappings
{
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class ThirdPartyValueMap : ClassMap<ThirdPartyValue>
    {
        public ThirdPartyValueMap()
        {
            this.Schema("dbo");
            this.Table("ThirdPartyValue");
            this.Id(x => x.ThirdPartyValueId).GeneratedBy.Identity();
            this.Map(x => x.Value).Column("ThirdPartyValue").Not.Nullable();
            this.Map(x => x.VisitPayUserId).Nullable();
            this.Map(x => x.ThirdPartyKeyType).Column("ThirdPartyKeyTypeId").CustomType<ThirdPartyKeyTypeEnum>().Not.Nullable();
        }
    }
}
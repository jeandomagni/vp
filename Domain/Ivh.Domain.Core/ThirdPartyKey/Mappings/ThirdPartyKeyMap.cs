﻿namespace Ivh.Domain.Core.ThirdPartyKey.Mappings
{
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class ThirdPartyKeyMap : ClassMap<ThirdPartyKey>
    {
        public ThirdPartyKeyMap()
        {
            this.Schema("dbo");
            this.Table("ThirdPartyKey");
            this.Id(x => x.ThirdPartyKeyId).GeneratedBy.Identity();
            this.Map(x => x.Value).Column("ThirdPartyKey").Not.Nullable();
            this.Map(x => x.ThirdPartyKeyType).Column("ThirdPartyKeyTypeId").CustomType<ThirdPartyKeyTypeEnum>().Not.Nullable();
        }
    }
}
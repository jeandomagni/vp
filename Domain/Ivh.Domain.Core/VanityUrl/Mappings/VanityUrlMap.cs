﻿
namespace Ivh.Domain.Core.VanityUrl.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class VanityUrlMap : ClassMap<VanityUrl>
    {
        public VanityUrlMap()
        {
            this.Schema("dbo");
            this.Table("VanityUrl");
            this.ReadOnly();

            this.Id(x => x.VanityUrlId);
            this.Map(x => x.VanityUrlKeyName).Not.Nullable();
            this.Map(x => x.RedirectController).Not.Nullable();
            this.Map(x => x.RedirectAction).Not.Nullable();
            this.Map(x => x.DateActiveUtc).Nullable();
            this.Map(x => x.DateInactiveUtc).Nullable();
            
            this.Cache.ReadOnly();
        }
    }
}

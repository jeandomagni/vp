﻿
namespace Ivh.Domain.Core.VanityUrl.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IVanityUrlService : IDomainService
    {
        VanityUrl GetVanityUrl(string vanityUrlKeyName);
    }
}
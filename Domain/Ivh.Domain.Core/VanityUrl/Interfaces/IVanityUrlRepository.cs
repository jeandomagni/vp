﻿
namespace Ivh.Domain.Core.VanityUrl.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IVanityUrlRepository : IRepository<VanityUrl>
    {
        IList<VanityUrl> GetVanityUrls();
    }
}

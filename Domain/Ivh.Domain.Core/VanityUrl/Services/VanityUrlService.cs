﻿
namespace Ivh.Domain.Core.VanityUrl.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Entities;
    using Interfaces;
    using Visit.Interfaces;

    public class VanityUrlService: DomainService, IVanityUrlService
    {
        private readonly Lazy<IVanityUrlRepository> _vanityUrlRepository;

        public VanityUrlService(
            Lazy<IVanityUrlRepository> vanityUrlRepository, 
            Lazy<IDomainServiceCommonService> serviceCommonService) : base(serviceCommonService)
        {
            this._vanityUrlRepository = vanityUrlRepository;
        }

        public VanityUrl GetVanityUrl(string vanityUrlKeyName)
        {
            IList<VanityUrl> urls = this._vanityUrlRepository.Value.GetVanityUrls();
            VanityUrl urlMatch = urls.FirstOrDefault(x => x.VanityUrlKeyName.ToLower() == vanityUrlKeyName.ToLower());
            if (urlMatch == null)
            {
                urlMatch = urls.FirstOrDefault(x => x.VanityUrlKeyName.ToLower() == "default");
            }
            return urlMatch;
        }

    }
}

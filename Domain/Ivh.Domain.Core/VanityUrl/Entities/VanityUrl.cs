﻿namespace Ivh.Domain.Core.VanityUrl.Entities
{
    using System;

    public class VanityUrl
    {
        public virtual int VanityUrlId { get; set; }
        public virtual string VanityUrlKeyName { get; set; }
        public virtual string RedirectController { get; set; }
        public virtual string RedirectAction { get; set; }
        public virtual DateTime? DateActiveUtc { get; set; }
        public virtual DateTime? DateInactiveUtc { get; set; }
    }
}

﻿namespace Ivh.Domain.Core.VisitPayDocument.Interfaces
{
    using System.Collections.Generic;
    using Application.Core.Common.Dtos;
    using Common.Base.Interfaces;
    using Entities;

    public interface IVisitPayDocumentRepository : IRepository<VisitPayDocument>
    {
        IList<VisitPayDocumentResult> GetVisitPayDocumentResults(VisitPayDocumentFilter VisitPayDocumentFilter);
    }
}
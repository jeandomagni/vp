﻿namespace Ivh.Domain.Core.VisitPayDocument.Interfaces
{
    using System.Collections.Generic;
    using Application.Core.Common.Dtos;
    using Common.Base.Interfaces;
    using Entities;

    public interface IVisitPayDocumentService : IDomainService
    {
        void AddVisitPayDocument(VisitPayDocument visitPayDocument);
        IList<VisitPayDocumentResult> GetVisitPayDocumentResults(VisitPayDocumentFilter visitPayDocumentFilter);
        void ApproveVisitPayDocument(int visitPayDocumentId, int approvedByVpUserId);
        VisitPayDocument GetVisitPayDocumentId(int visitPayDocumentId);
        VisitPayDocumentResult GetVisitPayDocumentResultById(int visitPayDocumentId);
        void SavevisitPayDocumentId(VisitPayDocument visitPayDocumentIdDocument);
        void DeletevisitPayDocumentId(int visitPayDocumentId);
    }
}
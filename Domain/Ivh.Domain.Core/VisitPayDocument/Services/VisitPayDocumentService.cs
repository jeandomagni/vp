﻿namespace Ivh.Domain.Core.VisitPayDocument.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Application.Core.Common.Dtos;
    using Base.Interfaces;
    using Base.Services;
    using Entities;
    using Interfaces;

    public class VisitPayDocumentService : DomainService, IVisitPayDocumentService
    {
        private readonly Lazy<IVisitPayDocumentRepository> _visitPayDocumentRepository;

        public VisitPayDocumentService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IVisitPayDocumentRepository> visitPayDocumentRepository
        ) : base(serviceCommonService)
        {
            this._visitPayDocumentRepository = visitPayDocumentRepository;
        }

        public void AddVisitPayDocument(VisitPayDocument visitPayDocument)
        {
            this._visitPayDocumentRepository.Value.Insert(visitPayDocument);
        }

        public IList<VisitPayDocumentResult> GetVisitPayDocumentResults(VisitPayDocumentFilter visitPayDocumentFilter)
        {
            IList<VisitPayDocumentResult> visitPayDocumentResults =
                this._visitPayDocumentRepository
                    .Value
                    .GetVisitPayDocumentResults(visitPayDocumentFilter);

            return visitPayDocumentResults;
        }

        public void ApproveVisitPayDocument(int visitPayDocumentId, int approvedByVpUserId)
        {
            VisitPayDocument visitPayDocument = this._visitPayDocumentRepository
                .Value
                .GetById(visitPayDocumentId);

            visitPayDocument.ApprovedByVpUserId = approvedByVpUserId;
            visitPayDocument.ApprovedDate = DateTime.UtcNow;
        }

        public VisitPayDocument GetVisitPayDocumentId(int visitPayDocumentId)
        {
            VisitPayDocument visitPayDocument = this._visitPayDocumentRepository.Value.GetById(visitPayDocumentId);
            return visitPayDocument;
        }

        public VisitPayDocumentResult GetVisitPayDocumentResultById(int visitPayDocumentId)
        {
            VisitPayDocumentFilter visitPayDocumentFilter = new VisitPayDocumentFilter {VisitPayDocumentId = visitPayDocumentId};
            VisitPayDocumentResult visitPayDocumentResult = this._visitPayDocumentRepository.Value.GetVisitPayDocumentResults(visitPayDocumentFilter).FirstOrDefault();
            return visitPayDocumentResult;
        }

        public void SavevisitPayDocumentId(VisitPayDocument visitPayDocumentIdDocument)
        {
            this._visitPayDocumentRepository.Value.InsertOrUpdate(visitPayDocumentIdDocument);
        }

        public void DeletevisitPayDocumentId(int visitPayDocumentId)
        {
            VisitPayDocument entity = this._visitPayDocumentRepository.Value.GetById(visitPayDocumentId);
            if (entity == null)
            {
                return;
            }

            this._visitPayDocumentRepository.Value.Delete(entity);
        }
    }
}
﻿namespace Ivh.Domain.Core.VisitPayDocument.Entities
{
    using System;
    using Common.VisitPay.Enums;

    public class VisitPayDocument
    {
        
        public VisitPayDocument()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        
        public virtual int VisitPayDocumentId { get; set; }
        public virtual int UploadedByVpUserId { get; set; }
        public virtual int? ApprovedByVpUserId { get; set; }
        public virtual DateTime InsertDate { get; set; } 
        public virtual DateTime? ActiveDate { get; set; }
        public virtual DateTime? ApprovedDate { get; set; }
        public virtual string AttachmentFileName { get; set; }
        public virtual string DisplayTitle { get; set; }
        public virtual string MimeType { get; set; }
        public virtual int FileSize { get; set; }
        public virtual byte[] FileContent { get; set; }
        public virtual VisitPayDocumentTypeEnum VisitPayDocumentType { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.Core.VisitPayDocument.Entities
{
    public class VisitPayDocumentType
    {
        public virtual int VisitPayDocumentTypeId { get; set; }
        public virtual string VisitPayDocumentTypeName { get; set; }
    }
}

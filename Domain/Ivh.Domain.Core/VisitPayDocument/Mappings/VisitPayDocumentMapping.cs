﻿namespace Ivh.Domain.Core.VisitPayDocument.Mappings
{
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class VisitPayDocumentMapping : ClassMap<VisitPayDocument>
    {
        public VisitPayDocumentMapping()
        {
            this.Schema("dbo");
            this.Table(nameof(VisitPayDocument));
            this.Id(x => x.VisitPayDocumentId);
            this.Map(x => x.UploadedByVpUserId);
            this.Map(x => x.ApprovedByVpUserId);
            this.Map(x => x.InsertDate);
            this.Map(x => x.ActiveDate);
            this.Map(x => x.ApprovedDate);

            this.Map(x => x.AttachmentFileName).Not.Nullable();
            this.Map(x => x.DisplayTitle);
            this.Map(x => x.MimeType).Not.Nullable();
            this.Map(x => x.FileSize).Not.Nullable();
            this.Map(x => x.FileContent)
                //.CustomSqlType("VARBINARY (MAX)")
                .Length(2147483647).Not.Nullable();

            this.Map(x => x.VisitPayDocumentType)
                .Column("VisitPayDocumentTypeId").CustomType<VisitPayDocumentTypeEnum>()
                .Not.Nullable();
        }
    }
}
﻿namespace Ivh.Domain.Core.SystemMessage.Entities
{
    using System;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class SystemMessage
    {
        public virtual int SystemMessageId { get; set; }
        public virtual SystemMessageTypeEnum SystemMessageType { get; set; }
        public virtual SystemMessageCriteriaEnum? SystemMessageCriteria { get; set; }
        public virtual CmsRegionEnum CmsRegion { get; set; }
        public virtual int DisplayOrder { get; set; }
        public virtual DateTime? DateActive { get; set; }
        public virtual DateTime? DateExpires { get; set; }
    }
}
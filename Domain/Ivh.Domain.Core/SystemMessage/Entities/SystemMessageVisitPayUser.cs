﻿namespace Ivh.Domain.Core.SystemMessage.Entities
{
	using System;

	public class SystemMessageVisitPayUser
	{
		public virtual int SystemMessageVisitPayUserId { get; set; }
		public virtual int VisitPayUserId { get; set; }
		public virtual int SystemMessageId { get; set; }
		public virtual bool MessageRead { get; set; }
		public virtual bool MessageHidden { get; set; }
		public virtual bool MessageDismissed { get; set; }
		public virtual DateTime? InsertDate { get; set; }

		public virtual bool IsDismissedOrHidden()
		{
			return this.MessageDismissed || this.MessageHidden;
		}

		public virtual SystemMessage SystemMessage { get; set; }
	}
}
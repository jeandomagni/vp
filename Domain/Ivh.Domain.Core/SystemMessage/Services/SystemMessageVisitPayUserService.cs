﻿namespace Ivh.Domain.Core.SystemMessage.Services
{
    using System;
    using System.Collections.Generic;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Enums;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.Core;
    using Interfaces;
    using Entities;
    using Guarantor.Interfaces;
    using NHibernate;

    public class SystemMessageVisitPayUserService : DomainService, ISystemMessageVisitPayUserService
    {
        private readonly ISession _session;
        private readonly Lazy<ISystemMessageVisitPayUserRepository> _systemMessageVisitPayUserRepository;
        private readonly Lazy<IGuarantorService> _guarantorService;

        public SystemMessageVisitPayUserService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            ISessionContext<VisitPay> sessionContext,
            Lazy<ISystemMessageVisitPayUserRepository> systemMessageVisitPayUserRepository,
            Lazy<IGuarantorService> guarantorService) : base(serviceCommonService)
        {
            this._session = sessionContext.Session;
            this._systemMessageVisitPayUserRepository = systemMessageVisitPayUserRepository;
            this._guarantorService = guarantorService;
        }

        public IList<SystemMessageVisitPayUser> GetActiveSystemMessagesVisitPayUser(int visitPayUserId)
        {
            IList<SystemMessageVisitPayUser> userMessages = this._systemMessageVisitPayUserRepository.Value.GetActiveSystemMessagesVisitPayUser(visitPayUserId);
            return userMessages;
        }

        public void InsertSystemMessageVisitPayUser(int visitPayUserId, SystemMessageEnum systemMessageEnum)
        {
            this.UpdateMessage(visitPayUserId, (int)systemMessageEnum, x => x.MessageHidden = false, x => x.MessageRead = false);
        }

        public void InsertSystemMessageVisitPayUserByGuarantorId(int vpGuarantorId, SystemMessageEnum systemMessageEnum)
        {
            int visitPayUserId = this._guarantorService.Value.GetVisitPayUserId(vpGuarantorId);
            this.InsertSystemMessageVisitPayUser(visitPayUserId, systemMessageEnum);
        }

        public void InsertSystemMessageVisitPayUsers(IList<AddSystemMessageVisitPayUserMessage> messages)
        {
            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                foreach (AddSystemMessageVisitPayUserMessage systemMessage in messages)
                {
                    this.InsertSystemMessageVisitPayUser(
                        systemMessage.VisitPayUserId,
                        systemMessage.SystemMessageEnum);
                }
                uow.Commit();
            }
        }

        public void InsertSystemMessageVisitPayUsers(IList<AddSystemMessageVisitPayUserByGuarantorMessage> messages)
        {
            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                foreach (AddSystemMessageVisitPayUserByGuarantorMessage systemMessage in messages)
                {
                    int visitPayUserId = this._guarantorService.Value.GetVisitPayUserId(systemMessage.VpGuarantorId);
                    this.InsertSystemMessageVisitPayUser(visitPayUserId, systemMessage.SystemMessageEnum);
                }
                uow.Commit();
            }
        }

        public void DismissMessage(int visitPayUserId, int systemMessageId)
        {
            this.UpdateMessage(visitPayUserId, systemMessageId, x => x.MessageDismissed = true);
        }

        public void HideMessage(int visitPayUserId, int systemMessageId)
        {
            this.UpdateMessage(visitPayUserId, systemMessageId, x => x.MessageHidden = true);
        }

        public void MarkMessageAsRead(int visitPayUserId, int systemMessageId)
        {
            this.UpdateMessage(visitPayUserId, systemMessageId, x => x.MessageRead = true);
        }

        private void UpdateMessage(int visitPayUserId, int systemMessageId, params Action<SystemMessageVisitPayUser>[] updateActions)
        {
            SystemMessageVisitPayUser message = this.GetSystemMessageVisitPayUser(visitPayUserId, systemMessageId);

            if (message == null)
            {
                return;
            }

            foreach (Action<SystemMessageVisitPayUser> updateAction in updateActions)
            {
                updateAction(message);
            }

            if (message.SystemMessageVisitPayUserId == default(int))
            {
	            message.InsertDate = DateTime.UtcNow;
            }
            this._systemMessageVisitPayUserRepository.Value.InsertOrUpdate(message);
        }

        public void MarkAllMessagesAsRead(IList<int> systemMessageIds, int visitPayUserId)
        {
            if (systemMessageIds == null || systemMessageIds.Count == 0)
            {
                return;
            }

            IList<SystemMessageVisitPayUser> userMessages = this._systemMessageVisitPayUserRepository.Value.GetActiveSystemMessagesVisitPayUser(visitPayUserId);

            foreach (SystemMessageVisitPayUser userMessage in userMessages)
            {
                if (userMessage.MessageRead)
                {
                    continue;
                }

                userMessage.MessageRead = true;
                this._systemMessageVisitPayUserRepository.Value.InsertOrUpdate(userMessage);
            }
        }
        public SystemMessageVisitPayUser GetSystemMessageVisitPayUser(int vpGuarantorId, SystemMessageEnum systemMessageEnum)
        {
            int visitPayUserId = this._guarantorService.Value.GetVisitPayUserId(vpGuarantorId);
            SystemMessageVisitPayUser message = this._systemMessageVisitPayUserRepository.Value.GetSystemMessageVisitPayUser(visitPayUserId, (int)systemMessageEnum);
            return message;
        }


        private SystemMessageVisitPayUser GetSystemMessageVisitPayUser(int visitPayUserId, int systemMessageId)
        {
            SystemMessageVisitPayUser message = this._systemMessageVisitPayUserRepository.Value.GetSystemMessageVisitPayUser(visitPayUserId, systemMessageId) ?? this.TryCreateSystemMessageVisitPayUser(visitPayUserId, systemMessageId);
            return message;
        }

        private SystemMessageVisitPayUser TryCreateSystemMessageVisitPayUser(int visitPayUserId, int systemMessageId)
        {
            bool messageIsActive = this._systemMessageVisitPayUserRepository.Value.SystemMessageIsEnabled(systemMessageId);

            if (messageIsActive)
            {
                return new SystemMessageVisitPayUser
                {
                    VisitPayUserId = visitPayUserId,
                    SystemMessageId = systemMessageId
                };

            }

            return null;
        }
    }
}
﻿namespace Ivh.Domain.Core.SystemMessage.Mappings
{
	using Entities;
	using FluentNHibernate.Mapping;

	public class SystemMessageVisitPayUserMap : ClassMap<SystemMessageVisitPayUser>
	{
		public SystemMessageVisitPayUserMap()
		{
			this.Schema("dbo");
			this.Table("SystemMessageVisitPayUser");
			this.Id(x => x.SystemMessageVisitPayUserId);
			this.Map(x => x.VisitPayUserId).Not.Nullable();
			this.Map(x => x.SystemMessageId).Not.Nullable();
			this.Map(x => x.MessageRead).Not.Nullable();
			this.Map(x => x.MessageHidden).Not.Nullable();
			this.Map(x => x.MessageDismissed).Not.Nullable();
			this.Map(x => x.InsertDate).Nullable();

			this.References(x => x.SystemMessage)
				.Column("SystemMessageId")
				.Not.Insert()
				.Not.Update();
		}
	}
}
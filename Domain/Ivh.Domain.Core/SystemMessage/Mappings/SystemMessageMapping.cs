﻿namespace Ivh.Domain.Core.SystemMessage.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class SystemMessageMapping : ClassMap<SystemMessage>
    {
        public SystemMessageMapping()
        {
            this.Schema("dbo");
            this.Table("SystemMessage");
            this.ReadOnly();
            this.Id(x => x.SystemMessageId).Not.Nullable();
            this.Map(x => x.SystemMessageType, "SystemMessageTypeId").CustomType<SystemMessageTypeEnum>().Not.Nullable();
            this.Map(x => x.SystemMessageCriteria, "SystemMessageCriteriaId").CustomType<SystemMessageCriteriaEnum?>().Nullable();
            this.Map(x => x.CmsRegion, "CmsRegionId").CustomType<CmsRegionEnum>().Not.Nullable();
            this.Map(x => x.DisplayOrder).Not.Nullable();
            this.Map(x => x.DateActive).Nullable();
            this.Map(x => x.DateExpires).Nullable();

            this.Cache.ReadOnly();
        }
    }
}
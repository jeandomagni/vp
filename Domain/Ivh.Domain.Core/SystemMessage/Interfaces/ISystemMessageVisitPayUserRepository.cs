﻿namespace Ivh.Domain.Core.SystemMessage.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface ISystemMessageVisitPayUserRepository : IRepository<SystemMessageVisitPayUser>
    {
        IList<SystemMessageVisitPayUser> GetActiveSystemMessagesVisitPayUser(int visitPayUserId);
        SystemMessageVisitPayUser GetSystemMessageVisitPayUser(int visitPayUserId, int systemMessageId);
        bool SystemMessageIsEnabled(int systemMessageId);
    }
}
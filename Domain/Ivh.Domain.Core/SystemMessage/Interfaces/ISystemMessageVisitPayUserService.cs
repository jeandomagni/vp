﻿namespace Ivh.Domain.Core.SystemMessage.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.Core;
    using Entities;

    public interface ISystemMessageVisitPayUserService : IDomainService
    {
        IList<SystemMessageVisitPayUser> GetActiveSystemMessagesVisitPayUser(int visitPayUserId);
        void InsertSystemMessageVisitPayUser(int visitPayUserId, SystemMessageEnum systemMessageEnum);
        void InsertSystemMessageVisitPayUserByGuarantorId(int vpGuarantorId, SystemMessageEnum systemMessageEnum);
        void InsertSystemMessageVisitPayUsers(IList<AddSystemMessageVisitPayUserMessage> messages);
        void InsertSystemMessageVisitPayUsers(IList<AddSystemMessageVisitPayUserByGuarantorMessage> messages);
        void DismissMessage(int visitPayUserId, int systemMessageId);
        void HideMessage(int visitPayUserId, int systemMessageId);
        void MarkMessageAsRead(int visitPayUserId, int systemMessageId);
        void MarkAllMessagesAsRead(IList<int> systemMessageIds, int visitPayUserId);
        SystemMessageVisitPayUser GetSystemMessageVisitPayUser(int vpGuarantorId, SystemMessageEnum systemMessageEnum);
    }
}
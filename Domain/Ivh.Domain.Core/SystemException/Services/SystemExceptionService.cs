﻿namespace Ivh.Domain.Core.SystemException.Services
{
    using System;
    using Base.Interfaces;
    using Base.Services;
    using Interfaces;
    using SystemException = Entities.SystemException;

    public class SystemExceptionService : DomainService, ISystemExceptionService
    {
        private readonly ISystemExceptionRepository _systemExceptionRepository;

        public SystemExceptionService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            ISystemExceptionRepository systemExceptionRepository) : base(serviceCommonService)
        {
            this._systemExceptionRepository = systemExceptionRepository;
        }

        public void LogSystemException(SystemException systemException)
        {
            this._systemExceptionRepository.InsertOrUpdate(systemException);
        }
    }
}

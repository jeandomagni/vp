﻿using System;

namespace Ivh.Domain.Core.SystemException.Entities
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class SystemException
    {
        public SystemException()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        //General Fields
        public virtual int SystemExceptionId { get; set; }
        public virtual SystemExceptionTypeEnum SystemExceptionType { get; set; }
        public virtual string SystemExceptionDescription { get; set; }
        public virtual DateTime InsertDate { get; set; }

        //Sparse Fields
        public virtual int? VpGuarantorId { get; set; }
        public virtual int? GpGuarantorId { get; set; }
        public virtual int? PaymentId { get; set; }
        public virtual int? GuestPayPaymentId { get; set; }
        public virtual string TransactionId { get; set; }
        public virtual int? FinancePlanId { get; set; }
    }
}

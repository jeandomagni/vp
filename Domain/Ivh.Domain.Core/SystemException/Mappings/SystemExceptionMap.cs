﻿namespace Ivh.Domain.Core.SystemException.Mappings
{
    using Common.Base.Enums;
    using Common.Data.Extensions;
    using Common.VisitPay.Enums;
    using FluentNHibernate.Mapping;
    using SystemException = Entities.SystemException;

    public class SystemExceptionMap : ClassMap<SystemException>
    {
        public SystemExceptionMap()
        {
            this.Schema("common");
            this.Table("SystemException");
            this.Id(x => x.SystemExceptionId);
            this.Map(x => x.SystemExceptionType, "SystemExceptionTypeId").CustomType<SystemExceptionTypeEnum>().Not.Nullable();
            this.Map(x => x.SystemExceptionDescription).Not.Nullable().AsNVarcharMax();
            this.Map(x => x.InsertDate);


            this.Map(x => x.VpGuarantorId).Nullable();
            this.Map(x => x.GpGuarantorId).Nullable();
            this.Map(x => x.PaymentId).Nullable();
            this.Map(x => x.GuestPayPaymentId).Nullable();
            this.Map(x => x.TransactionId).Nullable();
            this.Map(x => x.FinancePlanId).Nullable();
        }
    }
}
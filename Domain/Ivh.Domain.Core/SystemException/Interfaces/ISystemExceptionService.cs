﻿namespace Ivh.Domain.Core.SystemException.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface ISystemExceptionService : IDomainService
    {
        void LogSystemException(SystemException systemException);
    }
}

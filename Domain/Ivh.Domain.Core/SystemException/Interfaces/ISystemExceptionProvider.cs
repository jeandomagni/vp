﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.Core.SystemException.Interfaces
{
    using Entities;

    public interface ISystemExceptionProvider
    {
        IEnumerable<SystemException> GetExceptions(DateTime exceptionsAsOfDateTime);
    }
}

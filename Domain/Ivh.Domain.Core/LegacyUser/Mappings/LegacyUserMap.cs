﻿namespace Ivh.Domain.Core.LegacyUser.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class LegacyUserMap : ClassMap<LegacyUser>
    {
        public LegacyUserMap()
        {
            this.Schema("dbo");
            this.Table("LegacyUser");
            this.Id(x => x.LegacyUserId);

            this.Map(x => x.IsMigrated);
            this.Map(x => x.UserName);
            this.Map(x => x.VpGuarantorId).Nullable();

            this.ReadOnly();
        }
    }
}
﻿namespace Ivh.Domain.Core.LegacyUser.Services
{
    using System;
    using Base.Interfaces;
    using Base.Services;
    using Interfaces;

    public class LegacyUserService : DomainService, ILegacyUserService
    {
        private readonly ILegacyUserRepository _legacyUserRepository;

        public LegacyUserService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            ILegacyUserRepository legacyUserRepository) : base(serviceCommonService)
        {
            this._legacyUserRepository = legacyUserRepository;
        }

        public bool IsNonMigratedLegacyUser(string username)
        {
            return this._legacyUserRepository.IsNonMigratedLegacyUser(username);
        }
    }
}
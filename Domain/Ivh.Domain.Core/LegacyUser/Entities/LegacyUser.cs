﻿namespace Ivh.Domain.Core.LegacyUser.Entities
{
    public class LegacyUser
    {
        public virtual int LegacyUserId { get; set; }

        public virtual int? VpGuarantorId { get; set; }

        public virtual string UserName { get; set; }

        public virtual bool IsMigrated { get; set; }
    }
}
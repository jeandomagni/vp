﻿namespace Ivh.Domain.Core.LegacyUser.Interfaces
{
    public interface ILegacyUserRepository
    {
        bool IsNonMigratedLegacyUser(string username);
    }
}
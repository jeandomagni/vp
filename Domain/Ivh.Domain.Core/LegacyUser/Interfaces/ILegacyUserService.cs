﻿namespace Ivh.Domain.Core.LegacyUser.Interfaces
{
    using Common.Base.Interfaces;

    public interface ILegacyUserService : IDomainService
    {
        bool IsNonMigratedLegacyUser(string username);
    }
}
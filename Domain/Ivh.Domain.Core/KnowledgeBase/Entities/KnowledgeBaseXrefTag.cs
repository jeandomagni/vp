﻿namespace Ivh.Domain.Core.KnowledgeBase.Entities
{
    using System;

    public class KnowledgeBaseXrefTag : IEquatable<KnowledgeBaseXrefTag>
    {
        public virtual KnowledgeBaseSubCategoryQuestionAnswerXref KnowledgeBaseSubCategoryQuestionAnswerXref { get; set; }

        public virtual KnowledgeBaseTag KnowledgeBaseTag { get; set; }

        #region Equality Members

        public virtual bool Equals(KnowledgeBaseXrefTag other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }
            return this.KnowledgeBaseSubCategoryQuestionAnswerXref?.KnowledgeBaseSubCategoryQuestionAnswerXrefId == other.KnowledgeBaseSubCategoryQuestionAnswerXref?.KnowledgeBaseSubCategoryQuestionAnswerXrefId 
                && this.KnowledgeBaseTag?.KnowledgeBaseTagId == other.KnowledgeBaseTag?.KnowledgeBaseTagId;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (this.KnowledgeBaseSubCategoryQuestionAnswerXref.KnowledgeBaseSubCategoryQuestionAnswerXrefId * 397) ^ this.KnowledgeBaseTag.KnowledgeBaseTagId;
            }
        }

        public static bool operator ==(KnowledgeBaseXrefTag left, KnowledgeBaseXrefTag right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(KnowledgeBaseXrefTag left, KnowledgeBaseXrefTag right)
        {
            return !Equals(left, right);
        }

        public override bool Equals(object obj)
        {
            return obj?.GetType() == this.GetType() && this.Equals((KnowledgeBaseXrefTag)obj);
        }

        #endregion Equality Members
    }
}

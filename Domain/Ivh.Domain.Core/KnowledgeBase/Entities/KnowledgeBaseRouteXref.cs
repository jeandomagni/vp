﻿namespace Ivh.Domain.Core.KnowledgeBase.Entities
{
    public class KnowledgeBaseRouteXref
    {
        public virtual KnowledgeBaseSubCategoryQuestionAnswerXref Xref { get; set; }

        public virtual KnowledgeBaseRoute Route { get; set; }

        #region Equality Members

        public virtual bool Equals(KnowledgeBaseRouteXref other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }
            return this.Xref?.KnowledgeBaseSubCategoryQuestionAnswerXrefId == other.Xref?.KnowledgeBaseSubCategoryQuestionAnswerXrefId
                   && this.Route?.KnowledgeBaseRouteId == other.Route?.KnowledgeBaseRouteId;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (this.Xref.KnowledgeBaseSubCategoryQuestionAnswerXrefId * 397) ^ this.Route.KnowledgeBaseRouteId;
            }
        }

        public static bool operator ==(KnowledgeBaseRouteXref left, KnowledgeBaseRouteXref right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(KnowledgeBaseRouteXref left, KnowledgeBaseRouteXref right)
        {
            return !Equals(left, right);
        }

        public override bool Equals(object obj)
        {
            return obj?.GetType() == this.GetType() && this.Equals((KnowledgeBaseRouteXref)obj);
        }

        #endregion Equality Members
    }
}

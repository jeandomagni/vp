﻿
namespace Ivh.Domain.Core.KnowledgeBase.Entities
{
    using System;

    public class SearchTextLog
    {
        public virtual int SearchTextId { get; set; }
        public virtual string SearchText { get; set; }
        public virtual DateTime Date { get; set; }
    }
}

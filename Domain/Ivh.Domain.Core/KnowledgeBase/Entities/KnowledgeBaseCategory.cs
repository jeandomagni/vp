﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.Core.KnowledgeBase.Entities
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class KnowledgeBaseCategory
    {
        public virtual int KnowledgeBaseCategoryId { get; set; }
        public virtual string Name { get; set; }
        public virtual CmsRegionEnum CmsRegion { get; set; }
        public virtual IList<KnowledgeBaseSubCategory> KnowledgeBaseSubCategories { get;set;}
        public virtual bool IsActive { get; set; }
        public virtual int DisplayOrder { get; set; }
        public virtual string IconCssClass { get; set; }
    }
}

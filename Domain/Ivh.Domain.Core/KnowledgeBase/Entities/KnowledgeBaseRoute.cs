﻿namespace Ivh.Domain.Core.KnowledgeBase.Entities
{
    public class KnowledgeBaseRoute
    {
        public virtual int KnowledgeBaseRouteId { get; set; }
        public virtual string Controller { get; set; }
        public virtual string Action { get; set; }
    }
}

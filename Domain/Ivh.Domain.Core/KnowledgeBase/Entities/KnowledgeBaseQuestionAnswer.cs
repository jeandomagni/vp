﻿namespace Ivh.Domain.Core.KnowledgeBase.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class KnowledgeBaseQuestionAnswer
    {
        public virtual int KnowledgeBaseQuestionAnswerId { get; set; }
        public virtual CmsRegionEnum QuestionCmsRegion { get; set; }
        public virtual CmsRegionEnum AnswerCmsRegion { get; set; }
        public virtual bool IsMobileKnowledgeBase { get; set; }
        public virtual bool IsWebKnowledgeBase { get; set; }
        public virtual bool IsClientKnowledgeBase { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual IList<KnowledgeBaseSubCategoryQuestionAnswerXref> KnowledgeBaseSubCategoryQuestionAnswerXrefs { get; set; }
        public virtual IList<KnowledgeBaseQuestionAnswerVote> KnowledgeBaseQuestionAnswerVotes { get; set; }

        public virtual int Rank(int? windowInDays)
        {
            List<KnowledgeBaseQuestionAnswerVote> questionAnswerVotes = this.KnowledgeBaseQuestionAnswerVotes.ToList();
            if (windowInDays.HasValue)
            {
                DateTime startDate = DateTime.UtcNow.AddDays(windowInDays.Value * -1);
                questionAnswerVotes = questionAnswerVotes.Where(x => x.InsertDate >= startDate)
                .Where(x => DateTime.UtcNow.AddDays(windowInDays.Value) >= x.InsertDate).ToList();
            }
            if (questionAnswerVotes.Any())
            {
                return questionAnswerVotes.Sum(x => x.VoteValue);
            }
            return 0;
        }
    }
}
﻿namespace Ivh.Domain.Core.KnowledgeBase.Entities
{
    using System;
    using System.Collections.Generic;

    public class KnowledgeBaseQuestionAnswerVote
    {

        public KnowledgeBaseQuestionAnswerVote()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        
        public virtual int KnowledgeBaseQuestionAnswerVoteId { get; set; }
            
        public virtual int KnowledgeBaseQuestionAnswerId { get; set; }
         
        public virtual int VoteValue { get; set; }
              
        public virtual DateTime InsertDate { get; set; }
        
        public virtual bool VisitPaySeeded { get; set; }

    }
}

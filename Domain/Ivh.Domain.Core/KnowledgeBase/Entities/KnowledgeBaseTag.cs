﻿namespace Ivh.Domain.Core.KnowledgeBase.Entities
{
    public class KnowledgeBaseTag
    {
        public virtual int KnowledgeBaseTagId { get; set; }
        public virtual string KnowledgeBaseTagName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.Core.KnowledgeBase.Entities
{
    public class KnowledgeBaseSubCategoryQuestionAnswerXref 
    {
        private IList<KnowledgeBaseXrefTag> _tags;
        private IList<KnowledgeBaseRouteXref> _routes;
        public KnowledgeBaseSubCategoryQuestionAnswerXref()
        {
            this._tags = new List<KnowledgeBaseXrefTag>();
            this._routes = new List<KnowledgeBaseRouteXref>();
        }

        public virtual int KnowledgeBaseSubCategoryQuestionAnswerXrefId { get; set; }
        public virtual KnowledgeBaseSubCategory KnowledgeBaseSubCategory { get; set; }
        public virtual KnowledgeBaseQuestionAnswer KnowledgeBaseQuestionAnswer { get; set; }
        public virtual int DisplayOrder { get; set; }

        public virtual IList<KnowledgeBaseXrefTag> Tags
        {
            get { return this._tags; }
            set { this._tags = value; }
        }

        public virtual IList<KnowledgeBaseRouteXref> Routes
        {
            get { return this._routes; }
            set { this._routes = value; }
        }
    }
}

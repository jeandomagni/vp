﻿namespace Ivh.Domain.Core.KnowledgeBase.Entities
{
    using System;

    public class KnowledgeBaseQuestionAnswerFeedback
    {

        public KnowledgeBaseQuestionAnswerFeedback()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        public virtual int KnowledgeBaseQuestionAnswerFeedbackId { get; set; }
        public virtual int KnowledgeBaseQuestionAnswerId { get; set; }
        public virtual string KnowledgeBaseQuestionFeebackText { get; set; }
        public virtual DateTime InsertDate { get; set; }
    }
}
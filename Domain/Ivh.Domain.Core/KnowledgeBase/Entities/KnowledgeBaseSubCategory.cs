﻿namespace Ivh.Domain.Core.KnowledgeBase.Entities
{
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class KnowledgeBaseSubCategory
    {
        public virtual int KnowledgeBaseSubCategoryId { get; set; }
        public virtual KnowledgeBaseCategory KnowledgeBaseCategory { get; set; }
        public virtual CmsRegionEnum? NameCmsRegion { get; set; }
        public virtual IList<KnowledgeBaseSubCategoryQuestionAnswerXref> KnowledgeBaseSubCategoryQuestionAnswerXref { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual int DisplayOrder { get; set; }
    }
}
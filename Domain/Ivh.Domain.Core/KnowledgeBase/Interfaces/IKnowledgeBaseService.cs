﻿namespace Ivh.Domain.Core.KnowledgeBase.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IKnowledgeBaseService : IDomainService
    {
        IList<KnowledgeBaseCategory> GetActiveKnowledgeBaseCategories();

        void SaveKnowledgeBaseQuestionAnswerVote(int knowledgeBaseQuestionAnswerId, int voteValue);
        void SaveKnowledgeBaseQuestionAnswerFeedback(int knowledgeBaseQuestionAnswerId, string feedbackText);

        IList<KnowledgeBaseQuestionAnswer> GetTopVotedKnowledgeBaseQuestionAnswers(int windowInDays, int numberOfQuestions);
    }
}
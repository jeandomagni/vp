﻿namespace Ivh.Domain.Core.KnowledgeBase.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IKnowledgeBaseQuestionAnswerFeedbackRepository : IRepository<KnowledgeBaseQuestionAnswerFeedback>
    {
    }
}
﻿namespace Ivh.Domain.Core.KnowledgeBase.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IKnowledgeBaseQuestionAnswerRepository : IRepository<KnowledgeBaseQuestionAnswer>
    {
        IList<KnowledgeBaseQuestionAnswer> GetTopVotedKnowledgeBaseQuestionAnswers(int windowInDays, int numberOfQuestions);
    }
}
﻿namespace Ivh.Domain.Core.KnowledgeBase.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IKnowledgeBaseCategoryRepository : IRepository<KnowledgeBaseCategory>
    {
        IList<KnowledgeBaseCategory> GetActiveKnowledgeBaseCategories();
    }
}
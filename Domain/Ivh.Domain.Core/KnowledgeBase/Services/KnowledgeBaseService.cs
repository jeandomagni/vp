﻿namespace Ivh.Domain.Core.KnowledgeBase.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Entities;
    using Interfaces;

    public class KnowledgeBaseService : DomainService, IKnowledgeBaseService
    {
        private readonly Lazy<IKnowledgeBaseCategoryRepository> _knowledgeBaseCategoryRepository;
        private readonly Lazy<IKnowledgeBaseQuestionAnswerVoteRepository> _knowledgeBaseQuestionAnswerVoteRepository;
        private readonly Lazy<IKnowledgeBaseQuestionAnswerFeedbackRepository> _knowledgeBaseQuestionAnswerFeedbackRepository;
        private readonly Lazy<IKnowledgeBaseQuestionAnswerRepository> _knowledgeBaseQuestionAnswerRepository;


        public KnowledgeBaseService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IKnowledgeBaseCategoryRepository> knowledgeBaseCategoryRepository,
            Lazy<IKnowledgeBaseQuestionAnswerVoteRepository> knowledgeBaseQuestionAnswerVoteRepository,
            Lazy<IKnowledgeBaseQuestionAnswerFeedbackRepository> knowledgeBaseQuestionAnswerFeedbackRepository,
            Lazy<IKnowledgeBaseQuestionAnswerRepository> knowledgeBaseQuestionAnswerRepository
            ) : base(serviceCommonService)
        {
            this._knowledgeBaseCategoryRepository = knowledgeBaseCategoryRepository;
            this._knowledgeBaseQuestionAnswerVoteRepository = knowledgeBaseQuestionAnswerVoteRepository;
            this._knowledgeBaseQuestionAnswerFeedbackRepository = knowledgeBaseQuestionAnswerFeedbackRepository;
            this._knowledgeBaseQuestionAnswerRepository = knowledgeBaseQuestionAnswerRepository;
        }

        public IList<KnowledgeBaseCategory> GetActiveKnowledgeBaseCategories()
        {
            return this._knowledgeBaseCategoryRepository.Value.GetActiveKnowledgeBaseCategories().OrderBy(x => x.DisplayOrder).ToList();
        }

        public void SaveKnowledgeBaseQuestionAnswerVote(int knowledgeBaseQuestionAnswerId, int voteValue)
        {
            KnowledgeBaseQuestionAnswerVote knowledgeBaseQuestionAnswerVote = new KnowledgeBaseQuestionAnswerVote
            {
                KnowledgeBaseQuestionAnswerId = knowledgeBaseQuestionAnswerId,
                VoteValue = voteValue
            };
            
            this._knowledgeBaseQuestionAnswerVoteRepository.Value.InsertOrUpdate(knowledgeBaseQuestionAnswerVote);
        }

        public void SaveKnowledgeBaseQuestionAnswerFeedback(int knowledgeBaseQuestionAnswerId, string feedbackText)
        {
            KnowledgeBaseQuestionAnswerFeedback knowledgeBaseQuestionAnswerFeedback = new KnowledgeBaseQuestionAnswerFeedback
            {
                KnowledgeBaseQuestionAnswerId = knowledgeBaseQuestionAnswerId,
                KnowledgeBaseQuestionFeebackText = feedbackText
            };
            
            this._knowledgeBaseQuestionAnswerFeedbackRepository.Value.InsertOrUpdate(knowledgeBaseQuestionAnswerFeedback);
              
        }

        public IList<KnowledgeBaseQuestionAnswer> GetTopVotedKnowledgeBaseQuestionAnswers(int windowInDays, int numberOfQuestions)
        {
            return this._knowledgeBaseQuestionAnswerRepository.Value.GetTopVotedKnowledgeBaseQuestionAnswers(windowInDays, numberOfQuestions);
        }
    }
}
﻿namespace Ivh.Domain.Core.KnowledgeBase.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class KnowledgeBaseXrefTagMapping : ClassMap<KnowledgeBaseXrefTag>
    {
        public KnowledgeBaseXrefTagMapping()
        {
            this.Schema("dbo");
            this.Table("KnowledgeBaseXrefTag");
            this.CompositeId()
                .KeyReference(xt => xt.KnowledgeBaseSubCategoryQuestionAnswerXref, "KnowledgeBaseSubCategoryQuestionAnswerXrefId")
                .KeyReference(xt => xt.KnowledgeBaseTag, "KnowledgeBaseTagId");

            this.Cache.ReadOnly();
        }
    }
}

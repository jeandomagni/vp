﻿namespace Ivh.Domain.Core.KnowledgeBase.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class KnowledgeBaseQuestionAnswerVoteMapping : ClassMap<KnowledgeBaseQuestionAnswerVote>
    {
        public KnowledgeBaseQuestionAnswerVoteMapping()
        {
            this.Schema("dbo");
            this.Table(nameof(KnowledgeBaseQuestionAnswerVote));
            this.Id(x => x.KnowledgeBaseQuestionAnswerVoteId); 
            this.Map(x => x.KnowledgeBaseQuestionAnswerId);
            this.Map(x => x.InsertDate);
            this.Map(x => x.VoteValue);
            this.Map(x => x.VisitPaySeeded);
        }
    }
}
﻿namespace Ivh.Domain.Core.KnowledgeBase.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class KnowledgeBaseCategoryMapping : ClassMap<KnowledgeBaseCategory>
    {
        public KnowledgeBaseCategoryMapping()
        {
            this.Schema("dbo");
            this.Table("KnowledgeBaseCategory");
            this.Id(x => x.KnowledgeBaseCategoryId).Not.Nullable();
            this.Map(x => x.Name);
            this.Map(x => x.CmsRegion, "CmsRegionId").CustomType<CmsRegionEnum>().Not.Nullable();
            this.Map(x => x.IsActive);
            this.Map(x => x.DisplayOrder);
            this.Map(x => x.IconCssClass);

            this.HasMany(x => x.KnowledgeBaseSubCategories)
                .KeyColumn("KnowledgeBaseCategoryId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();

            this.Cache.ReadOnly();
        }
    }
}
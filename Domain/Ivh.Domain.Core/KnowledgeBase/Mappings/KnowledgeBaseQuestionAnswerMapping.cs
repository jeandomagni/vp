﻿namespace Ivh.Domain.Core.KnowledgeBase.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class KnowledgeBaseQuestionAnswerMapping : ClassMap<KnowledgeBaseQuestionAnswer>
    {
        public KnowledgeBaseQuestionAnswerMapping()
        {
            this.Schema("dbo");
            this.Table("KnowledgeBaseQuestionAnswer");
            this.Id(x => x.KnowledgeBaseQuestionAnswerId).Not.Nullable();
            this.Map(x => x.QuestionCmsRegion, "QuestionCmsRegionId").CustomType<CmsRegionEnum>().Not.Nullable();
            this.Map(x => x.AnswerCmsRegion, "AnswerCmsRegionId").CustomType<CmsRegionEnum>().Not.Nullable();
            this.Map(x => x.IsActive);
            this.Map(x => x.IsClientKnowledgeBase);
            this.Map(x => x.IsMobileKnowledgeBase);
            this.Map(x => x.IsWebKnowledgeBase);
            this.HasMany(x => x.KnowledgeBaseSubCategoryQuestionAnswerXrefs).KeyColumn("KnowledgeBaseQuestionAnswerId");
            this.HasMany(x => x.KnowledgeBaseQuestionAnswerVotes).KeyColumn("KnowledgeBaseQuestionAnswerId");
            //NOTE:Intentionally not mapping KnowledgeBaseQuestionAnswerFeedback for performance purposes
            this.Cache.ReadOnly();
        }
    }
}

﻿namespace Ivh.Domain.Core.KnowledgeBase.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class KnowledgeBaseRouteMapping : ClassMap<KnowledgeBaseRoute>
    {
        public KnowledgeBaseRouteMapping()
        {
            this.Schema("dbo");
            this.Table("KnowledgeBaseRoute");

            this.Id(x => x.KnowledgeBaseRouteId);
            this.Map(x => x.Controller);
            this.Map(x => x.Action);

            this.Cache.ReadOnly();
        }
    }
}

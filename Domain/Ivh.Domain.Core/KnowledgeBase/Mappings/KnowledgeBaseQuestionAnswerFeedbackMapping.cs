﻿namespace Ivh.Domain.Core.KnowledgeBase.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class KnowledgeBaseQuestionAnswerFeedbackMapping : ClassMap<KnowledgeBaseQuestionAnswerFeedback>
    {
        public KnowledgeBaseQuestionAnswerFeedbackMapping()
        {
            this.Schema("dbo");
            this.Table(nameof(KnowledgeBaseQuestionAnswerFeedback));
            this.Id(x => x.KnowledgeBaseQuestionAnswerFeedbackId);
            this.Map(x => x.KnowledgeBaseQuestionAnswerId);
            this.Map(x => x.KnowledgeBaseQuestionFeebackText);
            this.Map(x => x.InsertDate);
        }
    }
}
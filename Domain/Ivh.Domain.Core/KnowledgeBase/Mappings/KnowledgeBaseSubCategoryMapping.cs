﻿namespace Ivh.Domain.Core.KnowledgeBase.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class KnowledgeBaseSubCategoryMapping : ClassMap<KnowledgeBaseSubCategory>
    {
        public KnowledgeBaseSubCategoryMapping()
        {
            this.Schema("dbo");
            this.Table("KnowledgeBaseSubCategory");
            this.Id(x => x.KnowledgeBaseSubCategoryId).Not.Nullable();
            this.Map(x => x.NameCmsRegion)
                .Column("NameCmsRegionId")
                .CustomType<CmsRegionEnum>()
                .Nullable();
            this.References(x => x.KnowledgeBaseCategory).Column("KnowledgeBaseCategoryId");
            this.Map(x => x.IsActive);
            this.Map(x => x.DisplayOrder);

            this.HasMany(x => x.KnowledgeBaseSubCategoryQuestionAnswerXref)
                .KeyColumn("KnowledgeBaseSubCategoryId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();

            this.Cache.ReadOnly();
        }
    }
}

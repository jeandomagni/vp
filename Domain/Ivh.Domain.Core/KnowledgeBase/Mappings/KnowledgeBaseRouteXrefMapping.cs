﻿namespace Ivh.Domain.Core.KnowledgeBase.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class KnowledgeBaseRouteXrefMapping : ClassMap<KnowledgeBaseRouteXref>
    {
        public KnowledgeBaseRouteXrefMapping()
        {
            this.Schema("dbo");
            this.Table("KnowledgeBaseRouteXref");
            this.CompositeId()
                .KeyReference(xt => xt.Xref, "KnowledgeBaseSubCategoryQuestionAnswerXrefId")
                .KeyReference(xt => xt.Route, "KnowledgeBaseRouteId");

            this.Cache.ReadOnly();
        }
    }
}

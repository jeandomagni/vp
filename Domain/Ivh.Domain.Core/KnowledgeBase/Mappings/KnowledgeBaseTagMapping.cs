﻿namespace Ivh.Domain.Core.KnowledgeBase.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class KnowledgeBaseTagMapping : ClassMap<KnowledgeBaseTag>
    {
        public KnowledgeBaseTagMapping()
        {
            this.Schema("dbo");
            this.Table("KnowledgeBaseTag");

            this.Id(x => x.KnowledgeBaseTagId);
            this.Map(x => x.KnowledgeBaseTagName);

            this.Cache.ReadOnly();
        }
    }
}

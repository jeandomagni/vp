﻿namespace Ivh.Domain.Core.KnowledgeBase.Mappings
{
    using Common.Base.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class KnowledgeBaseSubCategoryQuestionAnswerXrefMapping : ClassMap<KnowledgeBaseSubCategoryQuestionAnswerXref>
    {
        public KnowledgeBaseSubCategoryQuestionAnswerXrefMapping()
        {
            this.Schema("dbo");
            this.Table("KnowledgeBaseSubCategoryQuestionAnswerXref");
            this.Id(x => x.KnowledgeBaseSubCategoryQuestionAnswerXrefId).Not.Nullable();
            this.References(x => x.KnowledgeBaseSubCategory).Column("KnowledgeBaseSubCategoryId");
            this.References(x => x.KnowledgeBaseQuestionAnswer).Column("KnowledgeBaseQuestionAnswerId");
            this.Map(x => x.DisplayOrder).Column("DisplayOrder");

            this.HasMany(x => x.Tags)
                .KeyColumn("KnowledgeBaseSubCategoryQuestionAnswerXrefId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();

            this.HasMany(x => x.Routes)
                .KeyColumn("KnowledgeBaseSubCategoryQuestionAnswerXrefId")
                .Inverse()
                .Not.KeyNullable()
                .Not.KeyUpdate()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.AllDeleteOrphan();

            this.Cache.ReadOnly();
        }
    }
}

﻿namespace Ivh.Domain.Core.KnowledgeBase.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class SearchTextLogMapping : ClassMap<SearchTextLog>
    {
        public SearchTextLogMapping()
        {
            this.Schema("dbo");
            this.Table("SearchTextLog");
            this.Id(x => x.SearchTextId).Column("SearchTextId");
            this.Map(x => x.SearchText).Not.Nullable();
            this.Map(x => x.Date).Not.Nullable();
        }
    }
}
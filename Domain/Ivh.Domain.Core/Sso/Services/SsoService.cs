﻿namespace Ivh.Domain.Core.Sso.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using SystemMessage.Interfaces;
    using Application.Core.Common.Dtos;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Common.Base.Utilities.Helpers;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Content.Interfaces;
    using Encryption.Interfaces;
    using Entities;
    using Guarantor.Entities;
    using Guarantor.Interfaces;
    using Interfaces;
    using Logging.Interfaces;
    using Settings.Interfaces;
    using User.Interfaces;

    public class SsoService : DomainService, ISsoService
    {
        private const int ApiTimeoutSeconds = 15;

        private readonly Lazy<ISsoProviderRepository> _ssoProviderRepository;
        private readonly Lazy<IVisitPayUserRepository> _visitPayUserRepository;
        private readonly Lazy<IContentService> _contentService;
        private readonly Lazy<IEnumerable<IEncryptionService>> _encryptionServices;
        private readonly Lazy<IEnumerable<ISsoSpecificService>> _ssoSpecificServices;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<ISystemMessageVisitPayUserService> _systemMessageVisitPayUserService;
        private readonly Lazy<ISsoVisitPayUserSettingRepository> _ssoVisitPayUserSettingRepository;

        private IEncryptionService GetEncryptionServiceForProvider(SsoProviderEnum providerEnum)
        {
            return this._encryptionServices.Value.FirstOrDefault(x => x.ProviderEnum == providerEnum);
        }
        private ISsoSpecificService GetSsoSpecificServiceForProvider(SsoProviderEnum providerEnum)
        {
            return this._ssoSpecificServices.Value.FirstOrDefault(x => x.ProviderEnum == providerEnum);
        }

        public SsoService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<ISsoVisitPayUserSettingRepository> ssoVisitPayUserSettingRepository,
            Lazy<ISsoProviderRepository> ssoProviderRepository,
            Lazy<IVisitPayUserRepository> visitPayUserRepository,
            Lazy<IContentService> contentService,
            Lazy<IEnumerable<IEncryptionService>> encryptionServices,
            Lazy<IEnumerable<ISsoSpecificService>> ssoSpecificServices,
            Lazy<IGuarantorService> guarantorService,
            Lazy<ISystemMessageVisitPayUserService> systemMessageVisitPayUserService) : base(serviceCommonService)
        {
            this._ssoVisitPayUserSettingRepository = ssoVisitPayUserSettingRepository;
            this._ssoProviderRepository = ssoProviderRepository;
            this._visitPayUserRepository = visitPayUserRepository;
            this._contentService = contentService;
            this._encryptionServices = encryptionServices;
            this._ssoSpecificServices = ssoSpecificServices;
            this._guarantorService = guarantorService;
            this._systemMessageVisitPayUserService = systemMessageVisitPayUserService;
        }

        public SsoProvider GetProvider(SsoProviderEnum ssoProvider)
        {
            return this._ssoProviderRepository.Value.GetById((int)ssoProvider);
        }

        public IList<SsoProvider> GetProviders()
        {
            IList<SsoProvider> ssoProviders = this._ssoProviderRepository.Value.GetQueryable().ToList();

            return ssoProviders.Where(x => x.RequiredFeatures.All(this.FeatureService.Value.IsFeatureEnabled)).ToList();
        }

        public SsoVisitPayUserSetting GetUserSettings(SsoProviderEnum ssoProvider, int visitPayUserId)
        {
            return this._ssoVisitPayUserSettingRepository.Value.GetByVisitPayUserIdAndProvider(ssoProvider, visitPayUserId);
        }

        public IList<SsoVisitPayUserSetting> GetUserSettings(int visitPayUserId)
        {
            return this._ssoVisitPayUserSettingRepository.Value.GetByVisitPayUserId(visitPayUserId);
        }

        public SsoVisitPayUserSetting AcceptSso(SsoProviderEnum ssoProvider, int visitPayUserId, int? actionTakenByVisitPayUserId = null)
        {
            SsoVisitPayUserSetting userSetting = this.GetUserSetting(ssoProvider, visitPayUserId, actionTakenByVisitPayUserId);
            userSetting.SsoStatus = SsoStatusEnum.Accepted;
            userSetting.SetIgnore(false);

            this._ssoVisitPayUserSettingRepository.Value.InsertOrUpdate(userSetting);
            int systemMessageId = (int)(ssoProvider == SsoProviderEnum.OpenEpic ? SystemMessageEnum.Sso : SystemMessageEnum.Hqy);
            this._systemMessageVisitPayUserService.Value.DismissMessage(visitPayUserId, systemMessageId);
            return userSetting;
        }

        public SsoVisitPayUserSetting DeclineSso(SsoProviderEnum ssoProvider, int visitPayUserId, int? actionTakenByVisitPayUserId = null)
        {
            SsoVisitPayUserSetting userSetting = this.GetUserSetting(ssoProvider, visitPayUserId, actionTakenByVisitPayUserId);
            userSetting.SsoStatus = SsoStatusEnum.Rejected;

            this._ssoVisitPayUserSettingRepository.Value.InsertOrUpdate(userSetting);
            ISsoSpecificService ssoSpecificService = this.GetSsoSpecificServiceForProvider(ssoProvider);

            if (ssoSpecificService != null)
            {
                Guarantor guarantor = this._guarantorService.Value.GetGuarantorEx(visitPayUserId);
                ssoSpecificService.RemoveAllVerificationsForVpGuarantor(guarantor);
            }

            return userSetting;
        }

        public SsoVisitPayUserSetting IgnoreSso(SsoProviderEnum ssoProvider, int visitPayUserId, bool ignore, int? actionTakenByVisitPayUserId = null)
        {
            SsoVisitPayUserSetting userSetting = this.GetUserSetting(ssoProvider, visitPayUserId, actionTakenByVisitPayUserId);
            if (userSetting == null)
            {
                return null;
            }

            userSetting.SetIgnore(ignore);
            this._ssoVisitPayUserSettingRepository.Value.Insert(userSetting);
            return userSetting;
        }

        public SsoVisitPayUserSetting InvalidateSso(SsoProviderEnum ssoProvider, int visitPayUserId)
        {
            SsoVisitPayUserSetting userSetting = this.GetUserSetting(ssoProvider, visitPayUserId, SystemUsers.SystemUserId);
            userSetting.SsoStatus = SsoStatusEnum.Invalidated;

            this._ssoVisitPayUserSettingRepository.Value.InsertOrUpdate(userSetting);

            return userSetting;
        }

        public SsoVisitPayUserSetting AcknowledgeInvalidatedSso(SsoProviderEnum ssoProvider, int visitPayUserId, int? actionTakenByVisitPayUserId = null)
        {
            SsoVisitPayUserSetting userSetting = this.GetUserSetting(ssoProvider, visitPayUserId, actionTakenByVisitPayUserId);
            userSetting.SsoStatus = SsoStatusEnum.Invalidated_Acknowledged;

            this._ssoVisitPayUserSettingRepository.Value.InsertOrUpdate(userSetting);

            return userSetting;
        }

        public string GenerateEncryptedSsoToken(string firstName, string lastName, SsoProviderEnum providerEnum, string ssoSsk, DateTime dob)
        {
            IEncryptionService encryptionProvider = this.GetEncryptionServiceForProvider(providerEnum);
            ISsoSpecificService ssoSpecificService = this.GetSsoSpecificServiceForProvider(providerEnum);

            string sampleRequestString = ssoSpecificService.GenerateSampleRequest(firstName, lastName, ssoSsk, dob);

            return encryptionProvider.Encrypt(sampleRequestString);
        }

        public SsoResponseDto SignInWithSso(SsoRequestDto request)
        {

            if (request == null)
            {
                return new SsoResponseDto() { ProviderEnum = SsoProviderEnum.Unknown, IsError = true };
            }

            SsoResponseDto response = new SsoResponseDto() { ProviderEnum = request.ProviderEnum };
            SsoProvider provider = this.GetProvider(request.ProviderEnum);
            if (!provider.IsEnabled)
            {
                response.IsError = true;
                return response;
            }

            string decryptedValue = string.Empty;
            if (request.EncryptedPayload == null || request.ProviderEnum == SsoProviderEnum.Unknown)
            {
                response.IsError = true;
                response.ErrorEnum = SsoResponseErrorEnum.MissingFields;
                this.LoggingService.Value.Warn(() => string.Format("SsoApplicationService::SignInWithMyChartSso - Request was null or payload was null or unknown provider"));
                return response;
            }

            IEncryptionService encryptionProvider = this.GetEncryptionServiceForProvider(request.ProviderEnum);
            ISsoSpecificService ssoSpecificService = this.GetSsoSpecificServiceForProvider(request.ProviderEnum);

            try
            {
                decryptedValue = encryptionProvider.Decrypt(request.EncryptedPayload);
                this.LoggingService.Value.Info(() => $"SsoApplicationService::SignInWithMyChartSso - Decrypt worked = {decryptedValue}");
            }
            catch (Exception e)
            {
                response.IsError = true;
                response.ErrorEnum = SsoResponseErrorEnum.FailedToDecrypt;
                this.LoggingService.Value.Warn(() =>  $"SsoApplicationService::SignInWithMyChartSso - Decrypt threw = {ExceptionHelper.AggregateExceptionToString(e)}");
                return response;
            }

            if (decryptedValue == String.Empty)
            {
                response.IsError = true;
                response.ErrorEnum = SsoResponseErrorEnum.FailedToDecrypt;
                this.LoggingService.Value.Warn(() => string.Format("SsoApplicationService::SignInWithMyChartSso - Decrypt worked but didnt contain any data"));
                return response;
            }

            response = ssoSpecificService.PopulateResponse(response, decryptedValue);
            if (response != null && response.VpGuarantorId.HasValue)
            {
                Guarantor guarantor = this._guarantorService.Value.GetGuarantor(response.VpGuarantorId.Value);
                if (guarantor != null)
                {
                    SsoVisitPayUserSetting setting = this.GetUserSetting(response.ProviderEnum, guarantor.User.VisitPayUserId, null);

                    response.AcceptedTheSsoAgreement = setting != null && setting.SsoStatus == SsoStatusEnum.Accepted;
                }
            }
            return response;
        }

        public bool AddVpGuarantorVerification(int vpGuarantorId, string ssoSourceSystemKey, DateTime dateOfBirth, SsoProviderEnum ssoProviderEnum)
        {
            ISsoSpecificService ssoSpecificService = this.GetSsoSpecificServiceForProvider(ssoProviderEnum);

            return ssoSpecificService.AddVpGuarantorVerification(vpGuarantorId, ssoSourceSystemKey, dateOfBirth);
        }

        public IList<SsoProvider> GetEligibleSsoProvidersForUser(int visitPayUserId, int vpGuarantorId)
        {
            IList<SsoProvider> enabledSsoProviders = this._ssoProviderRepository.Value.GetEnabledProviders();
            IList<SsoProviderEnum> enabledUserProviders = this._ssoVisitPayUserSettingRepository.Value.GetEnabledProvidersForUser(visitPayUserId);

            IList<SsoProvider> eligibleSsoProviders = new List<SsoProvider>();
            if (enabledSsoProviders.Any())
            {
                foreach (SsoProvider provider in enabledSsoProviders)
                {
                    if (enabledUserProviders.Any(x => x == (SsoProviderEnum)provider.SsoProviderId))
                    {
                        // user has either accepted or (rejected + ignored)
                        // don't want to show these cases
                    }
                    else
                    {
                        ISsoSpecificService specificService = this.GetSsoSpecificServiceForProvider((SsoProviderEnum)provider.SsoProviderId);
                        // check if VpGuarantor could sso if they tried.
                        if (specificService != null && specificService.IsVpGuarantorEligible(vpGuarantorId))
                        {
                            eligibleSsoProviders.Add(provider);
                        }
                    }
                }
            }

            return eligibleSsoProviders;
        }

        public SsoProvider GetProviderBySourceSystemKey(string sourceSystemKey)
        {
            return this._ssoProviderRepository.Value.GetBySourceSystemKey(sourceSystemKey);
        }

        public void UpdateSsoVisitPayUserSettingWithOAuthInfo(SsoProviderEnum oAuthProvider, int currentUserId, string authorizationStateAccessToken, string authorizationStateRefreshToken, string scope)
        {
            SsoVisitPayUserSetting userSetting = this.GetUserSetting(oAuthProvider, currentUserId);
            userSetting.AccessToken = authorizationStateAccessToken;
            userSetting.RefreshToken = authorizationStateRefreshToken;
            userSetting.Scope = scope;
            userSetting.RefreshTokenFailure = 0;
            if (!string.IsNullOrWhiteSpace(authorizationStateAccessToken) && !string.IsNullOrWhiteSpace(authorizationStateRefreshToken))
            {
                userSetting.SetIgnore(false);
                userSetting.SsoStatus = SsoStatusEnum.Accepted;
            }
            this._ssoVisitPayUserSettingRepository.Value.InsertOrUpdate(userSetting);
        }

        private static readonly HttpClient IsActiveHttpClient = new HttpClient() { Timeout = TimeSpan.FromSeconds(ApiTimeoutSeconds) };
        public async Task<bool> CheckIfProviderIsActiveAsync(SsoProvider provider)
        {
            if (string.IsNullOrEmpty(provider?.AuthorizationServerBaseAddress))
            {
                return false;
            }
            try
            {
                Uri baseUri = new Uri(provider.AuthorizationServerBaseAddress);
                Uri myUri = new Uri(baseUri, provider.AuthorizePath);

                HttpResponseMessage response = await IsActiveHttpClient.GetAsync(myUri);
                string content = await response.Content.ReadAsStringAsync();
                //Seems strange.  When I hit these urls it seems to always say 404, hopefully this is expected
                if (string.IsNullOrWhiteSpace(content) || response.StatusCode != HttpStatusCode.OK && response.StatusCode != HttpStatusCode.BadRequest && response.StatusCode != HttpStatusCode.NotFound && response.StatusCode != HttpStatusCode.InternalServerError)
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                this.LoggingService.Value.Fatal(() => $"SsoService::CheckIfProviderIsActiveAsync: {ex}");
                return false;
            }
            return true;
        }

        public void RemoveAccessToken(SsoVisitPayUserSetting connected)
        {
            if (connected == null)
            {
                return;
            }
            connected.AccessToken = string.Empty;
            this._ssoVisitPayUserSettingRepository.Value.InsertOrUpdate(connected);
        }

        public void IncrementRefreshTokenFailureCount(SsoVisitPayUserSetting connected)
        {
            if (connected == null)
            {
                return;
            }
            connected.RefreshTokenFailure++;
            if (connected.RefreshTokenFailure >= this.MaxRefreshFailures(connected.SsoProvider))
            {
                connected.SsoStatus = SsoStatusEnum.Rejected;
                connected.RefreshTokenFailure = 0;
            }
            this._ssoVisitPayUserSettingRepository.Value.InsertOrUpdate(connected);
        }

        private int MaxRefreshFailures(SsoProvider ssoProvider)
        {
            const int maxRefreshFailures = 3;

            //dont reject sso for the mock hqy provider
            if (ssoProvider.SsoProviderEnum == SsoProviderEnum.HealthEquityOAuthImplicit)
            {
                return 1000;
            }
            return maxRefreshFailures;
        }

        public IList<SsoVerification> GetVerificationsForVpGuarantor(SsoProviderEnum ssoProvider, int vpGuarantorId)
        {
            ISsoSpecificService ssoSpecificService = this.GetSsoSpecificServiceForProvider(ssoProvider);
            if (ssoSpecificService == null)
            {
                return new List<SsoVerification>();
            }
            return ssoSpecificService.GetVpGuarantorVerifications(vpGuarantorId);
        }

        private SsoVisitPayUserSetting GetUserSetting(SsoProviderEnum ssoProvider, int visitPayUserId, int? actionTakenByVisitPayUserId = null)
        {
            SsoVisitPayUserSetting userSetting = this._ssoVisitPayUserSettingRepository.Value.GetByVisitPayUserIdAndProvider(ssoProvider, visitPayUserId);
            if (userSetting == null)
            {
                SsoProvider provider = this._ssoProviderRepository.Value.GetById((int)ssoProvider);
                if (provider == null)
                {
                    return null;
                }

                userSetting = new SsoVisitPayUserSetting
                {
                    SsoProvider = provider,
                    SsoTermsCmsVersion = this._contentService.Value.GetCurrentVersionAsync((CmsRegionEnum)provider.SsoTermsCmsRegion.CmsRegionId, false).Result,
                    VisitPayUser = this._visitPayUserRepository.Value.FindById(visitPayUserId.ToString()),
                    SsoStatus = SsoStatusEnum.Unknown
                };
            }

            userSetting.ActionDateTime = DateTime.UtcNow;
            userSetting.ActionVisitPayUser = this._visitPayUserRepository.Value.FindById(actionTakenByVisitPayUserId.GetValueOrDefault(visitPayUserId).ToString());

            return userSetting;
        }
    }
}
namespace Ivh.Domain.Core.Sso.Services
{
    using System;
    using System.Collections.Generic;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;
    using User.Entities;

    public class SsoOauthIssuerService : DomainService, ISsoOauthIssuerService
    {
        private readonly Lazy<ISsoVisitPayUserOauthTokenRepository> _oauthTokenRepository;
        private readonly Lazy<ISsoOauthTokenGenerator> _tokenGenerator;


        public SsoOauthIssuerService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<ISsoVisitPayUserOauthTokenRepository> oauthTokenRepository,
            Lazy<ISsoOauthTokenGenerator> tokenGenerator) : base(serviceCommonService)
        {
            this._oauthTokenRepository = oauthTokenRepository;
            this._tokenGenerator = tokenGenerator;
        }

        public SsoVisitPayUserOauthToken CreateRefreshToken(VisitPayUser user, string ticket)
        {
            if (user == null) return null;
            this.CancelAnyAccessTokensForUser(user.VisitPayUserId, OAuthTokenTypeEnum.RefreshToken);

            SsoVisitPayUserOauthToken token = new SsoVisitPayUserOauthToken()
            {
                ExpiresOn = DateTime.UtcNow.AddDays(100),
                Token = this._tokenGenerator.Value.GenerateToken(),
                Ticket = ticket,
                IsActive = true,
                VisitPayUserId = user.VisitPayUserId,
                TokenType = OAuthTokenTypeEnum.RefreshToken
            };
            this._oauthTokenRepository.Value.InsertOrUpdate(token);
            return token;
        }

        public SsoVisitPayUserOauthToken GetRefreshTokenWithRefreshTokenString(string refreshTokenString)
        {
            return this._oauthTokenRepository.Value.GetByTokenString(refreshTokenString, OAuthTokenTypeEnum.RefreshToken);
        }

        public SsoVisitPayUserOauthToken GetAccessTokenWithTokenString(string tokenString)
        {
            return this._oauthTokenRepository.Value.GetByTokenString(tokenString, OAuthTokenTypeEnum.AccessToken);
        }

        public void CancelAnyAccessTokensForUser(int visitPayUserId, OAuthTokenTypeEnum type)
        {
            IList<SsoVisitPayUserOauthToken> allTokensForUser = this._oauthTokenRepository.Value.GetAllTokensForUser(visitPayUserId, type, true);
            foreach (SsoVisitPayUserOauthToken ssoVisitPayUserOauthToken in allTokensForUser)
            {
                ssoVisitPayUserOauthToken.IsActive = false;
                this._oauthTokenRepository.Value.InsertOrUpdate(ssoVisitPayUserOauthToken);
            }
        }

        public SsoVisitPayUserOauthToken CreateAccessToken(VisitPayUser user, string ticket)
        {
            if (user == null) return null;
            this.CancelAnyAccessTokensForUser(user.VisitPayUserId, OAuthTokenTypeEnum.AccessToken);

            SsoVisitPayUserOauthToken access = new SsoVisitPayUserOauthToken()
            {
                Token = this._tokenGenerator.Value.GenerateToken(),
                Ticket = ticket,
                IsActive = true,
                VisitPayUserId = user.VisitPayUserId,
                TokenType = OAuthTokenTypeEnum.AccessToken
            };

            this._oauthTokenRepository.Value.InsertOrUpdate(access);

            return access;
        }
    }
}
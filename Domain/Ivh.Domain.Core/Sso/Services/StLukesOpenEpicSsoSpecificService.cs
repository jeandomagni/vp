﻿namespace Ivh.Domain.Core.Sso.Services
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Globalization;
    using System.Linq;
    using System.Web;
    using Application.Core.Common.Dtos;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;
    using Entities;
    using Guarantor.Entities;
    using Interfaces;
    using Logging.Interfaces;
    using Settings.Interfaces;

    public class StLukesOpenEpicSsoSpecificService : DomainService, ISsoSpecificService
    {
        private readonly Lazy<ISsoSpecificRespository> _repo;
        private const string TimeFormatter = "yyyyMMddHHmmss";

        public StLukesOpenEpicSsoSpecificService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<ISsoSpecificRespository> repo) : base(serviceCommonService)
        {
            this._repo = repo;
        }

        public SsoProviderEnum ProviderEnum => SsoProviderEnum.OpenEpic;

        public string GenerateSampleRequest(string firstName, string lastName, string ssoSsk, DateTime dob)
        {
            //"20160908004800"
            string timestamp = DateTime.UtcNow.ToString(TimeFormatter);

            if (ssoSsk.IsNullOrEmpty())
            {
                ssoSsk = "1234";
            }
            string birthDay = dob.ToString("MM/dd/yyyy");

            string stringToEncryptAndDecript = "time=" + HttpUtility.UrlEncode(timestamp) +
                                               "&MyChartID=" + HttpUtility.UrlEncode(ssoSsk) +
                                               "&FNAME=" + HttpUtility.UrlEncode(firstName) +
                                               "&LNAME=" + HttpUtility.UrlEncode(lastName) +
                                               "&birthdate=" + HttpUtility.UrlEncode(birthDay);
            return stringToEncryptAndDecript;
        }

        public SsoResponseDto PopulateResponse(SsoResponseDto response, string decryptedValue)
        {
            NameValueCollection qsCollection = HttpUtility.ParseQueryString(decryptedValue);

            string keyForTime = qsCollection.AllKeys.FirstOrDefault(x => string.Equals("time", x, StringComparison.InvariantCultureIgnoreCase));
            if (keyForTime == null)
            {
                response.IsError = true;
                this.LoggingService.Value.Warn(() => string.Format("StLukesOpenEpicSsoSpecificService::PopulateResponse - time didnt exist in the decrypted text"));
                return response;
            }
            DateTime parsedValue;
            if (DateTime.TryParseExact(qsCollection[keyForTime], TimeFormatter, System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.None, out parsedValue))
            {
                if (DateTime.UtcNow > parsedValue.AddMinutes(this.ApplicationSettingsService.Value.SsoTimeoutTokensAfterMinutes.Value))
                {
                    response.IsError = true;
                    response.ErrorEnum = SsoResponseErrorEnum.FailedToVerifyTimeStamp;
                    this.LoggingService.Value.Warn(() => string.Format("StLukesOpenEpicSsoSpecificService::PopulateResponse - The time was older than configured value.  Cannot use."));
                    return response;
                }
            }
            else
            {
                response.IsError = true;
                this.LoggingService.Value.Warn(() => string.Format("StLukesOpenEpicSsoSpecificService::PopulateResponse - Couldnt parse the time"));
                return response;
            }


            string keyForMyChart = qsCollection.AllKeys.FirstOrDefault(x => string.Equals("MyChartID", x, StringComparison.InvariantCultureIgnoreCase));
            if (keyForMyChart == null)
            {
                response.IsError = true;
                response.ErrorEnum = SsoResponseErrorEnum.MissingFields;
                this.LoggingService.Value.Warn(() => string.Format("StLukesOpenEpicSsoSpecificService::PopulateResponse - MyChartID didnt exist in the decrypted text"));
                return response;
            }
            string keyForBirthDate = qsCollection.AllKeys.FirstOrDefault(x => string.Equals("birthdate", x, StringComparison.InvariantCultureIgnoreCase));
            if (keyForBirthDate == null)
            {
                response.IsError = true;
                response.ErrorEnum = SsoResponseErrorEnum.MissingFields;
                this.LoggingService.Value.Warn(() => string.Format("StLukesOpenEpicSsoSpecificService::PopulateResponse - birthdate didnt exist in the decrypted text"));
                return response;
            }

            DateTime parsedDOB;
            if (!DateTime.TryParse(qsCollection[keyForBirthDate], out parsedDOB))
            {
                response.IsError = true;
                response.ErrorEnum = SsoResponseErrorEnum.MissingFields;
                this.LoggingService.Value.Warn(() => string.Format("StLukesOpenEpicSsoSpecificService::PopulateResponse - Couldnt parse the DOB"));
                return response;
            }
            response.DateOfBirth = parsedDOB;
            response.SsoSourceSystemKey = qsCollection[keyForMyChart];

            string keyforFirst = qsCollection.AllKeys.FirstOrDefault(x => string.Equals("FNAME", x, StringComparison.InvariantCultureIgnoreCase));
            if (keyforFirst != null)
            {
                response.FirstName = qsCollection[keyforFirst];
            }
            string keyforLast = qsCollection.AllKeys.FirstOrDefault(x => string.Equals("LNAME", x, StringComparison.InvariantCultureIgnoreCase));
            if (keyforLast != null)
            {
                response.LastName = qsCollection[keyforLast];
            }

            IList<SsoPossibleHsGuarantorResult> possibleHsGs = this._repo.Value.HasPossibleHsGuarantorsToRegister(response.SsoSourceSystemKey, response.DateOfBirth);
            IList<SsoPossibleVpGuarantorResult> possibleVpHsGs = this._repo.Value.HasPossibleVisitPayGuarantors(response.SsoSourceSystemKey, response.DateOfBirth);

            response.MatchesThatCouldBeRegistered = possibleHsGs;

            response.FoundMatchesThatCouldBeRegistered = possibleHsGs != null && possibleHsGs.Count > 0;
            response.FoundRegisteredMatches = possibleVpHsGs != null && possibleVpHsGs.Count > 0;

            if (possibleVpHsGs != null && possibleVpHsGs.Any(x => x.SingleSignOnVerificationId != null))
            {
                response.VpGuarantorId = possibleVpHsGs.First(x => x.SingleSignOnVerificationId != null).VpGuarantorID;
            }

            return response;
        }

        public bool AddVpGuarantorVerification(int vpGuarantorId, string ssoSourceSystemKey, DateTime dateOfBirth)
        {
            IList<SsoVerificationResult> listOfAdditions = this._repo.Value.RegisterVpGuarantor(vpGuarantorId, ssoSourceSystemKey, dateOfBirth);
            return listOfAdditions.Any(x => x.VpGuarantorId == vpGuarantorId && x.DOB == dateOfBirth && x.SingleSignOn_SourceSystemKey == ssoSourceSystemKey);
        }

        public IList<SsoVerification> GetVpGuarantorVerifications(int vpGuarantorId)
        {
            return this._repo.Value.GetVerificationsByVpGuarantor(vpGuarantorId);
        }

        public void RemoveAllVerificationsForVpGuarantor(Guarantor vpGuarantor)
        {
            this._repo.Value.RemoveAllVerificationsForVpGuarantor(vpGuarantor.VpGuarantorId);
        }

        public bool IsVpGuarantorEligible(int vpGuarantorId)
        {
            return this._repo.Value.IsVpGuarantorEligible(vpGuarantorId);
        }
    }
}
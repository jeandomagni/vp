﻿
namespace Ivh.Domain.Core.Sso.Services
{
    using System;
    using Base.Interfaces;
    using Base.Services;
    using Interfaces;

    public class HealthEquityEmployerIdResolutionService : DomainService, IHealthEquityEmployerIdResolutionService
    {
        private readonly Lazy<IHealthEquityEmployerIdResolutionProvider> _healthEquityEmployerIdResolutionProvider;

        public HealthEquityEmployerIdResolutionService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IHealthEquityEmployerIdResolutionProvider> healthEquityEmployerIdResolutionProvider) : base(serviceCommonService)
        {
            this._healthEquityEmployerIdResolutionProvider = healthEquityEmployerIdResolutionProvider;
        }

        public string GetHealthEquityEmployerId(int gurarantorId)
        {
            return this._healthEquityEmployerIdResolutionProvider.Value.GeHealthEquityEmployerId(gurarantorId);
        }
    }
}

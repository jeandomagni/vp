﻿namespace Ivh.Domain.Core.Sso.Services
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Security.Cryptography.X509Certificates;
    using System.Security.Cryptography.Xml;
    using System.Text;
    using System.Xml;
    using System.Xml.Serialization;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Responses;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Guarantor.Interfaces;
    using Interfaces;
    using Logging.Interfaces;
    using Settings.Enums;
    using Settings.Interfaces;
    using Entities;

    public class SamlService : DomainService, ISamlService
    {
        private static readonly IDictionary<string, string> SamlNamespaces = new Dictionary<string, string>()
        {
            {"samlp", "urn:oasis:names:tc:SAML:2.0:protocol"},
            {"saml", "urn:oasis:names:tc:SAML:2.0:assertion"},
            {"md", "urn:oasis:names:tc:SAML:2.0:metadata"},
            {"ds", "http://www.w3.org/2000/09/xmldsig#"},
            {"xenc", "http://www.w3.org/2001/04/xmlenc#"},
            {"xs", "http://www.w3.org/2001/XMLSchema"}
        };

        private static XmlSerializerNamespaces _xmlSerializerSamlNamespaces;
        private static XmlSerializerNamespaces XmlSerializerSamlNamespaces
        {
            get
            {
                if (_xmlSerializerSamlNamespaces == null)
                {
                    _xmlSerializerSamlNamespaces = new XmlSerializerNamespaces();
                    foreach (KeyValuePair<string, string> namespace1 in SamlNamespaces)
                    {
                        _xmlSerializerSamlNamespaces.Add(namespace1.Key, namespace1.Value);
                    }
                }
                return _xmlSerializerSamlNamespaces;
            }
        }

        private static XmlNamespaceManager _samlNamespaceManager;

        private static XmlNamespaceManager SamlNamespaceManager
        {
            get
            {
                if (_samlNamespaceManager == null)
                {
                    _samlNamespaceManager = new XmlNamespaceManager(new NameTable());
                    foreach (KeyValuePair<string, string> namespace1 in SamlNamespaces)
                    {
                        _samlNamespaceManager.AddNamespace(namespace1.Key, namespace1.Value);
                    }
                }
                return _samlNamespaceManager;
            }
        }

        private readonly Lazy<IGuarantorRepository> _guarantorRepository;
        private readonly Lazy<IHealthEquityEmployerIdResolutionService> _healthEquityEmployerIdResolutionService;
        private readonly Lazy<IMetricsProvider> _metricsProvider;

        public SamlService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IGuarantorRepository> guarantorRepository,
            Lazy<IHealthEquityEmployerIdResolutionService> healthEquityEmployerIdResolutionService,
            Lazy<IMetricsProvider> metricsProvider) : base(serviceCommonService)
        {
            this._guarantorRepository = guarantorRepository;
            this._healthEquityEmployerIdResolutionService = healthEquityEmployerIdResolutionService;
            this._metricsProvider = metricsProvider;
        }

        public Response<SamlResponse> GetSamlResponse(int visitPayUserId, SamlResponseTargetEnum samlResponseTarget, string samlRequest = null, string relayState = null)
        {
            AuthnRequestType authnRequest = samlRequest.IsNotNullOrEmpty() ? GetSamlRequest(samlRequest) : null;
            Response<SamlResponse> samlResponse;

            switch (samlResponseTarget)
            {
                case SamlResponseTargetEnum.HealthEquity:
                    samlResponse = this.GetHealthEquitySamlResponse(authnRequest, visitPayUserId);
                    break;
                case SamlResponseTargetEnum.Unknown:
                default:
                    throw new ArgumentOutOfRangeException(nameof(samlResponseTarget), samlResponseTarget, null);
            }

            if (samlResponse?.Object != null)
            {
                samlResponse.Object.RelayState = relayState;
            }

            return samlResponse;
        }

        private Response<SamlResponse> GetHealthEquitySamlResponse(AuthnRequestType authnRequest, int visitPayUserId)
        {
            if (this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.HealthEquityFeature))
            {
                using (HttpClient client = new HttpClient())
                {
                    try
                    {
                        if (this.ApplicationSettingsService.Value.HealthEquitySsoSamlEndpointValidate.Value)
                        {
                            HttpResponseMessage response = client.GetAsync(this.ApplicationSettingsService.Value.GetUrl(UrlEnum.HealthEquitySso)).Result;
                            if (response.StatusCode != HttpStatusCode.OK)
                            {
                                this._metricsProvider.Value.Increment(Metrics.Increment.Hqy.NotAuthenticated);
                                return new Response<SamlResponse>(null, false, SsoMessages.FailureEndpoint);
                            }
                            this._metricsProvider.Value.Increment(Metrics.Increment.Hqy.Authenticated);
                        }
                    }
                    catch (Exception ex)
                    {
                        this.LoggingService.Value.Fatal(() => $"SamlService::GetHealthEquitySamlResponse: {ex}");
                        this._metricsProvider.Value.Increment(Metrics.Increment.Hqy.AuthenticateFailed);
                        return new Response<SamlResponse>(null, false, SsoMessages.FailureEndpoint);
                    }
                }

                int vpGuarantorId = this._guarantorRepository.Value.GetGuarantorId(visitPayUserId);
                if (vpGuarantorId != 0)
                {
                    string ssn = this._guarantorRepository.Value.GetGuarantorMatchSsn(vpGuarantorId);
                    if (ssn.IsNotNullOrEmpty())
                    {
                        string employerId = this._healthEquityEmployerIdResolutionService.Value.GetHealthEquityEmployerId(vpGuarantorId);
                        if (employerId == null)
                        {
                            return new Response<SamlResponse>(null, false, SsoMessages.FailureUnknown);
                        }

                        IDictionary<string, string> attributes = new Dictionary<string, string>()
                        {
                            {"EmployerID", employerId},
                            {"InsuredSSN", ssn.RemoveNonNumericCharacters().Trim()},
                        };

                        return new Response<SamlResponse>(new SamlResponse
                        {
                            Url = this.ApplicationSettingsService.Value.GetUrl(UrlEnum.HealthEquitySso).AbsoluteUri,
                            Value = CreateSaml20Response(
                                authnRequest,
                                this.ApplicationSettingsService.Value.HealthEquitySsoSamlResponseIssuer.Value,
                                this.ApplicationSettingsService.Value.HealthEquitySsoSamlResponseExpiration.Value,
                                "_" + Guid.NewGuid().ToString("N"),
                                this.ApplicationSettingsService.Value.GetUrl(UrlEnum.HealthEquitySso).AbsoluteUri,
                                attributes,
                                this.ApplicationSettingsService.Value.HealthEquitySsoCertificate.Value) //probably could cache this certificate to speed up creation.
                        }, true);
                    }
                }
            }

            return new Response<SamlResponse>(null, false, SsoMessages.FailureUnknown);
        }

        private static AuthnRequestType GetSamlRequest(string samlRequest)
        {
            string temp = "<samlp:AuthnRequest xmlns:samlp=\"urn:oasis:names:tc:SAML:2.0:protocol\" xmlns:saml=\"urn:oasis:names:tc:SAML:2.0:assertion\" ID=\"identifier_1\" Version=\"2.0\" IssueInstant=\"2004-12-05T09:21:59\" AssertionConsumerServiceIndex=\"0\"><saml:Issuer>https://pi.HealthEquity.com/HESaml2.aspx</saml:Issuer><samlp:NameIDPolicy AllowCreate = \"true\" Format = \"urn:oasis:names:tc:SAML:2.0:nameid-format:transient\"/></samlp:AuthnRequest>";
            samlRequest = System.Convert.ToBase64String(Encoding.UTF8.GetBytes(temp));
            XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();
            using (StringReader stringReader = new StringReader(Encoding.UTF8.GetString(Convert.FromBase64String(samlRequest))))
            using (XmlReader requestReader = XmlReader.Create(stringReader, xmlReaderSettings))
            {
                XmlSerializer requestSerializer = new XmlSerializer(typeof(AuthnRequestType));
                return (AuthnRequestType)requestSerializer.Deserialize(requestReader);
            }
        }

        private static string CreateSaml20Response(
            AuthnRequestType authnRequest,
            string issuer,
            int assertionExpirationMinutes,
            string subject,
            string destination,
            IDictionary<string, string> attributes,
            X509Certificate2 signingCert)
        {
            // Create SAML Response object with a unique ID and correct version

            DateTime issueInstant = DateTime.UtcNow;
            if (authnRequest != null)
            {
                destination = authnRequest.Issuer.Value;
            }

            ResponseType response = new ResponseType
            {
                ID = "_" + System.Guid.NewGuid().ToString("N"),
                Version = "2.0",
                IssueInstant = issueInstant.ToString(),
                Destination = destination.Trim(),
                Issuer = new NameIDType()
                {
                    Value = issuer.Trim(),
                    Format = "urn:oasis:names:tc:SAML:2.0:nameid-format:entity",
                },
                Status = new StatusType()
                {
                    StatusCode = new StatusCodeType()
                    {
                        Value = "urn:oasis:names:tc:SAML:2.0:status:Success"
                    }
                },
                Items = new AssertionType[]
                    {
                        CreateSaml20Assertion(issueInstant,issuer,assertionExpirationMinutes,subject,destination,attributes)
                    }
            };

            if (authnRequest != null)
            {
                response.InResponseTo = authnRequest.ID;
            }


            // Put SAML 2.0 Assertion in Response
            XmlDocument xmlResponse = SerializeAndSignSamlResponse(response, signingCert);
            return System.Convert.ToBase64String(Encoding.UTF8.GetBytes(xmlResponse.OuterXml));
        }

        private static AssertionType CreateSaml20Assertion(
            DateTime issueInstant,
            string issuer,
            int assertionExpirationMinutes,
            string subject,
            string recipient,
            IDictionary<string, string> attributes)
        {
            string id = "_" + Guid.NewGuid().ToString("N");

            AssertionType assertion = new AssertionType
            {
                Version = "2.0",
                IssueInstant = issueInstant.ToString(),
                ID = id,
                // Create Issuer
                Issuer = new NameIDType()
                {
                    Value = issuer.Trim(),
                    Format = "urn:oasis:names:tc:SAML:2.0:nameid-format:entity"
                },
                Conditions = new ConditionsType
                {
                    NotBefore = issueInstant.AddMinutes(-1).ToString(),
                    NotBeforeSpecified = true,
                    NotOnOrAfter = issueInstant.AddMinutes(assertionExpirationMinutes).ToString(),
                    NotOnOrAfterSpecified = true,
                },
                Subject = new SubjectType()
                {
                    Items = new object[]
                    {
                        new NameIDType()
                        {
                            Value = subject.Trim(),
                            Format = "urn:oasis:names:tc:SAML:2.0:nameid-format:unspecified"
                        },
                        new SubjectConfirmationType()
                        {
                            Method = "urn:oasis:names:tc:SAML:2.0:cm:bearer",
                            SubjectConfirmationData = new SubjectConfirmationDataType()
                            {
                                NotOnOrAfter = DateTime.UtcNow.AddMinutes(assertionExpirationMinutes).ToString(),
                                Recipient = recipient
                            }
                        }
                    }
                },
                Items = new StatementAbstractType[]
                {
                    new AuthnStatementType()
                    {
                        AuthnInstant = DateTime.UtcNow.ToString(),
                        SessionIndex = id,
                        AuthnContext = new AuthnContextType
                        {
                            ItemsElementName = new ItemsChoiceType5[]
                            {
                                ItemsChoiceType5.AuthnContextClassRef
                            },
                                Items = new object[]
                            {
                                "urn:oasis:names:tc:SAML:2.0:ac:classes:Password"
                            }
                        }
                    },
                    new AttributeStatementType
                    {
                        Items = attributes.Select(x => new AttributeType() {
                            Name = x.Key,
                            AttributeValue = new object[] { x.Value },
                            NameFormat = "urn:oasis:names:tc:SAML:2.0:attrname-format:basic"
                        }).Cast<object>().ToArray()
                    }
                }
            };

            return assertion;
        }

        private static XmlDocument SerializeAndSignSamlResponse(ResponseType response, X509Certificate2 signingCert, bool? deflate = null)
        {
            // Set serializer and writers for action
            XmlSerializer responseSerializer = new XmlSerializer(response.GetType());
            StringWriter stringWriter = new StringWriter();
            XmlWriter responseWriter = XmlWriter.Create(stringWriter, new XmlWriterSettings()
            {
                OmitXmlDeclaration = true,
                Indent = !(deflate ?? true),
                Encoding = Encoding.UTF8,
                NamespaceHandling = NamespaceHandling.Default,
                ConformanceLevel = ConformanceLevel.Document,
                DoNotEscapeUriAttributes = true,
            });

            responseSerializer.Serialize(responseWriter, response, XmlSerializerSamlNamespaces);
            responseWriter.Close();
            XmlDocument xmlResponse = new XmlDocument();
            xmlResponse.LoadXml(stringWriter.ToString());

            Func<string, string> referenceId = x => "#" + x;
            //sign the assertion
            if (xmlResponse.DocumentElement != null)
            {
                XmlElement assertion = xmlResponse.DocumentElement.SelectSingleNode("saml:Assertion", SamlNamespaceManager) as XmlElement;
                XmlNodeList xmlNodeList = assertion.SelectNodes("descendant-or-self::*[namespace-uri()='http://www.w3.org/2000/09/xmldsig#']");
                if (xmlNodeList != null)
                    foreach (XmlNode n in xmlNodeList)
                    {
                        n.Prefix = "ds";
                    }

                SignXmlElement(assertion, referenceId(((AssertionType)response.Items[0]).ID), signingCert);
            }

            return xmlResponse;
        }

        public static void SignXmlElement(XmlElement xmlElement, string referenceUri, X509Certificate2 signingCert)
        {
            if (xmlElement != null && signingCert.HasPrivateKey)
            {
                SignedXml signedXml = new SignedXml(xmlElement)
                {
                    SigningKey = signingCert.PrivateKey
                };

                signedXml.SignedInfo.CanonicalizationMethod = SignedXml.XmlDsigExcC14NTransformUrl;
                signedXml.KeyInfo = new KeyInfo();
                signedXml.KeyInfo.AddClause(new KeyInfoX509Data(signingCert));

                Reference reference = new Reference
                {
                    Uri = referenceUri
                };
                reference.AddTransform(new XmlDsigEnvelopedSignatureTransform());
                reference.AddTransform(new XmlDsigExcC14NTransform());
                signedXml.AddReference(reference);
                signedXml.ComputeSignature();
                XmlElement signature = signedXml.GetXml();
                xmlElement.InsertBefore(signature, xmlElement.FirstChild);
            }
        }
    }
}

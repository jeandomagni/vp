namespace Ivh.Domain.Core.Sso.Services
{
    using System;
    using Interfaces;

    public class SsoOauthTokenGenerator : ISsoOauthTokenGenerator
    {
        public string GenerateToken()
        {
            return Guid.NewGuid().ToString("n") + Guid.NewGuid().ToString("n");
        }
    }
}
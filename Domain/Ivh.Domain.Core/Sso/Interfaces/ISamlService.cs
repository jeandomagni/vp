﻿namespace Ivh.Domain.Core.Sso.Interfaces
{
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.Base.Utilities.Responses;
    using Common.VisitPay.Enums;
    using Entities;

    public interface ISamlService : IDomainService
    {
        Response<SamlResponse> GetSamlResponse(int visitPayUserId, SamlResponseTargetEnum samlResponseTarget, string samlRequest = null, string relayState = null);
    }
}

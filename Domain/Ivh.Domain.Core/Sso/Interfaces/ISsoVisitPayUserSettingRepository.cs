﻿namespace Ivh.Domain.Core.Sso.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface ISsoVisitPayUserSettingRepository : IRepository<SsoVisitPayUserSetting>
    {
        IList<SsoVisitPayUserSetting> GetByVisitPayUserId(int visitPayUserId);
        SsoVisitPayUserSetting GetByVisitPayUserIdAndProvider(SsoProviderEnum ssoProvider, int visitPayUserId);
        IList<SsoProviderEnum> GetEnabledProvidersForUser(int visitPayUserId);
    }
}
﻿namespace Ivh.Domain.Core.Sso.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface ISsoProviderRepository : IRepository<SsoProvider>
    {
        IList<SsoProvider> GetEnabledProviders();
        SsoProvider GetBySourceSystemKey(string sourceSystemKey);
    }
}
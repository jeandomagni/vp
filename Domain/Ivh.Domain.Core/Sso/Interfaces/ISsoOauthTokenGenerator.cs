namespace Ivh.Domain.Core.Sso.Interfaces
{
    public interface ISsoOauthTokenGenerator
    {
        string GenerateToken();
    }
}
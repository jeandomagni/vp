﻿namespace Ivh.Domain.Core.Sso.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Application.Core.Common.Dtos;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface ISsoService : IDomainService
    {
        SsoProvider GetProvider(SsoProviderEnum ssoProvider);
        IList<SsoProvider> GetProviders();
        SsoVisitPayUserSetting GetUserSettings(SsoProviderEnum ssoProvider, int visitPayUserId);
        IList<SsoVisitPayUserSetting> GetUserSettings(int visitPayUserId);
        SsoVisitPayUserSetting AcceptSso(SsoProviderEnum ssoProvider, int visitPayUserId, int? actionTakenByVisitPayUserId = null);
        SsoVisitPayUserSetting DeclineSso(SsoProviderEnum ssoProvider, int visitPayUserId, int? actionTakenByVisitPayUserId = null);
        SsoVisitPayUserSetting IgnoreSso(SsoProviderEnum ssoProvider, int visitPayUserId, bool ignore, int? actionTakenByVisitPayUserId = null);
        SsoVisitPayUserSetting InvalidateSso(SsoProviderEnum ssoProvider, int visitPayUserId);
        SsoVisitPayUserSetting AcknowledgeInvalidatedSso(SsoProviderEnum ssoProvider, int visitPayUserId, int? actionTakenByVisitPayUserId = null);
        string GenerateEncryptedSsoToken(string firstName, string lastName, SsoProviderEnum providerEnum, string ssoSsk, DateTime dob);
        SsoResponseDto SignInWithSso(SsoRequestDto request);
        bool AddVpGuarantorVerification(int vpGuarantorId, string ssoSourceSystemKey, DateTime dateOfBirth, SsoProviderEnum ssoProviderEnum);
        IList<SsoVerification> GetVerificationsForVpGuarantor(SsoProviderEnum ssoProvider, int vpGuarantorId);
        IList<SsoProvider> GetEligibleSsoProvidersForUser(int visitPayUserId, int vpGuarantorId);
        SsoProvider GetProviderBySourceSystemKey(string sourceSystemKey);
        void UpdateSsoVisitPayUserSettingWithOAuthInfo(SsoProviderEnum oAuthProvider, int currentUserId, string authorizationStateAccessToken, string authorizationStateRefreshToken, string scope);
        Task<bool> CheckIfProviderIsActiveAsync(SsoProvider provider);
        void RemoveAccessToken(SsoVisitPayUserSetting connected);
        void IncrementRefreshTokenFailureCount(SsoVisitPayUserSetting connected);
    }
}
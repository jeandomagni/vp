namespace Ivh.Domain.Core.Sso.Interfaces
{
    using Entities;
    using User.Entities;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;

    public interface ISsoOauthIssuerService : IDomainService
    {
        SsoVisitPayUserOauthToken CreateRefreshToken(VisitPayUser user, string ticket);
        SsoVisitPayUserOauthToken CreateAccessToken(VisitPayUser user, string ticket);
        SsoVisitPayUserOauthToken GetRefreshTokenWithRefreshTokenString(string refreshTokenString);
        SsoVisitPayUserOauthToken GetAccessTokenWithTokenString(string tokenString);
        void CancelAnyAccessTokensForUser(int visitPayUserId, OAuthTokenTypeEnum type);
        
    }
}
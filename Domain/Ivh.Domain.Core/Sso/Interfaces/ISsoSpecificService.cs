﻿namespace Ivh.Domain.Core.Sso.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Application.Core.Common.Dtos;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;
    using Guarantor.Entities;

    public interface ISsoSpecificService : IDomainService
    {
        SsoProviderEnum ProviderEnum { get; }
        string GenerateSampleRequest(string firstName, string lastName, string ssoSsk, DateTime dob);
        SsoResponseDto PopulateResponse(SsoResponseDto response, string decryptedValue);
        bool AddVpGuarantorVerification(int vpGuarantorId, string ssoSourceSystemKey, DateTime dateOfBirth);
        IList<SsoVerification> GetVpGuarantorVerifications(int vpGuarantorId);
        void RemoveAllVerificationsForVpGuarantor(Guarantor vpGuarantor);
        bool IsVpGuarantorEligible(int vpGuarantorId);
    }
}
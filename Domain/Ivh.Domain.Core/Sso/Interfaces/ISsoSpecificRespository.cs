﻿namespace Ivh.Domain.Core.Sso.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Application.Core.Common.Dtos;
    using Entities;

    public interface ISsoSpecificRespository
    {
        /// <summary>
        /// Registration matches (Has possible VPGs)
        /// </summary>
        IList<SsoPossibleVpGuarantorResult> HasPossibleVisitPayGuarantors(string ssoSourceSystemKey, DateTime dateOfBirth);
        /// <summary>
        /// Could be registered
        /// </summary>
        IList<SsoPossibleHsGuarantorResult> HasPossibleHsGuarantorsToRegister(string ssoSourceSystemKey, DateTime dateOfBirth);
        /// <summary>
        /// Save the relationship because they agreed.  Also delete any other relationships for that ssoSSK  (happens before we save)
        /// Note:  If the VPG has multiple ssoSsk's that's ok.  This means that they just have multiple MyChart logins (SLHS only)
        /// </summary>
        IList<SsoVerificationResult> RegisterVpGuarantor(int vpGuarantorId, string ssoSourceSystemKey, DateTime dateOfBirth);

        IList<SsoVerification> GetVerificationsByVpGuarantor(int vpGuarantorId);
        void RemoveAllVerificationsForVpGuarantor(int vpGuarantorId);
        bool IsVpGuarantorEligible(int vpGuarantorId);
    }
}
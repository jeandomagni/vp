namespace Ivh.Domain.Core.Sso.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface ISsoVisitPayUserOauthTokenRepository : IRepository<SsoVisitPayUserOauthToken>
    {
        SsoVisitPayUserOauthToken GetByTokenString(string tokenString, OAuthTokenTypeEnum type);
        IList<SsoVisitPayUserOauthToken> GetAllTokensForUser(int visitPayUserId, OAuthTokenTypeEnum type, bool? active = null);
    }
}
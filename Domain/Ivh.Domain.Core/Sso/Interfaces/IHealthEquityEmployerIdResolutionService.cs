﻿namespace Ivh.Domain.Core.Sso.Interfaces
{
    using Common.Base.Interfaces;

    public interface IHealthEquityEmployerIdResolutionService : IDomainService
    {
        string GetHealthEquityEmployerId(int gurarantorId);
    }
}

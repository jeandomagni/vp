﻿namespace Ivh.Domain.Core.Sso.Interfaces
{
    public interface IHealthEquityEmployerIdResolutionProvider
    {
        string GeHealthEquityEmployerId(int guarantorId);
    }
}

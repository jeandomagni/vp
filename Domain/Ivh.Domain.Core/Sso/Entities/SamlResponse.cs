﻿namespace Ivh.Domain.Core.Sso.Entities
{
    public class SamlResponse
    {
        public string Url { get; set; }
        public string Value { get; set; }
        public string RelayState { get; set; }
    }
}

﻿namespace Ivh.Domain.Core.Sso.Entities
{
    using System;

    public class SsoVerification
    {
        public SsoVerification()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int SingleSignOnVerificationId { get; set; }
        public virtual string SingleSignOnSourceSystemKey { get; set; }
        public virtual DateTime Dob { get; set; }
        public virtual int VpGuarantorId { get; set; }
        public virtual DateTime InsertDate { get; set; }
    }
}
﻿namespace Ivh.Domain.Core.Sso.Entities
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Content.Entities;

    public class SsoProvider
    {
        public virtual int SsoProviderId { get; set; }
        public virtual string SsoProviderName { get; set; }
        public virtual string SsoProviderDisplayName { get; set; }
        public virtual string SsoProviderUrl { get; set; }
        public virtual bool IsEnabled { get; set; }
        public virtual CmsRegion SsoTermsCmsRegion { get; set; }
        public virtual string SourceSystemKeyLabel { get; set; }
        public virtual bool EnableFromManageSso { get; set; }
        public virtual string ShortDescription { get; set; }
        public virtual string SsoProviderSourceSystemKey { get; set; }
        public virtual string SsoProviderSecretSettingsKey { get; set; }
        public virtual string Callback { get; set; }
        public virtual string Scope { get; set; }
        public virtual string AuthorizePath { get; set; }
        public virtual string TokenPath { get; set; }
        public virtual string LoginPath { get; set; }
        public virtual string LogoutPath { get; set; }
        public virtual string AuthorizationServerBaseAddress { get; set; }

        public virtual SsoProviderEnum SsoProviderEnum => (SsoProviderEnum) this.SsoProviderId;

        public virtual VisitPayFeatureEnum[] RequiredFeatures
        {
            get
            {
                switch ((SsoProviderEnum) this.SsoProviderId)
                {
                    case SsoProviderEnum.HealthEquityOutbound:
                    case SsoProviderEnum.HealthEquityOAuthGrant:
                    case SsoProviderEnum.HealthEquityOAuthImplicit:
                        return new[] {VisitPayFeatureEnum.HealthEquityFeature};
                    case SsoProviderEnum.OpenEpic:
                        return new[] {VisitPayFeatureEnum.MyChartSso};
                    default:
                        return new VisitPayFeatureEnum[0];
                }
            }
        }
    }
}



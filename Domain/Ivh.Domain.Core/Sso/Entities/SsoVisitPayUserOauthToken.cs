﻿namespace Ivh.Domain.Core.Sso.Entities
{
    using System;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class SsoVisitPayUserOauthToken
    {
        public SsoVisitPayUserOauthToken()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        public virtual int SsoVisitPayUserOauthTokenId { get; set; }
        public virtual OAuthTokenTypeEnum TokenType { get; set; }
        public virtual string Token { get; set; }
        public virtual string Ticket { get; set; }
        public virtual int VisitPayUserId { get; set; }
        public virtual DateTime ExpiresOn { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual bool IsActive { get; set; }

        public virtual bool IsValid()
        {
            if (!this.IsActive) return false;
            //Needs to be valid date
            if (this.ExpiresOn < DateTime.UtcNow) return false;

            //others?

            return true;
        }
    }
}
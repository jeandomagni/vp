﻿namespace Ivh.Domain.Core.Sso.Entities
{
    using System;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Content.Entities;
    using User.Entities;

    public class SsoVisitPayUserSetting
    {
        public virtual int SsoVisitPayUserSettingId { get; set; }
        public virtual VisitPayUser VisitPayUser { get; set; }
        public virtual SsoProvider SsoProvider { get; set; }
        public virtual SsoStatusEnum SsoStatus { get; set; }
        public virtual VisitPayUser ActionVisitPayUser { get; set; }
        public virtual DateTime ActionDateTime { get; set; }
        public virtual CmsVersion SsoTermsCmsVersion { get; set; }
        public virtual DateTime? PromptAfterDateTime { get; protected internal set; }
        public virtual string Scope { get; set; }
        public virtual string RefreshToken { get; set; }
        public virtual string AccessToken { get; set; }
        public virtual int? RefreshTokenFailure { get; set; } = 0;


        public virtual void SetIgnore(bool ignore = true)
        {
            this.PromptAfterDateTime = ignore ? DateTime.MaxValue : (DateTime?)null;
        }
        public virtual bool IsIgnored()
        {
            bool value = this.PromptAfterDateTime.HasValue && this.PromptAfterDateTime.Value.Date >= DateTime.UtcNow.Date;
            return value;
        }
    }
}
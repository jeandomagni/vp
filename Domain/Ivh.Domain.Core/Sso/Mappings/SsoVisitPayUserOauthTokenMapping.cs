namespace Ivh.Domain.Core.Sso.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class SsoVisitPayUserOauthTokenMapping : ClassMap<SsoVisitPayUserOauthToken>
    {
        public SsoVisitPayUserOauthTokenMapping()
        {
            this.Schema("qa");
            this.Table("SsoVisitPayUserOauthToken");
            this.Id(x => x.SsoVisitPayUserOauthTokenId);
            this.Map(x => x.Token).Not.Nullable();
            this.Map(x => x.Ticket).Not.Nullable();
            this.Map(x => x.VisitPayUserId).Not.Nullable();
            this.Map(x => x.ExpiresOn).Nullable();
            this.Map(x => x.InsertDate);
            this.Map(x => x.IsActive).Not.Nullable();

            this.Map(x => x.TokenType).Column("OAuthTokenTypeId").CustomType<OAuthTokenTypeEnum>()
                .Not.Nullable();
        }
    }
}
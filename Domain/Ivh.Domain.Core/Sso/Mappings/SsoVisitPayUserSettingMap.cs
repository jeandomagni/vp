namespace Ivh.Domain.Core.Sso.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class SsoVisitPayUserSettingMap : ClassMap<SsoVisitPayUserSetting>
    {
        public SsoVisitPayUserSettingMap()
        {
            this.Table("SsoVisitPayUserSetting");
            this.Schema("dbo");
            this.Id(x => x.SsoVisitPayUserSettingId);
            this.References(x => x.VisitPayUser).Column("VisitPayUserId").Not.Nullable();
            this.References(x => x.SsoProvider).Column("SsoProviderId").Not.Nullable();
            this.Map(x => x.SsoStatus, "SsoStatusId").CustomType<SsoStatusEnum>().Not.Nullable();
            this.References(x => x.ActionVisitPayUser).Column("ActionVisitPayUserId").Not.Nullable();
            this.Map(x => x.ActionDateTime).Not.Nullable();
            this.References(x => x.SsoTermsCmsVersion, "SsoTermsCmsVersionId").Not.Nullable();
            this.Map(x => x.PromptAfterDateTime).Nullable();
            this.Map(x => x.Scope).Nullable();
            this.Map(x => x.AccessToken).Nullable();
            this.Map(x => x.RefreshToken).Nullable();
            this.Map(x => x.RefreshTokenFailure).Nullable();
        }
    }
}
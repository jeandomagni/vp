﻿namespace Ivh.Domain.Core.Sso.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class SsoVerificationMapping : ClassMap<SsoVerification>
    {
        public SsoVerificationMapping()
        {
            this.Schema("vp");
            this.Table("SingleSignOnVerification");
            this.Id(x => x.SingleSignOnVerificationId);
            this.Map(x => x.SingleSignOnSourceSystemKey).Column("SingleSignOn_SourceSystemKey").Not.Nullable();
            this.Map(x => x.Dob).Column("DOB").Not.Nullable();
            this.Map(x => x.VpGuarantorId).Not.Nullable();
            this.Map(x => x.InsertDate);
        }
    }
}
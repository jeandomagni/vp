namespace Ivh.Domain.Core.Sso.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class SsoProviderMap : ClassMap<SsoProvider>
    {
        public SsoProviderMap()
        {
            this.Table("SsoProvider");
            this.Schema("dbo");
            this.Id(x => x.SsoProviderId);
            this.Map(x => x.SsoProviderName).Not.Nullable();
            this.Map(x => x.SsoProviderDisplayName).Not.Nullable();
            this.Map(x => x.SsoProviderUrl).Nullable();
            this.Map(x => x.IsEnabled).Not.Nullable();
            this.Map(x => x.SourceSystemKeyLabel).Not.Nullable();
            this.Map(x => x.EnableFromManageSso).Not.Nullable();
            this.Map(x => x.ShortDescription);
            this.References(x => x.SsoTermsCmsRegion, "SsoTermsCmsRegionId").Not.Nullable();
            this.Map(x => x.SsoProviderSourceSystemKey).Nullable();
            this.Map(x => x.SsoProviderSecretSettingsKey, "SsoProviderSecret").Nullable(); // TODO: refactor this and the MerchantAccount password management to share a json setting
            this.Map(x => x.Callback).Nullable();
            this.Map(x => x.Scope).Nullable();
            this.Map(x => x.AuthorizePath).Nullable();
            this.Map(x => x.LoginPath).Nullable();
            this.Map(x => x.LogoutPath).Nullable();
            this.Map(x => x.TokenPath).Nullable();
            this.Map(x => x.AuthorizationServerBaseAddress).Nullable();

        }
    }
}
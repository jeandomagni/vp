﻿
namespace Ivh.Domain.Core.Chat.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class ChatAvailabilityMapping : ClassMap<ChatAvailability>
    {
        public ChatAvailabilityMapping()
        {
            this.Schema("dbo");
            this.Table("ChatAvailability");
            this.Id(x => x.ChatAvailabilityId).Not.Nullable();
            this.Map(x => x.DayOfWeek).Nullable().ReadOnly();
            this.Map(x => x.Date).Nullable();
            this.Map(x => x.TimeStart).CustomType<NHibernate.Type.TimeAsTimeSpanType>().Nullable();
            this.Map(x => x.TimeEnd).CustomType<NHibernate.Type.TimeAsTimeSpanType>().Nullable();
        }
    }
}

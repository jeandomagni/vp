﻿
namespace Ivh.Domain.Core.Chat.Entities
{
    using System;

    public class ChatAvailability
    {
        public virtual int ChatAvailabilityId { get; set; }
        public virtual int? DayOfWeek { get; set; }
        public virtual DateTime? Date { get; set; }
        public virtual TimeSpan? TimeStart { get; set; }
        public virtual TimeSpan? TimeEnd { get; set; }
    }
}

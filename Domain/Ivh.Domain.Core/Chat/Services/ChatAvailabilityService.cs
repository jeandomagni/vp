﻿
namespace Ivh.Domain.Core.Chat.Services
{
    using Base.Interfaces;
    using Base.Services;
    using Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Entities;

    public class ChatAvailabilityService : DomainService, IChatAvailabilityService
    {
        private readonly Lazy<IChatAvailabilityRepository> _chatAvailabilityRepository;

        public ChatAvailabilityService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IChatAvailabilityRepository> chatAvailabilityRepository) : base(serviceCommonService)
        {
            this._chatAvailabilityRepository = chatAvailabilityRepository;
        }

        public IList<ChatAvailability> GetChatAvailabilities()
        {
            IList<ChatAvailability> chatAvailabilities = this._chatAvailabilityRepository.Value.GetQueryable().ToList();
            return chatAvailabilities;
        }

        public void SaveChatAvailability(ChatAvailability chatAvailability)
        {
            this._chatAvailabilityRepository.Value.InsertOrUpdate(chatAvailability);
        }

        public void DeleteChatAvailability(int chatAvailabilityId)
        {
            ChatAvailability entity = this._chatAvailabilityRepository.Value.GetById(chatAvailabilityId);
            if (entity == null)
            {
                return;
            }

            //Only allow deleting custom chat availabilities (ones without a DayOfWeek)
            if (entity.DayOfWeek.HasValue)
            {
                return;
            }

            this._chatAvailabilityRepository.Value.Delete(entity);
        }
    }
}

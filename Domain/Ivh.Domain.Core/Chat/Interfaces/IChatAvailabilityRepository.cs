﻿
namespace Ivh.Domain.Core.Chat.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IChatAvailabilityRepository : IRepository<ChatAvailability>
    {
    }
}

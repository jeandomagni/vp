﻿
namespace Ivh.Domain.Core.Chat.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IChatAvailabilityService : IDomainService
    {
        IList<ChatAvailability> GetChatAvailabilities();
        void SaveChatAvailability(ChatAvailability chatAvailability);
        void DeleteChatAvailability(int chatAvailabilityId);
    }
}

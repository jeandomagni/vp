﻿namespace Ivh.Domain.Core.Modules
{
    using Audit.Interfaces;
    using Audit.Services;
    using Autofac;
    using IpAccess.Interfaces;
    using IpAccess.Services;
    using SystemMessage.Interfaces;
    using SystemMessage.Services;
    using Alert.Interfaces;
    using Alert.Services;
    using Chat.Interfaces;
    using Chat.Services;
    using Eob.Interfaces;
    using Eob.Services;
    using Guarantor.Interfaces;
    using Guarantor.Services;
    using KnowledgeBase.Interfaces;
    using KnowledgeBase.Services;
    using Settings.Interfaces;
    using Settings.Services;
    using TreatmentLocation.Interfaces;
    using TreatmentLocation.Services;
    using User.Interfaces;
    using User.Services;
    using Visit.Interfaces;
    using Visit.Services;
    using VisitPayDocument.Interfaces;
    using VisitPayDocument.Services;
    using Ivh.Domain.Core.ThirdPartyKey.Interfaces;
    using Ivh.Domain.Core.ThirdPartyKey.Services;
    using VanityUrl.Interfaces;
    using VanityUrl.Services;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<VisitPayRoleService>().As<IVisitPayRoleService>();

            builder.RegisterType<ClientService>().As<IClientService>();

            builder.RegisterType<BillingSystemService>().As<IBillingSystemService>();
            builder.RegisterType<FeatureService>().As<IFeatureService>();
            builder.RegisterType<IpAccessService>().As<IIpAccessService>().SingleInstance();
            builder.RegisterType<SecurityQuestionService>().As<ISecurityQuestionService>();
            builder.RegisterType<SystemMessageVisitPayUserService>().As<ISystemMessageVisitPayUserService>();
            builder.RegisterType<TreatmentLocationService>().As<ITreatmentLocationService>();
            builder.RegisterType<VisitStateHistoryService>().As<IVisitStateHistoryService>();
            builder.RegisterType<VisitAgingHistoryService>().As<IVisitAgingHistoryService>();
            builder.RegisterType<VisitItemizationStorageService>().As<IVisitItemizationStorageService>();
            builder.RegisterType<VisitPayUserJournalEventService>().As<IVisitPayUserJournalEventService>();
            builder.RegisterType<VpUserLoginSummaryService>().As<IVpUserLoginSummaryService>();
            builder.RegisterType<AuditService>().As<IAuditService>();
            builder.RegisterType<AlertService>().As<IAlertService>();
            builder.RegisterType<KnowledgeBaseService>().As<IKnowledgeBaseService>();
            builder.RegisterType<AuditEventService>().As<IAuditEventService>();
            builder.RegisterType<VisitPayDocumentService>().As<IVisitPayDocumentService>();
            builder.RegisterType<ChatAvailabilityService>().As<IChatAvailabilityService>();
            builder.RegisterType<ThirdPartyKeyService>().As<IThirdPartyKeyService>();
            builder.RegisterType<ThirdPartyValueService>().As<IThirdPartyValueService>();
            builder.RegisterType<VanityUrlService>().As<IVanityUrlService>();
        }
    }
}

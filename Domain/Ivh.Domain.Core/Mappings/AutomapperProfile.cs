﻿namespace Ivh.Domain.Core.Mappings
{
    using System.Linq;
    using Application.HospitalData.Common.Models;
    using AutoMapper;
    using Guarantor.Entities;
    using Ivh.Application.Core.Common.Dtos.Eob;
    using Ivh.Application.Core.Common.Models.Eob;
    using Ivh.Domain.Eob.Entities;
    using EobIndicatorResultModel = Application.Core.Common.Models.Eob.EobIndicatorResultModel;
    using VisitEobClaimAdjustmentModel = Application.Core.Common.Models.Eob.VisitEobClaimAdjustmentModel;
    using VisitEobDetailsResultModel = Application.Core.Common.Models.Eob.VisitEobDetailsResultModel;
    using VisitEobIndustryCodeIdentificationModel = Application.Core.Common.Models.Eob.VisitEobIndustryCodeIdentificationModel;

    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            // Automapper mapping go here.
            // Use this.CreateMap<,>().
            // DO NOT USE Mapper.CreateMap<,>().

            this.CreateMap<HsGuarantorVpSearchResultModel,HsGuarantorVpSearchResult>();

            this.CreateMap<VisitEobDetailsResultModel, VisitEobDetails>()
                .ForMember(dest => dest.ClaimAdjustments, opts => opts.MapFrom(src => src.ServiceLevelClaimAdjustmentsSummed.Any() ? src.ServiceLevelClaimAdjustmentsSummed : src.ClaimLevelClaimAdjustmentsSummed));
            this.CreateMap<VisitEobClaimAdjustmentModel, VisitEobClaimAdjustment>();
            this.CreateMap<VisitEobIndustryCodeIdentificationModel, VisitEobIndustryCodeIdentification>();
            this.CreateMap<HsGuarantorModel, HsGuarantorDetails>();
            this.CreateMap<EobIndicatorResultModel, EobIndicatorResult>();

            this.CreateMap<ClaimPaymentInformation, VisitEobDetailsResultDto>()
                .ForMember(dest => dest.VisitSourceSystemKey, opts => opts.MapFrom(src => src.VisitSourceSystemKey))
                .ForMember(dest => dest.VisitBillingSystemId, opts => opts.MapFrom(src => src.BillingSystemId))
                .ForMember(dest => dest.ClaimStatusCode, opts => opts.MapFrom(src => src.Clp02ClaimStatusCode))
                .ForMember(dest => dest.TotalClaimChargeAmount, opts => opts.MapFrom(src => src.Clp03MonetaryAmount))
                .ForMember(dest => dest.ClaimPaymentAmount, opts => opts.MapFrom(src => src.Clp04MonetaryAmount))
                .ForMember(dest => dest.PatientResponsibilityAmount, opts => opts.MapFrom(src => src.Clp05MonetaryAmount))
                // ignore the manually mapped pieces
                .ForMember(dest => dest.ServicePaymentInformations, opts => opts.Ignore())
                .ForMember(dest => dest.HeaderNumber, opts => opts.Ignore())
                .ForMember(dest => dest.TransactionDate, opts => opts.Ignore())
                .ForMember(dest => dest.PayerName, opts => opts.Ignore())
                .ForMember(dest => dest.PayerLinkUrl, opts => opts.Ignore())
                .ForMember(dest => dest.PayerIcon, opts => opts.Ignore())
                .ForMember(dest => dest.PlanNumber, opts => opts.Ignore());

            this.CreateMap<ClaimAdjustmentPivot, VisitEobClaimAdjustmentDto>()
                .ForMember(dest => dest.UIDisplay, opts => opts.MapFrom(src => src.EobClaimAdjustmentReasonCode.UIDisplay));

            this.CreateMap<ServicePaymentInformation, VisitEobServicePaymentInformationDto>();

            this.CreateMap<IndustryCodeIdentification, VisitEobIndustryCodeIdentificationDto>()
                .ForMember(dest => dest.IndustryCode, opts => opts.MapFrom(src => src.Lq02IndustryCode))
                .ForMember(dest => dest.Description, opts => opts.MapFrom(src => src.EobRemittanceAdviceRemarkCode.Description));

            this.CreateMap<EobIndicatorResult, EobIndicatorResultDto>();
            this.CreateMap<VisitEobIndustryCodeIdentification, VisitEobIndustryCodeIdentificationDto>();
            this.CreateMap<VisitEobClaimAdjustment, VisitEobClaimAdjustmentDto>();
        }
    }
}

﻿namespace Ivh.Domain.Core.Alert.Services
{
    using System;
    using System.Collections.Generic;
    using Base.Interfaces;
    using Base.Services;
    using Entities;
    using Interfaces;

    public class AlertService : DomainService, IAlertService
    {
        private readonly Lazy<IAlertRepository> _alertRepository;

        public AlertService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IAlertRepository> alertRepository) : base(serviceCommonService)
        {
            this._alertRepository = alertRepository;
        }

        public IList<Alert> GetActiveAlerts()
        {
            return this._alertRepository.Value.GetActiveAlerts();
        }
    }
}
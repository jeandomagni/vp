﻿namespace Ivh.Domain.Core.Alert.Interfaces
{
    using System.Collections.Generic;
    using Entities;

    public interface IAlertRepository
    {
        IList<Alert> GetActiveAlerts();
    }
}
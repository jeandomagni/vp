﻿namespace Ivh.Domain.Core.Alert.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IAlertService : IDomainService
    {
        IList<Alert> GetActiveAlerts();
    }
}
﻿namespace Ivh.Domain.Core.Alert.Entities
{
    using Common.VisitPay.Enums;

    public class Alert
    {
        public virtual int AlertId { get; set; }
        public virtual AlertTypeEnum AlertType { get; set; }
        public virtual AlertCriteriaEnum? AlertCriteria { get; set; }
        public virtual CmsRegionEnum CmsRegion { get; set; }
        public virtual int DisplayOrder { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual bool IsDismissable { get; set; }
    }
}
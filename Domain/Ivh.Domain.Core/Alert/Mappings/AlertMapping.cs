﻿namespace Ivh.Domain.Core.Alert.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class AlertMapping : ClassMap<Alert>
    {
        public AlertMapping()
        {
            this.Schema("dbo");
            this.Table("Alert");
            this.ReadOnly();
            this.Id(x => x.AlertId).Not.Nullable();
            this.Map(x => x.AlertType, "AlertTypeId").CustomType<AlertTypeEnum>().Not.Nullable();
            this.Map(x => x.AlertCriteria, "AlertCriteriaId").CustomType<AlertCriteriaEnum?>().Nullable();
            this.Map(x => x.CmsRegion, "CmsRegionId").CustomType<CmsRegionEnum>().Not.Nullable();
            this.Map(x => x.DisplayOrder).Not.Nullable();
            this.Map(x => x.IsActive).Nullable();
            this.Map(x => x.IsDismissable).Not.Nullable();

            this.Cache.ReadOnly();
        }
    }
}
﻿namespace Ivh.Domain.Core.Audit.Entities
{
    using System;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Ivh.Common.Base.Utilities.Helpers;

    public class AuditUserChange
    {
        public virtual string UserName { get; set; }
        public virtual string Email { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual DateTime EventDate { get; set; }
        public virtual string EventType { get; set; }
        public virtual string IpAddress { get; set; }
        public virtual string TargetUserName { get; set; }
        public virtual int TargetVisitPayUserId { get; set; }
        public virtual JournalEventTypeEnum JournalEventTypeId { get; set; }
        public virtual string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);
        public virtual string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);
    }
}
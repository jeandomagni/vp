﻿namespace Ivh.Domain.Core.Audit.Entities
{
    using Ivh.Common.Base.Utilities.Helpers;
    using System;

    public class AuditUserAccess
    {
        public virtual string UserName { get; set; }
        public virtual string Email { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual DateTime EventDate { get; set; }
        public virtual string EventType { get; set; }
        public virtual string IpAddress { get; set; }
        public virtual string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName,this.FirstName);
        public virtual string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName,this.LastName);
    }
}
﻿namespace Ivh.Domain.Core.Audit.Entities
{
    using System;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Ivh.Common.Base.Utilities.Helpers;

    public class AuditUserChangeDetail
    {
        public virtual DateTime ChangeTime { get; set; }
        public virtual DateTime ExpiredTime { get; set; }
        public virtual int VisitPayUserId { get; set; }
        public virtual string PriorUserName { get; set; }
        public virtual string UserName { get; set; }
        public virtual string PriorEmail { get; set; }
        public virtual string Email { get; set; }
        public virtual string PriorFirstName { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string PriorLastName { get; set; }
        public virtual string LastName { get; set; }
        public virtual bool PriorLockoutEnabled { get; set; }
        public virtual bool LockoutEnabled { get; set; }
        public virtual DateTime PriorLockoutEndDateUtc { get; set; }
        public virtual DateTime LockoutEndDateUtc { get; set; }
        public virtual JournalEventTypeEnum JournalEventTypeId { get; set; }
        public virtual string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);
        public virtual string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);
        public virtual string PriorLastNameFirstName => FormatHelper.LastNameFirstName(this.PriorLastName, this.PriorFirstName);
        public virtual string PriorFirstNameLastName => FormatHelper.FirstNameLastName(this.PriorFirstName, this.PriorLastName);
    }
}
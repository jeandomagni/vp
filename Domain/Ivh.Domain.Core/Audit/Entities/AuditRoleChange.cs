﻿namespace Ivh.Domain.Core.Audit.Entities
{
    using System;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class AuditRoleChange
    {
        public virtual int VisitPayUserId { get; set; }
        public virtual string UserName { get; set; }
        public virtual string Email { get; set; }
        public virtual int VisitPayRoleId { get; set; }
        public virtual int ChangeEventId { get; set; }
        public virtual int ExpiredChangeEventId { get; set; }
        public virtual DateTime ChangeTime { get; set; }
        public virtual DateTime ExpiredTime { get; set; }
        public virtual DateTime PriorDelete { get; set; }
        public virtual JournalEventTypeEnum JournalEventTypeId { get; set; }
    }
}
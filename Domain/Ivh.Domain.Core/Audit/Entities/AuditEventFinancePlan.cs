﻿namespace Ivh.Domain.Core.Audit.Entities
{
    using System;

    public class AuditEventFinancePlan
    {
        public AuditEventFinancePlan()
        {
            this.InsertDate = DateTime.UtcNow;
        }

        public virtual int AuditEventId { get; set; }
        public virtual DateTime AuditEventDate { get; set; }


        public virtual int FinancePlanId { get; set; }
        public virtual int VpGuarantorId { get; set; }
        public virtual int? FinancePlanOfferId { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual DateTime? OriginationDate { get; set; }
        public virtual DateTime? TermsAgreedDate { get; set; }
        public virtual int? TermsCmsVersionId { get; set; }
        public virtual decimal OriginatedFinancePlanBalance { get; set; }
        public virtual decimal AdjustmentsSinceOrigination { get; set; }
        public virtual decimal? InterestAssessed { get; set; }
        public virtual decimal CurrentFinancePlanBalance { get; set; }
        public virtual int CreatedVpStatementId { get; set; }
        public virtual decimal? OriginalPaymentAmount { get; set; }
        public virtual int? OriginalDuration { get; set; }
        public virtual int? OriginalFinancePlanId { get; set; }
        public virtual int? ReconfiguredFinancePlanId { get; set; }
        public virtual int CurrentBucket { get; set; }
        public virtual string OfferCalculationStrategy { get; set; }
        public virtual int? CurrentBucketOverride { get; set; }
        public virtual int? CustomizedVisitPayUserId { get; set; }
        public virtual int? AcceptedTermsVisitPayUserId { get; set; }
        public virtual int CurrentFinancePlanStatusId { get; set; }
        public virtual bool IsCombined { get; set; }
        public virtual int? CombinedFinancePlanId { get; set; }
        public virtual string FinancePlanUniqueId { get; set; }
        public virtual DateTime? FirstPaymentDueDate { get; set; }

        public virtual decimal CurrentPaymentAmount { get; set; }
        public virtual int? CurrentDuration { get; set; }
        public virtual decimal OriginalInterestRate { get; set; }
        public virtual decimal CurrentInterestRate { get; set; }
        public virtual string CurrentFinancePlanStatus { get; set; }
        public virtual string PreviousFinancePlanStatus { get; set; }
        public virtual int? RemainingMonths { get; set; }
        public virtual DateTime? ExpectedMaturityDate { get; set; }
        public virtual decimal PrincipalPaidToDate { get; set; }
        public virtual string UpdateEvent { get; set; }
        public virtual string AssociatedVisits { get; set; }
        public virtual int? FinancePlanOfferSetTypeId { get; set; }
    }
}

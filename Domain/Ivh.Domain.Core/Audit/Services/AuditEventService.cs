﻿namespace Ivh.Domain.Core.Audit.Services
{
    using System;
    using Base.Interfaces;
    using Base.Services;
    using Entities;
    using Interfaces;

    public class AuditEventService : DomainService, IAuditEventService
    {
        private readonly Lazy<IAuditEventFinancePlanRepository> _auditEventFinancePlanRepository;

        public AuditEventService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IAuditEventFinancePlanRepository> auditEventFinancePlanRepository) : base(serviceCommonService)
        {
            this._auditEventFinancePlanRepository = auditEventFinancePlanRepository;
        }

        public void LogAuditEventFinancePlan(AuditEventFinancePlan auditEventFinancePlan)
        {
            this._auditEventFinancePlanRepository.Value.Insert(auditEventFinancePlan);
        }
    }
}

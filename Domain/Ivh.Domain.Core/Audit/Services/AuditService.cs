﻿namespace Ivh.Domain.Core.Audit.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Enums;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;

    public class AuditService : DomainService, IAuditService
    {
        private readonly IAuditRepository _repository;

        public AuditService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            IAuditRepository repository) : base(serviceCommonService)
        {
            this._repository = repository;
        }
        public IList<AuditUserAccess> GetUserAccessLogsSince(AuditFilter auditFilter)
        {
            IList<AuditUserAccess> results = this._repository.GetUserAccessLogsSince(auditFilter.StartDate, auditFilter.EndDate);
            if (!auditFilter.IncludeClient)
            {
                //Exclude non-Ivh users
                results = results.Where(r => r.Email.IsVisitPayEmailAddress())
                                 .ToList();
            }
            if (!auditFilter.IncludeIvinci)
            {
                //Exclude Ivh users
                results = results.Where(r => !r.Email.IsVisitPayEmailAddress())
                                 .ToList();
            }
            bool isAscendingOrder = "asc".Equals(auditFilter.SortOrder, StringComparison.InvariantCultureIgnoreCase);
            switch (auditFilter.SortField)
            {
                case "UserName":
                    return isAscendingOrder ? results.OrderBy(r => r.UserName).ToList() : results.OrderByDescending(r => r.UserName).ToList();
                case "Email":
                    return isAscendingOrder ? results.OrderBy(r => r.Email).ToList() : results.OrderByDescending(r => r.Email).ToList();
                case "FirstName":
                    return isAscendingOrder ? results.OrderBy(r => r.FirstName).ToList() : results.OrderByDescending(r => r.FirstName).ToList();
                case "LastName":
                    return isAscendingOrder ? results.OrderBy(r => r.LastName).ToList() : results.OrderByDescending(r => r.LastName).ToList();
                case "EventDate":
                    return isAscendingOrder ? results.OrderBy(r => r.EventDate).ToList() : results.OrderByDescending(r => r.EventDate).ToList();
                case "EventType":
                    return isAscendingOrder ? results.OrderBy(r => r.EventType).ToList() : results.OrderByDescending(r => r.EventType).ToList();
                case "IpAddress":
                    return isAscendingOrder ? results.OrderBy(r => r.IpAddress).ToList() : results.OrderByDescending(r => r.IpAddress).ToList();
                default:
                    return results;
            }
        }

        public IList<AuditUserChange> GetUserChangeLogsSince(AuditFilter auditFilter)
        {
            IList<AuditUserChange> results = this._repository.GetUserChangeLogsSince(auditFilter.StartDate, auditFilter.EndDate);
            if (!auditFilter.IncludeIvinci)
            {
                results = results.Where(r => r.JournalEventTypeId != JournalEventTypeEnum.ClientIvhAdminModifyClientAccount).ToList();
            }

            if (!auditFilter.IncludeClient)
            {
                results = results.Where(r => r.JournalEventTypeId != JournalEventTypeEnum.ClientModifiedAClientUserAcct).ToList();
            }
            bool isAscendingOrder = "asc".Equals(auditFilter.SortOrder, StringComparison.InvariantCultureIgnoreCase);
            switch (auditFilter.SortField)
            {
                case "UserName":
                    return isAscendingOrder ? results.OrderBy(r => r.UserName).ToList() : results.OrderByDescending(r => r.UserName).ToList();
                case "Email":
                    return isAscendingOrder ? results.OrderBy(r => r.Email).ToList() : results.OrderByDescending(r => r.Email).ToList();
                case "FirstName":
                    return isAscendingOrder ? results.OrderBy(r => r.FirstName).ToList() : results.OrderByDescending(r => r.FirstName).ToList();
                case "LastName":
                    return isAscendingOrder ? results.OrderBy(r => r.LastName).ToList() : results.OrderByDescending(r => r.LastName).ToList();
                case "EventDate":
                    return isAscendingOrder ? results.OrderBy(r => r.EventDate).ToList() : results.OrderByDescending(r => r.EventDate).ToList();
                case "EventType":
                    return isAscendingOrder ? results.OrderBy(r => r.EventType).ToList() : results.OrderByDescending(r => r.EventType).ToList();
                case "IpAddress":
                    return isAscendingOrder ? results.OrderBy(r => r.IpAddress).ToList() : results.OrderByDescending(r => r.IpAddress).ToList();
                case "TargetUserName":
                    return isAscendingOrder ? results.OrderBy(r => r.TargetUserName).ToList() : results.OrderByDescending(r => r.TargetUserName).ToList();
                case "TargetVisitPayUserId":
                    return isAscendingOrder ? results.OrderBy(r => r.TargetVisitPayUserId).ToList() : results.OrderByDescending(r => r.TargetVisitPayUserId).ToList();
                default:
                    return results;
            }
        }

        public IList<AuditUserChangeDetail> GetUserChangeDetailLogsSince(AuditFilter auditFilter)
        {
            IList<AuditUserChangeDetail> results = this._repository.GetUserChangeDetailLogsSince(auditFilter.StartDate, auditFilter.EndDate);
            if (!auditFilter.IncludeIvinci)
            {
                results = results.Where(r => r.JournalEventTypeId != JournalEventTypeEnum.ClientIvhAdminModifyClientAccount).ToList();
            }

            if (!auditFilter.IncludeClient)
            {
                results = results.Where(r => r.JournalEventTypeId != JournalEventTypeEnum.ClientModifiedAClientUserAcct).ToList();
            }
            bool isAscendingOrder = "asc".Equals(auditFilter.SortOrder, StringComparison.InvariantCultureIgnoreCase);
            switch (auditFilter.SortField)
            {
                case "ChangeTime":
                    return isAscendingOrder ? results.OrderBy(r => r.ChangeTime).ToList() : results.OrderByDescending(r => r.ChangeTime).ToList();
                case "ExpiredTime":
                    return isAscendingOrder ? results.OrderBy(r => r.ExpiredTime).ToList() : results.OrderByDescending(r => r.ExpiredTime).ToList();
                case "VisitPayUserId":
                    return isAscendingOrder ? results.OrderBy(r => r.VisitPayUserId).ToList() : results.OrderByDescending(r => r.VisitPayUserId).ToList();
                case "PriorUserName":
                    return isAscendingOrder ? results.OrderBy(r => r.PriorUserName).ToList() : results.OrderByDescending(r => r.PriorUserName).ToList();
                case "UserName":
                    return isAscendingOrder ? results.OrderBy(r => r.UserName).ToList() : results.OrderByDescending(r => r.UserName).ToList();
                case "PriorEmail":
                    return isAscendingOrder ? results.OrderBy(r => r.PriorEmail).ToList() : results.OrderByDescending(r => r.PriorEmail).ToList();
                case "Email":
                    return isAscendingOrder ? results.OrderBy(r => r.Email).ToList() : results.OrderByDescending(r => r.Email).ToList();
                case "PriorFirstName":
                    return isAscendingOrder ? results.OrderBy(r => r.PriorFirstName).ToList() : results.OrderByDescending(r => r.PriorFirstName).ToList();
                case "FirstName":
                    return isAscendingOrder ? results.OrderBy(r => r.FirstName).ToList() : results.OrderByDescending(r => r.FirstName).ToList();
                case "PriorLastName":
                    return isAscendingOrder ? results.OrderBy(r => r.PriorLastName).ToList() : results.OrderByDescending(r => r.PriorLastName).ToList();
                case "LastName":
                    return isAscendingOrder ? results.OrderBy(r => r.LastName).ToList() : results.OrderByDescending(r => r.LastName).ToList();
                case "PriorLockoutEnabled":
                    return isAscendingOrder ? results.OrderBy(r => r.PriorLockoutEnabled).ToList() : results.OrderByDescending(r => r.PriorLockoutEnabled).ToList();
                case "LockoutEnabled":
                    return isAscendingOrder ? results.OrderBy(r => r.LockoutEnabled).ToList() : results.OrderByDescending(r => r.LockoutEnabled).ToList();
                case "PriorLockoutEndDateUtc":
                    return isAscendingOrder ? results.OrderBy(r => r.PriorLockoutEndDateUtc).ToList() : results.OrderByDescending(r => r.PriorLockoutEndDateUtc).ToList();
                case "LockoutEndDateUtc":
                    return isAscendingOrder ? results.OrderBy(r => r.LockoutEndDateUtc).ToList() : results.OrderByDescending(r => r.LockoutEndDateUtc).ToList();
                default:
                    return results;
            }
        }

        public IList<AuditRoleChange> GetRoleChangeLogsSince(AuditFilter auditFilter)
        {
            IList<AuditRoleChange> results = this._repository.GetRoleChangeLogsSince(auditFilter.StartDate, auditFilter.EndDate);
            if (!auditFilter.IncludeIvinci)
            {
                results = results.Where(r => r.JournalEventTypeId != JournalEventTypeEnum.ClientIvhAdminModifyClientAccount).ToList();
            }

            if (!auditFilter.IncludeClient)
            {
                results = results.Where(r => r.JournalEventTypeId != JournalEventTypeEnum.ClientModifiedAClientUserAcct).ToList();
            }
            bool isAscendingOrder = "asc".Equals(auditFilter.SortOrder, StringComparison.InvariantCultureIgnoreCase);
            switch (auditFilter.SortField)
            {
                case "VisitPayUserId":
                    return isAscendingOrder ? results.OrderBy(r => r.VisitPayUserId).ToList() : results.OrderByDescending(r => r.VisitPayUserId).ToList();
                case "UserName":
                    return isAscendingOrder ? results.OrderBy(r => r.UserName).ToList() : results.OrderByDescending(r => r.UserName).ToList();
                case "VisitPayRoleId":
                    return isAscendingOrder ? results.OrderBy(r => r.VisitPayRoleId).ToList() : results.OrderByDescending(r => r.VisitPayRoleId).ToList();
                case "ChangeEventId":
                    return isAscendingOrder ? results.OrderBy(r => r.ChangeEventId).ToList() : results.OrderByDescending(r => r.ChangeEventId).ToList();
                case "ExpiredTime":
                    return isAscendingOrder ? results.OrderBy(r => r.ExpiredTime).ToList() : results.OrderByDescending(r => r.ExpiredTime).ToList();
                case "PriorDelete":
                    return isAscendingOrder ? results.OrderBy(r => r.PriorDelete).ToList() : results.OrderByDescending(r => r.PriorDelete).ToList();
                default:
                    return results;
            }
        }
    }
}
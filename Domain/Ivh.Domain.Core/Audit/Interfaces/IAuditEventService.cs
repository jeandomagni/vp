﻿namespace Ivh.Domain.Core.Audit.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IAuditEventService : IDomainService
    {
        void LogAuditEventFinancePlan(AuditEventFinancePlan auditEventFinancePlan);
    }
}

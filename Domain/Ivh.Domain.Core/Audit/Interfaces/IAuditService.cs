﻿namespace Ivh.Domain.Core.Audit.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IAuditService : IDomainService
    {
        IList<AuditUserAccess> GetUserAccessLogsSince(AuditFilter auditFilter);
        IList<AuditUserChange> GetUserChangeLogsSince(AuditFilter auditFilter);
        IList<AuditUserChangeDetail> GetUserChangeDetailLogsSince(AuditFilter auditFilter);
        IList<AuditRoleChange> GetRoleChangeLogsSince(AuditFilter auditFilter);
    }
}
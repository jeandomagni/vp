﻿namespace Ivh.Domain.Core.Audit.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IAuditRepository
    {
        IList<AuditUserAccess> GetUserAccessLogsSince(DateTime startDate, DateTime? endDate);
        IList<AuditUserChange> GetUserChangeLogsSince(DateTime startDate, DateTime? endDate);
        IList<AuditUserChangeDetail> GetUserChangeDetailLogsSince(DateTime startDate, DateTime? endDate);
        IList<AuditRoleChange> GetRoleChangeLogsSince(DateTime startDate, DateTime? endDate);
    }
}
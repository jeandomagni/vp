﻿namespace Ivh.Domain.Core.Audit.Interfaces
{
    using System;

    public class AuditFilter
    {
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IncludeIvinci { get; set; }
        public bool IncludeClient { get; set; }
        public string SortField { get; set; }
        public string SortOrder { get; set; }
    }
}
﻿namespace Ivh.Domain.Core.Audit.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class AuditEventFinancePlanMapping : ClassMap<AuditEventFinancePlan>
    {
        public AuditEventFinancePlanMapping()
        {
            this.Schema("audit");
            this.Table("FinancePlan");
            this.Id(x => x.AuditEventId);
            this.Map(x => x.AuditEventDate).Not.Update().Not.Update();

            this.Map(x => x.FinancePlanId).Not.Nullable().Not.Update();
            this.Map(x => x.VpGuarantorId).Not.Nullable().Not.Update();
            this.Map(x => x.FinancePlanOfferId).Nullable().Not.Update();
            this.Map(x => x.InsertDate);
            this.Map(x => x.OriginationDate).Nullable().Not.Update();
            this.Map(x => x.TermsAgreedDate).Nullable().Not.Update();
            this.Map(x => x.TermsCmsVersionId).Nullable().Not.Update();
            this.Map(x => x.OriginatedFinancePlanBalance).Not.Nullable().Not.Update();
            this.Map(x => x.AdjustmentsSinceOrigination).Not.Nullable().Not.Update();
            this.Map(x => x.InterestAssessed).Nullable().Not.Update();
            this.Map(x => x.CurrentFinancePlanBalance).Not.Nullable().Not.Update();
            this.Map(x => x.CreatedVpStatementId).Nullable().Not.Update();
            this.Map(x => x.OriginalPaymentAmount).Nullable().Not.Update();
            this.Map(x => x.OriginalDuration).Nullable().Not.Update();
            this.Map(x => x.OriginalFinancePlanId).Nullable().Not.Update();
            this.Map(x => x.ReconfiguredFinancePlanId).Nullable().Not.Update();
            this.Map(x => x.CurrentBucket).Nullable().Not.Update();
            this.Map(x => x.OfferCalculationStrategy).Not.Nullable().Not.Update();
            this.Map(x => x.CurrentBucketOverride).Nullable().Not.Update();
            this.Map(x => x.CustomizedVisitPayUserId).Nullable().Not.Update();
            this.Map(x => x.AcceptedTermsVisitPayUserId).Nullable().Not.Update();
            this.Map(x => x.CurrentFinancePlanStatusId).Not.Nullable().Not.Update();
            this.Map(x => x.IsCombined).Not.Nullable().Not.Update();
            this.Map(x => x.CombinedFinancePlanId).Nullable().Not.Update();
            this.Map(x => x.FinancePlanUniqueId).Nullable().Not.Update();
            this.Map(x => x.FirstPaymentDueDate).Nullable().Not.Update();

            this.Map(x => x.CurrentPaymentAmount).Not.Nullable().Not.Update();
            this.Map(x => x.CurrentDuration).Nullable().Not.Update();
            this.Map(x => x.OriginalInterestRate).Not.Nullable().Not.Update();
            this.Map(x => x.CurrentInterestRate).Not.Nullable().Not.Update();
            this.Map(x => x.CurrentFinancePlanStatus).Not.Nullable().Not.Update();
            this.Map(x => x.PreviousFinancePlanStatus).Nullable().Not.Update();
            this.Map(x => x.RemainingMonths).Not.Nullable().Not.Update();
            this.Map(x => x.ExpectedMaturityDate).Nullable().Not.Update();
            this.Map(x => x.PrincipalPaidToDate).Nullable().Not.Update();
            this.Map(x => x.UpdateEvent).Nullable().Not.Update().Length(10000);
            this.Map(x => x.AssociatedVisits).Not.Nullable().Not.Update().Length(10000);
            this.Map(x => x.FinancePlanOfferSetTypeId).Nullable().Not.Update();
        }
    }
}

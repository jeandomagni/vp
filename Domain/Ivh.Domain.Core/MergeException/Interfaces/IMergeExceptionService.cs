﻿namespace Ivh.Domain.Core.MergeException.Interfaces
{
    using System.Runtime.Serialization;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;

    public interface IMergeExceptionService : IDomainService
    {
        void SaveMergeException(object mergeExceptionObject, MergeExceptionTypeEnum mergeExceptionType);
    }
}
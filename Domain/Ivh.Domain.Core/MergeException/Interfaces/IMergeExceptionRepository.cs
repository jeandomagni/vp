﻿namespace Ivh.Domain.Core.MergeException.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IMergeExceptionRepository : IRepository<MergeException>
    {
    }
}
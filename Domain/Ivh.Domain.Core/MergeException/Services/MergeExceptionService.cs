﻿namespace Ivh.Domain.Core.MergeException.Services
{
    using System;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;
    using Newtonsoft.Json;

    public class MergeExceptionService : DomainService, IMergeExceptionService
    {
        private readonly IMergeExceptionRepository _mergeExceptionRepository;

        public MergeExceptionService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            IMergeExceptionRepository mergeExceptionRepository) : base(serviceCommonService)
        {
            this._mergeExceptionRepository = mergeExceptionRepository;
        }

        public void SaveMergeException(object mergeExceptionObject, MergeExceptionTypeEnum mergeExceptionType)
        {
            if (!mergeExceptionObject.GetType().IsSerializable)
            {
                throw new ArgumentException("mergeExceptionObject is not serializable", nameof(mergeExceptionObject));
            }

            this._mergeExceptionRepository.InsertOrUpdate(new MergeException
            {
                MergeExceptionType = mergeExceptionType,
                SerializedContent = JsonConvert.SerializeObject(mergeExceptionObject)
            });
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.Core.MergeException.Entities
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class MergeException
    {
        public MergeException()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int MergeExceptionId { get; set; }
        public virtual MergeExceptionTypeEnum MergeExceptionType { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual string SerializedContent { get; set; }
    }
}

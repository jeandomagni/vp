﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.Core.MergeException.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class MergeExceptionMap : ClassMap<MergeException>
    {
        public MergeExceptionMap()
        {
            this.Schema("dbo");
            this.Table("MergeException");
            this.Id(x => x.MergeExceptionId);
            this.Map(x => x.MergeExceptionType, "MergeExceptionTypeId").CustomType<MergeExceptionTypeEnum>().Not.Nullable();
            this.Map(x => x.InsertDate);
            this.Map(x => x.SerializedContent).Not.Nullable();
        }
    }
}

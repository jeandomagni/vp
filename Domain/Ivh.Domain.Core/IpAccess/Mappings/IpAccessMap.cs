﻿using FluentNHibernate.Mapping;

namespace Ivh.Domain.Core.IpAccess.Mappings
{
    public class IpAccessMap : ClassMap<Entities.IpAccess>
    {
        public IpAccessMap()
        {
            this.Schema("dbo");
            this.Table("IPAccessList");
            this.Id(x => x.IpAddress, "IPAddress").GeneratedBy.Assigned();
            this.Map(x => x.IpAddressStartRange)
                .Nullable();
            this.Map(x => x.IpAddressEndRange)
                .Nullable();
            this.Map(x => x.CreatedDate);
            this.Map(x => x.Note);
        }
    }
}
﻿namespace Ivh.Domain.Core.IpAccess.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Utilities.Extensions;
    using Entities;
    using Interfaces;

    public class IpAccessService : DomainService, IIpAccessService
    {
        private readonly IIpAccessRepository _ipAccessRepository;

        public IpAccessService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            IIpAccessRepository ipAccessRepository) : base(serviceCommonService)
        {
            this._ipAccessRepository = ipAccessRepository;
        }

        private static volatile HashSet<string> _ipHash;

        private static readonly object IpLock = new object();
        private static List<Tuple<IPAddress, IPAddress>> _builtRanges;

        private HashSet<string> GetIpHash()
        {
            if (_ipHash == null)
            {
                lock (IpLock)
                {
                    if (_ipHash == null)
                    {
                        IQueryable<IpAccess> ipGoodList = this._ipAccessRepository.GetQueryable();
                        _ipHash = ipGoodList.Select(x => x.IpAddress.ToLowerInvariant()).ToHashSet();

                        var ranges = ipGoodList.Where(x => x.IpAddressEndRange != null && x.IpAddressStartRange != null).ToList();
                        List<Tuple<IPAddress, IPAddress>> builtRanges = new List<Tuple<IPAddress, IPAddress>>();
                        foreach (IpAccess ipAccess in ranges)
                        {
                            IPAddress startAddress, endAddress;
                            if (IPAddress.TryParse(ipAccess.IpAddressStartRange, out startAddress) &&
                                IPAddress.TryParse(ipAccess.IpAddressEndRange, out endAddress))
                            {
                                builtRanges.Add(new Tuple<IPAddress, IPAddress>(startAddress, endAddress));
                            }
                        }
                        _builtRanges = builtRanges;
                    }
                }
            }
            return _ipHash;
        }

        public bool IsIpAddressAllowed(string ipToCheck)
        {
            //Need to make sure this is always called first.
            var ipHash = this.GetIpHash();
            if (!ipHash.Any())
            {
                //If there arent any ip's on the white list, allow all.
                return true;
            }

            bool isIpAddressAllowed = ipHash.Contains(ipToCheck.ToLowerInvariant());
            if (isIpAddressAllowed) return true;

            IPAddress parsed;
            if (IPAddress.TryParse(ipToCheck, out parsed))
            {
                foreach (var range in _builtRanges)
                {
                    if (this.IsInRange(parsed, range.Item1, range.Item2))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool IsInRange(IPAddress addressToCheck, IPAddress lowerBoundry, IPAddress upperBoundry)
        {
            var lowerBytes = lowerBoundry.GetAddressBytes();
            var upperBytes = upperBoundry.GetAddressBytes();

            if (addressToCheck.AddressFamily != lowerBoundry.AddressFamily)
            {
                return false;
            }

            byte[] addressBytes = addressToCheck.GetAddressBytes();

            bool lowerBoundary = true, upperBoundary = true;

            for (int i = 0; i < lowerBytes.Length &&
                (lowerBoundary || upperBoundary); i++)
            {
                if ((lowerBoundary && addressBytes[i] < lowerBytes[i]) ||
                    (upperBoundary && addressBytes[i] > upperBytes[i]))
                {
                    return false;
                }

                lowerBoundary &= (addressBytes[i] == lowerBytes[i]);
                upperBoundary &= (addressBytes[i] == upperBytes[i]);
            }

            return true;
        }
    }
}
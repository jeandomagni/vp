﻿namespace Ivh.Domain.Core.IpAccess.Entities
{
    using System;

    public class IpAccess
    {
        public virtual string IpAddress { get; set; }

        public virtual string IpAddressStartRange { get; set; }
        public virtual string IpAddressEndRange { get; set; }
        public virtual DateTime CreatedDate { get; set; }
        public virtual string Note { get; set; }
    }
}
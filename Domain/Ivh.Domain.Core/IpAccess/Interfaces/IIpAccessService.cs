﻿namespace Ivh.Domain.Core.IpAccess.Interfaces
{
    using Common.Base.Interfaces;

    public interface IIpAccessService : IDomainService
    {
        bool IsIpAddressAllowed(string ipToCheck);
    }
}
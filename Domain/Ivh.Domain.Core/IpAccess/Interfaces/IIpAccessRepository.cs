﻿namespace Ivh.Domain.Core.IpAccess.Interfaces
{
    using Common.Base.Interfaces;

    public interface IIpAccessRepository : IRepository<Entities.IpAccess>
    {
    }
}
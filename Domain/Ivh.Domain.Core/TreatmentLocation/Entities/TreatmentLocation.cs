﻿namespace Ivh.Domain.Core.TreatmentLocation.Entities
{
    public class TreatmentLocation
    {
        public virtual int TreatmentLocationId { get; set; }
        public virtual string TreatmentLocationName { get; set; }
        public virtual bool IsActive { get; set; }
    }
}

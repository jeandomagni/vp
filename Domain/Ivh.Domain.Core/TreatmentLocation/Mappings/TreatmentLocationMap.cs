﻿namespace Ivh.Domain.Core.TreatmentLocation.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class TreatmentLocationMap : ClassMap<TreatmentLocation>
    {
        public TreatmentLocationMap()
        {
            this.Schema(Schemas.VisitPay.Dbo);
            this.Table(nameof(TreatmentLocation));
            this.Id(x => x.TreatmentLocationId);
            this.Map(x => x.TreatmentLocationName).Not.Nullable();
            this.Map(x => x.IsActive).Not.Nullable();
        }
    }
}

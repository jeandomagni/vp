﻿namespace Ivh.Domain.Core.TreatmentLocation.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface ITreatmentLocationService : IDomainService
    {
        TreatmentLocation GetTreatmentLocation(int treatmentLocationId);
        IList<TreatmentLocation> GetTreatmentLocations();
    }
}

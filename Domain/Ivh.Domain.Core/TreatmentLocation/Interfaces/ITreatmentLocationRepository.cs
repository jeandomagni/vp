﻿namespace Ivh.Domain.Core.TreatmentLocation.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface ITreatmentLocationRepository : IRepository<TreatmentLocation>
    {
    }
}

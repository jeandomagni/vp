﻿namespace Ivh.Domain.Core.TreatmentLocation.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Entities;
    using Interfaces;

    public class TreatmentLocationService : DomainService, ITreatmentLocationService
    {
        private readonly ITreatmentLocationRepository _treatmentLocationRepository;

        public TreatmentLocationService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            ITreatmentLocationRepository treatmentLocationRepository) : base(serviceCommonService)
        {
            this._treatmentLocationRepository = treatmentLocationRepository;
        }

        public TreatmentLocation GetTreatmentLocation(int treatmentLocationId)
        {
            return this._treatmentLocationRepository.GetById(treatmentLocationId);
        }

        public IList<TreatmentLocation> GetTreatmentLocations()
        {
            return this._treatmentLocationRepository.GetQueryable()
                       .Where(x => x.IsActive)
                       .OrderBy(x => x.TreatmentLocationName)
                       .ToList();
        }
    }
}

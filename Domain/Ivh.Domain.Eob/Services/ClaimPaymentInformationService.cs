﻿namespace Ivh.Domain.Eob.Services
{
    using System;
    using System.Collections.Generic;
    using Base.Interfaces;
    using Base.Services;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Entities;
    using Interfaces;
    using NHibernate;

    public class ClaimPaymentInformationService : DomainService, IClaimPaymentInformationService
    {
        private readonly Lazy<IClaimPaymentInformationRepository> _claimPaymentInformationRepository;
        private readonly Lazy<IEobPayerFilterRepository> _eobPayerFilterRepository;
        private readonly Lazy<IEobPayerFilterEobExternalLinkRepository> _eobPayerFilterEobExternalLinkRepository;
        private readonly Lazy<IEobExternalLinkRepository> _eobExternalLinkRepository;
        private readonly Lazy<IEobDisplayCategoryRepository> _eobDisplayCategoryRepository;
        private readonly Lazy<IEobClaimAdjustmentReasonCodeRepository> _eobClaimAdjustmentReasonCodeRepository;
        private readonly Lazy<IEobClaimAdjustmentReasonCodeDisplayMapRepository> _eobClaimAdjustmentReasonCodeDisplayMapRepository;
        private readonly ISession _session;

        public ClaimPaymentInformationService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IClaimPaymentInformationRepository> claimPaymentInformationRepository,
            Lazy<IEobPayerFilterRepository> eobPayerFilterRepository,
            Lazy<IEobPayerFilterEobExternalLinkRepository> eobPayerFilterEobExternalLinkRepository,
            Lazy<IEobExternalLinkRepository> eobExternalLinkRepository,
            Lazy<IEobDisplayCategoryRepository> eobDisplayCategoryRepository,
            Lazy<IEobClaimAdjustmentReasonCodeRepository> eobClaimAdjustmentReasonCodeRepository,
            Lazy<IEobClaimAdjustmentReasonCodeDisplayMapRepository> eobClaimAdjustmentReasonCodeDisplayMapRepository,
            ISessionContext<VisitPay> sessionContext
            ) : base(serviceCommonService)
        {
            this._claimPaymentInformationRepository = claimPaymentInformationRepository;
            this._eobPayerFilterRepository = eobPayerFilterRepository;
            this._eobPayerFilterEobExternalLinkRepository = eobPayerFilterEobExternalLinkRepository;
            this._eobExternalLinkRepository = eobExternalLinkRepository;
            this._eobDisplayCategoryRepository = eobDisplayCategoryRepository;
            this._eobClaimAdjustmentReasonCodeRepository = eobClaimAdjustmentReasonCodeRepository;
            this._eobClaimAdjustmentReasonCodeDisplayMapRepository = eobClaimAdjustmentReasonCodeDisplayMapRepository;
            this._session = sessionContext.Session;
        }

        public IList<Eob835Result> Get835Files(int billingSystemId, string visitSourceSystemKey)
        {
            return this._claimPaymentInformationRepository.Value.Get835Remits(billingSystemId, visitSourceSystemKey);
        }

        public bool Has835Remits(int billingSystemId, string visitSourceSystemKey)
        {
            return this._claimPaymentInformationRepository.Value.Has835Remits(billingSystemId, visitSourceSystemKey);
        }

        public IList<EobIndicatorResult> Get835RemitIndicators(IDictionary<string, int> visits)
        {
            return this._claimPaymentInformationRepository.Value.Get835RemitIndicators(visits);
        }

        public void SaveClaimPaymentInformationHeader(ClaimPaymentInformationHeader claimPaymentInformationHeader)
        {
            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                this._claimPaymentInformationRepository.Value.SaveClaimPaymentInformationHeader(claimPaymentInformationHeader);
                uow.Commit();
            }
        }

        #region Data Sync

        public void SaveEobPayerFilter(EobPayerFilter eobPayerFilter)
        {
            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                this._eobPayerFilterRepository.Value.SaveEobPayerFilter(eobPayerFilter);
                uow.Commit();
            }
        }

        public void SaveEobPayerFilterExternalLink(EobPayerFilterEobExternalLink eobPayerFilterExternalLink)
        {
            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                this._eobPayerFilterEobExternalLinkRepository.Value.SaveEobPayerFilterExternalLink(eobPayerFilterExternalLink);
                uow.Commit();
            }
        }

        public void SaveEobExternalLink(EobExternalLink eobExternalLink)
        {
            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                this._eobExternalLinkRepository.Value.SaveEobExternalLink(eobExternalLink);
                uow.Commit();
            }
        }

        public void SaveEobDisplayCategory(EobDisplayCategory eobDisplayCategory)
        {
            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                this._eobDisplayCategoryRepository.Value.SaveEobDisplayCategory(eobDisplayCategory);
                uow.Commit();
            }
        }

        public void SaveEobClaimAdjustmentReasonCode(EobClaimAdjustmentReasonCode eobClaimAdjustmentReasonCode)
        {
            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                this._eobClaimAdjustmentReasonCodeRepository.Value.SaveEobClaimAdjustmentReasonCode(eobClaimAdjustmentReasonCode);
                uow.Commit();
            }
        }

        public void SaveEobClaimAdjustmentReasonCodeDisplayMap(EobClaimAdjustmentReasonCodeDisplayMap eobClaimAdjustmentReasonCodeDisplayMap)
        {
            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                this._eobClaimAdjustmentReasonCodeDisplayMapRepository.Value.SaveEobClaimAdjustmentReasonCodeDisplayMap(eobClaimAdjustmentReasonCodeDisplayMap);
                uow.Commit();
            }
        }

        public string GetUiDisplay(string claimAdjustmentReasonCode, string uniquePayerValue, string claimStatusCode, string claimAdjustmentGroupCode)
        {
            int? eobDisplayCategoryId = this._eobClaimAdjustmentReasonCodeDisplayMapRepository.Value.GetEobDisplayCategoryId(claimAdjustmentReasonCode, uniquePayerValue, claimStatusCode, claimAdjustmentGroupCode);
            if (eobDisplayCategoryId == null)
            {
                this.LoggingService.Value.Warn(()=>$"EOB CARC code {claimAdjustmentReasonCode} does not have a mapped UI display value");
                return null;
            }
            else
            {
                EobDisplayCategory eobDisplayCategory = this._eobDisplayCategoryRepository.Value.GetById(eobDisplayCategoryId ?? 0);
                return eobDisplayCategory.DisplayCategoryName;
            }
        }

        #endregion

    }
}

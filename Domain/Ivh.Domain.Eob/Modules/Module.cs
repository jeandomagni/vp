﻿namespace Ivh.Domain.Eob.Modules
{
    using Autofac;
    using Interfaces;
    using Services;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ClaimPaymentInformationService>().As<IClaimPaymentInformationService>();
        }
    }
}

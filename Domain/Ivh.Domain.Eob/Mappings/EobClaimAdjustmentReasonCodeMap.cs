﻿namespace Ivh.Domain.Eob.Mappings
{
    using FluentNHibernate.Mapping;

    public class EobClaimAdjustmentReasonCodeMap : ClassMap<Entities.EobClaimAdjustmentReasonCode>
    {
        public EobClaimAdjustmentReasonCodeMap()
        {
            this.Schema("eob");
            this.Table("EobClaimAdjustmentReasonCodes");
            //Use the value that is already specified in the property
            this.Id(x => x.EobClaimAdjustmentReasonCodeId).Not.Nullable().GeneratedBy.Assigned();
            this.Map(x => x.ClaimAdjustmentReasonCode).Not.Nullable();
            this.Map(x => x.Description).Not.Nullable();
            this.Map(x => x.StartDate).Not.Nullable();
            this.Map(x => x.EndDate).Nullable();
        }
    }
}

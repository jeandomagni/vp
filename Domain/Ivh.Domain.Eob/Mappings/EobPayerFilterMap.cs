﻿namespace Ivh.Domain.Eob.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class EobPayerFilterMap : ClassMap<EobPayerFilter>
    {
        public EobPayerFilterMap()
        {
            this.Schema("eob");
            this.Table("EobPayerFilter");
            //Use the value that is already specified in the property
            this.Id(x => x.EobPayerFilterId).Not.Nullable().GeneratedBy.Assigned();
            this.Map(x => x.UniquePayerValue).Not.Nullable();
            this.Map(x => x.IsActive).Not.Nullable();

            this.HasMany(x => x.EobPayerFilterEobExternalLinks)
                .KeyColumn("EobPayerFilterId")
                .Cascade.None()
                .NotFound.Ignore()
                .ReadOnly();
        }
    }
}

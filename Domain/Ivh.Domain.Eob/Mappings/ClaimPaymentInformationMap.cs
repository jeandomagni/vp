﻿namespace Ivh.Domain.Eob.Mappings
{
    using System;
    using Entities;
    using FluentNHibernate.Mapping;

    public class ClaimPaymentInformationMap : ClassMap<ClaimPaymentInformationHeader>
    {
        public ClaimPaymentInformationMap()
        {
            this.Schema("eob");
            this.Table("ClaimPaymentInformation");
            this.Id(x => x.ClaimPaymentInformationId).Not.Nullable().GeneratedBy.Identity();
            this.Map(x => x.VisitSourceSystemKey).Nullable();
            this.Map(x => x.BillingSystemId).Nullable();
            this.Map(x => x.InterchangeSenderId).Not.Nullable();
            this.Map(x => x.InterchangeReceiverId).Not.Nullable();
            this.Map(x => x.InterchangeDateTime).Not.Nullable();
            this.Map(x => x.InterchangeControlNumber).Not.Nullable();
            this.Map(x => x.GroupControlNumber).Not.Nullable();
            this.Map(x => x.TransactionDate).Not.Nullable();
            this.Map(x => x.TransactionSetControlNumber).Not.Nullable();
            this.Map(x => x.TransactionSetLineNumber).Not.Nullable();
            this.Map(x => x.EobPayerFilterId).Not.Nullable();
            this.Map(x => x.PayerName).Not.Nullable();
            this.Map(x => x.ClaimNumber).Not.Nullable();
            this.Map(x => x.ClaimPaymentInformationJson).Not.Nullable().Length(Int32.MaxValue);
            this.Map(x => x.InsertDate).Generated.Always();

            this.References(x => x.EobPayerFilter).Column("EobPayerFilterId")
            .Nullable().NotFound.Ignore()
            .ReadOnly()
            .Cascade.All()
            .Not.LazyLoad()
            .Fetch.Join();
        }
    }
}

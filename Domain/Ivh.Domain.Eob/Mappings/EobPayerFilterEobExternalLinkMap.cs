﻿namespace Ivh.Domain.Eob.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class EobPayerFilterEobExternalLinkMap : ClassMap<EobPayerFilterEobExternalLink>
    {
        public EobPayerFilterEobExternalLinkMap()
        {
            this.Schema("eob");
            this.Table("EobPayerFilterEobExternalLink");
            //Use the value that is already specified in the property
            this.Id(x => x.EobPayerFilterEobExternalLinkId).Not.Nullable().GeneratedBy.Assigned();
            this.Map(x => x.EobPayerFilterId).Not.Nullable();
            this.Map(x => x.EobExternalLinkId).Not.Nullable();
            this.Map(x => x.InsurancePlanBillingSystemId).Nullable();
            this.Map(x => x.InsurancePlanSourceSystemKey).Nullable();
            
            this.References(x => x.EobPayerFilter)
                .Column("EobPayerFilterId")
                .Fetch.Join()
                .Cascade.None()
                .Not.Update()
                .Not.Insert()
                .Not.LazyLoad();

            this.References(x => x.EobExternalLink)
                .Column("EobExternalLinkId")
                .Fetch.Join()
                .Cascade.None()
                .Not.Update()
                .Not.Insert()
                .Not.LazyLoad();
        
        }
    }
}

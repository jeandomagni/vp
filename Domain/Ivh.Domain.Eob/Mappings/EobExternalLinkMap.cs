﻿namespace Ivh.Domain.Eob.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class EobExternalLinkMap : ClassMap<EobExternalLink>
    {
        public EobExternalLinkMap()
        {
            this.Schema("eob");
            this.Table("EobExternalLink");
            //Use the value that is already specified in the property
            this.Id(x => x.EobExternalLinkId).Not.Nullable().GeneratedBy.Assigned();
            this.Map(x => x.Url).Not.Nullable();
            this.Map(x => x.Text).Not.Nullable();
            this.Map(x => x.Icon).Nullable();
            this.Map(x => x.PersistDays).Nullable();
            this.Map(x => x.ClaimNumberLocation).Nullable();
        }
    }
}

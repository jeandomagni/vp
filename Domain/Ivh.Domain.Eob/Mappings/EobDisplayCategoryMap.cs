﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.Eob.Mappings
{
    using FluentNHibernate.Mapping;

    public class EobDisplayCategoryMap : ClassMap<Entities.EobDisplayCategory>
    {
        public EobDisplayCategoryMap()
        {
            this.Schema("eob");
            this.Table("EobDisplayCategory");
            //Use the value that is already specified in the property
            this.Id(x => x.EobDisplayCategoryId).Not.Nullable().GeneratedBy.Assigned();
            this.Map(x => x.DisplayCategoryName).Not.Nullable();            
        }
    }
}

﻿namespace Ivh.Domain.Eob.Entities
{
    public class VisitEobClaimAdjustment
    {
        public string ClaimAdjustmentReasonCode { get; set; }
        public decimal MonetaryAmount { get; set; }
        public string UIDisplay { get; set; }
    }
}
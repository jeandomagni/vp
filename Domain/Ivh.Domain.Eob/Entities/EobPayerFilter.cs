﻿namespace Ivh.Domain.Eob.Entities
{
    using System.Collections.Generic;

    public class EobPayerFilter
    {
        public virtual int EobPayerFilterId { get; set; }
        public virtual string UniquePayerValue { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual IList<EobPayerFilterEobExternalLink> EobPayerFilterEobExternalLinks { get; protected set; }
    }
}

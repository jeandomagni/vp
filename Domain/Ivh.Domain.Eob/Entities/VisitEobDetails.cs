﻿namespace Ivh.Domain.Eob.Entities
{
    using System;
    using System.Collections.Generic;

    public class VisitEobDetails
    {
        public int HeaderNumber { get; set; }
        public string ClaimNumber { get; set; }
        public string PlanNumber { get; set; }
        public string VisitSourceSystemKey { get; set; }
        public int VisitBillingSystemId { get; set; }
        public string PayerName { get; set; }
        public string PayerLinkUrl { get; set; }
        public string PayerIcon { get; set; }
        public string ClaimStatusCode { get; set; }
        public decimal TotalClaimChargeAmount { get; set; }
        public decimal ClaimPaymentAmount { get; set; }
        public decimal PatientResponsibilityAmount { get; set; }
        public DateTime TransactionDate { get; set; }
        public IList<VisitEobClaimAdjustment> ClaimAdjustments { get; set; }
        public IList<VisitEobIndustryCodeIdentification> HealthCareRemarkCodes { get; set; }

    }
}
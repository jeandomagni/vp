﻿namespace Ivh.Domain.Eob.Entities
{
    public class EobPayerFilterEobExternalLink
    {
        public virtual int EobPayerFilterEobExternalLinkId { get; set; }
        public virtual int EobPayerFilterId { get; set; }
        public virtual int EobExternalLinkId { get; set; }
        public virtual EobPayerFilter EobPayerFilter { get; set; }
        public virtual EobExternalLink EobExternalLink { get; set; }
        public virtual int? InsurancePlanBillingSystemId { get; set; }
        public virtual string InsurancePlanSourceSystemKey { get; set; }
    }
}

﻿namespace Ivh.Domain.Eob.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using Application.Core.Common.Models.Eob;
    using Common.Base.Utilities.Extensions;

    public class Eob835Result
    {
        public virtual int TransactionSetHeaderTrailerId { get; set; }
        public virtual int HeaderNumberId { get; set; }
        public virtual DateTime TransactionDate { get; set; }
        public virtual int HeaderNumber { get; set; }
        public virtual int ClaimPaymentInformationId { get; set; }
        public virtual int EobPayerFilterId { get; set; }
        public virtual IList<EobPayerFilterEobExternalLink> EobPayerFilterEobExternalLinks { get; set; }
        public virtual IEnumerable<ClaimPaymentInformation> ClaimPaymentInformations { get; set; }

        public override bool Equals(object obj)
        {
            Eob835Result other = obj as Eob835Result;

            if (ReferenceEquals(null, other))
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return
                //this.TransactionSetHeaderTrailerId == other.TransactionSetHeaderTrailerId && // ID cannot be used when checking for value equality
                //this.HeaderNumberId == other.HeaderNumberId && // ID cannot be used when checking for value equality
                this.TransactionDate == other.TransactionDate &&
                //this.HeaderNumber == other.HeaderNumber && // ID cannot be used when checking for value equality
                //this.ClaimPaymentInformationId == other.ClaimPaymentInformationId && // ID cannot be used when checking for value equality
                this.EobPayerFilterId == other.EobPayerFilterId &&

                // this.EobPayerFilterEobExternalLinks.SequencesAreEqual(other.EobPayerFilterEobExternalLinks) && // Not relevant when checking for value equality
                this.ClaimPaymentInformations.SequencesAreEqual(other.ClaimPaymentInformations)
                ;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = this.GetType().GetHashCode();

                //hash = (hash * 31) ^ this.TransactionSetHeaderTrailerId.GetHashCode(); // ID cannot be used when checking for value equality
                //hash = (hash * 31) ^ this.HeaderNumberId.GetHashCode(); // ID cannot be used when checking for value equality
                hash = (hash * 31) ^ this.TransactionDate.GetHashCode();
                //hash = (hash * 31) ^ this.HeaderNumber.GetHashCode(); // ID cannot be used when checking for value equality
                //hash = (hash * 31) ^ this.ClaimPaymentInformationId.GetHashCode(); // ID cannot be used when checking for value equality
                hash = (hash * 31) ^ this.EobPayerFilterId.GetHashCode();

                hash = (hash * 31) ^ this.EobPayerFilterEobExternalLinks.GetSequenceHashCode();
                hash = (hash * 31) ^ this.ClaimPaymentInformations.GetSequenceHashCode();

                #if DEBUG
                    Debug.WriteLine($"{ClaimPaymentInformations.First().Clp07ReferenceIdentifier} = {hash}");
                #endif

                return hash;
            }
        }
    }
}

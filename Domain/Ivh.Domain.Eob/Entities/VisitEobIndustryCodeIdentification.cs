﻿namespace Ivh.Domain.Eob.Entities
{
    public class VisitEobIndustryCodeIdentification
    {
        public string IndustryCode { get; set; }
        public string Description { get; set; }
    }
}
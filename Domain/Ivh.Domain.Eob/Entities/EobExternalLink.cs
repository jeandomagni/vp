﻿namespace Ivh.Domain.Eob.Entities
{
    public class EobExternalLink
    {
        public virtual int EobExternalLinkId { get; set; }
        public virtual string Url { get; set; }
        public virtual string Text { get; set; }
        public virtual string Icon { get; set; }
        public virtual int PersistDays { get; set; }
        public virtual string ClaimNumberLocation { get; set; }
    }
}

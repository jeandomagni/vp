﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.Eob.Entities
{
    public class EobDisplayCategory
    {
        public virtual int EobDisplayCategoryId { get; set; }
        public virtual string DisplayCategoryName { get; set; }
    }
}

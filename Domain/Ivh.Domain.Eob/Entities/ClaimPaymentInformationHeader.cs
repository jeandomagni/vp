﻿namespace Ivh.Domain.Eob.Entities
{
    using Ivh.Application.Core.Common.Models.Eob;
    using System;
    using Newtonsoft.Json;

    public class ClaimPaymentInformationHeader
    {
        public virtual int ClaimPaymentInformationId { get; set; }
        public virtual string VisitSourceSystemKey { get; set; }
        public virtual int BillingSystemId { get; set; }
        public virtual string InterchangeSenderId { get; set; }
        public virtual string InterchangeReceiverId { get; set; }
        public virtual DateTime InterchangeDateTime { get; set; }
        public virtual int InterchangeControlNumber { get; set; }
        public virtual int GroupControlNumber { get; set; }
        public virtual DateTime? TransactionDate { get; set; }
        public virtual string TransactionSetControlNumber { get; set; }
        public virtual int TransactionSetLineNumber { get; set; }
        public virtual int EobPayerFilterId { get; set; }
        public virtual string PayerName { get; set; }
        public virtual string ClaimNumber { get; set; }
        public virtual string ClaimPaymentInformationJson { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual EobPayerFilter EobPayerFilter { get; set; }

        private ClaimPaymentInformation _claimPaymentInformation;
        public virtual ClaimPaymentInformation ClaimPaymentInformation
        {
            get
            {
                if (this._claimPaymentInformation == null)
                {
                    this._claimPaymentInformation = JsonConvert.DeserializeObject<ClaimPaymentInformation>(this.ClaimPaymentInformationJson);
                }

                return this._claimPaymentInformation;
            }
        }

    }
}

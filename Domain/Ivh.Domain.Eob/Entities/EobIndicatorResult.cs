﻿namespace Ivh.Domain.Eob.Entities
{
    using System;
    using System.Collections.Generic;

    public class EobIndicatorResult
    {
        public virtual int BillingSystemId { get; set; }
        public virtual string VisitSourceSystemKey { get; set; }
        public virtual bool HasEob { get; set; }
        public IList<EobPayerFilterEobExternalLink> EobPayerFilterEobExternalLinks { get; set; }
        public virtual string LogoUrl { get; set; }
        public virtual string PayerName { get; set; }
        public virtual DateTime? Bpr16Date { get; set; }
        public virtual int Lx01AssignedNumber { get; set; }
    }
}

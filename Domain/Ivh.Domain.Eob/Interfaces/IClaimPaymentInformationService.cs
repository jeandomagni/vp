﻿namespace Ivh.Domain.Eob.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IClaimPaymentInformationService : IDomainService
    {
        IList<Eob835Result> Get835Files(int billingSystemId, string visitSourceSystemKey);
        bool Has835Remits(int billingSystemId, string visitSourceSystemKey);
        IList<EobIndicatorResult> Get835RemitIndicators(IDictionary<string, int> visits);
        void SaveClaimPaymentInformationHeader(ClaimPaymentInformationHeader claimPaymentInformationHeader);
        void SaveEobPayerFilter(EobPayerFilter eobPayerFilter);
        void SaveEobPayerFilterExternalLink(EobPayerFilterEobExternalLink eobPayerFilterExternalLink);
        void SaveEobExternalLink(EobExternalLink eobExternalLink);
        void SaveEobDisplayCategory(EobDisplayCategory eobDisplayCategory);
        void SaveEobClaimAdjustmentReasonCode(EobClaimAdjustmentReasonCode eobClaimAdjustmentReasonCode);
        void SaveEobClaimAdjustmentReasonCodeDisplayMap(EobClaimAdjustmentReasonCodeDisplayMap eobClaimAdjustmentReasonCodeDisplayMap);
        string GetUiDisplay(string claimAdjustmentReasonCode, string uniquePayerValue, string claimStatusCode, string claimAdjustmentGroupCode);
    }
}
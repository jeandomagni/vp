﻿namespace Ivh.Domain.Eob.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IEobDisplayCategoryRepository : IRepository<EobDisplayCategory>
    {
        void SaveEobDisplayCategory(EobDisplayCategory eobDisplayCategory);
    }
}

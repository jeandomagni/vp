﻿namespace Ivh.Domain.Eob.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IEobExternalLinkRepository : IRepository<EobExternalLink>
    {
        void SaveEobExternalLink(EobExternalLink eobExternalLink);
    }
}

﻿namespace Ivh.Domain.Eob.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IEobPayerFilterRepository : IRepository<EobPayerFilter>
    {
        void SaveEobPayerFilter(EobPayerFilter eobPayerFilter);
    }
}

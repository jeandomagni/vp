﻿namespace Ivh.Domain.Eob.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IEobClaimAdjustmentReasonCodeDisplayMapRepository : IRepository<EobClaimAdjustmentReasonCodeDisplayMap>
    {
        void SaveEobClaimAdjustmentReasonCodeDisplayMap(EobClaimAdjustmentReasonCodeDisplayMap eobClaimAdjustmentReasonCodeDisplayMap);
        int? GetEobDisplayCategoryId(string claimAdjustmentReasonCode, string uniquePayerValue, string claimStatusCode, string claimAdjustmentGroupCode);
    }
}

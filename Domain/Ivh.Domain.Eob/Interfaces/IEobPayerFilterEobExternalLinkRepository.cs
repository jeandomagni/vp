﻿namespace Ivh.Domain.Eob.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IEobPayerFilterEobExternalLinkRepository : IRepository<EobPayerFilterEobExternalLink>
    {
        void SaveEobPayerFilterExternalLink(EobPayerFilterEobExternalLink eobPayerFilterExternalLink);
    }
}

﻿namespace Ivh.Domain.Eob.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IClaimPaymentInformationRepository : IRepository<ClaimPaymentInformationHeader>
    {
        IList<Eob835Result> Get835Remits(int billingSystemId, string visitSourceSystemKey);
        bool Has835Remits(int billingSystemId, string visitSourceSystemKey);
        IList<EobIndicatorResult> Get835RemitIndicators(IDictionary<string, int> visits);
        void SaveClaimPaymentInformationHeader(ClaimPaymentInformationHeader claimPaymentInformationHeader);
    }
}

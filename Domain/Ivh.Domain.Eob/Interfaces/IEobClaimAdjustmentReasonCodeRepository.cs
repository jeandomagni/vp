﻿namespace Ivh.Domain.Eob.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IEobClaimAdjustmentReasonCodeRepository : IRepository<EobClaimAdjustmentReasonCode>
    {
        void SaveEobClaimAdjustmentReasonCode(EobClaimAdjustmentReasonCode eobClaimAdjustmentReasonCode);
    }
}

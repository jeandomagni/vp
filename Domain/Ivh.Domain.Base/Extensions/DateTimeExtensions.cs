﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.Base.Extensions
{
    using System;
    using Autofac;
    using Ivh.Common.Base.Utilities.Helpers;
    using Common.DependencyInjection;

    public static class DateTimeExtensions
    {
        private static TimeZoneHelper _timeZoneHelper;

        private static TimeZoneHelper TimeZoneHelper()
        {
            if (_timeZoneHelper == null)
            {
                _timeZoneHelper = IvinciContainer.Instance.Container().Resolve<TimeZoneHelper>();
            }

            return _timeZoneHelper;
        }

        public static DateTime ToClientDateTime(this DateTime databaseDateTime)
        {
            DateTime? utcValue = GetUtcDateTime(databaseDateTime);
            return TimeZoneHelper().Client.ToLocalDateTime(utcValue.Value);
        }

        public static DateTime? ToClientDateTime(this DateTime? databaseDateTime)
        {
            DateTime? clientDateTime = databaseDateTime.HasValue ? ToClientDateTime(databaseDateTime.Value) : (DateTime?)null;
            return clientDateTime;
        }

        private static DateTime? GetUtcDateTime(DateTime? value)
        {
            if (value.HasValue && value.Value.Kind == DateTimeKind.Local)
            {
                return value.Value.ToUniversalTime();
            }

            return value;
        }

    }
}

﻿namespace Ivh.Domain.Base.Services
{
    using System;
    using Common.Base.Utilities.Helpers;
    using Common.Cache;
    using Common.ServiceBus.Interfaces;
    using Interfaces;
    using Logging.Interfaces;
    using Settings.Entities;
    using Settings.Interfaces;

    public abstract class DomainService : IDomainService
    {
        private readonly Lazy<IDomainServiceCommonService> _serviceCommonService;

        protected DomainService(Lazy<IDomainServiceCommonService> serviceCommonService)
        {
            this._serviceCommonService = serviceCommonService;
        }

        public Lazy<IApplicationSettingsService> ApplicationSettingsService => this._serviceCommonService.Value.ApplicationSettingsService;
        public Lazy<IFeatureService> FeatureService => this._serviceCommonService.Value.FeatureService;
        public Lazy<Client> Client => new Lazy<Client>(() => this.ClientService.Value.GetClient());
        public Lazy<IClientService> ClientService => this._serviceCommonService.Value.ClientService;
        public Lazy<ILoggingService> LoggingService => this._serviceCommonService.Value.LoggingService;
        public Lazy<IBus> Bus => this._serviceCommonService.Value.Bus;
        public Lazy<IDistributedCache> Cache => this._serviceCommonService.Value.Cache;
        public DateTime SystemNow => this._serviceCommonService.Value.TimeZoneHelper.Value.Server.Now;
        public DateTime ClientNow => this._serviceCommonService.Value.TimeZoneHelper.Value.Client.Now;
        public DateTime OperationsNow => this._serviceCommonService.Value.TimeZoneHelper.Value.Operations.Now;
        public Lazy<TimeZoneHelper> TimeZoneHelper => this._serviceCommonService.Value.TimeZoneHelper;
        public Lazy<IInstanceCache> InstanceCache => this._serviceCommonService.Value.InstanceCache;

    }
}

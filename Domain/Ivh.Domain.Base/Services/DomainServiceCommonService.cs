﻿namespace Ivh.Domain.Base.Services
{
    using System;
    using Common.Base.Utilities.Helpers;
    using Common.Cache;
    using Common.ServiceBus.Interfaces;
    using Interfaces;
    using Logging.Interfaces;
    using Settings.Entities;
    using Settings.Interfaces;

    public class DomainServiceCommonService : IDomainServiceCommonService
    {
        public DomainServiceCommonService(
            Lazy<IApplicationSettingsService> applicationSettingsService,
            Lazy<IFeatureService> featureService,
            Lazy<IClientService> clientService,
            Lazy<ILoggingService> loggingService,
            Lazy<IBus> bus,
            Lazy<IDistributedCache> cache,
            Lazy<TimeZoneHelper> timeZoneHelper,
            Lazy<IInstanceCache> instanceCache)
        {
            this.TimeZoneHelper = timeZoneHelper;
            this.InstanceCache = instanceCache;
            this.FeatureService = featureService;
            this.ClientService = clientService;
            this.Cache = cache;
            this.ApplicationSettingsService = applicationSettingsService;
            this.LoggingService = loggingService;
            this.Bus = bus;
        }

        public Lazy<IApplicationSettingsService> ApplicationSettingsService { get; }
        public Lazy<IFeatureService> FeatureService { get; }
        public Lazy<Client> Client => new Lazy<Client>(() => this.ClientService.Value.GetClient());
        public Lazy<IClientService> ClientService { get; }
        public Lazy<ILoggingService> LoggingService { get; }
        public Lazy<IBus> Bus { get; }
        public Lazy<IDistributedCache> Cache { get; }
        public Lazy<TimeZoneHelper> TimeZoneHelper { get; }
        public Lazy<IInstanceCache> InstanceCache { get; }
    }
}

﻿namespace Ivh.Domain.Base.Interfaces
{
    using System;
    using Common.Cache;
    using Common.ServiceBus.Interfaces;
    using Logging.Interfaces;
    using Settings.Entities;
    using Settings.Interfaces;

    public interface IDomainService :  Ivh.Common.Base.Interfaces.IDomainService
    {
        Lazy<IApplicationSettingsService> ApplicationSettingsService { get; }
        Lazy<IFeatureService> FeatureService { get; }
        Lazy<Client> Client { get; }
        Lazy<IClientService> ClientService { get; }
        Lazy<ILoggingService> LoggingService { get; }
        Lazy<IBus> Bus { get; }
        Lazy<IDistributedCache> Cache { get; }
        DateTime SystemNow { get; }
        DateTime ClientNow { get; }
        DateTime OperationsNow { get; }
    }
}

﻿namespace Ivh.Domain.Base.Interfaces
{
    using System;
    using Common.Base.Utilities.Helpers;
    using Common.Cache;
    using Common.ServiceBus.Interfaces;
    using Logging.Interfaces;
    using Settings.Entities;
    using Settings.Interfaces;

    public interface IDomainServiceCommonService
    {
        Lazy<IApplicationSettingsService> ApplicationSettingsService { get; }
        Lazy<IFeatureService> FeatureService { get; }
        Lazy<Client> Client { get; }
        Lazy<IClientService> ClientService { get; }
        Lazy<ILoggingService> LoggingService { get; }
        Lazy<IBus> Bus { get; }
        Lazy<IDistributedCache> Cache { get; }
        Lazy<TimeZoneHelper> TimeZoneHelper { get; }
        Lazy<IInstanceCache> InstanceCache { get; }
    }
}

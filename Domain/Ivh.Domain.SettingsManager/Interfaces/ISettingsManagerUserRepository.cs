﻿using System.Collections.Generic;
using Ivh.Common.Data.Core.Interfaces;
using Ivh.Domain.SettingsManager.Entities;
using Ivh.Domain.SettingsManager.Enums;

namespace Ivh.Domain.SettingsManager.Interfaces
{
    public interface ISettingsManagerUserRepository : IRepository<SettingsManagerUser>
    {
        SettingsManagerUser GetSettingsManagerUser(int id);
        SettingsManagerUser GetSettingsManagerUserFromUserName(string userName);
        int AddSettingsManagerUser(string userName);
    }
}
﻿using System.Collections.Generic;
using Ivh.Common.Data.Core.Interfaces;
using Ivh.Domain.SettingsManager.Entities;

namespace Ivh.Domain.SettingsManager.Interfaces
{
    public interface IReleaseRepository : IRepository<Release>
    {
        IEnumerable<Release> GetReleases();
        int AddRelease(int major, int minor, int patch, int build, string insertedBy);
        Release GetRelease(int major, int minor, int patch, int build);
        Release GetLatestReleaseForVersion(int major, int minor, int patch, int build);
        string GetLatestReleaseLabel();
    }
}
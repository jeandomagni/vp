﻿namespace Ivh.Domain.SettingsManager.Interfaces
{
    using Ivh.Common.Data.Core.Interfaces;
    using Ivh.Domain.SettingsManager.Common.Models;
    using Ivh.Domain.SettingsManager.Entities;

    public interface ISettingRepository : IRepository<Setting>
    {
       
    }
}
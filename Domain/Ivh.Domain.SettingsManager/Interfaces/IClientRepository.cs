﻿using System.Collections.Generic;
using Ivh.Common.Data.Core.Interfaces;
using Ivh.Domain.SettingsManager.Entities;

namespace Ivh.Domain.SettingsManager.Interfaces
{
    public interface IClientRepository : IRepository<Client>
    {
        IEnumerable<Client> GetClients();
    }
}
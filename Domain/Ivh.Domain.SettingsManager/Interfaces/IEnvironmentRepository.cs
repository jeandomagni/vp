﻿using System.Collections.Generic;
using Ivh.Common.Data.Core.Interfaces;
using Ivh.Domain.SettingsManager.Entities;

namespace Ivh.Domain.SettingsManager.Interfaces
{
    public interface IEnvironmentRepository : IRepository<Environment>
    {
        IEnumerable<Environment> GetEnvironments();
    }
}
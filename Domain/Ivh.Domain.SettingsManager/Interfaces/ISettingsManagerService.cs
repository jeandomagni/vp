﻿using System.Collections.Generic;
using System.Linq;
using Ivh.Domain.SettingsManager.Common.Models;
using Ivh.Domain.SettingsManager.Entities;
using Ivh.Domain.SettingsManager.Enums;

namespace Ivh.Domain.SettingsManager.Interfaces
{
    public interface ISettingsManagerService
    {
        IQueryable<SettingComposite> GetSettings(int clientId, int environmentId, int releaseId);
        IEnumerable<SettingComposite> GetSettingsFromServer(int clientId, int environmentId);
        IQueryable<ClientSetting> GetClientSettingsById(int clientId, int environmentId, int releaseId);
        IQueryable<ClientSetting> GetClientSettings(string client, string environment, string release);
        IEnumerable<KeyValuePair<int, string>> GetReleases();
        IEnumerable<KeyValuePair<int, string>> GetClients();
        IEnumerable<KeyValuePair<int, string>> GetEnvironments();
        IEnumerable<KeyValuePair<int, string>> GetSettingTypes();
        IEnumerable<KeyValuePair<int, string>> GetSettingNames();
        void AddSetting(Setting setting);
        int AddRelease(int major, int minor, int patch, int build, string insertedBy);
        SettingType GetSettingType(SettingTypeEnum settingTypeEnum);
        IEnumerable<KeyValuePair<string, string>> GetSettingDataTypes();
        void AddSetting(SettingInput model);
        void AddSettingOverride(SettingOverrideInput model);
        BootGridResponse<SettingComposite> GetSettingsMaxBootgrid(BootGridRequest model);
        string GetSettingsHashCode(string client, string environment, string release);
        string GetSettingsHashCode(IQueryable<ClientSetting> clientSettings);
        string GetLatestApprovedSettingsHashCode(int clientId, int environmentId, int releaseId);
        void AddClientReleaseEnvironmentApproval(DeploymentContextApproval deploymentContextApproval);
        string GetLatestReleaseLabel();
        DeploymentContext GetDeploymentContext(string client, string environment, string release);
    }
}

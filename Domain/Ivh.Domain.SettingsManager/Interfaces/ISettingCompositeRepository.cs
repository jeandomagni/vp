﻿using System.Collections.Generic;
using System.Linq;
using Ivh.Common.Data.Core.Interfaces;
using Ivh.Domain.SettingsManager.Common.Models;

namespace Ivh.Domain.SettingsManager.Interfaces
{
    public interface ISettingCompositeRepository : IRepository<SettingComposite>
    {
        IQueryable<SettingComposite> GetSettings(int clientId, int environmentId, int releaseId);
    }
}
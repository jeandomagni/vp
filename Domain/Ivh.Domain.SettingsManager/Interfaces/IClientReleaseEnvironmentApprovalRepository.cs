﻿namespace Ivh.Domain.SettingsManager.Interfaces
{
    using Entities;
    using Ivh.Common.Data.Core.Interfaces;

    public interface IClientReleaseEnvironmentApprovalRepository : IRepository<ClientReleaseEnvironmentApproval>
    {
    }
}

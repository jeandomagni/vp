﻿namespace Ivh.Domain.SettingsManager.Constants
{
    class UrlPath
    {
        public static string ClientDataApi = "/api/settings/getsettings";
        public static string ClientDataApiManaged = "/api/settings/getmanagedsettings";
    }
}

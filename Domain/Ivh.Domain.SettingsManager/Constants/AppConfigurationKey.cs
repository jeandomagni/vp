﻿namespace Ivh.Domain.SettingsManager.Constants
{   
    public class AppConfigurationKey
    {
        public static string SectionName = "AppConfiguration";
        public static string ApplicationName = "ApplicationName";
        public static string ClientDataApiAppId = "ClientDataApi.AppId";
        public static string ClientDataApiAppKey = "ClientDataApi.AppKey";
    }
}

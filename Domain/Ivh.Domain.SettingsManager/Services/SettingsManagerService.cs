﻿namespace Ivh.Domain.SettingsManager.Services
{
    using System;
    using System.ComponentModel;
    using Constants;
    using Ivh.Common.Base.Core.Utilities.Helpers;
    using Ivh.Common.Data.Core;
    using NHibernate;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Net.Http;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading.Tasks;
    using Ivh.Common.Api.Core.Client;
    using Ivh.Common.Web.Core.LoggingHelper;
    using Ivh.Domain.SettingsManager.Common.Models;
    using Ivh.Domain.SettingsManager.Entities;
    using Ivh.Domain.SettingsManager.Enums;
    using Ivh.Domain.SettingsManager.Interfaces;
    using Microsoft.Extensions.Configuration;
    using Newtonsoft.Json;
    using Environment = Entities.Environment;
    using Ivh.Common.Base.Core.Utilities.Extensions;

    public class SettingsManagerService : ISettingsManagerService
    {
        private readonly IConfiguration _configuration;
        private readonly ISession _session;
        private readonly IClientRepository _clientRepository;
        private readonly IEnvironmentRepository _environmentRepository;
        private readonly IReleaseRepository _releaseRepository;
        private readonly ISettingCompositeRepository _settingCompositeRepository;
        private readonly ISettingRepository _settingRepository;
        private readonly ISettingTypeRepository _settingTypeRepository;
        private readonly ISettingReleaseRepository _settingReleaseRepository;
        private readonly ISettingClientReleaseRepository _settingClientReleaseRepository;
        private readonly ISettingClientReleaseEnvironmentRepository _settingClientReleaseEnvironmentRepository;
        private readonly ISettingEnvironmentReleaseRepository _settingEnvironmentReleaseRepository;
        private readonly ISettingsManagerUserRepository _settingsManagerUserRepository;
        private readonly IClientReleaseEnvironmentApprovalRepository _clientReleaseEnvironmentApprovalRepository;

        readonly List<string> _dataTypeList = new List<string>(new string[] { "byte", "sbyte", "int", "uint", "short", "ushort", "long", "ulong", "float", "double", "char", "bool", "object", "string", "decimal", "DateTime" });

        public SettingsManagerService(
            IConfiguration configuration,
            ISession session,
            IClientRepository clientRepository,
            IEnvironmentRepository environmentRepository,
            IReleaseRepository releaseRepository,
            ISettingCompositeRepository settingCompositeRepository,
            ISettingRepository settingRepository,
            ISettingTypeRepository settingTypeRepository,
            ISettingReleaseRepository settingReleaseRepository,
            ISettingClientReleaseRepository settingClientReleaseRepository,
            ISettingClientReleaseEnvironmentRepository settingClientReleaseEnvironmentRepository,
            ISettingEnvironmentReleaseRepository settingEnvironmentReleaseRepository,
            ISettingsManagerUserRepository settingsManagerUserRepository,
            IClientReleaseEnvironmentApprovalRepository clientReleaseEnvironmentApprovalRepository)
        {
            this._configuration = configuration;
            this._session = session;
            this._clientRepository = clientRepository;
            this._environmentRepository = environmentRepository;
            this._releaseRepository = releaseRepository;
            this._settingCompositeRepository = settingCompositeRepository;
            this._settingRepository = settingRepository;
            this._settingTypeRepository = settingTypeRepository;
            this._settingReleaseRepository = settingReleaseRepository;
            this._settingClientReleaseRepository = settingClientReleaseRepository;
            this._settingClientReleaseEnvironmentRepository = settingClientReleaseEnvironmentRepository;
            this._settingEnvironmentReleaseRepository = settingEnvironmentReleaseRepository;
            this._settingsManagerUserRepository = settingsManagerUserRepository;
            this._clientReleaseEnvironmentApprovalRepository = clientReleaseEnvironmentApprovalRepository;
        }

        #region Name To Id Resolution

        private int GetEnvironmentIdFromName(string name)
        {
            return this._environmentRepository.GetEnvironments().Single(x => x.UrlName.Equals(name, StringComparison.InvariantCultureIgnoreCase)).EnvironmentId;
        }

        private int GetReleaseIdFromName(string name)
        {
            int[] releaseVersion = name.Split('.').Select(int.Parse).ToArray();

            return this._releaseRepository.GetLatestReleaseForVersion(
                    major: releaseVersion[0],
                    minor: releaseVersion[1],
                    patch: releaseVersion[2],
                    build: releaseVersion[3])
                .ReleaseId;
        }

        private int GetClientIdFromName(string name)
        {
            return this._clientRepository.GetClients().Single(x => x.UrlName.Equals(name, StringComparison.InvariantCultureIgnoreCase)).ClientId;
        }

        public DeploymentContext GetDeploymentContext(string client, string environment, string release)
        {
            int clientId = this.GetClientIdFromName(client);
            int environmentId = this.GetEnvironmentIdFromName(environment);
            int releaseId = this.GetReleaseIdFromName(release);

            return new DeploymentContext()
            {
                ClientId = clientId,
                EnvironmentId = environmentId,
                ReleaseId = releaseId
            };
        }

        #endregion

        #region Get ClientSettings

        public IQueryable<ClientSetting> GetClientSettingsById(int clientId, int environmentId, int releaseId)
        {
            return this.GetSettings(clientId, environmentId, releaseId).Select(x => new ClientSetting()
            {
                ClientSettingKey = x.Name,
                ClientSettingName = x.Name,
                ClientSettingValue = x.Value,
                ClientSettingValueOverride = null,
                ClientSettingDescription = $"{x.SettingType} - {x.ValueSource} - {x.SinceRelease}",
                EditInUi = x.IsEditable,
                IsEncrypted = x.IsEncrypted,
                DataType = x.DataType,
                IsCmsVariable = false,
                ChangeEventId = 1
            });
        }

        public IQueryable<ClientSetting> GetClientSettings(string client, string environment, string release)
        {
            int clientId = this.GetClientIdFromName(client);
            int environmentId = this.GetEnvironmentIdFromName(environment);
            int releaseId = this.GetReleaseIdFromName(release);

            return this.GetClientSettingsById(clientId, environmentId, releaseId);
        }

        #endregion

        #region Get SettingsComposite

        public IQueryable<SettingComposite> GetSettings(int clientId, int environmentId, int releaseId)
        {
            return this._settingCompositeRepository.GetSettings(clientId, environmentId, releaseId);
        }

        public IEnumerable<SettingComposite> GetSettingsFromServer(int clientId, int environmentId)
        {
            string applicationName = this._configuration.GetSection(AppConfigurationKey.SectionName)[AppConfigurationKey.ApplicationName];
            string clientDataApiAppId = this._configuration.GetSection(AppConfigurationKey.SectionName)[AppConfigurationKey.ClientDataApiAppId];
            string clientDataApiAppKey = this._configuration.GetSection(AppConfigurationKey.SectionName)[AppConfigurationKey.ClientDataApiAppKey];

            string clientDataApiUrl = this.GetClientDataApiUrl(clientId, environmentId);
            clientDataApiUrl += UrlPath.ClientDataApiManaged;
            int apiTimeout = 10;

            ApiHandler apiHandler = new ApiHandler(clientDataApiAppId, clientDataApiAppKey, applicationName);
            HttpClient client = new HttpClient(apiHandler)
            {
                Timeout = TimeSpan.FromSeconds(apiTimeout)
            };
            try
            {
                HttpResponseMessage response = Task.Run(() => client.GetAsync(clientDataApiUrl)).Result;
                if (response.IsSuccessStatusCode)
                {
                    string responseString = Task.Run(() => response.Content.ReadAsStringAsync()).Result;
                    Dictionary<string, string> settings = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseString);
                    ReadOnlyDictionary<string, string> settingsReadOnlyDictionary = new ReadOnlyDictionary<string, string>(new Dictionary<string, string>(settings, StringComparer.OrdinalIgnoreCase)); //Need to ensure that the key is case-insensitive

                    IEnumerable<SettingComposite> settingCompositeList = settingsReadOnlyDictionary
                        .Select(x => new SettingComposite()
                        {
                            Name = x.Key,
                            Value = x.Value,
                            ValueSource = "Server",
                            DataType = "",
                            IsEditable = false,
                            IsEncrypted = false,
                            SettingId = -1,
                            SettingType = "",
                            SinceRelease = ""
                        });
                    return settingCompositeList;
                }
                throw new Exception($"SettingsProvider::Failed to call the Settings API because of response code. WebResponse: {AggregateWeb.AggregateWebResponse(System.Reflection.MethodBase.GetCurrentMethod().Name, response)}");
            }
            catch (Exception ex1)
            {
                throw new Exception($"SettingsProvider::Failed to call the Settings API because of exception. Exception: {ExceptionHelper.AggregateExceptionToString(ex1)}");
            }
        }

        #endregion

        #region Get BootGrid SettingComposite

        public BootGridResponse<SettingComposite> GetSettingsMaxBootgrid(BootGridRequest model)
        {
            if (model.release == SettingsSource.Server)
            {
                return this.GetBootGridResponseFromServer(model);
            }
            else
            {
                IQueryable<SettingComposite> query = this.GetSettings(model.client, model.environment, model.release);
                return this.GetBootGridResponse(query, model);
            }
        }

        private BootGridResponse<SettingComposite> GetBootGridResponseFromServer(BootGridRequest model)
        {
            IQueryable<SettingComposite> query = this.GetSettingsFromServer(model.client, model.environment).ToList().AsQueryable();
            return this.GetBootGridResponse(query, model);
        }

        private BootGridResponse<SettingComposite> GetBootGridResponse(IQueryable<SettingComposite> query, BootGridRequest model)
        {
            // LINQ order must be filter, then sort, then page

            int filteredRowCount = this.ApplyFilter(ref query, model.searchPhrase);
            IOrderedQueryable<SettingComposite> orderedQueryable = this.ApplySort(query, model.sortItems);
            int pageIndex = (model.current - 1) * model.rowCount;
            query = this.ApplyPaging(orderedQueryable, pageIndex, model.rowCount);

            BootGridResponse<SettingComposite> bootGridResponse = new BootGridResponse<SettingComposite>()
            {
                current = model.current,
                rowCount = model.rowCount,
                rows = query.ToList(),
                total = filteredRowCount
            };
            return bootGridResponse;
        }

        #endregion

        #region Hash Code

        public string GetLatestApprovedSettingsHashCode(int clientId, int environmentId, int releaseId)
        {
            //First try to find approval for the exact release specified Major.Minor.Patch.Build
            ClientReleaseEnvironmentApproval approval = this._clientReleaseEnvironmentApprovalRepository.GetQueryable()
                .OrderByDescending(x => x.InsertDate)
                .FirstOrDefault(x =>
                    x.ClientId == clientId
                    && x.EnvironmentId == environmentId
                    && x.ReleaseId == releaseId);

            if (approval == null)
            {
                // Exact release label was not found
                // Get the latest approval for the release label that matches Major.Minor.Patch and has a lower Build
                Release release = this._releaseRepository.GetById(releaseId);
                if (release != null)
                {
                    approval = this._clientReleaseEnvironmentApprovalRepository.GetQueryable()
                        .OrderByDescending(x => x.Release.Number)
                        .FirstOrDefault(x =>
                            x.ClientId == clientId
                            && x.EnvironmentId == environmentId
                            && x.Release.Major == release.Major
                            && x.Release.Minor == release.Minor
                            && x.Release.Patch == release.Patch
                            && x.Release.Build < release.Build);
                }
            }

            return approval?.HashValue ?? string.Empty;
        }

        public string GetSettingsHashCode(IQueryable<ClientSetting> clientSettings)
        {
            Dictionary<string, string> clientSettingsDictionary = clientSettings.ToDictionary(x => x.ClientSettingName, x => x.ClientSettingValueOverride ?? x.ClientSettingValue);
            return clientSettingsDictionary.DictionaryHashCodeHex<string, string, UTF8Encoding, SHA1Managed>();
        }

        public string GetSettingsHashCode(string client, string environment, string release)
        {
            return this.GetSettingsHashCode(this.GetClientSettings(client, environment, release));
        }

        #endregion

        #region Client Environment Release Lookup Values

        public IEnumerable<KeyValuePair<int, string>> GetReleases()
        {
            return this._releaseRepository.GetReleases().Select(x => new KeyValuePair<int, string>(x.ReleaseId, x.Label));
        }

        public IEnumerable<KeyValuePair<int, string>> GetClients()
        {
            return this._clientRepository.GetClients().Select(x => new KeyValuePair<int, string>(x.ClientId, x.Name));
        }

        public IEnumerable<KeyValuePair<int, string>> GetEnvironments()
        {
            return this._environmentRepository.GetEnvironments().Select(x => new KeyValuePair<int, string>(x.EnvironmentId, x.Description));
        }

        public string GetLatestReleaseLabel()
        {
            return this._releaseRepository.GetLatestReleaseLabel();
        }

        #endregion

        #region Setting Lookup Values

        public IEnumerable<KeyValuePair<int, string>> GetSettingTypes()
        {
            return this._settingTypeRepository.GetAll().OrderBy(x => x.Name).Select(x => new KeyValuePair<int, string>(x.SettingTypeId, x.Name)).ToList();
        }

        public IEnumerable<KeyValuePair<int, string>> GetSettingNames()
        {
            return this._settingRepository.GetAll().OrderBy(x => x.Name).Select(x => new KeyValuePair<int, string>(x.SettingId, x.Name)).ToList();
        }

        public IEnumerable<KeyValuePair<string, string>> GetSettingDataTypes()
        {
            return this._dataTypeList.Concat(this._dataTypeList.Select(x => "List<" + x.ToString() + ">")).OrderBy(x => x.ToString()).Select(x => new KeyValuePair<string, string>(x, x));
        }

        public SettingType GetSettingType(SettingTypeEnum settingTypeEnum)
        {
            return this._settingTypeRepository.GetSettingType(settingTypeEnum);
        }

        #endregion

        #region Add Settings

        public void AddSetting(SettingInput model)
        {
            int releaseId = this.AddRelease(model.StartReleaseMajor, model.StartReleaseMinor, model.StartReleasePatch, model.StartReleaseBuild, model.Username);
            int userId = this._settingsManagerUserRepository.AddSettingsManagerUser(model.Username);

            Setting newSetting = new Setting()
            {
                DataType = model.DataType,
                Description = model.Description,
                IsEditable = model.IsEditable,
                IsEncrypted = model.IsEncrypted,
                Name = model.Name,
                SettingTypeId = model.SettingTypeId,
                StartReleaseId = releaseId,
                Value = model.Value ?? String.Empty,
                InsertDate = DateTime.UtcNow,
                SettingsManagerUserId = userId
            };

            this.ValidateSettingValue(newSetting.Value, newSetting.DataType);
            this.AddSetting(newSetting);
        }

        public void AddSetting(Setting setting)
        {
            this._settingRepository.InsertOrUpdate(setting);
        }

        #endregion

        #region Add Release

        /// <summary>
        /// Adds a new release to the database if it does not already exist.
        /// </summary>
        /// <param name="major"></param>
        /// <param name="minor"></param>
        /// <param name="patch"></param>
        /// <param name="build"></param>
        /// <param name="insertedBy"></param>
        /// <returns>ReleaseId of the release.</returns>
        public int AddRelease(int major, int minor, int patch, int build, string insertedBy)
        {
            return this._releaseRepository.AddRelease(major, minor, patch, build, insertedBy);
        }

        #endregion

        #region Add Setting Override

        public void AddSettingOverride(SettingOverrideInput model)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                int releaseId = this.AddRelease(model.StartReleaseMajor, model.StartReleaseMinor, model.StartReleasePatch, model.StartReleaseBuild, model.Username);
                int userId = this._settingsManagerUserRepository.AddSettingsManagerUser(model.Username);
                bool hasClient = (model.ClientId > 0);
                bool hasEnvironment = (model.EnvironmentId > 0);

                if (!model.IsDeprecated)
                {
                    string dataType = this._settingRepository.GetById(model.SettingId).DataType;
                    string value = model.Value ?? string.Empty;
                    this.ValidateSettingValue(value, dataType);
                }

                if (model.IsDeprecated)
                {
                    SettingRelease newOverride = new SettingRelease()
                    {
                        StartReleaseId = releaseId,
                        IsEncrypted = false,
                        IsDeprecated = model.IsDeprecated,
                        SettingId = model.SettingId,
                        Value = string.Empty,
                        InsertDate = DateTime.UtcNow,
                        SettingsManagerUserId = userId
                    };
                    this._settingReleaseRepository.InsertOrUpdate(newOverride);
                }
                else if (hasClient && hasEnvironment)
                {
                    SettingClientReleaseEnvironment newOverride = new SettingClientReleaseEnvironment()
                    {
                        ClientId = model.ClientId,
                        StartReleaseId = releaseId,
                        EnvironmentId = model.EnvironmentId,
                        IsEncrypted = model.IsEncrypted,
                        SettingId = model.SettingId,
                        Value = model.Value ?? string.Empty,
                        InsertDate = DateTime.UtcNow,
                        SettingsManagerUserId = userId
                    };
                    this._settingClientReleaseEnvironmentRepository.InsertOrUpdate(newOverride);
                }
                else if (hasClient)
                {
                    SettingClientRelease newOverride = new SettingClientRelease()
                    {
                        ClientId = model.ClientId,
                        StartReleaseId = releaseId,
                        IsEncrypted = model.IsEncrypted,
                        SettingId = model.SettingId,
                        Value = model.Value ?? string.Empty,
                        InsertDate = DateTime.UtcNow,
                        SettingsManagerUserId = userId
                    };
                    this._settingClientReleaseRepository.InsertOrUpdate(newOverride);
                }
                else if (hasEnvironment)
                {
                    SettingEnvironmentRelease newOverride = new SettingEnvironmentRelease()
                    {
                        StartReleaseId = releaseId,
                        EnvironmentId = model.EnvironmentId,
                        IsEncrypted = model.IsEncrypted,
                        SettingId = model.SettingId,
                        Value = model.Value ?? string.Empty,
                        InsertDate = DateTime.UtcNow,
                        SettingsManagerUserId = userId
                    };
                    this._settingEnvironmentReleaseRepository.InsertOrUpdate(newOverride);
                }
                else
                {
                    SettingRelease newOverride = new SettingRelease()
                    {
                        StartReleaseId = releaseId,
                        IsEncrypted = model.IsEncrypted,
                        IsDeprecated = model.IsDeprecated,
                        SettingId = model.SettingId,
                        Value = model.Value ?? string.Empty,
                        InsertDate = DateTime.UtcNow,
                        SettingsManagerUserId = userId
                    };
                    this._settingReleaseRepository.InsertOrUpdate(newOverride);
                }

                unitOfWork.Commit();
            }
        }

        #endregion

        #region Client Release Environment Approval

        public void AddClientReleaseEnvironmentApproval(DeploymentContextApproval deploymentContextApproval)
        {
            bool hasClient = (deploymentContextApproval.ClientId > 0);
            bool hasEnvironment = (deploymentContextApproval.EnvironmentId > 0);
            bool hasRelease = (deploymentContextApproval.ReleaseId > 0);
            bool hasHashCode = !string.IsNullOrWhiteSpace(deploymentContextApproval.HashCode);

            if (hasClient && hasEnvironment && hasRelease && hasHashCode)
            {
                using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
                {
                    string currentHashCode = this.GetLatestApprovedSettingsHashCode(
                        deploymentContextApproval.ClientId,
                        deploymentContextApproval.EnvironmentId,
                        deploymentContextApproval.ReleaseId);

                    if (deploymentContextApproval.HashCode == currentHashCode)
                    {
                        return; //the context has already been approved
                    }

                    int userId = this._settingsManagerUserRepository.AddSettingsManagerUser(deploymentContextApproval.UserName);

                    ClientReleaseEnvironmentApproval clientReleaseEnvironmentApproval = new ClientReleaseEnvironmentApproval()
                    {
                        ClientId = deploymentContextApproval.ClientId,
                        ReleaseId = deploymentContextApproval.ReleaseId,
                        EnvironmentId = deploymentContextApproval.EnvironmentId,
                        HashValue = deploymentContextApproval.HashCode,
                        SettingsManagerUserId = userId
                    };
                    this._clientReleaseEnvironmentApprovalRepository.InsertOrUpdate(clientReleaseEnvironmentApproval);

                    unitOfWork.Commit();
                }
            }
        }

        #endregion

        #region Client Data API

        private string GetClientDataApiUrl(int clientId, int environmentId)
        {
            Client client = this._clientRepository.GetById(clientId);
            string clientUrlName = client?.UrlName;

            Environment environment = this._environmentRepository.GetById(environmentId);
            string environmentUrlName = environment?.UrlName;
            bool isDevelopment = (environment?.EnvironmentId == (int)EnvironmentEnum.Development);

            //This should be release agnostic, but we want to make sure we are using the latest value of the URL_BaseDomain setting
            int releaseIdLatest = this._releaseRepository.GetAll().OrderByDescending(x => x.Number).Select(x => x.ReleaseId).FirstOrDefault();
            string urlBaseDomain = this.GetSettings(clientId, environmentId, releaseIdLatest).SingleOrDefault(x => x.SettingId == (int)SettingEnum.UrlBaseDomain)?.Value;

            string clientDataApiUrl = $"https://clientdataapi-{environmentUrlName}-{clientUrlName}.{urlBaseDomain}";
            if (isDevelopment)
            {
                clientDataApiUrl = $"http://{urlBaseDomain}:15647";
            }
            return clientDataApiUrl;
        }

        #endregion

        #region Setting Value Validation

        private void ValidateSettingValue(string value, string dataType)
        {
            bool isListDataType = dataType.ToLower().StartsWith("list");
            if (!isListDataType)
            {
                //Make sure value can cast to the target data type
                try
                {
                    Type t = TypeHelper.GetTypeFromPrimitiveType(dataType);
                    TypeConverter converter = TypeDescriptor.GetConverter(t);
                    object tempValue = converter.ConvertFromString(value);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw new Exception($"The value \"{value}\" cannot be converted to the data type {dataType}");
                }
            }
        }

        #endregion

        #region Paged Query Functions

        private int ApplyFilter(ref IQueryable<SettingComposite> query, string searchPhrase)
        {
            int filteredRowCount;
            if (!string.IsNullOrWhiteSpace(searchPhrase))
            {
                searchPhrase = searchPhrase.ToUpper();
                query = query.Where(
                    x => x.Value.ToUpper().Contains(searchPhrase)
                    || x.Name.ToUpper().Contains(searchPhrase)
                    || x.SettingType.ToUpper().Contains(searchPhrase)
                    || x.DataType.ToUpper().Contains(searchPhrase)
                    || x.SinceRelease.ToUpper().Contains(searchPhrase)
                    || x.ValueSource.ToUpper().Contains(searchPhrase)
                    ); //filter
                filteredRowCount = query.Count();
            }
            else
            {
                filteredRowCount = query.Count();
            }
            return filteredRowCount;
        }

        /// <summary>
        /// Applies multi-column sorting instructions to a query
        /// </summary>
        /// <param name="filteredQuery">IQueryable that has already had a filter applied to it.</param>
        /// <param name="sortItems">List of column names and sort direction.</param>
        /// <returns></returns>
        private IOrderedQueryable<SettingComposite> ApplySort(IQueryable<SettingComposite> filteredQuery, IEnumerable<SortData> sortItems)
        {
            IOrderedQueryable<SettingComposite> orderedQueryable = null;

            List<SortData> sortItemList = new List<SortData>();
            if (sortItems != null)
            {
                sortItemList = sortItems.ToList();
            }

            if (sortItemList.Any())
            {
                for (int i = 0; i < sortItemList.Count; i++)
                {
                    string columnName = sortItemList.ElementAt(i).Field;
                    string direction = sortItemList.ElementAt(i).Type;
                    if (i == 0)
                    {
                        switch (columnName)
                        {
                            case "settingId":
                                orderedQueryable = direction == "asc" ? filteredQuery.OrderBy(x => x.SettingId) : filteredQuery.OrderByDescending(x => x.SettingId);
                                break;
                            case "settingType":
                                orderedQueryable = direction == "asc" ? filteredQuery.OrderBy(x => x.SettingType) : filteredQuery.OrderByDescending(x => x.SettingType);
                                break;
                            case "name":
                                orderedQueryable = direction == "asc" ? filteredQuery.OrderBy(x => x.Name) : filteredQuery.OrderByDescending(x => x.Name);
                                break;
                            case "dataType":
                                orderedQueryable = direction == "asc" ? filteredQuery.OrderBy(x => x.DataType) : filteredQuery.OrderByDescending(x => x.DataType);
                                break;
                            case "value":
                                orderedQueryable = direction == "asc" ? filteredQuery.OrderBy(x => x.Value) : filteredQuery.OrderByDescending(x => x.Value);
                                break;
                            case "isEditable":
                                orderedQueryable = direction == "asc" ? filteredQuery.OrderBy(x => x.IsEditable) : filteredQuery.OrderByDescending(x => x.IsEditable);
                                break;
                            case "isEncrypted":
                                orderedQueryable = direction == "asc" ? filteredQuery.OrderBy(x => x.IsEncrypted) : filteredQuery.OrderByDescending(x => x.IsEncrypted);
                                break;
                            case "sinceRelease":
                                orderedQueryable = direction == "asc" ? filteredQuery.OrderBy(x => x.SinceRelease) : filteredQuery.OrderByDescending(x => x.SinceRelease);
                                break;
                            case "valueSource":
                                orderedQueryable = direction == "asc" ? filteredQuery.OrderBy(x => x.ValueSource) : filteredQuery.OrderByDescending(x => x.ValueSource);
                                break;
                        }
                    }
                    else
                    {
                        this.ApplyAdditionalFilters(ref orderedQueryable, columnName, direction);
                    }
                }
            }
            else
            {
                orderedQueryable = filteredQuery.OrderBy(x => x.SettingType).ThenBy(x => x.Name);
            }

            return orderedQueryable;
        }

        private void ApplyAdditionalFilters(ref IOrderedQueryable<SettingComposite> orderedQueryable, string columnName, string direction)
        {
            switch (columnName)
            {
                case "settingId":
                    orderedQueryable = direction == "asc" ? orderedQueryable.ThenBy(x => x.SettingId) : orderedQueryable.ThenByDescending(x => x.SettingId);
                    break;
                case "settingType":
                    orderedQueryable = direction == "asc" ? orderedQueryable.ThenBy(x => x.SettingType) : orderedQueryable.ThenByDescending(x => x.SettingType);
                    break;
                case "name":
                    orderedQueryable = direction == "asc" ? orderedQueryable.ThenBy(x => x.Name) : orderedQueryable.ThenByDescending(x => x.Name);
                    break;
                case "dataType":
                    orderedQueryable = direction == "asc" ? orderedQueryable.ThenBy(x => x.DataType) : orderedQueryable.ThenByDescending(x => x.DataType);
                    break;
                case "value":
                    orderedQueryable = direction == "asc" ? orderedQueryable.ThenBy(x => x.Value) : orderedQueryable.ThenByDescending(x => x.Value);
                    break;
                case "isEditable":
                    orderedQueryable = direction == "asc" ? orderedQueryable.ThenBy(x => x.IsEditable) : orderedQueryable.ThenByDescending(x => x.IsEditable);
                    break;
                case "isEncrypted":
                    orderedQueryable = direction == "asc" ? orderedQueryable.ThenBy(x => x.IsEncrypted) : orderedQueryable.ThenByDescending(x => x.IsEncrypted);
                    break;
                case "sinceRelease":
                    orderedQueryable = direction == "asc" ? orderedQueryable.ThenBy(x => x.SinceRelease) : orderedQueryable.ThenByDescending(x => x.SinceRelease);
                    break;
                case "valueSource":
                    orderedQueryable = direction == "asc" ? orderedQueryable.ThenBy(x => x.ValueSource) : orderedQueryable.ThenByDescending(x => x.ValueSource);
                    break;
            }
        }

        /// <summary>
        /// Applys record set paging instructions
        /// </summary>
        /// <param name="orderedQuery">reference to IOrderedQueryable&lt:SettingComposite&gt; object</param>
        /// <param name="startRecordPosition">record number to start from</param>
        /// <param name="maxRecordCount"> number of records to return (-1 means return all records)</param>
        private IQueryable<SettingComposite> ApplyPaging(IOrderedQueryable<SettingComposite> orderedQuery, int startRecordPosition, int maxRecordCount)
        {
            if (maxRecordCount > -1)
            {
                return orderedQuery.Skip(startRecordPosition).Take(maxRecordCount);
            }
            return orderedQuery;
        }

        #endregion
    }
}

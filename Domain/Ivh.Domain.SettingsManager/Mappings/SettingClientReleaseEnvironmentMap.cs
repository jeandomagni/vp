﻿namespace Ivh.Domain.SettingsManager.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;
    public class SettingClientReleaseEnvironmentMap : ClassMap<SettingClientReleaseEnvironment>
    {
        public SettingClientReleaseEnvironmentMap()
        {
            this.Schema("dbo");
            this.Table("SettingClientReleaseEnvironment");
            this.Id(x => x.SettingClientReleaseEnvironmentId);
            this.Map(x => x.SettingId).Not.Nullable();
            this.Map(x => x.EnvironmentId).Not.Nullable();
            this.Map(x => x.ClientId).Not.Nullable();
            this.Map(x => x.StartReleaseId).Not.Nullable();
            this.Map(x => x.Value).Not.Nullable();
            this.Map(x => x.IsEncrypted).Not.Nullable();
            this.Map(x => x.InsertDate).Generated.Insert();
            this.Map(x => x.SettingsManagerUserId).Not.Nullable();
        }
    }
}

﻿namespace Ivh.Domain.SettingsManager.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;
    public class ClientReleaseEnvironmentApprovalMap : ClassMap<ClientReleaseEnvironmentApproval>
    {
        public ClientReleaseEnvironmentApprovalMap()
        {
            this.Schema("dbo");
            this.Table("ClientReleaseEnvironmentApproval");
            this.Id(x => x.ClientReleaseEnvironmentApprovalId);
            this.Map(x => x.ClientId).Not.Nullable();
            this.Map(x => x.EnvironmentId).Not.Nullable();
            this.Map(x => x.ReleaseId).Not.Nullable();
            this.Map(x => x.HashValue).Not.Nullable();
            this.Map(x => x.InsertDate).Generated.Insert();
            this.Map(x => x.SettingsManagerUserId).Not.Nullable();
            this.References(x => x.Release).Column("ReleaseId").ReadOnly();
        }
    }
}
﻿namespace Ivh.Domain.SettingsManager.Mappings
{
    using System;
    using System.Collections.Generic;
    using Entities;
    using FluentNHibernate.Mapping;

    public class ClientMap : ClassMap<Client>
    {
        public ClientMap()
        {
            this.Schema("dbo");
            this.Id(x => x.ClientId);
            this.Map(x => x.Name).Not.Nullable();
            this.Map(x => x.Description).Nullable();
            this.Map(x => x.UrlName).Not.Nullable();
            this.Map(x => x.InsertDate).Generated.Insert();
            this.Map(x => x.SettingsManagerUserId).Not.Nullable();
            this.Map(x => x.DeprecateDate).Nullable();
        }
    }
}


﻿namespace Ivh.Domain.SettingsManager.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;
    public class SettingTypeMap : ClassMap<SettingType>
    {
        public SettingTypeMap()
        {
            this.Schema("dbo");
            this.Table("SettingType");
            this.Id(x => x.SettingTypeId);
            this.Map(x => x.Name).Not.Nullable();
        }

        public virtual int SettingTypeId { get; set; }
        public virtual string Name { get; set; }
    }
}

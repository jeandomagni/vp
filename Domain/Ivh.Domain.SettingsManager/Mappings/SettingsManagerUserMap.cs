﻿namespace Ivh.Domain.SettingsManager.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;
    public class SettingsManagerUserMap : ClassMap<SettingsManagerUser>
    {
        public SettingsManagerUserMap()
        {
            this.Schema("dbo");
            this.Table("SettingsManagerUser");
            this.Id(x => x.SettingsManagerUserId);
            this.Map(x => x.UserName).Not.Nullable();
        }

        public virtual int SettingsManagerUserId { get; set; }
        public virtual string UserName { get; set; }
    }
}

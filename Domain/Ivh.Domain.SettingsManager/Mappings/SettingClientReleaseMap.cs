﻿namespace Ivh.Domain.SettingsManager.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;
    public class SettingClientReleaseMap : ClassMap<SettingClientRelease>
    {
        public SettingClientReleaseMap()
        {
            this.Schema("dbo");
            this.Table("SettingClientRelease");
            this.Id(x => x.SettingClientReleaseId);
            this.Map(x => x.SettingId).Not.Nullable();
            this.Map(x => x.ClientId).Not.Nullable();
            this.Map(x => x.StartReleaseId).Not.Nullable();
            this.Map(x => x.Value).Not.Nullable();
            this.Map(x => x.IsEncrypted).Not.Nullable();
            this.Map(x => x.InsertDate).Generated.Insert();
            this.Map(x => x.SettingsManagerUserId).Not.Nullable();
        }
    }
}

﻿namespace Ivh.Domain.SettingsManager.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class EnvironmentMap : ClassMap<Environment>
    {
        public EnvironmentMap()
        {
            this.Schema("dbo");
            this.Table("Environment");
            this.Id(x => x.EnvironmentId);
            this.Map(x => x.Name).Not.Nullable();
            this.Map(x => x.Description).Nullable();
            this.Map(x => x.UrlName).Not.Nullable();
            this.Map(x => x.InsertDate).Generated.Insert();
            this.Map(x => x.SettingsManagerUserId).Not.Nullable();
            this.Map(x => x.DeprecateDate).Nullable();
        }
    }
}

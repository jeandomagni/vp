﻿namespace Ivh.Domain.SettingsManager.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;
    public class ReleaseMap : ClassMap<Release>
    {
        public ReleaseMap()
        {
            this.Schema("dbo");
            this.Table("Release");
            this.Id(x => x.ReleaseId);
            this.Map(x => x.Major).Not.Nullable();
            this.Map(x => x.Minor).Not.Nullable();
            this.Map(x => x.Patch).Not.Nullable();
            this.Map(x => x.Build).Not.Nullable();
            this.Map(x => x.InsertDate).Generated.Insert();
            this.Map(x => x.SettingsManagerUserId).Not.Nullable();
            this.Map(x => x.Label).ReadOnly();
            this.Map(x => x.Number).ReadOnly();
        }
    }
}

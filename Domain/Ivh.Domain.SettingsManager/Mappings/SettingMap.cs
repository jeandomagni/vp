﻿namespace Ivh.Domain.SettingsManager.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;
    public class SettingMap : ClassMap<Setting>
    {
        public SettingMap()
        {
            this.Schema("dbo");
            this.Table("Setting");
            this.Id(x => x.SettingId);

            this.Map(x => x.Name).Not.Nullable();
            this.Map(x => x.Description).Nullable();
            this.Map(x => x.DataType).Nullable();
            this.Map(x => x.SettingTypeId).Not.Nullable();
            this.Map(x => x.StartReleaseId).Not.Nullable();
            this.Map(x => x.Value).Not.Nullable();
            this.Map(x => x.IsEncrypted).Not.Nullable();
            this.Map(x => x.IsEditable).Not.Nullable();
            this.Map(x => x.InsertDate).Generated.Insert();
            this.Map(x => x.SettingsManagerUserId).Not.Nullable();
        }
    }
}

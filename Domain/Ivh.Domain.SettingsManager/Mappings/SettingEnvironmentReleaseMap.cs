﻿namespace Ivh.Domain.SettingsManager.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;
    public class SettingEnvironmentReleaseMap : ClassMap<SettingEnvironmentRelease>
    {
        public SettingEnvironmentReleaseMap()
        {
            this.Schema("dbo");
            this.Table("SettingEnvironmentRelease");
            this.Id(x => x.SettingEnvironmentReleaseId);
            this.Map(x => x.SettingId).Not.Nullable();
            this.Map(x => x.EnvironmentId).Not.Nullable();
            this.Map(x => x.StartReleaseId).Not.Nullable();
            this.Map(x => x.Value).Not.Nullable();
            this.Map(x => x.IsEncrypted).Not.Nullable();
            this.Map(x => x.InsertDate).Generated.Insert();
            this.Map(x => x.SettingsManagerUserId).Not.Nullable();
        }
    }
}

﻿namespace Ivh.Domain.SettingsManager.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class SettingReleaseMap : ClassMap<SettingRelease>
    {
        public SettingReleaseMap()
        {
            this.Schema("dbo");
            this.Table("SettingRelease");
            this.Id(x => x.SettingReleaseId);
            
            this.Map(x => x.SettingId).Not.Nullable();
            this.Map(x => x.StartReleaseId).Not.Nullable();
            this.Map(x => x.Value).Not.Nullable();
            this.Map(x => x.IsEncrypted).Not.Nullable();
            this.Map(x => x.IsDeprecated).Not.Nullable();
            this.Map(x => x.InsertDate).Generated.Insert();
            this.Map(x => x.SettingsManagerUserId).Not.Nullable();

        }
    }
}

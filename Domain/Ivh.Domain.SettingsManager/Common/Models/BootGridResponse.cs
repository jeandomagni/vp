﻿using System.Collections.Generic;

namespace Ivh.Domain.SettingsManager.Common.Models
{
    public class BootGridResponse<T> where T : class
    {
        public int current { get; set; }
        public int rowCount { get; set; }
        public IEnumerable<T> rows { get; set; }
        public int total { get; set; }
    }
}

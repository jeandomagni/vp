﻿using System.Collections.Generic;

namespace Ivh.Domain.SettingsManager.Common.Models
{
    public class BootGridRequest
    {
        public int current { get; set; }
        public int rowCount { get; set; }
        public string searchPhrase { get; set; }
        public IEnumerable<SortData> sortItems { get; set; }
        public int client { get; set; }
        public int environment { get; set; }
        public int release { get; set; }
    }
}

﻿namespace Ivh.Domain.SettingsManager.Common.Models
{
    public class SortData
    {
        public string Field { get; set; } // Field Name
        public string Type { get; set; } // ASC or DESC
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace Ivh.Domain.SettingsManager.Common.Models
{
    public class SettingOverrideInput
    {
        [Required]
        public int SettingId { get; set; }

        public int ClientId { get; set; }

        public int EnvironmentId { get; set; }

        [Required]
        public int StartReleaseMajor { get; set; }

        [Required]
        public int StartReleaseMinor { get; set; }

        [Required]
        public int StartReleasePatch { get; set; }

        [Required]
        public int StartReleaseBuild { get; set; }

        public string Value { get; set; }

        public bool IsEncrypted { get; set; }

        public bool IsDeprecated { get; set; }

        public string Username { get; set; }
    }
}
﻿namespace Ivh.Domain.SettingsManager.Common.Models
{
    public class ClientSetting
    {
        public string ClientSettingKey { get; set; }
        public string ClientSettingValue { get; set; }
        public string ClientSettingValueOverride { get; set; }
        public string DataType { get; set; }
        public string ClientSettingName { get; set; }
        public string ClientSettingDescription { get; set; }
        public bool EditInUi { get; set; }
        public bool IsCmsVariable { get; set; }
        public bool IsEncrypted { get; set; }
        public int ChangeEventId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ivh.Domain.SettingsManager.Common.Models
{
    public class DeploymentContext
    {
        public int ClientId { get; set; }
        public int EnvironmentId { get; set; }
        public int ReleaseId { get; set; }
        public string HashCode{ get; set; }
    }
}

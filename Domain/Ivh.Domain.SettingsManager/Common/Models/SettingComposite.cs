﻿namespace Ivh.Domain.SettingsManager.Common.Models
{
    public partial class SettingComposite
    {
        public int SettingId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string SettingType { get; set; }
        public string DataType { get; set; }
        public bool IsEncrypted { get; set; }
        public bool IsEditable { get; set; }
        public string SinceRelease { get; set; }
        public string ValueSource { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace Ivh.Domain.SettingsManager.Common.Models
{
    public class SettingInput
    {
        [Required]
        [RegularExpression("[A-Za-z0-9].*")]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public string DataType { get; set; }

        [Required]
        public int SettingTypeId { get; set; }

        [Required]
        public int StartReleaseMajor { get; set; }

        [Required]
        public int StartReleaseMinor { get; set; }

        [Required]
        public int StartReleasePatch { get; set; }

        [Required]
        public int StartReleaseBuild { get; set; }

        public bool IsEncrypted { get; set; }

        public bool IsEditable { get; set; }

        public string Username { get; set; }

        public string Value { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ivh.Domain.SettingsManager.Common.Models
{
    public class DeploymentContextApprovalStatus
    {
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }
        public string ApprovedHashCode { get; set; }
        public string CurrentHashCode { get; set; }
    }
}

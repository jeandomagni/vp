﻿using System.Collections.Generic;

namespace Ivh.Domain.SettingsManager.Entities
{
    public class SettingType
    {
        public virtual int SettingTypeId { get; set; }
        public virtual string Name { get; set; }
    }
}

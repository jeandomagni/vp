﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ivh.Domain.SettingsManager.Entities
{
    public class ClientReleaseEnvironmentApproval
    {
        public virtual int ClientReleaseEnvironmentApprovalId { get; set; }
        public virtual int ClientId { get; set; }
        public virtual int EnvironmentId { get; set; }
        public virtual int ReleaseId { get; set; }
        public virtual string HashValue { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual int SettingsManagerUserId { get; set; }
        public virtual Release Release { get; set; }
    }
}

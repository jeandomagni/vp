﻿using System;
using System.Collections.Generic;

namespace Ivh.Domain.SettingsManager.Entities
{
    public class Setting
    {
        public Setting()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int SettingId { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual string Value { get; set; }
        public virtual string DataType { get; set; }
        public virtual int SettingTypeId { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual int SettingsManagerUserId { get; set; }
        public virtual int StartReleaseId { get; set; }
        public virtual bool IsEncrypted { get; set; }
        public virtual bool IsEditable { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace Ivh.Domain.SettingsManager.Entities
{
    public class SettingsManagerUser
    {
        public virtual int SettingsManagerUserId { get; set; }
        public virtual string UserName { get; set; }
    }
}

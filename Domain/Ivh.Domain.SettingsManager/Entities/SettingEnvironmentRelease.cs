﻿using System;

namespace Ivh.Domain.SettingsManager.Entities
{
    public class SettingEnvironmentRelease
    {
        public SettingEnvironmentRelease()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int SettingEnvironmentReleaseId { get; set; }
        public virtual int SettingId { get; set; }
        public virtual int EnvironmentId { get; set; }
        public virtual int StartReleaseId { get; set; }
        public virtual string Value { get; set; }
        public virtual bool IsEncrypted { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual int SettingsManagerUserId { get; set; }
    }
}

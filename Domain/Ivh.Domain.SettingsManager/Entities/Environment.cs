﻿using System;
using System.Collections.Generic;

namespace Ivh.Domain.SettingsManager.Entities
{
    public class Environment
    {
        public Environment()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int EnvironmentId { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual string UrlName { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual int SettingsManagerUserId { get; set; }
        public virtual DateTime? DeprecateDate { get; set; }
    }
}

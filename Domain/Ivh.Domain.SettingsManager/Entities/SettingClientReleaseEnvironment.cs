﻿using System;

namespace Ivh.Domain.SettingsManager.Entities
{
    public class SettingClientReleaseEnvironment
    {
        public SettingClientReleaseEnvironment()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int SettingClientReleaseEnvironmentId { get; set; }
        public virtual int SettingId { get; set; }
        public virtual int ClientId { get; set; }
        public virtual int EnvironmentId { get; set; }
        public virtual int StartReleaseId { get; set; }
        public virtual string Value { get; set; }
        public virtual bool IsEncrypted { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual int SettingsManagerUserId { get; set; }
    }
}

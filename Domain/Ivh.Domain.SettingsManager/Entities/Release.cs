﻿using System;
using System.Collections.Generic;

namespace Ivh.Domain.SettingsManager.Entities
{
    public class Release
    {
        public Release()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int ReleaseId { get; set; }
        public virtual int Major { get; set; }
        public virtual int Minor { get; set; }
        public virtual int Patch { get; set; }
        public virtual int Build { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual int SettingsManagerUserId { get; set; }
        public virtual string Label { get; set; }
        public virtual decimal? Number { get; set; }
    }
}

﻿namespace Ivh.Domain.SettingsManager.Modules
{
    using Interfaces;
    using Microsoft.Extensions.DependencyInjection;
    using Services;
    
    public static class Module
    {
        public static void RegisterSettingsManagerDomainServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<ISettingsManagerService, SettingsManagerService>();
        }
    }
}

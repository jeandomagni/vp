﻿namespace Ivh.Domain.SettingsManager.Enums
{
    public enum SettingTypeEnum
    {
        Theme = 1,
        Feature = 2,
        Display = 3,
        Behavior = 4,
        Location = 5,
        Sensitive = 6
    }
}

﻿namespace Ivh.Domain.Monitoring.Exceptions
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using Common.Base.Utilities.Extensions;
    using Interfaces;

    [Serializable]
    public class MonitorFailureException :Exception
    {
        protected MonitorFailureException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public MonitorFailureException(IMonitor monitor)
        {
            this.LoadException(monitor.ToListOfOne());
        }

        public MonitorFailureException(IEnumerable<IMonitor> monitors)
        {
            this.LoadException(monitors);
        }

        private void LoadException(IEnumerable<IMonitor> monitors)
        {
            foreach (IMonitor monitor in monitors)
            {
                this.Data.Add(monitor.Name, monitor.Information);
            }
        }
    }
}

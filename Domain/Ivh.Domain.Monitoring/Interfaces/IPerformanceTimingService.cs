﻿namespace Ivh.Domain.Monitoring.Interfaces
{
    using System;
    using Common.Base.Interfaces;

    public interface IPerformanceTimingService : IDomainService
    {
        void LogControllerTiming(string url, string requestIdenfitier, DateTime timeActionExecuting, DateTime timeActionExecuted, DateTime timeResultExecuted);
    }
}
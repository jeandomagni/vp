﻿namespace Ivh.Domain.Monitoring.Interfaces
{
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;
    public interface IMonitorRepository : IRepository<Monitor>
    {
        Monitor GetById(MonitorEnum monitorEnum);
    }
}

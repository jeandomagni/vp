﻿namespace Ivh.Domain.Monitoring.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IPerformanceTimingSourceRepository : IRepository<PerformanceTimingSource>
    {
    }
}
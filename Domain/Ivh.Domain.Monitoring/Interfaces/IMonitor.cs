﻿namespace Ivh.Domain.Monitoring.Interfaces
{
    using System.Collections.Generic;

    public interface IMonitor
    {
        bool Success { get; }
        string Name { get; }
        IDictionary<string,decimal> Parameters { get; }
        IDictionary<string, decimal> Data { get; }
        string Information { get; }
    }
}

﻿namespace Ivh.Domain.Monitoring.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IPerformanceTimingRepository : IRepository<PerformanceTiming>
    {
    }
}
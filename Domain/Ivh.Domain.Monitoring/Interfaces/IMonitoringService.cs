﻿namespace Ivh.Domain.Monitoring.Interfaces
{
    using System;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IMonitoringService : IDomainService
    {
        IMonitor GetMonitor(MonitorEnum monitor, DateTime? dateTimeBegin = null, DateTime? dateTimeEnd = null);
        T GetDatum<T>(DatumEnum datum, DateTime? dateTimeBegin = null, DateTime? dateTimeEnd = null);
    }
}

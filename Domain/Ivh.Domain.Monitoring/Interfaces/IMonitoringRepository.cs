﻿namespace Ivh.Domain.Monitoring.Interfaces
{
    using System;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IMonitoringRepository
    {
        T GetDatum<T>(DatumAttribute datumAttribute, DateTime? dateTimeBegin = null, DateTime? dateTimeEnd = null);
        T GetDatum<T>(DatumEnum datumEnum, DateTime? dateTimeBegin = null, DateTime? dateTimeEnd = null);
        
    }
}

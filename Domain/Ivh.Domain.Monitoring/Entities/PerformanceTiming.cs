﻿namespace Ivh.Domain.Monitoring.Entities
{
    using System;

    public class PerformanceTiming
    {
        public virtual int PerformanceTimingId { get; set; }
        public virtual string RequestIdentifier { get; set; }
        public virtual DateTime TimeStart { get; set; }
        public virtual long? TimeActionExecuted { get; set; }
        public virtual long TimeTotal { get; set; }
        public virtual PerformanceTimingSource PerformanceTimingSource { get; set; }
    }
}
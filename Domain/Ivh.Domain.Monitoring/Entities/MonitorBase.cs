﻿namespace Ivh.Domain.Monitoring.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Attributes;
    using Interfaces;
    using Newtonsoft.Json;

    public abstract class MonitorBase : IMonitor
    {
        /// <summary>
        /// Override in derived class to implement decision making
        /// </summary>
        /// <returns></returns>
        protected virtual bool IsSuccess()
        {
            return true;
        }

        protected MonitorBase()
        {
        }

        public bool IsInitialized()
        {
            IEnumerable<PropertyInfo> properties = this.GetType().GetProperties()
                .Where(prop => prop.PropertyType == typeof(decimal?)
                    && (Attribute.IsDefined(prop, typeof(DatumAttribute))
                || Attribute.IsDefined(prop, typeof(ParameterAttribute))));

            return properties.All(x =>
            {
                decimal? value = (decimal?)x.GetValue(this);
                return value.HasValue;
            });
        }

        public bool Success
        {
            get { return this.IsInitialized() && this.IsSuccess(); }
        }
        
        private string _name;
        public string Name
        {
            get
            {
                if (this._name.IsNullOrEmpty())
                {
                    if (Attribute.IsDefined(this.GetType(), typeof (MonitorAttribute)))
                    {
                        MonitorAttribute monitorAttribute = (MonitorAttribute) Attribute.GetCustomAttribute(this.GetType(), typeof (MonitorAttribute));
                        this._name = monitorAttribute.Monitor.ToString();
                        return this._name;
                    }
                    this._name = this.GetType().Name;
                }
                return this._name;
            }
        }

        private IDictionary<string, decimal> _parameters = null;
        public IDictionary<string, decimal> Parameters
        {
            get
            {
                if (this._parameters.IsNullOrEmpty())
                {
                    this._parameters = this.LoadAttributes<ParameterAttribute>();
                }
                return this._parameters;
            }
        }

        private IDictionary<string, decimal> _data = null;
        public IDictionary<string, decimal> Data
        {
            get
            {
                if (this._data.IsNullOrEmpty())
                {
                    this._data = this.LoadAttributes<DatumAttribute>();
                }
                return this._data;
            }
        }

        public string Information
        {
            get
            {
                var information = new
                {
                    this.Data,
                    this.Parameters
                };
                return JsonConvert.SerializeObject(information);
            }
        }

        private IDictionary<string, decimal> LoadAttributes<T>() where T : AttributeBase
        {
            Type attributeType = typeof (T);
            IList<PropertyInfo> publicProperties = this.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(prop => Attribute.IsDefined(prop, attributeType)).ToList();
            IList<PropertyInfo> nonPublicProperties = this.GetType().GetProperties(BindingFlags.NonPublic | BindingFlags.Instance).Where(prop => Attribute.IsDefined(prop, attributeType)).ToList();
            IDictionary<Tuple<int, string>, decimal> returnValue = new Dictionary<Tuple<int, string>, decimal>(publicProperties.Count);
            foreach (PropertyInfo property in publicProperties.Concat(nonPublicProperties).OrderBy(x => x.Name))
            {
                object[] attributes = property.GetCustomAttributes(attributeType, true);
                if (attributes.Any())
                {
                    T attribute = (T)attributes[0];
                    string name = property.Name;
                    object valueObject = property.GetValue(this);
                    try
                    {
                        decimal value = Convert.ToDecimal(valueObject);
                        returnValue[new Tuple<int, string>(attribute.Order, attribute.Name ?? name)] = value;
                    }
                    catch (FormatException)
                    {
                    }
                    catch (InvalidCastException)
                    {
                    }
                    catch (OverflowException)
                    {
                    }
                }
            }
            return returnValue.OrderBy(x=> x.Key.Item1).ThenBy(x => x.Key.Item2).ToDictionary(key => key.Key.Item2, v => v.Value);
        }


        public void Reload()
        {
            throw new NotImplementedException();
        }
    }
}

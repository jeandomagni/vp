﻿namespace Ivh.Domain.Monitoring.Entities
{
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class Monitor
    {
        public virtual int MonitorId { get; set; }

        public virtual MonitorEnum MonitorEnum
        {
            get { return (MonitorEnum) this.MonitorId; }
            set { this.MonitorId = (int)value; }
        }

        public virtual string MonitorName { get; set; }

        public virtual IList<MonitorParameter> MonitorParameters{get; set; }
    }
}

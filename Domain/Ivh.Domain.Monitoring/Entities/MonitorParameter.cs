﻿namespace Ivh.Domain.Monitoring.Entities
{
    public class MonitorParameter
    {
        public virtual int MonitorParameterId { get; set; }
        public virtual Monitor Monitor { get; set; }
        public virtual string MonitorParameterName { get; set; }
        public virtual decimal MonitorParameterValue { get; set; }
    }
}

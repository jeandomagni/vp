﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.EtlVsOutAccountReconciliationMonitor)]
    public class EtlVsOutAccountReconciliationMonitor : MonitorBase
    {
        [Datum(DatumEnum.AccountReconciliationCountActual)]
        public int? AccountReconciliationCountActual { get; set; }

        [Datum(DatumEnum.AccountReconciliationCountExpected)]
        public int? AccountReconciliationCountExpected { get; set; }
    }
}
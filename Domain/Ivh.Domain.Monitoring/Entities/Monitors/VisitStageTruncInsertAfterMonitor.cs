﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.VisitStageTruncInsertAfterMonitor)]
    public class VisitStageTruncInsertAfterMonitor : MonitorBase
    {
        [Datum(DatumEnum.HSAccountDeltasRecordCount)]
        public int? HSAccountDeltasRecordCount { get; set; }

        [Datum(DatumEnum.HSVisitStageRecordCount)]
        public int? HSVisitStageRecordCount { get; set; }

        protected override bool IsSuccess()
        {
            return base.IsSuccess();
        }
    }
}

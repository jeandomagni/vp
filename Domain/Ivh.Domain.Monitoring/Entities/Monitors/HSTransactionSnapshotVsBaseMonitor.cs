﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.HSTransactionSnapshotVsBaseMonitor)]
    public class HSTransactionSnapshotVsBaseMonitor : MonitorBase
    {
        [Datum(DatumEnum.HSTransactionSnapshotsRecordCount)]
        public int? HSTransactionSnapshotsRecordCount { get; set; }

        [Datum(DatumEnum.VisitTransactionRecordCdiCount)]
        public int? VisitTransactionRecordCdiCount { get; set; }
    }
}
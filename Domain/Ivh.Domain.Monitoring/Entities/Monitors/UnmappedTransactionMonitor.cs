﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.UnmappedTransactionMonitor)]
    public class UnmappedTransactionMonitor : MonitorBase
    {
        [Datum(DatumEnum.UnmappedTransactionCodesCount)]
        public decimal? UnmappedTransactionCodesCount { get; set; }

        [Datum(DatumEnum.VisitPayTransactionsWithUnmappedTransactionCount)]
        public decimal? VisitPayTransactionsWithUnmappedTransactionCount { get; set; }
    }
}
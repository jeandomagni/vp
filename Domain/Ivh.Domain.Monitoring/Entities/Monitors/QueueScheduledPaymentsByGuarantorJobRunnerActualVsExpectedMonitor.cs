﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.QueueScheduledPaymentsByGuarantorJobRunnerActualVsExpectedMonitor)]
    public class QueueScheduledPaymentsByGuarantorJobRunnerActualVsExpectedMonitor : MonitorBase
    {
        [Datum(DatumEnum.QueueScheduledPaymentsByGuarantorJobRunnerActual)]
        public decimal? QueueScheduledPaymentsByGuarantorJobRunnerActual { get; set; }
        [Datum(DatumEnum.QueueScheduledPaymentsByGuarantorJobRunnerExpected)]
        public decimal? QueueScheduledPaymentsByGuarantorJobRunnerExpected { get; set; }

        [Datum(DatumEnum.ScheduledPaymentsActual)]
        public decimal? ScheduledPaymentsActual { get; set; }
        [Datum(DatumEnum.ScheduledPaymentsExpected)]
        public decimal? ScheduledPaymentsExpected { get; set; }
        protected override bool IsSuccess()
        {
            return this.QueueScheduledPaymentsByGuarantorJobRunnerExpected == this.QueueScheduledPaymentsByGuarantorJobRunnerActual;
        }
    }
}

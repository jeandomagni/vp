﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.EtlVsOutTotalTransactionAmountMonitor)]
    public class EtlVsOutTotalTransactionAmountMonitor : MonitorBase
    {
        [Datum(DatumEnum.TransactionAmountActual)]
        public decimal? TransactionAmountActual { get; set; }

        [Datum(DatumEnum.TransactionAmountExpected)]
        public decimal? TransactionAmountExpected { get; set; }
    }
}
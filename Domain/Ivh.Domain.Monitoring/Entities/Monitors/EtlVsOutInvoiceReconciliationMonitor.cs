﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.EtlVsOutInvoiceReconciliationMonitor)]
    public class EtlVsOutInvoiceReconciliationMonitor : MonitorBase
    {
        [Datum(DatumEnum.InvoiceReconciliationCountActual)]
        public int? InvoiceReconciliationCountActual { get; set; }

        [Datum(DatumEnum.InvoiceReconciliationCountExpected)]
        public int? InvoiceReconciliationCountExpected { get; set; }
    }
}
﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.StatementFPLineVsFPBalanceMismatchCountMonitor)]
    public class StatementFPLineVsFPBalanceMismatchCountMonitor : MonitorBase
    {
        [Datum(DatumEnum.StatementFPLineVsFPBalanceMismatchCount)]
        public decimal? StatementFPLineVsFPBalanceMismatchCount { get; set; }

        protected override bool IsSuccess()
        {
            return this.StatementFPLineVsFPBalanceMismatchCount == 0;
        }
    }
}

﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.HSAccountDeltaVsStageMonitor)]
    public class HSAccountDeltaVsStageMonitor : MonitorBase
    {
        [Datum(DatumEnum.HSAccountDeltasRecordCount)]
        public int? HSAccountDeltasRecordCount { get; set; }

        [Datum(DatumEnum.HSVisitStageRecordCount)]
        public int? HSVisitStageRecordCount { get; set; }

        [Datum(DatumEnum.HSCernerAccountDeltaNotInStage)]
        public int? HSCernerAccountDeltaNotInStage { get; set; }

        [Datum(DatumEnum.HSiSeriesARAccountDeltaNotInStage)]
        public int? HSiSeriesARAccountDeltaNotInStage { get; set; }

        [Datum(DatumEnum.HSiSeriesBDAccountDeltaNotInStage)]
        public int? HSiSeriesBDAccountDeltaNotInStage { get; set; }


    }
}
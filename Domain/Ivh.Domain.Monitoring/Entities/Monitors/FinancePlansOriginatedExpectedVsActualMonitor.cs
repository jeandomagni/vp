﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.FinancePlansOriginatedExpectedVsActualMonitor)]
    public class FinancePlansOriginatedExpectedVsActualMonitor : MonitorBase
    {
        [Datum(DatumEnum.FinancePlansOriginatedExpected)]
        public int? FinancePlansOriginatedExpected { get; set; }

        [Datum(DatumEnum.FinancePlansOriginatedActual)]
        public int? FinancePlansOriginatedActual { get; set; }
    }
}

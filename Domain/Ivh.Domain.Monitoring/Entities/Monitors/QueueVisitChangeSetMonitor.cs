﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.QueueVisitChangeSetMonitor)]
    public class QueueVisitChangeSetMonitor : MonitorBase
    {
        [Datum(DatumEnum.VpGuarantorIdDifferencesCount)]
        public int? VpGuarantorIdDifferencesCount { get; set; }

        [Datum(DatumEnum.VisitsRecalledCountExpected)]
        public int? VisitsRecalledCountExpected { get; set; }
    }
}
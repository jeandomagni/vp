﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.EmailFinalPastDueExpectedVsActualMonitor)]
    public class EmailFinalPastDueExpectedVsActualMonitor : MonitorBase
    {
        [Datum(DatumEnum.EmailFinalPastDueActual)]
        public int? EmailFinalPastDueActual { get; set; }

        [Datum(DatumEnum.EmailFinalPastDueExpected)]
        public int? EmailFinalPastDueExpected { get; set; }

        protected override bool IsSuccess()
        {
            return this.EmailFinalPastDueActual.Value == this.EmailFinalPastDueExpected.Value;
        }
    }
}

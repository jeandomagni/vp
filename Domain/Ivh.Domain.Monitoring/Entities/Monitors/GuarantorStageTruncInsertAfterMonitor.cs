﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.GuarantorStageTruncInsertAfterMonitor)]
    public class GuarantorStageTruncInsertAfterMonitor : MonitorBase
    {
        [Datum(DatumEnum.HSGuarantorDeltaRecordCount)]
        public int? HSGuarantorDeltaRecordCount { get; set; }

        [Datum(DatumEnum.HSGuarantorStageRecordCount)]
        public int? HSGuarantorStageRecordCount { get; set; }

        protected override bool IsSuccess()
        {
            return base.IsSuccess();
        }
    }
}

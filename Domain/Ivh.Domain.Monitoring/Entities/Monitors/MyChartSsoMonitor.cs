﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.MyChartSsoMonitor)]
    public class MyChartSsoMonitor : MonitorBase
    {
        [Datum(DatumEnum.MyChartGuarantorDailyRecordCount)]
        public int? MyChartGuarantorDailyRecordCount { get; set; }

    }
}

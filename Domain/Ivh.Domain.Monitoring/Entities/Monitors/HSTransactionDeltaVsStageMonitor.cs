﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.HSTransactionDeltaVsStageMonitor)]
    public class HSTransactionDeltaVsStageMonitor : MonitorBase
    {
        [Datum(DatumEnum.HSTransactionDeltasRecordCount)]
        public int? HSTransactionDeltasRecordCount { get; set; }

        [Datum(DatumEnum.VisitTransactionStageRecordCount)]
        public int? VisitTransactionStageRecordCount { get; set; }

        [Datum(DatumEnum.HSCernerChargesDeltaNotInStage)]
        public int? HSCernerChargesDeltaNotInStage { get; set; }

        [Datum(DatumEnum.HSCernerTransactionDeltaNotInStage)]
        public int? HSCernerTransactionDeltaNotInStage { get; set; }

        [Datum(DatumEnum.HSiSeriesARTransactionDeltaNotInStage)]
        public int? HSiSeriesARTransactionDeltaNotInStage { get; set; }

        [Datum(DatumEnum.HSiSeriesBDTransactionDeltaNotInStage)]
        public int? HSiSeriesBDTransactionDeltaNotInStage { get; set; }

    }
}
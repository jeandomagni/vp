﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.EtlVsOutWorkQueueAccountsMonitor)]
    public class EtlVsOutWorkQueueAccountsMonitor : MonitorBase
    {
        [Datum(DatumEnum.WorkQueueCountActual)]
        public int? WorkQueueCountActual { get; set; }

        [Datum(DatumEnum.WorkQueueCountExpected)]
        public int? WorkQueueCountExpected { get; set; }
    }
}
﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.HSPersonRecordCountSnapshotVsBaseMonitor)]
    public class HSPersonRecordCountSnapshotVsBaseMonitor : MonitorBase
    {
        [Datum(DatumEnum.HSPersonSnapshotRecordCount)]
        public int? HSPersonSnapshotRecordCount { get; set; }

        [Datum(DatumEnum.HSGuarantorRecordCdiCount)]
        public int? HSGuarantorRecordCdiCount { get; set; }
    }
}
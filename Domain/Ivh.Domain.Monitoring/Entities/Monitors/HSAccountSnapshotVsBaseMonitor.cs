﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.HSAccountSnapshotVsBaseMonitor)]
    public class HSAccountSnapshotVsBaseMonitor : MonitorBase
    {
        [Datum(DatumEnum.HSAccountSnapshotsRecordCount)]
        public int? HSAccountSnapshotsRecordCount { get; set; }

        [Datum(DatumEnum.VisitRecordCdiCount)]
        public int? VisitRecordCdiCount { get; set; }
    }
}
﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.EmailFinancePlanDurationIncreaseMonitor)]
    public class EmailFinancePlanDurationIncreaseMonitor : MonitorBase
    {
        [Datum(DatumEnum.FinancePlanDurationIncreaseActual)]
        public int? FinancePlanDurationIncreaseActual { get; set; }
    }
}

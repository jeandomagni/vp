﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.EmailUncollectableExpectedVsActualMonitor)]
    public class EmailUncollectableExpectedVsActualMonitor : MonitorBase
    {
        [Datum(DatumEnum.EmailUncollectableActual)]
        public decimal? EmailUncollectableActual { get; set; }

        [Datum(DatumEnum.EmailUncollectableExpected)]
        public decimal? EmailUncollectableExpected { get; set; }

        protected override bool IsSuccess()
        {
            return this.EmailUncollectableExpected == this.EmailUncollectableActual;
        }
    }
}
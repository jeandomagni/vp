﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.QueueClosedUncollectablesExpectedMonitor)]
    public class QueueClosedUncollectablesExpectedMonitor : MonitorBase
    {
        [Datum(DatumEnum.ClosedUncollectableNonFPVisitsExpected)]
        public decimal? ClosedUncollectableNonFPVisitsExpected { get; set; }

        [Datum(DatumEnum.ClosedUncollectableFinancePlanVisitsExpected)]
        public decimal? ClosedUncollectableFinancePlanVisitsExpected { get; set; }

        public bool Equals(QueueClosedUncollectablesActualMonitor actual)
        {
            return actual != null
                && this.IsInitialized()
                && actual.IsInitialized()
                && this.ClosedUncollectableNonFPVisitsExpected.Value == actual.ClosedUncollectableVisitsActual.Value
                && actual.ClosedUncollectableFinancePlanVisitsActual.Value == this.ClosedUncollectableFinancePlanVisitsExpected.Value;
        }
    }
}

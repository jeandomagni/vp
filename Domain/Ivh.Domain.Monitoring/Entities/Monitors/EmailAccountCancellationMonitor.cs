﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.EmailAccountCancellationMonitor)]
    public class EmailAccountCancellationMonitor : MonitorBase
    {
        [Datum(DatumEnum.AccountCancellationCountActual)]
        public int? AccountCancellationCountActual { get; set; }
    }
}

﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.QueueChangeEventsAfterMonitor)]
    public class QueueChangeEventsAfterMonitor : MonitorBase
    {
        [Datum(DatumEnum.ChangeEventRecordType2Count)]
        public int? ChangeEventRecordType2Count { get; set; }

        [Datum(DatumEnum.ChangeEventRecordType3Count)]
        public int? ChangeEventRecordType3Count { get; set; }

        [Datum(DatumEnum.ChangeEventRecordType4Count)]
        public int? ChangeEventRecordType4Count { get; set; }

        [Datum(DatumEnum.ChangeEventRecordType5Count)]
        public int? ChangeEventRecordType5Count { get; set; }

        [Datum(DatumEnum.ChangeEventRecordType6Count)]
        public int? ChangeEventRecordType6Count { get; set; }

        [Datum(DatumEnum.ChangeEventRecordType7Count)]
        public int? ChangeEventRecordType7Count { get; set; }

        protected override bool IsSuccess()
        {
            return base.IsSuccess();
        }
    }
}

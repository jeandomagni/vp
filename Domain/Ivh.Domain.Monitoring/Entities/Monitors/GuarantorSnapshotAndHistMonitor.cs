﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.GuarantorSnapshotAndHistMonitor)]
    public class GuarantorSnapshotAndHistMonitor : MonitorBase
    {
        [Datum(DatumEnum.GuarantorDailyRecordCount)]
        public decimal? GuarantorDailyRecordCount { get; set; }

        [Parameter(ParameterEnum.GuarantorDailyRecordMin)]
        public decimal? GuarantorDailyRecordMin { get; set; }

        protected override bool IsSuccess()
        {
            return this.GuarantorDailyRecordCount >= this.GuarantorDailyRecordMin;
        }
    }
}
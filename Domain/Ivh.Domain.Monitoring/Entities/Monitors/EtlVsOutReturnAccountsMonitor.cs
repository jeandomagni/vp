﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.EtlVsOutReturnAccountsMonitor)]
    public class EtlVsOutReturnAccountsMonitor : MonitorBase
    {
        [Datum(DatumEnum.ReturnAccountsCountActual)]
        public int? ReturnAccountsCountActual { get; set; }

        [Datum(DatumEnum.UncollectableAccountsCountExpected)]
        public int? UncollectableAccountsCountExpected { get; set; }
    }
}
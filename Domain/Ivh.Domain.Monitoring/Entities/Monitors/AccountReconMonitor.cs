﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.AccountReconMonitor)]
    public class AccountReconMonitor : MonitorBase
    {
        [Datum(DatumEnum.AccountReconActual)]
        public int? AccountReconActual { get; set; }

        [Datum(DatumEnum.AccountReconCDIExpected)]
        public int? AccountReconCDIExpected { get; set; }
        [Datum(DatumEnum.AccountReconAppExpected)]
        public int? AccountReconAppExpected { get; set; }
    }
}
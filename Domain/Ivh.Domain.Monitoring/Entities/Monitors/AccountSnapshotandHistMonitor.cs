﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.AccountSnapshotandHistMonitor)]
    public class AccountSnapshotandHistMonitor : MonitorBase
    {
        [Datum(DatumEnum.AccountDailyRecordCount)]
        public decimal? AccountDailyRecordCount { get; set; }

        [Parameter(ParameterEnum.AccountDailyRecordMin)]
        public decimal? AccountDailyRecordMin { get; set; }

        protected override bool IsSuccess()
        {
            return this.AccountDailyRecordCount >= this.AccountDailyRecordMin;
        }
    }
}

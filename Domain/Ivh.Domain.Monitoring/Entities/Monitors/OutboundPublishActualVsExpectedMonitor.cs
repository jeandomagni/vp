﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.OutboundPublishActualVsExpectedMonitor)]
    public class OutboundPublishActualVsExpectedMonitor : MonitorBase
    {
        [Datum(DatumEnum.ExpectedSumOfPrincipalPayments, 1)]
        public decimal? ExpectedSumOfPrincipalPayments { get; set; }//OB_001

        [Datum(DatumEnum.ExpectedSumOfInterestPayments, 3)]
        public decimal? ExpectedSumOfInterestPayments { get; set; }//OB_003

        [Datum(DatumEnum.ExpectedSumOfAdjustments, 5)]
        public decimal? ExpectedSumOfAdjustments { get; set; }//OB_005

        [Datum(DatumEnum.ExpectedGuarantorStatus118Count, 7)]
        public int? ExpectedGuarantorStatus118Count { get; set; }//OB_007

        [Datum(DatumEnum.ExpectedVisitPayDrivenBillingIndicatorCount, 9)]
        public int? ExpectedVisitPayDrivenBillingIndicatorCount { get; set; }//OB_009

        [Datum(DatumEnum.ExpectedNotesCount, 11)]
        public int? ExpectedNotesCount { get; set; }//OB_011

        [Datum(DatumEnum.ExpectedGuarantorReconCount, 13)]
        public int? ExpectedGuarantorReconCount { get; set; }//OB_013

        [Datum(DatumEnum.ExpectedAccountReconCount, 15)]
        public int? ExpectedAccountReconCount { get; set; }//OB_015



        [Datum(DatumEnum.ActualSumOfPrincipalPayments, 2)]
        public decimal? ActualSumOfPrincipalPayments { get; set; }//OB_002

        [Datum(DatumEnum.ActualSumOfInterestPayments, 4)]
        public decimal? ActualSumOfInterestPayments { get; set; }//OB_004

        [Datum(DatumEnum.ActualSumOfAdjustments, 6)]
        public decimal? ActualSumOfAdjustments { get; set; }//OB_006

        [Datum(DatumEnum.ActualGuarantorStatus118Count, 8)]
        public int? ActualGuarantorStatus118Count { get; set; }//OB_008

        [Datum(DatumEnum.ActualVisitPayDrivenBillingIndicatorCount, 10)]
        public int? ActualVisitPayDrivenBillingIndicatorCount { get; set; }//OB_010

        [Datum(DatumEnum.ActualNotesCount, 12)]
        public int? ActualNotesCount { get; set; }//OB_012

        [Datum(DatumEnum.ActualGuarantorReconCount, 14)]
        public int? ActualGuarantorReconCount { get; set; }//OB_014

        [Datum(DatumEnum.ActualAccountReconCount, 16)]
        public int? ActualAccountReconCount { get; set; }//OB_016
       
        protected override bool IsSuccess()
        {
            return base.IsSuccess();
        }
    }
}

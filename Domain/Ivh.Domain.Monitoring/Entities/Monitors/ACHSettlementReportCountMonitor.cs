﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.ACHSettlementReportCountMonitor)]
    public class ACHSettlementReportCountMonitor : MonitorBase
    {
        [Datum(DatumEnum.ACHSettlementReportCount)]
        public decimal? ACHSettlementReportCount { get; set; }

        protected override bool IsSuccess()
        {
            return this.ACHSettlementReportCount > 0;
        }
    }
}

﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.HSCernerTransactionDailyRecordCountMonitor)]
    public class HSCernerTransactionDailyRecordCountMonitor : MonitorBase
    {
        [Datum(DatumEnum.HSCernerTransactionDailyRecordCount)]
        public int? HSCernerTransactionDailyRecordCount { get; set; }
    }
}
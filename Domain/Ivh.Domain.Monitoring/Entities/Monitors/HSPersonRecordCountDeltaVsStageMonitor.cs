﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.HSPersonRecordCountDeltaVsStageMonitor)]
    public class HSPersonRecordCountDeltaVsStageMonitor : MonitorBase
    {
        [Datum(DatumEnum.HSPersonDeltaRecordCount)]
        public int? HSPersonDeltaRecordCount { get; set; }

        [Datum(DatumEnum.HSGuarantorStageRecordCount)]
        public int? HSGuarantorStageRecordCount { get; set; }

        [Datum(DatumEnum.HSPersonDeltaNotInStageCount)]
        public int? HSPersonDeltaNotInStageCount { get; set; }
    }
}
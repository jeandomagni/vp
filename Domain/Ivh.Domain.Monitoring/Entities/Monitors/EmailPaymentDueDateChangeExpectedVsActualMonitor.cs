﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.EmailPaymentDueDateChangeExpectedVsActualMonitor)]
    public class EmailPaymentDueDateChangeExpectedVsActualMonitor : MonitorBase
    {
        [Datum(DatumEnum.EmailPaymentDueDateChangeActual)]
        public decimal? EmailPaymentDueDateChangeActual { get; set; }

        [Datum(DatumEnum.EmailPaymentDueDateChangeExpected)]
        public decimal? EmailPaymentDueDateChangeExpected { get; set; }

        protected override bool IsSuccess()
        {
            return this.EmailPaymentDueDateChangeActual == this.EmailPaymentDueDateChangeExpected;
        }
    }
}
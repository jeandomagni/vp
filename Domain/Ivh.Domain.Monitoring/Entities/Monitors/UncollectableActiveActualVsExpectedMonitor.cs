﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.UncollectableActiveActualVsExpectedMonitor)]
    public class UncollectableActiveActualVsExpectedMonitor : MonitorBase
    {
        [Datum(DatumEnum.ActiveUncollectableVisitsExpected)]
        public int? ActiveUncollectableVisitsExpected { get; set; }

        [Datum(DatumEnum.ActiveUncollectableVisitsActual)]
        public int? ActiveUncollectableVisitsActual { get; set; }

        [Datum(DatumEnum.ActiveUncollectableFinancePlansExpected)]
        public int? ActiveUncollectableFinancePlansExpected { get; set; }

        [Datum(DatumEnum.ActiveUncollectableFinancePlansActual)]
        public int? ActiveUncollectableFinancePlansActual { get; set; }

        protected override bool IsSuccess()
        {
            return this.ActiveUncollectableVisitsExpected.Value == this.ActiveUncollectableVisitsActual.Value
                && this.ActiveUncollectableFinancePlansExpected.Value == this.ActiveUncollectableFinancePlansActual.Value;
        }
    }
}

﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.ACHClosedPaidExpectedVsActualMonitor)]
    public class ACHClosedPaidExpectedVsActualMonitor : MonitorBase
    {
        [Datum(DatumEnum.ACHClosedPaidActual)]
        public int? ACHClosedPaidActual { get; set; }

        [Datum(DatumEnum.ACHClosedPaidExpected)]
        public int? ACHClosedPaidExpected { get; set; }

        protected override bool IsSuccess()
        {
            return this.ACHClosedPaidExpected == this.ACHClosedPaidActual;
        }
    }
}

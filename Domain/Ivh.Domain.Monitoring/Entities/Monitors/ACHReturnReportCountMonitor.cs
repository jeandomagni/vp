﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.ACHReturnReportCountMonitor)]
    public class ACHReturnReportCountMonitor : MonitorBase
    {
        [Datum(DatumEnum.ACHReturnReportCount)]
        public decimal? ACHReturnReportCount { get; set; }

        protected override bool IsSuccess()
        {
            return this.ACHReturnReportCount > 0;
        }
    }
}

﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.ACHClosedFailedExpectedVsActualMonitor)]
    public class ACHClosedFailedExpectedVsActualMonitor : MonitorBase
    {
        [Datum(DatumEnum.ACHClosedFailedActual)]
        public int? ACHClosedFailedActual { get; set; }

        [Datum(DatumEnum.ACHClosedFailedExpected)]
        public int? ACHClosedFailedExpected { get; set; }

        protected override bool IsSuccess()
        {
            return this.ACHClosedFailedActual == this.ACHClosedFailedExpected;
        }
    }
}

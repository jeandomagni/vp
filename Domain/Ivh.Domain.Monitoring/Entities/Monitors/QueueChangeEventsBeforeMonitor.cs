﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.QueueChangeEventsBeforeMonitor)]
    public class QueueChangeEventsBeforeMonitor : MonitorBase
    {
        [Datum(DatumEnum.GuarantorChangeSetRecordCount)]
        public int? GuarantorChangeSetRecordCount { get; set; }

        [Datum(DatumEnum.VisitChangeSetRecordCount)]
        public int? VisitChangeSetRecordCount { get; set; }

        [Datum(DatumEnum.VisitTransactionChangeSetRecordCount)]
        public int? VisitTransactionChangeSetRecordCount { get; set; }
        protected override bool IsSuccess()
        {
            return base.IsSuccess();
        }
    }
}

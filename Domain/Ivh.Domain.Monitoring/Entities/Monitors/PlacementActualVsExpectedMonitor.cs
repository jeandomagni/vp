﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.PlacementActualVsExpectedMonitor)]
    public class PlacementActualVsExpectedMonitor : MonitorBase
    {
        [Datum(DatumEnum.PlacementCountActual)]
        public int? PlacementCountActual { get; set; }

        [Datum(DatumEnum.PlacementCountExpected)]
        public int? PlacementCountExpected { get; set; }
    }
}
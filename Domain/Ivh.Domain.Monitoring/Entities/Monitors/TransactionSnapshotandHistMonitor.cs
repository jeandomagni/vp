﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.TransactionSnapshotandHistMonitor)]
    public class TransactionSnapshotandHistMonitor : MonitorBase
    {
        [Datum(DatumEnum.TransactionDailyRecordCount)]
        public decimal? TransactionDailyRecordCount { get; set; }

        [Parameter(ParameterEnum.TransactionDailyRecordMin)]
        public decimal? TransactionDailyRecordMin { get; set; }

        protected override bool IsSuccess()
        {
            return this.TransactionDailyRecordCount >= this.TransactionDailyRecordMin;
        }
    }
}

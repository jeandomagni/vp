﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.EmailStatementNotificationExpectedVsActualMonitor)]
    public class EmailStatementNotificationExpectedVsActualMonitor : MonitorBase
    {
        [Datum(DatumEnum.EmailStatementNotificationActual)]
        public decimal? EmailStatementNotificationActual { get; set; }

        [Datum(DatumEnum.EmailStatementNotificationExpected)]
        public decimal? EmailStatementNotificationExpected { get; set; }

        protected override bool IsSuccess()
        {
            return this.EmailStatementNotificationActual == this.EmailStatementNotificationExpected;
        }
    }
}
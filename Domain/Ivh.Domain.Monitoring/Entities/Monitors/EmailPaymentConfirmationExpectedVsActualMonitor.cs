﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.EmailPaymentConfirmationExpectedVsActualMonitor)]
    public class EmailPaymentConfirmationExpectedVsActualMonitor : MonitorBase
    {
        [Datum(DatumEnum.EmailPaymentConfirmationActual)]
        public decimal? EmailPaymentConfirmationActual { get; set; }

        [Datum(DatumEnum.EmailPaymentConfirmationExpected)]
        public decimal? EmailPaymentConfirmationExpected { get; set; }

        protected override bool IsSuccess()
        {
            return this.EmailPaymentConfirmationActual == this.EmailPaymentConfirmationExpected;
        }
    }
}

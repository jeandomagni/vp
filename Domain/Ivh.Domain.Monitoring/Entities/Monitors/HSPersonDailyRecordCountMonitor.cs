﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.HSPersonDailyRecordCountMonitor)]
    public class HSPersonDailyRecordCountMonitor : MonitorBase
    {
        [Datum(DatumEnum.HSPersonDailyRecordCount)]
        public int? HSPersonDailyRecordCount { get; set; }
    }

}
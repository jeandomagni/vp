﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.EmailPaymentFailuresExpectedVsActualMonitor)]
    public class EmailPaymentFailuresExpectedVsActualMonitor : MonitorBase
    {
        [Datum(DatumEnum.EmailPaymentFailureActual)]
        public decimal? EmailPaymentFailureActual { get; set; }

        [Datum(DatumEnum.EmailPaymentFailureExpected)]
        public decimal? EmailPaymentFailureExpected { get; set; }

        protected override bool IsSuccess()
        {
            return this.EmailPaymentFailureExpected == this.EmailPaymentFailureActual;
        }
    }
}

﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.PublishCdiEtlToVpEtlMonitor)]
    public class PublishCdiEtlToVpEtlMonitor : MonitorBase
    {
        [Datum(DatumEnum.HsCurrentBalanceVisitBalanceMismatchCount)]
        public int? HsCurrentBalanceVisitBalanceMismatchCount { get; set; }

        [Parameter(ParameterEnum.HsCurrentBalanceVisitBalanceMismatchMaxAllowable)]
        public int? HsCurrentBalanceVisitBalanceMismatchMaxAllowable { get; set; }

        protected override bool IsSuccess()
        {
            return this.HsCurrentBalanceVisitBalanceMismatchCount <= this.HsCurrentBalanceVisitBalanceMismatchMaxAllowable;
        }
    }
}

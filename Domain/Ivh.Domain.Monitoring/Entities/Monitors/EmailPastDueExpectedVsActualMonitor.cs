﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.EmailPastDueExpectedVsActualMonitor)]
    public class EmailPastDueExpectedVsActualMonitor : MonitorBase
    {
        [Datum(DatumEnum.EmailPastDueActual)]
        public int? EmailPastDueActual { get; set; }

        [Datum(DatumEnum.EmailPastDueExpected)]
        public int? EmailPastDueExpected { get; set; }

        protected override bool IsSuccess()
        {
            return this.EmailPastDueActual.Value == this.EmailPastDueExpected.Value;
        }
    }
}

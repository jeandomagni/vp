﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.PaymentActualVsExpectedMonitor)]
    public class PaymentActualVsExpectedMonitor : MonitorBase
    {
        [Datum(DatumEnum.PaymentActual)]
        public decimal? PaymentActual { get; set; }

        [Datum(DatumEnum.PaymentExpected)]
        public decimal? PaymentExpected { get; set; }

        protected override bool IsSuccess()
        {
            return this.PaymentActual == this.PaymentExpected;
        }
    }
}

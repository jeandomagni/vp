﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.HSCernerChargesDailyRecordCountMonitor)]
    public class HSCernerChargesDailyRecordCountMonitor : MonitorBase
    {
        [Datum(DatumEnum.HSCernerChargesDailyRecordCount)]
        public int? HSCernerChargesDailyRecordCount { get; set; }
    }
}
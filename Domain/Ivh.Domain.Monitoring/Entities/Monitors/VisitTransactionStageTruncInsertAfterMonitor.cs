﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.VisitTransactionStageTruncInsertAfterMonitor)]
    public class VisitTransactionStageTruncInsertAfterMonitor : MonitorBase
    {
        [Datum(DatumEnum.HSTransactionDeltaRecordCount)]
        public int? HSTransactionDeltaRecordCount { get; set; }//CDI_018
        

        [Datum(DatumEnum.VisitTransactionStageRecordCount)]
        public int? VisitTransactionStageRecordCount { get; set; }//CDI_017

        protected override bool IsSuccess()
        {
            return base.IsSuccess();
        }
    }
}

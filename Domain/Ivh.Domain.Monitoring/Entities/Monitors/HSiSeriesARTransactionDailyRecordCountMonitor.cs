﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.HSiSeriesARTransactionDailyRecordCountMonitor)]
    public class HSiSeriesARTransactionDailyRecordCountMonitor : MonitorBase
    {
        [Datum(DatumEnum.HSiSeriesARTransactionDailyRecordCount)]
        public int? HSiSeriesARTransactionDailyRecordCount { get; set; }
    }
}
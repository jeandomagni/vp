﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.StatementLineMismatchCountMonitor)]
    public class StatementLineMismatchCountMonitor : MonitorBase
    {
        [Datum(DatumEnum.StatementLineMismatchCount)]
        public decimal? StatementLineMismatchCount { get; set; }

        protected override bool IsSuccess()
        {
            return this.StatementLineMismatchCount == 0;
        }
    }
}

﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.ClosedUncollectableFinancePlansMonitor)]
    public class ClosedUncollectableFinancePlansMonitor : MonitorBase
    {
        [Datum(DatumEnum.ClosedUncollectableFinancePlansActual)]
        public int? ClosedUncollectableFinancePlansActual { get; set; }

        [Datum(DatumEnum.ClosedUncollectableFinancePlansExpected)]
        public int? ClosedUncollectableFinancePlansExpected { get; set; }
    }
}
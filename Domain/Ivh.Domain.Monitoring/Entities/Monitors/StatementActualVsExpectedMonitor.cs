﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.StatementActualVsExpectedMonitor)]
    public class StatementActualVsExpectedMonitor : MonitorBase
    {
        [Datum(DatumEnum.StatementCountExpected)]
        public decimal? StatementCountExpected { get; set; }

        [Datum(DatumEnum.StatementCountActual)]
        public decimal? StatementCountActual { get; set; }

        protected override bool IsSuccess()
        {
            return this.StatementCountExpected == this.StatementCountActual;
        }
    }
}

﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.PaymentAllocationMonitor)]
    public class PaymentAllocationMonitor : MonitorBase
    {
        [Datum(DatumEnum.PaymentAllocationSumVsPaymentAmountDifferences)]
        public int? PaymentAllocationSumVsPaymentAmountDifferences { get; set; }

        protected override bool IsSuccess()
        {
            return base.IsSuccess();
        }
    }
}

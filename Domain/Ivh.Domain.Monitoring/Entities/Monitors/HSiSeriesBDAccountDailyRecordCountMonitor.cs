﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.HSiSeriesBDAccountDailyRecordCountMonitor)]
    public class HSiSeriesBDAccountDailyRecordCountMonitor : MonitorBase
    {
        [Datum(DatumEnum.HSiSeriesBDAccountDailyRecordCount)]
        public int? HSiSeriesBDAccountDailyRecordCount { get; set; }
    }
}
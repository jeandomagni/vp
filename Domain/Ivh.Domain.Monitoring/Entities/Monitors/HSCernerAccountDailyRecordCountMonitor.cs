﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.HSCernerAccountDailyRecordCountMonitor)]
    public class HSCernerAccountDailyRecordCountMonitor : MonitorBase
    {
        [Datum(DatumEnum.HSCernerAccountDailyRecordCount)]
        public int? HSCernerAccountDailyRecordCount { get; set; }
    }
}
﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.UncollectableClosedActualVsExpectedMonitor)]
    public class UncollectableClosedActualVsExpectedMonitor : MonitorBase
    {
        [Datum(DatumEnum.ClosedUncollectableNonFPVisitsExpected)]
        public decimal? ClosedUncollectableNonFPVisitsExpected { get; set; }

        [Datum(DatumEnum.ClosedUncollectableVisitsActual)]
        public decimal? ClosedUncollectableVisitsActual { get; set; }

        [Datum(DatumEnum.ClosedUncollectableVisitsExpected)]
        public decimal? ClosedUncollectableVisitsExpected { get; set; }

        [Datum(DatumEnum.ClosedUncollectableFinancePlanVisitsExpected)]
        public decimal? ClosedUncollectableFinancePlanVisitsExpected { get; set; }

        [Datum(DatumEnum.ClosedUncollectableFinancePlanVisitsActual)]
        public decimal? ClosedUncollectableFinancePlanVisitsActual { get; set; }

        protected override bool IsSuccess()
        {
            return this.ClosedUncollectableNonFPVisitsExpected.Value == this.ClosedUncollectableVisitsActual.Value
                && this.ClosedUncollectableFinancePlanVisitsExpected.Value == this.ClosedUncollectableFinancePlanVisitsActual.Value;
        }
    }
}

﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.HSiSeriesARAccountDailyRecordCountMonitor)]
    public class HSiSeriesARAccountDailyRecordCountMonitor : MonitorBase
    {
        [Datum(DatumEnum.HSiSeriesARAccountDailyRecordCount)]
        public int? HSiSeriesARAccountDailyRecordCount { get; set; }
    }
}
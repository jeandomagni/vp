﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.InboundPublishActualVsExpectedMonitor)]
    public class InboundPublishActualVsExpectedMonitor : MonitorBase
    {
        [Datum(DatumEnum.HSGuarantorRecordCdiCount, 5)]
        public int? HSGuarantorRecordCdiCount { get; set; }

        [Datum(DatumEnum.VisitRecordCdiCount, 7)]
        public int? VisitRecordCdiCount { get; set; }

        [Datum(DatumEnum.VisitTransactionRecordCdiCount, 9)]
        public int? VisitTransactionRecordCdiCount { get; set; }

        [Datum(DatumEnum.HSTransactionSnapshotRecordCount, 11)]
        public int? HSTransactionSnapshotRecordCount { get; set; }

        [Datum(DatumEnum.HSAccountSnapshotRecordCount, 12)]
        public int? HSAccountSnapshotRecordCount { get; set; }

        [Datum(DatumEnum.HSGuarantorSnapshotRecordCount, 13)]
        public int? HSGuarantorSnapshotRecordCount { get; set; }

        [Datum(DatumEnum.HsCurrentBalanceVisitBalanceMismatchCount, 14)]
        public int? HsCurrentBalanceVisitBalanceMismatchCount { get; set; }
        protected override bool IsSuccess()
        {
            return base.IsSuccess();
        }
    }
}

﻿namespace Ivh.Domain.Monitoring.Entities.Monitors
{
    using Common.Base.Enums;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;

    [Monitor(MonitorEnum.QueueClosedUncollectablesActualMonitor)]
    public class QueueClosedUncollectablesActualMonitor : MonitorBase
    {
        [Datum(DatumEnum.ClosedUncollectableVisitsActual)]
        public decimal? ClosedUncollectableVisitsActual { get; set; }

        [Datum(DatumEnum.ClosedUncollectableFinancePlanVisitsActual)]
        public decimal? ClosedUncollectableFinancePlanVisitsActual { get; set; }

        public bool Equals(QueueClosedUncollectablesExpectedMonitor expected)
        {
            return expected != null
                && this.IsInitialized()
                && expected.IsInitialized()
                && this.ClosedUncollectableVisitsActual.Value == expected.ClosedUncollectableNonFPVisitsExpected.Value
                && expected.ClosedUncollectableFinancePlanVisitsExpected.Value == this.ClosedUncollectableFinancePlanVisitsActual.Value;
        }
    }
}

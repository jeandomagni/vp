﻿namespace Ivh.Domain.Monitoring.Entities
{
    public class PerformanceTimingSource
    {
        public virtual int PerformanceTimingSourceId { get; set; }
        public virtual string PerformanceTimingSourceName { get; set; }
    }
}
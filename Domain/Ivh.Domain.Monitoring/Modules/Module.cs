﻿namespace Ivh.Domain.Monitoring.Modules
{
    using Autofac;
    using Interfaces;
    using Services;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<MonitoringService>().As<IMonitoringService>();
            builder.RegisterType<PerformanceTimingService>().As<IPerformanceTimingService>();
        }
    }
}

﻿namespace Ivh.Domain.Monitoring.Services
{
    using System;
    using Base.Interfaces;
    using Base.Services;
    using Entities;
    using Interfaces;

    public class PerformanceTimingService : DomainService, IPerformanceTimingService
    {
        private readonly Lazy<IPerformanceTimingRepository> _performanceTimingRepository;
        private readonly Lazy<IPerformanceTimingSourceRepository> _performanceTimingSourceRepository;

        public PerformanceTimingService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IPerformanceTimingRepository> performanceTimingRepository,
            Lazy<IPerformanceTimingSourceRepository> performanceTimingSourceRepository) : base(serviceCommonService)
        {
            this._performanceTimingRepository = performanceTimingRepository;
            this._performanceTimingSourceRepository = performanceTimingSourceRepository;
        }

        public void LogControllerTiming(string url, string requestIdentifier, DateTime timeActionExecuting, DateTime timeActionExecuted, DateTime timeResultExecuted)
        {
            PerformanceTimingSource source = this.GetPerformanceTimingSource(url);

            Func<DateTime, long> getElapsedTicks = d => d.Ticks - timeActionExecuting.Ticks;

            PerformanceTiming timing = new PerformanceTiming
            {
                PerformanceTimingSource = source,
                RequestIdentifier = requestIdentifier,
                TimeStart = timeActionExecuting,
                TimeActionExecuted = getElapsedTicks(timeActionExecuted),
                TimeTotal = getElapsedTicks(timeResultExecuted),
            };

            this._performanceTimingRepository.Value.Insert(timing);
        }

        private PerformanceTimingSource GetPerformanceTimingSource(string url)
        {
            string lower = url.ToLower();
            int hashCode = lower.GetHashCode();

            PerformanceTimingSource source = this._performanceTimingSourceRepository.Value.GetById(hashCode);
            if (source != null)
            {
                return source;
            }

            source = new PerformanceTimingSource
            {
                PerformanceTimingSourceId = hashCode,
                PerformanceTimingSourceName = lower
            };

            this._performanceTimingSourceRepository.Value.InsertOrUpdate(source);

            return source;
        }
    }
}
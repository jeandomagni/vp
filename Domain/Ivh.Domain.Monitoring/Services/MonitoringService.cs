﻿namespace Ivh.Domain.Monitoring.Services
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;

    public class MonitoringService : DomainService, IMonitoringService
    {
        private readonly Lazy<IMonitoringRepository> _monitoringRepository;
        private readonly Lazy<IMonitorRepository> _monitorRepository;
        public MonitoringService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IMonitorRepository> monitorRepository,
            Lazy<IMonitoringRepository> monitoringRepository) : base(serviceCommonService)
        {
            this._monitorRepository = monitorRepository;
            this._monitoringRepository = monitoringRepository;
        }

        public IMonitor GetMonitor(MonitorEnum monitorEnum, DateTime? dateTimeBegin = null, DateTime? dateTimeEnd = null)
        {
            Type type = this.GetMonitorType(monitorEnum);
            IMonitor returnValue = (IMonitor)Activator.CreateInstance(type);
            Monitor monitor = this._monitorRepository.Value.GetById(monitorEnum);

            if (monitor == null)
            {
                throw new InvalidOperationException(string.Format("monitor {0} is null or not configured in the database", type.Name));
            }

            //get monitor parameters
            if (monitor.MonitorParameters.IsNotNullOrEmpty())
            {
                Action<IEnumerable<PropertyInfo>> setParams = (dp) =>
                {
                    foreach (PropertyInfo property in dp)
                    {
                        object[] attributes = property.GetCustomAttributes(typeof(ParameterAttribute), true);
                        if (attributes.Any())
                        {
                            ParameterAttribute parameterAttribute = (ParameterAttribute)attributes.First();
                            MonitorParameter monitorParameter = monitor.MonitorParameters.FirstOrDefault(x => x.MonitorParameterName.Equals(parameterAttribute.Parameter.ToString(), StringComparison.OrdinalIgnoreCase));
                            if (monitorParameter != null)
                            {
                                if (property.PropertyType == typeof(decimal?))
                                {
                                    property.SetValue(returnValue, new decimal?(monitorParameter.MonitorParameterValue));
                                }
                                else if (property.PropertyType == typeof(decimal))
                                {
                                    property.SetValue(returnValue, monitorParameter.MonitorParameterValue);
                                }
                                else if (property.PropertyType == typeof(int?))
                                {
                                    property.SetValue(returnValue, new int?(Convert.ToInt32(monitorParameter.MonitorParameterValue)));
                                }
                                else if (property.PropertyType == typeof(int))
                                {
                                    property.SetValue(returnValue, Convert.ToInt32(monitorParameter.MonitorParameterValue));
                                }
                            }
                        }
                    }
                };
                setParams(type.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(prop => Attribute.IsDefined(prop, typeof(ParameterAttribute))));
                setParams(type.GetProperties(BindingFlags.NonPublic | BindingFlags.Instance).Where(prop => Attribute.IsDefined(prop, typeof(ParameterAttribute))));
            }

            Action<IEnumerable<PropertyInfo>> setData = (dp) =>
            {
                foreach (PropertyInfo datumProperty in dp)
                {
                    object[] attributes = datumProperty.GetCustomAttributes(typeof(DatumAttribute), true);
                    if (attributes.Any())
                    {
                        DatumAttribute datumAttribute = (DatumAttribute)attributes.First();
                        if (datumProperty.PropertyType == typeof(decimal?))
                        {
                            decimal? value = this._monitoringRepository.Value.GetDatum<decimal?>(datumAttribute, dateTimeBegin, dateTimeEnd);
                            datumProperty.SetValue(returnValue, value);
                        }
                        else if (datumProperty.PropertyType == typeof(decimal))
                        {
                            decimal value = this._monitoringRepository.Value.GetDatum<decimal>(datumAttribute, dateTimeBegin, dateTimeEnd);
                            datumProperty.SetValue(returnValue, value);
                        }
                        else if (datumProperty.PropertyType == typeof(int?))
                        {
                            int? value = this._monitoringRepository.Value.GetDatum<int?>(datumAttribute, dateTimeBegin, dateTimeEnd);
                            datumProperty.SetValue(returnValue, value);
                        }
                        else if (datumProperty.PropertyType == typeof(int))
                        {
                            int value = this._monitoringRepository.Value.GetDatum<int>(datumAttribute, dateTimeBegin, dateTimeEnd);
                            datumProperty.SetValue(returnValue, value);
                        }
                    }
                }
            };
            setData(type.GetProperties(BindingFlags.NonPublic | BindingFlags.Instance).Where(prop => Attribute.IsDefined(prop, typeof(DatumAttribute))));
            setData(type.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(prop => Attribute.IsDefined(prop, typeof(DatumAttribute))));

            return returnValue;
        }

        private static readonly ConcurrentDictionary<MonitorEnum, Type> _monitorTypes = new ConcurrentDictionary<MonitorEnum, Type>();
        private static object _lock = new object();
        private Type GetMonitorType(MonitorEnum monitorEnum)
        {
            if (!_monitorTypes.ContainsKey(monitorEnum))
            {
                foreach (Type type in this.GetType().Assembly.GetTypes()
                    .Where(x => Attribute.IsDefined(x, typeof(MonitorAttribute))))
                {
                    object[] attributes = type.GetCustomAttributes(typeof(MonitorAttribute), true);
                    if (attributes.Any())
                    {
                        MonitorAttribute monitorAttribute = (MonitorAttribute)attributes.First();
                        _monitorTypes[monitorAttribute.Monitor] = type;
                    }
                }
            }

            return _monitorTypes[monitorEnum];
        }

        public T GetDatum<T>(DatumEnum datum, DateTime? dateTimeBegin = null, DateTime? dateTimeEnd = null)
        {
            return this._monitoringRepository.Value.GetDatum<T>(datum, dateTimeBegin ?? DateTime.UtcNow, dateTimeEnd ?? DateTime.UtcNow);
        }
    }
}

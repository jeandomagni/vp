﻿namespace Ivh.Domain.Monitoring.Mappings
{
    using Common.Base.Enums;
    using FluentNHibernate.Mapping;
    using Monitoring.Entities;

    public class MonitorMap : ClassMap<Monitor>
    {
        public MonitorMap()
        {
            this.Schema("monitor");
            this.Table("Monitor");
            this.Cache.ReadOnly();
            this.Id(x => x.MonitorId);
            this.Map(x => x.MonitorName);
            this.HasMany(x => x.MonitorParameters).KeyColumn("MonitorId");
        }
    }
}

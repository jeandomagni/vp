﻿namespace Ivh.Domain.Monitoring.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class PerformanceTimingSourceMap : ClassMap<PerformanceTimingSource>
    {
        public PerformanceTimingSourceMap()
        {
            this.Schema("qa");
            this.Table("PerformanceTimingSource");
            this.Id(x => x.PerformanceTimingSourceId).GeneratedBy.Assigned();
            this.Map(x => x.PerformanceTimingSourceName);
        }
    }
}
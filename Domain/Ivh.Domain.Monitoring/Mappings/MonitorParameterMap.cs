﻿namespace Ivh.Domain.Monitoring.Mappings
{
    using FluentNHibernate.Mapping;
    using Entities;

    public class MonitorParameterMap : ClassMap<MonitorParameter>
    {
        public MonitorParameterMap()
        {
            this.Schema("monitor");
            this.Table("MonitorParameter");
            this.Cache.ReadOnly();
            this.Id(x => x.MonitorParameterId);
            this.References<Monitor>(x => x.Monitor,"MonitorId");
            this.Map(x => x.MonitorParameterName);
            this.Map(x => x.MonitorParameterValue);
        }
    }
}

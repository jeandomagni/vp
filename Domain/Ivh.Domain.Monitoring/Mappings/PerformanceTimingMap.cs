﻿namespace Ivh.Domain.Monitoring.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class PerformanceTimingMap : ClassMap<PerformanceTiming>
    {
        public PerformanceTimingMap()
        {
            this.Schema("qa");
            this.Table("PerformanceTiming");
            this.Id(x => x.PerformanceTimingId);
            this.Map(x => x.RequestIdentifier);
            this.Map(x => x.TimeStart);
            this.Map(x => x.TimeActionExecuted);
            this.Map(x => x.TimeTotal);
            this.References(x => x.PerformanceTimingSource, "PerformanceTimingSourceId");
        }
    }
}
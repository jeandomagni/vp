﻿
namespace Ivh.Domain.HospitalData.Outbound.Enums
{
    /*
     For now we only have a single provider, but in the future we may have more depending on what
     type of file the client expects
    */
    public enum VpOutboundFileTypeProviderEnum
    {
        OutboundZipFileProvider = 1
    }
}

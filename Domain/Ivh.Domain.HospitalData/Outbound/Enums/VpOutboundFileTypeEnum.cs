﻿namespace Ivh.Domain.HospitalData.Outbound.Enums
{
    public enum VpOutboundFileTypeEnum
    {
        NonFinancePlanStatements = 1,
        FinancePlanStatements = 2,
        MailedContracts = 3,
        Letters = 4
    }
}
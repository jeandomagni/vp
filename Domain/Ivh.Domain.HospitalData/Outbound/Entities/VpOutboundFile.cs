﻿namespace Ivh.Domain.HospitalData.Outbound.Entities
{
    using System;

    public class VpOutboundFile
    {
        public virtual int VpOutboundFileId { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual string FileName { get; set; }
        public virtual string ExternalKey { get; set; }
        public virtual int VpOutboundFileTypeId { get; set; }
        public virtual int? OutboundLoadTrackerId { get; set; }
        public virtual int? SentFileTrackerId { get; set; }
    }
}
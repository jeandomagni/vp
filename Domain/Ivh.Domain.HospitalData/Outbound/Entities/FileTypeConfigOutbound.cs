﻿namespace Ivh.Domain.HospitalData.Outbound.Entities
{
    public class FileTypeConfigOutbound
    {
        public virtual int FileTypeConfigOutboundId { get; set; }
        public virtual int FileTypeID { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual string FileNamePattern { get; set; }
        public virtual string VpOutboundFileTypeIds { get; set; }
        public virtual string FileTypeProvider { get; set; }
        public virtual string Delimiter { get; set; }
        public virtual bool RemoveQuotes { get; set; }
        public virtual bool HeaderIncludeColumnNames { get; set; }
        public virtual bool BodyIncludeColumnNames { get; set; }
        public virtual bool FooterIncludeColumnNames { get; set; }
        public virtual string HeaderQuery { get; set; }
        public virtual string BodyQuery { get; set; }
        public virtual string FooterQuery { get; set; }
        public virtual bool SftpHoldForReview { get; set; }
        public virtual string SftpLeafFolder { get; set; }
        public virtual int LoadTypeId { get; set; }
        public virtual int SourceSql_FileTypeConfigOutboundId { get; set; }
        public virtual bool SupressEmptyFile { get; set; }
    }
}
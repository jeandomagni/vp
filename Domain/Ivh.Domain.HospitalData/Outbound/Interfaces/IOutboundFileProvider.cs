﻿
namespace Ivh.Domain.HospitalData.Outbound.Interfaces
{
    using System.Collections.Generic;

    public interface IOutboundFileProvider
    {
        bool GenerateOutboundFile(IList<string> fileGuids, string destinationDirectory, string fileName, bool includeManifest = false);
    }
}

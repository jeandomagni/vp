﻿namespace Ivh.Domain.HospitalData.Outbound.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IOutboundPaperFileService : IDomainService
    {
        void CreateOutboundFile(VpOutboundFile vpOutboundFile);
        void ExportFiles(string destinationDirectory, int loadTrackerId);
    }
}
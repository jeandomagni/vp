﻿namespace Ivh.Domain.HospitalData.Outbound.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IVpOutboundFileRepository : IRepository<VpOutboundFile>
    {
        List<VpOutboundFile> GetRecordsByType(int loadTrackerId, string vpOutboundFileTypeIds);
        void UpdateSentFileTrackerId(IList<VpOutboundFile> fileRecords, int fileTrackerFileTrackerId);
    }
}
﻿namespace Ivh.Domain.HospitalData.Outbound.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IFileTypeConfigOutboundRepository : IRepository<FileTypeConfigOutbound>
    {
        IList<FileTypeConfigOutbound> GetFileTypeConfigOutbounds();
    }
}
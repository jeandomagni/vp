﻿namespace Ivh.Domain.HospitalData.Outbound.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class FileTypeConfigOutboundMap : ClassMap<FileTypeConfigOutbound>
    {
        public FileTypeConfigOutboundMap()
        {
            this.ReadOnly();
            this.Schema(Schemas.Cdi.Etl);
            this.Table(nameof(FileTypeConfigOutbound));
            this.Id(x => x.FileTypeConfigOutboundId).Not.Nullable();

            this.Map(x => x.FileTypeID).Not.Nullable();
            this.Map(x => x.IsActive).Not.Nullable();
            this.Map(x => x.FileNamePattern).Not.Nullable();
            this.Map(x => x.FileTypeProvider).Nullable();
            this.Map(x => x.VpOutboundFileTypeIds).Nullable();
            this.Map(x => x.Delimiter).Not.Nullable();
            this.Map(x => x.RemoveQuotes).Not.Nullable();
            this.Map(x => x.HeaderIncludeColumnNames).Not.Nullable();
            this.Map(x => x.BodyIncludeColumnNames).Not.Nullable();
            this.Map(x => x.FooterIncludeColumnNames).Not.Nullable();
            this.Map(x => x.HeaderQuery).Nullable();
            this.Map(x => x.BodyQuery).Nullable();
            this.Map(x => x.FooterQuery).Nullable();
            this.Map(x => x.SftpHoldForReview).Not.Nullable();
            this.Map(x => x.SftpLeafFolder).Nullable();
            this.Map(x => x.LoadTypeId).Nullable();
            this.Map(x => x.SourceSql_FileTypeConfigOutboundId);
            this.Map(x => x.SupressEmptyFile).Not.Nullable();
        }
    }
}
﻿namespace Ivh.Domain.HospitalData.Outbound.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class VpOutboundFileMap : ClassMap<VpOutboundFile>
    {
        public VpOutboundFileMap()
        {
            this.Schema(Schemas.Cdi.Vp);
            this.Table(nameof(VpOutboundFile));
            this.Id(x => x.VpOutboundFileId).Not.Nullable();

            this.Map(x => x.InsertDate);
            this.Map(x => x.FileName);
            this.Map(x => x.ExternalKey);
            this.Map(x => x.VpOutboundFileTypeId);
            this.Map(x => x.OutboundLoadTrackerId).Nullable();
            this.Map(x => x.SentFileTrackerId).Nullable();
        }
    }
}
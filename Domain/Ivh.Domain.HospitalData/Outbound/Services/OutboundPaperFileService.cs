﻿namespace Ivh.Domain.HospitalData.Outbound.Services
{
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Utilities.Helpers;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;
    using Enums;
    using FileLoadTracking.Entities;
    using FileLoadTracking.Interfaces;
    using Interfaces;
    using NHibernate;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class OutboundPaperFileService : DomainService, IOutboundPaperFileService
    {
        private readonly Lazy<IFileTrackerService> _fileTrackerService;
        private readonly Lazy<IFileTypeConfigOutboundRepository> _fileTypeConfigOutboundRepository;
        private readonly Lazy<ILoadTrackerService> _loadTrackerService;
        private readonly Lazy<TimeZoneHelper> _timeZoneHelper;
        private readonly Lazy<IVpOutboundFileRepository> _vpOutboundFileRepository;
        private readonly Lazy<IOutboundFileProvider> _outboundZipFileProvider;
        private readonly ISession _session;

        public OutboundPaperFileService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IFileTrackerService> fileTrackerService,
            Lazy<IFileTypeConfigOutboundRepository> fileTypeConfigOutboundRepository,
            Lazy<ILoadTrackerService> loadTrackerService,
            ISessionContext<CdiEtl> sessionContext,
            Lazy<TimeZoneHelper> timeZoneHelper,
            Lazy<IVpOutboundFileRepository> vpOutboundFileRepository,
            Lazy<IOutboundFileProvider> outboundZipFileProvider
        ) : base(serviceCommonService)
        {
            this._fileTrackerService = fileTrackerService;
            this._fileTypeConfigOutboundRepository = fileTypeConfigOutboundRepository;
            this._loadTrackerService = loadTrackerService;
            this._timeZoneHelper = timeZoneHelper;
            this._vpOutboundFileRepository = vpOutboundFileRepository;
            this._session = sessionContext.Session;
            this._outboundZipFileProvider = outboundZipFileProvider;
        }

        public void CreateOutboundFile(VpOutboundFile vpOutboundFile)
        {
            this._vpOutboundFileRepository.Value.InsertOrUpdate(vpOutboundFile);
        }

        public void ExportFiles(string destinationDirectory, int loadTrackerId)
        {
            LoadTracker loadTracker = this._loadTrackerService.Value.GetActiveLoadTrackerById(loadTrackerId);

            IList<FileTypeConfigOutbound> fileTypeConfigOutbounds = this._fileTypeConfigOutboundRepository.Value.GetFileTypeConfigOutbounds();

            foreach (FileTypeConfigOutbound fileTypeConfigOutbound in fileTypeConfigOutbounds)
            { 
                DateTime datetime = this._timeZoneHelper.Value.UtcToClient(DateTime.UtcNow);
                string fileName = this._fileTrackerService.Value.GenerateFileNameWithDate(fileTypeConfigOutbound.FileNamePattern, datetime);
                FileTracker fileTracker = this._fileTrackerService.Value.StartOrContinueFileTracker(loadTracker, fileTypeConfigOutbound.FileTypeID, fileName, datetime);

                // get the guids for the fileType(s). 
                IList<VpOutboundFile> fileRecords = this._vpOutboundFileRepository.Value.GetRecordsByType(loadTrackerId, fileTypeConfigOutbound.VpOutboundFileTypeIds);
                IList<string> guids = fileRecords.Select(x => x.ExternalKey).ToList();

                bool fileCreationSucceeded = false;
                if (fileTypeConfigOutbound.SupressEmptyFile && guids.Count == 0)
                {
                    fileCreationSucceeded = true;
                }
                else
                {
                    //Determine which provider to use
                    VpOutboundFileTypeProviderEnum providerType;
                    if (Enum.TryParse(fileTypeConfigOutbound.FileTypeProvider, out providerType))
                    {
                        /*
                         For now we only have a single provider, but in the future we may have more depending on what
                         type of file the client expects
                        */
                        switch (providerType)
                        {
                            case VpOutboundFileTypeProviderEnum.OutboundZipFileProvider:
                                fileCreationSucceeded = this._outboundZipFileProvider.Value.GenerateOutboundFile(guids, destinationDirectory, fileName);
                                break;
                        }
                    }
                    else
                    {
                        //Throw exception, invalid FileTypeProvider
                        throw new Exception($"No outbound file provider found for FileTypeProvider: {fileTypeConfigOutbound.FileTypeProvider}");
                    }

                    this._vpOutboundFileRepository.Value.UpdateSentFileTrackerId(fileRecords, fileTracker.FileTrackerId);
                }

                this._fileTrackerService.Value.SetFileTrackerStatus(fileTracker, fileCreationSucceeded);

            }

        }
    }
}
﻿namespace Ivh.Domain.HospitalData.VpGuarantor.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.Matching.Dtos;
    using Entities;
    using Guarantor.Entities;
    using HsGuarantor.Entities;
    using HsGuarantor.Interfaces;
    using Interfaces;
    using Ivh.Application.HospitalData.Common.Responses;
    using Ivh.Application.User.Common.Dtos;
    using NHibernate;
    using IHsGuarantorService = Interfaces.IHsGuarantorService;

    public class HsGuarantorService : DomainService, IHsGuarantorService
    {
        private readonly Lazy<IGuarantorMatchingLogRepository> _guarantorMatchingLogRepository;
        private readonly Lazy<IHospitalGuarantorRepository> _hospitalGuarantorRepository;
        private readonly Lazy<IVpGuarantorHsMatchRepository> _vpGuarantorHsMatchRepository;
        private readonly Lazy<IHsGuarantorRepository> _hsGuarantorRepository;
        private readonly ISession _session;

        public HsGuarantorService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IHospitalGuarantorRepository> repository,
            Lazy<IGuarantorMatchingLogRepository> guarantorMatchingLogRepository,
            Lazy<IVpGuarantorHsMatchRepository> vpGuarantorHsMatchRepository,
            Lazy<IHsGuarantorRepository> hsGuarantorRepository,
            ISessionContext<CdiEtl> session) : base(serviceCommonService)
        {
            this._hospitalGuarantorRepository = repository;
            this._guarantorMatchingLogRepository = guarantorMatchingLogRepository;
            this._vpGuarantorHsMatchRepository = vpGuarantorHsMatchRepository;
            this._hsGuarantorRepository = hsGuarantorRepository;
            this._session = session.Session;
        }

        public GuarantorMatchResponse GetInitialMatchExpress(string registrationMatchString, string lastName, HttpContextDto context)
        {
            VpGuarantorMatchInfo vpGuarantorMatchInfo = new VpGuarantorMatchInfo
            {
                LastName = lastName,
                RegistrationMatchString = registrationMatchString
            };
            GuarantorMatchResponse guarantorMatchResponse = this._hospitalGuarantorRepository.Value.GetInitialMatchExpress(vpGuarantorMatchInfo);

            GuarantorMatchingLog guarantorMatchingLog = new GuarantorMatchingLog
            {
                LastName = vpGuarantorMatchInfo.LastName,
                LogDate = DateTime.UtcNow,
                RegistrationMatchString = vpGuarantorMatchInfo.RegistrationMatchString,
                UserAgent = (context != null && context.Request != null) ? context.Request.UserAgent : string.Empty,
                IpAddress = (context != null && context.Request != null) ? context.Request.UserHostAddress : string.Empty,
                MatchResult = (int)guarantorMatchResponse.GuarantorMatchResultEnum
            };

            int parsedssn4 = -1;
            if (int.TryParse(vpGuarantorMatchInfo.SSN4, out parsedssn4))
            {
                guarantorMatchingLog.SSN4 = parsedssn4;
            }
            if (!string.IsNullOrEmpty(vpGuarantorMatchInfo.PostalCodeShort))
            {
                guarantorMatchingLog.PostalCodeShort = vpGuarantorMatchInfo.PostalCodeShort;
            }
            if (vpGuarantorMatchInfo.DOB != DateTime.MinValue)
            {
                guarantorMatchingLog.DOB = vpGuarantorMatchInfo.DOB;
            }

            try
            {
                this._guarantorMatchingLogRepository.Value.InsertOrUpdate(guarantorMatchingLog);
            }
            catch (Exception)
            {
                { }
                throw;
            }


            return guarantorMatchResponse;
        }

        public GuarantorMatchResponse GetInitialMatch(string registrationMatchString, string lastName, DateTime dateOfBirth, string ssn4, string zip,DateTime? patientDateOfBirth, HttpContextDto context)
        {
            VpGuarantorMatchInfo vpGuarantorMatchInfo = new VpGuarantorMatchInfo
            {
                DOB = dateOfBirth,
                PatientDOB = patientDateOfBirth,
                LastName = lastName,
                PostalCodeShort = zip,
                SSN4 = ssn4,
                RegistrationMatchString = registrationMatchString
            };
            GuarantorMatchResponse guarantorMatchResponse = this._hospitalGuarantorRepository.Value.GetInitialMatch(vpGuarantorMatchInfo);
            if (guarantorMatchResponse.GuarantorMatchResultEnum == GuarantorMatchResultEnum.Matched && guarantorMatchResponse.MatchedHsGuarantorId.HasValue)
            {
                IList<VpGuarantorHsMatch> vpGuarantorHsMatches = this._vpGuarantorHsMatchRepository.Value.GetById(guarantorMatchResponse.MatchedHsGuarantorId.Value, vpGuarantorMatchInfo.VpGuarantorId);
                if (vpGuarantorHsMatches.IsNotNullOrEmpty())
                {
                    if (vpGuarantorHsMatches.OrderByDescending(x => x.VpGuarantorHsMatchId).First().HsGuarantorMatchStatus == HsGuarantorMatchStatusEnum.UnmatchedHard)
                    {
                        guarantorMatchResponse.GuarantorMatchResultEnum = GuarantorMatchResultEnum.NoMatchFound;
                        return guarantorMatchResponse;
                    }
                }
            }

            int parsedssn4 = -1;
            int.TryParse(vpGuarantorMatchInfo.SSN4, out parsedssn4);
            this._guarantorMatchingLogRepository.Value.InsertOrUpdate(new GuarantorMatchingLog
            {
                DOB = dateOfBirth,
                LastName = vpGuarantorMatchInfo.LastName,
                LogDate = DateTime.UtcNow,
                PostalCodeShort = vpGuarantorMatchInfo.PostalCodeShort,
                RegistrationMatchString = vpGuarantorMatchInfo.RegistrationMatchString,
                SSN4 = parsedssn4,
                UserAgent = (context != null && context.Request != null) ? context.Request.UserAgent : string.Empty,
                IpAddress = (context != null && context.Request != null) ? context.Request.UserHostAddress : string.Empty,
                MatchResult = (int)guarantorMatchResponse.GuarantorMatchResultEnum
            });

            return guarantorMatchResponse;
        }

        public void UpdateMatches(Guarantor guarantor)
        {

            VpGuarantorMatchInfo vpGuarantor = new VpGuarantorMatchInfo
            {
                VpGuarantorId = guarantor.VpGuarantorId,
                LastName = guarantor.User.LastName,
                PostalCodeShort = guarantor.User.CurrentPhysicalAddress?.PostalCode
            };
            this._hospitalGuarantorRepository.Value.UpdateMatch(vpGuarantor);
        }

        public void GetNewMatches(int vpGuarantorId)
        {
            this._hospitalGuarantorRepository.Value.GetNewMatches(vpGuarantorId);
        }

        public int? AddInitialMatch(Guarantor guarantor, string registrationMatchString, DateTime? patientDateOfBirth, HttpContextDto context)
        {
            if (guarantor.User.DateOfBirth != null)
            {
                GuarantorMatchResponse match = this.GetInitialMatch(
                    registrationMatchString, 
                    guarantor.User.LastName, 
                    guarantor.User.DateOfBirth.Value, 
                    guarantor.User.SSN4?.ToString(), 
                    guarantor.User.CurrentPhysicalAddress.PostalCode, 
                    patientDateOfBirth, 
                    context);

                if (match.GuarantorMatchResultEnum == GuarantorMatchResultEnum.Matched && match.MatchedHsGuarantorId.HasValue)
                {
                    //check to see if this match is not allowed for rematch
                    IList<VpGuarantorHsMatch> vpGuarantorHsMatches = this._vpGuarantorHsMatchRepository.Value.GetById(match.MatchedHsGuarantorId.Value, guarantor.VpGuarantorId);
                    if (vpGuarantorHsMatches.IsNotNullOrEmpty())
                    {
                        if (vpGuarantorHsMatches.OrderByDescending(x => x.VpGuarantorHsMatchId).First().HsGuarantorMatchStatus == HsGuarantorMatchStatusEnum.UnmatchedHard)
                        {
                            return null;
                        }
                    }

                    VpGuarantorMatchInfo vpGuarantorMatchInfo = new VpGuarantorMatchInfo
                    {
                        DOB = guarantor.User.DateOfBirth.Value,
                        PatientDOB = patientDateOfBirth,
                        RegistrationMatchString = registrationMatchString,
                        LastName = guarantor.User.LastName,
                        PostalCodeShort = guarantor.User.CurrentPhysicalAddress?.PostalCode,
                        SSN4 = guarantor.User.SSN4?.ToString("D4"),
                        VpGuarantorId = guarantor.VpGuarantorId,
                        HsGuarantorId = match.MatchedHsGuarantorId.Value
                    };

                    if(guarantor.IsOfflineGuarantor())
                    {
                        this._hospitalGuarantorRepository.Value.AddInitialMatchVpcc(vpGuarantorMatchInfo);
                    }
                    else
                    {
                        this._hospitalGuarantorRepository.Value.AddInitialMatch(vpGuarantorMatchInfo);
                    }

                    return match.MatchedHsGuarantorId.Value;
                }
                throw new Exception("Couldn't add the initial match because there wasn't a match!");
            }

            return null;
        }

        public void CancelGuarantor(Guarantor guarantor)
        {
            VpGuarantorMatchInfo matchInfo = this._hospitalGuarantorRepository.Value.GetById(guarantor.VpGuarantorId);
            if (matchInfo != null)
            {
                matchInfo.CancelledDate = DateTime.UtcNow;
                this._hospitalGuarantorRepository.Value.InsertOrUpdate(matchInfo);
            }
        }

        public void ReactivateGuarantor(Guarantor guarantor)
        {
            VpGuarantorMatchInfo matchInfo = this._hospitalGuarantorRepository.Value.GetById(guarantor.VpGuarantorId);
            if (matchInfo != null)
            {
                matchInfo.CancelledDate = null;
                this._hospitalGuarantorRepository.Value.InsertOrUpdate(matchInfo);
            }
        }

        public bool CredentialsMatchCanceledAccount(string lastName, DateTime dob, string ssn4, string postalCodeShort)
        {
            return this._hospitalGuarantorRepository.Value.CredentialsMatchCanceledAccount(lastName, dob, ssn4, postalCodeShort);
        }

        public IList<int> AddMatches(IEnumerable<GuarantorMatchResult> matchesToAdd, int vpGuarantorId)
        {
            IList<VpGuarantorHsMatch> newMatches = new List<VpGuarantorHsMatch>();
            IList<GuarantorMatchResult> remainingMatchesToAdd = matchesToAdd.Where(x => x.GuarantorMatchResultEnum == GuarantorMatchResultEnum.Matched).ToList();
            DateTime matchedOn = DateTime.UtcNow;
            IList<VpGuarantorHsMatch> existingMatches = this._vpGuarantorHsMatchRepository.Value.GetHsGuarantorVpMatches(vpGuarantorId);

            void AddVpGuarantorHsMatch(HsGuarantor guarantor, MatchOptionEnum matchOption)
            {
                if (existingMatches?.Select(x => x.HsGuarantorId).Contains(guarantor.HsGuarantorId) ?? false)
                {
                    return;
                }

                VpGuarantorHsMatch match = new VpGuarantorHsMatch
                {
                    VpGuarantorId = vpGuarantorId,
                    HsGuarantorId = guarantor.HsGuarantorId,
                    MatchedOn = matchedOn,
                    IsActive = true,
                    LastName = guarantor.LastName,
                    DOB = guarantor.DOB,
                    SSN = guarantor.SSN,
                    SSN4 = guarantor.SSN4.ToNullableInt32(),
                    SentToHsOn = null,
                    HsConfirmedOn = null,
                    PostalCode = guarantor.PostalCode.Substring(0,5).ToNullableInt32(),
                    HsGuarantorMatchStatus = HsGuarantorMatchStatusEnum.Matched,
                    MatchOption = matchOption,
                };

                this._vpGuarantorHsMatchRepository.Value.InsertOrUpdate(match);

                newMatches.Add(match);
            }

            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                // add match info and hsg match for initial match
                GuarantorMatchResult initialMatch = remainingMatchesToAdd.FirstOrDefault(x => x.MatchType == PatternUseEnum.Initial) ?? remainingMatchesToAdd.First();

                if (initialMatch != null)
                {
                    HsGuarantor guarantor = this._hsGuarantorRepository.Value.GetHsGuarantor(initialMatch.MatchedHsGuarantorBillingSystemId, initialMatch.MatchedHsGuarantorSourceSystemKey);
                    VpGuarantorMatchInfo matchInfo = this._hospitalGuarantorRepository.Value.GetById(vpGuarantorId);

                    if (matchInfo == null)
                    {
                        matchInfo = new VpGuarantorMatchInfo
                        {
                            VpGuarantorId = vpGuarantorId,
                            HsBillingSystemID = initialMatch.MatchedHsGuarantorBillingSystemId,
                            RegistrationMatchString = initialMatch.MatchedHsGuarantorSourceSystemKey,
                            FirstName = guarantor.FirstName,
                            LastName = guarantor.LastName,
                            SSN = guarantor.SSN,
                            SSN4 = guarantor.SSN4,
                            DOB = guarantor.DOB ?? DateTime.MinValue,
                            AddressLine1 = guarantor.AddressLine1,
                            City = guarantor.City,
                            StateProvince = guarantor.StateProvince,
                            PostalCodeShort = guarantor.PostalCode.Substring(0, 5),
                            HsGuarantorId = guarantor.HsGuarantorId,
                        };

                        // add match info
                        this._hospitalGuarantorRepository.Value.InsertOrUpdate(matchInfo);
                    }

                    // add hsg match
                    AddVpGuarantorHsMatch(guarantor, initialMatch.MatchOption);

                    remainingMatchesToAdd = remainingMatchesToAdd.Where(x => !(x.MatchedHsGuarantorSourceSystemKey.Equals(initialMatch.MatchedHsGuarantorSourceSystemKey)
                                                                               && x.MatchedHsGuarantorBillingSystemId == initialMatch.MatchedHsGuarantorBillingSystemId)).ToList();
                }

                // add hsg matches for the rest of the matches
                foreach (GuarantorMatchResult guarantorMatchResult in remainingMatchesToAdd)
                {
                    HsGuarantor guarantor = this._hsGuarantorRepository.Value.GetHsGuarantor(guarantorMatchResult.MatchedHsGuarantorBillingSystemId, guarantorMatchResult.MatchedHsGuarantorSourceSystemKey);
                    AddVpGuarantorHsMatch(guarantor, guarantorMatchResult.MatchOption);
                }

                unitOfWork.Commit();
            }

            return newMatches.Select(x => x.HsGuarantorId).ToList();
        }
    }
}
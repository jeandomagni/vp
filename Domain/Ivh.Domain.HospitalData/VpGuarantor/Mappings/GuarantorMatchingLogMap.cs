﻿namespace Ivh.Domain.HospitalData.VpGuarantor.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class GuarantorMatchingLogMap : ClassMap<GuarantorMatchingLog>
    {
        public GuarantorMatchingLogMap()
        {
            this.Schema("log");
            this.Table("GuarantorMatching");
            this.Id(x => x.GuarantorMatchingId);
            this.Map(x => x.LogDate);
            this.Map(x => x.HsBillingSystemId).Nullable();
            this.Map(x => x.RegistrationMatchString).Not.Nullable();
            this.Map(x => x.LastName).Length(250).Not.Nullable();
            this.Map(x => x.SSN4).Nullable();
            this.Map(x => x.DOB).Nullable();
            this.Map(x => x.PostalCodeShort).Nullable();
            this.Map(x => x.IpAddress).Length(50);
            this.Map(x => x.UserAgent);
            this.Map(x => x.MatchResult);
        }
    }
}
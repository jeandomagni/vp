namespace Ivh.Domain.HospitalData.VpGuarantor.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class VpGuarantorMatchInfoMap : ClassMap<VpGuarantorMatchInfo>
    {
        public VpGuarantorMatchInfoMap()
        {
            this.Schema("Vp");
            this.Table("VpGuarantorMatchInfo");
            this.Id(x => x.VpGuarantorId).Column("VpGuarantorID").GeneratedBy.Assigned();
            this.Map(x => x.FirstName);
            this.Map(x => x.LastName);
            this.Map(x => x.AddressLine1);
            this.Map(x => x.City);
            this.Map(x => x.StateProvince);
            this.Map(x => x.SSN);
            this.Map(x => x.SSN4);
            this.Map(x => x.DOB).Not.Nullable();
            this.Map(x => x.PostalCodeShort).Not.Nullable();
            this.Map(x => x.HsBillingSystemID).Nullable().Column("RegistrationHsBillingSystemID");
            this.Map(x => x.RegistrationMatchString).Not.Nullable();
            this.Map(x => x.CancelledDate);
            this.Map(x => x.PatientDOB);
        }
    }
}
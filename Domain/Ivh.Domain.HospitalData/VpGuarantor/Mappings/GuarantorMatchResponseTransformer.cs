﻿namespace Ivh.Domain.HospitalData.VpGuarantor.Mappings
{
    using System;
    using System.Collections;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Ivh.Application.HospitalData.Common.Responses;
    using VpGuarantor.Entities;

    public class GuarantorMatchResponseTransformer : NHibernate.Transform.IResultTransformer
    {
        public object TransformTuple(object[] tuple, string[] aliases)
        {
            var model = new GuarantorMatchResponse();
            for (int i = 0; i < aliases.Length; i++)
            {
                var columnName = aliases[i];
                var value = tuple[i];
                switch (columnName)
                {
                    case "GuarantorMatchResult":
                        GuarantorMatchResultEnum myResultEnum;
                        if (!(Enum.TryParse(value.ToString(), out myResultEnum)))
                        {
                            myResultEnum = GuarantorMatchResultEnum.NoMatchFound;
                        }
                        model.GuarantorMatchResultEnum = myResultEnum;
                        break;
                    case "MatchedHsGuarantorId":
                        model.MatchedHsGuarantorId = (int?)value;
                        break;
                }
            }
            return model;
        }

        public IList TransformList(IList collection)
        {
            return collection;
        }
    }
}
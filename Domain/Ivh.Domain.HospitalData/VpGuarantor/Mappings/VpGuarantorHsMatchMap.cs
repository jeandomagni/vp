namespace Ivh.Domain.HospitalData.VpGuarantor.Mappings
{
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class VpGuarantorHsMatchMap : ClassMap<VpGuarantorHsMatch>
    {
        public VpGuarantorHsMatchMap()
        {
            this.Schema("Vp");
            this.Table("VpGuarantorHsMatch");
            this.Id(x => x.VpGuarantorHsMatchId);
            this.Map(x => x.HsGuarantorId, "HsGuarantorID").Not.Nullable();
            this.Map(x => x.VpGuarantorId, "VpGuarantorID").Not.Nullable();
            this.Map(x => x.MatchedOn).Not.Nullable();
            this.Map(x => x.SentToHsOn).Nullable();
            this.Map(x => x.HsConfirmedOn).Nullable();
            this.Map(x => x.IsActive, "isActive").Not.Nullable();
            this.Map(x => x.LastName).Nullable();
            this.Map(x => x.DOB).Nullable();
            this.Map(x => x.PostalCode).Nullable();

            this.Map(x => x.HsGuarantorMatchStatus, "HsGuarantorMatchStatusId")
                .CustomType<HsGuarantorMatchStatusEnum>()
                .Not.Nullable();

            this.Map(x => x.UnmatchedOn).Nullable();
            this.Map(x => x.UnmatchedSentToHsOn).Nullable();
            this.Map(x => x.UnmatchedHsConfirmedOn).Nullable();
            this.Map(x => x.OutboundLoadTrackerId).Nullable();
            this.Map(x => x.MatchOutboundLoadTrackerId).Nullable();
            this.Map(x => x.MatchOption).CustomType<MatchOptionEnum?>().Column("MatchOptionId").Nullable();

            this.Map(x => x.SSN);
            this.Map(x => x.SSN4);

        }
    }
}
﻿namespace Ivh.Domain.HospitalData.VpGuarantor.Entities
{
    using System;
    using Common.Base.Enums;

    public class VpGuarantorMatchInfo
    {
        public virtual int VpGuarantorId { get; set; }

        public virtual int? HsBillingSystemID { get; set; }

        public virtual string RegistrationMatchString { get; set; }

        public virtual string FirstName { get; set; }

        public virtual string LastName { get; set; }

        public virtual string AddressLine1 { get; set; }

        public virtual string City { get; set; }

        public virtual string StateProvince { get; set; }

        public virtual string SSN { get; set; }

        public virtual string SSN4 { get; set; }

        public virtual DateTime DOB { get; set; }

        public virtual DateTime? PatientDOB { get; set; }

        public virtual string PostalCodeShort { get; set; }

        public virtual int HsGuarantorId { get; set; }

        public virtual DateTime? CancelledDate { get; set; }
    }
}

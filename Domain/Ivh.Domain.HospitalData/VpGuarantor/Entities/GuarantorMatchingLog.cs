﻿namespace Ivh.Domain.HospitalData.VpGuarantor.Entities
{
    using System;
    public class GuarantorMatchingLog
    {
        public virtual int GuarantorMatchingId { get; set; }
        public virtual DateTime LogDate { get; set; }
        public virtual int? HsBillingSystemId { get; set; }
        public virtual string RegistrationMatchString { get; set; }
        public virtual string LastName { get; set; }
        public virtual int? SSN4 { get; set; }
        public virtual DateTime? DOB { get; set; }
        public virtual string PostalCodeShort { get; set; }
        public virtual string IpAddress { get; set; }
        public virtual string UserAgent { get; set; }
        public virtual int MatchResult { get; set; }
    }
}

/*	GuarantorMatchingId INT IDENTITY(1,1) NOT NULL,
	LogDate DATETIMEOFFSET(2) NOT NULL,
	HsBillingSystemId int NOT NULL,
	RegistrationMatchString NVARCHAR(60) NOT NULL,
	LastName NVARCHAR(250) NOT NULL,
	SSN4 INT NOT NULL,
	DOB DATE NOT NULL,
	PostalCodeShort NVARCHAR(10) NOT NULL,
	IpAddress NVARCHAR(50) NULL,
	UserAgent NVARCHAR(Max) NULL,*/
﻿namespace Ivh.Domain.HospitalData.VpGuarantor.Entities
{
    using System;
    using Common.VisitPay.Enums;

    public class VpGuarantorHsMatch
    {
        public VpGuarantorHsMatch()
        {
        }

        public virtual int VpGuarantorHsMatchId { get; set; }
        public virtual int VpGuarantorId { get; set; }
        public virtual int HsGuarantorId { get; set; }
        public virtual DateTime MatchedOn { get; set; }
        public virtual DateTime? SentToHsOn { get; set; }
        public virtual DateTime? HsConfirmedOn { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual string LastName { get; set; }
        public virtual DateTime? DOB { get; set; }
        public virtual string SSN { get; set; }
        public virtual int? SSN4 { get; set; }
        public virtual int? PostalCode { get; set; }
        public virtual HsGuarantorMatchStatusEnum HsGuarantorMatchStatus { get; set; }
        public virtual DateTime? UnmatchedOn { get; set; }
        public virtual DateTime? UnmatchedSentToHsOn { get; set; }
        public virtual DateTime? UnmatchedHsConfirmedOn { get; set; }
        public virtual int? OutboundLoadTrackerId { get; set; }
        public virtual int? MatchOutboundLoadTrackerId { get; set; }
        public virtual MatchOptionEnum? MatchOption { get; set; }

        public override bool Equals(object obj)
        {
            VpGuarantorHsMatch t = obj as VpGuarantorHsMatch;
            if (t == null)
            {
                return false;
            }

            if (this.VpGuarantorId == t.VpGuarantorId && this.HsGuarantorId == t.HsGuarantorId)
            {
                return true;
            }

            return false;
        }
        public override int GetHashCode()
        {
            return (this.VpGuarantorId + "|" + this.HsGuarantorId).GetHashCode();
        }

        public override string ToString()
        {
            return (this.VpGuarantorId + "|" + this.HsGuarantorId);
        }
    }
}
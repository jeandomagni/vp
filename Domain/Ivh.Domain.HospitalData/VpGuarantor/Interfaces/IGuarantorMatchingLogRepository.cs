﻿namespace Ivh.Domain.HospitalData.VpGuarantor.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IGuarantorMatchingLogRepository : IRepository<GuarantorMatchingLog>
    {
    }
}
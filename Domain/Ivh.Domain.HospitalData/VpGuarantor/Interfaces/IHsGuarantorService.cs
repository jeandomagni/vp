﻿namespace Ivh.Domain.HospitalData.VpGuarantor.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.Matching.Dtos;
    using Guarantor.Entities;
    using Ivh.Application.HospitalData.Common.Responses;
    using Ivh.Application.User.Common.Dtos;

    public interface IHsGuarantorService : IDomainService
    {
        GuarantorMatchResponse GetInitialMatchExpress(string registrationMatchString, string lastName, HttpContextDto context);
        GuarantorMatchResponse GetInitialMatch(string registrationMatchString, string lastName, DateTime dateOfBirth, string ssn4, string zip,DateTime? patientDateOfBirth, HttpContextDto context);
        int? AddInitialMatch(Guarantor guarantor, string registrationMatchString, DateTime? patientDateOfBirth, HttpContextDto context);
        void UpdateMatches(Guarantor guarantor);
        void GetNewMatches(int vpGuarantorId);
        void CancelGuarantor(Guarantor guarantor);
        void ReactivateGuarantor(Guarantor guarantor);
        bool CredentialsMatchCanceledAccount(string lastName, DateTime dob, string ssn4, string postalCodeShort);
        IList<int> AddMatches(IEnumerable<GuarantorMatchResult> matchesToAdd, int vpGuarantorId);
    }
}
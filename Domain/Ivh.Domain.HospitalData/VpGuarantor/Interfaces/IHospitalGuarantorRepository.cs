﻿namespace Ivh.Domain.HospitalData.VpGuarantor.Interfaces
{
    using System;
    using Common.Base.Interfaces;
    using Ivh.Application.HospitalData.Common.Responses;
    using Entities;

    public interface IHospitalGuarantorRepository : IRepository<VpGuarantorMatchInfo>
    {
        GuarantorMatchResponse GetInitialMatchExpress(VpGuarantorMatchInfo vpGuarantorMatchInfo);
        GuarantorMatchResponse GetInitialMatch(VpGuarantorMatchInfo vpGuarantorMatchInfo);
        void AddInitialMatch(VpGuarantorMatchInfo vpGuarantorMatchInfo);
        void AddInitialMatchVpcc(VpGuarantorMatchInfo vpGuarantorMatchInfo);
        void UpdateMatch(VpGuarantorMatchInfo vpGuarantorMatchInfo);
        bool CredentialsMatchCanceledAccount(string lastName, DateTime dob, string ssn4, string postalCodeShort);
        void GetNewMatches(int vpGuarantorId);
    }
}
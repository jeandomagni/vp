﻿namespace Ivh.Domain.HospitalData.Analytics.Interfaces
{
    using System.Collections.Generic;
    using Entities;

    public interface IAnalyticReportService
    {
        IList<AnalyticReportClassification> GetAnalyticReportClassifications();
        IList<AnalyticReport> GetAnalyticReports();
    }
}
﻿namespace Ivh.Domain.HospitalData.Analytics.Interfaces

{
    using Common.Base.Interfaces;
    using Entities;

    public interface IAnalyticReportRepository : IRepository<AnalyticReport> { }

}

﻿namespace Ivh.Domain.HospitalData.Analytics.Entities
{
    public class AnalyticReport
    {
        public virtual int AnalyticReportId { get; set; }
        public virtual string ReportName { get; set; }
        public virtual string DisplayName { get; set; }
        public virtual AnalyticReportClassification AnalyticReportClassification { get; set; }
        public virtual bool Visible { get; set; }
    }
}

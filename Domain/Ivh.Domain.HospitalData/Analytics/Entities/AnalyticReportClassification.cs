﻿namespace Ivh.Domain.HospitalData.Analytics.Entities
{
    public class AnalyticReportClassification
    {
        public virtual int AnalyticReportClassificationId { get; set; }
        public virtual string ClassificationName { get; set; }
    }
}

﻿namespace Ivh.Domain.HospitalData.Analytics.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class AnalyticReportMap : ClassMap<AnalyticReport>
    {
        public AnalyticReportMap()
        {
            this.Schema("analytics");
            this.Table("AnalyticReport");
            this.Id(x => x.AnalyticReportId).Not.Nullable();
            this.Map(x => x.ReportName).Not.Nullable();
            this.Map(x => x.DisplayName).Nullable();
            this.Map(x => x.Visible).Nullable();
            this.References(x => x.AnalyticReportClassification).Column("AnalyticReportClassificationId")
                .Cascade.All();
        }
    }
}

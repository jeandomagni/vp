﻿namespace Ivh.Domain.HospitalData.Analytics.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class AnalyticReportClassificationMap : ClassMap<AnalyticReportClassification>
    {
        public AnalyticReportClassificationMap()
        {
            this.Schema("analytics");
            this.Table("AnalyticReportClassification");
            this.Id(x => x.AnalyticReportClassificationId).Not.Nullable();
            this.Map(x => x.ClassificationName).Not.Nullable();
        }
    }
}

﻿namespace Ivh.Domain.HospitalData.Analytics.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Entities;
    using Interfaces;
    using Base.Interfaces;

    public class AnalyticReportService : IAnalyticReportService
    {
        private readonly Lazy<IAnalyticReportRepository> _analyticReportRepository;
        private readonly Lazy<IAnalyticReportClassificationRepository> _analyticReportClassificationRepository;

        public AnalyticReportService(Lazy<IAnalyticReportRepository> analyticReportRepository,
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IAnalyticReportClassificationRepository> analyticReportClassificationRepository)
        {
            this._analyticReportClassificationRepository = analyticReportClassificationRepository;
            this._analyticReportRepository = analyticReportRepository;
        }

        public IList<AnalyticReportClassification> GetAnalyticReportClassifications()
        {
            return this._analyticReportClassificationRepository.Value.GetQueryable().ToList();
        }

        public IList<AnalyticReport> GetAnalyticReports()
        {
            List<AnalyticReport> visibleReports = this._analyticReportRepository.Value.GetQueryable().Where(x => x.Visible).ToList();

            return visibleReports;
        }
    }
}
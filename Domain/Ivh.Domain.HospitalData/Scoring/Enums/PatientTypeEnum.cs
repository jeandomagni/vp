﻿namespace Ivh.Domain.HospitalData.Scoring.Enums
{
    public enum PatientTypeEnum
    {
        Unknown = 1,
        Inpatient = 2,
        Outpatient = 3,
        Emergency = 4
    }
}
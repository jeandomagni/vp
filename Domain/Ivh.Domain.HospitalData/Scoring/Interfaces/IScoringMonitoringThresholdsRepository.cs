﻿namespace Ivh.Domain.HospitalData.Scoring.Interfaces
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IScoringMonitoringThresholdsRepository
    {
        ScoringMonitoringThresholds GetScoringMonitoringThresholds(ScoringMonitoringTypeEnum scoringMonitoringTypeEnum);
    }
}
﻿namespace Ivh.Domain.HospitalData.Scoring.Interfaces
{
    using System.Threading.Tasks;
    using Entities;

    public interface IThirdPartyDataProvider
    {
        Task<ThirdPartyData> CallThirdPartyDataService(ThirdPartyRequestInput requestInput, string url);
    }
}

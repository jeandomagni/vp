﻿namespace Ivh.Domain.HospitalData.Scoring.Interfaces
{
    using System.Collections.Generic;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using Entities;

    public interface IThirdPartyDataService
    {
        ScoringThirdPartyDto ExtractThirdParty(int hsGuarantorId, bool required, int scoringBatchId, ScoringThirdPartyDatasetEnum dataSet, ScoringThirdPartyDataSetTypeEnum dataSetType);
        ScoringThirdPartyDto ExtractThirdParty(GuarantorBatch guarantorBatch, bool required, ScoringThirdPartyDatasetEnum dataSet, ScoringThirdPartyDataSetTypeEnum dataSetType, bool requireThirdPartyExtract);
        IList<int> GetValidPcThirdPartyData(int thirdPartyExpireDays);
    }
}

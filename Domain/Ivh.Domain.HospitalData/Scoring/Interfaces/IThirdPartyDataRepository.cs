﻿namespace Ivh.Domain.HospitalData.Scoring.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IThirdPartyDataRepository : IRepository<ThirdPartyData>
    {
        IList<ThirdPartyDataMonitoring> GetThirdPartyDataByDate(DateTime date);
        IList<ThirdPartyDataMonitoring> GetThirdPartyDataByDates(DateTime minDate, DateTime maxDate);
        IList<int> GetThirdPartyDataSet(int thirdPartyExpireDays, params ScoringThirdPartyDatasetEnum[] datasets);
        ThirdPartyData GetThirdPartyData(int hsGuarantorId, ScoringThirdPartyDataSetTypeEnum dataSetType, int refreshAge);
        ThirdPartyData GetThirdPartyDataByRequestInput(ThirdPartyRequestInput thirdPartyRequestInput, ScoringThirdPartyDataSetTypeEnum dataSetType, int refreshAge);
        void InsertStateless(ThirdPartyData thirdPartyData);
    }
}

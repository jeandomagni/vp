﻿namespace Ivh.Domain.HospitalData.Scoring.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IGuarantorBatchRepository : IRepository<GuarantorBatch>
    {
        GuarantorBatch GetByGuarantorIdScoringBatchId(int hsGuarantorId, int scoringBatchId);
        IList<int> GetPresumptiveCharityGuarantorBatches(int thirdPartyExpireDays, bool evaluateVisitsWithoutPtpScores);
    }
}
﻿namespace Ivh.Domain.HospitalData.Scoring.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IScoreRepository : IRepository<Score>
    {
        IList<PtpScore> GetPtpScore(Dictionary<string, List<int>> hsGuarantors);
    }
}

﻿namespace Ivh.Domain.HospitalData.Scoring.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IMatchedGuarantorsVisitBatchRepository : IRepository<MatchedGuarantorsVisitBatch>
    {
        IList<MatchedGuarantorsVisitBatch> GetMatchedGuarantors(int scoringBatchId);
        void RunDailyBatchLoadSp(int batchId, decimal minimumHsCurrentBalanceToScore, decimal minimumGuarantorBatchBalanceToScore, decimal minimumGuarantorBalanceToScore);
        void RunRetroBatchLoadSp(int batchId, DateTime dateToScore, decimal minimumHsCurrentBalanceToScore, decimal minimumGuarantorBatchBalanceToScore, decimal minimumGuarantorBalanceToScore);
        void RunDailyGuarantorMatching(bool useSskForGuarantorMatching);
        void RunGuarantorMatchingForRetroScore(bool useSskForGuarantorMatching);
        string CallScoringServiceApi(object scoringServiceInput, string baseUrl, string functionName, string authorizationCode, string appId, string appKey, string requestingAppName);
        void UpdateMatchedGuarantorsVisitBatchStatus(int scoringBatchId, ScoringStatusEnum scoringStatusEnum, IEnumerable<int> guarantorIds);
        void UpdateMatchedGuarantorsVisitBatchStatus(int parentHsGuarantorId, IList<VisitBatch> vistBatches, ScoringStatusEnum scoringStatusEnum);
    }
}

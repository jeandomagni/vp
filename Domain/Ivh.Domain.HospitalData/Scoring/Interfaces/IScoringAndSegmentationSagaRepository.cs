﻿namespace Ivh.Domain.HospitalData.Scoring.Interfaces
{
    using System;
    using Common.Base.Interfaces;
    using Entities;

    public interface IScoringAndSegmentationSagaRepository : IRepository<ScoringAndSegmentationSaga>
    {
        ScoringAndSegmentationSaga GetScoringAndSegmentationSaga(Guid correlationId);
    }
}

﻿namespace Ivh.Domain.HospitalData.Scoring.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IScoringMatchedGuarantorScoreHistoryRepository : IRepository<MatchedGuarantorScoreHistory>
    {
        void GenerateMatchedGuarantorScoreHistory(int scoringBatchId);
    }
}
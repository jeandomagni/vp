﻿namespace Ivh.Domain.HospitalData.Scoring.Interfaces
{
    using Entities;

    public interface IScoringStatisticalMonitoringService
    {
        ScoringStatisticalMonitoring RunDailyScoringStatisticalMonitoring(int minimumBatchSize);
        ScoringStatisticalMonitoring RunWeeklyScoringStatisticalMonitoring();
    }
}
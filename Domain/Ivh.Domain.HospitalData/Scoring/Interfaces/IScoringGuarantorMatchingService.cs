﻿namespace Ivh.Domain.HospitalData.Scoring.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Common.VisitPay.Messages.HospitalData.Dtos;

    public interface IScoringGuarantorMatchingService
    {
        void ExecuteScoring();
        List<List<ScoringGuarantorDto>> ExecuteRetroScoring(DateTime scoringDate);
        void RunGuarantorMatchingForRetroScore(bool useSskForGuarantorMatching);
    }
}

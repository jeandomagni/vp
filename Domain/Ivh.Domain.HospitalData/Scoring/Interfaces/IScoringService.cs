﻿namespace Ivh.Domain.HospitalData.Scoring.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using Entities;

    public interface IScoringService
    {
        void InitializeMapping();
        IList<ScoringCalculatedInput> CalculateScoringInput(IList<ScoringServiceInput> scoringServiceInputs);
        ScoringResult GenerateGuarantorPtpScore(ScoringPtpScoreRequest scoringPtpScoreRequest, ScoringScriptStorage scoringScriptStorage);
        ScoringResult GenerateGuarantorProtoScores(IList<ScoringServiceInput> scoringServiceInputs, ScoringScriptStorage scoringScriptStorage);
        IList<PtpScore> GetGuarantorPtpScore(Dictionary<string, List<int>> hsGuarantors);
        Stopwatch LogInfo(Func<string> message, bool startTimer=false);
        void LogInfoEndTimer(Func<string> message, Stopwatch sw);

    }
}

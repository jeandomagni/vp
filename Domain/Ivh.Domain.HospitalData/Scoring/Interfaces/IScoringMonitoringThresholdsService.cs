﻿namespace Ivh.Domain.HospitalData.Scoring.Interfaces
{
    using Entities;

    public interface IScoringMonitoringThresholdsService
    {
        ScoringMonitoringThresholds GetDailyScoringMonitoringThresholds();
        ScoringMonitoringThresholds GetWeeklyScoringMonitoringThresholds();
    }
}
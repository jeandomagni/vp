﻿namespace Ivh.Domain.HospitalData.Scoring.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IBaseScoreRepository : IRepository<BaseScore>
    {
        IList<BaseScore> GetScoresByBatchId(int scoringBatchId);
        IList<BaseScore> GetScoresByBatchIds(IList<int> scoringBatchIds);
        IList<BaseScore> GetTopScoresNotInScoringBatchId(int count, int scoringBatchId);

    }
}
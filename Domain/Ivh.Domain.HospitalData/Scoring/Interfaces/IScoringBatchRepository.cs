﻿namespace Ivh.Domain.HospitalData.Segmentation.Interfaces
{
    using System.Collections.Generic;
    using Scoring.Entities;

    public interface IScoringBatchRepository
    {
        int GenerateScoringBatchId();
        ScoringBatch GetLatestScoringBatch();
        IList<ScoringBatch> GetScoringBatchesForDays(int numberOfDaysBeforeToday);
    }
}
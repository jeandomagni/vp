﻿namespace Ivh.Domain.HospitalData.Scoring.Interfaces
{
    using System.Collections.Generic;
    using Entities;

    public interface IRetroScoreService
    {
        void SaveRetroScores(IList<RetroScore> calculatedInputs);
    }
}
﻿namespace Ivh.Domain.HospitalData.Scoring.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IRetroScoreRepository : IRepository<RetroScore> { }

}

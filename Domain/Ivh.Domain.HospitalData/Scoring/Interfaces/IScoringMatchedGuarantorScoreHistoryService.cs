﻿namespace Ivh.Domain.HospitalData.Scoring.Interfaces
{
    public interface IScoringMatchedGuarantorScoreHistoryService
    {
        void GenerateMatchedGuarantorScoreHistory(int scoringBatchId);
    }
}
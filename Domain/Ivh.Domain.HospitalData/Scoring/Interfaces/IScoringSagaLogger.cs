﻿namespace Ivh.Domain.HospitalData.Scoring.Interfaces
{
    using System;
    using Entities;

    public interface IScoringSagaLogger
    {
        void LogStartTime(int scoringBatchId, string state, string eventName, Guid correlationId, Guid processId);
        void LogEndTime(int scoringBatchId, string state, string eventName, Guid correlationId, Guid processId);
        ScoringSagaLog GetScoringSagaLog(int scoringBatchId, string state, string eventName, Guid correlationId, Guid processId);
        ScoringAndSegmentationSaga GetScoringAndSegmentationSaga(Guid correlationId);
    }
}

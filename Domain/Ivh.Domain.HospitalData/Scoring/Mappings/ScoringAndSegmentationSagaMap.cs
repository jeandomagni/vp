﻿namespace Ivh.Domain.HospitalData.Scoring.Mappings
{
    using System.Security.Cryptography.X509Certificates;
    using Entities;
    using FluentNHibernate.Mapping;

    public class ScoringAndSegmentationSagaMap : ClassMap<ScoringAndSegmentationSaga>
    {
        public ScoringAndSegmentationSagaMap()
        {
            this.Schema("scoring");
            this.Table("ScoringAndSegmentationSaga");
            this.Id(x => x.CorrelationId);
            this.Map(x => x.CurrentState);
            this.Map(x => x.ScoringBatchId);
            this.Map(x => x.BadDebtSegmentationBatchId);
            this.Map(x => x.AccountsReceivableSegmentationBatchId);
            this.Map(x => x.ActivePassiveSegmentationBatchId);
            this.Map(x => x.PresumptiveCharitySegmentationBatchId);
            this.Map(x => x.Version);
            this.Map(x => x.Timestamp);
            this.Map(x => x.ScoringEventFailures);
            this.Map(x => x.ScoringEventSuccesses);
            this.Map(x => x.ScoringEventTotal);
            this.Map(x => x.AccountReceivableSegmentationEventFailures);
            this.Map(x => x.AccountReceivableSegmentationEventSuccesses);
            this.Map(x => x.AccountReceivableSegmentationEventTotal);
            this.Map(x => x.BadDebtSegmentationEventFailures);
            this.Map(x => x.BadDebtSegmentationEventSuccesses);
            this.Map(x => x.BadDebtSegmentationEventTotal);
            this.Map(x => x.ActivePassiveSegmentationEventSuccesses);
            this.Map(x => x.ActivePassiveSegmentationEventFailures);
            this.Map(x => x.ActivePassiveSegmentationEventTotal);
            this.Map(x => x.PresumptiveCharitySegmentationEventSuccesses);
            this.Map(x => x.PresumptiveCharitySegmentationEventFailures);
            this.Map(x => x.PresumptiveCharitySegmentationEventTotal);
        }
    }
}
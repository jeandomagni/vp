﻿namespace Ivh.Domain.HospitalData.Scoring.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class ScoringStatisticalMonitoringMap : ClassMap<ScoringStatisticalMonitoring>
    {
        public ScoringStatisticalMonitoringMap()
        {
            this.Schema("scoring");
            this.Table("ScoringStatisticalMonitoring");
            this.Id(x => x.ScoringStatisticalMonitoringId);
            this.Map(x => x.ScoringMonitoringTypeId).Not.Nullable();
            this.Map(x => x.ScoreCount).Not.Nullable();
            this.Map(x => x.ScoringBatchId).Nullable();
            this.Map(x => x.GuarantorAllBalance30Days0Count).Not.Nullable();
            this.Map(x => x.GuarantorAllBalance30DaysMedian).Not.Nullable();
            this.Map(x => x.GuarantorAllBalance31To60Days0Count).Not.Nullable();
            this.Map(x => x.GuarantorAllBalance31To60DaysMedian).Not.Nullable();
            this.Map(x => x.GuarantorAllBalance61To90Days0Count).Not.Nullable();
            this.Map(x => x.GuarantorAllBalance61To90DaysMedian).Not.Nullable();
            this.Map(x => x.GuarantorAllBalance91To180Days0Count).Not.Nullable();
            this.Map(x => x.GuarantorAllBalance91To180DaysMedian).Not.Nullable();
            this.Map(x => x.GuarantorAllBalance181To270Days0Count).Not.Nullable();
            this.Map(x => x.GuarantorAllBalance181To270DaysMedian).Not.Nullable();
            this.Map(x => x.GuarantorAllBalance271To365Days0Count).Not.Nullable();
            this.Map(x => x.GuarantorAllBalance271To365DaysMedian).Not.Nullable();
            this.Map(x => x.GuarantorInactive120DaysBalance91To180Days0Count).Not.Nullable();
            this.Map(x => x.GuarantorInactive120DaysBalance91To180DaysMedian).Not.Nullable();
            this.Map(x => x.GuarantorInactive120DaysBalance181To270Days0Count).Not.Nullable();
            this.Map(x => x.GuarantorInactive120DaysBalance181To270DaysMedian).Not.Nullable();
            this.Map(x => x.GuarantorInactive120DaysBalance271To365Days0Count).Not.Nullable();
            this.Map(x => x.GuarantorInactive120DaysBalance271To365DaysMedian).Not.Nullable();
            this.Map(x => x.GuarantorPaidOffAccountsWPatientPaymentsNum30Days0Count).Not.Nullable();
            this.Map(x => x.GuarantorPaidOffAccountsWPatientPaymentsNum30DaysMedian).Not.Nullable();
            this.Map(x => x.GuarantorPaidOffAccountsWPatientPaymentsNum31To60Days0Count).Not.Nullable();
            this.Map(x => x.GuarantorPaidOffAccountsWPatientPaymentsNum31To60DaysMedian).Not.Nullable();
            this.Map(x => x.GuarantorPaidOffAccountsWPatientPaymentsNum61To90Days0Count).Not.Nullable();
            this.Map(x => x.GuarantorPaidOffAccountsWPatientPaymentsNum61To90DaysMedian).Not.Nullable();
            this.Map(x => x.GuarantorPaidOffAccountsWPatientPaymentsNum91To180Days0Count).Not.Nullable();
            this.Map(x => x.GuarantorPaidOffAccountsWPatientPaymentsNum91To180DaysMedian).Not.Nullable();
            this.Map(x => x.GuarantorPaidOffAccountsPatientPayments30Days0Count).Not.Nullable();
            this.Map(x => x.GuarantorPaidOffAccountsPatientPayments30DaysMedian).Not.Nullable();
            this.Map(x => x.GuarantorPaidOffAccountsPatientPayments31To60Days0Count).Not.Nullable();
            this.Map(x => x.GuarantorPaidOffAccountsPatientPayments31To60DaysMedian).Not.Nullable();
            this.Map(x => x.GuarantorPaidOffAccountsPatientPayments61To90Days0Count).Not.Nullable();
            this.Map(x => x.GuarantorPaidOffAccountsPatientPayments61To90DaysMedian).Not.Nullable();
            this.Map(x => x.GuarantorPaidOffAccountsPatientPayments91To180Days0Count).Not.Nullable();
            this.Map(x => x.GuarantorPaidOffAccountsPatientPayments91To180DaysMedian).Not.Nullable();
            this.Map(x => x.GuarantorPaymentsByDischarge30Days0Count).Not.Nullable();
            this.Map(x => x.GuarantorPaymentsByDischarge30DaysMedian).Not.Nullable();
            this.Map(x => x.GuarantorPaymentsByDischarge31To60Days0Count).Not.Nullable();
            this.Map(x => x.GuarantorPaymentsByDischarge31To60DaysMedian).Not.Nullable();
            this.Map(x => x.GuarantorPaymentsByDischarge61To90Days0Count).Not.Nullable();
            this.Map(x => x.GuarantorPaymentsByDischarge61To90DaysMedian).Not.Nullable();
            this.Map(x => x.GuarantorPaymentsByDischarge91To180Days0Count).Not.Nullable();
            this.Map(x => x.GuarantorPaymentsByDischarge91To180DaysMedian).Not.Nullable();
            this.Map(x => x.NoPriorData30DaysMean).Not.Nullable();
            this.Map(x => x.NoPriorData31To60DaysMean).Not.Nullable();
            this.Map(x => x.NoPriorData61To90DaysMean).Not.Nullable();
            this.Map(x => x.NoPriorData91To180DaysMean).Not.Nullable();
            this.Map(x => x.PatientType2NullCount).Not.Nullable();
            this.Map(x => x.PatientType2Mean).Not.Nullable();
            this.Map(x => x.PrimaryInsuranceTypeNNullCount).Not.Nullable();
            this.Map(x => x.PrimaryInsuranceTypeNMean).Not.Nullable();
            this.Map(x => x.PrimaryInsuranceTypeANullCount).Not.Nullable();
            this.Map(x => x.PrimaryInsuranceTypeAMean).Not.Nullable();
            this.Map(x => x.PrimaryInsuranceType1NullCount).Not.Nullable();
            this.Map(x => x.PrimaryInsuranceType1Mean).Not.Nullable();
            this.Map(x => x.ThirdPartyDataCount).Not.Nullable();
            this.Map(x => x.ProbabilityScoreMedian).Not.Nullable();
            this.Map(x => x.OwnRentNullCount).Not.Nullable();
            this.Map(x => x.OwnRentMean).Not.Nullable();
            this.Map(x => x.HouseholdIncomeNullCount).Not.Nullable();
            this.Map(x => x.HouseholdIncomeMean).Not.Nullable();
            this.Map(x => x.HouseholdMemberCountNullCount).Not.Nullable();
            this.Map(x => x.HouseholdMemberCountMean).Not.Nullable();
        }
    }
}
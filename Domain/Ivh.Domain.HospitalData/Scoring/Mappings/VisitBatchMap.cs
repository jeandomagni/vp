﻿namespace Ivh.Domain.HospitalData.Scoring.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class VisitBatchMap : ClassMap<VisitBatch>
    {
        public VisitBatchMap()
        {
            this.Schema("scoring");
            this.Table("VisitBatch");
            this.Id(x => x.VisitBatchId).Not.Nullable();
            this.Map(x => x.VisitId).Not.Nullable().Not.Update();
            this.Map(x => x.ChangeEventId).Nullable().Not.Update();
            this.Map(x => x.HsGuarantorId).Not.Nullable().Not.Update();
            this.Map(x => x.ScoringBatchId).Nullable().Not.Update();
            this.Map(x => x.TriggeredScoring).Not.Nullable().Not.Update();
            this.Map(x => x.IsEligibleToScore).Nullable().Not.Update();
            this.Map(x => x.BillingSystemId).Not.Nullable().Not.Update();
            this.Map(x => x.SourceSystemKey).Not.Nullable().Not.Update();
            this.Map(x => x.DischargeDate).Nullable().Not.Update();
            this.Map(x => x.HsCurrentBalance).Not.Nullable().Not.Update();
            this.Map(x => x.SelfPayBalance).Not.Nullable().Not.Update();
            this.Map(x => x.BillingApplication).Nullable().Not.Update();
            this.Map(x => x.LifeCycleStageId).Not.Nullable().Not.Update();
            this.Map(x => x.PatientTypeId).Not.Nullable().Not.Update();
            this.Map(x => x.SelfPayClassId).Not.Nullable().Not.Update();
            this.Map(x => x.FirstSelfPayDate).Nullable().Not.Update();
            this.Map(x => x.ScoreId).Nullable().Update();

            this.References(x => x.Visit)
                .Column("VisitId")
                .Not.Insert()
                .Not.Update();
        }
    }
}

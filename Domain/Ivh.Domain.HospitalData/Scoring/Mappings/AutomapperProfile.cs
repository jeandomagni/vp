﻿namespace Ivh.Domain.HospitalData.Scoring.Mappings
{
    using AutoMapper;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using Entities;

    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            this.CreateMap<ScoringCalculatedInput, ScoringCalculatedInputDto>()
                .ForMember(dest => dest.Phase1ScriptVersion, opts => opts.MapFrom(src => src.S1Version))
                .ForMember(dest => dest.Phase2ScriptVersion, opts => opts.MapFrom(src => src.S2Version));
            this.CreateMap<ScoringGuarantorScore, ScoringGuarantorScoreDto>();
        }
    }
}
﻿namespace Ivh.Domain.HospitalData.Scoring.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class GuarantorBatchMap : ClassMap<GuarantorBatch>
    {
        public GuarantorBatchMap()
        {
            this.Schema("scoring");
            this.Table("GuarantorBatch");
            this.Id(x => x.GuarantorBatchId).Not.Nullable();
            this.Map(x => x.ParentHsGuarantorId);
            this.Map(x => x.ChildHsGuarantorId);
            this.Map(x => x.SourceSystemKey);
            this.Map(x => x.ScoringBatchId);
            this.Map(x => x.IsEligibleToScore);
            this.Map(x => x.Dob);
            this.Map(x => x.Ssn);
            this.Map(x => x.FirstName);
            this.Map(x => x.LastName);
            this.Map(x => x.Address1);
            this.Map(x => x.City);
            this.Map(x => x.StateProvince);
            this.Map(x => x.PostalCode);
        }
    }
}
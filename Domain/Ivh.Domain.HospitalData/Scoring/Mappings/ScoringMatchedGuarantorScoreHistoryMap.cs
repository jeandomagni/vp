﻿namespace Ivh.Domain.HospitalData.Scoring.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class ScoringMatchedGuarantorScoreHistoryMap : ClassMap<MatchedGuarantorScoreHistory>
    {
        public ScoringMatchedGuarantorScoreHistoryMap()
        {
            this.Schema("scoring");
            this.Table("HsGuarantorScoreHistory");
            this.Id(x => x.HsGuarantorScoreHistoryId).Not.Nullable();
            this.Map(x => x.HsGuarantorId).Not.Nullable();
            this.Map(x => x.ScoreId).Not.Nullable();
        }
    }
}
﻿namespace Ivh.Domain.HospitalData.Scoring.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class RetroScoreMap : ClassMap<RetroScore>
    {
        public RetroScoreMap()
        {
            //This prevents nHibernate from finding this map from inherited types
            this.Polymorphism.Explicit();

            this.Schema("scoring");
            this.Table("RetroScore");
            this.Id(x => x.RetroScoreId);
            this.Map(x => x.ParentHsGuarantorId).Nullable();
            this.Map(x => x.ScoringBatchId).Nullable();
            this.Map(x => x.FirstSelfPayDate).Nullable();
            this.Map(x => x.ScoringDate).Nullable();
            this.Map(x => x.SelfPayAllBalance).Nullable();
            this.Map(x => x.PrimaryInsuranceTypeN).Nullable();
            this.Map(x => x.PrimaryInsuranceTypeA).Nullable();
            this.Map(x => x.PrimaryInsuranceType1).Nullable();
            this.Map(x => x.PatientType2).Nullable();
            this.Map(x => x.ScoringAccountHasPayorTrans).Nullable();
            this.Map(x => x.GuarantorAllBalance30Days).Nullable();
            this.Map(x => x.GuarantorAllBalance31To60Days).Nullable();
            this.Map(x => x.GuarantorAllBalance61To90Days).Nullable();
            this.Map(x => x.GuarantorAllBalance91To180Days).Nullable();
            this.Map(x => x.GuarantorAllBalance181To270Days).Nullable();
            this.Map(x => x.GuarantorAllBalance271To365Days).Nullable();
            this.Map(x => x.GuarantorInactive120DaysBalance91To180Days).Nullable();
            this.Map(x => x.GuarantorInactive120DaysBalance181To270Days).Nullable();
            this.Map(x => x.GuarantorInactive120DaysBalance271To365Days).Nullable();
            this.Map(x => x.GuarantorPaidOffAccountsWPatientPaymentsNum30Days).Nullable();
            this.Map(x => x.GuarantorPaidOffAccountsWPatientPaymentsNum31To60Days).Nullable();
            this.Map(x => x.GuarantorPaidOffAccountsWPatientPaymentsNum61To90Days).Nullable();
            this.Map(x => x.GuarantorPaidOffAccountsWPatientPaymentsNum91To180Days).Nullable();
            this.Map(x => x.GuarantorPaidOffAccountsPatientPayments30Days).Nullable();
            this.Map(x => x.GuarantorPaidOffAccountsPatientPayments31To60Days).Nullable();
            this.Map(x => x.GuarantorPaidOffAccountsPatientPayments61To90Days).Nullable();
            this.Map(x => x.GuarantorPaidOffAccountsPatientPayments91To180Days).Nullable();
            this.Map(x => x.GuarantorPaymentsByDischarge30Days).Nullable();
            this.Map(x => x.GuarantorPaymentsByDischarge31To60Days).Nullable();
            this.Map(x => x.GuarantorPaymentsByDischarge61To90Days).Nullable();
            this.Map(x => x.GuarantorPaymentsByDischarge91To180Days).Nullable();
            this.Map(x => x.NoPriorData30Days).Nullable();
            this.Map(x => x.NoPriorData31To60Days).Nullable();
            this.Map(x => x.NoPriorData61To90Days).Nullable();
            this.Map(x => x.NoPriorData91To180Days).Nullable();
        }

    }
}
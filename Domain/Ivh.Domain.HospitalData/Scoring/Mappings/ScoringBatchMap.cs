﻿namespace Ivh.Domain.HospitalData.Scoring.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class ScoringBatchMap : ClassMap<ScoringBatch>
    {
        public ScoringBatchMap()
        {
            this.Schema("scoring");
            this.Table("ScoringBatch");
            this.Id(x => x.ScoringBatchId).Not.Nullable();
            this.Map(x => x.BatchStartDateTime).Not.Nullable();
        }
    }
}
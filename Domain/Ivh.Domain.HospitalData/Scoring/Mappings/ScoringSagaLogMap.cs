﻿namespace Ivh.Domain.HospitalData.Scoring.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class ScoringSagaLogMap : ClassMap<ScoringSagaLog>
    {
        public ScoringSagaLogMap()
        {
            this.Schema("scoring");
            this.Table("SagaLog");
            this.CompositeId()
                .KeyProperty(x => x.State)
                .KeyProperty(x => x.Event)
                .KeyProperty(x => x.CorrelationId)
                .KeyProperty(x => x.ProcessId);
            this.Map(x => x.ScoringBatchId).Not.Nullable();
            this.Map(x => x.StartTime).Nullable();
            this.Map(x => x.EndTime).Nullable();
        }
    }
}
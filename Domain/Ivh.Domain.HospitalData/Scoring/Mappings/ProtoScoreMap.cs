﻿namespace Ivh.Domain.HospitalData.Scoring.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class ProtoScoreMap : ClassMap<ProtoScore>
    {
        public ProtoScoreMap()
        {
            this.Schema("scoring");
            this.Table("ProtoScore");
            this.Id(x => x.ProtoScoreId).Not.Nullable();
            this.Map(x => x.ProtoScore1).Not.Nullable();
            this.Map(x => x.ProtoScore2).Not.Nullable();
        }
    }
}

﻿namespace Ivh.Domain.HospitalData.Scoring.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class ThirdPartyDataMap : ClassMap<ThirdPartyData>
    {
        public ThirdPartyDataMap()
        {
            this.Schema("scoring");
            this.Table("ThirdPartyData");
            this.Id(x => x.ThirdPartyDataId).Not.Nullable();
            this.Map(x => x.ThirdPartyProviderId).Not.Nullable();
            this.Map(x => x.HsGuarantorId).Not.Nullable();
            this.Map(x => x.ProbabilityScore).Nullable();
            this.Map(x => x.WriteOffs5Years).Nullable();
            this.Map(x => x.WriteOffsContinuity5Years).Nullable();
            this.Map(x => x.CreditCardPayments1Year).Nullable();
            this.Map(x => x.CreditCardPayments5Year).Nullable();
            this.Map(x => x.ContinuityContractWriteOffs5Years).Nullable();
            this.Map(x => x.HouseholdIncome).Nullable();
            this.Map(x => x.HouseholdMemberCount).Nullable();
            this.Map(x => x.OwnRent).Nullable();
            this.Map(x => x.ImputedHouseholdIncome).Nullable();
            this.Map(x => x.ImputedMemberCount).Nullable();
            this.Map(x => x.MessageId).Nullable();
            this.Map(x => x.CreatedDate).Not.Nullable();
            this.Map(x => x.Dataset).Nullable();
            this.Map(x => x.RequestInputString).Nullable();

            this.References(x => x.HouseholdIncomeCodeToIncomeAmount).Column("HouseholdIncome").PropertyRef("HouseholdIncomeCode")
                .Nullable().NotFound.Ignore()
                .Not.Insert()
                .Not.Update();
        }
    }
}

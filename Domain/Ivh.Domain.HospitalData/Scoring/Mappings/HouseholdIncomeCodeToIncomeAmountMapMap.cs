﻿namespace Ivh.Domain.HospitalData.Scoring.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;
    public class HouseholdIncomeCodeToIncomeAmountMapMap : ClassMap<HouseholdIncomeCodeToIncomeAmountMap>
    {
        public HouseholdIncomeCodeToIncomeAmountMapMap()
        {
            this.Schema("scoring");
            this.Table("HouseholdIncomeCodeToIncomeAmountMap");
            this.Id(x => x.HouseholdIncomeCodeToIncomeAmountMapId).Not.Nullable();
            this.Map(x => x.HouseholdIncomeCode).Not.Nullable().Not.Update();
            this.Map(x => x.MinIncome).Not.Nullable().Not.Update();
            this.Map(x => x.MaxIncome).Not.Nullable().Not.Update();
            this.Map(x => x.IncomeAmount).Not.Nullable().Not.Update();
        }
    }
}
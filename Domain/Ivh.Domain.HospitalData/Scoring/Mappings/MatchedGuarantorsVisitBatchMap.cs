﻿namespace Ivh.Domain.HospitalData.Scoring.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class MatchedGuarantorsVisitBatchMap : ClassMap<MatchedGuarantorsVisitBatch>
    {
        public MatchedGuarantorsVisitBatchMap()
        {
            this.Schema("scoring");
            this.Table("MatchedGuarantorsVisitBatch");
            this.Id(x => x.MatchedGuarantorsVisitBatchId).Not.Nullable();
            this.Map(x => x.ParentHsGuarantorId).Not.Nullable().Not.Update();
            this.Map(x => x.ChildHsGuarantorId).Not.Nullable().Not.Update();
            this.Map(x => x.ScoringBatchId).Not.Nullable().Not.Update();
            this.Map(x => x.ScoringStatusId).Nullable().Update();
            this.Map(x => x.VisitBatchId).Nullable().Not.Update();

            this.References(x => x.VisitBatch)
                .Column("VisitBatchId")
                .Not.Insert()
                .Not.Update();
        }
    }
}
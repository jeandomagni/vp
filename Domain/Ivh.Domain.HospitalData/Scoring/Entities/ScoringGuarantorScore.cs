﻿namespace Ivh.Domain.HospitalData.Scoring.Entities
{
    public class ScoringGuarantorScore
    {
        public virtual int HsGuarantorId { get; set; }
        public virtual int VisitId { get; set; }
        public virtual decimal PtpScore { get; set; }
        public virtual bool Am1Flag { get; set; }
        public virtual string S1Version { get; set; }
        public virtual string S2Version { get; set; }
        public virtual ProtoScore ProtoScore { get; set; }
    }
}

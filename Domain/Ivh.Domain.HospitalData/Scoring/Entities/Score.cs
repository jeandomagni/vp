﻿namespace Ivh.Domain.HospitalData.Scoring.Entities
{
    using System;
    using Enums;

    public class Score : BaseScore
    {
        public virtual int? ThirdPartyDataId { get; set; }
        public virtual decimal PtpScore { get; set; }
        public virtual string VisitTransactionJson { get; set; }
        public virtual int GuarantorCRAInfoId { get; set; }
        public virtual string Phase1ScriptVersion { get; set; }
        public virtual string Phase2ScriptVersion { get; set; }
        public virtual int? ScoreTypeId { get; set; }
        public virtual ProtoScore ProtoScore { get; set; }
        public virtual Guid ProcessId { get; set; }

    }
}

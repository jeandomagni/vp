﻿namespace Ivh.Domain.HospitalData.Scoring.Entities
{
    public class MessagingEndpoint
    {
        public virtual string ServiceBusConnectionString { get; set; }
        public virtual string ServiceBusUserName { get; set; }
        public virtual string ServiceBusPassword { get; set; }
        public virtual bool ServiceBusEnableSsl { get; set; }
        public virtual string ServiceBusVirtualHost { get; set; }
        public virtual string ServiceBusSslCertificatePath { get; set; }
        public virtual string ServiceBusSslCertificatePassword { get; set; }
        public virtual string ServiceBusServiceName { get; set; }
        public virtual string ServiceBusScheduleServiceName { get; set; }
    }
}

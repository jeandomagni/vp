﻿namespace Ivh.Domain.HospitalData.Scoring.Entities
{
    using System.Collections.Generic;

    public class ScoringServiceInput
    {
        public virtual int HsGuarantorId { get; set; }
        public virtual IList<GuarantorAccountVisit> GuarantorAccountVisits { get; set; }
    }
}

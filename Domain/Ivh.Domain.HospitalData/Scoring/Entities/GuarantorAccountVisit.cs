﻿namespace Ivh.Domain.HospitalData.Scoring.Entities
{
    using System;
    using System.Collections.Generic;
    using Common.VisitPay.Enums;
    using Enums;

    public class GuarantorAccountVisit
    {
        public virtual int VisitBatchId { get; set; }
        public virtual int VisitId { get; set; }
        public virtual int HsGuarantorId { get; set; }
        public virtual int ScoringBatchId { get; set; }
        public virtual bool TriggeredScoring { get; set; }
        public virtual bool IsEligibleToScore { get; set; }
        public virtual int BillingSystemId { get; set; }
        public virtual string SourceSystemKey { get; set; }
        public virtual DateTime DischargeDate { get; set; }
        public virtual decimal HsCurrentBalance { get; set; }
        public virtual decimal SelfPayBalance { get; set; }
        public virtual string BillingApplication { get; set; }
        public virtual int LifeCycleStageId { get; set; }
        public virtual int PatientTypeId { get; set; }
        public virtual PatientTypeEnum PatientTypeEnum => (PatientTypeEnum)this.PatientTypeId;
        public virtual int SelfPayClassId { get; set; }
        public virtual SelfPayClassEnum SelfPayClassEnum => (SelfPayClassEnum)this.SelfPayClassId;
        public virtual DateTime FirstSelfPayDate { get; set; }
        public virtual DateTime ScoringDate { get; set; }
        public virtual IList<GuarantorAccountTransaction> GuarantorAccountTransactions { get; set; }
        public int FacilityId { get; set; }
        public string FacilityDescription { get; set; }
        public string StateCode { get; set; }
        public int? RegionId { get; set; }
        public string RegionScope { get; set; }
        public string BillingSystemName { get; set; }

    }
}

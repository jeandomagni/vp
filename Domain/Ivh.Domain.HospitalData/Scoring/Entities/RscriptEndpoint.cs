﻿namespace Ivh.Domain.HospitalData.Scoring.Entities
{
    public class RscriptEndpoint
    {
        public virtual string SourceRScript { get; set; }
        public virtual string DataUrls { get; set; }
    }
}

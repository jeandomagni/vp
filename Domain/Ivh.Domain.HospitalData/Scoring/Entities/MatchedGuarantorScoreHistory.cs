﻿namespace Ivh.Domain.HospitalData.Scoring.Entities
{
    public class MatchedGuarantorScoreHistory
    {
        public virtual int HsGuarantorScoreHistoryId { get; set; }
        public virtual int HsGuarantorId { get; set; }
        public virtual int ScoreId { get; set; }
    }
}

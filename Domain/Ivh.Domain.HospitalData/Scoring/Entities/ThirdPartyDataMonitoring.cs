﻿namespace Ivh.Domain.HospitalData.Scoring.Entities
{
    public class ThirdPartyDataMonitoring
    {
        public virtual decimal? ProbabilityScore { get; set; }
        public virtual int? OwnRent { get; set; }
        public virtual decimal? HouseholdIncome { get; set; }
        public virtual int? HouseholdMemberCount { get; set; }
    }
}
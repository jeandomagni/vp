﻿namespace Ivh.Domain.HospitalData.Scoring.Entities
{
    using System;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Visit.Entities;

    public class GuarantorAccountTransaction
    {
        public virtual int VisitTransactionId { get; set; }
        public virtual int BillingSystemId { get; set; }
        public virtual string SourceSystemKey { get; set; }
        public virtual DateTime TransactionDate { get; set; }
        public virtual DateTime PostDate { get; set; }
        public virtual decimal TransactionAmount { get; set; }
        public virtual VpTransactionTypeEnum VpTransactionTypeEnum => (VpTransactionTypeEnum) this.VpTransactionType.VpTransactionTypeId;
        public virtual VpTransactionType VpTransactionType { get; set; }
        public virtual string TransactionDescription { get; set; }
        public virtual int? VpPaymentAllocationId { get; set; }
        public virtual int TransactionCodeId { get; set; }
    }
}

﻿namespace Ivh.Domain.HospitalData.Scoring.Entities
{
    using System;

    public class ScoringBatch
    {
        public virtual int ScoringBatchId { get; set; }
        public virtual DateTime BatchStartDateTime { get; set; }
    }
}
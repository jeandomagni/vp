﻿namespace Ivh.Domain.HospitalData.Scoring.Entities
{
    using System;

    public class ScoringAndSegmentationSaga
    {
        public virtual Guid CorrelationId { get; set; }
        public virtual int Version { get; set; }
        public virtual int ScoringBatchId { get; set; }
        public virtual int BadDebtSegmentationBatchId { get; set; }
        public virtual int AccountsReceivableSegmentationBatchId { get; set; }
        public virtual int ActivePassiveSegmentationBatchId { get; set; }
        public virtual int PresumptiveCharitySegmentationBatchId { get; set; }
        public virtual DateTime Timestamp { get; set; }
        public virtual int ScoringEventTotal { get; set; }
        public virtual int ScoringEventSuccesses { get; set; }
        public virtual int ScoringEventFailures { get; set; }
        public virtual int BadDebtSegmentationEventTotal { get; set; }
        public virtual int BadDebtSegmentationEventSuccesses { get; set; }
        public virtual int BadDebtSegmentationEventFailures { get; set; }
        public virtual int AccountReceivableSegmentationEventTotal { get; set; }
        public virtual int AccountReceivableSegmentationEventSuccesses { get; set; }
        public virtual int AccountReceivableSegmentationEventFailures { get; set; }
        public virtual int ActivePassiveSegmentationEventTotal { get; set; }
        public virtual int ActivePassiveSegmentationEventSuccesses { get; set; }
        public virtual int PresumptiveCharitySegmentationEventFailures { get; set; }
        public virtual int PresumptiveCharitySegmentationEventTotal { get; set; }
        public virtual int PresumptiveCharitySegmentationEventSuccesses { get; set; }
        public virtual int ActivePassiveSegmentationEventFailures { get; set; }
        public virtual string CurrentState { get; set; }


    }
}

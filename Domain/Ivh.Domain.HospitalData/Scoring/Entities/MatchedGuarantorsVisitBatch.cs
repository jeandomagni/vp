﻿namespace Ivh.Domain.HospitalData.Scoring.Entities
{
    public class MatchedGuarantorsVisitBatch
    {
        public virtual int MatchedGuarantorsVisitBatchId { get; set; }
        public virtual int ParentHsGuarantorId { get; set; }
        public virtual int ChildHsGuarantorId { get; set; }
        public virtual int ScoringBatchId { get; set; }
        public virtual int ScoringStatusId { get; set; }
        public virtual int VisitBatchId { get; set; }
        public virtual VisitBatch VisitBatch { get; set; }
    }
}

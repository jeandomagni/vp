﻿namespace Ivh.Domain.HospitalData.Scoring.Entities
{
    using System;

    public class ScoringSagaLog
    {
        public virtual string State { get; set; }
        public virtual string Event { get; set; }
        public virtual Guid CorrelationId { get; set; }
        public virtual Guid ProcessId { get; set; }
        public virtual int ScoringBatchId { get; set; }
        public virtual DateTime StartTime { get; set; }
        public virtual DateTime EndTime { get; set; }

        public override bool Equals(Object obj)
        {
            if (!(obj is ScoringSagaLog t))
            {
                return false;
            }
            return this.State == t.State && this.Event == t.Event && this.CorrelationId == t.CorrelationId && this.ProcessId == t.ProcessId;
        }
        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = 13;
                hashCode = (hashCode * 397) ^ (this.State.GetHashCode());
                hashCode = (hashCode * 397) ^ (this.Event.GetHashCode());
                hashCode = (hashCode * 397) ^ (this.CorrelationId.GetHashCode());
                hashCode = (hashCode * 397) ^ (this.ProcessId.GetHashCode());
                return hashCode;
            }
        }

    }
}

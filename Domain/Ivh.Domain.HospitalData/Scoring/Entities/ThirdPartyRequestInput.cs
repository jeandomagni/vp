﻿namespace Ivh.Domain.HospitalData.Scoring.Entities
{
    using System;
    using System.Xml.Serialization;

    [XmlType(TypeName = "RTRequest")]
    [Serializable]
    public class ThirdPartyRequestInput
    {
        [XmlIgnore]
        public virtual int HsGuarantorId { get; set; }

        [XmlIgnore]
        public virtual int ThirdPartyProvider { get; set; }

        [XmlElement("RequestType")]
        public virtual string RequestType { get; set; }

        [XmlElement("SecurityToken")]
        public virtual string SecurityToken { get; set; }

        [XmlElement("FirstName")]
        public virtual string FirstName { get; set; }

        [XmlElement("LastName")]
        public virtual string LastName { get; set; }

        [XmlElement("AddressLine1")]
        public virtual string AddressLine1 { get; set; }

        [XmlElement("City")]
        public virtual string City { get; set; }

        [XmlElement("State")]
        public virtual string State { get; set; }

        [XmlElement("ZipCode")]
        public virtual string ZipCode { get; set; }

        [XmlElement("MessageID")]
        public virtual string MessageId { get; set; }

        [XmlElement("VariableList")]
        public virtual string VariableList { get; set; }

        [XmlElement("Userfield1")]
        public virtual int Userfield1 { get; set; }

        public virtual string RequestInputString => string.Concat(this.FirstName, this.LastName, this.AddressLine1, this.City, this.State, this.ZipCode).Replace(" ",string.Empty);
    }
}

﻿namespace Ivh.Domain.HospitalData.Scoring.Entities
{
    public class HouseholdIncomeCodeToIncomeAmountMap
    {
        public virtual int HouseholdIncomeCodeToIncomeAmountMapId { get; set; }
        public virtual string HouseholdIncomeCode { get; set; }
        public virtual decimal MinIncome { get; set; }
        public virtual decimal MaxIncome { get; set; }
        public virtual decimal IncomeAmount { get; set; }
    }
}
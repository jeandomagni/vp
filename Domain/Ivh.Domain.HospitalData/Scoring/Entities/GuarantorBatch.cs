﻿namespace Ivh.Domain.HospitalData.Scoring.Entities
{
    using System;

    public class GuarantorBatch
    {
        public virtual int GuarantorBatchId { get; set; }
        public virtual int ParentHsGuarantorId { get; set; }
        public virtual int ChildHsGuarantorId { get; set; }
        public virtual string SourceSystemKey { get; set; }
        public virtual int ScoringBatchId { get; set; }
        public virtual bool IsEligibleToScore { get; set; }
        public virtual DateTime Dob { get; set; }
        public virtual string Ssn { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string Address1 { get; set; }
        public virtual string City { get; set; }
        public virtual string StateProvince { get; set; }
        public virtual string PostalCode { get; set; }
    }
}
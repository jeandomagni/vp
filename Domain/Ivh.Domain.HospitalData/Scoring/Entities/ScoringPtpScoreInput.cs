﻿namespace Ivh.Domain.HospitalData.Scoring.Entities
{
    using System.Collections.Generic;
    using Common.VisitPay.Messages.HospitalData.Dtos;

    public class ScoringPtpScoreInput
    {
        public IList<ScoringServiceInput> ScoringServiceInputs { get; set; }
        public IList<ScoringCalculatedInput> CalculatedInputs { get; set; }
        public IList<ScoringThirdPartyDto> ThirdPartyData { get; set; }
        public IList<ScoringGuarantorScore> GuarantorScores { get; set; }

        public ScoringPtpScoreInput()
        {
            this.CalculatedInputs = new List<ScoringCalculatedInput>();
            this.ThirdPartyData = new List<ScoringThirdPartyDto>();
            this.GuarantorScores = new List<ScoringGuarantorScore>();
        }

        public bool ShouldSerializeScoringServiceInputs()
        {
            return this.ScoringServiceInputs != null;
        }
    }
}

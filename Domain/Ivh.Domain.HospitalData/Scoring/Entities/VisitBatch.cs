﻿namespace Ivh.Domain.HospitalData.Scoring.Entities
{
    using System;
    using Ivh.Domain.HospitalData.Visit.Entities;

    public class VisitBatch
    {
        public virtual int VisitBatchId { get; set; }
        public virtual int VisitId { get; set; }
        public virtual Visit Visit { get; set; }
        public virtual int ScoreId { get; set; }
        public virtual int HsGuarantorId { get; set; }
        public virtual int ScoringBatchId { get; set; }
        public virtual int ChangeEventId { get; set; }
        public virtual bool TriggeredScoring { get; set; }
        public virtual bool IsEligibleToScore { get; set; }
        public virtual int BillingSystemId { get; set; }
        public virtual string SourceSystemKey { get; set; }
        public virtual DateTime DischargeDate { get; set; }
        public virtual decimal HsCurrentBalance { get; set; }
        public virtual decimal SelfPayBalance { get; set; }
        public virtual string BillingApplication { get; set; }
        public virtual int LifeCycleStageId { get; set; }
        public virtual int PatientTypeId { get; set; }
        public virtual int SelfPayClassId { get; set; }
        public virtual DateTime FirstSelfPayDate { get; set; }
    }
}

﻿namespace Ivh.Domain.HospitalData.Scoring.Entities
{
    public class RetroScore : BaseScore
    {
        public virtual int RetroScoreId { get; set; }
    }
}
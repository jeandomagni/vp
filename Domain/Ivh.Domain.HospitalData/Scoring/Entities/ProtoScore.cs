﻿namespace Ivh.Domain.HospitalData.Scoring.Entities
{
    public class ProtoScore
    {
        public virtual int ProtoScoreId { get; set; }
        public virtual decimal ProtoScore1 { get; set; }
        public virtual decimal ProtoScore2 { get; set; }
    }
}

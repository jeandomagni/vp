﻿namespace Ivh.Domain.HospitalData.Scoring.Entities
{
    public class PtpScore
    {
        public virtual int ScoreId { get; set; }
        public virtual decimal Score { get; set; }
        public virtual int ScoreTypeId { get; set; }
        public virtual string Version { get; set; }
    }
}

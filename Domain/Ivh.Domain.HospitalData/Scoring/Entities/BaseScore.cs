﻿namespace Ivh.Domain.HospitalData.Scoring.Entities
{
    using System;

    //Warning: If you add child objects to this and expect nHibernate to populate them you're going to have a bad time, We're fetching this using the statelessSession
    public class BaseScore
    {
        public virtual int ScoreId { get; set; }
        public virtual int ParentHsGuarantorId { get; set; }
        public virtual int ScoringBatchId { get; set; }
        public virtual DateTime FirstSelfPayDate { get; set; }
        public virtual DateTime ScoringDate { get; set; }
        public virtual decimal SelfPayAllBalance { get; set; }
        public virtual string PrimaryInsuranceTypeN { get; set; }
        public virtual string PrimaryInsuranceTypeA { get; set; }
        public virtual string PrimaryInsuranceType1 { get; set; }
        public virtual string PatientType2 { get; set; }
        public virtual bool ScoringAccountHasPayorTrans { get; set; }
        public virtual decimal GuarantorAllBalance30Days { get; set; }
        public virtual decimal GuarantorAllBalance31To60Days { get; set; }
        public virtual decimal GuarantorAllBalance61To90Days { get; set; }
        public virtual decimal GuarantorAllBalance91To180Days { get; set; }
        public virtual decimal GuarantorAllBalance181To270Days { get; set; }
        public virtual decimal GuarantorAllBalance271To365Days { get; set; }
        public virtual decimal GuarantorInactive120DaysBalance91To180Days { get; set; }
        public virtual decimal GuarantorInactive120DaysBalance181To270Days { get; set; }
        public virtual decimal GuarantorInactive120DaysBalance271To365Days { get; set; }
        public virtual int GuarantorPaidOffAccountsWPatientPaymentsNum30Days { get; set; }
        public virtual int GuarantorPaidOffAccountsWPatientPaymentsNum31To60Days { get; set; }
        public virtual int GuarantorPaidOffAccountsWPatientPaymentsNum61To90Days { get; set; }
        public virtual int GuarantorPaidOffAccountsWPatientPaymentsNum91To180Days { get; set; }
        public virtual decimal GuarantorPaidOffAccountsPatientPayments30Days { get; set; }
        public virtual decimal GuarantorPaidOffAccountsPatientPayments31To60Days { get; set; }
        public virtual decimal GuarantorPaidOffAccountsPatientPayments61To90Days { get; set; }
        public virtual decimal GuarantorPaidOffAccountsPatientPayments91To180Days { get; set; }
        public virtual decimal GuarantorPaymentsByDischarge30Days { get; set; }
        public virtual decimal GuarantorPaymentsByDischarge31To60Days { get; set; }
        public virtual decimal GuarantorPaymentsByDischarge61To90Days { get; set; }
        public virtual decimal GuarantorPaymentsByDischarge91To180Days { get; set; }
        public virtual bool NoPriorData30Days { get; set; }
        public virtual bool NoPriorData31To60Days { get; set; }
        public virtual bool NoPriorData61To90Days { get; set; }
        public virtual bool NoPriorData91To180Days { get; set; }
    }
}
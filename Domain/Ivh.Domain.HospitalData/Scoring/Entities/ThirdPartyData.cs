﻿namespace Ivh.Domain.HospitalData.Scoring.Entities
{
    using System;

    public class ThirdPartyData
    {
        public virtual int ThirdPartyDataId { get; set; }

        public virtual int ThirdPartyProviderId { get; set; }

        public virtual int HsGuarantorId { get; set; }

        public virtual DateTime CreatedDate { get; set; }

        public virtual decimal? ProbabilityScore { get; set; }

        public virtual int? WriteOffs5Years { get; set; }

        public virtual int? WriteOffsContinuity5Years { get; set; }

        public virtual int? CreditCardPayments1Year { get; set; }

        public virtual int? CreditCardPayments5Year { get; set; }

        public virtual int? ContinuityContractWriteOffs5Years { get; set; }

        public virtual string HouseholdIncome { get; set; }

        public virtual int? HouseholdMemberCount { get; set; }

        public virtual int? OwnRent { get; set; }

        public virtual string ImputedHouseholdIncome { get; set; }

        public virtual string ImputedMemberCount { get; set; }

        public virtual string MessageId { get; set; }

        public virtual int? Dataset { get; set; }

        public virtual string RequestInputString { get; set; }

        public virtual HouseholdIncomeCodeToIncomeAmountMap HouseholdIncomeCodeToIncomeAmount { get; set; }
    }
}

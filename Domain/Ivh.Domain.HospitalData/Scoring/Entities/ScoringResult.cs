﻿using System.Collections.Generic;

namespace Ivh.Domain.HospitalData.Scoring.Entities
{
    using Common.VisitPay.Messages.HospitalData.Dtos;

    public class ScoringResult
    {
        public virtual IList<ScoringServiceInput> SourceInputs { get; set; }
        public virtual IList<ScoringCalculatedInput> CalculatedInputs { get; set; }
        public virtual IList<ScoringGuarantorScore> GuarantorScores { get; set; }
        public virtual IList<ScoringThirdPartyDto> ThirdPartyDtos { get; set; }
    }
}

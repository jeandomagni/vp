﻿namespace Ivh.Domain.HospitalData.Scoring.Services
{
    using System;
    using System.Linq;
    using Entities;
    using Interfaces;

    public class ScoringSagaLogger : IScoringSagaLogger
    {
        private readonly Lazy<ISagaLogRepository> _sagaLogRepository;
        private readonly Lazy<IScoringAndSegmentationSagaRepository> _scoringAndSegmentationSagaRepository;

        public ScoringSagaLogger(Lazy<ISagaLogRepository> sagaLogRepository,
            Lazy<IScoringAndSegmentationSagaRepository> scoringAndSegmentationSagaRepository)
        {
            this._sagaLogRepository = sagaLogRepository;
            this._scoringAndSegmentationSagaRepository = scoringAndSegmentationSagaRepository;
        }

        public ScoringSagaLog GetScoringSagaLog(int scoringBatchId, string state, string eventName, Guid correlationId, Guid processId)
        {
            ScoringSagaLog log = this._sagaLogRepository.Value.GetQueryable()
                .FirstOrDefault(x => x.State.Equals(state)
                                && x.Event.Equals(eventName)
                                && x.CorrelationId == correlationId
                                && x.ProcessId == processId);
            return log;
        }

        public void LogStartTime(int scoringBatchId, string state, string eventName, Guid correlationId, Guid processId)
        {
            ScoringSagaLog startTime = new ScoringSagaLog
            {
                ScoringBatchId = scoringBatchId,
                State = state,
                Event = eventName,
                CorrelationId = correlationId,
                ProcessId = processId,
                StartTime = DateTime.UtcNow
            };
            this._sagaLogRepository.Value.InsertOrUpdate(startTime);
        }

        public void LogEndTime(int scoringBatchId, string state, string eventName, Guid correlationId, Guid processId)
        {
            ScoringSagaLog log = this._sagaLogRepository.Value.GetQueryable().FirstOrDefault(x => x.State.Equals(state)
                                                                        && x.Event.Equals(eventName)
                                                                        && x.CorrelationId == correlationId
                                                                        && x.ProcessId == processId);
            if (log != null)
            {
                log.EndTime = DateTime.UtcNow;
                this._sagaLogRepository.Value.InsertOrUpdate(log);
            }
        }

        public ScoringAndSegmentationSaga GetScoringAndSegmentationSaga(Guid correlationId)
        {
            return this._scoringAndSegmentationSagaRepository.Value.GetScoringAndSegmentationSaga(correlationId);
        }

    }
}

﻿namespace Ivh.Domain.HospitalData.Scoring.Services
{
    using System;
    using Interfaces;

    public class ScoringMatchedGuarantorScoreHistoryService : IScoringMatchedGuarantorScoreHistoryService
    {
        private readonly Lazy<IScoringMatchedGuarantorScoreHistoryRepository> _scoringMatchedGuarantorScoreHistoryRepository;

        public ScoringMatchedGuarantorScoreHistoryService(
            Lazy<IScoringMatchedGuarantorScoreHistoryRepository> scoringMatchedGuarantorScoreHistoryRepository
            )
        {
            this._scoringMatchedGuarantorScoreHistoryRepository = scoringMatchedGuarantorScoreHistoryRepository;
        }

        public void GenerateMatchedGuarantorScoreHistory(int scoringBatchId)
        {
            this._scoringMatchedGuarantorScoreHistoryRepository.Value.GenerateMatchedGuarantorScoreHistory(scoringBatchId);
        }
    }
}
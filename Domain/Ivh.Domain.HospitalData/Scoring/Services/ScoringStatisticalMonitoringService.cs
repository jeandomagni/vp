﻿namespace Ivh.Domain.HospitalData.Scoring.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;
    using Segmentation.Interfaces;

    public class ScoringStatisticalMonitoringService : IScoringStatisticalMonitoringService
    {
        private readonly Lazy<IScoreRepository> _scoreRespository;
        private readonly Lazy<IBaseScoreRepository> _baseScoreRespository;
        private readonly Lazy<IScoringStatisticalMonitoringRepository> _scoringStatisticalMonitoringRepository;
        private readonly Lazy<IScoringBatchRepository> _scoringBatchRepository;
        private readonly Lazy<IThirdPartyDataRepository> _thirdPartyDataRepository;

        public ScoringStatisticalMonitoringService(
            Lazy<IScoreRepository> scoreRespository,
            Lazy<IBaseScoreRepository> baseScoreRespository,
            Lazy<IScoringStatisticalMonitoringRepository> scoringStatisticalMonitoringRepository,
            Lazy<IScoringBatchRepository> scoringBatchRepository,
            Lazy<IThirdPartyDataRepository> thirdPartyDataRepository)
        {
            this._scoreRespository = scoreRespository;
            this._baseScoreRespository = baseScoreRespository;
            this._scoringStatisticalMonitoringRepository = scoringStatisticalMonitoringRepository;
            this._scoringBatchRepository = scoringBatchRepository;
            this._thirdPartyDataRepository = thirdPartyDataRepository;
        }

        public ScoringStatisticalMonitoring RunDailyScoringStatisticalMonitoring(int minimumBatchSize)
        {
            ScoringBatch scoringBatch = this._scoringBatchRepository.Value.GetLatestScoringBatch();
            int scoringBatchId = scoringBatch.ScoringBatchId;
            IList<BaseScore> latestScoreBatch = this._baseScoreRespository.Value.GetScoresByBatchId(scoringBatchId);

            IList<ThirdPartyDataMonitoring> thirdPartyData = this._thirdPartyDataRepository.Value.GetThirdPartyDataByDate(scoringBatch.BatchStartDateTime);

            if (latestScoreBatch.Count < minimumBatchSize)
            {
                int weNeedTheseMoreScores = minimumBatchSize - latestScoreBatch.Count;
                IList<BaseScore> moreScores = this._baseScoreRespository.Value.GetTopScoresNotInScoringBatchId(weNeedTheseMoreScores, scoringBatchId);
                if (moreScores.Count > 0)
                {
                    latestScoreBatch.AddRange(moreScores);
                }
            }

            if (latestScoreBatch.Count > 0)
            {
                return this.RunScoringStatisticalMonitoring(latestScoreBatch, thirdPartyData, scoringBatchId, ScoringMonitoringTypeEnum.Daily);
            }

            return new ScoringStatisticalMonitoring();
        }

        public ScoringStatisticalMonitoring RunWeeklyScoringStatisticalMonitoring()
        {
            const int numberOfDaysBeforeToday = 6;
            IList<ScoringBatch> scoringBatches = this._scoringBatchRepository.Value.GetScoringBatchesForDays(numberOfDaysBeforeToday);
            DateTime minDate = scoringBatches.Min(x => x.BatchStartDateTime);
            DateTime maxDate = scoringBatches.Max(x => x.BatchStartDateTime);
            IList<ThirdPartyDataMonitoring> thirdPartyData = this._thirdPartyDataRepository.Value.GetThirdPartyDataByDates(minDate, maxDate);
            IList<BaseScore> latestScoreBatch = this._baseScoreRespository.Value.GetScoresByBatchIds(scoringBatches.Select(x => x.ScoringBatchId).ToList());
            if (latestScoreBatch.Count > 0)
            {
                return this.RunScoringStatisticalMonitoring(latestScoreBatch, thirdPartyData, null, ScoringMonitoringTypeEnum.Weekly);
            }

            return new ScoringStatisticalMonitoring();
        }

        private ScoringStatisticalMonitoring RunScoringStatisticalMonitoring(IList<BaseScore> latestScoreBatch, IList<ThirdPartyDataMonitoring> thirdPartyData, int? scoringBatchId, ScoringMonitoringTypeEnum scoringMonitoringTypeEnum)
        {
            ScoringStatisticalMonitoring scoringStatisticalMonitoring = new ScoringStatisticalMonitoring
            {
                ScoringMonitoringTypeId = (int)scoringMonitoringTypeEnum,
                ScoreCount = latestScoreBatch.Count,
                ScoringBatchId = scoringBatchId,

                GuarantorAllBalance30Days0Count = latestScoreBatch.Count(x => x.GuarantorAllBalance30Days == 0),
                GuarantorAllBalance30DaysMedian = this.CalculateMedian(latestScoreBatch.Select(x => x.GuarantorAllBalance30Days).ToList()),
                GuarantorAllBalance31To60Days0Count = latestScoreBatch.Count(x => x.GuarantorAllBalance31To60Days == 0),
                GuarantorAllBalance31To60DaysMedian = this.CalculateMedian(latestScoreBatch.Select(x => x.GuarantorAllBalance31To60Days).ToList()),
                GuarantorAllBalance61To90Days0Count = latestScoreBatch.Count(x => x.GuarantorAllBalance61To90Days == 0),
                GuarantorAllBalance61To90DaysMedian = this.CalculateMedian(latestScoreBatch.Select(x => x.GuarantorAllBalance61To90Days).ToList()),
                GuarantorAllBalance91To180Days0Count = latestScoreBatch.Count(x => x.GuarantorAllBalance91To180Days == 0),
                GuarantorAllBalance91To180DaysMedian = this.CalculateMedian(latestScoreBatch.Select(x => x.GuarantorAllBalance91To180Days).ToList()),
                GuarantorAllBalance181To270Days0Count = latestScoreBatch.Count(x => x.GuarantorAllBalance181To270Days == 0),
                GuarantorAllBalance181To270DaysMedian = this.CalculateMedian(latestScoreBatch.Select(x => x.GuarantorAllBalance181To270Days).ToList()),
                GuarantorAllBalance271To365Days0Count = latestScoreBatch.Count(x => x.GuarantorAllBalance271To365Days == 0),
                GuarantorAllBalance271To365DaysMedian = this.CalculateMedian(latestScoreBatch.Select(x => x.GuarantorAllBalance271To365Days).ToList()),
                GuarantorInactive120DaysBalance91To180Days0Count = latestScoreBatch.Count(x => x.GuarantorInactive120DaysBalance91To180Days == 0),
                GuarantorInactive120DaysBalance91To180DaysMedian = this.CalculateMedian(latestScoreBatch.Select(x => x.GuarantorInactive120DaysBalance91To180Days).ToList()),
                GuarantorInactive120DaysBalance181To270Days0Count = latestScoreBatch.Count(x => x.GuarantorInactive120DaysBalance181To270Days == 0),
                GuarantorInactive120DaysBalance181To270DaysMedian = this.CalculateMedian(latestScoreBatch.Select(x => x.GuarantorInactive120DaysBalance181To270Days).ToList()),
                GuarantorInactive120DaysBalance271To365Days0Count = latestScoreBatch.Count(x => x.GuarantorInactive120DaysBalance271To365Days == 0),
                GuarantorInactive120DaysBalance271To365DaysMedian = this.CalculateMedian(latestScoreBatch.Select(x => x.GuarantorInactive120DaysBalance271To365Days).ToList()),
                GuarantorPaidOffAccountsWPatientPaymentsNum30Days0Count = latestScoreBatch.Count(x => x.GuarantorPaidOffAccountsWPatientPaymentsNum30Days == 0),
                GuarantorPaidOffAccountsWPatientPaymentsNum30DaysMedian = this.CalculateMedian(latestScoreBatch.Select(x => x.GuarantorPaidOffAccountsWPatientPaymentsNum30Days).ToList().ConvertAll(x => (decimal)x)),
                GuarantorPaidOffAccountsWPatientPaymentsNum31To60Days0Count = latestScoreBatch.Count(x => x.GuarantorPaidOffAccountsWPatientPaymentsNum31To60Days == 0),
                GuarantorPaidOffAccountsWPatientPaymentsNum31To60DaysMedian = this.CalculateMedian(latestScoreBatch.Select(x => x.GuarantorPaidOffAccountsWPatientPaymentsNum31To60Days).ToList().ConvertAll(x => (decimal)x)),
                GuarantorPaidOffAccountsWPatientPaymentsNum61To90Days0Count = latestScoreBatch.Count(x => x.GuarantorPaidOffAccountsWPatientPaymentsNum61To90Days == 0),
                GuarantorPaidOffAccountsWPatientPaymentsNum61To90DaysMedian = this.CalculateMedian(latestScoreBatch.Select(x => x.GuarantorPaidOffAccountsWPatientPaymentsNum61To90Days).ToList().ConvertAll(x => (decimal)x)),
                GuarantorPaidOffAccountsWPatientPaymentsNum91To180Days0Count = latestScoreBatch.Count(x => x.GuarantorPaidOffAccountsWPatientPaymentsNum91To180Days == 0),
                GuarantorPaidOffAccountsWPatientPaymentsNum91To180DaysMedian = this.CalculateMedian(latestScoreBatch.Select(x => x.GuarantorPaidOffAccountsWPatientPaymentsNum91To180Days).ToList().ConvertAll(x => (decimal)x)),
                GuarantorPaidOffAccountsPatientPayments30Days0Count = latestScoreBatch.Count(x => x.GuarantorPaidOffAccountsPatientPayments30Days == 0),
                GuarantorPaidOffAccountsPatientPayments30DaysMedian = this.CalculateMedian(latestScoreBatch.Select(x => x.GuarantorPaidOffAccountsPatientPayments30Days).ToList()),
                GuarantorPaidOffAccountsPatientPayments31To60Days0Count = latestScoreBatch.Count(x => x.GuarantorPaidOffAccountsPatientPayments31To60Days == 0),
                GuarantorPaidOffAccountsPatientPayments31To60DaysMedian = this.CalculateMedian(latestScoreBatch.Select(x => x.GuarantorPaidOffAccountsPatientPayments31To60Days).ToList()),
                GuarantorPaidOffAccountsPatientPayments61To90Days0Count = latestScoreBatch.Count(x => x.GuarantorPaidOffAccountsPatientPayments61To90Days == 0),
                GuarantorPaidOffAccountsPatientPayments61To90DaysMedian = this.CalculateMedian(latestScoreBatch.Select(x => x.GuarantorPaidOffAccountsPatientPayments61To90Days).ToList()),
                GuarantorPaidOffAccountsPatientPayments91To180Days0Count = latestScoreBatch.Count(x => x.GuarantorPaidOffAccountsPatientPayments91To180Days == 0),
                GuarantorPaidOffAccountsPatientPayments91To180DaysMedian = this.CalculateMedian(latestScoreBatch.Select(x => x.GuarantorPaidOffAccountsPatientPayments91To180Days).ToList()),
                GuarantorPaymentsByDischarge30Days0Count = latestScoreBatch.Count(x => x.GuarantorPaymentsByDischarge30Days == 0),
                GuarantorPaymentsByDischarge30DaysMedian = this.CalculateMedian(latestScoreBatch.Select(x => x.GuarantorPaymentsByDischarge30Days).ToList()),
                GuarantorPaymentsByDischarge31To60Days0Count = latestScoreBatch.Count(x => x.GuarantorPaymentsByDischarge31To60Days == 0),
                GuarantorPaymentsByDischarge31To60DaysMedian = this.CalculateMedian(latestScoreBatch.Select(x => x.GuarantorPaymentsByDischarge31To60Days).ToList()),
                GuarantorPaymentsByDischarge61To90Days0Count = latestScoreBatch.Count(x => x.GuarantorPaymentsByDischarge61To90Days == 0),
                GuarantorPaymentsByDischarge61To90DaysMedian = this.CalculateMedian(latestScoreBatch.Select(x => x.GuarantorPaymentsByDischarge61To90Days).ToList()),
                GuarantorPaymentsByDischarge91To180Days0Count = latestScoreBatch.Count(x => x.GuarantorPaymentsByDischarge91To180Days == 0),
                GuarantorPaymentsByDischarge91To180DaysMedian = this.CalculateMedian(latestScoreBatch.Select(x => x.GuarantorPaymentsByDischarge91To180Days).ToList()),
                NoPriorData30DaysMean = this.CalculateMean(latestScoreBatch.Select(x => x.NoPriorData30Days).ToList()),
                NoPriorData31To60DaysMean = this.CalculateMean(latestScoreBatch.Select(x => x.NoPriorData31To60Days).ToList()),
                NoPriorData61To90DaysMean = this.CalculateMean(latestScoreBatch.Select(x => x.NoPriorData61To90Days).ToList()),
                NoPriorData91To180DaysMean = this.CalculateMean(latestScoreBatch.Select(x => x.NoPriorData91To180Days).ToList()),
                PatientType2NullCount = latestScoreBatch.Count(x => x.PatientType2 == null),
                PatientType2Mean = this.CalculatPatientType2Mean(latestScoreBatch),
                PrimaryInsuranceTypeNNullCount = latestScoreBatch.Count(x => x.PrimaryInsuranceTypeN == null),
                PrimaryInsuranceTypeNMean = this.CalculatePrimaryInsuranceTypeMean(latestScoreBatch.Select(x => x.PrimaryInsuranceTypeN).ToList()),
                PrimaryInsuranceTypeANullCount = latestScoreBatch.Count(x => x.PrimaryInsuranceTypeA == null),
                PrimaryInsuranceTypeAMean = this.CalculatePrimaryInsuranceTypeMean(latestScoreBatch.Select(x => x.PrimaryInsuranceTypeA).ToList()),
                PrimaryInsuranceType1NullCount = latestScoreBatch.Count(x => x.PrimaryInsuranceType1 == null),
                PrimaryInsuranceType1Mean = this.CalculatePrimaryInsuranceTypeMean(latestScoreBatch.Select(x => x.PrimaryInsuranceType1).ToList()),

                ThirdPartyDataCount = thirdPartyData.Count
            };

            if (thirdPartyData.Count > 0)
            {
                scoringStatisticalMonitoring.ProbabilityScoreMedian = this.CalculateMedian(thirdPartyData.Select(x => x.ProbabilityScore).ToList());
                scoringStatisticalMonitoring.OwnRentNullCount = thirdPartyData.Count(x => x.OwnRent == null);
                scoringStatisticalMonitoring.OwnRentMean = this.CalculateMean(thirdPartyData.Select(x => x.OwnRent).ToList().ConvertAll(x => (decimal?)x));
                scoringStatisticalMonitoring.HouseholdIncomeNullCount = thirdPartyData.Count(x => x.HouseholdIncome == null);
                scoringStatisticalMonitoring.HouseholdIncomeMean = this.CalculateMean(thirdPartyData.Select(x => x.HouseholdIncome).ToList());
                scoringStatisticalMonitoring.HouseholdMemberCountNullCount = thirdPartyData.Count(x => x.HouseholdMemberCount == null);
                scoringStatisticalMonitoring.HouseholdMemberCountMean = this.CalculateMean(thirdPartyData.Select(x => x.HouseholdMemberCount).ToList().ConvertAll(x => (decimal?)x));
            }

            this._scoringStatisticalMonitoringRepository.Value.InsertOrUpdate(scoringStatisticalMonitoring);
            return scoringStatisticalMonitoring;
        }

        private decimal CalculateMedian(IList<decimal?> list)
        {
            IList<decimal> noNullsList = this.GetNotNullDecimalList(list);
            return this.CalculateMedian(noNullsList);
        }

        private decimal CalculateMedian(IList<decimal> list)
        {
            int listCount = list.Count();
            int halfIndex = listCount / 2;
            List<decimal> sorted = list.OrderBy(n => n).ToList();

            if (listCount % 2 == 0)
            {
                return (sorted.ElementAt(halfIndex) + sorted.ElementAt(halfIndex - 1)) / 2;
            }
            return sorted.ElementAt(halfIndex);
        }

        private decimal CalculateMean(IList<bool> list)
        {
            int trues = list.Count(x => x);
            return (decimal)trues / list.Count;
        }

        private decimal CalculateMean(IList<decimal?> list)
        {
            IList<decimal> noNullsList = this.GetNotNullDecimalList(list);
            decimal sum = noNullsList.Sum();
            return sum / list.Count;
        }

        private decimal CalculatePrimaryInsuranceTypeMean(List<string> primaryInsuranceTypes)
        {
            // S = 0, AP = 1
            decimal apCount = primaryInsuranceTypes.Count(x => x != null && x.Equals("AP"));
            return apCount / primaryInsuranceTypes.Count;
        }

        private decimal CalculatPatientType2Mean(IList<BaseScore> latestScoreBatch)
        {
            // O = 0, I = 1, E = 2
            decimal totalCount = latestScoreBatch.Count;
            decimal iCount = latestScoreBatch.Count(x => x.PatientType2 == "I");
            decimal eCount = latestScoreBatch.Count(x => x.PatientType2 == "E");

            return (iCount + (eCount * 2)) / totalCount;
        }

        private IList<decimal> GetNotNullDecimalList(IList<decimal?> list)
        {
            return list.Where(x => x.HasValue).Select(x => (decimal)x).ToList();
        }
    }
}
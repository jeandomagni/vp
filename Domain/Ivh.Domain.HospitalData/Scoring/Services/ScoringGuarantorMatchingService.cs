﻿namespace Ivh.Domain.HospitalData.Scoring.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Base.Interfaces;
    using Base.Services;
    using Change.Interfaces;
    using Common.Base.Utilities.Extensions;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using Common.VisitPay.Messages.Scoring;
    using Entities;
    using Interfaces;
    using Logging.Interfaces;
    using NHibernate;
    using Segmentation.Interfaces;

    public class ScoringGuarantorMatchingService : DomainService, IScoringGuarantorMatchingService
    {
        private readonly Lazy<IChangeEventRepository> _changeEventRepository;
        private readonly Lazy<IMetricsProvider> _metricsProvider;
        private readonly Lazy<IMatchedGuarantorsVisitBatchRepository> _matchedGuarantorsVisitBatchRepository;
        private readonly Lazy<IScoringBatchRepository> _scoringBatchRepository;
        private readonly ISession _session;
        private readonly Lazy<IVisitBatchRepository> _visitBatchRepository;
        private readonly Lazy<ILoggingService> _loggingService;

        public ScoringGuarantorMatchingService(
            Lazy<IChangeEventRepository> changeEventRepository,
            Lazy<IMetricsProvider> metricsProvider,
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IMatchedGuarantorsVisitBatchRepository> matchedGuarantorsVisitBatchRepository,
            Lazy<IScoringBatchRepository> scoringBatchRepository,
            ISessionContext<CdiEtl> sessionContext,
            Lazy<IVisitBatchRepository> visitBatchRepository,
            Lazy<ILoggingService> loggingService
            ) : base(serviceCommonService)
        {
            this._changeEventRepository = changeEventRepository;
            this._metricsProvider = metricsProvider;
            this._matchedGuarantorsVisitBatchRepository = matchedGuarantorsVisitBatchRepository;
            this._scoringBatchRepository = scoringBatchRepository;
            this._session = sessionContext.Session;
            this._visitBatchRepository = visitBatchRepository;
            this._loggingService = loggingService;
        }

        public void ExecuteScoring()
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
                {
                    this._matchedGuarantorsVisitBatchRepository.Value.RunDailyGuarantorMatching(this.Client.Value.ScoringUseSskForGuarantorMatching);

                    int scoringBatchId = this._scoringBatchRepository.Value.GenerateScoringBatchId();

                    this._matchedGuarantorsVisitBatchRepository.Value.RunDailyBatchLoadSp(scoringBatchId,
                        this.Client.Value.ScoringMinimumHsCurrentBalanceToScore,
                        this.Client.Value.ScoringMinimumGuarantorBatchBalanceToScore,
                        this.Client.Value.ScoringMinimumGuarantorBalanceToScore);

                    this.InitialzeBatchLoad(scoringBatchId);

                    List<List<ScoringGuarantorDto>> scoringBatchRequests = this.ProcessGuarantorMatches(scoringBatchId);
                    this._session.Transaction.RegisterPostCommitSuccessAction((p, s) =>
                    {
                        this.Bus.Value.PublishMessage<ScoringAndSegmentationStartMessage, Guid>(new ScoringAndSegmentationStartMessage
                        {
                            ScoringGuarantorDtos = p,
                            StartTime = DateTime.UtcNow,
                            ScoringBatchId = s,
                            SegmentationEnable =  new SegmentationEnable
                            {
                                EnableBadDebtSegmentation = this.Client.Value.SegmentationEnableBadDebt,
                                EnableAccountsReceivableSegmentation = this.Client.Value.SegmentationEnableAccountsReceivables,
                                EnableActivePassiveSegmentation = this.Client.Value.SegmentationEnableActivePassive,
                                EnablePresumptiveCharitySegmentation = this.Client.Value.SegmentationEnablePresumptiveCharity
                            }
                        }).Wait();
                    }, scoringBatchRequests, scoringBatchId);

                    unitOfWork.Commit();
                }
            }
            catch (Exception e)
            {
                this._loggingService.Value.Fatal(() => $"Scoring error: {e}");
            }
        }

        private void InitialzeBatchLoad(int scoringBatchId)
        {
            List<VisitBatch> visitBatches = this._visitBatchRepository.Value.GetQueryable().Where(x => x.ScoringBatchId == scoringBatchId && x.TriggeredScoring).ToList();
            this._metricsProvider.Value.Increment(Metrics.Increment.Scoring.ChangeEventExpected, visitBatches.Count);

            IList<VisitBatch> ineligibleToScore = visitBatches.Where(x => !x.IsEligibleToScore).ToList();
            this._metricsProvider.Value.Increment(Metrics.Increment.Scoring.ChangeEventActual, ineligibleToScore.Count);

            this._changeEventRepository.Value.BulkUpdateIneligibleToScoreChangeEvents(scoringBatchId);
        }

        private List<List<ScoringGuarantorDto>> ProcessGuarantorMatches(int scoringBatchId)
        {
            IList<MatchedGuarantorsVisitBatch> matchedGuarantors = this._matchedGuarantorsVisitBatchRepository.Value.GetMatchedGuarantors(scoringBatchId);
            this._matchedGuarantorsVisitBatchRepository.Value.UpdateMatchedGuarantorsVisitBatchStatus(scoringBatchId, ScoringStatusEnum.InPhaseOne, null);
            List<List<ScoringGuarantorDto>> scoringBatchRequests = this.GenerateScoringBatchRequest(matchedGuarantors,
                this.ApplicationSettingsService.Value.ScoringBatchLoadBatchSize.Value);

            return scoringBatchRequests;
        }

        private List<List<ScoringGuarantorDto>> GenerateScoringBatchRequest(IList<MatchedGuarantorsVisitBatch> matchedGuarantors, int batchSize)
        {
            ILookup<int, VisitBatch> lookup = matchedGuarantors.ToLookup(p => p.ParentHsGuarantorId, p => p.VisitBatch);
            List<ScoringGuarantorDto> guarantorDtos = lookup.Select(d => new ScoringGuarantorDto { HsGuarantorId = d.Key, Visits = Mapper.Map<List<ScoringGuarantorVisitBatchDto>>(d) }).ToList();

            List<MatchedGuarantorsVisitBatch> triggereringVisits = matchedGuarantors.Where(x => x.VisitBatch.TriggeredScoring).ToList();
            this._metricsProvider.Value.Increment(Metrics.Increment.Scoring.ScoreExpected, triggereringVisits.Count);

            return guarantorDtos.ToList().BatchBy(batchSize);
        }

        public List<List<ScoringGuarantorDto>> ExecuteRetroScoring(DateTime scoringDate)
        {
            int scoringBatchId = this._scoringBatchRepository.Value.GenerateScoringBatchId();
            this._matchedGuarantorsVisitBatchRepository.Value.RunRetroBatchLoadSp(scoringBatchId, scoringDate, 0, 0, 0); // Need to ignore minimum balances since this is retroscoring
            this.InitialzeBatchLoad(scoringBatchId);
            return this.ProcessGuarantorMatches(scoringBatchId);
        }

        public void RunGuarantorMatchingForRetroScore(bool useSskForGuarantorMatching)
        {
            this._matchedGuarantorsVisitBatchRepository.Value.RunGuarantorMatchingForRetroScore(useSskForGuarantorMatching);
        }

    }

}

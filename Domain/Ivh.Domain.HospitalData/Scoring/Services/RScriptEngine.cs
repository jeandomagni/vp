﻿namespace Ivh.Domain.HospitalData.Scoring.Services
{
    using System;
    using System.IO;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using Interfaces;
    using Ivh.Application.HospitalData.Common.Interfaces;
    using RDotNet;

    public class RScriptEngine : IRscriptEngine
    {
        private const string OutputSymbol = "output";
        private static REngine _rengine;
        private static readonly object RengineLock = new object();
        private readonly Lazy<IScoringService> _scoringService;

        public RScriptEngine(Lazy<IScoringService> scoringService)
        {
            this._scoringService = scoringService;
        }

        public string ExecuteRScript(string inputJson, ScoringScriptStorage scoringScriptStorage)
        {
            string execution = "source('" + scoringScriptStorage.RwrapperPath.Replace("\\", "/") + "')";

            string result = "";
            string[] outputSymbolResult;
            lock (RengineLock)
            {
                GetInitializedEngine();

                string[] arguments = { scoringScriptStorage.SourceRScript, scoringScriptStorage.RdataUrls ?? "", inputJson };
                _rengine.EnableLock = true;
                _rengine.SetCommandLineArguments(arguments);
                this._scoringService.Value.LogInfo(() => $"Executing Rengine.Evaluate for input: {inputJson}");
                _rengine.Evaluate(execution);
                this._scoringService.Value.LogInfo(() => "Getting output symbol");
                outputSymbolResult = _rengine.GetSymbol(OutputSymbol).AsCharacter().ToArray();
                this._scoringService.Value.LogInfo(() => $"Output symbol: {outputSymbolResult[0]}");
                _rengine.ForceGarbageCollection();
            }
            if (outputSymbolResult != null && outputSymbolResult.Length > 0)
            {
                result = outputSymbolResult[0].Substring(outputSymbolResult[0].IndexOf("[", StringComparison.Ordinal));
            }
            if (string.IsNullOrEmpty(result))
            {
                throw new Exception($"Score result is null. Cannot find 'output' symbol from rscript. Please make sure the final json output is assigned to 'output' variable, eg, output <- ");
            }

            return result;
        }

        private static void GetInitializedEngine()
        {
            if (_rengine != null)
            {
                return;
            }
            string rhome = Environment.GetEnvironmentVariable("R_HOME");
            if (rhome != null)
            {
                string rpath = Environment.Is64BitProcess ? Path.Combine(rhome, @"bin\x64") : Path.Combine(rhome, @"bin\i386");
                REngine.SetEnvironmentVariables(rpath, rhome);
            }
            _rengine = REngine.GetInstance();
            _rengine.Initialize();
        }

    }
}

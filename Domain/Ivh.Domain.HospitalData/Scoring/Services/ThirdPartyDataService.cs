﻿namespace Ivh.Domain.HospitalData.Scoring.Services
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using AutoMapper;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Utilities.Extensions;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using Interfaces;
    using Entities;
    using Enums;
    using Logging.Interfaces;

    public class ThirdPartyDataService : DomainService, IThirdPartyDataService
    {
        private readonly Lazy<IMetricsProvider> _metricsProvider;
        private readonly Lazy<IGuarantorBatchRepository> _guarantorBatchRepository;
        private readonly Lazy<IThirdPartyDataRepository> _thirdPartyRepository;
        private readonly Lazy<IThirdPartyDataProvider> _thirdPartyDataProvider;
        private readonly Lazy<ILoggingService> _loggingService;


        public ThirdPartyDataService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IGuarantorBatchRepository> guarantorBatchRepository,
            Lazy<IThirdPartyDataRepository> thirdPartyRepository,
            Lazy<IThirdPartyDataProvider> thirdPartyDataProvider,
            Lazy<IMetricsProvider> metricsProvider,
            Lazy<ILoggingService> loggingService) : base(serviceCommonService)
        {
            this._guarantorBatchRepository = guarantorBatchRepository;
            this._thirdPartyRepository = thirdPartyRepository;
            this._thirdPartyDataProvider = thirdPartyDataProvider;
            this._metricsProvider = metricsProvider;
            this._loggingService = loggingService;
        }

        public ScoringThirdPartyDto ExtractThirdParty(int hsGuarantorId, bool required, int scoringBatchId, ScoringThirdPartyDatasetEnum dataSet, ScoringThirdPartyDataSetTypeEnum dataSetType)
        {
            return this.ExtractThirdParty(null, required, hsGuarantorId, scoringBatchId, dataSet, dataSetType);
        }

        public ScoringThirdPartyDto ExtractThirdParty(GuarantorBatch guarantorBatch, bool required, ScoringThirdPartyDatasetEnum dataSet, ScoringThirdPartyDataSetTypeEnum dataSetType, bool requireThirdPartyExtract)
        {
            return this.ExtractThirdParty(guarantorBatch, required, -1, -1, dataSet, dataSetType, requireThirdPartyExtract);
        }

        public IList<int> GetValidPcThirdPartyData(int thirdPartyExpireDays)
        {
            IList<int> pcList = this._thirdPartyRepository.Value.GetThirdPartyDataSet(thirdPartyExpireDays, ScoringThirdPartyDatasetEnum.PresumptiveCharity, ScoringThirdPartyDatasetEnum.ScoringAndSegmentation);

            return pcList;
        }

        private ScoringThirdPartyDto ExtractThirdParty(GuarantorBatch guarantorBatch, bool required, int hsGuarantorId, int scoringBatchId, ScoringThirdPartyDatasetEnum dataSet, ScoringThirdPartyDataSetTypeEnum dataSetType, bool requireThirdPartyCheck=true)
        {
            GuarantorBatch guarantor = guarantorBatch ?? this._guarantorBatchRepository.Value.GetByGuarantorIdScoringBatchId(hsGuarantorId, scoringBatchId);
            ThirdPartyRequestInput requestInput = this.GenerateThirdPartyRequestInput(guarantor, ThirdPartyProviderEnum.America, dataSet);
            ThirdPartyData thirdPartyData = this.GetAvailableThirdPartyData(requestInput, dataSetType, requireThirdPartyCheck);
            ScoringThirdPartyDto thirdPartyDataDto = null;
            if (thirdPartyData == null && required)
            {
                if (!this.ValidRequestInput(requestInput))
                {
                    this._loggingService.Value.Info(() => $"Third party request input is invalid: {requestInput.HsGuarantorId}: " +
                                                    $"FirstName={requestInput.FirstName}" +
                                                    $"LastName={requestInput.LastName}" +
                                                    $"AddressLIne1={requestInput.AddressLine1}" +
                                                    $"City={requestInput.City}" +
                                                    $"State={requestInput.State}" +
                                                    $"Zip={requestInput.ZipCode}");
                    this._metricsProvider.Value.Increment(Metrics.Increment.Scoring.ThirdPartyDataInvalidRequestInput);
                    return null;
                }
                string thirdPartyUrl = this.ApplicationSettingsService.Value.ScoringThirdPartyUrl.Value;
                try
                {
                    thirdPartyData = Task.Run(() => this._thirdPartyDataProvider.Value.CallThirdPartyDataService(requestInput, thirdPartyUrl)).Result;
                }
                catch (Exception e)
                {
                    this._loggingService.Value.Info(() => $"Third Party API access failure: {e}");
                }
                if (thirdPartyData != null)
                {
                    this._thirdPartyRepository.Value.InsertStateless(thirdPartyData);
                    this._metricsProvider.Value.Increment(thirdPartyData.ProbabilityScore > 0 ? "Scoring.ThirdPartyDataFromAmericaValid" : "Scoring.ThirdPartyDataFromAmericaInvalid");
                }
                else
                {
                    this._metricsProvider.Value.Increment(Metrics.Increment.Scoring.ThirdPartyDataFromAmericaInvalid);
                }
            }
            else if (thirdPartyData != null)
            {
                if (thirdPartyData.HsGuarantorId != requestInput.HsGuarantorId)
                {
                    ThirdPartyData newThirdPartyData = Mapper.Map<ThirdPartyData>(thirdPartyData);
                    newThirdPartyData.ThirdPartyDataId = 0;
                    newThirdPartyData.HsGuarantorId = requestInput.HsGuarantorId;
                    this._thirdPartyRepository.Value.InsertOrUpdate(newThirdPartyData);
                    thirdPartyDataDto = Mapper.Map<ScoringThirdPartyDto>(newThirdPartyData);
                }
                this._metricsProvider.Value.Increment(Metrics.Increment.Scoring.ThirdPartyDataFromDB);
            }
            if (thirdPartyData != null)
            {
                thirdPartyDataDto = thirdPartyDataDto ?? Mapper.Map<ScoringThirdPartyDto>(thirdPartyData);
                thirdPartyDataDto.OwnRent = thirdPartyData.OwnRent ?? -1;
                thirdPartyDataDto.HouseholdMemberCount = thirdPartyData.HouseholdMemberCount ?? -1;
                thirdPartyDataDto.HsGuarantorId = requestInput.HsGuarantorId;
            }
            return thirdPartyDataDto;
        }

        private bool ValidRequestInput(ThirdPartyRequestInput requestInput)
        {
            bool cityStateValid = !(string.IsNullOrEmpty(requestInput.City) || string.IsNullOrEmpty(requestInput.State));
            bool zipValid = !string.IsNullOrEmpty(requestInput.ZipCode);

            bool nameAddressValid = !(string.IsNullOrEmpty(requestInput.FirstName) ||
                                      string.IsNullOrEmpty(requestInput.LastName) ||
                                      string.IsNullOrEmpty(requestInput.AddressLine1));

            return nameAddressValid && (cityStateValid || zipValid);
        }

        private ThirdPartyData GetAvailableThirdPartyData(ThirdPartyRequestInput thirdPartyRequestInput, ScoringThirdPartyDataSetTypeEnum dataSetType, bool requireThirdPartyCheck)
        {
            ThirdPartyData thirdPartyData = null;
            if (requireThirdPartyCheck)
            {
                thirdPartyData = this._thirdPartyRepository.Value.GetThirdPartyData(thirdPartyRequestInput.HsGuarantorId, dataSetType, this.Client.Value.ThirdPartyRefreshAge);
            }

            if (thirdPartyData == null )
            {
                return this._thirdPartyRepository.Value.GetThirdPartyDataByRequestInput(thirdPartyRequestInput, dataSetType, this.Client.Value.ThirdPartyRefreshAge);
            }

            return thirdPartyData;
        }

        private ThirdPartyRequestInput GenerateThirdPartyRequestInput(GuarantorBatch guarantor, ThirdPartyProviderEnum thirdPartyProvider, ScoringThirdPartyDatasetEnum dataset)
        {
            return new ThirdPartyRequestInput
            {
                HsGuarantorId = guarantor.ChildHsGuarantorId,
                ThirdPartyProvider = (int) thirdPartyProvider,
                RequestType = "S", // This is the only allowable request type
                SecurityToken = this.ApplicationSettingsService.Value.ScoringThirdPartySecurityToken.Value,
                FirstName = guarantor.FirstName ?? string.Empty,
                LastName = guarantor.LastName ?? string.Empty,
                AddressLine1 = guarantor.Address1 ?? string.Empty,
                City = guarantor.City ?? string.Empty,
                State = guarantor.StateProvince ?? string.Empty,
                ZipCode = guarantor.PostalCode ?? string.Empty,
                MessageId = this.ApplicationSettingsService.Value.Client.Value.ToUpper(),
                VariableList = "Y",
                Userfield1 = (int)dataset
            };
        }
    }
}

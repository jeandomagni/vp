﻿namespace Ivh.Domain.HospitalData.Scoring.Services
{
    using System;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;

    public class ScoringMonitoringThresholdsService : IScoringMonitoringThresholdsService
    {
        private readonly Lazy<IScoringMonitoringThresholdsRepository> _scoringMonitoringThresholdsRepository;

        public ScoringMonitoringThresholdsService(
            Lazy<IScoringMonitoringThresholdsRepository> scoringMonitoringThresholdsRepository
            )
        {
            this._scoringMonitoringThresholdsRepository = scoringMonitoringThresholdsRepository;
        }
        public ScoringMonitoringThresholds GetDailyScoringMonitoringThresholds()
        {
            return this._scoringMonitoringThresholdsRepository.Value.GetScoringMonitoringThresholds(ScoringMonitoringTypeEnum.Daily);
        }
        public ScoringMonitoringThresholds GetWeeklyScoringMonitoringThresholds()
        {
            return this._scoringMonitoringThresholdsRepository.Value.GetScoringMonitoringThresholds(ScoringMonitoringTypeEnum.Weekly);
        }
    }
}
﻿namespace Ivh.Domain.HospitalData.Scoring.Services
{
    using System;
    using System.Collections.Generic;
    using Entities;
    using Interfaces;
    public class RetroScoreService : IRetroScoreService
    {
        private readonly Lazy<IRetroScoreRepository> _retroScoreRepository;

        public RetroScoreService (
            Lazy<IRetroScoreRepository> retroScoreRepository
            )
        {
            this._retroScoreRepository = retroScoreRepository;
        }

        public void SaveRetroScores(IList<RetroScore> calculatedInputs)
        {
            this._retroScoreRepository.Value.BulkInsert(calculatedInputs);
        }
    }
}
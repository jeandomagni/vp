﻿namespace Ivh.Domain.HospitalData.Scoring.Services
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using AutoMapper;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using Entities;
    using Enums;
    using Interfaces;
    using Ivh.Application.HospitalData.Common.Interfaces;
    using Logging.Interfaces;
    using Newtonsoft.Json;

    public class ScoringService : DomainService, IScoringService
    {
        private readonly Lazy<IRscriptEngine> _rscriptEngine;
        private readonly Lazy<ILoggingService> _loggingService;
        private readonly Lazy<IScoreRepository> _scoreRepository;

        private const string NullValueForR = "ZZZ";

        private readonly bool _enableScoringLogInfo;

        public ScoringService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IRscriptEngine> rscriptEngine,
            Lazy<ILoggingService> loggingService,
            Lazy<IScoreRepository> scoreRepository) : base(serviceCommonService)
        {
            this._rscriptEngine = rscriptEngine;
            this._loggingService = loggingService;
            this._scoreRepository = scoreRepository;
            this._enableScoringLogInfo = this.Client.Value.ScoringLogInfoEnable;
        }

        public void InitializeMapping()
        {
            Mapper.Initialize(x => x.AddProfile<Mappings.AutomapperProfile>());
        }

        public IList<PtpScore> GetGuarantorPtpScore(Dictionary<string, List<int>> hsGuarantors)
        {
            return this._scoreRepository.Value.GetPtpScore(hsGuarantors);
        }

        public ScoringResult GenerateGuarantorPtpScore(ScoringPtpScoreRequest scoringPtpScoreInput, ScoringScriptStorage scoringScriptStorage)
        {
            this.LogInfo(() => "Start executing ptp scoring rscript");
            string ptpScoreInput = JsonConvert.SerializeObject(scoringPtpScoreInput.PtpScoreInput);
            this.LogInfo(() => $"Start ExecuteRScript: {scoringScriptStorage.SourceRScript} {ptpScoreInput}");
            string scoringResult = this._rscriptEngine.Value.ExecuteRScript(ptpScoreInput, scoringScriptStorage);
            this.LogInfo(() => "Finished ExecuteRScript");
            IList<ScoringGuarantorScore> scoreResults = this.WrapScoreResults(scoringResult);
            this.LogInfo(() => "Finished executing ptp scoring rscript");
            ScoringResult ptpScoreResult = new ScoringResult
            {
                SourceInputs = scoringPtpScoreInput.PtpScoreInput.ScoringServiceInputs,
                CalculatedInputs = scoringPtpScoreInput.PtpScoreInput.CalculatedInputs,
                GuarantorScores = scoreResults,
                ThirdPartyDtos = scoringPtpScoreInput.PtpScoreInput.ThirdPartyData.ToList()
            };

            return ptpScoreResult;
        }

        public ScoringResult GenerateGuarantorProtoScores(IList<ScoringServiceInput> scoringServiceInputs, ScoringScriptStorage scoringScriptStorage)
        {

            // Calculate scoring vars from rawInput
            this.LogInfo(() => "Start calculation of proto scoring input");
            IList<ScoringCalculatedInput> calculatedInputs = this.CalculateScoringInput(scoringServiceInputs);
            if (calculatedInputs.Count <= 0)
            {
                return null;
            }
            this.LogInfo(() => "Finished calculation of proto scoring input");

            // Run script to generate score
            this.LogInfo(() => "Start executing proto scoring rscript");
            string protoScoreInput = JsonConvert.SerializeObject(calculatedInputs);
            string scoringResult = this._rscriptEngine.Value.ExecuteRScript(protoScoreInput, scoringScriptStorage);
            this.LogInfo(() => "Finished executing proto scoring rscript");
            IList<ScoringGuarantorScore> scoreResults = this.WrapScoreResults(scoringResult);

            ScoringResult sr = new ScoringResult
            {
                SourceInputs = scoringServiceInputs,
                CalculatedInputs = calculatedInputs,
                GuarantorScores =scoreResults
            };

            return sr;
        }

        private IList<ScoringGuarantorScore> WrapScoreResults(string scoreResults)
        {
            // scoreResults should be converted to have the following json format:
            //[
            //  {
            //    "HsGuarantorId": 20354,
            //    "VisitId": 12061,
            //    "PtpScore": 0,
            //    "ProtoScore": 107,
            //    "S1Version": "Version 1",
            //    "S2Version": "Version 2",
            //  },
            //  {
            //    "HsGuarantorId": 20354,
            //    "VisitId": 12061,
            //    "PtpScore": 0,
            //    "ProtoScores": 108
            //    "S1Version": "Version 1",
            //    "S2Version": "Version 2",
            //  }
            //]
            IList<RScoringResult> rscores = JsonConvert.DeserializeObject<List<RScoringResult>>(scoreResults);
            IList<ScoringGuarantorScore> scoringOutputs = ResolveScoringOutput(rscores);
            return scoringOutputs;
        }

        private static IList<ScoringGuarantorScore> ResolveScoringOutput(IEnumerable<RScoringResult> rscores)
        {
            IList<ScoringGuarantorScore> scoringOutputs = new List<ScoringGuarantorScore>();
            foreach (RScoringResult rscore in rscores)
            {
                ScoringGuarantorScore scoringOutput = scoringOutputs.FirstOrDefault(x => x.HsGuarantorId == rscore.HsGuarantorId && x.VisitId == rscore.VisitId);
                if (scoringOutput == null)
                {
                    scoringOutput = new ScoringGuarantorScore
                    {
                        HsGuarantorId = rscore.HsGuarantorId,
                        VisitId = rscore.VisitId,
                        PtpScore = rscore.PtpScore,
                        Am1Flag = rscore.Am1Flag,
                        S1Version = rscore.S1Version,
                        S2Version = rscore.S2Version,
                        ProtoScore = new ProtoScore
                        {
                            ProtoScore1 = rscore.ProtoScore1,
                            ProtoScore2 = rscore.ProtoScore2
                        }
                    };
                }
                scoringOutputs.Add(scoringOutput);
            }

            return scoringOutputs;
        }

        public IList<ScoringCalculatedInput> CalculateScoringInput(IList<ScoringServiceInput> scoringServiceInputs)
        {
            IList<ScoringCalculatedInput> calculatedInputs = new List<ScoringCalculatedInput>();
            foreach (ScoringServiceInput input in scoringServiceInputs)
            {
                ScoringCalculatedInput calculatedInput = new ScoringCalculatedInput { HsGuarantorId = input.HsGuarantorId };
                List<Visit> triggeringAccounts = GetTriggeringVisits(input.GuarantorAccountVisits);
                if (triggeringAccounts == null || triggeringAccounts.Count == 0)
                {
                    continue;
                }

                calculatedInput.Client = this.Client.Value.ClientName;
                calculatedInput.VisitId = triggeringAccounts[0].VisitId;
                calculatedInput.ScoringBatchId = triggeringAccounts[0].ScoringBatchId;
                calculatedInput.PatientType2 = PatientType(triggeringAccounts);
                calculatedInput.FirstSelfPayDate = triggeringAccounts[0].FirstSelfPayDate;
                calculatedInput.ScoringDate = triggeringAccounts[0].ScoringDate;
                calculatedInput.BillingSystemId = triggeringAccounts[0].BillingSystemId;
                calculatedInput.BillingSystemName = triggeringAccounts[0].BillingSystemName;
                calculatedInput.FacilityId = triggeringAccounts[0].FacilityId;
                calculatedInput.FacilityDescription = triggeringAccounts[0].FacilityDescription;
                calculatedInput.StateCode = triggeringAccounts[0].StateCode;
                calculatedInput.RegionId = triggeringAccounts[0].RegionId;
                calculatedInput.RegionScope = triggeringAccounts[0].RegionScope;
                calculatedInput.SourceSystemKey = triggeringAccounts[0].SourceSystemKey;

                GenerateNoPriorData(calculatedInput, input.GuarantorAccountVisits);
                PrimaryInsuranceType(calculatedInput, triggeringAccounts);

                foreach (GuarantorAccountVisit visit in input.GuarantorAccountVisits)
                {
                    visit.ScoringDate = triggeringAccounts[0].FirstSelfPayDate;
                    if (visit.TriggeredScoring && IsNullDate(visit.FirstSelfPayDate))
                    {
                        visit.FirstSelfPayDate = visit.ScoringDate;
                    }
                    calculatedInput.SelfPayAllBalance += visit.TriggeredScoring ? visit.HsCurrentBalance : 0;
                    GenerateGuarantorAllBalance(calculatedInput, visit);
                    GenerateGuarantorPaidOffAccountsWPatientPaymentsNum(calculatedInput, visit);

                    if (!calculatedInput.ScoringAccountHasPayorTrans)
                    {
                        calculatedInput.ScoringAccountHasPayorTrans = ScoringAccountHasPayorTrans(visit);
                    }
                    GenerateGuarantorInactive120DaysBalance(calculatedInput, visit);
                    GenerateGuarantorPaidOffAccountsPatientPayments(calculatedInput, visit);
                    GenerateGuarantorPaymentsByDischarge(calculatedInput, visit);
                }
                calculatedInputs.Add(calculatedInput);
            }
            return calculatedInputs;
        }

        public Stopwatch LogInfo(Func<string> message, bool startTimer=false)
        {
            if (this._enableScoringLogInfo)
            {
                Stopwatch sw = null;
                if (startTimer)
                {
                    sw = new Stopwatch();
                    sw.Start();
                }
                this._loggingService.Value.Info(message);
                return sw;
            }

            return null;
        }

        public void LogInfoEndTimer(Func<string> message, Stopwatch sw)
        {
            if (this._enableScoringLogInfo)
            {
                Func<string> endMessage = message;
                if (sw != null)
                {
                    sw.Stop();
                    endMessage = () => $"{message} {sw.Elapsed.Minutes:00}:{sw.Elapsed.Seconds:00}.{sw.ElapsedMilliseconds / 10:00}";
                }
                this._loggingService.Value.Info(endMessage);
            }
        }

        private static bool IsNullDate(DateTime date)
        {
            return date == DateTime.MinValue;
        }

        private static List<Visit> GetTriggeringVisits(IList<GuarantorAccountVisit> visits)
        {
            return visits
                .Where(x => x.TriggeredScoring)
                .Select(x => new Visit
                {
                    VisitId = x.VisitId,
                    ScoringBatchId = x.ScoringBatchId,
                    BillingApplication = x.BillingApplication,
                    SelfPayClassId = x.SelfPayClassId,
                    FirstSelfPayDate = x.FirstSelfPayDate == DateTime.MinValue ? DateTime.UtcNow : x.FirstSelfPayDate,
                    ScoringDate = DateTime.UtcNow,
                    PatientTypeId = x.PatientTypeId,
                    FacilityId = x.FacilityId,
                    FacilityDescription = x.FacilityDescription,
                    RegionScope = x.RegionScope,
                    RegionId = x.RegionId,
                    StateCode = x.StateCode,
                    BillingSystemId = x.BillingSystemId,
                    BillingSystemName = x.BillingSystemName,
                    SourceSystemKey = x.SourceSystemKey
                })
                .OrderByDescending(x => x.FirstSelfPayDate)
                .ToList();
        }

        private static void GenerateGuarantorAllBalance(ScoringCalculatedInput scoringInput, GuarantorAccountVisit visit)
        {
            scoringInput.GuarantorAllBalance30Days += GuarantorBalance(visit, 0, -30);
            scoringInput.GuarantorAllBalance31To60Days += GuarantorBalance(visit, -31, -60);
            scoringInput.GuarantorAllBalance61To90Days += GuarantorBalance(visit, -61, -90);
            scoringInput.GuarantorAllBalance91To180Days += GuarantorBalance(visit, -91, -180);
            scoringInput.GuarantorAllBalance181To270Days += GuarantorBalance(visit, -181, -270);
            scoringInput.GuarantorAllBalance271To365Days += GuarantorBalance(visit, -271, -365);
        }

        private static void GenerateGuarantorInactive120DaysBalance(ScoringCalculatedInput scoringInput, GuarantorAccountVisit visit)
        {
            scoringInput.GuarantorInactive120DaysBalance91To180Days += GuarantorInactiveBalance(visit, -91, -180);
            scoringInput.GuarantorInactive120DaysBalance181To270Days += GuarantorInactiveBalance(visit, -181, -270);
            scoringInput.GuarantorInactive120DaysBalance271To365Days += GuarantorInactiveBalance(visit, -271, -365);
        }

        private static void GenerateGuarantorPaidOffAccountsWPatientPaymentsNum(ScoringCalculatedInput scoringInput, GuarantorAccountVisit visit)
        {
            scoringInput.GuarantorPaidOffAccountsWPatientPaymentsNum30Days += GuarantorPaidOffAccountsWPatientPaymentsNum(visit, 0, -30);
            scoringInput.GuarantorPaidOffAccountsWPatientPaymentsNum31To60Days += GuarantorPaidOffAccountsWPatientPaymentsNum(visit, -31, -60);
            scoringInput.GuarantorPaidOffAccountsWPatientPaymentsNum61To90Days += GuarantorPaidOffAccountsWPatientPaymentsNum(visit, -61, -90);
            scoringInput.GuarantorPaidOffAccountsWPatientPaymentsNum91To180Days += GuarantorPaidOffAccountsWPatientPaymentsNum(visit, -91, -180);
        }

        private static void GenerateGuarantorPaidOffAccountsPatientPayments(ScoringCalculatedInput scoringInput, GuarantorAccountVisit visit)
        {
            scoringInput.GuarantorPaidOffAccountsPatientPayments30Days += GuarantorPaidOffAccountsPatientPayments(visit, 0, -30);
            scoringInput.GuarantorPaidOffAccountsPatientPayments31To60Days += GuarantorPaidOffAccountsPatientPayments(visit, -31, -60);
            scoringInput.GuarantorPaidOffAccountsPatientPayments61To90Days += GuarantorPaidOffAccountsPatientPayments(visit, -61, -90);
            scoringInput.GuarantorPaidOffAccountsPatientPayments91To180Days += GuarantorPaidOffAccountsPatientPayments(visit, -91, -180);
        }

        private static void GenerateGuarantorPaymentsByDischarge(ScoringCalculatedInput scoringInput, GuarantorAccountVisit visit)
        {
            scoringInput.GuarantorPaymentsByDischarge30Days += GuarantorPaymentsByDischarge(visit, 0, -30);
            scoringInput.GuarantorPaymentsByDischarge31To60Days += GuarantorPaymentsByDischarge(visit, -31, -60);
            scoringInput.GuarantorPaymentsByDischarge61To90Days += GuarantorPaymentsByDischarge(visit, -61, -90);
            scoringInput.GuarantorPaymentsByDischarge91To180Days += GuarantorPaymentsByDischarge(visit, -91, -180);
        }

        private static void GenerateNoPriorData(ScoringCalculatedInput scoringInput, IList<GuarantorAccountVisit> visits)
        {
            scoringInput.NoPriorData30Days = !DataExists(visits, 0, -30);
            scoringInput.NoPriorData31To60Days = !DataExists(visits, -31, -60);
            scoringInput.NoPriorData61To90Days = !DataExists(visits, -61, -90);
            scoringInput.NoPriorData91To180Days = !DataExists(visits, -91, -180);
        }


        private static void PrimaryInsuranceType(ScoringCalculatedInput scoringInput, List<Visit> triggeredVisits)
        {
            if (triggeredVisits[0]?.SelfPayClassId == 0)
            {
                scoringInput.PrimaryInsuranceTypeN = NullValueForR;
                scoringInput.PrimaryInsuranceTypeA = NullValueForR;
                scoringInput.PrimaryInsuranceType1 = NullValueForR;
                return;
            }

            string insurance = "AP";
            string selfPay = "S";

            if (triggeredVisits.Count == 1)
            {
                string result = triggeredVisits[0].SelfPayClassEnum == SelfPayClassEnum.PureSelfPay ? selfPay : insurance;
                scoringInput.PrimaryInsuranceTypeN = result;
                scoringInput.PrimaryInsuranceTypeA = result;
                scoringInput.PrimaryInsuranceType1 = result;
            }
            else
            {
                DateTime firstSelfPayDate = triggeredVisits[0].FirstSelfPayDate.Date;
                bool hasInsuranceVisits = triggeredVisits.Any(x => x.SelfPayClassEnum.IsInCategory(SelfPayClassEnumCategory.HasInsurance) && x.FirstSelfPayDate.Date == firstSelfPayDate);
                bool hasSelfPayVisits = triggeredVisits.Any(x => x.SelfPayClassEnum.IsInCategory(SelfPayClassEnumCategory.Uninsured) && x.FirstSelfPayDate.Date == firstSelfPayDate);

                // use the insurance type of the most recent account based on fisrtSelfPay date
                // if there are more than 1 most recent accounts, and they differ, set to AP
                if (hasInsuranceVisits && hasSelfPayVisits)
                {
                    scoringInput.PrimaryInsuranceTypeN = insurance;
                }
                else
                {
                    scoringInput.PrimaryInsuranceTypeN = hasInsuranceVisits ? insurance : selfPay;
                }

                hasInsuranceVisits = triggeredVisits.Any(x => x.SelfPayClassEnum.IsInCategory(SelfPayClassEnumCategory.HasInsurance));
                hasSelfPayVisits = triggeredVisits.Any(x => x.SelfPayClassEnum.IsInCategory(SelfPayClassEnumCategory.Uninsured));
                // set to S if and only if all acounts are S, else set to AP
                if (hasSelfPayVisits && !hasInsuranceVisits)
                {
                    scoringInput.PrimaryInsuranceTypeA = selfPay;
                }
                else
                {
                    scoringInput.PrimaryInsuranceTypeA = insurance;
                }

                //set to AP if and only if all accounts are AP, else set to S
                if (!hasSelfPayVisits && hasInsuranceVisits)
                {
                    scoringInput.PrimaryInsuranceType1 = insurance;
                }
                else
                {
                    scoringInput.PrimaryInsuranceType1 = selfPay;
                }
            }
        }

        private static string PatientType(List<Visit> triggeredVisits)
        {
            if (triggeredVisits.Any(x => x.PatientTypeEnum == PatientTypeEnum.Emergency))
            {
                return "E";
            }

            if (triggeredVisits.Any(x => x.PatientTypeEnum == PatientTypeEnum.Inpatient))
            {
                return "I";
            }

            if (triggeredVisits.Any(x => x.PatientTypeEnum == PatientTypeEnum.Outpatient))
            {
                return "O";
            }

            return NullValueForR;
        }

        private static bool ScoringAccountHasPayorTrans(GuarantorAccountVisit visit)
        {
            // Check for Payor Payments After Discharge
            decimal payorPaymentsAfterChildDischarge = visit.GuarantorAccountTransactions.Where(x => x.VpTransactionTypeEnum == VpTransactionTypeEnum.HsPayorCash && x.PostDate.Date > visit.DischargeDate.Date && visit.TriggeredScoring)
                .Sum(x => x.TransactionAmount);
            if (payorPaymentsAfterChildDischarge != 0)
            {
                return true;
            }

            // Check for Payor Adjustments After Discharge
            return visit.GuarantorAccountTransactions.Where(x => x.VpTransactionTypeEnum == VpTransactionTypeEnum.HsPayorNonCash && x.PostDate.Date > visit.DischargeDate.Date && visit.TriggeredScoring)
                    .Sum(x => x.TransactionAmount) != 0;
        }

        private static decimal GuarantorBalance(GuarantorAccountVisit visit, int daysFrom, int daysTo)
        {
            if (DischargedBetweenDates(visit.DischargeDate.Date, visit.ScoringDate.Date, daysFrom, daysTo) &&
                !visit.TriggeredScoring)
            {
                return visit.HsCurrentBalance;
            }
            return 0;
        }

        private static decimal GuarantorInactiveBalance(GuarantorAccountVisit visit, int daysFrom, int daysTo)
        {
            if (DischargedBetweenDates(visit.DischargeDate.Date, visit.ScoringDate.Date, daysFrom, daysTo) &&
                visit.HsCurrentBalance > 10 &&
                IsInactive120DaysBalance(visit))
            {
                return visit.HsCurrentBalance;
            }
            return 0;
        }

        private static bool IsInactive120DaysBalance(GuarantorAccountVisit visit)
        {
            // get active/inactive flag
            // consider inactive if there were no patient payments in last 120 days, AND no charges or payor activity in last 120 days 
            // (charges/payor activity would indicate the account was not self-pay yet or that there was perhaps late activity)
            bool active120DaysFlag = false;
            if (visit.DischargeDate.Date >= visit.ScoringDate.AddDays(-120).Date)
            {
                active120DaysFlag = true;
            }
            else
            {
                // if charge, payor payment, or payor adjustment in last 120 days (regardless of sign) then active 
                if ((visit.GuarantorAccountTransactions.Any(x => x.PostDate.Date > visit.ScoringDate.AddDays(-120).Date) && visit.GuarantorAccountTransactions.Any(x => x.VpTransactionTypeEnum == VpTransactionTypeEnum.HsPayorCash))
                    || (visit.GuarantorAccountTransactions.Any(x => x.PostDate.Date > visit.ScoringDate.AddDays(-120).Date) && visit.GuarantorAccountTransactions.Any(x => x.VpTransactionTypeEnum == VpTransactionTypeEnum.HsPayorNonCash))
                    || (visit.GuarantorAccountTransactions.Any(x => x.PostDate.Date > visit.ScoringDate.AddDays(-120).Date) && visit.GuarantorAccountTransactions.Any(x => x.VpTransactionTypeEnum == VpTransactionTypeEnum.HsCharge))
                    || (visit.GuarantorAccountTransactions.Any(x => x.PostDate.Date > visit.ScoringDate.AddDays(-120).Date) && visit.GuarantorAccountTransactions.Any(x => x.VpTransactionTypeEnum.IsInCategory(VpTransactionTypeEnumCategory.ScoringPatientCash))
                   ))
                {
                    active120DaysFlag = true;
                }
            }

            return !active120DaysFlag;
        }

        private static int GuarantorPaidOffAccountsWPatientPaymentsNum(GuarantorAccountVisit visit, int daysFrom, int daysTo)
        {
            if (DischargedBetweenDates(visit.DischargeDate.Date, visit.ScoringDate.Date, daysFrom, daysTo) &&
                !visit.TriggeredScoring &&
                visit.HsCurrentBalance == 0 &&
                visit.GuarantorAccountTransactions.Any(x => x.VpTransactionTypeEnum.IsInCategory(VpTransactionTypeEnumCategory.ScoringPatientCash)))
            {
                return 1;
            }
            return 0;
        }

        private static decimal GuarantorPaidOffAccountsPatientPayments(GuarantorAccountVisit visit, int daysFrom, int daysTo)
        {
            if (DischargedBetweenDates(visit.DischargeDate.Date, visit.ScoringDate.Date, daysFrom, daysTo) &&
                !visit.TriggeredScoring &&
                visit.HsCurrentBalance == 0)
            {
                return visit.GuarantorAccountTransactions.Where(x => x.VpTransactionTypeEnum.IsInCategory(VpTransactionTypeEnumCategory.ScoringPatientCash) && x.PostDate.Date > visit.DischargeDate.Date)
                    .Sum(x => x.TransactionAmount) * -1;
            }
            return 0;
        }

        private static decimal GuarantorPaymentsByDischarge(GuarantorAccountVisit visit, int daysFrom, int daysTo)
        {
            if (DischargedBetweenDates(visit.DischargeDate.Date, visit.ScoringDate.Date, daysFrom, daysTo) &&
                visit.HsCurrentBalance == 0)
            {
                return visit.GuarantorAccountTransactions.Where(x => x.VpTransactionTypeEnum.IsInCategory(VpTransactionTypeEnumCategory.ScoringPatientCash) && x.PostDate.Date > visit.DischargeDate.Date)
                    .Sum(x => x.TransactionAmount) * -1;
            }
            return 0;
        }

        private static bool DataExists(IList<GuarantorAccountVisit> visit, int daysFrom, int daysTo)
        {
            return visit.Any(x => !x.TriggeredScoring &&  // not trigering visit
                            x.DischargeDate.Date <= x.ScoringDate.Date.AddDays(daysFrom) &&  // discharge date is between days
                            x.DischargeDate.Date >= x.ScoringDate.Date.AddDays(daysTo));
        }


        private static bool DischargedBetweenDates(DateTime dischargeDate, DateTime scoringDate, int daysFrom, int daysTo)
        {
            return dischargeDate <= scoringDate.AddDays(daysFrom).Date &&
                   dischargeDate >= scoringDate.AddDays(daysTo).Date;
        }

    }

    class Visit
    {
        public virtual int VisitBatchId { get; set; }
        public virtual int VisitId { get; set; }
        public virtual int HsGuarantorId { get; set; }
        public virtual int ScoringBatchId { get; set; }
        public virtual bool TriggeredScoring { get; set; }
        public virtual bool IsEligibleToScore { get; set; }
        public virtual int BillingSystemId { get; set; }
        public virtual string SourceSystemKey { get; set; }
        public virtual DateTime DischargeDate { get; set; }
        public virtual decimal HsCurrentBalance { get; set; }
        public virtual decimal SelfPayBalance { get; set; }
        public virtual string BillingApplication { get; set; }
        public virtual int LifeCycleStageId { get; set; }
        public virtual int PatientTypeId { get; set; }
        public virtual PatientTypeEnum PatientTypeEnum => (PatientTypeEnum)this.PatientTypeId;
        public virtual int SelfPayClassId { get; set; }
        public virtual SelfPayClassEnum SelfPayClassEnum => (SelfPayClassEnum)this.SelfPayClassId;
        public virtual DateTime FirstSelfPayDate { get; set; }
        public virtual DateTime ScoringDate { get; set; }
        public virtual IList<VisitTransaction> GuarantorAccountTransactions { get; set; }
        public int FacilityId { get; set; }
        public string FacilityDescription { get; set; }
        public string StateCode { get; set; }
        public int? RegionId { get; set; }
        public string RegionScope { get; set; }
        public string BillingSystemName { get; set; }
    }

    class VisitTransaction
    {
        public virtual int VisitTransactionId { get; set; }
        public virtual int BillingSystemId { get; set; }
        public virtual string SourceSystemKey { get; set; }
        public virtual DateTime TransactionDate { get; set; }
        public virtual DateTime PostDate { get; set; }
        public virtual decimal TransactionAmount { get; set; }
        public virtual VisitTransactionType VpTransactionType { get; set; }
        public virtual string TransactionDescription { get; set; }
        public virtual int? VpPaymentAllocationId { get; set; }
        public virtual int TransactionCodeId { get; set; }
    }

    class VisitTransactionType
    {
        public virtual int VpTransactionTypeId { get; set; }
        public virtual string TransactionType { get; set; }
        public virtual string TransactionGroup { get; set; }
        public virtual string VpTransactionTypeName { get; set; }
        public virtual string DisplayName { get; set; }
        public virtual string TransactionSource { get; set; }
        public virtual string Notes { get; set; }
    }

    class RScoringResult
    {
        public int HsGuarantorId { get; set; }
        public int VisitId { get; set; }
        public decimal PtpScore { get; set; }
        public decimal ProtoScore1 { get; set; }
        public decimal ProtoScore2 { get; set; }
        public bool Am1Flag { get; set; }
        public virtual string S1Version { get; set; }
        public virtual string S2Version { get; set; }
    }

}

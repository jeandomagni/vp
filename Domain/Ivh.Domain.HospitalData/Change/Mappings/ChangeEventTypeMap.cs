﻿namespace Ivh.Domain.HospitalData.Change.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class ChangeEventTypeMap: ClassMap<ChangeEventType>
    {
        public ChangeEventTypeMap()
        {
            this.Schema("dbo");
            this.Table("ChangeEventType");
            this.Cache.ReadOnly();
            this.Id(x => x.ChangeEventTypeId);
            this.References(x => x.Application)
                .Column("ApplicationId")
                .Not.Nullable()
                .Fetch.Join();
            this.Map(x => x.ChangeEventTypeName).Not.Nullable();
            this.Map(x => x.ChangeEventTypeDescription).Not.Nullable();
        }
    }
}
﻿namespace Ivh.Domain.HospitalData.Change.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class ChangeEventStatusMap: ClassMap<ChangeEventStatus>
    {
        public ChangeEventStatusMap()
        {
            this.Schema("dbo");
            this.Table("ChangeEventStatu");
            this.Id(x => x.ChangeEventStatusId);
            this.Map(x => x.ChangeEventStatusName).Not.Nullable();
            this.Map(x => x.ChangeEventStatusDescription).Not.Nullable();
        }
    }
}
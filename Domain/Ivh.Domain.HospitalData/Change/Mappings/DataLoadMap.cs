﻿namespace Ivh.Domain.HospitalData.Change.Mappings
{
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class DataLoadMap : ClassMap<DataLoad>
    {
        public DataLoadMap()
        {
            this.Schema("etl");
            this.Table("LoadTracker");
            this.Id(x => x.LoadTrackerId);
            this.Map(x => x.LoadStatus, "LoadStatusId").CustomType<LoadStatusEnum>().Not.Nullable();
            this.Map(x => x.Notes);
            this.Map(x => x.LoadStartDateTime).Not.Nullable();
            this.Map(x => x.LoadEndDateTime).Not.Nullable();
            this.Map(x => x.LoadType, "LoadTypeId").CustomType<LoadTypeEnum>().Not.Nullable();
        }
    }

}

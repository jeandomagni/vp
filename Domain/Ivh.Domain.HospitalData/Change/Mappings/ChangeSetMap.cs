﻿namespace Ivh.Domain.HospitalData.Change.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;
    using HsGuarantor.Entities;
    using Visit.Entities;

    public class ChangeSetMap: ClassMap<ChangeSet>
    {
        public ChangeSetMap()
        {
            this.Schema("dbo");
            this.Table("ChangeSet");
            this.Id(x => x.ChangeSetId);
            this.Map(x => x.ChangeSetType, "ChangeSetTypeId").CustomType<ChangeSetTypeEnum>().Not.Nullable();
            this.Map(x => x.DataLoadId);
            this.Map(x => x.ChangeSetDateTime).Not.Nullable();

            this.HasMany<HsGuarantorChange>(x => x.HsGuarantorChanges)
                .Schema("dbo")
                .Table("HsGuarantorChange")
                .KeyColumn("ChangeSetId")
                .Cascade.All();

            this.HasMany<VisitChange>(x => x.VisitChanges)
                .Schema("dbo")
                .Table("VisitChange")
                .KeyColumn("ChangeSetId")
                .Cascade.All();

            this.HasMany<ChangeEvent>(x => x.ChangeEvents)
                .Schema("dbo")
                .Table("ChangeEvent")
                .KeyColumn("ChangeSetId")
                .Not.KeyUpdate()
                .Cascade.All();
        }
    }
}
﻿namespace Ivh.Domain.HospitalData.Change.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class ChangeEventMap : ClassMap<ChangeEvent>
    {
        public ChangeEventMap()
        {
            this.Schema("dbo");
            this.Table("ChangeEvent");
            this.Id(x => x.ChangeEventId);
            this.Map(x => x.EventTriggerDescription).Not.Nullable();
            this.Map(x => x.ChangeEventDateTime);
            this.Map(x => x.ChangeEventStatus).Column("ChangeEventStatusId").CustomType<ChangeEventStatusEnum>();
            this.Map(x => x.DataLoadId).Nullable();
            this.Map(x => x.VisitId).Nullable();
            this.Map(x => x.HsGuarantorId).Nullable();
            this.References(x => x.ChangeEventType).Column("ChangeEventTypeId")
                .Not.Nullable()
                .Fetch.Join();
        }
    }
}
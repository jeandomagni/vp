﻿namespace Ivh.Domain.HospitalData.Change.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.HospitalData;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using Entities;
    using HsGuarantor.Entities;
    using HsGuarantor.Interfaces;
    using Interfaces;
    using NHibernate;
    using Visit.Entities;
    using Visit.Interfaces;
    using VpGuarantor.Entities;

    public class ChangeEventService : DomainService, IChangeEventService
    {
        private const int QueueAllUnprocessedChangeEventsBatchCount = 10;

        private readonly Lazy<IDataLoadService> _dataLoadService;
        private readonly Lazy<IChangeEventRepository> _changeEventRepository;
        private readonly Lazy<IHsGuarantorRepository> _hsGuarantorRepository;
        private readonly ISession _session;
        private readonly Lazy<IVisitService> _visitService;
        private readonly Lazy<IInsurancePlanRepository> _insurancePlanRepository;
        private readonly Lazy<IVisitMatchStatusRepository> _visitMatchStatusRepo;
        private readonly Lazy<IVisitRepository> _visitRepository;
        private readonly Lazy<IFacilityRepository> _facilityRepository;

        public ChangeEventService(
            Lazy<IDataLoadService> dataLoadService,
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IChangeEventRepository> changeEventRepository,
            Lazy<IHsGuarantorRepository> hsGuarantorRepository,
            ISessionContext<CdiEtl> sessionContext,
            Lazy<IVisitService> visitService,
            Lazy<IInsurancePlanRepository> insurancePlanRepository,
            Lazy<IVisitMatchStatusRepository> visitMatchStatusRepo,
            Lazy<IVisitRepository> visitRepository,
            Lazy<IFacilityRepository> facilityRepository) : base(serviceCommonService)
        {
            this._dataLoadService = dataLoadService;
            this._changeEventRepository = changeEventRepository;
            this._hsGuarantorRepository = hsGuarantorRepository;
            this._session = sessionContext.Session;
            this._visitService = visitService;
            this._insurancePlanRepository = insurancePlanRepository;
            this._visitMatchStatusRepo = visitMatchStatusRepo;
            this._visitRepository = visitRepository;
            this._facilityRepository = facilityRepository;
        }

        private ChangeEventType GetChangeEventTypeByEnum(ChangeEventTypeEnum changeEventType)
        {
            IList<ChangeEventType> types = this._changeEventRepository.Value.GetAllChangeEventTypes();
            return types.FirstOrDefault(x => x.ChangeEventTypeEnum == changeEventType);
        }

        private readonly List<ChangeEventTypeEnum> _visitChangeTypes = EnumHelper<ChangeEventTypeEnum>.GetValues(ChangeEventTypeEnumCategory.Visit).ToList();

        public void QueueAllUnprocessedChangeEvents()
        {
            for (int i = 0; i < 1; i++)//doesn't do anything, but ensures we don't spin forever
            {
                ChangeSetEventsMessage changeSetEventsMessage = new ChangeSetEventsMessage { ChangeEventIds = new List<int>() };

                using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
                {
                    IList<ChangeEvent> changeEvents = this._changeEventRepository.Value.GetAllUnprocessedChangeEvents(QueueAllUnprocessedChangeEventsBatchCount);
                    if (changeEvents.Count >= QueueAllUnprocessedChangeEventsBatchCount)
                    {
                        i--;//keep it going. There might be more
                    }

                    foreach (ChangeEvent changeEvent in changeEvents)
                    {
                        if (changeEvent.ChangeEventStatus != ChangeEventStatusEnum.Unprocessed)
                        {
                            continue;
                        }

                        changeEvent.ChangeEventStatus = this.ValidateMatching(changeEvent);

                        bool hasNoAllowedGuarantorMatch = (changeEvent.ChangeEventStatus == ChangeEventStatusEnum.GuarantorMismatch);
                        bool isVisitChange = (this._visitChangeTypes.Contains(changeEvent.ChangeEventType.ChangeEventTypeEnum));

                        //allow visit changes to go through for unmatched guarantors
                        if (hasNoAllowedGuarantorMatch && isVisitChange)
                        {
                            changeEvent.ChangeEventStatus = ChangeEventStatusEnum.Queued;
                        }

                        this._changeEventRepository.Value.InsertOrUpdate(changeEvent);

                        if (changeEvent.ChangeEventStatus == ChangeEventStatusEnum.Queued)
                        {
                            changeSetEventsMessage.ChangeEventIds.Add(changeEvent.ChangeEventId);
                        }
                    }

                    if (changeSetEventsMessage.ChangeEventIds.IsNotNullOrEmpty())
                    {
                        this._session.Transaction.RegisterPostCommitSuccessAction(p1 =>
                        {
                            this.Bus.Value.PublishMessage(changeSetEventsMessage).Wait();
                        }, changeSetEventsMessage);
                    }

                    unitOfWork.Commit();
                    this._session.Clear();
                }
            }
        }

        private ChangeEventStatusEnum ValidateMatching(ChangeEvent changeEvent)
        {
            IList<VpGuarantorHsMatch> vpGuarantorHsMatches = this._changeEventRepository.Value.GetAllVpGuarantorHsMatchesByChangeEvent(changeEvent, EnumHelper<HsGuarantorMatchStatusEnum>.GetValues(HsGuarantorMatchStatusEnumCategory.Allowed).ToList());
            if (!vpGuarantorHsMatches.Any())
            {
                return ChangeEventStatusEnum.GuarantorMismatch;
            }
            IList<VisitMatchStatus> visitMatchStatuses = this._visitMatchStatusRepo.Value.GetAllVisitMatchStatues(vpGuarantorHsMatches).Where(x => x.HsGuarantorMatchStatus.IsInCategory(HsGuarantorMatchStatusEnumCategory.Prohibited)).ToList();
            if (visitMatchStatuses.Select(x => x.VisitId).ToList().Contains(changeEvent.VisitId.GetValueOrDefault()))
            {
                return ChangeEventStatusEnum.VisitMismatch;
            }

            return ChangeEventStatusEnum.Queued;
        }

        public void ProcessChangeSetEvents(ChangeSetEventsMessage message)
        {
            ChangeSetEventsAggregatedMessage changeSetEventsAggregatedMessage = new ChangeSetEventsAggregatedMessage { Messages = message.ToListOfOne() };
            this.ProcessChangeSetEventsAggregatedMessage(changeSetEventsAggregatedMessage);
        }

        public void ProcessChangeSetEventsAggregatedMessage(ChangeSetEventsAggregatedMessage changeSetEventsAggregatedMessage)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                IList<ChangeSetMessage> changeSetMessages = new List<ChangeSetMessage>();
                foreach (ChangeSetEventsMessage changeSetEventMessage in changeSetEventsAggregatedMessage.Messages)
                {
                    changeSetMessages.AddRange(this.GetChangeSetMessages(changeSetEventMessage));
                }

                ChangeSetAggregatedMessage changeSetAggregatedMessage = new ChangeSetAggregatedMessage { Messages = changeSetMessages };

                this._session.Transaction.RegisterPostCommitSuccessAction((p1) =>
                {
                    this.Bus.Value.PublishMessage(p1).Wait();
                }, changeSetAggregatedMessage);

                unitOfWork.Commit();
            }
        }

        private IList<ChangeSetMessage> GetChangeSetMessages(ChangeSetEventsMessage message)
        {
            IList<ChangeSetMessage> changeSetMessages = new List<ChangeSetMessage>();

            foreach (int changeEventId in message.ChangeEventIds)
            {
                ChangeEvent changeEvent = this._changeEventRepository.Value.GetById(changeEventId);

                IList<HsGuarantor> hsGuarantors = this._changeEventRepository.Value.GetHsGuarantorsByChangeEvent(changeEventId);
                VisitChange visitChange = this._changeEventRepository.Value.GetVisitChangeForChangeEvent(changeEventId);

                // if a ChangeEvent has both visitId and HsGuarantorId it's still only a Visit Change.
                // This will keep the app from running through the guarantor change logic for a visit change.
                HsGuarantorChange hsGuarantorChange = visitChange == null ? this._changeEventRepository.Value.GetHsGuarantorChangeForChangeEvent(changeEventId) : null;

                IList<VisitTransactionChange> visitTransactionChanges = this._changeEventRepository.Value.GetVisitTransactionChangesForChangeEvent(changeEventId);
                IList<VpGuarantorHsMatch> vpGuarantorHsMatches = this._changeEventRepository.Value.GetAllVpGuarantorHsMatchesByChangeEvent(changeEvent, EnumHelper<HsGuarantorMatchStatusEnum>.GetValues(HsGuarantorMatchStatusEnumCategory.Allowed).ToList());

                ChangeSetMessage changeSetMessage = new ChangeSetMessage
                {
                    ChangeEvent = Mapper.Map<ChangeEventMessage>(changeEvent),
                    VisitChanges = visitChange == null ? new List<VisitChangeDto>() : new List<VisitChangeDto> { Mapper.Map<VisitChangeDto>(visitChange) },
                    VisitTransactionChanges = Mapper.Map<IList<VisitTransactionChangeDto>>(visitTransactionChanges),
                    HsGuarantors = Mapper.Map<IList<HsGuarantorDto>>(hsGuarantors),
                    VpGuarantorHsMatches = Mapper.Map<IList<VpGuarantorHsMatchDto>>(vpGuarantorHsMatches),
                    HsGuarantorChanges = hsGuarantorChange == null ? new List<HsGuarantorChangeDto>() : new List<HsGuarantorChangeDto> { Mapper.Map<HsGuarantorChangeDto>(hsGuarantorChange) }
                };

                // VPNG-19827 - Changed from ChangeEventStatusEnum.Processed. 
                // ChangeEventStatusEnum.Processed will be set after VP App sends a confirmation message back 
                changeEvent.ChangeEventStatus = ChangeEventStatusEnum.SentToVpApp;
                this._changeEventRepository.Value.InsertOrUpdate(changeEvent);

                changeSetMessages.Add(changeSetMessage);
            }

            return changeSetMessages;
        }

        /// <summary>
        /// VPNG-19827- ProcessChangeSetEvents updates the change set status to ChangeEventStatusEnum.SentToVpApp and publishes a ChangeSetMessage message
        /// This method handles the acknowledgement that the ChangeSetMessage message was received and processed by the consumer
        /// </summary>
        /// <param name="message"></param>
        public void ChangeEventProcessedAcknowledgement(ChangeEventProcessedMessage message)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                ChangeEvent changeEvent = this._changeEventRepository.Value.GetById(message.ChangeEventId);
                changeEvent.ChangeEventStatus = ChangeEventStatusEnum.Processed;
                this._changeEventRepository.Value.InsertOrUpdate(changeEvent);

                unitOfWork.Commit();
            }
        }

        public IList<ChangeEvent> GetChangeEventsByVisit(int visitId)
        {
            return this._changeEventRepository.Value.GetChangeEventsByVisit(visitId);
        }

        public void GenerateChangeEventForHsGuarantor(int vpGuarantorId, int hsGuarantorId, IList<EnrollmentResponseDto> response)
        {
            IReadOnlyList<VpGuarantorHsMatch> hsGuarantors = this._changeEventRepository.Value.GetAllMatchesByVpGuarantor(vpGuarantorId);
            hsGuarantors = hsGuarantors.Where(x => x.HsGuarantorId == hsGuarantorId).ToList();
            this.GenerateChangeEventForGuarantor(vpGuarantorId, hsGuarantors, response);
        }

        public void GenerateChangeEventForVpGuarantor(int vpGuarantorId, IList<EnrollmentResponseDto> response)
        {
            IReadOnlyList<VpGuarantorHsMatch> hsGuarantors = this._changeEventRepository.Value.GetAllMatchesByVpGuarantor(vpGuarantorId);
            this.GenerateChangeEventForGuarantor(vpGuarantorId, hsGuarantors, response);
        }

        private void GenerateChangeEventForGuarantor(int vpGuarantorId, IReadOnlyList<VpGuarantorHsMatch> hsGuarantors, IList<EnrollmentResponseDto> response) { 
            if (hsGuarantors.IsNullOrEmpty())
            {
                //Seems odd that we could make it here without any matches.  Thus logging a warning.
                this.LoggingService.Value.Warn(() =>  $"ChangeEventService::GenerateChangeEventForGuarantor - Trying to generate ChangeSet Guarantor registration, but couldn't find in the base tables.  vpGuarantorId = '{vpGuarantorId}'");
                return;
            }

            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                bool Condition(Visit v)
                {
                    return response.SelectMany(x => x.Accounts)
                            .Any(y => string.Equals(y.AccountMatchString, v.SourceSystemKey, StringComparison.InvariantCultureIgnoreCase) && 
                                      (y.eligiblitStatus == EnrollmentAccountEligibilityEnum.Eligible || y.eligiblitStatus == EnrollmentAccountEligibilityEnum.Suspended));
                }

                IList<ChangeEvent> changeEvents = new List<ChangeEvent>();
                foreach (VpGuarantorHsMatch hsGuarantor in hsGuarantors)
                {
                    changeEvents.AddRange(this.CreateCurrentStateForHsGuarantorAsChangeEvent(hsGuarantor, Condition, ChangeEventTypeEnum.DataChange));
                }

                this.PublishChangeSetMessages(changeEvents);
                unitOfWork.Commit();
            }
        }

        public void GenerateChangeEventForGuarantor(IList<VpGuarantorHsMatch> vpGuarantorHsMatches)
        {
            if (vpGuarantorHsMatches.IsNullOrEmpty())
            {
                //Seems odd that we could make it here without any matches.  Thus logging a warning.
                this.LoggingService.Value.Warn(() =>  "ChangeEventService::GenerateChangeEventForGuarantor - Trying to generate ChangeSet for Guarantor rematch, " +
                                               $"but couldn't find in the base tables.  HsGuarantorId = {string.Join(",", vpGuarantorHsMatches.Select(x => x.HsGuarantorId))}");
                return;
            }

            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                IList<ChangeEvent> changeEvents = new List<ChangeEvent>();
                foreach (VpGuarantorHsMatch hsGuarantor in vpGuarantorHsMatches)
                {
                    changeEvents.AddRange(this.CreateCurrentStateForHsGuarantorAsChangeEvent(hsGuarantor, null, ChangeEventTypeEnum.DataChange));//this will always process the visit
                }

                this.PublishChangeSetMessages(changeEvents);
                unitOfWork.Commit();
            }
            return;
        }

        public void GenerateHsGuarantorMatchDiscrepancyChangeEventsForGuarantor(int vpGuarantorId, string sourceSystemKey, int billingSystemId)
        {
            HsGuarantor hsGuarantor = this._hsGuarantorRepository.Value.GetHsGuarantor(billingSystemId, sourceSystemKey);

            if (hsGuarantor == null)
            {
                return;
            }

            this.GenerateChangeEventForGuarantorInternal(vpGuarantorId,
                ChangeEventTypeEnum.HsGuarantorMatchDiscrepancy,
                x => x.HsGuarantorId == hsGuarantor.HsGuarantorId);
        }

        public void GenerateDataChangeEventsForGuarantor(int vpGuarantorId, IList<int> hsGuarantorIds)
        {
            this.GenerateChangeEventForGuarantorInternal(vpGuarantorId,
                ChangeEventTypeEnum.DataChange,
                x => x.HsGuarantorMatchStatus.IsInCategory(HsGuarantorMatchStatusEnumCategory.Matched)
                     && hsGuarantorIds.Contains(x.HsGuarantorId));
        }

        private void GenerateChangeEventForGuarantorInternal(int vpGuarantorId, ChangeEventTypeEnum changeEventType, Predicate<VpGuarantorHsMatch> hsGuarantorMatchPredicate)
        {
            IReadOnlyList<VpGuarantorHsMatch> hsGuarantors = this._changeEventRepository.Value.GetAllMatchesByVpGuarantor(vpGuarantorId);

            if (hsGuarantors.IsNullOrEmpty())
            {
                //Seems odd that we could make it here without any matches.  Thus logging a warning.
                this.LoggingService.Value.Warn(() => $"ChangeEventService::GenerateChangeEventForGuarantor - Trying to generate ChangeSet, but couldn't find in the base tables.  vpGuarantorId = '{vpGuarantorId}'");
                return;
            }

            if (hsGuarantorMatchPredicate != null)
            {
                hsGuarantors = hsGuarantors.Where(hsGuarantorMatchPredicate.Invoke).ToList();
            }

            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                IList<ChangeEvent> changeEvents = new List<ChangeEvent>();
                foreach (VpGuarantorHsMatch hsGuarantor in hsGuarantors)
                {
                    changeEvents.AddRange(this.CreateCurrentStateForHsGuarantorAsChangeEvent(hsGuarantor,null, changeEventType));
                }

                this.PublishChangeSetMessages(changeEvents);
                unitOfWork.Commit();
            }
        }

        public void SaveChangeEvent(ChangeEvent changeEvent)
        {
            this._changeEventRepository.Value.InsertOrUpdate(changeEvent);
        }

        public void SaveChangeEvent(ChangeEvent changeEvent, HsGuarantorChange hsGuarantorChange)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                int dataLoadId = this._dataLoadService.Value.CreateDataLoadId(LoadTypeEnum.QaToolsChangeSets);

                // save hsGuarantor to base table
                HsGuarantor hsGuarantor = this._hsGuarantorRepository.Value.GetHsGuarantor(hsGuarantorChange.BillingSystemId, hsGuarantorChange.SourceSystemKey) ?? new HsGuarantor { };
                Mapper.Map(hsGuarantorChange, hsGuarantor);
                hsGuarantor.DataLoadId = dataLoadId;
                this._hsGuarantorRepository.Value.InsertOrUpdate(hsGuarantor);

                // save changeEvent
                changeEvent.DataLoadId = dataLoadId;
                changeEvent.HsGuarantorId = hsGuarantor.HsGuarantorId;
                this._changeEventRepository.Value.InsertOrUpdate(changeEvent);

                unitOfWork.Commit();
            }
        }

        public void SaveChangeEvent(ChangeEvent changeEvent, VisitChange visitChange)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                int dataLoadId = this._dataLoadService.Value.CreateDataLoadId(LoadTypeEnum.QaToolsChangeSets);

                Visit visit = this._visitRepository.Value.GetById(visitChange.VisitId) ?? new Visit();
                Mapper.Map(visitChange, visit);
                visit.DataLoadId = dataLoadId;
                visit.VisitTransactions = visit.VisitTransactions ?? new List<VisitTransaction>();
                visit.VisitInsurancePlans = visit.VisitInsurancePlans ?? new List<VisitInsurancePlan>();
                visit.VisitPaymentAllocations = visit.VisitPaymentAllocations ?? new List<VisitPaymentAllocation>();

                this.CreateOrMergeFacility(visitChange, visit);
                this.CreateOrMergeVisitTransactions(visitChange, visit);
                this.CreateOrMergeVisitPaymentAllocations(visitChange, visit);
                this.CreateOrMergeVisitInsurancePlans(visitChange, visit);

                this._visitRepository.Value.InsertOrUpdate(visit);
                
                // save changeEvent
                changeEvent.DataLoadId = dataLoadId;
                changeEvent.VisitId = visit.VisitId;
                changeEvent.HsGuarantorId = visit.HsGuarantorId;
                this._changeEventRepository.Value.InsertOrUpdate(changeEvent);

                unitOfWork.Commit();
            }
        }

        public void SaveChangeEvents(ChangeSet changeSet, ChangeSetTypeEnum changeSetType)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                foreach (VisitChange visitChange in changeSet.VisitChanges)
                {
                    ChangeEvent changeEvent = changeSet.ChangeEvents[0];
                    this.SaveChangeEvent(changeEvent, visitChange);
                }

                foreach (HsGuarantorChange hsGuarantorChange in changeSet.HsGuarantorChanges)
                {
                    ChangeEvent changeEvent = changeSet.ChangeEvents[0];
                    this.SaveChangeEvent(changeEvent, hsGuarantorChange);
                }

                for (int i = 0; i < changeSet.ChangeEvents.Count; i++)
                {
                    bool hasAlreadSavedFirstChangeEvent = changeSet.VisitChanges.Count > 0 || changeSet.HsGuarantorChanges.Count > 0;
                    if (i == 0 && hasAlreadSavedFirstChangeEvent)
                    {
                        continue;
                    }

                    this.SaveChangeEvent(changeSet.ChangeEvents[i]);
                }

                unitOfWork.Commit();
            }
        }
        
        private void CreateOrMergeFacility(VisitChange visitChange, Visit visit)
        {
            int facilityId = visitChange.Facility?.FacilityId ?? 0;
            
            Facility facility = this._facilityRepository.Value.GetById(facilityId);
            visit.Facility = facility;
        }

        private void CreateOrMergeVisitInsurancePlans(VisitChange visitChange, Visit visit)
        {
            // insurance plans
            if (this.InsurancePlansChanged(visit.VisitInsurancePlans, visitChange.VisitInsurancePlans))
            {
                visit.VisitInsurancePlans.Clear();
                foreach (VisitInsurancePlanChange visitInsurancePlanChange in visitChange.VisitInsurancePlans.Where(x => x.InsurancePlanId != default(int)))
                {
                    visitInsurancePlanChange.VisitId = visitChange.VisitId;
                    visitInsurancePlanChange.VisitChange = visitChange;
                    visitInsurancePlanChange.DataLoadId = visitChange.DataLoadId;

                    VisitInsurancePlan visitInsurancePlan = new VisitInsurancePlan();
                    Mapper.Map(visitInsurancePlanChange, visitInsurancePlan);
                    visitInsurancePlan.DataLoadId = visit.DataLoadId;
                    visitInsurancePlan.Visit = visit;
                    visitInsurancePlan.InsurancePlan = this._insurancePlanRepository.Value.GetById(visitInsurancePlanChange.InsurancePlanId);
                    visit.VisitInsurancePlans.Add(visitInsurancePlan);
                }
            }
        }

        private void CreateOrMergeVisitTransactions(VisitChange visitChange, Visit visit)
        {
            foreach (VisitTransactionChange visitTransactionChange in visitChange.VisitTransactions)
            {
                VisitTransaction visitTransaction = visit.VisitTransactions.FirstOrDefault(x => x.SourceSystemKey == visitTransactionChange.SourceSystemKey && x.BillingSystemId == visitTransactionChange.BillingSystemId) ?? new VisitTransaction();
                Mapper.Map(visitTransactionChange, visitTransaction);

                visitTransaction.DataLoadId = visit.DataLoadId;
                visitTransaction.Visit = visit;

                if (visitTransaction.VisitTransactionId == default(int))
                {
                    visit.VisitTransactions.Add(visitTransaction);
                }
            }
        }

        private void CreateOrMergeVisitPaymentAllocations(VisitChange visitChange, Visit visit)
        {
            foreach (VisitPaymentAllocationChange visitPaymentAllocationChange in visitChange.VisitPaymentAllocations)
            {
                VisitPaymentAllocation visitPaymentAllocation = visit.VisitPaymentAllocations?.FirstOrDefault(x => x.PaymentAllocationId == visitPaymentAllocationChange.PaymentAllocationId) ?? new VisitPaymentAllocation();
                Mapper.Map(visitPaymentAllocationChange, visitPaymentAllocation);

                visitPaymentAllocation.DataLoadId = visit.DataLoadId;
                visitPaymentAllocation.Visit = visit;
                visitPaymentAllocation.PaymentAllocationId = visitPaymentAllocationChange.PaymentAllocationId;

                if (visitPaymentAllocation.VisitPaymentAllocationId == default(int))
                {
                    visit.VisitPaymentAllocations = visit.VisitPaymentAllocations ?? new List<VisitPaymentAllocation>();
                    visit.VisitPaymentAllocations.Add(visitPaymentAllocation);
                }
            }
        }

        private bool InsurancePlansChanged(ICollection<VisitInsurancePlan> visitInsurancePlans, ICollection<VisitInsurancePlanChange> visitInsurancePlanChanges)
        {
            bool sameCount = visitInsurancePlans.Count == visitInsurancePlanChanges.Count;
            if (!sameCount)
            {
                return true;
            }

            bool match = visitInsurancePlanChanges.All(c => visitInsurancePlans.Any(x => x.PayOrder == c.PayOrder && x.InsurancePlan.InsurancePlanId == c.InsurancePlanId));
            if (!match)
            {
                return true;
            }

            return false;
        }
        
        private IList<ChangeEvent> CreateCurrentStateForHsGuarantorAsChangeEvent(VpGuarantorHsMatch hsGuarantor, Predicate<Visit> condition, ChangeEventTypeEnum changeEventTypeEnum)
        {
            IReadOnlyList<Visit> visits = this._visitService.Value.GetVisitsForGuarantor(hsGuarantor.HsGuarantorId);

            IList<Visit> visitsToCheckToSeeIfWeShouldProcessThem = visits.Where(x => condition?.Invoke(x) ?? true).ToList();
            IList<Visit> visitsToProcess = new List<Visit>();
            foreach (Visit visit in visitsToCheckToSeeIfWeShouldProcessThem)
            {
                visitsToProcess.Add(visit);
            }

            List<ChangeEvent> listOfChangeEvents = this.GetChangeEvents(visitsToProcess, hsGuarantor.HsGuarantorId, hsGuarantor.VpGuarantorId, changeEventTypeEnum);

            if (listOfChangeEvents.IsNullOrEmpty())
            {
                this.LoggingService.Value.Info(() => $"ChangeEventService::GenerateChangeEventForGuarantor - No change events found for HsGuarantorId = {hsGuarantor.HsGuarantorId}");
            }

            return listOfChangeEvents;
        }

        private List<ChangeEvent> GetChangeEvents(IList<Visit> visits, int hsGuarantorId, int vpGuarantorId, ChangeEventTypeEnum changeEventTypeEnum)
        {
            //Only create event, dataload, and changeset if there are visit changes.
            //If we were doing more than placements, might have to do more logic here.
            List<ChangeEvent> events = new List<ChangeEvent>();
            DateTime changeEventDateTime = DateTime.UtcNow;
            int dataLoadId = this._dataLoadService.Value.CreateDataLoadId(LoadTypeEnum.Realtime);

            if (visits.IsNullOrEmpty())
            {
                // Need to make change event for HsGuarantor if no visits found so statement can be made after registration
                ChangeEvent changeEvent = this.CreateChangeEventGuarantor(changeEventDateTime, dataLoadId, this.GetChangeEventTypeByEnum(changeEventTypeEnum), hsGuarantorId);
                events.Add(changeEvent);
                this.LoggingService.Value.Info(() => $"ChangeEventService::CreateCurrentStateForHsGuarantorAsChangeEvent::GetChangeEvents - None of the visits returned from the API Matched. - HsGuarantor {hsGuarantorId} - VpGuarantor = {vpGuarantorId}");
            }
            else
            {
                IList<ChangeEvent> changeEvents = this.CreateChangeEvents(visits, changeEventDateTime, dataLoadId, this.GetChangeEventTypeByEnum(changeEventTypeEnum));
                if (changeEvents.Count > 0)
                {
                    events.AddRange(changeEvents);
                }
            }

            return events;
        }

        private ChangeEvent CreateChangeEventGuarantor(DateTime changeEventDateTime, int dataLoadId, ChangeEventType changeEventType, int hsGuarantorId)
        {
            ChangeEvent changeEvent = this.CreateChangeEvent(changeEventDateTime, dataLoadId, changeEventType);
            changeEvent.HsGuarantorId = hsGuarantorId;
            return changeEvent;
        }

        private IList<ChangeEvent> CreateChangeEvents(IList<Visit> visits, DateTime changeEventDateTime, int dataLoadId, ChangeEventType changeEventType)
        {
            IList<ChangeEvent> changeEvents = new List<ChangeEvent>();
            foreach (Visit visit in visits)
            {
                ChangeEvent changeEvent = this.CreateChangeEvent(changeEventDateTime, dataLoadId, changeEventType);
                changeEvent.VisitId = visit.VisitId;
                changeEvent.HsGuarantorId = visit.HsGuarantorId;
                changeEvents.Add(changeEvent);
            }

            foreach (int guarantorId in visits.Select(x => x.HsGuarantorId).Distinct())
            {
                ChangeEvent changeEvent = this.CreateChangeEventGuarantor(changeEventDateTime, dataLoadId, changeEventType, guarantorId);
                changeEvents.Add(changeEvent);
            }

            return changeEvents;
        }

        private ChangeEvent CreateChangeEvent(DateTime changeEventDateTime, int dataLoadId, ChangeEventType changeEventType)
        {
            return new ChangeEvent()
            {
                ChangeEventStatus = ChangeEventStatusEnum.Unprocessed,
                ChangeEventType = changeEventType,
                ChangeEventDateTime = changeEventDateTime,
                DataLoadId = dataLoadId
            };
        }

        private void PublishChangeSetMessages(ICollection<ChangeEvent> changeEvents)
        {
            IList<ChangeSetEventsMessage> messages = new List<ChangeSetEventsMessage>();
            if (changeEvents != null && changeEvents.Count > 0)
            {
                foreach (ChangeEvent changeEvent in changeEvents)
                {
                    this._changeEventRepository.Value.InsertOrUpdate(changeEvent);
                    messages.Add(new ChangeSetEventsMessage()
                    {
                        ChangeEventIds = new List<int>() { changeEvent.ChangeEventId }
                    });
                }
            }

            this._session.Transaction.RegisterPostCommitSuccessAction((p1) =>
            {
                if (messages.Count > 0)
                {
                    ChangeSetEventsAggregatedMessage changeSetAggregatedMessage = new ChangeSetEventsAggregatedMessage { Messages = messages };
                    this.Bus.Value.PublishMessage(changeSetAggregatedMessage).Wait();
                }

            }, messages);

            this.LoggingService.Value.Info(() => $"ChangeEventService::GenerateChangeEventForGuarantor - Generated ChangeEventId = {string.Join(",", messages.Select(x => x.ChangeEventIds))}");
        }
    }
}
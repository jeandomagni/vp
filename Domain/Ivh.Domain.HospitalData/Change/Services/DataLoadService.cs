﻿namespace Ivh.Domain.HospitalData.Change.Services
{
    using System;
    using Base.Interfaces;
    using Base.Services;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;

    public class DataLoadService : DomainService, IDataLoadService
    {
        private readonly IDataLoadRepository _dataLoadRepository;

        public DataLoadService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            IDataLoadRepository dataLoadRepository) : base(serviceCommonService)
        {
            this._dataLoadRepository = dataLoadRepository;
        }

        public int CreateDataLoadId(LoadTypeEnum loadType)
        {
            DataLoad dataLoad = new DataLoad()
            {
                LoadStatus = LoadStatusEnum.Completed,
                LoadStartDateTime = DateTime.UtcNow,
                LoadEndDateTime = DateTime.UtcNow,
                LoadType = loadType
            };

            this._dataLoadRepository.InsertOrUpdate(dataLoad);

            return dataLoad.LoadTrackerId;
        }

    }
}

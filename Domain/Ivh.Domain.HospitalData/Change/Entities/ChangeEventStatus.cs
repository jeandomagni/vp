﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.HospitalData.Change.Entities
{
    public class ChangeEventStatus
    {
        public virtual int ChangeEventStatusId { get; set; }
        public virtual string ChangeEventStatusName { get; set; }
        public virtual string ChangeEventStatusDescription { get; set; }
    }
}

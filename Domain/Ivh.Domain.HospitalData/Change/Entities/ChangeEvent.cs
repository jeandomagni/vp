﻿namespace Ivh.Domain.HospitalData.Change.Entities
{
    using System;
    using Common.VisitPay.Enums;

    public class ChangeEvent
    {
        public ChangeEvent()
        {
            this.EventTriggerDescription = string.Empty;
        }
        public virtual int ChangeEventId { get; set; }
        public virtual ChangeEventType ChangeEventType { get; set; }
        public virtual string EventTriggerDescription { get; set; }
        public virtual DateTime ChangeEventDateTime { get; set; }
        public virtual ChangeEventStatusEnum ChangeEventStatus { get; set; }
        public virtual int DataLoadId { get; set; }
        public virtual int? VisitId { get; set; }
        public virtual int? HsGuarantorId { get; set; }
    }
}
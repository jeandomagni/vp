﻿namespace Ivh.Domain.HospitalData.Change.Entities
{
    using Application.Entities;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class ChangeEventType
    {
        public virtual int ChangeEventTypeId { get; set; }
        public virtual Application Application { get; set; }
        public virtual string ChangeEventTypeName { get; set; }
        public virtual string ChangeEventTypeDescription { get; set; }
        public virtual int ApplicationProcessingSortOrder { get; set; }

        public virtual ChangeEventTypeEnum ChangeEventTypeEnum
        {
            get
            {
                return (ChangeEventTypeEnum)this.ChangeEventTypeId;
            }
        }
    }
}
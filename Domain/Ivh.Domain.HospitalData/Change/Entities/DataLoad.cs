﻿namespace Ivh.Domain.HospitalData.Change.Entities
{
    using System;
    using Common.VisitPay.Enums;

    public class DataLoad
    {
        public virtual int LoadTrackerId { get; set; }
        public virtual LoadStatusEnum LoadStatus { get; set; }
        public virtual string Notes { get; set; }
        public virtual DateTime LoadStartDateTime { get; set; }
        public virtual DateTime LoadEndDateTime { get; set; }
        public virtual LoadTypeEnum LoadType { get; set; }
    }

}

﻿namespace Ivh.Domain.HospitalData.Change.Entities
{
    using System;
    using System.Collections.Generic;
    using Common.VisitPay.Enums;
    using HsGuarantor.Entities;
    using Visit.Entities;

    public class ChangeSet
    {
        public virtual int ChangeSetId { get; set; }
        public virtual ChangeSetTypeEnum ChangeSetType { get; set; }
        public virtual int DataLoadId { get; set; }
        public virtual DateTime ChangeSetDateTime { get; set; }
        public virtual IList<HsGuarantorChange> HsGuarantorChanges { get; set; }
        public virtual IList<VisitChange> VisitChanges { get; set; }
        public virtual IList<ChangeEvent> ChangeEvents { get; set; }
    }
}
﻿namespace Ivh.Domain.HospitalData.Change.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IDataLoadRepository : IRepository<DataLoad>
    {
    }
}

﻿namespace Ivh.Domain.HospitalData.Change.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Entities;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.HospitalData;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using HsGuarantor.Entities;
    using Visit.Entities;
    using VpGuarantor.Entities;

    public interface IChangeEventService : IDomainService
    {
        void QueueAllUnprocessedChangeEvents();
        void ProcessChangeSetEvents(ChangeSetEventsMessage message);
        void ProcessChangeSetEventsAggregatedMessage(ChangeSetEventsAggregatedMessage changeSetEventsAggregatedMessage);
        void ChangeEventProcessedAcknowledgement(ChangeEventProcessedMessage message);
        void GenerateChangeEventForHsGuarantor(int vpGuarantorId, int hsGuarantorId, IList<EnrollmentResponseDto> response);
        void GenerateChangeEventForVpGuarantor(int vpGuarantorId, IList<EnrollmentResponseDto> response);
        void GenerateChangeEventForGuarantor(IList<VpGuarantorHsMatch> vpGuarantorHsMatches);
        void GenerateHsGuarantorMatchDiscrepancyChangeEventsForGuarantor(int vpGuarantorId, string sourceSystemKey, int billingSystemId);
        void GenerateDataChangeEventsForGuarantor(int vpGuarantorId, IList<int> hsGuarantorIds);
        void SaveChangeEvent(ChangeEvent changeEvent);
        void SaveChangeEvent(ChangeEvent changeEvent, HsGuarantorChange hsGuarantorChange);
        void SaveChangeEvent(ChangeEvent changeEvent, VisitChange visitChange);
        void SaveChangeEvents(ChangeSet changeSetDto, ChangeSetTypeEnum changeSetType);
        IList<ChangeEvent> GetChangeEventsByVisit(int visitId);
    }
}
﻿namespace Ivh.Domain.HospitalData.Change.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;
    using HsGuarantor.Entities;
    using Visit.Entities;
    using VpGuarantor.Entities;

    public interface IChangeEventRepository : IRepository<ChangeEvent>
    {
        IList<ChangeEvent> GetAllUnprocessedChangeEvents(int? batchSize = null);
        IList<ChangeEventType> GetAllChangeEventTypes();
        void BulkUpdateIneligibleToScoreChangeEvents(int scoringBatchId);
        IList<VpGuarantorHsMatch> GetAllVpGuarantorHsMatchesByChangeEvent(ChangeEvent changeEvent, IList<HsGuarantorMatchStatusEnum> hsGuarantorMatchStatuses = null);
        IList<HsGuarantor> GetHsGuarantorsByChangeEvent(int changeEventId);
        IList<VisitTransactionChange> GetVisitTransactionChangesForChangeEvent(int changeEventId);
        VisitChange GetVisitChangeForChangeEvent(int changeEventId);
        HsGuarantorChange GetHsGuarantorChangeForChangeEvent(int changeEventId);
        IList<ChangeEvent> GetChangeEventsByVisit(int visitId);
        IReadOnlyList<VpGuarantorHsMatch> GetAllMatchesByVpGuarantor(int vpGuarantorId);
    }
}

﻿namespace Ivh.Domain.HospitalData.Change.Interfaces
{
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;

    public interface IDataLoadService : IDomainService
    {
        int CreateDataLoadId(LoadTypeEnum loadType);
    }
}

﻿namespace Ivh.Domain.HospitalData.Inbound.Services
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Common.Base.Utilities.Extensions;
    using Entities;
    using Interfaces;

    public class InboundFileService : IInboundFileService
    {

        public IList<FilesByDestination> GetFiles(IList<FileTypeConfigInbound> fileTypeConfigInbounds, string inboundStagingDirectory)
        {
            List<FilesToImport> filesToImport = new List<FilesToImport>();
            List<FilesByDestination> filesByFileType = new List<FilesByDestination>();

            // get the paths of all the files we're going to inbound
            List<string> fullPaths = new List<string>();
            List<string> filesInDirectory = Directory.GetFiles(inboundStagingDirectory).ToList();
            List<string> inboundFileNames = fileTypeConfigInbounds.Select(x => x.FileNamePattern).ToList();
            fullPaths.AddRange(filesInDirectory.Where(x => inboundFileNames.Any(z => x.Contains(z))).ToList());

            foreach (string file in fullPaths)
            {
                string fileName = Path.GetFileName(file);
                FileTypeConfigInbound fileTypeConfigInbound = fileTypeConfigInbounds.FirstOrDefault(x => fileName.Contains(x.FileNamePattern));

                if (fileTypeConfigInbound != null && fileTypeConfigInbound.IsActive == false)
                {
                    Console.WriteLine($"File Pattern {fileTypeConfigInbound.FileNamePattern} inactive");
                }

                if (fileTypeConfigInbound != null && fileTypeConfigInbound.IsActive)
                {
                    filesToImport.Add(new FilesToImport
                        {
                            FileName = Path.GetFileName(fileName),
                            FileFullPath = file,
                            FileTypeId = Convert.ToInt32(fileTypeConfigInbound.FileType.FileTypeId),
                            FileTypeConfigInbound = fileTypeConfigInbound
                        }
                    );
                }
                else
                {
                    Console.WriteLine($"No matching file spec found for: {fileName}");
                }
            }

            var distincttypes = (from ft in filesToImport
                select new {DestinationTableName = ft.FileTypeConfigInbound.DestinationTable.RemoveBrackets()}).Distinct();

            foreach (var r in distincttypes)
            {
                List<FilesToImport> filesorder = new List<FilesToImport>();
                foreach (FilesToImport f in filesToImport)
                {
                    if (r.DestinationTableName == f.FileTypeConfigInbound.DestinationTable.RemoveBrackets())
                    {
                        filesorder.Add(new FilesToImport
                        {
                            FileTypeId = f.FileTypeId,
                            FileFullPath = f.FileFullPath,
                            FileName = f.FileName,
                            FileTypeConfigInbound = f.FileTypeConfigInbound
                        });
                    }
                }

                filesByFileType.Add(new FilesByDestination
                {
                    DestinationTableName = r.DestinationTableName,
                    Files = filesorder.OrderBy(x => x.FileName).ToList(),
                });
            }

            return filesByFileType;
        }
    }
}
﻿namespace Ivh.Domain.HospitalData.Inbound.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Utilities.Extensions;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Messages.FinanceManagement;
    using Entities;
    using Enums;
    using FileLoadTracking.Entities;
    using FileLoadTracking.Interfaces;
    using Interfaces;
    using Ivh.Application.Core.Common.Models.Lockbox;
    using Logging.Interfaces;
    using NHibernate;

    public class PaymentVendorFileService : DomainService, IPaymentVendorFileService
    {
        private readonly Lazy<IBaseLockboxVendorFileRepository> _baseLockboxVendorFileRepository;
        private readonly Lazy<IMetricsProvider> _metricsProvider;
        private readonly Lazy<IEtlLockboxVendorFileRepository> _etlLockboxVendorFileRepository;
        private readonly Lazy<IInboundFileService> _inboundFileService;
        private readonly Lazy<ILoadTrackerService> _loadTrackerService;
        private readonly ISession _session;
        private readonly Lazy<IFileTrackerService> _fileTrackerService;
        private readonly Lazy<IFileTypeConfigInboundRepository> _fileTypeConfigInboundRepository;

        public PaymentVendorFileService(
            Lazy<IBaseLockboxVendorFileRepository> baseLockboxVendorFileRepository,
            Lazy<IMetricsProvider> metricsProvider,
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IEtlLockboxVendorFileRepository> etlLockboxVendorFileRepository,
            Lazy<IInboundFileService> inboundFileService,
            Lazy<IFileTrackerService> fileTrackerService,
            Lazy<IFileTypeConfigInboundRepository> fileTypeConfigInboundRepository,
            Lazy<ILoadTrackerService> loadTrackerService,
            ISessionContext<CdiEtl> sessionContext
        ) : base(serviceCommonService)
        {
            this._baseLockboxVendorFileRepository = baseLockboxVendorFileRepository;
            this._metricsProvider = metricsProvider;
            this._etlLockboxVendorFileRepository = etlLockboxVendorFileRepository;
            this._inboundFileService = inboundFileService;
            this._loadTrackerService = loadTrackerService;
            this._session = sessionContext.Session;
            this._fileTrackerService = fileTrackerService;
            this._fileTypeConfigInboundRepository = fileTypeConfigInboundRepository;
        }

        public void LoadFile(int loadTrackerId, string inboundStagingDirectory)
        {
            IList<FileTypeConfigInbound> fileTypeConfigInbounds = this._fileTypeConfigInboundRepository.Value.GetFileTypeConfigInbounds();

            LoadTracker loadTracker = this._loadTrackerService.Value.GetActiveLoadTrackerById(loadTrackerId);

            //call getfiles to import
            IList<FilesByDestination> filesByDestination = this._inboundFileService.Value.GetFiles(fileTypeConfigInbounds, inboundStagingDirectory);

            // get the list of files to process
            foreach (FilesByDestination fileGroup in filesByDestination)
            {
                foreach (FilesToImport file in fileGroup.Files)
                {
                    FileTracker fileTracker = this._fileTrackerService.Value.StartOrContinueFileTracker(loadTracker, file.FileTypeId, file.FileName);

                    try
                    {
                        // call the parser (send uri string, returned object)
                        LockboxFile lockboxFile = LockboxFile.GetLockboxFile(file.FileFullPath);

                        // update datadog
                        int paymentCount = lockboxFile.FileHeader.BatchHeaders.SelectMany(x => x.Details).Count();
                        this.DataDogRecordPaymentCount(paymentCount);

                        // turn list into json
                        string json = lockboxFile.ToJSON(true, true, true);

                        // next two steps will probably need become a vendor specific provider in the future
                        // save into etl table
                        EtlLockboxVendorPayment etlPayment = new EtlLockboxVendorPayment
                        {
                            FileTrackerId = fileTracker.FileTrackerId,
                            LockboxVendorPaymentContent = json
                        };
                        this._etlLockboxVendorFileRepository.Value.InsertOrUpdate(etlPayment);

                        // ETL into base table
                        // I'm skipping the Stage level right now.
                        this.EtlIntoBaseTable(fileTracker, json);

                        this.PublishMessageToApp(lockboxFile);

                        //update filetracker
                        this._fileTrackerService.Value.SetFileTrackerStatus(fileTracker, true);
                    }
                    catch (Exception e)
                    {
                        this._fileTrackerService.Value.SetFileTrackerStatus(fileTracker, false);
                        this._loadTrackerService.Value.SetLoadTrackerFail(loadTracker);
                        Console.Out.WriteLine(e);
                        throw;
                    }
                }
            }
        }

        private void PublishMessageToApp(LockboxFile lockboxFile)
        {
            PaymentImportFileMessage paymentImportFileMessage = Mapper.Map<PaymentImportFileMessage>(lockboxFile);
            this._session.Transaction.RegisterPostCommitSuccessAction(p1 => { this.Bus.Value.PublishMessage(paymentImportFileMessage).Wait(); }, paymentImportFileMessage);
        }

        private void EtlIntoBaseTable(FileTracker fileTracker, string json)
        {
            BaseLockboxVendorPayment basePayment = new BaseLockboxVendorPayment
            {
                FileTrackerId = fileTracker.FileTrackerId,
                LockboxVendorPaymentContent = json,
                LockboxVendorId = (int) LockboxVendorEnum.JPMorgan
            };
            this._baseLockboxVendorFileRepository.Value.InsertOrUpdate(basePayment);
        }

        private void DataDogRecordPaymentCount(int paymentCount)
        {
            this._metricsProvider.Value.Increment(Metrics.Increment.PaymentImport.PaymentImportInbound, paymentCount);
        }
    }
}
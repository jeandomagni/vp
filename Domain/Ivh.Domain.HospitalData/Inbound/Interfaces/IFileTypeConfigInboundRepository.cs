﻿namespace Ivh.Domain.HospitalData.Inbound.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IFileTypeConfigInboundRepository : IRepository<FileTypeConfigInbound>
    {
        IList<FileTypeConfigInbound> GetFileTypeConfigInbounds();
    }
}
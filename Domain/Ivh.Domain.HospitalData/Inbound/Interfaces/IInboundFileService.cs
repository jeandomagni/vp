﻿namespace Ivh.Domain.HospitalData.Inbound.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IInboundFileService : IDomainService
    {
        IList<FilesByDestination> GetFiles(IList<FileTypeConfigInbound> fileTypeConfigInbounds, string inboundStagingDirectory);
    }
}
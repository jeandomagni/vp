﻿namespace Ivh.Domain.HospitalData.Inbound.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IBaseLockboxVendorFileRepository : IRepository<BaseLockboxVendorPayment>
    {
        
    }
}
﻿namespace Ivh.Domain.HospitalData.Inbound.Interfaces
{
    using Common.Base.Interfaces;

    public interface IPaymentVendorFileService : IDomainService
    {
        void LoadFile(int loadTrackerId, string inboundStagingDirectory);
    }
}
﻿namespace Ivh.Domain.HospitalData.Inbound.Entities
{
    using System;
    using FileLoadTracking.Entities;

    public class FileTypeConfigInbound
    {
        public virtual int FileTypeConfigInboundId { get; set; }
        public virtual FileType FileType { get; set; }
        public virtual string FileNamePattern { get; set; }
        public virtual string FileTypeProvider { get; set; }
        public virtual int MinFileRecordCount { get; set; }
        public virtual int NumberOfFields { get; set; }
        public virtual string FieldDelimiter { get; set; }
        public virtual string DestinationTable { get; set; }
        public virtual string BadDataTable { get; set; }
        public virtual DateTime StartDate { get; set; }
        public virtual DateTime EndDate { get; set; }
        public virtual bool OutputHashFields { get; set; }
        public virtual string HashFieldName { get; set; }
        public virtual string HashFieldsLengthFieldName { get; set; }
        public virtual string FileTrackerIdFieldName { get; set; }
        public virtual bool HasHeader { get; set; }
        public virtual string FieldEncapsulator { get; set; }
        public virtual int IgnoreFirstNRows { get; set; }
        public virtual int IgnoreLastNRows { get; set; }
        public virtual bool RemoveEscapeCharacters { get; set; }
        public virtual bool IsRequired { get; set; }
        public virtual string BadDataProcedure { get; set; }
        public virtual bool StronglyTypedTable { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual string ImportProcedure { get; set; }
        public virtual bool MultiFileImport { get; set; }
        public virtual bool IsLookUpTable { get; set; }
        public virtual int? MetricGroupId { get; set; }
        public virtual short? AutoEtl_DestinationTableTypeId { get; set; }
        public virtual short? IsRequiredGroup { get; set; }
        public virtual bool RestrictDuplicateFiles { get; set; }
    }
}
﻿namespace Ivh.Domain.HospitalData.Inbound.Entities
{
    using System.Collections.Generic;

    public class FilesByDestination
    {
        public string DestinationTableName { get; set; }
        public IList<FilesToImport> Files { get; set; }

    }
}
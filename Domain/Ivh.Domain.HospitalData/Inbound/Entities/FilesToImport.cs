﻿namespace Ivh.Domain.HospitalData.Inbound.Entities
{
    public class FilesToImport
    {
        public string FileName { get; set; }
        public int FileTypeId { get; set; }
        public int StatusId { get; set; }
        public string FileFullPath { get; set; }
        public FileTypeConfigInbound FileTypeConfigInbound { get; set; }
    }
}
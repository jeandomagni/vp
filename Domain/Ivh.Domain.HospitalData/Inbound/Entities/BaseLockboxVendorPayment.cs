﻿namespace Ivh.Domain.HospitalData.Inbound.Entities
{
    public class BaseLockboxVendorPayment
    {
        public virtual int LockboxVendorPaymentId { get; set; }
        public virtual string LockboxVendorPaymentContent { get; set; }
        public virtual int LockboxVendorId { get; set; }
        public virtual int FileTrackerId { get; set; }
    }
}
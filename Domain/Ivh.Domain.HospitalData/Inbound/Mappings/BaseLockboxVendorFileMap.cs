﻿namespace Ivh.Domain.HospitalData.Inbound.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class BaseLockboxVendorFileMap : ClassMap<BaseLockboxVendorPayment>
    {
        public BaseLockboxVendorFileMap()
        {
            this.Schema(Schemas.Cdi.Base);
            this.Table("LockboxVendorPayment");
            this.Id(x => x.LockboxVendorPaymentId).Not.Nullable();

            this.Map(x => x.LockboxVendorPaymentContent);
            this.Map(x => x.LockboxVendorId);
            this.Map(x => x.FileTrackerId);
        }
    }
}
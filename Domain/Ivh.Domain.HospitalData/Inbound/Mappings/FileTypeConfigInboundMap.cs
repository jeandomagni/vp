﻿namespace Ivh.Domain.HospitalData.Inbound.Mappings
{
    using Common.Base.Utilities.Extensions;
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class FileTypeConfigInboundMap : ClassMap<FileTypeConfigInbound>
    {
        public FileTypeConfigInboundMap()
        {
            this.ReadOnly();
            this.Schema(Schemas.Cdi.Etl);
            this.Table(nameof(FileTypeConfigInbound));
            this.Id(x => x.FileTypeConfigInboundId).Not.Nullable();

            this.Map(x => x.FileNamePattern);
            this.Map(x => x.FileTypeProvider).IsNullable();
            this.Map(x => x.MinFileRecordCount);
            this.Map(x => x.NumberOfFields);
            this.Map(x => x.FieldDelimiter);
            this.Map(x => x.DestinationTable);
            this.Map(x => x.BadDataTable);
            this.Map(x => x.StartDate);
            this.Map(x => x.EndDate);
            this.Map(x => x.OutputHashFields);
            this.Map(x => x.HashFieldName);
            this.Map(x => x.HashFieldsLengthFieldName);
            this.Map(x => x.FileTrackerIdFieldName);
            this.Map(x => x.HasHeader);
            this.Map(x => x.FieldEncapsulator).IsNullable();
            this.Map(x => x.IgnoreFirstNRows);
            this.Map(x => x.IgnoreLastNRows);
            this.Map(x => x.RemoveEscapeCharacters);
            this.Map(x => x.IsRequired);
            this.Map(x => x.BadDataProcedure).IsNullable();
            this.Map(x => x.StronglyTypedTable);
            this.Map(x => x.IsActive);
            this.Map(x => x.ImportProcedure).IsNullable();
            this.Map(x => x.MultiFileImport);
            this.Map(x => x.IsLookUpTable);
            this.Map(x => x.MetricGroupId).IsNullable();
            this.Map(x => x.AutoEtl_DestinationTableTypeId).IsNullable();
            this.Map(x => x.IsRequiredGroup).IsNullable();
            this.Map(x => x.RestrictDuplicateFiles);

            this.References(x => x.FileType).Column("FileTypeId")
              .Not.Nullable()
              .Cascade.All()
              .LazyLoad()
              .Fetch.Join();
        }
    }
}
﻿namespace Ivh.Domain.HospitalData.Inbound.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class EtlLockboxVendorFileMap : ClassMap<EtlLockboxVendorPayment>
    {
        public EtlLockboxVendorFileMap()
        {
            this.Schema(Schemas.Cdi.Etl);
            this.Table("LockboxVendorPayment");
            this.Id(x => x.LockboxVendorPaymentId).Not.Nullable();

            this.Map(x => x.LockboxVendorPaymentContent);
            this.Map(x => x.FileTrackerId);
        }
    }
}
﻿namespace Ivh.Domain.HospitalData.FileLoadTracking.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface ILoadTypeRepository : IRepository<LoadType>
    {
        
    }
}
﻿namespace Ivh.Domain.HospitalData.FileLoadTracking.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface ILoadTrackerRepository : IRepository<LoadTracker>
    {
        IList<LoadTracker> GetLastEobLoadTrackers();
        LoadTracker GetLastFailedEobLoadTracker();

        IList<LoadTrackerStep> GetLoadTrackerSteps(int beforeLoadTrackerStepId);
        LoadTracker GetLastLoadTracker(int loadTypeId);
    }
}
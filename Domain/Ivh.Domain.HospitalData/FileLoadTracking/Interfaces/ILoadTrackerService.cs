﻿namespace Ivh.Domain.HospitalData.FileLoadTracking.Interfaces
{
    using System;
    using Common.VisitPay.Enums;
    using Entities;

    public interface ILoadTrackerService
    {
        LoadTracker StartOrContinueInboundLoadTracker(LoadTypeEnum loadTypeEnum, int daysToAdd, bool ignoreMissingFiles);
        LoadTracker GetActiveLoadTrackerById(int loadTrackerId);
        void SetLoadTrackerSuccess(LoadTracker loadTracker);
        void SetLoadTrackerFail(LoadTracker loadTracker);
    }
}
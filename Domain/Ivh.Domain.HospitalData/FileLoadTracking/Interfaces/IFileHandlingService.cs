﻿namespace Ivh.Domain.HospitalData.FileLoadTracking.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IFileHandlingService : IDomainService
    {
        IList<LoadTracker> GetDirectoriesAndFiles(string path, string subDirectoryName);
    }
}
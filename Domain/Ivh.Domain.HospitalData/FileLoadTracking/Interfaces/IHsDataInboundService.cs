﻿namespace Ivh.Domain.HospitalData.FileLoadTracking.Interfaces
{
    public interface IHsDataInboundService
    {
        int StartNewInboundLoadTracker();
        void CompleteLoadTracker(int loadTrackerId);
        void SkipStepsBeforeGuestPayInbound(int loadTrackerId);
    }
}

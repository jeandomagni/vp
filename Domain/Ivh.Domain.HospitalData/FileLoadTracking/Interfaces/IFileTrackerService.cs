﻿namespace Ivh.Domain.HospitalData.FileLoadTracking.Interfaces
{
    using System;
    using Entities;

    public interface IFileTrackerService
    {
        void SetFileTrackerStatus(FileTracker fileTracker, bool succeeded);
        FileTracker StartOrContinueFileTracker(LoadTracker loadTracker, int fileTypeId, string fileName, DateTime? fileDate = null);
        string GenerateFileNameWithDate(string fileNamePattern, DateTime clientDateTime);
    }
}
﻿namespace Ivh.Domain.HospitalData.FileLoadTracking.Services
{
    using System;
    using System.Collections.Generic;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;

    public class HsDataInboundService : IHsDataInboundService
    {
        private readonly Lazy<ILoadTrackerRepository> _loadTrackerRepository;
        private readonly Lazy<ILoadTrackerStepHistoryRepository> _loadTrackerStepHistoryRepository;

        public HsDataInboundService(
            Lazy<ILoadTrackerRepository> loadTrackerRepository,
            Lazy<ILoadTrackerStepHistoryRepository> loadTrackerStepHistoryRepository
        )
        {
            this._loadTrackerRepository = loadTrackerRepository;
            this._loadTrackerStepHistoryRepository = loadTrackerStepHistoryRepository;
        }

        public int StartNewInboundLoadTracker()
        {
            LoadTracker loadTracker = new LoadTracker()
            {
                LoadType = new LoadType{ LoadTypeId = (int)LoadTypeEnum.Inbound },
                LoadStatus = new LoadStatus{ LoadStatusId = (int)LoadStatusEnum.InProgress },
                LoadStartDateTime = DateTime.UtcNow
            };

            this._loadTrackerRepository.Value.InsertOrUpdate(loadTracker);

            return loadTracker.LoadTrackerId;
        }

        public void SkipStepsBeforeGuestPayInbound(int loadTrackerId)
        {
            const int startGuestPayLoadTrackerStep = 9100;

            this.SkipLoadTrackerSteps(startGuestPayLoadTrackerStep, loadTrackerId);
        }

        private void SkipLoadTrackerSteps(int startAtLoadTrackerStepId, int loadTrackerId)
        {
            IList<LoadTrackerStep> steps = this._loadTrackerRepository.Value.GetLoadTrackerSteps(startAtLoadTrackerStepId);

            foreach (LoadTrackerStep loadTrackerStep in steps)
            {
                LoadTrackerStepHistory stepHistory = new LoadTrackerStepHistory
                {
                    LoadTrackerId = loadTrackerId,
                    LoadTrackerStepId = loadTrackerStep.LoadTrackerStepId,
                    LoadTypeId = loadTrackerStep.LoadTypeId,
                    LoadTrackerStepStatusId = LoadTrackerStepStatusEnum.Skip
                };

                this._loadTrackerStepHistoryRepository.Value.InsertOrUpdate(stepHistory);
            }
        }

        public void CompleteLoadTracker(int loadTrackerId)
        {
            LoadTracker loadTracker = this._loadTrackerRepository.Value.GetById(loadTrackerId);

            if (loadTracker != null)
            {
                loadTracker.LoadStatus = new LoadStatus{ LoadStatusId = (int)LoadStatusEnum.Completed };
                loadTracker.LoadEndDateTime = DateTime.UtcNow;
            }

            this._loadTrackerRepository.Value.InsertOrUpdate(loadTracker);
        }
    }
}

﻿namespace Ivh.Domain.HospitalData.FileLoadTracking.Services
{
    using System;
    using Common.Base.Utilities.Helpers;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Entities;
    using Interfaces;
    using Logging.Interfaces;

    public class LoadTrackerService : ILoadTrackerService
    {
        private readonly Lazy<ILoadTrackerRepository> _loadTrackerRepository;
        private readonly Lazy<ILoggingService> _loggingService;
        private readonly Lazy<TimeZoneHelper> _timeZoneHelper;

        public LoadTrackerService(
            Lazy<ILoadTrackerRepository> loadTrackerRepository,
            Lazy<ILoggingService> loggingService,
            Lazy<TimeZoneHelper> timeZoneHelper
        )
        {
            this._loadTrackerRepository = loadTrackerRepository;
            this._loggingService = loggingService;
            this._timeZoneHelper = timeZoneHelper;
        }


        public LoadTracker StartOrContinueInboundLoadTracker(LoadTypeEnum loadTypeEnum, int daysToAdd, bool ignoreMissingFiles)
        {
            // this should do a bit more.  it's a basic mimic of etl.LoadInboundFileSelection
            LoadTracker lastLoadTracker = this._loadTrackerRepository.Value.GetLastLoadTracker((int) loadTypeEnum);
            if (lastLoadTracker == null)
            {
                string error = "No previous LoadTracker for PaymentVendorFiles (LoadTypeId = 12) has been created in the etl.LoadTracker table.  Add a record in the table so I know which file folder date I need to start on. Thanks.";
                this._loggingService.Value.Fatal(() => error);
                throw new Exception(error);
            }

            if (lastLoadTracker.LoadStatus.LoadStatusId == (int) LoadStatusEnum.Completed)
            {
                DateTime leafFileFolderDate = lastLoadTracker.LeafFileFolderDate.Value.AddDays(daysToAdd).Date;
                LoadTracker newLoadTracker = new LoadTracker
                {
                    LoadStatus = new LoadStatus {LoadStatusId = (int) LoadStatusEnum.InProgress},
                    LoadType = new LoadType {LoadTypeId = (int) loadTypeEnum,},
                    LoadStartDateTime = DateTime.UtcNow,
                    LeafFileFolderDate = leafFileFolderDate,
                    LeafFileFolder = leafFileFolderDate.ToString(Format.FolderDate),
                    ExtractDateTime = this._timeZoneHelper.Value.UtcToClient(DateTime.UtcNow),
                    IgnoreMissingFiles = ignoreMissingFiles
                };
                this._loadTrackerRepository.Value.InsertOrUpdate(newLoadTracker);
                return newLoadTracker;
            }
            else
            {
                lastLoadTracker.LoadStatus = new LoadStatus {LoadStatusId = (int) LoadStatusEnum.InProgress};
                this._loadTrackerRepository.Value.InsertOrUpdate(lastLoadTracker);
            }

            return lastLoadTracker;
        }

        public LoadTracker GetActiveLoadTrackerById(int loadTrackerId)
        {
            LoadTracker loadTracker = this._loadTrackerRepository.Value.GetById(loadTrackerId);
            if (loadTracker == null || loadTracker.LoadStatus.LoadStatusId != (int)LoadTrackerStepStatusEnum.InProgress)
            {
                throw new Exception($"No active LoadTracker found for LoadTrackerID: {loadTrackerId}");
            }

            return loadTracker;
        }

        public void SetLoadTrackerSuccess(LoadTracker loadTracker)
        {
            loadTracker.LoadStatus = new LoadStatus {LoadStatusId = (int) LoadStatusEnum.Completed};
            this._loadTrackerRepository.Value.InsertOrUpdate(loadTracker);
        }

        public void SetLoadTrackerFail(LoadTracker loadTracker)
        {
            loadTracker.LoadStatus = new LoadStatus {LoadStatusId = (int) LoadStatusEnum.Failed};
            this._loadTrackerRepository.Value.InsertOrUpdate(loadTracker);
        }
    }
}
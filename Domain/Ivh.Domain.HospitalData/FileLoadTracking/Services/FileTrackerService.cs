﻿namespace Ivh.Domain.HospitalData.FileLoadTracking.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Entities;
    using Interfaces;
    using NHibernate;

    public class FileTrackerService : IFileTrackerService
    {
        private readonly Lazy<IFileTrackerRepository> _fileTrackerRepository;
        private readonly Lazy<IFileTypeRepository> _fileTypeRepository;
        private readonly Lazy<ILoadTrackerRepository> _loadTrackerRepository;
        private readonly ISession _session;

        public FileTrackerService(
            Lazy<IFileTrackerRepository> fileTrackerRepository,
            Lazy<IFileTypeRepository> fileTypeRepository,
            Lazy<ILoadTrackerRepository> loadTrackerRepository,
            ISessionContext<CdiEtl> sessionContext
        )
        {
            this._fileTrackerRepository = fileTrackerRepository;
            this._fileTypeRepository = fileTypeRepository;
            this._loadTrackerRepository = loadTrackerRepository;
            this._session = sessionContext.Session;
        }

        public void SetFileTrackerStatus(FileTracker fileTracker, bool succeeded)
        {
            fileTracker.FileStatus = succeeded ? new FileStatus {FileStatusId = (int) FileStatusEnum.Completed} : new FileStatus {FileStatusId = (int) FileStatusEnum.Failed};
            this._fileTrackerRepository.Value.InsertOrUpdate(fileTracker);
        }

        public FileTracker StartOrContinueFileTracker(LoadTracker loadTracker, int fileTypeId, string fileName, DateTime? fileDate = null)
        {
            FileTracker fileTracker = loadTracker.FileTrackers?.FirstOrDefault(x => x.FileName == fileName);
            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                if (fileTracker == null)
                {
                    FileType fileType = this._fileTypeRepository.Value.GetById(fileTypeId);
                    fileTracker = new FileTracker
                    {
                        FileStatus = new FileStatus {FileStatusId = (int) FileStatusEnum.InProgress},
                        FileName = fileName,
                        FileType = fileType,
                        StartDateTime = DateTime.UtcNow,
                        FileRecordCount = 0,
                        FileDate = fileDate
                    };

                    if (loadTracker.FileTrackers == null)
                    {
                        loadTracker.FileTrackers = new List<FileTracker>();
                    }

                    loadTracker.FileTrackers.Add(fileTracker);
                }
                else
                {
                    fileTracker.FileStatus = new FileStatus {FileStatusId = (int) FileStatusEnum.InProgress};
                }

                this._loadTrackerRepository.Value.InsertOrUpdate(loadTracker);
                uow.Commit();
            }

            return fileTracker;
        }

        public string GenerateFileNameWithDate(string fileNamePattern, DateTime clientDateTime)
        {
            string date = clientDateTime.ToString(Format.FolderDate);
            string time = clientDateTime.ToString(Format.FolderTime);

            string formattedFileName = fileNamePattern
                .Replace(Format.FolderDate, date)
                .Replace(Format.FolderTime, time)
                .Replace("{", "")
                .Replace("}", "");
            return formattedFileName;
        }
    }
}
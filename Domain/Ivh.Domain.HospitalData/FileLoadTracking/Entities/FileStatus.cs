﻿namespace Ivh.Domain.HospitalData.FileLoadTracking.Entities
{
    public class FileStatus
    {
        public virtual int FileStatusId { get; set; }
        public virtual string FileStatusName { get; set; }
        public virtual string FileStatusDescription { get; set; }
    }
}
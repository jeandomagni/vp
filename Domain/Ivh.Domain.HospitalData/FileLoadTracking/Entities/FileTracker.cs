﻿namespace Ivh.Domain.HospitalData.FileLoadTracking.Entities
{
    using System;

    public class FileTracker
    {
        public virtual int FileTrackerId { get; set; }
        public virtual FileStatus FileStatus { get; set; }
        public virtual string FileName { get; set; }
        public virtual DateTime? FileDate { get; set; }
        public virtual FileType FileType { get; set; }
        public virtual int FileRecordCount { get; set; }
        public virtual string Notes { get; set; }
        public virtual DateTime StartDateTime { get; set; }
    }
}
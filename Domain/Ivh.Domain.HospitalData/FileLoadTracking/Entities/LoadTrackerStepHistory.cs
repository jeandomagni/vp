﻿namespace Ivh.Domain.HospitalData.FileLoadTracking.Entities
{
    using System;
    using Common.VisitPay.Enums;

    public class LoadTrackerStepHistory : IEquatable<LoadTrackerStepHistory>
    {
        public virtual int LoadTrackerId { get; set; }
        public virtual int LoadTrackerStepId { get; set; }
        public virtual LoadTypeEnum LoadTypeId { get; set; }
        public virtual LoadTrackerStepStatusEnum LoadTrackerStepStatusId { get; set; }

        #region equality
        public virtual bool Equals(LoadTrackerStepHistory other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return this.LoadTrackerId == other.LoadTrackerId && this.LoadTrackerStepId == other.LoadTrackerStepId;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != this.GetType())
            {
                return false;
            }

            return Equals((LoadTrackerStepHistory)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (this.LoadTrackerId * 397) ^ this.LoadTrackerStepId;
            }
        }
        #endregion equality
    }
}

﻿namespace Ivh.Domain.HospitalData.FileLoadTracking.Entities
{
    public class FileDirection
    {
        public virtual int FileDirectionId { get; set; }
        public virtual string FileDirectionName { get; set; }
    }
}
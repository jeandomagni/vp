﻿namespace Ivh.Domain.HospitalData.FileLoadTracking.Entities
{
    using System;

    public class FileType
    {
        public virtual int FileTypeId { get; set; }
        public virtual string FileTypeName { get; set; }
        public virtual string FileTypeDescription { get; set; }
        public virtual DateTime StartDate { get; set; }
        public virtual DateTime EndDate { get; set; }
        public virtual FileDirection FileDirection { get; set; }
        public virtual string FileDatePattern { get; set; }
        public virtual string FileMatchPattern { get; set; }
    }
}
﻿namespace Ivh.Domain.HospitalData.FileLoadTracking.Entities
{
    public class LoadType
    {
        public virtual int LoadTypeId { get; set; }
        public virtual string LoadTypeName { get; set; }
    }
}
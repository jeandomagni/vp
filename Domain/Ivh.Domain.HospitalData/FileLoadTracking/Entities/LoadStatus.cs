﻿namespace Ivh.Domain.HospitalData.FileLoadTracking.Entities
{
    public class LoadStatus
    {
        public virtual int LoadStatusId { get; set; }
        public virtual string LoadStatusName { get; set; }
        public virtual  string LoadStatusDescription { get; set; }
    }
}
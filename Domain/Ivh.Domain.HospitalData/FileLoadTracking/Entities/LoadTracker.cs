﻿namespace Ivh.Domain.HospitalData.FileLoadTracking.Entities
{
    using System;
    using System.Collections.Generic;

    public class LoadTracker
    {
        public virtual int LoadTrackerId { get; set; }
        public virtual LoadStatus LoadStatus { get; set; }
        public virtual string Notes { get; set; }
        public virtual DateTime LoadStartDateTime { get; set; }
        public virtual DateTime? LoadEndDateTime { get; set; }
        public virtual LoadType LoadType { get; set; }
        public virtual DateTime? ExtractDateTime { get; set; }
        public virtual string LeafFileFolder { get; set; }
        public virtual DateTime? LeafFileFolderDate { get; set; }
        public virtual bool IgnoreMissingFiles { get; set; }
        public virtual IList<FileTracker> FileTrackers { get; set; }
    }
}
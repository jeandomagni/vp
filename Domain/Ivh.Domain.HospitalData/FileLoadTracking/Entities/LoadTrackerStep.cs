﻿namespace Ivh.Domain.HospitalData.FileLoadTracking.Entities
{
    using Common.VisitPay.Enums;

    public class LoadTrackerStep
    {
        public virtual LoadTypeEnum LoadTypeId { get; set; }
        public virtual int LoadTrackerStepId { get; set; }
        public virtual bool IsActive { get; set; }
    }
}

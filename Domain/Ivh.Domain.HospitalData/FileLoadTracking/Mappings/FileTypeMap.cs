﻿namespace Ivh.Domain.HospitalData.FileLoadTracking.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class FileTypeMap : ClassMap<FileType>
    {
        public FileTypeMap()
        {
            this.ReadOnly();
            this.Schema(Schemas.Cdi.Etl);
            this.Table(nameof(FileType));
            this.Id(x => x.FileTypeId).Not.Nullable();
            this.Map(x => x.FileTypeName).Not.Nullable();
            this.Map(x => x.FileTypeDescription).Not.Nullable();
            this.Map(x => x.StartDate).Not.Nullable();
            this.Map(x => x.EndDate).Not.Nullable();
            this.Map(x => x.FileDatePattern).Nullable();
            this.Map(x => x.FileMatchPattern).Nullable();

            this.References(x => x.FileDirection).Column("FileDirectionId")
                .Not.Nullable()
                .Cascade.None()
                .LazyLoad()
                .Fetch.Join();
        }
    }
}
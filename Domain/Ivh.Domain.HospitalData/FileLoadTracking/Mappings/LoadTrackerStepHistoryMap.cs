﻿namespace Ivh.Domain.HospitalData.FileLoadTracking.Mappings
{
    using Common.Data.Constants;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class LoadTrackerStepHistoryMap : ClassMap<LoadTrackerStepHistory>
    {
        public LoadTrackerStepHistoryMap()
        {
            this.Schema(Schemas.Cdi.Etl);
            this.Table(nameof(LoadTrackerStepHistory));
            this.CompositeId()
                .KeyProperty(x => x.LoadTrackerId)
                .KeyProperty(x => x.LoadTrackerStepId);

            this.Map(x => x.LoadTrackerStepStatusId).CustomType<LoadTrackerStepStatusEnum>();
            this.Map(x => x.LoadTypeId).CustomType<LoadTypeEnum>();
        }
    }
}

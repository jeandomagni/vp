﻿namespace Ivh.Domain.HospitalData.FileLoadTracking.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class LoadStatusMap : ClassMap<LoadStatus>
    {
        public LoadStatusMap()
        {
            this.ReadOnly();
            this.Schema(Schemas.Cdi.Etl);
            this.Table(nameof(LoadStatus));
            this.Id(x => x.LoadStatusId).Not.Nullable();
            this.Map(x => x.LoadStatusName, "LoadStatus").Not.Nullable();
            this.Map(x => x.LoadStatusDescription).Not.Nullable();
        }
    }
}
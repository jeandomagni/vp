﻿namespace Ivh.Domain.HospitalData.FileLoadTracking.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class FileTrackerMap : ClassMap<FileTracker>
    {
        public FileTrackerMap()
        {
            this.Schema(Schemas.Cdi.Etl);
            this.Table(nameof(FileTracker));
            this.Id(x => x.FileTrackerId).Not.Nullable();
            this.Map(x => x.FileName).Nullable();
            this.Map(x => x.FileDate).Nullable();
            this.Map(x => x.FileRecordCount).Nullable();
            this.Map(x => x.Notes).Nullable();
            this.Map(x => x.StartDateTime).Not.Nullable();

            this.References(x => x.FileStatus).Column("FileStatusId")
                .Not.Nullable()
                .Cascade.None()
                .LazyLoad()
                .Fetch.Join();

            this.References(x => x.FileType).Column("FileTypeId")
                .Not.Nullable()
                .Cascade.All()
                .LazyLoad()
                .Fetch.Join();
        }
    }
}
﻿namespace Ivh.Domain.HospitalData.FileLoadTracking.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class FileStatusMap : ClassMap<FileStatus>
    {
        public FileStatusMap()
        {
            this.ReadOnly();
            this.Schema(Schemas.Cdi.Etl);
            this.Table(nameof(FileStatus));
            this.Id(x => x.FileStatusId).Not.Nullable();
            this.Map(x => x.FileStatusName, "FileStatus").Not.Nullable();
            this.Map(x => x.FileStatusDescription).Not.Nullable();
        }
    }
}
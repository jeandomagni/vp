﻿namespace Ivh.Domain.HospitalData.FileLoadTracking.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class LoadTypeMap : ClassMap<LoadType>
    {
        public LoadTypeMap()
        {
            this.ReadOnly();
            this.Schema(Schemas.Cdi.Etl);
            this.Table(nameof(LoadType));
            this.Id(x => x.LoadTypeId).Not.Nullable();
            this.Map(x => x.LoadTypeName).Not.Nullable();
        }
    }
}
﻿namespace Ivh.Domain.HospitalData.FileLoadTracking.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class LoadTrackerMap : ClassMap<LoadTracker>
    {
        public LoadTrackerMap()
        {
            this.Schema(Schemas.Cdi.Etl);
            this.Table(nameof(LoadTracker));
            this.Id(x => x.LoadTrackerId).Not.Nullable();
            this.Map(x => x.Notes).Nullable();
            this.Map(x => x.LoadStartDateTime).Not.Nullable();
            this.Map(x => x.LoadEndDateTime).Nullable();
            this.Map(x => x.ExtractDateTime).Nullable();
            this.Map(x => x.LeafFileFolder).Nullable();
            this.Map(x => x.LeafFileFolderDate).Nullable();
            this.Map(x => x.IgnoreMissingFiles).Nullable();

            this.References(x => x.LoadStatus).Column("LoadStatusId")
                .Not.Nullable()
                .Cascade.None()
                .LazyLoad()
                .Fetch.Join();

            this.References(x => x.LoadType).Column("LoadTypeId")
                .Not.Nullable()
                .Cascade.None()
                .LazyLoad()
                .Fetch.Join();

            this.HasMany(x => x.FileTrackers)
                .KeyColumn("LoadTrackerId")
                .Not.KeyNullable()
                .Cascade.All()
                .NotFound.Ignore()
                .Not.LazyLoad();
        }
    }
}
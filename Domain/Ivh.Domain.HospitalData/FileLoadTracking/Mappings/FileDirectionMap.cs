﻿namespace Ivh.Domain.HospitalData.FileLoadTracking.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class FileDirectionMap : ClassMap<FileDirection>
    {
        public FileDirectionMap()
        {
            this.Schema(Schemas.Cdi.Etl);
            this.Table(nameof(FileDirection));
            this.Id(x => x.FileDirectionId).Not.Nullable();
            this.Map(x => x.FileDirectionName).Not.Nullable();
        }
    }
}
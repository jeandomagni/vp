﻿namespace Ivh.Domain.HospitalData.FileLoadTracking.Mappings
{
    using Common.Data.Constants;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class LoadTrackerStepMap : ClassMap<LoadTrackerStep>
    {
        public LoadTrackerStepMap()
        {
            this.Schema(Schemas.Cdi.Etl);
            this.Table(nameof(LoadTrackerStep));
            this.Id(x => x.LoadTrackerStepId).Not.Nullable();

            this.Map(x => x.IsActive);
            this.Map(x => x.LoadTypeId).CustomType<LoadTypeEnum>();
        }
    }
}

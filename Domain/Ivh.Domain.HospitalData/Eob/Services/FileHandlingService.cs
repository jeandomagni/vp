﻿namespace Ivh.Domain.HospitalData.Eob.Services
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using FileLoadTracking.Entities;
    using FileLoadTracking.Interfaces;
    using Logging.Interfaces;

    public class FileHandlingService : DomainService, IFileHandlingService
    {
        private readonly Lazy<ILoadTrackerRepository> _loadTrackerRepository;
        private readonly Lazy<ILoadStatusRepository> _loadStatusRepository;
        private readonly Lazy<ILoadTypeRepository> _loadTypeRepository;
        private readonly Lazy<ILoggingProvider> _logger;
        private readonly Lazy<IFileStatusRepository> _fileStatusRepository;
        private readonly Lazy<IFileTypeRepository> _fileTypeRepository;

        public FileHandlingService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<ILoadTrackerRepository> loadTrackerRepository,
            Lazy<ILoadStatusRepository> loadStatusRepository,
            Lazy<ILoadTypeRepository> loadTypeRepository,
            Lazy<ILoggingProvider> logger,
            Lazy<IFileStatusRepository> fileStatusRepository,
            Lazy<IFileTypeRepository> fileTypeRepository) : base(serviceCommonService)
        {
            this._loadTrackerRepository = loadTrackerRepository;
            this._loadStatusRepository = loadStatusRepository;
            this._loadTypeRepository = loadTypeRepository;
            this._logger = logger;
            this._fileStatusRepository = fileStatusRepository;
            this._fileTypeRepository = fileTypeRepository;
        }

        /// <summary>
        /// Looks for files and folders. Expects the pattern "path\YYYYMMDD\subDirectoryName" to be found on the file system.
        /// </summary>
        /// <param name="path">folder containing date named folders</param>
        /// <param name="subDirectoryName">folder inside of date named folder YYYYMMDD\subDirectoryName</param>
        /// <returns></returns>
        public IList<LoadTracker> GetDirectoriesAndFiles(string path, string subDirectoryName)
        {

            if (Directory.Exists(path))
            {
                // get latest folder processed from LoadTracker of status, processing, succeeded
                IList<LoadTracker> lastLoadTrackers = this._loadTrackerRepository.Value.GetLastEobLoadTrackers();

                // sometimes all the LoadTrackers are failed, check to see if the last one has failed
                // so we don't keep reloading all the failed LoadTrackers
                if (lastLoadTrackers.Count == 0)
                {
                    LoadTracker lastFailedEobLoadTracker = this._loadTrackerRepository.Value.GetLastFailedEobLoadTracker();
                    if (lastFailedEobLoadTracker != null)
                    {
                        lastLoadTrackers.Add(lastFailedEobLoadTracker);
                    }
                }

                this.LogMessage(lastLoadTrackers, "lastLoadTrackers");
                DateTime lastLoadedDate = lastLoadTrackers.Count > 0 ? lastLoadTrackers[0].LeafFileFolderDate.Value : new DateTime(1900, 1, 1);

                // get that folder and all newer folders
                DateTime datetime;
                IList<DirectoryInfo> inboundArchiveDirectory = new DirectoryInfo(path).GetDirectories().ToList();
                IList<DirectoryInfo> dateDirectories = inboundArchiveDirectory
                    .Where(dir => DateTime.TryParseExact(dir.Name, Format.FolderDate, new CultureInfo("en-US"), DateTimeStyles.None, out datetime))
                    .Where(dir => DateTime.Compare(DateTime.ParseExact(dir.Name, Format.FolderDate, null), lastLoadedDate) >= 0).ToList();

                LoadStatus loadStatus = this._loadStatusRepository.Value.GetById((int)LoadStatusEnum.InProgress);
                LoadType loadType = this._loadTypeRepository.Value.GetById((int)LoadTypeEnum.Daily835RemitInboundProcess);
                FileStatus fileStatus = this._fileStatusRepository.Value.GetById((int)FileStatusEnum.InProgress);
                FileType fileType = this._fileTypeRepository.Value.GetById((int)FileTypeEnum.Eob835Remit);


                IList<LoadTracker> loadTrackersToProcess = new List<LoadTracker>();

                // if we didn't complete the last load:
                //   1. get the files we haven't completed 
                //   2. add any new ones in that directory. 
                //   3. add any new dirs and files since last load
                int[] processableLoadTrackerStatuses = new[] { (int)LoadStatusEnum.InProgress, (int)LoadStatusEnum.Failed };
                if (lastLoadTrackers.Count > 0 && lastLoadTrackers.Any(x => x.LoadStatus.LoadStatusId != (int)LoadStatusEnum.Completed))
                {
                    // 1. here are all the ones from the database. We need the FileTrackerId so we can update the record in the db.
                    // Also remove the processed filetrackers from the returned loadtrackers.
                    foreach (LoadTracker loadTracker in lastLoadTrackers.Where(x => processableLoadTrackerStatuses.Contains(x.LoadStatus.LoadStatusId)))
                    {
                        IList<FileTracker> fileTrackers = loadTracker.FileTrackers
                            .Where(x => x.FileStatus.FileStatusId != (int)FileStatusEnum.Completed)
                            .Where(x => x.FileStatus.FileStatusId != (int)FileStatusEnum.Failed).ToList();

                        // 2. get the ones not in the database but in the directory. Maybe some new files were added to this dir since the last time we tired to process this dir.
                        DirectoryInfo unprocessedDir = dateDirectories.FirstOrDefault(dir => dir.Name.Equals(loadTracker.LeafFileFolder));
                        IList<string> allFilesInLoadTracker = loadTracker.FileTrackers.Select(x => x.FileName).ToList();

                        fileTrackers.AddRange(unprocessedDir?.GetDirectories().FirstOrDefault(d => d.Name.Equals(subDirectoryName))?
                            .GetFiles()
                            .Where(f => !allFilesInLoadTracker.Contains(f.FullName))
                            .Select(f => new FileTracker
                            {
                                FileName = f.FullName,
                                FileType = fileType,
                                FileStatus = fileStatus,
                                Notes = f.CreationTime.ToString(Format.DateTimeFormatSql),
                                StartDateTime = DateTime.UtcNow,
                                FileDate = null
                            })
                            .ToList());

                        loadTracker.FileTrackers = fileTrackers;
                    }

                    ((List<LoadTracker>)loadTrackersToProcess).AddRange(lastLoadTrackers.Where(x => processableLoadTrackerStatuses.Contains(x.LoadStatus.LoadStatusId)));


                    // 3. now get any from any other newer directories
                    List<string> loadTrackerFolders = lastLoadTrackers.Select(x => x.LeafFileFolder).ToList();
                    List<DirectoryInfo> newDirectoryFolders = dateDirectories.Where(x => !loadTrackerFolders.Contains(x.Name)).ToList();

                    IList<LoadTracker> otherDirsAndFiles = newDirectoryFolders
                        .Select(dir => new LoadTracker()
                        {
                            LoadStartDateTime = DateTime.UtcNow,
                            LeafFileFolder = dir.Name,
                            LeafFileFolderDate = DateTime.ParseExact(dir.Name, Format.FolderDate, null),
                            LoadStatus = loadStatus,
                            LoadType = loadType,
                            ExtractDateTime = null,
                            LoadEndDateTime = null,
                            FileTrackers = dir.GetDirectories().FirstOrDefault(d => d.Name.Equals(subDirectoryName))?.GetFiles()
                                .Select(f => new FileTracker
                                {
                                    FileName = f.FullName,
                                    FileType = fileType,
                                    FileStatus = fileStatus,
                                    Notes = f.CreationTime.ToString(Format.DateTimeFormatSql),
                                    StartDateTime = DateTime.UtcNow,
                                    FileDate = null
                                })
                                .ToList()
                        })
                        .ToList();

                    ((List<LoadTracker>)loadTrackersToProcess).AddRange(otherDirsAndFiles);
                    this.LogMessage(loadTrackersToProcess, "loadTrackersToProcess - not happy path");
                }
                else
                {
                    IList<string> allFilesInLastLoadTracker = lastLoadTrackers.SelectMany(x => x.FileTrackers).Select(x => x.FileName).ToList();
                    // for all folders get any files added to directory since last file processed date to list
                    loadTrackersToProcess = dateDirectories.OrderBy(dir => dir.CreationTime)
                        .Where(dir => DateTime.Compare(DateTime.ParseExact(dir.Name, Format.FolderDate, null), lastLoadedDate) >= 0)
                        .Select(dir => new LoadTracker()
                        {
                            LoadStartDateTime = DateTime.UtcNow,
                            LeafFileFolder = dir.Name,
                            LeafFileFolderDate = DateTime.ParseExact(dir.Name, Format.FolderDate, null),
                            LoadStatus = loadStatus,
                            LoadType = loadType,
                            ExtractDateTime = null,
                            LoadEndDateTime = null,
                            FileTrackers = dir.GetDirectories().FirstOrDefault(d => d.Name.Equals(subDirectoryName))?.GetFiles()
                                .Where(f => !allFilesInLastLoadTracker.Contains(f.FullName))
                                .Select(f => new FileTracker
                                {
                                    FileName = f.FullName,
                                    FileType = fileType,
                                    FileStatus = fileStatus,
                                    Notes = f.CreationTime.ToString(Format.DateTimeFormatSql),
                                    StartDateTime = DateTime.UtcNow,
                                    FileDate = null
                                })
                                .ToList()
                        })
                        .ToList();
                    this.LogMessage(loadTrackersToProcess, "loadTrackersToProcess - happy path");
                }
                List<LoadTracker> finalLoadTrackersToProcess = loadTrackersToProcess
                    .OrderBy(x => x.LeafFileFolder).ToList()
                    .Where(x => x.FileTrackers != null)
                    .Where(x => x.FileTrackers.Any()).ToList();

                this.LogMessage(finalLoadTrackersToProcess, "finalLoadTrackersToProcess");
                return finalLoadTrackersToProcess;

            }
            throw new IOException($"Exception reading Inbound Claims directory. {path} does not exist.");
        }


        private void LogMessage(IList<LoadTracker> loadTrackers, string extraLogInfo)
        {
            if (loadTrackers.Count > 0)
            {
                string loadTrackerIds = string.Join(", ", loadTrackers.Select(x => x.LoadTrackerId.ToString()));
                string loadTrackerFolders = string.Join(", ", loadTrackers.Select(x => x.LeafFileFolder));
                string loadTrackerFileCounts = string.Join(", ", loadTrackers.Select(x => x.FileTrackers?.Count.ToString() ?? string.Empty));
                this._logger.Value.Debug(() => $"FileHandlingService: {extraLogInfo}: LoadTrackerId to process {loadTrackerIds}, folder: {loadTrackerFolders}, file count: {loadTrackerFileCounts}");
            } else
            {
                this._logger.Value.Debug(() => $"FileHandlingService: {extraLogInfo}: No loadTrackers returned.");
            }
        }
    }

}
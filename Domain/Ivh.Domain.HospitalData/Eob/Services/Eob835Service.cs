﻿namespace Ivh.Domain.HospitalData.Eob.Services
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.ServiceBus.Interfaces;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.Eob;
    using Common.VisitPay.Strings;
    using Constants;
    using Core.SystemException.Interfaces;
    using FileLoadTracking.Entities;
    using FileLoadTracking.Interfaces;
    using indice.Edi;
    using Interfaces;
    using Logging.Interfaces;
    using MassTransit;
    using Newtonsoft.Json;
    using NHibernate;
    using NHibernate.Criterion;
    using SystemException = Core.SystemException.Entities.SystemException;
    using HsEob = Entities;
    using EobPayerFilter = Entities.EobPayerFilter;
    using AppEob = Domain.Eob.Entities;
    using CommonEob = Ivh.Application.Core.Common.Models.Eob;

    public class Eob835Service : DomainService, IEob835Service
    {
        private const string InactiveRarcMetric = "Eob.Inactive_RARC";
        private const string InactiveCarcMetric = "Eob.Inactive_CARC";
        private const string UnknownPayerMetric = "Eob.Unknown_Payer";
        private const string ErrorProcessing835 = "Eob.Processing_Error";

        private readonly Lazy<IEob835Repository> _eob835Repository;
        private readonly Lazy<IEobPayerFilterRepository> _eobPayerFilterRepository;
        private readonly Lazy<IExternalLinkRepository> _externalLinkRepository;
        private readonly Lazy<IEobClaimAdjustmentReasonCodeRepository> _remittanceMasterCodeRepository;
        private readonly Lazy<IEobClaimAdjustmentReasonCodeDisplayMapRepository> _eobClaimAdjustmentReasonCodeDisplayMapRepository;
        private readonly Lazy<IEobDisplayCategoryRepository> _eobDisplayCategoryRepository;
        private readonly Lazy<IEobClaimsLinkRepository> _eobClaimsLinkRepository;
        private readonly Lazy<IEobRemittanceAdviceRemarkCodeRepository> _remittanceRemarkCodeRepository;
        private readonly Lazy<IMetricsProvider> _metricsProvider;
        private readonly Lazy<ILoadTrackerRepository> _loadTrackerRepository;
        private readonly Lazy<IFileTrackerRepository> _fileTrackerRepository;
        private readonly Lazy<IFileHandlingService> _fileHandlingService;
        private readonly Lazy<ILoadStatusRepository> _loadStatusRepository;
        private readonly Lazy<IFileStatusRepository> _fileStatusRepository;
        private readonly Lazy<ISystemExceptionService> _systemExceptionService;
        private readonly ISession _session;

        public Eob835Service(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IEob835Repository> eob835Repository,
            Lazy<IEobPayerFilterRepository> eobPayerFilterRepository,
            Lazy<IExternalLinkRepository> externalLinkRepository,
            Lazy<IEobClaimsLinkRepository> eobClaimsLinkRepository,
            Lazy<IEobClaimAdjustmentReasonCodeRepository> remittanceCodeRepository,
            Lazy<IEobClaimAdjustmentReasonCodeDisplayMapRepository> eobClaimAdjustmentReasonCodeDisplayMapRepository,
            Lazy<IEobDisplayCategoryRepository> eobDisplayCategoryRepository,
            Lazy<IEobRemittanceAdviceRemarkCodeRepository> remittanceRemarkCodeRepository,
            Lazy<IMetricsProvider> metricsProvider,
            Lazy<ILoadTrackerRepository> loadTrackerRepository,
            Lazy<IFileTrackerRepository> fileTrackerRepository,
            Lazy<IFileHandlingService> fileHandlingService,
            Lazy<ILoadStatusRepository> loadStatusRepository,
            Lazy<IFileStatusRepository> fileStatusRepository,
            Lazy<ISystemExceptionService> systemExceptionService,
            ISessionContext<CdiEtl> sessionContext) : base(serviceCommonService)
        {
            this._eob835Repository = eob835Repository;
            this._eobPayerFilterRepository = eobPayerFilterRepository;
            this._externalLinkRepository = externalLinkRepository;
            this._eobClaimsLinkRepository = eobClaimsLinkRepository;
            this._remittanceMasterCodeRepository = remittanceCodeRepository;
            this._eobClaimAdjustmentReasonCodeDisplayMapRepository = eobClaimAdjustmentReasonCodeDisplayMapRepository;
            this._eobDisplayCategoryRepository = eobDisplayCategoryRepository;
            this._remittanceRemarkCodeRepository = remittanceRemarkCodeRepository;
            this._metricsProvider = metricsProvider;
            this._loadTrackerRepository = loadTrackerRepository;
            this._fileTrackerRepository = fileTrackerRepository;
            this._fileHandlingService = fileHandlingService;
            this._loadStatusRepository = loadStatusRepository;
            this._fileStatusRepository = fileStatusRepository;
            this._systemExceptionService = systemExceptionService;
            this._session = sessionContext.Session;
            this._shouldTruncate = new Lazy<bool>(() => this.Client.Value.FeatureEobImportCheckStringLengths);

        }

        private ConcurrentBag<HsEob.EobRemittanceAdviceRemarkCode> RemittanceAdviceRemarkCodes { get; set; }
        private ConcurrentBag<HsEob.EobClaimAdjustmentReasonCode> ClaimAdjustmentReasonCodes { get; set; }
        private ConcurrentBag<EobPayerFilter> EobPayerFilters { get; set; }
        private ConcurrentBag<FileStatus> FileStatuses { get; set; }

        private static readonly object Lock = new object();
        private readonly Lazy<bool> _shouldTruncate;
        private bool FailLoadTracker { get; set; }

        private class FileData
        {
            public DateTime StartDate { get; set; }
            public DateTime StopDate { get; set; }
            public string FileName { get; set; }
            public DateTime FileDate { get; set; }
            public char Delimiter { get; set; }
        }

        private void InitLookupCollections()
        {
            this.RemittanceAdviceRemarkCodes = new ConcurrentBag<HsEob.EobRemittanceAdviceRemarkCode>(this._remittanceRemarkCodeRepository.Value.GetQueryable().ToList());
            this.ClaimAdjustmentReasonCodes = new ConcurrentBag<HsEob.EobClaimAdjustmentReasonCode>(this._remittanceMasterCodeRepository.Value.GetQueryable().ToList());
            this.EobPayerFilters = new ConcurrentBag<EobPayerFilter>(this._eobPayerFilterRepository.Value.GetQueryable().ToList());
            this.FileStatuses = new ConcurrentBag<FileStatus>(this._fileStatusRepository.Value.GetQueryable().ToList());
        }

        public void Load835File()
        {
            Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.BelowNormal;
            string eobPath = this.Client.Value.EobArchive835Location;
            IList<LoadTracker> directoriesAndFiles = this._fileHandlingService.Value.GetDirectoriesAndFiles(eobPath, "Remits");

            this.InitLookupCollections();
            this.SyncEobLookupTables();

            foreach (LoadTracker loadTracker in directoriesAndFiles)
            {
                if (loadTracker.FileTrackers.Count == 0)
                {
                    return;
                }

                this.FailLoadTracker = false;
                loadTracker.LoadStartDateTime = DateTime.UtcNow;
                this._loadTrackerRepository.Value.Insert(loadTracker);

                ConcurrentBag<Tuple<HsEob.InterchangeControlHeaderTrailer, FileTracker>> listOfObjects = this.GetProcessed835sParallel(loadTracker.FileTrackers);

                IEnumerable<Tuple<HsEob.InterchangeControlHeaderTrailer, FileTracker>> validInterchanges = listOfObjects.Where(x => x.Item2.FileStatus.FileStatusId == (int)FileStatusEnum.InProgress);

                List<AppEob.ClaimPaymentInformationHeader> claimPaymentInformationHeaderList = this.ExtractClaimPaymentInformation(validInterchanges);

                this.SendClaimPaymentInformationNotifications(claimPaymentInformationHeaderList);

                MarkFilesCompleted(validInterchanges);

                loadTracker.LoadStatus = this.FailLoadTracker ? this._loadStatusRepository.Value.GetById((int)LoadStatusEnum.Failed) : this._loadStatusRepository.Value.GetById((int)LoadStatusEnum.Completed);
                loadTracker.LoadEndDateTime = DateTime.UtcNow;
                this._loadTrackerRepository.Value.InsertOrUpdate(loadTracker);
            }
        }

        public void SendClaimPaymentInformationNotifications(List<AppEob.ClaimPaymentInformationHeader> claimPaymentInformationHeaderList)
        {
            foreach (AppEob.ClaimPaymentInformationHeader claimPaymentInformationHeader in claimPaymentInformationHeaderList)
            {
                EobClaimPaymentMessage message = Mapper.Map<EobClaimPaymentMessage>(claimPaymentInformationHeader);
                this.Bus.Value.PublishMessage(message).Wait();
            }
        }

        private void SyncEobLookupTables()
        {
            List<HsEob.EobPayerFilter> eobPayerFilters = this._eobPayerFilterRepository.Value.GetQueryable().ToList();
            foreach (HsEob.EobPayerFilter filter in eobPayerFilters)
            {
                EobPayerFilterMessage eobPayerFilterMessage = Mapper.Map<EobPayerFilterMessage>(filter);
                this.Bus.Value.PublishMessage(eobPayerFilterMessage).Wait();
            }

            List<HsEob.EobPayerFilterExternalLink> eobPayerFilterEobExternalLinks = eobPayerFilters
                .SelectMany(x => x.EobPayerFilterEobExternalLinks).ToList();
            foreach (HsEob.EobPayerFilterExternalLink eobPayerFilterExternalLink in eobPayerFilterEobExternalLinks)
            {
                EobPayerFilterEobExternalLinkMessage eobPayerFilterExternalLinkMessage = Mapper.Map<EobPayerFilterEobExternalLinkMessage>(eobPayerFilterExternalLink);
                this.Bus.Value.PublishMessage(eobPayerFilterExternalLinkMessage).Wait();
            }

            List<HsEob.ExternalLink> eobExternalLinks = this._externalLinkRepository.Value.GetQueryable().ToList();
            foreach (HsEob.ExternalLink externalLink in eobExternalLinks)
            {
                EobExternalLinkMessage eobExternalLinkMessage = Mapper.Map<EobExternalLinkMessage>(externalLink);
                this.Bus.Value.PublishMessage(eobExternalLinkMessage).Wait();
            }

            List<HsEob.EobDisplayCategory> eobDisplayCategories = this._eobDisplayCategoryRepository.Value.GetQueryable().ToList();
            foreach (HsEob.EobDisplayCategory eobDisplayCategory in eobDisplayCategories)
            {
                EobDisplayCategoryMessage eobDisplayCategoryMessage = Mapper.Map<EobDisplayCategoryMessage>(eobDisplayCategory);
                this.Bus.Value.PublishMessage(eobDisplayCategoryMessage).Wait();
            }

            List<HsEob.EobClaimAdjustmentReasonCodeDisplayMap> eobClaimAdjustmentReasonCodeDisplayMaps = this._eobClaimAdjustmentReasonCodeDisplayMapRepository.Value.GetQueryable().ToList();
            foreach (HsEob.EobClaimAdjustmentReasonCodeDisplayMap eobClaimAdjustmentReasonCodeDisplayMap in eobClaimAdjustmentReasonCodeDisplayMaps)
            {
                EobClaimAdjustmentReasonCodeDisplayMapMessage eobClaimAdjustmentReasonCodeDisplayMapMessage = Mapper.Map<EobClaimAdjustmentReasonCodeDisplayMapMessage>(eobClaimAdjustmentReasonCodeDisplayMap);
                this.Bus.Value.PublishMessage(eobClaimAdjustmentReasonCodeDisplayMapMessage).Wait();
            }

            List<HsEob.EobClaimAdjustmentReasonCode> eobClaimAdjustmentReasonCodes = this._remittanceMasterCodeRepository.Value.GetQueryable().ToList();
            foreach (HsEob.EobClaimAdjustmentReasonCode eobClaimAdjustmentReasonCode in eobClaimAdjustmentReasonCodes)
            {
                EobClaimAdjustmentReasonCodeMessage eobClaimAdjustmentReasonCodeMessage = Mapper.Map<EobClaimAdjustmentReasonCodeMessage>(eobClaimAdjustmentReasonCode);
                this.Bus.Value.PublishMessage(eobClaimAdjustmentReasonCodeMessage).Wait();
            }
        }

        private void MarkFilesCompleted(IEnumerable<Tuple<HsEob.InterchangeControlHeaderTrailer, FileTracker>> listOfObjects)
        {
            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                foreach (Tuple<HsEob.InterchangeControlHeaderTrailer, FileTracker> tuple in listOfObjects)
                {
                    tuple.Item2.FileStatus = this.FileStatuses.FirstOrDefault(x => x.FileStatusId == (int)FileStatusEnum.Completed);
                    this._fileTrackerRepository.Value.InsertOrUpdate(tuple.Item2);
                    this._session.Evict(tuple.Item2);
                }
                uow.Commit();
            }
        }

        public List<AppEob.ClaimPaymentInformationHeader> ExtractClaimPaymentInformation(HsEob.InterchangeControlHeaderTrailer interchange)
        {
            Tuple<HsEob.InterchangeControlHeaderTrailer, FileTracker> tuple = new Tuple<HsEob.InterchangeControlHeaderTrailer, FileTracker>(interchange, null);
            return this.ExtractClaimPaymentInformation(tuple.ToListOfOne());
        }

        private List<AppEob.ClaimPaymentInformationHeader> ExtractClaimPaymentInformation(IEnumerable<Tuple<HsEob.InterchangeControlHeaderTrailer, FileTracker>> listOfObjects)
        {
            List<AppEob.ClaimPaymentInformationHeader> claimPaymentInformationHeaderList = new List<AppEob.ClaimPaymentInformationHeader>();

            foreach (Tuple<HsEob.InterchangeControlHeaderTrailer, FileTracker> tuple in listOfObjects)
            {
                HsEob.InterchangeControlHeaderTrailer interchange = tuple.Item1;

                List<string> claimNumbers = null;
                try
                {
                    claimNumbers = interchange.FunctionalGroupHeaderTrailerHeaders
                        .SelectMany(x => x.TransactionHeaders)
                        .SelectMany(x => x.Transactions)
                        .SelectMany(x => x.ClaimPaymentInformations)
                        .Select(x => x.Clp01ClaimSubmittersIdentifier)
                        .ToList();
                }
                catch (Exception e)
                {
                    this.LogException(tuple.Item2, false, $"Unable to find Clp01ClaimSubmittersIdentifier values in ClaimPaymentInformations collection.\n{ExceptionHelper.AggregateExceptionToString(e)}", tuple.Item2.FileName);
                    continue;
                }

                List<HsEob.EobClaimsLink> eobClaimsLinkList = this._eobClaimsLinkRepository.Value.GetClaimsLinks(claimNumbers);

                foreach (HsEob.FunctionalGroupHeaderTrailer gs in interchange.FunctionalGroupHeaderTrailerHeaders)
                {
                    foreach (HsEob.TransactionSetHeaderTrailer835 th in gs.TransactionHeaders)
                    {
                        foreach (HsEob.HeaderNumber t in th.Transactions)
                        {
                            foreach (HsEob.ClaimPaymentInformation claimPaymentInformationHs in t.ClaimPaymentInformations)
                            {
                                HsEob.EobClaimsLink eobClaimsLink = eobClaimsLinkList.FirstOrDefault(x => x.ClaimNumber == claimPaymentInformationHs.Clp01ClaimSubmittersIdentifier);

                                CommonEob.ClaimPaymentInformation commonClaimPaymentInformation = Mapper.Map<CommonEob.ClaimPaymentInformation>(claimPaymentInformationHs);

                                JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings
                                {
                                    NullValueHandling = NullValueHandling.Ignore
                                };
                                string claimPaymentInformationJson = JsonConvert.SerializeObject(commonClaimPaymentInformation, jsonSerializerSettings);
                                string payerName = th.PayerDetails?.N102PayerName ?? this.EobPayerFilters.First(x => x.EobPayerFilterId == 0).UniquePayerValue;

                                AppEob.ClaimPaymentInformationHeader claimPaymentInformationHeader = new AppEob.ClaimPaymentInformationHeader()
                                {
                                    InterchangeSenderId = interchange.Isa06SenderId,
                                    InterchangeReceiverId = interchange.Isa08ReceiverId,
                                    InterchangeDateTime = interchange.Isa0910DateTime,
                                    InterchangeControlNumber = interchange.Isa13ControlNumber,
                                    GroupControlNumber = gs.Gs06GroupControlNumber,
                                    TransactionDate = th.Bpr16Date,
                                    TransactionSetControlNumber = th.St02TransactionSetControlNumber ?? "1",
                                    TransactionSetLineNumber = t.Lx01AssignedNumber,
                                    EobPayerFilterId = th.EobPayerFilter?.EobPayerFilterId ?? -1,
                                    PayerName = payerName,
                                    ClaimNumber = claimPaymentInformationHs.Clp01ClaimSubmittersIdentifier,
                                    BillingSystemId = eobClaimsLink?.BillingSystemId ?? -1,
                                    VisitSourceSystemKey = eobClaimsLink?.VisitSourceSystemKey,
                                    ClaimPaymentInformationJson = claimPaymentInformationJson
                                };

                                claimPaymentInformationHeaderList.Add(claimPaymentInformationHeader);
                            }
                            //parsed CLP segments are no longer needed in memory at this point 
                            t.ClaimPaymentInformations = null;
                        }
                    }
                }
            }

            return claimPaymentInformationHeaderList;
        }

        private ConcurrentBag<Tuple<HsEob.InterchangeControlHeaderTrailer, FileTracker>> GetProcessed835sParallel(IList<FileTracker> fileTrackers)
        {
            ConcurrentBag<Tuple<HsEob.InterchangeControlHeaderTrailer, FileTracker>> listOfObjects = new ConcurrentBag<Tuple<HsEob.InterchangeControlHeaderTrailer, FileTracker>>();
            Parallel.ForEach(fileTrackers, new ParallelOptions()
            {
                MaxDegreeOfParallelism = this.ApplicationSettingsService.Value.Load835FileMaxDegreeOfParallelism.Value
            }, fileTracker =>
            {
                FileData fileData = new FileData
                {
                    FileName = fileTracker.FileName,
                    FileDate = DateTime.ParseExact(fileTracker.Notes, Format.DateTimeFormatSql, null)
                };

                using (StreamReader stream = new StreamReader(fileData.FileName))
                {
                    if (this.VerifyItsAn835File(stream, fileData))
                    {
                        stream.BaseStream.Position = 0;
                        stream.DiscardBufferedData();
                        try
                        {
                            fileTracker.StartDateTime = DateTime.UtcNow;
                            IEdiGrammar grammar = this.GetGrammar(fileData.Delimiter);
                            HsEob.InterchangeControlHeaderTrailer healthCareRemit835 = new EdiSerializer().Deserialize<HsEob.InterchangeControlHeaderTrailer>(stream, grammar);
                            healthCareRemit835 = Mapper.Map<HsEob.InterchangeControlHeaderTrailer>(healthCareRemit835);

                            healthCareRemit835.FileTrackerId = fileTracker.FileTrackerId;
                            this.Process835(healthCareRemit835.FunctionalGroupHeaderTrailerHeaders, fileData);
                            if (this._shouldTruncate.Value)
                            {
                                MaxLengthTruncator.TruncateProperties(healthCareRemit835);
                            }
                            listOfObjects.Add(new Tuple<HsEob.InterchangeControlHeaderTrailer, FileTracker>(healthCareRemit835, fileTracker));

                        }
                        catch (Exception e)
                        {
                            this.LogException(fileTracker, true, ExceptionHelper.AggregateExceptionToString(e), fileData.FileName);
                        }
                    }
                    else
                    {
                        this.LogException(fileTracker, false, "This does not seem to be an EOB 835 file", fileData.FileName);
                    }
                }
            });
            return listOfObjects;
        }

        private void Process835(IList<HsEob.FunctionalGroupHeaderTrailer> functionalGroupHeaderTrailers, FileData fileData)
        {
            if (functionalGroupHeaderTrailers == null)
            {
                return;
            }
            foreach (HsEob.FunctionalGroupHeaderTrailer functionalGroup in functionalGroupHeaderTrailers)
            {
                if (functionalGroup.TransactionHeaders == null)
                {
                    continue;
                }
                foreach (HsEob.TransactionSetHeaderTrailer835 transactionSet in functionalGroup.TransactionHeaders)
                {
                    this.TieTransactionSetToPayer(transactionSet);

                    if (transactionSet.Transactions == null)
                    {
                        continue;
                    }

                    foreach (HsEob.HeaderNumber transaction in transactionSet.Transactions)
                    {

                        if (transaction.ClaimPaymentInformations == null)
                        {
                            continue;
                        }
                        foreach (HsEob.ClaimPaymentInformation claimPaymentInformation in transaction.ClaimPaymentInformations)
                        {

                            bool claimPaymentCodesNeedValidating = false;
                            fileData.StartDate = DateTime.MinValue;
                            fileData.StopDate = DateTime.MinValue;

                            this.SetDatesForCodeChecking(claimPaymentInformation.Dtm1StatementFromOrToDates, fileData);

                            // normalize the CAS segments. validate remit codes (CAS02, CAS05, CAS08, CAS11, CAS14 and CAS17) if there are dates to use.
                            claimPaymentInformation.ClaimAdjustmentPivots = this.PivotCasSegments(claimPaymentInformation.CasClaimAdjustments, fileData);

                            // if Claim Payment Information DTM Statement From or to Date is null, we have to 
                            // use the dates from the Service Payment Information(s) to verify the codes are valid.
                            if (claimPaymentInformation.Dtm1StatementFromOrToDates == null)
                            {
                                claimPaymentCodesNeedValidating = true;
                            }
                            else
                            {
                                // make sure remark codes are valid (MIA03, MOA03)
                                this.ValidateMoaMiaRemarkCodes(claimPaymentInformation, fileData);
                            }

                            if (claimPaymentInformation.ServicePaymentInformations == null)
                            {
                                continue;
                            }
                            foreach (HsEob.ServicePaymentInformation servicePaymentInformation in claimPaymentInformation.ServicePaymentInformations)
                            {
                                this.SetDatesForCodeChecking(servicePaymentInformation.DtmServiceDates, fileData);

                                // if Claim Payment Information DTM Statement From or to Date was null, 
                                // we have to verify the codes are valid for each of the Service Payment Info dates
                                if (claimPaymentCodesNeedValidating)
                                {
                                    this.ValidateClaimPaymentInformationCodes(claimPaymentInformation, fileData);
                                }

                                // normalize the CAS segments and validate remit codes (CAS02, CAS05, CAS08, CAS11, CAS14 and CAS17)
                                servicePaymentInformation.ClaimAdjustmentPivots = this.PivotCasSegments(servicePaymentInformation.CasServiceAdjustments, fileData);

                                // make sure remark codes are valid (LQ02)
                                this.ValidateLqRemarkCodes(servicePaymentInformation, fileData);
                            }
                        }
                    }
                }
            }
        }

        private void TieTransactionSetToPayer(HsEob.TransactionSetHeaderTrailer835 transactionSet)
        {
            string payerName = transactionSet.PayerDetails?.N102PayerName;
            if (payerName != null)
            {
                transactionSet.EobPayerFilter = this.EobPayerFilters.FirstOrDefault(x => payerName.Contains(x.UniquePayerValue));
            }

            if (payerName == null || transactionSet.EobPayerFilter == null)
            {
                // if we can't find a payer, set it to Unknown, increment metric, and throw system exception
                transactionSet.EobPayerFilter = this.EobPayerFilters.First(x => x.EobPayerFilterId == 0);
                this._metricsProvider.Value.Increment(UnknownPayerMetric);
                lock (Lock)
                {
                    this._systemExceptionService.Value.LogSystemException(new SystemException
                    {
                        SystemExceptionDescription = $"Payer with contact information '{payerName}' is not in the ebo.EobPayerFilter table. This 835 will not be shown in the UI.",
                        SystemExceptionType = SystemExceptionTypeEnum.EobUnknownPayer
                    });
                }
            }
        }


        private void ValidateClaimPaymentInformationCodes(HsEob.ClaimPaymentInformation claimPaymentInformation, FileData fileData)
        {
            // make sure remark codes are valid (MIA03, MOA03)
            this.ValidateMoaMiaRemarkCodes(claimPaymentInformation, fileData);
            // validate the CAS codes
            foreach (HsEob.ClaimAdjustmentPivot cap in claimPaymentInformation.ClaimAdjustmentPivots)
            {
                this.ValidateClaimAdjustmentReasonCode(cap.ClaimAdjustmentReasonCode, fileData);
            }
        }

        private void SetDatesForCodeChecking(IList<HsEob.DateTimeReference> dateTimeReferences, FileData fileData)
        {
            if (dateTimeReferences == null)
            {
                return;
            }

            foreach (HsEob.DateTimeReference dtr in dateTimeReferences)
            {
                switch (dtr.Dtm01DateTimeQualifier)
                {
                    case EobDateTimeReference.ServiceStartDate:
                        fileData.StartDate = dtr.Dtm02Date;
                        break;
                    case EobDateTimeReference.ServiceEndDate:
                        fileData.StopDate = dtr.Dtm02Date;
                        break;
                    case EobDateTimeReference.ClaimStartDate:
                        fileData.StartDate = dtr.Dtm02Date;
                        break;
                    case EobDateTimeReference.ClaimEndDate:
                        fileData.StopDate = dtr.Dtm02Date;
                        break;
                    case EobDateTimeReference.ServiceDay:
                        fileData.StartDate = dtr.Dtm02Date;
                        fileData.StopDate = dtr.Dtm02Date;
                        break;
                }
            }
        }

        private void ValidateMoaMiaRemarkCodes(HsEob.ClaimPaymentInformation claimPaymentInformation, FileData fileData)
        {
            //MIA05, MIA20, MIA21, MIA22, MIA23
            if (claimPaymentInformation.MiaInpatientInformation != null)
            {
                this.ValidateRemittanceAdviceRemarkCode(claimPaymentInformation.MiaInpatientInformation.Mia05ReferenceIdentifier, fileData);
                this.ValidateRemittanceAdviceRemarkCode(claimPaymentInformation.MiaInpatientInformation.Mia20ReferenceIdentifier, fileData);
                this.ValidateRemittanceAdviceRemarkCode(claimPaymentInformation.MiaInpatientInformation.Mia21ReferenceIdentifier, fileData);
                this.ValidateRemittanceAdviceRemarkCode(claimPaymentInformation.MiaInpatientInformation.Mia22ReferenceIdentifier, fileData);
                this.ValidateRemittanceAdviceRemarkCode(claimPaymentInformation.MiaInpatientInformation.Mia23ReferenceIdentifier, fileData);
            }

            //MOA03, MOA04, MOA05, MOA06, MOA07
            if (claimPaymentInformation.MoaOutpatientInformation != null)
            {
                this.ValidateRemittanceAdviceRemarkCode(claimPaymentInformation.MoaOutpatientInformation.Moa03ReferenceIdentifier, fileData);
                this.ValidateRemittanceAdviceRemarkCode(claimPaymentInformation.MoaOutpatientInformation.Moa04ReferenceIdentifier, fileData);
                this.ValidateRemittanceAdviceRemarkCode(claimPaymentInformation.MoaOutpatientInformation.Moa05ReferenceIdentifier, fileData);
                this.ValidateRemittanceAdviceRemarkCode(claimPaymentInformation.MoaOutpatientInformation.Moa06ReferenceIdentifier, fileData);
                this.ValidateRemittanceAdviceRemarkCode(claimPaymentInformation.MoaOutpatientInformation.Moa07ReferenceIdentifier, fileData);
            }
        }

        private void ValidateLqRemarkCodes(HsEob.ServicePaymentInformation servicePaymentInformation, FileData fileData)
        {
            if (servicePaymentInformation.LqHealthCareRemarkCodes == null)
            {
                return;
            }
            foreach (HsEob.IndustryCodeIdentification lq in servicePaymentInformation.LqHealthCareRemarkCodes)
            {
                this.ValidateRemittanceAdviceRemarkCode(lq.Lq02IndustryCode, fileData);
            }
        }

        private void ValidateRemittanceAdviceRemarkCode(string remarkCode, FileData fileData)
        {
            if (remarkCode == null || fileData.StartDate == DateTime.MinValue)
            {
                return;
            }

            HsEob.EobRemittanceAdviceRemarkCode remittanceAdviceRemarkCode = this.RemittanceAdviceRemarkCodes.FirstOrDefault(x => x.RemittanceAdviceRemarkCode == remarkCode);

            if (remittanceAdviceRemarkCode == null ||
                remittanceAdviceRemarkCode.StartDate > fileData.StartDate ||
                remittanceAdviceRemarkCode.EndDate != DateTime.MinValue &&
                remittanceAdviceRemarkCode.EndDate <= fileData.StopDate)
            {
                this._metricsProvider.Value.Increment(InactiveRarcMetric);
                lock (Lock)
                {
                    this._systemExceptionService.Value.LogSystemException(new SystemException
                    {
                        SystemExceptionDescription = $"Remittance Advice Remark code {remarkCode} either doesn't exist or is not valid for the dates {fileData.StartDate:d} to {fileData.StopDate:d} in the file {fileData.FileName}",
                        SystemExceptionType = SystemExceptionTypeEnum.EobInactiveRarc
                    });
                }
            }
        }


        /// <summary>
        /// We need to take the CAS segments (CAS*CO*1*11.11*10*2*22.22*11*3*33.33*12*4*44.44*13*5*55.55*14*6*66.66*15~)
        /// and break them down into it individual rows. 
        /// CO*1*1.11*10
        /// CO*2*2.22*11 etc..
        /// We also validate each of the Codes (CAS02, CAS05, CAS08, CAS11, CAS14 and CAS17)
        /// </summary>
        private List<HsEob.ClaimAdjustmentPivot> PivotCasSegments(IList<HsEob.ClaimAdjustment> claimAdjustments, FileData fileData)
        {
            List<HsEob.ClaimAdjustmentPivot> caps = new List<HsEob.ClaimAdjustmentPivot>();
            if (claimAdjustments == null)
            {
                return caps;
            }

            foreach (HsEob.ClaimAdjustment ca in claimAdjustments)
            {
                if (ca.Cas02ClaimAdjustmentReasonCode == null)
                {
                    continue;
                }
                caps.Add(this.CreateNewClaimAdjustmentPivot(ca.Cas01ClaimAdjustmentGroupCode, ca.Cas02ClaimAdjustmentReasonCode, ca.Cas03MonetaryAmount, ca.Cas04Quantity, fileData));

                if (ca.Cas05ClaimAdjustmentReasonCode == null)
                {
                    continue;
                }
                caps.Add(this.CreateNewClaimAdjustmentPivot(ca.Cas01ClaimAdjustmentGroupCode, ca.Cas05ClaimAdjustmentReasonCode, ca.Cas06MonetaryAmount, ca.Cas07Quantity, fileData));

                if (ca.Cas08ClaimAdjustmentReasonCode == null)
                {
                    continue;
                }
                caps.Add(this.CreateNewClaimAdjustmentPivot(ca.Cas01ClaimAdjustmentGroupCode, ca.Cas08ClaimAdjustmentReasonCode, ca.Cas09MonetaryAmount, ca.Cas10Quantity, fileData));

                if (ca.Cas11ClaimAdjustmentReasonCode == null)
                {
                    continue;
                }
                caps.Add(this.CreateNewClaimAdjustmentPivot(ca.Cas01ClaimAdjustmentGroupCode, ca.Cas11ClaimAdjustmentReasonCode, ca.Cas12MonetaryAmount, ca.Cas13Quantity, fileData));

                if (ca.Cas14ClaimAdjustmentReasonCode == null)
                {
                    continue;
                }
                caps.Add(this.CreateNewClaimAdjustmentPivot(ca.Cas01ClaimAdjustmentGroupCode, ca.Cas14ClaimAdjustmentReasonCode, ca.Cas15MonetaryAmount, ca.Cas16Quantity, fileData));

                if (ca.Cas17ClaimAdjustmentReasonCode == null)
                {
                    continue;
                }
                caps.Add(this.CreateNewClaimAdjustmentPivot(ca.Cas01ClaimAdjustmentGroupCode, ca.Cas17ClaimAdjustmentReasonCode, ca.Cas18MonetaryAmount, ca.Cas19Quantity, fileData));
            }
            return caps;
        }

        // If reasonCode isn't null, validate it and create a new ClaimAdjustmentPivot
        private HsEob.ClaimAdjustmentPivot CreateNewClaimAdjustmentPivot(string groupCode, string reasonCode, decimal amount, decimal quantity, FileData fileData)
        {
            this.ValidateClaimAdjustmentReasonCode(reasonCode, fileData);

            return new HsEob.ClaimAdjustmentPivot
            {
                ClaimAdjustmentGroupCode = groupCode,
                ClaimAdjustmentReasonCode = reasonCode,
                MonetaryAmount = amount,
                Quantity = quantity,
                // must populate the property EobClaimAdjustmentReasonCode for serialization
                EobClaimAdjustmentReasonCode = this.ClaimAdjustmentReasonCodes.FirstOrDefault(x => x.ClaimAdjustmentReasonCode == reasonCode)
            };
        }

        // Validate that the Remit Code exists and is not deactivated.
        // This will be called to look at CAS02, CAS05, CAS08, CAS11, CAS14 and CAS17
        private void ValidateClaimAdjustmentReasonCode(string reasonCode, FileData fileData)
        {
            if (reasonCode == null || fileData.StartDate == DateTime.MinValue)
            {
                return;
            }

            HsEob.EobClaimAdjustmentReasonCode claimAdjustmentReasonCode = this.ClaimAdjustmentReasonCodes.FirstOrDefault(x => x.ClaimAdjustmentReasonCode == reasonCode);

            if (claimAdjustmentReasonCode == null ||
                claimAdjustmentReasonCode.StartDate > fileData.StartDate ||
                claimAdjustmentReasonCode.EndDate != DateTime.MinValue &&
                claimAdjustmentReasonCode.EndDate <= fileData.StopDate)
            {
                this._metricsProvider.Value.Increment(InactiveCarcMetric);
                lock (Lock)
                {
                    this._systemExceptionService.Value.LogSystemException(new SystemException
                    {
                        SystemExceptionDescription = $"Claim Adjustment Reason code {reasonCode} either doesn't exist or is not valid for the dates {fileData.StartDate:d} to {fileData.StopDate:d} in the file {fileData.FileName}",
                        SystemExceptionType = SystemExceptionTypeEnum.EobInactiveCarc
                    });
                }
            }
        }

        private IEdiGrammar GetGrammar(char delimiter)
        {
            IEdiGrammar grammar = EdiGrammar.NewX12();

            grammar.SetAdvice(
                segmentNameDelimiter: delimiter,
                dataElementSeparator: delimiter,

                // Change this to the value of ISA16 if we want to deal with the composite data elements.
                // Right now I put the whole value in a string variable instead of breaking it apart
                // Check out the SVC01 data element for an example (SVC*HC:87081*34*0*0306*1~)
                componentDataElementSeparator: '`',

                segmentTerminator: '~',
                releaseCharacter: null,
                reserved: null,
                decimalMark: '.');

            return grammar;
        }

        private bool VerifyItsAn835File(StreamReader stream, FileData fileData)
        {
            string readLine = stream.ReadLine();
            if (readLine != null)
            {
                // if everything is on one line.
                try
                {
                    fileData.Delimiter = Convert.ToChar(readLine.Substring(3, 1));
                    return readLine.Substring(100, 200).Contains($"ST{fileData.Delimiter}835{fileData.Delimiter}");
                }
                catch (Exception)
                {
                    // if the segments are on separate lines
                    // loop through to get the third line with actual text.
                    // some companies are helpful with single space lines, 
                    // others like to double space. some may like to triple space? I have no idea.
                    try
                    {
                        int lineCount = 1;
                        while (lineCount < 3)
                        {
                            readLine = stream.ReadLine();
                            if (!readLine.Equals(""))
                            {
                                lineCount++;
                            }
                        }
                        return readLine.Substring(3, 3).Equals("835");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                }
            }
            return false;
        }

        private void LogException(FileTracker fileTracker, bool failLoadTracker, string message, string fileName)
        {
            lock (Lock)
            {
                fileTracker.FileStatus = this.FileStatuses.FirstOrDefault(x => x.FileStatusId == (int)FileStatusEnum.Failed);
                this._fileTrackerRepository.Value.InsertOrUpdate(fileTracker);
                if (failLoadTracker)
                {
                    this.FailLoadTracker = true;
                    this._metricsProvider.Value.Increment(ErrorProcessing835);
                    this._systemExceptionService.Value.LogSystemException(new SystemException
                    {
                        SystemExceptionDescription = $"EOB: Error processing EOB 835 file {fileName}. {message}",
                        SystemExceptionType = SystemExceptionTypeEnum.EobErrorProcessing835
                    });

                }
                else
                {
                    this._systemExceptionService.Value.LogSystemException(new SystemException
                    {
                        SystemExceptionDescription = $"EOB: {message} {fileName}.",
                        SystemExceptionType = SystemExceptionTypeEnum.EobErrorProcessing835
                    });
                }
            }
        }


        public void MigrateHsEobToApplicationEob()
        {
            this.InitLookupCollections();
            this.SyncEobLookupTables();

            List<int> displayedInterchangeIds = this._eob835Repository.Value.GetUnmigratedDisplayedEobInterchangeIds().ToList();

            foreach (int interchangeId in displayedInterchangeIds)
            {
                this.Bus.Value.PublishMessage(new EobInterchangeMigrationMessage()
                {
                    InterchangeControlHeaderTrailerId = interchangeId
                }).Wait();
            }

            List<int> interchangeIds = this._eob835Repository.Value.GetStatelessAll()
                .Where(x => x.MigrationDate == null)
                .Select(x => x.InterchangeControlHeaderTrailerId)
                .ToList();

            IEnumerable<int> remainingInterchangeIds = interchangeIds.Except(displayedInterchangeIds);

            foreach (int interchangeId in remainingInterchangeIds)
            {
                this.Bus.Value.PublishMessage(new EobInterchangeMigrationMessage()
                {
                    InterchangeControlHeaderTrailerId = interchangeId
                }).Wait();
            }
        }

        public void MigrateInterchange(int interchangeId)
        {
            HsEob.InterchangeControlHeaderTrailer interchange = this._eob835Repository.Value.GetStatelessById(interchangeId);
            List<Tuple<HsEob.InterchangeControlHeaderTrailer, FileTracker>> migrationList = new List<Tuple<HsEob.InterchangeControlHeaderTrailer, FileTracker>>();
            FileTracker fileTracker = this._fileTrackerRepository.Value.GetById(interchange.FileTrackerId ?? 0);
            migrationList.Add(new Tuple<HsEob.InterchangeControlHeaderTrailer, FileTracker>(interchange, fileTracker));
            List<AppEob.ClaimPaymentInformationHeader> claimPaymentInformationHeaderList = this.ExtractClaimPaymentInformation(migrationList);
            foreach (AppEob.ClaimPaymentInformationHeader cpi in claimPaymentInformationHeaderList)
            {
                cpi.BillingSystemId = cpi.ClaimPaymentInformation.BillingSystemId;
                cpi.VisitSourceSystemKey = cpi.ClaimPaymentInformation.VisitSourceSystemKey;
                if (string.IsNullOrEmpty(cpi.TransactionSetControlNumber))
                {
                    cpi.TransactionSetControlNumber = "1";
                }
            }
            this.SendClaimPaymentInformationNotifications(claimPaymentInformationHeaderList);
            this._eob835Repository.Value.UpdateMigrationDate(interchange);
            claimPaymentInformationHeaderList = null;
            interchange = null;
            fileTracker = null;
            migrationList = null;
        }

    }
}

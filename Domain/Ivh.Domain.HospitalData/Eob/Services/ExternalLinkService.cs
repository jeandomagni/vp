﻿namespace Ivh.Domain.HospitalData.Eob.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Entities;
    using Interfaces;

    public class ExternalLinkService : IExternalLinkService
    {
        private readonly Lazy<IExternalLinkRepository> _externalLinkRepository;

        public ExternalLinkService(Lazy<IExternalLinkRepository> externalLinkRepository)
        {
            this._externalLinkRepository = externalLinkRepository;
        }

        public IList<ExternalLink> GetAllExternalLinks()
        {
            return this._externalLinkRepository.Value.GetQueryable().ToList();
        }

        public ExternalLink GetExternalLink(int id)
        {
            return this._externalLinkRepository.Value.GetById(id);
        }
    }
}

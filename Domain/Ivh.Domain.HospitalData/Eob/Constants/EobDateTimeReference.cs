﻿namespace Ivh.Domain.HospitalData.Eob.Constants
{
    public class EobDateTimeReference
    {
        public const string ServiceStartDate = "150";
        public const string ServiceEndDate = "151";
        public const string ServiceDay = "472";
        public const string ClaimStartDate = "232";
        public const string ClaimEndDate = "233";
    }
}
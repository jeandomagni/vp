﻿namespace Ivh.Domain.HospitalData.Eob.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Common.ServiceBus.Interfaces;
    using Common.VisitPay.Messages.Eob;
    using HsEob = Entities;
    using AppEob = Domain.Eob.Entities;

    public interface IEob835Service : IDomainService
    {
        void Load835File();
        List<AppEob.ClaimPaymentInformationHeader> ExtractClaimPaymentInformation(HsEob.InterchangeControlHeaderTrailer interchange);
        void SendClaimPaymentInformationNotifications(List<AppEob.ClaimPaymentInformationHeader> claimPaymentInformationHeaderList);
        void MigrateHsEobToApplicationEob();
        void MigrateInterchange(int interchangeId);
    }
}
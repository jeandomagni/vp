﻿namespace Ivh.Domain.HospitalData.Eob.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IEobRemittanceAdviceRemarkCodeRepository : IRepository<EobRemittanceAdviceRemarkCode>
    {
        EobRemittanceAdviceRemarkCode GetEobRemittanceAdviceRemarkCode(string remarkCode);
    }
}
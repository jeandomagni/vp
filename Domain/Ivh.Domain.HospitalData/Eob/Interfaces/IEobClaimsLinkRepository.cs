﻿namespace Ivh.Domain.HospitalData.Eob.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IEobClaimsLinkRepository : IRepository<EobClaimsLink>
    {
        EobClaimsLink GetClaimsLink(string claimNumber);
        List<EobClaimsLink> GetClaimsLinks(List<string> claimNumbers);
    }
}
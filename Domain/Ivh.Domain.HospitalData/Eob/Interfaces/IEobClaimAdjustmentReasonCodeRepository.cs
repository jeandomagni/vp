﻿namespace Ivh.Domain.HospitalData.Eob.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IEobClaimAdjustmentReasonCodeRepository : IRepository<EobClaimAdjustmentReasonCode>
    {
        EobClaimAdjustmentReasonCode GetEobClaimAdjustmentReasonCode(string remitCode);
    }
}
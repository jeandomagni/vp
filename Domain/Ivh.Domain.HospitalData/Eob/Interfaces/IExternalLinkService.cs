﻿namespace Ivh.Domain.HospitalData.Eob.Interfaces
{
    using System.Collections.Generic;
    using Entities;

    public interface IExternalLinkService
    {
        IList<ExternalLink> GetAllExternalLinks();
        ExternalLink GetExternalLink(int id);
    }
}

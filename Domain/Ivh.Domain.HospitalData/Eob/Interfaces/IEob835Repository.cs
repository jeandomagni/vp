﻿namespace Ivh.Domain.HospitalData.Eob.Interfaces
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Interfaces;
    using Entities;

    public interface IEob835Repository : IRepository<InterchangeControlHeaderTrailer>
    {
        InterchangeControlHeaderTrailer GetStatelessById(int id);
        IQueryable<InterchangeControlHeaderTrailer> GetStatelessAll();
        void UpdateMigrationDate(InterchangeControlHeaderTrailer entity);
        IList<int> GetUnmigratedDisplayedEobInterchangeIds();
    }
}
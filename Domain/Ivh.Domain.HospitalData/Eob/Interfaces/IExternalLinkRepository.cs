﻿namespace Ivh.Domain.HospitalData.Eob.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IExternalLinkRepository : IRepository<ExternalLink>
    {

    }
}

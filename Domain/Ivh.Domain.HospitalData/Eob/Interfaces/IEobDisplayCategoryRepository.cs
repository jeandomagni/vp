﻿namespace Ivh.Domain.HospitalData.Eob.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IEobDisplayCategoryRepository : IRepository<EobDisplayCategory>
    {
    }
}

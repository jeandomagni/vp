﻿namespace Ivh.Domain.HospitalData.Eob.Entities
{
    using Ivh.Domain.HospitalData.Visit.Entities;

    public class EobPayerFilterExternalLink
    {
        public virtual int EobPayerFilterExternalLinkId { get; set; }
        public virtual EobPayerFilter EobPayerFilter { get; set; }
        public virtual ExternalLink ExternalLink { get; set; }
        public virtual int? InsurancePlanId { get; set; }
        public virtual InsurancePlan InsurancePlan { get; set; }
    }
}
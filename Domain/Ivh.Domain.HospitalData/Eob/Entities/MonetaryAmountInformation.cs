﻿namespace Ivh.Domain.HospitalData.Eob.Entities
{
    using Common.Base.Attributes;
    using indice.Edi.Serialization;

    [EdiSegment, EdiPath("AMT")]
    [MaxStringLengthClass]
    public class MonetaryAmountInformation
    {
        public virtual int MonetaryAmountInformationId { get; set; }

        [EdiValue("X(3)", Path = "AMT/0", Description = "AMT01 - Amount Qualifier Code")]
        [MaxStringLength(3)]
        public virtual string Amt01AmountQualifierCode { get; set; }

        [EdiValue("9(16)V9(2)", Path = "AMT/1", Description = "AMT02 - Monetary Amount")]
        public virtual decimal Amt02MonetaryAmount { get; set; }
    }
}
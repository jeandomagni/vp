﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.HospitalData.Eob.Entities
{
    public class EobIndicatorResult
    {
        public virtual int BillingSystemId { get; set; }
        public virtual string VisitSourceSystemKey { get; set; }
        public virtual bool HasEob { get; set; }
        public IList<EobPayerFilterExternalLink> EobPayerFilterEobExternalLinks { get; set; }
        public virtual string LogoUrl { get; set; }
        public virtual string PayerName { get; set; }
        public virtual DateTime? Bpr16Date { get; set; }
        public virtual int Lx01AssignedNumber { get; set; }
    }
}

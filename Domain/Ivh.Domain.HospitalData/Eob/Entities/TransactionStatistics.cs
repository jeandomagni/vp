﻿namespace Ivh.Domain.HospitalData.Eob.Entities
{
    using System;
    using Common.Base.Attributes;
    using indice.Edi.Serialization;

    [EdiSegment, EdiPath("TS3/0")]
    [MaxStringLengthClass]
    public class TransactionStatistics
    {
        public virtual int TransactionStatisticsId { get; set; }

        [EdiValue("X(50)", Path = "TS3/0", Description = "TS301 - Reference Identification")]
        [MaxStringLength(50)]
        public virtual string Ts301ReferenceIdentification { get; set; }

        [EdiValue("X(2)", Path = "TS3/1", Description = "TS302 - Facility Code")]
        [MaxStringLength(2)]
        public virtual string Ts302FacilityCode { get; set; }

        [EdiValue("9(8)", Path = "TS3/2", Format = "yyyyMMdd", Description = "TS303 - Date format = CCYYMMDD")]
        public virtual DateTime? Ts303Date { get; set; }

        [EdiValue("9(15)", Path = "TS3/3", Description = "TS304 - total number of claims")]
        public virtual int Ts304Quantity { get; set; }

        [EdiValue("9(16)V9(2)", Path = "TS3/4", Description = "TS305 - Total Reported Charges")]
        public virtual decimal Ts305MonetaryAmount { get; set; }

        [EdiValue("9(16)V9(2)", Path = "TS3/12", Description = "TS313 - Total Medicare Secondary Payer primary payer amount")]
        public virtual decimal Ts313MonetaryAmount { get; set; }

        [EdiValue("9(16)V9(2)", Path = "TS3/14", Description = "TS315 - Summary of non-lab charges")]
        public virtual decimal Ts315MonetaryAmount { get; set; }

        [EdiValue("9(16)V9(2)", Path = "TS3/16", Description = "TS317 - HCPCS reported Charges")]
        public virtual decimal Ts317MonetaryAmount { get; set; }

        [EdiValue("9(16)V9(2)", Path = "TS3/17", Description = "TS318 - HCPCS payable amount")]
        public virtual decimal Ts318MonetaryAmount { get; set; }

        [EdiValue("9(16)V9(2)", Path = "TS3/19", Description = "TS320 - Total Professional Component Amount")]
        public virtual decimal Ts320MonetaryAmount { get; set; }

        [EdiValue("9(16)V9(2)", Path = "TS3/20", Description = "TS321 - Medicare SEcondary Payer Patient Liability met")]
        public virtual decimal Ts321MonetaryAmount { get; set; }

        [EdiValue("9(16)V9(2)", Path = "TS3/21", Description = "TS322 - Total Patient Reimbursement")]
        public virtual decimal Ts322MonetaryAmount { get; set; }

        [EdiValue("9(15)", Path = "TS3/22", Description = "TS323 - Total Periodic interim payment number of claims")]
        public virtual int Ts323Quantity { get; set; }

        [EdiValue("9(16)V9(2)", Path = "TS3/23", Description = "TS324 - Total Periodic interim payment adjustment")]
        public virtual decimal Ts324MonetaryAmount { get; set; }


    }
}
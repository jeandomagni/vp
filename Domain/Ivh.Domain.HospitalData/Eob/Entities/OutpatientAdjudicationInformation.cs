﻿namespace Ivh.Domain.HospitalData.Eob.Entities
{
    using Common.Base.Attributes;
    using indice.Edi.Serialization;

    [EdiSegment, EdiPath("MOA/0")]
    [MaxStringLengthClass]
    public class OutpatientAdjudicationInformation
    {
        public virtual int OutpatientAdjudicationInformationId { get; set; }

        [EdiValue("9(10)", Path = "MOA/0", Description = "MOA01 - Reimbursement Rate")]
        public virtual decimal Moa01Percent { get; set; }

        [EdiValue("9(16)V9(2)", Path = "MOA/1", Description = "MOA02 - HCPCS Payable Amount")]
        public virtual decimal Moa02MonetaryAmount { get; set; }

        [EdiValue("X(50)", Path = "MOA/2", Description = "MOA03 - Claim Payment Remark Code")]
        [MaxStringLength(50)]
        public virtual string Moa03ReferenceIdentifier { get; set; }

        [EdiValue("X(50)", Path = "MOA/3", Description = "MOA04 - Claim Payment Remark Code")]
        [MaxStringLength(50)]
        public virtual string Moa04ReferenceIdentifier { get; set; }

        [EdiValue("X(50)", Path = "MOA/4", Description = "MOA05 - Claim Payment Remark Code")]
        [MaxStringLength(50)]
        public virtual string Moa05ReferenceIdentifier { get; set; }

        [EdiValue("X(50)", Path = "MOA/5", Description = "MOA06 - Claim Payment Remark Code")]
        [MaxStringLength(50)]
        public virtual string Moa06ReferenceIdentifier { get; set; }

        [EdiValue("X(50)", Path = "MOA/6", Description = "MOA07 - Claim Payment Remark Code")]
        [MaxStringLength(50)]
        public virtual string Moa07ReferenceIdentifier { get; set; }

        [EdiValue("9(16)V9(2)", Path = "MOA/7", Description = "MOA08 - End Stage Renal Deisease payment amount")]
        public virtual decimal Moa08MonetaryAmount { get; set; }

        [EdiValue("9(16)V9(2)", Path = "MOA/8", Description = "MOA09 - Professional component amount billed but not payable")]
        public virtual decimal Moa09MonetaryAmount { get; set; }

        public virtual EobRemittanceAdviceRemarkCode Moa03EobRemittanceAdviceRemarkCode { get; set; }
        
    }
}
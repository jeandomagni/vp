﻿namespace Ivh.Domain.HospitalData.Eob.Entities
{
    using Common.Base.Attributes;
    using indice.Edi.Serialization;

    [EdiSegment, EdiPath("REF")]
    [MaxStringLengthClass]
    public class ReferenceInformation
    {
        public virtual int ReferenceInformationId { get; set;  }

        [EdiValue("X(3)", Path = "REF/0", Description = "REF01 - Reference Identification Qualifier")]
        [MaxStringLength(3)]
        public virtual string Ref01ReferenceIdentificationQualifier { get; set; }

        [EdiValue("X(50)", Path = "REF/1", Description = "REF02 - Reference Identification")]
        [MaxStringLength(50)]
        public virtual string Ref02ReferenceIdentification { get; set; }
    }
}
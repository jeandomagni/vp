﻿namespace Ivh.Domain.HospitalData.Eob.Entities
{
    using Common.Base.Attributes;
    using indice.Edi.Serialization;

    [EdiSegment, EdiPath("LQ")]
    [MaxStringLengthClass]
    public class IndustryCodeIdentification
    {
        public virtual int IndustryCodeIdentificationId { get; set; }

        [EdiValue("X(3)", Path = "LQ/0", Description = "LQ01 - Code List Qualifier Code")]
        [MaxStringLength(3)]
        public virtual string Lq01CodeListQualifierCode { get; set; }

        [EdiValue("X(30)", Path = "LQ/1", Description = "LQ02 - Industry Code")]
        [MaxStringLength(30)]
        public virtual string Lq02IndustryCode { get; set; }

        // for the description of the Lq02IndustryCode
        public virtual EobRemittanceAdviceRemarkCode EobRemittanceAdviceRemarkCode { get; set; }

        // Back reference to its parent
        public virtual ServicePaymentInformation ServicePaymentInformation { get; set; }
    }
}
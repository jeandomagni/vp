﻿namespace Ivh.Domain.HospitalData.Eob.Entities
{
    using Common.Base.Attributes;
    using indice.Edi.Serialization;

    // This table is not mapped to the database.  
    // Use the ClaimAdjustmentPivot class instead.
    [EdiSegment, EdiPath("CAS/0")]
    [MaxStringLengthClass]
    public class ClaimAdjustment
    {
        [EdiValue("X(2)", Path = "CAS/0", Description = "CAS01 - Claim Adjustment Group Code")]
        [MaxStringLength(2)]
        public virtual string Cas01ClaimAdjustmentGroupCode { get; set; }

        [EdiValue("X(5)", Path = "CAS/1", Description = "CAS02 - Claim Adjustment Reason Code")]
        [MaxStringLength(5)]
        public virtual string Cas02ClaimAdjustmentReasonCode { get; set; }

        [EdiValue("9(16)V9(2)", Path = "CAS/2", Description = "CAS03 - Amount of adjustment")]
        public virtual decimal Cas03MonetaryAmount { get; set; }

        [EdiValue("9(15)V9(5)", Path = "CAS/3", Description = "CAS04 - Units of service being adjusted")]
        public virtual decimal Cas04Quantity { get; set; }

        [EdiValue("X(5)", Path = "CAS/4", Description = "CAS05 - Claim Adjustment Reason Code")]
        [MaxStringLength(5)]
        public virtual string Cas05ClaimAdjustmentReasonCode { get; set; }

        [EdiValue("9(16)V9(2)", Path = "CAS/5", Description = "CAS06 - Amount of adjustment")]
        public virtual decimal Cas06MonetaryAmount { get; set; }

        [EdiValue("9(15)V9(5)", Path = "CAS/6", Description = "CAS07 - Units of service being adjusted")]
        public virtual decimal Cas07Quantity { get; set; }

        [EdiValue("X(5)", Path = "CAS/7", Description = "CAS08 - Claim Adjustment Reason Code")]
        [MaxStringLength(5)]
        public virtual string Cas08ClaimAdjustmentReasonCode { get; set; }

        [EdiValue("9(16)V9(2)", Path = "CAS/8", Description = "CAS09 - Amount of adjustment")]
        public virtual decimal Cas09MonetaryAmount { get; set; }

        [EdiValue("9(15)V9(5)", Path = "CAS/9", Description = "CAS10 - Units of service being adjusted")]
        public virtual decimal Cas10Quantity { get; set; }

        [EdiValue("X(5)", Path = "CAS/10", Description = "CAS11 - Claim Adjustment Reason Code")]
        [MaxStringLength(5)]
        public virtual string Cas11ClaimAdjustmentReasonCode { get; set; }

        [EdiValue("9(16)V9(2)", Path = "CAS/11", Description = "CAS12 - Amount of adjustment")]
        public virtual decimal Cas12MonetaryAmount { get; set; }

        [EdiValue("9(15)V9(5)", Path = "CAS/12", Description = "CAS13 - Units of service being adjusted")]
        public virtual decimal Cas13Quantity { get; set; }

        [EdiValue("X(5)", Path = "CAS/13", Description = "CAS14 - Claim Adjustment Reason Code")]
        [MaxStringLength(5)]
        public virtual string Cas14ClaimAdjustmentReasonCode { get; set; }

        [EdiValue("9(16)V9(2)", Path = "CAS/14", Description = "CAS15 - Amount of adjustment")]
        public virtual decimal Cas15MonetaryAmount { get; set; }

        [EdiValue("9(15)V9(5)", Path = "CAS/15", Description = "CAS16 - Units of service being adjusted")]
        public virtual decimal Cas16Quantity { get; set; }

        [EdiValue("X(5)", Path = "CAS/16", Description = "CAS17 - Claim Adjustment Reason Code")]
        [MaxStringLength(5)]
        public virtual string Cas17ClaimAdjustmentReasonCode { get; set; }

        [EdiValue("9(16)V9(2)", Path = "CAS/17", Description = "CAS18 - Amount of adjustment")]
        public virtual decimal Cas18MonetaryAmount { get; set; }

        [EdiValue("9(15)V9(5)", Path = "CAS/18", Description = "CAS19 - Units of service being adjusted")]
        public virtual decimal Cas19Quantity { get; set; }
    }
}
﻿namespace Ivh.Domain.HospitalData.Eob.Entities
{
    public class ClaimAdjustmentPivot
    {
        // this class takes the CAS segment and puts each of the (up to six) amounts into its own row
        // that way we can use sum and group by.
        public virtual int ClaimAdjustmentId { get; set; }
        public virtual string ClaimAdjustmentGroupCode { get; set; }
        public virtual string ClaimAdjustmentReasonCode { get; set; }
        public virtual decimal MonetaryAmount { get; set; }
        public virtual decimal Quantity { get; set; }
        // to get the UIDisplay action
        public virtual EobClaimAdjustmentReasonCode EobClaimAdjustmentReasonCode { get; set; }

        // Back references to its parents
        public virtual ClaimPaymentInformation ClaimPaymentInformation { get; set; }
        public virtual ServicePaymentInformation ServicePaymentInformation { get; set; }
    }
}
﻿namespace Ivh.Domain.HospitalData.Eob.Entities
{
    using System.Collections.Generic;
    using Common.Base.Attributes;
    using indice.Edi.Serialization;

    [EdiSegment, EdiSegmentGroup("N1", SequenceEnd = "N1")]
    [MaxStringLengthClass]
    public class PayerIdentification
    {
        public virtual int PayerIdentificationId { get; set; }

        [EdiValue("X(3)", Path = "N1/0", Description = "N101 - Entity Identifier Code")]
        [MaxStringLength(3)]
        public virtual string N101EntityIdentifierCode { get; set; }

        [EdiValue("X(60)", Path = "N1/1", Description = "N102 - Name")]
        [MaxStringLength(60)]
        public virtual string N102PayerName { get; set; }

        [EdiValue("X(2)", Path = "N1/2", Description = "N103 - Identification Code Qualifier")]
        [MaxStringLength(2)]
        public virtual string N103IdentificationCodeQualifier { get; set; }

        [EdiValue("X(80)", Path = "N1/3", Description = "N104 - Identification Code")]
        [MaxStringLength(80)]
        public virtual string N104IdentificationCode { get; set; }

        [EdiValue("X(2)", Path = "N1/4", Description = "N105 - Entity Relationship Code")]
        [MaxStringLength(2)]
        public virtual string N105EntityRelationshipCode { get; set; }

        [EdiValue("X(3)", Path = "N1/5", Description = "N106 - Entity Identifier Code")]
        [MaxStringLength(3)]
        public virtual string N106EntityIdentifierCode { get; set; }

        //public virtual PartyLocation N3PayerAddress { get; set; }
        //public virtual PartyGeographicLocation N4PayerCityStateZip { get; set; }
        //public virtual IList<ReferenceInformation> RefAdditionalPayerIdentification { get; set; }

        [EdiCondition("CX", Path = "PER/0/0")]
        public virtual PersonInformation Per1PayerBusinessContactInformation { get; set; }

        [EdiCondition("BL", Path = "PER/0/0")]
        public virtual IList<PersonInformation> Per2PayerTechnicalContactInformation { get; set; }

        [EdiCondition("IC", Path = "PER/0/0")]
        public virtual PersonInformation Per3PayerWebSite { get; set; }
    }
}
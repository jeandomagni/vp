﻿namespace Ivh.Domain.HospitalData.Eob.Entities
{
    using Common.Base.Attributes;
    using indice.Edi.Serialization;

    [EdiSegment, EdiPath("PER")]
    [MaxStringLengthClass]
    public class PersonInformation
    {
        public virtual int PersonInformationId { get; set; }

        [EdiValue("X(2)", Path = "PER/0", Description = "PER01 - Contact Function Code")]
        [MaxStringLength(2)]
        public virtual string Per01ContactFunctionCode { get; set; }

        [EdiValue("X(60)", Path = "PER/1", Description = "PER02 - Name")]
        [MaxStringLength(60)]
        public virtual string Per02PersonName { get; set; }

        [EdiValue("X(2)", Path = "PER/2", Description = "PER03 - Code identifying the type of communication number")]
        [MaxStringLength(2)]
        public virtual string Per03CommunicationNumberQualifer { get; set; }

        [EdiValue(Path = "PER/3", Description = "PER04 - Complete communications number")]
        public virtual string Per04CommunicationNumber { get; set; }

        [EdiValue("X(2)", Path = "PER/4", Description = "PER05 - Code identifying the type of communication number")]
        [MaxStringLength(2)]
        public virtual string Per05CommunicationNumberQualifer { get; set; }

        [EdiValue(Path = "PER/5", Description = "PER06 - Complete communications number")]
        public virtual string Per06CommunicationNumber { get; set; }

        [EdiValue("X(2)", Path = "PER/6", Description = "PER07 - Code identifying the type of communication number")]
        [MaxStringLength(2)]
        public virtual string Per07CommunicationNumberQualifer { get; set; }

        [EdiValue(Path = "PER/7", Description = "PER08 - Complete communications number")]
        public virtual string Per08CommunicationNumber { get; set; }

        [EdiValue("X(20)", Path = "PER/8", Description = "PER09 - Contact Inquiry Reference")]
        [MaxStringLength(20)]
        public virtual string Per09ContactInquiryReference { get; set; }

    }
}
﻿namespace Ivh.Domain.HospitalData.Eob.Entities
{
    using Common.Base.Attributes;
    using indice.Edi.Serialization;

    [EdiSegment, EdiPath("NM1/0")]
    [MaxStringLengthClass]
    public class IndividualOrOrganizationalName
    {
        public virtual int IndividualOrOrganizationalNameId { get; set; }

        [EdiValue("X(3)", Path = "NM1/0", Description = "NM101 - Entity Identifier Code")]
        [MaxStringLength(3)]
        public virtual string Nm101EntityIdentifierCode { get; set; }

        [EdiValue("X(1)", Path = "NM1/1", Description = "NM102 - Entity Type Qualifier")]
        [MaxStringLength(1)]
        public virtual string Nm102EntityTypeQualifier { get; set; }

        [EdiValue("X(60)", Path = "NM1/2", Description = "NM103 - Last Name or Organization Name")]
        [MaxStringLength(60)]
        public virtual string Nm103NameLastOrganizationName { get; set; }

        [EdiValue("X(35)", Path = "NM1/3", Description = "NM104 - First Name")]
        [MaxStringLength(35)]
        public virtual string Nm104NameFirst { get; set; }

        [EdiValue("X(25)", Path = "NM1/4", Description = "NM105 - MiddleName")]
        [MaxStringLength(25)]
        public virtual string Nm105NameMiddle { get; set; }

        [EdiValue("X(10)", Path = "NM1/6", Description = "NM107 - Suffix")]
        [MaxStringLength(10)]
        public virtual string Nm107NameSuffix { get; set; }

        [EdiValue("X(2)", Path = "NM1/7", Description = "NM108 - Identification Code Qualifier")]
        [MaxStringLength(2)]
        public virtual string Nm108IdentificationCodeQualifier { get; set; }

        [EdiValue("X(80)", Path = "NM1/8", Description = "NM109 - Identification Code")]
        [MaxStringLength(80)]
        public virtual string Nm109IdentificationCode { get; set; }
    }
}
﻿namespace Ivh.Domain.HospitalData.Eob.Entities
{
    using System.Collections.Generic;
    using System.Linq;

    public class ExternalLink
    {
        public virtual int ExternalLinkId { get; set; }
        public virtual string Url { get; set; }
        public virtual string Text { get; set; }
        public virtual string Icon { get; set; }
        public virtual int PersistDays { get; set; }
        public virtual string ClaimNumberLocation { get; set; }
    }
}
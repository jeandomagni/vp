﻿namespace Ivh.Domain.HospitalData.Eob.Entities
{
    using indice.Edi.Serialization;

    [EdiSegment, EdiPath("TS2/0")]
    public class TransactionSupplementalStatistics
    {
        public virtual int TransactionSupplementalStatisticsId { get; set; }

        [EdiValue("9(16)V9(2)", Path = "TS2/0", Description = "TS201 - Total diagnosis related group amount")]
        public virtual decimal Ts201MonetaryAmount { get; set; }

        [EdiValue("9(16)V9(2)", Path = "TS2/1", Description = "TS202 - Total federal specific amount")]
        public virtual decimal Ts202MonetaryAmount { get; set; }

        [EdiValue("9(16)V9(2)", Path = "TS2/2", Description = "TS203 - Total hospital specificamount")]
        public virtual decimal Ts203MonetaryAmount { get; set; }

        [EdiValue("9(16)V9(2)", Path = "TS2/3", Description = "TS204 - Disproportionate share amount")]
        public virtual decimal Ts204MonetaryAmount { get; set; }

        [EdiValue("9(16)V9(2)", Path = "TS2/4", Description = "TS205 - Total capital amount")]
        public virtual decimal Ts205MonetaryAmount { get; set; }

        [EdiValue("9(16)V9(2)", Path = "TS2/5", Description = "TS206 - Total indirect medical eduation amount")]
        public virtual decimal Ts206MonetaryAmount { get; set; }

        [EdiValue("9(15)", Path = "TS2/6", Description = "TS207 - Total number of outlier days")]
        public virtual int Ts207Quantity { get; set; }

        [EdiValue("9(16)V9(2)", Path = "TS2/7", Description = "TS208 - Total day outlier amount")]
        public virtual decimal Ts208MonetaryAmount { get; set; }

        [EdiValue("9(16)V9(2)", Path = "TS2/8", Description = "TS209 - Total cost outlier amount")]
        public virtual decimal Ts209MonetaryAmount { get; set; }

        [EdiValue("9(15)", Path = "TS2/9", Description = "TS210 - DRG average day lenght of stay")]
        public virtual int Ts210Quantity { get; set; }

        [EdiValue("9(15)", Path = "TS2/10", Description = "TS211 - Total number of discharges")]
        public virtual int Ts211Quantity { get; set; }

        [EdiValue("9(15)", Path = "TS2/11", Description = "TS212 - Total number of cost report days")]
        public virtual int Ts212Quantity { get; set; }

        [EdiValue("9(15)", Path = "TS2/12", Description = "TS213 - Total number of covered days")]
        public virtual int Ts213Quantity { get; set; }

        [EdiValue("9(15)", Path = "TS2/13", Description = "TS214 - Total number of non-covered days")]
        public virtual int Ts214Quantity { get; set; }

        [EdiValue("9(16)V9(2)", Path = "TS2/14", Description = "TS215 - Total MSP pass-though amount calculated for a non-Medicare payer")]
        public virtual decimal Ts215MonetaryAmount { get; set; }

        [EdiValue("9(15)", Path = "TS2/15", Description = "TS216 - Average DRG weight")]
        public virtual decimal Ts216Quantity { get; set; }

        [EdiValue("9(16)V9(2)", Path = "TS2/16", Description = "TS217 - Total PPS capital, federal-specific portion, DRG amount")]
        public virtual decimal Ts217MonetaryAmount { get; set; }

        [EdiValue("9(16)V9(2)", Path = "TS2/17", Description = "TS218 - Total PPS capital, hospital-specific portion, DRG amount")]
        public virtual decimal Ts218MonetaryAmount { get; set; }

        [EdiValue("9(16)V9(2)", Path = "TS2/18", Description = "TS219 - Total PPS disproportionate share, hospital DRG amount")]
        public virtual decimal Ts219MonetaryAmount { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.HospitalData.Eob.Entities
{
    public class EobClaimAdjustmentReasonCodeDisplayMap
    {
        public virtual int EobClaimAdjustmentReasonCodeDisplayMapId { get; set; }
        public virtual string ClaimAdjustmentReasonCode { get; set; }
        public virtual EobDisplayCategory EobDisplayCategory { get; set; }
        /// <summary>
        /// N102 when N101 value is PR
        /// </summary>
        public virtual string UniquePayerValue { get; set; }
        /// <summary>
        /// CAS01 in 2100 Loop
        /// </summary>
        public virtual string ClaimAdjustmentGroupCode { get; set; }
        /// <summary>
        /// CLP02
        /// </summary>
        public virtual string ClaimStatusCode { get; set; }
    }
}

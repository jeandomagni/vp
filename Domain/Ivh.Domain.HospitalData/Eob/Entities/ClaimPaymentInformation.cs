﻿namespace Ivh.Domain.HospitalData.Eob.Entities
{
    using System.Collections.Generic;
    using Common.Base.Attributes;
    using indice.Edi.Serialization;

    [EdiSegment, EdiSegmentGroup("CLP")]
    [EdiPath("CLP")]
    [MaxStringLengthClass]
    public class ClaimPaymentInformation
    {
        public virtual int ClaimPaymentInformationId { get; set; }
        public virtual string VisitSourceSystemKey { get; set; }
        public virtual int BillingSystemId { get; set; }
        public virtual int HeaderNumberId { get; set; }

        [EdiValue("X(38)", Path = "CLP/0", Description = "CLP01 - Claim Submitters Identifier")]
        [MaxStringLength(38)]
        public virtual string Clp01ClaimSubmittersIdentifier { get; set; }

        [EdiValue("X(2)", Path = "CLP/1", Description = "CLP02 - Claim Status Code")]
        [MaxStringLength(2)]
        public virtual string Clp02ClaimStatusCode { get; set; }

        [EdiValue("9(16)V9(2)", Path = "CLP/2", Description = "CLP03 - Amount of submitted charges this claim")]
        public virtual decimal Clp03MonetaryAmount { get; set; }

        [EdiValue("9(16)V9(2)", Path = "CLP/3", Description = "CLP04 - Amount paid this claim")]
        public virtual decimal Clp04MonetaryAmount { get; set; }

        [EdiValue("9(16)V9(2)", Path = "CLP/4", Description = "CLP05 - Patient Responsibility Amount")]
        public virtual decimal Clp05MonetaryAmount { get; set; }

        [EdiValue("X(2)", Path = "CLP/5", Description = "CLP06 - Claim Filing Indicator Code")]
        [MaxStringLength(2)]
        public virtual string Clp06ClaimFileIndicatorCode { get; set; }

        [EdiValue("X(50)", Path = "CLP/6", Description = "CLP07 - Reference Identifier")]
        [MaxStringLength(50)]
        public virtual string Clp07ReferenceIdentifier { get; set; }

        [EdiValue("X(2)", Path = "CLP/7", Description = "CLP08 - Facility Code")]
        [MaxStringLength(2)]
        public virtual string Clp08FacilityCode { get; set; }

        [EdiValue("X(1)", Path = "CLP/8", Description = "CLP09 - Claim Frequency Type Code")]
        [MaxStringLength(1)]
        public virtual string Clp09ClaimFrequencyTypeCode { get; set; }

        [EdiValue("X(4)", Path = "CLP/10", Description = "CLP11 - Diagnosis Related Group Code")]
        [MaxStringLength(4)]
        public virtual string Clp11DiagnosisRelatedGroupCode { get; set; }

        [EdiValue("9(15)", Path = "CLP/11", Description = "CLP12 - DRG weight")]
        public virtual decimal Clp12Quantity { get; set; }

        [EdiValue("9(10)V9(5)", Path = "CLP/12", Description = "CLP13 - Discharge fraction")]
        public virtual decimal Clp13Percent { get; set; }

        // CAS
        public virtual IList<ClaimAdjustment> CasClaimAdjustments { get; set; }  // to parse the actual file
        public virtual IList<ClaimAdjustmentPivot> ClaimAdjustmentPivots { get; set; } // to normalize data to make sum and group by useful

        // NM1
        [EdiCondition("QC", Path = "NM1/0/0")]
        public virtual IndividualOrOrganizationalName Nm11PatientName { get; set; }

        // NM1
        //[EdiCondition("IL", Path = "NM1/0/0")]
        //public virtual IndividualOrOrganizationalName Nm12InsuredName { get; set; }

        // NM1
        //[EdiCondition("74", Path = "NM1/0/0")]
        //public virtual IndividualOrOrganizationalName Nm13CorrectedPatientInsuredName { get; set; }

        // NM1
        //[EdiCondition("82", Path = "NM1/0/0")]
        //public virtual IndividualOrOrganizationalName Nm14ServiceProviderNameName { get; set; }

        // NM1
        //[EdiCondition("TT", Path = "NM1/0/0")]
        //public virtual IndividualOrOrganizationalName Nm15CrossoverCarrierName { get; set; }

        // NM1
        //[EdiCondition("PR", Path = "NM1/0/0")]
        //public virtual IndividualOrOrganizationalName Nm16CorrectedPriorityPayerName { get; set; }

        // NM1
        //[EdiCondition("GB", Path = "NM1/0/0")]
        //public virtual IndividualOrOrganizationalName Nm17OtherSubscriberName { get; set; }

        // MIA
        public virtual InpatientAdjudicationInformation MiaInpatientInformation { get; set; }
        
        // MOA
        public virtual OutpatientAdjudicationInformation MoaOutpatientInformation { get; set; }

        // REF
        //[EdiCondition("1L", "1W", "28", "6P", "9A", "9C", "BB", "CE", "EA", "F8", "G1", "G3", "IG", "SY", Path = "REF/0/0")]
        //public virtual IList<ReferenceInformation> Ref1OtherClaimRelatedIdentifications { get; set; }

        // REF
        //[EdiCondition("0B", "1A", "1B", "1C", "1D", "1G", "1H", "1J", "D3", "G2", "LU", Path = "REF/0/0")]
        //public virtual IList<ReferenceInformation> Ref2RenderingProviderIdentifications { get; set; }

        // DTM
        [EdiCondition("232", "233", Path = "DTM/0/0")]
        public virtual IList<DateTimeReference> Dtm1StatementFromOrToDates { get; set; }

        // DTM
        [EdiCondition("036", Path = "DTM/0/0")]
        public virtual DateTimeReference Dtm2CoverageExpirationDate { get; set; }

        // DTM
        [EdiCondition("050", Path = "DTM/0/0")]
        public virtual DateTimeReference Dtm3ClaimReceivedDate { get; set; }
        
        // PER
        //public virtual IList<PersonInformation> PerClaimContactInformations { get; set; }

        // AMT
        //public virtual IList<MonetaryAmountInformation> AmtClaimSupplementalInformations { get; set; }

        // QTY
        //public virtual IList<QuantityInformation> QtyClaimSupplementalInformationQuantities { get; set; }

        // Loop 2110
        public virtual IList<ServicePaymentInformation> ServicePaymentInformations { get; set; }

        // Back reference to its parent
        public virtual HeaderNumber HeaderNumber { get; set; }
    }
}
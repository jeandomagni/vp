﻿namespace Ivh.Domain.HospitalData.Eob.Entities
{
    using Common.Base.Attributes;
    using indice.Edi.Serialization;

    [EdiSegment, EdiPath("N3/0")]
    [MaxStringLengthClass]
    public class PartyLocation
    {
        public virtual int PartyLocationId { get; set; }

        [EdiValue("X(55)", Path = "N3/0", Description = "N301 - Address Information")]
        [MaxStringLength(55)]
        public virtual string N301AddressInformation { get; set; }

        [EdiValue("X(55)", Path = "N3/1", Description = "N302 - Address Information")]
        [MaxStringLength(55)]
        public virtual string N302AddressInformation { get; set; }
    }
}
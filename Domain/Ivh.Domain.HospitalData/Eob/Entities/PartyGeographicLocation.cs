﻿namespace Ivh.Domain.HospitalData.Eob.Entities
{
    using Common.Base.Attributes;
    using indice.Edi.Serialization;

    [EdiSegment, EdiPath("N4/0")]
    [MaxStringLengthClass]
    public class PartyGeographicLocation
    {
        public virtual int PartyGeographicLocationId { get; set; }

        [EdiValue("X(30)", Path = "N4/0", Description = "N401 - City Name")]
        [MaxStringLength(30)]
        public virtual string N401CityName { get; set; }

        [EdiValue("X(2)", Path = "N4/1", Description = "N402 - State Or Province Code")]
        [MaxStringLength(2)]
        public virtual string N402StateOrProvinceCode { get; set; }

        [EdiValue("X(15)", Path = "N4/2", Description = "N403 - Postal Code")]
        [MaxStringLength(15)]
        public virtual string N403PostalCode { get; set; }

        [EdiValue("X(3)", Path = "N4/3", Description = "N404 - Country Code")]
        [MaxStringLength(3)]
        public virtual string N404CountryCode { get; set; }

        [EdiValue("X(2)", Path = "N4/4", Description = "N405 - Location Qualifier")]
        [MaxStringLength(2)]
        public virtual string N405LocationQualifier { get; set; }

        [EdiValue("X(30)", Path = "N4/5", Description = "N406 - Location Identifier")]
        [MaxStringLength(30)]
        public virtual string N406LocationIdentifier { get; set; }

        [EdiValue("X(3)", Path = "N4/6", Description = "N407 - Country Sub Code")]
        [MaxStringLength(3)]
        public virtual string N407CountrySubCode { get; set; }
    }
}
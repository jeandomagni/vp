﻿namespace Ivh.Domain.HospitalData.Eob.Entities
{
    using System;

    public class EobClaimAdjustmentReasonCode
    {
        public virtual int EobClaimAdjustmentReasonCodeId { get; set; }
        public virtual string ClaimAdjustmentReasonCode { get; set; }
        public virtual string Description { get; set; }
        public virtual DateTime StartDate { get; set; }
        public virtual DateTime? EndDate { get; set; }
    }
}
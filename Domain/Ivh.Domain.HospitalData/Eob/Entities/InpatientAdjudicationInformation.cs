﻿namespace Ivh.Domain.HospitalData.Eob.Entities
{
    using Common.Base.Attributes;
    using indice.Edi.Serialization;

    [EdiSegment, EdiPath("MIA/0")]
    [MaxStringLengthClass]
    public class InpatientAdjudicationInformation
    {
        public virtual int InpatientAdjudicationInformationId { get; set; }

        [EdiValue("9(15)", Path = "MIA/0", Description = "MIA01 - Quantity of covered days")]
        public virtual int Mia01Quantity { get; set; }

        [EdiValue("9(16)V9(2)", Path = "MIA/1", Description = "MIA02 - Prospective Payment System (PPS) Operating Outlier Amount")]
        public virtual decimal Mia02MonetaryAmount { get; set; }

        [EdiValue("9(15)", Path = "MIA/2", Description = "MIA03 - Quantity of lifetime psychiatric days")]
        public virtual int Mia03Quantity { get; set; }

        [EdiValue("9(16)V9(2)", Path = "MIA/3", Description = "MIA04 - Diagnosis Related Group (DRG) Amount")]
        public virtual decimal Mia04MonetaryAmount { get; set; }

        [EdiValue("X(50)", Path = "MIA/4", Description = "MIA05 - Claim Payment Rempark Code")]
        [MaxStringLength(50)]
        public virtual string Mia05ReferenceIdentifier { get; set; }

        [EdiValue("9(16)V9(2)", Path = "MIA/5", Description = "MIA06 - Disproprtonate Share Amount")]
        public virtual decimal Mia06MonetaryAmount { get; set; }

        [EdiValue("9(16)V9(2)", Path = "MIA/6", Description = "MIA07 - Medicare Secondary Payer pass-through Amount")]
        public virtual decimal Mia07MonetaryAmount { get; set; }

        [EdiValue("9(16)V9(2)", Path = "MIA/7", Description = "MIA08 - Prospective Payment System (PPS) capital Amount")]
        public virtual decimal Mia08MonetaryAmount { get; set; }

        [EdiValue("9(16)V9(2)", Path = "MIA/8", Description = "MIA09 - PPS capital, Federal specific portion, DRG Amount")]
        public virtual decimal Mia09MonetaryAmount { get; set; }

        [EdiValue("9(16)V9(2)", Path = "MIA/9", Description = "MIA10 - PPS capital, Hospital specific portion, DRG Amount")]
        public virtual decimal Mia10MonetaryAmount { get; set; }

        [EdiValue("9(16)V9(2)", Path = "MIA/10", Description = "MIA11 - PPS capital, disproportionate share, Hospital DRG Amount")]
        public virtual decimal Mia11MonetaryAmount { get; set; }

        [EdiValue("9(16)V9(2)", Path = "MIA/11", Description = "MIA12 - old capital Amount")]
        public virtual decimal Mia12MonetaryAmount { get; set; }

        [EdiValue("9(16)V9(2)", Path = "MIA/12", Description = "MIA13 - PPS capital indirect medical education claim Amount")]
        public virtual decimal Mia13MonetaryAmount { get; set; }

        [EdiValue("9(16)V9(2)", Path = "MIA/13", Description = "MIA14 - Hospital specific DRG Amount")]
        public virtual decimal Mia14MonetaryAmount { get; set; }

        [EdiValue("9(15)", Path = "MIA/14", Description = "MIA15 - Quantity of cost report days")]
        public virtual int Mia15Quantity { get; set; }

        [EdiValue("9(16)V9(2)", Path = "MIA/15", Description = "MIA16 - Federal specific DRG Amount")]
        public virtual decimal Mia16MonetaryAmount { get; set; }

        [EdiValue("9(16)V9(2)", Path = "MIA/16", Description = "MIA17 - PPS Capital Outiler Amount")]
        public virtual decimal Mia17MonetaryAmount { get; set; }

        [EdiValue("9(16)V9(2)", Path = "MIA/17", Description = "MIA18 - Indirect Teaching Amount")]
        public virtual decimal Mia18MonetaryAmount { get; set; }

        [EdiValue("9(16)V9(2)", Path = "MIA/18", Description = "MIA19 - Professional component amount builled but not payable")]
        public virtual decimal Mia19MonetaryAmount { get; set; }

        [EdiValue("X(50)", Path = "MIA/19", Description = "MIA20 - Claim Payment Remark Code")]
        [MaxStringLength(50)]
        public virtual string Mia20ReferenceIdentifier { get; set; }

        [EdiValue("X(50)", Path = "MIA/20", Description = "MIA21 - Claim Payment Remark Code")]
        [MaxStringLength(50)]
        public virtual string Mia21ReferenceIdentifier { get; set; }

        [EdiValue("X(50)", Path = "MIA/21", Description = "MIA22 - Claim Payment Remark Code")]
        [MaxStringLength(50)]
        public virtual string Mia22ReferenceIdentifier { get; set; }

        [EdiValue("X(50)", Path = "MIA/22", Description = "MIA23 - Claim Payment Remark Code")]
        [MaxStringLength(50)]
        public virtual string Mia23ReferenceIdentifier { get; set; }

        [EdiValue("9(16)V9(2)", Path = "MIA/23", Description = "MIA24 - Capital Exception Amount")]
        public virtual decimal Mia24MonetaryAmount { get; set; }

        public virtual EobRemittanceAdviceRemarkCode Mia05EobRemittanceAdviceRemarkCode { get; set; }
    }
}
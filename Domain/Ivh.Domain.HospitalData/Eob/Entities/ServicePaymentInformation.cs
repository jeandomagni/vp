﻿namespace Ivh.Domain.HospitalData.Eob.Entities
{
    using System.Collections.Generic;
    using Common.Base.Attributes;
    using indice.Edi.Serialization;

    [EdiGroup, EdiSegmentGroup("SVC")]
    [EdiPath("SVC/0/0")]
    [MaxStringLengthClass]
    public class ServicePaymentInformation
    {
        public virtual int ServicePaymentInformationId { get; set; }

        [EdiValue(Path = "SVC/0", Description = "SVC01 - Composite Medical Procedure Identifier")]
        public virtual string Svc01CompositeMedicalProcedureIdentifier { get; set; }

        [EdiValue("9(16)V9(2)", Path = "SVC/1", Description = "SVC02 - Submitted Service Charge")]
        public virtual decimal Svc02MonetaryAmount { get; set; }

        [EdiValue("9(16)V9(2)", Path = "SVC/2", Description = "SVC03 - Amount Paid this Service")]
        public virtual decimal Svc03MonetaryAmount { get; set; }

        [EdiValue("X(48)", Path = "SVC/3", Description = "SVC04 - Product Service Id")]
        [MaxStringLength(48)]
        public virtual string Svc04ProductServiceId { get; set; }

        [EdiValue("9(15)", Path = "SVC/4", Description = "SVC05 - Paid Units of Service")]
        public virtual decimal Svc05Quantity { get; set; }

        [EdiValue(Path = "SVC/5", Description = "SVC06 - Composite Medical Procedure Identifier")]
        public virtual string Svc06CompositeMedicalProcedureIdentifier { get; set; }

        [EdiValue("9(15)", Path = "SVC/6", Description = "SVC07 - Original Submitted Units of Service")]
        public virtual decimal Svc07Quantity { get; set; }

        // DTM
        public virtual IList<DateTimeReference> DtmServiceDates { get; set; }

        // CAS
        public virtual IList<ClaimAdjustment> CasServiceAdjustments { get; set; }  // to parse the actual file
        public virtual IList<ClaimAdjustmentPivot> ClaimAdjustmentPivots { get; set; } // to normalize data to make sum and group by useful

        // REF
        //[EdiCondition("1S", "APC", "BB", "E9", "G1", "G3", "LU", "RB", Path = "REF/0/0")]
        //public virtual IList<ReferenceInformation> Ref1ServiceIdentifications { get; set; }

        // REF
        //[EdiCondition("6R", Path = "REF/0/0")]
        //public virtual ReferenceInformation Ref2LineItemControlNumber { get; set; }

        // REF
        //[EdiCondition("0B", "1A", "1B", "1C", "1D", "1G", "1H", "1J", "D3", "G2", "HPI", "SY", "TJ", Path = "REF/0/0")]
        //public virtual IList<ReferenceInformation> Ref3RenderingProviderIdentifications { get; set; }

        // REF
        //[EdiCondition("0K", Path = "REF/0/0")]
        //public virtual IList<ReferenceInformation> Ref4HealthCarePolicyIdentifications { get; set; }

        // AMT
        //public virtual IList<MonetaryAmountInformation> AmtServiceSupplementalAmounts { get; set; }

        // QTY
        //public virtual IList<QuantityInformation> QtyServiceSupplementalQuantities { get; set; }

        //LQ
        public virtual IList<IndustryCodeIdentification> LqHealthCareRemarkCodes { get; set; }

        // Back reference to its parent
        public virtual ClaimPaymentInformation ClaimPaymentInformation { get; set; }
    }
}
﻿namespace Ivh.Domain.HospitalData.Eob.Entities
{
    using System.Collections.Generic;
    using Common.Base.Attributes;
    using indice.Edi.Serialization;

    [EdiSegment, EdiSegmentGroup("LX", SequenceEnd = "PLB")]
    [EdiPath("LX")]
    [MaxStringLengthClass]
    public class HeaderNumber
    {
        public virtual int HeaderNumberId { get; set; }
        public virtual int TransactionSetHeaderTrailerId { get; set; }

        [EdiValue("X(6)", Path = "LX/0", Description = "LX01 - Assigned Number")]
        [MaxStringLength(6)]
        public virtual int Lx01AssignedNumber { get; set; }

        // TS3
        //public virtual TransactionStatistics Ts3ProviderSummaryInformation { get; set; }

        // TS2
        //public virtual TransactionSupplementalStatistics Ts2ProviderSupplementalSummaryInformation { get; set; }

        // Loops 2100-2110
        public virtual IList<ClaimPaymentInformation> ClaimPaymentInformations { get; set; }

        public virtual TransactionSetHeaderTrailer835 TransactionSetHeaderTrailer { get; set; }
    }
}
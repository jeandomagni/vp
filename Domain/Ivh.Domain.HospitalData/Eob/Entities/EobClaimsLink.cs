﻿namespace Ivh.Domain.HospitalData.Eob.Entities
{
    public class EobClaimsLink
    {
        public virtual int EobClaimsLinkId { get; set; }
        public virtual string ClaimNumber { get; set; }
        public virtual string VisitSourceSystemKey { get; set; }
        public virtual int BillingSystemId { get; set; }
    }
}

﻿namespace Ivh.Domain.HospitalData.Eob.Entities
{
    using System.Collections.Generic;
    using System.Linq;

    public class EobPayerFilter
    {
        public virtual int EobPayerFilterId { get; set; }
        public virtual string UniquePayerValue { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual IList<EobPayerFilterExternalLink> EobPayerFilterEobExternalLinks { get; protected set; }
    }
}
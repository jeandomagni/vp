﻿namespace Ivh.Domain.HospitalData.Eob.Entities
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Attributes;
    using indice.Edi.Serialization;

    [MaxStringLengthClass]
    public class InterchangeControlHeaderTrailer
    {
        public virtual int InterchangeControlHeaderTrailerId { get; set; }
        public virtual int? FileTrackerId { get; set; }

        [EdiValue("9(2)", Path = "ISA/0", Description = "ISA01 - Authorization Information Qualifier")]
        public virtual int Isa01AuthorizationInformationQualifier { get; set; }

        [EdiValue("X(10)", Path = "ISA/1", Description = "ISA02 - Authorization Information")]
        [MaxStringLength(10)]
        public virtual string Isa02AuthorizationInformation { get; set; }

        [EdiValue("9(2)", Path = "ISA/2", Description = "ISA03 - Security Information Qualifier")]
        public virtual string Isa03SecurityInformationQualifier { get; set; }

        [EdiValue("X(10)", Path = "ISA/3", Description = "ISA04 - Security Information")]
        [MaxStringLength(10)]
        public virtual string Isa04SecurityInformation { get; set; }

        [EdiValue("9(2)", Path = "ISA/4", Description = "ISA05 - Interchange ID Qualifier")]
        public virtual string Isa05IdQualifier { get; set; }

        [EdiValue("X(15)", Path = "ISA/5", Description = "ISA06 - Interchange Sender ID")]
        [MaxStringLength(15)]
        public virtual string Isa06SenderId { get; set; }

        [EdiValue("9(2)", Path = "ISA/6", Description = "ISA07 - Interchange ID Qualifier")]
        public virtual string Isa07IdQualifier { get; set; }

        [EdiValue("X(15)", Path = "ISA/7", Description = "ISA08 - Interchange Receiver ID")]
        [MaxStringLength(15)]
        public virtual string Isa08ReceiverId { get; set; }

        [EdiValue("9(6)", Path = "ISA/8", Format = "yyMMdd", Description = "I09 - Interchange Date")]
        [EdiValue("9(4)", Path = "ISA/9", Format = "HHmm", Description = "I10 - Interchange Time")]
        public virtual DateTime Isa0910DateTime { get; set; }

        [EdiValue("X(1)", Path = "ISA/10", Description = "ISA11 - Interchange Control Standards ID")]
        [MaxStringLength(1)]
        public virtual string Isa11ControlStandardsId { get; set; }

        [EdiValue("9(5)", Path = "ISA/11", Description = "ISA12 - Interchange Control Version Num")]
        public virtual int Isa12ControlVersion { get; set; }

        [EdiValue("9(9)", Path = "ISA/12", Description = "ISA13 - Interchange Control Number")]
        public virtual int Isa13ControlNumber { get; set; }

        [EdiValue("9(1)", Path = "ISA/13", Description = "ISA14 - Acknowledgement Requested")]
        public virtual bool? Isa14AcknowledgementRequested { get; set; }

        [EdiValue("X(1)", Path = "ISA/14", Description = "ISA15 - Usage Indicator")]
        [MaxStringLength(1)]
        public virtual string Isa15UsageIndicator { get; set; }

        [EdiValue("X(1)", Path = "ISA/15", Description = "ISA16 - Component Element Separator")]
        public virtual char Isa16ComponentElementSeparator { get; set; }

        [EdiValue("9(1)", Path = "IEA/0", Description = "IEA01 - Num of Included Functional Grps")]
        public virtual int Iea01GroupsCount { get; set; }

        [EdiValue("9(9)", Path = "IEA/1", Description = "IEA02 - Interchange Control Number")]
        public virtual int Iea02TrailerControlNumber { get; set; }

        public virtual DateTime? MigrationDate { get; set; }

        // GS - GE
        public virtual IList<FunctionalGroupHeaderTrailer> FunctionalGroupHeaderTrailerHeaders { get; set; }

    }
}
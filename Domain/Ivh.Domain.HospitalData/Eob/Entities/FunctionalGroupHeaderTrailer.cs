﻿namespace Ivh.Domain.HospitalData.Eob.Entities
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Attributes;
    using indice.Edi.Serialization;

    [MaxStringLengthClass]
    [EdiGroup]
    public class FunctionalGroupHeaderTrailer
    {
        public virtual int FunctionalGroupHeaderTrailerId { get; set; }

        [EdiValue("X(2)", Path = "GS/0", Description = "GS01 - Functional Identifier Code")]
        [MaxStringLength(2)]
        public virtual string Gs01FunctionalIdentifierCode { get; set; }

        [EdiValue("X(15)", Path = "GS/1", Description = "GS02 - Application Sender's Code")]
        [MaxStringLength(15)]
        public virtual string Gs02ApplicationSenderCode { get; set; }

        [EdiValue("X(15)", Path = "GS/2", Description = "GS03 - Application Receiver's Code")]
        [MaxStringLength(15)]
        public virtual string Gs03ApplicationReceiverCode { get; set; }

        [EdiValue("9(8)", Path = "GS/3", Format = "yyyyMMdd", Description = "GS04 - Date")]
        [EdiValue("9(4)", Path = "GS/4", Format = "HHmm", Description = "GS05 - Time")]
        public virtual DateTime Gs0405DateTime { get; set; }

        [EdiValue("9(9)", Path = "GS/5", Description = "GS06 - Group Control Number")]
        public virtual int Gs06GroupControlNumber { get; set; }

        [EdiValue("X(2)", Path = "GS/6", Description = "GS07 - Responsible Agency Code")]
        [MaxStringLength(2)]
        public virtual string Gs07AgencyCode { get; set; }

        [EdiValue("X(2)", Path = "GS/7", Description = "GS08 - Version / Release / Industry Identifier Code")]
        [MaxStringLength(2)]
        public virtual string Gs08Version { get; set; }

        // ST - SE
        public virtual IList<TransactionSetHeaderTrailer835> TransactionHeaders { get; set; }


        [EdiValue("9(1)", Path = "GE/0", Description = "GE01 - Number of Transaction Sets Included")]
        public virtual int Ge01TransactionsCount { get; set; }

        [EdiValue("9(9)", Path = "GE/1", Description = "GE02 -  Group Control Number")]
        public virtual int Ge02GroupTrailerControlNumber { get; set; }

        // Back reference to its parent
        public virtual InterchangeControlHeaderTrailer InterchangeControlHeaderTrailer { get; set; }
    }
}
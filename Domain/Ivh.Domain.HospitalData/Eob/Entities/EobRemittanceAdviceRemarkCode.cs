﻿namespace Ivh.Domain.HospitalData.Eob.Entities
{
    using System;

    public class EobRemittanceAdviceRemarkCode
    {
        public virtual int EobRemittanceAdviceRemarkCodeId { get; set; }
        public virtual string RemittanceAdviceRemarkCode { get; set; }
        public virtual string Description { get; set; }
        public virtual DateTime StartDate { get; set; }
        public virtual DateTime EndDate { get; set; }
    }
}
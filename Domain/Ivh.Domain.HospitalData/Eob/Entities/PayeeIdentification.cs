﻿namespace Ivh.Domain.HospitalData.Eob.Entities
{
    using System.Collections.Generic;
    using Common.Base.Attributes;
    using indice.Edi.Serialization;

    [EdiSegment, EdiSegmentGroup("N1", SequenceEnd = "LX")]
    [MaxStringLengthClass]
    public class PayeeIdentification
    {
        public virtual int PayeeIdentificationId { get; set; }

        [EdiValue("X(3)", Path = "N1/0", Description = "N101 - Entity Identifier Code")]
        [MaxStringLength(3)]
        public virtual string N101EntityIdentifierCode { get; set; }

        [EdiValue("X(60)", Path = "N1/1", Description = "N102 - Name")]
        [MaxStringLength(60)]
        public virtual string N102PayeeName { get; set; }

        [EdiValue("X(2)", Path = "N1/2", Description = "N103 - Identification Code Qualifier")]
        [MaxStringLength(2)]
        public virtual string N103IdentificationCodeQualifier { get; set; }

        [EdiValue("X(80)", Path = "N1/3", Description = "N104 - Identification Code")]
        [MaxStringLength(80)]
        public virtual string N104IdentificationCode { get; set; }

        [EdiValue("X(2)", Path = "N1/4", Description = "N105 - Entity Relationship Code")]
        [MaxStringLength(2)]
        public virtual string N105EntityRelationshipCode { get; set; }

        [EdiValue("X(3)", Path = "N1/5", Description = "N106 - Entity Identifier Code")]
        [MaxStringLength(3)]
        public virtual string N106EntityIdentifierCode { get; set; }

        //public virtual PartyLocation N3PayeeAddress { get; set; }
        //public virtual PartyGeographicLocation N4PayeeCityStateZip { get; set; }
        //public virtual IList<ReferenceInformation> RefAdditionalPayeeIdentification { get; set; }

        [EdiValue("X(2)", Path = "RDM/0", Description = "RDM01 - Report Transmission Code")]
        [MaxStringLength(2)]
        public virtual string Rdm01ReportTransmissionCode { get; set; }

        [EdiValue("X(60)", Path = "RDM/1", Description = "RDM02 - Third Party Processor Name")]
        [MaxStringLength(60)]
        public virtual string Rdm02ThirdPartyProcessorName { get; set; }

        [EdiValue(Path = "RDM/2", Description = "RDM03 - Communication Number")]
        public virtual string Rdm03CommunicationNumber { get; set; }
    }
}
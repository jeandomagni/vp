﻿namespace Ivh.Domain.HospitalData.Eob.Entities
{
    using System;
    using Common.Base.Attributes;
    using indice.Edi.Serialization;

    [EdiSegment, EdiPath("PLB/0")]
    //[EdiMessage]
    //[EdiPath("PLB/0/0")]
    [MaxStringLengthClass]
    public class ProviderLevelAdjustment
    {
        public virtual int ProviderLevelAdjustmentId { get; set; }

        [EdiValue("X(50)", Path = "PLB/0", Description = "PLB01 - Reference Identification")]
        [MaxStringLength(50)]
        public virtual string Plb01ReferenceIdentification { get; set; }

        [EdiValue("9(8)", Path = "PLB/1", Format = "yyyyMMdd", Description = "PLB02 - Date format = CCYYMMDD")]
        public virtual DateTime Plb02Date { get; set; }

        [EdiValue(Path = "PLB/2", Description = "PLB03 - Adjustment Identifier")]
        public virtual string Plb03CompositeAdjustmentIdentifier { get; set; }

        [EdiValue("9(16)V9(2)", Path = "PLB/3", Description = "PLB04 - Monetary Amount")]
        public virtual decimal Plb04MonetaryAmount { get; set; }

        [EdiValue(Path = "PLB/4", Description = "PLB05 - Adjustment Identifier")]
        public virtual string Plb05CompositeAdjustmentIdentifier { get; set; }

        [EdiValue("9(16)V9(2)", Path = "PLB/5", Description = "PLB06 - Monetary Amount")]
        public virtual decimal Plb06MonetaryAmount { get; set; }

        [EdiValue(Path = "PLB/6", Description = "PLB07 - Adjustment Identifier")]
        public virtual string Plb07CompositeAdjustmentIdentifier { get; set; }

        [EdiValue("9(16)V9(2)", Path = "PLB/7", Description = "PLB08 - Monetary Amount")]
        public virtual decimal Plb08MonetaryAmount { get; set; }

        [EdiValue(Path = "PLB/8", Description = "PLB09 - Adjustment Identifier")]
        public virtual string Plb09CompositeAdjustmentIdentifier { get; set; }

        [EdiValue("9(16)V9(2)", Path = "PLB/9", Description = "PLB10 - Monetary Amount")]
        public virtual decimal Plb10MonetaryAmount { get; set; }

        [EdiValue(Path = "PLB/10", Description = "PLB11 - Adjustment Identifier")]
        public virtual string Plb11CompositeAdjustmentIdentifier { get; set; }

        [EdiValue("9(16)V9(2)", Path = "PLB/11", Description = "PLB12 - Monetary Amount")]
        public virtual decimal Plb12MonetaryAmount { get; set; }

        [EdiValue(Path = "PLB/12", Description = "PLB13 - Adjustment Identifier")]
        public virtual string Plb13CompositeAdjustmentIdentifier { get; set; }

        [EdiValue("9(16)V9(2)", Path = "PLB/13", Description = "PLB14 - Monetary Amount")]
        public virtual decimal Plb14MonetaryAmount { get; set; }
    }
}
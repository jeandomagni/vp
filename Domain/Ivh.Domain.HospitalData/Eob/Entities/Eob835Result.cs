﻿namespace Ivh.Domain.HospitalData.Eob.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class Eob835Result
    {
        public virtual int TransactionSetHeaderTrailerId { get; set; }
        public virtual DateTime TransactionDate { get; set; }
        public virtual int HeaderNumberId { get; set; }
        public virtual int HeaderNumber { get; set; }
        public virtual int ClaimPaymentInformationId { get; set; }
        public virtual int EobPayerFilterId { get; set; }
        public virtual IList<EobPayerFilterExternalLink> EobPayerFilterEobExternalLinks { get; set; }
        public virtual IEnumerable<ClaimPaymentInformation> ClaimPaymentInformations { get; set; }
    }
}
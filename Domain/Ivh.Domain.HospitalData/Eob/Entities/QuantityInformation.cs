﻿namespace Ivh.Domain.HospitalData.Eob.Entities
{
    using Common.Base.Attributes;
    using indice.Edi.Serialization;

    [EdiSegment, EdiPath("QTY")]
    [MaxStringLengthClass]
    public class QuantityInformation
    {
        public virtual int QuantityInformationId { get; set; }

        [EdiValue("X(2)", Path = "QTY/0", Description = "QTY01 - Quantity Qualifier")]
        [MaxStringLength(2)]
        public virtual string Qty01QuantityQualifier { get; set; }

        [EdiValue("9(15)", Path = "QTY/1", Description = "QTY02 - Quantity")]
        public virtual int Qty02Quantity { get; set; }

        [EdiValue(Path = "QTY/2", Description = "QTY03 - Composite Unit Of Measure")]
        public virtual string Qty03CompositeUnitOfMeasure { get; set; }

        [EdiValue("X(30)", Path = "QTY/3", Description = "QTY04 - FreeFormMessage")]
        [MaxStringLength(30)]
        public virtual string Qty04FreeFormMessage { get; set; }
    }
}
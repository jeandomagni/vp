﻿namespace Ivh.Domain.HospitalData.Eob.Entities
{
    using System;
    using Common.Base.Attributes;
    using indice.Edi.Serialization;

    [MaxStringLengthClass]
    [EdiSegment, EdiPath("DTM")]
    public class DateTimeReference
    {
        public virtual int DateTimeReferenceId { get; set; }

        [EdiValue("9(3)", Path = "DTM/0", Description = "DTM01 - Date Time Qualifier")]
        public virtual string Dtm01DateTimeQualifier { get; set; }

        [EdiValue("9(8)", Path = "DTM/1", Format = "yyyyMMdd", Description = "DTM02 - Date format = CCYYMMDD")]
        public virtual DateTime Dtm02Date { get; set; }

        // Back references to its parents
        public virtual TransactionSetHeaderTrailer835 TransactionSetHeaderTrailer { get; set; }
        public virtual ClaimPaymentInformation ClaimPaymentInformation { get; set; }
        public virtual ServicePaymentInformation ServicePaymentInformation { get; set; }
    }
}
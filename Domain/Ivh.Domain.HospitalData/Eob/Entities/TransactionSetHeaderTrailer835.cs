﻿namespace Ivh.Domain.HospitalData.Eob.Entities
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Attributes;
    using indice.Edi.Serialization;
    [EdiPath("ST/0/0")]
    [EdiMessage]
    [MaxStringLengthClass]
    public class TransactionSetHeaderTrailer835
    {
        public virtual int TransactionSetHeaderTrailerId { get; set; }
        public virtual int PayerIdentificationId { get; set; }
        public virtual EobPayerFilter EobPayerFilter { get; set; }

        [EdiValue("9(3)", Path = "ST/0", Description = "ST01 - Transaction Set Identifier Code")]
        public virtual int St01TransactionSetIdentifierCode { get; set; }

        [EdiValue("X(9)", Path = "ST/1", Description = "ST02 - Transaction Set Control Number")]
        [MaxStringLength(9)]
        public virtual string St02TransactionSetControlNumber { get; set; }


        [EdiValue("X(2)", Path = "BPR/0", Description = "BPR01 - Transactioning Handling Code")]
        [MaxStringLength(2)]
        public virtual string Bpr01TransactionHandleCode { get; set; }

        [EdiValue("9(16)V9(2)", Path = "BPR/1", Description = "BPR02 - Monetary Amount")]
        public virtual decimal Bpr02MonetaryAmount { get; set; }

        [EdiValue("X(1)", Path = "BPR/2", Description = "BPR03 - Credit/Debit Code")]
        [MaxStringLength(1)]
        public virtual string Bpr03CreditDebitFlagCode { get; set; }

        [EdiValue("X(3)", Path = "BPR/3", Description = "BPR04 - Payment Method Code")]
        [MaxStringLength(3)]
        public virtual string Bpr04PaymentMethodFlagCode { get; set; }

        [EdiValue("X(10)", Path = "BPR/4", Description = "BPR05 - Payment Format Code")]
        [MaxStringLength(10)]
        public virtual string Bpr05PaymentFormatCode { get; set; }

        [EdiValue("X(2)", Path = "BPR/5", Description = "BPR06 - Depository Financial Institution (DFI) Id Number Qualifier")]
        [MaxStringLength(2)]
        public virtual string Bpr06EfiIdentificationNumberQualifier { get; set; }

        [EdiValue("X(12)", Path = "BPR/6", Description = "BPR07 - DepositoryFinancialInstitution (DFI) Id Number ")]
        [MaxStringLength(12)]
        public virtual string Bpr07DfiIdentificationNumber { get; set; }

        [EdiValue("X(3)", Path = "BPR/7", Description = "BPR08 - Account Number Qualifier")]
        [MaxStringLength(3)]
        public virtual string Bpr08AccountNumberQualifier { get; set; }

        [EdiValue("X(35)", Path = "BPR/8", Description = "BPR09 - Account Number")]
        [MaxStringLength(35)]
        public virtual string Bpr09AccountNumber { get; set; }

        [EdiValue("X(10)", Path = "BPR/9", Description = "BPR10 - Originating Company Identifier")]
        [MaxStringLength(10)]
        public virtual string Bpr10OriginatingCompanyIdentifier { get; set; }
        
        private string _bpr11OriginCompaySupplementalCode;
        [EdiValue("X(9)", Path = "BPR/10", Description = "BPR11 - Origin Compay Supplemental Code")]
        public virtual string Bpr11OriginCompaySupplementalCode
        {
            get
            {
                if (this._bpr11OriginCompaySupplementalCode?.Length > 9)
                    return this._bpr11OriginCompaySupplementalCode.Substring(0, 9);
                return this._bpr11OriginCompaySupplementalCode;
            }
            set { this._bpr11OriginCompaySupplementalCode = value; }
        }

        [EdiValue("X(2)", Path = "BPR/11", Description = "BPR12 - DepositoryFinancialInstitution (DFI) Id Number ")]
        [MaxStringLength(2)]
        public virtual string Bpr12DfiIdentificationNumber { get; set; }

        [EdiValue("X(12)", Path = "BPR/12", Description = "BPR13 - Account Number Qualifier")]
        [MaxStringLength(12)]
        public virtual string Bpr13AccountNumberQualifier { get; set; }

        [EdiValue("X(3)", Path = "BPR/13", Description = "BPR14 - Account Number")]
        [MaxStringLength(3)]
        public virtual string Bpr14AccountNumber { get; set; }

        [EdiValue("X(35)", Path = "BPR/14", Description = "BPR15 - Originating Company Identifier")]
        [MaxStringLength(35)]
        public virtual string Bpr15OriginatingCompanyIdentifier { get; set; }

        [EdiValue("9(8)", Path = "BPR/15", Format = "yyyyMMdd", Description = "BPR16 - Date format = CCYYMMDD")]
        public virtual DateTime? Bpr16Date { get; set; }


        [EdiValue("X(2)", Path = "TRN/0", Description = "TRN01 - Trace Type Code")]
        [MaxStringLength(2)]
        public virtual string Trn01TraceTypeCode { get; set; }

        [EdiValue("X(50)", Path = "TRN/1", Description = "TRN02 - Reference Identification")]
        [MaxStringLength(50)]
        public virtual string Trn02ReferenceIdentification { get; set; }

        [EdiValue("X(10)", Path = "TRN/2", Description = "TRN03 - Originating Company Identification")]
        [MaxStringLength(10)]
        public virtual string Trn03OriginatingCompanyId { get; set; }

        [EdiValue("X(50)", Path = "TRN/3", Description = "TRN04 - Reference Identification")]
        [MaxStringLength(50)]
        public virtual string Trn04ReferenceIdentification { get; set; }


        [EdiValue("9(3)", Path = "CUR/0", Description = "CUR01 - Entity Identifier Code")]
        [MaxStringLength(3)]
        public virtual string Cur01EntityIdentifierCode { get; set; }

        [EdiValue("9(3)", Path = "CUR/1", Description = "CUR02 - Currency Code")]
        public virtual string Cur02CurrencyCode { get; set; }


        //REF
        //[EdiCondition("EV", Path = "REF/0/0")]
        //public virtual ReferenceInformation Ref1ReceiverIdentification { get; set; }

        //REF
        //[EdiCondition("F2", Path = "REF/0/0")]
        //public virtual ReferenceInformation Ref2VersionIdentification { get; set; }

        // DTM
        public virtual DateTimeReference DtmProductionDate { get; set; }

        // Loop 1000A
        [EdiCondition("PR", Path = "N1/0/0")]
        public virtual PayerIdentification PayerDetails { get; set; }

        // Loop 1000B
        [EdiCondition("PE", Path = "N1/0/0")]
        public virtual PayeeIdentification PayeeDetails { get; set; }

        // Loops 2000 - 2110
        public virtual IList<HeaderNumber> Transactions { get; set; }

        // PLB
        //[EdiPath("PLB/0/0")]
        //public virtual IList<ProviderLevelAdjustment> PlbProviderAdjustments { get; set; }


        [EdiValue("9(1)", Path = "SE/0", Description = "SE01 - Number of Segments Included")]
        public virtual int Se01MessageSegmentsCount { get; set; }

        [EdiValue("X(9)", Path = "SE/1", Description = "SE02 - Transaction Set Control Number")]
        [MaxStringLength(9)]
        public virtual string Se02MessageControlNumber { get; set; }

        // Back reference to its parent
        public virtual FunctionalGroupHeaderTrailer FunctionalGroupHeaderTrailer { get; set; }

    }
}
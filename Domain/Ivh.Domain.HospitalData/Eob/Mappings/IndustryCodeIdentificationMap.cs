﻿namespace Ivh.Domain.HospitalData.Eob.Mappings
{
    using FluentNHibernate.Mapping;

    public class IndustryCodeIdentificationMap : ClassMap<Entities.IndustryCodeIdentification>
    {
        public IndustryCodeIdentificationMap()
        {
            this.Schema("`eob`");
            this.Table("`835IndustryCodeIdentification`");
            this.Id(x => x.IndustryCodeIdentificationId)
                .GeneratedBy.HiLo("EOB_HiLo", "NextHi", "1000", "TableKey = '835IndustryCodeIdentification'");
            this.Map(x => x.Lq01CodeListQualifierCode, "LQ01").Nullable().Length(3);
            this.Map(x => x.Lq02IndustryCode, "LQ02").Nullable().Length(30);

            this.References(x => x.EobRemittanceAdviceRemarkCode).Column("LQ02").PropertyRef("RemittanceAdviceRemarkCode")
                .Nullable().NotFound.Ignore()
                .Cascade.All()
                .Not.LazyLoad()
                .Fetch.Join()
                .Not.Insert()
                .Not.Update();

            // Back reference to its parent
            this.References(x => x.ServicePaymentInformation)
                .Column("ServicePaymentInformationId")
                .Not.Insert()
                .Not.Update();
        }
    }
}
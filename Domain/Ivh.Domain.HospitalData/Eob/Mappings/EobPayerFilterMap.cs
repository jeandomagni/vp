﻿namespace Ivh.Domain.HospitalData.Eob.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class EobPayerFilterMap : ClassMap<EobPayerFilter>
    {
        public EobPayerFilterMap()
        {
            this.Schema("`eob`");
            this.Table("`EobPayerFilter`");
            this.Id(x => x.EobPayerFilterId).Not.Nullable();
            this.Map(x => x.UniquePayerValue).Not.Nullable();
            this.Map(x => x.IsActive).Not.Nullable();

            this.HasMany(x => x.EobPayerFilterEobExternalLinks)
                .KeyColumn("EobPayerFilterId")
                .Cascade.All()
                .NotFound.Ignore()
                .ReadOnly();
        }
    }
}
﻿namespace Ivh.Domain.HospitalData.Eob.Mappings
{
    using FluentNHibernate.Mapping;

    public class ClaimAdjustmentPivotMap : ClassMap<Entities.ClaimAdjustmentPivot>
    {
        public ClaimAdjustmentPivotMap()
        {
            this.Schema("`eob`");
            this.Table("`835ClaimAdjustment`");
            this.Id(x => x.ClaimAdjustmentId)
                .GeneratedBy.HiLo("EOB_HiLo", "NextHi", "1000", "TableKey = '835ClaimAdjustment'");
            this.Map(x => x.ClaimAdjustmentGroupCode).Nullable();
            this.Map(x => x.ClaimAdjustmentReasonCode).Nullable();
            this.Map(x => x.MonetaryAmount).Nullable();
            this.Map(x => x.Quantity).Nullable();

            this.References(x => x.EobClaimAdjustmentReasonCode).Column("ClaimAdjustmentReasonCode").PropertyRef("ClaimAdjustmentReasonCode")
                .Nullable().NotFound.Ignore()
                .Cascade.All()
                .Not.LazyLoad()
                .Fetch.Join()
                .Not.Insert()
                .Not.Update();

            // Back references to its parents
            this.References(x => x.ClaimPaymentInformation)
                .Column("ClaimPaymentInformationId")
                .Not.Insert()
                .Not.Update();

            this.References(x => x.ServicePaymentInformation)
                .Column("ServicePaymentInformationId")
                .Not.Insert()
                .Not.Update();
        }

    }
}
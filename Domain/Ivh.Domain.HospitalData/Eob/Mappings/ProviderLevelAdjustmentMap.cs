﻿namespace Ivh.Domain.HospitalData.Eob.Mappings
{
    using FluentNHibernate.Mapping;

    public class ProviderLevelAdjustmentMap : ClassMap<Entities.ProviderLevelAdjustment>
    {
        public ProviderLevelAdjustmentMap()
        {
            this.Schema("`eob`");
            this.Table("`835ProviderLevelAdjustment`");
            this.Id(x => x.ProviderLevelAdjustmentId)
                .GeneratedBy.HiLo("EOB_HiLo", "NextHi", "10", "TableKey = '835ProviderLevelAdjustment'");
            this.Map(x => x.Plb01ReferenceIdentification, "PLB01").Nullable().Length(50);
            this.Map(x => x.Plb02Date, "PLB02").Nullable();
            this.Map(x => x.Plb03CompositeAdjustmentIdentifier, "PLB03").Nullable();
            this.Map(x => x.Plb04MonetaryAmount, "PLB04").Nullable();
            this.Map(x => x.Plb05CompositeAdjustmentIdentifier, "PLB05").Nullable();
            this.Map(x => x.Plb06MonetaryAmount, "PLB06").Nullable();
            this.Map(x => x.Plb07CompositeAdjustmentIdentifier, "PLB07").Nullable();
            this.Map(x => x.Plb08MonetaryAmount, "PLB08").Nullable();
            this.Map(x => x.Plb09CompositeAdjustmentIdentifier, "PLB09").Nullable();
            this.Map(x => x.Plb10MonetaryAmount, "PLB10").Nullable();
            this.Map(x => x.Plb11CompositeAdjustmentIdentifier, "PLB11").Nullable();
            this.Map(x => x.Plb12MonetaryAmount, "PLB12").Nullable();
            this.Map(x => x.Plb13CompositeAdjustmentIdentifier, "PLB13").Nullable();
            this.Map(x => x.Plb14MonetaryAmount, "PLB14").Nullable();
        }

    }
}
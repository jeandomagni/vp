﻿namespace Ivh.Domain.HospitalData.Eob.Mappings
{
    using FluentNHibernate.Mapping;

    public class PayerIdentificationMap : ClassMap<Entities.PayerIdentification>
    {
        public PayerIdentificationMap()
        {
            this.Schema("`eob`");
            this.Table("`835PayerIdentification`");
            this.Id(x => x.PayerIdentificationId)
                .GeneratedBy.HiLo("EOB_HiLo", "NextHi", "10", "TableKey = '835PayerIdentification'");
            this.Map(x => x.N101EntityIdentifierCode, "N101").Nullable().Length(3);
            this.Map(x => x.N102PayerName, "N102").Nullable().Length(60);
            this.Map(x => x.N103IdentificationCodeQualifier, "N103").Nullable().Length(2);
            this.Map(x => x.N104IdentificationCode, "N104").Nullable().Length(80);
            this.Map(x => x.N105EntityRelationshipCode, "N105").Nullable().Length(2);
            this.Map(x => x.N106EntityIdentifierCode, "N106").Nullable().Length(3);

            // N3
            //this.References(x => x.N3PayerAddress).Column("PartyLocationId")
            //    .Nullable().NotFound.Ignore()
            //    .Cascade.All()
            //    .Not.LazyLoad()
            //    .Fetch.Join();

            // N4
            //this.References(x => x.N4PayerCityStateZip).Column("PartyGeographicLocationId")
            //    .Nullable().NotFound.Ignore()
            //    .Cascade.All()
            //    .Not.LazyLoad()
            //    .Fetch.Join();

            // PER
            this.References(x => x.Per1PayerBusinessContactInformation).Column("Per1PersonInformationId")
                .Nullable().NotFound.Ignore()
                .Cascade.All()
                .Not.LazyLoad()
                .Fetch.Join();

            // PER
            this.References(x => x.Per3PayerWebSite).Column("Per3PersonInformationId")
                .Nullable().NotFound.Ignore()
                .Cascade.All()
                .Not.LazyLoad()
                .Fetch.Join();


            // REF
            //this.HasMany(x => x.RefAdditionalPayerIdentification)
            //    .KeyColumn("PayerIdentificationId")
            //    .Cascade.All()
            //    .NotFound.Ignore()
            //    .Not.LazyLoad();

            // PER
            this.HasMany(x => x.Per2PayerTechnicalContactInformation)
                .KeyColumn("PayerIdentificationId")
                .Cascade.All()
                .NotFound.Ignore()
                .Not.LazyLoad();


        }

    }
}
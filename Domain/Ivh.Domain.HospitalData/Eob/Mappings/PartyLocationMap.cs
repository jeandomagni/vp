﻿namespace Ivh.Domain.HospitalData.Eob.Mappings
{
    using FluentNHibernate.Mapping;

    public class PartyLocationMap : ClassMap<Entities.PartyLocation>
    {
        public PartyLocationMap()
        {
            this.Schema("`eob`");
            this.Table("`835PartyLocation`");
            this.Id(x => x.PartyLocationId)
                .GeneratedBy.HiLo("EOB_HiLo", "NextHi", "10", "TableKey = '835PartyLocation'");
            this.Map(x => x.N301AddressInformation, "N301").Nullable().Length(55);
            this.Map(x => x.N302AddressInformation, "N302").Nullable().Length(55);
        }

    }
}
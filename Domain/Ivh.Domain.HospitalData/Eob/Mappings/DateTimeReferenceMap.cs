﻿namespace Ivh.Domain.HospitalData.Eob.Mappings
{
    using FluentNHibernate.Mapping;

    public class DateTimeReferenceMap : ClassMap<Entities.DateTimeReference>
    {
        public DateTimeReferenceMap()
        {
            this.Schema("`eob`");
            this.Table("`835DateTimeReference`");
            this.Id(x => x.DateTimeReferenceId)
                .GeneratedBy.HiLo("EOB_HiLo", "NextHi", "1000", "TableKey = '835DateTimeReference'");
            this.Map(x => x.Dtm01DateTimeQualifier, "DTM01").Nullable();
            this.Map(x => x.Dtm02Date, "DTM02").Nullable();

            // Back references to its parents
            this.References(x => x.TransactionSetHeaderTrailer)
                .Column("TransactionSetHeaderTrailerId")
                .Not.Insert()
                .Not.Update();

            this.References(x => x.ClaimPaymentInformation)
                .Column("ClaimPaymentInformationId")
                .Not.Insert()
                .Not.Update();

            this.References(x => x.ServicePaymentInformation)
                .Column("ServicePaymentInformationId")
                .Not.Insert()
                .Not.Update();
        }
    }
}
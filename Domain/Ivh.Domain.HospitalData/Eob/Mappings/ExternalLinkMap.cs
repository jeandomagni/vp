﻿namespace Ivh.Domain.HospitalData.Eob.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class ExternalLinkMap : ClassMap<ExternalLink>
    {
        public ExternalLinkMap()
        {
            this.Schema("`eob`");
            this.Table("`ExternalLink`");
            this.Id(x => x.ExternalLinkId).Not.Nullable();
            this.Map(x => x.Url).Not.Nullable();
            this.Map(x => x.Text).Not.Nullable();
            this.Map(x => x.Icon).Nullable();
            this.Map(x => x.PersistDays).Nullable();
            this.Map(x => x.ClaimNumberLocation).Nullable();
        }
    }
}
﻿namespace Ivh.Domain.HospitalData.Eob.Mappings
{
    using System.Collections.Generic;
    using Entities;
    using FluentNHibernate.Mapping;
    using indice.Edi.Serialization;

    public class ServicePaymentInformationMap : ClassMap<Entities.ServicePaymentInformation>
    {
        public ServicePaymentInformationMap()
        {
            this.Schema("`eob`");
            this.Table("`835ServicePaymentInformation`");
            this.Id(x => x.ServicePaymentInformationId)
                .GeneratedBy.HiLo("EOB_HiLo", "NextHi", "1000", "TableKey = '835ServicePaymentInformation'");
            this.Map(x => x.Svc01CompositeMedicalProcedureIdentifier, "SVC01").Nullable();
            this.Map(x => x.Svc02MonetaryAmount, "SVC02").Nullable();
            this.Map(x => x.Svc03MonetaryAmount, "SVC03").Nullable();
            this.Map(x => x.Svc04ProductServiceId, "SVC04").Nullable().Length(48);
            this.Map(x => x.Svc05Quantity, "SVC05").Nullable();
            this.Map(x => x.Svc06CompositeMedicalProcedureIdentifier, "SVC06").Nullable();
            this.Map(x => x.Svc07Quantity, "SVC07").Nullable();

            //this.References(x => x.Ref2LineItemControlNumber).Column("ReferenceInformationID")
            //    .Nullable().NotFound.Ignore()
            //    .Cascade.All()
            //    .Not.LazyLoad()
            //    .Fetch.Join();

            // DTM
            this.HasMany(x => x.DtmServiceDates)
                .KeyColumn("ServicePaymentInformationId")
                .Cascade.All()
                .NotFound.Ignore()
                .Not.LazyLoad();

            // CAS
            this.HasMany(x => x.ClaimAdjustmentPivots)
                .KeyColumn("ServicePaymentInformationId")
                .Cascade.All()
                .NotFound.Ignore()
                .Not.LazyLoad();

            // REF
            //this.HasMany(x => x.Ref1ServiceIdentifications)
            //    .Where("REF01 in ('1S','APC','BB','E9','G1','G3','LU','RB')")
            //    .KeyColumn("ServicePaymentInformationId")
            //    .Cascade.All()
            //    .NotFound.Ignore()
            //    .Not.LazyLoad();

            // REF
            //this.HasMany(x => x.Ref3RenderingProviderIdentifications)
            //    .Where("REF01 in ('0B','1A','1B','1C','1D','1G','1H','1J','D3','G2','HPI','SY','TJ')")
            //    .KeyColumn("ServicePaymentInformationId")
            //    .Cascade.All()
            //    .NotFound.Ignore()
            //    .Not.LazyLoad();

            // REF
            //this.HasMany(x => x.Ref4HealthCarePolicyIdentifications)
            //    .Where("REF01 = '0K'")
            //    .KeyColumn("ServicePaymentInformationId")
            //    .Cascade.All()
            //    .NotFound.Ignore()
            //    .Not.LazyLoad();

            // AMT
            //this.HasMany(x => x.AmtServiceSupplementalAmounts)
            //    .KeyColumn("ServicePaymentInformationId")
            //    .Cascade.All()
            //    .NotFound.Ignore()
            //    .Not.LazyLoad();

            // QTY
            //this.HasMany(x => x.QtyServiceSupplementalQuantities)
            //    .KeyColumn("ServicePaymentInformationId")
            //    .Cascade.All()
            //    .NotFound.Ignore()
            //    .Not.LazyLoad();

            //LQ
            this.HasMany(x => x.LqHealthCareRemarkCodes)
                .KeyColumn("ServicePaymentInformationId")
                .Cascade.All()
                .NotFound.Ignore()
                .Not.LazyLoad();

            // Back reference to its parent
            this.References(x => x.ClaimPaymentInformation)
                .Column("ClaimPaymentInformationId")
                .Not.Insert()
                .Not.Update();

        }

    }
}
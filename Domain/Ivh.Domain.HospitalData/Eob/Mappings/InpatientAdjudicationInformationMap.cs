﻿namespace Ivh.Domain.HospitalData.Eob.Mappings
{
    using FluentNHibernate.Mapping;

    public class InpatientAdjudicationInformationMap : ClassMap<Entities.InpatientAdjudicationInformation>
    {
        public InpatientAdjudicationInformationMap()
        {
            this.Schema("`eob`");
            this.Table("`835InpatientAdjudicationInformation`");
            this.Id(x => x.InpatientAdjudicationInformationId)
                .GeneratedBy.HiLo("EOB_HiLo", "NextHi", "10", "TableKey = '835InpatientAdjudicationInformation'");
            this.Map(x => x.Mia01Quantity, "MIA01").Nullable();
            this.Map(x => x.Mia02MonetaryAmount, "MIA02").Nullable();
            this.Map(x => x.Mia03Quantity, "MIA03").Nullable();
            this.Map(x => x.Mia04MonetaryAmount, "MIA04").Nullable();
            this.Map(x => x.Mia05ReferenceIdentifier, "MIA05").Nullable().Length(50);
            this.Map(x => x.Mia06MonetaryAmount, "MIA06").Nullable();
            this.Map(x => x.Mia07MonetaryAmount, "MIA07").Nullable();
            this.Map(x => x.Mia08MonetaryAmount, "MIA08").Nullable();
            this.Map(x => x.Mia09MonetaryAmount, "MIA09").Nullable();
            this.Map(x => x.Mia10MonetaryAmount, "MIA10").Nullable();
            this.Map(x => x.Mia11MonetaryAmount, "MIA11").Nullable();
            this.Map(x => x.Mia12MonetaryAmount, "MIA12").Nullable();
            this.Map(x => x.Mia13MonetaryAmount, "MIA13").Nullable();
            this.Map(x => x.Mia14MonetaryAmount, "MIA14").Nullable();
            this.Map(x => x.Mia15Quantity, "MIA15").Nullable();
            this.Map(x => x.Mia16MonetaryAmount, "MIA16").Nullable();
            this.Map(x => x.Mia17MonetaryAmount, "MIA17").Nullable();
            this.Map(x => x.Mia18MonetaryAmount, "MIA18").Nullable();
            this.Map(x => x.Mia19MonetaryAmount, "MIA19").Nullable();
            this.Map(x => x.Mia20ReferenceIdentifier, "MIA20").Nullable().Length(50);
            this.Map(x => x.Mia21ReferenceIdentifier, "MIA21").Nullable().Length(50);
            this.Map(x => x.Mia22ReferenceIdentifier, "MIA22").Nullable().Length(50);
            this.Map(x => x.Mia23ReferenceIdentifier, "MIA23").Nullable().Length(50);
            this.Map(x => x.Mia24MonetaryAmount, "MIA24").Nullable();

            this.References(x => x.Mia05EobRemittanceAdviceRemarkCode).Column("MIA05").PropertyRef("RemittanceAdviceRemarkCode")
                .Nullable().NotFound.Ignore()
                .Cascade.All()
                .Not.LazyLoad()
                .Fetch.Join()
                .Not.Insert()
                .Not.Update();
        }

    }
}
﻿namespace Ivh.Domain.HospitalData.Eob.Mappings
{
    using FluentNHibernate.Mapping;

    public class ReferenceInformationMap : ClassMap<Entities.ReferenceInformation>
    {
        public ReferenceInformationMap()
        {
            this.Schema("`eob`");
            this.Table("`835ReferenceInformation`");
            this.Id(x => x.ReferenceInformationId)
                .GeneratedBy.HiLo("EOB_HiLo", "NextHi", "1000", "TableKey = '835ReferenceInformation'");
            this.Map(x => x.Ref01ReferenceIdentificationQualifier, "REF01").Nullable().Length(3);
            this.Map(x => x.Ref02ReferenceIdentification, "REF02").Nullable().Length(50);
        }

    }
}
﻿namespace Ivh.Domain.HospitalData.Eob.Mappings
{
    using FluentNHibernate.Mapping;

    public class TransactionSupplementalStatisticsMap : ClassMap<Entities.TransactionSupplementalStatistics>
    {
        public TransactionSupplementalStatisticsMap()
        {
            this.Schema("`eob`");
            this.Table("`835TransactionSupplementalStatistics`");
            this.Id(x => x.TransactionSupplementalStatisticsId)
                .GeneratedBy.HiLo("EOB_HiLo", "NextHi", "10", "TableKey = '835TransactionSupplementalStatistics'");
            this.Map(x => x.Ts201MonetaryAmount, "TS201").Nullable();
            this.Map(x => x.Ts202MonetaryAmount, "TS202").Nullable();
            this.Map(x => x.Ts203MonetaryAmount, "TS203").Nullable();
            this.Map(x => x.Ts204MonetaryAmount, "TS204").Nullable();
            this.Map(x => x.Ts205MonetaryAmount, "TS205").Nullable();
            this.Map(x => x.Ts206MonetaryAmount, "TS206").Nullable();
            this.Map(x => x.Ts207Quantity, "TS207").Nullable();
            this.Map(x => x.Ts208MonetaryAmount, "TS208").Nullable();
            this.Map(x => x.Ts209MonetaryAmount, "TS209").Nullable();
            this.Map(x => x.Ts210Quantity, "TS210").Nullable();
            this.Map(x => x.Ts211Quantity, "TS211").Nullable();
            this.Map(x => x.Ts212Quantity, "TS212").Nullable();
            this.Map(x => x.Ts213Quantity, "TS213").Nullable();
            this.Map(x => x.Ts214Quantity, "TS214").Nullable();
            this.Map(x => x.Ts215MonetaryAmount, "TS215").Nullable();
            this.Map(x => x.Ts216Quantity, "TS216").Nullable();
            this.Map(x => x.Ts217MonetaryAmount, "TS217").Nullable();
            this.Map(x => x.Ts218MonetaryAmount, "TS218").Nullable();
            this.Map(x => x.Ts219MonetaryAmount, "TS219").Nullable();
        }

    }
}
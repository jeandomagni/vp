﻿namespace Ivh.Domain.HospitalData.Eob.Mappings
{
    using FluentNHibernate.Mapping;

    public class FunctionalGroupHeaderTrailerMap : ClassMap<Entities.FunctionalGroupHeaderTrailer>
    {
        public FunctionalGroupHeaderTrailerMap()
        {
            this.Schema("`eob`");
            this.Table("`835FunctionalGroupHeaderTrailer`");
            this.Id(x => x.FunctionalGroupHeaderTrailerId)
                .GeneratedBy.HiLo("EOB_HiLo", "NextHi", "10", "TableKey = '835FunctionalGroupHeaderTrailer'");
            this.Map(x => x.Gs01FunctionalIdentifierCode, "GS01").Nullable().Length(2);
            this.Map(x => x.Gs02ApplicationSenderCode, "GS02").Nullable().Length(15);
            this.Map(x => x.Gs03ApplicationReceiverCode, "GS03").Nullable().Length(15);
            this.Map(x => x.Gs0405DateTime, "GS0405").Nullable();
            this.Map(x => x.Gs06GroupControlNumber, "GS06").Nullable();
            this.Map(x => x.Gs07AgencyCode, "GS07").Nullable().Length(2);
            this.Map(x => x.Gs08Version, "GS08").Nullable().Length(2);
            this.Map(x => x.Ge01TransactionsCount, "GE01").Nullable();
            this.Map(x => x.Ge02GroupTrailerControlNumber, "GE02").Nullable();

            this.HasMany(x => x.TransactionHeaders)
                .KeyColumn("FunctionalGroupHeaderTrailerId")
                .Cascade.All()
                .Not.KeyNullable()
                .Not.LazyLoad();

            // Back reference to its parent
            this.References(x => x.InterchangeControlHeaderTrailer)
                .Column("InterchangeControlHeaderTrailerId")
                .Not.Insert()
                .Not.Update();

        }
    }
}
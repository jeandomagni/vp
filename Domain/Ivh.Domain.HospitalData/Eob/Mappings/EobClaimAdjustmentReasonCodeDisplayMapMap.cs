﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.HospitalData.Eob.Mappings
{
    using FluentNHibernate.Mapping;

    public class EobClaimAdjustmentReasonCodeDisplayMapMap  : ClassMap<Entities.EobClaimAdjustmentReasonCodeDisplayMap>
    {

        public EobClaimAdjustmentReasonCodeDisplayMapMap()
        {
            this.Schema("`eob`");
            this.Table("`EobClaimAdjustmentReasonCodeDisplayMap`");
            this.Id(x => x.EobClaimAdjustmentReasonCodeDisplayMapId).Not.Nullable();
            this.Map(x => x.ClaimAdjustmentReasonCode).Not.Nullable();
            this.Map(x => x.UniquePayerValue).Nullable();
            this.Map(x => x.ClaimAdjustmentGroupCode).Nullable();
            this.Map(x => x.ClaimStatusCode).Nullable();

            this.References(x => x.EobDisplayCategory)
                .Column("EobDisplayCategoryId")
                .Not.Nullable()
                .Fetch.Join()
                .Not.Insert()
                .Not.Update();
        }

    }
}

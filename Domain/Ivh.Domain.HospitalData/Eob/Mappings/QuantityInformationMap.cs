﻿namespace Ivh.Domain.HospitalData.Eob.Mappings
{
    using FluentNHibernate.Mapping;

    public class QuantityInformationMap : ClassMap<Entities.QuantityInformation>
    {
        public QuantityInformationMap()
        {
            this.Schema("`eob`");
            this.Table("`835QuantityInformation`");
            this.Id(x => x.QuantityInformationId)
                .GeneratedBy.HiLo("EOB_HiLo", "NextHi", "10", "TableKey = '835QuantityInformation'");
            this.Map(x => x.Qty01QuantityQualifier, "QTY01").Nullable().Length(2);
            this.Map(x => x.Qty02Quantity, "QTY02").Nullable();
            this.Map(x => x.Qty03CompositeUnitOfMeasure, "QTY03").Nullable();
            this.Map(x => x.Qty04FreeFormMessage, "QTY04").Nullable().Length(30);
        }

    }
}
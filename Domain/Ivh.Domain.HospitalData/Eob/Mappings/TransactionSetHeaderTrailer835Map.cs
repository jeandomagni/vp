﻿namespace Ivh.Domain.HospitalData.Eob.Mappings
{
    using FluentNHibernate.Mapping;

    public class TransactionSetHeaderTrailer835Map : ClassMap<Entities.TransactionSetHeaderTrailer835>
    {
        public TransactionSetHeaderTrailer835Map()
        {
            this.Schema("`eob`");
            this.Table("`835TransactionSetHeaderTrailer`");
            this.Id(x => x.TransactionSetHeaderTrailerId)
                .GeneratedBy.HiLo("EOB_HiLo", "NextHi", "10", "TableKey = '835TransactionSetHeaderTrailer'");
            this.Map(x => x.St01TransactionSetIdentifierCode, "ST01").Nullable();
            this.Map(x => x.St02TransactionSetControlNumber,"ST02").Nullable().Length(9);
            this.Map(x => x.Bpr01TransactionHandleCode, "BPR01").Nullable().Length(2);
            this.Map(x => x.Bpr02MonetaryAmount, "BPR02").Nullable();
            this.Map(x => x.Bpr03CreditDebitFlagCode, "BPR03").Nullable().Length(1);
            this.Map(x => x.Bpr04PaymentMethodFlagCode, "BPR04").Nullable().Length(3);
            this.Map(x => x.Bpr05PaymentFormatCode, "BPR05").Nullable().Length(10);
            this.Map(x => x.Bpr06EfiIdentificationNumberQualifier, "BPR06").Nullable().Length(2);
            this.Map(x => x.Bpr07DfiIdentificationNumber, "BPR07").Nullable().Length(12);
            this.Map(x => x.Bpr08AccountNumberQualifier, "BPR08").Nullable().Length(3);
            this.Map(x => x.Bpr09AccountNumber, "BPR09").Nullable().Length(35);
            this.Map(x => x.Bpr10OriginatingCompanyIdentifier, "BPR10").Nullable().Length(10);
            this.Map(x => x.Bpr11OriginCompaySupplementalCode, "BPR11").Nullable().Length(9);
            this.Map(x => x.Bpr12DfiIdentificationNumber, "BPR12").Nullable().Length(2);
            this.Map(x => x.Bpr13AccountNumberQualifier, "BPR13").Nullable().Length(12);
            this.Map(x => x.Bpr14AccountNumber, "BPR14").Nullable().Length(3);
            this.Map(x => x.Bpr15OriginatingCompanyIdentifier, "BPR15").Nullable().Length(35);
            this.Map(x => x.Bpr16Date, "BPR16").Nullable();
            this.Map(x => x.Trn01TraceTypeCode, "TRN01").Nullable().Length(2);
            this.Map(x => x.Trn02ReferenceIdentification, "TRN02").Nullable().Length(50);
            this.Map(x => x.Trn03OriginatingCompanyId, "TRN03").Nullable().Length(10);
            this.Map(x => x.Trn04ReferenceIdentification, "TRN04").Nullable().Length(50);
            this.Map(x => x.Cur01EntityIdentifierCode, "CUR01").Nullable().Length(3);
            this.Map(x => x.Cur02CurrencyCode, "CUR02").Nullable();
            this.Map(x => x.Se01MessageSegmentsCount, "SE01").Nullable();
            this.Map(x => x.Se02MessageControlNumber, "SE02").Nullable().Length(9);

            this.References(x => x.EobPayerFilter).Column("EobPayerFilterId")
                .Nullable().NotFound.Ignore()
                .Cascade.All()
                .Not.LazyLoad()
                .Fetch.Join();

            //REF
            //this.References(x => x.Ref1ReceiverIdentification).Column("RefReceiverIdentificationId")
            //    .Nullable().NotFound.Ignore()
            //    .Cascade.All()
            //    .Not.LazyLoad()
            //    .Fetch.Join();

            ////REF
            //this.References(x => x.Ref2VersionIdentification).Column("RefVersionIdentificationId")
            //    .Nullable().NotFound.Ignore()
            //    .Cascade.All()
            //    .Not.LazyLoad()
            //    .Fetch.Join();

            // DTM
            this.References(x => x.DtmProductionDate).Column("DateTimeReferenceId")
                .Nullable().NotFound.Ignore()
                .Cascade.All()
                .Not.LazyLoad()
                .Fetch.Join();

            // Loop 1000A
            this.References(x => x.PayerDetails).Column("PayerIdentificationId")
                .Nullable().NotFound.Ignore()
                .Cascade.All()
                .Not.LazyLoad()
                .Fetch.Join();

            // Loop 1000B
            //this.References(x => x.PayeeDetails).Column("PayeeIdentificationId")
            //    .Nullable().NotFound.Ignore()
            //    .Cascade.All()
            //    .Not.LazyLoad()
            //    .Fetch.Join();

            // Loops 2000 - 2110
            this.HasMany(x => x.Transactions)
                .KeyColumn("TransactionSetHeaderTrailerId")
                .Cascade.All()
                .Not.KeyNullable()
                .Not.LazyLoad();

            // PLB
            //this.HasMany(x => x.PlbProviderAdjustments)
            //    .KeyColumn("TransactionSetHeaderTrailerId")
            //    .Cascade.All()
            //    .NotFound.Ignore()
            //    .Not.LazyLoad();

            // Back reference to its parent
            this.References(x => x.FunctionalGroupHeaderTrailer)
                .Column("FunctionalGroupHeaderTrailerId")
                .Not.Insert()
                .Not.Update();

        }
    }
}
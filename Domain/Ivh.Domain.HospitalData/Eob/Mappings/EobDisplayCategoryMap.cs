﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.HospitalData.Eob.Mappings
{
    using FluentNHibernate.Mapping;

    public class EobDisplayCategoryMap : ClassMap<Entities.EobDisplayCategory>
    {
        public EobDisplayCategoryMap()
        {
            this.Schema("`eob`");
            this.Table("`EobDisplayCategory`");
            this.ReadOnly();
            this.Id(x => x.EobDisplayCategoryId).Not.Nullable();
            this.Map(x => x.DisplayCategoryName).Not.Nullable();            
        }
    }
}

﻿namespace Ivh.Domain.HospitalData.Eob.Mappings
{
    using FluentNHibernate.Mapping;

    public class EobRemittanceAdviceRemarkCodeMap : ClassMap<Entities.EobRemittanceAdviceRemarkCode>
    {
        public EobRemittanceAdviceRemarkCodeMap()
        {
            this.Schema("`eob`");
            this.Table("`EobRemittanceAdviceRemarkCodes`");
            this.Id(x => x.EobRemittanceAdviceRemarkCodeId).Not.Nullable();
            this.Map(x => x.RemittanceAdviceRemarkCode).Not.Nullable();
            this.Map(x => x.Description).Not.Nullable();
            this.Map(x => x.StartDate).Not.Nullable();
            this.Map(x => x.EndDate).Nullable().Not.Update();
        }
    }
}
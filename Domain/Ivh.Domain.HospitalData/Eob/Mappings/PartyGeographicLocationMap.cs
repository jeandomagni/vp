﻿namespace Ivh.Domain.HospitalData.Eob.Mappings
{
    using FluentNHibernate.Mapping;

    public class PartyGeographicLocationMap : ClassMap<Entities.PartyGeographicLocation>
    {
        public PartyGeographicLocationMap()
        {
            this.Schema("`eob`");
            this.Table("`835PartyGeographicLocation`");
            this.Id(x => x.PartyGeographicLocationId)
                .GeneratedBy.HiLo("EOB_HiLo", "NextHi", "10", "TableKey = '835PartyGeographicLocation'");
            this.Map(x => x.N401CityName, "N401").Nullable().Length(30);
            this.Map(x => x.N402StateOrProvinceCode, "N402").Nullable().Length(2);
            this.Map(x => x.N403PostalCode, "N403").Nullable().Length(15);
            this.Map(x => x.N404CountryCode, "N404").Nullable().Length(3);
            this.Map(x => x.N405LocationQualifier, "N405").Nullable().Length(2);
            this.Map(x => x.N406LocationIdentifier, "N406").Nullable().Length(30);
            //this.Map(x => x.N407CountrySubCode, "N407").Nullable().Length(3);
        }
        

    }
}
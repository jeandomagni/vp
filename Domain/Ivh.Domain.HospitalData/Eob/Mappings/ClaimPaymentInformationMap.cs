﻿namespace Ivh.Domain.HospitalData.Eob.Mappings
{
    using FluentNHibernate.Conventions.Inspections;
    using FluentNHibernate.Mapping;

    public class ClaimPaymentInformationMap : ClassMap<Entities.ClaimPaymentInformation>
    {
        public ClaimPaymentInformationMap()
        {
            this.Schema("`eob`");
            this.Table("`835ClaimPaymentInformation`");
            this.Id(x => x.ClaimPaymentInformationId)
                .GeneratedBy.HiLo("EOB_HiLo", "NextHi", "1000", "TableKey = '835ClaimPaymentInformation'");
            this.Map(x => x.Clp01ClaimSubmittersIdentifier, "CLP01").Nullable().Length(38);
            this.Map(x => x.Clp02ClaimStatusCode, "CLP02").Nullable().Length(2);
            this.Map(x => x.Clp03MonetaryAmount, "CLP03").Nullable();
            this.Map(x => x.Clp04MonetaryAmount, "CLP04").Nullable();
            this.Map(x => x.Clp05MonetaryAmount, "CLP05").Nullable();
            this.Map(x => x.Clp06ClaimFileIndicatorCode, "CLP06").Nullable().Length(2);
            this.Map(x => x.Clp07ReferenceIdentifier, "CLP07").Nullable().Length(50);
            this.Map(x => x.Clp08FacilityCode, "CLP08").Nullable().Length(2);
            this.Map(x => x.Clp09ClaimFrequencyTypeCode, "CLP09").Nullable().Length(1);
            this.Map(x => x.Clp11DiagnosisRelatedGroupCode, "CLP11").Nullable().Length(4);
            this.Map(x => x.Clp12Quantity, "CLP12").Nullable();
            this.Map(x => x.Clp13Percent, "CLP13").Nullable();
            this.Map(x => x.BillingSystemId).Nullable();
            this.Map(x => x.VisitSourceSystemKey).Nullable();
            this.Map(x => x.HeaderNumberId).Not.Insert();

            this.References(x => x.Nm11PatientName).Column("PatientNameId")
                .Nullable().NotFound.Ignore()
                .Cascade.All()
                .Not.LazyLoad()
                .Fetch.Join();

            //this.References(x => x.Nm12InsuredName).Column("InsuredNameId")
            //    .Nullable().NotFound.Ignore()
            //    .Cascade.All()
            //    .Not.LazyLoad()
            //    .Fetch.Join();

            //this.References(x => x.Nm13CorrectedPatientInsuredName).Column("CorrectedPatientInsuredNameId")
            //    .Nullable().NotFound.Ignore()
            //    .Cascade.All()
            //    .Not.LazyLoad()
            //    .Fetch.Join();

            //this.References(x => x.Nm14ServiceProviderNameName).Column("ServiceProviderNameId")
            //    .Nullable().NotFound.Ignore()
            //    .Cascade.All()
            //    .Not.LazyLoad()
            //    .Fetch.Join();

            //this.References(x => x.Nm15CrossoverCarrierName).Column("CrossoverCarrierNameId")
            //    .Nullable().NotFound.Ignore()
            //    .Cascade.All()
            //    .Not.LazyLoad()
            //    .Fetch.Join();

            //this.References(x => x.Nm16CorrectedPriorityPayerName).Column("CorrectedPriorityPayerNameId")
            //    .Nullable().NotFound.Ignore()
            //    .Cascade.All()
            //    .Not.LazyLoad()
            //    .Fetch.Join();

            //this.References(x => x.Nm17OtherSubscriberName).Column("OtherSubscriberNameId")
            //    .Nullable().NotFound.Ignore()
            //    .Cascade.All()
            //    .Not.LazyLoad()
            //    .Fetch.Join();

            this.References(x => x.MiaInpatientInformation).Column("InpatientAdjudicationInformationId")
                .Nullable().NotFound.Ignore()
                .Cascade.All()
                .Not.LazyLoad()
                .Fetch.Join();

            this.References(x => x.MoaOutpatientInformation).Column("OutpatientAdjudicationInformationId")
                .Nullable().NotFound.Ignore()
                .Cascade.All()
                .Not.LazyLoad()
                .Fetch.Join();

            //this.References(x => x.Dtm2CoverageExpirationDate).Column("CoverageExpirationDateId")
            //    .Nullable().NotFound.Ignore()
            //    .Cascade.All()
            //    .Not.LazyLoad()
            //    .Fetch.Join();

            //this.References(x => x.Dtm3ClaimReceivedDate).Column("ClaimReceivedDateId")
            //    .Nullable().NotFound.Ignore()
            //    .Cascade.All()
            //    .Not.LazyLoad()
            //    .Fetch.Join();

            //CAS
            this.HasMany(x => x.ClaimAdjustmentPivots)
                .KeyColumn("ClaimPaymentInformationId")
                .Cascade.All()
                .NotFound.Ignore()
                .Not.LazyLoad();

            // REF
            //this.HasMany(x => x.Ref1OtherClaimRelatedIdentifications)
            //    .Where("REF01 in ('1L','1W','28','6P','9A','9C','BB','CE','EA','F8','G1','G3','IG','SY')")
            //    .KeyColumn("ClaimPaymentInformationId")
            //    .Cascade.All()
            //    .NotFound.Ignore()
            //    .Not.LazyLoad();

            // REF
            //this.HasMany(x => x.Ref2RenderingProviderIdentifications)
            //    .Where("REF01 in ('0B','1A','1B','1C','1D','1G','1H','1J','D3','G2','LU')")
            //    .KeyColumn("ClaimPaymentInformationId")
            //    .Cascade.All()
            //    .NotFound.Ignore()
            //    .Not.LazyLoad();

            // DTM
            this.HasMany(x => x.Dtm1StatementFromOrToDates)
                .KeyColumn("ClaimPaymentInformationId")
                .Cascade.All()
                .NotFound.Ignore()
                .Not.LazyLoad();

            //// PER
            //this.HasMany(x => x.PerClaimContactInformations)
            //    .KeyColumn("ClaimPaymentInformationId")
            //    .Cascade.All()
            //    .NotFound.Ignore()
            //    .Not.LazyLoad();

            //// AMT
            //this.HasMany(x => x.AmtClaimSupplementalInformations)
            //    .KeyColumn("ClaimPaymentInformationId")
            //    .Cascade.All()
            //    .NotFound.Ignore()
            //    .Not.LazyLoad();

            // QTY
            //this.HasMany(x => x.QtyClaimSupplementalInformationQuantities)
            //    .KeyColumn("ClaimPaymentInformationId")
            //    .Cascade.All()
            //    .NotFound.Ignore()
            //    .Not.LazyLoad();

            // Loop 2110
            this.HasMany(x => x.ServicePaymentInformations)
                .KeyColumn("ClaimPaymentInformationId")
                .Cascade.All()
                .NotFound.Ignore()
                .Not.LazyLoad();

            // Back reference to its parent
            this.References(x => x.HeaderNumber)
                .Column("HeaderNumberId")
                .Not.Insert()
                .Not.Update();

        }
    }
}
﻿namespace Ivh.Domain.HospitalData.Eob.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class EobPayerFilterExternalLinkMap : ClassMap<EobPayerFilterExternalLink>
    {
        public EobPayerFilterExternalLinkMap()
        {
            this.Schema("`eob`");
            this.Table("`EobPayerFilterExternalLink`");
            this.Id(x => x.EobPayerFilterExternalLinkId).Not.Nullable();
            this.Map(x => x.InsurancePlanId).Nullable();
            
            this.References(x => x.EobPayerFilter)
                .Column("EobPayerFilterId")
                .Fetch.Join()
                .Not.LazyLoad();

            this.References(x => x.ExternalLink)
                .Column("ExternalLinkId")
                .Fetch.Join()
                .Not.LazyLoad();

            this.References(x => x.InsurancePlan)
                .Column("InsurancePlanId")
                .Fetch.Join()
                .Not.LazyLoad()
                .Cascade.None()
                .Not.Update()
                .Not.Insert()
                .NotFound.Ignore();
        }
    }
}
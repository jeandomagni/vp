﻿namespace Ivh.Domain.HospitalData.Eob.Mappings
{
    using FluentNHibernate.Mapping;

    public class InterchangeControlHeaderTrailerMap : ClassMap<Entities.InterchangeControlHeaderTrailer>
    {
        public InterchangeControlHeaderTrailerMap()
        {
            this.Schema("`eob`");
            this.Table("`835InterchangeControlHeaderTrailer`");
            this.Id(x => x.InterchangeControlHeaderTrailerId)
                .GeneratedBy.HiLo("EOB_HiLo", "NextHi", "10", "TableKey = '835InterchangeControlHeaderTrailer'");
            this.Map(x => x.FileTrackerId).Nullable();
            this.Map(x => x.Isa01AuthorizationInformationQualifier, "ISA01").Nullable();
            this.Map(x => x.Isa02AuthorizationInformation, "ISA02").Nullable().Length(10);
            this.Map(x => x.Isa03SecurityInformationQualifier, "ISA03").Nullable();
            this.Map(x => x.Isa04SecurityInformation, "ISA04").Nullable().Length(10);
            this.Map(x => x.Isa05IdQualifier, "ISA05").Nullable();
            this.Map(x => x.Isa06SenderId, "ISA06").Nullable().Length(15);
            this.Map(x => x.Isa07IdQualifier, "ISA07").Nullable();
            this.Map(x => x.Isa08ReceiverId, "ISA08").Nullable();
            this.Map(x => x.Isa0910DateTime, "ISA0910").Nullable();
            this.Map(x => x.Isa11ControlStandardsId, "ISA11").Nullable().Length(1);
            this.Map(x => x.Isa12ControlVersion, "ISA12").Nullable();
            this.Map(x => x.Isa13ControlNumber, "ISA13").Nullable();
            this.Map(x => x.Isa14AcknowledgementRequested, "ISA14").Nullable();
            this.Map(x => x.Isa15UsageIndicator, "ISA15").Nullable().Length(1);
            this.Map(x => x.Isa16ComponentElementSeparator, "ISA16").Nullable();
            this.Map(x => x.Iea01GroupsCount, "IEA01").Nullable();
            this.Map(x => x.Iea02TrailerControlNumber, "IEA02").Nullable();
            this.Map(x => x.MigrationDate, "MigrationDate").Nullable();

            this.HasMany(x => x.FunctionalGroupHeaderTrailerHeaders)
                .KeyColumn("InterchangeControlHeaderTrailerId")
                .Cascade.All()
                .Not.KeyNullable()
                .Not.LazyLoad();

        }

    }
}
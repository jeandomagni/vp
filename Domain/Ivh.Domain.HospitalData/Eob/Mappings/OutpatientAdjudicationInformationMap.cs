﻿namespace Ivh.Domain.HospitalData.Eob.Mappings
{
    using FluentNHibernate.Mapping;

    public class OutpatientAdjudicationInformationMap : ClassMap<Entities.OutpatientAdjudicationInformation>
    {
        public OutpatientAdjudicationInformationMap()
        {
            this.Schema("`eob`");
            this.Table("`835OutpatientAdjudicationInformation`");
            this.Id(x => x.OutpatientAdjudicationInformationId)
                .GeneratedBy.HiLo("EOB_HiLo", "NextHi", "10", "TableKey = '835OutpatientAdjudicationInformation'");
            this.Map(x => x.Moa01Percent, "MOA01").Nullable();
            this.Map(x => x.Moa02MonetaryAmount, "MOA02").Nullable();
            this.Map(x => x.Moa03ReferenceIdentifier, "MOA03").Nullable().Length(50);
            this.Map(x => x.Moa04ReferenceIdentifier, "MOA04").Nullable().Length(50);
            this.Map(x => x.Moa05ReferenceIdentifier, "MOA05").Nullable().Length(50);
            this.Map(x => x.Moa06ReferenceIdentifier, "MOA06").Nullable().Length(50);
            this.Map(x => x.Moa07ReferenceIdentifier, "MOA07").Nullable().Length(50);
            this.Map(x => x.Moa08MonetaryAmount, "MOA08").Nullable();
            this.Map(x => x.Moa09MonetaryAmount, "MOA09").Nullable();

            this.References(x => x.Moa03EobRemittanceAdviceRemarkCode).Column("MOA03").PropertyRef("RemittanceAdviceRemarkCode")
                .Nullable().NotFound.Ignore()
                .Cascade.All()
                .Not.LazyLoad()
                .Fetch.Join()
                .Not.Insert()
                .Not.Update();
        }

    }
}
﻿namespace Ivh.Domain.HospitalData.Eob.Mappings
{
    using FluentNHibernate.Mapping;

    public class PersonInformationMap : ClassMap<Entities.PersonInformation>
    {
        public PersonInformationMap()
        {
            this.Schema("`eob`");
            this.Table("`835PersonInformation`");
            this.Id(x => x.PersonInformationId)
                .GeneratedBy.HiLo("EOB_HiLo", "NextHi", "1000", "TableKey = '835PersonInformation'");
            this.Map(x => x.Per01ContactFunctionCode, "PER01").Nullable().Length(2);
            this.Map(x => x.Per02PersonName, "PER02").Nullable().Length(60);
            this.Map(x => x.Per03CommunicationNumberQualifer, "PER03").Nullable().Length(2);
            this.Map(x => x.Per04CommunicationNumber, "PER04").Nullable();
            this.Map(x => x.Per05CommunicationNumberQualifer, "PER05").Nullable().Length(2);
            this.Map(x => x.Per06CommunicationNumber, "PER06").Nullable();
            this.Map(x => x.Per07CommunicationNumberQualifer, "PER07").Nullable().Length(2);
            this.Map(x => x.Per08CommunicationNumber, "PER08").Nullable();
            this.Map(x => x.Per09ContactInquiryReference, "PER09").Nullable().Length(20);
        }

    }
}
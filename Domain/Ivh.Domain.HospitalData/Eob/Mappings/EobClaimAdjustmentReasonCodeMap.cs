﻿namespace Ivh.Domain.HospitalData.Eob.Mappings
{
    using FluentNHibernate.Mapping;

    public class EobClaimAdjustmentReasonCodeMap : ClassMap<Entities.EobClaimAdjustmentReasonCode>
    {
        public EobClaimAdjustmentReasonCodeMap()
        {
            this.Schema("`eob`");
            this.Table("`EobClaimAdjustmentReasonCodes`");
            this.Id(x => x.EobClaimAdjustmentReasonCodeId).Not.Nullable();
            this.Map(x => x.ClaimAdjustmentReasonCode).Not.Nullable();
            this.Map(x => x.Description).Not.Nullable();
            this.Map(x => x.StartDate).Not.Nullable();
            this.Map(x => x.EndDate).Nullable().Not.Update();
        }
    }
}
﻿namespace Ivh.Domain.HospitalData.Eob.Mappings
{
    using FluentNHibernate.Mapping;

    public class EobClaimsLinkMap : ClassMap<Entities.EobClaimsLink>
    {
        public EobClaimsLinkMap()
        {
            this.Schema("`eob`");
            this.Table("`EobClaimsLink`");
            this.Id(x => x.EobClaimsLinkId).Not.Nullable();
            this.Map(x => x.VisitSourceSystemKey).Not.Nullable();
            this.Map(x => x.ClaimNumber).Not.Nullable();
            this.Map(x => x.BillingSystemId);
        }
    }
}

﻿namespace Ivh.Domain.HospitalData.Eob.Mappings
{
    using FluentNHibernate.Mapping;

    public class TransactionStatisticsMap : ClassMap<Entities.TransactionStatistics>
    {
        public TransactionStatisticsMap()
        {
            this.Schema("`eob`");
            this.Table("`835TransactionStatistics`");
            this.Id(x => x.TransactionStatisticsId)
                .GeneratedBy.HiLo("EOB_HiLo", "NextHi", "10", "TableKey = '835TransactionStatistics'");
            this.Map(x => x.Ts301ReferenceIdentification, "TS301").Nullable().Length(50);
            this.Map(x => x.Ts302FacilityCode, "TS302").Nullable().Length(2);
            this.Map(x => x.Ts303Date, "TS303").Nullable();
            this.Map(x => x.Ts304Quantity, "TS304").Nullable();
            this.Map(x => x.Ts305MonetaryAmount, "TS305").Nullable();
            this.Map(x => x.Ts313MonetaryAmount, "TS313").Nullable();
            this.Map(x => x.Ts315MonetaryAmount, "TS315").Nullable();
            this.Map(x => x.Ts317MonetaryAmount, "TS317").Nullable();
            this.Map(x => x.Ts318MonetaryAmount, "TS318").Nullable();
            this.Map(x => x.Ts320MonetaryAmount, "TS320").Nullable();
            this.Map(x => x.Ts321MonetaryAmount, "TS321").Nullable();
            this.Map(x => x.Ts322MonetaryAmount, "TS322").Nullable();
            this.Map(x => x.Ts323Quantity, "TS323").Nullable();
            this.Map(x => x.Ts324MonetaryAmount, "TS324").Nullable();
        }

    }
}
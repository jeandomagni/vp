﻿namespace Ivh.Domain.HospitalData.Eob.Mappings
{
    using FluentNHibernate.Mapping;

    public class HeaderNumberMap : ClassMap<Entities.HeaderNumber>
    {
        public HeaderNumberMap()
        {
            this.Schema("`eob`");
            this.Table("`835HeaderNumber`");
            this.Id(x => x.HeaderNumberId)
                .GeneratedBy.HiLo("EOB_HiLo", "NextHi", "1000", "TableKey = '835HeaderNumber'");
            this.Map(x => x.Lx01AssignedNumber, "LX01").Not.Nullable().Length(6);

            //this.References(x => x.Ts3ProviderSummaryInformation).Column("TransactionStatisticsId")
            //    .Nullable().NotFound.Ignore()
            //    .Cascade.All()
            //    .Not.LazyLoad()
            //    .Fetch.Join();

            //this.References(x => x.Ts2ProviderSupplementalSummaryInformation).Column("TransactionSupplementalStatisticsId")
            //    .Nullable().NotFound.Ignore()
            //    .Cascade.All()
            //    .Not.LazyLoad()
            //    .Fetch.Join();

            this.HasMany(x => x.ClaimPaymentInformations)
                .KeyColumn("HeaderNumberId")
                .Cascade.All()
                .Not.KeyNullable()
                .Not.LazyLoad();

            // Back reference to its parent
            this.References(x => x.TransactionSetHeaderTrailer)
                .Column("TransactionSetHeaderTrailerId")
                .Not.Insert()
                .Not.Update();

        }
    }
}
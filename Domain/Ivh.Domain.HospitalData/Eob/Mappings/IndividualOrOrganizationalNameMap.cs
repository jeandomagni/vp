﻿namespace Ivh.Domain.HospitalData.Eob.Mappings
{
    using FluentNHibernate.Mapping;

    public class IndividualOrOrganizationalNameMap : ClassMap<Entities.IndividualOrOrganizationalName>
    {
        public IndividualOrOrganizationalNameMap()
        {
            this.Schema("`eob`");
            this.Table("`835IndividualOrOrganizationalName`");
            this.Id(x => x.IndividualOrOrganizationalNameId)
                .GeneratedBy.HiLo("EOB_HiLo", "NextHi", "1000", "TableKey = '835IndividualOrOrganizationalName'");
            this.Map(x => x.Nm101EntityIdentifierCode, "NM101").Nullable().Length(3);
            this.Map(x => x.Nm102EntityTypeQualifier, "NM102").Nullable().Length(1);
            this.Map(x => x.Nm103NameLastOrganizationName, "NM103").Nullable().Length(60);
            this.Map(x => x.Nm104NameFirst, "NM104").Nullable().Length(35);
            this.Map(x => x.Nm105NameMiddle, "NM105").Nullable().Length(25);
            this.Map(x => x.Nm107NameSuffix, "NM107").Nullable().Length(10);
            this.Map(x => x.Nm108IdentificationCodeQualifier, "NM108").Nullable().Length(2);
            this.Map(x => x.Nm109IdentificationCode, "NM109").Nullable().Length(80);
        }

    }
}
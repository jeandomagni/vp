﻿namespace Ivh.Domain.HospitalData.Eob.Mappings
{
    using AutoMapper;
    using Entities;
    using CommonEob = Ivh.Application.Core.Common.Models.Eob;
    using AppEob = Domain.Eob.Entities;
    using MessageEob = Common.VisitPay.Messages.Eob;
    using HsEob = Entities;

    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            // Automapper mapping go here.
            // Use this.CreateMap<,>().
            // DO NOT USE Mapper.CreateMap<,>().
            this.CreateMap<InterchangeControlHeaderTrailer, InterchangeControlHeaderTrailer>()
                .AfterMap((s, d) =>
                {
                    foreach (FunctionalGroupHeaderTrailer c in d.FunctionalGroupHeaderTrailerHeaders)
                    {
                        c.InterchangeControlHeaderTrailer = d;
                    }
                });

            this.CreateMap<FunctionalGroupHeaderTrailer, FunctionalGroupHeaderTrailer>()
                .AfterMap((s, d) =>
                {
                    foreach (TransactionSetHeaderTrailer835 c in d.TransactionHeaders)
                    {
                        c.FunctionalGroupHeaderTrailer = d;
                    }
                });

            this.CreateMap<TransactionSetHeaderTrailer835, TransactionSetHeaderTrailer835>()
                .AfterMap((s, d) =>
                {
                    foreach (HeaderNumber c in d.Transactions)
                    {
                        c.TransactionSetHeaderTrailer = d;
                    }
                });

            this.CreateMap<HeaderNumber, HeaderNumber>()
                .AfterMap((s, d) =>
                {
                    foreach (ClaimPaymentInformation c in d.ClaimPaymentInformations)
                    {
                        c.HeaderNumber = d;
                    }
                });

            this.CreateMap<ClaimPaymentInformation, ClaimPaymentInformation>()
                .AfterMap((s, d) =>
                {
                    foreach (ServicePaymentInformation c in d.ServicePaymentInformations)
                    {
                        c.ClaimPaymentInformation = d;
                    }

                    foreach (ClaimAdjustmentPivot c in d.ClaimAdjustmentPivots)
                    {
                        c.ClaimPaymentInformation = d;
                    }

                    foreach (DateTimeReference c in d.Dtm1StatementFromOrToDates)
                    {
                        c.ClaimPaymentInformation = d;
                    }
                });

            this.CreateMap<ServicePaymentInformation, ServicePaymentInformation>()
                .AfterMap((s, d) =>
                {
                    foreach (IndustryCodeIdentification c in d.LqHealthCareRemarkCodes)
                    {
                        c.ServicePaymentInformation = d;
                    }

                    foreach (ClaimAdjustmentPivot c in d.ClaimAdjustmentPivots)
                    {
                        c.ServicePaymentInformation = d;
                    }

                    foreach (DateTimeReference c in d.DtmServiceDates)
                    {
                        c.ServicePaymentInformation = d;
                    }
                });

            //HsEOB to AppEOB

            this.CreateMap<HsEob.ClaimAdjustment, CommonEob.ClaimAdjustment>();
            this.CreateMap<HsEob.ClaimAdjustmentPivot, CommonEob.ClaimAdjustmentPivot>();
            this.CreateMap<HsEob.ClaimPaymentInformation, CommonEob.ClaimPaymentInformation>();
            this.CreateMap<HsEob.DateTimeReference, CommonEob.DateTimeReference>();
            this.CreateMap<HsEob.EobClaimAdjustmentReasonCode, CommonEob.EobClaimAdjustmentReasonCode>();
            this.CreateMap<HsEob.EobRemittanceAdviceRemarkCode, CommonEob.EobRemittanceAdviceRemarkCode>();
            this.CreateMap<HsEob.FunctionalGroupHeaderTrailer, CommonEob.FunctionalGroupHeaderTrailer>();
            this.CreateMap<HsEob.HeaderNumber, CommonEob.HeaderNumber>();
            this.CreateMap<HsEob.IndividualOrOrganizationalName, CommonEob.IndividualOrOrganizationalName>();
            this.CreateMap<HsEob.IndustryCodeIdentification, CommonEob.IndustryCodeIdentification>();
            this.CreateMap<HsEob.InpatientAdjudicationInformation, CommonEob.InpatientAdjudicationInformation>();
            this.CreateMap<HsEob.InterchangeControlHeaderTrailer, CommonEob.InterchangeControlHeaderTrailer>();
            this.CreateMap<HsEob.OutpatientAdjudicationInformation, CommonEob.OutpatientAdjudicationInformation>();
            this.CreateMap<HsEob.PayeeIdentification, CommonEob.PayeeIdentification>();
            this.CreateMap<HsEob.PayerIdentification, CommonEob.PayerIdentification>();
            this.CreateMap<HsEob.PersonInformation, CommonEob.PersonInformation>();
            this.CreateMap<HsEob.ServicePaymentInformation, CommonEob.ServicePaymentInformation>();
            this.CreateMap<HsEob.TransactionSetHeaderTrailer835, CommonEob.TransactionSetHeaderTrailer835>();
            this.CreateMap<AppEob.ClaimPaymentInformationHeader, MessageEob.EobClaimPaymentMessage>();
            this.CreateMap<HsEob.EobPayerFilter, MessageEob.EobPayerFilterMessage>();
            this.CreateMap<HsEob.EobPayerFilterExternalLink, MessageEob.EobPayerFilterEobExternalLinkMessage>()
                .ForMember(dest => dest.EobPayerFilterId, opts => opts.MapFrom(src => src.EobPayerFilter.EobPayerFilterId))
                .ForMember(dest => dest.EobExternalLinkId, opts => opts.MapFrom(src => src.ExternalLink.ExternalLinkId))
                .ForMember(dest => dest.EobPayerFilterEobExternalLinkId, opts => opts.MapFrom(src => src.EobPayerFilterExternalLinkId))
                .ForMember(dest => dest.InsurancePlanBillingSystemId, opts => opts.MapFrom(src => src.InsurancePlan == null ? (int?)null : src.InsurancePlan.BillingSystemId))
                .ForMember(dest => dest.InsurancePlanSourceSystemKey, opts => opts.MapFrom(src => src.InsurancePlan == null ? null : src.InsurancePlan.SourceSystemKey));
            this.CreateMap<HsEob.ExternalLink, MessageEob.EobExternalLinkMessage>()
                .ForMember(dest => dest.EobExternalLinkId, opts => opts.MapFrom(src => src.ExternalLinkId));
            this.CreateMap<HsEob.EobDisplayCategory, MessageEob.EobDisplayCategoryMessage>();
            this.CreateMap<HsEob.EobClaimAdjustmentReasonCode, MessageEob.EobClaimAdjustmentReasonCodeMessage>();
            this.CreateMap<HsEob.EobClaimAdjustmentReasonCodeDisplayMap, MessageEob.EobClaimAdjustmentReasonCodeDisplayMapMessage>()
                .ForMember(dest => dest.EobDisplayCategoryId, opts => opts.MapFrom(src => src.EobDisplayCategory.EobDisplayCategoryId));
        }
    }
}

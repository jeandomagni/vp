﻿namespace Ivh.Domain.HospitalData.Eob.Mappings
{
    using FluentNHibernate.Mapping;

    public class PayeeIdentificationMap : ClassMap<Entities.PayeeIdentification>
    {
        public PayeeIdentificationMap()
        {
            this.Schema("`eob`");
            this.Table("`835PayeeIdentification`");
            this.Id(x => x.PayeeIdentificationId)
                .GeneratedBy.HiLo("EOB_HiLo", "NextHi", "10", "TableKey = '835PayeeIdentification'");
            this.Map(x => x.N101EntityIdentifierCode, "N101").Nullable().Length(3);
            this.Map(x => x.N102PayeeName, "N102").Nullable().Length(60);
            this.Map(x => x.N103IdentificationCodeQualifier, "N103").Nullable().Length(2);
            this.Map(x => x.N104IdentificationCode, "N104").Nullable().Length(80);
            this.Map(x => x.N105EntityRelationshipCode, "N105").Nullable().Length(2);
            this.Map(x => x.N106EntityIdentifierCode, "N106").Nullable().Length(3);
            this.Map(x => x.Rdm01ReportTransmissionCode, "RDM01").Nullable().Length(2);
            this.Map(x => x.Rdm02ThirdPartyProcessorName, "RDM02").Nullable().Length(60);
            this.Map(x => x.Rdm03CommunicationNumber, "RDM03").Nullable();

            // N3
            //this.References(x => x.N3PayeeAddress).Column("PartyLocationId")
            //    .Nullable().NotFound.Ignore()
            //    .Cascade.All()
            //    .Not.LazyLoad()
            //    .Fetch.Join();

            // N4
            //this.References(x => x.N4PayeeCityStateZip).Column("PartyGeographicLocationId")
            //    .Nullable().NotFound.Ignore()
            //    .Cascade.All()
            //    .Not.LazyLoad()
            //    .Fetch.Join();

            // REF
            //this.HasMany(x => x.RefAdditionalPayeeIdentification)
            //    .KeyColumn("PayeeIdentificationId")
            //    .Cascade.All()
            //    .NotFound.Ignore()
            //    .Not.LazyLoad();
        }

    }
}
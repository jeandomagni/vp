﻿namespace Ivh.Domain.HospitalData.Eob.Mappings
{
    using FluentNHibernate.Mapping;

    public class MonetaryAmountInformationMap : ClassMap<Entities.MonetaryAmountInformation>
    {
        public MonetaryAmountInformationMap()
        {
            this.Schema("`eob`");
            this.Table("`835MonetaryAmountInformation`");
            this.Id(x => x.MonetaryAmountInformationId)
                .GeneratedBy.HiLo("EOB_HiLo", "NextHi", "1000", "TableKey = '835MonetaryAmountInformation'");
            this.Map(x => x.Amt01AmountQualifierCode, "AMT01").Nullable().Length(3);
            this.Map(x => x.Amt02MonetaryAmount, "AMT02").Nullable();
        }

    }
}
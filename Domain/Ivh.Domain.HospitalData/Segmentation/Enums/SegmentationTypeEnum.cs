﻿namespace Ivh.Domain.HospitalData.Segmentation.Enums
{
    public enum SegmentationTypeEnum
    {
        BadDebt = 1,
        AccountsReceivable = 2,
        ActivePassive = 3,
        PresumptiveCharity = 4
    }
}
﻿namespace Ivh.Domain.HospitalData.Segmentation.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Entities;
    using Enums;
    using Interfaces;

    public class GuarantorSegmentationDailyService : DomainService, IGuarantorSegmentationDailyService
    {
        private readonly Lazy<IGuarantorSegmentationDailyRepository> _guarantorSegmentationDailyRepository;
        private readonly Lazy<IMatchedGuarantorsRepository> _matchedGuarantorsRepository;

        public GuarantorSegmentationDailyService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IGuarantorSegmentationDailyRepository> guarantorSegmentationDailyRepository,
            Lazy<IMatchedGuarantorsRepository> matchedGuarantorsRepository) : base(serviceCommonService)
        {
            this._guarantorSegmentationDailyRepository = guarantorSegmentationDailyRepository;
            this._matchedGuarantorsRepository = matchedGuarantorsRepository;
        }

        public void SaveBulkGuarantorSegmentationDaily(List<GuarantorSegmentationDaily> guarantorSegmentationDaily)
        {
            this._guarantorSegmentationDailyRepository.Value.BulkInsert(guarantorSegmentationDaily);
        }

        public void UpdateAccountsReceivableGuarantorSegmentationHistory(int segmentationBatchId)
        {
            this._guarantorSegmentationDailyRepository.Value.UpdateAccountsReceivableGuarantorSegmentationHistory(segmentationBatchId);
        }

        public void UpdateActivePassiveGuarantorSegmentationHistory(int segmentationBatchId)
        {
            this._guarantorSegmentationDailyRepository.Value.UpdateActivePassiveGuarantorSegmentationHistory(segmentationBatchId);
        }
        public void UpdateBadDebtGuarantorSegmentationHistory(int segmentationBatchId)
        {
            this._guarantorSegmentationDailyRepository.Value.UpdateBadDebtGuarantorSegmentationHistory(segmentationBatchId);
        }
    }
}
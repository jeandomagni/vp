﻿namespace Ivh.Domain.HospitalData.Segmentation.Services
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using AutoMapper;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using Entities;
    using Interfaces;
    using Ivh.Application.HospitalData.Common.Interfaces;
    using Newtonsoft.Json;
    using Scoring.Interfaces;

    public class SegmentationService : ISegmentationProcessService
    {
        private readonly Lazy<IAccountsReceivableSegmentationThresholdRepository> _accountsReceivableSegmentationThresholdRepository;
        private readonly Lazy<IActivePassiveSegmentationThresholdRepository> _activePassiveSegmentationThresholdRepository;
        private readonly Lazy<IRscriptEngine> _rscriptEngine;
        private readonly Lazy<IScoringService> _scoringService;

        public SegmentationService(
            Lazy<IAccountsReceivableSegmentationThresholdRepository> accountsReceivableSegmentationThresholdRepository,
            Lazy<IActivePassiveSegmentationThresholdRepository> activePassiveSegmentationThresholdRepository,
            Lazy<IRscriptEngine> rscriptEngine,
            Lazy<IScoringService> scoringService)
        {
            this._accountsReceivableSegmentationThresholdRepository = accountsReceivableSegmentationThresholdRepository;
            this._activePassiveSegmentationThresholdRepository = activePassiveSegmentationThresholdRepository;
            this._rscriptEngine = rscriptEngine;
            this._scoringService = scoringService;
        }

        // Active Passive
        public IList<SegmentationActivePassiveDto> ExecuteActivePassiveSegmentation(List<SegmentationActivePassiveDto> activePassiveSegmentationDtos, ScoringScriptStorage scriptStorage)
        {
            // Calculate segmentation vars from rawInput
            Stopwatch sw = this._scoringService.Value.LogInfo(() => "SegmentationService::ExecuteActivePassiveSegmentation CalculateSegmentationInput Start", true);
            SegmentationServiceActivePassive calculatedInputs = this.CalculateSegmentationInput(activePassiveSegmentationDtos);
            this._scoringService.Value.LogInfoEndTimer(() => "SegmentationService::ExecuteActivePassiveSegmentation CalculateSegmentationInput End", sw);

            this._scoringService.Value.LogInfo(() => "Start Active Passive execute segmentation rscript");
            sw  = this._scoringService.Value.LogInfo(() => "SegmentationService::ExecuteActivePassiveSegmentation ExecuteRScript Start", true);
            string segmentationResult = this._rscriptEngine.Value.ExecuteRScript(JsonConvert.SerializeObject(calculatedInputs), scriptStorage);
            this._scoringService.Value.LogInfoEndTimer(() => "SegmentationService::ExecuteActivePassiveSegmentation ExecuteRScript End", sw);
            IList<SegmentationActivePassiveDto> segmentationVisits = JsonConvert.DeserializeObject<List<SegmentationActivePassiveDto>>(segmentationResult);

            return segmentationVisits;
        }

        public SegmentationServiceActivePassive CalculateSegmentationInput(IList<SegmentationActivePassiveDto> activePassiveVisitRawInput)
        {
            SegmentationServiceActivePassive activePassiveInput = new SegmentationServiceActivePassive();
            ActivePassiveSegmentationThreshold threshold = this._activePassiveSegmentationThresholdRepository.Value.GetQueryable().First();
            activePassiveInput.ActivePassiveSegmentationThreshold = threshold;

            activePassiveInput.SegmentationServiceActivePassiveData = Mapper.Map<List<SegmentationServiceActivePassiveData>>(activePassiveVisitRawInput); ;
            return activePassiveInput;
        }

        // Accounts Receivable
        public IList<SegmentationAccountsReceivableDto> ExecuteAccountsReceivableSegmentation(List<SegmentationAccountsReceivableDto> accountReceivableSegmentationDtos, ScoringScriptStorage scriptStorage)
        {
            Stopwatch sw = this._scoringService.Value.LogInfo(() => "SegmentationService::ExecuteAccountsReceivableSegmentation CalculateSegmentationInput Start", true);
            // Calculate segmentation vars from rawInput
            SegmentationServiceAccountsReceivable calculatedInputs = this.CalculateSegmentationInput(accountReceivableSegmentationDtos);
            this._scoringService.Value.LogInfoEndTimer(() => "SegmentationService::ExecuteAccountsReceivableSegmentation CalculateSegmentationInput End", sw);

            // Run script to generate segmentation raw score
            sw = this._scoringService.Value.LogInfo(() => "SegmentationService::ExecuteAccountsReceivableSegmentation ExecuteRScript Start", true);
            string segmentationResult = this._rscriptEngine.Value.ExecuteRScript(JsonConvert.SerializeObject(calculatedInputs), scriptStorage);
            this._scoringService.Value.LogInfoEndTimer(() => "SegmentationService::ExecuteAccountsReceivableSegmentation ExecuteRScript End", sw);
            IList<SegmentationAccountsReceivableDto> segmentationVisits = JsonConvert.DeserializeObject<List<SegmentationAccountsReceivableDto>>(segmentationResult);

            return segmentationVisits;
        }

        public SegmentationServiceAccountsReceivable CalculateSegmentationInput(IList<SegmentationAccountsReceivableDto> accountsReceivableVisitRawInput)
        {
            SegmentationServiceAccountsReceivable accountsReceivableInput = new SegmentationServiceAccountsReceivable();
            List<AccountsReceivableSegmentationThreshold> thresholds = this._accountsReceivableSegmentationThresholdRepository.Value.GetQueryable().ToList();
            accountsReceivableInput.AccountsReceivableSegmentationThresholds = thresholds;

            accountsReceivableInput.SegmentationServiceAccountsReceivableData = Mapper.Map<List<SegmentationServiceData>>(accountsReceivableVisitRawInput);
            return accountsReceivableInput;
        }

        // Bad Debt
        public IList<SegmentationBadDebtDto> ExecuteBadDebtSegmentation(List<SegmentationBadDebtDto> segmentationBadDebtDtos, ScoringScriptStorage scriptStorage)
        {
            // Calculate segmentation vars from rawInput
            Stopwatch sw = this._scoringService.Value.LogInfo(() => "SegmentationService::ExecuteBadDebtSegmentation CalculateSegmentationInput Start", true);
            IList<SegmentationServiceData> calculatedInputs = Mapper.Map<List<SegmentationServiceData>>(segmentationBadDebtDtos);
            this._scoringService.Value.LogInfoEndTimer(() => "SegmentationService::ExecuteBadDebtSegmentation CalculateSegmentationInput End", sw);

            // Run script to generate segmentation raw score
            sw = this._scoringService.Value.LogInfo(() => "SegmentationService::ExecuteBadDebtSegmentation ExecuteRScript Start", true);
            string segmentationResult = this._rscriptEngine.Value.ExecuteRScript(JsonConvert.SerializeObject(calculatedInputs), scriptStorage);
            this._scoringService.Value.LogInfoEndTimer(() => "SegmentationService::ExecuteBadDebtSegmentation ExecuteRScript End", sw);
            IList<SegmentationBadDebtDto> segmentationVisits = JsonConvert.DeserializeObject<List<SegmentationBadDebtDto>>(segmentationResult);

            return segmentationVisits;
        }

    }
}
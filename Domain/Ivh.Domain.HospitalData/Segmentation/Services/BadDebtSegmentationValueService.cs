﻿namespace Ivh.Domain.HospitalData.Segmentation.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Entities;
    using Interfaces;

    public class BadDebtSegmentationValueService : DomainService, IBadDebtSegmentationValueService
    {
        private readonly Lazy<IBadDebtSegmentationValueRepository> _badDebtSegmentationValueRepository;

        public BadDebtSegmentationValueService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IBadDebtSegmentationValueRepository> badDebtSegmentationValueRepository) : base(serviceCommonService)
        {
            this._badDebtSegmentationValueRepository = badDebtSegmentationValueRepository;
        }

        public IList<BadDebtSegmentationValue> GetAll()
        {
            return this._badDebtSegmentationValueRepository.Value.GetQueryable().ToList();
        }

    }
}
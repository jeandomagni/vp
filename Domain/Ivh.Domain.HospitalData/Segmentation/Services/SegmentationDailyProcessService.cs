﻿namespace Ivh.Domain.HospitalData.Segmentation.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Utilities.Extensions;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using Entities;
    using Enums;
    using Interfaces;
    using Logging.Interfaces;
    using NHibernate;
    using Scoring.Entities;
    using Scoring.Interfaces;
    using Settings.Interfaces;
    using Visit.Entities;

    public class SegmentationDailyProcessService : DomainService, ISegmentationDailyProcessService
    {
        private readonly Lazy<IAccountsReceivableVisitDailyRepository> _accountsReceivableVisitDailyRepository;
        private readonly Lazy<IActivePassiveVisitDailyRepository> _activePassiveVisitDailyRepository;
        private readonly Lazy<IBadDebtVisitDailyRepository> _badDebtVisitDailyRepository;
        private readonly Lazy<IClientService> _clientService;
        private readonly Lazy<IPresumptiveCharityEvaluationProvider> _presumptiveCharityEvaluationProvider;
        private readonly Lazy<IPresumptiveCharityGuarantorVisitDailyRepository> _presumptiveCharityGuarantorVisitDailyRepository;
        private readonly Lazy<IPresumptiveCharityRepository> _presumptiveCharityRepository;
        private readonly Lazy<ISegmentationBatchRepository> _segmentationBatchRepository;
        private readonly Lazy<IThirdPartyDataService> _thirdPartyDataService;
        private readonly Lazy<IMetricsProvider> _metricsProvider;
        private readonly Lazy<IGuarantorBatchRepository> _guarantorBatchRepository;
        private readonly ISession _session;

        public SegmentationDailyProcessService(
            Lazy<IAccountsReceivableVisitDailyRepository> accountsReceivableVisitDailyRepository,
            Lazy<IActivePassiveVisitDailyRepository> activePassiveVisitDailyRepository,
            Lazy<IBadDebtVisitDailyRepository> badDebtVisitDailyRepository,
            Lazy<IClientService> clientService, 
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IPresumptiveCharityEvaluationProvider> presumptiveCharityEvaluationProvider,
            Lazy<IPresumptiveCharityGuarantorVisitDailyRepository> presumptiveCharityGuarantorVisitDailyRepository,
            Lazy<IPresumptiveCharityRepository> presumptiveCharityRepository,
            Lazy<ISegmentationBatchRepository> segmentationBatchRepository,
            Lazy<IThirdPartyDataService> thirdPartyDataService,
            Lazy<IMetricsProvider> metricsProvider,
            Lazy<IGuarantorBatchRepository> guarantorBatchRepository,
            ISessionContext<CdiEtl> sessionContext) : base(serviceCommonService)
        {
            this._accountsReceivableVisitDailyRepository = accountsReceivableVisitDailyRepository;
            this._activePassiveVisitDailyRepository = activePassiveVisitDailyRepository;
            this._badDebtVisitDailyRepository = badDebtVisitDailyRepository;
            this._clientService = clientService;
            this._presumptiveCharityEvaluationProvider = presumptiveCharityEvaluationProvider;
            this._presumptiveCharityGuarantorVisitDailyRepository = presumptiveCharityGuarantorVisitDailyRepository;
            this._presumptiveCharityRepository = presumptiveCharityRepository;
            this._segmentationBatchRepository = segmentationBatchRepository;
            this._thirdPartyDataService = thirdPartyDataService;
            this._metricsProvider = metricsProvider;
            this._guarantorBatchRepository = guarantorBatchRepository;
            this._session = sessionContext.Session;
        }

        public int GenerateSegmentationBatchId(SegmentationTypeEnum segmentationType)
        {
            return this._segmentationBatchRepository.Value.GenerateSegmentationBatchId(segmentationType);
        }

        public void PresumptiveCharitySegmentationGuarantorVisitDaily(int segmentationBatchId)
        {
            this._presumptiveCharityRepository.Value.ClearPresumptiveCharityGuarantorVisitDaily();

            int batchSize = this.ApplicationSettingsService.Value.SegmentationBatchLoadBatchSize.Value;
            DateTime minSelfPayDate = this._clientService.Value.GetClient().SegmentationPresumptiveCharityMinSelfPayDate;

            int lastVisitId = 0;
            IList<PresumptiveCharityEligibleVisit> potentialVisits;
            do
            {
                potentialVisits = this._presumptiveCharityRepository.Value.GetBasePcEligibleVisits(minSelfPayDate, batchSize, lastVisitId);
                IList<PresumptiveCharityGuarantorVisitDaily> eligibleVisits = new List<PresumptiveCharityGuarantorVisitDaily>();

                if (potentialVisits.IsNotNullOrEmpty())
                {
                    lastVisitId = potentialVisits.Max(x => x.VisitId);
                    eligibleVisits = this._presumptiveCharityEvaluationProvider.Value.GetEligibleVisits(potentialVisits, segmentationBatchId);
                    if (eligibleVisits.IsNotNullOrEmpty())
                    {
                        this._presumptiveCharityGuarantorVisitDailyRepository.Value.BulkInsert(eligibleVisits);
                    }
                }
                this._session.Evict(eligibleVisits);
            } while (potentialVisits.IsNotNullOrEmpty());
        }

        public void ExecuteBadDebtSegmentation(int segmentationBatchId)
        {
            this._badDebtVisitDailyRepository.Value.ExecuteBadDebtSegmentation(segmentationBatchId);
        }



        public void ExecuteAccountsReceivableSegmentation(int segmentationBatchId)
        {
            this._accountsReceivableVisitDailyRepository.Value.ExecuteAccountsReceivableSegmentation(segmentationBatchId);
        }

        public void ExecutePresumptiveCharitySegmentation(int segmentationBatchId)
        {
            this.PresumptiveCharitySegmentationGuarantorVisitDaily(segmentationBatchId);
            IList<int> guarantors = this._guarantorBatchRepository.Value.GetPresumptiveCharityGuarantorBatches(this._clientService.Value.GetClient().ThirdPartyRefreshAge, this._clientService.Value.GetClient().SegmentationPCEvaluateNoPtpScoreVisits);
            foreach (int guarantor in guarantors)
            {
                this._metricsProvider.Value.Increment(Metrics.Increment.Scoring.ThirdPartyDataExpected);
                GuarantorBatch guarantorBatch = this._guarantorBatchRepository.Value.GetById(guarantor);
                this._session.Evict(guarantorBatch);
                this._thirdPartyDataService.Value.ExtractThirdParty(guarantorBatch, true, (ScoringThirdPartyDatasetEnum) this._clientService.Value.GetClient().PcEvaluationThirdPartyDataSet, ScoringThirdPartyDataSetTypeEnum.Segmentation, false);
            }

            bool evaluateVisitsWithoutPtpScores = this._clientService.Value.GetClient().SegmentationPCEvaluateNoPtpScoreVisits;
            this._presumptiveCharityRepository.Value.ExecutePresumptiveCharitySegmentation(evaluateVisitsWithoutPtpScores);
        }

        public void UpdatePresumptiveCharityHistory(int segmentationBatchId)
        {
            this._presumptiveCharityRepository.Value.ExecutePresumptiveCharityHistoryUpdate(segmentationBatchId);
        }

        public void ExecuteActivePassiveSegmentation(int segmentationBatchId)
        {
            this._activePassiveVisitDailyRepository.Value.ExecuteActivePassiveSegmentation(segmentationBatchId);
        }

        public IList<SegmentationAccountsReceivableDto> ProcessAccountsReceivableVisitDaily(int segmentationBatchId, int lastHsGuarantorId)
        {
            int batchSize = this.ApplicationSettingsService.Value.SegmentationBatchLoadBatchSize.Value;
            IList<AccountsReceivableVisitDaily> accountsReceivableVisits = this._accountsReceivableVisitDailyRepository.Value.GetDailyAccountsReceivableVisits(segmentationBatchId, batchSize, lastHsGuarantorId);
            return Mapper.Map<List<SegmentationAccountsReceivableDto>>(accountsReceivableVisits);
        }


        public IList<SegmentationBadDebtDto> ProcessBadDebtVisitDaily(int segmentationBatchId, int lastHsGuarantorId)
        {
            int batchSize = this.ApplicationSettingsService.Value.SegmentationBatchLoadBatchSize.Value;
            IList<BadDebtVisitDaily> dailyBadDebtVisits = this._badDebtVisitDailyRepository.Value.GetDailyBadDebtVisits(segmentationBatchId, batchSize, lastHsGuarantorId);
            return Mapper.Map<List<SegmentationBadDebtDto>>(dailyBadDebtVisits);
        }


        public IList<SegmentationActivePassiveDto> ProcessActivePassiveVisitDaily(int segmentationBatchId, int lastHsGuarantorId)
        {
            int batchSize = this.ApplicationSettingsService.Value.SegmentationBatchLoadBatchSize.Value;
            IList<ActivePassiveVisitDaily> dailyActivePassiveVisits = this._activePassiveVisitDailyRepository.Value.GetDailyActivePassiveVisits(segmentationBatchId, batchSize, lastHsGuarantorId);
            return Mapper.Map<List<SegmentationActivePassiveDto>>(dailyActivePassiveVisits);
        }

    }
}
﻿namespace Ivh.Domain.HospitalData.Segmentation.Interfaces
{
    using System.Collections.Generic;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using Entities;
    using Enums;

    public interface ISegmentationDailyProcessService
    {
        void ExecuteAccountsReceivableSegmentation(int segmentationBatchId);
        void ExecuteBadDebtSegmentation(int segmentationBatchId);
        void ExecutePresumptiveCharitySegmentation(int segmentationBatchId);
        void ExecuteActivePassiveSegmentation(int segmentationBatchId);
        int GenerateSegmentationBatchId(SegmentationTypeEnum segmentationType);
        void PresumptiveCharitySegmentationGuarantorVisitDaily(int segmentationBatchId);
        void UpdatePresumptiveCharityHistory(int segmentationBatchId);
        IList<SegmentationBadDebtDto> ProcessBadDebtVisitDaily(int segmentationBatchId, int lastHsGuarantorId);
        IList<SegmentationAccountsReceivableDto> ProcessAccountsReceivableVisitDaily(int segmentationBatchId, int lastHsGuarantorId);
        IList<SegmentationActivePassiveDto> ProcessActivePassiveVisitDaily(int segmentationBatchId, int lastHsGuarantorId);
    }
}

﻿namespace Ivh.Domain.HospitalData.Segmentation.Interfaces
{
    using System.Collections.Generic;
    using Entities;
    using Enums;

    public interface IGuarantorSegmentationDailyService
    {
        void SaveBulkGuarantorSegmentationDaily(List<GuarantorSegmentationDaily> guarantorSegmentationDaily);
        void UpdateAccountsReceivableGuarantorSegmentationHistory(int segmentationBatchId);
        void UpdateActivePassiveGuarantorSegmentationHistory(int segmentationBatchId);
        void UpdateBadDebtGuarantorSegmentationHistory(int segmentationBatchId);
    }
}
﻿namespace Ivh.Domain.HospitalData.Segmentation.Interfaces
{
    using System.Collections.Generic;
    using Common.VisitPay.Messages.HospitalData.Dtos;

    public interface ISegmentationProcessService
    {
        IList<SegmentationActivePassiveDto> ExecuteActivePassiveSegmentation(List<SegmentationActivePassiveDto> activePassiveSegmentationDtos, ScoringScriptStorage scriptStorage);
        IList<SegmentationAccountsReceivableDto> ExecuteAccountsReceivableSegmentation(List<SegmentationAccountsReceivableDto> accountReceivableSegmentationDtos, ScoringScriptStorage scriptStorage);
        IList<SegmentationBadDebtDto> ExecuteBadDebtSegmentation(List<SegmentationBadDebtDto> segmentationBadDebtDtos, ScoringScriptStorage scriptStorage);
    }
}

﻿namespace Ivh.Domain.HospitalData.Segmentation.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IActivePassiveSegmentationThresholdRepository : IRepository<ActivePassiveSegmentationThreshold>
    {
    }
}

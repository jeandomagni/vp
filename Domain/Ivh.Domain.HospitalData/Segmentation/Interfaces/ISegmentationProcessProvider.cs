﻿namespace Ivh.Domain.HospitalData.Segmentation.Interfaces
{
    public interface ISegmentationProcessProvider
    {
        string CallSegmentationFunctionService(object scoringServiceInput, string baseUrl, string functionName, string authorizationCode, string appId, string appKey, string requestingAppName);
    }
}

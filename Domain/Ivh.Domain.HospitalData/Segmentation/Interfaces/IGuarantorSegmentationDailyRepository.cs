﻿namespace Ivh.Domain.HospitalData.Segmentation.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IGuarantorSegmentationDailyRepository : IRepository<GuarantorSegmentationDaily>
    {
        void UpdateAccountsReceivableGuarantorSegmentationHistory(int segmentationBatchId);
        void UpdateActivePassiveGuarantorSegmentationHistory(int segmentationBatchId);
        void UpdateBadDebtGuarantorSegmentationHistory(int segmentationBatchId);
    }
}
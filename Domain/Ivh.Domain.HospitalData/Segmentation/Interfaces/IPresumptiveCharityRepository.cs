﻿namespace Ivh.Domain.HospitalData.Segmentation.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Entities;

    public interface IPresumptiveCharityRepository
    {
        void ExecutePresumptiveCharitySegmentation(bool evaluateVisitsWithoutPtpScores);
        void ExecutePresumptiveCharityHistoryUpdate(int segmentationBatchId);
        void ClearPresumptiveCharityGuarantorVisitDaily();
        IList<PresumptiveCharityEligibleVisit> GetBasePcEligibleVisits(DateTime minSelfPayDate, int batchSize, int lastProcessedVisitId);
    }
}
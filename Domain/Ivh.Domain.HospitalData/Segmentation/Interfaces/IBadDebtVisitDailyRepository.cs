﻿namespace Ivh.Domain.HospitalData.Segmentation.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IBadDebtVisitDailyRepository : IRepository<BadDebtVisitDaily>
    {
        void ExecuteBadDebtSegmentation(int segmentationBatchId);
        IList<BadDebtVisitDaily> GetDailyBadDebtVisits(int segmentationBatchId, int batchSize, int lastProcessedGuarantorId);
    }
}

﻿namespace Ivh.Domain.HospitalData.Segmentation.Interfaces
{
    using Enums;

    public interface ISegmentationBatchRepository
    {
        int GenerateSegmentationBatchId(SegmentationTypeEnum segmentationTypeEnum);
    }
}
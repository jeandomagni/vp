﻿namespace Ivh.Domain.HospitalData.Segmentation.Interfaces
{
    using System.Collections.Generic;
    using Domain.HospitalData.Visit.Entities;
    using Entities;

    public interface IPresumptiveCharityEvaluationProvider
    {
        IList<PresumptiveCharityGuarantorVisitDaily> GetEligibleVisits(IList<PresumptiveCharityEligibleVisit> potentialVisits, int segmentationBatchId);
    }
}
﻿namespace Ivh.Domain.HospitalData.Segmentation.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IAccountsReceivableVisitDailyRepository : IRepository<AccountsReceivableVisitDaily>
    {
        void ExecuteAccountsReceivableSegmentation(int segmentationBatchId);
        IList<AccountsReceivableVisitDaily> GetDailyAccountsReceivableVisits(int segmentationBatchId, int batchSize, int lastProcessedGuarantorId);
    }
}
﻿namespace Ivh.Domain.HospitalData.Segmentation.Interfaces
{
    using System.Collections.Generic;
    using Entities;

    public interface IBadDebtSegmentationValueService
    {
        IList<BadDebtSegmentationValue> GetAll();
    }
}
﻿namespace Ivh.Domain.HospitalData.Segmentation.Interfaces
{
    using System.Collections.Generic;
    using Entities;

    public interface IActivePassiveVisitDailyRepository
    {
        void ExecuteActivePassiveSegmentation(int segmentationBatchId);
        IList<ActivePassiveVisitDaily> GetDailyActivePassiveVisits(int segmentationBatchId, int batchSize, int lastProcessedGuarantorId);

    }
}
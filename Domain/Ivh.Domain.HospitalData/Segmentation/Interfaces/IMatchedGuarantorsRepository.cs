﻿namespace Ivh.Domain.HospitalData.Segmentation.Interfaces
{
    using System.Collections.Generic;
    using Entities;

    public interface IMatchedGuarantorsRepository
    {
        IList<MatchedGuarantors> GetMatchedGuarantors(int parentHsGuarantorId);
    }
}
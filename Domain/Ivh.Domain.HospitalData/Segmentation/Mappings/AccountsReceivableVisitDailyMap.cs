﻿namespace Ivh.Domain.HospitalData.Segmentation.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class AccountsReceivableVisitDailyMap : ClassMap<AccountsReceivableVisitDaily>
    {
        public AccountsReceivableVisitDailyMap()
        {
            this.ReadOnly();
            this.Schema("scoring");
            this.Table("AccountsReceivableVisitDaily");
            this.Id(x => x.HsGuarantorId).Not.Nullable();
            this.Map(x => x.SegmentationBatchId).Not.Nullable();
            this.Map(x => x.HsCurrentBalance).Not.Nullable();
            this.Map(x => x.PtpScore).Nullable();
            this.Map(x => x.AssignedAgencyIds).Nullable();
        }
    }
}
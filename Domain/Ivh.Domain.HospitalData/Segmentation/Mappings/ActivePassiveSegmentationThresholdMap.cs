﻿namespace Ivh.Domain.HospitalData.Segmentation.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;
    public class ActivePassiveSegmentationThresholdMap : ClassMap<ActivePassiveSegmentationThreshold>
    {
        public ActivePassiveSegmentationThresholdMap()
        {
            this.Schema("scoring");
            this.Table("ActivePassiveSegmentationThreshold");
            this.Id(x => x.ActivePassiveSegmentationThresholdId).Not.Nullable();
            this.Map(x => x.PtpScore).Not.Nullable();
            this.Map(x => x.HsGuarantorBalanceLowerLimit).Not.Nullable();
            this.Map(x => x.HsGuarantorBalanceUpperLimit).Not.Nullable();
            this.Map(x => x.HsGuarantorBalanceNoPtpLimit).Not.Nullable();
        }
    }
}
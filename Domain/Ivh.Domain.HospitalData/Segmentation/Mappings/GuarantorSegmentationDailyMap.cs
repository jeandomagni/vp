﻿namespace Ivh.Domain.HospitalData.Segmentation.Mappings
{
    using Entities;
    using Enums;
    using FluentNHibernate.Mapping;

    public class GuarantorSegmentationDailyMap : ClassMap<GuarantorSegmentationDaily>
    {
        public GuarantorSegmentationDailyMap()
        {
            this.Schema("scoring");
            this.Table("GuarantorSegmentationDaily");
            this.Id(x => x.GuarantorSegmentationDailyId).Not.Nullable();
            this.Map(x => x.HsGuarantorId).Not.Nullable();
            this.Map(x => x.SegmentationTypeEnum, "SegmentationTypeId").CustomType<SegmentationTypeEnum>().Not.Nullable();
            this.Map(x => x.SegmentationBatchId).Not.Nullable();
            this.Map(x => x.RawSegmentationScore).Nullable();
            this.Map(x => x.SegmentationValue).Nullable();
            this.Map(x => x.ScriptVersion).Not.Nullable();
            this.Map(x => x.IsActive).Nullable();
            this.Map(x => x.AssignedAgencyIds).Nullable();
        }
    }
}

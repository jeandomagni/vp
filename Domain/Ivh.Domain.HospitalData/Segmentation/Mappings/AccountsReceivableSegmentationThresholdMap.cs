﻿namespace Ivh.Domain.HospitalData.Segmentation.Mappings
{
    using FluentNHibernate.Mapping;
    using Entities;

    public class AccountsReceivableSegmentationThresholdMap : ClassMap<AccountsReceivableSegmentationThreshold>
    {
        public AccountsReceivableSegmentationThresholdMap()
        {
            this.ReadOnly();
            this.Schema("scoring");
            this.Table("AccountsReceivableSegmentationThreshold");
            this.Id(x => x.AccountsReceivableSegmentationThresholdId).Not.Nullable();
            this.Map(x => x.MinPtpScore).Nullable();
            this.Map(x => x.MaxPtpScore).Nullable();
            this.Map(x => x.MinHsGuarantorBalance).Nullable();
            this.Map(x => x.MaxHsGuarantorBalance).Nullable();
            this.Map(x => x.SegmentationValue).Not.Nullable();
            this.Map(x => x.Description).Not.Nullable();
        }
    }
}
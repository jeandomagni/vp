﻿namespace Ivh.Domain.HospitalData.Segmentation.Mappings
{
    using FluentNHibernate.Mapping;
    using Entities;

    public class BadDebtVisitDailyMap : ClassMap<BadDebtVisitDaily>
    {
        public BadDebtVisitDailyMap()
        {
            this.ReadOnly();
            this.Schema("scoring");
            this.Table("BadDebtVisitDaily");
            this.Id(x => x.HsGuarantorId).Not.Nullable();
            this.Map(x => x.SegmentationBatchId).Not.Nullable();
            this.Map(x => x.HasCashPayment).Not.Nullable();
            this.Map(x => x.HsCurrentBalance).Not.Nullable();
            this.Map(x => x.PtpScore).Nullable();
            this.Map(x => x.AssignedAgencyIds).Nullable();
        }
    }
}

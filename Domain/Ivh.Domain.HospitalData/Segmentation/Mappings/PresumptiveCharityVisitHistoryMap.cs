﻿namespace Ivh.Domain.HospitalData.Segmentation.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class PresumptiveCharityVisitHistoryMap : ClassMap<PresumptiveCharityVisitHistory>
    {
        public PresumptiveCharityVisitHistoryMap()
        {
            this.ReadOnly();
            this.Schema("scoring");
            this.Table("PresumptiveCharityVisitHistory");
            this.Id(x => x.PresumptiveCharityVisitHistoryId).Nullable();
            this.Map(x => x.VisitId).Nullable();
            this.Map(x => x.PresumptiveCharityVisitGuarantorHistoryId).Nullable();
            this.Map(x => x.SegmentationBatchId).Nullable();
        }
    }
}

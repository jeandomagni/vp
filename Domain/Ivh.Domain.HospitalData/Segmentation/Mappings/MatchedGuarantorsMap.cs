﻿namespace Ivh.Domain.HospitalData.Segmentation.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class MatchedGuarantorsMap : ClassMap<MatchedGuarantors>
    {
        public MatchedGuarantorsMap()
        {
            this.Schema("scoring");
            this.Table("MatchedGuarantors");
            this.Id(x => x.MatchedGuarantorsId).Not.Nullable();
            this.Map(x => x.ParentHsGuarantorId).Not.Nullable();
            this.Map(x => x.ChildHsGuarantorId).Not.Nullable();
        }
    }
}
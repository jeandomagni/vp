﻿namespace Ivh.Domain.HospitalData.Segmentation.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class ActivePassiveVisitDailyMap : ClassMap<ActivePassiveVisitDaily>
    {
        public ActivePassiveVisitDailyMap()
        {
            this.ReadOnly();
            this.Schema("scoring");
            this.Table("ActivePassiveVisits");
            this.Id(x => x.HsGuarantorId).Not.Nullable();
            this.Map(x => x.SegmentationBatchId).Not.Nullable();
            this.Map(x => x.FirstSelfPayDate).Nullable();
            this.Map(x => x.HasCashPayment).Not.Nullable();
            this.Map(x => x.SelfPayDatesWithinBounds).Not.Nullable();
            this.Map(x => x.AllOldVisitsHavePriorPayment).Not.Nullable();
            this.Map(x => x.HsCurrentBalance).Not.Nullable();
            this.Map(x => x.PtpScore).Nullable();
            this.Map(x => x.AssignedAgencyIds).Nullable();
        }
    }
}
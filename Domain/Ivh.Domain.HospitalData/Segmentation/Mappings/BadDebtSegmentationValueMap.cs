﻿namespace Ivh.Domain.HospitalData.Segmentation.Mappings
{
    using FluentNHibernate.Mapping;
    using Entities;

    public class BadDebtSegmentationValueMap : ClassMap<BadDebtSegmentationValue>
    {
        public BadDebtSegmentationValueMap()
        {
            this.ReadOnly();
            this.Schema("scoring");
            this.Table("BadDebtSegmentationValue");
            this.Id(x => x.BadDebtSegmentationValueId).Not.Nullable();
            this.Map(x => x.SegmentationValue).Not.Nullable();
            this.Map(x => x.Description).Not.Nullable();
            this.Map(x => x.MinValue).Nullable();
            this.Map(x => x.MaxValue).Nullable();
            this.Map(x => x.CharValue).Nullable();
        }
    }
}

﻿namespace Ivh.Domain.HospitalData.Segmentation.Mappings
{
    using Entities;
    using Enums;
    using FluentNHibernate.Mapping;

    public class SegmentationBatchMap : ClassMap<SegmentationBatch>
    {
        public SegmentationBatchMap()
        {
            this.Schema("scoring");
            this.Table("SegmentationBatch");
            this.Id(x => x.SegmentationBatchId).Not.Nullable();
            this.Map(x => x.BatchStartDateTime).Not.Nullable();
            this.Map(x => x.SegmentationTypeEnum, "SegmentationTypeId").CustomType<SegmentationTypeEnum>().Not.Nullable();
        }
    }
}
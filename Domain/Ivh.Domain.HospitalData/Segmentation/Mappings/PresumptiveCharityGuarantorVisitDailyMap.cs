﻿namespace Ivh.Domain.HospitalData.Segmentation.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class PresumptiveCharityGuarantorVisitDailyMap : ClassMap<PresumptiveCharityGuarantorVisitDaily>
    {
        public PresumptiveCharityGuarantorVisitDailyMap()
        {
            this.Schema("scoring");
            this.Table("PresumptiveCharityGuarantorVisitDaily");
            this.Id(x => x.PresumptiveCharityGuarantorVisitDailyId).Not.Nullable();
            this.Map(x => x.VisitId).Not.Nullable();
            this.Map(x => x.HsGuarantorId).Not.Nullable();
            this.Map(x => x.SegmentationBatchId).Not.Nullable();
        }
    }
}

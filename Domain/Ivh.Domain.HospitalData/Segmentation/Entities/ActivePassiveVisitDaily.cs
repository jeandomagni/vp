﻿namespace Ivh.Domain.HospitalData.Segmentation.Entities
{
    using System;
    public class ActivePassiveVisitDaily
    {
        public virtual int HsGuarantorId { get; set; }
        public virtual int SegmentationBatchId { get; set; }
        public virtual DateTime FirstSelfPayDate { get; set; }
        public virtual bool HasCashPayment { get; set; }
        public virtual bool SelfPayDatesWithinBounds { get; set; }
        public virtual bool AllOldVisitsHavePriorPayment { get; set; }
        public virtual decimal HsCurrentBalance { get; set; }
        public virtual decimal? PtpScore { get; set; }
        public virtual string AssignedAgencyIds { get; set; }
    }
}
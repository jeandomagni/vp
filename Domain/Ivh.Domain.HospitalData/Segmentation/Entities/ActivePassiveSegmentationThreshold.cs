﻿namespace Ivh.Domain.HospitalData.Segmentation.Entities
{
    using System;

    public class ActivePassiveSegmentationThreshold
    {
        public virtual int ActivePassiveSegmentationThresholdId { get; set; }
        public virtual int DaysSinceSelfPayDate { get; set; }
        public virtual decimal PtpScore { get; set; }
        public virtual decimal HsGuarantorBalanceLowerLimit { get; set; }
        public virtual decimal HsGuarantorBalanceUpperLimit { get; set; }
        public virtual decimal HsGuarantorBalanceNoPtpLimit { get; set; }
    }
}
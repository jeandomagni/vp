﻿namespace Ivh.Domain.HospitalData.Segmentation.Entities
{
    using Enums;

    public class GuarantorSegmentationDaily
    {
        public virtual int GuarantorSegmentationDailyId { get; set; }
        public virtual int HsGuarantorId { get; set; }
        public virtual SegmentationTypeEnum SegmentationTypeEnum { get; set; }
        public virtual int SegmentationBatchId { get; set; }
        public virtual string RawSegmentationScore { get; set; }
        public virtual string SegmentationValue { get; set; }
        public virtual string ScriptVersion { get; set; }
        public virtual bool? IsActive { get; set; }
        public virtual string AssignedAgencyIds { get; set; }
    }
}
﻿namespace Ivh.Domain.HospitalData.Segmentation.Entities
{
    public class PresumptiveCharityGuarantorVisitDaily
    {
        public virtual int PresumptiveCharityGuarantorVisitDailyId { get; set; }
        public virtual int VisitId { get; set; }
        public virtual int HsGuarantorId { get; set; }
        public virtual int SegmentationBatchId { get; set; }
    }
}

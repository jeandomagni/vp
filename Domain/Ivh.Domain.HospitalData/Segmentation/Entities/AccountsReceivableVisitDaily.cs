﻿namespace Ivh.Domain.HospitalData.Segmentation.Entities
{
    public class AccountsReceivableVisitDaily
    {
        public virtual int HsGuarantorId { get; set; }
        public virtual int SegmentationBatchId { get; set; }
        public virtual decimal HsCurrentBalance { get; set; }
        public virtual decimal? PtpScore { get; set; }
        public virtual string AssignedAgencyIds { get; set; }
    }
}
﻿namespace Ivh.Domain.HospitalData.Segmentation.Entities
{
    public class SegmentationServiceActivePassiveData : SegmentationServiceData
    {
        public virtual bool SelfPayDatesWithinBounds { get; set; }
        public virtual bool AllOldVisitsHavePriorPayment { get; set; }
    }
}
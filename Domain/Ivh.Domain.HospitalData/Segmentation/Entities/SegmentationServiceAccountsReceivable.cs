﻿namespace Ivh.Domain.HospitalData.Segmentation.Entities
{
    using System.Collections.Generic;

    public class SegmentationServiceAccountsReceivable
    {
        public IList<SegmentationServiceData> SegmentationServiceAccountsReceivableData { get; set; }
        public IList<AccountsReceivableSegmentationThreshold> AccountsReceivableSegmentationThresholds { get; set; }
    }
}
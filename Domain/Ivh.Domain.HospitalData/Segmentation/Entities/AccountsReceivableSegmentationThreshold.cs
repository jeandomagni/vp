﻿using System;

namespace Ivh.Domain.HospitalData.Segmentation.Entities
{
    public class AccountsReceivableSegmentationThreshold
    {
        public virtual int AccountsReceivableSegmentationThresholdId { get; set; }
        public virtual int MinPtpScore { get; set; }
        public virtual int MaxPtpScore { get; set; }
        public virtual decimal MinHsGuarantorBalance { get; set; }
        public virtual decimal MaxHsGuarantorBalance { get; set; }
        public virtual int SegmentationValue { get; set; }
        public virtual string Description { get; set; }
    }
}
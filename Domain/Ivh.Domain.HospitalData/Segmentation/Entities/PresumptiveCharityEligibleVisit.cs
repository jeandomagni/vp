﻿using System;

namespace Ivh.Domain.HospitalData.Segmentation.Entities
{
    using Common.VisitPay.Enums;
    using Visit.Entities;

    public class PresumptiveCharityEligibleVisit
    {
        public virtual int VisitId { get; set; }
        public virtual int HsGuarantorId { get; set; }
        public virtual bool? EvaluateForPCSegmentation { get; set; }
        public virtual DateTime? FirstSelfPayDate { get; set; }
        public virtual int SelfPayClassId { get; set; }
    }
}

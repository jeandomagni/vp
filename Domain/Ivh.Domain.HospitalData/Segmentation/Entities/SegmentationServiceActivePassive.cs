﻿namespace Ivh.Domain.HospitalData.Segmentation.Entities
{
    using System.Collections.Generic;

    public class SegmentationServiceActivePassive
    {
        public IList<SegmentationServiceActivePassiveData> SegmentationServiceActivePassiveData { get; set; }
        public ActivePassiveSegmentationThreshold ActivePassiveSegmentationThreshold { get; set; }
    }
}
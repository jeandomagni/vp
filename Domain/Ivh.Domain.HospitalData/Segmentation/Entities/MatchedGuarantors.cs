﻿namespace Ivh.Domain.HospitalData.Segmentation.Entities
{
    public class MatchedGuarantors
    {
        public virtual int MatchedGuarantorsId { get; set; }
        public virtual int ParentHsGuarantorId { get; set; }
        public virtual int ChildHsGuarantorId { get; set; }
    }
}
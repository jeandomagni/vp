﻿namespace Ivh.Domain.HospitalData.Segmentation.Entities
{
    public class PresumptiveCharityVisitHistory
    {
        public virtual int? PresumptiveCharityVisitHistoryId { get; set; }
        public virtual int? VisitId { get; set; }
        public virtual int? PresumptiveCharityVisitGuarantorHistoryId { get; set; }
        public virtual int? SegmentationBatchId { get; set; }

    }
}
﻿using System;

namespace Ivh.Domain.HospitalData.Segmentation.Entities
{
    public class BadDebtSegmentationValue
    {
        public virtual int BadDebtSegmentationValueId { get; set; }
        public virtual string SegmentationValue { get; set; }
        public virtual string Description { get; set; }
        public virtual int MinValue { get; set; }
        public virtual int MaxValue { get; set; }
        public virtual string CharValue { get; set; }
    }
}
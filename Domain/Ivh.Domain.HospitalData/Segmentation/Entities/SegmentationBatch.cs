﻿namespace Ivh.Domain.HospitalData.Segmentation.Entities
{
    using System;
    using Enums;

    public class SegmentationBatch
    {
        public virtual int SegmentationBatchId { get; set; }
        public virtual DateTime BatchStartDateTime { get; set; }
        public virtual SegmentationTypeEnum SegmentationTypeEnum { get; set; }
    }
}
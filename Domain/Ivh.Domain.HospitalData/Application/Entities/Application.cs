﻿namespace Ivh.Domain.HospitalData.Application.Entities
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class Application
    {
        public virtual int ApplicationId { get; set; }
        public virtual string ApplicationName { get; set; }

        public virtual ApplicationEnum ApplicationEnum
        {
            get
            {
                return (ApplicationEnum) this.ApplicationId;
            }
        }
    }
}

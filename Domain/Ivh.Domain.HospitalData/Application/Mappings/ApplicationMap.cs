﻿namespace Ivh.Domain.HospitalData.Application.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class ApplicationMap : ClassMap<Application>
    {
        public ApplicationMap()
        {
            this.Schema("dbo");
            this.Table("Application");
            this.Cache.ReadOnly();
            this.Id(x => x.ApplicationId).Column("ApplicationId");

        }
    }
}

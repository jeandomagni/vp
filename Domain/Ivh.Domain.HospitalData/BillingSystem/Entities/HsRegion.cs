﻿namespace Ivh.Domain.HospitalData.BillingSystem.Entities
{
    public class HsRegion
    {
        public virtual int RegionId { get; set; }
        public virtual string RegionScope { get; set; }
    }
}

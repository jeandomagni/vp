﻿namespace Ivh.Domain.HospitalData.BillingSystem.Entities
{
    public class HsBillingSystem
    {
        public virtual int BillingSystemId { get; set; }
        public virtual string BillingSystemName { get; set; }
        public virtual int? RegionId { get; set; }
        public virtual HsRegion Region { get; set; }
    }
}

﻿namespace Ivh.Domain.HospitalData.BillingSystem.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class HsBillingSystemMap : ClassMap<HsBillingSystem>
    {
        public HsBillingSystemMap()
        {
            this.Schema("base");
            this.Table("BillingSystem");
            this.Id(x => x.BillingSystemId);
            this.Map(x => x.BillingSystemName).Not.Nullable();
            this.Map(x => x.RegionId).Nullable();

            this.References(x => x.Region)
                .Column("RegionId")
                .Not.Insert()
                .Not.Update();
        }
    }
}

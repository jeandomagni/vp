﻿namespace Ivh.Domain.HospitalData.BillingSystem.Mappings
{
    using FluentNHibernate.Mapping;
    using Ivh.Domain.HospitalData.BillingSystem.Entities;

    public class HsRegionMap : ClassMap<HsRegion>
    {
        public HsRegionMap()
        {
            this.Schema("base");
            this.Table("Region");
            this.Id(x => x.RegionId);
            this.Map(x => x.RegionScope).Not.Nullable();
        }
    }
}

﻿namespace Ivh.Domain.HospitalData.Visit.Services
{
    using System;
    using System.Collections.Generic;
    using Base.Interfaces;
    using Base.Services;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.FinanceManagement;
    using Common.VisitPay.Messages.HospitalData;
    using Entities;
    using Interfaces;

    public class OutboundVisitTransactionService : DomainService, IOutboundVisitTransactionService
    {
        private readonly Lazy<IOutboundVisitTransactionRepository> _outboundVisitTransactionRepository;
        private readonly Lazy<IVisitService> _visitService;

        public OutboundVisitTransactionService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IOutboundVisitTransactionRepository> outboundVisitTransactionRepository,
            Lazy<IVisitService> visitService) : base(serviceCommonService)
        {
            this._outboundVisitTransactionRepository = outboundVisitTransactionRepository;
            this._visitService = visitService;
        }

        public void CreateOutboundVisitTransactions(IList<PaymentAllocationMessage> messages)
        {
            foreach (PaymentAllocationMessage paymentAllocationMessage in messages)
            {
                Visit visit = this.GetVisit(paymentAllocationMessage.VisitBillingSystemId, paymentAllocationMessage.VisitSourceSystemKey, nameof(paymentAllocationMessage.VisitSourceSystemKey));

                VpOutboundVisitTransaction newVpOutboundVisitTransaction = new VpOutboundVisitTransaction
                {
                    Visit = visit,
                    VisitBillingSystemId = paymentAllocationMessage.VisitBillingSystemId,
                    VisitSourceSystemKey = paymentAllocationMessage.VisitSourceSystemKey,
                    VpPaymentAllocationOpposingPaymentAllocationId = paymentAllocationMessage.VpPaymentAllocationOpposingPaymentAllocationId,
                    VpPaymentAllocationTypeId = paymentAllocationMessage.VpPaymentAllocationTypeId,
                    VpPaymentId = paymentAllocationMessage.VpPaymentId,
                    VpPaymentAllocationId = paymentAllocationMessage.VpPaymentAllocationId,
                    VpPaymentMethodType = paymentAllocationMessage.VpPaymentMethodType,
                    TransactionAmount = paymentAllocationMessage.AllocationAmount * -1m,
                    InsertDate = DateTime.UtcNow,
                    VpPaymentType = (paymentAllocationMessage.VpPaymentType == PaymentTypeEnum.Unknown ? default(PaymentTypeEnum?) : paymentAllocationMessage.VpPaymentType),
                    VpPaymentActualPaymentDate = paymentAllocationMessage.VpPaymentActualPaymentDate,
                    VpPaymentAmount = paymentAllocationMessage.VpPaymentAmount,
                    VpVisitTransactionInsertDate = paymentAllocationMessage.VpPaymentAllocationInsertDate,
                    VpParentPaymentId = paymentAllocationMessage.VpParentPaymentId,
                    VpFinancePlanId = paymentAllocationMessage.VpFinancePlanId,
                    FinancePlanDuration = paymentAllocationMessage.VpFinancePlanCurrentTotalDuration,
                    FinancePlanOriginalDuration = paymentAllocationMessage.VpFinancePlanOriginalDuration,
                    VpGuarantorId = paymentAllocationMessage.VpGuarantorId,
                    BalanceTransferStatus = paymentAllocationMessage.BalanceTransferStatusId == null ? null : new BalanceTransferStatus { BalanceTransferStatusId = paymentAllocationMessage.BalanceTransferStatusId.Value },
                    MerchantAccountId = paymentAllocationMessage.MerchantAccountId,
                    ProcessedDate = paymentAllocationMessage.VpPaymentActualPaymentDate,
                    AchSettlementType = paymentAllocationMessage.AchSettlementType ?? AchSettlementTypeEnum.None
                };

                this._outboundVisitTransactionRepository.Value.InsertOrUpdate(newVpOutboundVisitTransaction);
            }
        }

        public void CreateOutboundVisitTransactions(IList<InterestEventMessage> messages)
        {
            foreach (InterestEventMessage interestEventMessage in messages)
            {
                Visit visit = this.GetVisit(interestEventMessage.VisitBillingSystemId, interestEventMessage.VisitSourceSystemKey, nameof(interestEventMessage.VisitSourceSystemKey));

                VpOutboundVisitTransaction newVpOutboundVisitTransaction = new VpOutboundVisitTransaction
                {
                    Visit = visit,
                    VisitBillingSystemId = interestEventMessage.VisitBillingSystemId,
                    VisitSourceSystemKey = interestEventMessage.VisitSourceSystemKey,
                    VpGuarantorId = interestEventMessage.VpGuarantorId,
                    VpFinancePlanId = interestEventMessage.VpFinancePlanId,
                    VpFinancePlanVisitInterestAmount = interestEventMessage.VpFinancePlanVisitInterestAmount,
                    VpFinancePlanVisitInterestDueId = interestEventMessage.VpFinancePlanVisitInterestDueId,
                    VpFinancePlanVisitInterestDueParentId = interestEventMessage.VpFinancePlanVisitInterestDueParentId,
                    VpFinancePlanVisitInterestDueTypeId = interestEventMessage.VpFinancePlanVisitInterestDueTypeId,
                    VpFinancePlanVisitInterestInsertDate = interestEventMessage.VpFinancePlanVisitInterestInsertDate,
                    InsertDate = DateTime.UtcNow,
                    ProcessedDate = interestEventMessage.ProcessedDate
                };

                this._outboundVisitTransactionRepository.Value.InsertOrUpdate(newVpOutboundVisitTransaction);
            }
        }

        public IList<VpOutboundVisitTransaction> GetOutboundVisitTransactionsByVisit(int visitId)
        {
            IList<VpOutboundVisitTransaction> result = this._outboundVisitTransactionRepository.Value.GetOutboundVisitTransactionsByVisit(visitId);
            return result;
        }

        private Visit GetVisit(int visitBillingSystemId, string visitSourceSystemKey, string paramName)
        {
            Visit visit = this._visitService.Value.GetVisit(visitBillingSystemId, visitSourceSystemKey);

            if (visit == null)
            {
                throw new ArgumentNullException(paramName, "Could not find corresponding Visit.");
            }

            return visit;
        }
    }
}
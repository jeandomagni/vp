﻿namespace Ivh.Domain.HospitalData.Visit.Services
{
    using System;
    using System.Collections.Generic;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Utilities.Extensions;
    using Entities;
    using Interfaces;
    using IFacilityRepository = Interfaces.IFacilityRepository;
    using IInsurancePlanRepository = Interfaces.IInsurancePlanRepository;
    using IPrimaryInsuranceTypeRepository = Interfaces.IPrimaryInsuranceTypeRepository;
    using IVisitRepository = Interfaces.IVisitRepository;
    using IVisitService = Interfaces.IVisitService;
    using IVisitTransactionRepository = Interfaces.IVisitTransactionRepository;

    public class VisitService : DomainService, IVisitService
    {
        private readonly IInsurancePlanRepository _insurancePlanRepository;
        private readonly ILifeCycleStageRepository _lifeCycleStageRepository;
        private readonly IPatientTypeRepository _patientTypeRepository;
        private readonly IPrimaryInsuranceTypeRepository _primaryInsuranceTypeRepository;
        private readonly IRevenueWorkCompanyRepository _revenueWorkCompanyRepository;
        private readonly IFacilityRepository _facilityRepository;
        private readonly IVisitRepository _visitRepository;
        private readonly IVisitTransactionRepository _visitTransactionRepository;

        public VisitService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            IVisitRepository hsVisitRepository,
            IVisitTransactionRepository hsVisitTransactionRepository,
            IPrimaryInsuranceTypeRepository primaryInsuranceTypeRepository,
            IPatientTypeRepository patientTypeRepository,
            ILifeCycleStageRepository lifeCycleStageRepository,
            IRevenueWorkCompanyRepository revenueWorkCompanyRepository,
            IFacilityRepository facilityRepository,
            IInsurancePlanRepository insurancePlanRepository) : base(serviceCommonService)
        {
            this._visitRepository = hsVisitRepository;
            this._visitTransactionRepository = hsVisitTransactionRepository;
            this._primaryInsuranceTypeRepository = primaryInsuranceTypeRepository;
            this._patientTypeRepository = patientTypeRepository;
            this._lifeCycleStageRepository = lifeCycleStageRepository;
            this._revenueWorkCompanyRepository = revenueWorkCompanyRepository;
            this._facilityRepository = facilityRepository;
            this._insurancePlanRepository = insurancePlanRepository;
        }

        public IList<PrimaryInsuranceType> GetPrimaryInsuranceTypes()
        {
            return this._primaryInsuranceTypeRepository.GetAllPrimaryInsuranceType();
        }

        public IList<RevenueWorkCompany> GetRevenueWorkCompanies()
        {
            return this._revenueWorkCompanyRepository.GetAllRevenueWorkCompany();
        }

        public IList<Facility> GetFacilities()
        {
            return this._facilityRepository.GetAllFacilities();
        }

        public IList<PatientType> GetPatientTypes()
        {
            return this._patientTypeRepository.GetAllPatientType();
        }

        public IList<InsurancePlan> GetInsurancePlans()
        {
            return this._insurancePlanRepository.GetAllInsurancePlan();
        }

        public IList<LifeCycleStage> GetLifeCycleStages()
        {
            return this._lifeCycleStageRepository.GetAllLifeCycleStage();
        }

        public Visit GetVisit(int visitId)
        {
            return this._visitRepository.GetVisitWithBillingSystem(visitId);
        }

        public Visit GetVisit(int billingSystemId, string sourceSystemKey)
        {
            return this._visitRepository.GetVisit(billingSystemId, sourceSystemKey);
        }

        public IReadOnlyList<Visit> GetVisitsForGuarantor(int hsGuarantorId)
        {
            return this._visitRepository.GetVisitsForGuarantor(hsGuarantorId);
        }

        public void SaveVisitTransaction(VisitTransaction visitTransaction)
        {
            this._visitTransactionRepository.InsertOrUpdate(visitTransaction);
        }

        public void SaveVisitTransactions(IEnumerable<VisitTransaction> visitTransactions)
        {
            visitTransactions.ForEach(this.SaveVisitTransaction);
        }

        public bool GuarantorHasBadDebtVisits(IEnumerable<int> hsGuarantorIds)
        {
            return this._visitRepository.GuarantorHasBadDebtVisits(hsGuarantorIds);
        }

        public bool GuarantorHasVisitOnPrePaymentPlan(IEnumerable<int> hsGuarantorIds)
        {
            return this._visitRepository.GuarantorHasVisitOnPrePaymentPlan(hsGuarantorIds);
        }
    }
}
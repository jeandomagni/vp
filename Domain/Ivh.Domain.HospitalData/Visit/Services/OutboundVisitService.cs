﻿namespace Ivh.Domain.HospitalData.Visit.Services
{
    using System;
    using System.Collections.Generic;
    using Base.Interfaces;
    using Base.Services;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;

    public class OutboundVisitService : DomainService, IOutboundVisitService
    {
        private readonly Lazy<IOutboundVisitRepository> _outboundVisitRepository;
        private readonly Lazy<IVisitService> _hsVisitService;
        private List<VpOutboundVisitMessageTypeEnum> DisabledMessageEnumValues => new List<VpOutboundVisitMessageTypeEnum>
        {
            VpOutboundVisitMessageTypeEnum.VisitAgingCountChange,
            VpOutboundVisitMessageTypeEnum.UncollectableNotification
        };

        public OutboundVisitService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IOutboundVisitRepository> outboundVisitRepository,
            Lazy<IVisitService> hsVisitService) : base(serviceCommonService)
        {
            this._outboundVisitRepository = outboundVisitRepository;
            this._hsVisitService = hsVisitService;
        }

        public void CreateOutboundVisit(
           VpOutboundVisit vpOutboundVisit)
        {
            // VP-4572 - don't send outbound VisitAgingCountChange when VisitPayAging is disabled
            if (DisabledMessageEnumValues.Contains(vpOutboundVisit.VpOutboundVisitMessageType) &&
                !this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureVisitPayAgingIsEnabled))
            {
                return;
            }

            Visit hsVisit = this._hsVisitService.Value.GetVisit(vpOutboundVisit.BillingSystemId, vpOutboundVisit.SourceSystemKey);
            if (hsVisit == null)
            {
                throw new ArgumentNullException(nameof(vpOutboundVisit.SourceSystemKey), "Could not find corresponding Visit.");
            }

            vpOutboundVisit.Visit = hsVisit;
            vpOutboundVisit.HsGuarantorId = hsVisit.HsGuarantorId;

            this._outboundVisitRepository.Value.InsertOrUpdate(vpOutboundVisit);


        }

        public IList<VpOutboundVisit> GetOutboundVisitsByVisit(int visitId)
        {
            IList<VpOutboundVisit> result = this._outboundVisitRepository.Value.GetOutboundVisitsByVisit(visitId);
            return result;
        }
    }
}
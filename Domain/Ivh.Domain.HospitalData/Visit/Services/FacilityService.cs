﻿namespace Ivh.Domain.HospitalData.Visit.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Entities;
    using Interfaces;

    public class FacilityService : DomainService, IFacilityService
    {
        private readonly Lazy<IFacilityRepository> _facilityRepo;

        public FacilityService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IFacilityRepository> facilityRepo) 
            : base(serviceCommonService)
        {
            this._facilityRepo = facilityRepo;
        }

        public IReadOnlyList<Facility> GetAllFacilities() => this._facilityRepo.Value.GetQueryable().ToList();
    }
}
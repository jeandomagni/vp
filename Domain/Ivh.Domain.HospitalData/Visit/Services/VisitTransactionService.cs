﻿namespace Ivh.Domain.HospitalData.Visit.Services
{
    using System;
    using System.Collections.Generic;
    using Base.Interfaces;
    using Base.Services;
    using Entities;
    using Interfaces;

    public class VisitTransactionService : DomainService, IVisitTransactionService
    {
        private readonly IVisitTransactionRepository _visitTransactionRepository;

        public VisitTransactionService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            IVisitTransactionRepository visitTransactionRepository) : base(serviceCommonService)
        {
            this._visitTransactionRepository = visitTransactionRepository;
        }

        public IList<TransactionCode> GetTransactionCodes()
        {
            return this._visitTransactionRepository.GetTransactionCodes();
        }

        public IList<VpTransactionType> GetTransactionTypes()
        {
            return this._visitTransactionRepository.GetTransactionTypes();
        }
    }
}
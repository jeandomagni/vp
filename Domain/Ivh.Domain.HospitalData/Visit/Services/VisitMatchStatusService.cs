﻿namespace Ivh.Domain.HospitalData.Visit.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;
    using Entities;
    using HsGuarantor.Interfaces;
    using Interfaces;
    using VpGuarantor.Entities;

    public class VisitMatchStatusService : DomainService, IVisitMatchStatusService
    {
        private readonly Lazy<IVpGuarantorHsMatchRepository> _vpGuarantorHsMatchRepository;
        private readonly Lazy<IVisitMatchStatusRepository> _visitMatchStatusRepository;
        private readonly Lazy<IVisitRepository> _visitRepository;
        private readonly Lazy<IOutboundVisitService> _outboundVisitService;

        public VisitMatchStatusService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IVpGuarantorHsMatchRepository> vpGuarantorHsMatchRepository,
            Lazy<IVisitMatchStatusRepository> visitMatchStatusRepository,
            Lazy<IOutboundVisitService> outboundVisitService,
            Lazy<IVisitRepository> visitRepository) : base(serviceCommonService)
        {
            this._vpGuarantorHsMatchRepository = vpGuarantorHsMatchRepository;
            this._visitMatchStatusRepository = visitMatchStatusRepository;
            this._visitRepository = visitRepository;
            this._outboundVisitService = outboundVisitService;
        }

        public void UnmatchVpVisit(int hsGuarantorId, int vpGuarantorId, int billingSystemId, string sourceSystemKey, int vpVisitId, HsGuarantorMatchStatusEnum hsGuarantorMatchStatus)
        {
            Visit visit = this._visitRepository.Value.GetVisit(billingSystemId, sourceSystemKey);

            if (visit != null)
            {
                IList<VpGuarantorHsMatch> vpGuarantorHsMatches = this._vpGuarantorHsMatchRepository.Value.GetById(hsGuarantorId, vpGuarantorId);
                if (vpGuarantorHsMatches.IsNotNullOrEmpty())
                {
                    //get the active one if possible, or get the most recently matched one.
                    VpGuarantorHsMatch vpGuarantorHsMatch = vpGuarantorHsMatches.OrderBy(x => x.HsGuarantorMatchStatus).ThenByDescending(x => x.MatchedOn).First();
                    VisitMatchStatus visitMatchStatus = this._visitMatchStatusRepository.Value.GetVisitMatchStatus(vpGuarantorHsMatch, visit.VisitId);

                    if (visitMatchStatus == null)
                    {
                        visitMatchStatus = new VisitMatchStatus
                        {
                            HsGuarantorId = vpGuarantorHsMatch.HsGuarantorId,
                            VpGuarantorId = vpGuarantorHsMatch.VpGuarantorId,
                            VisitId = visit.VisitId
                        };
                    }

                    visitMatchStatus.HsGuarantorMatchStatus = hsGuarantorMatchStatus;
                    this._visitMatchStatusRepository.Value.InsertOrUpdate(visitMatchStatus);

                    // TODO: consider publishing a post commit message that will raise an OutboundVisitMessage
                    VpOutboundVisit vpOutboundVisit = new VpOutboundVisit {
                        VpVisitId = vpVisitId,
                        BillingSystemId = visit.BillingSystemId,
                        SourceSystemKey = visit.SourceSystemKey,
                        VpOutboundVisitMessageType = VpOutboundVisitMessageTypeEnum.VpVisitUnmatch,
                        ActionDate = DateTime.UtcNow
                    };

                    this._outboundVisitService.Value.CreateOutboundVisit(vpOutboundVisit);
                }
            }
        }
    }
}

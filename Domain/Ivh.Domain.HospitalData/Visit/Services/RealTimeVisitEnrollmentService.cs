﻿namespace Ivh.Domain.HospitalData.Visit.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.HospitalData;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using Entities;
    using Interfaces;
    using Ivh.Application.HospitalData.Common.Interfaces;
    using Logging.Interfaces;
    using NHibernate;

    public class RealTimeVisitEnrollmentService : DomainService, IRealTimeVisitEnrollmentService
    {
        private readonly Lazy<ILoggingService> _loggingService;
        private readonly Lazy<IChangeEventApplicationService> _changeEventApplicationService;
        private readonly ISession _session;
        private readonly Lazy<IRealTimeVisitEnrollmentRepository> _realTimeVisitEnrollmentRepository;
        private readonly Lazy<IVisitRepository> _visitRepository;

        public RealTimeVisitEnrollmentService(
            Lazy<ILoggingService> loggingService,
            ISessionContext<CdiEtl> sessionContext,
            Lazy<IChangeEventApplicationService> changeEventApplicationService,
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IRealTimeVisitEnrollmentRepository> realTimeVisitEnrollmentRepository,
            Lazy<IVisitRepository> visitRepository)
            : base(serviceCommonService)
        {
            this._loggingService = loggingService;
            this._changeEventApplicationService = changeEventApplicationService;
            this._session = sessionContext.Session;
            this._realTimeVisitEnrollmentRepository = realTimeVisitEnrollmentRepository;
            this._visitRepository = visitRepository;
        }

        public void ProcessRealTimeVisitEnrollmentMessage(RealTimeVisitEnrollmentMessage realTimeVisitEnrollmentMessage)
        {
            IList<RealTimeVisitEnrollment> realTimeVisits = new List<RealTimeVisitEnrollment>();
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                foreach (RealTimeEnrollmentAccountDto account in realTimeVisitEnrollmentMessage.GuarantorAccounts)
                {
                    Visit visit = this._visitRepository.Value.GetVisit(account.BillingSystemId, account.SourceSystemKey);

                    if (visit == null)
                    {
                        this._loggingService.Value.Info(() => $"RealTimeVisitEnrollmentService:ProcessRealTimeVisitEnrollmentMessage: No base.Visit found for SourceSystemKey {account.SourceSystemKey} and BillingSystemId {account.BillingSystemId} for GuarantorSourceSystemKey {realTimeVisitEnrollmentMessage.HsGuarantorSourceSystemKey}");
                    }
                    else
                    {
                        bool pendingRealtimeEnrollmentExists = this._realTimeVisitEnrollmentRepository.Value.PendingRealtimeEnrollmentExists(visit.VisitId, visit.BillingSystemId, visit.HsGuarantorId);
                        if (pendingRealtimeEnrollmentExists)
                        {
                            continue;
                        }

                        visit.VpEligible = true;
                        visit.BillingHold = account.BillingHold;
                        this._visitRepository.Value.InsertOrUpdate(visit);

                        realTimeVisits.Add(new RealTimeVisitEnrollment
                        {
                            VisitId = visit.VisitId,
                            HsGuarantorId = visit.HsGuarantorId,
                            BillingSystemId = account.BillingSystemId,
                            BillingHold = account.BillingHold,
                            CreateDate = DateTime.UtcNow
                        });
                    }
                }

                this._realTimeVisitEnrollmentRepository.Value.BulkInsert(realTimeVisits);

                int hsGuarantorId = realTimeVisits.Select(x => x.HsGuarantorId).FirstOrDefault();
                // publish the change events after we've updated the base.Visit table
                this._session.RegisterPostCommitSuccessAction((rtvem) =>
                {
                    IList<EnrollmentResponseDto> responseList = new List<EnrollmentResponseDto> { rtvem.Response };
                    this._changeEventApplicationService.Value.GenerateChangeEventForHsGuarantor(rtvem.VpGuarantorId, hsGuarantorId, responseList);

                }, realTimeVisitEnrollmentMessage);

                unitOfWork.Commit();
            }
        }
    }
}
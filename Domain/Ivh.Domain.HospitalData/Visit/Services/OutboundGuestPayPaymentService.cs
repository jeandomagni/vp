﻿namespace Ivh.Domain.HospitalData.Visit.Services
{
    using System;
    using System.Collections.Generic;
    using Base.Interfaces;
    using Base.Services;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.HospitalData;
    using Entities;
    using Interfaces;
    using NHibernate;

    public class OutboundGuestPayPaymentService : DomainService, IOutboundGuestPayPaymentService
    {
        private readonly IOutboundGuestPayPaymentRepository _outboundGuestPayPaymentRepository;
        private readonly ISession _session;
        

        public OutboundGuestPayPaymentService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            IOutboundGuestPayPaymentRepository outboundGuestPayPaymentRepository,
            ISessionContext<CdiEtl> sessionContext) : base(serviceCommonService)
        {
            this._outboundGuestPayPaymentRepository = outboundGuestPayPaymentRepository;
            this._session = sessionContext.Session;
        }

        public void CreateOutboundGuestPayPayment(GuestPayPaymentMessage guestPayPaymentMessage)
        {
            VpOutboundGuestPayPayment vpOutboundGuestPayPayment = new VpOutboundGuestPayPayment()
            {
                VpGuarantorEmailAddress = guestPayPaymentMessage.VpGuarantorEmailAddress,
                VpGuarantorNumber = guestPayPaymentMessage.VpGuarantorNumber,
                VpGuarantorLastName = guestPayPaymentMessage.VpGuarantorLastName,
                VpGuarantorSourceSystemKey = guestPayPaymentMessage.VpGuarantorSourceSystemKey,
                VpPaymentUnitBillingSystemId = guestPayPaymentMessage.VpPaymentUnitBillingSystemId,
                VpPaymentUnitSourceSystemKey = guestPayPaymentMessage.VpPaymentUnitSourceSystemKey,
                VpPaymentUnitId = guestPayPaymentMessage.VpPaymentUnitId,
                VpPaymentId = guestPayPaymentMessage.VpPaymentId,
                VpPaymentAmount = guestPayPaymentMessage.VpPaymentAmount,
                VpPaymentDate = guestPayPaymentMessage.VpPaymentDate,
                Voided = false,
                VpPaymentMethodTypeId = guestPayPaymentMessage.VpPaymentMethodTypeId,
                MerchantAccountId = guestPayPaymentMessage.MerchantAccountId,
                StatementIdentifierId = guestPayPaymentMessage.StatementIdentifierId,
                PaymentSystemType = guestPayPaymentMessage.PaymentSystemType,
                AchSettlementType = guestPayPaymentMessage.AchSettlementType ?? AchSettlementTypeEnum.None
            };

            this._outboundGuestPayPaymentRepository.Insert(vpOutboundGuestPayPayment);
        }


        public void VoidOutboundGuestPayPayment(GuestPayPaymentVoidMessage guestPayPaymentVoidMessage)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                IList<VpOutboundGuestPayPayment> vpOutboundGuestPayPayments = this._outboundGuestPayPaymentRepository.GetVpOutboundGuestPayPayment(guestPayPaymentVoidMessage.VpPaymentId, guestPayPaymentVoidMessage.VpPaymentUnitId);
                foreach (VpOutboundGuestPayPayment vpOutboundGuestPayPayment in vpOutboundGuestPayPayments)
                {
                    vpOutboundGuestPayPayment.Voided = true;
                    this._outboundGuestPayPaymentRepository.InsertOrUpdate(vpOutboundGuestPayPayment);
                }
                    

                unitOfWork.Commit();
            }
        }
    }
}
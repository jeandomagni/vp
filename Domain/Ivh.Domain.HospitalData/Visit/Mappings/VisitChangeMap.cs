namespace Ivh.Domain.HospitalData.Visit.Mappings
{
    using Change.Entities;
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class VisitChangeMap : ClassMap<VisitChange>
    {
        public VisitChangeMap()
        {
            this.ReadOnly();
            this.Schema(Schemas.Cdi.Base);
            this.Table(nameof(VisitChange));
            this.CompositeId()
                .KeyProperty(x => x.ChangeEventId)
                .KeyProperty(x => x.VisitId);
            this.Map(x => x.DataLoadId).Not.Nullable();
            this.References<ChangeEvent>(x => x.ChangeEvent, "ChangeEventId").Not.Nullable().ReadOnly();
            this.Map(x => x.HsGuarantorId).Not.Nullable();
            this.Map(x => x.BillingSystemId).Not.Nullable();
            this.Map(x => x.SourceSystemKey).Length(400).Not.Nullable();
            this.Map(x => x.VisitDescription).Length(250).Nullable();
            this.Map(x => x.AdmitDate).Nullable();
            this.Map(x => x.DischargeDate).Nullable();
            this.Map(x => x.HsCurrentBalance).Not.Nullable();
            this.Map(x => x.InsuranceBalance).Not.Nullable();
            this.Map(x => x.SelfPayBalance).Not.Nullable();
            this.Map(x => x.FirstSelfPayDate).Nullable();
            this.Map(x => x.PatientFirstName).Length(250).Nullable();
            this.Map(x => x.PatientLastName).Length(250).Nullable();
            this.Map(x => x.PatientDOB).Nullable();
            this.Map(x => x.BillingApplication).Length(2).Nullable();
            this.Map(x => x.SourceSystemKeyDisplay).Length(400).Nullable();
            this.Map(x => x.IsPatientGuarantor).Nullable();
            this.Map(x => x.IsPatientMinor).Nullable();
            this.Map(x => x.InterestRefund).Nullable();
            this.Map(x => x.InterestZero).Nullable();
            this.Map(x => x.ServiceGroupId).Nullable();
            this.Map(x => x.PaymentPriority).Nullable();
            this.Map(x => x.AgingCount).Nullable();
            this.Map(x => x.OnPrePaymentPlan).Nullable();
            this.Map(x => x.FirstFinancePlanOriginationDate).Nullable();
            this.Map(x => x.FirstFinancePlanDuration).Nullable();
            this.Map(x => x.FirstFinancePlanPaymentAmount).Nullable();
            this.Map(x => x.AssignedAgencyId).Nullable();
            this.Map(x => x.VpEligible).Nullable();
            this.Map(x => x.BillingHold).Nullable();
            this.Map(x => x.Redact).Nullable();
            this.Map(x => x.AgingTierId).Nullable();

            this.References(x => x.LifeCycleStage, "LifeCycleStageID").Not.Nullable();
            this.References(x => x.PatientType, "PatientTypeID").Not.Nullable();
            this.References(x => x.PrimaryInsuranceType, "PrimaryInsuranceTypeID").Not.Nullable();
            this.References(x => x.RevenueWorkCompany, "RevenueWorkCompanyID").Not.Nullable();
            this.References(x => x.Facility, "FacilityId").Nullable();
        }
    }
}
﻿namespace Ivh.Domain.HospitalData.Visit.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class VisitMatchStatusMap : ClassMap<VisitMatchStatus>
    {
        public VisitMatchStatusMap()
        {
            this.Schema("Vp");
            this.Table("VisitMatchStatus");
            this.Id(x => x.VisitMatchStatusId);
            this.Map(x => x.InsertDate);
            this.Map(x => x.VisitId).Not.Nullable();
            this.Map(x => x.HsGuarantorId).Not.Nullable();
            this.Map(x => x.VpGuarantorId).Not.Nullable();
            this.Map(x => x.HsGuarantorMatchStatus, "HsGuarantorMatchStatusId").CustomType<HsGuarantorMatchStatusEnum>().Not.Nullable();
        }
    }
}

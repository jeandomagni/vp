﻿namespace Ivh.Domain.HospitalData.Visit.Mappings
{
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class PrimaryInsuranceTypeMap : ClassMap<PrimaryInsuranceType>
    {
        public PrimaryInsuranceTypeMap()
        {
            this.Schema("base");
            this.Table("PrimaryInsuranceType");
            this.Cache.ReadWrite();
            this.Id(x => x.PrimaryInsuranceTypeId).Column("PrimaryInsuranceTypeID");
            this.Map(x => x.PrimaryInsuranceTypeName)
                .Nullable();
            this.Map(x => x.SelfPayClass, "SelfPayClassId")
                .CustomType<SelfPayClassEnum>()
                .Not.Nullable()
                .Not.Update();
        }
    }
}
﻿namespace Ivh.Domain.HospitalData.Visit.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class VisitTransactionChangeMap : ClassMap<VisitTransactionChange>
    {
        public VisitTransactionChangeMap()
        {
            this.ReadOnly();
            this.Schema(Schemas.Cdi.Base);
            this.Table(nameof(VisitTransactionChange));

            this.CompositeId()
                .KeyProperty(x => x.ChangeEventId)
                .KeyProperty(x => x.VisitTransactionId);

            this.Map(x => x.DataLoadId).Not.Nullable();
            this.Map(x => x.BillingSystemId).Not.Nullable();
            this.Map(x => x.SourceSystemKey).Length(400).Not.Nullable();
            this.Map(x => x.VisitId).Not.Nullable();
            this.Map(x => x.TransactionDate).Not.Nullable();
            this.Map(x => x.PostDate).Not.Nullable();
            this.Map(x => x.TransactionAmount).Not.Nullable();
            this.Map(x => x.VpTransactionTypeId).Not.Nullable();
            this.Map(x => x.TransactionDescription).Length(1000).Nullable();
            this.Map(x => x.VpPaymentAllocationId).Nullable();
            this.Map(x => x.TransactionCodeId).Not.Nullable();
            this.Map(x => x.ReassessmentExempt).Not.Nullable();

        }
    }
}
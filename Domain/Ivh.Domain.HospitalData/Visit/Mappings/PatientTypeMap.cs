﻿namespace Ivh.Domain.HospitalData.Visit.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class PatientTypeMap : ClassMap<PatientType>
    {
        public PatientTypeMap()
        {
            this.Schema("base");
            this.Table("PatientType");
            this.Cache.ReadWrite();
            this.Id(x => x.PatientTypeId).Column("PatientTypeID");
            this.Map(x => x.PatientTypeName)
                .Nullable();
        }
    }
}
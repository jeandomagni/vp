﻿namespace Ivh.Domain.HospitalData.Visit.Mappings
{
    using Common.Base.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class VisitSuspensionListMap : ClassMap<VisitSuspensionList>
    {
        public VisitSuspensionListMap()
        {
            this.Schema("base");
            this.Table("VisitSuspensionList");
            this.Id(x => x.VisitId);
            this.Map(x => x.SuspensionList).Not.Nullable();
        }
    }
}

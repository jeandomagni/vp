﻿namespace Ivh.Domain.HospitalData.Visit.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class FacilityMap : ClassMap<Facility>
    {
        public FacilityMap()
        {
            this.Schema(Schemas.Cdi.Base);
            this.Table(nameof(Facility));

            this.Id(x => x.FacilityId);

            this.Map(x => x.FacilityDescription).Length(200).Nullable();
            this.Map(x => x.StateCode).Length(2).Nullable();
            this.Map(x => x.SourceSystemKey).Length(100).Nullable();
            this.Map(x => x.SegmentationEnabled).Nullable();
        }
    }
}
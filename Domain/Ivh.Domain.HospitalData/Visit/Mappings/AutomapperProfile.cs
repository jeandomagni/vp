﻿namespace Ivh.Domain.HospitalData.Visit.Mappings
{
    using AutoMapper;
    using Change.Entities;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Messages.HospitalData;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using Common.VisitPay.Utilities.Helpers;
    using Entities;
    using HsGuarantor.Entities;

    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            // Automapper mapping go here.
            // Use this.CreateMap<,>().
            // DO NOT USE Mapper.CreateMap<,>().
            this.CreateMap<HsGuarantorChange, HsGuarantorChangeDto>()
                .ForMember(dest => dest.SSN4, opts => opts.MapFrom(src => BaseMappingHelper.MapNullableToString(src.SSN4)));
            this.CreateMap<HsGuarantorChangeDto, HsGuarantorChange>();

            this.CreateMapBidirectional<HsGuarantorDto, HsGuarantorChangeDto>();
            this.CreateMapBidirectional<HsGuarantor, HsGuarantorChangeDto>();

            this.CreateMap<ChangeEvent, ChangeEventMessage>()
                .ForMember(dest => dest.ChangeEventType, opts => opts.MapFrom(src => src.ChangeEventType.ChangeEventTypeEnum));

            this.CreateMap<HsGuarantorChange, HsGuarantor>()
                .ForMember(dest => dest.SSN4, opts => opts.MapFrom(src => BaseMappingHelper.MapNullableToString(src.SSN4)))
                .ForMember(dest => dest.HsBillingSystemId, opts => opts.MapFrom(s => s.BillingSystemId));

            this.CreateMap<HsGuarantor, HsGuarantorChange>()
                .ForMember(x => x.BillingSystemId, x => x.MapFrom(s => s.HsBillingSystemId));

            this.CreateMapBidirectional<VisitMatchStatus, VisitMatchStatusDto>();
            
            this.CreateMap<VisitTransaction, VisitTransactionChange>()
                .ForMember(d => d.VisitId, opts => opts.MapFrom(src => src.Visit.VisitId))
                .ForMember(d => d.VpTransactionTypeId, opts => opts.MapFrom(src => src.VpTransactionType.VpTransactionTypeId));
            
            this.CreateMap<VisitTransactionChange, VisitTransaction>()
                .ForMember(dest => dest.VpTransactionType, opts => opts.MapFrom(src => new VpTransactionType { VpTransactionTypeId = src.VpTransactionTypeId }))
                .ForMember(dest => dest.Visit, opts => opts.MapFrom(x => x.VisitChange != null ? Mapper.Map<Visit>(x.VisitChange) : null))
                .ForMember(dest => dest.TransactionCodeId, opts => opts.MapFrom(x => x.TransactionCodeId));

        }
    }
}
﻿namespace Ivh.Domain.HospitalData.Visit.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class RealTimeVisitEnrollmentMap : ClassMap<RealTimeVisitEnrollment>
    {
        public RealTimeVisitEnrollmentMap()
        {
            this.Schema(Schemas.Cdi.Base);
            this.Table(nameof(RealTimeVisitEnrollment));
            this.Id(x => x.RealTimeVisitEnrollmentId);
            this.Map(x => x.VisitId).Not.Nullable();
            this.Map(x => x.HsGuarantorId).Not.Nullable();
            this.Map(x => x.BillingSystemId).Not.Nullable();
            this.Map(x => x.BillingHold).Not.Nullable();
            this.Map(x => x.CreateDate).Not.Nullable();
            this.Map(x => x.OverrideExpireDate).Nullable();
        }
    }
}
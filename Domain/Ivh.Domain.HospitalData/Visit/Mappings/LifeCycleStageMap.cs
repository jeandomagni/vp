﻿namespace Ivh.Domain.HospitalData.Visit.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class LifeCycleStageMap : ClassMap<LifeCycleStage>
    {
        public LifeCycleStageMap()
        {
            this.Schema("base");
            this.Table("LifeCycleStage");
            this.Cache.ReadWrite();
            this.Id(x => x.LifeCycleStageId).Column("LifeCycleStageID");
            this.Map(x => x.LifeCycleStageName)
                .Nullable();
        }
    }
}
namespace Ivh.Domain.HospitalData.Visit.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;
    using Ivh.Common.Data.Constants;

    public class VisitPaymentAllocationChangeMap : ClassMap<VisitPaymentAllocationChange>
    {
        public VisitPaymentAllocationChangeMap()
        {
            this.ReadOnly();
            this.Schema(Schemas.Cdi.Base);
            this.Table(nameof(VisitPaymentAllocationChange));
            this.CompositeId()
                .KeyProperty(x => x.ChangeEventId)
                .KeyProperty(x => x.PaymentAllocationId);
            this.Map(x => x.DataLoadId).Not.Nullable();
            this.Map(x => x.VisitId).Not.Nullable();
        }
    }
}
﻿namespace Ivh.Domain.HospitalData.Visit.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class VpOutboundVisitMap : ClassMap<VpOutboundVisit>
    {
        public VpOutboundVisitMap()
        {
            this.Schema("Vp");
            this.Table("VpOutboundVisit");
            this.Id(x => x.VpOutboundVisitId);
            this.References(x => x.Visit).Column("VisitId").Not.Nullable();
            this.Map(x => x.BillingSystemId).Not.Nullable();
            this.Map(x => x.SourceSystemKey).Not.Nullable();
            this.Map(x => x.VpVisitId).Not.Nullable();
            this.Map(x => x.VpOutboundVisitMessageType).Column("VpOutboundVisitMessageTypeId").CustomType<VpOutboundVisitMessageTypeEnum>();
            this.Map(x => x.ActionDate).Not.Nullable();
            this.Map(x => x.InsertDate);
            this.Map(x => x.ProcessedDate).Nullable();
            this.Map(x => x.Notes).Nullable();
            this.Map(x => x.OutboundLoadTrackerId).Nullable();
            this.Map(x => x.OutboundValue).Nullable();
            this.Map(x => x.OriginalFinancePlanDuration).Nullable();
            this.Map(x => x.FinancePlanClosedReasonEnum).Column("FinancePlanClosedReasonId").CustomType<FinancePlanClosedReasonEnum?>().Nullable();
            this.Map(x => x.IsCallCenter).Nullable();
            this.Map(x => x.IsAutoPay).Nullable();
            this.Map(x => x.HsGuarantorId).Nullable();
        }
    }
}
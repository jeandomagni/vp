﻿namespace Ivh.Domain.HospitalData.Visit.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class InsurancePlanMap : ClassMap<InsurancePlan>
    {
        public InsurancePlanMap()
        {
            this.Schema("base");
            this.Table("InsurancePlan");
            this.Cache.ReadWrite();
            this.Id(x => x.InsurancePlanId).GeneratedBy.Assigned();
            this.Map(x => x.BillingSystemId).Not.Nullable();
            this.Map(x => x.SourceSystemKey).Length(100).Not.Nullable();
            this.Map(x => x.InsurancePlanName).Length(100).Nullable();

            this.References(x => x.PrimaryInsuranceType)
                .Column("PrimaryInsuranceTypeId")
                .Not.Nullable();
        }
    }
}
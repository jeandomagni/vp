﻿namespace Ivh.Domain.HospitalData.Visit.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class VisitTransactionMap : ClassMap<VisitTransaction>
    {
        public VisitTransactionMap()
        {
            this.Schema("base");
            this.Table("VisitTransaction");
            this.Id(x => x.VisitTransactionId).GeneratedBy.Sequence("etl.base_VisitTransactionId_sequence").Default("next value for etl.base_VisitTransactionId_sequence");
            this.Map(x => x.BillingSystemId).Not.Nullable();
            this.Map(x => x.SourceSystemKey).Not.Nullable().Length(400);

            this.References(x => x.Visit)
                .Column("VisitId")
                .Not.Nullable();

            this.Map(x => x.TransactionDate).Not.Nullable();
            this.Map(x => x.PostDate).Not.Nullable();
            this.Map(x => x.TransactionAmount).Not.Nullable();

            this.Map(x => x.TransactionDescription).Length(4000);
            this.Map(x => x.VpPaymentAllocationId);
            this.Map(x => x.ReassessmentExempt).Not.Nullable();
            this.Map(x => x.DataLoadId).Nullable();

            this.Map(x => x.TransactionCodeId)
                .Column("TransactionCodeID")
                .Not.Nullable();

            this.References(x => x.VpTransactionType)
                .Column("VpTransactionTypeId")
                .Not.Nullable();
        }
    }
}
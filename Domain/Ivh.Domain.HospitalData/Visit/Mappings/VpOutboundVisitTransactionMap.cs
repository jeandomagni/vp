﻿namespace Ivh.Domain.HospitalData.Visit.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class VpOutboundVisitTransactionMap : ClassMap<VpOutboundVisitTransaction>
    {
        public VpOutboundVisitTransactionMap()
        {
            this.Schema("Vp");
            this.Table("VpOutboundVisitTransaction");
            this.Id(x => x.VpOutboundVisitTransactionId);
            this.Map(x => x.VpPaymentAllocationId).Nullable();
            this.Map(x => x.VpPaymentAllocationTypeId).Nullable();
            this.Map(x => x.VpPaymentAllocationOpposingPaymentAllocationId).Nullable();
            this.Map(x => x.InsertDate);
            this.Map(x => x.TransactionAmount).Not.Nullable();
            this.Map(x => x.ProcessedDate).Nullable();
            this.Map(x => x.VisitBillingSystemId).Not.Nullable();
            this.Map(x => x.VisitSourceSystemKey).Not.Nullable();
            this.Map(x => x.VpPaymentId).Nullable();
            this.Map(x => x.VpPaymentType).CustomType<PaymentTypeEnum>().Column("VpPaymentTypeId").Nullable();
            this.Map(x => x.VpPaymentActualPaymentDate).Nullable();
            this.Map(x => x.VpPaymentAmount);
            this.Map(x => x.VpVisitTransactionInsertDate).Nullable();
            this.Map(x => x.VpParentPaymentId).Nullable();
            this.Map(x => x.VpFinancePlanId).Nullable();
            this.Map(x => x.VpGuarantorId).Nullable();
            this.Map(x => x.FinancePlanDuration).Nullable();
            this.Map(x => x.FinancePlanOriginalDuration).Nullable();
            this.Map(x => x.VpPaymentMethodType).CustomType<PaymentMethodTypeEnum>().Column("VpPaymentMethodTypeId").Nullable();
            this.Map(x => x.MerchantAccountId).Nullable();
            this.Map(x => x.VpFinancePlanVisitInterestDueId).Nullable();
            this.Map(x => x.VpFinancePlanVisitInterestDueParentId).Nullable();
            this.Map(x => x.VpFinancePlanVisitInterestDueTypeId).Nullable();
            this.Map(x => x.VpFinancePlanVisitInterestInsertDate).Nullable();
            this.Map(x => x.VpFinancePlanVisitInterestAmount).Nullable();
            this.Map(x => x.AchSettlementType).CustomType<AchSettlementTypeEnum>().Column("AchSettlementTypeId").Nullable();
            this.References(x => x.Visit).Column("VisitId").Not.Nullable();
            this.References(x => x.BalanceTransferStatus).Column("BalanceTransferStatusId").Nullable();
        }
    }
}
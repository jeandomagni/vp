﻿namespace Ivh.Domain.HospitalData.Visit.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class VisitInsurancePlanChangeMap : ClassMap<VisitInsurancePlanChange>
    {
        public VisitInsurancePlanChangeMap()
        {
            this.ReadOnly();
            this.Schema(Schemas.Cdi.Base);
            this.Table(nameof(VisitInsurancePlanChange));

            this.CompositeId()
                .KeyProperty(x => x.ChangeEventId)
                .KeyProperty(x => x.InsurancePlanId);

            this.Map(x => x.DataLoadId).Not.Nullable();
            this.Map(x => x.VisitId).Not.Nullable();
            this.Map(x => x.PayOrder).Not.Nullable();
            this.References(x => x.InsurancePlan, "InsurancePlanId").Not.Nullable().ReadOnly();
        }
    }
}
﻿namespace Ivh.Domain.HospitalData.Visit.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class VisitInsurancePlanMap : ClassMap<VisitInsurancePlan>
    {
        public VisitInsurancePlanMap()
        {
            this.Schema("base");
            this.Table("VisitInsurancePlan");
            this.Id(x => x.VisitInsurancePlanId);
            this.References(x => x.Visit, "VisitId").Not.Nullable();
            this.Map(x => x.PayOrder).Not.Nullable();
            this.Map(x => x.DataLoadId).Nullable();
            this.References(x => x.InsurancePlan, "InsurancePlanId").Not.Nullable();
        }
    }
}
﻿namespace Ivh.Domain.HospitalData.Visit.Mappings
{
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class VisitMap : ClassMap<Visit>
    {
        public VisitMap()
        {
            this.Schema(Schemas.Cdi.Base);
            this.Table(nameof(Visit));
            this.Id(x => x.VisitId).GeneratedBy.Sequence("base.VisitId_Sequence").Default("next value for base.VisitId_Sequence");
            this.Map(x => x.HsGuarantorId).Not.Nullable();
            this.Map(x => x.BillingSystemId).Not.Nullable();
            this.Map(x => x.SourceSystemKey).Length(400).Not.Nullable();
            this.Map(x => x.SourceSystemKeyDisplay).Length(400).Nullable();
            this.Map(x => x.VisitDescription).Length(250).Nullable();
            this.Map(x => x.AdmitDate).Nullable();
            this.Map(x => x.DischargeDate).Nullable();
            this.Map(x => x.HsCurrentBalance).Not.Nullable();
            this.Map(x => x.InsuranceBalance).Not.Nullable();
            this.Map(x => x.SelfPayBalance).Not.Nullable();
            this.Map(x => x.FirstSelfPayDate).Nullable();
            this.Map(x => x.PatientFirstName).Length(250).Nullable();
            this.Map(x => x.PatientLastName).Length(250).Nullable();
            this.Map(x => x.PatientDOB).Nullable();
            this.Map(x => x.BillingApplication).Length(2).Nullable();
            this.Map(x => x.IsPatientGuarantor).Nullable();
            this.Map(x => x.IsPatientMinor).Nullable();
            this.Map(x => x.InterestRefund).Nullable();
            this.Map(x => x.InterestZero).Nullable();
            this.Map(x => x.ServiceGroupId).Nullable();
            this.Map(x => x.PaymentPriority).Nullable();
            this.Map(x => x.AgingCount).Nullable();
            this.Map(x => x.OnPrePaymentPlan).Nullable();
            this.Map(x => x.VpEligible).Nullable();
            this.Map(x => x.BillingHold).Nullable();
            this.Map(x => x.Redact).Nullable();
            this.Map(x => x.AgingTierId).Nullable();
            this.Map(x => x.DataLoadId).Nullable();
            this.Map(x => x.DataChangeDate).Nullable();
            this.Map(x => x.EvaluateForPCSegmentation).Nullable();

            this.References(x => x.Facility)
                .Column("FacilityId")
                .NotFound.Ignore()
                .Not.Nullable();

            this.References(x => x.PrimaryInsuranceType)
                .Column("PrimaryInsuranceTypeID")
                .Not.Nullable();

            this.References(x => x.RevenueWorkCompany)
                .Column("RevenueWorkCompanyID")
                .Not.Nullable();

            this.References(x => x.PatientType)
                .Column("PatientTypeID")
                .Not.Nullable();

            this.References(x => x.LifeCycleStage)
                .Column("LifeCycleStageID")
                .Not.Nullable();

            this.HasMany(x => x.VisitPaymentAllocations)
                .BatchSize(50)
                .KeyColumn("VisitId")
                .Not.LazyLoad()
                .Cascade.All();

            this.HasMany(x => x.VisitTransactions)
                .BatchSize(50)
                .KeyColumn("VisitId")
                .Not.LazyLoad()
                .Cascade.All();

            this.HasMany(x => x.VisitInsurancePlans)
                .BatchSize(50)
                .KeyColumn("VisitId")
                .Not.LazyLoad()
                .Cascade.All();

            this.References(x => x.BillingSystem)
                .Column("BillingSystemId")
                .Not.Insert()
                .Not.Update();

        }
    }
}
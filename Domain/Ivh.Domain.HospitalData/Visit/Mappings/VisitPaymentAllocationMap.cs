namespace Ivh.Domain.HospitalData.Visit.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class VisitPaymentAllocationMap : ClassMap<VisitPaymentAllocation>
    {
        public VisitPaymentAllocationMap()
        {
            this.Schema("base");
            this.Table("VisitPaymentAllocation");
            this.Id(x => x.VisitPaymentAllocationId);
            this.Map(x => x.PaymentAllocationId).Not.Nullable();
            this.Map(x => x.DataLoadId).Nullable();
            this.References(x => x.Visit, "VisitId").Not.Nullable();
        }
    }
}
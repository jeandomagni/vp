namespace Ivh.Domain.HospitalData.Visit.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class RevenueWorkCompanyMap : ClassMap<RevenueWorkCompany>
    {
        public RevenueWorkCompanyMap()
        {
            this.Schema("base");
            this.Table("RevenueWorkCompany");
            this.Cache.ReadWrite();
            this.Id(x => x.RevenueWorkCompanyId).Column("RevenueWorkCompanyID");
            this.Map(x => x.RevenueWorkCompanyName)
                .Nullable();
        }
    }
}
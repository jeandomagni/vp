﻿namespace Ivh.Domain.HospitalData.Visit.Mappings
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class VpOutboundGuestPayPaymentMap : ClassMap<VpOutboundGuestPayPayment>
    {
        public VpOutboundGuestPayPaymentMap()
        {
            this.Schema("Vp");
            this.Table("VpOutboundGuestPayPayment");
            this.Id(x => x.VpOutboundGuestPayPaymentId);
            this.Map(x => x.InsertDate);

            this.Map(x => x.VpGuarantorEmailAddress).Not.Nullable();
            this.Map(x => x.VpGuarantorNumber).Not.Nullable();
            this.Map(x => x.VpGuarantorLastName).Not.Nullable();
            this.Map(x => x.VpGuarantorSourceSystemKey).Not.Nullable();
            
            this.Map(x => x.VpPaymentUnitBillingSystemId).Not.Nullable();
            this.Map(x => x.VpPaymentUnitSourceSystemKey).Not.Nullable();
            this.Map(x => x.VpPaymentUnitId).Nullable();
            
            this.Map(x => x.VpPaymentId).Not.Nullable();
            this.Map(x => x.VpPaymentAmount).Not.Nullable();
            this.Map(x => x.VpPaymentDate).Nullable();
            this.Map(x => x.Voided).Not.Nullable();
            this.Map(x => x.VpPaymentMethodTypeId).Nullable();
            this.Map(x => x.MerchantAccountId).Nullable();
            this.Map(x => x.StatementIdentifierId).Not.Nullable();
            this.Map(x => x.PaymentSystemType).Column("PaymentSystemTypeId").CustomType<PaymentSystemTypeEnum>();
            this.Map(x => x.AchSettlementType).CustomType<AchSettlementTypeEnum>().Column("AchSettlementTypeId").Nullable();
        }
    }
}
﻿namespace Ivh.Domain.HospitalData.Visit.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class TransactionCodeMap : ClassMap<TransactionCode>
    {
        public TransactionCodeMap()
        {
            this.Schema("base");
            this.Table("TransactionCode");
            this.Id(x => x.TransactionCodeId);
            this.Map(x => x.SourceSystemKey);
            this.Map(x => x.TransactionCodeName);
            this.Map(x => x.VpTransactionTypeId);
            this.Map(x => x.BillingSystemId);
            this.Map(x => x.AmountMultiplier);
            this.Map(x => x.TranSskAppend);
        }
    }
}
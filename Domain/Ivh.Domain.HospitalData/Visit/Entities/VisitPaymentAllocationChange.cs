﻿namespace Ivh.Domain.HospitalData.Visit.Entities
{
    using System;

    public class VisitPaymentAllocationChange
    {
        public virtual int ChangeEventId { get; set; }
        public virtual int VisitId { get; set; }
        public virtual int DataLoadId { get; set; }
        public virtual int PaymentAllocationId { get; set; }
        public virtual VisitChange VisitChange { get; set; }

        public override bool Equals(Object obj)
        {
            VisitPaymentAllocationChange t = obj as VisitPaymentAllocationChange;
            if (t == null)
                return false;
            if (this.DataLoadId == t.DataLoadId
                && this.ChangeEventId == t.ChangeEventId
                && (
                    (this.VisitChange != null && t.VisitChange != null && this.VisitChange.ChangeEventId == t.VisitChange.ChangeEventId)
                    || (this.VisitChange == null && t.VisitChange == null)
                )
                && this.VisitId == t.VisitId
                && this.PaymentAllocationId == t.PaymentAllocationId
            )
                return true;
            return false;
        }

        public override int GetHashCode()
        {
            return (
                this.DataLoadId + "|"
                                + this.ChangeEventId + "|"
                                + (this.VisitChange != null ? this.VisitChange.ChangeEventId.ToString() : "") + "|"
                                + this.VisitId + "|"
                                + this.PaymentAllocationId
            ).GetHashCode();
        }
    }
}
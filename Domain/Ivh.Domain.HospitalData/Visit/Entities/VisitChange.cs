﻿namespace Ivh.Domain.HospitalData.Visit.Entities
{
    using System;
    using System.Collections.Generic;
    using Change.Entities;
    using Common.Base.Utilities.Helpers;

    public class VisitChange
    {
        //IDENTITY
        public virtual int ChangeEventId { get; set; }

        //UNIQUE CONSTRAINT
        public virtual int DataLoadId { get; set; }
        public virtual int VisitId { get; set; }

        public virtual ChangeEvent ChangeEvent { set; get; }
        public virtual int HsGuarantorId { get; set; }
        public virtual int BillingSystemId { get; set; }
        public virtual string SourceSystemKey { get; set; }
        public virtual string VisitDescription { get; set; }
        public virtual DateTime? AdmitDate { get; set; }
        public virtual DateTime? DischargeDate { get; set; }
        public virtual decimal HsCurrentBalance { get; set; }
        public virtual decimal InsuranceBalance { get; set; }
        public virtual decimal SelfPayBalance { get; set; }
        public virtual DateTime? FirstSelfPayDate { get; set; }
        public virtual string PatientFirstName { get; set; }
        public virtual string PatientLastName { get; set; }
        public virtual DateTime? PatientDOB { get; set; }
        public virtual string BillingApplication { get; set; }

        public virtual PrimaryInsuranceType PrimaryInsuranceType { get; set; }
        public virtual RevenueWorkCompany RevenueWorkCompany { get; set; }
        public virtual PatientType PatientType { get; set; }
        public virtual LifeCycleStage LifeCycleStage { get; set; }
        public virtual Facility Facility { get; set; }
        public virtual int? AgingCount { get; set; }
        public virtual bool? OnPrePaymentPlan { get; set; }

        public virtual IList<VisitTransactionChange> VisitTransactions { get; set; }

        public virtual IList<VisitInsurancePlanChange> VisitInsurancePlans { get; set; }

        public virtual IList<VisitPaymentAllocationChange> VisitPaymentAllocations { get; set; }

        public virtual string SourceSystemKeyDisplay { get; set; }

        public virtual bool? IsPatientGuarantor { get; set; }
        public virtual bool? IsPatientMinor { get; set; }
        public virtual bool? InterestRefund { get; set; }
        public virtual bool? InterestZero { get; set; }
        public virtual int? ServiceGroupId { get; set; }
        public virtual int? PaymentPriority { get; set; }

        public virtual DateTime? FirstFinancePlanOriginationDate { get; set; }
        public virtual int? FirstFinancePlanDuration { get; set; }
        public virtual decimal? FirstFinancePlanPaymentAmount { get; set; }
        public virtual int? AssignedAgencyId { get; set; }
        public virtual bool? VpEligible { get; set; }
        public virtual bool? BillingHold { get; set; }
        public virtual bool? Redact { get; set; }
        public virtual int? AgingTierId { get; set; }//NOT SURE THIS GOES HERE. Pending Aging Service Definition.


        public virtual string PatientLastNameFirstName => FormatHelper.LastNameFirstName(this.PatientLastName, this.PatientFirstName);
        public virtual string PatientFirstNameLastName => FormatHelper.FirstNameLastName(this.PatientFirstName, this.PatientLastName);

        public override bool Equals(Object obj)
        {
            VisitChange t = obj as VisitChange;
            if (t == null)
                return false;
            if (this.DataLoadId == t.DataLoadId
                && this.VisitId == t.VisitId
                && this.ChangeEventId == t.ChangeEventId
                && this.HsGuarantorId == t.HsGuarantorId
                && this.BillingSystemId == t.BillingSystemId
                && this.SourceSystemKey == t.SourceSystemKey
                && this.VisitDescription == t.VisitDescription
                && this.AdmitDate == t.AdmitDate
                && this.DischargeDate == t.DischargeDate
                && this.HsCurrentBalance == t.HsCurrentBalance
                && this.InsuranceBalance == t.InsuranceBalance
                && this.SelfPayBalance == t.SelfPayBalance
                && this.PatientFirstName == t.PatientFirstName
                && this.PatientLastName == t.PatientLastName
                && this.PatientDOB == t.PatientDOB
                && this.BillingApplication == t.BillingApplication
                && this.LifeCycleStage == t.LifeCycleStage
                && this.PatientType == t.PatientType
                && this.PrimaryInsuranceType == t.PrimaryInsuranceType
                && this.RevenueWorkCompany == t.RevenueWorkCompany
                && this.SourceSystemKeyDisplay == t.SourceSystemKeyDisplay
                && this.IsPatientGuarantor == t.IsPatientGuarantor
                && this.IsPatientMinor == t.IsPatientMinor
                && this.InterestRefund == t.InterestRefund
                && this.InterestZero == t.InterestZero
                && this.ServiceGroupId == t.ServiceGroupId
                && this.PaymentPriority == t.PaymentPriority
                && this.Facility == t.Facility
                && this.FirstFinancePlanOriginationDate == t.FirstFinancePlanOriginationDate
                && this.FirstFinancePlanDuration == t.FirstFinancePlanDuration
                && this.FirstFinancePlanPaymentAmount == t.FirstFinancePlanPaymentAmount
                && this.AssignedAgencyId == t.AssignedAgencyId
                && this.VpEligible == t.VpEligible
                && this.BillingHold == t.BillingHold
                && this.Redact == t.Redact
                && this.AgingTierId == t.AgingTierId)
                return true;
            return false;
        }
        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }

        public override string ToString()
        {
            return (
                this.DataLoadId + "|"
                + this.VisitId + "|"
                + this.ChangeEventId + "|"
                + this.HsGuarantorId + "|"
                + this.BillingSystemId + "|"
                + this.SourceSystemKey + "|"
                + this.VisitDescription + "|"
                + this.AdmitDate + "|"
                + this.DischargeDate + "|"
                + this.HsCurrentBalance + "|"
                + this.InsuranceBalance + "|"
                + this.SelfPayBalance + "|"
                + this.PatientFirstName + "|"
                + this.PatientLastName + "|"
                + this.PatientDOB + "|"
                + this.BillingApplication + "|"
                + this.LifeCycleStage + "|"
                + this.PatientType + "|"
                + this.PrimaryInsuranceType + "|"
                + this.RevenueWorkCompany + "|"
                + this.SourceSystemKeyDisplay + "|"
                + this.IsPatientGuarantor + "|"
                + this.IsPatientMinor + "|"
                + this.InterestRefund + "|"
                + this.InterestZero + "|"
                + this.ServiceGroupId + "|"
                + this.PaymentPriority + "|"
                + this.FirstFinancePlanOriginationDate + "|"
                + this.FirstFinancePlanDuration + "|"
                + this.FirstFinancePlanPaymentAmount + "|"
                + this.AssignedAgencyId + "|"
                + this.VpEligible + "|"
                + this.BillingHold + "|"
                + this.Redact + "|"
                + this.AgingTierId + "|"
                + this.Facility);
        }
    }
}
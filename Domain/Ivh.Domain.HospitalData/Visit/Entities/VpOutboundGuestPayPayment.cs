﻿namespace Ivh.Domain.HospitalData.Visit.Entities
{
    using System;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class VpOutboundGuestPayPayment
    {
        public VpOutboundGuestPayPayment()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int VpOutboundGuestPayPaymentId { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual string VpGuarantorEmailAddress { get; set; }
        public virtual string VpGuarantorNumber { get; set; }
        public virtual string VpGuarantorLastName { get; set; }
        public virtual string VpGuarantorSourceSystemKey { get; set; }
        public virtual int VpPaymentUnitBillingSystemId { get; set; }
        public virtual string VpPaymentUnitSourceSystemKey { get; set; }
        public virtual int? VpPaymentUnitId { get; set; }
        public virtual int VpPaymentId { get; set; }
        public virtual decimal VpPaymentAmount { get; set; }
        public virtual DateTime? VpPaymentDate { get; set; }
        public virtual bool Voided { get; set; }
        public virtual int VpPaymentMethodTypeId { get; set; }
        public virtual int? MerchantAccountId { get; set; }
        public virtual int StatementIdentifierId { get; set; }
        public virtual PaymentSystemTypeEnum PaymentSystemType { get; set; }
        public virtual AchSettlementTypeEnum AchSettlementType { get; set; }
    }
}
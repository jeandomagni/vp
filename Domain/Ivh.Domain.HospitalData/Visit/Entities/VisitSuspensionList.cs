﻿namespace Ivh.Domain.HospitalData.Visit.Entities
{
    public class VisitSuspensionList
    {
        public virtual int VisitId { get; set; }
        public virtual string SuspensionList { get; set; }
    }
}
﻿namespace Ivh.Domain.HospitalData.Visit.Entities
{
    public class VisitPaymentAllocation
    {
        public virtual int VisitPaymentAllocationId { get; set; }
        public virtual Visit Visit { get; set; }
        public virtual int PaymentAllocationId { get; set; }
        public virtual int? DataLoadId { get; set; }
    }
}
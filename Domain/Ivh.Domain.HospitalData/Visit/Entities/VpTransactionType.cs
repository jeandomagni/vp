﻿namespace Ivh.Domain.HospitalData.Visit.Entities
{
    public class VpTransactionType
    {
        public virtual int VpTransactionTypeId { get; set; }
        public virtual string TransactionType { get; set; }
        public virtual string TransactionGroup { get; set; }
        public virtual string VpTransactionTypeName { get; set; }
        public virtual string DisplayName { get; set; }
        public virtual string TransactionSource { get; set; }
        public virtual string Notes { get; set; }
    }
}

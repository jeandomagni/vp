﻿namespace Ivh.Domain.HospitalData.Visit.Entities
{
    using System;
    using Common.VisitPay.Enums;

    public class VpOutboundVisit
    {
        public VpOutboundVisit()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int VpOutboundVisitId { get; set; }
        public virtual Visit Visit { get; set; }
        public virtual int BillingSystemId { get; set; }
        public virtual string SourceSystemKey { get; set; }
        public virtual int VpVisitId { get; set; }
        public virtual VpOutboundVisitMessageTypeEnum VpOutboundVisitMessageType { get; set; }
        public virtual DateTime ActionDate { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual DateTime? ProcessedDate { get; set; }
        public virtual string Notes { get; set; }
        public virtual int? OutboundLoadTrackerId { get; set; }
        public virtual string OutboundValue { get; set; }
        public virtual int? OriginalFinancePlanDuration { get; set; }
        public virtual FinancePlanClosedReasonEnum? FinancePlanClosedReasonEnum { get; set; }
        public virtual bool? IsCallCenter { get; set; }
        public virtual bool? IsAutoPay { get; set; }
        public virtual int? HsGuarantorId { get; set; }
    }
}

	
namespace Ivh.Domain.HospitalData.Visit.Entities
{
    using Common.VisitPay.Enums;
    using Scoring.Enums;

    public class PrimaryInsuranceType
    {
        public virtual int PrimaryInsuranceTypeId { get; set; }
        public virtual string PrimaryInsuranceTypeName { get; set; }
        public virtual SelfPayClassEnum SelfPayClass { get; set; }
    }
}
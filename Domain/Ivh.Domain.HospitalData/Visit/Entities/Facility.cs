﻿namespace Ivh.Domain.HospitalData.Visit.Entities
{
    public class Facility
    {
        public virtual int FacilityId { get; set; }

        public virtual string FacilityDescription { get; set; }

        public virtual string StateCode { get; set; }

        public virtual string SourceSystemKey { get; set; }

        public virtual bool SegmentationEnabled { get; set; }
    }
}
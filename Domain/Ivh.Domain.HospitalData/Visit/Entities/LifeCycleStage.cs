namespace Ivh.Domain.HospitalData.Visit.Entities
{
    public class LifeCycleStage
    {
        public virtual int LifeCycleStageId { get; set; }
        public virtual string LifeCycleStageName { get; set; }
        /*	[LifeCycleStageID] [tinyint] IDENTITY(1,1) NOT NULL,
	[LifeCycleStageName] [varchar](250) NULL,*/
    }
}
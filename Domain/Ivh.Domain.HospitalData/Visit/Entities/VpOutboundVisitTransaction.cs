﻿namespace Ivh.Domain.HospitalData.Visit.Entities
{
    using System;
    using Common.VisitPay.Enums;

    public class VpOutboundVisitTransaction
    {
        public VpOutboundVisitTransaction()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int VpOutboundVisitTransactionId { get; set; }
        public virtual int? VpPaymentAllocationId { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual decimal TransactionAmount { get; set; }
        public virtual DateTime? ProcessedDate { get; set; }
        public virtual int VisitBillingSystemId { get; set; }
        public virtual string VisitSourceSystemKey { get; set; }
        public virtual int? VpPaymentId { get; set; }
        public virtual Visit Visit { get; set; }
        public virtual PaymentTypeEnum? VpPaymentType { get; set; }
        public virtual DateTime? VpPaymentActualPaymentDate { get; set; }
        public virtual decimal VpPaymentAmount { get; set; }
        public virtual DateTime? VpVisitTransactionInsertDate { get; set; }
        public virtual int? VpParentPaymentId { get; set; }
        public virtual PaymentMethodTypeEnum? VpPaymentMethodType { get; set; }
        public virtual int? VpFinancePlanId { get; set; }
        public virtual int? FinancePlanDuration { get; set; }
        public virtual int? FinancePlanOriginalDuration { get; set; }
        public virtual int? VpGuarantorId { get; set; }
        public virtual BalanceTransferStatus BalanceTransferStatus { get; set; }
        public virtual int? MerchantAccountId { get; set; }
        public virtual int? VpPaymentAllocationTypeId { get; set; }
        public virtual int? VpPaymentAllocationOpposingPaymentAllocationId { get; set; }
        public virtual int? VpFinancePlanVisitInterestDueId { get; set; }
        public virtual int? VpFinancePlanVisitInterestDueParentId { get; set; }
        public virtual int? VpFinancePlanVisitInterestDueTypeId { get; set; }
        public virtual DateTime? VpFinancePlanVisitInterestInsertDate { get; set; }
        public virtual decimal? VpFinancePlanVisitInterestAmount { get; set; }
        public virtual AchSettlementTypeEnum AchSettlementType { get; set; }
    }
}
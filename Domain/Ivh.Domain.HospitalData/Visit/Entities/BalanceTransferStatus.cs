﻿namespace Ivh.Domain.HospitalData.Visit.Entities
{
    public class BalanceTransferStatus
    {
        public virtual int BalanceTransferStatusId { get; set; }
        public virtual string BalanceTransferStatusName { get; set; }
        public virtual string BalanceTransferStatusDisplayName { get; set; }
    }
}
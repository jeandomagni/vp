﻿namespace Ivh.Domain.HospitalData.Visit.Entities
{
    using System;

    public class VisitTransaction
    {
        public VisitTransaction()
        {
            //Hard coding this to the first transactioncodeid
            this.TransactionCodeId = 1;
        }

        public virtual int VisitTransactionId { get; set; }
        public virtual int BillingSystemId { get; set; }
        public virtual string SourceSystemKey { get; set; }
        public virtual Visit Visit { get; set; }
        public virtual DateTime TransactionDate { get; set; }
        public virtual DateTime PostDate { get; set; }
        public virtual decimal TransactionAmount { get; set; }
        public virtual VpTransactionType VpTransactionType { get; set; }
        public virtual string TransactionDescription { get; set; }
        public virtual int? VpPaymentAllocationId { get; set; }
        public virtual int TransactionCodeId { get; set; }
        public virtual bool ReassessmentExempt { get; set; }
        public virtual int? DataLoadId { get; set; }
    }
}

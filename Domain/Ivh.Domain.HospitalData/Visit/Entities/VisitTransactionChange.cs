namespace Ivh.Domain.HospitalData.Visit.Entities
{
    using System;

    public class VisitTransactionChange
    {
        public virtual int DataLoadId { get; set; }
        public virtual int ChangeEventId { get; set; }
        public virtual int VisitTransactionId { get; set; }
        public virtual VisitChange VisitChange { get; set; }
        public virtual int BillingSystemId { get; set; }
        public virtual string SourceSystemKey { get; set; }
        public virtual int VisitId { get; set; }
        public virtual DateTime TransactionDate { get; set; }
        public virtual DateTime PostDate { get; set; }
        public virtual decimal TransactionAmount { get; set; }
        public virtual int VpTransactionTypeId { get; set; }
        public virtual string TransactionDescription { get; set; }
        public virtual int? VpPaymentAllocationId { get; set; }
        public virtual int TransactionCodeId { get; set; }
        public virtual string TransactionCodeSourceSystemKey { get; set; }
        public virtual int TransactionCodeBillingSystemId { get; set; }
        public virtual bool ReassessmentExempt { get; set; }

        //public virtual TransactionCode TransactionCode { get; set; }

        public override bool Equals(Object obj)
        {
            VisitTransactionChange t = obj as VisitTransactionChange;
            if (t == null)
                return false;
            if (this.DataLoadId == t.DataLoadId
                 && this.ChangeEventId == t.ChangeEventId
                 && (
                     (this.VisitChange != null && t.VisitChange != null && this.VisitChange.ChangeEventId == t.VisitChange.ChangeEventId)
                     || (this.VisitChange == null && t.VisitChange == null)
                     )
                 && this.BillingSystemId == t.BillingSystemId
                 && this.SourceSystemKey == t.SourceSystemKey
                 && this.VisitId == t.VisitId
                 && this.TransactionDate == t.TransactionDate
                 && this.PostDate == t.PostDate
                 && this.TransactionAmount == t.TransactionAmount
                 && this.VpTransactionTypeId == t.VpTransactionTypeId
                 && this.TransactionDescription == t.TransactionDescription
                 && this.VpPaymentAllocationId == t.VpPaymentAllocationId
                 && this.TransactionCodeId == t.TransactionCodeId
                 )
                return true;
            return false;
        }
        public override int GetHashCode()
        {
            return (
                this.DataLoadId + "|"
                + this.ChangeEventId + "|"
                + (this.VisitChange != null ? this.VisitChange.ChangeEventId.ToString() : "") + "|"
                + this.BillingSystemId + "|"
                + this.SourceSystemKey + "|"
                + this.VisitId + "|"
                + this.TransactionDate + "|"
                + this.PostDate + "|"
                + this.TransactionAmount + "|"
                + this.VpTransactionTypeId + "|"
                + this.TransactionDescription + "|"
                + this.VpPaymentAllocationId + "|"
                + this.TransactionCodeId
                ).GetHashCode();
        }

        public override string ToString()
        {
            return (
                this.DataLoadId + "|"
                + this.ChangeEventId + "|"
                + (this.VisitChange != null ? this.VisitChange.ChangeEventId.ToString() : "") + "|"
                + this.BillingSystemId + "|"
                + this.SourceSystemKey + "|"
                + this.VisitId + "|"
                + this.TransactionDate + "|"
                + this.PostDate + "|"
                + this.TransactionAmount + "|"
                + this.VpTransactionTypeId + "|"
                + this.TransactionDescription + "|"
                + this.VpPaymentAllocationId + "|"
                + this.TransactionCodeId
                );
        }
    }
}
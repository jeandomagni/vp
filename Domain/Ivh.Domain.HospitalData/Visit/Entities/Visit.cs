﻿namespace Ivh.Domain.HospitalData.Visit.Entities
{
    using System;
    using System.Collections.Generic;
    using BillingSystem.Entities;
    using Common.Base.Utilities.Helpers;
    using Ivh.Application.Base.Common.Interfaces.Entities.HsVisit;

    public class Visit : IHsVisit
    {
        public virtual int VisitId { get; set; }
        public virtual int HsGuarantorId { get; set; }
        public virtual int BillingSystemId { get; set; }
        public virtual string SourceSystemKey { get; set; }
        public virtual string SourceSystemKeyDisplay { get; set; }
        public virtual string VisitDescription { get; set; }
        public virtual DateTime? AdmitDate { get; set; }
        public virtual DateTime? DischargeDate { get; set; }
        public virtual decimal HsCurrentBalance { get; set; }
        public virtual decimal InsuranceBalance { get; set; }
        public virtual decimal SelfPayBalance { get; set; }
        public virtual DateTime? FirstSelfPayDate { get; set; }
        public virtual string PatientFirstName { get; set; }
        public virtual string PatientLastName { get; set; }
        public virtual DateTime? PatientDOB { get; set; }
        //public virtual SelfPayClass? SelfPayClass { get; set; }
        public virtual string BillingApplication { get; set; }
        public virtual HsBillingSystem BillingSystem { get; set; }

        public virtual IList<VisitTransaction> VisitTransactions { get; set; }
        public virtual PrimaryInsuranceType PrimaryInsuranceType { get; set; }
        public virtual RevenueWorkCompany RevenueWorkCompany { get; set; }
        public virtual PatientType PatientType { get; set; }
        public virtual LifeCycleStage LifeCycleStage { get; set; }
        public virtual IList<VisitInsurancePlan> VisitInsurancePlans { get; set; }
        public virtual IList<VisitPaymentAllocation> VisitPaymentAllocations { get; set; }
        public virtual Facility Facility { get; set; }

        public virtual bool? IsPatientGuarantor { get; set; }
        public virtual bool? IsPatientMinor { get; set; }
        public virtual bool? InterestRefund { get; set; }
        public virtual bool? InterestZero { get; set; }
        public virtual int? ServiceGroupId { get; set; }
        public virtual int? PaymentPriority { get; set; }
        public virtual int? AgingCount { get; set; }
        public virtual bool? OnPrePaymentPlan { get; set; }
        public virtual bool? VpEligible { get; set; }
        public virtual bool? BillingHold { get; set; }
        public virtual bool? Redact { get; set; }
        public virtual int?  AgingTierId { get; set; }//NOT SURE THIS GOES HERE. Pending Aging Service Definition.
        public virtual int? DataLoadId { get; set; }
        public virtual DateTime? DataChangeDate { get; set; }
        public virtual bool? EvaluateForPCSegmentation { get; set; }

        public virtual string PatientLastNameFirstName => FormatHelper.LastNameFirstName(this.PatientLastName, this.PatientFirstName);
        public virtual string PatientFirstNameLastName => FormatHelper.FirstNameLastName(this.PatientFirstName, this.PatientLastName);
    }
}
﻿namespace Ivh.Domain.HospitalData.Visit.Entities
{
    public class TransactionCode
    {
        public virtual int TransactionCodeId { get; set; }
        public virtual string SourceSystemKey { get; set; }
        public virtual string TransactionCodeName { get; set; }
        public virtual int VpTransactionTypeId { get; set; }
        public virtual int BillingSystemId { get; set; }
        public virtual short? AmountMultiplier { get; set; }
        public virtual string TranSskAppend { get; set; }

    }
}

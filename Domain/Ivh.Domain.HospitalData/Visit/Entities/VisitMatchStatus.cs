﻿namespace Ivh.Domain.HospitalData.Visit.Entities
{
    using System;
    using Common.VisitPay.Enums;

    public class VisitMatchStatus
    {
        public VisitMatchStatus()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int VisitMatchStatusId { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual int HsGuarantorId { get; set; }
        public virtual int VpGuarantorId { get; set; }
        public virtual int VisitId { get; set; }
        public virtual HsGuarantorMatchStatusEnum HsGuarantorMatchStatus { get; set; }
    }
}

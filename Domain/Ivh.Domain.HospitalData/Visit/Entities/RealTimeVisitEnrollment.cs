﻿namespace Ivh.Domain.HospitalData.Visit.Entities
{
    using System;

    public class RealTimeVisitEnrollment
    {
        public virtual int RealTimeVisitEnrollmentId { get; set; }
        public virtual int VisitId { get; set; }
        public virtual int HsGuarantorId { get; set; }
        public virtual int BillingSystemId { get; set; }
        public virtual bool BillingHold { get; set; }
        public virtual DateTime CreateDate { get; set; }
        public virtual DateTime? OverrideExpireDate { get; set; }
    }
}
﻿namespace Ivh.Domain.HospitalData.Visit.Entities
{
    using System;

    public class VisitInsurancePlanChange
    {
        public virtual int ChangeEventId { get; set; }
        public virtual int DataLoadId { get; set; }
        public virtual int InsurancePlanId { get; set; }
        public virtual int VisitId { get; set; }
        public virtual VisitChange VisitChange { get; set; }
        public virtual int PayOrder { get; set; }
        public virtual InsurancePlan InsurancePlan { get; set; }

        public override bool Equals(Object obj)
        {
            VisitInsurancePlanChange t = obj as VisitInsurancePlanChange;
            if (t == null)
                return false;
            if (this.DataLoadId == t.DataLoadId
                && this.ChangeEventId == t.ChangeEventId
                && (
                    (this.VisitChange != null && t.VisitChange != null && this.VisitChange.ChangeEventId == t.VisitChange.ChangeEventId)
                    || (this.VisitChange == null && t.VisitChange == null)
                )
                && this.VisitId == t.VisitId
                && this.InsurancePlanId == t.InsurancePlanId
                && this.PayOrder == t.PayOrder
            )
                return true;
            return false;
        }

        public override int GetHashCode()
        {
            return (
                this.DataLoadId + "|"
                                + this.ChangeEventId + "|"
                                + (this.VisitChange != null ? this.VisitChange.ChangeEventId.ToString() : "") + "|"
                                + this.VisitId + "|"
                                + this.InsurancePlanId + "|"
                                + this.PayOrder
            ).GetHashCode();
        }

    }
}
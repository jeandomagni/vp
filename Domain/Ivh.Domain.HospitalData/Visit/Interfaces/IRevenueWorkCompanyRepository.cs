﻿namespace Ivh.Domain.HospitalData.Visit.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IRevenueWorkCompanyRepository : IRepository<RevenueWorkCompany>
    {
        IList<RevenueWorkCompany> GetAllRevenueWorkCompany();
    }
}
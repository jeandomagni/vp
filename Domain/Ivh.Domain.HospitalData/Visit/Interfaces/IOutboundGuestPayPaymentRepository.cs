﻿namespace Ivh.Domain.HospitalData.Visit.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IOutboundGuestPayPaymentRepository : IRepository<VpOutboundGuestPayPayment>
    {
        IList<VpOutboundGuestPayPayment> GetVpOutboundGuestPayPayment(int vpPaymentId, int vpPaymentUnitId);
    }
}
﻿namespace Ivh.Domain.HospitalData.Visit.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IOutboundVisitTransactionRepository : IRepository<VpOutboundVisitTransaction>
    {
        IList<VpOutboundVisitTransaction> GetOutboundVisitTransactionsByVisit(int visitId);
        IList<VpOutboundVisitTransaction> GetUnclearedOutboundVisitTransactions();
        IList<VpOutboundVisitTransaction> GetUnclearedOutboundVisitTransactions(int visitId);
    }
}
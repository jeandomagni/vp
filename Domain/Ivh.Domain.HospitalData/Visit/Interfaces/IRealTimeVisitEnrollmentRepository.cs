﻿namespace Ivh.Domain.HospitalData.Visit.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IRealTimeVisitEnrollmentRepository : IRepository<RealTimeVisitEnrollment>
    {
        bool PendingRealtimeEnrollmentExists(int visitId, int billingSystemId, int hsGuarantorId);
    }
}
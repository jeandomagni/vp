﻿namespace Ivh.Domain.HospitalData.Visit.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IVisitTransactionService : IDomainService
    {
        IList<TransactionCode> GetTransactionCodes();
        IList<VpTransactionType> GetTransactionTypes();
    }
}
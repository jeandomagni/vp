﻿namespace Ivh.Domain.HospitalData.Visit.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IVisitRepository : IRepository<Visit>
    {
        Visit GetVisitWithBillingSystem(int visitId);
        Visit GetVisit(int billingSystemId, string sourceSystemKey);
        IReadOnlyList<Visit> GetVisitsForGuarantor(int hsGuarantorId);
        bool IsVisitSuspended(int visitId);
        bool GuarantorHasBadDebtVisits(IEnumerable<int> hsGuarantorIds);
        bool GuarantorHasVisitOnPrePaymentPlan(IEnumerable<int> hsGuarantorIds);
    }
}
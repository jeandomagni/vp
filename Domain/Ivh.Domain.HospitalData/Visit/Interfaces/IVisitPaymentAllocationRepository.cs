﻿namespace Ivh.Domain.HospitalData.Visit.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IVisitPaymentAllocationRepository : IRepository<VisitPaymentAllocation>
    {
    }
}
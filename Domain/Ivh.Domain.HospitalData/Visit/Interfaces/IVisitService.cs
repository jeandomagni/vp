﻿namespace Ivh.Domain.HospitalData.Visit.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IVisitService : IDomainService
    {
        Visit GetVisit(int visitId);
        Visit GetVisit(int billingSystemId, string sourceSystemKey);
        IReadOnlyList<Visit> GetVisitsForGuarantor(int hsGuarantorId);

        void SaveVisitTransaction(VisitTransaction visitTransaction);
        void SaveVisitTransactions(IEnumerable<VisitTransaction> visitTransactions);

        IList<PrimaryInsuranceType> GetPrimaryInsuranceTypes();
        IList<RevenueWorkCompany> GetRevenueWorkCompanies();
        IList<LifeCycleStage> GetLifeCycleStages();
        IList<PatientType> GetPatientTypes();
        IList<InsurancePlan> GetInsurancePlans();
        IList<Facility> GetFacilities();

        bool GuarantorHasBadDebtVisits(IEnumerable<int> hsGuarantorIds);
        bool GuarantorHasVisitOnPrePaymentPlan(IEnumerable<int> hsGuarantorIds);
    }
}
﻿namespace Ivh.Domain.HospitalData.Visit.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IOutboundVisitService : IDomainService
    {
        void CreateOutboundVisit(VpOutboundVisit vpOutbound);
        IList<VpOutboundVisit> GetOutboundVisitsByVisit(int visitId);
    }
}
﻿namespace Ivh.Domain.HospitalData.Visit.Interfaces
{
    using System;
    using Common.Base.Interfaces;
    using Common.VisitPay.Messages.HospitalData;

    public interface IOutboundGuestPayPaymentService : IDomainService
    {
        
        void CreateOutboundGuestPayPayment(GuestPayPaymentMessage guestPayPaymentMessage);
        void VoidOutboundGuestPayPayment(GuestPayPaymentVoidMessage guestPayPaymentVoidMessage);
    }
}
﻿namespace Ivh.Domain.HospitalData.Visit.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;
    using VpGuarantor.Entities;

    public interface IVisitMatchStatusRepository : IRepository<VisitMatchStatus>
    {
        VisitMatchStatus GetVisitMatchStatus(VpGuarantorHsMatch vpGuarantorHsMatch, int visitId);
        IList<VisitMatchStatus> GetAllVisitMatchStatues(IList<VpGuarantorHsMatch> vpGuarantorHsMatches);
    }
}
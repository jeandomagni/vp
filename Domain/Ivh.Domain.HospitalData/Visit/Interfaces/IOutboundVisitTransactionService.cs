﻿namespace Ivh.Domain.HospitalData.Visit.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Common.VisitPay.Messages.FinanceManagement;
    using Common.VisitPay.Messages.HospitalData;
    using Entities;

    public interface IOutboundVisitTransactionService : IDomainService
    {
        IList<VpOutboundVisitTransaction> GetOutboundVisitTransactionsByVisit(int visitId);
        void CreateOutboundVisitTransactions(IList<PaymentAllocationMessage> messages);
        void CreateOutboundVisitTransactions(IList<InterestEventMessage> messages);
    }
}
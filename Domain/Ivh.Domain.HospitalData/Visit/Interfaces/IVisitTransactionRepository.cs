﻿namespace Ivh.Domain.HospitalData.Visit.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IVisitTransactionRepository : IRepository<VisitTransaction>
    {
        IList<TransactionCode> GetTransactionCodes();
        IList<VpTransactionType> GetTransactionTypes();
        IList<VisitTransaction> GetVisitTransactions(int hsGuarantorId, ICollection<VpTransactionTypeEnum> vpTransactionTypes, DateTime? postDateBegin = null, DateTime? postDateEnd = null);
        IList<VisitTransaction> GetGuarantorAccountTransactions(int visitId, DateTime? postDateBegin = null);
    }
}
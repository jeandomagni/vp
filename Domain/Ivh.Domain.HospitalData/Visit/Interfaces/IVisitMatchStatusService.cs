﻿namespace Ivh.Domain.HospitalData.Visit.Interfaces
{
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;

    public interface IVisitMatchStatusService : IDomainService
    {
        void UnmatchVpVisit(int hsGuarantorId, int vpGuarantorId, int billingSystemId, string sourceSystemKey,int vpVisitId, HsGuarantorMatchStatusEnum hsGuarantorMatchStatus);
    }
}
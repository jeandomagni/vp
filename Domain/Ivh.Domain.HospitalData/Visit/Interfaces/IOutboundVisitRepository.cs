﻿namespace Ivh.Domain.HospitalData.Visit.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IOutboundVisitRepository : IRepository<VpOutboundVisit>
    {
        IList<VpOutboundVisit> GetOutboundVisitsByVisit(int visitId);
    }
}
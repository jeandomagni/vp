﻿namespace Ivh.Domain.HospitalData.Visit.Interfaces
{
    using Common.Base.Interfaces;
    using Common.VisitPay.Messages.HospitalData;

    public interface IRealTimeVisitEnrollmentService : IDomainService
    {
        void ProcessRealTimeVisitEnrollmentMessage(RealTimeVisitEnrollmentMessage realTimeVisitEnrollmentMessage);
    }
}
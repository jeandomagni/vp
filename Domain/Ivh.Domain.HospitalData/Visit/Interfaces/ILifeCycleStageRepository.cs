namespace Ivh.Domain.HospitalData.Visit.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface ILifeCycleStageRepository : IRepository<LifeCycleStage>
    {
        IList<LifeCycleStage> GetAllLifeCycleStage();
    }
}
﻿namespace Ivh.Domain.HospitalData.HsGuarantor.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IHsGuarantorService : IDomainService
    {
        HsGuarantor GetHsGuarantor(int hsGuarantorId);
        IReadOnlyList<HsGuarantorSearchResult> GetHsGuarantors(HsGuarantorFilter filter);
        HsGuarantor AddHsGuarantor(HsGuarantor hsGuarantor);
        IList<string> GetEligibleVisitsAfterEnrollment(string hsGuarantorSourceSystemKey, int billingSystem);
        IReadOnlyList<HsGuarantor> GetHsGuarantors(int[] hsGuarantorIds, string hsGuarantorId, string firstName, string lastName, string ssn4, string dob);
    }
}

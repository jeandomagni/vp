﻿namespace Ivh.Domain.HospitalData.HsGuarantor.Interfaces
{
    using System.Collections.Generic;
    using Entities;

    public interface IVpOutboundHsGuarantorRepository
    {
        void Insert(VpOutboundHsGuarantor vpOoutboundHsGuarantor);
        IReadOnlyList<VpOutboundHsGuarantor> GetOutboundEvents(int hsGuarantorId);
    }
}
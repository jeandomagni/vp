﻿namespace Ivh.Domain.HospitalData.HsGuarantor.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Common.VisitPay.Messages.HospitalData;
    using Entities;

    public interface IVpOutboundHsGuarantorService : IDomainService
    {
        void Insert(VpOutboundHsGuarantor vpOutboundHsGuarantor);
        IReadOnlyList<VpOutboundHsGuarantor> GetOutboundEvents(int hsGuarantorId);
        void CreateOutboundPayment(PaymentMessage message);
    }
}
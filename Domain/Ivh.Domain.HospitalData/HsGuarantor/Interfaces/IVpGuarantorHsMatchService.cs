﻿namespace Ivh.Domain.HospitalData.HsGuarantor.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;
    using VpGuarantor.Entities;

    public interface IVpGuarantorHsMatchService : IDomainService
    {
        IList<VpGuarantorHsMatch> GetVpGuarantorHsMatches(int hsGuarantorId);
        IList<VpGuarantorHsMatch> GetHsGuarantorVpMatches(int vpGuarantorId);
        IList<VpGuarantorHsMatch> GetVpGuarantorHsMatches(int hsGuarantorId, int vpGuarantorId);
        void UnmatchVpGuarantorHsMatch(int hsGuarantorId, int vpGuarantorId, HsGuarantorMatchStatusEnum hsGuarantorMatchStatus);
        IList<HsGuarantor> GetGuarantorsByVpGuarantorId(int vpGuarantorId);
        void InsertOrUpdate(VpGuarantorHsMatch vpGuarantorHsMatch);
    }
}
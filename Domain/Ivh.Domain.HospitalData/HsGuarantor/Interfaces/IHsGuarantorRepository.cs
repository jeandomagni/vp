﻿namespace Ivh.Domain.HospitalData.HsGuarantor.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;

    public interface IHsGuarantorRepository : IRepository<HsGuarantor>
    {
        IReadOnlyList<HsGuarantorSearchResult> GetGuarantors(HsGuarantorFilter filter);
        HsGuarantor GetHsGuarantor(int billingSystemId, string sourceSystemKey);
        IList<string> GetEligibleVisitsAfterEnrollment(string hsGuarantorSourceSystemKey, int billingSystem);
        IReadOnlyList<HsGuarantor> GetHsGuarantors(int[] hsGuarantorIds, string hsGuarantorId, string firstName, string lastName, string ssn4, string dob);
    }
}
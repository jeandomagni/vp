﻿namespace Ivh.Domain.HospitalData.HsGuarantor.Interfaces
{
    using Common.Base.Interfaces;
    using Entities;

    public interface IHsGuarantorChangeRepository : IRepository<HsGuarantorChange>
    {
    }
}

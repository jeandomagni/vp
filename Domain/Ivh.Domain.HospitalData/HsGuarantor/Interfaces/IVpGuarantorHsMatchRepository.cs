﻿namespace Ivh.Domain.HospitalData.HsGuarantor.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Common.VisitPay.Enums;
    using Entities;
    using VpGuarantor.Entities;

    public interface IVpGuarantorHsMatchRepository : IRepository<VpGuarantorHsMatch>
    {
        IList<VpGuarantorHsMatch> GetVpGuarantorHsMatches(int hsGuarantorId, int vpGuarantorId);
        IList<VpGuarantorHsMatch> GetVpGuarantorHsMatches(int hsGuarantorId);
        IList<VpGuarantorHsMatch> GetHsGuarantorVpMatches(int vpGuarantorId);
        IList<VpGuarantorHsMatch> GetById(int hsGuarantorId, int vpGuarantorId, IList<HsGuarantorMatchStatusEnum> hsGuarantorMatchStatuses = null);
        IList<HsGuarantor> GetGuarantorsByVpGuarantorId(int vpGuarantorId, bool activeMatchesOnly = false);
    }
}
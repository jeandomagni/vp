﻿namespace Ivh.Domain.HospitalData.HsGuarantor.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Entities;
    using Interfaces;
    using IHsGuarantorService = Interfaces.IHsGuarantorService;

    public class HsGuarantorService : DomainService, IHsGuarantorService
    {
        private readonly Lazy<IHsGuarantorRepository> _hsGuarantorRepository;
        private readonly Lazy<IVpGuarantorHsMatchService> _vpGuarantorHsMatchService;

        public HsGuarantorService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IHsGuarantorRepository> hsGuarantorRepository,
            Lazy<IVpGuarantorHsMatchService> vpGuarantorHsMatchService) : base(serviceCommonService)
        {
            this._hsGuarantorRepository = hsGuarantorRepository;
            this._vpGuarantorHsMatchService = vpGuarantorHsMatchService;
        }

        public HsGuarantor GetHsGuarantor(int hsGuarantorId)
        {
            return this._hsGuarantorRepository.Value.GetById(hsGuarantorId);
        }

        public IReadOnlyList<HsGuarantorSearchResult> GetHsGuarantors(HsGuarantorFilter filter)
        {
            return this._hsGuarantorRepository.Value.GetGuarantors(filter);
        }

        public HsGuarantor AddHsGuarantor(HsGuarantor hsGuarantor)
        {
            this._hsGuarantorRepository.Value.InsertOrUpdate(hsGuarantor);
            return hsGuarantor;
        }

        public IList<string> GetEligibleVisitsAfterEnrollment(string hsGuarantorSourceSystemKey, int billingSystem)
        {
            return this._hsGuarantorRepository.Value.GetEligibleVisitsAfterEnrollment(hsGuarantorSourceSystemKey, billingSystem);
        }

        public IReadOnlyList<HsGuarantor> GetHsGuarantors(int[] hsGuarantorIds, string sourceSystemKey, string firstName, string lastName, string ssn4, string birthDate)
        {
            IReadOnlyList<HsGuarantor> guarantors = this._hsGuarantorRepository.Value.GetHsGuarantors(hsGuarantorIds, sourceSystemKey, firstName, lastName, ssn4, birthDate);
            return guarantors.Any(x => this._vpGuarantorHsMatchService.Value.GetVpGuarantorHsMatches(x.HsGuarantorId).Count > 0) ? new List<HsGuarantor>() : guarantors;
        }
    }
}

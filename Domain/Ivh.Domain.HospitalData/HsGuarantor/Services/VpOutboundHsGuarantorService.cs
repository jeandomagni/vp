﻿namespace Ivh.Domain.HospitalData.HsGuarantor.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.HospitalData;
    using Entities;
    using Interfaces;
    using VpGuarantor.Entities;

    public class VpOutboundHsGuarantorService : DomainService, IVpOutboundHsGuarantorService
    {
        private readonly IVpOutboundHsGuarantorRepository _vpOutboundHsGuarantorRepository;
        private readonly IVpGuarantorHsMatchService _vpGuarantorHsMatchService;

        public VpOutboundHsGuarantorService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            IVpOutboundHsGuarantorRepository vpOutboundHsGuarantorRepository, 
            IVpGuarantorHsMatchService vpGuarantorHsMatchService) : base(serviceCommonService)
        {
            this._vpOutboundHsGuarantorRepository = vpOutboundHsGuarantorRepository;
            this._vpGuarantorHsMatchService = vpGuarantorHsMatchService;
        }

        public void Insert(VpOutboundHsGuarantor vpOutboundHsGuarantor)
        {
            this._vpOutboundHsGuarantorRepository.Insert(vpOutboundHsGuarantor);
        }

        public IReadOnlyList<VpOutboundHsGuarantor> GetOutboundEvents(int hsGuarantorId)
        {
            return this._vpOutboundHsGuarantorRepository.GetOutboundEvents(hsGuarantorId);
        }

        public void CreateOutboundPayment(PaymentMessage message)
        {
            IList<VpGuarantorHsMatch> vpGuarantorHsMatches = this._vpGuarantorHsMatchService.GetHsGuarantorVpMatches(message.VpGuarantorId);
            if (!vpGuarantorHsMatches.Any())
                return;

            foreach (VpOutboundHsGuarantor outbound in vpGuarantorHsMatches.Select(vpGuarantorHsMatch => new VpOutboundHsGuarantor
            {
                VpOutboundHsGuarantorMessageType = this.TranslatePaymentType((PaymentTypeEnum) message.VpPaymentTypeId, (PaymentStatusEnum)message.VpPaymentStatusId, (PaymentMethodTypeEnum) message.VpPaymentMethodTypeId),
                HsGuarantorId = vpGuarantorHsMatch.HsGuarantorId,
                InsertDate = DateTime.UtcNow,
                Notes = null,
                ProcessedDate = null,
                VpGuarantorId = message.VpGuarantorId,
                VpPaymentId = message.VpPaymentId,
                FullPaymentAmount = message.FullPaymentAmount,
                ActualPaymentDate = message.ActualPaymentDate.HasValue ? message.ActualPaymentDate.Value.Date : (DateTime?)null,
                OffsetPaymentId = message.OffsetPaymentId,
                OffsetPaymentAmount = message.OffsetPaymentAmount,
                OffsetPaymentDate = message.OffsetPaymentDate.HasValue ? message.OffsetPaymentDate.Value.Date : (DateTime?)null,
                AchSettlementType = message.AchSettlementType ?? AchSettlementTypeEnum.None
            }))
            {
                this.Insert(outbound);
            }
        }

        private VpOutboundHsGuarantorMessageTypeEnum TranslatePaymentType(PaymentTypeEnum paymentType, PaymentStatusEnum paymentStatus, PaymentMethodTypeEnum paymentMethodType)
        {
            if (paymentType == PaymentTypeEnum.Void)
                return VpOutboundHsGuarantorMessageTypeEnum.VppVoid;

            if (paymentType == PaymentTypeEnum.Refund || paymentType == PaymentTypeEnum.PartialRefund)
                return VpOutboundHsGuarantorMessageTypeEnum.VppRefund;

            if ((paymentMethodType == PaymentMethodTypeEnum.AchChecking || paymentMethodType == PaymentMethodTypeEnum.AchSavings) && paymentStatus == PaymentStatusEnum.ClosedFailed)
                return VpOutboundHsGuarantorMessageTypeEnum.VppAchReturn;

            return VpOutboundHsGuarantorMessageTypeEnum.VppPayment;
        }
    }
}
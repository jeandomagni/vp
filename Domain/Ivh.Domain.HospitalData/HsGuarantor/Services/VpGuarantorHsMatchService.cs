﻿namespace Ivh.Domain.HospitalData.HsGuarantor.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Interfaces;
    using Base.Services;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;
    using Entities;
    using Interfaces;
    using VpGuarantor.Entities;

    public class VpGuarantorHsMatchService : DomainService, IVpGuarantorHsMatchService
    {
        private readonly Lazy<IVpGuarantorHsMatchRepository> _vpGuarantorHsMatchRepository;
        private readonly Lazy<IVpOutboundHsGuarantorService> _vpOutboundHsGuarantorService;

        public VpGuarantorHsMatchService(
            Lazy<IDomainServiceCommonService> serviceCommonService,
            Lazy<IVpGuarantorHsMatchRepository> vpGuarantorHsMatchRepository,
            Lazy<IVpOutboundHsGuarantorService> vpOutboundHsGuarantorService) : base(serviceCommonService)
        {
            this._vpGuarantorHsMatchRepository = vpGuarantorHsMatchRepository;
            this._vpOutboundHsGuarantorService = vpOutboundHsGuarantorService;
        }
        public IList<VpGuarantorHsMatch> GetVpGuarantorHsMatches(int hsGuarantorId, int vpGuarantorId)
        {
            return this._vpGuarantorHsMatchRepository.Value.GetById(hsGuarantorId, vpGuarantorId);
        }
        public IList<VpGuarantorHsMatch> GetVpGuarantorHsMatches(int hsGuarantorId)
        {
            return this._vpGuarantorHsMatchRepository.Value.GetVpGuarantorHsMatches(hsGuarantorId);
        }

        public IList<VpGuarantorHsMatch> GetHsGuarantorVpMatches(int vpGuarantorId)
        {
            return this._vpGuarantorHsMatchRepository.Value.GetHsGuarantorVpMatches(vpGuarantorId);
        }

        public void UnmatchVpGuarantorHsMatch(int hsGuarantorId, int vpGuarantorId, HsGuarantorMatchStatusEnum hsGuarantorMatchStatus)
        {
            IList<VpGuarantorHsMatch> vpGuarantorHsMatches = this._vpGuarantorHsMatchRepository.Value.GetById(hsGuarantorId, vpGuarantorId, HsGuarantorMatchStatusEnum.Matched.ToListOfOne());
            if (vpGuarantorHsMatches.IsNotNullOrEmpty())
            {
                VpGuarantorHsMatch vpGuarantorHsMatch = vpGuarantorHsMatches.First();

                vpGuarantorHsMatch.HsGuarantorMatchStatus = hsGuarantorMatchStatus;
                vpGuarantorHsMatch.UnmatchedOn = DateTime.UtcNow;
                vpGuarantorHsMatch.IsActive = false;
                vpGuarantorHsMatch.MatchOutboundLoadTrackerId = vpGuarantorHsMatch.OutboundLoadTrackerId;
                vpGuarantorHsMatch.OutboundLoadTrackerId = null;


                this._vpGuarantorHsMatchRepository.Value.InsertOrUpdate(vpGuarantorHsMatch);

                this._vpOutboundHsGuarantorService.Value.Insert(new VpOutboundHsGuarantor
                {
                    InsertDate = DateTime.UtcNow,
                    HsGuarantorId = hsGuarantorId,
                    VpGuarantorId = vpGuarantorId,
                    VpOutboundHsGuarantorMessageType = VpOutboundHsGuarantorMessageTypeEnum.VpGuarantorUnmatch
                });

            }
        }

        public IList<HsGuarantor> GetGuarantorsByVpGuarantorId(int vpGuarantorId)
        {
            return this._vpGuarantorHsMatchRepository.Value.GetGuarantorsByVpGuarantorId(vpGuarantorId);
        }

        public void InsertOrUpdate(VpGuarantorHsMatch vpGuarantorHsMatch)
        {
            this._vpGuarantorHsMatchRepository.Value.InsertOrUpdate(vpGuarantorHsMatch);
        }
    }
}

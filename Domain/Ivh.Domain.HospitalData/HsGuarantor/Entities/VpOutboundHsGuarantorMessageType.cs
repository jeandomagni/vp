﻿namespace Ivh.Domain.HospitalData.HsGuarantor.Entities
{
    public class VpOutboundHsGuarantorMessageType
    {
        public virtual int VpOutboundHsGuarantorMessageTypeId { get; set; }
        public virtual string VpOutboundHsGuarantorMessageName { get; set; }
        public virtual string VpOutboundHsGuarantorMessageDescription { get; set; }
    }
}
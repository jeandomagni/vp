﻿namespace Ivh.Domain.HospitalData.HsGuarantor.Entities
{
    using System;
    using Change.Entities;
    using Common.VisitPay.Attributes;
    using Ivh.Common.Base.Utilities.Helpers;

    public class HsGuarantorChange
    {
        // IDENTITY
        public virtual int ChangeEventId { get; set; }

        //UNIQUE CONSTRAINT
        public virtual int DataLoadId { get; set; }
        public virtual int HsGuarantorId { get; set; }

        public virtual ChangeEvent ChangeEvent { get; set; }
        public virtual int BillingSystemId { get; set; }
        public virtual string SourceSystemKey { get; set; }
        [GuarantorMatchField("FirstName")]
        public virtual string FirstName { get; set; }
        [GuarantorMatchField("LastName")]
        public virtual string LastName { get; set; }
        [GuarantorMatchField("DOB")]
        public virtual DateTime? DOB { get; set; }
        [GuarantorMatchField("SSN")]
        public virtual string SSN { get; set; }
        [GuarantorMatchField("SSN4")]
        public virtual int? SSN4 { get; set; }
        [GuarantorMatchField("AddressLine1")]
        public virtual string AddressLine1 { get; set; }
        [GuarantorMatchField("AddressLine2")]
        public virtual string AddressLine2 { get; set; }
        [GuarantorMatchField("City")]
        public virtual string City { get; set; }
        [GuarantorMatchField("StateProvince")]
        public virtual string StateProvince { get; set; }
        [GuarantorMatchField("PostalCode")]
        public virtual string PostalCode { get; set; }
        [GuarantorMatchField("Country")]
        public virtual string Country { get; set; }

        public virtual bool VpEligible { get; set; }
        public virtual int? GenderId { get; set; }
        public virtual string Email { get; set; }


        public virtual string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);
        public virtual string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);

    }
}
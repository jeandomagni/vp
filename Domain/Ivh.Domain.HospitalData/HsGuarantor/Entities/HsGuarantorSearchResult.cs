﻿using Ivh.Common.Base.Utilities.Helpers;
using System;

namespace Ivh.Domain.HospitalData.HsGuarantor.Entities
{
    public class HsGuarantorSearchResult
    {
        public virtual int HsGuarantorID { get; set; }
        public virtual int HsBillingSystemId { get; set; }
        public virtual string SourceSystemKey { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual int? VpGuarantorID { get; set; }
        public virtual int VisitCount { get; set; }
        public virtual string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);
        public virtual string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);
    }
}

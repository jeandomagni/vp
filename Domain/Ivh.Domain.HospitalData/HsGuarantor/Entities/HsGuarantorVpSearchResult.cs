﻿using Ivh.Common.Base.Utilities.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Domain.HospitalData.HsGuarantor.Entities
{
    public class HsGuarantorVpSearchResult
    {
        public virtual int HsGuarantorId { get; set; }
        public virtual int HsBillingSystemId { get; set; }
        public virtual string SourceSystemKey { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual DateTime? DOB { get; set; }
        public virtual string Ssn4 { get; set; }
        public virtual string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);
        public virtual string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);
    }
}

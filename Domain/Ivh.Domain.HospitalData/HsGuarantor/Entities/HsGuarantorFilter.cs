﻿namespace Ivh.Domain.HospitalData.HsGuarantor.Entities
{
    public class HsGuarantorFilter
    {
        public int? VPGID { get; set; }
        public string HsSourceKey { get; set; }
        public string LastName { get; set; }
        public bool VpRegistered { get; set; }
        public bool VpUnregistered { get; set; }
    }
}

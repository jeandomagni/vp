﻿using System;

namespace Ivh.Domain.HospitalData.HsGuarantor.Entities
{
    using System.Collections.Generic;
    using Common.Base.Utilities.Helpers;
    using Common.VisitPay.Attributes;
    using Ivh.Application.Base.Common.Interfaces.Entities.HsGuarantor;
    using VpGuarantor.Entities;

    public class HsGuarantor : IHsGuarantor
    {
        public virtual int HsGuarantorId { get; set; }
        public virtual int HsBillingSystemId { get; set; }
        public virtual string SourceSystemKey { get; set; }
        [GuarantorMatchField("FirstName")]
        public virtual string FirstName { get; set; }
        [GuarantorMatchField("LastName")]
        public virtual string LastName { get; set; }
        [GuarantorMatchField("DOB")]
        public virtual DateTime? DOB { get; set; }
        [GuarantorMatchField("SSN")]
        public virtual string SSN { get; set; }
        [GuarantorMatchField("SSN4")]
        public virtual string SSN4 { get; set; }
        [GuarantorMatchField("AddressLine1")]
        public virtual string AddressLine1 { get; set; }
        [GuarantorMatchField("AddressLine2")]
        public virtual string AddressLine2 { get; set; }
        [GuarantorMatchField("City")]
        public virtual string City { get; set; }
        [GuarantorMatchField("StateProvince")]
        public virtual string StateProvince { get; set; }
        /// <summary>
        /// may be zip or zip+4
        /// </summary>
        [GuarantorMatchField("PostalCode")]
        public virtual string PostalCode { get; set; }
        [GuarantorMatchField("Country")]
        public virtual string Country { get; set; }
        public virtual int? VpGuarantorId { get; set; }
        public virtual bool VpEligible { get; set; }
        public virtual int? GenderId { get; set; }
        public virtual string Email { get; set; }
        public virtual int? DataLoadId { get; set; }
        public virtual DateTime? DataChangeDate { get; set; }

        public virtual IList<VpGuarantorHsMatch> VpGuarantorHsMatches { get; set; }

        public virtual string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);
        public virtual string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);
    }
}

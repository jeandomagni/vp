﻿namespace Ivh.Domain.HospitalData.HsGuarantor.Entities
{
    using System;
    using Common.VisitPay.Enums;

    public class VpOutboundHsGuarantor
    {
        public VpOutboundHsGuarantor()
        {
            this.InsertDate = DateTime.UtcNow;
        }
        public virtual int VpOutboundHsGuarantorId { get; set; }
        public virtual int? HsGuarantorId { get; set; }
        public virtual string Notes { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual DateTime? ProcessedDate { get; set; }
        public virtual int? VpGuarantorId { get; set; }
        public virtual int? VpPaymentId { get; set; }
        public virtual decimal? FullPaymentAmount { get; set; }
        public virtual DateTime? ActualPaymentDate { get; set; }
        public virtual int? OffsetPaymentId { get; set; }
        public virtual decimal? OffsetPaymentAmount { get; set; }
        public virtual DateTime? OffsetPaymentDate { get; set; }
        public virtual AchSettlementTypeEnum AchSettlementType { get; set; }
        public virtual VpOutboundHsGuarantorMessageTypeEnum VpOutboundHsGuarantorMessageType { get; set; }
    }
}
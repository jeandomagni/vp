﻿namespace Ivh.Domain.HospitalData.HsGuarantor.Entities
{
    public struct HsGuarantorAcknowledgementInfo
    {
        public int VpGuarantorId { get; set; }
        public int HsGuarantorId { get; set; }
        public int HsBillingSystemId { get; set; }
        public string SourceSystemKey { get; set; }
    }
}
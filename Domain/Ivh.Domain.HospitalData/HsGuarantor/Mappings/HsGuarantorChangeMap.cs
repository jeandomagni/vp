﻿namespace Ivh.Domain.HospitalData.HsGuarantor.Mappings
{
    using Change.Entities;
    using Common.Data.Constants;
    using Entities;
    using FluentNHibernate.Mapping;

    public class HsGuarantorChangeMap : ClassMap<HsGuarantorChange>
    {
        public HsGuarantorChangeMap()
        {
            this.ReadOnly();
            this.Schema(Schemas.Cdi.Base);
            this.Table(nameof(HsGuarantorChange));

            this.Id((x => x.ChangeEventId));

            this.Map(x => x.DataLoadId).Not.Nullable();
            this.References<ChangeEvent>(x => x.ChangeEvent, "ChangeEventId").Not.Nullable().ReadOnly();
            this.Map(x => x.HsGuarantorId).Not.Nullable();
            this.Map(x => x.BillingSystemId).Not.Nullable();
            this.Map(x => x.SourceSystemKey).Not.Nullable();
            this.Map(x => x.FirstName).Length(250).Nullable();
            this.Map(x => x.LastName).Length(250).Nullable();
            this.Map(x => x.DOB).Nullable();
            this.Map(x => x.SSN).Length(50).Nullable();
            this.Map(x => x.SSN4).Nullable();
            this.Map(x => x.AddressLine1).Length(250).Nullable();
            this.Map(x => x.AddressLine2).Length(250).Nullable();
            this.Map(x => x.City).Length(250).Nullable();
            this.Map(x => x.StateProvince).Length(250).Nullable();
            this.Map(x => x.PostalCode).Length(250).Nullable();
            this.Map(x => x.Country).Length(250).Nullable();
            this.Map(x => x.VpEligible).Nullable();
            this.Map(x => x.GenderId).Nullable();
            this.Map(x => x.Email).Length(120).Nullable();
        }
    }
}
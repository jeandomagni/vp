﻿namespace Ivh.Domain.HospitalData.HsGuarantor.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class HsGuarantorMap : ClassMap<HsGuarantor>
    {
        public HsGuarantorMap()
        {
            this.Schema("base");
            this.Table("HsGuarantor");
            this.Id(x => x.HsGuarantorId).Column("HsGuarantorID").GeneratedBy.Sequence("base.HsGuarantorId_Sequence").Default("next value for base.HsGuarantorId_Sequence");
            this.Map(x => x.HsBillingSystemId).Column("HsBillingSystemID").Not.Nullable();
            this.Map(x => x.SourceSystemKey).Not.Nullable();
            this.Map(x => x.FirstName);
            this.Map(x => x.LastName);
            this.Map(x => x.DOB);
            this.Map(x => x.SSN);
            this.Map(x => x.SSN4);
            this.Map(x => x.AddressLine1);
            this.Map(x => x.AddressLine2);
            this.Map(x => x.City);
            this.Map(x => x.StateProvince);
            this.Map(x => x.PostalCode);
            this.Map(x => x.Country);
            this.Map(x => x.VpEligible).Nullable();
            this.Map(x => x.GenderId).Nullable();
            this.Map(x => x.Email).Nullable();
            this.Map(x => x.DataLoadId).Nullable();
            this.Map(x => x.DataChangeDate).Nullable();

            this.HasMany(x => x.VpGuarantorHsMatches)
                .KeyColumn("HsGuarantorId")
                .Inverse()
                .Not.LazyLoad()
                .BatchSize(50)
                .Cascade.All();
        }
    }
}

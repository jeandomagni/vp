﻿namespace Ivh.Domain.HospitalData.HsGuarantor.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class VpOutboundHsGuarantorMessageTypeMap : ClassMap<VpOutboundHsGuarantorMessageType>
    {
        public VpOutboundHsGuarantorMessageTypeMap()
        {
            this.Schema("Vp");
            this.Table("VpOutboundHsGuarantorMessageType");
            this.Id(x => x.VpOutboundHsGuarantorMessageTypeId);
            this.Map(x => x.VpOutboundHsGuarantorMessageDescription).Not.Nullable();
            this.Map(x => x.VpOutboundHsGuarantorMessageName).Not.Nullable();
        }
    }
}
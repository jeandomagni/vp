﻿namespace Ivh.Domain.HospitalData.HsGuarantor.Mappings
{
    using AutoMapper;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using Entities;

    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            // Automapper mapping go here.
            // Use this.CreateMap<,>().
            // DO NOT USE Mapper.CreateMap<,>().

            this.CreateMap<HsGuarantorAcknowledgementDto, HsGuarantorAcknowledgementInfo>();
            this.CreateMap<HsGuarantorAcknowledgementInfo, HsGuarantorAcknowledgementDto>();
        }
    }
}
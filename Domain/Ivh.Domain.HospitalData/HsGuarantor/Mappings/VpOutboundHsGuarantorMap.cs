﻿namespace Ivh.Domain.HospitalData.HsGuarantor.Mappings
{
    using Common.VisitPay.Enums;
    using Entities;
    using FluentNHibernate.Mapping;

    public class VpOutboundHsGuarantorMap : ClassMap<VpOutboundHsGuarantor>
    {
        public VpOutboundHsGuarantorMap()
        {
            this.Schema("Vp");
            this.Table("VpOutboundHsGuarantor");
            this.Id(x => x.VpOutboundHsGuarantorId);
            this.Map(x => x.HsGuarantorId).Nullable();
            this.Map(x => x.VpOutboundHsGuarantorMessageType).Column("VpOutboundHsGuarantorMessageTypeId").CustomType<VpOutboundHsGuarantorMessageTypeEnum>();
            this.Map(x => x.Notes);
            this.Map(x => x.InsertDate);
            this.Map(x => x.ProcessedDate);
            this.Map(x => x.VpGuarantorId).Nullable();
            this.Map(x => x.VpPaymentId).Nullable();
            this.Map(x => x.FullPaymentAmount).Nullable();
            this.Map(x => x.ActualPaymentDate).Nullable();
            this.Map(x => x.OffsetPaymentId).Nullable();
            this.Map(x => x.OffsetPaymentAmount).Nullable();
            this.Map(x => x.OffsetPaymentDate).Nullable();
            this.Map(x => x.AchSettlementType).CustomType<AchSettlementTypeEnum>().Column("AchSettlementTypeId").Nullable();
        }
    }
}
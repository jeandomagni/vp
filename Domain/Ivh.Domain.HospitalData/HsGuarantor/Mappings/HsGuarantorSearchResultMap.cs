﻿namespace Ivh.Domain.HospitalData.HsGuarantor.Mappings
{
    using System;
    using System.Web.WebSockets;
    using Entities;
    using  Ivh.Domain.HospitalData.VpGuarantor.Entities;
    using FluentNHibernate.Mapping;

    public class HsGuarantorSearchResultMap : ClassMap<HsGuarantorSearchResult>
    {
        public HsGuarantorSearchResultMap()
        {
            this.Id(x => x.HsGuarantorID);
            this.Map(x => x.HsBillingSystemId);
            this.Map(x => x.SourceSystemKey);
            this.Map(x => x.FirstName);
            this.Map(x => x.LastName);
            this.Map(x => x.VisitCount);
            this.Map(x => x.VpGuarantorID).Nullable();
        }
    }
}

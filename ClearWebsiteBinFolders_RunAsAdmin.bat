del "%~dp0Presentation\Web\Ivh.Web.AuthorizationServer\bin\*.*?"
del "%~dp0Presentation\Web\Ivh.Web.Client\bin\*.*?"
del "%~dp0Presentation\Web\Ivh.Web.ClientDataApi\bin\*.*?"
del "%~dp0Presentation\Web\Ivh.Web.Express\bin\*.*?"
del "%~dp0Presentation\Web\Ivh.Web.GuestPay\bin\*.*?"
del "%~dp0Presentation\Web\Ivh.Web.HttpRedirect\bin\*.*?"
del "%~dp0Presentation\Web\Ivh.Web.Patient\bin\*.*?"
del "%~dp0Presentation\Web\Ivh.Web.QATools\bin\*.*?"
del "%~dp0Presentation\Web\Ivh.Web.SendMailWebHookEvents\bin\*.*?"
del "%~dp0Presentation\Web\Ivh.Web.SettingsManager\bin\*.*?"

set folder="%UserProfile%\AppData\Local\Temp\Temporary ASP.NET Files\vs"
cd /d %folder% || exit
set /p UserConfirmation=Are you sure you want to delete %folder% (y/n)?
IF /I "%UserConfirmation%" neq "Y" goto end
for /F "delims=" %%i in ('dir /b') do (rmdir "%%i" /s/q || del "%%i" /s/q)

:end
pause
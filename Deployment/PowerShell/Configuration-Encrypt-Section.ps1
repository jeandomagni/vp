param(
	[Parameter(Mandatory=$true)] [string]$websiteDirectory,
	[Parameter(Mandatory=$true)] [string]$sectionToEncrypt,
    [Parameter(Mandatory=$false)] [string]$provider = "",
    [Parameter(Mandatory=$false)] [string]$configFile = "web.config",
    [Parameter(Mandatory=$false)] [string]$otherFiles
)

$ErrorActionPreference = "Stop" 

function HandleError($message) {
	if (!$whatIf) {
		throw $message
	} else {
		Write-Output $message -Foreground Yellow
	}
}

$otherFiles = $otherFiles -split ',' | where {$_} | %{$_.Trim()}

Write-Output "Configuration - Encrypt .config"
Write-Output "WebsiteDirectory: $websiteDirectory"
Write-Output "SectionToEncrypt: $sectionToEncrypt"
Write-Output "Provider: $provider"
Write-Output "ConfigFile: $configFile"


if (!(Test-Path $websiteDirectory)) {
	HandleError "The directory $websiteDirectory must exist"
}
$websiteDirectory = [System.IO.Path]::GetFullPath((Join-Path (pwd) $websiteDirectory))
Write-Output "websiteDirectory: $websiteDirectory"
$configFilePath = Join-Path $websiteDirectory $configFile
Write-Output "configFilePath: $configFilePath"
if (!(Test-Path $configFilePath)) {
	HandleError "Specified file $configFile or a Web.Config file must exist in the directory $websiteDirectory"
}

$frameworkPath = [System.Runtime.InteropServices.RuntimeEnvironment]::GetRuntimeDirectory();
$regiis = "$frameworkPath\aspnet_regiis.exe"

if (!(Test-Path $regiis)) {
	HandleError "The tool aspnet_regiis does not exist in the directory $frameworkPath"
}

# Create a temp directory to work out of and copy our config file to web.config
$tempPath = Join-Path $websiteDirectory $([guid]::NewGuid()).ToString()
if (!$whatIf) {
	New-Item $tempPath -ItemType "directory"
} else {
	Write-Output "WhatIf: New-Item $tempPath -ItemType ""directory""" -Foreground Yellow
}

$tempFile = Join-Path $tempPath "web.config"
if (!$whatIf) {
	Copy-Item $configFilePath $tempFile
} else {
	Write-Output "WhatIf: Copy-Item $configFilePath $tempFile" -Foreground Yellow
}

Foreach($fileName in $otherFiles){
  if (!$whatIf) {
	 Copy-Item (Join-Path $websiteDirectory $fileName) (Join-Path $tempPath $fileName)
  } else {
	 Write-Output "WhatIf: Copy-Item $configFilePath $tempFile" -Foreground Yellow
  }
}

Write-Output "sectiontoencrypt: $sectionToEncrypt"
Write-Output "tempPath: $tempPath"
Write-Output "provider: $provider"
# Determine arguments
if ($provider) {
	$args = "-pef", $sectionToEncrypt, $tempPath, "-prov", $provider
} else {
	$args = "-pef", $sectionToEncrypt, $tempPath
}
Write-Output "Args: $args"
Write-Output "Will be called: $regiis $args"
& ls $tempPath
# Encrypt Web.Config file in directory
if (!$whatIf) {
	& $regiis $args
} else {
	Write-Output "WhatIf: $regiis $args" -Foreground Yellow
}

# Copy the web.config back to original file and delete the temp dir
if (!$whatIf) {
	Copy-Item $tempFile $configFilePath -Force

  Foreach($fileName in $otherFiles){
    if (!$whatIf) {
  	 Copy-Item (Join-Path $tempPath $fileName) (Join-Path $websiteDirectory $fileName) -Force
    } else {
  	 Write-Output "WhatIf: Copy-Item $configFilePath $tempFile" -Foreground Yellow
    }
  }

	Remove-Item $tempPath -Recurse
} else {
	Write-Output "WhatIf: Copy-Item $tempFile $configFilePath -Force" -Foreground Yellow
	Write-Output "WhatIf: Remove-Item $tempPath -Recurse" -Foreground Yellow
}

﻿# clear the queues
Add-Type -Path 'C:\Repos\visitpay\Source\packages\EasyNetQ.Management.Client.0.51.3.116\lib\net40\EasyNetQ.Management.Client.dll'
$rabbitUrl = 'http://localhost'
$rabbitUandP = 'guest'

$rabbitmgr = [EasyNetQ.Management.Client.ManagementClient]::new($rabbitUrl, $rabbitUandP, $rabbitUandP)

$rabbitmgr.GetQueues() | %{ $rabbitmgr.Purge($_) }

#$rabbitmgr.GetExchanges() | %{ $rabbitmgr.DeleteExchange($_) }

#$rabbitmgr.GetQueues() | %{ $rabbitmgr.DeleteQueue($_) }



# before mahoney made it fast
#
#$path = 'C:\Program Files\RabbitMQ Server\rabbitmq_server-3.6.10\sbin\rabbitmqctl'
#$vhostOutput = &$path list_vhosts
#$vhosts = $vhostOutput.split([environment]::NewLine)
#For ($v = 1; $v -lt $vhosts.Count; $v++) {
#    $queueOutput = &$path list_queues -p $vhosts[$v]
#    $queues = $queueOutput.Split([environment]::NewLine)
#    For ($q = 1; $q -lt $queues.Count; $q++) {
#        $name = $queues[$q] -split '\s+|\t+'
#        $count = [int]$name[1]
#        If ($count -gt 0) {
#            $o = &$path purge_queue $name[0] -p $vhosts[$v]
#            write-host $o
#        }
#    }
#}